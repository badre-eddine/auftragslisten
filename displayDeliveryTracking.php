<?php
	require_once('inc/requires.inc.php');

	if(!$arrGetUserRights["displayTracking"]) {
		#header('location: ' . PAGE_EXIT_LOCATION);
		#exit;
	}

	$jswindowMessage = '';
	$warningMessage = '';
	$errorMessage = '';
	$successMessage = '';
	$infoMessage = '';


	if($_POST["form.sendungsnummer"] != '') {
		$_POST["deliveryTrackingNumber"] = $_POST["form.sendungsnummer"];
	}

	if($_GET["searchTrackingNumber"] != ""){
		$_POST["deliveryTrackingNumber"] = $_GET["searchTrackingNumber"];
		$_POST["deliveryTrackingService"] = 'dpd';
	}

	if($_REQUEST["searchBoxDeliveryTracking"] != ""){
		$_POST["deliveryTrackingNumber"] = $_REQUEST["searchBoxDeliveryTracking"];
		$_POST["deliveryTrackingService"] = 'dpd';
	}

	$dbConnection = new DB_Connection();
	$db_open = $dbConnection->db_connect();
?>
<?php
	require_once('inc/headerHTML.inc.php');
	$thisTitle = "Tracking-Nummern verfolgen";

	$headerHTML = preg_replace("/{###TITLE###}/", strip_tags($thisTitle), $headerHTML);
	echo $headerHTML;

	require_once(FILE_MENUE_TOP);
?>

<div id="mainArea">
	<div id="mainContent">
		<?php require_once(FILE_MENUE_SIDEBAR); ?>
		<div id="contentArea">
			<a name="top"></a>
			<div id="contentAreaElements">
				<h1><?php if(SHOW_PAGE_TITLE_ICONS) { echo '<img src="' . PATH_ICONS_MENUE_TITLES . 'tracking.png" alt="" />'; } ?> <?php echo $thisTitle; ?></h1>

				<div class="actionButtonsArea">
					<table border="0" cellpadding="0" cellspacing="0" class="searchFilterContent">
						<tr>
							<td>
								<form name="formTrackingDatasDPD" method="post" action="<?php echo $_SERVER["PHP_SELF"]; ?>">
								<table border="0" cellpadding="0" cellspacing="0">
									<tr style="vertical-align:middle">
										<td style="vertical-align:middle">
											<label for="deliveryTrackingNumberDPD"><img src="layout/icons/dpd_logo.png" width="72" height="30" alt="DPD" /><input type="hidden" name="deliveryTrackingService" value="dpd" /></label>
										</td>
										<td style="vertical-align:middle">
											<input type="text" id="deliveryTrackingNumberDPD" name="deliveryTrackingNumber" class="inputField_120" value="<?php if($_POST["deliveryTrackingService"] == 'dpd') { echo $_POST["deliveryTrackingNumber"]; } ?>" />
										</td>
										<td style="vertical-align:middle">
											<input type="submit" name="submitTracking" class="inputButton0" value="DPD-Tracking anzeigen" />
										</td>
									</tr>
								</table>
								</form>
							</td>
							<td>&nbsp;</td>
							<td>
								<form name="formTrackingDatasDePost" method="post" target="iframeTracking" action="https://www.deutschepost.de/sendung/simpleQueryResult.html">
								<table border="0" cellpadding="0" cellspacing="0">
									<tr style="vertical-align:middle">
										<td style="vertical-align:middle">
											<label for="deliveryTrackingNumberDePost"><img src="layout/icons/post_logo.png" width="72" height="30" alt="Deutsche Post" /><input type="hidden" name="deliveryTrackingService" value="post" /></label>
										</td>
										<td style="vertical-align:middle">
											<input type="text" id="deliveryTrackingNumberDePost" name="form.sendungsnummer" class="inputField_120" value="<?php if($_POST["deliveryTrackingService"] == 'post') { echo $_POST["deliveryTrackingNumber"]; } ?>" />
										</td>
										<td style="vertical-align:middle">
											<input type="submit" name="submitTracking" class="inputButton0" value="Post-Tracking anzeigen" />
										</td>
									</tr>
								</table>
								</form>
							</td>
						</tr>
						<tr>
							<td>
								<form name="formTrackingDatasGLS" method="post" action="<?php echo $_SERVER["PHP_SELF"]; ?>">
								<table border="0" cellpadding="0" cellspacing="0">
									<tr style="vertical-align:middle">
										<td style="vertical-align:middle">
											<label for="deliveryTrackingNumberGLS"><img src="layout/icons/gls_logo.png" width="72" height="30" alt="GLS" /><input type="hidden" name="deliveryTrackingService" value="gls" /></label>
										</td>
										<td style="vertical-align:middle">
											<input type="text" id="deliveryTrackingNumberGLS" name="deliveryTrackingNumber" class="inputField_120" value="<?php if($_POST["deliveryTrackingService"] == 'gls') { echo $_POST["deliveryTrackingNumber"]; } ?>" />
										</td>
										<td style="vertical-align:middle">
											<input type="submit" name="submitTracking" class="inputButton0" value="GLS-Tracking anzeigen" />
										</td>
									</tr>
								</table>
								</form>
							</td>
							<td>&nbsp;</td>
							<td>
								<form name="formTrackingDatasDHL" method="post" action="<?php echo $_SERVER["PHP_SELF"]; ?>">
								<table border="0" cellpadding="0" cellspacing="0">
									<tr style="vertical-align:middle">
										<td style="vertical-align:middle">
											<label for="deliveryTrackingNumberDHL"><img src="layout/icons/dhl_logo.png" width="72" height="30" alt="DHL" /><input type="hidden" name="deliveryTrackingService" value="dhl" /></label>
										</td>
										<td style="vertical-align:middle">
											<input type="text" id="deliveryTrackingNumberDHL" name="deliveryTrackingNumber" class="inputField_120" value="<?php  if($_POST["deliveryTrackingService"] == 'dhl') { echo $_POST["deliveryTrackingNumber"]; } ?>" />
										</td>
										<td style="vertical-align:middle">
											<input type="submit" name="submitTracking" class="inputButton0" value="DHL-Tracking anzeigen" />
										</td>
									</tr>
								</table>
								</form>
							</td>
						</tr>
					</table>
				</div>

				<hr />

				<div class="contentDisplay">
					<?php displayMessages(); ?>
					
					<?php						
						if($_POST["deliveryTrackingNumber"] != "" && $_POST["deliveryTrackingService"] != ""){
							$thisSearchUrl = getTrackingUrl($_POST["deliveryTrackingService"], $_POST["deliveryTrackingNumber"]);							
							$thisFrameContent = file_get_contents($thisSearchUrl);
							
							$iframeContent = '
									<p><a href="' . $thisSearchUrl . '" target="_blank"><b>Tracking-Link</b>: ' . $thisSearchUrl . '</a></p>
									<iframe id="iframeTracking" name="iframeTracking" class="iFrameModule" src="' . $thisSearchUrl . '">
										<a href="' . $thisSearchUrl . '">Tracking-Link: ' . $thisSearchUrl . '</a>
									</iframe>
								';
							
							echo $iframeContent;
						}
					?>
				</div>

			</div>
		</div>
		<div class="clear"></div>
	</div>
</div>
<script language="javascript" type="text/javascript">
	$(document).ready(function() {
		$(function() {
			$.datepicker.setDefaults($.datepicker.regional[""]);
			$('#exportOrdersFreigabeDatumFrom').datepicker($.datepicker.regional["de"]);
			$('#exportOrdersFreigabeDatumTo').datepicker($.datepicker.regional["de"]);
		});
		// $('.contentDisplay').html('<?php echo $iframeContent; ?>');
	});
</script>
<?php require_once('inc/footerHTML.inc.php'); ?>