<?php
	require_once('inc/requires.inc.php');

	if(!$arrGetUserRights["editInvoices"]) {
		header('location: ' . PAGE_EXIT_LOCATION);
		exit;
	}

	$jswindowMessage = "";
	$warningMessage = '';
	$errorMessage = '';
	$successMessage = '';
	$infoMessage = '';

	$dbConnection = new DB_Connection();
	$db_open = $dbConnection->db_connect();

	$defaultSearchDateFrom = date("Y-m-d", mktime(0, 0, 0, date("m"), (date("d") - 2), date("Y")));
	$defaultSearchDateTo = date("Y-m-d", mktime(0, 0, 0, date("m"), (date("d") - 1), date("Y")));

	$thisSearchDateFrom = formatDate($defaultSearchDateFrom, "display");
	if($_REQUEST["searchDateFrom"] != ""){
		$thisSearchDateFrom = $_REQUEST["searchDateFrom"];
	}
	$thisSearchDateTo = formatDate($defaultSearchDateTo, "display");
	if($_REQUEST["searchDateTo"] != ""){
		$thisSearchDateTo = $_REQUEST["searchDateTo"];
	}

	// BOF READ PAYMENT STATUS TYPE DATAS
		$arrPaymentStatusTypeDatas = getPaymentStatusTypes();
		$arrPaymentStatusTypeDatas2 = array();
		if(!empty($arrPaymentStatusTypeDatas)){
			foreach($arrPaymentStatusTypeDatas as $thisKey => $thisValue){
				$arrPaymentStatusTypeDatas2[$thisValue["paymentStatusTypesShortName"]] = $thisValue["paymentStatusTypesName"];
			}
		}
	// EOF READ PAYMENT STATUS TYPE DATAS

	// BOF READ PAYMENT TYPES
		$arrPaymentTypeDatas = getPaymentTypes();
	// EOF READ PAYMENT TYPES

	// BOF READ PAYMENT CONDITIONS
		$arrPaymentConditionDatas = getPaymentConditions();
	// EOF READ PAYMENT CONDITIONS

	// BOF READ BANK TYPES
		$arrBankAccountTypeDatas = getBankAccountTypes('all');
		#$arrBankAccountTypeDatasActive = getBankAccountTypes();
	// EOF READ BANK TYPES

	// BOF ADD DATA COMMENT
		if($_POST["addDataCommentText"] != '' && $_POST["addDataCommentDate"] != ''){
			$sql_deleteOldComment = "
					DELETE FROM `" . TABLE_FINANCIAL_PAYMENT_STATISTICS . "`
					WHERE 1
						AND `financialPaymentStatisticsSearchDate` = '" . $_POST["addDataCommentDate"] . "'
						AND `financialPaymentStatisticsTableType` = 'comment';
				";

			$rs_deleteOldComment = $dbConnection->db_query($sql_deleteOldComment);

			$sql_insertComment = "
					INSERT INTO `" . TABLE_FINANCIAL_PAYMENT_STATISTICS . "` (
							`financialPaymentStatisticsID`,
							`financialPaymentStatisticsTableType`,
							`financialPaymentStatisticsStoreDateTime`,
							`financialPaymentStatisticsItemsCount`,
							`financialPaymentStatisticsStatus`,
							`financialPaymentStatisticsPaymentDates`,
							`financialPaymentStatisticsTotalSum`,
							`financialPaymentStatisticsBankAccount`,
							`financialPaymentStatisticsEntryDate`,
							`financialPaymentStatisticsSearchDate`,
							`financialPaymentStatisticsComment`
						)
						VALUES (
							'%',
							'comment',
							'0000-00-00 00:00:00',
							1,
							'',
							NULL,
							'0.00',
							'',
							'0000-00-00',
							'" . $_POST["addDataCommentDate"] . "',
							'" . utf8_encode($_POST["addDataCommentText"]) . "'
						);
				";
			$rs_insertComment = $dbConnection->db_query($sql_insertComment);
			#dd('sql_deleteOldComment');
			#dd('sql_insertComment');
		}
	// EOF ADD DATA COMMENT

	// BOF GET DATA
		$sql = "
				SELECT
					`financialPaymentStatisticsID`,
					`financialPaymentStatisticsTableType`,
					`financialPaymentStatisticsStoreDateTime`,
					`financialPaymentStatisticsItemsCount`,
					`financialPaymentStatisticsStatus`,
					`financialPaymentStatisticsPaymentDates`,
					`financialPaymentStatisticsTotalSum`,
					`financialPaymentStatisticsBankAccount`,
					`financialPaymentStatisticsEntryDate`,
					`financialPaymentStatisticsSearchDate`,
					`financialPaymentStatisticsComment`

				FROM `" . TABLE_FINANCIAL_PAYMENT_STATISTICS . "`

				WHERE 1
					AND `financialPaymentStatisticsSearchDate` >= '" . formatDate($thisSearchDateFrom, "store") . "'
					AND `financialPaymentStatisticsSearchDate` <= '" . formatDate($thisSearchDateTo, "store") . "'

				ORDER BY
					`financialPaymentStatisticsSearchDate` DESC,
					`financialPaymentStatisticsTableType`,
					`financialPaymentStatisticsBankAccount`,
					FIELD(`financialPaymentStatisticsStatus`, '" . implode("', '", array_keys($arrPaymentStatusTypeDatas2)) . "')
			";

		$rs = $dbConnection->db_query($sql);
		$arrFinancialStatisticData = array();
		$arrStoreTime = array();
		$arrDateComments = array();
		while($ds = mysqli_fetch_assoc($rs)){
			#$arrFinancialStatisticData[$ds["financialPaymentStatisticsSearchDate"]][$ds["financialPaymentStatisticsTableType"]][$ds["financialPaymentStatisticsBankAccount"]][$ds["financialPaymentStatisticsStatus"]][] = $ds;
			$arrFinancialStatisticData[$ds["financialPaymentStatisticsSearchDate"]][$ds["financialPaymentStatisticsTableType"]][$ds["financialPaymentStatisticsBankAccount"]][$ds["financialPaymentStatisticsStatus"]] = $ds;
			$arrStoreTime[$ds["financialPaymentStatisticsSearchDate"]] = $ds["financialPaymentStatisticsStoreDateTime"];
			if($ds["financialPaymentStatisticsComment"] != ''){
				$arrDateComments[$ds["financialPaymentStatisticsSearchDate"]] .= $ds["financialPaymentStatisticsComment"] . "\n";
			}
		}
		#dd('arrFinancialStatisticData');
	// EOF GET DATA
?>

	<?php
		require_once('inc/headerHTML.inc.php');
		$thisTitle = "Finanzbewegungen";
		$thisIcon = 'payment.png';
		$headerHTML = preg_replace("/{###TITLE###}/", strip_tags($thisTitle), $headerHTML);
		echo $headerHTML;

		require_once(FILE_MENUE_TOP);
	?>
	<div id="xmainArea">
		<div id="xmainContent">
			<div id="menueSidebarToggleArea">
				<img src="layout/icons/iconToggle.png" class="buttonToggleSidebarMenue" width="10" height="10" alt="" title="Seitennavigation ein-/ausblenden" />
				<div id="menueSidebarToggleContent">
				<?php require_once(FILE_MENUE_SIDEBAR); ?>
				<div class="clear"></div>
				</div>
			</div>
			<div id="contentArea2">
				<div id="contentAreaElements">
					<h1><?php if(SHOW_PAGE_TITLE_ICONS) { echo '<img src="' . PATH_ICONS_MENUE_TITLES . $thisIcon . '" alt="" />'; } ?> <?php echo $thisTitle; ?></h1>

					<p class="infoArea">
						<u>INFO: Gutschriften in irgendeiner Form mit einbeziehen????</u><br />
						<span style="line-height:20px;">&bull; 16.08.2016: Zu Gutschrift GU-1606000065 (Betrag 206,32 EUR) gehörende Rechnung RE-1605002860 war noch offen - auf bezahlt gesetzt. Neue RE-1607003711 wurde geschrieben. Kunde 41038, ATV Auto Teile Vahid RE-1605002860</span>
						<br />
						<span style="line-height:20px;">&bull; 24.08.2016: Es wurden zwei Gutschriften mit geringeren Betr&auml;gen als die Rechnung geschrieben und BEIDE mit dem Status 'bezahlt' versehen, obwohl bei einer RECHNUNG noch KEIN Zahlungseingang existiert(e)!!!</span>
						<br />
						<span style="line-height:20px;">&bull; 05.09.2016: Zahlung AB mit RE (Status teilbezahlt) in Höhe von 282,03 EUR eingetragen.</span>
					</p>

					<?php displayMessages(); ?>

					<div id="searchFilterArea">
						<form name="formFilterSearch" method="get" action="<?php echo $_SERVER["REDIRECT_URL"]; ?>">
							<table border="0" cellpadding="0" cellspacing="0" class="searchFilterContent">
								<tr>
									<td>
										<label for="searchDateFrom">Datum von:</label>
										<input type="text" id="searchDateFrom" name="searchDateFrom" class="inputField_70" value="<?php echo $thisSearchDateFrom; ?>" readonly="readonly" />
									</td>
									<td>
										<label for="searchDateTo">Datum von:</label>
										<input type="text" id="searchDateTo" name="searchDateTo" class="inputField_70" value="<?php echo $thisSearchDateTo; ?>" readonly="readonly" />
									</td>
									<td>
										<input type="submit" name="submitSearch" class="inputButton0" value="Suchen" />
									</td>
								</tr>
							</table>
						</form>
					</div>

					<?php
						// BOF CURRENT LIVE DATA
							$nowDateTime = date("Y-m-d H:i:s");
							$nowDate = date("Y-m-d");
							echo '<h2>Momentaner Stand (' . date('d.m.Y H:i:s', strtotime($nowDateTime)). '):</h2>';

							$arrFinancialStatisticDataLive = array();

							// BOF GET COMMENT
								$sql_getLiveComment = "
										SELECT
											`financialPaymentStatisticsComment`
										FROM `" . TABLE_FINANCIAL_PAYMENT_STATISTICS . "`
										WHERE 1
											AND `financialPaymentStatisticsSearchDate` = '" . $nowDate . "'
										LIMIT 1

									";
								$rs_getLiveComment = $dbConnection->db_query($sql_getLiveComment);

								while($ds_getLiveComment = mysqli_fetch_assoc($rs_getLiveComment)){
									$arrFinancialStatisticDataLive[$nowDate]["comment"] = $ds_getLiveComment["financialPaymentStatisticsComment"];
								}
								#dd('arrFinancialStatisticDataLive');
							// EOF GET COMMENT

							// BOF GET OPEN INVOICES
								$sql_getOpenInvoices = "
											SELECT
													'unpaid_RE' AS `financialPaymentStatisticsTableType`,
													COUNT(`" . TABLE_ORDER_INVOICES . "`.`orderDocumentsID`) AS `financialPaymentStatisticsItemsCount`,
													`" . TABLE_PAYMENT_STATUS_TYPES . "`.`paymentStatusTypesShortName` AS `financialPaymentStatisticsStatus`,
													SUM(`" . TABLE_ORDER_INVOICES . "`.`orderDocumentsTotalPrice`) AS `financialPaymentStatisticsTotalSum`,
													`" . TABLE_ORDER_INVOICES . "`.`orderDocumentsBankAccount` AS `financialPaymentStatisticsBankAccount`

												FROM `" . TABLE_ORDER_INVOICES . "`

												LEFT JOIN `" . TABLE_PAYMENT_STATUS_TYPES . "`
												ON(`" . TABLE_ORDER_INVOICES . "`.`orderDocumentsStatus` = `" . TABLE_PAYMENT_STATUS_TYPES . "`.`paymentStatusTypesID`)

												LEFT JOIN `" . TABLE_PAYMENT_CONDITIONS . "`
												ON(`" . TABLE_ORDER_INVOICES . "`.`orderDocumentsPaymentCondition` = `" . TABLE_PAYMENT_CONDITIONS . "`.`paymentConditionsID`)

												LEFT JOIN `" . TABLE_BANK_ACCOUNT_TYPES . "`
												ON(`" . TABLE_ORDER_INVOICES . "`.`orderDocumentsBankAccount` = `" . TABLE_BANK_ACCOUNT_TYPES . "`.`bankAccountTypesShortName`)

												WHERE 1
													AND (
														`" . TABLE_PAYMENT_STATUS_TYPES . "`.`paymentStatusTypesShortName` = 'OF'
														OR `" . TABLE_PAYMENT_STATUS_TYPES . "`.`paymentStatusTypesShortName` = 'TB'
														OR `" . TABLE_PAYMENT_STATUS_TYPES . "`.`paymentStatusTypesShortName` = 'MA'
														OR `" . TABLE_PAYMENT_STATUS_TYPES . "`.`paymentStatusTypesShortName` = 'M1'
														OR `" . TABLE_PAYMENT_STATUS_TYPES . "`.`paymentStatusTypesShortName` = 'M2'
														OR `" . TABLE_PAYMENT_STATUS_TYPES . "`.`paymentStatusTypesShortName` = 'M3'
														OR `" . TABLE_PAYMENT_STATUS_TYPES . "`.`paymentStatusTypesShortName` = 'IK'
														OR `" . TABLE_PAYMENT_STATUS_TYPES . "`.`paymentStatusTypesShortName` = 'MB'
													)

												GROUP BY
													CONCAT(
														`" . TABLE_PAYMENT_STATUS_TYPES . "`.`paymentStatusTypesShortName`,
														`" . TABLE_ORDER_INVOICES . "`.`orderDocumentsBankAccount`
													)
									";
								// dd('sql_getOpenInvoices');
								$rs_getOpenInvoices = $dbConnection->db_query($sql_getOpenInvoices);

								while($ds_getOpenInvoices = mysqli_fetch_assoc($rs_getOpenInvoices)){
									$arrFinancialStatisticDataLive[$nowDate][$ds_getOpenInvoices["financialPaymentStatisticsTableType"]][$ds_getOpenInvoices["financialPaymentStatisticsBankAccount"]][$ds_getOpenInvoices["financialPaymentStatisticsStatus"]] = $ds_getOpenInvoices;
								}
							// EOF GET OPEN INVOICES

							// BOF GET PAYMENTS AB
								$sql_getPaymentsConfirmations = "
											SELECT
													'payments_AB' AS `financialPaymentStatisticsTableType`,
													COUNT(`" . TABLE_ORDER_CONFIRMATION_PAYMENTS . "`.`orderPaymentID`) AS `financialPaymentStatisticsItemsCount`,
													`" . TABLE_PAYMENT_STATUS_TYPES . "`.`paymentStatusTypesShortName` AS `financialPaymentStatisticsStatus`,
													SUM(`" . TABLE_ORDER_CONFIRMATION_PAYMENTS . "`.`orderPaymentValue`) AS `financialPaymentStatisticsTotalSum`,
													`" . TABLE_ORDER_CONFIRMATION_PAYMENTS . "`.`orderPaymentBankAccountID` AS `financialPaymentStatisticsBankAccount`

												FROM `" . TABLE_ORDER_CONFIRMATION_PAYMENTS . "`

												LEFT JOIN `" . TABLE_BANK_ACCOUNT_TYPES . "`
												ON(`" . TABLE_ORDER_CONFIRMATION_PAYMENTS . "`.`orderPaymentBankAccountID` = `" . TABLE_BANK_ACCOUNT_TYPES . "`.`bankAccountTypesShortName`)

												LEFT JOIN `" . TABLE_ORDER_CONFIRMATIONS . "`
												ON(`" . TABLE_ORDER_CONFIRMATION_PAYMENTS . "`.`orderPaymentOrderNumber` = `" . TABLE_ORDER_CONFIRMATIONS . "`.`orderDocumentsNumber`)

												LEFT JOIN `" . TABLE_PAYMENT_STATUS_TYPES . "`
												ON(`" . TABLE_ORDER_CONFIRMATIONS . "`.`orderDocumentsStatus` = `" . TABLE_PAYMENT_STATUS_TYPES . "`.`paymentStatusTypesID`)

												WHERE 1
													AND `" . TABLE_ORDER_CONFIRMATION_PAYMENTS . "`.`orderPaymentEntryDate` = '" . $nowDate . "'

												GROUP BY
													CONCAT(
														`" . TABLE_ORDER_CONFIRMATION_PAYMENTS . "`.`orderPaymentEntryDate`,
														`" . TABLE_ORDER_CONFIRMATION_PAYMENTS . "`.`orderPaymentBankAccountID`,
														`" . TABLE_PAYMENT_STATUS_TYPES . "`.`paymentStatusTypesShortName`
													)
												ORDER BY `" . TABLE_ORDER_CONFIRMATION_PAYMENTS . "`.`orderPaymentEntryDate` DESC
									";
								// dd('sql_getPaymentsConfirmations');
								$rs_getPaymentsConfirmations = $dbConnection->db_query($sql_getPaymentsConfirmations);

								while($ds_getPaymentsConfirmations = mysqli_fetch_assoc($rs_getPaymentsConfirmations)){
									$arrFinancialStatisticDataLive[$nowDate][$ds_getPaymentsConfirmations["financialPaymentStatisticsTableType"]][$ds_getPaymentsConfirmations["financialPaymentStatisticsBankAccount"]][$ds_getPaymentsConfirmations["financialPaymentStatisticsStatus"]] = $ds_getPaymentsConfirmations;
								}
							// EOF GET PAYMENTS AB

							// BOF GET PAYMENTS AB WITH RE
								$sql_getPaymentsConfirmationsWithInvoices = "
											SELECT
													'payments_AB_with_RE' AS `financialPaymentStatisticsTableType`,
													COUNT(`" . TABLE_ORDER_CONFIRMATION_PAYMENTS . "`.`orderPaymentID`) AS `financialPaymentStatisticsItemsCount`,
													`" . TABLE_PAYMENT_STATUS_TYPES . "`.`paymentStatusTypesShortName` AS `financialPaymentStatisticsStatus`,
													SUM(`" . TABLE_ORDER_CONFIRMATION_PAYMENTS . "`.`orderPaymentValue`) AS `financialPaymentStatisticsTotalSum`,
													`" . TABLE_ORDER_CONFIRMATION_PAYMENTS . "`.`orderPaymentBankAccountID` AS `financialPaymentStatisticsBankAccount`

												FROM `" . TABLE_ORDER_CONFIRMATION_PAYMENTS . "`

												LEFT JOIN `" . TABLE_BANK_ACCOUNT_TYPES . "`
												ON(`" . TABLE_ORDER_CONFIRMATION_PAYMENTS . "`.`orderPaymentBankAccountID` = `" . TABLE_BANK_ACCOUNT_TYPES . "`.`bankAccountTypesShortName`)

												LEFT JOIN `" . TABLE_RELATED_DOCUMENTS. "`
												ON(`" . TABLE_ORDER_CONFIRMATION_PAYMENTS . "`.`orderPaymentOrderNumber` = `" . TABLE_RELATED_DOCUMENTS. "`.`relatedDocuments_AB`)

												INNER JOIN `" . TABLE_ORDER_INVOICES . "`
												ON(`" . TABLE_RELATED_DOCUMENTS . "`.`relatedDocuments_RE` = `" . TABLE_ORDER_INVOICES . "`.`orderDocumentsNumber`)

												LEFT JOIN `" . TABLE_PAYMENT_STATUS_TYPES . "`
												ON(`" . TABLE_ORDER_INVOICES . "`.`orderDocumentsStatus` = `" . TABLE_PAYMENT_STATUS_TYPES . "`.`paymentStatusTypesID`)

												WHERE 1
													AND `" . TABLE_ORDER_CONFIRMATION_PAYMENTS . "`.`orderPaymentEntryDate` = '" . $nowDate . "'

												GROUP BY
													CONCAT(
														`" . TABLE_ORDER_CONFIRMATION_PAYMENTS . "`.`orderPaymentEntryDate`,
														`" . TABLE_ORDER_CONFIRMATION_PAYMENTS . "`.`orderPaymentBankAccountID`,
														`" . TABLE_PAYMENT_STATUS_TYPES . "`.`paymentStatusTypesShortName`
													)
												ORDER BY `" . TABLE_ORDER_CONFIRMATION_PAYMENTS . "`.`orderPaymentEntryDate` DESC
									";
								// dd('sql_getPaymentsConfirmationsWithInvoices');
								$rs_getPaymentsConfirmationsWithInvoices = $dbConnection->db_query($sql_getPaymentsConfirmationsWithInvoices);

								while($ds_getPaymentsConfirmationsWithInvoices = mysqli_fetch_assoc($rs_getPaymentsConfirmationsWithInvoices)){
									$arrFinancialStatisticDataLive[$nowDate][$ds_getPaymentsConfirmationsWithInvoices["financialPaymentStatisticsTableType"]][$ds_getPaymentsConfirmationsWithInvoices["financialPaymentStatisticsBankAccount"]][$ds_getPaymentsConfirmationsWithInvoices["financialPaymentStatisticsStatus"]] = $ds_getPaymentsConfirmationsWithInvoices;
								}
							// EOF GET PAYMENTS AB WITH RE

							// BOF GET PAYMENTS RE
								$sql_getPaymentsInvoices = "
											SELECT
												'payments_RE' AS `financialPaymentStatisticsTableType`,
												COUNT(`" . TABLE_ORDER_INVOICE_PAYMENTS . "`.`orderPaymentID`) AS `financialPaymentStatisticsItemsCount`,
												`" . TABLE_PAYMENT_STATUS_TYPES . "`.`paymentStatusTypesShortName` AS `financialPaymentStatisticsStatus`,
												SUM(`" . TABLE_ORDER_INVOICE_PAYMENTS . "`.`orderPaymentValue`) AS `financialPaymentStatisticsTotalSum`,
												`" . TABLE_ORDER_INVOICE_PAYMENTS . "`.`orderPaymentBankAccountID` AS `financialPaymentStatisticsBankAccount`


											FROM `" . TABLE_ORDER_INVOICE_PAYMENTS . "`

											LEFT JOIN `" . TABLE_BANK_ACCOUNT_TYPES . "`
											ON(`" . TABLE_ORDER_INVOICE_PAYMENTS . "`.`orderPaymentBankAccountID` = `" . TABLE_BANK_ACCOUNT_TYPES . "`.`bankAccountTypesShortName`)

											LEFT JOIN `" . TABLE_ORDER_INVOICES . "`
											ON(`" . TABLE_ORDER_INVOICE_PAYMENTS . "`.`orderPaymentOrderNumber` = `" . TABLE_ORDER_INVOICES . "`.`orderDocumentsNumber`)

											LEFT JOIN `" . TABLE_PAYMENT_STATUS_TYPES . "`
											ON(`" . TABLE_ORDER_INVOICES . "`.`orderDocumentsStatus` = `" . TABLE_PAYMENT_STATUS_TYPES . "`.`paymentStatusTypesID`)

											WHERE 1
												AND `" . TABLE_ORDER_INVOICE_PAYMENTS . "`.`orderPaymentEntryDate` = '" . $nowDate . "'

											GROUP BY
												CONCAT(
													`" . TABLE_ORDER_INVOICE_PAYMENTS . "`.`orderPaymentEntryDate`,
													`" . TABLE_ORDER_INVOICE_PAYMENTS . "`.`orderPaymentBankAccountID`,
													`" . TABLE_PAYMENT_STATUS_TYPES . "`.`paymentStatusTypesShortName`
												)
											ORDER BY `" . TABLE_ORDER_INVOICE_PAYMENTS . "`.`orderPaymentEntryDate` DESC
									";
								// dd('sql_getPaymentsInvoices');
								$rs_getPaymentsInvoices = $dbConnection->db_query($sql_getPaymentsInvoices);

								while($ds_getPaymentsInvoices = mysqli_fetch_assoc($rs_getPaymentsInvoices)){
									$arrFinancialStatisticDataLive[$nowDate][$ds_getPaymentsInvoices["financialPaymentStatisticsTableType"]][$ds_getPaymentsInvoices["financialPaymentStatisticsBankAccount"]][$ds_getPaymentsInvoices["financialPaymentStatisticsStatus"]] = $ds_getPaymentsInvoices;
								}
							// EOF GET PAYMENTS RE

							// BOF GET WRITTEN REs
								$sql_getWrittenInvoices = "
											SELECT
													'written_RE' AS `financialPaymentStatisticsTableType`,
													COUNT(`" .TABLE_ORDER_INVOICES . "`.`orderDocumentsID`) AS `financialPaymentStatisticsItemsCount`,
													`" . TABLE_PAYMENT_STATUS_TYPES . "`.`paymentStatusTypesShortName` AS `financialPaymentStatisticsStatus`,
													SUM(`" .TABLE_ORDER_INVOICES . "`.`orderDocumentsTotalPrice`) AS `financialPaymentStatisticsTotalSum`,
													`" .TABLE_ORDER_INVOICES . "`.`orderDocumentsBankAccount` AS `financialPaymentStatisticsBankAccount`

												FROM `" .TABLE_ORDER_INVOICES . "`

												LEFT JOIN `" . TABLE_PAYMENT_STATUS_TYPES . "`
												ON(`" .TABLE_ORDER_INVOICES . "`.`orderDocumentsStatus` = `" . TABLE_PAYMENT_STATUS_TYPES . "`.`paymentStatusTypesID`)
													WHERE 1
														AND `" .TABLE_ORDER_INVOICES . "`.`orderDocumentsDocumentDate` = '" . $nowDate . "'
												GROUP BY
													CONCAT(
														`" . TABLE_PAYMENT_STATUS_TYPES . "`.`paymentStatusTypesShortName`,
														`" .TABLE_ORDER_INVOICES . "`.`orderDocumentsBankAccount`
													)
									";
								// dd('sql_getWrittenInvoices');
								$rs_getWrittenInvoices = $dbConnection->db_query($sql_getWrittenInvoices);

								while($ds_getWrittenInvoices = mysqli_fetch_assoc($rs_getWrittenInvoices)){
									$arrFinancialStatisticDataLive[$nowDate][$ds_getWrittenInvoices["financialPaymentStatisticsTableType"]][$ds_getWrittenInvoices["financialPaymentStatisticsBankAccount"]][$ds_getWrittenInvoices["financialPaymentStatisticsStatus"]] = $ds_getWrittenInvoices;
								}
							// EOF GET WRITTEN REs

							// BOF GET WRITTEN ABs
								$sql_getWrittenConfirmations = "
											SELECT
												'written_AB' AS `financialPaymentStatisticsTableType`,
												COUNT(`" .TABLE_ORDER_CONFIRMATIONS . "`.`orderDocumentsID`) AS `financialPaymentStatisticsItemsCount`,
												`" . TABLE_PAYMENT_STATUS_TYPES . "`.`paymentStatusTypesShortName` AS `financialPaymentStatisticsStatus`,
												SUM(`" .TABLE_ORDER_CONFIRMATIONS . "`.`orderDocumentsTotalPrice`) AS `financialPaymentStatisticsTotalSum`,
												`" .TABLE_ORDER_CONFIRMATIONS . "`.`orderDocumentsBankAccount` AS `financialPaymentStatisticsBankAccount`

											FROM `" .TABLE_ORDER_CONFIRMATIONS . "`

											LEFT JOIN `" . TABLE_PAYMENT_STATUS_TYPES . "`
											ON(`" .TABLE_ORDER_CONFIRMATIONS . "`.`orderDocumentsStatus` = `" . TABLE_PAYMENT_STATUS_TYPES . "`.`paymentStatusTypesID`)
												WHERE 1
													AND `" .TABLE_ORDER_CONFIRMATIONS . "`.`orderDocumentsDocumentDate` = '" . $nowDate . "'
											GROUP BY
												CONCAT(
													`" . TABLE_PAYMENT_STATUS_TYPES . "`.`paymentStatusTypesShortName`,
													`" .TABLE_ORDER_CONFIRMATIONS . "`.`orderDocumentsBankAccount`
												)
									";
								// dd('sql_getWrittenConfirmations');
								$rs_getWrittenConfirmations = $dbConnection->db_query($sql_getWrittenConfirmations);

								while($ds_getWrittenConfirmations = mysqli_fetch_assoc($rs_getWrittenConfirmations)){
									$arrFinancialStatisticDataLive[$nowDate][$ds_getWrittenConfirmations["financialPaymentStatisticsTableType"]][$ds_getWrittenConfirmations["financialPaymentStatisticsBankAccount"]][$ds_getWrittenConfirmations["financialPaymentStatisticsStatus"]] = $ds_getWrittenConfirmations;
								}
							// EOF GET WRITTEN ABs

							// BOF GET WRITTEN GUs
								$sql_getWrittenCredits = "
											SELECT
												'written_GS' AS `financialPaymentStatisticsTableType`,
												COUNT(`" .TABLE_ORDER_CREDITS . "`.`orderDocumentsID`) AS `financialPaymentStatisticsItemsCount`,
												`" . TABLE_PAYMENT_STATUS_TYPES . "`.`paymentStatusTypesShortName` AS `financialPaymentStatisticsStatus`,
												SUM(`" .TABLE_ORDER_CREDITS . "`.`orderDocumentsTotalPrice`) AS `financialPaymentStatisticsTotalSum`,
												`" .TABLE_ORDER_CREDITS . "`.`orderDocumentsBankAccount` AS `financialPaymentStatisticsBankAccount`

											FROM `" .TABLE_ORDER_CREDITS . "`

											LEFT JOIN `" . TABLE_PAYMENT_STATUS_TYPES . "`
											ON(`" .TABLE_ORDER_CREDITS . "`.`orderDocumentsStatus` = `" . TABLE_PAYMENT_STATUS_TYPES . "`.`paymentStatusTypesID`)
												WHERE 1
													AND `" .TABLE_ORDER_CREDITS . "`.`orderDocumentsDocumentDate` = '" . $nowDate . "'
											GROUP BY
												CONCAT(
													`" . TABLE_PAYMENT_STATUS_TYPES . "`.`paymentStatusTypesShortName`,
													`" .TABLE_ORDER_CREDITS . "`.`orderDocumentsBankAccount`
												)
									";
								// dd('sql_getWrittenCredits');
								$rs_getWrittenCredits = $dbConnection->db_query($sql_getWrittenCredits);

								while($ds_getWrittenCredits = mysqli_fetch_assoc($rs_getWrittenCredits)){
									$arrFinancialStatisticDataLive[$nowDate][$ds_getWrittenCredits["financialPaymentStatisticsTableType"]][$ds_getWrittenCredits["financialPaymentStatisticsBankAccount"]][$ds_getWrittenCredits["financialPaymentStatisticsStatus"]] = $ds_getWrittenCredits;
								}
							// EOF GET WRITTEN GUs

							#dd('arrFinancialStatisticDataLive');

							if(!empty($arrFinancialStatisticDataLive)){
								echo '<table border="0" cellpadding="0" cellspacing="0" width="100%" class="displayOrders" style="width:100% !important;">';
									echo '<colgroup>';
										echo '<col width="12%" />';
										echo '<col width="11%" />';
										echo '<col width="11%" />';
										echo '<col width="11%" />';
										echo '<col width="11%" />';
										echo '<col width="11%" />';
										echo '<col width="11%" />';
										echo '<col width="11%" />';
										echo '<col width="11%" />';
									echo '</colgroup>';
									echo '<thead>';
										echo '<tr>';
											echo '<th width="12%">offene REs</th>';
											echo '<th width="11%">Zahlung AB</th>';
											echo '<th width="11%">Zahlung AB mit RE</th>';
											echo '<th width="11%">Zahlung RE</th>';
											echo '<th width="11%">geschriebene RE</th>';
											echo '<th width="11%">geschriebene GS</th>';
											echo '<th width="11%">geschriebene AB</th>';
											echo '<th width="11%">Differenz (?)</th>';
											echo '<th width="11%">Kommentar</th>';
										echo '</tr>';
									echo '</thead>';
									echo '<tbody>';

										$thisMarker = '';
										$countRow = 0;
										foreach($arrFinancialStatisticDataLive as $thisDateKey => $thisDateValue){
											if($thisMarker != $thisDateKey){
												echo '<tr>';
												echo '<td class="tableRowTitle1" colspan="9">';
												echo '<span class="dataDate">' . formatDate($thisDateKey, "display") . '</span>';
												echo ' &bull; <b>Stand: </b>' . $nowDateTime;
												echo ' <img src="layout/icons/iconToggle3.png" class="buttonShowAllDetails" width="10" height="10" alt="Alle Details" title="Alle Details vom ' . formatDate($thisDateKey, "display") . 'anzeigen" />';
												echo '</td>';
												echo '</tr>';
												$thisMarker = $thisDateKey;
											}

											if($countRow%2 == 0){ $rowClass = 'row0'; }
											else { $rowClass = 'row1'; }

											echo '<tr class="' . $rowClass . '">';

											// BOF UNPAID RE
												echo '<td>';
												$thisTotalSumUnpaid_RE = 0;
												if(!empty($thisDateValue["unpaid_RE"])){
													echo '<div class="dataBoxItemContent" style="display:none;">';
													foreach($thisDateValue["unpaid_RE"] as $thisBankAccountKey => $thisBankAccountData){
														echo '<p style="white-space:nowrap;font-size:11px;margin:0 0 2px 0;padding:0"><!--<b>Bank</b>: -->' . $arrBankAccountTypeDatas[$thisBankAccountKey]["bankAccountTypesName"] . '<br /><b>Konto</b>: ' . $arrBankAccountTypeDatas[$thisBankAccountKey]["bankAccountTypesAccountNumber"] . ' [' . $thisBankAccountKey . ']' . '</p>';

														if(!empty($thisBankAccountData)){
															echo '<table cellpadding="0" cellspacing="0" border="0" width="100%" class="dataBoxTable">';
															echo '<tr>';
															echo '<th>Status</th>';
															echo '<th>Menge</th>';
															echo '<th>Summe</th>';
															echo '</tr>';
															$thisBankAccountTotalSumUnpaid_RE = 0;
															$thisBankAccountTotalItemsUnpaid_RE = 0;
															foreach($thisBankAccountData as $thisKey => $thisData){
																echo '<tr>';
																echo '<td><span title="' . $arrPaymentStatusTypeDatas2[$thisData["financialPaymentStatisticsStatus"]] . '">' . $thisData["financialPaymentStatisticsStatus"] . '</span></td>';
																echo '<td style="text-align:right;">' . $thisData["financialPaymentStatisticsItemsCount"] . '</td>';
																echo '<td style="text-align:right;">' . number_format($thisData["financialPaymentStatisticsTotalSum"], 2, ',', '.') . '</td>';
																echo '</tr>';
																$thisBankAccountTotalSumUnpaid_RE += $thisData["financialPaymentStatisticsTotalSum"];
																$thisBankAccountTotalItemsUnpaid_RE += $thisData["financialPaymentStatisticsItemsCount"];
															}

															echo '<tr class="row2" style="font-weight:bold;">';

															echo '<td>';
															echo '<b>Insg.:</b>';
															echo '</td>';

															echo '<td style="text-align:right;">';
															echo $thisBankAccountTotalItemsUnpaid_RE;
															echo '</td>';

															echo '<td style="text-align:right;">';
															echo number_format($thisBankAccountTotalSumUnpaid_RE, 2, ',', '.');
															echo '</td>';

															echo '</tr>';
															echo '</table>';
															echo '<hr />';
															$thisTotalSumUnpaid_RE += $thisBankAccountTotalSumUnpaid_RE;
														}
													}
													echo '<hr class="doubleline" />';
													echo '</div>';

													echo '<div style="text-align:right;">';
													echo '<b>' . number_format($thisTotalSumUnpaid_RE, 2, ',', '.') . '</b>';
													# echo ' <img src="layout/icons/iconToggle3.png" class="buttonShowDetails" width="10" height="10" alt="Details" title="Details anzeigen" />';
													echo '</div>';
												}

												echo '</td>';
											// EOF UNPAID RE

											// BOF PAYMENTS AB
												echo '<td>';
												$thisTotalSumPayments_AB = 0;
												if(!empty($thisDateValue["payments_AB"])){
													echo '<div class="dataBoxItemContent" style="display:none;">';
													foreach($thisDateValue["payments_AB"] as $thisBankAccountKey => $thisBankAccountData){
														echo '<p style="white-space:nowrap;font-size:11px;margin:0 0 2px 0;padding:0"><!--<b>Bank</b>: -->' . $arrBankAccountTypeDatas[$thisBankAccountKey]["bankAccountTypesName"] . '<br /><b>Konto</b>: ' . $arrBankAccountTypeDatas[$thisBankAccountKey]["bankAccountTypesAccountNumber"] . ' [' . $thisBankAccountKey . ']' . '</p>';

														if(!empty($thisBankAccountData)){
															echo '<table cellpadding="0" cellspacing="0" border="0" width="100%" class="dataBoxTable">';
															echo '<tr>';
															echo '<th>Status</th>';
															echo '<th>Menge</th>';
															echo '<th>Summe</th>';
															echo '</tr>';
															$thisBankAccountTotalSumPayments_AB = 0;
															$thisBankAccountTotalItemsPayments_AB = 0;
															foreach($thisBankAccountData as $thisKey => $thisData){
																echo '<tr>';
																echo '<td><span title="' . $arrPaymentStatusTypeDatas2[$thisData["financialPaymentStatisticsStatus"]]["paymentStatusTypesName"] . '">' . $thisData["financialPaymentStatisticsStatus"] . '</span></td>';
																echo '<td style="text-align:right;">' . $thisData["financialPaymentStatisticsItemsCount"] . '</td>';
																echo '<td style="text-align:right;">' . number_format($thisData["financialPaymentStatisticsTotalSum"], 2, ',', '.') . '</td>';
																echo '</tr>';
																$thisBankAccountTotalSumPayments_AB += $thisData["financialPaymentStatisticsTotalSum"];
																$thisBankAccountTotalItemsPayments_AB += $thisData["financialPaymentStatisticsItemsCount"];
															}

															echo '<tr class="row2" style="font-weight:bold;">';

															echo '<td>';
															echo '<b>Insg.:</b>';
															echo '</td>';

															echo '<td style="text-align:right;">';
															echo $thisBankAccountTotalItemsPayments_AB;
															echo '</td>';

															echo '<td style="text-align:right;">';
															echo number_format($thisBankAccountTotalSumPayments_AB, 2, ',', '.');
															echo '</td>';

															echo '</tr>';
															echo '</table>';
															echo '<hr />';
															$thisTotalSumPayments_AB += $thisBankAccountTotalSumPayments_AB;
														}
													}
													echo '<hr class="doubleline" />';
													echo '</div>';

													echo '<div style="text-align:right;">';
													echo '<b>' . number_format($thisTotalSumPayments_AB, 2, ',', '.') . '</b>';
													# echo ' <img src="layout/icons/iconToggle3.png" class="buttonShowDetails" width="10" height="10" alt="Details" title="Details anzeigen" />';
													echo '</div>';
												}
												echo '</td>';
											// EOF PAYMENTS AB

											// BOF PAYMENTS AB WITH RE
												echo '<td>';
												$thisTotalSumPayments_AB_with_RE = 0;
												if(!empty($thisDateValue["payments_AB_with_RE"])){
													echo '<div class="dataBoxItemContent" style="display:none;">';
													foreach($thisDateValue["payments_AB_with_RE"] as $thisBankAccountKey => $thisBankAccountData){
														echo '<p style="white-space:nowrap;font-size:11px;margin:0 0 2px 0;padding:0"><!--<b>Bank</b>: -->' . $arrBankAccountTypeDatas[$thisBankAccountKey]["bankAccountTypesName"] . '<br /><b>Konto</b>: ' . $arrBankAccountTypeDatas[$thisBankAccountKey]["bankAccountTypesAccountNumber"] . ' [' . $thisBankAccountKey . ']' . '</p>';

														if(!empty($thisBankAccountData)){
															echo '<table cellpadding="0" cellspacing="0" border="0" width="100%" class="dataBoxTable">';
															echo '<tr>';
															echo '<th>Status</th>';
															echo '<th>Menge</th>';
															echo '<th>Summe</th>';
															echo '</tr>';
															$thisBankAccountTotalSumPayments_AB_with_RE = 0;
															$thisBankAccountTotalItemsPayments_AB_with_RE = 0;
															foreach($thisBankAccountData as $thisKey => $thisData){
																echo '<tr>';
																echo '<td><span title="' . $arrPaymentStatusTypeDatas2[$thisData["financialPaymentStatisticsStatus"]]["paymentStatusTypesName"] . '">' . $thisData["financialPaymentStatisticsStatus"] . '</span></td>';
																echo '<td style="text-align:right;">' . $thisData["financialPaymentStatisticsItemsCount"] . '</td>';
																echo '<td style="text-align:right;">' . number_format($thisData["financialPaymentStatisticsTotalSum"], 2, ',', '.') . '</td>';
																echo '</tr>';
																$thisBankAccountTotalSumPayments_AB_with_RE += $thisData["financialPaymentStatisticsTotalSum"];
																$thisBankAccountTotalItemsPayments_AB_with_RE += $thisData["financialPaymentStatisticsItemsCount"];
															}

															echo '<tr class="row2" style="font-weight:bold;">';

															echo '<td>';
															echo '<b>Insg.:</b>';
															echo '</td>';

															echo '<td style="text-align:right;">';
															echo $thisBankAccountTotalItemsPayments_AB_with_RE;
															echo '</td>';

															echo '<td style="text-align:right;">';
															echo number_format($thisBankAccountTotalSumPayments_AB_with_RE, 2, ',', '.');
															echo '</td>';

															echo '</tr>';
															echo '</table>';
															echo '<hr />';
															$thisTotalSumPayments_AB_with_RE += $thisBankAccountTotalSumPayments_AB_with_RE;
														}
													}
													echo '<hr class="doubleline" />';
													echo '</div>';

													echo '<div style="text-align:right;">';
													echo '<b>' . number_format($thisTotalSumPayments_AB_with_RE, 2, ',', '.') . '</b>';
													# echo ' <img src="layout/icons/iconToggle3.png" class="buttonShowDetails" width="10" height="10" alt="Details" title="Details anzeigen" />';
													echo '</div>';
												}
												echo '</td>';
											// EOF PAYMENTS AB WITH RE

											// BOF PAYMENTS RE
												echo '<td>';
												$thisTotalSumPayments_RE = 0;
												$thisTotalSumPaymentsPartlyPaid_RE = 0;
												if(!empty($thisDateValue["payments_RE"])){
													echo '<div class="dataBoxItemContent" style="display:none;">';
													foreach($thisDateValue["payments_RE"] as $thisBankAccountKey => $thisBankAccountData){
														echo '<p style="white-space:nowrap;font-size:11px;margin:0 0 2px 0;padding:0"><!--<b>Bank</b>: -->' . $arrBankAccountTypeDatas[$thisBankAccountKey]["bankAccountTypesName"] . '<br /><b>Konto</b>: ' . $arrBankAccountTypeDatas[$thisBankAccountKey]["bankAccountTypesAccountNumber"] . ' [' . $thisBankAccountKey . ']' . '</p>';
														if(!empty($thisBankAccountData)){
															echo '<table cellpadding="0" cellspacing="0" border="0" width="100%" class="dataBoxTable">';
															echo '<tr>';
															echo '<th>Status</th>';
															echo '<th>Menge</th>';
															echo '<th>Summe</th>';
															echo '</tr>';
															$thisBankAccountTotalSumPayments_RE = 0;
															$thisBankAccountTotalItemsPayments_RE = 0;
															foreach($thisBankAccountData as $thisKey => $thisData){
																echo '<tr>';
																echo '<td><span title="' . $arrPaymentStatusTypeDatas2[$thisData["financialPaymentStatisticsStatus"]]["paymentStatusTypesName"] . '">' . $thisData["financialPaymentStatisticsStatus"] . '</span></td>';
																echo '<td style="text-align:right;">' . $thisData["financialPaymentStatisticsItemsCount"] . '</td>';
																echo '<td style="text-align:right;">' . number_format($thisData["financialPaymentStatisticsTotalSum"], 2, ',', '.') . '</td>';
																echo '</tr>';
																$thisBankAccountTotalSumPayments_RE += $thisData["financialPaymentStatisticsTotalSum"];
																$thisBankAccountTotalItemsPayments_RE += $thisData["financialPaymentStatisticsItemsCount"];

																if($thisKey == 'TB'){
																	$thisTotalSumPaymentsPartlyPaid_RE += $thisData["financialPaymentStatisticsTotalSum"];
																}
															}

															echo '<tr class="row2" style="font-weight:bold;">';

															echo '<td>';
															echo '<b>Insg.:</b>';
															echo '</td>';

															echo '<td style="text-align:right;">';
															echo $thisBankAccountTotalItemsPayments_RE;
															echo '</td>';

															echo '<td style="text-align:right;">';
															echo number_format($thisBankAccountTotalSumPayments_RE, 2, ',', '.');
															echo '</td>';

															echo '</tr>';
															echo '</table>';
															echo '<hr />';
															$thisTotalSumPayments_RE += $thisBankAccountTotalSumPayments_RE;
														}
													}
													echo '<hr class="doubleline" />';
													echo '</div>';

													echo '<div style="text-align:right;">';
													echo '<b>' . number_format($thisTotalSumPayments_RE, 2, ',', '.') . '</b>';
													# echo ' <img src="layout/icons/iconToggle3.png" class="buttonShowDetails" width="10" height="10" alt="Details" title="Details anzeigen" />';
													echo '</div>';
												}
												echo '</td>';
											// EOF PAYMENTS RE

											// EOF WRITTEN RE
												echo '<td>';
												$thisTotalSumWritten_RE = 0;
												if(!empty($thisDateValue["written_RE"])){
													echo '<div class="dataBoxItemContent" style="display:none;">';
													foreach($thisDateValue["written_RE"] as $thisBankAccountKey => $thisBankAccountData){
														echo '<p style="white-space:nowrap;font-size:11px;margin:0 0 2px 0;padding:0"><!--<b>Bank</b>: -->' . $arrBankAccountTypeDatas[$thisBankAccountKey]["bankAccountTypesName"] . '<br /><b>Konto</b>: ' . $arrBankAccountTypeDatas[$thisBankAccountKey]["bankAccountTypesAccountNumber"] . ' [' . $thisBankAccountKey . ']' . '</p>';

														if(!empty($thisBankAccountData)){
															echo '<table cellpadding="0" cellspacing="0" border="0" width="100%" class="dataBoxTable">';
															echo '<tr>';
															echo '<th>Status</th>';
															echo '<th>Menge</th>';
															echo '<th>Summe</th>';
															echo '</tr>';
															$thisBankAccountTotalSumWritten_RE = 0;
															$thisBankAccountTotalItemsWritten_RE = 0;
															foreach($thisBankAccountData as $thisKey => $thisData){
																echo '<tr>';
																echo '<td><span title="' . $arrPaymentStatusTypeDatas2[$thisData["financialPaymentStatisticsStatus"]]["paymentStatusTypesName"] . '">' . $thisData["financialPaymentStatisticsStatus"] . '</span></td>';
																echo '<td style="text-align:right;">' . $thisData["financialPaymentStatisticsItemsCount"] . '</td>';
																echo '<td style="text-align:right;">' . number_format($thisData["financialPaymentStatisticsTotalSum"], 2, ',', '.') . '</td>';
																echo '</tr>';
																$thisBankAccountTotalSumWritten_RE += $thisData["financialPaymentStatisticsTotalSum"];
																$thisBankAccountTotalItemsWritten_RE += $thisData["financialPaymentStatisticsItemsCount"];
															}

															echo '<tr class="row2" style="font-weight:bold;">';

															echo '<td>';
															echo '<b>Insg.:</b>';
															echo '</td>';

															echo '<td style="text-align:right;">';
															echo $thisBankAccountTotalItemsWritten_RE;
															echo '</td>';

															echo '<td style="text-align:right;">';
															echo number_format($thisBankAccountTotalSumWritten_RE, 2, ',', '.');
															echo '</td>';

															echo '</tr>';
															echo '</table>';
															echo '<hr />';
															$thisTotalSumWritten_RE += $thisBankAccountTotalSumWritten_RE;
														}
													}
													echo '<hr class="doubleline" />';
													echo '</div>';

													echo '<div style="text-align:right;">';
													echo '<b>' . number_format($thisTotalSumWritten_RE, 2, ',', '.') . '</b>';
													# echo ' <img src="layout/icons/iconToggle3.png" class="buttonShowDetails" width="10" height="10" alt="Details" title="Details anzeigen" />';
													echo '</div>';
												}
												echo '</td>';
											// EOF WRITTEN RE

											// EOF WRITTEN GS
												echo '<td>';
												$thisTotalSumWritten_GS = 0;
												if(!empty($thisDateValue["written_GS"])){
													echo '<div class="dataBoxItemContent" style="display:none;">';
													foreach($thisDateValue["written_GS"] as $thisBankAccountKey => $thisBankAccountData){
														echo '<p style="white-space:nowrap;font-size:11px;margin:0 0 2px 0;padding:0"><!--<b>Bank</b>: -->' . $arrBankAccountTypeDatas[$thisBankAccountKey]["bankAccountTypesName"] . '<br /><b>Konto</b>: ' . $arrBankAccountTypeDatas[$thisBankAccountKey]["bankAccountTypesAccountNumber"] . ' [' . $thisBankAccountKey . ']' . '</p>';

														if(!empty($thisBankAccountData)){
															echo '<table cellpadding="0" cellspacing="0" border="0" width="100%" class="dataBoxTable">';
															echo '<tr>';
															echo '<th>Status</th>';
															echo '<th>Menge</th>';
															echo '<th>Summe</th>';
															echo '</tr>';
															$thisBankAccountTotalSumWritten_GS = 0;
															$thisBankAccountTotalItemsWritten_GS = 0;
															foreach($thisBankAccountData as $thisKey => $thisData){
																echo '<tr>';
																echo '<td><span title="' . $arrPaymentStatusTypeDatas2[$thisData["financialPaymentStatisticsStatus"]]["paymentStatusTypesName"] . '">' . $thisData["financialPaymentStatisticsStatus"] . '</span></td>';
																echo '<td style="text-align:right;">' . $thisData["financialPaymentStatisticsItemsCount"] . '</td>';
																echo '<td style="text-align:right;">' . number_format($thisData["financialPaymentStatisticsTotalSum"], 2, ',', '.') . '</td>';
																echo '</tr>';
																$thisBankAccountTotalSumWritten_GS += $thisData["financialPaymentStatisticsTotalSum"];
																$thisBankAccountTotalItemsWritten_GS += $thisData["financialPaymentStatisticsItemsCount"];
															}

															echo '<tr class="row2" style="font-weight:bold;">';

															echo '<td>';
															echo '<b>Insg.:</b>';
															echo '</td>';

															echo '<td style="text-align:right;">';
															echo $thisBankAccountTotalItemsWritten_GS;
															echo '</td>';

															echo '<td style="text-align:right;">';
															echo number_format($thisBankAccountTotalSumWritten_GS, 2, ',', '.');
															echo '</td>';

															echo '</tr>';
															echo '</table>';
															echo '<hr />';
															$thisTotalSumWritten_GS += $thisBankAccountTotalSumWritten_GS;
														}
													}
													echo '<hr class="doubleline" />';
													echo '</div>';

													echo '<div style="text-align:right;">';
													echo '<b>' . number_format($thisTotalSumWritten_GS, 2, ',', '.') . '</b>';
													# echo ' <img src="layout/icons/iconToggle3.png" class="buttonShowDetails" width="10" height="10" alt="Details" title="Details anzeigen" />';
													echo '</div>';
												}
												echo '</td>';
											// EOF WRITTEN GS

											// BOF WRITTEN AB
												echo '<td>';
												$thisTotalSumWritten_AB = 0;
												if(!empty($thisDateValue["written_AB"])){
													echo '<div class="dataBoxItemContent" style="display:none;">';
													foreach($thisDateValue["written_AB"] as $thisBankAccountKey => $thisBankAccountData){
														echo '<p style="white-space:nowrap;font-size:11px;margin:0 0 2px 0;padding:0"><!--<b>Bank</b>: -->' . $arrBankAccountTypeDatas[$thisBankAccountKey]["bankAccountTypesName"] . '<br /><b>Konto</b>: ' . $arrBankAccountTypeDatas[$thisBankAccountKey]["bankAccountTypesAccountNumber"] . ' [' . $thisBankAccountKey . ']' . '</p>';

														if(!empty($thisBankAccountData)){
															echo '<table cellpadding="0" cellspacing="0" border="0" width="100%" class="dataBoxTable">';
															echo '<tr>';
															echo '<th>Status</th>';
															echo '<th>Menge</th>';
															echo '<th>Summe</th>';
															echo '</tr>';
															$thisBankAccountTotalSumWritten_AB = 0;
															$thisBankAccountTotalItemsWritten_AB = 0;
															foreach($thisBankAccountData as $thisKey => $thisData){
																echo '<tr>';
																echo '<td><span title="' . $arrPaymentStatusTypeDatas2[$thisData["financialPaymentStatisticsStatus"]]["paymentStatusTypesName"] . '">' . $thisData["financialPaymentStatisticsStatus"] . '</span></td>';
																echo '<td style="text-align:right;">' . $thisData["financialPaymentStatisticsItemsCount"] . '</td>';
																echo '<td style="text-align:right;">' . number_format($thisData["financialPaymentStatisticsTotalSum"], 2, ',', '.') . '</td>';
																echo '</tr>';
																$thisBankAccountTotalSumWritten_AB += $thisData["financialPaymentStatisticsTotalSum"];
																$thisBankAccountTotalItemsWritten_AB += $thisData["financialPaymentStatisticsItemsCount"];
															}

															echo '<tr class="row2" style="font-weight:bold;">';

															echo '<td>';
															echo '<b>Insg.:</b>';
															echo '</td>';

															echo '<td style="text-align:right;">';
															echo $thisBankAccountTotalItemsWritten_AB;
															echo '</td>';

															echo '<td style="text-align:right;">';
															echo number_format($thisBankAccountTotalSumWritten_AB, 2, ',', '.');
															echo '</td>';

															echo '</tr>';
															echo '</table>';
															echo '<hr />';
															$thisTotalSumWritten_AB += $thisBankAccountTotalSumWritten_AB;
														}
													}
													echo '<hr class="doubleline" />';
													echo '</div>';

													echo '<div style="text-align:right;">';
													echo '<b>' . number_format($thisTotalSumWritten_AB, 2, ',', '.') . '</b>';
													# echo ' <img src="layout/icons/iconToggle3.png" class="buttonShowDetails" width="10" height="10" alt="Details" title="Details anzeigen" />';
													echo '</div>';
												}
												echo '</td>';
											// EOF WRITTEN AB

											// BOF CALCULATION
												echo '<td>';
												$thisCalculationDifference = 0;
												$previousDayTimestamp = strtotime($thisDateKey);
												$previousDay = date('Y-m-d', mktime(0, 0, 0, date("m", $previousDayTimestamp), (date("d", $previousDayTimestamp) - 1), date("Y", $previousDayTimestamp)));

												if(!empty($arrFinancialStatisticData[$previousDay])){
													// BOF GET SUM AND DIFFERENCE OPEN RE PREVIOUS DAY AND DIFFERENCE
														$previousDaySumUnpaid_RE = 0;
														if(!empty($arrFinancialStatisticData[$previousDay]["unpaid_RE"])){
															foreach($arrFinancialStatisticData[$previousDay]["unpaid_RE"] as $thisBankAccountKey => $thisBankAccountData){
																foreach($thisBankAccountData as $thisStatusKey => $thisStatusData){
																	$previousDaySumUnpaid_RE +=  $thisStatusData["financialPaymentStatisticsTotalSum"];
																}
															}
														}
														$previousDayToCurrentDayDifferenceUnpaid_RE = $previousDaySumUnpaid_RE - $thisTotalSumUnpaid_RE;
													// BOF GET SUM AND DIFFERENCE OPEN RE PREVIOUS DAY AND DIFFERENCE

													// BOF GET SUM OPEN WRITTEN RE
														$totalSumOpenWritten_RE = 0;
														if(!empty($arrFinancialStatisticDataLive[$thisDateKey]["written_RE"])){
															foreach($arrFinancialStatisticDataLive[$thisDateKey]["written_RE"] as $thisBankAccountKey => $thisBankAccountData){
																foreach($thisBankAccountData as $thisStatusKey => $thisStatusData){
																	if($thisStatusKey != "BZ"){
																		$totalSumOpenWritten_RE +=  $thisStatusData["financialPaymentStatisticsTotalSum"];
																	}
																}
															}
														}

														$thisCalculationDifference = round(($previousDayToCurrentDayDifferenceUnpaid_RE + $totalSumOpenWritten_RE - $thisTotalSumPayments_RE - $thisTotalSumPayments_AB_with_RE + $thisTotalSumPaymentsPartlyPaid_RE), 2);

														echo '<div class="dataBoxItemContent" style="display:none;">';

														echo '<p style="white-space:nowrap;font-size:11px;margin:0 0 2px 0;padding:0">Differenzberechnung <br />zum Vortag (' . formatDate($previousDay, "display") . '):</p>';

														echo '<table cellpadding="0" cellspacing="0" border="0" width="100%" class="dataBoxTable">';
														echo '<tr>';
														echo '<th>&sum; offene REs Vortag: </th>';
														echo '<td style="text-align:right;">' . number_format($previousDaySumUnpaid_RE, 2, ',', '.'). '</td>';
														echo '</tr>';

														echo '<tr>';
														echo '<th>&sum; offene REs: </th>';
														echo '<td style="text-align:right;">-' . number_format($thisTotalSumUnpaid_RE, 2, ',', '.'). '</td>';
														echo '</tr>';

														echo '<tr>';
														echo '<td colspan="2"><hr /></td>';
														echo '</tr>';

														echo '<tr>';
														echo '<th>Differenz: </th>';
														echo '<td style="text-align:right;">' . number_format($previousDayToCurrentDayDifferenceUnpaid_RE, 2, ',', '.'). '</td>';
														echo '</tr>';

														echo '<tr>';
														echo '<th>&sum; neue OF-REs: </th>';
														echo '<td style="text-align:right;">+' . number_format($totalSumOpenWritten_RE, 2, ',', '.'). '</td>';
														echo '</tr>';

														echo '<tr>';
														echo '<th>&sum; RE Zahlung (Alle): </th>';
														echo '<td style="text-align:right;">-' . number_format($thisTotalSumPayments_RE, 2, ',', '.'). '</td>';
														echo '</tr>';

														echo '<tr>';
														echo '<th>&sum; RE Zahlung (TB): </th>';
														echo '<td style="text-align:right;">+' . number_format($thisTotalSumPaymentsPartlyPaid_RE, 2, ',', '.'). '</td>';
														echo '</tr>';

														echo '<tr>';
														echo '<th>&sum; AB mit RE Zahlung: </th>';
														echo '<td style="text-align:right;">-' . number_format($thisTotalSumPayments_AB_with_RE, 2, ',', '.'). '</td>';
														echo '</tr>';

														echo '<tr class="row2" style="font-weight:bold;">';
														echo '<td>Insg.: </td>';
														echo '<td style="text-align:right;">' . number_format($thisCalculationDifference, 2, ',', '.'). '</td>';
														echo '</tr>';

														echo '</table>';

														echo '<hr class="doubleline" />';
														echo '</div>';

														echo '<div style="text-align:right;">';
														echo '<b>' . number_format($thisCalculationDifference, 2, ',', '.') . '</b>';
														# echo ' <img src="layout/icons/iconToggle3.png" class="buttonShowDetails" width="10" height="10" alt="Details" title="Details anzeigen" />';
														echo '</div>';
													// EOF GET SUM OPEN WRITTEN RE
												}
												else {
													echo '<p class="infoArea">Keine Daten vom Vortag vorhanden oder Datum nicht in Auswahl!</p>';
												}
												echo '</td>';
											// EOF CALCULATION

											// BOF COMMENT
												echo '<td style="font-size:11px;" class="commentArea" data-rel="' . $thisDateKey . '">';
												echo '<div class="dataBoxItemContentComment" style="overflow:hidden;max-width:100%;max-height:14px">';
												echo '<span class="commentText">' . nl2br(utf8_decode($arrFinancialStatisticDataLive[$thisDateKey]["comment"])) . '</span>';
												echo '</div>';
												echo '</td>';
											// EOF COMMENT

											echo '</tr>';
											$countRow++;
										}
									echo '</tbody>';
								echo '</table>';
							}
						// EOF CURRENT LIVE DATA
					?>

					<hr />

					<h2>Historie:</h2>
					<?php if(!empty($arrFinancialStatisticData)) { ?>
						<table border="0" cellpadding="0" cellspacing="0" width="100%" class="displayOrders" style="width:100% !important;">
							<colgroup>
								<col width="12%" />
								<col width="11%" />
								<col width="11%" />
								<col width="11%" />
								<col width="11%" />
								<col width="11%" />
								<col width="11%" />
								<col width="11%" />
								<col width="11%" />
							</colgroup>
							<thead>
								<tr>
									<th width="12%">offene REs</th>
									<th width="11%">Zahlung AB</th>
									<th width="11%">Zahlung AB mit RE</th>
									<th width="11%">Zahlung RE</th>
									<th width="11%">geschriebene RE</th>
									<th width="11%">geschriebene GS</th>
									<th width="11%">geschriebene AB</th>
									<th width="11%">Differenz (?)</th>
									<th width="11%">Kommentar</th>
								</tr>
							</thead>
							<tbody>
								<?php
									#dd('arrFinancialStatisticData');
									$thisMarker = '';
									$countRow = 0;
									foreach($arrFinancialStatisticData as $thisDateKey => $thisDateValue){
										if($thisMarker != $thisDateKey){
											echo '<tr>';
											echo '<td class="tableRowTitle1" colspan="9">';
											echo '<span class="dataDate">' . formatDate($thisDateKey, "display") . '</span>';
											echo ' &bull; <b>Stand: </b>' . date("d.m.Y H:i:s", strtotime($arrStoreTime[$thisDateKey]));
											echo ' <img src="layout/icons/iconToggle3.png" class="buttonShowAllDetails" width="10" height="10" alt="Alle Details" title="Alle Details vom ' . formatDate($thisDateKey, "display") . 'anzeigen" />';
											echo '</td>';
											echo '</tr>';
											$thisMarker = $thisDateKey;
										}

										if($countRow%2 == 0){ $rowClass = 'row0'; }
										else { $rowClass = 'row1'; }

										echo '<tr class="' . $rowClass . '">';

										// BOF UNPAID RE
											echo '<td>';
											$thisTotalSumUnpaid_RE = 0;
											if(!empty($thisDateValue["unpaid_RE"])){
												echo '<div class="dataBoxItemContent" style="display:none;">';
												foreach($thisDateValue["unpaid_RE"] as $thisBankAccountKey => $thisBankAccountData){
													echo '<p style="white-space:nowrap;font-size:11px;margin:0 0 2px 0;padding:0"><!--<b>Bank</b>: -->' . $arrBankAccountTypeDatas[$thisBankAccountKey]["bankAccountTypesName"] . '<br /><b>Konto</b>: ' . $arrBankAccountTypeDatas[$thisBankAccountKey]["bankAccountTypesAccountNumber"] . ' [' . $thisBankAccountKey . ']' . '</p>';

													if(!empty($thisBankAccountData)){
														echo '<table cellpadding="0" cellspacing="0" border="0" width="100%" class="dataBoxTable">';
														echo '<tr>';
														echo '<th>Status</th>';
														echo '<th>Menge</th>';
														echo '<th>Summe</th>';
														echo '</tr>';
														$thisBankAccountTotalSumUnpaid_RE = 0;
														$thisBankAccountTotalItemsUnpaid_RE = 0;
														foreach($thisBankAccountData as $thisKey => $thisData){
															echo '<tr>';
															echo '<td><span title="' . $arrPaymentStatusTypeDatas2[$thisData["financialPaymentStatisticsStatus"]] . '">' . $thisData["financialPaymentStatisticsStatus"] . '</span></td>';
															echo '<td style="text-align:right;">' . $thisData["financialPaymentStatisticsItemsCount"] . '</td>';
															echo '<td style="text-align:right;">' . number_format($thisData["financialPaymentStatisticsTotalSum"], 2, ',', '.') . '</td>';
															echo '</tr>';
															$thisBankAccountTotalSumUnpaid_RE += $thisData["financialPaymentStatisticsTotalSum"];
															$thisBankAccountTotalItemsUnpaid_RE += $thisData["financialPaymentStatisticsItemsCount"];
														}

														echo '<tr class="row2" style="font-weight:bold;">';

														echo '<td>';
														echo '<b>Insg.:</b>';
														echo '</td>';

														echo '<td style="text-align:right;">';
														echo $thisBankAccountTotalItemsUnpaid_RE;
														echo '</td>';

														echo '<td style="text-align:right;">';
														echo number_format($thisBankAccountTotalSumUnpaid_RE, 2, ',', '.');
														echo '</td>';

														echo '</tr>';
														echo '</table>';
														echo '<hr />';
														$thisTotalSumUnpaid_RE += $thisBankAccountTotalSumUnpaid_RE;
													}
												}
												echo '<hr class="doubleline" />';
												echo '</div>';

												echo '<div style="text-align:right;">';
												echo '<b>' . number_format($thisTotalSumUnpaid_RE, 2, ',', '.') . '</b>';
												# echo ' <img src="layout/icons/iconToggle3.png" class="buttonShowDetails" width="10" height="10" alt="Details" title="Details anzeigen" />';
												echo '</div>';
											}

											echo '</td>';
										// EOF UNPAID RE

										// BOF PAYMENTS AB
											echo '<td>';
											$thisTotalSumPayments_AB = 0;
											if(!empty($thisDateValue["payments_AB"])){
												echo '<div class="dataBoxItemContent" style="display:none;">';
												foreach($thisDateValue["payments_AB"] as $thisBankAccountKey => $thisBankAccountData){
													echo '<p style="white-space:nowrap;font-size:11px;margin:0 0 2px 0;padding:0"><!--<b>Bank</b>: -->' . $arrBankAccountTypeDatas[$thisBankAccountKey]["bankAccountTypesName"] . '<br /><b>Konto</b>: ' . $arrBankAccountTypeDatas[$thisBankAccountKey]["bankAccountTypesAccountNumber"] . ' [' . $thisBankAccountKey . ']' . '</p>';

													if(!empty($thisBankAccountData)){
														echo '<table cellpadding="0" cellspacing="0" border="0" width="100%" class="dataBoxTable">';
														echo '<tr>';
														echo '<th>Status</th>';
														echo '<th>Menge</th>';
														echo '<th>Summe</th>';
														echo '</tr>';
														$thisBankAccountTotalSumPayments_AB = 0;
														$thisBankAccountTotalItemsPayments_AB = 0;
														foreach($thisBankAccountData as $thisKey => $thisData){
															echo '<tr>';
															echo '<td><span title="' . $arrPaymentStatusTypeDatas2[$thisData["financialPaymentStatisticsStatus"]]["paymentStatusTypesName"] . '">' . $thisData["financialPaymentStatisticsStatus"] . '</span></td>';
															echo '<td style="text-align:right;">' . $thisData["financialPaymentStatisticsItemsCount"] . '</td>';
															echo '<td style="text-align:right;">' . number_format($thisData["financialPaymentStatisticsTotalSum"], 2, ',', '.') . '</td>';
															echo '</tr>';
															$thisBankAccountTotalSumPayments_AB += $thisData["financialPaymentStatisticsTotalSum"];
															$thisBankAccountTotalItemsPayments_AB += $thisData["financialPaymentStatisticsItemsCount"];
														}

														echo '<tr class="row2" style="font-weight:bold;">';

														echo '<td>';
														echo '<b>Insg.:</b>';
														echo '</td>';

														echo '<td style="text-align:right;">';
														echo $thisBankAccountTotalItemsPayments_AB;
														echo '</td>';

														echo '<td style="text-align:right;">';
														echo number_format($thisBankAccountTotalSumPayments_AB, 2, ',', '.');
														echo '</td>';

														echo '</tr>';
														echo '</table>';
														echo '<hr />';
														$thisTotalSumPayments_AB += $thisBankAccountTotalSumPayments_AB;
													}
												}
												echo '<hr class="doubleline" />';
												echo '</div>';

												echo '<div style="text-align:right;">';
												echo '<b>' . number_format($thisTotalSumPayments_AB, 2, ',', '.') . '</b>';
												# echo ' <img src="layout/icons/iconToggle3.png" class="buttonShowDetails" width="10" height="10" alt="Details" title="Details anzeigen" />';
												echo '</div>';
											}
											echo '</td>';
										// EOF PAYMENTS AB

										// BOF PAYMENTS AB WITH RE
											echo '<td>';
											$thisTotalSumPayments_AB_with_RE = 0;
											if(!empty($thisDateValue["payments_AB_with_RE"])){
												echo '<div class="dataBoxItemContent" style="display:none;">';
												foreach($thisDateValue["payments_AB_with_RE"] as $thisBankAccountKey => $thisBankAccountData){
													echo '<p style="white-space:nowrap;font-size:11px;margin:0 0 2px 0;padding:0"><!--<b>Bank</b>: -->' . $arrBankAccountTypeDatas[$thisBankAccountKey]["bankAccountTypesName"] . '<br /><b>Konto</b>: ' . $arrBankAccountTypeDatas[$thisBankAccountKey]["bankAccountTypesAccountNumber"] . ' [' . $thisBankAccountKey . ']' . '</p>';

													if(!empty($thisBankAccountData)){
														echo '<table cellpadding="0" cellspacing="0" border="0" width="100%" class="dataBoxTable">';
														echo '<tr>';
														echo '<th>Status</th>';
														echo '<th>Menge</th>';
														echo '<th>Summe</th>';
														echo '</tr>';
														$thisBankAccountTotalSumPayments_AB_with_RE = 0;
														$thisBankAccountTotalItemsPayments_AB_with_RE = 0;
														foreach($thisBankAccountData as $thisKey => $thisData){
															echo '<tr>';
															echo '<td><span title="' . $arrPaymentStatusTypeDatas2[$thisData["financialPaymentStatisticsStatus"]]["paymentStatusTypesName"] . '">' . $thisData["financialPaymentStatisticsStatus"] . '</span></td>';
															echo '<td style="text-align:right;">' . $thisData["financialPaymentStatisticsItemsCount"] . '</td>';
															echo '<td style="text-align:right;">' . number_format($thisData["financialPaymentStatisticsTotalSum"], 2, ',', '.') . '</td>';
															echo '</tr>';
															$thisBankAccountTotalSumPayments_AB_with_RE += $thisData["financialPaymentStatisticsTotalSum"];
															$thisBankAccountTotalItemsPayments_AB_with_RE += $thisData["financialPaymentStatisticsItemsCount"];
														}

														echo '<tr class="row2" style="font-weight:bold;">';

														echo '<td>';
														echo '<b>Insg.:</b>';
														echo '</td>';

														echo '<td style="text-align:right;">';
														echo $thisBankAccountTotalItemsPayments_AB_with_RE;
														echo '</td>';

														echo '<td style="text-align:right;">';
														echo number_format($thisBankAccountTotalSumPayments_AB_with_RE, 2, ',', '.');
														echo '</td>';

														echo '</tr>';
														echo '</table>';
														echo '<hr />';
														$thisTotalSumPayments_AB_with_RE += $thisBankAccountTotalSumPayments_AB_with_RE;
													}
												}
												echo '<hr class="doubleline" />';
												echo '</div>';

												echo '<div style="text-align:right;">';
												echo '<b>' . number_format($thisTotalSumPayments_AB_with_RE, 2, ',', '.') . '</b>';
												# echo ' <img src="layout/icons/iconToggle3.png" class="buttonShowDetails" width="10" height="10" alt="Details" title="Details anzeigen" />';
												echo '</div>';
											}
											echo '</td>';
										// EOF PAYMENTS AB WITH RE

										// BOF PAYMENTS RE
											echo '<td>';
											$thisTotalSumPayments_RE = 0;
											$thisTotalSumPaymentsPartlyPaid_RE = 0;
											if(!empty($thisDateValue["payments_RE"])){
												echo '<div class="dataBoxItemContent" style="display:none;">';
												foreach($thisDateValue["payments_RE"] as $thisBankAccountKey => $thisBankAccountData){
													echo '<p style="white-space:nowrap;font-size:11px;margin:0 0 2px 0;padding:0"><!--<b>Bank</b>: -->' . $arrBankAccountTypeDatas[$thisBankAccountKey]["bankAccountTypesName"] . '<br /><b>Konto</b>: ' . $arrBankAccountTypeDatas[$thisBankAccountKey]["bankAccountTypesAccountNumber"] . ' [' . $thisBankAccountKey . ']' . '</p>';

													if(!empty($thisBankAccountData)){
														echo '<table cellpadding="0" cellspacing="0" border="0" width="100%" class="dataBoxTable">';
														echo '<tr>';
														echo '<th>Status</th>';
														echo '<th>Menge</th>';
														echo '<th>Summe</th>';
														echo '</tr>';
														$thisBankAccountTotalSumPayments_RE = 0;
														$thisBankAccountTotalItemsPayments_RE = 0;
														foreach($thisBankAccountData as $thisKey => $thisData){
															echo '<tr>';
															echo '<td><span title="' . $arrPaymentStatusTypeDatas2[$thisData["financialPaymentStatisticsStatus"]]["paymentStatusTypesName"] . '">' . $thisData["financialPaymentStatisticsStatus"] . '</span></td>';
															echo '<td style="text-align:right;">' . $thisData["financialPaymentStatisticsItemsCount"] . '</td>';
															echo '<td style="text-align:right;">' . number_format($thisData["financialPaymentStatisticsTotalSum"], 2, ',', '.') . '</td>';
															echo '</tr>';
															$thisBankAccountTotalSumPayments_RE += $thisData["financialPaymentStatisticsTotalSum"];
															$thisBankAccountTotalItemsPayments_RE += $thisData["financialPaymentStatisticsItemsCount"];

															if($thisKey == 'TB'){
																$thisTotalSumPaymentsPartlyPaid_RE += $thisData["financialPaymentStatisticsTotalSum"];
															}
														}

														echo '<tr class="row2" style="font-weight:bold;">';

														echo '<td>';
														echo '<b>Insg.:</b>';
														echo '</td>';

														echo '<td style="text-align:right;">';
														echo $thisBankAccountTotalItemsPayments_RE;
														echo '</td>';

														echo '<td style="text-align:right;">';
														echo number_format($thisBankAccountTotalSumPayments_RE, 2, ',', '.');
														echo '</td>';

														echo '</tr>';
														echo '</table>';
														echo '<hr />';
														$thisTotalSumPayments_RE += $thisBankAccountTotalSumPayments_RE;
													}
												}
												echo '<hr class="doubleline" />';
												echo '</div>';

												echo '<div style="text-align:right;">';
												echo '<b>' . number_format($thisTotalSumPayments_RE, 2, ',', '.') . '</b>';
												# echo ' <img src="layout/icons/iconToggle3.png" class="buttonShowDetails" width="10" height="10" alt="Details" title="Details anzeigen" />';
												echo '</div>';
											}
											echo '</td>';
										// EOF PAYMENTS RE

										// EOF WRITTEN RE
											echo '<td>';
											$thisTotalSumWritten_RE = 0;
											if(!empty($thisDateValue["written_RE"])){
												$thisTotalSumWritten_RE = 0;
												echo '<div class="dataBoxItemContent" style="display:none;">';
												foreach($thisDateValue["written_RE"] as $thisBankAccountKey => $thisBankAccountData){
													echo '<p style="white-space:nowrap;font-size:11px;margin:0 0 2px 0;padding:0"><!--<b>Bank</b>: -->' . $arrBankAccountTypeDatas[$thisBankAccountKey]["bankAccountTypesName"] . '<br /><b>Konto</b>: ' . $arrBankAccountTypeDatas[$thisBankAccountKey]["bankAccountTypesAccountNumber"] . ' [' . $thisBankAccountKey . ']' . '</p>';

													if(!empty($thisBankAccountData)){
														echo '<table cellpadding="0" cellspacing="0" border="0" width="100%" class="dataBoxTable">';
														echo '<tr>';
														echo '<th>Status</th>';
														echo '<th>Menge</th>';
														echo '<th>Summe</th>';
														echo '</tr>';
														$thisBankAccountTotalSumWritten_RE = 0;
														$thisBankAccountTotalItemsWritten_RE = 0;
														foreach($thisBankAccountData as $thisKey => $thisData){
															echo '<tr>';
															echo '<td><span title="' . $arrPaymentStatusTypeDatas2[$thisData["financialPaymentStatisticsStatus"]]["paymentStatusTypesName"] . '">' . $thisData["financialPaymentStatisticsStatus"] . '</span></td>';
															echo '<td style="text-align:right;">' . $thisData["financialPaymentStatisticsItemsCount"] . '</td>';
															echo '<td style="text-align:right;">' . number_format($thisData["financialPaymentStatisticsTotalSum"], 2, ',', '.') . '</td>';
															echo '</tr>';
															$thisBankAccountTotalSumWritten_RE += $thisData["financialPaymentStatisticsTotalSum"];
															$thisBankAccountTotalItemsWritten_RE += $thisData["financialPaymentStatisticsItemsCount"];
														}

														echo '<tr class="row2" style="font-weight:bold;">';

														echo '<td>';
														echo '<b>Insg.:</b>';
														echo '</td>';

														echo '<td style="text-align:right;">';
														echo $thisBankAccountTotalItemsWritten_RE;
														echo '</td>';

														echo '<td style="text-align:right;">';
														echo number_format($thisBankAccountTotalSumWritten_RE, 2, ',', '.');
														echo '</td>';

														echo '</tr>';
														echo '</table>';
														echo '<hr />';
														$thisTotalSumWritten_RE += $thisBankAccountTotalSumWritten_RE;
													}
												}
												echo '<hr class="doubleline" />';
												echo '</div>';

												echo '<div style="text-align:right;">';
												echo '<b>' . number_format($thisTotalSumWritten_RE, 2, ',', '.') . '</b>';
												# echo ' <img src="layout/icons/iconToggle3.png" class="buttonShowDetails" width="10" height="10" alt="Details" title="Details anzeigen" />';
												echo '</div>';
											}
											echo '</td>';
										// EOF WRITTEN RE

										// EOF WRITTEN GS
											echo '<td>';
											$thisTotalSumWritten_GS = 0;
											if(!empty($thisDateValue["written_GS"])){
												echo '<div class="dataBoxItemContent" style="display:none;">';
												foreach($thisDateValue["written_GS"] as $thisBankAccountKey => $thisBankAccountData){
													echo '<p style="white-space:nowrap;font-size:11px;margin:0 0 2px 0;padding:0"><!--<b>Bank</b>: -->' . $arrBankAccountTypeDatas[$thisBankAccountKey]["bankAccountTypesName"] . '<br /><b>Konto</b>: ' . $arrBankAccountTypeDatas[$thisBankAccountKey]["bankAccountTypesAccountNumber"] . ' [' . $thisBankAccountKey . ']' . '</p>';

													if(!empty($thisBankAccountData)){
														echo '<table cellpadding="0" cellspacing="0" border="0" width="100%" class="dataBoxTable">';
														echo '<tr>';
														echo '<th>Status</th>';
														echo '<th>Menge</th>';
														echo '<th>Summe</th>';
														echo '</tr>';
														$thisBankAccountTotalSumWritten_GS = 0;
														$thisBankAccountTotalItemsWritten_GS = 0;
														foreach($thisBankAccountData as $thisKey => $thisData){
															echo '<tr>';
															echo '<td><span title="' . $arrPaymentStatusTypeDatas2[$thisData["financialPaymentStatisticsStatus"]]["paymentStatusTypesName"] . '">' . $thisData["financialPaymentStatisticsStatus"] . '</span></td>';
															echo '<td style="text-align:right;">' . $thisData["financialPaymentStatisticsItemsCount"] . '</td>';
															echo '<td style="text-align:right;">' . number_format($thisData["financialPaymentStatisticsTotalSum"], 2, ',', '.') . '</td>';
															echo '</tr>';
															$thisBankAccountTotalSumWritten_GS += $thisData["financialPaymentStatisticsTotalSum"];
															$thisBankAccountTotalItemsWritten_GS += $thisData["financialPaymentStatisticsItemsCount"];
														}

														echo '<tr class="row2" style="font-weight:bold;">';

														echo '<td>';
														echo '<b>Insg.:</b>';
														echo '</td>';

														echo '<td style="text-align:right;">';
														echo $thisBankAccountTotalItemsWritten_GS;
														echo '</td>';

														echo '<td style="text-align:right;">';
														echo number_format($thisBankAccountTotalSumWritten_GS, 2, ',', '.');
														echo '</td>';

														echo '</tr>';
														echo '</table>';
														echo '<hr />';
														$thisTotalSumWritten_GS += $thisBankAccountTotalSumWritten_GS;
													}
												}
												echo '<hr class="doubleline" />';
												echo '</div>';

												echo '<div style="text-align:right;">';
												echo '<b>' . number_format($thisTotalSumWritten_GS, 2, ',', '.') . '</b>';
												# echo ' <img src="layout/icons/iconToggle3.png" class="buttonShowDetails" width="10" height="10" alt="Details" title="Details anzeigen" />';
												echo '</div>';
											}
											echo '</td>';
										// EOF WRITTEN GS

										// BOF WRITTEN AB
											echo '<td>';
											$thisTotalSumWritten_AB = 0;
											if(!empty($thisDateValue["written_AB"])){
												echo '<div class="dataBoxItemContent" style="display:none;">';
												foreach($thisDateValue["written_AB"] as $thisBankAccountKey => $thisBankAccountData){
													echo '<p style="white-space:nowrap;font-size:11px;margin:0 0 2px 0;padding:0"><!--<b>Bank</b>: -->' . $arrBankAccountTypeDatas[$thisBankAccountKey]["bankAccountTypesName"] . '<br /><b>Konto</b>: ' . $arrBankAccountTypeDatas[$thisBankAccountKey]["bankAccountTypesAccountNumber"] . ' [' . $thisBankAccountKey . ']' . '</p>';

													if(!empty($thisBankAccountData)){
														echo '<table cellpadding="0" cellspacing="0" border="0" width="100%" class="dataBoxTable">';
														echo '<tr>';
														echo '<th>Status</th>';
														echo '<th>Menge</th>';
														echo '<th>Summe</th>';
														echo '</tr>';
														$thisBankAccountTotalSumWritten_AB = 0;
														$thisBankAccountTotalItemsWritten_AB = 0;
														foreach($thisBankAccountData as $thisKey => $thisData){
															echo '<tr>';
															echo '<td><span title="' . $arrPaymentStatusTypeDatas2[$thisData["financialPaymentStatisticsStatus"]]["paymentStatusTypesName"] . '">' . $thisData["financialPaymentStatisticsStatus"] . '</span></td>';
															echo '<td style="text-align:right;">' . $thisData["financialPaymentStatisticsItemsCount"] . '</td>';
															echo '<td style="text-align:right;">' . number_format($thisData["financialPaymentStatisticsTotalSum"], 2, ',', '.') . '</td>';
															echo '</tr>';
															$thisBankAccountTotalSumWritten_AB += $thisData["financialPaymentStatisticsTotalSum"];
															$thisBankAccountTotalItemsWritten_AB += $thisData["financialPaymentStatisticsItemsCount"];
														}

														echo '<tr class="row2" style="font-weight:bold;">';

														echo '<td>';
														echo '<b>Insg.:</b>';
														echo '</td>';

														echo '<td style="text-align:right;">';
														echo $thisBankAccountTotalItemsWritten_AB;
														echo '</td>';

														echo '<td style="text-align:right;">';
														echo number_format($thisBankAccountTotalSumWritten_AB, 2, ',', '.');
														echo '</td>';

														echo '</tr>';
														echo '</table>';
														echo '<hr />';
														$thisTotalSumWritten_AB += $thisBankAccountTotalSumWritten_AB;
													}
												}
												echo '<hr class="doubleline" />';
												echo '</div>';

												echo '<div style="text-align:right;">';
												echo '<b>' . number_format($thisTotalSumWritten_AB, 2, ',', '.') . '</b>';
												# echo ' <img src="layout/icons/iconToggle3.png" class="buttonShowDetails" width="10" height="10" alt="Details" title="Details anzeigen" />';
												echo '</div>';
											}
											echo '</td>';
										// EOF WRITTEN AB

										// BOF CALCULATION
											echo '<td>';
											$thisCalculationDifference = 0;
											$previousDayTimestamp = strtotime($thisDateKey);
											$previousDay = date('Y-m-d', mktime(0, 0, 0, date("m", $previousDayTimestamp), (date("d", $previousDayTimestamp) - 1), date("Y", $previousDayTimestamp)));

											if(!empty($arrFinancialStatisticData[$previousDay])){
												// BOF GET SUM AND DIFFERENCE OPEN RE PREVIOUS DAY AND DIFFERENCE
													$previousDaySumUnpaid_RE = 0;
													if(!empty($arrFinancialStatisticData[$previousDay]["unpaid_RE"])){
														foreach($arrFinancialStatisticData[$previousDay]["unpaid_RE"] as $thisBankAccountKey => $thisBankAccountData){
															foreach($thisBankAccountData as $thisStatusKey => $thisStatusData){
																$previousDaySumUnpaid_RE +=  $thisStatusData["financialPaymentStatisticsTotalSum"];
															}
														}
													}
													$previousDayToCurrentDayDifferenceUnpaid_RE = $previousDaySumUnpaid_RE - $thisTotalSumUnpaid_RE;
												// BOF GET SUM AND DIFFERENCE OPEN RE PREVIOUS DAY AND DIFFERENCE

												// BOF GET SUM OPEN WRITTEN RE
													$totalSumOpenWritten_RE = 0;
													if(!empty($arrFinancialStatisticData[$thisDateKey]["written_RE"])){
														foreach($arrFinancialStatisticData[$thisDateKey]["written_RE"] as $thisBankAccountKey => $thisBankAccountData){
															foreach($thisBankAccountData as $thisStatusKey => $thisStatusData){
																if($thisStatusKey != "BZ"){
																	$totalSumOpenWritten_RE +=  $thisStatusData["financialPaymentStatisticsTotalSum"];
																}
															}
														}
													}

													$thisCalculationDifference = round(($previousDayToCurrentDayDifferenceUnpaid_RE + $totalSumOpenWritten_RE - $thisTotalSumPayments_RE - $thisTotalSumPayments_AB_with_RE + $thisTotalSumPaymentsPartlyPaid_RE), 2);

													echo '<div class="dataBoxItemContent" style="display:none;">';

													echo '<p style="white-space:nowrap;font-size:11px;margin:0 0 2px 0;padding:0">Differenzberechnung <br />zum Vortag (' . formatDate($previousDay, "display") . '):</p>';

													echo '<table cellpadding="0" cellspacing="0" border="0" width="100%" class="dataBoxTable">';
													echo '<tr>';
													echo '<th>&sum; offene REs Vortag: </th>';
													echo '<td style="text-align:right;">' . number_format($previousDaySumUnpaid_RE, 2, ',', '.'). '</td>';
													echo '</tr>';

													echo '<tr>';
													echo '<th>&sum; offene REs: </th>';
													echo '<td style="text-align:right;">-' . number_format($thisTotalSumUnpaid_RE, 2, ',', '.'). '</td>';
													echo '</tr>';

													echo '<tr>';
													echo '<td colspan="2"><hr /></td>';
													echo '</tr>';

													echo '<tr>';
													echo '<th>Differenz: </th>';
													echo '<td style="text-align:right;">' . number_format($previousDayToCurrentDayDifferenceUnpaid_RE, 2, ',', '.'). '</td>';
													echo '</tr>';

													echo '<tr>';
													echo '<th>&sum; neue OF-REs: </th>';
													echo '<td style="text-align:right;">+' . number_format($totalSumOpenWritten_RE, 2, ',', '.'). '</td>';
													echo '</tr>';

													echo '<tr>';
													echo '<th>&sum; RE Zahlung (Alle): </th>';
													echo '<td style="text-align:right;">-' . number_format($thisTotalSumPayments_RE, 2, ',', '.'). '</td>';
													echo '</tr>';

													echo '<tr>';
													echo '<th>&sum; RE Zahlung (TB): </th>';
													echo '<td style="text-align:right;">+' . number_format($thisTotalSumPaymentsPartlyPaid_RE, 2, ',', '.'). '</td>';
													echo '</tr>';

													echo '<tr>';
													echo '<th>&sum; AB mit RE Zahlung: </th>';
													echo '<td style="text-align:right;">-' . number_format($thisTotalSumPayments_AB_with_RE, 2, ',', '.'). '</td>';
													echo '</tr>';

													echo '<tr class="row2" style="font-weight:bold;">';
													echo '<td>Insg.: </td>';
													echo '<td style="text-align:right;">' . number_format($thisCalculationDifference, 2, ',', '.'). '</td>';
													echo '</tr>';

													echo '</table>';

													echo '<hr class="doubleline" />';
													echo '</div>';

													echo '<div style="text-align:right;">';
													echo '<b>' . number_format($thisCalculationDifference, 2, ',', '.') . '</b>';
													# echo ' <img src="layout/icons/iconToggle3.png" class="buttonShowDetails" width="10" height="10" alt="Details" title="Details anzeigen" />';
													echo '</div>';
												// EOF GET SUM OPEN WRITTEN RE
											}
											else {
												echo '<p class="infoArea">Keine Daten vom Vortag vorhanden oder Datum nicht in Auswahl!</p>';
											}
											echo '</td>';
										// EOF CALCULATION

										// BOF COMMENT
											echo '<td style="font-size:11px;" class="commentArea" data-rel="' . $thisDateKey . '">';
											#echo $thisDateValue["financialPaymentStatisticsComment"];
											if(1 || $arrDateComments[$thisDateKey] != ""){
												echo '<div class="dataBoxItemContentComment" style="overflow:hidden;max-width:100%;max-height:14px">';
												echo '<span class="commentText">' . nl2br(utf8_decode($arrDateComments[$thisDateKey])) . '</span>';
												echo '</div>';
											}
											echo '</td>';
										// EOF COMMENT

										echo '</tr>';
										$countRow++;
									}
								?>
							</tbody>
						</table>

					<?php } else { ?>
						<p class="infoArea">Es qurden keine Daten gefunden!</p>
					<?php } ?>
				</div>
			</div>
		</div>
	</div>

	<script language="javascript" type="text/javascript">
		<!--
		/* <![CDATA[ */
	
		$(document).ready(function() {
			$('.buttonToggleSidebarMenue').click(function() {
				$('#menueSidebarToggleContent').toggle();
			});

			$( "#datepicker" ).datepicker();
			$.datepicker.setDefaults($.datepicker.regional[""]);
			$('#searchDateFrom').datepicker($.datepicker.regional["de"]);
			$('#searchDateFrom').datepicker("option", "maxDate", "-1" );

			$('#searchDateTo').datepicker($.datepicker.regional["de"]);
			$('#searchDateTo').datepicker("option", "maxDate", "-1" );

			$('.buttonShowDetails').css({
				'margin-left': '10px',
				'float': 'right',
				'cursor': 'pointer'
			});
			$('.buttonShowDetails').live('click', function() {
				$(this).parent().find('.dataBoxItemContent').toggle();
			});

			$('.buttonShowAllDetails').css({
				'margin-left': '10px',
				'float': 'right',
				'cursor': 'pointer'
			});
			$('.buttonShowAllDetails').live('click', function() {
				var nextTR = $(this).parent().parent().next('tr');
				nextTR.find('.dataBoxItemContent').toggle();
				if(nextTR.find('.dataBoxItemContent').css('display') == "none"){
					nextTR.find('.dataBoxItemContentComment').css({"overflow": "hidden", "max-width": "100%", "max-height": "14px"});
				}
				else{
					nextTR.find('.dataBoxItemContentComment').css({"overflow": "auto", "max-width": "100%", "max-height": "100%"});
				}
			});

			// BOF ADD COMMENT TO DATA
				<?php if($arrGetUserRights["adminArea"] == '1'){ ?>
					var editButton = '<img src="layout/icons/iconEdit.gif" width="14" height="14" class="buttonAddComment" alt="Kommentar hinzuf&uuml;gen" title="Kommentar hinzuf&uuml;gen" \/>';
					var storeButton = '<img src="layout/icons/iconSave.png" width="14" height="14" class="buttonStoreComment" alt="Kommentar speichern" title="Kommentar speichern" \/>';
					var cancelButton = '<img src="layout/icons/iconClose.png" width="14" height="14" class="buttonCancelComment" alt="Kommentar abbrechen" title="Kommentar abbrechen" \/>';
					$('.commentArea').append(editButton);
					$('.buttonAddComment').css({"cursor": "pointer", "float": "right"});
					$('.buttonAddComment').live('click', function(){
						var dataDate = $(this).parent().attr('data-rel');
						var commentTextElement = $(this).parent().parent().find('.commentText');
						var dataBoxItemContentComment = $(this).parent().find('.dataBoxItemContentComment');
						dataBoxItemContentComment.css({"overflow": "show", "max-height": "100%"});
						var dataComment = commentTextElement.text();

						var commentForm = '';
						commentForm += '<form class="formAddComment" name="formAddComment_' + dataDate + '" method="post" action="">';
						commentForm += '<textarea name="addDataCommentText" class="addDataCommentText" style="font-size:11px;font-family:Arial;height:200px;width:96%;border:1px solid #999;">' + dataComment + '<\/textarea>';
						commentForm += '<input type="hidden" name="addDataCommentDate" value="' + dataDate + '" \/>';
						commentForm += '</form>';
						commentForm += storeButton;
						commentForm += cancelButton;
						commentTextElement.replaceWith(commentForm);

						$(this).remove();
						$('.buttonStoreComment').css({"cursor": "pointer", "float": "right", "padding-right": "10px"});
						$('.buttonCancelComment').css({"cursor": "pointer", "float": "right", "padding-right": "10px"});
					});

					$('.buttonCancelComment').live('click', function(){
						var dataComment = $(this).parent().parent().find('.addDataCommentText').val();
						$(this).parent().find('.formAddComment').remove();
						$(this).parent().append('<span class="commentText">' + dataComment + '</span>');
						$(this).parent().append(editButton);
						$(this).parent().find('.buttonStoreComment').remove();
						$(this).remove();
					});

					$('.buttonStoreComment').live('click', function(){
						var formAddComment = $(this).parent().find('form');
						var formAddCommentName = formAddComment.attr('name');
						formAddComment.submit();
					});

				<?php } ?>
			// EOF ADD COMMENT TO DATA

		});
		/* ]]> */
		// -->
	</script>
<?php require_once('inc/footerHTML.inc.php'); ?>