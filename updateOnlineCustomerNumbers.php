<?php
	require_once('inc/requires.inc.php');

	if(!$arrGetUserRights["updateOnlineCustomers"]) {
		header('location: ' . PAGE_EXIT_LOCATION);
		exit;
	}

	$jswindowMessage = "";
	$warningMessage = '';
	$errorMessage = '';
	$successMessage = '';
	$infoMessage = '';

	$dbConnection = new DB_Connection();
	$db_open = $dbConnection->db_connect();

	$dbConnection_ExternShop = new DB_Connection(DB_HOST_EXTERN_SHOP, '', DB_NAME_EXTERN_SHOP, DB_USER_EXTERN_SHOP, DB_PASSWORD_EXTERN_SHOP);
	$dbOpen_ExternShop = $dbConnection_ExternShop->db_connect();

	#dd('DB_NAME_EXTERN_SHOP');
?>

<?php
	require_once('inc/headerHTML.inc.php');
	$thisTitle = '&quot;' . COMPANY_INTERNET . '&quot;-Kunden die Auftragslisten-Kundennummer zuweisen';
	$headerHTML = preg_replace("/{###TITLE###}/", strip_tags($thisTitle), $headerHTML);
	echo $headerHTML;

	require_once(FILE_MENUE_TOP);
?>
<div id="mainArea">
	<div id="mainContent">
		<?php require_once(FILE_MENUE_SIDEBAR); ?>
		<div id="contentArea">
			<div id="contentAreaElements">
				<h1><?php if(SHOW_PAGE_TITLE_ICONS) { echo '<img src="' . PATH_ICONS_MENUE_TITLES . 'shop.png" alt="" />'; } ?> <?php echo $thisTitle; ?></h1>
				<div class="contentDisplay">

				<?php displayMessages(); ?>
				<form name="formUpdateOnlineCustomerNumbers" method="post" action="<?php echo $_SERVER["PHP_SELF"]; ?>">
					<div class="actionButtonsArea">
						<input type="submit" class="inputButton0" name="submitDatas" value="Aktualisierung starten" />
					</div>
				</form>
				<?php
					if($_POST["submitDatas"] != ''){

						$sql_local = "DROP TABLE IF EXISTS `_temp_" . MANDATOR . "_shop_customers`";
						$rs_local = $dbConnection->db_query($sql_local);

						$sql_local = "CREATE TABLE IF NOT EXISTS `_temp_" . MANDATOR . "_shop_customers` (
											`customers_customers_id` int(11) NOT NULL AUTO_INCREMENT,
											`customers_customers_cid` varchar(32) COLLATE latin1_german1_ci DEFAULT NULL,
											`customers_customers_vat_id` varchar(20) COLLATE latin1_german1_ci DEFAULT NULL,
											`customers_customers_vat_id_status` int(2) NOT NULL DEFAULT '0',
											`customers_customers_warning` varchar(32) COLLATE latin1_german1_ci DEFAULT NULL,
											`customers_customers_status` int(5) NOT NULL DEFAULT '1',
											`customers_customers_gender` char(1) COLLATE latin1_german1_ci NOT NULL,
											`customers_customers_firstname` varchar(32) COLLATE latin1_german1_ci NOT NULL,
											`customers_customers_lastname` varchar(32) COLLATE latin1_german1_ci NOT NULL,
											`customers_customers_manager` varchar(255) COLLATE latin1_german1_ci NOT NULL,
											`customers_customers_type` enum('Vertragshaendler','Gebrauchtwagenhaendler','Wiederverkaeufer','burhan') COLLATE latin1_german1_ci NOT NULL,
											`customers_customers_dob` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
											`customers_customers_email_address` varchar(96) COLLATE latin1_german1_ci NOT NULL,
											`customers_customers_default_address_id` int(11) NOT NULL,
											`customers_customers_telephone` varchar(32) COLLATE latin1_german1_ci NOT NULL,
											`customers_customers_fax` varchar(32) COLLATE latin1_german1_ci DEFAULT NULL,
											`customers_customers_password` varchar(40) COLLATE latin1_german1_ci NOT NULL,
											`customers_customers_newsletter` char(1) COLLATE latin1_german1_ci DEFAULT NULL,
											`customers_customers_newsletter_mode` char(1) COLLATE latin1_german1_ci NOT NULL DEFAULT '0',
											`customers_member_flag` char(1) COLLATE latin1_german1_ci NOT NULL DEFAULT '0',
											`customers_delete_user` char(1) COLLATE latin1_german1_ci NOT NULL DEFAULT '1',
											`customers_account_type` int(1) NOT NULL DEFAULT '0',
											`customers_password_request_key` varchar(32) COLLATE latin1_german1_ci NOT NULL,
											`customers_payment_unallowed` varchar(255) COLLATE latin1_german1_ci NOT NULL,
											`customers_shipping_unallowed` varchar(255) COLLATE latin1_german1_ci NOT NULL,
											`customers_refferers_id` varchar(32) COLLATE latin1_german1_ci NOT NULL DEFAULT '0',
											`customers_customers_date_added` datetime DEFAULT '0000-00-00 00:00:00',
											`customers_customers_last_modified` datetime DEFAULT '0000-00-00 00:00:00',

											`address_book_address_book_id` int(11) NOT NULL ,
											`address_book_customers_id` int(11) NOT NULL,
											`address_book_entry_gender` char(1) COLLATE latin1_german1_ci NOT NULL,
											`address_book_entry_company` varchar(255) COLLATE latin1_german1_ci DEFAULT NULL,
											`address_book_entry_firstname` varchar(32) COLLATE latin1_german1_ci NOT NULL,
											`address_book_entry_lastname` varchar(32) COLLATE latin1_german1_ci NOT NULL,
											`address_book_entry_street_address` varchar(64) COLLATE latin1_german1_ci NOT NULL,
											`address_book_entry_suburb` varchar(32) COLLATE latin1_german1_ci DEFAULT NULL,
											`address_book_entry_postcode` varchar(10) COLLATE latin1_german1_ci NOT NULL,
											`address_book_entry_city` varchar(32) COLLATE latin1_german1_ci NOT NULL,
											`address_book_entry_state` varchar(32) COLLATE latin1_german1_ci DEFAULT NULL,
											`address_book_entry_country_id` int(11) NOT NULL DEFAULT '0',
											`address_book_entry_zone_id` int(11) NOT NULL DEFAULT '0',
											`address_book_address_date_added` datetime DEFAULT '0000-00-00 00:00:00',
											`address_book_address_last_modified` datetime DEFAULT '0000-00-00 00:00:00',

											UNIQUE KEY `SORT_INDEX` (`customers_customers_email_address`,`customers_customers_id`),
											KEY `customers_customers_id` (`customers_customers_id`),
											KEY `customers_customers_firstname` (`customers_customers_firstname`),
											KEY `customers_customers_lastname` (`customers_customers_lastname`),
											KEY `customers_customers_email_address` (`customers_customers_email_address`),
											KEY `address_book_entry_street_address` (`address_book_entry_street_address`),
											KEY `address_book_entry_postcode` (`address_book_entry_postcode`)

									) ENGINE=MyISAM  DEFAULT CHARSET=latin1 COLLATE=latin1_german1_ci;
						";
						$rs_local = $dbConnection->db_query($sql_local);

						$sql_extern = "SELECT
									`" . TABLE_SHOP_CUSTOMERS . "`.`customers_id`,
									`" . TABLE_SHOP_CUSTOMERS . "`.`customers_cid`,
									`" . TABLE_SHOP_CUSTOMERS . "`.`customers_vat_id`,
									`" . TABLE_SHOP_CUSTOMERS . "`.`customers_vat_id_status`,
									`" . TABLE_SHOP_CUSTOMERS . "`.`customers_warning`,
									`" . TABLE_SHOP_CUSTOMERS . "`.`customers_status`,
									`" . TABLE_SHOP_CUSTOMERS . "`.`customers_gender`,
									`" . TABLE_SHOP_CUSTOMERS . "`.`customers_firstname`,
									`" . TABLE_SHOP_CUSTOMERS . "`.`customers_lastname`,
									`" . TABLE_SHOP_CUSTOMERS . "`.`customers_manager`,
									`" . TABLE_SHOP_CUSTOMERS . "`.`customers_type`,
									`" . TABLE_SHOP_CUSTOMERS . "`.`customers_dob`,
									`" . TABLE_SHOP_CUSTOMERS . "`.`customers_email_address`,
									`" . TABLE_SHOP_CUSTOMERS . "`.`customers_default_address_id`,
									`" . TABLE_SHOP_CUSTOMERS . "`.`customers_telephone`,
									`" . TABLE_SHOP_CUSTOMERS . "`.`customers_fax`,
									`" . TABLE_SHOP_CUSTOMERS . "`.`customers_password`,
									`" . TABLE_SHOP_CUSTOMERS . "`.`customers_newsletter`,
									`" . TABLE_SHOP_CUSTOMERS . "`.`customers_newsletter_mode`,
									`" . TABLE_SHOP_CUSTOMERS . "`.`member_flag`,
									`" . TABLE_SHOP_CUSTOMERS . "`.`delete_user`,
									`" . TABLE_SHOP_CUSTOMERS . "`.`account_type`,
									`" . TABLE_SHOP_CUSTOMERS . "`.`password_request_key`,
									`" . TABLE_SHOP_CUSTOMERS . "`.`payment_unallowed`,
									`" . TABLE_SHOP_CUSTOMERS . "`.`shipping_unallowed`,
									`" . TABLE_SHOP_CUSTOMERS . "`.`refferers_id`,
									`" . TABLE_SHOP_CUSTOMERS . "`.`customers_date_added`,
									`" . TABLE_SHOP_CUSTOMERS . "`.`customers_last_modified`,

									`" . TABLE_SHOP_ADDRESS_BOOK . "`.`address_book_id`,
									`" . TABLE_SHOP_ADDRESS_BOOK . "`.`customers_id` AS `customers_id2`,
									`" . TABLE_SHOP_ADDRESS_BOOK . "`.`entry_gender`,
									`" . TABLE_SHOP_ADDRESS_BOOK . "`.`entry_company`,
									`" . TABLE_SHOP_ADDRESS_BOOK . "`.`entry_firstname`,
									`" . TABLE_SHOP_ADDRESS_BOOK . "`.`entry_lastname`,
									`" . TABLE_SHOP_ADDRESS_BOOK . "`.`entry_street_address`,
									`" . TABLE_SHOP_ADDRESS_BOOK . "`.`entry_suburb`,
									`" . TABLE_SHOP_ADDRESS_BOOK . "`.`entry_postcode`,
									`" . TABLE_SHOP_ADDRESS_BOOK . "`.`entry_city`,
									`" . TABLE_SHOP_ADDRESS_BOOK . "`.`entry_state`,
									`" . TABLE_SHOP_ADDRESS_BOOK . "`.`entry_country_id`,
									`" . TABLE_SHOP_ADDRESS_BOOK . "`.`entry_zone_id`,
									`" . TABLE_SHOP_ADDRESS_BOOK . "`.`address_date_added`,
									`" . TABLE_SHOP_ADDRESS_BOOK . "`.`address_last_modified`

								FROM `" . TABLE_SHOP_CUSTOMERS . "`

								LEFT JOIN `" . TABLE_SHOP_ADDRESS_BOOK . "`
								ON(`" . TABLE_SHOP_CUSTOMERS . "`.`customers_id` = `" . TABLE_SHOP_ADDRESS_BOOK . "`.`customers_id`)

								WHERE
									`" . TABLE_SHOP_CUSTOMERS . "`.`customers_cid` IS NULL
									OR `" . TABLE_SHOP_CUSTOMERS . "`.`customers_cid` = '' ;
						";

						$rs_extern = $dbConnection_ExternShop->db_query($sql_extern);
						$countDatas = $dbConnection_ExternShop->db_getMysqlNumRows($rs_extern);

						$infoMessage .= 'Es wurden ' . $countDatas . ' in den Online-Kunden ohne Kundennummern gefunden.' . '<br />';

						if($countDatas > 0) {
							$sqlTemp_local = "INSERT IGNORE INTO `_temp_" . MANDATOR . "_shop_customers` (
												`customers_customers_id`,
												`customers_customers_cid`,
												`customers_customers_vat_id`,
												`customers_customers_vat_id_status`,
												`customers_customers_warning`,
												`customers_customers_status`,
												`customers_customers_gender`,
												`customers_customers_firstname`,
												`customers_customers_lastname`,
												`customers_customers_manager`,
												`customers_customers_type`,
												`customers_customers_dob`,
												`customers_customers_email_address`,
												`customers_customers_default_address_id`,
												`customers_customers_telephone`,
												`customers_customers_fax`,
												`customers_customers_password`,
												`customers_customers_newsletter`,
												`customers_customers_newsletter_mode`,
												`customers_member_flag`,
												`customers_delete_user`,
												`customers_account_type`,
												`customers_password_request_key`,
												`customers_payment_unallowed`,
												`customers_shipping_unallowed`,
												`customers_refferers_id`,
												`customers_customers_date_added`,
												`customers_customers_last_modified`,

												`address_book_address_book_id`,
												`address_book_customers_id`,
												`address_book_entry_gender`,
												`address_book_entry_company`,
												`address_book_entry_firstname`,
												`address_book_entry_lastname`,
												`address_book_entry_street_address`,
												`address_book_entry_suburb`,
												`address_book_entry_postcode`,
												`address_book_entry_city`,
												`address_book_entry_state`,
												`address_book_entry_country_id`,
												`address_book_entry_zone_id`,
												`address_book_address_date_added`,
												`address_book_address_last_modified`
											)
											VALUES
							";
							$fileName = tempnam("/tmp", "FOO");
							$infoMessage .= 'Die tempor&auml;re Datei &quot;' . $fileName . '&quot; wurde angelegt.' . '<br />';
							$fp = fopen($fileName, 'a+');
							fwrite($fp, removeUnnecessaryChars($sqlTemp_local) . "\n");
							$count = 0;

							while ($ds = mysqli_fetch_assoc($rs_extern)) {
								$ds["customers_telephone"] = cleanPhoneNumbers($ds["customers_telephone"]);
								$ds["customers_fax"] = cleanPhoneNumbers($ds["customers_fax"]);
								foreach($ds as $thisKey => $thisValue){
									$ds[$thisKey] = addslashes($thisValue);
								}
								$sqlTemp_local = "('" . implode("', '", $ds) . "')";
								if($count == ($countDatas - 1)) {
									$sqlTemp_local .= ";";
								}
								else {
									$sqlTemp_local .= ",";
								}
								fwrite($fp, removeUnnecessaryChars($sqlTemp_local) . "\n");
								$count++;
							}
							fclose($fp);

							$fp = fopen($fileName, 'r');
							$sqlTemp_local = fread($fp, filesize($fileName));
							fclose($fp);
							@unlink($fileName);

							$rsTemp_local = $dbConnection->db_query($sqlTemp_local);


							// BOF COMPARE E_MAIL
								$infoMessage . ' Zuweisung der Auftragslisten-Kundennummer im Online-Shop aufgrund der Mail-Adresse. ' . '<br />';
								$sql_local = "
										SELECT
											`_temp_" . MANDATOR . "_shop_customers`.`customers_customers_email_address`,
											`_temp_" . MANDATOR . "_shop_customers`.`customers_customers_id` AS `shopCustomerID`,
											CONCAT(
												'UPDATE `" . TABLE_SHOP_CUSTOMERS . "` SET `customers_cid` = \'',
												`" . TABLE_CUSTOMERS . "`.`customersKundennummer`,
												'\' WHERE `customers_email_address` = \'',
												`_temp_" . MANDATOR . "_shop_customers`.`customers_customers_email_address`,
												'\' AND (`customers_cid` IS NULL OR `customers_cid` = \'\');'
											) AS `sqlTemp_extern`

										FROM `_temp_" . MANDATOR . "_shop_customers`
										INNER  JOIN `" . TABLE_CUSTOMERS . "`
										ON(
											`_temp_" . MANDATOR . "_shop_customers`.`customers_customers_email_address` = `" . TABLE_CUSTOMERS . "`.`customersMail1`
											OR
											`_temp_" . MANDATOR . "_shop_customers`.`customers_customers_email_address` = `" . TABLE_CUSTOMERS . "`.`customersMail2`
										)

										WHERE 1

										GROUP BY `_temp_" . MANDATOR . "_shop_customers`.`customers_customers_email_address`
								";

								$rs_local = $dbConnection->db_query($sql_local);


								$countUpdateRows = 0;
								while ($ds = mysqli_fetch_assoc($rs_local)) {
									echo $ds["shopCustomerID"] . ' | ' . $ds["sqlTemp_extern"] . '<hr />';
									$sqlTemp_local = "DELETE FROM `_temp_" . MANDATOR . "_shop_customers` WHERE `customers_customers_id` = '" . $ds["shopCustomerID"] . "'";
									$rsTemp_local = $dbConnection->db_query($sqlTemp_local);
									$rsTemp_extern = $dbConnection_ExternShop->db_query($ds["sqlTemp_extern"]);
									if($rsTemp_extern) {
										$countUpdateRows++;
									}
								}

								$infoMessage .= 'Es wurden ' . $countUpdateRows . ' Kundennummern in den Kundendaten aufgrund der Mail-Adresse aktualisiert.' . '<br />';
							// EOF COMPARE E_MAIL

							// BOF COMPARE ADRESS AND PHONE NUMBERS
								if(false){
									$infoMessage .= ' Zuweisung der Auftragslisten-Kundennummer im Online-Shop aufgrund der Anschrift und der Telefonnummer. ' . '<br />';
									$sql_local = "
											SELECT
												`_temp_" . MANDATOR . "_shop_customers`.`customers_customers_email_address`,
												`_temp_" . MANDATOR . "_shop_customers`.`customers_customers_id` AS `shopCustomerID`,
												CONCAT(
													'UPDATE `" . TABLE_SHOP_CUSTOMERS . "` SET `customers_cid` = \'',
													`" . TABLE_CUSTOMERS . "`.`customersKundennummer`,
													'\' WHERE `customers_id` = \'',
													`_temp_" . MANDATOR . "_shop_customers`.`customers_customers_id`,
													'\' AND (`customers_cid` IS NULL OR `customers_cid` = \'\');'
												) AS `sqlTemp_extern`

											FROM `_temp_" . MANDATOR . "_shop_customers`
											INNER  JOIN `" . TABLE_CUSTOMERS . "`
											ON(
												/*
												`_temp_" . MANDATOR . "_shop_customers`.`address_book_entry_street_address` = CONCAT(`" . TABLE_CUSTOMERS . "`.`customersCompanyStrasse`, ' ', `" . TABLE_CUSTOMERS . "`.`customersCompanyHausnummer`)
												*/
												TRIM(REPLACE(REPLACE(REPLACE(`_temp_" . MANDATOR . "_shop_customers`.`address_book_entry_street_address`, ' ', ''), '-', ''), '.', '')) = TRIM(REPLACE(REPLACE(REPLACE(CONCAT(`" . TABLE_CUSTOMERS . "`.`customersCompanyStrasse`, `" . TABLE_CUSTOMERS . "`.`customersCompanyHausnummer`), ' ', ''), '-', ''), '.', ''))
												AND
												`_temp_" . MANDATOR . "_shop_customers`.`address_book_entry_postcode` = `" . TABLE_CUSTOMERS . "`.`customersCompanyPLZ`
												AND (
													(`_temp_" . MANDATOR . "_shop_customers`.`customers_customers_telephone` = `" . TABLE_CUSTOMERS . "`.`customersTelefon1` AND `_temp_" . MANDATOR . "_shop_customers`.`customers_customers_telephone` != ''  AND `_temp_" . MANDATOR . "_shop_customers`.`customers_customers_telephone` IS NOT NULL )
													OR
													(`_temp_" . MANDATOR . "_shop_customers`.`customers_customers_telephone` = `" . TABLE_CUSTOMERS . "`.`customersTelefon2` AND `_temp_" . MANDATOR . "_shop_customers`.`customers_customers_telephone` != ''  AND `_temp_" . MANDATOR . "_shop_customers`.`customers_customers_telephone` IS NOT NULL )
												)
											)

											WHERE 1

											GROUP BY `_temp_" . MANDATOR . "_shop_customers`.`customers_customers_email_address`
									";

									$rs_local = $dbConnection->db_query($sql_local);

									$countUpdateRows = 0;
									while ($ds = mysqli_fetch_assoc($rs_local)) {
										$infoMessage .= ' | ' . $ds["sqlTemp_extern"] . '<hr />';
										$sqlTemp_local = "DELETE FROM `_temp_" . MANDATOR . "_shop_customers` WHERE `customers_customers_id` = '" . $ds["shopCustomerID"] . "'";
										$rsTemp_local = $dbConnection->db_query($sqlTemp_local);
										$rsTemp_extern = $dbConnection_ExternShop->db_query($ds["sqlTemp_extern"]);
										if($rsTemp_extern) {
											$countUpdateRows++;
										}
									}

									$infoMessage .= 'Es wurden ' . $countUpdateRows . ' Kundennummern in den Kundendaten aufgrund der Anschrift aktualisiert.' . '<br />';

								}
							// EOF COMPARE ADRESS AND PHONE NUMBERS


							// BOF UPDATE ORDERS WITH CUSTOMER NUMBERS
								$sql_extern = "UPDATE `" . TABLE_SHOP_ORDERS . "`
											LEFT JOIN `" . TABLE_SHOP_CUSTOMERS . "`
											ON(`" . TABLE_SHOP_ORDERS . "`.`customers_id` = `" . TABLE_SHOP_CUSTOMERS . "`.`customers_id`)
											SET `" . TABLE_SHOP_ORDERS . "`.`customers_cid` = `" . TABLE_SHOP_CUSTOMERS . "`.`customers_cid`
											WHERE (
												`" . TABLE_SHOP_ORDERS . "`.`customers_cid` = ''
												OR
												`" . TABLE_SHOP_ORDERS . "`.`customers_cid` IS NULL
											)

								";
								dd('sql_extern', 1);
								$rs_extern = $dbConnection_ExternShop->db_query($sql_extern);

								$countUpdates = $dbConnection_ExternShop->db_getMysqlNumRows($rs_extern);
								if($countUpdates == '') { $countUpdates = 0; }
								$infoMessage .= 'Es wurden ' . $countUpdates . ' Kundennummern in den Online-Bestellungen aktualisiert.' . '<br />';
							// EOF UPDATE ORDERS WITH CUSTOMER NUMBERS

							$sql_local = "DROP TABLE IF EXISTS `_temp_" . MANDATOR . "_shop_customers`";
							$rs_local = $dbConnection->db_query($sql_local);

						}
					}
				?>
				<?php displayMessages(); ?>
				</div>
			</div>
		</div>
		<div class="clear"></div>
	</div>
</div>

<script language="javascript" type="text/javascript">
	$(document).ready(function() {
		colorRowMouseOver('.displayOrders tbody tr');
		// toggleAreas();
	});
</script>

<?php require_once('inc/footerHTML.inc.php'); ?>