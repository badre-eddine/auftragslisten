<?php
	session_start();

	require_once('config/configMandator.inc.php');
	require_once('config/configParams_' . strtolower(MANDATOR) . '.inc.php');
	require_once('config/configBasic.inc.php');
	require_once('config/configFiles.inc.php');
	require_once('config/configTables.inc.php');
	require_once('inc/functions.inc.php');
	##require_once('header.inc.php');

	DEFINE('SHOW_ZIPCODE_AREAS', true);
	DEFINE('SHOW_COUNTRY_AREAS', true);

	$arrColorsPLZ = array(
			'#FFF200',
			'#FDB913',
			'#F58220',
			'#E31D3C',
			'#F58F98',
			'#9A258F',
			'#0094C9',
			'#00C0E8',
			'#008060',
			'#23AA4A',
			'#80C342',
			'#905501',

			#'#FFCCFF',
			#'#FFCC99',
			#'#CC6600',
			#'#99CCCC',
			#'#66FF33',
			#'#FFFF33',
			#'#9acd32',
			#'#ee82ee',
			#'#d2b48c',
			#'#66cdaa',
			#'#98fb98',
			#'#00bfff',
			#'#f0b488',
		);

	$thisSearchString = (trim($_GET["salesmanID"]));

	$warningMessage = '';
	$errorMessage = '';
	$successMessage = '';
	$infoMessage = '';

	$useUf8Encode = false;

	$dbConnection = new DB_Connection(DB_HOST, '', DB_NAME, DB_USER, DB_PASSWORD);
	$dbOpen = $dbConnection->db_connect();

	// BOF GET DOUBLE DIGIT ZIPCODE AREAS
		$sql_mapDoubleDigitZipCodeAreas = "SELECT
					`zipcodeDoubleDigit`,
					`coordinates`

					FROM `" . TABLE_GEODB_DOUBLEDIGIT_AREAS . "`

					WHERE 1

			";
		$rs_mapDoubleDigitZipCodeAreas = $dbConnection->db_query($sql_mapDoubleDigitZipCodeAreas);
		$arrGoogleMapDoubleDigitZipCodeAreas = array();
		while($ds_mapDoubleDigitZipCodeAreas = mysqli_fetch_assoc($rs_mapDoubleDigitZipCodeAreas)) {
			$thisCoordinates = $ds_mapDoubleDigitZipCodeAreas["coordinates"];
			#dd('thisCoordinates');

			$searchPattern = "/(\[)([0-9\.\-]*),([0-9\.\-]*)(\])/ismU";
			$replace = "new google.maps.LatLng($2, $3)";

			$thisCoordinates = preg_replace($searchPattern, $replace, $thisCoordinates);
			$arrGoogleMapDoubleDigitZipCodeAreas[$ds_mapDoubleDigitZipCodeAreas["zipcodeDoubleDigit"]] = $thisCoordinates;
		}
		$arrGoogleMapZipCodeAreas = $arrGoogleMapDoubleDigitZipCodeAreas;

	// EOF GET DOUBLE DIGIT ZIPCODE AREAS

	// BOF GET SALESMEN DATAS
		$sql = "SELECT
				`" . TABLE_SALESMEN . "`.`salesmenID`,
				`" . TABLE_SALESMEN . "`.`salesmenKundennummer` AS `salesmenKundennummer2`,
				`" . TABLE_SALESMEN . "`.`salesmenFirmenname` AS `salesmenFirmenname2`,
				`" . TABLE_SALESMEN . "`.`salesmenAreas`,
				`" . TABLE_SALESMEN . "`.`salesmenAreasActive`,
				`" . TABLE_SALESMEN . "`.`salesmenTypesID`,
				`" . TABLE_SALESMEN . "`.`salesmenTypesName`,
				`" . TABLE_SALESMEN . "`.`salesmenActive`,
				`" . TABLE_SALESMEN . "`.`salesmenMapPoints`,
				`" . TABLE_SALESMEN . "`.`salesmenMapAreaPoints`,

				`" . TABLE_CUSTOMERS . "`.`customersID`,
				`" . TABLE_CUSTOMERS . "`.`customersKundennummer` AS `salesmenKundennummer`,
				`" . TABLE_CUSTOMERS . "`.`customersMail1` AS `salesmenMail1`,
				`" . TABLE_CUSTOMERS . "`.`customersMail2` AS `salesmenMail2`,
				`" . TABLE_CUSTOMERS . "`.`customersTelefon1` AS `salesmenTelefon1`,
				`" . TABLE_CUSTOMERS . "`.`customersTelefon2` AS `salesmenTelefon2`,
				`" . TABLE_CUSTOMERS . "`.`customersFax1` AS `salesmenFax1`,
				`" . TABLE_CUSTOMERS . "`.`customersFax2` AS `salesmenFax2`,
				`" . TABLE_CUSTOMERS . "`.`customersMobil1` AS `salesmenMobile1`,
				`" . TABLE_CUSTOMERS . "`.`customersMobil2` AS `salesmenMobile2`,

				`" . TABLE_CUSTOMERS . "`.`customersFirmenname` AS `salesmenFirmenname`,
				`" . TABLE_CUSTOMERS . "`.`customersFirmenInhaberVorname` AS `salesmenFirmenInhaberVorname`,
				`" . TABLE_CUSTOMERS . "`.`customersFirmenInhaberNachname` AS `salesmenFirmenInhaberNachname`,
				`" . TABLE_CUSTOMERS . "`.`customersCompanyPLZ` AS `salesmenCompanyPLZ`,
				`" . TABLE_CUSTOMERS . "`.`customersCompanyOrt` AS `salesmenCompanyOrt`,
				`" . TABLE_CUSTOMERS . "`.`customersCompanyCountry` AS `salesmenCompanyCountry`,
				`" . TABLE_CUSTOMERS . "`.`customersTyp` AS `salesmenTyp`

				FROM `" . TABLE_SALESMEN . "`

				LEFT JOIN `" . TABLE_CUSTOMERS . "`
				ON (`" . TABLE_SALESMEN . "`.`salesmenKundennummer` = `" . TABLE_CUSTOMERS . "`.`customersKundennummer`)

				WHERE 1
					AND `" . TABLE_SALESMEN . "`.`salesmenAreas` != ''
					AND `" . TABLE_SALESMEN . "`.`salesmenActive` = '1'
					AND `" . TABLE_SALESMEN . "`.`salesmenAreasActive` = '1'

			";
		$rs = $dbConnection->db_query($sql);

		$arrSalesmenDatas = array();
		$arrSalesmanAreas = array();
		$arrGoogleMapPoints = array();
		$countRows = 0;
		while($ds = mysqli_fetch_assoc($rs)) {
			foreach(array_keys($ds) as $field) {
				$arrSalesmenDatas[$ds["salesmenID"]][$field] = $ds[$field];
			}
			$arrTempSalesmenZipcode = explode(";", $ds["salesmenAreas"]);
			$arrSalesmanAreas[$ds["salesmenID"]] = $arrTempSalesmenZipcode;
			$arrSalesmanNames[$ds["salesmenID"]] = $ds["salesmenFirmenname"];
			foreach($arrTempSalesmenZipcode as $thisTempSalesmenZipcodeValue){
				$arrSalesmanGoogleMapZipCodeAreas[$ds["salesmenID"]][substr($thisTempSalesmenZipcodeValue, 0, 2)] = $arrGoogleMapZipCodeAreas[substr($thisTempSalesmenZipcodeValue, 0, 2)];
			}

			$arrSalesmanIDs[$ds["salesmenID"]] = $countRows;

			$countRows++;
		}
	// EOF GET SALESMEN DATAS

	// BOF GET GOOGLE MAPS AREAS
		$arrGoogleMapCountryAreas = array();
		if(SHOW_COUNTRY_AREAS){
			$sql_mapAreas = "SELECT
						`lat` AS `latitude`,
						`lon` AS `longitude`,
						`country`,
						`subcountry`,
						IF(`subcountry` != '' , CONCAT(`country`, '_', `subcountry`), `country`) AS `area`

						FROM `" . TABLE_GEODB_AREAS . "`

						WHERE 1
							AND `subcountry` = ''
							AND `country` = 'DE'
				";
			$rs_mapAreas = $dbConnection->db_query($sql_mapAreas);

			while($ds_mapAreas = mysqli_fetch_assoc($rs_mapAreas)) {
				$arrGoogleMapCountryAreas[$ds_mapAreas["area"]][] = 'new google.maps.LatLng(' . $ds_mapAreas["latitude"] . ', ' . $ds_mapAreas["longitude"] . ')';
			}
		}
	// ROF GET GOOGLE MAPS AREAS
?>

<?php
	require_once('inc/headerHTML.inc.php');
	$thisTitle = '';
	$headerHTML = preg_replace("/{###TITLE###}/", strip_tags($thisTitle), $headerHTML);
	$headerHTML = preg_replace('/<div id="headerArea">(.*)<noscript>/ism', '<noscript>', $headerHTML);
	$headerHTML = preg_replace('/<body/ism', '<body id="bodyFrame" onload="initialize();"', $headerHTML);

	echo $headerHTML;
?>
	<div id="contentAreaElements">
		<div id="myGoogleMapsArea">
			<div id="myGoogleMapCanvas">
				<span class="loadingArea"><img class="loader" src="layout/ajax-loader.gif" /> Bitte haben Sie ein wenig Geduld. Die Daten werden geladen...</span>
			</div>
		</div>
	</div>
	<script language="javascript" type="text/javascript">
		$('#myGoogleMapCanvas').css({
			'width': '100%',
			'height': ($(window).height() - 30) + 'px'
		});
	</script>

	<!-- GOOGLE MAPS START -->
	<script type="text/javascript" src="<?php echo PATH_GOOGLE_MAPS_API; ?>"></script>
	<script type="text/javascript" language="javascript">
		// try {
		function setGmapMarkers(map, i, k) {
			var infWindowKey = i+'_'+k;
			var thisMarkerImage;
			if(i == 0) {
				thisMarkerImage = "bctr_googleMarkerFlag.png";
				thisMarkerShadowImage = "bctr_googleMarkerFlagShadow.png";
				thisMarkerSize = new Array(81, 84);
				thisMarkerShadowSize = new Array(81, 84);
				thisZindex = (myPointDatas.length + 10);
			}
			else {
				thisMarkerImage = myPointDatas[i][k]["markerImage"];
				thisMarkerShadowImage = "googleMapsIconShadow.png";
				thisMarkerSize = new Array(28, 20);
				thisMarkerShadowSize = new Array(28, 20);
				thisZindex = (myPointDatas.length - i);
			}

			var image = new google.maps.MarkerImage(
						"layout/icons/" + thisMarkerImage,
						new google.maps.Size(thisMarkerSize[0], thisMarkerSize[1]),
						new google.maps.Point(0,0),
						new google.maps.Point(0, thisMarkerSize[1])
			);

			var shadow = new google.maps.MarkerImage(
						"layout/icons/" + thisMarkerShadowImage,
						new google.maps.Size(thisMarkerShadowSize[0], thisMarkerShadowSize[1]),
						new google.maps.Point(0, 0),
						new google.maps.Point(0, thisMarkerShadowSize[1])
			);

			var shape = {
				 // coord: [1, (thisMarkerSize[1] + 1 - 20), 1, thisMarkerSize[1], thisMarkerSize[0], thisMarkerSize[1], thisMarkerSize[0] , (thisMarkerSize[1] + 1 - 20)],
				coord: [1, 1, 1, 28, 20, 28, 20 , 1],
				type: "poly"
			};

			var myLatLng = new google.maps.LatLng(myPointDatas[i][k]["latitude"], myPointDatas[i][k]["longitude"]);

			marker[infWindowKey] = new google.maps.Marker({
				position: myLatLng,
				map: map,
				shadow: shadow,
				icon: image,
				shape: shape,
				title: myPointDatas[i][k]["title"],
				zIndex: thisZindex
			});

			shadow[infWindowKey] = new google.maps.Marker({
				position: myLatLng,
				map: map,
				icon: shadow,
				shape: shape,
				title: myPointDatas[i][k]["title"],
				zIndex: thisZindex
			});

			// DEFINE INFO WINDOW
				var html		= new Array();
				var imageExists	= false;
				html[infWindowKey] = "";
				html[infWindowKey] += "<div class=\"myGmapInfo\">";
				// html[infWindowKey] += "  <p class='headline'>" + myPointDatas[i][k]["title"] + "</p>";
				html[infWindowKey] += "  <p class='headline'>" + myPointDatas[i][k]["header"] + "</p>";
				if(myPointDatas[i][k]["imagePath"] != "") {
				  imageExists = true;
				  // html[infWindowKey] += '  <img src="' + myPointDatas[i][k]["imagePath"] + '" ' + myPointDatas[i][k]["imageDimension"] + ' alt="' + myPointDatas[i][k]["title"] + '"/>';
				}

				html[infWindowKey] += '<table border="0" width="" cellpadding="0" cellspacing="0">';

				if(myPointDatas[i][k]["text"] != "") {
				  // html[infWindowKey] += "  <p class='text'>" + myPointDatas[i][k]["text"].replace(/, /g, ",<br />") + "</p>";
				  html[infWindowKey] += "  <tr><td><b>Vertreter:</b></td><td>" + myPointDatas[i][k]["text"] + "</td></tr>";
				}

				if(myPointDatas[i][k]["city"] != "") {
				  // html[infWindowKey] += "  <tr><td><b>PLZ:</b></td><td>" + myPointDatas[i][k]["city"] + "</td></tr>";
				}

				if(myPointDatas[i][k]["region_1"] != "") {
				  html[infWindowKey] += "  <tr><td><b>Bundesland:</b></td><td>" + myPointDatas[i][k]["region_1"] + "</td></tr>";
				}

				if(myPointDatas[i][k]["region_2"] != "") {
				  html[infWindowKey] += "  <tr><td><b>Region:</b></td><td>" + myPointDatas[i][k]["region_2"] + "</td></tr>";
				}

				if(myPointDatas[i][k]["region_3"] != "") {
				  html[infWindowKey] += "  <tr><td><b>Kreis:</b></td><td>" + myPointDatas[i][k]["region_3"] + "</td></tr>";
				}

				html[infWindowKey] += "  <tr><td><b>Longitude:</b></td><td>" + myPointDatas[i][k]["longitude"] + "</td></tr>";
				html[infWindowKey] += "  <tr><td><b>Latitude: </b></td><td>" + myPointDatas[i][k]["latitude"] + "</td></tr>";

				html[infWindowKey] += '</table>';

				html[infWindowKey] += "<div class='clear'></div>";
				html[infWindowKey] += "</div>";

				infowindow[infWindowKey] = new Array();
				infowindow[infWindowKey] = new google.maps.InfoWindow({
					content: html[infWindowKey]
				});
			// END DEFINE INFO WINDOW

			// SHOW INFO WINDOW

				// DEFINE INFO WINDOW
				var html		= new Array();
				var imageExists	= false;
				html[infWindowKey] = "";
				html[infWindowKey] += "<div class=\"myGmapInfo\">";
				// html[infWindowKey] += "  <p class='headline'>" + myPointDatas[i][k]["title"] + "</p>";
				html[infWindowKey] += "  <p class='headline'>" + myPointDatas[i][k]["header"] + "</p>";
				if(myPointDatas[i][k]["imagePath"] != "") {
				  imageExists = true;
				  // html[infWindowKey] += '  <img src="' + myPointDatas[i][k]["imagePath"] + '" ' + myPointDatas[i][k]["imageDimension"] + ' alt="' + myPointDatas[i][k]["title"] + '"/>';
				}

				html[infWindowKey] += '<table border="0" width="" cellpadding="0" cellspacing="0">';

				if(myPointDatas[i][k]["text"] != "") {
				  // html[infWindowKey] += "  <p class='text'>" + myPointDatas[i][k]["text"].replace(/, /g, ",<br />") + "</p>";
				  html[infWindowKey] += "  <tr><td><b>Vertreter:</b></td><td>" + myPointDatas[i][k]["text"] + "</td></tr>";
				}

				if(myPointDatas[i][k]["city"] != "") {
				  // html[infWindowKey] += "  <tr><td><b>PLZ:</b></td><td>" + myPointDatas[i][k]["city"] + "</td></tr>";
				}

				if(myPointDatas[i][k]["region_1"] != "") {
				  html[infWindowKey] += "  <tr><td><b>Bundesland:</b></td><td>" + myPointDatas[i][k]["region_1"] + "</td></tr>";
				}

				if(myPointDatas[i][k]["region_2"] != "") {
				  html[infWindowKey] += "  <tr><td><b>Region:</b></td><td>" + myPointDatas[i][k]["region_2"] + "</td></tr>";
				}

				if(myPointDatas[i][k]["region_3"] != "") {
				  html[infWindowKey] += "  <tr><td><b>Kreis:</b></td><td>" + myPointDatas[i][k]["region_3"] + "</td></tr>";
				}

				html[infWindowKey] += "  <tr><td><b>Longitude:</b></td><td>" + myPointDatas[i][k]["longitude"] + "</td></tr>";
				html[infWindowKey] += "  <tr><td><b>Latitude: </b></td><td>" + myPointDatas[i][k]["latitude"] + "</td></tr>";

				html[infWindowKey] += '</table>';

				html[infWindowKey] += "<div class='clear'></div>";
				html[infWindowKey] += "</div>";

				infowindow[infWindowKey] = new Array();
				infowindow[infWindowKey] = new google.maps.InfoWindow({
					content: html[infWindowKey]
				});
			// END DEFINE INFO WINDOW

				google.maps.event.addListener(marker[infWindowKey], "click", function() {
					if(activeMarker != '') {
						infowindow[activeMarker].close();
					}
					try{
						infowindow[infWindowKey].open(map, marker[infWindowKey]);
					}
					catch(e){
						alert("error: " + e + " | i: " + i + " | k: " + k);
					}
					activeMarker = infWindowKey;
				});
				// createMarkerLinkInGmapSidebar(i, myPointDatas[i][k]["title"], marker[infWindowKey], imageExists);
			// END SHOW INFO WINDOW
		}

		/* BOF DEFINE DEFAULT VALUES */
			var myPointDatas	= new Array();

			var elementId = "myGoogleMapCanvas";
			var default_arrayKey	= 0;
			var default_lat			= "52.103138";
			var default_lon			= "7.622817";
			var default_zoom		= 6;

			var sidebarHtml		= "";              			/* HTML-Code für die gesamte Sidebar */
			var sidebarMarkers	= [];              			/* Array für die Marker */
			var sidebarCount	= 0;               			/* Zähler für die Marker (= Zeile in der Sidebar) */
			var marker				= new Array();
			var markerPolygonCenter	= new Array();
			var markerPolygonCenterShadow	= new Array();
			var activeMarker	= '';
			var activeMarkerPolygonCenter	= '';
			var infowindow			= new Array();
			var infowindowArea		= new Array();
		/* EOF DEFINE DEFAULT VALUES */

		/* BOF DEFINE POINT DATAS */
			myPointDatas[0] = new Array();
			myPointDatas[0][0] = new Array();
			myPointDatas[0][0]["latitude"] = default_lat;
			myPointDatas[0][0]["longitude"] = default_lon;
			myPointDatas[0][0]["title"] = "BURHAN CTR";
			myPointDatas[0][0]["header"] = "BURHAN CTR";
			myPointDatas[0][0]["text"] = "";
			myPointDatas[0][0]["city"] = "";
			myPointDatas[0][0]["region_1"] = "";
			myPointDatas[0][0]["region_2"] = "";
			myPointDatas[0][0]["region_3"] = "";
			myPointDatas[0][0]["imagePath"] = "";
		/* EOF DEFINE POINT DATAS */

		<?php
		// BOF WRITE GOOGLE POINTS
			if(!empty($arrGoogleMapPoints)){
				#$arrSalesmenDatas[$ds["salesmenID"]][$field] = $ds[$field];
				foreach($arrGoogleMapPoints as $thisKey => $thisValue){
					$ds_mapPoints = $thisValue;
					echo ($ds_mapPoints);
				}
			}
		// EOF WRITE GOOGLE POINTS
		?>

		/* BOF CREATE GOOGLE MAP */
			function initialize() {
				if (!document.getElementById(elementId)) {
					alert("Fehler: das Element mit der id "+ elementId+ " konnte nicht auf dieser Webseite gefunden werden!");
					return false;
				}
				else {

				}
			}

			var latlng = new google.maps.LatLng(myPointDatas[default_arrayKey][default_arrayKey]["latitude"], myPointDatas[default_arrayKey][default_arrayKey]["longitude"]);

			var myOptions = {
				zoom: default_zoom,
				center: latlng,
				panControl: true,
				zoomControl: true,
				navigationControl: true,
				mapTypeControl: true,
				scaleControl: true,
				overviewMapControl: true,
				streetViewControl: true,
				mapTypeId: google.maps.MapTypeId.TERRAIN
				/* ROADMAP zeigt die normalen Standard-2D-Kacheln von Google Maps an. */
				/* SATELLITE zeigt Fotokacheln an. */
				/* HYBRID zeigt eine Mischung aus Fotokacheln und eine Kachelebene für markante Merkmale an (Straßen, Ortsnamen). */
				/* TERRAIN zeigt Reliefkacheln für Erhebungen und GewÃ¤sser (Berge, Flüsse usw.) an. */
			};
			var map = new google.maps.Map(document.getElementById(elementId), myOptions);

			// BOF COUNTRY BORDERS
			<?php if(SHOW_COUNTRY_AREAS){ ?>
			var arrMyCountryAreaPoints = new Array();
			<?php
			if(!empty($arrGoogleMapCountryAreas)){
				$countItem = 0;
				foreach($arrGoogleMapCountryAreas as $thisKey => $thisValue) {
			?>
			arrMyCountryAreaPoints[<?php echo $countItem; ?>] = [<?php echo implode(", ", $thisValue); ?>];
			<?php
					$countItem++;
				}
			}
			?>
			function showCountryBorders(map){
				if(arrMyCountryAreaPoints.length > 0){
					var arrMyCountryAreaPolygon = new Array();
					for(i = 0 ; i < arrMyCountryAreaPoints.length ; i++){
						arrMyCountryAreaPolygon[i] = new google.maps.Polygon({
								paths: arrMyCountryAreaPoints[i],
								strokeColor: "#00FF3A",
								strokeOpacity: 0.8,
								strokeWeight: 2,
								fillColor: "transparent",
								fillOpacity: 0.0
								});
						arrMyCountryAreaPolygon[i].setMap(map);
					}
				}
			}
			<?php } ?>
			// EOF COUNTRY BORDERS

			// BOF ZIPCODE AREAS
			<?php if(SHOW_ZIPCODE_AREAS){ ?>

			<?php if(!empty($arrSalesmanNames)){ ?>
				var arrSalesmanNames = new Array();
				<?php foreach($arrSalesmanNames as $thisKey => $thisValue){ ?>
					arrSalesmanNames[<?php echo $thisKey; ?>] = '<?php echo $thisValue; ?>';
				<?php } ?>
			<?php } ?>

			<?php if(!empty($arrSalesmanAreas)){ ?>
				var arrSalesmanAreas = new Array();
				<?php
					foreach($arrSalesmanAreas as $thisSalesmanID => $thisSalesmanAreasValue){
						?>
							arrSalesmanAreas[<?php echo $thisSalesmanID; ?>] = new Array();
						<?php
						foreach($thisSalesmanAreasValue as $thisKey => $thisValue){
				?>
					arrSalesmanAreas[<?php echo $thisSalesmanID; ?>][<?php echo $thisKey; ?>] = '<?php echo $thisValue; ?>';
				<?php
						}
					}
				?>
			<?php } ?>

			var arrMyZipcodeAreaPoints = new Array();
			<?php
			if(!empty($arrSalesmanGoogleMapZipCodeAreas)){
				foreach($arrSalesmanGoogleMapZipCodeAreas as $thisSalesmanID => $thisSalesmanGoogleMapZipCodeAreas) {
					?>
					arrMyZipcodeAreaPoints[<?php echo $thisSalesmanID; ?>] = new Array();
					<?php
					$countItem = 0;
					foreach($thisSalesmanGoogleMapZipCodeAreas as $thisKey => $thisValue) {
			?>
			arrMyZipcodeAreaPoints[<?php echo $thisSalesmanID; ?>][<?php echo $countItem; ?>] = [<?php echo $thisValue; ?>];
			<?php
						$countItem++;
					}
				}
			}
			?>

			function getColors(index){
				var getColor = '';
				<?php if(!empty($arrColorsPLZ)){ ?>
					var arrColors = new Array('<?php echo implode("', '", $arrColorsPLZ) ?>');
					getColor = arrColors[index];
				<?php } ?>
				return getColor;
			}

			function showZipCodeAreas(map){
				var count = 0;
				if(arrMyZipcodeAreaPoints.length > 0){
					var arrMyZipcodeAreaPolygon = new Array();
					for(i in arrMyZipcodeAreaPoints){
						var thisColor = getColors(count);

						arrMyZipcodeAreaPolygon[i] = new Array();
						for(k = 0 ; k < arrMyZipcodeAreaPoints[i].length ; k++){
							var infWindowKey = i+'_'+k;
							arrMyZipcodeAreaPolygon[infWindowKey] = new google.maps.Polygon({
									paths: arrMyZipcodeAreaPoints[i][k],
									strokeColor: thisColor,
									strokeOpacity: 1,
									strokeWeight: 1,
									fillColor: thisColor,
									fillOpacity: 0.5,
									zIndex:1
									});
							arrMyZipcodeAreaPolygon[infWindowKey].setMap(map);

							// BOF GET CENTER OF POLYGON
								var bounds = new google.maps.LatLngBounds();
								for (c = 0; c < arrMyZipcodeAreaPoints[i][k].length; c++) {
									bounds.extend(arrMyZipcodeAreaPoints[i][k][c]);
								}
								var thisPolygonCenter = bounds.getCenter();
							// EOF GET CENTER OF POLYGON

							// BOF SET MARKER IN CENTER OF POLYGON
								var thisMarkerImage;

								thisMarkerImage = "googleMapsIcon_" + count + ".png";
								thisMarkerShadowImage = "googleMapsIconShadow.png";
								thisMarkerSize = new Array(28, 20);
								thisMarkerShadowSize = new Array(28, 20);
								thisZindex = 9;

								var image = new google.maps.MarkerImage(
											"layout/icons/" + thisMarkerImage,
											new google.maps.Size(thisMarkerSize[0], thisMarkerSize[1]),
											new google.maps.Point(0,0),
											new google.maps.Point(0, thisMarkerSize[1])
								);

								var shadow = new google.maps.MarkerImage(
											"layout/icons/" + thisMarkerShadowImage,
											new google.maps.Size(thisMarkerShadowSize[0], thisMarkerShadowSize[1]),
											new google.maps.Point(0, 0),
											new google.maps.Point(0, thisMarkerShadowSize[1])
								);

								var shape = {
									coord: [1, 1, 1, 28, 20, 28, 20 , 1],
									type: "poly"
								};

								var myLatLng = thisPolygonCenter;

								markerPolygonCenter[infWindowKey] = new google.maps.Marker({
									position: myLatLng,
									map: map,
									shadow: shadow,
									icon: image,
									shape: shape,
									title: "PLZ-Gebiet: " + arrSalesmanAreas[i][k] + " | HV/ADM: " + arrSalesmanNames[i],
									zIndex: thisZindex
								});

								markerPolygonCenterShadow[infWindowKey] = new google.maps.Marker({
									position: myLatLng,
									map: map,
									icon: shadow,
									shape: shape,
									title: "PLZ-Gebiet: " + arrSalesmanAreas[i][k] + " | HV/ADM: " + arrSalesmanNames[i],
									zIndex: thisZindex
								});

							// EOF SET MARKER IN CENTER OF POLYGON
							/*
							// --------------------------------------
							// DEFINE INFO WINDOW
								var html		= new Array();
								var imageExists	= false;
								html[infWindowKey] = "";
								html[infWindowKey] += "<div class=\"myGmapInfo\">";

								html[infWindowKey] += "  <p class='headline'>" + "PLZ-Gebiet: " + arrSalesmanAreas[i][k] + "</p>";

								html[infWindowKey] += '<table border="0" width="" cellpadding="0" cellspacing="0">';

								html[infWindowKey] += "  <tr><td><b>PLZ-Gebiet:</b></td><td>" + arrSalesmanAreas[i][k] + "</td></tr>";
								html[infWindowKey] += "  <tr><td><b>ADM:</b></td><td>" + arrSalesmanNames[i] + "</td></tr>";

								html[infWindowKey] += '</table>';

								html[infWindowKey] += "<div class='clear'></div>";
								html[infWindowKey] += "</div>";

								infowindowArea[infWindowKey] = new Array();
								infowindowArea[infWindowKey] = new google.maps.InfoWindow({
									content: html[infWindowKey]
								});
							// END DEFINE INFO WINDOW

							// SHOW INFO WINDOW
								alert(infWindowKey);
								google.maps.event.addListener(markerPolygonCenter[infWindowKey], "click", function() {
									if(activeMarkerPolygonCenter != '') {
										infowindowArea[activeMarkerPolygonCenter].close();
									}
									try{
										infowindowArea[infWindowKey].open(map, markerPolygonCenter[infWindowKey]);
									}
									catch(e){
										alert("error: " + e + " | i: " + i + " | k: " + k);
									}
									activeMarkerPolygonCenter = infWindowKey;
								});

								// createMarkerLinkInGmapSidebar(i, myPointDatas[i][k]["title"], marker[infWindowKey], imageExists);
							// END SHOW INFO WINDOW
							*/
							// --------------------------------------
						}
						count++;
					}
				}
			}
			<?php } ?>
			// EOF ZIPCODE AREAS

			try{
				for (i = 0 ; i < myPointDatas.length ; i++) {
					for (k = 0 ; k < myPointDatas[i].length ; k++) {
						if(myPointDatas[i][k]){
							setGmapMarkers(map, i, k);
						}
					}
				}

				<?php if(SHOW_COUNTRY_AREAS){ ?>
				showCountryBorders(map);
				<?php } ?>

				<?php if(SHOW_ZIPCODE_AREAS){ ?>
				showZipCodeAreas(map);
				<?php } ?>
			}
			catch(e){
				alert("error: " + e + " | i: " + i + " | k: " + k);
			}
		/* EOF CREATE GOOGLE MAP */

	// } catch(err) { alert(err); } finally {}
	</script>
	<!-- GOOGLE MAPS END -->

<script language="javascript" type="text/javascript">
	$(document).ready(function() {
		// $('body').attr('onload', 'initialize();');
	});
</script>
<?php $noMandatorySwitch = true; ?>
<?php $noUserSwitch = true; ?>
<?php require_once('inc/footerHTML.inc.php'); ?>