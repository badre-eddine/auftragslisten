<?php
	require_once('inc/requires.inc.php');

	DEFINE('DATE_STOCK_START', '2013-02-10');

	DEFINE('MAX_STOCK_PRICE_ADDITIONAL_ROWS', 2);

	if(!$arrGetUserRights["editStocks"] && !$arrGetUserRights["displayStocks"]) {
		header('location: ' . PAGE_EXIT_LOCATION);
		exit;
	}

	if($_REQUEST["cancelProductDatas"] != "" || $_REQUEST["cancelProductPrices"] != "" || $_REQUEST["cancelProductsStockReceipt"] != ""){
		$_REQUEST["editID"] = "";
	}

	if(trim($_REQUEST["editID"]) == "") {
		header('location: ' . PAGE_PRODUCTS_STOCK_OVERVIEW);
		exit;
	}

	if($_REQUEST["tab"] == "")  {
		$_REQUEST["tab"] = "tabs-2";
	}

	$jswindowMessage = "";
	$warningMessage = '';
	$errorMessage = '';
	$successMessage = '';
	$infoMessage = '';

	$dbConnection = new DB_Connection();
	$db_open = $dbConnection->db_connect();

	// BOF GET MANDATORIES
		$arrMandatories = getMandatories();
	// EOF GET MANDATORIES

	// BOF GET PRODUCTS VPE
		$arrProductsVPE = getProductsVPE();
	// EOF GET PRODUCTS VPE

	// BOF GET PRODUCTS CATEGORIES
		$arrProductsCategories = getOrderCategories();
	// EOF GET PRODUCTS CATEGORIES

	// BOF GET CUSTOMER GROUPS
		$arrCustomerGroups = getCustomerGroups();
		// BOF ONLY USE CUSTOMER GROUPS HAVING PRICELIST
		foreach($arrCustomerGroups as $thisCustomerGroupKey => $thisCustomerGroupValue){
			if($thisCustomerGroupValue["customerGroupsHasPricelist"] != '1'){
				unset($arrCustomerGroups[$thisCustomerGroupKey]);
			}
		}
	// EOF GET CUSTOMER GROUPS

	// BOF GET CUSTOMER TYPES
		$arrCustomerTypes = getCustomerTypes();
		// BOF ONLY USE CUSTOMER TYPES HAVING PRICELIST
		foreach($arrCustomerTypes as $thisCustomerTypeKey => $thisCustomerTypeValue){
			if($thisCustomerTypeValue["customerTypesHasPricelist"] != '1'){
				unset($arrCustomerTypes[$thisCustomerTypeKey]);
			}
		}
	// EOF GET CUSTOMER GROUPS

	// EOF STORE PRODUCT STOCK RECEIPT
		if($_POST["storeProductsStockReceipt"] != ""){
			
			if((int)$_POST["editProductsStockQuantity"] != 0){
				$sql_storeProductStockReceipt = "
						INSERT INTO `" . TABLE_STOCK_PRODUCT_RECEIPTS . "` (
											
											`productStockItemNumber`,
											`productStockProductID`,
											`productStockProductLevelID`,
											`productStockQuantity`,
											`productStockDate`,
											`productsStockItemPrice`,
											`productsStockItemType`,
											`productsStockItemNotice`
										)
										VALUES (
											
											'" . $_POST["productArtikelNumber"] . "',
											'" . $_POST["editID"] . "',
											'" . $_POST["productLevelID"] . "',
											'" . trim($_POST["editProductsStockQuantity"]) . "',
											'" . formatDate($_POST["editProductsStockReceiptDate"], "store") . "',
											'" . addslashes(preg_replace("/,/", ".", preg_replace("/\./", "", $_POST["editProductsStockItemPrice"]))) . "',
											'" . trim($_POST["editProductsStockItemType"]) . "',
											'" . addslashes($_POST["editProductsStockItemNotice"]) . "'
										)
					";

				$rs_storeProductStockReceipt = $dbConnection->db_query($sql_storeProductStockReceipt);

				if($rs_storeProductStockReceipt){
					$successMessage .= 'Der Wareneingang wurde gespeichert.' . '<br />';
				}
				else {
					$errorMessage .= 'Der Wareneingang konnte nicht gespeichert werden.' . '<br />';
				}
			}
			else {
				$errorMessage .= 'Es wurde keine Menge eingegeben.' . '<br />';
			}
		}
	// BOF STORE PRODUCT STOCK RECEIPT

	// EOF DELETE PRODUCT STOCK RECEIPT
		if($_GET["deleteProductsStockID"] > 0){
			$sql_deleteProductStockReceipt = "
						DELETE FROM `" . TABLE_STOCK_PRODUCT_RECEIPTS . "`
						WHERE 1
							AND `productStockID` = '" . $_GET["deleteProductsStockID"] . "'
				";

			$rs_deleteProductStockReceipt = $dbConnection->db_query($sql_deleteProductStockReceipt);

			if($rs_deleteProductStockReceipt){
				$successMessage .= 'Der Wareneingang wurde entfernt.' . '<br />';
			}
			else {
				$errorMessage .= 'Der Wareneingang konnte nicht entfernt werden.' . '<br />';
			}
		}
	// BOF DELETE PRODUCT STOCK RECEIPT

	// BOF STORE STOCK PRODUCT PRICES
		if($_POST["storeProductPrices"] != ""){
			$arrSqlInsertPrices = array();

			// BOF STORE CUSTOMER GROUP PRICES
				if(!empty($_POST["editCustomerGroupProductQuantity"])){
					foreach($_POST["editCustomerGroupProductQuantity"] as $thisRowKey => $thisQuantityValue){

						if($thisQuantityValue > 0) {

							if(!empty($_POST["editCustomerGroupProductPrice"][$thisRowKey])){
								foreach($_POST["editCustomerGroupProductPrice"][$thisRowKey] as $thisGroupKey => $thisGroupPrice){
									if($thisGroupPrice > 0){
										$arrSqlInsertPrices[] = "
												(
													'" . $_POST["editCustomerGroupPriceID"][$thisRowKey][$thisGroupKey]. "',
													'" . $_POST["editID"]. "',
													'" . $_POST["productID"]. "',
													'" . $_POST["productNumber"]. "',
													0,
													'" . $thisGroupKey . "',
													'" . $thisGroupPrice. "',
													'" . $thisQuantityValue. "',
													'customerGroup'
												)
											";
									}
								}
							}
						}

					}
				}
			// EOF STORE CUSTOMER GROUP PRICES

			// BOF STORE CUSTOMER TYPE PRICES
				if(!empty($_POST["editCustomerTypeProductQuantity"])){
					foreach($_POST["editCustomerTypeProductQuantity"] as $thisRowKey => $thisQuantityValue){

						if($thisQuantityValue > 0){

							if(!empty($_POST["editCustomerTypeProductPrice"][$thisRowKey])){
								foreach($_POST["editCustomerTypeProductPrice"][$thisRowKey] as $thisTypeKey => $thisTypePrice){
									if($thisTypePrice > 0){
										$arrSqlInsertPrices[] = "
												(
													'" . $_POST["editCustomerTypePriceID"][$thisRowKey][$thisTypeKey]. "',
													'" . $_POST["editID"]. "',
													'" . $_POST["productID"]. "',
													'" . $_POST["productNumber"]. "',
													'" . $thisTypeKey . "',
													0,
													'" . $thisTypePrice. "',
													'" . $thisQuantityValue. "',
													'customerType'
												)
											";

									}
								}
							}
						}
					}
				}
			// EOF STORE CUSTOMER TYPE PRICES

			if(!empty($arrSqlInsertPrices)){
				$sql_storeStockPrices = "
						REPLACE
							INTO `" . TABLE_STOCK_PRODUCT_PRICES . "` (
								`stockProductPricesID`,
								`stockProductPricesProductID`,
								`stockProductPricesProductDataID`,
								`stockProductPricesProductNumber`,
								`stockProductPricesCustomerTypeID`,
								`stockProductPricesCustomerGroupID`,
								`stockProductPricesPrice`,
								`stockProductPricesQuantity`,
								`stockProductPricesPriceType`
							) VALUES

							" . implode(", ", $arrSqlInsertPrices) . "
					";

				$rs_storeStockPrices = $dbConnection->db_query($sql_storeStockPrices);

				if($rs_storeStockPrices){
					$successMessage .= 'Die Preise wurde gespeichert.' . '<br />';
				}
				else {
					$errorMessage .= 'Die Preise konnten nicht gespeichert werden.' . '<br />';
				}
			}
		}
	// EOF STORE STOCK PRODUCT PRICES
	
	// BOF GET SELECTED PRODUCT DATA
		if($_REQUEST["editID"] != ""){

			$sql_getProductsData = "
					SELECT
						
						`" . TABLE_STOCK_PRODUCT_CATEGORIES . "`.`stockProductCategoriesID`,
						`" . TABLE_STOCK_PRODUCT_CATEGORIES . "`.`stockProductCategoriesLevelID`,
						`" . TABLE_STOCK_PRODUCT_CATEGORIES . "`.`stockProductCategoriesParentID`,
						`" . TABLE_STOCK_PRODUCT_CATEGORIES . "`.`stockProductCategoriesRelationID`,
						`" . TABLE_STOCK_PRODUCT_CATEGORIES . "`.`stockProductCategoriesName`,
						`" . TABLE_STOCK_PRODUCT_CATEGORIES . "`.`stockProductCategoriesText`,
						`" . TABLE_STOCK_PRODUCT_CATEGORIES . "`.`stockProductCategoriesShortName`,
						`" . TABLE_STOCK_PRODUCT_CATEGORIES . "`.`stockProductCategoriesProductNumber`,
						`" . TABLE_STOCK_PRODUCT_CATEGORIES . "`.`stockProductCategoriesProductNumber_bctr`,
						`" . TABLE_STOCK_PRODUCT_CATEGORIES . "`.`stockProductCategoriesProductNumber_b3`,
						`" . TABLE_STOCK_PRODUCT_CATEGORIES . "`.`stockProductCategoriesSort`,
						`" . TABLE_STOCK_PRODUCT_CATEGORIES . "`.`stockProductCategoriesActive`,
						`" . TABLE_STOCK_PRODUCT_CATEGORIES . "`.`stockProductCategoriesTempID`,
						`" . TABLE_STOCK_PRODUCT_CATEGORIES . "`.`stockProductCategoriesContainsStockProducts`

					FROM `" . TABLE_STOCK_PRODUCT_CATEGORIES . "`
					
					WHERE 1

						AND `" . TABLE_STOCK_PRODUCT_CATEGORIES . "`.`stockProductCategoriesID` = '" . $_REQUEST["editID"] . "'

				";
#dd('sql_getProductsData');
			$rs_getProductsData = $dbConnection->db_query($sql_getProductsData);
			$arrProductsData = array();
			while($ds_getProductsData = mysqli_fetch_assoc($rs_getProductsData)){
				foreach(array_keys($ds_getProductsData) as $field){
					$arrProductsData[$field] = $ds_getProductsData[$field];
				}
			}
		}
	// EOF GET SELECTED PRODUCT DATA

	// BOF GET SELECTED PRODUCT STOCK
		if($_REQUEST["tab"] == "tabs-5") {
			$sql_getProductsStock = "
					SELECT
							`productStockID`,
							`productStockItemNumber`,
							`productStockProductID`,
							`productStockProductLevelID`,
							`productStockQuantity`,
							`productStockDate`,
							`productsStockItemPrice`,
							`productsStockItemType`,
							`productsStockItemNotice`

						FROM `" . TABLE_STOCK_PRODUCT_RECEIPTS . "`

						WHERE 1
							AND `productStockProductID` = '" . $_REQUEST["editID"] . "'

						ORDER BY
							`productStockDate` DESC

				";
			$rs_getProductsStock = $dbConnection->db_query($sql_getProductsStock);
			$arrProductsStock = array();
			while($ds_getProductsStock = mysqli_fetch_assoc($rs_getProductsStock)){
				foreach(array_keys($ds_getProductsStock) as $field){
					$arrProductsStock[$ds_getProductsStock["productStockID"]][$field] = $ds_getProductsStock[$field];
				}
			}
		}
	// EOF GET SELECTED PRODUCT STOCK

	// BOF GET SELECTED PRODUCT TOTAL STOCK
		if($_REQUEST["tab"] == "tabs-4") {
			$sql_getProductsTotalStock = "
					SELECT
							`productStockID`,
							`productStockItemNumber`,
							`productStockProductID`,
							`productStockProductLevelID`,
							SUM(`productStockQuantity`) AS `TOTAL_STOCK`,
							`productStockDate`,
							`productsStockItemPrice`,
							`productsStockItemType`,
							`productsStockItemNotice`

						FROM `" . TABLE_STOCK_PRODUCT_RECEIPTS . "`

						WHERE 1
							AND `productStockProductID` = '" . $_REQUEST["editID"] . "'

						GROUP BY `productStockProductID`

				";
			$rs_getProductsTotalStock = $dbConnection->db_query($sql_getProductsTotalStock);
			$arrProductsTotalStock = array();
			while($ds_getProductsTotalStock = mysqli_fetch_assoc($rs_getProductsTotalStock)){
				$thisProductTotalStock = $ds_getProductsTotalStock["TOTAL_STOCK"];
			}
		}
	// EOF GET SELECTED PRODUCT TOTAL STOCK

	// BOF GET SELECTED PRODUCT PRICES
		if($_REQUEST["tab"] == "tabs-3") {
			$sql_getProductsPrices = "
					SELECT
							`stockProductPricesID`,
							`stockProductPricesProductID`,
							`stockProductPricesProductDataID`,
							`stockProductPricesProductNumber`,
							IF(`stockProductPricesPriceType` = 'customerType', `stockProductPricesCustomerTypeID`,
								IF(`stockProductPricesPriceType` = 'customerGroup', `stockProductPricesCustomerGroupID`,
									'xxx'
								)
							) AS `stockProductPricesCustomerGroupTypeID`,
							`stockProductPricesPrice`,
							`stockProductPricesQuantity`,
							`stockProductPricesPriceType`

						FROM `" . TABLE_STOCK_PRODUCT_PRICES . "`

						WHERE 1
							AND `stockProductPricesProductID` = '" . $_REQUEST["editID"] . "'

						ORDER BY
							`stockProductPricesPriceType` ASC,
							`stockProductPricesQuantity` ASC

				";

			$rs_getProductsPrices = $dbConnection->db_query($sql_getProductsPrices);

			$arrProductsPrices = array();
			while($ds_getProductsPrices = mysqli_fetch_assoc($rs_getProductsPrices)){
				$arrProductsPrices[$ds_getProductsPrices["stockProductPricesPriceType"]][$ds_getProductsPrices["stockProductPricesQuantity"]][$ds_getProductsPrices["stockProductPricesCustomerGroupTypeID"]] = $ds_getProductsPrices;
			}
		}
	// EOF GET SELECTED PRODUCT PRICES
?>
<?php
	require_once('inc/headerHTML.inc.php');
	$thisTitle = "Waren-Bestandseingabe";

	$thisTitle .= ': <span class="headerSelectedEntry">' . htmlentities($arrProductsData["stockProductCategoriesName"]) . ' &bull; ' . htmlentities($arrProductsData["stockProductCategoriesProductNumber"]) . '</span>';

	$headerHTML = preg_replace("/{###TITLE###}/", strip_tags($thisTitle), $headerHTML);
	echo $headerHTML;

	require_once(FILE_MENUE_TOP);
?>
<div id="mainArea">
	<div id="mainContent">
		<?php require_once(FILE_MENUE_SIDEBAR); ?>
		<div id="contentArea">
			<div id="contentAreaElements">
				<h1><?php if(SHOW_PAGE_TITLE_ICONS) { echo '<img src="' . PATH_ICONS_MENUE_TITLES . 'editStock.png" alt="" />'; } ?> <?php echo $thisTitle; ?></h1>

				<div class="contentDisplay">
					<?php displayMessages(); ?>

					<div class="menueActionsArea">
						<a href="<?php echo PAGE_PRODUCTS_STOCK_OVERVIEW; ?>" class="linkButton">Zur &Uuml;bersicht</a>
						<div class="clear"></div>
					</div>

					<div id="tabs" class="ui-tabs ui-widget ui-widget-content ui-corner-all">
						<?php
							$thisPageUrl = $_SERVER["PHP_SELF"] . '?editID=' . $_REQUEST["editID"];
						?>
						<ul class="ui-tabs-nav ui-helper-reset ui-helper-clearfix ui-widget-header ui-corner-all">
							<!--<li class="ui-state-default ui-corner-top"><a href="<?php echo $thisPageUrl; ?>&amp;tab=tabs-1#tabs-1">Produkt-Daten</a></li>-->
							<?php if($_REQUEST["editID"] != 'NEW' && $_REQUEST["editID"] != ''){ ?>
							<li class="ui-state-default ui-corner-top"><a href="<?php echo $thisPageUrl; ?>&amp;tab=tabs-2#tabs-2">Neuer Wareneingang</a></li>
							<li class="ui-state-default ui-corner-top"><a href="<?php echo $thisPageUrl; ?>&amp;tab=tabs-5#tabs-5">Wareneing&auml;nge</a></li>
							<li class="ui-state-default ui-corner-top"><a href="<?php echo $thisPageUrl; ?>&amp;tab=tabs-3#tabs-3">Preise</a></li>
							<li class="ui-state-default ui-corner-top"><a href="<?php echo $thisPageUrl; ?>&amp;tab=tabs-4#tabs-4">Verbrauch</a></li>
							<?php } ?>
						</ul>

						<div class="adminEditArea">
							<?php if($_REQUEST["tab"] == "xxxxxxxtabs-1") { ?>
							<div id="tabs-1">
								<?php if(!empty($arrProductsData) || $_REQUEST["editID"] == 'NEW'){ ?>

								<form name="formEditProductsData" method="post" action="<?php echo $_SERVER["PHP_SELF"]; ?>#tabs-1" enctype="multipart/form-data" >
									<input type="hidden" name="tab" value="<?php echo $_REQUEST["tab"]; ?>" />
									<input type="hidden" name="editID" value="<?php echo $arrProductsData["stockProductCategoriesID"]; ?>" />									
									<input type="hidden" name="productLevelID" value="<?php echo $arrProductsData["stockProductCategoriesLevelID"]; ?>" />
									<input type="hidden" name="productArtikelNumber" value="<?php echo $arrProductsData["stockProductCategoriesProductNumber"]; ?>" />
									
									<fieldset>
										<legend>Produktdaten</legend>
										<?php
											dd('arrProductsData');
										?>

										<table border="0" width="100%" cellspacing="0" cellpadding="0">
											<?php if($_REQUEST["editID"] != 'NEW'){ ?>
											<?php } ?>
											<tr>
												<td style="width:200px;"><b>Artikel:</b></td>
												<td>
													<b><?php echo $arrProductsData["stockProductCategoriesName"]; ?> / <?php echo $arrProductsData["stockProductCategoriesProductNumber"]; ?></b>													
													<?php
														if($_REQUEST["editID"] != "NEW") {
															if($arrProductsData["stockProductCategoriesActive"] == '1'){
																echo ' <span style="color:#009900; font-weight:bold;">[Das Produkt ist aktiv]</span>';
															}
															else {
																echo ' <span style="color:#CC0000; font-weight:bold;">[Das Produkt ist nicht aktiv]</span>';
															}
														}
													?>
												</td>
											</tr>
											<!--
											<tr>
												<td style="width:200px;"><b>Artikel-Nr:</b></td>
												<td><?php echo $arrProductsData["stockProductCategoriesProductNumber"]; ?></td>
											</tr>
											<tr>
												<td style="width:200px;"><b>Artikel-Name:</b></td>
												<td><?php echo $arrProductsData["stockProductCategoriesName"]; ?></td>
											</tr>
											<tr>
												<td style="width:200px;"><b>Bezeichnung-Kurz:</b></td>
												<td>
													<input type="text" name="editProductDataProductShortName" id="editProductDataProductShortName" class="inputField_510" value="<?php echo $arrProductsData["productDataProductShortName"]; ?>" />
												</td>
											</tr>
											--
											<!--
											<tr>
												<td style="width:200px;"><b>Verpackungseinheit:</b></td>
												<td>
													<select name="editProductDataProductVpeID" id="editProductDataProductVpeID" class="inputSelect_510" >
														<option value=""></option>
														<?php
															if(!empty($arrProductsVPE)){
																foreach($arrProductsVPE as $thisProductsVpeKey => $thisProductsVpeValue){
																	$selected = '';
																	if($thisProductsVpeKey == $arrProductsData["productDataProductVpeID"]){
																		$selected = ' selected="selected" ';
																	}
																	echo '<option value="' . $thisProductsVpeKey . '" ' . $selected . ' >' . $thisProductsVpeValue["productVpeName"] . '</option>';
																}
															}
														?>
													</select>
												</td>
											</tr>
											-->
											
											<tr>
												<td style="width:200px;"><b>Herstellungspreis:</b></td>
												<td>
													<input type="text" name="editProductDataProductionPrice" class="inputField_70" value="<?php echo preg_replace("/\./", ",", $arrProductsData["productDataProductionPrice"]); ?>" /> &euro;
												</td>
											</tr>

											<tr>
												<td style="width:200px;"><b>Einkaufspreis:</b></td>
												<td>
													<input type="text" name="editProductDataEkPrice" class="inputField_70" value="<?php echo preg_replace("/\./", ",", $arrProductsData["productDataEkPrice"]); ?>" /> &euro;
												</td>
											</tr>

										</table>
									</fieldset>

									<!--
									<fieldset>
										<legend>Zuordnungen / Kategorien</legend>

										<table border="0" width="100%" cellspacing="0" cellpadding="0">
											<tr>
												<td style="width:200px;"><b>Haupt-Kategorie:</b></td>
												<td>
													
												</td>
											</tr>
										</table>
									</fieldset>
									-->
								</form>
								<?php
									}
									else {
										$infoMessage = ' Es wurden keine Daten gefunden!' . '<br />';
									}
								?>
								<?php displayMessages(); ?>
							</div>
							<?php } ?>

							<?php if(!empty($arrProductsData)){ ?>
								<?php if($_REQUEST["tab"] == "tabs-2") { ?>
								<div id="tabs-2">
									<form name="formEditProductsStockReceipt" method="post" action="<?php echo $_SERVER["PHP_SELF"]; ?>#tabs-2">
										<input type="hidden" name="tab" value="<?php echo $_REQUEST["tab"]; ?>" />
										<input type="hidden" name="editID" value="<?php echo $arrProductsData["stockProductCategoriesID"]; ?>" />										
										<input type="hidden" name="productLevelID" value="<?php echo $arrProductsData["stockProductCategoriesLevelID"]; ?>" />
										<input type="hidden" name="productArtikelNumber" value="<?php echo $arrProductsData["stockProductCategoriesProductNumber"]; ?>" />
										
										<fieldset>
											<legend>Artikel</legend>
											<table border="0" width="100%" cellspacing="0" cellpadding="0">
												<tr>
													<td style="width:200px;"><b>Artikel-Bezeichnung:</b></td>
													<td><?php echo $arrProductsData["stockProductCategoriesName"]; ?></td>
												</tr>
												<tr>
													<td style="width:200px;"><b>Artikel-Nr.:</b></td>
													<td><?php echo $arrProductsData["stockProductCategoriesProductNumber"]; ?></td>
												</tr>
											</table>
										</fieldset>

										<fieldset>
											<legend>Neuer Wareneingang</legend>
											<table border="0" width="100%" cellspacing="0" cellpadding="0">
												<tr>
													<td style="width:200px;"><b>Menge:</b></td>
													<td>
														<input type="text" name="editProductsStockQuantity" class="inputField_70" value="" />
													</td>
												</tr>
												<tr>
													<td style="width:200px;"><b>St&uuml;ckpreis:</b></td>
													<td>
														<input type="text" name="editProductsStockItemPrice" class="inputField_70" value="" />
													</td>
												</tr>
												<tr>
													<td style="width:200px;"><b>Art:</b></td>
													<td>
														<div class="inputArea1">
															<span class="checkBoxElement1" >
																<input type="radio" name="editProductsStockItemType" id="editProductsStockItemType_0" value="0" /> <label for="editProductsStockItemType_0">unbedruckt</label>
															</span>
															<span class="checkBoxElementSeparator">&nbsp;</span>
															<span class="checkBoxElement1" >
																<input type="radio" name="editProductsStockItemType" id="editProductsStockItemType_1" value="0" /> <label for="editProductsStockItemType_1">bedruckt</label>
															</span>
														</div>
													</td>
												</tr>
												<tr>
													<td style="width:200px;"><b>Eingangsdatum:</b></td>
													<td>
														<input type="text" name="editProductsStockReceiptDate" id="editProductsStockReceiptDate" class="inputField_70" value="<?php echo date("d.m.Y"); ?>" readonly="readonly"/>
													</td>
												</tr>

												<tr>
													<td style="width:200px;"><b>Notiz:</b></td>
													<td>
														<textarea name="editProductsStockItemNotice" id="editProductsStockItemNotice" class="inputTextarea_510x60"></textarea>
													</td>
												</tr>

											</table>
										</fieldset>
										<div class="actionButtonsArea">
											<input type="submit" class="inputButton1 inputButtonGreen" name="storeProductsStockReceipt" value="Speichern" />
											<input type="submit" class="inputButton1 inputButtonOrange" name="cancelProductsStockReceipt" value="Abbrechen" />
										</div>
									</form>
									<?php displayMessages(); ?>
								</div>
								<?php } ?>

								<?php if($_REQUEST["tab"] == "tabs-5") { ?>
								<div id="tabs-5">
									<fieldset>
										<legend>Bisherige Wareneing&auml;nge</legend>
										<?php
											if(!empty($arrProductsStock)){
												echo '<table border="0" width="100%" cellspacing="0" cellpadding="0" class="displayOrders">';
												
												$countTotalReceipts = 0;

												echo '<tr>';
												echo '<th style="width:30px">#</th>';
												echo '<th style="width:70px">Datum</th>';
												echo '<th style="width:70px">Menge</th>';
												echo '<th style="width:200px">Art</th>';
												echo '<th style="width:70px">St&uuml;ckpreis</th>';
												echo '<th>Notiz</th>';
												echo '<th style="width:100px">Aktion</th>';
												echo '</tr>';
												$countRow = 0;
												foreach($arrProductsStock as $thisProductsStockKey => $thisProductsStockValue){
													if($countRow%2 == 0){ $rowClass = 'row0'; }
													else { $rowClass = 'row1'; }

													echo '<tr class="' . $rowClass . '">';

													echo '<td><b>' . ($countRow  + 1) . '.</b></td>';

													echo '<td>';
													echo formatDate($thisProductsStockValue["productStockDate"], "display");
													echo '</td>';

													echo '<td style="text-align:right;">';
													echo number_format($thisProductsStockValue["productStockQuantity"], 0, ",", ".");
													$countTotalReceipts +=$thisProductsStockValue["productStockQuantity"];
													echo '</td>';

													echo '<td>';
													$thisProductsStockItemType = 'unbedruckt';
													if($thisProductsStockValue["productsStockItemType"] == '1'){
														$thisProductsStockItemType = 'bedruckt';
													}
													echo $thisProductsStockItemType;
													echo '</td>';

													echo '<td>';
													echo preg_replace("/\./", ",", $thisProductsStockValue["productsStockItemPrice"]);
													echo '</td>';

													echo '<td>';
													echo htmlentities(utf8_decode($thisProductsStockValue["productsStockItemNotice"]));
													echo '</td>';

													echo '<td>';

													echo '<span class="toolItem">';
													echo '<a href="' . $_SERVER["PHP_SELF"]. '?editID=' . $_REQUEST["editID"]. '&deleteProductsStockID=' . $thisProductsStockKey . '#tabs-2" onclick="return showWarning(\'Wollen Sie diesen Eintrag wirklich entfernen?\');" ><img src="layout/icons/iconDelete.png" width="14" height="14" alt="Entfernen" title="Diesen Eintrag entfernen" /></a>';
													echo '</span>';

													echo '</td>';

													echo '</tr>';

													$countRow ++;
												}
												
												echo '<tr class="row2" style="font-weight:bold;">';
												
												echo '<td colspan="2">Insg.</td>';
												
												echo '<td style="text-align:right;">';
												echo number_format($countTotalReceipts, 0, ",", ".");
												echo '</td>';
												
												echo '<td colspan="45" style="text-align:right;">';
												echo '</td>';
												
												echo '</tr>';

												echo '</table>';
											}
											else {
												echo '<p class="infoArea">Es liegen keine Wareneing&auml;nge vor!</p>';
											}
										?>
									</fieldset>
								</div>
								<?php } ?>

								<?php if($_REQUEST["tab"] == "tabs-3") { ?>
								<div id="tabs-3">

									<form name="formEditProductsPrice" method="post" action="<?php echo $_SERVER["PHP_SELF"]; ?>#tabs-3" enctype="multipart/form-data" >
										<input type="hidden" name="tab" value="<?php echo $_REQUEST["tab"]; ?>" />
										<input type="hidden" name="editID" value="<?php echo $arrProductsData["stockProductCategoriesID"]; ?>" />										
										<input type="hidden" name="productLevelID" value="<?php echo $arrProductsData["stockProductCategoriesLevelID"]; ?>" />
										<input type="hidden" name="productArtikelNumber" value="<?php echo $arrProductsData["stockProductCategoriesProductNumber"]; ?>" />

										<fieldset>
											<legend>Preise f&uuml;r Kunden-Gruppen</legend>

											<table width="100%" cellspacing="0" cellpadding="0" class="displayOrders">
												<tr>
													<th style="width:100px;">Staffelung</th>
													<th class="spacer"></th>
													<?php
														if(!empty($arrCustomerGroups)){
															foreach($arrCustomerGroups as $thisCustomerGroupsKey => $thisCustomerGroupsValue){
																?>
																<th><?php echo $thisCustomerGroupsValue["customerGroupsName"]; ?></th>
																<?php
															}
														}
													?>
												</tr>

												<?php
													$countCustomerGroupsRow = 0;

													// BOF DISPLAY STORED CUSTOMER GROUP PRICES
														if(!empty($arrProductsPrices["customerGroup"])){
															$countRow = 0;
															foreach($arrProductsPrices["customerGroup"] as $thisCustomerGroupPricesQuantity => $thisCustomerGroupPricesValue) {
																if($countRow%2 == 0){ $rowClass = 'row0'; }
																else { $rowClass = 'row1'; }
																?>
																	<tr class="<?php echo $rowClass; ?>">
																		<td><input type="text" name="editCustomerGroupProductQuantity[<?php echo $countCustomerGroupsRow; ?>]" class="inputField_70" value="<?php echo $thisCustomerGroupPricesQuantity; ?>" /></td>
																		<td class="spacer"></td>
																		<?php
																			if(!empty($arrCustomerGroups)){
																				foreach($arrCustomerGroups as $thisCustomerGroupsKey => $thisCustomerGroupsValue){
																					?>
																					<td>
																						<input type="hidden" name="editCustomerGroupPriceID[<?php echo $countCustomerGroupsRow; ?>][<?php echo $thisCustomerGroupsKey; ?>]" class="inputField_70" value="<?php echo $thisCustomerGroupPricesValue[$thisCustomerGroupsKey]["stockProductPricesID"]; ?>" />
																						<input type="text" name="editCustomerGroupProductPrice[<?php echo $countCustomerGroupsRow; ?>][<?php echo $thisCustomerGroupsKey; ?>]" class="inputField_70" value="<?php echo preg_replace("/\./", ",", $thisCustomerGroupPricesValue[$thisCustomerGroupsKey]["stockProductPricesPrice"]); ?>" />  &euro;
																					</td>
																					<?php
																				}
																			}
																		?>
																	</tr>
																<?php
																	$countCustomerGroupsRow++;
																	$countRow++;
															}
															?>
															<tr>
																<td colspan="<?php echo(count(array_keys($arrCustomerGroups)) + 2); ?>"><br /><b>Staffelungen hinzuf&uuml;gen:</b></td>
															</tr>
															<?php
														}
													// EOF DISPLAY STORED CUSTOMER GROUP PRICES

													// BOF DISPLAY ADD CUSTOMER GROUP PRICES
														for($i = 0 ; $i < MAX_STOCK_PRICE_ADDITIONAL_ROWS ; $i++){
															?>
																<tr>
																	<td><input type="text" name="editCustomerGroupProductQuantity[<?php echo $countCustomerGroupsRow; ?>]" class="inputField_70" value="" /></td>
																	<td class="spacer"></td>
																	<?php
																		if(!empty($arrCustomerGroups)){
																			foreach($arrCustomerGroups as $thisCustomerGroupsKey => $thisCustomerGroupsValue){
																				?>
																				<td>
																					<input type="hidden" name="editCustomerGroupPriceID[<?php echo $countCustomerGroupsRow; ?>][<?php echo $thisCustomerGroupsKey; ?>]" class="inputField_70" value="%" />
																					<input type="text" name="editCustomerGroupProductPrice[<?php echo $countCustomerGroupsRow; ?>][<?php echo $thisCustomerGroupsKey; ?>]" class="inputField_70" value="" />  &euro;
																				</td>
																				<?php
																			}
																		}
																	?>
																</tr>
															<?php
															$countCustomerGroupsRow++;
														}
													// EOF DISPLAY ADD CUSTOMER GROUP PRICES

												?>
											</table>
										</fieldset>

										<hr />

										<fieldset>
											<legend>Preise f&uuml;r Kunden-Typen</legend>

											<table width="100%" cellspacing="0" cellpadding="0" class="displayOrders">
												<tr>
													<th style="width:100px;">Staffelung</th>
													<th class="spacer"></th>

													<?php
														if(!empty($arrCustomerTypes)){
															foreach($arrCustomerTypes as $thisCustomerTypesKey => $thisCustomerTypesValue){
																if($thisCustomerTypesValue["customerTypesHasPricelist"] == '1') {
																	?>
																	<th><?php echo $thisCustomerTypesValue["customerTypesName"]; ?></th>
																	<?php
																}
															}
														}
													?>
												</tr>

												<?php
													$countCustomerTypesRow = 0;

													// BOF DISPLAY STORED CUSTOMER TYPE PRICES
														if(!empty($arrProductsPrices["customerType"])){
															$countRow = 0;
															foreach($arrProductsPrices["customerType"] as $thisCustomerTypePricesQuantity => $thisCustomerTypePricesValue) {
																if($countRow%2 == 0){ $rowClass = 'row0'; }
																else { $rowClass = 'row1'; }
																?>
																	<tr class="<?php echo $rowClass; ?>">
																		<td><input type="text" name="editCustomerTypeProductQuantity[<?php echo $countCustomerTypesRow; ?>]" class="inputField_70" value="<?php echo $thisCustomerTypePricesQuantity; ?>" /></td>
																		<td class="spacer"></td>
																		<?php
																			if(!empty($arrCustomerTypes)){
																				foreach($arrCustomerTypes as $thisCustomerTypesKey => $thisCustomerTypesValue){
																					?>
																					<td>
																						<input type="hidden" name="editCustomerTypePriceID[<?php echo $countCustomerTypesRow; ?>][<?php echo $thisCustomerTypesKey; ?>]" class="inputField_70" value="<?php echo $thisCustomerTypePricesValue[$thisCustomerTypesKey]["stockProductPricesID"]; ?>" />
																						<input type="text" name="editCustomerTypeProductPrice[<?php echo $countCustomerTypesRow; ?>][<?php echo $thisCustomerTypesKey; ?>]" class="inputField_70" value="<?php echo preg_replace("/\./", ",", $thisCustomerTypePricesValue[$thisCustomerTypesKey]["stockProductPricesPrice"]); ?>" />  &euro;
																					</td>
																					<?php
																				}
																			}
																		?>
																	</tr>
																<?php
																	$countCustomerTypesRow++;
																	$countRow++;
															}
															?>
															<tr>
																<td colspan="<?php echo(count(array_keys($arrCustomerTypes)) + 2); ?>"><br /><b>Staffelungen hinzuf&uuml;gen:</b></td>
															</tr>
															<?php
														}
													// EOF DISPLAY STORED CUSTOMER TYPE PRICES

													// BOF DISPLAY ADD CUSTOMER TYPE PRICES
														for($i = 0 ; $i < MAX_STOCK_PRICE_ADDITIONAL_ROWS ; $i++){
															?>
																<tr>
																	<td><input type="text" name="editCustomerTypeProductQuantity[<?php echo $countCustomerTypesRow; ?>]" class="inputField_70" value="" /></td>
																	<td class="spacer"></td>
																	<?php
																		if(!empty($arrCustomerTypes)){
																			foreach($arrCustomerTypes as $thisCustomerTypesKey => $thisCustomerTypesValue){
																				?>
																				<td>
																					<input type="hidden" name="editCustomerTypePriceID[<?php echo $countCustomerTypesRow; ?>][<?php echo $thisCustomerTypesKey; ?>]" class="inputField_70" value="%" />
																					<input type="text" name="editCustomerTypeProductPrice[<?php echo $countCustomerTypesRow; ?>][<?php echo $thisCustomerTypesKey; ?>]" class="inputField_70" value="" />  &euro;
																				</td>
																				<?php
																			}
																		}
																	?>
																</tr>
															<?php
															$countCustomerTypesRow++;
														}
													// EOF DISPLAY ADD CUSTOMER TYPE PRICES
												?>
											</table>
										</fieldset>

										<div class="actionButtonsArea">
											<input type="submit" class="inputButton1 inputButtonGreen" name="storeProductPrices" value="Speichern" />
											<input type="submit" class="inputButton1 inputButtonOrange" name="cancelProductPrices" value="Abbrechen" />
										</div>
									</form>
								</div>
								<?php } ?>

								<?php if($_REQUEST["tab"] == "tabs-4") { ?>
								<div id="tabs-4">

									<fieldset>
										<legend>Verbrauch insgesamt</legend>

										<?php
											// BOF GET USAGE											
												$sql_getProductsUsage = "
													SELECT

														`tempTable`.`ordersArtikelnummer`,
														SUM(`tempTable`.`ordersArtikelMenge`) AS `sumPerYear`,
														`tempTable`.`productionInterval`,

														IF(`tempTable`.`productionInterval` = DATE_FORMAT(NOW(), '%Y'), DATE_FORMAT(NOW(), '%m'), 12) AS `intervalDevideMonth`,
														ROUND((SUM(`tempTable`.`ordersArtikelMenge`) / IF(`tempTable`.`productionInterval` = DATE_FORMAT(NOW(), '%Y'), DATE_FORMAT(NOW(), '%m'), 12)), 0) AS `sumPerMonth`,

														IF(`tempTable`.`productionInterval` = DATE_FORMAT(NOW(), '%Y'), DATE_FORMAT(NOW(), '%u'), DATE_FORMAT(CONCAT(`tempTable`.`productionInterval`, '-12-31'), '%u')) AS `intervalDevideWeek`,
														ROUND((SUM(`tempTable`.`ordersArtikelMenge`) / IF(`tempTable`.`productionInterval` = DATE_FORMAT(NOW(), '%Y'), DATE_FORMAT(NOW(), '%u'), DATE_FORMAT(CONCAT(`tempTable`.`productionInterval`, '-12-31'), '%u'))), 0) AS `sumPerWeek`,

														`tempTable`.`printedProduct`,
														IF(`tempTable`.`printedProduct` = '1', 'bedruckt', 'unbedruckt') AS `printedProduct2`,
														`tempTable`.`productionLocation`

														FROM (
															SELECT
																`" . TABLE_ORDERS . "`.`ordersArtikelnummer`,
																`" . TABLE_ORDERS . "`.`ordersArtikelMenge`,
																`" . TABLE_ORDERS . "`.`ordersBestellDatum`,
																DATE_FORMAT(`" . TABLE_ORDERS . "`.`ordersBestellDatum`, '%Y') AS `productionInterval`,
																IF(`" . TABLE_ORDERS . "`.`ordersArtikelPrintColorsCount` > 0, '1', '0') AS `printedProduct`,

																IF(`" . TABLE_PRODUCTIONS_TRANSFER . "`.`ordersProductionsTransferOrdersID` IS NULL, 'DE', 'TR') AS `productionLocation`

															FROM `" . TABLE_STOCK_PRODUCT_CATEGORIES . "`

															LEFT JOIN `" . TABLE_ORDERS . "`
															ON(
																`" . TABLE_STOCK_PRODUCT_CATEGORIES . "`.`stockProductCategoriesProductNumber` = `" . TABLE_ORDERS . "`.`ordersArtikelnummer`
																OR
																`" . TABLE_STOCK_PRODUCT_CATEGORIES . "`.`stockProductCategoriesProductNumber_bctr` = `" . TABLE_ORDERS . "`.`ordersArtikelnummer`
																OR
																`" . TABLE_STOCK_PRODUCT_CATEGORIES . "`.`stockProductCategoriesProductNumber_b3` = `" . TABLE_ORDERS . "`.`ordersArtikelnummer`
															)

															LEFT JOIN `" . TABLE_PRODUCTIONS_TRANSFER . "`
															ON(`" . TABLE_ORDERS . "`.`ordersID` = `" . TABLE_PRODUCTIONS_TRANSFER . "`.`ordersProductionsTransferOrdersID`)

															WHERE 1
																AND `" . TABLE_ORDERS . "`.`ordersArtikelnummer` = '" . $arrProductsData["stockProductCategoriesProductNumber"] . "'
																AND (
																	`" . TABLE_ORDERS . "`.`ordersStatus` = '3'
																	OR `" . TABLE_ORDERS . "`.`ordersStatus` = '4'
																	OR `" . TABLE_ORDERS . "`.`ordersStatus` = '7'
																)
																/* AND DATE_FORMAT(`" . TABLE_ORDERS . "`.`ordersBestellDatum`, '%Y') > 2013 */

														) AS `tempTable`

														GROUP BY CONCAT(
															`tempTable`.`productionInterval`,
															`tempTable`.`ordersArtikelnummer`,
															`tempTable`.`printedProduct`,
															`tempTable`.`productionLocation`
														)

														ORDER BY
															`tempTable`.`productionLocation`,
															`tempTable`.`productionInterval` DESC
													";

												$rs_getProductsUsage = $dbConnection->db_query($sql_getProductsUsage);

												$countItems =  $dbConnection->db_getMysqlNumRows($rs_getProductsUsage);

												$arrProductsUsageData = array();
												$arrProductsPrintType = array();
												$arrProductsProductionLocation = array();

												while($ds_getProductsUsage = mysqli_fetch_assoc($rs_getProductsUsage)){
													$arrProductsUsageData[$ds_getProductsUsage["productionInterval"]][$ds_getProductsUsage["productionLocation"]][$ds_getProductsUsage["printedProduct"]] = $ds_getProductsUsage;
													$arrProductsPrintType[$ds_getProductsUsage["printedProduct"]] = $ds_getProductsUsage["printedProduct2"];
													$arrProductsProductionLocation[] = $ds_getProductsUsage["productionLocation"];
												}
												krsort($arrProductsUsageData);
												if(!empty($arrProductsPrintType)){
													$arrProductsPrintType = array_unique($arrProductsPrintType);
													krsort($arrProductsPrintType);
												}

												if(!empty($arrProductsProductionLocation)){
													$arrProductsProductionLocation = array_unique($arrProductsProductionLocation);
												}

												if(!empty($arrProductsUsageData)){
													$countRow = 0;

													echo '<table cellpadding="0" cellspacing="0" width="100%" class="displayOrders">';

													$arrTotalSums = array();
													$arrQuantitySumsColumn = array();

													foreach($arrProductsUsageData as $thisYear => $thisYearProductsUsageData){
														if($countRow == 0){
															echo '<tr>';
															echo '<th rowspan="3">#</th>';
															echo '<th rowspan="3">Jahr</th>';

															foreach($arrProductsProductionLocation as $thisProductionLocation){
																echo '<th colspan="' . ((count($arrProductsPrintType) + 1) * 2) . '">';
																echo $thisProductionLocation;
																echo ' <img src="layout/flags/flag_' . $thisProductionLocation . '.gif" width="14" height="10" alt="' . $thisProductionLocation . '" title="" />';
																echo '</th>';
															}

															echo '<th colspan="2">Insgesamt [' . implode(" &amp; ", $arrProductsProductionLocation) . ']</th>';
															echo '</tr>';

															echo '<tr>';

															foreach($arrProductsProductionLocation as $thisProductionLocation){
																foreach($arrProductsPrintType as $thisProductsPrintTypeKey => $thisProductsPrintTypeValue) {
																	echo '<th colspan="2">' . $thisProductsPrintTypeValue . '</th>';
																}
																echo '<th colspan="2">Summe</th>';
															}

															#foreach($arrProductsPrintType as $thisProductsPrintTypeKey => $thisProductsPrintTypeValue) {
																#echo '<th colspan="2">' . $thisProductsPrintTypeValue . '</th>';
															#}
															echo '<th colspan="2">Summe</th>';
															echo '</tr>';

															echo '<tr>';

															foreach($arrProductsProductionLocation as $thisProductionLocation){
																for($i = 0 ; $i < (count($arrProductsPrintType) + 1) ; $i++){
																	echo '<th>pro Jahr</th>';
																	echo '<th>&Oslash; pro Woche</th>';
																}
															}

															#for($i = 0 ; $i < (count($arrProductsPrintType) + 1) ; $i++){
																echo '<th>pro Jahr</th>';
																echo '<th>&Oslash; pro Woche</th>';
															#}

															echo '</tr>';
														}

														if($countRow%2 == 0){ $rowClass = 'row0'; }
														else { $rowClass = 'row1'; }

														$rowStyle = '';

														echo '<tr class="' . $rowClass . '">';

														echo '<td>';
														echo '<b>' . ($countRow + 1) . '.</b>';
														echo '</td>';

														echo '<td>';
														echo '<b>' . $thisYear . '</b>';
														echo '</td>';

														$arrQuantitySumsRow = array();

														// BOF USAGE PRO LOCATION
															foreach($arrProductsProductionLocation as $thisProductionLocation){
																foreach($arrProductsPrintType as $thisProductsPrintTypeKey => $thisProductsPrintTypeValue) {
																	echo '<td style="text-align:right;">';
																	echo number_format($thisYearProductsUsageData[$thisProductionLocation][$thisProductsPrintTypeKey]["sumPerYear"], 0, '', '.');
																	echo '</td>';

																	echo '<td style="text-align:right;">';
																	echo number_format($thisYearProductsUsageData[$thisProductionLocation][$thisProductsPrintTypeKey]["sumPerWeek"], 0, '', '.');
																	echo '</td>';

																	$arrTotalSums[$thisProductionLocation][$thisProductsPrintTypeKey]["perYear"] += $thisYearProductsUsageData[$thisProductionLocation][$thisProductsPrintTypeKey]["sumPerYear"];
																	$arrTotalSums[$thisProductionLocation][$thisProductsPrintTypeKey]["perYear"] += $thisYearProductsUsageData[$thisProductionLocation][$thisProductsPrintTypeKey]["sumPerWeek"];

																	$arrQuantitySumsRow["LOCATION_" . $thisProductionLocation]["perYear"] += $thisYearProductsUsageData[$thisProductionLocation][$thisProductsPrintTypeKey]["sumPerYear"];
																	$arrQuantitySumsRow["LOCATION_" . $thisProductionLocation]["perWeek"] += $thisYearProductsUsageData[$thisProductionLocation][$thisProductsPrintTypeKey]["sumPerWeek"];

																	$arrQuantitySumsColumn[$thisProductionLocation][$thisProductsPrintTypeKey]["perYear"] += $thisYearProductsUsageData[$thisProductionLocation][$thisProductsPrintTypeKey]["sumPerYear"];
																	$arrQuantitySumsColumn[$thisProductionLocation][$thisProductsPrintTypeKey]["perWeek"] += $thisYearProductsUsageData[$thisProductionLocation][$thisProductsPrintTypeKey]["sumPerWeek"];
																}

																echo '<td style="text-align:right;">';
																echo number_format($arrQuantitySumsRow["LOCATION_" . $thisProductionLocation]["perYear"], 0, '', '.');
																echo '</td>';

																echo '<td style="text-align:right;">';
																echo number_format($arrQuantitySumsRow["LOCATION_" . $thisProductionLocation]["perWeek"], 0, '', '.');
																echo '</td>';
															}

														// EOF USAGE PRO LOCATION

														// BOF USAGE ALL LOCATIONS
															foreach($arrProductsProductionLocation as $thisProductionLocation){
																$arrQuantitySumsRow["LOCATION_ALL"]["perYear"] += $arrQuantitySumsRow["LOCATION_" . $thisProductionLocation]["perYear"];
																$arrQuantitySumsRow["LOCATION_ALL"]["perWeek"] += $arrQuantitySumsRow["LOCATION_" . $thisProductionLocation]["perWeek"];
															}

															echo '<td style="text-align:right;">';
															echo number_format($arrQuantitySumsRow["LOCATION_ALL"]["perYear"], 0, '', '.');
															echo '</td>';

															echo '<td style="text-align:right;">';
															echo number_format($arrQuantitySumsRow["LOCATION_ALL"]["perWeek"], 0, '', '.');
															echo '</td>';

															echo '</tr>';
														// EOF USAGE ALL LOCATIONS

														$countRow++;
													}

													echo '<tr>';
													echo '<td colspan="' . (4 + ((count($arrProductsProductionLocation)) * (count($arrProductsPrintType) + 1)) * 2) . '"><hr /></td>';
													echo '</tr>';

													echo '<tr class="row2" style="font-weight:bold;">';

													echo '<td colspan="2">Insgesamt</td>';


													foreach($arrProductsProductionLocation as $thisProductionLocation){
														$arrTotalSums["LOCATION_" . $thisProductionLocation]["perYear"] = 0;
														$arrTotalSums["LOCATION_" . $thisProductionLocation]["perWeek"] = 0;
														foreach($arrProductsPrintType as $thisProductsPrintTypeKey => $thisProductsPrintTypeValue) {
															echo '<td style="text-align:right;">';
															echo number_format($arrQuantitySumsColumn[$thisProductionLocation][$thisProductsPrintTypeKey]["perYear"], 0, '', '.');
															echo '</td>';

															echo '<td style="text-align:right;">';
															echo number_format($arrQuantitySumsColumn[$thisProductionLocation][$thisProductsPrintTypeKey]["perWeek"], 0, '', '.');
															echo '</td>';

															$arrTotalSums["perYear"] += $arrQuantitySumsColumn[$thisProductionLocation][$thisProductsPrintTypeKey]["perYear"];
															$arrTotalSums["perWeek"] += $arrQuantitySumsColumn[$thisProductionLocation][$thisProductsPrintTypeKey]["perWeek"];

															$arrTotalSums["LOCATION_" . $thisProductionLocation]["perYear"] += $arrQuantitySumsColumn[$thisProductionLocation][$thisProductsPrintTypeKey]["perYear"];
															$arrTotalSums["LOCATION_" . $thisProductionLocation]["perWeek"] += $arrQuantitySumsColumn[$thisProductionLocation][$thisProductsPrintTypeKey]["perWeek"];
														}
														echo '<td style="text-align:right;">';
														echo number_format($arrTotalSums["LOCATION_" . $thisProductionLocation]["perYear"], 0, '', '.');
														echo '</td>';

														echo '<td style="text-align:right;">';
														echo number_format($arrTotalSums["LOCATION_" . $thisProductionLocation]["perWeek"], 0, '', '.');
														echo '</td>';
													}

													echo '<td style="text-align:right;">';
													echo number_format($arrTotalSums["perYear"], 0, '', '.');
													echo '</td>';

													echo '<td style="text-align:right;">';
													echo number_format($arrTotalSums["perWeek"], 0, '', '.');
													echo '</td>';

													echo '</tr>';


													echo '<tr class="row3" style="">';

													echo '<td style="font-weight:bold;text-align:right;" colspan="' . (2 + ((count($arrProductsProductionLocation)) * (count($arrProductsPrintType) + 1) * 2)) . '">';
													#echo 'DE: ';
													echo '<img src="layout/flags/flag_DE.gif" width="14" height="10" alt="DE" title="" /> ';
													echo 'Wareneingang Insg.:</b>';
													echo '</td>';


													echo '<td style="text-align:right;font-weight:bold;font-size:14px;" colspan="2">';
													echo number_format($thisProductTotalStock, 0, '', '.');
													echo '</td>';

													echo '</tr>';

													echo '<tr class="row3" style="">';

													echo '<td style="font-weight:bold;text-align:right;" colspan="' . (2 + ((count($arrProductsProductionLocation)) * (count($arrProductsPrintType) + 1) * 2)) . '">';
													#echo 'DE: ';
													echo '<img src="layout/flags/flag_DE.gif" width="14" height="10" alt="DE" title="" /> ';
													echo 'Aktueller Lagerbestand:';
													echo '</td>';

													echo '<td style="text-align:right;font-weight:bold;font-size:14px;" colspan="2">';
													echo number_format(($thisProductTotalStock - $arrTotalSums["LOCATION_DE"]["perYear"]), 0, '', '.');
													echo '</td>';

													echo '</tr>';

													echo '</table>';
												}
											// EOF GET USAGE
										?>
									</fieldset>
								</div>
								<?php } ?>
							<?php } ?>
						</div>
					</div>

				</div>
			</div>
		</div>
	</div>
</div>
<script language="javascript" type="text/javascript">
	$(document).ready(function() {

		$('#editProductDataProductName').keyup(function () {
			loadSuggestions('searchProductName', [{'triggerElement': '#editProductDataProductName', 'fieldNumber': '#editProductDataProductNumber', 'fieldName': '#editProductDataProductName', 'fieldID': '#editOrdersArtikelID'}], 0);
		});
		$('#editProductDataProductNumber').keyup(function () {
			loadSuggestions('searchProductNumber', [{'triggerElement': '#editProductDataProductNumber', 'fieldNumber': '#editProductDataProductNumber', 'fieldName': '#editProductDataProductName', 'fieldID': '#editOrdersArtikelID'}], 1);
		});

		colorRowMouseOver('.displayProducts tbody tr');
		$('.buttonNotice').click(function () {
			loadNotice($(this));
		});
		$('.buttonLoadProductDetails').click(function(){
			loadProductDetails($(this).attr('alt'));
		});
		$(".displayProducts .displayProductImage img").css('display', 'none').show().lazyload({
			container: $(".displayProductImage"),
			effect : "fadeIn"
		});

		$(function() {
			$.datepicker.setDefaults($.datepicker.regional[""]);
			$('#editProductsStockReceiptDate').datepicker($.datepicker.regional["de"]);
			$('#editProductsStockReceiptDate').datepicker("option", "maxDate", "0" );

		});

		$(function() {
			// $('#tabs').tabs();
		});
	});
</script>

<?php require_once('inc/footerHTML.inc.php'); ?>