<?php
	session_start();

	require_once('config/configMandator.inc.php');
	require_once('config/configParams_' . strtolower(MANDATOR) . '.inc.php');
	require_once('config/configBasic.inc.php');
	require_once('config/configFiles.inc.php');
	require_once('config/configTables.inc.php');
	require_once('inc/functions.inc.php');
	##require_once('header.inc.php');

	$thisAddressType = (trim($_GET["addressType"]));
	$arrRegionCodes = array(
						"BB" => "0",
						"BE" => "2",
						"BW" => "5",
						"BY" => "11",
						"HB" => "4",
						"HE" => "1",
						"HH" => "6",
						"MV" => "3",
						"NI" => "16",
						"NW" => "9",
						"RP" => "10",
						"SH" => "13",
						"SL" => "7",
						"SN" => "12",
						"ST" => "14",
						"TH" => "8"
					);

	$warningMessage = '';
	$errorMessage = '';
	$successMessage = '';
	$infoMessage = '';

	$useUf8Encode = false;

	$dbConnection = new DB_Connection(DB_HOST, '', DB_NAME, DB_USER, DB_PASSWORD);
	$dbOpen = $dbConnection->db_connect();

	// BOF GET GOOGLE MAPS POINTS
		$sql_mapPoints = "SELECT
				`" . TABLE_ADDRESS_DATAS . "`.`addressesID`,
				`" . TABLE_ADDRESS_DATAS . "`.`addressesName`,
				`" . TABLE_ADDRESS_DATAS . "`.`addressesNameTitle`,
				`" . TABLE_ADDRESS_DATAS . "`.`addressesAddress`,
				`" . TABLE_ADDRESS_DATAS . "`.`addressesPostbox`,
				`" . TABLE_ADDRESS_DATAS . "`.`addressesZipcode`,
				`" . TABLE_ADDRESS_DATAS . "`.`addressesCity`,
				`" . TABLE_ADDRESS_DATAS . "`.`addressesArea`,
				`" . TABLE_ADDRESS_DATAS . "`.`addressesPhone`,
				`" . TABLE_ADDRESS_DATAS . "`.`addressesFax`,
				`" . TABLE_ADDRESS_DATAS . "`.`addressesMail`,
				`" . TABLE_ADDRESS_DATAS . "`.`addressesInternet`,
				`" . TABLE_ADDRESS_DATAS . "`.`addressesAddressType`,

				`" . TABLE_ZIPCODES_CITIES . "`.`place_name`,
				`" . TABLE_ZIPCODES_CITIES . "`.`postal_code`,
				`" . TABLE_ZIPCODES_CITIES . "`.`firstOrderSubdivision_stateName` AS `region_1`,
				`" . TABLE_ZIPCODES_CITIES . "`.`firstOrderSubdivision_stateCode` AS `region_1_code`,
				`" . TABLE_ZIPCODES_CITIES . "`.`secondOrderSubdivision_stateName` AS `region_2`,
				`" . TABLE_ZIPCODES_CITIES . "`.`thirdOrderSubdivision_stateName` AS `region_3`,

				`" . TABLE_GEODB_TEXTDATA . "`.`text_val`,

				/*
				`geodb_textdata2`.`text_val` AS `text_val2`,
				*/

				`" . TABLE_GEODB_COORDINATES . "`.`lat` AS `latitude`,
				`" . TABLE_GEODB_COORDINATES . "`.`lon` AS `longitude`

				FROM `" . TABLE_ADDRESS_DATAS . "`

				LEFT JOIN `" . TABLE_GEODB_TEXTDATA . "`
				ON(`" . TABLE_ADDRESS_DATAS . "`.`addressesZipcode` = `" . TABLE_GEODB_TEXTDATA . "`.`text_val`)

				/*
				LEFT JOIN `" . TABLE_GEODB_TEXTDATA . "` AS `geodb_textdata2`
				ON(`" . TABLE_GEODB_TEXTDATA . "`.`loc_id` = `geodb_textdata2`.`loc_id`)
				*/

				LEFT JOIN `" . TABLE_GEODB_LOCATIONS . "`
				ON (`" . TABLE_GEODB_TEXTDATA . "`.`loc_id` = `" . TABLE_GEODB_LOCATIONS . "`.`loc_id`)

				LEFT JOIN `" . TABLE_GEODB_COORDINATES . "`
				ON(`" . TABLE_GEODB_TEXTDATA . "`.`loc_id` = `" . TABLE_GEODB_COORDINATES . "`.`loc_id`)

				LEFT JOIN `" . TABLE_ZIPCODES_CITIES . "`
				ON(`" . TABLE_ADDRESS_DATAS . "`.`addressesZipcode` = `" . TABLE_ZIPCODES_CITIES . "`.`postal_code`)

				WHERE 1
					AND `" . TABLE_ADDRESS_DATAS . "`.`addressesAddressType` = " . $thisAddressType . "
					AND (
						`" . TABLE_GEODB_TEXTDATA . "`.`text_type` = '500300000'
						/*
						AND
						`geodb_textdata2`.`text_type` = '500100000'
						*/
					)

				GROUP BY `" . TABLE_ADDRESS_DATAS . "`.`addressesID`
				";

	// EOF GET GOOGLE MAPS POINTS

	// BOF GET GOOGLE MAPS AREAS
		// COUNTRY
		$sql_mapAreas = "SELECT
					`lat` AS `latitude`,
					`lon` AS `longitude`,
					`country`,
					`subcountry`,
					IF(`subcountry` != '' , CONCAT(`country`, '_', `subcountry`), `country`) AS `area`

					FROM `" . TABLE_GEODB_AREAS . "`

					WHERE 1 AND `subcountry` = '' AND `country` = 'DE'
		";

		$sql_mapAreas2 = "SELECT
					`lat` AS `latitude`,
					`lon` AS `longitude`,
					`country`,
					`subcountry`,
					IF(`subcountry` != '' , CONCAT(`country`, '_', `subcountry`), `country`) AS `area`

					FROM `" . TABLE_GEODB_AREAS . "`

					WHERE 1 AND `subcountry` != '' AND `country` = 'DE'
		";
	// ROF GET GOOGLE MAPS AREAS
?>

<?php
	require_once('inc/headerHTML.inc.php');
	$thisTitle = '';
	$headerHTML = preg_replace("/{###TITLE###}/", strip_tags($thisTitle), $headerHTML);
	$headerHTML = preg_replace('/<div id="headerArea">(.*)<noscript>/ism', '<noscript>', $headerHTML);
	$headerHTML = preg_replace('/<body/ism', '<body id="bodyFrame" onload="initialize();"', $headerHTML);

	echo $headerHTML;
?>
	<div id="contentAreaElements">
		<div id="myGoogleMapsArea">
			<div id="myGoogleMapCanvas">
				<span class="loadingArea"><img class="loader" src="layout/ajax-loader.gif" /> Bitte haben Sie ein wenig Geduld. Die Daten werden geladen...</span>
			</div>
		</div>
	</div>
	<script language="javascript" type="text/javascript">
		$('#myGoogleMapCanvas').css({
			'width': '100%',
			'height': ($(window).height() - 30) + 'px'
		});
	</script>

	<!-- GOOGLE MAPS START -->
	<script type="text/javascript" src="<?php echo PATH_GOOGLE_MAPS_API; ?>"></script>
	<script type="text/javascript" language="javascript">
		// try {
		function setGmapMarkers(map, i) {
			var thisMarkerImage;
			if(i == 0) {
				thisMarkerImage = "bctr_googleMarkerFlag.png";
				thisMarkerShadowImage = "bctr_googleMarkerFlagShadow.png";
				thisMarkerSize = new Array(81, 84);
				thisMarkerShadowSize = new Array(81, 84);
				thisZindex = (myPointDatas.length + 10);
			}
			else {
				thisMarkerImage = myPointDatas[i]["markerImage"];
				thisMarkerShadowImage = "googleMapsIconShadow.png";
				thisMarkerSize = new Array(28, 20);
				thisMarkerShadowSize = new Array(28, 20);
				thisZindex = (myPointDatas.length - i);
			}

			var image = new google.maps.MarkerImage(
						"layout/icons/" + thisMarkerImage,
						new google.maps.Size(thisMarkerSize[0], thisMarkerSize[1]),
						new google.maps.Point(0,0),
						new google.maps.Point(0, thisMarkerSize[1])
			);

			var shadow = new google.maps.MarkerImage(
						"layout/icons/" + thisMarkerShadowImage,
						new google.maps.Size(thisMarkerShadowSize[0], thisMarkerShadowSize[1]),
						new google.maps.Point(0, 0),
						new google.maps.Point(0, thisMarkerShadowSize[1])
			);

			var shape = {
				/* coord: [1, (thisMarkerSize[1] + 1 - 20), 1, thisMarkerSize[1], thisMarkerSize[0], thisMarkerSize[1], thisMarkerSize[0] , (thisMarkerSize[1] + 1 - 20)], */
				coord: [1, 1, 1, 28, 20, 28, 20 , 1],
				type: "poly"
			};

			var myLatLng = new google.maps.LatLng(myPointDatas[i]["latitude"], myPointDatas[i]["longitude"]);
			marker[i] = new google.maps.Marker({
				position: myLatLng,
				map: map,
				shadow: shadow,
				icon: image,
				shape: shape,
				title: myPointDatas[i]["title"],
				zIndex: thisZindex
			});
			shadow[i] = new google.maps.Marker({
				position: myLatLng,
				map: map,
				icon: shadow,
				shape: shape,
				title: myPointDatas[i]["title"],
				zIndex: thisZindex
			});

			/* DEFINE INFO WINDOW */
				var html		= new Array();
				var imageExists	= false;
				html[i] = "";
				html[i] += "<div class=\"myGmapInfo\">";
				/* html[i] += "  <p class='headline'>" + myPointDatas[i]["title"] + "</p>"; */
				html[i] += "  <p class='headline'>" + myPointDatas[i]["header"] + "</p>";
				if(myPointDatas[i]["imagePath"] != "") {
				  imageExists = true;
				  html[i] += '  <img src="' + myPointDatas[i]["imagePath"] + '" ' + myPointDatas[i]["imageDimension"] + ' alt="' + myPointDatas[i]["title"] + '"/>';
				}

				html[i] += '<table border="0" width="" cellpadding="0" cellspacing="0">';

				if(myPointDatas[i]["text"] != "") {
				  // html[i] += "  <p class='text'>" + myPointDatas[i]["text"].replace(/, /g, ",<br />") + "</p>";
				  html[i] += "  <tr><td><b>Name:</b></td><td>" + myPointDatas[i]["text"] + "</td></tr>";
				}

				if(myPointDatas[i]["address"] != "") {
				  html[i] += "  <tr><td><b>Anschrift:</b></td><td>" + myPointDatas[i]["address"] + "</td></tr>";
				}

				if(myPointDatas[i]["city"] != "") {
				  html[i] += "  <tr><td><b>Ort:</b></td><td>" + myPointDatas[i]["city"] + "</td></tr>";
				}

				if(myPointDatas[i]["region_1"] != "") {
				  html[i] += "  <tr><td><b>Bundesland:</b></td><td>" + myPointDatas[i]["region_1"] + "</td></tr>";
				}

				if(myPointDatas[i]["region_2"] != "") {
				  html[i] += "  <tr><td><b>Region:</b></td><td>" + myPointDatas[i]["region_2"] + "</td></tr>";
				}

				if(myPointDatas[i]["region_3"] != "") {
				  html[i] += "  <tr><td><b>Kreis:</b></td><td>" + myPointDatas[i]["region_3"] + "</td></tr>";
				}

				html[i] += "  <tr><td><b>Longitude:</b></td><td>" + myPointDatas[i]["longitude"] + "</td></tr>";
				html[i] += "  <tr><td><b>Latitude: </b></td><td>" + myPointDatas[i]["latitude"] + "</td></tr>";

				html[i] += '</table>';

				html[i] += "<div class='clear'></div>";
				html[i] += "</div>";

				infowindow[i] = new google.maps.InfoWindow({
					content: html[i]
				});
			/* END DEFINE INFO WINDOW */

			/* SHOW INFO WINDOW	 */
				google.maps.event.addListener(marker[i], "click", function() {
					if(activeMarker > -1) {
						infowindow[activeMarker].close();
					}
					infowindow[i].open(map,marker[i]);
					activeMarker = i;
				});
				/* createMarkerLinkInGmapSidebar(i, myPointDatas[i]["title"], marker[i], imageExists); */
			/* END SHOW INFO WINDOW */
		}

		/* BOF DEFINE DEFAULT VALUES */
			var myPointDatas	= new Array();

			var elementId = "myGoogleMapCanvas";
			var default_arrayKey	= 0;
			var default_lat			= "52.103138";
			var default_lon			= "7.622817";
			var default_zoom		= 6;

			var sidebarHtml		= "";              			/* HTML-Code für die gesamte Sidebar */
			var sidebarMarkers	= [];              			/* Array für die Marker */
			var sidebarCount	= 0;               			/* Zähler für die Marker (= Zeile in der Sidebar) */
			var marker			= new Array();
			var activeMarker	= -1;
			var infowindow		= new Array();
		/* EOF DEFINE DEFAULT VALUES */

		/* BOF DEFINE POINT DATAS */
			myPointDatas[0] = new Array();
			myPointDatas[0]["latitude"] = default_lat;
			myPointDatas[0]["longitude"] = default_lon;
			myPointDatas[0]["title"] = "BURHAN CTR";
			myPointDatas[0]["header"] = "BURHAN CTR";
			myPointDatas[0]["text"] = "";
			myPointDatas[0]["city"] = "";
			myPointDatas[0]["region_1"] = "";
			myPointDatas[0]["region_2"] = "";
			myPointDatas[0]["region_3"] = "";
			myPointDatas[0]["imagePath"] = "";
		/* EOF DEFINE POINT DATAS */

		<?php
		// BOF WRITE GOOGLE POINTS
			$rs_mapPoints = $dbConnection->db_query($sql_mapPoints);
			$arrGoogleMapZipcodeAreas = array();
			$countPoints = 1;

			/*
			`" . TABLE_ADDRESS_DATAS . "`.``,
				`" . TABLE_ADDRESS_DATAS . "`.`addressesName`,
				`" . TABLE_ADDRESS_DATAS . "`.`addressesNameTitle`,
				`" . TABLE_ADDRESS_DATAS . "`.`addressesAddress`,
				`" . TABLE_ADDRESS_DATAS . "`.`addressesPostbox`,
				`" . TABLE_ADDRESS_DATAS . "`.`addressesZipcode`,
				`" . TABLE_ADDRESS_DATAS . "`.`addressesCity`,
				`" . TABLE_ADDRESS_DATAS . "`.`addressesArea`,
				`" . TABLE_ADDRESS_DATAS . "`.`addressesPhone`,
				`" . TABLE_ADDRESS_DATAS . "`.`addressesFax`,
				`" . TABLE_ADDRESS_DATAS . "`.`addressesMail`,
				`" . TABLE_ADDRESS_DATAS . "`.`addressesInternet`,
				`" . TABLE_ADDRESS_DATAS . "`.`addressesAddressType`,
			*/

			while($ds_mapPoints = mysqli_fetch_assoc($rs_mapPoints)) {
				echo 'myPointDatas[' . $countPoints . '] = new Array();' . "\n";
				echo 'myPointDatas[' . $countPoints . ']["latitude"] = "' . $ds_mapPoints["latitude"] . '";' . "\n";
				echo 'myPointDatas[' . $countPoints . ']["longitude"] = "' . $ds_mapPoints["longitude"] . '";' . "\n";
				echo 'myPointDatas[' . $countPoints . ']["title"] = "' . $ds_mapPoints["addressesZipcode"] . ' ' . $ds_mapPoints["addressesCity"] . ' / ' . $ds_mapPoints["addressesName"];
				if($ds_mapPoints["text_val2"] != $ds_mapPoints["place_name"]) { echo ' / ' . $ds_mapPoints["text_val2"]; }
				echo '";' . "\n";
				echo 'myPointDatas[' . $countPoints . ']["header"] = "' . $ds_mapPoints["addressesName"];
				if($ds_mapPoints["text_val2"] != $ds_mapPoints["place_name"]) { echo ' / ' . $ds_mapPoints["text_val2"]; }
				echo '";' . "\n";
				echo 'myPointDatas[' . $countPoints . ']["text"] = "' . $ds_mapPoints["addressesName"] . '";' . "\n";
				echo 'myPointDatas[' . $countPoints . ']["address"] = "' . $ds_mapPoints["addressesAddress"] . " / " . $ds_mapPoints["addressesPostbox"] . '";' . "\n";
				echo 'myPointDatas[' . $countPoints . ']["city"] = "' . $ds_mapPoints["addressesZipcode"] . ' ' . $ds_mapPoints["addressesCity"] . '";' . "\n";
				echo 'myPointDatas[' . $countPoints . ']["region_1"] = "' . $ds_mapPoints["region_1"] . '";' . "\n";
				echo 'myPointDatas[' . $countPoints . ']["region_2"] = "' . $ds_mapPoints["region_2"] . '";' . "\n";
				echo 'myPointDatas[' . $countPoints . ']["region_3"] = "' . $ds_mapPoints["region_3"] . '";' . "\n";
				echo 'myPointDatas[' . $countPoints . ']["imagePath"] = "";' . "\n";
				$thisIconImage = "googleMapsIcon.png";
				if(file_exists("layout/icons/googleMapsIcon_" . $arrRegionCodes[$ds_mapPoints["region_1_code"]] . ".png")) {
					$thisIconImage = "googleMapsIcon_" . $arrRegionCodes[$ds_mapPoints["region_1_code"]] . ".png";
				}
#echo "alert('" . $ds["region_1_code"] . " | ". $arrRegionCodes[$ds["region_1_code"]] . "'); ";
				echo 'myPointDatas[' . $countPoints . ']["markerImage"] = "' . $thisIconImage . '";' . "\n";
				$countPoints ++;
			}
		// EOF WRITE GOOGLE POINTS

		// BOF WRITE GOOGLE AREAS/POLYGONS
			$rs_mapAreas = $dbConnection->db_query($sql_mapAreas);
			$rs_mapAreas2 = $dbConnection->db_query($sql_mapAreas2);

			$arrGoogleMapAreas = array();
			while($ds_mapAreas = mysqli_fetch_assoc($rs_mapAreas)) {
				$arrGoogleMapAreas[$ds_mapAreas["area"]][] = 'new google.maps.LatLng(' . $ds_mapAreas["latitude"] . ', ' . $ds_mapAreas["longitude"] . ')';
			}
			if(!empty($arrGoogleMapAreas)){
				foreach($arrGoogleMapAreas as $thisKey => $thisValue) {
					echo 'var coords_' . $thisKey . ' = [' . implode(", ", $thisValue) . '];';
				}
			}
			/*
			$arrGoogleMapAreas2 = array();
			while($ds_mapAreas2 = mysqli_fetch_assoc($rs_mapAreas2)) {
				$arrGoogleMapAreas2[$ds_mapAreas2["subcountry"]][] = 'new google.maps.LatLng(' . $ds_mapAreas2["latitude"] . ', ' . $ds_mapAreas2["longitude"] . ')';
			}
			if(!empty($arrGoogleMapAreas2)){
				foreach($arrGoogleMapAreas2 as $thisKey => $thisValue) {
					echo 'var coords_' . $thisKey . ' = [' . implode(", ", $thisValue) . '];';
				}
			}
			*/
			#if(!empty($arrGoogleMapZipcodeAreas)){
				#foreach($arrGoogleMapZipcodeAreas as $thisKey => $thisValue) {
					#echo 'var coords_' . $thisKey . ' = [' . implode(", ", $thisValue) . '];';
				#}
			#}
		// EOF WRITE GOOGLE AREAS/POLYGONS
		?>

		/* BOF CREATE GOOGLE MAP */
			function initialize() {
				if (!document.getElementById(elementId)) {
					alert("Fehler: das Element mit der id "+ elementId+ " konnte nicht auf dieser Webseite gefunden werden!");
					return false;
				}
				else {

				}
			}

			var latlng = new google.maps.LatLng(myPointDatas[default_arrayKey]["latitude"], myPointDatas[default_arrayKey]["longitude"]);

			var myOptions = {
				zoom: default_zoom,
				center: latlng,
				panControl: true,
				zoomControl: true,
				navigationControl: true,
				mapTypeControl: true,
				scaleControl: true,
				overviewMapControl: true,
				streetViewControl: true,
				mapTypeId: google.maps.MapTypeId.TERRAIN
				/* ROADMAP zeigt die normalen Standard-2D-Kacheln von Google Maps an. */
				/* SATELLITE zeigt Fotokacheln an. */
				/* HYBRID zeigt eine Mischung aus Fotokacheln und eine Kachelebene für markante Merkmale an (Straßen, Ortsnamen). */
				/* TERRAIN zeigt Reliefkacheln für Erhebungen und GewÃ¤sser (Berge, Flüsse usw.) an. */
			};
			var map = new google.maps.Map(document.getElementById(elementId), myOptions);

			<?php
			if(!empty($arrGoogleMapAreas)){
				foreach($arrGoogleMapAreas as $thisKey => $thisValue) {
					echo 'var area_' . $thisKey . ' = new google.maps.Polygon({
							paths: coords_' . $thisKey . ',
							strokeColor: "#FF0000",
							strokeOpacity: 0.8,
							strokeWeight: 2,
							fillColor: "#FFFF00",
							fillOpacity: 0.1
							});
						area_' . $thisKey . '.setMap(map);
					';
				}
			}

			/*
			if(!empty($arrGoogleMapAreas2)){
				foreach($arrGoogleMapAreas2 as $thisKey => $thisValue) {
					echo 'var area_' . $thisKey . ' = new google.maps.Polygon({
							paths: coords_' . $thisKey . ',
							strokeColor: "#000033",
							strokeOpacity: 0.2,
							strokeWeight: 1,
							fillColor: "#FFFF00",
							fillOpacity: 0.0
							});
						area_' . $thisKey . '.setMap(map);
					';
				}
			}
			*/
			?>

			if(myPointDatas.length > 0) {
				for(i = 0 ; i < myPointDatas.length ; i++) {
					setGmapMarkers(map, i);
				}
			}
		/* EOF CREATE GOOGLE MAP */

	// } catch(err) { alert(err); } finally {}
	</script>
	<!-- GOOGLE MAPS END -->

<script language="javascript" type="text/javascript">
	$(document).ready(function() {
		// $('body').attr('onload', 'initialize();');
	});
</script>

<?php $noMandatorySwitch = true; ?>
<?php require_once('inc/footerHTML.inc.php'); ?>