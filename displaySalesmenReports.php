<?php
	require_once('inc/requires.inc.php');

	if(!$arrGetUserRights["displayDistribution"]) {
		header('location: ' . PAGE_EXIT_LOCATION);
		exit;
	}

	$jswindowMessage = "";
	$warningMessage = '';
	$errorMessage = '';
	$successMessage = '';
	$infoMessage = '';

	$dbConnection = new DB_Connection();
	$db_open = $dbConnection->db_connect();

	#if(DB_HOST_EXTERN_ADM_REPORTS != '' && DB_NAME_EXTERN_ADM_REPORTS != '' && DB_USER_EXTERN_ADM_REPORTS != '' && DB_PASSWORD_EXTERN_ADM_REPORTS) {
	if(DB_HOST_EXTERN_ACQUISITION != '' && DB_NAME_EXTERN_ACQUISITION != '' && DB_USER_EXTERN_ACQUISITION != '' && DB_PASSWORD_EXTERN_ACQUISITION) {
		$existsOnlineAquisition = true;
	}
	else {
		$existsOnlineAquisition = false;
		$errorMessage .= 'Es sind keine Zugansdaten f&uuml;r die Datenbankverbindung zur ' . strtoupper(MANDATOR) . '-Kundenerfassung eingetragen. ' . '<br />';
	}

	if($existsOnlineAquisition) {
		$dbConnection_ExternAcqisition = new DB_Connection(DB_HOST_EXTERN_ACQUISITION, '', DB_NAME_EXTERN_ACQUISITION, DB_USER_EXTERN_ACQUISITION, DB_PASSWORD_EXTERN_ACQUISITION);
		$db_openExternAcqisition = $dbConnection_ExternAcqisition->db_connect();

		// BOF READ VERTRETER
			$arrSalesmenDatas = getSalesmen2();
			$arrAquisitionUsers = getAquisitionUsers();

			$arrTemp = $arrAquisitionUsers;
			$arrTemp2 = array();
			foreach($arrTemp as $thisTemp){
				foreach($thisTemp as $thisTempKey => $thisTempValue){
					$thisTemp[$thisTempKey] = utf8_encode($thisTempValue);
				}
				if($thisTemp["usersAuftragslistenCustomerID"] > 0 && $thisTemp["usersActive"] == '1'){
					$arrTemp2[$thisTemp["usersID"]] = $thisTemp;
				}
				if($_COOKIE["isAdmin"] == '1'){
					$thisUserRights = unserialize($thisTemp["usersRights"]);

					if($thisUserRights["adminArea"] == '1'){
						$arrTemp2[$thisTemp["usersID"]] = $thisTemp;
					}
				}
			}
			$arrSalesmenDatas = $arrTemp2;
		// EOF READ VERTRETER

		// BOF READ SALUTATIONS
			$arrSalutationTypeDatas = getSalutationTypes();
		// EOF READ SALUTATIONS

		// BOF READ CONTACT TYPE DATAS
			$arrCustomerContactTypeDatas = getContactTypes();
		// EOF READ CONTACT TYPE DATAS

		// BOF SET SEARCH DATE
			if($_POST["searchInterval"] == ""){
				$_POST["searchInterval"] = "DAILY";
			}

			$todayWeekDayNumber = date('w');
			if($todayWeekDayNumber == 0){ $todayWeekDayNumber = 7; }
			else if($todayWeekDayNumber == 1){ $todayWeekDayNumber = 8; }
			$todayWeekNumber = date('W');
			$todayDate = date("Y-m-d");

			if($_POST["searchInterval"] != "DAILY"){
				$fromDate = "";
				$toDate = "";
				$_POST["displayReportsDateFrom"] = "";
				$_POST["displayReportsDateTo"] = "";
			}
			else {
				$fromDate = formatDate(date("Y-m-d", mktime(1, 1, 1, date("m"), (date("d") - ($todayWeekDayNumber - 1)), date("Y"))), "display");
				if($_POST["displayReportsDateFrom"] != ""){
					$fromDate = $_POST["displayReportsDateFrom"];
				}

				$toDate = formatDate(date("Y-m-d", mktime(1, 1, 1, date("m"), (date("d") + (7 - $todayWeekDayNumber)), date("Y"))), "display");
				if($_POST["displayReportsDateTo"] != ""){
					$toDate = $_POST["displayReportsDateTo"];
				}
			}
		// EOF SET SEARCH DATE

		$where = "";

		if($_POST["searchSalesmanOnlineID"] != ""){
			$where .= " AND `" . TABLE_ACQUISATION_USERS_DRIVERS_LOG . "`.`usersDriversLogsUser` = '" . $_POST["searchSalesmanOnlineID"] . "'";
		}
		if($fromDate != "" && $toDate != ""){
			$where .= "
				AND `" . TABLE_ACQUISATION_USERS_DRIVERS_LOG . "`.`usersDriversLogsDate` >= '" . formatDate($fromDate, "store") . "'
				AND `" . TABLE_ACQUISATION_USERS_DRIVERS_LOG . "`.`usersDriversLogsDate` <= '" . formatDate($toDate, "store") . "'
			";
		}


		if($_POST["searchInterval"] == "WEEK"){
			$where .= "";

			$dateField = "
				CONCAT(
					IF(DATE_FORMAT(`" . TABLE_ACQUISATION_USERS_DRIVERS_LOG . "`.`usersDriversLogsDate`, '%m') = '12' AND DATE_FORMAT(`" . TABLE_ACQUISATION_USERS_DRIVERS_LOG . "`.`usersDriversLogsDate`, '%v') = '01', (DATE_FORMAT(`" . TABLE_ACQUISATION_USERS_DRIVERS_LOG . "`.`usersDriversLogsDate`, '%Y') + 1),  DATE_FORMAT(`" . TABLE_ACQUISATION_USERS_DRIVERS_LOG . "`.`usersDriversLogsDate`, '%Y')),
					'#',
					DATE_FORMAT(`" . TABLE_ACQUISATION_USERS_DRIVERS_LOG . "`.`usersDriversLogsDate`, '%v')
				) AS `interval`,
			";
		}
		else if($_POST["searchInterval"] == "MONTH"){
			// DATE_FORMAT(`" . TABLE_ORDER_INVOICES . "`.`orderDocumentsDocumentDate`, '%M') AS `interval`,
			$where .= "";

			$dateField = "
				CONCAT(
					DATE_FORMAT(`" . TABLE_ACQUISATION_USERS_DRIVERS_LOG . "`.`usersDriversLogsDate`, '%Y'),
					'#',
					DATE_FORMAT(`" . TABLE_ACQUISATION_USERS_DRIVERS_LOG . "`.`usersDriversLogsDate`, '%m')
				) AS `interval`,
			";
		}
		else if($_POST["searchInterval"] == "QUARTER"){
			$where .= "";

			$dateField = "
				CONCAT(
					DATE_FORMAT(`" . TABLE_ACQUISATION_USERS_DRIVERS_LOG . "`.`usersDriversLogsDate`, '%Y'),
					'#',
					IF(DATE_FORMAT(`" . TABLE_ACQUISATION_USERS_DRIVERS_LOG . "`.`usersDriversLogsDate`, '%m') = '01' OR DATE_FORMAT(`" . TABLE_ACQUISATION_USERS_DRIVERS_LOG . "`.`usersDriversLogsDate`, '%m') = '02' OR DATE_FORMAT(`" . TABLE_ACQUISATION_USERS_DRIVERS_LOG . "`.`usersDriversLogsDate`, '%m') = '03', '1. Quartal',
						IF(DATE_FORMAT(`" . TABLE_ACQUISATION_USERS_DRIVERS_LOG . "`.`usersDriversLogsDate`, '%m') = '04' OR DATE_FORMAT(`" . TABLE_ACQUISATION_USERS_DRIVERS_LOG . "`.`usersDriversLogsDate`, '%m') = '05' OR DATE_FORMAT(`" . TABLE_ACQUISATION_USERS_DRIVERS_LOG . "`.`usersDriversLogsDate`, '%m') = '06', '2. Quartal',
							IF(DATE_FORMAT(`" . TABLE_ACQUISATION_USERS_DRIVERS_LOG . "`.`usersDriversLogsDate`, '%m') = '07' OR DATE_FORMAT(`" . TABLE_ACQUISATION_USERS_DRIVERS_LOG . "`.`usersDriversLogsDate`, '%m') = '08' OR DATE_FORMAT(`" . TABLE_ACQUISATION_USERS_DRIVERS_LOG . "`.`usersDriversLogsDate`, '%m') = '09', '3. Quartal',
								IF(DATE_FORMAT(`" . TABLE_ACQUISATION_USERS_DRIVERS_LOG . "`.`usersDriversLogsDate`, '%m') = '10' OR DATE_FORMAT(`" . TABLE_ACQUISATION_USERS_DRIVERS_LOG . "`.`usersDriversLogsDate`, '%m') = '11' OR DATE_FORMAT(`" . TABLE_ACQUISATION_USERS_DRIVERS_LOG . "`.`usersDriversLogsDate`, '%m') = '12', '4. Quartal',
									''
								)
							)
						)
					)
				) AS `interval`,
			";
		}
		else if($_POST["searchInterval"] == "YEAR"){
			$where .= "";

			$dateField = "
				CONCAT(
					DATE_FORMAT(`" . TABLE_ACQUISATION_USERS_DRIVERS_LOG . "`.`usersDriversLogsDate`, '%Y'),
					'#',
					DATE_FORMAT(`" . TABLE_ACQUISATION_USERS_DRIVERS_LOG . "`.`usersDriversLogsDate`, '%Y')
				) AS `interval`,
			";
		}
		else if($_POST["searchInterval"] == "DAILY"){
			$where .= "";

			$dateField = "
				`" . TABLE_ACQUISATION_USERS_DRIVERS_LOG . "`.`usersDriversLogsDate` AS `interval`,
			";
		}

		$arrIntervals = array();
		$arrDatas = array();

		$defaultORDER = "customersTimeCreated";
		$defaultSORT = "ASC";

		if($_REQUEST["sortField"] != "") {
			$thisORDER = $_REQUEST["sortField"];
		}
		else {
			$thisORDER = $defaultORDER;
		}

		if($_REQUEST["sortDirection"] != "") {
			$thisSORT = $_REQUEST["sortDirection"];
		}
		else {
			$thisSORT = $defaultSORT;
		}

		if($thisSORT == "DESC") { $thisSORT = "ASC"; }
		else { $thisSORT = "DESC"; }

		$sql = "SELECT

					SQL_CALC_FOUND_ROWS

					" . $dateField . "

					`" . TABLE_ACQUISATION_USERS_DRIVERS_LOG . "`.`usersDriversLogsID`,
					`" . TABLE_ACQUISATION_USERS_DRIVERS_LOG . "`.`usersDriversLogsDate`,
					`" . TABLE_ACQUISATION_USERS_DRIVERS_LOG . "`.`usersDriversLogsUser`,
			";

		if($_POST["searchInterval"] == "DAILY"){
			$sql .= "
					`" . TABLE_ACQUISATION_USERS_DRIVERS_LOG . "`.`usersDriversLogsDepartureTime`,
					`" . TABLE_ACQUISATION_USERS_DRIVERS_LOG . "`.`usersDriversLogsArrivalTime`,
					`" . TABLE_ACQUISATION_USERS_DRIVERS_LOG . "`.`usersDriversLogsVisitTotalTime`,
					`" . TABLE_ACQUISATION_USERS_DRIVERS_LOG . "`.`usersDriversLogsTotalTargets`,
					`" . TABLE_ACQUISATION_USERS_DRIVERS_LOG . "`.`usersDriversLogsTotalKilometers`,
					`" . TABLE_ACQUISATION_USERS_DRIVERS_LOG . "`.`usersDriversLogsDetailsDistance`,
					`" . TABLE_ACQUISATION_USERS_DRIVERS_LOG . "`.`usersDriversLogsTotalAreas`,
					`" . TABLE_ACQUISATION_USERS_DRIVERS_LOG . "`.`usersDriversLogsTotalDurance`,
					`" . TABLE_ACQUISATION_USERS_DRIVERS_LOG . "`.`usersDriversLogsTotalCharges`,
					`" . TABLE_ACQUISATION_USERS_DRIVERS_LOG . "`.`usersDriversLogsTotalRefueling`,
					`" . TABLE_ACQUISATION_USERS_DRIVERS_LOG . "`.`usersDriversLogsVisitTotalMadeOrders`,
				";
		}
		else if($_POST["searchInterval"] != "DAILY"){
			$sql .= "
					GROUP_CONCAT(`" . TABLE_ACQUISATION_USERS_DRIVERS_LOG . "`.`usersDriversLogsID`, '#', `" . TABLE_ACQUISATION_USERS_DRIVERS_LOG . "`.`usersDriversLogsDepartureTime` SEPARATOR ';') AS `usersDriversLogsDepartureTime`,
					GROUP_CONCAT(`" . TABLE_ACQUISATION_USERS_DRIVERS_LOG . "`.`usersDriversLogsID`, '#', `" . TABLE_ACQUISATION_USERS_DRIVERS_LOG . "`.`usersDriversLogsArrivalTime` SEPARATOR ';') AS `usersDriversLogsArrivalTime`,
					GROUP_CONCAT(`" . TABLE_ACQUISATION_USERS_DRIVERS_LOG . "`.`usersDriversLogsID`, '#', `" . TABLE_ACQUISATION_USERS_DRIVERS_LOG . "`.`usersDriversLogsVisitTotalTime` SEPARATOR ';') AS `usersDriversLogsVisitTotalTime`,

					COUNT(`" . TABLE_ACQUISATION_USERS_DRIVERS_LOG_DETAILS . "`.`usersDriversLogsDetailsID`) AS `usersDriversLogsTotalTargets`,
					SUM(`" . TABLE_ACQUISATION_USERS_DRIVERS_LOG_DETAILS . "`.`usersDriversLogsDetailsKilometersReadingEnd` - `" . TABLE_ACQUISATION_USERS_DRIVERS_LOG_DETAILS . "`.`usersDriversLogsDetailsKilometersReadingStart`) AS `usersDriversLogsTotalKilometers`,
					SUM(`" . TABLE_ACQUISATION_USERS_DRIVERS_LOG . "`.`usersDriversLogsDetailsDistance`) AS `usersDriversLogsDetailsDistance`,
					GROUP_CONCAT( DISTINCT `" . TABLE_ACQUISATION_USERS_DRIVERS_LOG . "`.`usersDriversLogsTotalAreas` SEPARATOR ';') AS `usersDriversLogsTotalAreas`,
					GROUP_CONCAT( DISTINCT `" . TABLE_ACQUISATION_USERS_DRIVERS_LOG . "`.`usersDriversLogsID`, '#', `" . TABLE_ACQUISATION_USERS_DRIVERS_LOG . "`.`usersDriversLogsTotalDurance` SEPARATOR ';') AS `usersDriversLogsTotalDurance`,
					SUM(`" . TABLE_ACQUISATION_USERS_DRIVERS_LOG_DETAILS . "`.`usersDriversLogsDetailsCharges`) AS `usersDriversLogsTotalCharges`,
					SUM(`" . TABLE_ACQUISATION_USERS_DRIVERS_LOG_DETAILS . "`.`usersDriversLogsDetailsRefueling`) AS `usersDriversLogsTotalRefueling`,
					SUM(IF(`" . TABLE_ACQUISATION_USERS_DRIVERS_LOG_DETAILS . "`.`usersDriversLogsDetailsVisitReportMadeOrder` = '1', 1, 0)) AS `usersDriversLogsVisitTotalMadeOrders`,
				";
		}
		$sql .= "
					`" . TABLE_ACQUISATION_USERS_DRIVERS_LOG . "`.`usersDriversLogsTimestamp`,

			";
		if($_POST["searchInterval"] == "DAILY"){
			$sql .= "
					`" . TABLE_ACQUISATION_USERS_DRIVERS_LOG_DETAILS . "`.`usersDriversLogsDetailsID`,
					`" . TABLE_ACQUISATION_USERS_DRIVERS_LOG_DETAILS . "`.`usersDriversLogsDetailsLogID`,
					`" . TABLE_ACQUISATION_USERS_DRIVERS_LOG_DETAILS . "`.`usersDriversLogsDetailsSalesmenID`,
					`" . TABLE_ACQUISATION_USERS_DRIVERS_LOG_DETAILS . "`.`usersDriversLogsDetailsDate`,
					`" . TABLE_ACQUISATION_USERS_DRIVERS_LOG_DETAILS . "`.`usersDriversLogsDetailsRowID`,
					`" . TABLE_ACQUISATION_USERS_DRIVERS_LOG_DETAILS . "`.`usersDriversLogsDetailsLocationStart`,
					`" . TABLE_ACQUISATION_USERS_DRIVERS_LOG_DETAILS . "`.`usersDriversLogsDetailsLocationEnd`,
					`" . TABLE_ACQUISATION_USERS_DRIVERS_LOG_DETAILS . "`.`usersDriversLogsDetailsZipcode`,
					`" . TABLE_ACQUISATION_USERS_DRIVERS_LOG_DETAILS . "`.`usersDriversLogsDetailsCustomerID`,
					`" . TABLE_ACQUISATION_USERS_DRIVERS_LOG_DETAILS . "`.`usersDriversLogsDetailsCustomerNumber`,
					`" . TABLE_ACQUISATION_USERS_DRIVERS_LOG_DETAILS . "`.`usersDriversLogsDetailsCustomerName`,
					`" . TABLE_ACQUISATION_USERS_DRIVERS_LOG_DETAILS . "`.`usersDriversLogsDetailsIntention`,
					`" . TABLE_ACQUISATION_USERS_DRIVERS_LOG_DETAILS . "`.`usersDriversLogsDetailsKilometersReadingStart`,
					`" . TABLE_ACQUISATION_USERS_DRIVERS_LOG_DETAILS . "`.`usersDriversLogsDetailsKilometersReadingEnd`,
					`" . TABLE_ACQUISATION_USERS_DRIVERS_LOG_DETAILS . "`.`usersDriversLogsDetailsDistance`,
					`" . TABLE_ACQUISATION_USERS_DRIVERS_LOG_DETAILS . "`.`usersDriversLogsDetailsTimeStart`,
					`" . TABLE_ACQUISATION_USERS_DRIVERS_LOG_DETAILS . "`.`usersDriversLogsDetailsTimeEnd`,
					`" . TABLE_ACQUISATION_USERS_DRIVERS_LOG_DETAILS . "`.`usersDriversLogsDetailsCharges`,
					`" . TABLE_ACQUISATION_USERS_DRIVERS_LOG_DETAILS . "`.`usersDriversLogsDetailsRefueling`,
					`" . TABLE_ACQUISATION_USERS_DRIVERS_LOG_DETAILS . "`.`usersDriversLogsDetailsVisitReportMadeOrder`,
					`" . TABLE_ACQUISATION_USERS_DRIVERS_LOG_DETAILS . "`.`usersDriversLogsDetailsVisitReportNotice`,
					`" . TABLE_ACQUISATION_USERS_DRIVERS_LOG_DETAILS . "`.`usersDriversLogsDetailsVisitReportContactSalutation`,
					`" . TABLE_ACQUISATION_USERS_DRIVERS_LOG_DETAILS . "`.`usersDriversLogsDetailsVisitReportContactFirstName`,
					`" . TABLE_ACQUISATION_USERS_DRIVERS_LOG_DETAILS . "`.`usersDriversLogsDetailsVisitReportContactSecondName`,
					`" . TABLE_ACQUISATION_USERS_DRIVERS_LOG_DETAILS . "`.`usersDriversLogsDetailsVisitReportContactFunction`,
					`" . TABLE_ACQUISATION_USERS_DRIVERS_LOG_DETAILS . "`.`usersDriversLogsDetailsVisitReportKzhDistributor`,
					`" . TABLE_ACQUISATION_USERS_DRIVERS_LOG_DETAILS . "`.`usersDriversLogsDetailsVisitReportKzhDistributorAdd`,
					`" . TABLE_ACQUISATION_USERS_DRIVERS_LOG_DETAILS . "`.`usersDriversLogsDetailsVisitReportNextVisit`,
				";
			}
			else {
				$sql .= "
						GROUP_CONCAT(DISTINCT `" . TABLE_ACQUISATION_USERS_DRIVERS_LOG_DETAILS . "`.`usersDriversLogsDetailsZipcode` SEPARATOR '#') AS `usersDriversLogsDetailsZipcodeAreas`,
					";
			}
			$sql .= "
					`" . TABLE_ACQUISATION_USERS . "`.`usersID`,
					`" . TABLE_ACQUISATION_USERS . "`.`usersLogin`,
					`" . TABLE_ACQUISATION_USERS . "`.`usersPassword`,
					`" . TABLE_ACQUISATION_USERS . "`.`usersFirstName`,
					`" . TABLE_ACQUISATION_USERS . "`.`usersLastName`,
					`" . TABLE_ACQUISATION_USERS . "`.`usersAddressStreet`,
					`" . TABLE_ACQUISATION_USERS . "`.`usersAddressStreetNumber`,
					`" . TABLE_ACQUISATION_USERS . "`.`usersAddressZipcode`,
					`" . TABLE_ACQUISATION_USERS . "`.`usersAddressCity`,
					`" . TABLE_ACQUISATION_USERS . "`.`usersAuftragslistenCustomerID`,
					`" . TABLE_ACQUISATION_USERS . "`.`usersRights`,
					`" . TABLE_ACQUISATION_USERS . "`.`userMailDatas`,
					`" . TABLE_ACQUISATION_USERS . "`.`usersAreas`,
					`" . TABLE_ACQUISATION_USERS . "`.`usersActive`


				FROM `" . TABLE_ACQUISATION_USERS_DRIVERS_LOG . "`
			";
		#if($_POST["searchInterval"] == "DAILY"){
		if(1){
			$sql .= "
					LEFT JOIN `" . TABLE_ACQUISATION_USERS_DRIVERS_LOG_DETAILS . "`
					ON(`" . TABLE_ACQUISATION_USERS_DRIVERS_LOG . "`.`usersDriversLogsID` = `" . TABLE_ACQUISATION_USERS_DRIVERS_LOG_DETAILS . "`.`usersDriversLogsDetailsLogID` )
				";
		}
		$sql .= "
				LEFT JOIN `" . TABLE_ACQUISATION_USERS . "`
				ON(`" . TABLE_ACQUISATION_USERS_DRIVERS_LOG . "`.`usersDriversLogsUser` = `" . TABLE_ACQUISATION_USERS . "`.`usersID` )

				WHERE 1

				" . $where . "
			";
		if($_POST["searchInterval"] != "DAILY"){
			$sql .= "
					GROUP BY CONCAT(`interval`, `usersDriversLogsUser`)
				";
		}
		$sql .= "
				ORDER BY
					`" . TABLE_ACQUISATION_USERS_DRIVERS_LOG . "`.`usersDriversLogsDate` DESC,
					`" . TABLE_ACQUISATION_USERS . "`.`usersFirstName`,
					`" . TABLE_ACQUISATION_USERS . "`.`usersLastName`

			";

		if($_REQUEST["page"] == "" || $_REQUEST["page"] == 0) {
			$_REQUEST["page"] = 1;
		}

		if(MAX_ORDERS_PER_PAGE > 0) {
			#$sql .= " LIMIT " . (($_REQUEST["page"] - 1) * MAX_ORDERS_PER_PAGE) . ", " . MAX_ORDERS_PER_PAGE." ";
		}

		$rs = $dbConnection_ExternAcqisition->db_query($sql);

		$sql_totalRows = "SELECT FOUND_ROWS() AS `countRows`";
		$rs_totalRows = $dbConnection_ExternAcqisition->db_query($sql_totalRows);
		list($countRows) = mysqli_fetch_array($rs_totalRows);

		$pagesCount = ceil($countRows / MAX_ORDERS_PER_PAGE);

		$arrReportDatas = array();
		$arrReportDetailDatas = array();
		while($ds = mysqli_fetch_assoc($rs)){
			$arrIntervals[] = $ds["interval"];
			if($_POST["searchInterval"] == "DAILY"){
				if(!empty($arrReportDatas[$ds["interval"]][$ds["usersID"]]["usersDriversLogsDetailsZipcodeAreas"])){
					$arrReportDatas[$ds["interval"]][$ds["usersID"]]["usersDriversLogsDetailsZipcodeAreas"] .= '#';
				}
				$arrReportDatas[$ds["interval"]][$ds["usersID"]]["usersDriversLogsDetailsZipcodeAreas"] .= utf8_decode($ds["usersDriversLogsDetailsZipcode"]);
			}
			foreach(array_keys($ds) as $field){
				#$arrReportDatas[$ds["interval"]][$ds["usersDriversLogsID"]][$field] = $ds[$field];
				$arrReportDatas[$ds["interval"]][$ds["usersID"]][$field] = ($ds[$field]);
				#$arrReportDatas[$ds["usersDriversLogsID"]][$field] = $ds[$field];
				#$arrReportDetailDatas[$ds["usersDriversLogsID"]][$ds["usersDriversLogsDetailsID"]][$field] = $ds[$field];
				$arrReportDetailDatas[$ds["interval"]][$ds["usersID"]][$ds["usersDriversLogsDetailsID"]][$field] = ($ds[$field]);
			}
		}

		$pagesCount = ceil($countRows / MAX_SALESMEN_PER_PAGE);

		if($pagesCount > 1) {
			if($_REQUEST["page"] == "" || $_REQUEST["page"] == 0) {
				$_REQUEST["page"] = 1;
			}
		}
		else {
			$_REQUEST["page"] = 1;
		}
	}
?>
<?php
	require_once('inc/headerHTML.inc.php');
	$thisTitle = "Au&szlig;endienst-Tagesberichte";
	$headerHTML = preg_replace("/{###TITLE###}/", strip_tags($thisTitle), $headerHTML);
	echo $headerHTML;

	require_once(FILE_MENUE_TOP);
?>

<div id="menueSidebarToggleArea">
	<img src="layout/icons/iconToggle.png" class="buttonToggleSidebarMenue" width="10" height="10" alt="" title="Seitennavigation ein-/ausblenden" />
	<div id="menueSidebarToggleContent">
	<?php require_once(FILE_MENUE_SIDEBAR); ?>
	<div class="clear"></div>
	</div>
</div>
<div id="contentArea2">
	<div id="contentAreaElements">
		<h1><?php if(SHOW_PAGE_TITLE_ICONS) { echo '<img src="' . PATH_ICONS_MENUE_TITLES . 'acquisition.png" alt="" />'; } ?> <?php echo $thisTitle; ?></h1>
		<?php
			if($existsOnlineAquisition) {
				echo '<div align="right"><a href="' . PATH_ONLINE_ACQUISITION . '" target="_blank">Zur Online-Kundenerfassung</a></div>';
			}
		?>
		<div class="contentDisplay">
			<div id="searchFilterArea">
				<form name="formFilterSearch" method="post" action="<?php echo $_SERVER["PHP_SELF"]; ?>">
					<table border="0" cellpadding="0" cellspacing="0" class="searchFilterContent">
						<tr>
							<td>
								<label for="searchSalesman">Vertretername:</label>
								<select name="searchSalesmanOnlineID"  class="inputSelect_200">
									<option value="">Alle ADM</option>
									<?php
										if(!empty($arrAquisitionUsers)){
											foreach($arrAquisitionUsers as $thisKey => $thisValue){
												$thisUserRights = unserialize($thisValue["usersRights"]);
												if($thisUserRights["isADM"] == '1' && $thisValue["usersAuftragslistenCustomerID"] > 0 && $thisValue["usersActive"] == "1"){
													$selected = '';
													if($_REQUEST["searchSalesmanOnlineID"] == $thisValue["usersID"]){
														$selected = ' selected="selected" ';
													}
													echo '<option value="' . $thisValue["usersID"] . '" ' . $selected . ' >' . utf8_encode($thisValue["usersLastName"] . ', ' . $thisValue["usersFirstName"]) . '</option>';
												}
											}
										}
									?>
								</select>
							</td>
							<!--
							<td>
								<label for="searchWord">Suchbegriff:</label>
								<input type="text" name="searchWord" id="searchWord" class="inputField_100" value="<?php echo $_REQUEST["searchWord"]; ?>" />
							</td>
							-->
							<td>
								<label for="displayReportsDateFrom">von:</label>
								<input type="text" name="displayReportsDateFrom" id="displayReportsDateFrom" maxlength="2" class="inputField_70" readonly="readonly" value="<?php echo $fromDate; ?>" />
							</td>
							<td>
								<label for="displayReportsDateTo">bis:</label>
								<input type="text" name="displayReportsDateTo" id="displayReportsDateTo" maxlength="2" class="inputField_70" readonly="readonly" value="<?php echo $toDate; ?>" />
							</td>
							<td>
								<label for="searchInterval">Ansicht:</label>
								<select name="searchInterval" id="searchInterval" class="inputField_130">
									<option value="DAILY" <?php if($_POST["searchInterval"] == 'DAILY'){ echo ' selected="selected" '; } ?> >pro Tag</option>
									<option value="WEEK" <?php if($_POST["searchInterval"] == 'WEEK'){ echo ' selected="selected" '; } ?> >pro Woche</option>
									<option value="MONTH" <?php if($_POST["searchInterval"] == 'MONTH'){ echo ' selected="selected" '; } ?> >pro Monat</option>
									<option value="QUARTER" <?php if($_POST["searchInterval"] == 'QUARTER'){ echo ' selected="selected" '; } ?> >pro Vierteljahr</option>
									<option value="YEAR" <?php if($_POST["searchInterval"] == 'YEAR'){ echo ' selected="selected" '; } ?> >pro Jahr</option>
								 </select>
							</td>
							<td>
								<input type="hidden" name="editID" id="editID" value="" />
								<input type="submit" name="submitSearch" class="inputButton1" value="Suchen" />
							</td>
						</tr>
					</table>
					<input type="hidden" name="sortDirection" id="inputSortDirection" value="<?php echo $_REQUEST["sortDirection"]; ?>" />
					<input type="hidden" name="sortField" id="inputSortField" value="<?php echo $_REQUEST["sortField"]; ?>" />
					<input type="hidden" name="displayType" id="inputDisplayType" value="<?php echo $_REQUEST["displayType"]; ?>" />
					<input type="hidden" name="submitSearchForm" id="inputSubmitSearchForm" value="1" />
				</form>
			</div>
			<?php
				if(!empty($arrReportDatas)) {
					$rs = $dbConnection_ExternAcqisition->db_query($sql);

					echo '<table border="0" width="100%" cellspacing="0" cellpadding="0" class="displayOrders">';
					echo '<tr>';
					echo '<th>#</th>';
					echo '<th>Vertreter</th>';
					echo '<th>Stamm-Gebiete</th>';
					echo '<th>Ziele</th>';
					echo '<th>Ziele<br />pro KM</th>';
					echo '<th>Bestellungen</th>';
					echo '<th>Bestellungen<br />pro Kunde</th>';
					echo '<th>KM</th>';
					echo '<th>Bestellungen<br />pro KM</th>';
					echo '<th>PLZ-Gebiet</th>';
					echo '<th>Gespr&auml;chsdauer</th>';
					echo '<th>Spesen</th>';
					echo '<th>Tanken</th>';
					echo '<th style="width:50px;">Info</th>';
					echo '</tr>';

					$count = 0;
					$thisMarker = '';
					foreach($arrReportDatas as $thisReportDateKey => $thisReportDateValue){
						$thisDatasShow = true;
						if($_POST["searchInterval"] == "DAILY"){
							$thisDayNumber = date("w", strtotime($thisReportDateKey));
							if($thisDayNumber == 0 || $thisDayNumber == 6) {
								$thisDatasShow = false;
							}
						}
						if($thisDatasShow){
							$arrTemp = explode('#', $thisReportDateKey);
							$thisMarker = $thisReportDateKey;
							echo '<tr>';
							echo '<td colspan="14" class="tableRowTitle1">';

							if($_POST["searchInterval"] == "MONTH"){
								echo $arrTemp[0];
								echo ' - ';
								$thisMonth = getTimeNames($arrTemp[1], 'month', 'long');
								echo $thisMonth;
							}
							else if($_POST["searchInterval"] == "WEEK"){
								echo $arrTemp[0];
								echo ' - KW ';
								echo $arrTemp[1];
							}
							else if($_POST["searchInterval"] == "QUARTER"){
								echo $arrTemp[0];
								echo ' - ';
								echo $arrTemp[1];
							}
							else if($_POST["searchInterval"] == "DAILY"){
								echo formatDate($thisReportDateKey, "display");
								echo ' - ';
								$thisDayNumber = date("w", strtotime($thisReportDateKey));
								echo getTimeNames($thisDayNumber, "day", "long");
							}
							else if($_POST["searchInterval"] == "YEAR"){
								echo $arrTemp[0];
							}
							echo '</td>';
							echo '</tr>';
							$count = 0;

							$arrDisplaySalesmenDatas = array();
							if($_POST["searchSalesmanOnlineID"] != ""){
								$arrDisplaySalesmenDatas[$_POST["searchSalesmanOnlineID"]] = $arrSalesmenDatas[$_POST["searchSalesmanOnlineID"]];
							}
							else{
								$arrDisplaySalesmenDatas = $arrSalesmenDatas;
							}

							foreach($arrDisplaySalesmenDatas as $thisSalesmenDatasKey => $thisReportSalesmenDatasValue){

								$thisReportSalesmenRights = unserialize($thisReportSalesmenDatasValue["usersRights"]);

								#if($thisReportSalesmenRights["isADM"] == '1' && $thisValue["usersAuftragslistenCustomerID"] > 0 && $thisValue["usersActive"] == "1"){

								if($thisReportSalesmenRights["adminArea"] == '1' || ($thisReportSalesmenRights["isADM"] == '1' && $thisReportSalesmenDatasValue["usersAuftragslistenCustomerID"] > 0)){

									$thisReportDatasValue = $thisReportDateValue[$thisSalesmenDatasKey];

									if($count%2 == 0){ $rowClass = 'row0'; }
									else { $rowClass = 'row1'; }

									$thisStyle = '';
									if(empty($thisReportDatasValue)){
										$thisStyle = 'style="color:#F00;font-style:italic;"';
									}

									echo '<tr ' . $thisStyle . ' class="'.$rowClass.'">';

									echo '<td style="text-align:right;" title="' . $thisReportDatasValue["usersDriversLogsUser"] . '">';
									echo '<b>' . ($count + 1) . '.</b>';
									echo '</td>';

									echo '<td style="white-space:nowrap;">';
									#echo ($thisReportDatasValue["usersFirstName"]) . ' ' . ($thisReportDatasValue["usersLastName"]);
									echo ($thisReportSalesmenDatasValue["usersFirstName"]) . ' ' . ($thisReportSalesmenDatasValue["usersLastName"]);
									echo ' <span style="font-size:11px;">&bull; ' . ($thisReportSalesmenDatasValue["usersAddressZipcode"]) . ' ' . ($thisReportSalesmenDatasValue["usersAddressCity"]) . '</span>';											echo '</td>';
									echo '</td>';

									echo '<td style="white-space:nowrap;">';
									// BOF GET VISITED AREAS
									$arrUserVisitedAreas = explode(';', $thisReportDatasValue["usersDriversLogsTotalAreas"]);
									$arrUserVisitedAreas = array_unique($arrUserVisitedAreas);
									asort($arrUserVisitedAreas);
									// EOF GET VISITED AREAS

									// BOF GET ALL AREAS
									$arrUserAllAreas = explode(";", $thisReportSalesmenDatasValue["usersAreas"]);
									$arrUserAllAreas = array_unique($arrUserAllAreas);
									// EOF GET ALL AREAS

									// BOF COMPARE ALL AND VISITED AREAS
									if(!empty($arrUserAllAreas) && !empty($arrUserVisitedAreas)){
										foreach($arrUserAllAreas as $thisUserArea){
											$thisAreaStyle = "color:#007F1D";
											$thisAreaTitle = "im gew&auml;hlten Zeitraum besuchtes Gebiet";
											if(!in_array($thisUserArea, $arrUserVisitedAreas)){
												$thisAreaStyle = "color:#990000";
												$thisAreaTitle = "im gew&auml;hlten Zeitraum nicht besuchtes Gebiet";
											}
											echo ' &bull; ' . '<b><span title="' . $thisAreaTitle . '" style="' . $thisAreaStyle . '">' . $thisUserArea . '</span></b>';
										}
									}
									// EOF COMPARE ALL AND VISITED AREAS


									echo '</td>';
									if(!empty($thisReportDatasValue)){
										echo '<td style="text-align:right;">';
										echo ($thisReportDatasValue["usersDriversLogsTotalTargets"]);
										echo '</td>';

										echo '<td style="text-align:right;">';
										echo number_format(($thisReportDatasValue["usersDriversLogsTotalTargets"] / $thisReportDatasValue["usersDriversLogsTotalKilometers"] * 100), 2, ",", "") , " %";
										echo '</td>';

										echo '<td style="text-align:right;">';
										echo ($thisReportDatasValue["usersDriversLogsVisitTotalMadeOrders"]);
										echo '</td>';

										echo '<td style="text-align:right;">';
										echo number_format(($thisReportDatasValue["usersDriversLogsVisitTotalMadeOrders"] / $thisReportDatasValue["usersDriversLogsTotalTargets"] * 100), 2, ",", "") , " %";
										echo '</td>';

										echo '<td style="text-align:right;">';
										echo number_format($thisReportDatasValue["usersDriversLogsTotalKilometers"], 2, ",", ".");
										echo '</td>';

										echo '<td style="text-align:right;">';
										echo number_format(($thisReportDatasValue["usersDriversLogsVisitTotalMadeOrders"] / $thisReportDatasValue["usersDriversLogsTotalKilometers"] * 100), 2, ",", "") , " %";
										echo '</td>';

										echo '<td style="white-space:nowrap;">';
										$thisUsersDriversLogsTotalAreas = implode(" &bull; ", $arrUserVisitedAreas);
										echo " &bull; " . $thisUsersDriversLogsTotalAreas;
										echo '</td>';

										echo '<td style="text-align:right;">';
										$thisUsersDriversLogsDurance = $thisReportDatasValue["usersDriversLogsTotalDurance"];

										if(preg_match("/;/", $thisUsersDriversLogsDurance)){
											$arrTemp = explode(";", $thisUsersDriversLogsDurance);
											$thisDurance = 0;
											if(!empty($arrTemp)){
												foreach($arrTemp as $thisTempValue){
													$arrTemp2 = explode("#", $thisTempValue);
													$arrTemp3 = explode(":", $arrTemp2[1]);
													$thisDurance += ($arrTemp3[0] * 60) + $arrTemp3[1];
												}

											}

											$thisDuranceMinutes = $thisDurance%60;
											$thisDuranceHours = ($thisDurance - $thisDuranceMinutes) / 60;
											if($thisDuranceMinutes < 10){$thisDuranceMinutes = "0" . $thisDuranceMinutes; }
											if($thisDuranceHours < 10){$thisDuranceHours = "0" . $thisDuranceHours; }
											$thisUsersDriversLogsDurance = $thisDuranceHours . ':' . $thisDuranceMinutes . ':00';
										}
										else if(preg_match("/#/", $thisUsersDriversLogsDurance)){
											$arrTemp = explode("#", $thisUsersDriversLogsDurance);
											$thisUsersDriversLogsDurance = $arrTemp[1];
										}

										echo formatTime($thisUsersDriversLogsDurance, "display");
										echo '</td>';

										echo '<td style="text-align:right;">';
										echo number_format($thisReportDatasValue["usersDriversLogsTotalCharges"], 2, ",", ".");
										echo '</td>';

										echo '<td style="text-align:right;">';
										echo number_format($thisReportDatasValue["usersDriversLogsTotalRefueling"], 2, ",", ".");
										echo '</td>';

										echo '<td style="white-space:nowrap;">';

										echo '<span class="toolItem">';
										if($_POST["searchInterval"] == "DAILY"){
											echo '<img src="layout/icons/iconInfo.png" class="buttonReportInfo" width="14" height="14" alt="Details" title="Detailinformationen zu diesem Bericht ansehen">';
											echo '<div class="orderDetailsContent" style="display:none; visibility:visible;">';

												echo '<table border="0" cellspacing="0" cellpadding="0" class="">
													<tr>
														<td><b>Vertreter:</b></td>
														<td>' . ($thisReportDatasValue["usersFirstName"] . ' ' . $thisReportDatasValue["usersLastName"]) . '</td>

														<td class="legendSeperator"></td>
														<td><b>Datum der Fahrt:</b></td>
														<td>' . formatDate($thisReportDatasValue["usersDriversLogsDate"], "display") . '</td>

														<td class="legendSeperator"></td>
														<td><b>Strecke:</b></td>
														<td>' . number_format($thisReportDatasValue["usersDriversLogsTotalKilometers"], 2, ",", ".") . '</td>

														<td class="legendSeperator"></td>
														<td><b>Fahrtzeit:</b></td>
														<td>' . convertTime(convertTime($thisReportDatasValue["usersDriversLogsArrivalTime"], "minutes") - convertTime($thisReportDatasValue["usersDriversLogsDepartureTime"], "minutes"), "hours") . '</td>

														<td class="legendSeperator"></td>
														<td><b>Gespr&auml;chsdauer:</b></td>
														<td>' . formatTime($thisReportDatasValue["usersDriversLogsTotalDurance"], "display") . '</td>

														<td class="legendSeperator"></td>
														<td><b>Bestellungen:</b></td>
														<td>' . ($thisReportDatasValue["usersDriversLogsVisitTotalMadeOrders"]) . '</td>

														<td class="legendSeperator"></td>
														<td><b>Spesen:</b></td>
														<td>' . number_format($thisReportDatasValue["usersDriversLogsTotalCharges"], 2, ",", ".") . '</td>

														<td class="legendSeperator"></td>
														<td><b>Tankbetrag:</b></td>
														<td>' . number_format($thisReportDatasValue["usersDriversLogsTotalRefueling"], 2, ",", ".") . '</td>
														</tr>
												</table>
												<hr />
												';

												echo '<table border="0" width="100%" cellspacing="0" cellpadding="0" class="displayReports">';
												echo '<tr>';
												echo '
													<tr>
														<th rowspan="2" style="width:20px;">#</th>
														<th rowspan="2">Heimatort<br />bzw.<br />Zielort</th>
														<th rowspan="2">PLZ</th>
														<th rowspan="2">KNR</th>
														<th rowspan="2">Beschreibung / Firma</th>
														<th rowspan="2" class="spacer"> </th>
														<th rowspan="2">Zweck</th>
														<th colspan="2">Gespr&auml;chszeit</th>
														<th colspan="2">Kilometerstand [km]</th>
														<th rowspan="2">Strecke [km]</th>
														<th rowspan="2" class="spacer"> </th>
														<th rowspan="2">&Uuml;bernachtung<br />(max. 50,- &euro;)</th>
														<th rowspan="2">Tanken<br />(Belege)</th>
														<th rowspan="2" class="spacer"> </th>
														<th rowspan="2">Kontakt</th>
														<th rowspan="2">KZH von</th>
														<th style="width:70px;" rowspan="2">n. Besuch</th>
														<th rowspan="2">Bestellt?</th>
														<th rowspan="2">Bericht</th>
														<th rowspan="2"></th>
													</tr>
													<tr>
														<th>von</th>
														<th>bis</th>
														<th>Start</th>
														<th>Ziel</th>
													</tr>
												';
												echo '</tr>';

												$countDetails = 0;

												foreach($arrReportDetailDatas[$thisReportDateKey][$thisSalesmenDatasKey] as $thisReportDetailDatasKey => $thisReportDetailDatasValue){
													if($countDetails%2 == 0){ $rowClass = 'row0'; }
													else { $rowClass = 'row1'; }
													$thisStyle = '';

													echo '<tr ' . $thisStyle . ' class="'.$rowClass.'">';

													echo '<td style="text-align:right;" title="' . $thisReportDatasValue["usersDriversLogsUser"] . '">';
													echo '<b>' . ($countDetails + 1) . '.</b>';
													echo '</td>';

													echo '<td>';
													echo $thisReportDetailDatasValue["usersDriversLogsDetailsLocationEnd"];
													echo '</td>';

													echo '<td>';
													echo $thisReportDetailDatasValue["usersDriversLogsDetailsZipcode"];
													echo '</td>';

													echo '<td>';
													echo $thisReportDetailDatasValue["usersDriversLogsDetailsCustomerNumber"];
													echo '</td>';

													echo '<td>';
													echo $thisReportDetailDatasValue["usersDriversLogsDetailsCustomerName"];
													echo '</td>';

													echo '<td class="spacer">';
													echo ' ';
													echo '</td>';

													echo '<td>';
													echo $thisReportDetailDatasValue["usersDriversLogsDetailsIntention"];
													echo '</td>';

													echo '<td style="text-align:right;">';
													echo formatTime($thisReportDetailDatasValue["usersDriversLogsDetailsTimeStart"], "display");
													echo '</td>';

													echo '<td style="text-align:right;">';
													echo formatTime($thisReportDetailDatasValue["usersDriversLogsDetailsTimeEnd"], "display");
													echo '</td>';

													echo '<td style="text-align:right;">';
													echo number_format($thisReportDetailDatasValue["usersDriversLogsDetailsKilometersReadingStart"], 2, ",", ".");
													echo '</td>';

													echo '<td style="text-align:right;">';
													echo number_format($thisReportDetailDatasValue["usersDriversLogsDetailsKilometersReadingEnd"], 2, ",", ".");
													echo '</td>';

													echo '<td style="text-align:right;">';
													echo number_format(($thisReportDetailDatasValue["usersDriversLogsDetailsKilometersReadingEnd"] - $thisReportDetailDatasValue["usersDriversLogsDetailsKilometersReadingStart"]), 2, ",", ".");
													echo '</td>';

													echo '<td class="spacer">';
													echo ' ';
													echo '</td>';

													echo '<td style="text-align:right;">';
													echo number_format($thisReportDetailDatasValue["usersDriversLogsDetailsCharges"], 2, ",", ".");
													echo '</td>';

													echo '<td style="text-align:right;">';
													echo number_format($thisReportDetailDatasValue["usersDriversLogsDetailsRefueling"], 2, ",", ".");
													echo '</td>';

													echo '<td class="spacer">';
													echo ' ';
													echo '</td>';

													echo '<td>';

													echo $arrSalutationTypeDatas[$thisReportDetailDatasValue["usersDriversLogsDetailsVisitReportContactSalutation"]]["salutationTypesName"];
													echo ' ';
													echo $thisReportDetailDatasValue["usersDriversLogsDetailsVisitReportContactFirstName"];
													echo ' ';
													echo $thisReportDetailDatasValue["usersDriversLogsDetailsVisitReportContactSecondName"];
													echo ' ';
													echo $arrCustomerContactTypeDatas[$thisReportDetailDatasValue["usersDriversLogsDetailsVisitReportContactFunction"]]["salutationTypesName"];

													echo ' ';
													echo '</td>';

													echo '<td>';
													echo (utf8_decode($thisReportDetailDatasValue["usersDriversLogsDetailsVisitReportKzhDistributor"]));
													if($thisReportDetailDatasValue["usersDriversLogsDetailsVisitReportKzhDistributor"] != "" && $thisReportDetailDatasValue["usersDriversLogsDetailsVisitReportKzhDistributorAdd"] != ""){
														echo ', ';
													}
													if($thisReportDetailDatasValue["usersDriversLogsDetailsVisitReportKzhDistributorAdd"] != ""){
														echo $thisReportDetailDatasValue["usersDriversLogsDetailsVisitReportKzhDistributorAdd"];
													}

													echo '</td>';

													echo '<td>';
													echo formatDate($thisReportDetailDatasValue["usersDriversLogsDetailsVisitReportNextVisit"], "display");
													echo '</td>';

													echo '<td>';
													$thisUsersDriversLogsDetailsVisitReportMadeOrder= "nein";
													if($thisReportDetailDatasValue["usersDriversLogsDetailsVisitReportMadeOrder"] == 1){
														$thisUsersDriversLogsDetailsVisitReportMadeOrder = "ja";
													}
													echo $thisUsersDriversLogsDetailsVisitReportMadeOrder;
													echo '</td>';

													echo '<td>';
													echo ($thisReportDetailDatasValue["usersDriversLogsDetailsVisitReportNotice"]);
													echo '</td>';

													echo '<td>';
													echo '';
													echo '</td>';

													echo '</tr>';
													$countDetails++;
												}

												echo '</table>';

											echo '</div>';
										}
										else {
											echo '<img src="layout/spacer.gif" width="14" height="14" alt="" title="" />';
										}
										echo '</span>';

										echo '<span class="toolItem">';
										$arrTemp = explode("#", $thisReportDatasValue["usersDriversLogsDetailsZipcodeAreas"]);
										$arrTemp = array_unique($arrTemp);
										asort($arrTemp);
										$thisUsersDriversLogsDetailsZipcodeAreas = implode("_", $arrTemp);

										$thisRel = '';

										$thisRel .= $thisReportDatasValue["usersDriversLogsUser"];

										$thisRel .= '|';

										$thisRel .= ($thisReportSalesmenDatasValue["usersAddressCity"]) . ',';
										$thisRel .= ($thisReportSalesmenDatasValue["usersAddressZipcode"]) . ',';
										$thisRel .= ($thisReportSalesmenDatasValue["usersAddressStreetNumber"]) . ',';
										$thisRel .= ($thisReportSalesmenDatasValue["usersAddressStreet"]) . '';
										#$thisRel .= ('germany') . '';

										$thisRel .= '|';
										$thisRel .= $thisUsersDriversLogsDetailsZipcodeAreas;


										echo '<img src="layout/icons/iconGoogleMap.png" class="buttonLoadMap" width="14" height="14" rel="' . $thisRel . '" alt="Karte" title="Karte anzeigen (PLZ: ' . $thisUsersDriversLogsDetailsZipcodeAreas . ')" />';
										echo '</span>';

										echo '<span class="toolItem">';
										$arrThisUserMaildatas = unserialize($thisReportSalesmenDatasValue["userMailDatas"]);
										if($arrThisUserMaildatas["mailAddress"] != ""){
											echo '<a href="mailto:' . $arrThisUserMaildatas["mailAddress"] . '"><img src="layout/icons/iconMail.gif" width="16" height="" alt="Mail" title="Mail an den ADM schreiben" /></a>';
										}
										else {
											echo '<img src="layout/spacer.gif" width="14" height="14" alt="" title="" />';
										}
										echo '</span>';
										echo '</td>';
									}
									else {
										echo '<td colspan="2" style="white-space:nowrap">';
										echo "nicht vorhanden";
										echo '</td>';
										echo '<td></td>';
										echo '<td></td>';
										echo '<td></td>';
										echo '<td></td>';
										echo '<td></td>';
										echo '<td></td>';
										echo '<td></td>';
										echo '<td></td>';
										echo '<td>';

										echo '<span class="toolItem">';
										echo '<img src="layout/spacer.gif" width="14" height="14" alt="" title="" />';
										echo '</span>';

										echo '<span class="toolItem">';
										echo '<img src="layout/spacer.gif" width="14" height="14" alt="" title="" />';
										echo '</span>';

										echo '<span class="toolItem">';
										$arrThisUserMaildatas = unserialize($thisReportSalesmenDatasValue["userMailDatas"]);
										if($arrThisUserMaildatas["mailAddress"] != ""){
											echo '<a href="mailto:' . $arrThisUserMaildatas["mailAddress"] . '"><img src="layout/icons/iconMail.gif" width="16" height="" alt="Mail" title="Mail an den ADM schreiben" /></a>';
										}
										else {
											echo '<img src="layout/spacer.gif" width="14" height="14" alt="" title="" />';
										}
										echo '</span>';
										echo '</td>';
									}
									echo '</tr>';
									$count++;
								}
							}
						}
					}

					echo '</table>';
					displayMessages();
				}

				displayMessages();
			?>
		</div>
	</div>
</div>
<div class="clear"></div>

<script language="javascript" type="text/javascript">
	$(document).ready(function() {
		$('#searchSalesman').keyup(function () {
			// loadSearchSuggestions('#searchSalesman', '<?php echo $_SESSION["usersID"]; ?>');
		});
		$('#searchSalesman').keyup(function () {
			loadSuggestions('searchSalesmen', [{'triggerElement': '#searchSalesman', 'fieldName': '#searchSalesman', 'fieldNumber': '', 'fieldID': ''}], 1);
		});

		$('.buttonLoadMap').click(function() {
			var strZipcodes = $(this).attr('rel');
			<?php
				if($_POST["searchInterval"] == "DAILY"){ $thisMapType = "routes"; }
				else { $thisMapType = "allpoints"; }
			?>
			loadGoogleMapSalesmenReportAreas(strZipcodes, '<?php echo $thisMapType; ?>');
		});

		$('#searchSalesman').click(function () {
			$('#searchWord').val('');
		});
		$('#searchWord').click(function () {
			$('#searchSalesman').val('');
		});

		$('.buttonNotice').click(function () {
			loadNotice($(this));
		});
		$('.buttonReportInfo').css('cursor', 'pointer');
		$('.buttonReportInfo').click(function () {
			loadReportDetails($(this), '<?php echo BASEPATH; ?>', 'Details des Tagesberichtes');
		});
		setFocus('formFilterSearch', 'searchWord');
		colorRowMouseOver('.displayOrders tbody tr');

		$(function() {
			$.datepicker.setDefaults($.datepicker.regional[""]);
			$('#displayReportsDateFrom').datepicker($.datepicker.regional["de"]);
			$('#displayReportsDateTo').datepicker($.datepicker.regional["de"]);

			$('#displayReportsDateFrom').datepicker("option", "maxDate", "0" );
			$('#displayReportsDateTo').datepicker("option", "maxDate", "0" );
		});

		$('.buttonToggleSidebarMenue').click(function() {
			$('#menueSidebarToggleContent').toggle();
		});

		// toggleAreas();
	});
</script>

<?php require_once('inc/footerHTML.inc.php'); ?>