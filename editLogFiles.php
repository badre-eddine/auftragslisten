﻿<?php
	require_once('inc/requires.inc.php');

	if(!$arrGetUserRights["editLogFiles"]) {
		header('location: ' . PAGE_EXIT_LOCATION);
		exit;
	}

	if($_REQUEST["displayType"] != "") {
		$displayType = $_REQUEST["displayType"];
	}
	else {
		$displayType = "errors";
	}
	if($displayType == "errors") {
		$thisFile = PATH_LOGFILES_DB_QUERY_ERRORS;
	}
	else {
		$thisFile = PATH_LOGFILES_DB_QUERY;
	}

	$jswindowMessage = '';
	$warningMessage = '';
	$errorMessage = '';
	$successMessage = '';
	$infoMessage = '';

	$dbConnection = new DB_Connection();
	$db_open = $dbConnection->db_connect();

	if($_GET["deleteLogFile"]) {
		if(file_exists($thisFile)) {
			if(unlink($thisFile)) {
				$successMessage .= 'Die Datei wurde entfernt.' . '<br />';
			}
			else {
				$errorMessage .= 'Die Datei konnte nicht entfernt werden.' . '<br />';
			}
		}
		else {
			$warningMessage .= 'Die Datei existiert nicht.' . '<br />';
		}
		clearstatcache();
	}
?>
<?php
	require_once('inc/headerHTML.inc.php');
	$thisTitle = "DB-Logdatei";
	$headerHTML = preg_replace("/{###TITLE###}/", strip_tags($thisTitle), $headerHTML);
	echo $headerHTML;

	require_once(FILE_MENUE_TOP);
?>
<div id="mainArea">
	<div id="mainContent">
		<?php require_once(FILE_MENUE_SIDEBAR); ?>
		<div id="contentArea">
			<div id="contentAreaElements">
				<h1><?php if(SHOW_PAGE_TITLE_ICONS) { echo '<img src="' . PATH_ICONS_MENUE_TITLES . 'log.png" alt="" />'; } ?> <?php echo $thisTitle; ?></h1>

				<div id="searchFilterArea">
					<a href="<?php echo $_SERVER["PHP_SELF"]; ?>?displayType=queries" <?php if($displayType == "queries"){ echo ' class="active" '; } ?> >SQL-Abfragen</a>
					<a href="<?php echo $_SERVER["PHP_SELF"]; ?>?displayType=errors" <?php if($displayType == "errors"){ echo ' class="active" '; } ?> >SQL-Fehler</a>
				</div>

				<?php

					if(file_exists($thisFile)) {
						echo '<div style="text-align:right;"><a href="' . $_SERVER["PHP_SELF"] . '?deleteLogFile=1&displayType='.$displayType.'" onclick="return showWarning(\' Wollen Sie diese Datei endgültig entfernen??? \');">Log-Datei l&ouml;schen</a></div>';
						echo '<hr />';

						echo '<table border="0" cellpadding="0" cellspacing="0" width="100%" class="displayOrders">';
						echo '<tr>';
						echo '<th style="width:45px;text-align:right;">#</th>';
						echo '<th>Datetime</th>';
						echo '<th>Host</th>';
						echo '<th>Datei</th>';
						echo '<th>Mandator</th>';
						echo '<th>User-ID</th>';
						echo '<th>SQL_Error</th>';
						echo '</tr>';

						$fp = fopen($thisFile, 'r');
						$count = 0;
						while($contentLine = fgets($fp)) {
							$contentLine = strip_tags($contentLine);
							if($count%2 == 0){ $rowClass = 'row0'; }
							else { $rowClass = 'row1'; }

							echo '<tr class="'.$rowClass.'">';
							echo '<td style="text-align:right;"><b>' . ($count + 1) . '.</b></td>';
							echo '<td>';
							$contentLine = preg_replace("/\\t/", "</td><td>", $contentLine);
							// $contentLine = preg_replace("/'(.*)'/", "<b>`$1`</b>", $contentLine);

							echo $contentLine;
							echo '</td>';
							echo '</tr>';
							$count++;
							flush();
						}
						echo '</table>';
					}
					else {
						$warningMessage .= 'Die Datei existiert nicht.' . '<br />';
					}
					clearstatcache();
				?>
				<?php displayMessages(); ?>
			</div>
		</div>
		<div class="clear"></div>
	</div>
	<div class="clear"></div>
</div>

<script language="javascript" type="text/javascript">
	$(document).ready(function() {
		colorRowMouseOver('.displayOrders tbody tr');
	});
</script>

<?php require_once('inc/footerHTML.inc.php'); ?>