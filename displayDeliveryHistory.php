<?php
	require_once('inc/requires.inc.php');

	if(!$arrGetUserRights["displayTracking"]) {
		header('location: ' . PAGE_EXIT_LOCATION);
		exit;
	}

	$jswindowMessage = "";
	$warningMessage = '';
	$errorMessage = '';
	$successMessage = '';
	$infoMessage = '';

	$dbConnection = new DB_Connection();
	$db_open = $dbConnection->db_connect();

	$defaultSortField = "Paketnummer";
	$defaultSortDirection = "ASC";

	if($_REQUEST["sortField"] != "") {
		$sqlSortField = $_REQUEST["sortField"];
	}
	else {
		$sqlSortField = $defaultSortField;
	}

	if($_REQUEST["sortDirection"] != "") {
		$sqlSortDirection = $_REQUEST["sortDirection"];
	}
	else {
		$sqlSortDirection = $defaultSortDirection;
	}
	if($sqlSortDirection == "DESC") { $sqlSortDirection = "ASC"; }
	else { $sqlSortDirection = "DESC"; }

	$imgAttention = ' <img src="layout/icons/iconAttention.png" width="14" height="14" alt="!!" title="DPD hat keinen Zustell-Scan gemacht." />';

?>
<?php
	require_once('inc/headerHTML.inc.php');
	$thisTitle = "Paket-Historie";

	if($_POST["searchCustomerNumber"] != "") {
		$thisTitle .= " - Kundennummer ".$_POST["searchCustomerNumber"];
	}
	else if($_REQUEST["searchBoxDeliveryTracking"] != ""){
		if(strlen($_REQUEST["searchBoxDeliveryTracking"]) == 14){
			$_REQUEST["searchWord"] = $_REQUEST["searchBoxDeliveryTracking"];
		}
		else {
			$_REQUEST["searchWord"] = $_REQUEST["searchBoxDeliveryTracking"];
		}
	}
	else if($_REQUEST["searchDeliveryNumber"] != "") {
		$thisTitle .= " - Sendungsnummer " . $_REQUEST["searchDeliveryNumber"];
	}
	else if($_REQUEST["searchDeliveryDate"] != "") {
		$thisTitle .= " - Versanddatum " . $_REQUEST["searchDeliveryDate"];
	}
	else if($_REQUEST["searchDeliveryDateStart"] != "" && $_REQUEST["searchDeliveryDateEnd"] != "") {
		$thisTitle .= " - Versanddatum von " . $_REQUEST["searchDeliveryDateStart"] . ' bis ' . $_REQUEST["searchDeliveryDateEnd"];
	}
	else if($_POST["searchDeliveryStatus"] != "") {
		$thisSearchDeliveryStatus = 'Nicht zugestellt';
		if($_POST["searchDeliveryStatus"] == '1'){
			$thisSearchDeliveryStatus = 'Zugestellt';
		}
		if($_POST["searchDeliveryStatus"] == '2'){
			$thisSearchDeliveryStatus = 'Kein Zustell-Scan';
		}
		$thisTitle .= " - Sendestatus ".$thisSearchDeliveryStatus;
	}
	else if($_POST["searchCustomerName"] != "") {
		$thisTitle .= " - Kunde ".$_POST["searchCustomerName"];
	}
	else if($_POST["searchPLZ"] != "") {
		$thisTitle .= " - PLZ ".$_POST["searchPLZ"];
	}
	else if($_POST["searchSalesmanName"] != "") {
		$thisTitle .= " - Vertreter ".$_POST["searchSalesmanName"];
	}
	else if($_POST["searchWord"] != "") {
		$thisTitle .= " - Suchbegriff ".$_POST["searchWord"];
	}
	else if($_REQUEST["displayOrderStatus"] != "") {
		$thisTitle .= " - Status ".$arrOrderStatusTypeDatas[$_REQUEST["displayOrderStatus"]]["orderStatusTypesName"];
	}
	else if($_REQUEST["searchDeliveryPlace"] != "") {
		$thisTitle .= " - Versandort ".$_REQUEST["searchDeliveryPlace"];
	}
	$headerHTML = preg_replace("/{###TITLE###}/", strip_tags($thisTitle), $headerHTML);
	echo $headerHTML;

	require_once(FILE_MENUE_TOP);

	$sqlWhere = "";

	// BOF CREATE SQL FOR DISPLAY ORDERS
		if($_GET["loadAll"] == "true") {
			$sqlWhere = " AND `Zustellung` = '' ";
		}
		else if($_GET["loadSingle"] == "true") {
			// $sqlWhere = " AND `Zustellung` = '' ";
		}
		else if($_REQUEST["searchPLZ"] != "") {
			$sqlWhere = " AND `PLZ_1` LIKE '" . $_REQUEST["searchPLZ"] . "%' ";
		}
		else if($_REQUEST["searchCustomerNumber"] != "") {
			$sqlWhere = " AND `Ref_Adresse_1` = '" . $_REQUEST["searchCustomerNumber"] . "' ";
		}
		else if($_REQUEST["searchDeliveryNumber"] != "") {
			$sqlWhere = " AND `Paketnummer` = '" . $_REQUEST["searchDeliveryNumber"] . "' ";
		}
		else if($_REQUEST["searchDeliveryDate"] != "") {
			$sqlWhere = " AND `Versanddatum` = '" . formatDate($_REQUEST["searchDeliveryDate"], "store") . "' ";
		}
		else if($_REQUEST["searchDeliveryDateStart"] != "" && $_REQUEST["searchDeliveryDateEnd"] != "") {
			$sqlWhere = "
					AND (
						DATE_FORMAT(`Datum`, '%Y-%m-%d') >= '" . formatDate($_REQUEST["searchDeliveryDateStart"], "store") . "'
						AND
						DATE_FORMAT(`Datum`, '%Y-%m-%d') <= '" . formatDate($_REQUEST["searchDeliveryDateEnd"], "store") . "'
					)
				";
		}
		else if($_REQUEST["searchSenderName"] != "") {
			$sqlWhere = " AND `Firma_Absender` LIKE '%" . ($_REQUEST["searchSenderName"]) . "%' ";
		}
		else if($_REQUEST["searchDeliveryStatus"] != "") {
			if($_REQUEST["searchDeliveryStatus"] == '1'){
				$sqlWhere = " AND `Zustellung` != '' ";
			}
			else if($_REQUEST["searchDeliveryStatus"] == '2'){
				$sqlWhere = " AND `Zustellung` LIKE '%[Kein Zustell-Scan]%' ";
			}
			else {
				$sqlWhere = " AND `Zustellung` = '' ";
			}
		}
		else if($_REQUEST["searchCustomerName"] != "") {
			$sqlWhere = " AND (`Firma_Empfaenger` LIKE '%" . $_REQUEST["searchCustomerName"] . "%' OR `Name_1` LIKE '%" . $_REQUEST["searchCustomerName"] . "%' OR `Zu_Haenden` LIKE '%" . $_REQUEST["searchCustomerName"] . "%') ";
			// $sqlWhere = " AND MATCH(`ordersKundenName`) AGAINST ('" . $_REQUEST["searchCustomerName"] . "') ";
		}
		else if($_REQUEST["searchDeliveryPlace"] != "") {
			$sqlWhere = " AND `Paketnummer` LIKE '" . $arrDeliveryTrackingBasicNumbers[$_REQUEST["searchDeliveryPlace"]] . "%' ";
		}

		else if($_REQUEST["searchWord"] != "") {
			$sqlWhere = " AND (
							`PLZ_1` LIKE '" . $_REQUEST["searchWord"] . "%'
							OR
							`Ref_Adresse_1` LIKE '" . $_REQUEST["searchWord"] . "%'
							OR
							`Firma_Empfaenger` LIKE '%" . $_REQUEST["searchWord"] . "%'
							OR
							`Name_1` LIKE '%" . $_REQUEST["searchWord"] . "%'
							OR
							`Zu_Haenden` LIKE '%" . $_REQUEST["searchWord"] . "%'
							OR
							`Stadt_1` LIKE '%" . $_REQUEST["searchWord"] . "%'
							OR
							`Paketnummer` LIKE '%" . $_REQUEST["searchWord"] . "%'
							OR
							`Referenznr_1` LIKE '%" . $_REQUEST["searchWord"] . "%'
							OR
							`Zustellung` LIKE '%" . $_REQUEST["searchWord"] . "%'
						) ";
						// MATCH(`ordersVertreter`) AGAINST ('" . $_REQUEST["searchWord"] . "')
						// MATCH(`ordersKundenName`) AGAINST ('" . $_REQUEST["searchWord"] . "')
		}
		else if($_GET["searchBoxProduction"] != ""){
			$sqlWhere = " AND (
							`PLZ_1` LIKE '" . $_GET["searchBoxProduction"] . "%'
							OR
							`Referenznr_1` LIKE '" . $_GET["searchBoxProduction"] . "%'
							OR
							`ordersKundenName` LIKE '%" . $_GET["searchBoxProduction"] . "%'
							OR
							`ordersKommission` LIKE '%" . $_GET["searchBoxProduction"] . "%'
							OR
							`ordersOrt` LIKE '%" . $_GET["searchBoxProduction"] . "%'
							OR
							`ordersVertreter` LIKE '%" . $_GET["searchBoxProduction"] . "%'
							OR
							`ordersArtikelBezeichnung` LIKE '%" . $_GET["searchBoxProduction"] . "%'
							OR
							`ordersArtikelNummer` LIKE '%" . $_GET["searchBoxProduction"] . "%'
							OR
							`ordersDruckFarbe` LIKE '%" . $_GET["searchBoxProduction"] . "%'
						) ";
						// MATCH(`ordersVertreter`) AGAINST ('" . $_GET["searchBoxProduction"] . "')
						// MATCH(`ordersKundenName`) AGAINST ('" . $_GET["searchBoxProduction"] . "')
		}
		else {
			$sqlWhere = "";
		}

		$sqlSortField = "ORDER BY `" . $sqlSortField . "` ";


		$sql = "SELECT
					SQL_CALC_FOUND_ROWS

					`" . TABLE_DELIVERY_DATAS . "`.`Paketnummer`,
					`" . TABLE_DELIVERY_DATAS . "`.`Versanddatum`,
					DATE_FORMAT(DATE_ADD(`" . TABLE_DELIVERY_DATAS . "`.`Versanddatum`, INTERVAL 12 DAY), '%Y-%m-%d') AS `Checkdatum`,
					`" . TABLE_DELIVERY_DATAS . "`.`PLZ_1`,
			";

		if($sqlWhere != ""){
			$sql .= "
					`deliveryDatasStatusImport_1`.`PARCELNO`,
					`deliveryDatasStatusImport_1`.`CONSIGNEE_ZIP`,
				";
		}

		$sql .= "
					`" . TABLE_DELIVERY_DATAS . "`.`Datum`,
					`" . TABLE_DELIVERY_DATAS . "`.`Produkt_Service`,
					`" . TABLE_DELIVERY_DATAS . "`.`Gewicht`,
					`" . TABLE_DELIVERY_DATAS . "`.`Referenznr_1`,
					`" . TABLE_DELIVERY_DATAS . "`.`Referenznr_2`,
					`" . TABLE_DELIVERY_DATAS . "`.`Referenznr_3`,
					`" . TABLE_DELIVERY_DATAS . "`.`Referenznr_4`,
					`" . TABLE_DELIVERY_DATAS . "`.`ID_Sendung`,
					`" . TABLE_DELIVERY_DATAS . "`.`ID_Versender_Scan`,

					`" . TABLE_DELIVERY_DATAS . "`.`Zustelldatum`,
					`" . TABLE_DELIVERY_DATAS . "`.`Firma_Empfaenger`,
					`" . TABLE_DELIVERY_DATAS . "`.`Name_1`,
					`" . TABLE_DELIVERY_DATAS . "`.`Zu_Haenden`,
					`" . TABLE_DELIVERY_DATAS . "`.`Adresse_1`,
					`" . TABLE_DELIVERY_DATAS . "`.`Adresse_2`,
					`" . TABLE_DELIVERY_DATAS . "`.`Land_1`,
					`" . TABLE_DELIVERY_DATAS . "`.`Region_1`,

					`" . TABLE_DELIVERY_DATAS . "`.`Stadt_1`,
					`" . TABLE_DELIVERY_DATAS . "`.`Tel_1`,
					`" . TABLE_DELIVERY_DATAS . "`.`Email_1`,
					`" . TABLE_DELIVERY_DATAS . "`.`Ref_Adresse_1`,
					`" . TABLE_DELIVERY_DATAS . "`.`Anrede_1`,
					`" . TABLE_DELIVERY_DATAS . "`.`Fax_1`,
					`" . TABLE_DELIVERY_DATAS . "`.`Bemerkung_Adresse`,
					`" . TABLE_DELIVERY_DATAS . "`.`Firma_Absender`,
					`" . TABLE_DELIVERY_DATAS . "`.`Name_2`,
					`" . TABLE_DELIVERY_DATAS . "`.`Adresse_Strasse_1`,
					`" . TABLE_DELIVERY_DATAS . "`.`Adresse_Strasse_2`,
					`" . TABLE_DELIVERY_DATAS . "`.`Land_2`,
					`" . TABLE_DELIVERY_DATAS . "`.`Region_2`,
					`" . TABLE_DELIVERY_DATAS . "`.`PLZ_2`,
					`" . TABLE_DELIVERY_DATAS . "`.`Stadt_2`,
					`" . TABLE_DELIVERY_DATAS . "`.`Tel_2`,
					`" . TABLE_DELIVERY_DATAS . "`.`Email_2`,
					`" . TABLE_DELIVERY_DATAS . "`.`Ref_Adresse_2`,
					`" . TABLE_DELIVERY_DATAS . "`.`Anrede_2`,
					`" . TABLE_DELIVERY_DATAS . "`.`Fax_2`,
					`" . TABLE_DELIVERY_DATAS . "`.`COD_Verwendungszweck`,
					`" . TABLE_DELIVERY_DATAS . "`.`COD_Betrag`,
					`" . TABLE_DELIVERY_DATAS . "`.`COD_Waehrung`,
					`" . TABLE_DELIVERY_DATAS . "`.`COD_Inkasoart`,
					`" . TABLE_DELIVERY_DATAS . "`.`AI_Laenge_cm`,
					`" . TABLE_DELIVERY_DATAS . "`.`AI_Breite_cm`,
					`" . TABLE_DELIVERY_DATAS . "`.`AI_Hoehe_cm`,
					`" . TABLE_DELIVERY_DATAS . "`.`AI_enthaelt_Begleitdokumente`,
					`" . TABLE_DELIVERY_DATAS . "`.`AI_Pakentinhalt`,
					`" . TABLE_DELIVERY_DATAS . "`.`AI_Betrag`,
					`" . TABLE_DELIVERY_DATAS . "`.`AI_Waehrung`,
					`" . TABLE_DELIVERY_DATAS . "`.`AI_Steuernummer`,
					`" . TABLE_DELIVERY_DATAS . "`.`AI_Kommentar`,
					`" . TABLE_DELIVERY_DATAS . "`.`AI_RE_Firma`,
					`" . TABLE_DELIVERY_DATAS . "`.`AI_RE_Adresse_1_Strasse`,
					`" . TABLE_DELIVERY_DATAS . "`.`AI_RE_Land`,
					`" . TABLE_DELIVERY_DATAS . "`.`AI_RE_Region`,
					`" . TABLE_DELIVERY_DATAS . "`.`AI_RE_PLZ`,
					`" . TABLE_DELIVERY_DATAS . "`.`AI_RE_Stadt`,
					`" . TABLE_DELIVERY_DATAS . "`.`AI_BI_Kommentar_1`,
					`" . TABLE_DELIVERY_DATAS . "`.`AI_BI_Inhalt`,
					`" . TABLE_DELIVERY_DATAS . "`.`AI_BI_Begleitdokumente`,
					`" . TABLE_DELIVERY_DATAS . "`.`Proaktive_Benachrichtigung`,
					`" . TABLE_DELIVERY_DATAS . "`.`HV_Betrag`,
					`" . TABLE_DELIVERY_DATAS . "`.`HV_Waehrung`,
					`" . TABLE_DELIVERY_DATAS . "`.`HV_Wareninhalt`,
					`" . TABLE_DELIVERY_DATAS . "`.`ID_Check_Name`,
					`" . TABLE_DELIVERY_DATAS . "`.`ABT_Gebaeude`,
					`" . TABLE_DELIVERY_DATAS . "`.`ABT_Stockwerk`,
					`" . TABLE_DELIVERY_DATAS . "`.`ABT_Abteilung`,
					`" . TABLE_DELIVERY_DATAS . "`.`SSCC_NVE`,
					`" . TABLE_DELIVERY_DATAS . "`.`Bankleitzahl`,
					`" . TABLE_DELIVERY_DATAS . "`.`Name_der_Bank`,
					`" . TABLE_DELIVERY_DATAS . "`.`Kontonummer`,
					`" . TABLE_DELIVERY_DATAS . "`.`Kontoinhaber`,
					`" . TABLE_DELIVERY_DATAS . "`.`IBAN`,
					`" . TABLE_DELIVERY_DATAS . "`.`BIC`,
					`" . TABLE_DELIVERY_DATAS . "`.`Shop_Kundennummer`,
					`" . TABLE_DELIVERY_DATAS . "`.`Shop_Firma`,
					`" . TABLE_DELIVERY_DATAS . "`.`Shop_Name`,
					`" . TABLE_DELIVERY_DATAS . "`.`Shop_Addresse_1`,
					`" . TABLE_DELIVERY_DATAS . "`.`Shop_Addresse_2`,
					`" . TABLE_DELIVERY_DATAS . "`.`Shop_Land`,
					`" . TABLE_DELIVERY_DATAS . "`.`Shop_Region`,
					`" . TABLE_DELIVERY_DATAS . "`.`Shop_PLZ`,
					`" . TABLE_DELIVERY_DATAS . "`.`Shop_Ort`,
					`" . TABLE_DELIVERY_DATAS . "`.`Shop_Telefon`,
					`" . TABLE_DELIVERY_DATAS . "`.`Mandant_ID`,
					`" . TABLE_DELIVERY_DATAS . "`.`Mandant_Name`,
					`" . TABLE_DELIVERY_DATAS . "`.`Benutzername`,
					`" . TABLE_DELIVERY_DATAS . "`.`Journal_ID`,
					`" . TABLE_DELIVERY_DATAS . "`.`Zustellung`
				";

		if($sqlWhere != ""){
			$sql .= "
					,
					GROUP_CONCAT(
						DISTINCT CONCAT(
							IF(`deliveryDatasStatusImport_1`.`SCAN_CODE` IS NULL, '', `deliveryDatasStatusImport_1`.`SCAN_CODE`),
							',',
							IF(`deliveryDatasStatusImport_2`.`SCAN_CODE` IS NULL, '', `deliveryDatasStatusImport_2`.`SCAN_CODE`)
						)
					) AS `SCAN_CODES`
				";
		}

		$sql .= "
				FROM `" . TABLE_DELIVERY_DATAS . "`
			";

		if($sqlWhere != ""){
			$sql .= "
				LEFT JOIN `" . TABLE_DELIVERY_DATAS_STATUS_IMPORT . "` AS `deliveryDatasStatusImport_1`
				ON(
					`" . TABLE_DELIVERY_DATAS . "`.`Paketnummer` = `deliveryDatasStatusImport_1`.`PARCELNO`
					AND (
						`" . TABLE_DELIVERY_DATAS . "`.`PLZ_1` = `deliveryDatasStatusImport_1`.`CONSIGNEE_ZIP`
						OR
						`" . TABLE_DELIVERY_DATAS . "`.`Referenznr_1` = `deliveryDatasStatusImport_1`.`CUSTOMER_REFERENCE`
					)
				)

				LEFT JOIN `" . TABLE_DELIVERY_DATAS_STATUS_IMPORT . "` AS `deliveryDatasStatusImport_2`
				ON(
					`deliveryDatasStatusImport_1`.`PARCELNO` = `deliveryDatasStatusImport_2`.`PARCELNO`
					AND (
						`deliveryDatasStatusImport_1`.`SOURCE_FILE` = `deliveryDatasStatusImport_2`.`SOURCE_FILE`
						OR DATE_FORMAT(`deliveryDatasStatusImport_1`.`EVENT_DATE_TIME`, '%Y-%m-%d') = DATE_FORMAT(`deliveryDatasStatusImport_2`.`EVENT_DATE_TIME`, '%Y-%m-%d')

						OR DATE_ADD(DATE_FORMAT(`deliveryDatasStatusImport_1`.`EVENT_DATE_TIME`, '%Y-%m-%d'), INTERVAL " . DPD_CHECK_EVENT_DATE_INTERVAL . ") <= DATE_FORMAT(`deliveryDatasStatusImport_2`.`EVENT_DATE_TIME`, '%Y-%m-%d')
						OR DATE_ADD(DATE_FORMAT(`deliveryDatasStatusImport_2`.`EVENT_DATE_TIME`, '%Y-%m-%d'), INTERVAL " . DPD_CHECK_EVENT_DATE_INTERVAL . ") <= DATE_FORMAT(`deliveryDatasStatusImport_1`.`EVENT_DATE_TIME`, '%Y-%m-%d')
					)
					AND
					`deliveryDatasStatusImport_2`.`CONSIGNEE_ZIP` = ''
				)
			";
		}

		$sql .= "
				WHERE 1
					" . $sqlWhere . "
			";

		if($sqlWhere != ""){
			$sql .= "
					GROUP BY
						CONCAT(
							`" . TABLE_DELIVERY_DATAS . "`.`Paketnummer`,
							`" . TABLE_DELIVERY_DATAS . "`.`Versanddatum`
						)
				";
		}
		$sql .= "
				ORDER BY
					`" . TABLE_DELIVERY_DATAS . "`.`Versanddatum` DESC,
					`" . TABLE_DELIVERY_DATAS . "`.`Paketnummer` DESC
			";

		//  . $sqlWhere . " ". $sqlSortField . " ". $sqlSortDirection;

	// EOF CREATE SQL FOR DISPLAY ORDERS
?>
<div id="xmainArea">
	<div id="xmainContent">
		<div id="menueSidebarToggleArea">
			<img src="layout/icons/iconToggle.png" class="buttonToggleSidebarMenue" width="10" height="10" alt="" title="Seitennavigation ein-/ausblenden" />
			<div id="menueSidebarToggleContent">
			<?php require_once(FILE_MENUE_SIDEBAR); ?>
			<div class="clear"></div>
			</div>
		</div>
		<div id="contentArea2">
			<a name="top"></a>
			<div id="contentAreaElements">
				<h1><?php if(SHOW_PAGE_TITLE_ICONS) { echo '<img src="' . PATH_ICONS_MENUE_TITLES . 'tracking.png" alt="" />'; } ?> <?php echo $thisTitle; ?></h1>

				<p class="infoArea">Die DPD-Schnittstelle liefert - im Gegensatz zu DPD-ONline - nicht immer den Status "ZUGESTELLT", wenn der Fahrer keinen Zustell-Scan gemacht hat.<br />Bitte direkt bei DPD abfragen (Klick auf DPD-Nummer)!</p>

				<div id="displayStatusCountArea">
					<span class="displayStatusCount">
						<a href="javascript:void(0);" class="buttonLoadDeliveryDatas">DPD-Abfrage erneut starten</a>
					</span>
					<?php if($_COOKIE["isAdmin"] == '1'){ ?>
					&bull;
					<span class="displayStatusCount">
						<a href="<?php echo $_SERVER["PHP_SELF"]; ?>?loadAll=true">Alle Lieferungen abrufen</a>
					</span>
					&bull;
					<span class="displayStatusCount">
						<a href="<?php echo $_SERVER["PHP_SELF"]; ?>?loadSingle=true">Alle Lieferungen pro Seite abrufen</a>
					</span>
					<?php } ?>
				</div>

				<div id="searchFilterArea">
					<form name="formFilterSearch" method="post" action="<?php echo $_SERVER["PHP_SELF"]; ?>">
					<table border="0" cellpadding="0" cellspacing="0" class="searchFilterContent">
						<tr>
							<td>
								<label for="searchCustomerNumber">K-NR:</label>
								<input type="text" name="searchCustomerNumber" id="searchCustomerNumber" class="inputField_70" value="<?php echo $_REQUEST["searchCustomerNumber"]; ?>" />
							</td>
							<td>
								<label for="searchCustomerName">Empf&auml;nger:</label>
								<input type="text" name="searchCustomerName" id="searchCustomerName" class="inputField_100" value="<?php echo $_REQUEST["searchCustomerName"]; ?>" />
							</td>
							<td>
								<label for="searchPLZ">PLZ:</label>
								<input type="text" name="searchPLZ" id="searchPLZ" class="inputField_40" value="<?php echo $_REQUEST["searchPLZ"]; ?>" />
							</td>
							<td>
								<label for="searchDeliveryNumber">Paketnummer:</label>
								<input type="text" name="searchDeliveryNumber" id="searchDeliveryNumber" class="inputField_100" value="<?php echo $_REQUEST["searchDeliveryNumber"]; ?>" />
							</td>
							<td>
								<label for="searchDeliveryPlace">Vers.Ort:</label>
								<select name="searchDeliveryPlace" id="searchDeliveryPlace" class="inputField_40">
									<option value=""> </option>
									<option value="DE" <?php if($_REQUEST["searchDeliveryPlace"] == 'DE'){ echo ' selected="selected" '; } ?>>DE</option>
									<option value="TR" <?php if($_REQUEST["searchDeliveryPlace"] == 'TR'){ echo ' selected="selected" '; } ?>>TR</option>
								</select>
							</td>

							<td>
								<label for="searchDeliveryDate">Versand:</label>
								<input type="text" name="searchDeliveryDate" id="searchDeliveryDate" class="inputField_70" value="<?php echo $_REQUEST["searchDeliveryDate"]; ?>" />
							</td>

							<td>
								<label for="searchDeliveryDateStart">Transfer von:</label>
								<input type="text" name="searchDeliveryDateStart" id="searchDeliveryDateStart" class="inputField_70" value="<?php echo $_REQUEST["searchDeliveryDateStart"]; ?>" />
							</td>
							<td>
								<label for="searchDeliveryDateEnd">Transfer bis:</label>
								<input type="text" name="searchDeliveryDateEnd" id="searchDeliveryDateEnd" class="inputField_70" value="<?php echo $_REQUEST["searchDeliveryDateEnd"]; ?>" />
							</td>

							<td>
								<label for="searchDeliveryStatus">Status:</label>
								<select name="searchDeliveryStatus" id="searchDeliveryStatus" class="inputField_120" >
									<option value="">ALLE</option>
									<option value="1" <?php if($_REQUEST["searchDeliveryStatus"] == '1'){ echo ' selected="selected" '; } ?> >Zugestellt</option>
									<option value="0" <?php if($_REQUEST["searchDeliveryStatus"] == '0'){ echo ' selected="selected" '; } ?> >Nicht zugestellt</option>
									<option value="2" <?php if($_REQUEST["searchDeliveryStatus"] == '2'){ echo ' selected="selected" '; } ?> >Kein Zustell-Scan</option>
								</select>
							</td>
							<td>
								<label for="searchSenderName">Absender:</label>
								<input type="text" name="searchSenderName" id="searchSenderName" class="inputField_100" value="<?php echo $_REQUEST["searchSenderName"]; ?>" />
							</td>
							<td>
								<label for="searchWord">Suchbegriff:</label>
								<input type="text" name="searchWord" id="searchWord" class="inputField_70" value="<?php echo $_REQUEST["searchWord"]; ?>" />
							</td>
							<td>
								<input type="hidden" name="editID" id="editID" value="" />
								<input type="submit" name="submitSearch" class="inputButton0" value="Suchen" />
							</td>
						</tr>
					</table>
					<input type="hidden" name="sortDirection" id="inputSortDirection" value="<?php echo $_REQUEST["sortDirection"]; ?>" />
					<input type="hidden" name="sortRow" id="inputSortRow" value="<?php echo $_REQUEST["sortRow"]; ?>" />
					<input type="hidden" name="sortField" id="inputSortField" value="<?php echo $_REQUEST["sortField"]; ?>" />
					<input type="hidden" name="displayType" id="inputDisplayType" value="<?php echo $_REQUEST["displayType"]; ?>" />
					<input type="hidden" name="submitSearchForm" id="inputSubmitSearchForm" value="1" />
					<input type="hidden" name="displayOrderStatus" value="<?php echo $_REQUEST["displayOrderStatus"]; ?>" />
					</form>
				</div>

				<?php displayMessages(); ?>

				<?php

					// BOF GET COUNT ALL ROWS
						$sql_getAllRows = "
								SELECT

									COUNT(`" . TABLE_DELIVERY_DATAS  . "`.`Paketnummer`) AS `countAllRows`

								FROM `" . TABLE_DELIVERY_DATAS  . "`

								WHERE 1
									" . $sqlWhere . "
							";

						$rs_getAllRows = $dbConnection->db_query($sql_getAllRows);
						list(
							$countAllRows
						) = mysqli_fetch_array($rs_getAllRows);						
					// EOF GET COUNT ALL ROWS
				
					if($_REQUEST["page"] == "" || $_REQUEST["page"] == 0) {
						$_REQUEST["page"] = 1;
					}

					if($_GET["loadAll"] != "true") {
						if(MAX_DELIVERIES_PER_PAGE > 0) {
							$sql .= " LIMIT " . (($_REQUEST["page"] - 1) * MAX_DELIVERIES_PER_PAGE) . ", " . MAX_DELIVERIES_PER_PAGE." ";
						}
					}
					else {

					}
					$rs = $dbConnection->db_query($sql, $db_open);

					// BOF GET ALL ROWS
						#$sql_totalRows = "SELECT FOUND_ROWS() AS `countRows`";
						#$rs_totalRows = $dbConnection->db_query($sql_totalRows);
						#list($countRows) = mysqli_fetch_array($rs_totalRows);
					// EOF GET ALL ROWS
					
					$countRows = $countAllRows;

					$pagesCount = ceil($countRows / MAX_DELIVERIES_PER_PAGE);					

					echo '<p class="infoArea">Anzahl Zeilen: ' . $countRows . '</p>';

					if($countRows > 0) {
				?>
				<?php
					if($pagesCount > 1 && $_GET["loadAll"] != "true") {
						include(FILE_MENUE_PAGES);
					}
				?>
				<?php if($sqlWhere != ""){ ?>
				<p class="infoArea" style="line-height:20px;">
					<span style="padding-right:20px;white-space:nowrap;">&bull; <span class="row12">blaue Zeile</span> : Aufkleber von DPD nicht eingescant bzw. noch nicht verschickt.</span>
					<span style="padding-right:20px;white-space:nowrap;">&bull; <span class="row11">durchgestrichene Zeile</span> : Aufkleber nicht verwendet</span>
				</p>
				<span class="buttonDpdScancodesLegend"></span>
				<?php } ?>


				<?php
					if(!empty($arrDpdScanCodes)){
						echo '<div style="display:none">';
						foreach($arrDpdScanCodes as $thisScanCodeKey => $thisScanCodeValue){
							echo '<span style="padding-right:20px;white-space:nowrap;">&bull; ' . $thisScanCodeKey . ': ' . $thisScanCodeValue . '</span> ';
						}
						echo '</div>';
					}
				?>

				<div class="contentDisplay">
					<table border="0" cellpadding="0" cellspacing="0" width="100%" class="displayOrders">
						<colgroup>
							<col />
							<col />
							<col />
							<!--
							<col />
							<col />
							-->
							<col />
							<col />
							<col />
							<col />
							<col />
							<col />
							<col />
							<col />
							<!--
							<col />
							-->
							<col />
							<col />

							<col />
							<col />
							<col />
							<col />

							<col />
							<col />
						</colgroup>

						<thead>
							<tr>
								<th style="width:45px;text-align:right;">#</th>
								<th class="sortColumn">Paketnummer</th>
								<th class="sortColumn">K-Nr</th>
								<!--
								<th class="sortColumn">Typ</th>
								<th class="sortColumn">Gewicht</th>
								-->
								<th class="sortColumn">Referenz</th>
								<th class="sortColumn">Versanddatum</th>
								<th class="sortColumn">Scan</th>
								<th>Zustelldatum</th>
								<th>Zustellung an</th>
								<th>Empf&auml;nger</th>
								<th>Adresse</th>
								<th>Land</th>
								<!--
								<th>Region</th>
								-->
								<th>PLZ</th>
								<th>Stadt</th>

								<th>Paket</th>
								<th>Verws-Zweck</th>
								<th>Betrag</th>
								<th>Inkassoart</th>

								<th colspan="2">Absender</th>
								<th>Benutzer</th>
							</tr>
						</thead>

						<tbody>
					<?php

						$count = 0;
						$arrUpdateDatas = array();

						while($ds = mysqli_fetch_assoc($rs)) {

							if($count%2 == 0){ $rowClass = 'row0'; }
							else { $rowClass = 'row1'; }

							$thisDeliveryData = '';
							$thisGetFileContent = '';

							$ds["SCAN_CODES"] = preg_replace("/[,]{1,}$/", "", $ds["SCAN_CODES"]);
							if(array_key_exists("SCAN_CODES", $ds) && $ds["Zustellung"] == '' && $ds["SCAN_CODES"] == ''){
								$thisCheckDate = checkActiveParcelNumbers($ds["Paketnummer"], $ds["Versanddatum"]);
								if(date("Y-m-d") > $thisCheckDate){
									$rowClass = 'row11';
								}
								else {
									$rowClass = 'row12';
								}
							}

							if($ds["Zustellung"] != ''){
								$rowClass = 'row3';
								if(preg_match('/#/', $ds["Zustellung"])){
									$thisDeliveryData = formatLoadedDeliveryDatas($ds["Zustellung"], '#');
								}
								else if(preg_match('/|/', $ds["Zustellung"])){
									$thisDeliveryData = formatLoadedDeliveryDatas($ds["Zustellung"], '|');
								}

								// BOF LOAD EXTERNAL DELIVERY DATAS
									if(preg_match("/\[Kein Zustell-Scan\]/", $ds["Zustellung"])){
										$rowClass = 'row2';
										if(preg_match('/#/', $ds["Zustellung"])){
											$arrTempDeliveryData = explode('#', $ds["Zustellung"]);
										}
										else if(preg_match('/|/', $ds["Zustellung"])){
											$arrTempDeliveryData = explode('|', $ds["Zustellung"]);
										}
										$deliveryDate = $arrTempDeliveryData[0];
										$deliveryTimestamp = strtotime($deliveryDate);
										$timestampInterval = 21 * (24 * 60 * 60); // 21 Tage zurück
										$todayTimestamp = time();

										if(($todayTimestamp - $deliveryTimestamp) <= $timestampInterval){
											// $thisDeliveryData = '<span class="loadThisDeliveryData" id="loadDeliveryDate_' . $ds["Paketnummer"] . '_' . $count . '" ref="' . $ds["Paketnummer"] . ':' . $ds["Versanddatum"] . ':' . $count . '">' . formatDate($arrTempDeliveryData[0], 'display') . '</span>#<span id="loadDeliveryName_' . $ds["Paketnummer"] . '_' . $count . '">' . $arrTempDeliveryData[1] . ' ' . $imgAttention . '</span>';

											#$thisRef = '' . $ds["Paketnummer"] . ':' . $ds["Versanddatum"] . ':' . $count . ':' . $ds["PLZ_1"] . ':' . $ds["Referenznr_1"];
											$thisRef = '' . $ds["Paketnummer"] . ':' . $ds["Versanddatum"] . ':' . $count . ':' . $ds["PLZ_1"] . ':' . $ds["Ref_Adresse_1"];
											$thisDeliveryData = '<span class="loadThisDeliveryData" id="loadDeliveryDate_' . $ds["Paketnummer"] . '_' . $count . '" ref="' . $thisRef . '">' . formatDate($arrTempDeliveryData[0], 'display') . '</span>#<span id="loadDeliveryName_' . $ds["Paketnummer"] . '_' . $count . '">' . $arrTempDeliveryData[1] . ' ' . $imgAttention . '</span>';
										}
										else {
											$thisDeliveryData .= ' ' . $imgAttention;
										}
									}
								// EOF LOAD EXTERNAL DELIVERY DATAS
							}
							if($ds["Zustellung"] == ''){
								// if($ds["Versanddatum"] < date('Y-m-d', mktime(0, 0, 0, date('m'), (date('d') - 2), date('Y')))) {
								$thisDeliveryData = '-#-';
								// BOF LOAD EXTERNAL DELIVERY DATAS
									// $thisDeliveryData = '<span class="loadThisDeliveryData" id="loadDeliveryDate_' . $ds["Paketnummer"] . '_' . $count . '" ref="' . $ds["Paketnummer"] . ':' . $ds["Versanddatum"] . ':' . $count . '">-</span>#<span id="loadDeliveryName_' . $ds["Paketnummer"] . '_' . $count . '">-</span>';

									$thisRef = '' . $ds["Paketnummer"] . ':' . $ds["Versanddatum"] . ':' . $count . ':' . $ds["PLZ_1"];
									$thisDeliveryData = '<span class="loadThisDeliveryData" id="loadDeliveryDate_' . $ds["Paketnummer"] . '_' . $count . '" ref="' . $thisRef . '">-</span>#<span id="loadDeliveryName_' . $ds["Paketnummer"] . '_' . $count . '">-</span>';
								// EOF LOAD EXTERNAL DELIVERY DATAS
							}

							echo '<tr class="'.$rowClass.'">';

								echo '<td style="text-align:right;"><b>' . ($count + 1) .'.</b></td>';

								echo '<td style="white-space:nowrap;">';
								echo '<a href="' . PAGE_SEARCH_TRACKING . '?searchTrackingNumber=' . $ds["Paketnummer"] . '" title="Sendungsverfolgung anzeigen">' . $ds["Paketnummer"] . '</a>';
								echo getPrintProductionPlace($ds["Paketnummer"], "flag");
								echo '</td>';

								echo '<td style="white-space:nowrap;">';
								if($arrGetUserRights["editCustomers"]){
									echo '<a href="' . PAGE_EDIT_CUSTOMER . '?editCustomerNumber=' . $ds["Ref_Adresse_1"] . '" title="Kundendaten anzeigen">';
								}
								echo '<b>' . $ds["Ref_Adresse_1"] . '</b>';
								if($arrGetUserRights["editCustomers"]){
									echo '</a>';
								}
								echo '</td>';
								#echo '<td>' . $ds["Produkt_Service"] .'</td>';
								#echo '<td>' . $ds["Gewicht"] .'</td>';

								echo '<td style="white-space:nowrap;">';
								echo preg_replace("/\//", " &bull; ", $ds["Referenznr_1"]);
								if($ds["Referenznr_2"] != ''){
									echo ' / ';
									echo preg_replace("/\//", " &bull; ", $ds["Referenznr_2"]);
								}
								if($ds["Referenznr_3"] != ''){
									echo ' / ';
									echo preg_replace("/\//", " &bull; ", $ds["Referenznr_3"]);
								}
								if($ds["Referenznr_4"] != ''){
									echo ' / ';
									echo preg_replace("/\//", " &bull; ", $ds["Referenznr_4"]);
								}
								echo '</td>';

								echo '<td>' . formatDate($ds["Versanddatum"], 'display') .'</td>';

								echo '<td style="white-space:nowrap;">';

								// $arrDpdScanCodes
								if($ds["SCAN_CODES"] != ''){
									$arrThisScanCodes = explode(",", $ds["SCAN_CODES"]);
									$arrThisScanCodes = array_filter($arrThisScanCodes);
									sort($arrThisScanCodes);
									$thisLastScanCode = implode("", array_slice($arrThisScanCodes, -1, 1, true));
									echo '<span style="font-size:11px;" title="' . $arrDpdScanCodes[$thisLastScanCode] . ' [' . $thisLastScanCode . ']' . '">';
									echo $thisLastScanCode;
									echo '</span>';
								}
								echo '</td>';

								echo '<td style="white-space:nowrap;width:200px;">';

								if(preg_match("/#/", $thisDeliveryData)){
									if(!preg_match('/class="loadThisDeliveryData"/', $thisDeliveryData)){
										$pattern = '([0-9]{4})-([0-9]{2})-([0-9]{2}) ([0-9]{2}:[0-9]{2})';
										$replace = '$3.$2.$1 $4 Uhr';
										$thisDeliveryData = preg_replace("/".$pattern."/", $replace, $thisDeliveryData);

										$pattern = '([0-9]{4})-([0-9]{2})-([0-9]{2})';
										$replace = '$3.$2.$1';
										$thisDeliveryData = preg_replace("/".$pattern."/", $replace, $thisDeliveryData);
									}
									echo preg_replace('/#/', '</td><td style="white-space:nowrap;width:200px;">', ($thisDeliveryData));
								}
								echo '</td>';

								echo '<td style="white-space:nowrap;">';
								echo htmlentities(($ds["Firma_Empfaenger"]));
								if($ds["Firma_Empfaenger"] != $ds["Name_1"]){
									echo ' / ' . $ds["Name_1"];
								}
								if($ds["Zu_Haenden"] != '' && $ds["Zu_Haenden"] != $ds["Firma_Empfaenger"]) {
									echo 'z. Hd. ' . htmlentities(($ds["Zu_Haenden"]));
								}
								echo '</td>';

								echo '<td style="white-space:nowrap;">' . htmlentities(($ds["Adresse_1"])) .' ' . htmlentities(($ds["Adresse_2"])) .'</td>';

								echo '<td>' . $ds["Land_1"] .'</td>';
								#echo '<td>' . $ds["Region_1"] .'</td>';

								echo '<td style="white-space:nowrap;">' . $ds["PLZ_1"] .'</td>';

								echo '<td style="white-space:nowrap;">' . htmlentities(($ds["Stadt_1"])) .'</td>';

								echo '<td style="white-space:nowrap;">' . $ds["Produkt_Service"] .'</td>';
								echo '<td style="white-space:nowrap;">' . $ds["COD_Verwendungszweck"] .'</td>';
								echo '<td style="white-space:nowrap;">' . $ds["COD_Betrag"] . ' ' . $ds["COD_Waehrung"] .'</td>';
								echo '<td style="white-space:nowrap;">' . $ds["COD_Inkasoart"] .'</td>';

								echo '<td style="white-space:nowrap;">';
								echo '<b>' . $ds["Ref_Adresse_2"] . '</b>';
								echo '</td>';

								echo '<td style="white-space:nowrap;">';
								if($ds["Firma_Absender"] != ''){
									echo $ds["Firma_Absender"];
								}
								else {
									echo 'BURHAN CTR';
								}
								echo '</td>';

								echo '<td style="white-space:nowrap;">' . htmlentities(($ds["Benutzername"])) .'</td>';
							echo '</tr>';
							$count++;
						}
					?>
						</tbody>
					</table>
				</div>
				<?php
					if($pagesCount > 1 && $_GET["loadAll"] != "true") {
						include(FILE_MENUE_PAGES);
					}
				?>
				<div style="text-align:right;" class="menueToTop">
					<a href="#top">nach oben</a>
				</div>
				<?php
					}
					else {
						echo '<p class="warningArea">Es wurden keine Daten gefunden!</p>';
					}
				?>
				<div class="clear"></div>
			</div>
			<div class="clear"></div>
		</div>
	</div>
</div>
<script language="javascript" type="text/javascript">
	<!--
	/* <![CDATA[ */
		$(document).ready(function() {

			setFocus('formFilterSearch', 'searchWord');
			colorRowMouseOver('.displayOrders tbody tr');

			$('#searchCustomerNumber').keyup(function () {
				// loadSuggestions('searchCustomerNumber', [{'triggerElement': '#searchCustomerNumber', 'fieldNumber': '#searchCustomerNumber', 'fieldName': '#searchCustomerName', 'fieldCity': '', 'fieldZipCode': ''}], 1);
			});
			$('#searchCustomerName').keyup(function () {
				// loadSuggestions('searchCustomerName', [{'triggerElement': '#searchCustomerName', 'fieldNumber': '#searchCustomerNumber', 'fieldName': '#searchCustomerName', 'fieldCity': '', 'fieldZipCode': ''}], 1);
			});
			$('#searchSalesmanName').keyup(function () {
				// loadSuggestions('searchSalesmanName', [{'triggerElement': '#searchSalesmanName', 'fieldName': '#searchSalesmanName'}], 1);
			});
			$('#searchPLZ').keyup(function () {
				// loadSuggestions('searchPLZ', [{'triggerElement': '#searchPLZ', 'fieldZipCode': '#searchPLZ'}], 1);
			});

			$('.buttonNotice').click(function () {
				loadNotice($(this));
			});

			// BOF LOAD EXTERNAL DELIVERY DATAS
				function loadDeliveryDatas(trackingID, trackingDate, trackingRow, trackingZipcode, thisTrackingCustomerNumber){
					var loadURL = 'inc/loadDeliveryDatas.inc.php?searchTrackingID=' + trackingID + '&searchTrackingDate=' + trackingDate + '&searchTrackingZipcode=' + trackingZipcode + '&searchTrackingCustomerNumber=' + thisTrackingCustomerNumber;

					$('#loadDeliveryDate_' + trackingID + '_' + trackingRow).html('<img class="loader" src="layout/ajax-loader.gif" alt="" height="10" width="10" \/> <span style="font-style:italic;font-size:10px;color:#FF0000;">Daten-Abruf von DPD<\/span>');
					$('#loadDeliveryName_' + trackingID + '_' + trackingRow).html('<img class="loader" src="layout/ajax-loader.gif" alt="" height="10" width="10" \/> <span style="font-style:italic;font-size:10px;color:#FF0000;">Daten-Abruf von DPD<\/span>');

					$.get(loadURL, function(result) {
						var arrResult = result.split('#');
						$('#loadDeliveryDate_' + trackingID + '_' + trackingRow).html('');
						$('#loadDeliveryName_' + trackingID + '_' + trackingRow).html('');
						var arrResult = result.split('#');
						var imgAttention = '';

						if(result.length > 10){
							var thisClass = 'row3';
							if(arrResult[1] == '[Kein Zustell-Scan]'){
								thisClass = 'row2';
								imgAttention = '<?php echo $imgAttention; ?>';
							}
							$('#loadDeliveryDate_' + trackingID + '_' + trackingRow).parent().attr('class', thisClass);
							$('#loadDeliveryName_' + trackingID + '_' + trackingRow).parent().attr('class', thisClass);
						}

						<?php if($_COOKIE["isAdmin"] == "1") { ?>
						var thisDate = arrResult[0];
						var arrDate = thisDate.split('-');
						var formattedDate = arrDate[2] + '.' + arrDate[1] + '.' + arrDate[0];
						<?php } ?>

						$('#loadDeliveryDate_' + trackingID + '_' + trackingRow).html(arrResult[0] + ' ' + imgAttention);
						$('#loadDeliveryName_' + trackingID + '_' + trackingRow).html(arrResult[1] + ' ' + imgAttention);

						// $('#loadDeliveryDate_' + trackingID + '_' + trackingRow).attr('class', 'loadedThisDeliveryData');
						$('#loadDeliveryDate_' + trackingID + '_' + trackingRow).removeAttr('class');

						// BOF SHOW INFO WINDOW
							/*
							<?php if($_COOKIE["isAdmin"] == "1" && $_GET["loadAll"] == "true") { ?>
							$('#loadNoticeArea').remove();
							var countUpdatedElements = $('td.row3').length / arrResult.length;
							var countAllElements = $('tr').length;
							var countNonUpdatedElements = countAllElements - countUpdatedElements;

							var content = '';
							var contentText = '';
							contentText += 'Anzahl aller Sendungen: ' + countAllElements + '<br \/>';
							contentText += 'Anzahl aktualisierter Zustellungen: ' + countUpdatedElements + '<br \/>';
							contentText += 'Anzahl nicht aktualisierter Zustellungen: ' + countNonUpdatedElements + '<br \/>';

							var contentHeader = 'DPD-UPDATE-Status';
							var arrWindowButtons = loadWindowButtons();

							content = '<div class="noticeBoxHeader">' + contentHeader + '<\/div>';
							content += '<div class="noticeBoxClose">' + arrWindowButtons['small'] + ' ' + arrWindowButtons['move'] + ' ' + arrWindowButtons['close'] + '<\/div>';
							content += '<div class="noticeBoxContents">' + contentText + '<\/div>';
							content += '<div class="noticeBoxFooter">' + '' + '<\/div>';
							content = '<div class="loadNoticeContent">' + content + '<\/div>';
							$('body').append('<div id="loadNoticeArea" class="loadedWindow"><\/div>');
							$('#loadNoticeArea').html(content);
							<?php } ?>
							*/
						// EOF SHOW INFO WINDOW

						<?php if($_GET["loadAll"] == "true" || $_GET["loadSingle"] == "true") { ?>
						var scroll_y = 20;
						var getRowHeight = $('#loadDeliveryDate_' + trackingID + '_' + trackingRow).parent().parent().innerHeight();
						scroll_y = getRowHeight;
						window.scrollBy(0, scroll_y);
						<?php } ?>
						startLoadDeliveryDatas();
					});
				}

				function startLoadDeliveryDatas(){
					if($('.loadThisDeliveryData').length > 0){
						if($('.loadThisDeliveryData').first().attr('ref')){
							var thisTrackingData = $('.loadThisDeliveryData').first().attr('ref');
							var arrThisTrackingData = thisTrackingData.split(':');
							var thisTrackingID = arrThisTrackingData[0];
							var thisTrackingDate = arrThisTrackingData[1];
							var thisTrackingRow = arrThisTrackingData[2];
							var thisTrackingZipcode = arrThisTrackingData[3];
							var thisTrackingCustomerNumber = arrThisTrackingData[4];

							loadDeliveryDatas(thisTrackingID, thisTrackingDate, thisTrackingRow, thisTrackingZipcode, thisTrackingCustomerNumber);
						}
					}
					else {
						<?php
							if($_REQUEST["loadSingle"] == 'true'){
								if($pagesCount > 1 && $_REQUEST["page"] <= $pagesCount){
						?>
							window.location.href = '<?php echo PAGE_DELIVERY_HISTORY; ?>?loadSingle=<?php echo $_REQUEST["loadSingle"]; ?>&page=<?php echo ($_REQUEST["page"] + 1); ?>';
						<?php
								}
							}
						?>
					}
				}

				$(function() {
					$.datepicker.setDefaults($.datepicker.regional[""]);
					$('#searchDeliveryDate').datepicker($.datepicker.regional["de"]);
					$('#searchDeliveryDate').datepicker("option", "maxDate", "0" );

					$('#searchDeliveryDateStart').datepicker($.datepicker.regional["de"]);
					$('#searchDeliveryDateStart').datepicker("option", "maxDate", "0" );

					$('#searchDeliveryDateEnd').datepicker($.datepicker.regional["de"]);
					$('#searchDeliveryDateEnd').datepicker("option", "maxDate", "0" );
				});

				<?php if($isWebConnection){ ?>
				$('.buttonLoadDeliveryDatas').click(function(){
					startLoadDeliveryDatas();
				});
				startLoadDeliveryDatas();
				<?php } ?>

				<?php if($_GET["loadAll"] == "true") { ?>
					// startLoadDeliveryDatas();
				<?php } ?>
			// EOF LOAD EXTERNAL DELIVERY DATAS

			$('.noticeBoxClose .iconClose').live('click', function(){
				$('#loadNoticeArea').fadeOut(animateTime, function(){
					$('#loadNoticeArea').remove()
				});
			});

			$('.buttonToggleSidebarMenue').click(function() {
				$('#menueSidebarToggleContent').toggle();
			});

			// BOF DPD SCANCODES LAYER
				function createDpdScanCodesLegend(){
					var legendContentDpdScanCodes = '';
					legendContentDpdScanCodes += ' ';
					legendContentDpdScanCodes += '';

					<?php
						$legendContentDpdScanCodes = '';
						if(!empty($arrDpdScanCodes)){
							$legendContentDpdScanCodes .= '<div class="buttonLegendDpdScanCodes" title="Legende DPD-Scancodes anzeigen" style="padding:0;margin: 0 0 6px 0;cursor:help;line-height:20px;" alt="Legende DPD-Scancodes" >';
							$legendContentDpdScanCodes .= '<img src="layout/icons/iconInfo.png" width="16" height="16"/> Legende DPD-Scancodes anzeigen';
							$legendContentDpdScanCodes .= '</div>';
							$legendContentDpdScanCodes .= '<div class="displayNoticeArea legendTransActionTypes">';
								#$legendContentDpdScanCodes .= '<h3>Legende DPD-Scancodes:</h3>';
								$legendContentDpdScanCodes .= '<table border="1" cellpadding="0" cellspacing="0">';
								$legendContentDpdScanCodes .= '<thead>';
								$legendContentDpdScanCodes .= '<tr>';
									$legendContentDpdScanCodes .= '<th>Abk.</th>';
									$legendContentDpdScanCodes .= '<th>Bedeutung</th>';
									$legendContentDpdScanCodes .= '<th>Beschreibung</th>';
								$legendContentDpdScanCodes .= '</tr>';
								$legendContentDpdScanCodes .= '</thead>';

								$legendContentDpdScanCodes .= '<tbody>';
								$countRow = 0;

								foreach($arrDpdScanCodes as $thisDpdScanCodesKey => $thisDpdScanCodesValue){
									$rowClass = 'row0';
									if($countRow%2 == 0) {
										$rowClass = 'row1';
									}

									$legendContentDpdScanCodes .= '<tr class="' . $rowClass . '" id="' . $thisDpdScanCodesKey . '">';
									$legendContentDpdScanCodes .= '<td><b>' . $thisDpdScanCodesKey . '</b></td>';
									$legendContentDpdScanCodes .= '<td><b>' . $thisDpdScanCodesValue . '</b></td>';
									$legendContentDpdScanCodes .= '<td style="font-size:11px;">' . addslashes(htmlentities(($arrDpdScanCodesDescription[$thisDpdScanCodesKey]))) . '</td>';
									$legendContentDpdScanCodes .= '</tr>';

									$countRow++;
								}
								$legendContentDpdScanCodes .= '</tbody>';
								$legendContentDpdScanCodes .= '</table>';
							$legendContentDpdScanCodes .= '</div>';
						}
						$legendContentDpdScanCodes = removeUnnecessaryChars($legendContentDpdScanCodes);
						$legendContentDpdScanCodes = preg_replace("/<\//", "<\/", $legendContentDpdScanCodes);
					?>
					legendContentDpdScanCodes += '<?php echo $legendContentDpdScanCodes; ?>';

					return legendContentDpdScanCodes;
				}

				var legendContentDpdScanCodes = createDpdScanCodesLegend();
				$('.buttonDpdScancodesLegend').append(legendContentDpdScanCodes);

				$('.buttonLegendDpdScanCodes').live('click', function (){
					loadNotice($(this));
				});
			// EOF DPD SCANCODES LAYER
	});
	/* ]]> */
	// -->
</script>

<?php require_once('inc/footerHTML.inc.php'); ?>