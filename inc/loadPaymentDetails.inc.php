<?php
	session_start();

	require_once('../config/configMandator.inc.php');
	require_once('../config/configParams_' . strtolower(MANDATOR) . '.inc.php');
	require_once('../config/configBasic.inc.php');
	require_once('../config/configFiles.inc.php');
	require_once('../config/configTables.inc.php');
	require_once('../inc/functions.inc.php');
	##require_once('header.inc.php');

	$thisSearchString = trim($_GET["searchString"]);
	$linkPath = trim($_GET["linkPath"]);

	$arrThisPath = parse_url($linkPath);
	$linkPath = $arrThisPath["path"];

	$content = '';

	$warningMessage = '';
	$errorMessage = '';
	$successMessage = '';
	$infoMessage = '';

	$useUf8Encode = false;

	if($thisSearchString != "") {
		$dbConnection = new DB_Connection();
		$db_open = $dbConnection->db_connect();

		// BOF READ BANK TYPES
			$arrBankAccountTypeDatas = getBankAccountTypes();
		// EOF READ BANK TYPES

		$userDatas = getUserDatas();

		$arrRelatedDocuments = getRelatedDocuments(array($thisSearchString));
		// Array ( [RE] => RE-1306001645 [LS] => LS-1306000385 [AB] => AB-1306001803 )
		$relatedDocumentsComment = getRelatedDocumentsComment(array($thisSearchString));

		if(!empty($arrRelatedDocuments)){
			// BOF GET DOCUMENT DATA
			$arrSqlDocuments = array();
			foreach($arrRelatedDocuments as $thisRelatedDocumentKey => $thisRelatedDocumentValue){
				# if(!preg_match("/collective/", $thisRelatedDocumentKey)){
				if(defined('TABLE_ORDER_' . $thisRelatedDocumentKey)){
					$arrSqlDocuments[] = "SELECT
									`" . constant('TABLE_ORDER_' . $thisRelatedDocumentKey) . "`.`orderDocumentsID`,
									`" . constant('TABLE_ORDER_' . $thisRelatedDocumentKey) . "`.`orderDocumentsNumber`,
									`" . constant('TABLE_ORDER_' . $thisRelatedDocumentKey) . "`.`orderDocumentsDocumentDate`,
									`" . constant('TABLE_ORDER_' . $thisRelatedDocumentKey) . "`.`orderDocumentsStatus`,
									`" . constant('TABLE_ORDER_' . $thisRelatedDocumentKey) . "`.`orderDocumentsTotalPrice`,

									`" . constant('TABLE_ORDER_' . $thisRelatedDocumentKey) . "`.`orderDocumentsInterestPercent`,
									`" . constant('TABLE_ORDER_' . $thisRelatedDocumentKey) . "`.`orderDocumentsChargesPrice`,
									`" . constant('TABLE_ORDER_' . $thisRelatedDocumentKey) . "`.`orderDocumentsInterestPrice`,

									SUBSTRING(`" . constant('TABLE_ORDER_' . $thisRelatedDocumentKey) . "`.`orderDocumentsType`, 1, 2) AS `orderDocumentsType`,
									`" . constant('TABLE_ORDER_' . $thisRelatedDocumentKey) . "`.`orderDocumentsType` AS `orderDocumentsType2`,
									`" . constant('TABLE_ORDER_' . $thisRelatedDocumentKey) . "`.`orderDocumentsDocumentPath`,

									`" . TABLE_PAYMENT_STATUS_TYPES . "`.`paymentStatusTypesName`

									FROM `" . constant('TABLE_ORDER_' . $thisRelatedDocumentKey) . "`
									LEFT JOIN `" . TABLE_PAYMENT_STATUS_TYPES . "`
									ON(`" . constant('TABLE_ORDER_' . $thisRelatedDocumentKey) . "`.`orderDocumentsStatus` = `" . TABLE_PAYMENT_STATUS_TYPES . "`.`paymentStatusTypesID`)

									WHERE 1
										AND `orderDocumentsNumber` = '" . $thisRelatedDocumentValue . "'
						";
				}
			}
			$sql = "";
			if(count($arrSqlDocuments) > 1){
				$sql = implode(" UNION ", $arrSqlDocuments);
			}
			else if(count($arrSqlDocuments) > 0){
				$sql = $arrSqlDocuments[0];
			}
			if($sql != ""){
				$sql .= " ORDER BY FIELD(`orderDocumentsType`, 'AN', 'AB', 'LS', 'RE', 'MA', 'M1', 'M2', 'M3', 'IK', 'GU') ";
				$rs = $dbConnection->db_query($sql);
				while($ds = mysqli_fetch_assoc($rs)) {
					foreach(array_keys($ds) as $field){
						$arrDocumentData[$ds["orderDocumentsNumber"]][$field] = $ds[$field];
					}
				}
			}
			// EOF GET DOCUMENT DATA

			if(mysqli_error()){
				$content .= mysqli_error() . '<br />' . $sql . '<br />';
			}
			$content .= mysqli_error();
			// BOF GET PAYMENT DATA
			$arrSqlPayments = array();
			if($arrRelatedDocuments["RE"] != ""){
				$arrSqlPayments[] = "SELECT
								`orderPaymentID`,
								`orderPaymentOrderID`,
								`orderPaymentOrderNumber`,
								`orderPaymentDate`,
								`orderPaymentBankAccountID`,
								`orderPaymentValue`,
								`orderPaymentSkontoValue`,
								`orderPaymentNotiz`

							FROM `" . TABLE_ORDER_INVOICE_PAYMENTS . "`

							WHERE `orderPaymentOrderNumber` = '" . $arrRelatedDocuments["RE"] . "'
					";
			}
			if($arrRelatedDocuments["AB"] != ""){
				$arrSqlPayments[] = "SELECT
								`orderPaymentID`,
								`orderPaymentOrderID`,
								`orderPaymentOrderNumber`,
								`orderPaymentDate`,
								`orderPaymentBankAccountID`,
								`orderPaymentValue`,
								`orderPaymentSkontoValue`,
								`orderPaymentNotiz`

							FROM `" . TABLE_ORDER_CONFIRMATION_PAYMENTS . "`

							WHERE `orderPaymentOrderNumber` = '" . $arrRelatedDocuments["AB"] . "'
					";
			}

			$sql = "";
			if(count($arrSqlPayments) > 1){
				$sql = implode(" UNION ", $arrSqlPayments);
			}
			else if(count($arrSqlPayments) > 0){
				$sql = $arrSqlPayments[0];
			}

			if($relatedDocumentsComment != ''){
				$content .= '<h2>Notizen</h2> ';
				$content .= '<p class="infoArea">' . $relatedDocumentsComment . '</p>';
				$content .= '<br />';
			}

			$content .= '<h2>Zahlungseing&auml;nge</h2> ';
			if($sql != ""){
				$rs = $dbConnection->db_query($sql);
				$countRows = $dbConnection->db_getMysqlNumRows($rs);

				if($countRows > 0){
					$content .= '<table border="1" width="100%" cellpadding="0" cellspacing="0" class="border">';
					$content .= '<tr>';

					$content .= '<th style="width:45px;">#</th>';
					$content .= '<th style="width:100px;">Dokument-Nummer</th>';
					$content .= '<th style="width:70px;">Zahl-Datum</th>';
					$content .= '<th style="width:80px;">Zahl-Betrag</th>';
					$content .= '<th style="width:80px;">Skonto</th>';
					$content .= '<th style="width:300px;">Bankverbindung</th>';
					$content .= '<th>Notiz</th>';
					$content .= '<th style="width:20px;"></th>';
					$content .= '</tr>';
					$count = 0;
					while($ds = mysqli_fetch_assoc($rs)) {
						if($count%2 == 0){ $rowClass = 'row0'; }
							else { $rowClass = 'row1'; }

						$content .= '<tr class="'.$rowClass.'">';

						$content .= '<td style="text-align:right;"><b>' . ($count + 1) . '.</b></td>';
						$content .= '<td>' . $ds["orderPaymentOrderNumber"] . '</td>';
						$content .= '<td>' . formatDate($ds["orderPaymentDate"], 'display') . '</td>';
						$content .= '<td style="text-align:right;">' . number_format($ds["orderPaymentValue"], 2, ',', '.') . ' &euro;</td>';
						$content .= '<td style="text-align:right;">' . number_format($ds["orderPaymentSkontoValue"], 2, ',', '.') . ' &euro;</td>';
						$content .= '<td>';
						if($ds["orderPaymentBankAccountID"] != '' && $ds["orderPaymentBankAccountID"] != '0'){
							$content .= $arrBankAccountTypeDatas[$ds["orderPaymentBankAccountID"]]["bankAccountTypesName"] . ' (Kto: ' . $arrBankAccountTypeDatas[$ds["orderPaymentBankAccountID"]]["bankAccountTypesAccountNumber"] . ')';
						}
						else {
							$content .= '';
						}
						$content .= '</td>';
						$content .= '<td>' . nl2br($ds["orderPaymentNotiz"]) . '</td>';
						$content .= '<td>';
						// BOF DELETE PAYMENT ENTRY
							// BOF GET CALLING SCRIPT
							$callers = debug_backtrace();
							$scriptCaller = $callers[1]['function'];
							$scriptCaller = $_SERVER['HTTP_REFERER'];
							$arrPathInfo = parse_url($scriptCaller);
							$scriptCaller = $arrPathInfo['path'];
							// EOF GET CALLING SCRIPT
							$content .= '<a href="' . $scriptCaller . '?deleteOrderPaymentID=' .$ds["orderPaymentID"] . '&searchDocumentNumber=' . $ds["orderPaymentOrderNumber"] . '&deleteDocumentType=' . substr($ds["orderPaymentOrderNumber"], 0, 2) . '" onclick="return showWarning(\'Soll dieser Zahlungseingang wirklich entfernt werden?\nWenn ja, beachten Sie den Zahlstatus der Rechnung!!!!!!\')">';
							$content .= '<img src="layout/icons/iconDelete.png" alt="Eintrag entfernen" title="Eintrag entfernen" />';
							$content .= '</a>';
						// EOF DELETE PAYMENT ENTRY
						$content .= '</td>';

						$content .= '</tr>';
						$count++;
					}
					$content .= '</table>';
				}
				else {
					$content .= '<p class="infoArea">Es sind keine Zahlungen vorhanden.</p>';
				}
				$content .= '<hr />';
			}
			// BOF GET RELATED DOCUMENT DATAS
				$content .= '<h2>Verkn&uuml;pfte Dokumente: ' . $thisSearchString . '</h2> ';

				// BOF NEW
				if($arrRelatedDocuments["collectiveABs"] != ""){
					$arrThisCollectiveABs = explode(";", $arrRelatedDocuments["collectiveABs"]);

					$sql = "
						SELECT
							`" . TABLE_ORDER_CONFIRMATIONS. "`.`orderDocumentsID`,
							`" . TABLE_ORDER_CONFIRMATIONS. "`.`orderDocumentsNumber`,
							`" . TABLE_ORDER_CONFIRMATIONS. "`.`orderDocumentsDocumentDate`,
							`" . TABLE_ORDER_CONFIRMATIONS. "`.`orderDocumentsStatus`,
							`" . TABLE_ORDER_CONFIRMATIONS. "`.`orderDocumentsTotalPrice`,
							`" . TABLE_ORDER_CONFIRMATIONS. "`.`orderDocumentsType`,
							`" . TABLE_ORDER_CONFIRMATIONS. "`.`orderDocumentsDocumentPath`,

							`" . TABLE_PAYMENT_STATUS_TYPES. "`.`paymentStatusTypesName`

							FROM `" . TABLE_ORDER_CONFIRMATIONS. "`

							LEFT JOIN `" . TABLE_PAYMENT_STATUS_TYPES. "`
							ON(`" . TABLE_ORDER_CONFIRMATIONS. "`.`orderDocumentsStatus` = `" . TABLE_PAYMENT_STATUS_TYPES. "`.`paymentStatusTypesID`)

							WHERE 1
								AND (
									`orderDocumentsNumber` = '" . implode(" ' OR `orderDocumentsNumber` = '", $arrThisCollectiveABs). "'
								)
						";
					$rs = $dbConnection->db_query($sql);

					while($ds = mysqli_fetch_assoc($rs)){
						foreach(array_keys($ds) as $field){
							$arrDocumentCollectiveABsData[$ds["orderDocumentsNumber"]][$field] = $ds[$field];
						}
						$arrDocumentCollectiveABsData[$ds["orderDocumentsNumber"]]["orderDocumentsType2"] = $ds["orderDocumentsType"];
					}

					foreach($arrThisCollectiveABs as $thisCollectiveABs){
						if(!empty($arrDocumentCollectiveABsData)){
							$arrDocumentData[$thisCollectiveABs] = $arrDocumentCollectiveABsData[$thisCollectiveABs];
						}
						else {
							$arrDocumentData[$thisCollectiveABs] = array(
								"orderDocumentsID" => "...",
								"orderDocumentsNumber" => $thisCollectiveABs,
								"orderDocumentsDocumentDate" => "...",
								"orderDocumentsStatus" => 0,
								"orderDocumentsTotalPrice" => "...",
								"orderDocumentsType" => "AB",
								"orderDocumentsType2" => "AB",
								"orderDocumentsDocumentPath" => $thisCollectiveABs,
								"paymentStatusTypesName" => "..."
							);
						}
					}
					echo '<span style="color:#FF0000;font-weight:bold;font-size:10px;">';
					echo 'SAMMELRECHNUNG';
					echo '</span><br />';
				}

				if(!empty($arrDocumentData)){
					ksort($arrDocumentData);
					$content .= '<table border="0" width="" cellpadding="0" cellspacing="0" class="border">';
					$content .= '<tr>';

					$content .= '<th style="width:45px;">#</th>';
					$content .= '<th style="width:100px;">Dokument-Nummer</th>';
					$content .= '<th style="width:70px;">Datum</th>';
					$content .= '<th style="width:70px;">Summe</th>';
					$content .= '<th style="width:80px;">Status</th>';
					$content .= '<th style="width:20px;">Info</th>';
					$content .= '</tr>';
					$count = 0;

					$thisMarker = '';

					foreach($arrDocumentData as $thisDocumentData){
						if($count%2 == 0){ $rowClass = 'row0'; }
						else { $rowClass = 'row1'; }

						if($thisDocumentData["orderDocumentsStatus"] == "1") {
							#$rowClass = 'row5';
						}
						else if($thisDocumentData["orderDocumentsStatus"] == "2") {
							$rowClass = 'row3';
						}
						else if($thisDocumentData["orderDocumentsStatus"] == "4") {
							$rowClass = 'row2';
						}
						else if($thisDocumentData["orderDocumentsStatus"] == "6") {
							$rowClass = '';
						}
						else if($thisDocumentData["orderDocumentsStatus"] == "3") {
							$rowClass = 'row6';
						}

						$thisRowStyle = '';
						if($thisMarker == ''){
							$thisMarker = $thisDocumentData["orderDocumentsType"];
						}
						if($thisMarker != $thisDocumentData["orderDocumentsType"]){
							$thisRowStyle = ' style="border-top: 2px solid #003300;" ';
						}

						$content .= '<tr class="' . $rowClass . '" ' . $thisRowStyle . '>';

						$content .= '<td style="text-align:right;"><b>' . ($count + 1) . '.</b></td>';
						$content .= '<td style="white-space:nowrap;">' . ($thisDocumentData["orderDocumentsNumber"]) . '</td>';
						$content .= '<td style="white-space:nowrap;">' . formatDate($thisDocumentData["orderDocumentsDocumentDate"], 'display') . '</td>';

						$content .= '<td style="white-space:nowrap;text-align:right;"><b>';

						if(in_array($thisDocumentData["orderDocumentsType"], array('M1', 'M2'))){
							$content .= number_format(($thisDocumentData["orderDocumentsTotalPrice"] + $thisDocumentData["orderDocumentsChargesPrice"] + $thisDocumentData[$thisDocumentNumber]["orderDocumentsChargesPrice"]), 2, ",", ".");
						}
						else {
							$content .= number_format($thisDocumentData["orderDocumentsTotalPrice"], 2, ",", ".");
						}
						$content .= ' &euro;</b></td>';

						$content .= '<td style="white-space:nowrap;"><b>' . ($thisDocumentData["paymentStatusTypesName"]) . '</b></td>';
						$content .= '<td style="white-space:nowrap;">';

						$content .= '<span class="toolItem">';

						$content .= '<a href="' . $linkPath . '?downloadFile=' . basename($thisDocumentData["orderDocumentsDocumentPath"]) . '&thisDocumentType=' . substr(basename($thisDocumentData["orderDocumentsDocumentPath"]), 0, 2) . '"><img src="layout/icons/iconPDF.gif" width="16" height="16" alt="Dokument herunterladen" title="Dokument herunterladen" /></a>';
						$content .= '</span>';

						$content .= '</td>';

						$content .= '</tr>';
						$count++;
					}
					$content .= '</table>';
				}
				// EOF NEW

				// BOF OLD
				/*
				$sql = "SELECT
							`createdDocumentsNumber` AS `documentsNumber`,

							`createdDocumentsID` AS `documentsID`,
							`createdDocumentsType` AS `documentsType`,
							`createdDocumentsCustomerNumber` AS `documentsCustomerNumber`,
							`createdDocumentsTitle` AS `documentsTitle`,
							`createdDocumentsFilename` AS `documentsFilename`,
							`createdDocumentsContent` AS `documentsContent`,
							`createdDocumentsUserID` AS `documentsUserID`,
							`createdDocumentsTimeCreated` AS `documentsTimeCreated`,
							`createdDocumentsOrderIDs` AS `documentsOrderIDs`

							FROM `" . TABLE_CREATED_DOCUMENTS. "`

							WHERE 1
								AND (`createdDocumentsNumber` = '" . implode("' OR `createdDocumentsNumber` = '", $arrRelatedDocuments) . "')
							GROUP BY `createdDocumentsNumber`
					";
				$rs = $dbConnection->db_query($sql);


				$content .= '<table border="0" width="" cellpadding="0" cellspacing="0" class="border">';
				$content .= '<tr>';

				$content .= '<th style="width:45px;">#</th>';
				$content .= '<th style="width:100px;">Dokument-Nummer</th>';
				$content .= '<th style="width:70px;">Datum</th>';
				$content .= '<th style="width:80px;">Status</th>';
				$content .= '<th style="width:20px;">Info</th>';
				$content .= '</tr>';
				$count = 0;
				while($ds = mysqli_fetch_assoc($rs)) {
					if(!in_array($ds["documentsType"], array('KA', 'BR'))){
						if($count%2 == 0){ $rowClass = 'row0'; }
						else { $rowClass = 'row1'; }

						if($arrDocumentData[$ds["documentsNumber"]]["orderDocumentsStatus"] == "1") {
							#$rowClass = 'row5';
						}
						else if($arrDocumentData[$ds["documentsNumber"]]["orderDocumentsStatus"] == "2") {
							$rowClass = 'row3';
						}
						else if($arrDocumentData[$ds["documentsNumber"]]["orderDocumentsStatus"] == "4") {
							$rowClass = 'row2';
						}
						else if($arrDocumentData[$ds["documentsNumber"]]["orderDocumentsStatus"] == "6") {
							$rowClass = '';
						}
						else if($arrDocumentData[$ds["documentsNumber"]]["orderDocumentsStatus"] == "3") {
							$rowClass = 'row6';
						}

						$content .= '<tr class="' . $rowClass . '">';

						$content .= '<td style="text-align:right;"><b>' . ($count + 1) . '.</b></td>';
						$content .= '<td style="white-space:nowrap;">' . ($ds["documentsNumber"]) . '</td>';
						$content .= '<td style="white-space:nowrap;">' . formatDate($arrDocumentData[$ds["documentsNumber"]]["orderDocumentsDocumentDate"], 'display') . '</td>';
						$content .= '<td style="white-space:nowrap;"><b>' . ($arrDocumentData[$ds["documentsNumber"]]["paymentStatusTypesName"]) . '</b></td>';
						$content .= '<td style="white-space:nowrap;">';

						$content .= '<span class="toolItem">';

						$content .= '<a href="' . $linkPath . '?downloadFile=' . basename($ds["documentsFilename"]) . '&thisDocumentType=' . substr(basename($ds["documentsFilename"]), 0, 2) . '"><img src="layout/icons/iconPDF.gif" width="16" height="16" alt="Dokument herunterladen" title="Dokument herunterladen" /></a>';
						$content .= '</span>';

						$content .= '</td>';

						$content .= '</tr>';
						$count++;
					}
				}
				$content .= '</table>';
				*/
				// EOF OLD
			// EOF GET RELATED DOCUMENT DATAS
		}
	}

	echo $content;
?>