<?php

	DEFINE('SEND_EXTERN_PRODUCTION_INFO_PER_MAIL', false);
	DEFINE('SEND_EXTERN_PRODUCTION_DATA_PER_MAIL', false);

	// BOF READ COUNTRIES
		if(empty($arrCountryTypeDatas)){
			$arrCountryTypeDatas = getCountryTypes();
		}
	// EOF READ COUNTRIES	

	function getOrderProcessDatas($thisOrderID='') {
		global $dbConnection, $arrPrintProductionFiles, $warningMessage, $errorMessage, $successMessage, $infoMessage;

		if($thisOrderID != ''){
			$sql = "
				SELECT
					`" . TABLE_ORDERS . "`.`ordersID`,
					`" . TABLE_ORDERS . "`.`ordersKundennummer`,
					`" . TABLE_ORDERS . "`.`ordersCustomerID`,
					`" . TABLE_ORDERS . "`.`ordersStatus`,
					`" . TABLE_ORDERS . "`.`ordersSourceType`,
					`" . TABLE_ORDERS . "`.`ordersSourceName`,
					`" . TABLE_ORDERS . "`.`ordersBestellDatum`,
					`" . TABLE_ORDERS . "`.`ordersStornoDatum`,
					`" . TABLE_ORDERS . "`.`ordersFreigabeDatum`,
					`" . TABLE_ORDERS . "`.`ordersFreigabeArt`,
					`" . TABLE_ORDERS . "`.`ordersBelichtungsDatum`,
					`" . TABLE_ORDERS . "`.`ordersProduktionsDatum`,
					`" . TABLE_ORDERS . "`.`ordersAuslieferungsDatum`,
					`" . TABLE_ORDERS . "`.`ordersLieferwoche`,
					`" . TABLE_ORDERS . "`.`ordersLieferDatum`,
					`" . TABLE_ORDERS . "`.`ordersKundenName`,
					`" . TABLE_ORDERS . "`.`ordersKommission`,
					`" . TABLE_ORDERS . "`.`ordersInfo`,
					`" . TABLE_ORDERS . "`.`ordersPLZ`,
					`" . TABLE_ORDERS . "`.`ordersOrt`,
					`" . TABLE_ORDERS . "`.`ordersLand`,
					`" . TABLE_ORDERS . "`.`ordersArtikelID`,
					`" . TABLE_ORDERS . "`.`ordersArtikelNummer`,
					`" . TABLE_ORDERS . "`.`ordersArtikelBezeichnung`,
					`" . TABLE_ORDERS . "`.`ordersArtikelKategorieID`,
					`" . TABLE_ORDERS . "`.`ordersArtikelMenge`,
					`" . TABLE_ORDERS . "`.`ordersArtikelPrintColorsCount`,
					`" . TABLE_ORDERS . "`.`ordersArtikelWithBorder`,
					`" . TABLE_ORDERS . "`.`ordersArtikelWithClearPaint`,
					`" . TABLE_ORDERS . "`.`ordersAufdruck`,
					`" . TABLE_ORDERS . "`.`ordersAdditionalArtikelID`,
					`" . TABLE_ORDERS . "`.`ordersAdditionalArtikelNummer`,
					`" . TABLE_ORDERS . "`.`ordersAdditionalArtikelBezeichnung`,
					`" . TABLE_ORDERS . "`.`ordersAdditionalArtikelKategorieID`,
					`" . TABLE_ORDERS . "`.`ordersAdditionalArtikelMenge`,
					`" . TABLE_ORDERS . "`.`ordersSinglePreis`,
					`" . TABLE_ORDERS . "`.`ordersTotalPreis`,
					`" . TABLE_ORDERS . "`.`ordersDruckFarbe`,
					`" . TABLE_ORDERS . "`.`ordersAdditionalCosts`,
					`" . TABLE_ORDERS . "`.`ordersAdditionalCostsID`,
					`" . TABLE_ORDERS . "`.`ordersVertreter`,
					`" . TABLE_ORDERS . "`.`ordersVertreterID`,
					`" . TABLE_ORDERS . "`.`ordersMailAdress`,
					`" . TABLE_ORDERS . "`.`ordersContactOthers`,
					`" . TABLE_ORDERS . "`.`ordersMandant`,
					`" . TABLE_ORDERS . "`.`ordersOrderType`,
					`" . TABLE_ORDERS . "`.`ordersPerExpress`,
					`" . TABLE_ORDERS . "`.`ordersBankAccountType`,
					`" . TABLE_ORDERS . "`.`ordersPaymentType`,
					`" . TABLE_ORDERS . "`.`ordersPaymentStatusType`,
					`" . TABLE_ORDERS . "`.`ordersNotizen`,
					`" . TABLE_ORDERS . "`.`ordersDruckerName`,
					`" . TABLE_ORDERS . "`.`ordersUserID`,
					`" . TABLE_ORDERS . "`.`ordersTimeCreated`,
					`" . TABLE_ORDERS . "`.`ordersArchive`,
					`" . TABLE_ORDERS . "`.`ordersUseNoStock`,
					`" . TABLE_ORDERS . "`.`ordersDirectSale`,
					`" . TABLE_ORDERS . "`.`ordersReklamationsgrund`,

					`customersData`.`customersID`,
					`customersData`.`customersKundennummer`,

					`customersData`.`customersFirmenname`,
					`customersData`.`customersFirmennameZusatz`,
					`customersData`.`customersFirmenInhaberVorname`,
					`customersData`.`customersFirmenInhaberNachname`,
					`customersData`.`customersFirmenInhaberAnrede`,
					`customersData`.`customersFirmenInhaber2Vorname`,
					`customersData`.`customersFirmenInhaber2Nachname`,
					`customersData`.`customersFirmenInhaber2Anrede`,

					`customersData`.`customersAnsprechpartner1Vorname`,
					`customersData`.`customersAnsprechpartner1Nachname`,
					`customersData`.`customersAnsprechpartner1Anrede`,
					`customersData`.`customersAnsprechpartner1Typ`,

					`customersData`.`customersAnsprechpartner2Vorname`,
					`customersData`.`customersAnsprechpartner2Nachname`,
					`customersData`.`customersAnsprechpartner2Anrede`,
					`customersData`.`customersAnsprechpartner2Typ`,

					`customersData`.`customersCompanyStrasse`,
					`customersData`.`customersCompanyHausnummer`,
					`customersData`.`customersCompanyCountry`,
					`customersData`.`customersCompanyPLZ`,
					`customersData`.`customersCompanyOrt`,

					`customersData`.`customersLieferadresseFirmenname`,
					`customersData`.`customersLieferadresseFirmennameZusatz`,
					`customersData`.`customersLieferadresseStrasse`,
					`customersData`.`customersLieferadresseHausnummer`,
					`customersData`.`customersLieferadressePLZ`,
					`customersData`.`customersLieferadresseOrt`,
					`customersData`.`customersLieferadresseLand`,

					`customersData`.`customersRechnungsadresseFirmenname`,
					`customersData`.`customersRechnungsadresseFirmennameZusatz`,
					`customersData`.`customersRechnungsadresseStrasse`,
					`customersData`.`customersRechnungsadresseHausnummer`,
					`customersData`.`customersRechnungsadressePLZ`,
					`customersData`.`customersRechnungsadresseOrt`,
					`customersData`.`customersRechnungsadresseLand`,

					`customersData`.`customersVertreterID`,
					`customersData`.`customersVertreterName`,
					`customersData`.`customersUseSalesmanDeliveryAdress`,
					`customersData`.`customersUseSalesmanInvoiceAdress`,

					`customersData`.`customersTyp`,
					`customersData`.`customersGruppe`,
					`customersData`.`customersNotiz`,
					`customersData`.`customersActive`,

					/* ------------------------------------------------------------------------------- */

					`agentsData`.`customersID` AS `agentsID`,
					`agentsData`.`customersKundennummer` AS `agentsKundennummer`,

					`agentsData`.`customersFirmenname` AS `agentsFirmenname`,
					`agentsData`.`customersFirmennameZusatz` AS `agentsFirmennameZusatz`,
					`agentsData`.`customersFirmenInhaberVorname` AS `agentsFirmenInhaberVorname`,
					`agentsData`.`customersFirmenInhaberNachname` AS `agentsFirmenInhaberNachname`,
					`agentsData`.`customersFirmenInhaberAnrede` AS `agentsFirmenInhaberAnrede`,
					`agentsData`.`customersFirmenInhaber2Vorname` AS `agentsFirmenInhaber2Vorname`,
					`agentsData`.`customersFirmenInhaber2Nachname` AS `agentsFirmenInhaber2Nachname`,
					`agentsData`.`customersFirmenInhaber2Anrede` AS `agentsFirmenInhaber2Anrede`,

					`agentsData`.`customersAnsprechpartner1Vorname` AS `agentsAnsprechpartner1Vorname`,
					`agentsData`.`customersAnsprechpartner1Nachname` AS `agentsAnsprechpartner1Nachname`,
					`agentsData`.`customersAnsprechpartner1Anrede` AS `agentsAnsprechpartner1Anrede`,
					`agentsData`.`customersAnsprechpartner1Typ` AS `agentsAnsprechpartner1Typ`,

					`agentsData`.`customersAnsprechpartner2Vorname` AS `agentsAnsprechpartner2Vorname`,
					`agentsData`.`customersAnsprechpartner2Nachname` AS `agentsAnsprechpartner2Nachname`,
					`agentsData`.`customersAnsprechpartner2Anrede` AS `agentsAnsprechpartner2Anrede`,
					`agentsData`.`customersAnsprechpartner2Typ` AS `agentsAnsprechpartner2Typ`,

					`agentsData`.`customersCompanyStrasse` AS `agentsCompanyStrasse`,
					`agentsData`.`customersCompanyHausnummer` AS `agentsCompanyHausnummer`,
					`agentsData`.`customersCompanyCountry` AS `agentsCompanyCountry`,
					`agentsData`.`customersCompanyPLZ` AS `agentsCompanyPLZ`,
					`agentsData`.`customersCompanyOrt` AS `agentsCompanyOrt`,

					`agentsData`.`customersLieferadresseFirmenname` AS `agentsLieferadresseFirmenname`,
					`agentsData`.`customersLieferadresseFirmennameZusatz` AS `agentsLieferadresseFirmennameZusatz`,
					`agentsData`.`customersLieferadresseStrasse` AS `agentsLieferadresseStrasse`,
					`agentsData`.`customersLieferadresseHausnummer` AS `agentsLieferadresseHausnummer`,
					`agentsData`.`customersLieferadressePLZ` AS `agentsLieferadressePLZ`,
					`agentsData`.`customersLieferadresseOrt` AS `agentsLieferadresseOrt`,
					`agentsData`.`customersLieferadresseLand` AS `agentsLieferadresseLand`,

					`agentsData`.`customersRechnungsadresseFirmenname` AS `agentsRechnungsadresseFirmenname`,
					`agentsData`.`customersRechnungsadresseFirmennameZusatz` AS `agentsRechnungsadresseFirmennameZusatz`,
					`agentsData`.`customersRechnungsadresseStrasse` AS `agentsRechnungsadresseStrasse`,
					`agentsData`.`customersRechnungsadresseHausnummer` AS `agentsRechnungsadresseHausnummer`,
					`agentsData`.`customersRechnungsadressePLZ` AS `agentsRechnungsadressePLZ`,
					`agentsData`.`customersRechnungsadresseOrt` AS `agentsRechnungsadresseOrt`,
					`agentsData`.`customersRechnungsadresseLand` AS `agentsRechnungsadresseLand`,

					`agentsData`.`customersVertreterID` AS `agentsVertreterID`,
					`agentsData`.`customersVertreterName` AS `agentsVertreterName`,
					`agentsData`.`customersUseSalesmanDeliveryAdress` AS `agentsUseSalesmanDeliveryAdress`,
					`agentsData`.`customersUseSalesmanInvoiceAdress` AS `agentsUseSalesmanInvoiceAdress`,

					`agentsData`.`customersTyp` AS `agentsTyp`,
					`agentsData`.`customersGruppe` AS `agentsGruppe`,
					`agentsData`.`customersNotiz` AS `agentsNotiz`,
					`agentsData`.`customersActive` AS `agentsActive`,

					/* ------------------------------------------------------------------------------- */

					`salesmansData`.`customersID` AS `salesmansID`,
					`salesmansData`.`customersKundennummer` AS `salesmansKundennummer`,

					`salesmansData`.`customersFirmenname` AS `salesmansFirmenname`,
					`salesmansData`.`customersFirmennameZusatz` AS `salesmansFirmennameZusatz`,
					`salesmansData`.`customersFirmenInhaberVorname` AS `salesmansFirmenInhaberVorname`,
					`salesmansData`.`customersFirmenInhaberNachname` AS `salesmansFirmenInhaberNachname`,
					`salesmansData`.`customersFirmenInhaberAnrede` AS `salesmansFirmenInhaberAnrede`,
					`salesmansData`.`customersFirmenInhaber2Vorname` AS `salesmansFirmenInhaber2Vorname`,
					`salesmansData`.`customersFirmenInhaber2Nachname` AS `salesmansFirmenInhaber2Nachname`,
					`salesmansData`.`customersFirmenInhaber2Anrede` AS `salesmansFirmenInhaber2Anrede`,

					`salesmansData`.`customersAnsprechpartner1Vorname` AS `salesmansAnsprechpartner1Vorname`,
					`salesmansData`.`customersAnsprechpartner1Nachname` AS `salesmansAnsprechpartner1Nachname`,
					`salesmansData`.`customersAnsprechpartner1Anrede` AS `salesmansAnsprechpartner1Anrede`,
					`salesmansData`.`customersAnsprechpartner1Typ` AS `salesmansAnsprechpartner1Typ`,

					`salesmansData`.`customersAnsprechpartner2Vorname` AS `salesmansAnsprechpartner2Vorname`,
					`salesmansData`.`customersAnsprechpartner2Nachname` AS `salesmansAnsprechpartner2Nachname`,
					`salesmansData`.`customersAnsprechpartner2Anrede` AS `salesmansAnsprechpartner2Anrede`,
					`salesmansData`.`customersAnsprechpartner2Typ` AS `salesmansAnsprechpartner2Typ`,

					`salesmansData`.`customersCompanyStrasse` AS `salesmansCompanyStrasse`,
					`salesmansData`.`customersCompanyHausnummer` AS `salesmansCompanyHausnummer`,
					`salesmansData`.`customersCompanyCountry` AS `salesmansCompanyCountry`,
					`salesmansData`.`customersCompanyPLZ` AS `salesmansCompanyPLZ`,
					`salesmansData`.`customersCompanyOrt` AS `salesmansCompanyOrt`,

					`salesmansData`.`customersLieferadresseFirmenname` AS `salesmansLieferadresseFirmenname`,
					`salesmansData`.`customersLieferadresseFirmennameZusatz` AS `salesmansLieferadresseFirmennameZusatz`,
					`salesmansData`.`customersLieferadresseStrasse` AS `salesmansLieferadresseStrasse`,
					`salesmansData`.`customersLieferadresseHausnummer` AS `salesmansLieferadresseHausnummer`,
					`salesmansData`.`customersLieferadressePLZ` AS `salesmansLieferadressePLZ`,
					`salesmansData`.`customersLieferadresseOrt` AS `salesmansLieferadresseOrt`,
					`salesmansData`.`customersLieferadresseLand` AS `salesmansLieferadresseLand`,

					`salesmansData`.`customersRechnungsadresseFirmenname` AS `salesmansRechnungsadresseFirmenname`,
					`salesmansData`.`customersRechnungsadresseFirmennameZusatz` AS `salesmansRechnungsadresseFirmennameZusatz`,
					`salesmansData`.`customersRechnungsadresseStrasse` AS `salesmansRechnungsadresseStrasse`,
					`salesmansData`.`customersRechnungsadresseHausnummer` AS `salesmansRechnungsadresseHausnummer`,
					`salesmansData`.`customersRechnungsadressePLZ` AS `salesmansRechnungsadressePLZ`,
					`salesmansData`.`customersRechnungsadresseOrt` AS `salesmansRechnungsadresseOrt`,
					`salesmansData`.`customersRechnungsadresseLand` AS `salesmansRechnungsadresseLand`,

					`salesmansData`.`customersVertreterID` AS `salesmansVertreterID`,
					`salesmansData`.`customersVertreterName` AS `salesmansVertreterName`,
					`salesmansData`.`customersUseSalesmanDeliveryAdress` AS `salesmansUseSalesmanDeliveryAdress`,
					`salesmansData`.`customersUseSalesmanInvoiceAdress` AS `salesmansUseSalesmanInvoiceAdress`,

					`salesmansData`.`customersTyp` AS `salesmansTyp`,
					`salesmansData`.`customersGruppe` AS `salesmansGruppe`,
					`salesmansData`.`customersNotiz` AS `salesmansNotiz`,
					`salesmansData`.`customersActive` AS `salesmansActive`

					/* ------------------------------------------------------------------------------- */

				FROM `" . TABLE_ORDERS . "`

				LEFT JOIN `" . TABLE_CUSTOMERS . "` AS `customersData`
				ON(`" . TABLE_ORDERS . "`.`ordersKundennummer` = `customersData`.`customersKundennummer` )

				LEFT JOIN `" . TABLE_CUSTOMERS . "` AS `agentsData`
				ON(`customersData`.`customersVertreterID` = `agentsData`.`customersID` AND `customersData`.`customersVertreterID` != '')

				LEFT JOIN `" . TABLE_CUSTOMERS . "` AS `salesmansData`
				ON(`" . TABLE_ORDERS . "`.`ordersVertreterID` = `salesmansData`.`customersID` AND `" . TABLE_ORDERS . "`.`ordersVertreterID` != '')

				WHERE 1
					AND `" . TABLE_ORDERS . "`.`ordersID` = '" . $thisOrderID . "'

				ORDER BY `ordersKundenName` ASC
			";
			// ORDER BY `ordersKundennummer` ASC

			$rs = $dbConnection->db_query($sql);

			$countRows = $dbConnection->db_getMysqlNumRows($rs);

			if($countRows > 0){
				while($ds = mysqli_fetch_assoc($rs)){
					foreach(array_keys($ds) as $field){
						$arrOrderProcessData[$ds["ordersID"]][$field] = $ds[$field];
					}
				}
			}
			else{
				$warningMessage .= ' Es wurden keine Produktionen gefunden!' . '<br />';
			}
		}
		return $arrOrderProcessData;
	}

	function getProductionFilesFileSheetNumber($thisFile){
		preg_match("/_([0-9]{2})\.pdf/", $thisFile, $arrFound);
		$thisSheetNumber = $arrFound[1];
		return $thisSheetNumber;
	}
	function printProductionFilesFileDate($thisFile) {
		preg_match("/([0-9]{4}-[0-9]{2}-[0-9]{2})/", $thisFile, $arrFound);
		$thisDate = $arrFound[1];
		return $thisDate;
	}
	function renamePrintProductionFile($thisFile){
		$thisNewFileName = basename($thisFile);

		$pattern = "_([0-9]{2})\.([0-9]{2})\.([0-9]{4})";
		$replace = "_$3-$2-$1";
		$thisNewFileName = preg_replace("/" . $pattern . "/", $replace, $thisNewFileName);

		$pattern = "_([0-9]{1})\.([0-9]{2})\.([0-9]{4})";
		$replace = "_$3-$2-0$1";
		$thisNewFileName = preg_replace("/" . $pattern . "/", $replace, $thisNewFileName);

		$pattern = "_([0-9]{2})\.([0-9]{2})\.([0-9]{2})";
		$replace = "_" . substr(date("Y"), 0, 2) . "$3-$2-$1";
		$thisNewFileName = preg_replace("/" . $pattern . "/", $replace, $thisNewFileName);

		$pattern = "(Belichtungsbogen|Belichtung)";
		$replace = "belichtung";
		$thisNewFileName = preg_replace("/" . $pattern . "/ism", $replace, $thisNewFileName);


		$pattern = "_([0-9])\.([a-z]{3})$";
		$replace = "_0$1.$2";
		$thisNewFileName = preg_replace("/" . $pattern . "/", $replace, $thisNewFileName);

		$pattern = "-([0-9]{2})(\.[a-z]{3})$";
		$replace = "-$1_01$2";
		$thisNewFileName = preg_replace("/" . $pattern . "/", $replace, $thisNewFileName);

		$thisNewFileName = strtolower($thisNewFileName);

		return $thisNewFileName;
	}

	function formatAdressData($arrDatas){
		$arrAdressFormatted = "";
		if($arrDatas["knr"] != "") { $arrAdressFormatted .= $arrDatas["knr"] . '<br />'; }
		if($arrDatas["name"] != "") { $arrAdressFormatted .= $arrDatas["name"] . '<br />'; }
		if($arrDatas["nameAdd"] != "") { $arrAdressFormatted .= $arrDatas["nameAdd"] . '<br />'; }
		if($arrDatas["street"] != "") { $arrAdressFormatted .= $arrDatas["street"] . ' '; }
		if($arrDatas["streetNumber"] != "") { $arrAdressFormatted .= $arrDatas["streetNumber"] . '<br />'; }
		if($arrDatas["zipcode"] != "") { $arrAdressFormatted .= $arrDatas["zipcode"] . ' '; }
		if($arrDatas["city"] != "") { $arrAdressFormatted .= $arrDatas["city"] . '<br />'; }
		if($arrDatas["country"] != "") { $arrAdressFormatted .= $arrDatas["country"] . ''; }
		return $arrAdressFormatted;
	}

	function getSenderAndRecipientAdress(){
		global $arrCountryTypeDatas, $arrThisRecipientAdress, $arrThisSenderAdress, $thisPrintProductionDataValue, $arrAllMandatoryCompanyDatas;

		// BOF RECIPIENT ADRESS
		$arrThisRecipientAdress = array();
		$arrThisRecipientAdress["USE"] = array();

		$arrThisRecipientAdress["CUSTOMER"] = array();
		$arrThisRecipientAdress["CUSTOMER"]["id"] = $thisPrintProductionDataValue["customersID"];
		$arrThisRecipientAdress["CUSTOMER"]["knr"] = $thisPrintProductionDataValue["customersKundennummer"];
		$arrThisRecipientAdress["CUSTOMER"]["name"] = $thisPrintProductionDataValue["customersLieferadresseFirmenname"];
		$arrThisRecipientAdress["CUSTOMER"]["nameAdd"] = $thisPrintProductionDataValue["customersLieferadresseFirmennameZusatz"];
		$arrThisRecipientAdress["CUSTOMER"]["street"] = $thisPrintProductionDataValue["customersLieferadresseStrasse"];
		$arrThisRecipientAdress["CUSTOMER"]["streetNumber"] = $thisPrintProductionDataValue["customersLieferadresseHausnummer"];
		$arrThisRecipientAdress["CUSTOMER"]["zipcode"] = $thisPrintProductionDataValue["customersLieferadressePLZ"];
		$arrThisRecipientAdress["CUSTOMER"]["city"] = $thisPrintProductionDataValue["customersLieferadresseOrt"];
		$arrThisRecipientAdress["CUSTOMER"]["country"] = $arrCountryTypeDatas[$thisPrintProductionDataValue["customersLieferadresseLand"]]["countries_name"];
		$arrThisRecipientAdress["CUSTOMER"]["countryID"] = $thisPrintProductionDataValue["customersLieferadresseLand"];

		$arrThisRecipientAdress["SALESMAN"] = array();
		$arrThisRecipientAdress["SALESMAN"]["id"] = $thisPrintProductionDataValue["salesmansID"];
		$arrThisRecipientAdress["SALESMAN"]["knr"] = $thisPrintProductionDataValue["salesmansKundennummer"];
		$arrThisRecipientAdress["SALESMAN"]["name"] = $thisPrintProductionDataValue["salesmansLieferadresseFirmenname"];
		$arrThisRecipientAdress["SALESMAN"]["nameAdd"] = $thisPrintProductionDataValue["salesmansLieferadresseFirmennameZusatz"];
		$arrThisRecipientAdress["SALESMAN"]["street"] = $thisPrintProductionDataValue["salesmansLieferadresseStrasse"];
		$arrThisRecipientAdress["SALESMAN"]["streetNumber"] = $thisPrintProductionDataValue["salesmansLieferadresseHausnummer"];
		$arrThisRecipientAdress["SALESMAN"]["zipcode"] = $thisPrintProductionDataValue["salesmansLieferadressePLZ"];
		$arrThisRecipientAdress["SALESMAN"]["city"] = $thisPrintProductionDataValue["salesmansLieferadresseOrt"];
		$arrThisRecipientAdress["SALESMAN"]["country"] = $arrCountryTypeDatas[$thisPrintProductionDataValue["salesmansLieferadresseLand"]]["countries_name"];
		$arrThisRecipientAdress["SALESMAN"]["countryID"] = $thisPrintProductionDataValue["salesmansLieferadresseLand"];

		if($thisPrintProductionDataValue["customersUseSalesmanDeliveryAdress"] == "1"){
			$arrThisRecipientAdress["USE"] = $arrThisRecipientAdress["SALESMAN"];
		}
		else {
			$arrThisRecipientAdress["USE"] = $arrThisRecipientAdress["CUSTOMER"];
		}
		// EOF RECIPIENT ADRESS

		// BOF SENDER ADRESS
		$arrThisSenderAdress = array();
		$arrThisSenderAdress["USE"] = array();

		$arrThisSenderAdress["CUSTOMER"] = array();
		$arrThisSenderAdress["CUSTOMER"]["knr"] = $thisPrintProductionDataValue["customersKundennummer"];
		$arrThisSenderAdress["CUSTOMER"]["name"] = $thisPrintProductionDataValue["customersFirmenname"];
		$arrThisSenderAdress["CUSTOMER"]["nameAdd"] = $thisPrintProductionDataValue["customersFirmennameZusatz"];
		$arrThisSenderAdress["CUSTOMER"]["street"] = $thisPrintProductionDataValue["customersCompanyStrasse"];
		$arrThisSenderAdress["CUSTOMER"]["streetNumber"] = $thisPrintProductionDataValue["customersCompanyHausnummer"];
		$arrThisSenderAdress["CUSTOMER"]["zipcode"] = $thisPrintProductionDataValue["customersCompanyPLZ"];
		$arrThisSenderAdress["CUSTOMER"]["city"] = $thisPrintProductionDataValue["customersCompanyCompanyOrt"];
		$arrThisSenderAdress["CUSTOMER"]["country"] = $arrCountryTypeDatas[$thisPrintProductionDataValue["customersCompanyCountry"]]["countries_name"];
		$arrThisSenderAdress["CUSTOMER"]["countryID"] = $thisPrintProductionDataValue["customersCompanyCountry"];

		$arrThisSenderAdress["SALESMAN"] = array();
		$arrThisSenderAdress["SALESMAN"]["knr"] = $thisPrintProductionDataValue["salesmansKundennummer"];
		$arrThisSenderAdress["SALESMAN"]["name"] = $thisPrintProductionDataValue["salesmansFirmenname"];
		$arrThisSenderAdress["SALESMAN"]["nameAdd"] = $thisPrintProductionDataValue["salesmansFirmennameZusatz"];
		$arrThisSenderAdress["SALESMAN"]["street"] = $thisPrintProductionDataValue["salesmansCompanyStrasse"];
		$arrThisSenderAdress["SALESMAN"]["streetNumber"] = $thisPrintProductionDataValue["salesmansCompanyHausnummer"];
		$arrThisSenderAdress["SALESMAN"]["zipcode"] = $thisPrintProductionDataValue["salesmansCompanyPLZ"];
		$arrThisSenderAdress["SALESMAN"]["city"] = $thisPrintProductionDataValue["salesmansCompanyOrt"];
		$arrThisSenderAdress["SALESMAN"]["country"] = $arrCountryTypeDatas[$thisPrintProductionDataValue["salesmansCompanyCountry"]]["countries_name"];
		$arrThisSenderAdress["SALESMAN"]["countryID"] = $thisPrintProductionDataValue["salesmansCompanyCountry"];

		$arrThisSenderAdress["AGENT"] = array();
		$arrThisSenderAdress["AGENT"]["knr"] = $thisPrintProductionDataValue["agentsKundennummer"];
		$arrThisSenderAdress["AGENT"]["name"] = $thisPrintProductionDataValue["agentsFirmenname"];
		$arrThisSenderAdress["AGENT"]["nameAdd"] = $thisPrintProductionDataValue["agentsFirmennameZusatz"];
		$arrThisSenderAdress["AGENT"]["street"] = $thisPrintProductionDataValue["agentsCompanyStrasse"];
		$arrThisSenderAdress["AGENT"]["streetNumber"] = $thisPrintProductionDataValue["agentsCompanyHausnummer"];
		$arrThisSenderAdress["AGENT"]["zipcode"] = $thisPrintProductionDataValue["agentsCompanyPLZ"];
		$arrThisSenderAdress["AGENT"]["city"] = $thisPrintProductionDataValue["agentsCompanyOrt"];
		$arrThisSenderAdress["AGENT"]["country"] = $arrCountryTypeDatas[$thisPrintProductionDataValue["agentsCompanyCountry"]]["countries_name"];
		$arrThisSenderAdress["AGENT"]["countryID"] = $thisPrintProductionDataValue["agentsCompanyCountry"];

		$arrThisSenderAdress["OUR_COMPANY"] = array();
		$arrThisSenderAdress["OUR_COMPANY"]["knr"] = '';
		$arrThisSenderAdress["OUR_COMPANY"]["name"] = $arrAllMandatoryCompanyDatas[strtolower($thisPrintProductionDataValue["ordersMandant"])]["COMPANY_NAME_LONG"];
		$arrThisSenderAdress["OUR_COMPANY"]["nameAdd"] = '';
		$arrThisSenderAdress["OUR_COMPANY"]["street"] = $arrAllMandatoryCompanyDatas[strtolower($thisPrintProductionDataValue["ordersMandant"])]["COMPANY_STREET_NAME"];
		$arrThisSenderAdress["OUR_COMPANY"]["streetNumber"] = $arrAllMandatoryCompanyDatas[strtolower($thisPrintProductionDataValue["ordersMandant"])]["COMPANY_STREET_NUMBER"];
		$arrThisSenderAdress["OUR_COMPANY"]["zipcode"] = $arrAllMandatoryCompanyDatas[strtolower($thisPrintProductionDataValue["ordersMandant"])]["COMPANY_ZIPCODE"];
		$arrThisSenderAdress["OUR_COMPANY"]["city"] = $arrAllMandatoryCompanyDatas[strtolower($thisPrintProductionDataValue["ordersMandant"])]["COMPANY_CITY"];
		$arrThisSenderAdress["OUR_COMPANY"]["country"] = $arrAllMandatoryCompanyDatas[strtolower($thisPrintProductionDataValue["ordersMandant"])]["COMPANY_COUNTRY"];
		$arrThisSenderAdress["OUR_COMPANY"]["countryID"] = strtolower($thisPrintProductionDataValue["ordersMandant"]);

		if($thisPrintProductionDataValue["ordersMandant"] == 'B3'){
			$arrThisSenderAdress["USE"] = $arrThisSenderAdress["OUR_COMPANY"];
			if($thisPrintProductionDataValue["customersUseSalesmanInvoiceAdress"] == "1"){
				$arrThisSenderAdress["USE"] = $arrThisSenderAdress["AGENT"];
			}
		}
		else if($thisPrintProductionDataValue["ordersMandant"] == 'BCTR'){
			$arrThisSenderAdress["USE"] = $arrThisSenderAdress["OUR_COMPANY"];
			if($thisPrintProductionDataValue["customersUseSalesmanInvoiceAdress"] == "1"){
				$arrThisSenderAdress["USE"] = $arrThisSenderAdress["AGENT"];
			}
		}
		// EOF SENDER ADRESS
	}

	// BOF OUR COMPANY DATAS
		$arrAllMandatoryCompanyDatas = getAllMandatoryCompanyDatas();
	// EOF OUR COMPANY DATAS

	// BOF SET FILE NAMES
		$sendAttachedMailLayoutFileName				= convertChars($_FILES["sendAttachedMailFile"]["name"]);
		$sendAttachedMailProductionSheetFileName	= "KNR-" . $_POST["editCustomerNumber"] . "_OID-" . $_POST["orderID"] . "_data_layout.pdf";
		$sendAttachedMailProductionDataZipName		= date("Y-m-d") . "_KNR-" . $_POST["editCustomerNumber"] . "_OID-" . $_POST["orderID"] . "_data.zip";
		$tmpDir = date("Y-m-d") . "_KNR-" . $_POST["editCustomerNumber"] . "_OID-" . $_POST["orderID"] . "/";
		#$thisPrintProductionDirPath = "/tmp/";
		#$thisPrintProductionDirPath = "_transferProductions/";
		$thisPrintProductionDirPath = "_tmp/";
		$thisPrintProductionDirPath .= $tmpDir;
		if(!is_dir($thisPrintProductionDirPath)){
			mkdir($thisPrintProductionDirPath);
			chmod($thisPrintProductionDirPath, 0777);
		}

		if(file_exists($thisPrintProductionDirPath . $sendAttachedMailProductionDataZipName)){
			unlink($thisPrintProductionDirPath . $sendAttachedMailProductionDataZipName);
		}
		if(file_exists($thisPrintProductionDirPath . $sendAttachedMailProductionSheetFileName)){
			unlink($thisPrintProductionDirPath . $sendAttachedMailProductionSheetFileName);
		}
		if(file_exists($thisPrintProductionDirPath . $sendAttachedMailLayoutFileName)){
			unlink($thisPrintProductionDirPath . $sendAttachedMailLayoutFileName);
		}

		$copyResult = copy($_FILES["sendAttachedMailFile"]["tmp_name"], $thisPrintProductionDirPath . $sendAttachedMailLayoutFileName);
		unlink($_FILES["sendAttachedMailFile"]["tmp_name"]);
	// EOF SET FILE NAMES

	// BOF READ DATAS FROM ORDERS PROCESS
		$arrOrderProcessData = getOrderProcessDatas($_POST["orderID"]);
	// BOF READ DATAS FROM ORDERS PROCESS

	// BOF GET ADRESSES
		$thisPrintProductionDataValue = $arrOrderProcessData[$_POST["orderID"]];
		getSenderAndRecipientAdress();

		#dd('arrCountryTypeDatas');
		#dd('arrThisRecipientAdress');
		#dd('arrThisSenderAdress');
	// BOF GET ADRESSES

	// BOF SERIALIZE PRODUCT DATAS
		$arrProductDatas = array();
		$arrProductDatas["ordersArtikelNummer"] = $thisPrintProductionDataValue["ordersArtikelNummer"];
		
		$arrProductDatas["ordersArtikelKategorieID"] = $thisPrintProductionDataValue["ordersArtikelKategorieID"];
		$arrProductDatas["ordersArtikelID"] = $thisPrintProductionDataValue["ordersArtikelID"];
		
		$arrProductDatas["ordersArtikelBezeichnung"] = addslashes($thisPrintProductionDataValue["ordersArtikelBezeichnung"]);
		$arrProductDatas["ordersArtikelOriginalMenge"] = $thisPrintProductionDataValue["ordersArtikelMenge"];
		$arrProductDatas["ordersArtikelMenge"] = $thisPrintProductionDataValue["ordersArtikelMenge"];
		if($_POST["sendAttachedMailNewQuantity"] > 0){
			$arrProductDatas["ordersArtikelMenge"] = $_POST["sendAttachedMailNewQuantity"];
		}
		$arrProductDatas["ordersDruckFarbe"] = $thisPrintProductionDataValue["ordersDruckFarbe"];		
		
		$arrProductDatas["ordersArtikelPrintColorsCount"] = $thisPrintProductionDataValue["ordersArtikelPrintColorsCount"];
		$arrProductDatas["ordersArtikelWithClearPaint"] = $thisPrintProductionDataValue["ordersArtikelWithClearPaint"];
		$arrProductDatas["ordersAufdruck"] = $thisPrintProductionDataValue["ordersAufdruck"];
		$arrProductDatas["ordersKommission"] = $thisPrintProductionDataValue["ordersKommission"];

		if($thisPrintProductionDataValue["ordersAdditionalArtikelMenge"] > 0){
			$arrProductDatas["orderCategoriesPartNumber"] = $arrOrderCategoriesTypeDatas[$thisPrintProductionDataValue["ordersAdditionalArtikelKategorieID"]]["orderCategoriesPartNumber"];

			$arrProductDatas["ordersAdditionalArtikelMenge"] = $thisPrintProductionDataValue["ordersAdditionalArtikelMenge"] ;

			$arrProductDatas["orderCategoriesName"] = $arrOrderCategoriesTypeDatas[$thisPrintProductionDataValue["ordersAdditionalArtikelKategorieID"]]["orderCategoriesName"];
			$arrProductDatas["ordersArtikelKategorieID"] = $thisPrintProductionDataValue["ordersArtikelKategorieID"];
		}

		$arrProductDatas["ordersPrintType"] = $thisPrintProductionDataValue["ordersAdditionalCostsID"];
		
		$arrProductDatas["ordersPrintingPlateNumber"] = $_POST["sendAttachedMailPrintingPlateNumber"];
		
		$arrProductDatas["ordersOrderType"] = $thisPrintProductionDataValue["ordersOrderType"];
		
		// BOF GET PRODUCT TRANSLATION
			$arrProductDatas["ordersArtikelBezeichnung_TR"] = '';
			$sql_getProductTranslation = "
					SELECT 	
							`stockProductCategoriesName_TR`						

						FROM `" . TABLE_STOCK_PRODUCT_CATEGORIES . "`

						WHERE 1
							AND `stockProductCategoriesID` = " . $arrProductDatas["ordersArtikelID"] . "
							
							LIMIT 1
				";
			$rs_getProductTranslation = $dbConnection->db_query($sql_getProductTranslation);
			
			list($productName_TR) = mysqli_fetch_array($rs_getProductTranslation);
			
			if($productName_TR != ''){
				$arrProductDatas["ordersArtikelBezeichnung_TR"] = $productName_TR;
			}
			
		// EOF GET PRODUCT TRANSLATION		

		$strProductDatas = serialize($arrProductDatas);
	// EOF SERIALIZE PRODUCT DATAS


	// BOF CREATE PDF CONTENT
		// BOF GET LANGUAGE FILE
			$thisSelectLanguageID = 'DE';
			if($_POST["sendAttachedMailLanguage"] != ""){
				$thisSelectLanguageID = $_POST["sendAttachedMailLanguage"];
			}
			require_once(preg_replace("/{###LANGUAGE_SHORT_NAME###}/", strtolower($thisSelectLanguageID), LANGUAGE_FILE_NAME));
		// EOF GET LANGUAGE FILE
		$thisCreatedPdfName = $sendAttachedMailProductionSheetFileName;

		$pdfContentPage = "";

		// BOF STYLES
			$pdfContentPage .= '
				<style type="text/css">
					<!--
						* { font-family: arial;}
						h1 { font-weight:normal; font-size: 16pt; padding: 0 0 2mm 0; margin:0;border-bottom: 0.1mm solid #000000;}
						h2 { font-weight:bold; font-size: 16pt; padding: 0 0 2mm 0; margin:0}
						table {font-weight: normal; font-size:14pt;margin:0 0 10mm 10mm;}
						td { vertical-align: top; border-bottom:0.1mm solid #333333; padding: 1mm 2mm 1mm 2mm;}
						p { font-size:15pt; color: #CC0000;  padding:0; margin: 2mm 0 2mm 0;}
						hr { height: 0.1mm; border: 0pt solid #333333; color:#333333; background-color:#333333; padding:0; margin: 2mm 0 2mm 0;}
					-->
				</style>
			';

		// EOF STYLES

		// BOF PAGE 1: DATAS
			$pdfContentPage .= '<page pageset="old" backtop="10mm" backbottom="55mm" backleft="0mm" backright="10mm">';
			$pdfContentPage .= '<h1>';
			$pdfContentPage .= '{###PRODUCTION_DATA###}' . '<br><br>';
			#$pdfContentPage .= ' • ';
			$pdfContentPage .= '{###CUSTUMER_NUMBER###}: <b style="font-size:18pt">' . $thisPrintProductionDataValue["ordersKundennummer"] . '</b>';
			$pdfContentPage .= ' • ';
			$pdfContentPage .= '{###COMPANY_NAME###}: <b style="font-size:18pt">' . $thisPrintProductionDataValue["ordersKundenName"] . '</b>';
			$pdfContentPage .= '</h1>';
			
			$thisIconImage = '';
			if($arrProductDatas["ordersOrderType"] == 3){
				$thisIconImage = '<img src="layout/icons/iconAttention.png" width="14" height="14" alt="" /> ';
			}
			else if($arrProductDatas["ordersOrderType"] == 1){
				$thisIconImage = '<img src="layout/icons/iconNewOrder.png" width="14" height="14" alt="" /> ';
			}			
			
			$pdfContentPage .= '<p>{###ORDER_TYPE###}: ' . $thisIconImage . ' ' . '{###ORDER_TYPE_' . $arrProductDatas["ordersOrderType"] . '###}' . '</p>';
			
			if(trim($arrProductDatas["ordersPrintingPlateNumber"] != '')){ 
				$pdfContentPage .= '<p>{###PRINTING_PLATE_NUMBER###}: ' . $arrProductDatas["ordersPrintingPlateNumber"] . '</p>';
			}

			if($_POST["sendAttachedMailRedDot"] == '1'){
				$pdfContentPage .= '<p style="font-size:18pt;">';
				$pdfContentPage .= '<img src="layout/icons/redDot_50.gif" width="20" height="20" alt="' . $arrLanguageData["CONTAINER_PLACE_BACK_NAME"] . '" title="' . $arrLanguageData["CONTAINER_PLACE_BACK_NAME"] . '" />';
				$pdfContentPage .= ' <b>' . $arrLanguageData["CONTAINER_PLACE_BACK"] . '</b>';
				$pdfContentPage .= '</p>';
			}

			if($_POST["sendAttachedMailUseNoNeutralPacking"] == '1'){
				$pdfContentPage .= '<p style="font-size:18pt;">';
				$pdfContentPage .= '<img src="layout/icons/greenDot_50.gif" width="20" height="20" alt="' . $arrLanguageData["PACKING"] . ': ' . $arrLanguageData["USE_NO_NEUTRAL_PACKING"] . '" title="' . $arrLanguageData["PACKING"] . ': ' . $arrLanguageData["USE_NO_NEUTRAL_PACKING"] . '" />';
				$pdfContentPage .= ' <b>' . $arrLanguageData["PACKING"] . ': ' . $arrLanguageData["USE_NO_NEUTRAL_PACKING"] . '</b>';
				$pdfContentPage .= '</p>';
			}

			if($arrPrintProductionFiles[$_REQUEST["editID"]]["printProductionFilesFileName"] != ''){
				$pdfContentPage .= '<p>{###EXPOSITION_FILM###} / {###FILE###}: &quot;' . $arrPrintProductionFiles[$_REQUEST["editID"]]["printProductionFilesFileName"] . '&quot; vom ' . formatDate($arrPrintProductionFiles[$_REQUEST["editID"]]["printProductionFilesFileDate"], "display") . ' - Bogen ' . getProductionFilesFileSheetNumber($arrPrintProductionFiles[$_REQUEST["editID"]]["printProductionFilesFileName"]) . '</p>';
			}

			if($arrProductDatas["ordersKommission"] != "" || $arrProductDatas["ordersAufdruck"] != ""){
				$pdfContentPage .= '<p>';
			}

			if($arrProductDatas["ordersKommission"] != ""){
				$pdfContentPage .= '{###COMMISSION###}: ' . $arrProductDatas["ordersKommission"];
			}

			if($arrProductDatas["ordersKommission"] != "" && $arrProductDatas["ordersAufdruck"] != ""){
				$pdfContentPage .= ' • ';
			}

			if($arrProductDatas["ordersAufdruck"] != ""){
				$pdfContentPage .= '{###PRINT_TEXT###}:: ' . $arrProductDatas["ordersAufdruck"];
			}

			if($arrProductDatas["ordersKommission"] != "" || $arrProductDatas["ordersAufdruck"] != ""){
				$pdfContentPage .= '</p>';
			}

			$pdfContentPage .= '<table border="0" cellpadding="0" cellspacing="0">';

			$pdfContentPage .= '<tr>';
			$pdfContentPage .= '<td>{###PRODUCT_NUMMER###}:</td>';
			$pdfContentPage .= '<td>';
			$pdfContentPage .= $arrProductDatas["ordersArtikelNummer"];
			$pdfContentPage .= '</td>';
			$pdfContentPage .= '</tr>';

			$pdfContentPage .= '<tr>';
			$pdfContentPage .= '<td>{###PRODUCT_NAME###}:</td>';
			$pdfContentPage .= '<td>';
			$pdfContentPage .= '<img src="layout/flags/flag_DE.gif" width="20" height="14" alt="DE" /> ';
			$pdfContentPage .= $arrProductDatas["ordersArtikelBezeichnung"];
			
			// BOF ADD PRODUCT NAME TR
				if($arrProductDatas["ordersArtikelBezeichnung_TR"] != ''){
					$pdfContentPage .= '<br>------<br>';
					$pdfContentPage .= '<img src="layout/flags/flag_TR.gif" width="20" height="14" alt="TR" /> ';
					$pdfContentPage .= $arrProductDatas["ordersArtikelBezeichnung_TR"];
				}
			// EOF ADD PRODUCT NAME TR
			
			$pdfContentPage .= '</td>';
			$pdfContentPage .= '</tr>';

			$pdfContentPage .= '<tr>';
			$pdfContentPage .= '<td>{###PRODUCT_QUANTITY###}:</td>';
			$pdfContentPage .= '<td>';
			$pdfContentPage .= $arrProductDatas["ordersArtikelMenge"];
			$pdfContentPage .= '</td>';
			$pdfContentPage .= '</tr>';

			$pdfContentPage .= '<tr>';
			$pdfContentPage .= '<td>{###PRODUCT_COLOR_NAMES###}:</td>';
			$pdfContentPage .= '<td>';
			#$pdfContentPage .= $arrProductDatas["ordersDruckFarbe"];
			#$pdfContentPage .= ' • ' . preg_replace("/\//", "<br> • ", $arrProductDatas["ordersDruckFarbe"]);
			
			$thisTransferOrdersColorsNames = $arrProductDatas["ordersDruckFarbe"];
			$thisTransferOrdersColorsNames = preg_replace("/[\/] (C|M|Y|K) ([0-9])/ismU", " | $1 $2", $thisTransferOrdersColorsNames);	
			$thisTransferOrdersColorsNames = preg_replace("/\//", "<br> • ", $thisTransferOrdersColorsNames);
			$pdfContentPage .= '<img src="layout/flags/flag_DE.gif" width="20" height="14" alt="DE" /> ';
			$pdfContentPage .= ' • ' . $thisTransferOrdersColorsNames;
	
			// BOF GET COLOR TRANSLATION AND ADD COLOR NAME TR				
				#$arrProductDatas["ordersDruckFarbe_TR"] = $arrProductDatas["ordersDruckFarbe"];
				$arrProductDatas["ordersDruckFarbe_TR"] = $thisTransferOrdersColorsNames;
				require_once(DIRECTORY_LANGUAGE_FILES . "translationColors.inc.php");		
			
				if(!empty($arrColorTranslations)){
					foreach($arrColorTranslations as $thisColorTranslationKey => $thisColorTranslationValue){
						$pattern = $thisColorTranslationValue["NAME_DE"];
						$replace = $thisColorTranslationValue["NAME_TR"];
						$arrProductDatas["ordersDruckFarbe_TR"] = preg_replace("/" . preg_quote($pattern, '/') . "/",$replace, $arrProductDatas["ordersDruckFarbe_TR"]);
						$test = $arrProductDatas["ordersDruckFarbe_TR"];
					}
				}	
				#if($arrProductDatas["ordersDruckFarbe_TR"] == $arrProductDatas["ordersDruckFarbe"]){
				if($arrProductDatas["ordersDruckFarbe_TR"] == $thisTransferOrdersColorsNames){
					$arrProductDatas["ordersDruckFarbe_TR"] = '';
				}
				
				if($arrProductDatas["ordersDruckFarbe_TR"] != ''){
					$pdfContentPage .= '<br>------<br>';
					$pdfContentPage .= '<img src="layout/flags/flag_TR.gif" width="20" height="14" alt="TR" /> ';
					$pdfContentPage .= ' • ' . $arrProductDatas["ordersDruckFarbe_TR"];
				}
			// EOF GET COLOR TRANSLATION AND ADD COLOR NAME TR					

			if($arrProductDatas["ordersArtikelWithClearPaint"] == '1'){
				$pdfContentPage .= ' <br> • plus ' . '{###PRODUCT_WITH_CLEARPAINT###} [Klarlack]';
			}

			$pdfContentPage .= '</td>';
			$pdfContentPage .= '</tr>';

			$pdfContentPage .= '<tr>';
			$pdfContentPage .= '<td>{###PRODUCT_COLOR_COUNT###}:</td>';
			$pdfContentPage .= '<td>';
			$pdfContentPage .= $arrProductDatas["ordersArtikelPrintColorsCount"];
			if($arrProductDatas["ordersArtikelWithClearPaint"] == '1'){
				$pdfContentPage .= ' plus 1 (' . '{###PRODUCT_WITH_CLEARPAINT###} [Klarlack])';
			}
			$pdfContentPage .= '</td>';
			$pdfContentPage .= '</tr>';

			if($thisPrintProductionDataValue["ordersAdditionalArtikelMenge"] > 0){
				$pdfContentPage .= '<tr>';
				$pdfContentPage .= '<td>{###PRODUCT_ADDITIONAL_NAME###}:</td>';
				$pdfContentPage .= '<td>';
				$pdfContentPage .= $arrProductDatas["orderCategoriesName"];
				$pdfContentPage .= '</td>';
				$pdfContentPage .= '</tr>';

				$pdfContentPage .= '<tr>';
				$pdfContentPage .= '<td>{###PRODUCT_ADDITIONAL_QUANTITY###}:</td>';
				$pdfContentPage .= '<td>';
				$pdfContentPage .= $arrProductDatas["ordersAdditionalArtikelMenge"];
				$pdfContentPage .= '</td>';
				$pdfContentPage .= '</tr>';
			}

			$pdfContentPage .= '</table>';

			$pdfContentPage .= '<hr>';

			$pdfContentPage .= '<h2>';
			$pdfContentPage .= '{###SENDER_ADRESS###}:';
			$pdfContentPage .= '</h2>';

			$pdfContentPage .= '<table border="0" cellpadding="0" cellspacing="0">';
			$pdfContentPage .= '<tr>';
			$pdfContentPage .= '<td>{###CUSTUMER_NUMBER###}:</td>';
			$pdfContentPage .= '<td>' . $arrThisSenderAdress["USE"]["knr"] . '</td>';
			$pdfContentPage .= '</tr>';
			$pdfContentPage .= '<tr>';
			$pdfContentPage .= '<td>{###COMPANY_NAME###}:</td>';
			$pdfContentPage .= '<td>' . $arrThisSenderAdress["USE"]["name"] . '</td>';
			$pdfContentPage .= '</tr>';
			$pdfContentPage .= '<tr>';
			$pdfContentPage .= '<td>{###STREET###}:</td>';
			$pdfContentPage .= '<td>' . $arrThisSenderAdress["USE"]["street"] . ' ' .  $arrThisSenderAdress["USE"]["streetNumber"] . '</td>';
			$pdfContentPage .= '</tr>';
			$pdfContentPage .= '<tr>';
			$pdfContentPage .= '<td>{###ZIPCODE###} / {###CITY###}:</td>';
			$pdfContentPage .= '<td>' . $arrThisSenderAdress["USE"]["zipcode"] . ' ' .  $arrThisSenderAdress["USE"]["city"] . '</td>';
			$pdfContentPage .= '</tr>';
			$pdfContentPage .= '<tr>';
			$pdfContentPage .= '<td>{###COUNTRY###}:</td>';
			$pdfContentPage .= '<td>' . $arrThisSenderAdress["USE"]["country"] . '</td>';
			$pdfContentPage .= '</tr>';
			$pdfContentPage .= '</table>';

			$pdfContentPage .= '<hr>';

			$pdfContentPage .= '<h2>';
			$pdfContentPage .= '{###RECIPIENT_ADRESS###}:';
			$pdfContentPage .= '</h2>';

			$pdfContentPage .= '<table border="0" cellpadding="0" cellspacing="0">';
			$pdfContentPage .= '<tr>';
			$pdfContentPage .= '<td>{###CUSTUMER_NUMBER###}:</td>';
			$pdfContentPage .= '<td>' . $arrThisRecipientAdress["USE"]["knr"] . '</td>';
			$pdfContentPage .= '</tr>';
			$pdfContentPage .= '<tr>';
			$pdfContentPage .= '<td>{###COMPANY_NAME###}:</td>';
			$pdfContentPage .= '<td>' . $arrThisRecipientAdress["USE"]["name"] . '</td>';
			$pdfContentPage .= '</tr>';
			$pdfContentPage .= '<tr>';
			$pdfContentPage .= '<td>{###STREET###}:</td>';
			$pdfContentPage .= '<td>' . $arrThisRecipientAdress["USE"]["street"] . ' ' .  $arrThisRecipientAdress["USE"]["streetNumber"] . '</td>';
			$pdfContentPage .= '</tr>';
			$pdfContentPage .= '<tr>';
			$pdfContentPage .= '<td>{###ZIPCODE###} / {###CITY###}:</td>';
			$pdfContentPage .= '<td>' . $arrThisRecipientAdress["USE"]["zipcode"] . ' ' .  $arrThisRecipientAdress["USE"]["city"] . '</td>';
			$pdfContentPage .= '</tr>';
			$pdfContentPage .= '<tr>';
			$pdfContentPage .= '<td>{###COUNTRY###}:</td>';
			$pdfContentPage .= '<td>' . $arrThisRecipientAdress["USE"]["country"] . '</td>';
			$pdfContentPage .= '</tr>';
			$pdfContentPage .= '</table>';

			if($_POST["arrSelectOrderPaymentTypeCashOnDelivery"][$thisSelectOrderIdKey] == "1"){
				$pdfContentPage .= '<p style="color:#FF0000;border:1mm solid #FF0000;padding: 2mm;"><b>NO DPD !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!</b></p>';
			}

			$pdfContentPage .= '';
			#$pdfContentPage .= '<hr>';
			$pdfContentPage .= '</page>';
		// EOF PAGE 1: DATAS

		// BOF PAGE 2: LAYOUT
			// UPLOADED FILE FILE BE MERGED WITH PAGE 1
		// EOF PAGE 2: LAYOUT

		// BOF INCLUDE LANGUAGE
			if(!empty($arrLanguageData)){
				foreach($arrLanguageData as $arrLanguageDataKey => $arrLanguageDataValue){
					$pattern = '{###' . $arrLanguageDataKey . '###}';

					if(mb_detect_encoding($arrLanguageDataValue) == "UTF-8"){
						#$arrLanguageDataValue = (utf8_decode($arrLanguageDataValue));
					}
					#$arrLanguageDataValue = htmlentities($arrLanguageDataValue);
					// BOF
						#$thisLocalType = strtolower($_POST["sendAttachedMailLanguage"]) . '_' . strtoupper($_POST["sendAttachedMailLanguage"]);
						setlocale(LC_CTYPE, $thisLocalType); // cp1254
						setlocale(LC_ALL, $thisLocalType); // cp1254


						#iconv_set_encoding("internal_encoding", "UTF-8");
						#iconv_set_encoding("output_encoding", "cp1254");
						#$arrLanguageDataValue = iconv('UTF-8', 'ISO8859-9', $arrLanguageDataValue);
						#$arrLanguageDataValue = iconv('UTF-8', 'ASCII//TRANSLIT', $arrLanguageDataValue);
						#$arrLanguageDataValue = iconv('UTF-8', 'cp1254', $arrLanguageDataValue);
						#$arrLanguageDataValue = iconv('UTF-8', 'ISO8859-9', $arrLanguageDataValue);
					// EOF

					$replace = ($arrLanguageDataValue);
					$pdfContentPage = preg_replace('/' . $pattern . '/', ($replace), $pdfContentPage);
				}
			}
		// EOF INCLUDE LANGUAGE

		$pdfContentPage = removeUnnecessaryChars($pdfContentPage);
		// BOF CREATE PDF
			require_once(DIRECTORY_HTML2PDF . 'html2pdf.class.php');
			// utf8
			try {
				#$html2pdf = new HTML2PDF('L', 'A4', strtolower($_POST["sendAttachedMailLanguage"]), true, 'utf8', array(22, 5, 5, 5));
				$html2pdf = new HTML2PDF('L', 'A4', strtolower($thisSelectLanguageID), true, 'utf8', array(22, 5, 5, 5));

				if(file_exists($thisPrintProductionDirPath . $thisCreatedPdfName)) {
					unlink($thisPrintProductionDirPath . $thisCreatedPdfName);
				}
				clearstatcache();

				// BOF TEST PAGE SPLIT
					if($userDatas["usersLogin"] != 'xxxthorsten'){
						$pdfContentPage = preg_replace('/#pdfContentArea/', '', $pdfContentPage);
						$pdfContentPage = preg_replace('/<div id="pdfContentArea">/', '', $pdfContentPage);
						$pdfContentPage = preg_replace('/<\/div><page_header>/ismU', '<page_header>', $pdfContentPage);
					}
				// BOF TEST PAGE SPLIT

				ob_start();
				echo $pdfContentPage;
				$contentPDF = ob_get_clean();

				$html2pdf->writeHTML($contentPDF, isset($_GET['vuehtml']));
				//echo $thisPrintProductionDirPath;die();
				$html2pdf->Output($thisCreatedPdfName, 'F', $thisPrintProductionDirPath);
				
				if(file_exists($thisCreatedPdfName)) {
					unlink($thisCreatedPdfName);
				}
				clearstatcache();
			}
			catch(HTML2PDF_exception $e) {
				echo $e;
				exit;
			}

			if(file_exists($thisPrintProductionDirPath . $thisCreatedPdfName)) {
				#$successMessage .= 'Die PDF-Datei &quot;' . $thisCreatedPdfName . '&quot wurde generiert. '.'<br />';
				$showPdfDownloadLink = false;
			}
			else {
				$errorMessage .= 'Die PDF-Datei &quot;' . $thisCreatedPdfName . '&quot konnte nicht generiert werden. '.'<br />';
			}
			clearstatcache();
		// BOF CREATE PDF

		// BOF CREATE ZIP FILE
			#$createCommand = ZIP_PATH.' a ' . $thisPrintProductionDirPath . basename($sendAttachedMailProductionDataZipName) . ' ' . $thisPrintProductionDirPath . $thisCreatedPdfName . '';
			#$createCommand = ZIP_PATH.' a ' . $thisPrintProductionDirPath . $sendAttachedMailProductionDataZipName . ' ' . $thisPrintProductionDirPath . '';
			#$createCommand = ZIP_PATH.' a ' . BASEPATH . $thisPrintProductionDirPath . $sendAttachedMailProductionDataZipName . ' ' . BASEPATH . $thisPrintProductionDirPath . '';
			$createCommand = ZIP_PATH.' a ' . BASEPATH . $thisPrintProductionDirPath . $sendAttachedMailProductionDataZipName . ' ' . BASEPATH . $thisPrintProductionDirPath . ' -mx=7';
			$arrExecResults[] = $createCommand;
			$arrExecResults[] = exec($createCommand, $error);
			if(!empty($error)) {
				$arrExecResults[] = array('1. ZIP: error', $error);
			}

			if(file_exists($thisPrintProductionDirPath . $thisCreatedPdfName)){
				unlink($thisPrintProductionDirPath . $thisCreatedPdfName);
			}
			if(file_exists($thisPrintProductionDirPath . $sendAttachedMailLayoutFileName)){
				unlink($thisPrintProductionDirPath . $sendAttachedMailLayoutFileName);
			}

		// EOF CREATE ZIP FILE
		#dd('arrExecResults');
	// EOF CREATE PDF CONTENT


	// BOF SEND MAIL WITH ATTACHED DOCUMENT
	if($_POST["sendAttachedDocument"] == '1') {
		$thisCreatedPdfName = $thisPrintProductionDirPath . $sendAttachedMailProductionDataZipName;
		if(file_exists($thisCreatedPdfName)){
			chmod($thisCreatedPdfName, 0777);
		}
		$generatedDocumentNumber = $sendAttachedMailProductionDataZipName;

		require_once(DIRECTORY_HTML_MIME_MAIL . 'htmlMimeMail.php');
		require_once(DIRECTORY_MAIL_TEMPLATES . 'template_BASIC.inc.php');
		require_once(DIRECTORY_MAIL_TEMPLATES . 'template_TEXT_DATAS.inc.php');
		#require_once("inc/mail.inc.php");

		// BOF SEND MAIL
		require_once("classes/createMail.class.php");

		// BOF CREATE SUBJECT
		$thisSubject = $_POST["selectSubject"];
		if($_POST["sendAttachedMailSubject"] != '') {
			$thisSubject = $_POST["sendAttachedMailSubject"] . ', ' . $thisSubject;
		}
		// EOF CREATE SUBJECT

		// BOF GET ATTACHED FILE

		$arrPathInfo = pathinfo($thisCreatedPdfName);

		$pathDocumentFolder = $arrPathInfo["dirname"] . "/";
		$thisCreatedPdfName = $arrPathInfo["basename"];

		$arrAttachmentFiles = array ();
		if(SEND_EXTERN_PRODUCTION_DATA_PER_MAIL){
			$arrAttachmentFiles = array (
				rawurldecode($pathDocumentFolder . $thisCreatedPdfName) => str_replace(" ", "_", rawurldecode(myUnHtmlEntities($thisCreatedPdfName)))
			);
		}
		// EOF GET ATTACHED FILE

		$thisSubject .= " - " . $arrPathInfo["filename"];
		$thisDocumentType = "PA";
		$generatedDocumentNumber = "";
		$arrThisMailContentDatas = $arrMailContentDatas["DEFAULT"];

		if(SEND_EXTERN_PRODUCTION_INFO_PER_MAIL || SEND_EXTERN_PRODUCTION_DATA_PER_MAIL){
			$createMail = new createMail(
								$thisSubject,													// TEXT:	MAIL_SUBJECT
								$thisDocumentType,												// STRING:	DOCUMENT TYPE
								$generatedDocumentNumber,										// STRING:	DOCUMENT NUMBER
								$arrAttachmentFiles,											// ARRAY:	ATTACHMENT FILES
								$_POST["selectCustomersRecipientMail"],	 						// STRING:	RECIPIENTS
								$arrThisMailContentDatas,										// MAIL_TEXT_TEMPLATE
								$mailTemplate["html"],											// MAIL_BODY_TEMPLATE
								'',																// STRING:	ADDITIONAL TEXT
								$_POST["sendAttachedMailText"],									// STRING:	SEND ATTACHED TEXT
								true,															// BOOLEAN:	SEND ATTACHED DOCUMENT
								DIRECTORY_MAIL_IMAGES,											// DIRECTORY_MAIL_IMAGES
								$_POST["selectMailtextSender"]									// STRING: SENDER
							);

			$createMailResult = $createMail->returnResult();
			$sendMailToClient = $createMailResult;
		}
		else {
			$sendMailToClient = true;
		}
		// EOF SEND MAIL

		#if(!$sendMailToUs) { $errorMessage .= ' Die Mail-Kopie konnte nicht an ' . implode(', ', $arrOrderMail) . ' versendet werden! '.'<br />'; }
		#else { $successMessage .= ' Die Mail-Kopie wurde an ' . implode(', ', $arrOrderMail) . ' versendet! '.'<br />'; }

		if(!$sendMailToClient) {
			if(SEND_EXTERN_PRODUCTION_INFO_PER_MAIL || SEND_EXTERN_PRODUCTION_DATA_PER_MAIL) {
				$errorMessage .= ' Der Produktionsauftrag (ID: ' . $_POST["orderID"] . ') &quot;KNR-' . $_POST["editCustomerNumber"] . '&quot; konnte nicht an ' . $_POST["selectCustomersRecipientName"] . ' (' . $_POST["selectCustomersRecipientMail"] . ') versendet werden! '.'<br />';
			}
		}
		else {
			if(SEND_EXTERN_PRODUCTION_INFO_PER_MAIL || SEND_EXTERN_PRODUCTION_DATA_PER_MAIL) {
				$successMessage .= ' Der Produktionsauftrag (ID: ' . $_POST["orderID"] . ') &quot;KNR-' . $_POST["editCustomerNumber"] . '&quot; wurde an ' . $_POST["selectCustomersRecipientName"] . ' (' . $_POST["selectCustomersRecipientMail"] . ') versendet! '.'<br />';
			}
			// BOF SET PRODUCTION PRINTING PLANT
				$sql = "UPDATE
							`" . TABLE_ORDERS . "`
							SET
								`ordersProductionPrintingPlant` = 'TR',
								`ordersStatus` = '7'
								/* , `ordersProduktionsDatum` = '" . date("Y-m-d") . "' */
							WHERE 1 AND (
								 `ordersID` = '" . $_POST["orderID"] . "'
							)
					";
				$rs = $dbConnection->db_query($sql);
				if(!$rs){
					$errorMessage .= ' Der Produktionsstandort konnte nicht gespeichert werden !' . '<br />';
					echo mysqli_error();
				}
				else {
					$successMessage .= ' Der Produktionsstandort wurde gespeichert! ' . '<br />';
				}
			// EOF SET PRODUCTION PRINTING PLANT
		}
		unset($_POST["exportOrdersVertreterID"]);
	}
	// EOF SEND MAIL WITH ATTACHED DOCUMENT


	// BOF STORE DATAS IN DB
	if($_POST["sendAttachedDocument"] == '1' && $sendMailToClient) {
		$thisOrderLayoutFileDate = '';

		$thisProductionsTransferProductionStatus = '1';
		$thisProductionsTransferProductionNotice = '';

		$arrPathinfo = pathinfo($_REQUEST["downloadFile"]);
		$thisDownloadFile = $arrPathinfo["basename"];
		$thisDownloadFileDir = $arrPathinfo["dirname"] . "/";
		$thisDownloadFileExt = $arrPathinfo["extension"];

		$thisOrderRecipientAddress = '';
		$thisOrderRecipientAddress .= $arrThisRecipientAdress["USE"]["name"];
		$thisOrderRecipientAddress .= "\n";
		$thisOrderRecipientAddress .= $arrThisRecipientAdress["USE"]["street"] . ' ' .  $arrThisRecipientAdress["USE"]["streetNumber"];
		$thisOrderRecipientAddress .= "\n";
		$thisOrderRecipientAddress .= $arrThisRecipientAdress["USE"]["zipcode"] . ' ' .  $arrThisRecipientAdress["USE"]["city"];
		$thisOrderRecipientAddress .= "\n";
		$thisOrderRecipientAddress .= $arrThisRecipientAdress["USE"]["country"];

		$thisOrderSenderAddress = '';
		$thisOrderSenderAddress .= $arrThisSenderAdress["USE"]["name"];
		$thisOrderSenderAddress .= "\n";
		$thisOrderSenderAddress .= $arrThisSenderAdress["USE"]["street"] . ' ' .  $arrThisSenderAdress["USE"]["streetNumber"];
		$thisOrderSenderAddress .= "\n";
		$thisOrderSenderAddress .= $arrThisSenderAdress["USE"]["zipcode"] . ' ' .  $arrThisSenderAdress["USE"]["city"];
		$thisOrderSenderAddress .= "\n";
		$thisOrderSenderAddress .= $arrThisSenderAdress["USE"]["country"];

		$sql_storeTransferDatas = "
				REPLACE INTO `" . TABLE_PRODUCTIONS_TRANSFER . "` (
					`ordersProductionsTransferID`,

					`ordersProductionsTransferOrdersID`,

					`ordersProductionsTransferDeliveryRedDot`,
					`ordersProductionsTransferDeliveryNoDPD`,
					`ordersProductionsTransferDeliveryContainerDate`,

					`ordersProductionsTransferMailSubmitDateTime`,
					`ordersProductionsTransferDeliveryPrintDate`,
					`ordersProductionsTransferMailLanguage`,
					`ordersProductionsTransferMailSubject`,
					`ordersProductionsTransferMailRecipient`,

					`ordersProductionsTransferLayoutFileName`,
					`ordersProductionsTransferLayoutFileSize`,
					`ordersProductionsTransferLayoutFileType`,

					`ordersProductionsTransferProductionFileName`,
					`ordersProductionsTransferProductionFileSize`,

					`ordersProductionsTransferOrdersCustomerNumber`,
					`ordersProductionsTransferOrdersCustomerName`,

					`ordersProductionsTransferOrdersPrintText`,
					`ordersProductionsTransferOrdersKomission`,

					`ordersProductionsTransferOrdersProductNumber`,
					`ordersProductionsTransferOrdersProductCategory`,
					`ordersProductionsTransferOrdersProductID`,
					
					`ordersProductionsTransferOrdersProductName`,
					`ordersProductionsTransferOrdersProductQuantity`,
					`ordersProductionsTransferOrdersProductOriginalQuantity`,

					`ordersProductionsTransferOrdersColorsQuantity`,
					`ordersProductionsTransferOrdersColorsNames`,
					`ordersProductionsTransferOrdersWithClearPaint`,

					`ordersProductionsTransferOrderRecipientCustomerNumber`,
					`ordersProductionsTransferOrderRecipientAddress`,

					`ordersProductionsTransferOrderSenderCustomerNumber`,
					`ordersProductionsTransferOrderSenderAddress`,

					`ordersProductionsTransferUserID`,

					`ordersProductionsTransferProductionPrinter`,
					`ordersProductionsTransferProductionStatus`,
					`ordersProductionsTransferProductionTrackingNumbers`,
					`ordersProductionsTransferProductionNotice`,

					`ordersProductionsTransferProductionModified`,

					`ordersProductionsTransferSuccess_MYSQL`,
					`ordersProductionsTransferSuccess_FTP`,

					`ordersProductionsTransferMandatory`,

					`ordersProductionsTransferOrdersPrintType`,

					`ordersProductionsTransferUseNoNeutralPacking`,
					
					`ordersProductionsTransferPrintPlateNumber`,
					`ordersProductionsTransferNotice`,
					
					`ordersProductionsTransferOrderType`
				)
				VALUES (
					'%',

					'" . $_POST["orderID"] . "',

					'" . $_POST["sendAttachedMailRedDot"] . "',
					'" . $_POST["arrSelectOrderPaymentTypeCashOnDelivery"][$thisSelectOrderIdKey] . "',
					'',

					NOW(),
					'',
					'" . $thisSelectLanguageID . "',
					'" . $thisSubject . "',
					'" . $_POST["selectCustomersRecipientMail"] . "',

					'" . addslashes($sendAttachedMailLayoutFileName) . "',
					'" . $_FILES["sendAttachedMailFile"]["size"] . "',
					'" . $_FILES["sendAttachedMailFile"]["type"] . "',

					'" . ($thisPrintProductionDirPath . $sendAttachedMailProductionDataZipName) . "',
					'" . filesize($thisPrintProductionDirPath . $sendAttachedMailProductionDataZipName) . "',

					'" . $thisPrintProductionDataValue["ordersKundennummer"] . "',
					'" . addslashes($thisPrintProductionDataValue["ordersKundenName"]) . "',

					'" . addslashes($arrProductDatas["ordersAufdruck"]) . "',
					'" . addslashes($arrProductDatas["ordersKommission"]) . "',

					'" . addslashes($arrProductDatas["ordersArtikelNummer"]) . "',
					'" . addslashes($arrProductDatas["ordersArtikelKategorieID"]) . "',
					'" . addslashes($arrProductDatas["ordersArtikelID"]) . "',
					'" . addslashes($arrProductDatas["ordersArtikelBezeichnung"]) . "',
					'" . $arrProductDatas["ordersArtikelMenge"] . "',
					'" . $arrProductDatas["ordersArtikelOriginalMenge"] . "',

					'" . $arrProductDatas["ordersArtikelPrintColorsCount"] . "',
					'" . addslashes($arrProductDatas["ordersDruckFarbe"]) . "',
					'" . $arrProductDatas["ordersArtikelWithClearPaint"] . "',

					'" . $arrThisRecipientAdress["USE"]["knr"] . "',
					'" . addslashes($thisOrderRecipientAddress) . "',

					'" . $arrThisSenderAdress["USE"]["knr"] . "',
					'" . addslashes( $thisOrderSenderAddress) . "',

					'" . $_SESSION["usersID"] . "',

					'',
					'" . $thisProductionsTransferProductionStatus . "',
					'',
					'" . addslashes($thisProductionsTransferProductionNotice) . "',

					NOW(),

					'0',
					'0',

					'" . $_POST["sendAttachedMailMandatory"] . "',

					'" . $arrProductDatas["ordersPrintType"] . "',

					'" . $_POST["sendAttachedMailUseNoNeutralPacking"] . "',
					
					'" . $arrProductDatas["ordersPrintingPlateNumber"] . "',
					'',
					
					'" . $arrProductDatas["ordersOrderType"] . "'
				);
			";
			
		$rs_storeTransferDatas = mysqli_query($sql_storeTransferDatas, $db_open);
		echo mysqli_error();
		if($rs_storeTransferDatas){
			$successMessage .= ' Die gesendeten Produktionsdaten wurden gespeichert.' .'<br />';
		}
		else {
			$errorMessage .= ' Die gesendeten Produktionsdaten konnten nicht  gespeichert werden.' .'<br />';
		}
	}
	// EOF STORE DATAS IN DB

	// DATAS SUBMITTET WITH CRONJOB "Auftragslisten/cronjobs/cronTransferProductionDatasToOnline.php"
	// TO PRODUCTION ONLINE
?>