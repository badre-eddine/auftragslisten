<?php

echo '<h2>Kunden-Import ins Vertriebs-Modul</h2>';


if(DB_HOST_EXTERN_DISTRIBUTION != '' && DB_NAME_EXTERN_DISTRIBUTION != '' && DB_USER_EXTERN_DISTRIBUTION != '' && DB_PASSWORD_EXTERN_DISTRIBUTION != ''){

	$dbConnection_ExternDistribution = new DB_Connection(DB_HOST_EXTERN_DISTRIBUTION, '', DB_NAME_EXTERN_DISTRIBUTION, DB_USER_EXTERN_DISTRIBUTION, DB_PASSWORD_EXTERN_DISTRIBUTION);
	$dbOpen_ExternDistribution = $dbConnection_ExternDistribution->db_connect();

	$thisTempTable = "_SICH" . date("YmdHis") . "_" . TABLE_RELATED_DOCUMENTS . "";

	// BOF GET ALL CUSTUMER DATAS
		$sql_local = "
				SELECT
					`common_customers`.`customersID`,
					`common_customers`.`customersKundennummer`,
					`common_customers`.`customersFirmenname`,
					`common_customers`.`customersFirmennameZusatz`,
					`common_customers`.`customersFirmenInhaberVorname`,
					`common_customers`.`customersFirmenInhaberNachname`,
					`common_customers`.`customersFirmenInhaberAnrede`,
					`common_customers`.`customersFirmenInhaber2Vorname`,
					`common_customers`.`customersFirmenInhaber2Nachname`,
					`common_customers`.`customersFirmenInhaber2Anrede`,
					`common_customers`.`customersAnsprechpartner1Vorname`,
					`common_customers`.`customersAnsprechpartner1Nachname`,
					`common_customers`.`customersAnsprechpartner1Anrede`,
					`common_customers`.`customersAnsprechpartner2Vorname`,
					`common_customers`.`customersAnsprechpartner2Nachname`,
					`common_customers`.`customersAnsprechpartner2Anrede`,
					`common_customers`.`customersTelefon1`,
					`common_customers`.`customersTelefon2`,
					`common_customers`.`customersMobil1`,
					`common_customers`.`customersMobil2`,
					`common_customers`.`customersFax1`,
					`common_customers`.`customersFax2`,
					`common_customers`.`customersMail1`,
					`common_customers`.`customersMail2`,
					`common_customers`.`customersHomepage`,
					`common_customers`.`customersCompanyStrasse`,
					`common_customers`.`customersCompanyHausnummer`,
					`common_customers`.`customersCompanyCountry`,
					`common_customers`.`customersCompanyPLZ`,
					`common_customers`.`customersCompanyOrt`,
					`common_customers`.`customersLieferadresseFirmenname`,
					`common_customers`.`customersLieferadresseFirmennameZusatz`,
					`common_customers`.`customersLieferadresseStrasse`,
					`common_customers`.`customersLieferadresseHausnummer`,
					`common_customers`.`customersLieferadressePLZ`,
					`common_customers`.`customersLieferadresseOrt`,
					`common_customers`.`customersLieferadresseLand`,
					`common_customers`.`customersRechnungsadresseFirmenname`,
					`common_customers`.`customersRechnungsadresseFirmennameZusatz`,
					`common_customers`.`customersRechnungsadresseStrasse`,
					`common_customers`.`customersRechnungsadresseHausnummer`,
					`common_customers`.`customersRechnungsadressePLZ`,
					`common_customers`.`customersRechnungsadresseOrt`,
					`common_customers`.`customersRechnungsadresseLand`,
					`common_customers`.`customersBankName`,
					`common_customers`.`customersKontoinhaber`,
					`common_customers`.`customersBankKontonummer`,
					`common_customers`.`customersBankLeitzahl`,
					`common_customers`.`customersBankIBAN`,
					`common_customers`.`customersBankBIC`,
					`common_customers`.`customersBezahlart`,
					`common_customers`.`customersZahlungskondition`,
					`common_customers`.`customersRabatt`,
					`common_customers`.`customersRabattType`,
					`common_customers`.`customersUseProductMwst`,
					`common_customers`.`customersUseProductDiscount`,
					`common_customers`.`customersUstID`,
					`common_customers`.`customersVertreterID`,
					`common_customers`.`customersVertreterName`,
					`common_customers`.`customersUseSalesmanDeliveryAdress`,
					`common_customers`.`customersUseSalesmanInvoiceAdress`,
					`common_customers`.`customersTyp`,
					`common_customers`.`customersGruppe`,
					`common_customers`.`customersNotiz`,
					`common_customers`.`customersUserID`,
					`common_customers`.`customersTimeCreated`,
					`common_customers`.`customersActive`,
					`common_customers`.`customersDatasUpdated`,
					`common_customers`.`customersTaxAccountID`,
					`common_customers`.`customersInvoicesNotPurchased`,
					`common_customers`.`customersBadPaymentBehavior`,
					`common_customers`.`customersEntryDate`
				FROM `common_customers`

				LEFT JOIN `common_countries`
				ON(`common_customers`.`customersLieferadresseLand` = `common_countries`.`countries_id`)

				WHERE 1

				ORDER BY `common_customers`.`customersID`


			";
	// EOF GET ALL CUSTUMER DATAS

	$rs_local = $dbConnection->db_query($sql_local);

	if($rs_local){
		$successMessage .= ' Die Kunden-Daten werden aus dem Auftragslisten ausgelesen. ' . '<br />';
	}
	else {
		$errorMessage .= ' Die Kunden-Daten k&ouml;nnen nicht aus dem Auftragslisten ausgelesen werden. ' . '<br />';
	}

	while($ds_local = mysqli_fetch_assoc($rs_local)){
		foreach(array_keys($ds_local) as $field){
			$ds_local[$field] = addslashes($ds_local[$field]);
		}

		$customerExists = true;
		// BOF CHECK IF CUSTOMER EXISTS
			$sql_extern_check = "
				SELECT
					`accountid`,
					`account_no`
					FROM `vtiger_account`
					WHERE 1
						AND `account_no` = '" . $ds_local["customersKundennummer"] . "'
					LIMIT 1
			";
			$rs_extern_check = $dbConnection_ExternDistribution->db_query($sql_extern_check);

			if($rs_extern_check){
				$successMessage .= ' Die Existenz des Auftragslisten-Kunden-Datensatzes (KNR ' . $ds_local["customersKundennummer"] . ') wurde im Vertriebs-Modul gecheckt. ' . '<br />';
			}
			else {
				$errorMessage .= ' Die Existenz des Auftragslisten-Kunden-Datensatzes (KNR ' . $ds_local["customersKundennummer"] . ') konnte nicht im Vertriebs-Modul gecheckt werden. ' . '<br />';
			}

			if($dbConnection_ExternDistribution->db_getMysqlNumRows($rs_extern_check) == 0){
				$customerExists = false;
				$warningMessage .= ' Der Auftragslisten-Kunden-Datensatzes (KNR ' . $ds_local["customersKundennummer"] . ') existiert noch nicht im Vertriebs-Modul. ' . '<br />';
			}
			else {
				$infoMessage .= ' Der Auftragslisten-Kunden-Datensatzes (KNR ' . $ds_local["customersKundennummer"] . ') existiert bereits im Vertriebs-Modul. ' . '<br />';
			}


		// EOF CHECK IF CUSTOMER EXISTS

		// BOF INSERT CUSTOMER DATA
			if($customerExists == false){
				$thisMail = '';
				if($ds_local["customersFax1"] != ''){ $thisFax = $ds_local["customersFax1"]; }
				else if($ds_local["customersFax2"] != ''){ $thisFax = $ds_local["customersFax2"]; }

				// UPDATE `vtiger_crmentity` SET `deleted` = '1' WHERE `setype` = 'Accounts';
				// DELETE FROM `vtiger_crmentity` WHERE `deleted` = '1';

				// BOF GET ITEM ID
					$sql_extern_get = "
						SELECT
							MAX(`crmid`) + 1

							FROM `vtiger_crmentity`
							WHERE 1

					";
					$rs_extern_get = $dbConnection_ExternDistribution->db_query($sql_extern_get);
					list($thisItemID) = mysqli_fetch_array($rs_extern_get);
				// EOF GET ITEM ID


				$sql_extern_0 = "
					INSERT INTO `vtiger_crmentity` (
						`crmid`,
						`smcreatorid`,
						`smownerid`,
						`modifiedby`,
						`setype`,
						`description`,
						`createdtime`,
						`modifiedtime`,
						`viewedtime`,
						`status`,
						`version`,
						`presence`,
						`deleted`
					)
					VALUES(
						'" . $thisItemID . "',
						1,
						1,
						1,
						'Accounts',
						'',
						NOW(),
						NOW(),
						NOW(),
						NULL,
						0,
						1,
						0
					)
				";
				$rs_extern_0 = $dbConnection_ExternDistribution->db_query($sql_extern_0);

				$sql_extern_1 = "
					REPLACE INTO `vtiger_account` (
						`accountid`,
						`account_no`,
						`accountname`,
						`parentid`,
						`account_type`,
						`industry`,
						`annualrevenue`,
						`rating`,
						`ownership`,
						`siccode`,
						`tickersymbol`,
						`phone`,
						`otherphone`,
						`email1`,
						`email2`,
						`website`,
						`fax`,
						`employees`,
						`emailoptout`,
						`notify_owner`
					)
					VALUES (
						'" . $thisItemID . "',
						'" . $ds_local["customersKundennummer"] . "',
						'" . $ds_local["customersFirmenname"] . "',
						'0',
						'',
						'Other',
						'0',
						'',
						'',
						'',
						'',
						'" . $ds_local["customersTelefon1"] . "',
						'" . $ds_local["customersTelefon2"] . "',
						'" . $ds_local["customersMail1"] . "',
						'" . $ds_local["customersMail2"] . "',

						'" . $ds_local["customersHomepage"] . "',
						'" . $thisFax . "',
						'0',
						'0',
						'0'
					);
				";
				$rs_extern_1 = $dbConnection_ExternDistribution->db_query($sql_extern_1);

				$sql_extern_2 = "
					REPLACE INTO `vtiger_accountscf` (
						`accountid`
					)
					VALUES (
						'" . $thisItemID . "'
					)
				";
				$rs_extern_2 = $dbConnection_ExternDistribution->db_query($sql_extern_2);


				$sql_extern_3 = "
					REPLACE INTO `vtiger_accountbillads` (
						`accountaddressid`,
						`bill_city`,
						`bill_code`,
						`bill_country`,
						`bill_state`,
						`bill_street`,
						`bill_pobox`
					)
					VALUES (
						'" . $thisItemID . "',
						'" . $ds_local["customersRechnungsadresseOrt"] . "',
						'" . $ds_local["customersRechnungsadressePLZ"] . "',
						'" . $ds_local["countries_name_DE"] . "',
						'',
						'" . $ds_local["customersRechnungsadresseStrasse"] . " " . $ds_local["customersRechnungsadresseHausnummer"] . "',
						''
					)
				";
				$rs_extern_3 = $dbConnection_ExternDistribution->db_query($sql_extern_3);

				$sql_extern_4 = "
					REPLACE INTO `vtiger_accountshipads` (
						`accountaddressid`,
						`ship_city`,
						`ship_code`,
						`ship_country`,
						`ship_state`,
						`ship_pobox`,
						`ship_street`
					)
					VALUES (
						'" . $thisItemID . "',
						'" . $ds_local["customersLieferadresseOrt"] . "',
						'" . $ds_local["customersLieferadressePLZ"] . "',
						'" . $ds_local["countries_name_DE"] . "',
						'',
						'',
						'" . $ds_local["customersLieferadresseStrasse"] . " " . $ds_local["customersLieferadresseHausnummer"] . "'
					)
				";
				$rs_extern_4 = $dbConnection_ExternDistribution->db_query($sql_extern_4);

				if($rs_extern_1 && $rs_extern_2 && $rs_extern_3 && $rs_extern_4){
					$successMessage .= ' Der Auftragslisten-Kunden-Datensatzes (KNR ' . $ds_local["customersKundennummer"] . ') wurde ins Vertriebs-Modul importiert. ' . '<br />';
				}
				else {
					$errorMessage .= ' Der Auftragslisten-Kunden-Datensatzes (KNR ' . $ds_local["customersKundennummer"] . ') konnte nicht im Vertriebs-Modul importiert werden. ' . '<br />';
				}
				displayMessages();
				echo '<hr />';
				flush();
			}
		// EOF INSERT CUSTOMER DATA
	}

	// BOF UPDATE LAST ID
		$sql_extern_5 = "UPDATE
					`vtiger_crmentity_seq`
					SET `id` = (
						SELECT
								MAX(`crmid`)

								FROM `vtiger_crmentity`
								WHERE 1
					);
			"
		$rs_extern_5 = $dbConnection_ExternDistribution->db_query($sql_extern_5);
	// EOF UPDATE LAST ID
}
?>