<noscript>
	<div class="errorArea">
		Achtung! Um alle Funktionen nutzen zu k&ouml;nnen, m&uuml;ssen Sie Javascript aktivieren!
	</div>
</noscript>

<script language="javascript" type="text/javascript">
	/* <![CDATA[ */
	if(!navigator.cookieEnabled)
	{
		document.write('<div class="errorArea">');
			document.write('Achtung! Um alle Funktionen nutzen zu k&ouml;nnen, m&uuml;ssen Sie Cookies zulassen!');
		document.write('</div>');
	}
	/* ]]> */
</script>