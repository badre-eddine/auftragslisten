<?php
echo '<h2>Kunden-Import ANAG</h2>';
<?php
	exit;
	require_once('config/configBasic.inc.php');
	require_once('classes/DB_Connection.class.php');
	$dbConnection = new DB_Connection();
	$db_open = $dbConnection->db_connect();

	$sql = "UPDATE 	`common_customers`
					INNER JOIN `kunden_anag`
					ON( `kunden_anag`.`E-Mail_TDL` = `common_customers`.`customersMail1` OR `kunden_anag`.`E-Mail_TDL` = `common_customers`.`customersMail2`)

					SET `common_customers`.`customersGruppe` = '3'

					WHERE 1
						AND `kunden_anag`.`E-Mail_TDL` != ''
		";

	$rs = $dbConnection->db_query($sql);
	echo 'BETROFFENE ZEILEN: ' . $dbConnection->db_getMysqlAffectedRows($rs);

	$sql = "UPDATE 	`common_customers`
					INNER JOIN `kunden_anag`
					ON(
							`kunden_anag`.`PLZ` = `common_customers`.`customersCompanyPLZ`
							AND
							`kunden_anag`.`Ort` = `common_customers`.`customersCompanyOrt`
							AND
							`kunden_anag`.`Straße` = `common_customers`.`customersCompanyStrasse`
							AND
							REPLACE(REPLACE(`kunden_anag`.`Straße`, 'Straße', 'str.'), ' ', '')  =  REPLACE(CONCAT (`common_customers`.`customersCompanyStrasse`, `common_customers`.`customersCompanyHausnummer`), ' ', '')
						)

					SET `common_customers`.`customersGruppe` = '3'

					WHERE 1
	";


	$rs = $dbConnection->db_query($sql);
	echo 'BETROFFENE ZEILEN: ' . $dbConnection->db_getMysqlAffectedRows($rs);


	$sql = "TRUNCATE TABLE IF EXISTS `_temp_common_customers_anag` ;";
	$rs = $dbConnection->db_query($sql);

	$sql = "INSERT INTO `_temp_common_customers_anag`(
				`customersID`,
				`customersKundennummer`,
				`customersFirmenname`,
				`customersFirmennameZusatz`,
				`customersFirmenInhaberVorname`,
				`customersFirmenInhaberNachname`,
				`customersFirmenInhaberAnrede`,
				`customersFirmenInhaber2Vorname`,
				`customersFirmenInhaber2Nachname`,
				`customersFirmenInhaber2Anrede`,
				`customersAnsprechpartner1Vorname`,
				`customersAnsprechpartner1Nachname`,
				`customersAnsprechpartner1Anrede`,
				`customersAnsprechpartner1Typ`,
				`customersAnsprechpartner2Vorname`,
				`customersAnsprechpartner2Nachname`,
				`customersAnsprechpartner2Anrede`,
				`customersAnsprechpartner2Typ`,
				`customersTelefon1`,
				`customersTelefon2`,
				`customersMobil1`,
				`customersMobil2`,
				`customersFax1`,
				`customersFax2`,
				`customersMail1`,
				`customersMail2`,
				`customersHomepage`,
				`customersCompanyStrasse`,
				`customersCompanyHausnummer`,
				`customersCompanyCountry`,
				`customersCompanyPLZ`,
				`customersCompanyOrt`,
				`customersLieferadresseFirmenname`,
				`customersLieferadresseFirmennameZusatz`,
				`customersLieferadresseStrasse`,
				`customersLieferadresseHausnummer`,
				`customersLieferadressePLZ`,
				`customersLieferadresseOrt`,
				`customersLieferadresseLand`,
				`customersRechnungsadresseFirmenname`,
				`customersRechnungsadresseFirmennameZusatz`,
				`customersRechnungsadresseStrasse`,
				`customersRechnungsadresseHausnummer`,
				`customersRechnungsadressePLZ`,
				`customersRechnungsadresseOrt`,
				`customersRechnungsadresseLand`,
				`customersBankName`,
				`customersKontoinhaber`,
				`customersBankKontonummer`,
				`customersBankLeitzahl`,
				`customersBankIBAN`,
				`customersBankBIC`,
				`customersBezahlart`,
				`customersZahlungskondition`,
				`customersRabatt`,
				`customersRabattType`,
				`customersUseProductMwst`,
				`customersUseProductDiscount`,
				`customersUstID`,
				`customersVertreterID`,
				`customersVertreterName`,
				`customersUseSalesmanDeliveryAdress`,
				`customersUseSalesmanInvoiceAdress`,
				`customersTyp`,
				`customersGruppe`,
				`customersNotiz`,
				`customersUserID`,
				`customersTimeCreated`,
				`customersActive`,
				`customersDatasUpdated`,
				`customersTaxAccountID`,
				`customersInvoicesNotPurchased`,
				`customersBadPaymentBehavior`,
				`customersEntryDate`
				)
				SELECT
					`ANAG_Kd-Nr`,
					'',
					`Firma`,
					'',
					'',
					`Gesellschafter`,
					'',
					'',
					'',
					'',
					'',
					`TDL`,
					'',
					'',
					'',
					'',
					'',
					'',
					`Telefon_TDL`,
					'',
					'',
					'',
					`Fax_TDL`,
					'',
					`E-Mail_TDL`,
					'',
					'',
					`Straße`,
					'',
					'81',
					`PLZ`,
					`Ort`,
					'',
					'',
					'',
					'',
					'',
					'',
					'',
					'',
					'',
					'',
					'',
					'',
					'',
					'',
					'',
					'',
					'',
					'',
					'',
					'',
					'',
					'',
					'',
					'',
					'',
					'',
					'',
					'',
					'',
					'',
					'',
					'1',
					'3',
					'',
					'0',
					NOW(),
					'0',
					'',
					'',
					'0',
					'0',
					NOW()
				FROM `kunden_anag`
				ORDER BY `PLZ`";
	$rs = $dbConnection->db_query($sql);

	$sql = "SELECT * FROM `_temp_common_customers_anag` WHERE 1";
	$rs = $dbConnection->db_query($sql);


	while($ds = mysqli_fetch_array($rs)){

		$generatedCustomerNumber = generateCustomerNumber('customers', '1', $ds["customersCompanyCountry"], $ds["customersCompanyPLZ"]){
		$customersTaxAccountID = getNewCustomerTaxAccountNumber($ds["customersCompanyCountry"], $arrCountryTypeDatas[$ds["customersCompanyCountry"]]["countries_iso_code_2"], '1');

		echo $generatedCustomerNumber . '<br />';
		echo $customersTaxAccountID . '<br />';

		$sql_insertData = "
				INSERT INTO `common_customers`(
					`customersID`,
					`customersKundennummer`,
					`customersFirmenname`,
					`customersFirmennameZusatz`,
					`customersFirmenInhaberVorname`,
					`customersFirmenInhaberNachname`,
					`customersFirmenInhaberAnrede`,
					`customersFirmenInhaber2Vorname`,
					`customersFirmenInhaber2Nachname`,
					`customersFirmenInhaber2Anrede`,
					`customersAnsprechpartner1Vorname`,
					`customersAnsprechpartner1Nachname`,
					`customersAnsprechpartner1Anrede`,
					`customersAnsprechpartner1Typ`,
					`customersAnsprechpartner2Vorname`,
					`customersAnsprechpartner2Nachname`,
					`customersAnsprechpartner2Anrede`,
					`customersAnsprechpartner2Typ`,
					`customersTelefon1`,
					`customersTelefon2`,
					`customersMobil1`,
					`customersMobil2`,
					`customersFax1`,
					`customersFax2`,
					`customersMail1`,
					`customersMail2`,
					`customersHomepage`,
					`customersCompanyStrasse`,
					`customersCompanyHausnummer`,
					`customersCompanyCountry`,
					`customersCompanyPLZ`,
					`customersCompanyOrt`,
					`customersLieferadresseFirmenname`,
					`customersLieferadresseFirmennameZusatz`,
					`customersLieferadresseStrasse`,
					`customersLieferadresseHausnummer`,
					`customersLieferadressePLZ`,
					`customersLieferadresseOrt`,
					`customersLieferadresseLand`,
					`customersRechnungsadresseFirmenname`,
					`customersRechnungsadresseFirmennameZusatz`,
					`customersRechnungsadresseStrasse`,
					`customersRechnungsadresseHausnummer`,
					`customersRechnungsadressePLZ`,
					`customersRechnungsadresseOrt`,
					`customersRechnungsadresseLand`,
					`customersBankName`,
					`customersKontoinhaber`,
					`customersBankKontonummer`,
					`customersBankLeitzahl`,
					`customersBankIBAN`,
					`customersBankBIC`,
					`customersBezahlart`,
					`customersZahlungskondition`,
					`customersRabatt`,
					`customersRabattType`,
					`customersUseProductMwst`,
					`customersUseProductDiscount`,
					`customersUstID`,
					`customersVertreterID`,
					`customersVertreterName`,
					`customersUseSalesmanDeliveryAdress`,
					`customersUseSalesmanInvoiceAdress`,
					`customersTyp`,
					`customersGruppe`,
					`customersNotiz`,
					`customersUserID`,
					`customersTimeCreated`,
					`customersActive`,
					`customersDatasUpdated`,
					`customersTaxAccountID`,
					`customersInvoicesNotPurchased`,
					`customersBadPaymentBehavior`,
					`customersEntryDate`
				)
				VALUES (
					'%',
					'".$generatedCustomerNumber."',
					'".$ds["customersFirmenname"]."',
					'".$ds["customersFirmennameZusatz"]."',
					'".$ds["customersFirmenInhaberVorname"]."',
					'".$ds["customersFirmenInhaberNachname"]."',
					'".$ds["customersFirmenInhaberAnrede"]."',
					'".$ds["customersFirmenInhaber2Vorname"]."',
					'".$ds["customersFirmenInhaber2Nachname"]."',
					'".$ds["customersFirmenInhaber2Anrede"]."',
					'".$ds["customersAnsprechpartner1Vorname"]."',
					'".$ds["customersAnsprechpartner1Nachname"]."',
					'".$ds["customersAnsprechpartner1Anrede"]."',
					'".$ds["customersAnsprechpartner1Typ"]."',
					'".$ds["customersAnsprechpartner2Vorname"]."',
					'".$ds["customersAnsprechpartner2Nachname"]."',
					'".$ds["customersAnsprechpartner2Anrede"]."',
					'".$ds["customersAnsprechpartner2Typ"]."',
					'".$ds["customersTelefon1"]."',
					'".$ds["customersTelefon2"]."',
					'".$ds["customersMobil1"]."',
					'".$ds["customersMobil2"]."',
					'".$ds["customersFax1"]."',
					'".$ds["customersFax2"]."',
					'".$ds["customersMail1"]."',
					'".$ds["customersMail2"]."',
					'".$ds["customersHomepage"]."',
					'".$ds["customersCompanyStrasse"]."',
					'".$ds["customersCompanyHausnummer"]."',
					'".$ds["customersCompanyCountry"]."',
					'".$ds["customersCompanyPLZ"]."',
					'".$ds["customersCompanyOrt"]."',
					'".$ds["customersLieferadresseFirmenname"]."',
					'".$ds["customersLieferadresseFirmennameZusatz"]."',
					'".$ds["customersLieferadresseStrasse"]."',
					'".$ds["customersLieferadresseHausnummer"]."',
					'".$ds["customersLieferadressePLZ"]."',
					'".$ds["customersLieferadresseOrt"]."',
					'".$ds["customersLieferadresseLand"]."',
					'".$ds["customersRechnungsadresseFirmenname"]."',
					'".$ds["customersRechnungsadresseFirmennameZusatz"]."',
					'".$ds["customersRechnungsadresseStrasse"]."',
					'".$ds["customersRechnungsadresseHausnummer"]."',
					'".$ds["customersRechnungsadressePLZ"]."',
					'".$ds["customersRechnungsadresseOrt"]."',
					'".$ds["customersRechnungsadresseLand"]."',
					'".$ds["customersBankName"]."',
					'".$ds["customersKontoinhaber"]."',
					'".$ds["customersBankKontonummer"]."',
					'".$ds["customersBankLeitzahl"]."',
					'".$ds["customersBankIBAN"]."',
					'".$ds["customersBankBIC"]."',
					'".$ds["customersBezahlart"]."',
					'".$ds["customersZahlungskondition"]."',
					'".$ds["customersRabatt"]."',
					'".$ds["customersRabattType"]."',
					'".$ds["customersUseProductMwst"]."',
					'".$ds["customersUseProductDiscount"]."',
					'".$ds["customersUstID"]."',
					'".$ds["customersVertreterID"]."',
					'".$ds["customersVertreterName"]."',
					'".$ds["customersUseSalesmanDeliveryAdress"]."',
					'".$ds["customersUseSalesmanInvoiceAdress"]."',
					'".$ds["customersTyp"]."',
					'".$ds["customersGruppe"]."',
					'".$ds["customersNotiz"]."',
					'".$ds["customersUserID"]."',
					'".$ds["customersTimeCreated"]."',
					'".$ds["customersActive"]."',
					'".$ds["customersDatasUpdated"]."',
					'".$customersTaxAccountID."',
					'".$ds["customersInvoicesNotPurchased"]."',
					'".$ds["customersBadPaymentBehavior"]."',
					'".$ds["customersEntryDate"]."'
				)
			";
			echo $sql_insertData . '<br />';
			$rs_insertData = $dbConnection->db_query($sql_insertData);
			echo mysqli_error();
		echo '<hr />';
	}
?>
</body>
<html>