<?php
	session_start();

	require_once("../config/configMandator.inc.php");
	require_once("../config/configParams_" . strtolower(MANDATOR) . ".inc.php");
	require_once("../config/configBasic.inc.php");
	require_once("../config/configFiles.inc.php");
	require_once("../config/configTables.inc.php");
	require_once("../inc/functions.inc.php");

	$content = "";

	$warningMessage = "";
	$errorMessage = "";
	$successMessage = "";
	$infoMessage = "";

	// BOF GER USER MAIL SETTINGS
	$userDatas = getUserDatas();
	$arrMailDatas = unserialize($userDatas["userMailDatas"]);
	// EOF GER USER MAIL SETTINGS

	// BOF DEFINE MAIL TEMPLATES
	$arrMailTextTemplates = array();

	$arrMailTextTemplates["DEFAULT"] = array();
	$arrMailTextTemplates["DEFAULT"]["TEXT"] = "Sehr geehrte Damen und Herren,\n\n";
	$arrMailTextTemplates["DEFAULT"]["SUBJECT"] = "";
	$arrMailTextTemplates["DEFAULT"]["OPTION"] = "Standard Textnachricht";

	$arrMailTextTemplates["LS"] = array();
	$arrMailTextTemplates["LS"]["TEXT"] = "Sehr geehrte Damen und Herren,\n\nanbei die Bestellung laut angehängtem Lieferschein mit der Bitte um neutrale Lieferung an den im Lieferschein genannten Kunden.";
	$arrMailTextTemplates["LS"]["SUBJECT"] = "Bestellung";
	$arrMailTextTemplates["LS"]["OPTION"] = "Externe Bestellung";

	$arrMailTextTemplates["SX"] = array();
	$arrMailTextTemplates["SX"]["TEXT"] = "Sehr geehrte Damen und Herren,\n\nanbei die Liste mit offenen Bestellungen im PDF-Format.";
	$arrMailTextTemplates["SX"]["SUBJECT"] = "Offene Bestellungen";
	$arrMailTextTemplates["SX"]["OPTION"] = "Export Vertreterliste";

	$arrMailTextTemplates["IS"] = array();
	$arrMailTextTemplates["IS"]["TEXT"] = "Sehr geehrte Damen und Herren,\n\nanbei die Montageanleitung im PDF-Format.";
	$arrMailTextTemplates["IS"]["SUBJECT"] = "Montageanleitung";
	$arrMailTextTemplates["IS"]["OPTION"] = "Montageanleitung";

	$arrMailTextTemplates["DF"] = array();
	$arrMailTextTemplates["DF"]["TEXT"] = "Sehr geehrte Damen und Herren,\n\nanbei die Datei.";
	$arrMailTextTemplates["DF"]["SUBJECT"] = "Datei";
	$arrMailTextTemplates["DF"]["OPTION"] = "Sonstige Datei";

	$arrMailTextTemplates["DC"] = array();
	$arrMailTextTemplates["DC"]["TEXT"] = "Sehr geehrte Damen und Herren,\n\nanbei der Katalog.";
	$arrMailTextTemplates["DC"]["SUBJECT"] = "Katalog";
	$arrMailTextTemplates["DC"]["OPTION"] = "Katalog";

	$arrMailTextTemplates["MF"] = array();
	$arrMailTextTemplates["MF"]["TEXT"] = "Sehr geehrte Damen und Herren,\n\nanbei das Handbuch im PDF-Format.";
	$arrMailTextTemplates["MF"]["SUBJECT"] = "Handbuch";
	$arrMailTextTemplates["MF"]["OPTION"] = "Handbuch";

	$arrMailTextTemplates["KA"] = array();
	$arrMailTextTemplates["KA"]["TEXT"] = "Sehr geehrte Damen und Herren,\n\nanbei der Korrekturabzug mit der Bitte um Überprüfung und schriftlicher Freigabe per Mail oder per Fax.";
	$arrMailTextTemplates["KA"]["SUBJECT"] = "Korrekturabzug";
	$arrMailTextTemplates["KA"]["OPTION"] = "Korrekturabzug";

	$arrMailTextTemplates["AN"] = array();
	$arrMailTextTemplates["AN"]["TEXT"] = "Sehr geehrte Damen und Herren,\n\nanbei das Angebot.";
	$arrMailTextTemplates["AN"]["SUBJECT"] = "Angebot";
	$arrMailTextTemplates["AN"]["OPTION"] = "Angebot";

	$arrMailTextTemplates["AB"] = array();
	$arrMailTextTemplates["AB"]["TEXT"] = "Sehr geehrte Damen und Herren,\n\nanbei die Auftragsbestätigung.";
	$arrMailTextTemplates["AB"]["SUBJECT"] = "Auftragsbestätigung";
	$arrMailTextTemplates["AB"]["OPTION"] = "Auftragsbestätigung";

	$arrMailTextTemplates["GU"] = array();
	$arrMailTextTemplates["GU"]["TEXT"] = "Sehr geehrte Damen und Herren,\n\nanbei die Gutschrift.";
	$arrMailTextTemplates["GU"]["SUBJECT"] = "Gutschrift";
	$arrMailTextTemplates["GU"]["OPTION"] = "Gutschrift";

	$arrMailTextTemplates["RE"] = array();
	$arrMailTextTemplates["RE"]["TEXT"] = "Sehr geehrte Damen und Herren,\n\nanbei die Rechnung.";
	$arrMailTextTemplates["RE"]["SUBJECT"] = "Rechnung";
	$arrMailTextTemplates["RE"]["OPTION"] = "Rechnung";

	$arrMailTextTemplates["PR"] = array();
	$arrMailTextTemplates["PR"]["TEXT"] = "Sehr geehrte Damen und Herren,\n\nanbei die Provisionsgutschrift.";
	$arrMailTextTemplates["PR"]["SUBJECT"] = "Provisionsgutschrift";
	$arrMailTextTemplates["PR"]["OPTION"] = "Provisionsgutschrift";

	$arrMailTextTemplates["MA"] = array();
	$arrMailTextTemplates["MA"]["TEXT"] = "Sehr geehrte Damen und Herren,\n\nanbei die Zahlungserinnerung.";
	$arrMailTextTemplates["MA"]["SUBJECT"] = "Zahlungserinnerung";
	$arrMailTextTemplates["MA"]["OPTION"] = "Zahlungserinnerung";

	$arrMailTextTemplates["M1"] = array();
	$arrMailTextTemplates["M1"]["TEXT"] = "Sehr geehrte Damen und Herren,\n\nanbei die 1. Mahnung.";
	$arrMailTextTemplates["M1"]["SUBJECT"] = "1. Mahnung";
	$arrMailTextTemplates["M1"]["OPTION"] = "1. Mahnung";

	$arrMailTextTemplates["M2"] = array();
	$arrMailTextTemplates["M2"]["TEXT"] = "Sehr geehrte Damen und Herren,\n\nanbei die 2. Mahnung.";
	$arrMailTextTemplates["M2"]["SUBJECT"] = "2. Mahnung";
	$arrMailTextTemplates["M2"]["OPTION"] = "2. Mahnung";

	$arrMailTextTemplates["M3"] = array();
	$arrMailTextTemplates["M3"]["TEXT"] = "Sehr geehrte Damen und Herren,\n\nanbei die 2. Mahnung.";
	$arrMailTextTemplates["M3"]["SUBJECT"] = "2. Mahnung";
	$arrMailTextTemplates["M3"]["OPTION"] = "2. Mahnung";

	$arrMailTextTemplates["EX"] = array();
	$arrMailTextTemplates["EX"]["TEXT"] = "Sehr geehrte Damen und Herren,\n\nanbei die exportierten Daten.";
	$arrMailTextTemplates["EX"]["SUBJECT"] = "Rechnungsexport";
	$arrMailTextTemplates["EX"]["OPTION"] = "Export Steuerberater";

	$arrMailTextTemplates["SP"] = array();
	$arrMailTextTemplates["SP"]["TEXT"] = "Sehr geehrte Damen und Herren,\n\nanbei das SEPA-Formular, das Sie bitte ausgefüllt an uns zurücksenden.";
	$arrMailTextTemplates["SP"]["SUBJECT"] = "SEPA-Formular";
	$arrMailTextTemplates["SP"]["OPTION"] = "SEPA-Formular";

	$arrMailTextTemplates["BB"] = array();
	$arrMailTextTemplates["BB"]["TEXT"] = "Sehr geehrte Damen und Herren,\n\nanbei der Belichtungsbogen.";
	$arrMailTextTemplates["BB"]["SUBJECT"] = "Belichtungsbogen / Pozlama-Filmi";
	$arrMailTextTemplates["BB"]["OPTION"] = "Belichtungsbogen";

	$arrMailTextTemplates["PA"] = array();
	$arrMailTextTemplates["PA"]["TEXT"] = "Sehr geehrte Damen und Herren,\n\nanbei der Produktionsauftrag.";
	$arrMailTextTemplates["PA"]["SUBJECT"] = "Produktions-Daten / üretim bilgileri";
	$arrMailTextTemplates["PA"]["OPTION"] = "Produktionsauftrag";
	// EOF DEFINE MAIL TEMPLATES

	// BOF DEFINE MAIL ADDRESSES
	$arrMailAddresses = array();
	if($userDatas["usersLogin"] != 'xxxthorsten'){
		$arrUserMailDatas = unserialize($userDatas["userMailDatas"]);
		if(!empty($arrUserMailDatas)){
			$i = 1;
			foreach($arrUserMailDatas as $thisKey => $thisValue){
				if($arrUserMailDatas[$thisKey]["mailAddress"] != ''){
					if($thisKey == MANDATOR){
						$arrMailAddresses[0]["MAIL"] = $arrUserMailDatas[$thisKey]["mailAddress"];
						$arrMailAddresses[0]["NAME"] = $userDatas["usersFirstName"] . ' ' . $userDatas["usersLastName"] . ' | ' . strtoupper($thisKey);
					}
					else {
						$arrMailAddresses[$i]["MAIL"] = $arrUserMailDatas[$thisKey]["mailAddress"];
						$arrMailAddresses[$i]["NAME"] = $userDatas["usersFirstName"] . ' ' . $userDatas["usersLastName"] . ' | ' . strtoupper($thisKey);
						$i++;
					}
				}
			}
			if($_COOKIE["isAdmin"] == '1'){
				#$arrMailAddresses[$i]["MAIL"] = "finanzen@burhan-ctr.de";
				#$arrMailAddresses[$i]["NAME"] = "Burhan CTR";
			}
		}
	}
	// EOF DEFINE MAIL ADDRESSES

	$arrReturnDatas = array();
	$arrReturnDatas["mailUserMailDatas"] = $arrMailDatas;
	$arrReturnDatas["mailTemplates"] = $arrMailTextTemplates;
	$arrReturnDatas["mailAddresses"] = $arrMailAddresses;



	$content = json_encode($arrReturnDatas);

	echo $content;
?>