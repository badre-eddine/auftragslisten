<?php
	session_start();

	require_once('../config/configMandator.inc.php');
	require_once('../config/configParams_' . strtolower(MANDATOR) . '.inc.php');
	require_once('../config/configBasic.inc.php');
	require_once('../config/configFiles.inc.php');
	require_once('../config/configTables.inc.php');
	require_once('../inc/functions.inc.php');
	##require_once('header.inc.php');

	$content = '';


	if(trim($_GET["salesmanID"]) != ''){
		$thisSalesmanID = trim($_GET["salesmanID"]);
	}
	else if(trim($_GET["customerID"]) != ''){
		$thisSalesmanID = trim($_GET["customerID"]);
	}
	else if(trim($_GET["salesmanNumber"]) != ''){
		$thisSalesmanCustomerNumber = trim($_GET["salesmanNumber"]);
	}
	else if(trim($_GET["customerNumber"]) != ''){
		$thisSalesmanCustomerNumber = trim($_GET["customerNumber"]);
	}

	$thisTypeAdress = trim($_GET["typeAdress"]);

	$arrSearchResults = array();

	$warningMessage = '';
	$errorMessage = '';
	$successMessage = '';
	$infoMessage = '';

	$dbConnection = new DB_Connection();
	$db_open = $dbConnection->db_connect();

	$where = "";

	if($thisSalesmanID != ''){
		$where = " AND `customersID` = " . $thisSalesmanID . " ";
	}
	else if($thisSalesmanCustomerNumber != ''){
		$where = " AND `customersKundennummer` = '" . $thisSalesmanCustomerNumber . "' ";
	}

	$xxx_sql = "
		SELECT

			`customersID`,
			`customersKundennummer`,
			`customersFirmenname`,

			`customersCompanyStrasse`,
			`customersCompanyHausnummer`,
			`customersCompanyCountry`,
			`customersCompanyPLZ`,
			`customersCompanyOrt`,

			IF(`customersMail1` IS NOT NULL AND `customersMail1`  != '', `customersMail1`,
				IF(`customersMail2` IS NOT NULL AND `customersMail2`  != '', `customersMail2`,
					''
				)
			) AS `customersMail`,

			`customersFirmenname` AS `customersLieferadresseFirmenname`,
			`customersFirmennameZusatz` AS `customersLieferadresseFirmennameZusatz`,
			`customersLieferadresseStrasse`,
			`customersLieferadresseHausnummer`,
			`customersLieferadressePLZ`,
			`customersLieferadresseOrt`,
			`customersLieferadresseLand`,

			`customersFirmenname` AS `customersRechnungsadresseFirmenname`,
			`customersFirmennameZusatz` AS `customersRechnungsadresseFirmennameZusatz`,
			`customersRechnungsadresseStrasse`,
			`customersRechnungsadresseHausnummer`,
			`customersRechnungsadressePLZ`,
			`customersRechnungsadresseOrt`,
			`customersRechnungsadresseLand`

			FROM `" . TABLE_CUSTOMERS . "`

			WHERE 1
				" . $where . "

			LIMIT 1
	";


	$sql = "
		SELECT

			`customersID`,
			`customersKundennummer`,
			`customersFirmenname`,

			`customersCompanyStrasse`,
			`customersCompanyHausnummer`,
			`customersCompanyCountry`,
			`customersCompanyPLZ`,
			`customersCompanyOrt`,

			IF(`customersMail1` IS NOT NULL AND `customersMail1`  != '', `customersMail1`,
				IF(`customersMail2` IS NOT NULL AND `customersMail2`  != '', `customersMail2`,
					''
				)
			) AS `customersMail`,

			`customersGroupCompanyNumber`,

			`customersLieferadresseKundennummer`,
			`customersLieferadresseFirmenname`,
			`customersLieferadresseFirmennameZusatz`,
			`customersLieferadresseStrasse`,
			`customersLieferadresseHausnummer`,
			`customersLieferadressePLZ`,
			`customersLieferadresseOrt`,
			`customersLieferadresseLand`,

			`customersRechnungsadresseKundennummer`,
			`customersRechnungsadresseFirmenname`,
			`customersRechnungsadresseFirmennameZusatz`,
			`customersRechnungsadresseStrasse`,
			`customersRechnungsadresseHausnummer`,
			`customersRechnungsadressePLZ`,
			`customersRechnungsadresseOrt`,
			`customersRechnungsadresseLand`

			FROM `" . TABLE_CUSTOMERS . "`

			WHERE 1
				" . $where . "

			LIMIT 1
		";

	$rs = $dbConnection->db_query($sql);

	while($ds = mysqli_fetch_assoc($rs)) {
		$arrSearchResults['customer']['ID']				= $ds["customersID"];
		$arrSearchResults['customer']['Number']			= utf8_encode($ds["customersKundennummer"]);
		$arrSearchResults['customer']['Name']			= $ds["customersFirmenname"];

		$arrSearchResults['company']['customerNumber']	= $ds["customersKundennummer"];

		$arrSearchResults['company']['street']			= $ds["customersCompanyStrasse"];
		$arrSearchResults['company']['number']			= $ds["customersCompanyHausnummer"];
		$arrSearchResults['company']['country']			= $ds["customersCompanyCountry"];
		$arrSearchResults['company']['zipcode']			= $ds["customersCompanyPLZ"];
		$arrSearchResults['company']['city']			= $ds["customersCompanyOrt"];
		$arrSearchResults['customer']['mail']			= $ds["customersMail"];
		$arrSearchResults['customer']['groupCompanyNumber']	= $ds["customersGroupCompanyNumber"];

		/*
		$arrSearchResults['invoice']['customerNumber']	= $ds["customersLieferadresseKundennummer"];
		$arrSearchResults['invoice']['company']			= $ds["customersLieferadresseFirmenname"];
		$arrSearchResults['invoice']['companyAdd']		= $ds["customersLieferadresseFirmennameZusatz"];
		$arrSearchResults['invoice']['street']			= $ds["customersLieferadresseStrasse"];
		$arrSearchResults['invoice']['number']			= $ds["customersLieferadresseHausnummer"];
		$arrSearchResults['invoice']['country']			= $ds["customersLieferadresseLand"];
		$arrSearchResults['invoice']['zipcode']			= $ds["customersLieferadressePLZ"];
		$arrSearchResults['invoice']['city']			= $ds["customersLieferadresseOrt"];
		$arrSearchResults['invoice']['mail']			= $ds["customersMail"];

		$arrSearchResults['delivery']['customerNumber']	= $ds["customersRechnungsadresseKundennummer"];
		$arrSearchResults['delivery']['company']		= $ds["customersRechnungsadresseFirmenname"];
		$arrSearchResults['delivery']['companyAdd']		= $ds["customersRechnungsadresseFirmennameZusatz"];
		$arrSearchResults['delivery']['street']			= $ds["customersRechnungsadresseStrasse"];
		$arrSearchResults['delivery']['number']			= $ds["customersRechnungsadresseHausnummer"];
		$arrSearchResults['delivery']['country']		= $ds["customersRechnungsadresseLand"];
		$arrSearchResults['delivery']['zipcode']		= $ds["customersRechnungsadressePLZ"];
		$arrSearchResults['delivery']['city']			= $ds["customersRechnungsadresseOrt"];
		$arrSearchResults['delivery']['mail']			= $ds["customersMail"];
		*/

		$arrSearchResults['delivery']['customerNumber']	= $ds["customersLieferadresseKundennummer"];
		$arrSearchResults['delivery']['company']		= $ds["customersLieferadresseFirmenname"];
		$arrSearchResults['delivery']['companyAdd']		= $ds["customersLieferadresseFirmennameZusatz"];
		$arrSearchResults['delivery']['street']			= $ds["customersLieferadresseStrasse"];
		$arrSearchResults['delivery']['number']			= $ds["customersLieferadresseHausnummer"];
		$arrSearchResults['delivery']['country']		= $ds["customersLieferadresseLand"];
		$arrSearchResults['delivery']['zipcode']		= $ds["customersLieferadressePLZ"];
		$arrSearchResults['delivery']['city']			= $ds["customersLieferadresseOrt"];
		$arrSearchResults['delivery']['mail']			= $ds["customersMail"];
		$arrSearchResults['delivery']['groupCompanyNumber']	= $ds["customersGroupCompanyNumber"];

		$arrSearchResults['invoice']['customerNumber']	= $ds["customersRechnungsadresseKundennummer"];
		$arrSearchResults['invoice']['company']			= $ds["customersRechnungsadresseFirmenname"];
		$arrSearchResults['invoice']['companyAdd']		= $ds["customersRechnungsadresseFirmennameZusatz"];
		$arrSearchResults['invoice']['street']			= $ds["customersRechnungsadresseStrasse"];
		$arrSearchResults['invoice']['number']			= $ds["customersRechnungsadresseHausnummer"];
		$arrSearchResults['invoice']['country']			= $ds["customersRechnungsadresseLand"];
		$arrSearchResults['invoice']['zipcode']			= $ds["customersRechnungsadressePLZ"];
		$arrSearchResults['invoice']['city']			= $ds["customersRechnungsadresseOrt"];
		$arrSearchResults['invoice']['mail']			= $ds["customersMail"];
		$arrSearchResults['invoice']['groupCompanyNumber']	= $ds["customersGroupCompanyNumber"];
	}

	if(!empty($arrSearchResults)) {
		/*
		foreach($arrSearchResults as $thisType => $thisTypeData){
			foreach($thisTypeData as $thisFieldKey => $thisFieldValue){
				$arrSearchResults[$thisType][$thisFieldKey] = ($thisFieldValue);
			}
		}
		*/
		// $jsonSearchResults = createJson2($arrSearchResults);
		$jsonSearchResults = json_encode($arrSearchResults);
	}
	if($dbConnection->db_displayErrors() != "") {
		$errorMessage .= $dbConnection->db_displayErrors(). '<br />';
	}

	if($dbConnection) {
		$dbConnection->db_close();
	}

	$content = $jsonSearchResults;

	#$fp = fopen("testLoad.txt", 'w');
	#fwrite($fp, addslashes($jsonSearchResults));
	#fwrite($fp, $arrSearchResults['customer']['Name'] . "\n");
	#fwrite($fp, mb_detect_encoding($arrSearchResults['customer']['Name']) . "\n");
	#fclose($fp);

	echo $content;
?>