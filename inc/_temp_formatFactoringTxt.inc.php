<?php
	set_time_limit(0);
	echo '<h2>csv formatieren</h2>';

	#$pathFileSource = '_bctr_auszahlung_anhang_re/bctr_auszahlung_anhang_re_edit/';
	#$pathFileTarget = '_bctr_auszahlung_anhang_re/bctr_auszahlung_anhang_re_format/';

	$pathFileSource = '_b3_auszahlung_anhang_re/b3_auszahlung_anhang_re_edit/';
	$pathFileTarget = '_b3_auszahlung_anhang_re/b3_auszahlung_anhang_re_format/';

	$pathFileSource = '_' . strtolower(MANDATOR) . '_auszahlung_anhang_re/b3_auszahlung_anhang_re_edit/';
	$pathFileTarget = '_' . strtolower(MANDATOR) . '_auszahlung_anhang_re/b3_auszahlung_anhang_re_format/';


	$dir = $pathFileSource;

	// BOF OPEN A DIRECTORY, AND READ ITS CONTENTS
		$arrFilenamesSource = array();
		if (is_dir($dir)){
			if ($dh = opendir($dir)){
				while (($file = readdir($dh)) !== false){
					if($file != "." && $file != ".."){
						$arrFilenamesSource[] = $file;
					}
				}
				closedir($dh);
			}
		}
		#dd('dir');
		#dd('arrFilenamesSource');
	// EOF OPEN A DIRECTORY, AND READ ITS CONTENTS

	// BOF READ AND CONVERT
		$countFile = 0;
		if(!empty($arrFilenamesSource)){
			#dd('arrFilenamesSource');
			foreach($arrFilenamesSource as $thisFileSource){

				// BOF INIT FIELD VALUES
					$importFilename = "";
					$importDate = "";
					$deb_id_vom_kreditor = "";
					$trans_kz = "";
					$sortierung = "";
					$rechnung_nr = "";
					$rechnung_betrag = "";
					$sperrkonto_prz = "";
					$rechnung_datum = "";
					$buchungsbetrag_sk_index_5_1 = "";
					$buchungsbetrag_sk_index_5_2 = "";
					$summe_hw_kre = "";
					$az_nummer = "";
					$factoringart = "";
					$kreditoren_id = "";
					$kreditorenname_1 = "";
					$simulation = "";
					$buchungsbetrag_sk_index_5_3 = "";
					$rechnungs_id = "";
					$auszahlung_datum = "";
					$fir_re_nummer = "";
					$firmen_id = "";
					$buchungsbetrag_sk_index_5_4 = "";
					$buchungsbetrag_sonst_hw_kre = "";
					$buchungsbetrag_sk_index_5_5 = "";
					$anzeigeformat = "";
					$anzeigeformat2 = "";
					$buchungsbetrag_sk_index_5_6 = "";
				// EOF INIT FIELD VALUES

				dd('thisFileSource');
				$fp_fileSource = fopen($pathFileSource . $thisFileSource, 'r');

				if(file_exists($pathFileTarget . $thisFileSource)){
					unlink($pathFileTarget . $thisFileSource);
				}
				$fp_fileTarget = fopen($pathFileTarget . $thisFileSource, 'a+');

				$countLine = 0;
				$current_deb_id_vom_kreditor = '';
				$current_rechnung_betrag = '';
				$current_sperrkonto_prz = '';
				while($line_fileSource = fgets($fp_fileSource)){

					$newLineContent = $line_fileSource;
					dd('newLineContent');
					$newLineContent = preg_replace("/.*Seite.*/", "", $newLineContent);
					$newLineContent = preg_replace("/.*Rechnungsjournal.*/", "", $newLineContent);
					$newLineContent = preg_replace("/^Gesamt:.*/", "", $newLineContent);
					$newLineContent = preg_replace("/[ ]{2,}/", ";", $newLineContent);
					$newLineContent = trim($newLineContent);

					$importFilename = strtolower(MANDATOR) . '_' . $thisFileSource;
					$importDate = date("Y-m-d");

					if($newLineContent != ''){
						#dd('countLine');
						#dd('newLineContent');
						// BOF GET auszahlung_datum
							if(preg_match("/[0-9]{2}\.[0-9]{2}\.[0-9]{4}/", $newLineContent)){
								if($auszahlung_datum == ''){
									$auszahlung_datum = preg_replace("/([0-9]{2})\.([0-9]{2})\.([0-9]{4})/", "$3-$2-$1", $newLineContent);
									$auszahlung_datum .= " 00:00:00";
								}
							}
							#dd('auszahlung_datum');
							#dd('newLineContent');
						// EOF GET auszahlung_datum

						// BOF GET az_nummer, kreditoren_id AND kreditorenname_1
							if(preg_match("/Anlage zur Auszahlung Nr:/", $newLineContent)){
								$arrTemp = explode(";", $newLineContent);
								$az_nummer = preg_replace("/[A-Za-z\.\-\: ]/", "", $arrTemp[1]);

								$strTemp = $arrTemp[0];
								$strTemp = preg_replace("/^([0-9]{1,})[ ]{1,}/", "$1;", $strTemp);
								$arrTemp = explode(";", $strTemp);
								$kreditoren_id = $arrTemp[0];
								$kreditorenname_1 = $arrTemp[1];
							}
						// EOF GET az_nummer, kreditoren_id AND kreditorenname_1

						// BOF GET DATA ROW
							// fields: Debitoren;Rechnung;Rechnung Sperr;Trans;eingereichte Forderungen;nicht angekaufte Forderungen;Sperrkonto;Warenstreit;Factoring Geb.;Sonstiges;Umsatzsteuer
							if(preg_match("/[RE|GU]-[0-9]{1,10}/", $newLineContent)){

								$newLineContent = trim($newLineContent);

								$arrDataFieldContents = explode(";", $newLineContent);

								if(!empty($arrDataFieldContents)){
									if($arrDataFieldContents[0] != '' && $current_deb_id_vom_kreditor != $arrDataFieldContents[0]){
										$current_deb_id_vom_kreditor = $arrDataFieldContents[0];
										$current_rechnung_betrag = $arrDataFieldContents[2];
										$current_sperrkonto_prz = $arrDataFieldContents[3];
									}
									if($arrDataFieldContents[0] == ""){
										$arrDataFieldContents[0] = $current_deb_id_vom_kreditor;
									}

									if(count($arrDataFieldContents) == 11){
										$arrTemp = array();
										$arrTemp[0] = $arrDataFieldContents[0];
										$arrTemp[1] = $arrDataFieldContents[1];

										$arrTemp[2] = $current_rechnung_betrag;
										$arrTemp[3] = $current_sperrkonto_prz;
										$arrTemp[4] = $arrDataFieldContents[2];
										$arrTemp[5] = $arrDataFieldContents[3];
										$arrTemp[6] = $arrDataFieldContents[4];
										$arrTemp[7] = $arrDataFieldContents[5];
										$arrTemp[8] = $arrDataFieldContents[6];
										$arrTemp[9] = $arrDataFieldContents[7];
										$arrTemp[10] = $arrDataFieldContents[8];
										$arrTemp[11] = $arrDataFieldContents[9];
										$arrTemp[12] = $arrDataFieldContents[10];

										$arrDataFieldContents = $arrTemp;
									}

									foreach($arrDataFieldContents as $thisDataFieldContentsKey => $thisDataFieldContentsValue){
										if(preg_match("/[0-9]{1,}\.[0-9]{1,},[0-9]{1,}/", $thisDataFieldContentsValue)){
											$thisDataFieldContentsValue = preg_replace("/\./", "", $thisDataFieldContentsValue);
										}
										if(preg_match("/[0-9]{1,},[0-9]{1,}/", $thisDataFieldContentsValue)){
											$thisDataFieldContentsValue = preg_replace("/,/", ".", $thisDataFieldContentsValue);
										}
										$arrDataFieldContents[$thisDataFieldContentsKey] = $thisDataFieldContentsValue;
									}
								}

								#dd('newLineContent');
								#dd('current_deb_id_vom_kreditor');
								#dd('arrDataFieldContents');

								$importFilename					= $importFilename;
								$importDate						= $importDate;
								$deb_id_vom_kreditor			= $arrDataFieldContents[0];
								$trans_kz						= $arrDataFieldContents[4];
								$sortierung						= $countLine;
								$rechnung_nr					= $arrDataFieldContents[1];
								$rechnung_betrag				= $arrDataFieldContents[2];
								$sperrkonto_prz					= $arrDataFieldContents[3];
								$rechnung_datum					= '0000-00-00';
								$buchungsbetrag_sk_index_5_1	= $arrDataFieldContents[5];
								$buchungsbetrag_sk_index_5_2	= $arrDataFieldContents[6];
								$summe_hw_kre					= $arrDataFieldContents[12];
								$az_nummer						= $az_nummer;
								$factoringart					= 'EF';
								$kreditoren_id					= $kreditoren_id;
								$kreditorenname_1				= $kreditorenname_1;
								$simulation						= "0";
								$buchungsbetrag_sk_index_5_3	= $arrDataFieldContents[7];
								$rechnungs_id					= '';
								$auszahlung_datum				= $auszahlung_datum;
								$fir_re_nummer					= '';
								$firmen_id						= "1";
								$buchungsbetrag_sk_index_5_4	= $arrDataFieldContents[8];
								$buchungsbetrag_sonst_hw_kre	= $arrDataFieldContents[10];
								$buchungsbetrag_sk_index_5_5	= $arrDataFieldContents[9];

								$anzeigeformat					= "#,##0.00'";
								$anzeigeformat2					= "#,##0.00'";

								$buchungsbetrag_sk_index_5_6	= $arrDataFieldContents[11];

								// BOF STORE NEW LINE
									$newLineStoreContent = $importFilename . ';' . $importDate . ';' . $deb_id_vom_kreditor . ';' . $trans_kz . ';' . $sortierung . ';' . $rechnung_nr . ';' . $rechnung_betrag . ';' . $sperrkonto_prz . ';' . $rechnung_datum . ';' . $buchungsbetrag_sk_index_5_1 . ';' . $buchungsbetrag_sk_index_5_2 . ';' . $summe_hw_kre . ';' . $az_nummer . ';' . $factoringart . ';' . $kreditoren_id . ';' . $kreditorenname_1 . ';' . $simulation . ';' . $buchungsbetrag_sk_index_5_3 . ';' . $rechnungs_id . ';' . $auszahlung_datum . ';' . $fir_re_nummer . ';' . $firmen_id . ';' . $buchungsbetrag_sk_index_5_4 . ';' . $buchungsbetrag_sonst_hw_kre . ';' . $buchungsbetrag_sk_index_5_5 . ';' . $anzeigeformat . ';' . $anzeigeformat2 . ';' . $buchungsbetrag_sk_index_5_6;
									dd('newLineStoreContent');
									fwrite($fp_fileTarget, $newLineStoreContent . "\n");
									$countLine++;
									echo '<hr />';
								// EOF STORE NEW LINE

								// BOF CATCHED VALUES
									/*
									dd('importFilename');
									dd('importDate');
									dd('deb_id_vom_kreditor');
									dd('trans_kz');
									dd('sortierung');
									dd('rechnung_nr');
									dd('rechnung_betrag');
									dd('sperrkonto_prz');
									dd('rechnung_datum');
									dd('buchungsbetrag_sk_index_5_1');
									dd('buchungsbetrag_sk_index_5_2');
									dd('summe_hw_kre');
									dd('az_nummer');
									dd('factoringart');
									dd('kreditoren_id');
									dd('kreditorenname_1');
									dd('simulation');
									dd('buchungsbetrag_sk_index_5_3');
									dd('rechnungs_id');
									dd('auszahlung_datum');
									dd('fir_re_nummer');
									dd('firmen_id');
									dd('buchungsbetrag_sk_index_5_4');
									dd('buchungsbetrag_sonst_hw_kre');
									dd('buchungsbetrag_sk_index_5_5');
									dd('anzeigeformat');
									dd('anzeigeformat2');
									dd('buchungsbetrag_sk_index_5_6');
									*/
								// EOF CATCHED VALUES
							}

						// EOF GET DATA ROW
					}
				}
				fclose($fp_fileSource);
				fclose($fp_fileTarget);

				$countFile++;
				if($countFile > 0){
					#break;
				}
				echo '<hr>';
				echo '<hr>';
				echo '<hr>';
			}
		}
	// EOF READ AND CONVERT
?>

