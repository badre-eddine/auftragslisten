<?php
	session_start();

	require_once('../config/configMandator.inc.php');
	require_once('../config/configParams_' . strtolower(MANDATOR) . '.inc.php');
	require_once('../config/configBasic.inc.php');
	require_once('../config/configFiles.inc.php');
	require_once('../config/configTables.inc.php');
	require_once('../inc/functions.inc.php');
	##require_once('header.inc.php');

	$thisSearchString = (trim($_GET["checkCustomerNumber"]));

	$content = '';

	$warningMessage = '';
	$errorMessage = '';
	$successMessage = '';
	$infoMessage = '';

	$useUf8Encode = false;

	if($thisSearchString != "") {
		$dbConnection = new DB_Connection();
		$db_open = $dbConnection->db_connect();

		$sql = "
			SELECT
				CONCAT (
					'ACHTUNG: <br />',
					'Diese Kundennummer (',
					`customersKundennummer`,
					') ist schon an den Kunden ',
					UPPER(`customersFirmenname`),
					' <br />vergeben!! '
				) AS `checkResult`

			FROM `" . TABLE_CUSTOMERS . "`
			WHERE `customersKundennummer` LIKE '" . $thisSearchString . "%'

			LIMIT 1
		";

		$rs = $dbConnection->db_query($sql);
		$thisNumRows = $dbConnection->db_getMysqlNumRows($rs);

		if($thisNumRows > 0) {
			list($checkResult) = mysqli_fetch_array($rs);
			$content = $checkResult;
		}
		else {
			$content = '';
		}

		if($dbConnection->db_displayErrors() != "") {
			$errorMessage .= $dbConnection->db_displayErrors(). '<br />';
		}

		$dbConnection->db_close();

		// $content = $sql;
		// $content = htmlentities($content);

		// $content = utf8_decode($content);
		// $content = htmlentities($content);

		// $content = $sql;

		echo $content;
	}

?>