<?php

if($thisAction == 'CREATE_SQL_CHANGE_CUSTOMER_NUMBERS'){

	echo '<h3>Kundennummern wechseln</h3>';
	$arrSqlQueries = array (
		// 0 => "SELECT `createdDocumentsFilename` FROM `" . TABLE_CREATED_DOCUMENTS . "` WHERE `createdDocumentsFilename` LIKE '%_KNR-{###CUSTOMER_NUMBER_OLD###}%';",
		1 => "UPDATE `" . TABLE_CREATED_DOCUMENTS . "` SET `createdDocumentsCustomerNumber` = '{###CUSTOMER_NUMBER_NEW###}' WHERE `createdDocumentsCustomerNumber` = '{###CUSTOMER_NUMBER_OLD###}';",
		// 2 => "UPDATE `bctr_createdDocuments` SET `createdDocumentsFilename` = '{###CUSTOMER_NUMBER_NEW###}' WHERE `createdDocumentsFilename` LIKE '%_KNR-{###CUSTOMER_NUMBER_OLD###}%';",

		3 => "UPDATE `" . TABLE_ORDER_CONFIRMATIONS . "` SET `orderDocumentsCustomerNumber` = '{###CUSTOMER_NUMBER_NEW###}' WHERE `orderDocumentsCustomerNumber` = '{###CUSTOMER_NUMBER_OLD###}';",
		4 => "UPDATE `" . TABLE_ORDER_CREDITS . "` SET `orderDocumentsCustomerNumber` = '{###CUSTOMER_NUMBER_NEW###}' WHERE `orderDocumentsCustomerNumber` = '{###CUSTOMER_NUMBER_OLD###}';",
		5 => "UPDATE `" . TABLE_ORDER_DELIVERIES . "` SET `orderDocumentsCustomerNumber` = '{###CUSTOMER_NUMBER_NEW###}' WHERE `orderDocumentsCustomerNumber` = '{###CUSTOMER_NUMBER_OLD###}';",
		6 => "UPDATE `" . TABLE_ORDER_INVOICES . "` SET `orderDocumentsCustomerNumber` = '{###CUSTOMER_NUMBER_NEW###}' WHERE `orderDocumentsCustomerNumber` = '{###CUSTOMER_NUMBER_OLD###}';",
		7 => "UPDATE `" . TABLE_ORDER_LETTERS . "` SET `orderDocumentsCustomerNumber` = '{###CUSTOMER_NUMBER_NEW###}' WHERE `orderDocumentsCustomerNumber` = '{###CUSTOMER_NUMBER_OLD###}';",
		8 => "UPDATE `" . TABLE_ORDER_OFFERS . "` SET `orderDocumentsCustomerNumber` = '{###CUSTOMER_NUMBER_NEW###}' WHERE `orderDocumentsCustomerNumber` = '{###CUSTOMER_NUMBER_OLD###}';",
		9 => "UPDATE `" . TABLE_ORDER_REMINDERS . "` SET `orderDocumentsCustomerNumber` = '{###CUSTOMER_NUMBER_NEW###}' WHERE `orderDocumentsCustomerNumber` = '{###CUSTOMER_NUMBER_OLD###}';",
		10 => "UPDATE `" . TABLE_ORDER_FIRST_DEMANDS . "` SET `orderDocumentsCustomerNumber` = '{###CUSTOMER_NUMBER_NEW###}' WHERE `orderDocumentsCustomerNumber` = '{###CUSTOMER_NUMBER_OLD###}';",
		11 => "UPDATE `" . TABLE_ORDER_SECOND_DEMANDS . "` SET `orderDocumentsCustomerNumber` = '{###CUSTOMER_NUMBER_NEW###}' WHERE `orderDocumentsCustomerNumber` = '{###CUSTOMER_NUMBER_OLD###}';",
		12 => "UPDATE `" . TABLE_ORDER_THIRD_DEMANDS . "` SET `orderDocumentsCustomerNumber` = '{###CUSTOMER_NUMBER_NEW###}' WHERE `orderDocumentsCustomerNumber` = '{###CUSTOMER_NUMBER_OLD###}';",

		13 => "UPDATE `" . TABLE_SALESMEN . "` SET `salesmenKundennummer` = '{###CUSTOMER_NUMBER_NEW###}' WHERE `salesmenKundennummer` = '{###CUSTOMER_NUMBER_OLD###}';",
		14 => "UPDATE `" . TABLE_SALESMEN_STOCKS . "` SET `salesmenStocksSalesmanCustomerNumber` = '{###CUSTOMER_NUMBER_NEW###}' WHERE `salesmenStocksSalesmanCustomerNumber` = '{###CUSTOMER_NUMBER_OLD###}';",
		15 => "UPDATE `" . TABLE_SALESMEN_PROVISIONS_DETAILS . "` SET `salesmenProvisionsDetailsProductRecipientCustomerNumber` = '{###CUSTOMER_NUMBER_NEW###}' WHERE `salesmenProvisionsDetailsProductRecipientCustomerNumber` = '{###CUSTOMER_NUMBER_OLD###}';",
		16 => "UPDATE `" . TABLE_GROUPS_PROVISIONS_DETAILS . "` SET `groupsProvisionsDetailsProductRecipientCustomerNumber` = '{###CUSTOMER_NUMBER_NEW###}' WHERE `groupsProvisionsDetailsProductRecipientCustomerNumber` = '{###CUSTOMER_NUMBER_OLD###}';",

		17 => "UPDATE `" . TABLE_CUSTOMERS . "` SET `customersKundennummer` = '{###CUSTOMER_NUMBER_NEW###}' WHERE `customersKundennummer` = '{###CUSTOMER_NUMBER_OLD###}';",
		// 18 => "UPDATE `" . TABLE_SHOP_CUSTOMERS . "` SET `customers_cid` = '{###CUSTOMER_NUMBER_NEW###}' WHERE `customers_cid` = '{###CUSTOMER_NUMBER_OLD###}';",
		19 => "UPDATE `" . TABLE_CUSTOMERS_RELATIONS . "` SET `customersRelationsParentCustomerNumber` = '{###CUSTOMER_NUMBER_NEW###}' WHERE `customersRelationsParentCustomerNumber` = '{###CUSTOMER_NUMBER_OLD###}';",
		20 => "UPDATE `" . TABLE_CUSTOMERS_RELATIONS . "` SET `customersRelationsChildCustomerNumber` = '{###CUSTOMER_NUMBER_NEW###}' WHERE `customersRelationsChildCustomerNumber` = '{###CUSTOMER_NUMBER_OLD###}';",
		21 => "UPDATE `" . TABLE_CUSTOMERS_ORDERS_INTERVALS . "` SET `customersOrdersIntervalsCustomerNumber` = '{###CUSTOMER_NUMBER_NEW###}' WHERE `customersOrdersIntervalsCustomerNumber` = '{###CUSTOMER_NUMBER_OLD###}';",
		22 => "UPDATE `" . TABLE_ORDERS . "` SET `ordersKundennummer` = '{###CUSTOMER_NUMBER_NEW###}' WHERE `ordersKundennummer` = '{###CUSTOMER_NUMBER_OLD###}';",
		23 => "UPDATE `" . TABLE_AMAZON_ORDERS . "` SET `KUNDENNUMMER` = '{###CUSTOMER_NUMBER_NEW###}' WHERE `KUNDENNUMMER` = '{###CUSTOMER_NUMBER_OLD###}';",
		24 => "UPDATE `" . TABLE_DELIVERY_DATAS . "` SET `Ref_Adresse_1` = '{###CUSTOMER_NUMBER_NEW###}' WHERE `Ref_Adresse_1` = '{###CUSTOMER_NUMBER_OLD###}';",
	);

	$arrCustomerNumbers = array(
		array("customerNumberOld" => "100001",	"customerNumberNew" => "ES-00002"),
		array("customerNumberOld" => "48072",	"customerNumberNew" => "NL-00565"),
		array("customerNumberOld" => "39006",	"customerNumberNew" => "IT-00001"),
		array("customerNumberOld" => "00007",	"customerNumberNew" => "GR-00001"),
		array("customerNumberOld" => "000001",	"customerNumberNew" => "GR-00002"),
		array("customerNumberOld" => "000002",	"customerNumberNew" => "GR-00003"),
		array("customerNumberOld" => "00004",	"customerNumberNew" => "PT-00001"),
		array("customerNumberOld" => "60024",	"customerNumberNew" => "ES-00003"),
		array("customerNumberOld" => "00006",	"customerNumberNew" => "ES-00004"),
		array("customerNumberOld" => "00008",	"customerNumberNew" => "AT-00002"),
		array("customerNumberOld" => "00009",	"customerNumberNew" => "LU-00002"),
		array("customerNumberOld" => "00011",	"customerNumberNew" => "LV-00001"),
		array("customerNumberOld" => "00015",	"customerNumberNew" => "JO-00001"),
		array("customerNumberOld" => "00016",	"customerNumberNew" => "SK-00001"),
		array("customerNumberOld" => "00017",	"customerNumberNew" => "LU-00003"),
		array("customerNumberOld" => "00018",	"customerNumberNew" => "ES-00005"),
		array("customerNumberOld" => "00019",	"customerNumberNew" => "JO-00002"),
		array("customerNumberOld" => "000004",	"customerNumberNew" => "NL-00566"),
		array("customerNumberOld" => "00003",	"customerNumberNew" => "FR-00001"),
		array("customerNumberOld" => "00020",	"customerNumberNew" => "FR-00002"),
		array("customerNumberOld" => "00021",	"customerNumberNew" => "LU-00004"),
		array("customerNumberOld" => "00022",	"customerNumberNew" => "LU-00005"),
		array("customerNumberOld" => "00023",	"customerNumberNew" => "US-00001"),
		array("customerNumberOld" => "00024",	"customerNumberNew" => "FR-00003"),
		array("customerNumberOld" => "00025",	"customerNumberNew" => "FR-00004"),
		array("customerNumberOld" => "00026",	"customerNumberNew" => "FR-00005"),
		array("customerNumberOld" => "00027",	"customerNumberNew" => "NL-00567"),
		array("customerNumberOld" => "00028",	"customerNumberNew" => "AT-00003"),
		array("customerNumberOld" => "00029",	"customerNumberNew" => "GR-00004"),
		array("customerNumberOld" => "00031",	"customerNumberNew" => "HR-00001"),
		array("customerNumberOld" => "00032",	"customerNumberNew" => "MK-00001"),
		array("customerNumberOld" => "00001",	"customerNumberNew" => "NL-00568"),
		array("customerNumberOld" => "00012",	"customerNumberNew" => "JO-00003"),
		array("customerNumberOld" => "00002",	"customerNumberNew" => "RU-00001"),
		array("customerNumberOld" => "000003",	"customerNumberNew" => "LB-00001"),
		array("customerNumberOld" => "00010",	"customerNumberNew" => "BE-00004"),
		array("customerNumberOld" => "00013",	"customerNumberNew" => "MT-00001"),
		array("customerNumberOld" => "00014",	"customerNumberNew" => "BE-00005"),
	);



	if(!empty($arrCustomerNumbers)){
		foreach($arrCustomerNumbers as $thisNumbers){
			echo '<pre>';
			if(!empty($arrSqlQueries)){
				foreach($arrSqlQueries as $thisSqlQueries){
					$thisSqlQuery =  $thisSqlQueries;
					$thisSqlQuery = preg_replace("/\{###CUSTOMER_NUMBER_NEW###\}/", $thisNumbers["customerNumberNew"], $thisSqlQuery);
					$thisSqlQuery = preg_replace("/\{###CUSTOMER_NUMBER_OLD###\}/", $thisNumbers["customerNumberOld"], $thisSqlQuery);
					echo $thisSqlQuery . "\n";
				}
			}
			echo "\n" . '-- ######################### ' . "\n";
			echo '</pre>';
		}
	}
}
?>