<?php
	function getDpdLocalTrackingFileDateTime($string){
		$arrTemp = explode("_", $string);
		// D20150807T123040
		$thisDateTime = preg_replace("/D([0-9]{4})([0-9]{2})([0-9]{2})T([0-9]{2})([0-9]{2})([0-9]{2})/", "$1-$2-$3 $4:$5:$6", $arrTemp[2]);
		return($thisDateTime);
	}

	// BOF SQL ACTION BUTTONS
	function createSqlExecButtons(){
		$content = '';
		$content .= '<div style="margin:2px 0 2px 0;padding:0;">';
		$content .= '<span class="buttonExecuteSqlOrders"><img src="layout/icons/iconExecuteSelected.png" width="16" height="16" alt="" title="" style="margin-bottom:-4px;" /> Markierte ausf&uuml;hren (<span class="displayCountSelectedItemsSqlOrders">0</span>)</span>';
		$content .= '<span class="buttonCheckAllCheckboxesSqlOrders"><img src="layout/icons/iconSelectAll.png" width="16" height="" alt="" title="" style="margin-bottom:-4px;" /> Alle SQL-Befehle ausw&auml;hlen</span>';
		$content .= '<span class="buttonUncheckAllCheckboxesSqlOrders"><img src="layout/icons/iconDeselectAll.png" width="16" height="" alt="" title="" style="margin-bottom:-4px;" /> Alle SQL-Befehle abw&auml;hlen</span>';
		$content .= '</div>';
		return $content;
	}
	// BOF SQL ACTION BUTTONS
	
	function getNotInformedCustomersOrderIDs() {
		global $dbConnection;
		$arrGetNotInformedCustomersOrderIDs = array();
		$sql = "
				SELECT
					`common_ordersamazonratings`.`ordersAmazonRatingsTransaktionsID`,
					`common_ordersamazon`.`AMAZON-BESTELLNUMMER`
				FROM `" . TABLE_AMAZON_ORDERS . "`
				LEFT JOIN `" . TABLE_AMAZON_ORDERS_RATINGS . "`
				ON(`common_ordersamazonratings`.`ordersAmazonRatingsTransaktionsID` = `common_ordersamazon`.`AMAZON-BESTELLNUMMER`)

				WHERE 1

				HAVING `ordersAmazonRatingsTransaktionsID` IS NULL
			";
		$rs = $dbConnection->db_query($sql);

		while($ds = mysqli_fetch_assoc($rs)){
			$arrGetNotInformedCustomersOrderIDs[] = $ds["AMAZON-BESTELLNUMMER"];
		}
		return $arrGetNotInformedCustomersOrderIDs;
	}

	function getNotInformedCustomersOrderIdLinks($string){
		$arrayData = explode("\n", $string);
		$arrGetNotInformedCustomersOrderDatas = array();
		$count = 0;
		if(!empty($arrayData)){
			foreach($arrayData as $thisDataValue){
				$strTemp = $thisDataValue;
				$strTemp = trim($strTemp);

				$arrGetNotInformedCustomersOrderDatas[$count]["ID"] = $strTemp;

				$pattern = '^(.*)$';
				$replace = '$1">$1';
				$strTemp = preg_replace("/" . $pattern . "/ism", $replace, $strTemp);

				$pattern = '^';
				$replace = '<a href="http://sellercentral.amazon.de/gp/orders-v2/details/ref=ag_orddet_cont_myo?ie=UTF8&orderID=';
				$strTemp = preg_replace("/" . $pattern . "/", $replace, $strTemp);

				$pattern = '$';
				$replace = '</a>';
				$strTemp = preg_replace("/" . $pattern . "/", $replace, $strTemp);

				$arrGetNotInformedCustomersOrderDatas[$count]["LINK"] = $strTemp;

				$count++;
			}
		}
		return $arrGetNotInformedCustomersOrderDatas;
	}

	function cleanLineSeparators($string){
		$pattern = "\\r";
		$replace = "\n";
		$string = preg_replace("/" . $pattern . "/", $replace, $string);

		$pattern = "\\n\\n";
		$replace = "\n";
		while(preg_match("/" . $pattern . "/", $string)) {
			$string = preg_replace("/" . $pattern . "/", $replace, $string);
		}
		return $string;
	}

	function cleanDate($string){
		$pattern = "([0-9]{2})\.([0-9]{2})\.([0-9]{4})";
		$replace = "$3-$2-$1";
		$string = preg_replace("/" . $pattern . "/", $replace, $string);
		return $string;
	}

	function cleanDecimals($string){
		$pattern = "([0-9]{1,}),([0-9]{1,})";
		$replace = "$1.$2";
		$string = preg_replace("/" . $pattern . "/", $replace, $string);
		return $string;
	}

	function createQueries($string, $tableName, $tableFields, $ignore, $delayed){
		$pattern = "'";
		$replace = "\'";
		$string = preg_replace("/" . $pattern . "/", $replace, $string);

		$pattern = ";";
		$replace = "', '";
		$string = preg_replace("/" . $pattern . "/", $replace, $string);

		$pattern = "\n|^";
		$replace = "\nINSERT " . ' ' . $delayed . ' ' . $ignore . " INTO `" . $tableName . "` VALUES ('";
		// "INSERT INTO ON DUPLICATE KEY UPDATE id=id."
		$string = preg_replace("/" . $pattern . "/", $replace, $string);

		$pattern = "\n|$";
		$replace = "');\n";
		$string = preg_replace("/" . $pattern . "/", $replace, $string);

		$pattern = "^'\);\\n";
		$replace = "";
		$string = preg_replace("/" . $pattern . "/", $replace, $string);

		return $string;
	}

	function convertMailData($string) {
		$arrOutput = array();

		#$string = html_entity_decode($string);

		$string = preg_replace("/\</", "\n", $string);
		$string = preg_replace("/\>/", "\n", $string);
		$string = preg_replace("/:/", "\n", $string);

		$string = cleanLineSeparators($string);

		$string = preg_replace('/(.*)@/', '{###MAIL###}$1@', $string);
		$string = preg_replace('/(.*) (.*)@/', '$2@', $string);
		$string = preg_replace('/@(.*) (.*)/', '@$1', $string);
		$string = preg_replace('/@(.*)\t(.*)/', '@$1', $string);

		$string = preg_replace("/\\t/", "", $string);

		while(preg_match("/ /", $string)) {
			$string = preg_replace("/ /", " ", $string);
		}
		$string = preg_replace("/ /", "\n", $string);
		$string = preg_replace("/\\n /", "\n", $string);

		$arrTempGetMails = explode("\n", $string);
		$arrTempGetMails = array_unique($arrTempGetMails);
		// print_r($arrTempGetMails);

		$arrGetMails = array();
		foreach($arrTempGetMails as $thisValue) {
			if(preg_match("/@/", $thisValue)) {
				$arrGetMails[] = $thisValue;
			}
		}
		sort($arrGetMails);
		$arrGetMails = array_unique($arrGetMails);
		// print_r($arrGetMails);

		$string = implode("\n", $arrGetMails);

		$string = preg_replace("/(.*)@www.burhan-ctr.de/", "", $string);
		$string = preg_replace("/(.*)@burhan-ctr.de/", "", $string);
		$string = preg_replace("/(.*)@www.b3-werbepartner.de/", "", $string);
		$string = preg_replace("/(.*)@b3-werbepartner.de/", "", $string);
		#$string = preg_replace("/.*?[^{###MAIL###}].*/", "", $string);
		$string = preg_replace("/{###MAIL###}/", "", $string);
		while(preg_match("/\\n\\n/", $string)) {
			$string = preg_replace("/\\n\\n/", "\n", $string);
		}
		$string = preg_replace('/@(.*)\t(.*)/', '@$1', $string);
		$string = preg_replace('/@(.*)&(.*)/', '@$1', $string);
		$string = preg_replace('/(.*)=(.*)/', '$2', $string);
		$string = preg_replace('/\(/', '', $string);
		$string = preg_replace('/\)/', '', $string);
		$string = preg_replace('/\'/', '', $string);
		$string = preg_replace('/"/', '', $string);
		$string = preg_replace('/;/', '', $string);
		$string = preg_replace('/#/', '', $string);
		$string = preg_replace('/,/', '', $string);
		$string = trim($string);

		$arrOutput["mail"] = $string;

		return $arrOutput;
	}

	function convertProductName($string){
		$pattern = "/(;|\t)([A-Z]{1,2})(;|\t)/";
		$replace = "$1$2 52$3";
		$string = preg_replace($pattern, $replace, $string);
		return $string;
	}
	
	function convertTrListsDataAuftragslisten($string) {
		// FIELDS:
		//	containerListsFilename,containerListsProductionsDateStart,containerListsProductionsDateEnd,containerListsDepartureKW,containerListsArrivalKW,containerListsDeliveryKW,containerListsCustomerNumber,containerListsCustomerName,containerListsCustomerKommission,containerListsProduct,containerListsQuantity,containerListsTirArkasi

		$rs_match = preg_match("/LISTE:(.*)/i", $string, $arrFound);
		$containerListsFilename = trim($arrFound[1]);

		$rs_match = preg_match("/PRODUKTION:.*([0-9]{2}\.[0-9]{2}\.[0-9]{4}).*([0-9]{2}\.[0-9]{2}\.[0-9]{4})/i", $string, $arrFound);
		$containerListsProductionsDateStart = trim($arrFound[1]);
		$containerListsProductionsDateEnd = trim($arrFound[2]);
		$containerListsProductionsDateStart = formatDate($containerListsProductionsDateStart, 'store');
		$containerListsProductionsDateEnd = formatDate($containerListsProductionsDateEnd, 'store');

		$rs_match = preg_match("/SCHIFFS-ABFAHRT: ca.(.*)/i", $string, $arrFound);
		$containerListsDepartureKW = $arrFound[1];
		$containerListsDepartureKW = preg_replace("/KW/", "", $containerListsDepartureKW);
		$containerListsDepartureKW = trim($containerListsDepartureKW);

		$rs_match = preg_match("/SCHIFFS-ANKUNFT: ca.(.*)/i", $string, $arrFound);
		$containerListsArrivalKW = $arrFound[1];
		$containerListsArrivalKW = preg_replace("/KW/", "", $containerListsArrivalKW);
		$containerListsArrivalKW = trim($containerListsArrivalKW);

		$rs_match = preg_match("/VERSAND: ca.(.*)/i", $string, $arrFound);
		$containerListsDeliveryKW = $arrFound[1];
		$containerListsDeliveryKW = preg_replace("/KW/", "", $containerListsDeliveryKW);
		$containerListsDeliveryKW = trim($containerListsDeliveryKW);

		$pattern = "/(LISTE|PRODUKTION|SCHIFFS-ABFAHRT|SCHIFFS-ANKUNFT|VERSAND):(.*)/";
		$replace = "";
		$string = preg_replace($pattern, $replace, $string);

		$pattern = "/\t/";
		$replace = ";";
		$string = preg_replace($pattern, $replace, $string);

		$pattern = "/( ;|; )/";
		$replace = ";";
		$string = preg_replace($pattern, $replace, $string);

		$pattern = "/^\n{1,}/";
		$replace = "\n";
		$string = preg_replace($pattern, $replace, $string);

		$addFields = "";
		$addFields .= $containerListsFilename . ';';
		$addFields .= $containerListsProductionsDateStart . ';';
		$addFields .= $containerListsProductionsDateEnd . ';';
		$addFields .= $containerListsDepartureKW . ';';
		$addFields .= $containerListsArrivalKW . ';';
		$addFields .= $containerListsDeliveryKW . ';';

		$pattern = "/\n/";
		$replace = "\n" . $addFields;
		$string = preg_replace($pattern, $replace, $string);

		return $string;
	}

	function convertTrListsDataPrint($string) {
		global $_POST, $_GET, $_REQUEST;
		$arrOutput = array();
		
		#$string = iconv("UTF-8", "ISO-8859-1//TRANSLIT", $string);
		#$string = iconv("UTF-8", "ISO-8859-1//IGNORE", $string);
		
		#$detectEncoding = mb_detect_encoding($string);		
		#echo 'dE:' . $detectEncoding . '<br />';		
		
		// BOF REPLACE QUOTES
		$pattern = '/\"/';
		$replace = "";
		$string = preg_replace($pattern, $replace, $string);
		
		$pattern = '/"/';
		$replace = "";
		$string = preg_replace($pattern, $replace, $string);
		
		$pattern = '/\\\/';
		$replace = "";
		$string = preg_replace($pattern, $replace, $string);
		// EOF REPLACE QUOTES
		
		$string = cleanLineSeparators($string);

		// BOF REPLACE SPECIAL CHARS
		$arrSpecialChars = array(
				array("replace" => "ä", "pattern" => "Ã¤"),
				array("replace" => "ö", "pattern" => "Ã¶"),
				array("replace" => "ü", "pattern" => "Ã¼"),
				array("replace" => "Ä", "pattern" => "Ã„"),
				array("replace" => "Ö", "pattern" => "Ã–"),
				array("replace" => "Ü", "pattern" => "Ãœ"),
				array("replace" => "ß", "pattern" => "ÃŸ"),
				array("replace" => "'", "pattern" => "Â´")
			);
		foreach($arrSpecialChars as $thisSpecialCharData){
			$string = preg_replace("/" . $thisSpecialCharData["pattern"] . "/", $thisSpecialCharData["replace"], $string);
		}
		// EOF REPLACE SPECIAL CHARS		
		
		// BOF REPLACE DATETIME FIELD		
		$pattern = "/\n[0-9]{2}\.[0-9]{2}\.[0-9]{4}, [0-9]{2}:[0-9]{2}[\t; ]/";
		$replace = "\n";
		$string = preg_replace($pattern, $replace, $string);		
		// EOF REPLACE DATETIME FIELD	
		
		// BOF REPLACE DATE FIELD
		$pattern = "/^[0-9]{2}\.[0-9]{2}\.[0-9]{4}[\t; ]/";
		$replace = "";
		$string = preg_replace($pattern, $replace, $string);

		$pattern = "/^[0-9]{2}\.[0-9]{2}\.[0-9]{2}[\t; ]/";
		$replace = "";
		$string = preg_replace($pattern, $replace, $string);

		$pattern = "/\n[0-9]{2}\.[0-9]{2}\.[0-9]{4}[\t; ]/";
		$replace = "\n";
		$string = preg_replace($pattern, $replace, $string);

		$pattern = "/\n[0-9]{2}\.[0-9]{2}\.[0-9]{2}[\t; ]/";
		$replace = "\n";
		$string = preg_replace($pattern, $replace, $string);
		// EOF REPLACE DATE FIELD


		// BOF REPLACE B3 WITH B3-CUSTOMER NUMBER
		$pattern = "/(\n)B3/";
		$replace = "\n48271";
		$string = preg_replace($pattern, $replace, $string);
		// EOF REPLACE B3 WITH B3-CUSTOMER NUMBER

		// BOF CONVERT PRODUCT NAME
		$string = convertProductName($string);
		// EOF CONVERT PRODUCT NAME

		// BOF Tır Arkası
		$pattern = "/Tır Arkası/";
		$replace = "TIR ARKASI";
		$string = preg_replace($pattern, $replace, $string);
		// EOF Tır Arkası

		// BOF plakalık
		$pattern = "/\tplakalık\t/";
		$replace = "\tKennzeichenhalter\t";
		$string = preg_replace($pattern, $replace, $string);
		// EOF Tır Arkası

		// BOF ps-plate
		$pattern = "/\tps\t/";
		$replace = "\tMiniletter\t";
		$string = preg_replace($pattern, $replace, $string);
		// EOF Tır Arkası

		// BOF paspas
		$pattern = "/\tpaspas\t/";
		$replace = "\tPapierfußmatten\t";
		$string = preg_replace($pattern, $replace, $string);
		// EOF Tır Arkası


		#$pattern = "\n.*(Sevk Tarihi|Sevh Tarihi|SevkTarihi|SEVK TARİHİ).*";
		$pattern = "\n.*(Sevk Tar|Sevh Tar|SevkTar|SEVK TAR).*";
		$replace = "";
		$string = preg_replace("/" . $pattern . "/i", $replace, $string);

		$pattern = "\n;.*";
		$replace = "";
		$string = preg_replace("/" . $pattern . "/", $replace, $string);

		$pattern = "\n([0-9]{1,4};)";
		$replace = "\n0$1";
		$string = preg_replace("/" . $pattern . "/", $replace, $string);

		$pattern = "\n([0-9]{1,4}\t)";
		$replace = "\n0$1";
		$string = preg_replace("/" . $pattern . "/", $replace, $string);

		$pattern = ";TIRIN";
		$replace = " TIRIN";
		$string = preg_replace("/" . $pattern . "/i", $replace, $string);

		$pattern = "\tTIRIN";
		$replace = " TIRIN";
		$string = preg_replace("/" . $pattern . "/i", $replace, $string);

		$pattern = ";Ç(.*){1}TA";
		$replace = " ÇİTA";
		#$string = preg_replace("/" . $pattern . "/i", $replace, $string);

		$pattern = "\tÇ(.*){1}TA";
		$replace = " ÇİTA";
		#$string = preg_replace("/" . $pattern . "/i", $replace, $string);


		$pattern = "ÇİTA";
		$replace = "LEISTEN";
		$string = preg_replace("/" . $pattern . "/i", $replace, $string);

		$pattern = "^M.*\n";
		$replace = "";
		$string = preg_replace("/" . $pattern . "/", $replace, $string);

		$pattern = ";{2}\n";
		$replace = ";\n";
		$string = preg_replace("/" . $pattern . "/", $replace, $string);

		$pattern = ";";
		$replace = "\t";
		$string = preg_replace("/" . $pattern . "/", $replace, $string);

		$pattern = "[ ]{1,}";
		$replace = " ";
		$string = preg_replace("/" . $pattern . "/", $replace, $string);

		#$pattern = "[ ]{1,}\t";
		#$replace = " ";
		#$string = preg_replace("/" . $pattern . "/", $replace, $string);

		#$pattern = "\t[ ]{1,}";
		#$replace = " ";
		##$replace = "\t";
		#$string = preg_replace("/" . $pattern . "/", $replace, $string);

		$arrString = explode("\n", $string);

		sort($arrString);

		$arrOutput["csv"] = '';

		$thisListFileName = $_REQUEST["handleThisDeliveryDate"];
		$arrListDates = convertTrFileNameToDate($thisListFileName);

		$thisDateSheepingWeek = 1 * getWeekFromDate($arrListDates[1]);
		$arrOutput["csv"] .= 'LISTE: ' . $thisListFileName . "\n";
		$arrOutput["csv"] .= 'PRODUKTION: ' . implode(" & ", $arrListDates) . "\n";
		$arrOutput["csv"] .= 'SCHIFFS-ABFAHRT: ' . 'ca. KW ' . $thisDateSheepingWeek . "\n";
		$arrOutput["csv"] .= 'SCHIFFS-ANKUNFT: ' . 'ca. KW ' . ($thisDateSheepingWeek + 2) . "\n";
		$arrOutput["csv"] .= 'VERSAND: ' . 'ca. KW ' . ($thisDateSheepingWeek + 3) . "\n";
		$arrOutput["csv"] .= implode("\n", $arrString);
		$arrOutput["sql"] = '';
		return $arrOutput;
	}
	function getWeekFromDate($date){
		$thisDate = formatDate($date, "store");
		$thisDateWeek = date("W", strtotime($thisDate));
		return $thisDateWeek;
	}
	function convertTrFileNameToDate($string){
		$arrDates = array();
		$arrThisListFileInfo = pathinfo("/" . $string);
		$string = trim($arrThisListFileInfo["filename"]);
		$string = preg_replace("/ /", "", $string);
		if(preg_match("/[0-9]{1,2}_[0-9]{1,2}_[0-9]{4}&[0-9]{1,2}_[0-9]{1,2}_[0-9]{4}/", $string)){
			$arrTempDate = explode("&", $string);
			if(!empty($arrTempDate)){
				$countDate = 0;
				foreach($arrTempDate as $thisTempData){
					$arrThisTempDateData = explode("_", $thisTempData);
					$arrDates[$countDate] = '';

					if(!empty($arrThisTempDateData)){
						foreach($arrThisTempDateData as $thisTempDateDataKey => $thisTempDateData){
							#$thisTempDateData = $thisTempDateData * 1;
							$thisTempDateData = intval($thisTempDateData);
							if($thisTempDateData < 10){
								$thisTempDateData = "0" . $thisTempDateData;
							}
							$arrDates[$countDate] .= $thisTempDateData;
							if($thisTempDateDataKey < 2){
								$arrDates[$countDate] .= ".";
							}
						}
					}
					$countDate++;
				}
			}
		}
		else if(preg_match("/[0-9]{1,2}\.[0-9]{1,2}\.[0-9]{4}-[0-9]{1,2}\.[0-9]{1,2}\.[0-9]{4}/", $string)){
			$arrTempDate = explode("-", $string);
			$arrDates = $arrTempDate;
		}
		else if(preg_match("/[0-9]{1,2}\.[0-9]{1,2}\.[0-9]{4}&[0-9]{1,2}\.[0-9]{1,2}\.[0-9]{4}/", $string)){
			$arrTempDate = explode("&", $string);
			$arrDates = $arrTempDate;
		}

		return $arrDates;
	}

	function convertDeliSprintDeliveryData($string, $tableName) {
		$arrOutput = array();
		$string = trim($string);
		$string = cleanLineSeparators($string);
		$string = cleanDate($string);
		$string = cleanDecimals($string);
		if(mb_detect_encoding($string) == 'UTF-8'){
			#$string = utf8_encode($string);
		}

		$arrOutput["csv"] = $string;
		$string = createQueries($string, $tableName, '', 'IGNORE', 'DELAYED');

		// BOF ADD EMPTY FIELD FOR `isInfoMailSended`
			$string = preg_replace("/(\);)/", ",'0'$1", $string);
		// EOF ADD EMPTY FIELD FOR `isInfoMailSended`
		$arrOutput["sql"] = $string;
		return $arrOutput;
	}

	function convertDpdParcelInvoiceData($string, $tableName) {
		$arrOutput = array();
		$string = trim($string);
		$string = cleanLineSeparators($string);
		$string = cleanDate($string);
		$string = cleanDecimals($string);
		if(mb_detect_encoding($string) == 'UTF-8'){
			#$string = utf8_encode($string);
		}

		$arrOutput["csv"] = $string;
		$string = createQueries($string, $tableName, '', 'IGNORE', 'DELAYED');
		$arrOutput["sql"] = $string;
		return $arrOutput;
	}

	function convertDpdInterfaceDeliveryData($string, $tableName) {
		$arrOutput = array();
		$string = trim($string);
		$string = cleanLineSeparators($string);
		$string = cleanDate($string);
		$string = cleanDecimals($string);
		if(mb_detect_encoding($string) == 'UTF-8'){
			#$string = utf8_encode($string);
		}

		$arrOutput["csv"] = $string;
		$string = createQueries($string, $tableName, '', 'IGNORE', ' DELAYED');
		$arrOutput["sql"] = $string;
		return $arrOutput;
	}

	function convertLayoutData($string) {
		$arrOutput = array();

		$string = addslashes($string);

		$string = cleanLineSeparators($string);

		$pattern = "(.*)<DIR>(.*)";
		$replace = "";
		$string = preg_replace("/" . $pattern . "/", $replace, $string);

		#$pattern = "?!(.*[0-9]{2}\.[0-9]{2}\.[0-9]{4}.*)";
		#$replace = "";
		#$string = preg_replace("/" . $pattern . "/ism", $replace, $string);

		$string = cleanLineSeparators($string);



		$string = cleanDate($string);

		$pattern = "(\.[0-9]{1,3}) ";
		$replace = "$1;";
		$string = preg_replace("/" . $pattern . "/", $replace, $string);

		$pattern = "(:[0-9]{2})[ ]{1,}";
		$replace = "$1;";
		$string = preg_replace("/" . $pattern . "/", $replace, $string);

		$arrOutput["csv"] = $string;

		$string = createQueries($string, TABLE_CREATED_LAYOUT_FILES, '', 'IGNORE', ' DELAYED');

		$string = preg_replace("/\\\\\\\'/", "\'", $string);

		$arrOutput["sql"] = $string;
		return $arrOutput;
	}

	function convertAmazonOrdersData($string) {
		$arrOutput = array();
		#$string = utf8_decode($string);

		$tempFieldSeparator = "{###FIELD_SEPARATOR###}";
		$tempLineSeparator = "{###LINE_SEPARATOR###}";

		if(preg_match("/Bestellung stornieren/", $string)){
			$pattern = "(Bestellung stornieren)";
			$replace = "$1" . $tempLineSeparator;
		}
		else if(preg_match("/Bestellung erstatten/", $string)){
			$pattern = "(Bestellung erstatten)";
			$replace = "$1" . $tempLineSeparator;
		}
		$string = preg_replace("/" . $pattern . "/", $replace, $string);

		$pattern = "\\r";
		$replace = "";
		$string = preg_replace("/" . $pattern . "/", $replace, $string);

		$pattern = "\\n[\t ]{1,}";
		$replace = $tempFieldSeparator;
		$string = preg_replace("/" . $pattern . "/", $replace, $string);

		$pattern = "\\n";
		$replace = $tempFieldSeparator;
		$string = preg_replace("/" . $pattern . "/", $replace, $string);

		$pattern = "\\t";
		$replace = $tempFieldSeparator;
		$string = preg_replace("/" . $pattern . "/", $replace, $string);

		$pattern = " " . $tempFieldSeparator;
		$replace = $tempFieldSeparator;
		$string = preg_replace("/" . $pattern . "/", $replace, $string);

		$pattern = $tempFieldSeparator . " ";
		$replace = $tempFieldSeparator;
		$string = preg_replace("/" . $pattern . "/", $replace, $string);

		$pattern = " bis" . $tempFieldSeparator;
		$replace = " bis ";
		$string = preg_replace("/" . $pattern . "/", $replace, $string);

		$arrOrders = explode($tempLineSeparator, $string);

		if(!empty($arrOrders)){
			foreach($arrOrders as $thisOrderKey => $thisOrderValue){
				if($thisOrderValue != ""){
					$arrData = explode($tempFieldSeparator, $thisOrderValue);
					if($arrData[0] == "")	{
						array_shift($arrData);
					}
					$string = "";

					$COUNT = '%';
					$BESTELLDATUM = $arrData[0] . " " . $arrData[1];

					$arrTemp = explode(":", $arrData[8]);
					$VERTRIEBSKANAL = trim($arrTemp[1]);

					$arrTemp = explode(":", $arrData[9]);
					$VERSAND_DURCH = trim($arrTemp[1]);

					$AMAZON_BESTELLNUMMER = $arrData[2];

					$PRODUKTNAME = $arrData[3];

					$arrTemp = explode(":", $arrData[4]);
					$MENGE = trim($arrTemp[1]);

					$arrTemp = explode(":", $arrData[5]);
					$ASIN = trim($arrTemp[1]);

					$arrTemp = explode(":", $arrData[6]);
					$SKU = trim($arrTemp[1]);

					$arrTemp = explode(":", $arrData[7]);
					$KAEUFER = trim($arrTemp[1]);

					$VERSANDART = $arrData[10];
					$STATUS = $arrData[17];

					$arrTemp = explode(" bis ", $arrData[15]);
					$LIEFERDATUM_VON = trim($arrTemp[1]);
					$LIEFERDATUM_BIS = trim($arrTemp[2]);
					$KUNDENNUMMER = '';

					#$string .= $COUNT;
					$string .= $BESTELLDATUM;
					$string .= ';';
					$string .= $VERTRIEBSKANAL;
					$string .= ';';
					$string .= $VERSAND_DURCH;
					$string .= ';';
					$string .= $AMAZON_BESTELLNUMMER;
					$string .= ';';
					$string .= $PRODUKTNAME;
					$string .= ';';
					$string .= $MENGE;
					$string .= ';';
					$string .= $ASIN;
					$string .= ';';
					$string .= $SKU;
					$string .= ';';
					$string .= $KAEUFER;
					$string .= ';';
					$string .= $VERSANDART;
					$string .= ';';
					$string .= $STATUS;
					$string .= ';';
					$string .= $LIEFERDATUM_VON;
					$string .= ';';
					$string .= $LIEFERDATUM_BIS;
					$string .= ';';
					$string .= $KUNDENNUMMER;

					$pattern = "([0-9]{2}\.[0-9]{2}\.[0-9]{4};[0-9]{2}\.[0-9]{2}\.[0-9]{4})";
					$replace = "$1";
					$string = preg_replace("/" . $pattern . "/", $replace, $string);

					$pattern = "^";
					$replace = "%;";
					$string = preg_replace("/" . $pattern . "/", $replace, $string);

					$arrOrders[$thisOrderKey] = $string;
				}
			}
		}

		$string = implode("\n", $arrOrders) . "\n";
		#$string = cleanLineSeparators($string);

		$string = cleanDate($string);

		$arrOutput["csv"] = $string;

		$pattern = ";";
		$replace = "', '";
		$string = preg_replace("/" . $pattern . "/", $replace, $string);

		$pattern = "(%')";
		$replace = "INSERT INTO `" . TABLE_AMAZON_ORDERS . "` (`COUNT`, `BESTELLDATUM`, `VERTRIEBSKANAL`, `VERSAND_DURCH`, `AMAZON-BESTELLNUMMER`, `PRODUKTNAME`, `MENGE`, `ASIN`, `SKU`, `KAEUFER`, `VERSANDART`, `STATUS`, `LIEFERDATUM_VON`, `LIEFERDATUM_BIS`, `KUNDENNUMMER`) VALUES ('$1";
		$string = preg_replace("/" . $pattern . "/", $replace, $string);

		$pattern = "(')\n";
		$replace = "$1');\n";
		$string = preg_replace("/" . $pattern . "/", $replace, $string);

		$arrOutput["sql"] = $string;

		return $arrOutput;
	}

	function _old_convertAmazonOrdersData($string) {
		$arrOutput = array();
		#$string = utf8_decode($string);

		$string = cleanLineSeparators($string);

		$pattern = "\\nbis\\n";
		$replace = ";";
		$string = preg_replace("/" . $pattern . "/", $replace, $string);

		$pattern = "([0-9]{2}:[0-9]{2}:[0-9]{2} GMT)";
		$replace = " $1";
		$string = preg_replace("/" . $pattern . "/", $replace, $string);

		$pattern = "([0-9]{3}-[0-9]{7}-[0-9]{7})";
		$replace = "$1;";
		$string = preg_replace("/" . $pattern . "/", $replace, $string);

		$pattern = "\\n";
		$replace = "";
		$string = preg_replace("/" . $pattern . "/", $replace, $string);

		$pattern = "([0-9]{2}\.[0-9]{2}\.[0-9]{4};[0-9]{2}\.[0-9]{2}\.[0-9]{4})";
		$replace = "$1;\n";
		$string = preg_replace("/" . $pattern . "/", $replace, $string);

		$pattern = "^[A-Za-zäöüß \t]{1,}";
		$replace = "";
		$string = preg_replace("/" . $pattern . "/", $replace, $string);

		$pattern = "\n[A-Za-zäöüß \t]{1,}";
		$replace = "\n";
		$string = preg_replace("/" . $pattern . "/", $replace, $string);

		$pattern = "^([0-9]{2}\.[0-9]{2}\.[0-9]{4})";
		$replace = "$1 ";
		#$string = preg_replace("/" . $pattern . "/", $replace, $string);

		$pattern = " \\t";
		$replace = ";";
		$string = preg_replace("/" . $pattern . "/", $replace, $string);

		$pattern = "\\n([0-9])";
		$replace = "\n%;$1";
		$string = preg_replace("/" . $pattern . "/", $replace, $string);

		$pattern = "^";
		$replace = "%;";
		$string = preg_replace("/" . $pattern . "/", $replace, $string);

		$pattern = "\\t";
		$replace = ";";
		$string = preg_replace("/" . $pattern . "/", $replace, $string);

		$string = cleanDate($string);

		$pattern = "Menge";
		$replace = ";Menge";
		$string = preg_replace("/" . $pattern . "/", $replace, $string);

		$pattern = "Menge: |ASIN: |SKU: ";
		$replace = "";
		$string = preg_replace("/" . $pattern . "/", $replace, $string);

		$pattern = " ";
		$replace = " ";
		$string = preg_replace("/" . $pattern . "/", $replace, $string);

		$arrOutput["csv"] = $string;

		$pattern = ";";
		$replace = "', '";
		$string = preg_replace("/" . $pattern . "/", $replace, $string);

		$pattern = "(%')";
		$replace = "INSERT INTO `" . TABLE_AMAZON_ORDERS . "` VALUES ('$1";
		$string = preg_replace("/" . $pattern . "/", $replace, $string);

		$pattern = "(, ')\\n";
		$replace = "$1');\n";
		$string = preg_replace("/" . $pattern . "/", $replace, $string);

		$arrOutput["sql"] = $string;
		return $arrOutput;
	}

	function convertAmazonTransactionsData($string) {
		$arrOutput = array();
		#$string = utf8_decode($string);

		$string = cleanLineSeparators($string);

		$arrString = explode("\n", $string);
		$string = '';

		if(!empty($arrString)){
			foreach($arrString as $thisString){
				$thisString = cleanDecimals($thisString);

				$pattern = "€";
				$replace = "";
				$thisString = preg_replace("/" . $pattern . "/", $replace, $thisString);

				#if(preg_match("/(echte Galvanik|An Konto mit der Endung)/", $thisString)){ echo $thisString . '<hr />'; }
				#if(preg_match("/(An Konto mit der Endung)/", $thisString)){ echo $thisString . '<hr />'; }
				#if(preg_match("/(echte Galvanik)/", $thisString)){ echo $thisString . '<hr />'; }

				$pattern = '","","';
				$replace = ";;";
				$thisString = preg_replace("/" . $pattern . "/", $replace, $thisString);

				$pattern = '""';
				$replace = '######';
				$thisString = preg_replace("/" . $pattern . "/", $replace, $thisString);

				$pattern = '","';
				$replace = ";";
				$thisString = preg_replace("/" . $pattern . "/", $replace, $thisString);

				$pattern = '",';
				$replace = ";";
				$thisString = preg_replace("/" . $pattern . "/", $replace, $thisString);

				$pattern = ',"';
				$replace = ";";
				$thisString = preg_replace("/" . $pattern . "/", $replace, $thisString);

				$pattern = '^"';
				$replace = '';
				$thisString = preg_replace("/" . $pattern . "/", $replace, $thisString);

				$pattern = '"$';
				$replace = '';
				$thisString = preg_replace("/" . $pattern . "/", $replace, $thisString);

				$arrFields = explode(";", $thisString);

				$thisDate = $arrFields[0];
				$pattern = "([0-9]{2})\.([0-9]{2})\.([0-9]{4})";
				$replace = "$3-$2-$1";
				$thisDate = preg_replace("/" . $pattern . "/", $replace, $thisDate);

				$pattern = "( GMT[+-][0-9]{2}:[0-9]{2})";
				$replace = "";
				$thisDate = preg_replace("/" . $pattern . "/", $replace, $thisDate);
				$arrFields[0] = $thisDate;

				$thisMD5 = md5($arrFields[1] . $arrFields[2] . $arrFields[3] . $arrFields[4]);
				$arrFields[count($arrFields)] = $thisMD5;

				$thisString = implode("\t", $arrFields);

				$thisString = createQueries($thisString, TABLE_AMAZON_ORDERS_TRANSACTIONS, '', 'IGNORE', '');

				$pattern = "\\t";
				$replace = "', '";
				$thisString = preg_replace("/" . $pattern . "/", $replace, $thisString);

				$pattern = '######';
				$replace = '"';
				$thisString = preg_replace("/" . $pattern . "/", $replace, $thisString);

				$string .= $thisString;
			}
		}
		$arrOutput["csv"] = '';

		$arrOutput["sql"] = $string;
		return $arrOutput;
	}

	function _old_convertAmazonTransactionsData($string) {

		$arrOutput = array();
		#$string = utf8_decode($string);

		####
		/*
		"," -> ;
		"" -> "
		^" ->
		"$ ->
		([0-9]),([0-9]) -> $1.$2
		*/
		#####

		$string = cleanLineSeparators($string);

		$arrString = explode("\n", $string);
		$string = '';

		if(!empty($arrString)){
			foreach($arrString as $thisString){
				$thisString = cleanDecimals($thisString);

				$pattern = "€";
				$replace = "";
				$thisString = preg_replace("/" . $pattern . "/", $replace, $thisString);

				$arrFields = explode("\t", $thisString);

				$thisDate = $arrFields[0];
				$pattern = "([0-9]{2})\.([0-9]{2})\.([0-9]{4})";
				$replace = "$3-$2-$1";
				$thisDate = preg_replace("/" . $pattern . "/", $replace, $thisDate);
				$arrFields[0] = $thisDate;

				$thisMD5 = md5($arrFields[1] . $arrFields[2] . $arrFields[3] . $arrFields[4] . $arrFields[5]);
				$arrFields[count($arrFields)] = $thisMD5;

				$thisString = implode("\t", $arrFields);

				$thisString = createQueries($thisString, TABLE_AMAZON_ORDERS_TRANSACTIONS, '', 'IGNORE', '');

				$pattern = "\\t";
				$replace = "', '";
				$thisString = preg_replace("/" . $pattern . "/", $replace, $thisString);

				$string .= $thisString;
			}
		}

		$arrOutput["csv"] = '';

		$arrOutput["sql"] = $string;
		return $arrOutput;
	}
?>