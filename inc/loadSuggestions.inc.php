<?php
	session_start();

	require_once('../config/configMandator.inc.php');
	require_once('../config/configParams_' . strtolower(MANDATOR) . '.inc.php');
	require_once('../config/configBasic.inc.php');
	require_once('../config/configFiles.inc.php');
	require_once('../config/configTables.inc.php');
	require_once('../inc/functions.inc.php');
	##require_once('header.inc.php');

	#$arrUrlData = parse_url(urldecode($_SERVER["REQUEST_URI"]));
	$arrUrlData = parse_url(($_SERVER["REQUEST_URI"]));
	$strUrlParams = ($arrUrlData["query"]);
	$arrUrlParams = explode('&', $strUrlParams);
	if(!empty($arrUrlParams)){
		foreach($arrUrlParams as $thisParam){
			$arrTemp = explode('=', $thisParam);
			$_GET[$arrTemp[0]] = urldecode(preg_replace("/xxxxxx/", " ", $arrTemp[1]));
		}
	}

	$thisSearchString = trim($_GET["searchString"]);
	#$thisSearchString = trim(urldecode($_GET["searchString"]));
	#$thisSearchString = preg_replace("/[ ]{1,}$/", "", $thisSearchString);
	#$thisSearchString = preg_replace("/ $/", "", $thisSearchString);

	$content = '';

	DEFINE('SEARCH_STRING_LENGTH_MIN', 1);
	$thisSearchStringLength = strlen($thisSearchString);

	$warningMessage = '';
	$errorMessage = '';
	$successMessage = '';
	$infoMessage = '';

	$useUf8Encode = false;

	#if($thisSearchString != "") {
	if($thisSearchStringLength >= SEARCH_STRING_LENGTH_MIN) {
		if($_GET["type"] == "searchFinanceOffice") {
			$dbConnection = new DB_Connection();
			$db_open = $dbConnection->db_connect();

			$sql = "SELECT
						`FINANZAMT_NAME` AS `searchResult`
						FROM `" . TABLE_FINANCE_OFFICES . "`
						WHERE 1
							AND `PLZ` LIKE '" . $thisSearchString . "%'

							GROUP BY `FINANZAMT_NAME`
						LIMIT " . MAX_SEARCH_SUGGESTIONS . "

					UNION

					SELECT
						`FINANZAMT_NAME` AS `searchResult`
						FROM `" . TABLE_FINANCE_OFFICES . "`
						WHERE 1
							AND `ORT` LIKE '" . $thisSearchString . "%'
							GROUP BY `FINANZAMT_NAME`
						LIMIT " . MAX_SEARCH_SUGGESTIONS . "
				";
			$rs = $dbConnection->db_query($sql);

			while($ds = mysqli_fetch_assoc($rs)) {
				if($ds["searchResult"] != ''){
					$arrSearchResults[] = $ds["searchResult"];
				}
			}

			if(!empty($arrSearchResults)) {
				#for($i = 0; $i < MAX_SEARCH_SUGGESTIONS ; $i++) {
				for($i = 0; $i < count($arrSearchResults) ; $i++) {
					$thisSearchResult = $arrSearchResults[$i];
					$thisSearchString = $thisSearchString;
					$displaySearchResult = preg_replace('/'.$thisSearchString.'/ism', '[span class={###QUOT###}foundString{###QUOT###}]'.($thisSearchString).'[/span]', $thisSearchResult);
					$content .= '[div class={###QUOT###}searchResultItem{###QUOT###}]' . $displaySearchResult . '[/div]';
				}
			}

			if($dbConnection->db_displayErrors() != "") {
				$errorMessage .= $dbConnection->db_displayErrors(). '<br />';
			}

			if($dbConnection) {
				$dbConnection->db_close();
			}
		}

		else if($_GET["type"] == "searchMailAddress") {
			$dbConnection = new DB_Connection();
			$db_open = $dbConnection->db_connect();

			// BOF HANDLE ";"-SEPERATED MAIL-ADRESSES
				$strCurrentMailAdresses = '';
				#if($_COOKIE["isAdmin"] == '1'){
				if(1){
					$strCurrentMailAdresses = '';
					$arrTempSearchString = explode(';', $thisSearchString);
					if(count($arrTempSearchString) > 1){
						$thisSearchString = $arrTempSearchString[(count($arrTempSearchString) - 1)];
						$thisSearchString = array_pop($arrTempSearchString);
						$strCurrentMailAdresses = implode(";", $arrTempSearchString) . ";";
					}
				}
			// EOF HANDLE ";"-SEPERATED MAIL-ADRESSES

			$sql = "SELECT
						`customersMail1` AS `searchResult`

						FROM `" . TABLE_CUSTOMERS. "`

						WHERE 1
							AND (
								`customersMail1` LIKE '%" . $thisSearchString . "%'
								OR
								`customersKundennummer` = '" . $thisSearchString . "'
							)

						LIMIT " . MAX_SEARCH_SUGGESTIONS . "

					UNION

					SELECT
						`customersMail2` AS `searchResult`

						FROM `" . TABLE_CUSTOMERS. "`

						WHERE 1
							AND (
								`customersMail2` LIKE '%" . $thisSearchString . "%'
								OR
								`customersKundennummer` = '" . $thisSearchString . "'
							)

						LIMIT " . MAX_SEARCH_SUGGESTIONS . "

					UNION

					SELECT
						`suppliersMail1` AS `searchResult`

						FROM `" . TABLE_SUPPLIERS. "`

						WHERE 1
							AND (
								`suppliersMail1` LIKE '%" . $thisSearchString . "%'
								OR
								`suppliersKundennummer` = '" . $thisSearchString . "'
							)

						LIMIT " . MAX_SEARCH_SUGGESTIONS . "

					UNION

					SELECT
						`suppliersMail2` AS `searchResult`

						FROM `" . TABLE_SUPPLIERS. "`

						WHERE 1
							AND (
								`suppliersMail2` LIKE '%" . $thisSearchString . "%'
								OR
								`suppliersKundennummer` = '" . $thisSearchString . "'
							)

						LIMIT " . MAX_SEARCH_SUGGESTIONS . "
				";

			$rs = $dbConnection->db_query($sql);

			while($ds = mysqli_fetch_assoc($rs)) {
				if($ds["searchResult"] != ''){
					#$arrSearchResults[] = $ds["searchResult"];
					$arrSearchResults[] = $strCurrentMailAdresses . $ds["searchResult"];
				}
			}

			if(!empty($arrSearchResults)) {
				#for($i = 0; $i < MAX_SEARCH_SUGGESTIONS ; $i++) {
				for($i = 0; $i < count($arrSearchResults) ; $i++) {
					$thisSearchResult = $arrSearchResults[$i];
					$thisSearchString = $thisSearchString;
					$displaySearchResult = preg_replace('/'.$thisSearchString.'/ism', '[span class={###QUOT###}foundString{###QUOT###}]'.($thisSearchString).'[/span]', $thisSearchResult);
					$content .= '[div class={###QUOT###}searchResultItem{###QUOT###}]' . $displaySearchResult . '[/div]';
				}
			}

			if($dbConnection->db_displayErrors() != "") {
				$errorMessage .= $dbConnection->db_displayErrors(). '<br />';
			}

			if($dbConnection) {
				$dbConnection->db_close();
			}
		}

		else if($_GET["type"] == "searchBankName" || $_GET["type"] == "searchBankCode") {
			$dbConnection = new DB_Connection();
			$db_open = $dbConnection->db_connect();

			$sql = "
				SELECT
					CONCAT(
						'[span class={###QUOT###}searchBankCode{###QUOT###}]', `bankdatasBankleitzahl`, '[/span]', ': ',
						'[span class={###QUOT###}searchBankName{###QUOT###}]', `bankdatasBezeichnung`, '[/span]', ' - ',
						'[span class={###QUOT###}searchBankBIC{###QUOT###}]', `bankdatasBIC`, '[/span]'
					) AS `searchResult`

					FROM `" . TABLE_BANK_DATAS . "`

					WHERE
			";

			if($_GET["type"] == "searchBankName") {
				$sql .= " `bankdatasBezeichnung` LIKE '%" . $thisSearchString . "%' ORDER BY `bankdatasBezeichnung` ";
			}
			else if($_GET["type"] == "searchBankCode") {
					$sql .= " `bankdatasBankleitzahl` LIKE '" . $thisSearchString . "%' ORDER BY `bankdatasBankleitzahl` ";
			}

			$sql .= "
					LIMIT " . MAX_SEARCH_SUGGESTIONS . "
			";

			$rs = $dbConnection->db_query($sql);

			while($ds = mysqli_fetch_assoc($rs)) {
				$arrSearchResults[] = $ds["searchResult"];
			}

			if(!empty($arrSearchResults)) {
				#for($i = 0; $i < MAX_SEARCH_SUGGESTIONS ; $i++) {
				for($i = 0; $i < count($arrSearchResults) ; $i++) {
					$thisSearchResult = $arrSearchResults[$i];
					$thisSearchString = $thisSearchString;
					$displaySearchResult = preg_replace('/^'.$thisSearchString.'/ism', '[span class={###QUOT###}foundString{###QUOT###}]'.ucfirst($thisSearchString).'[/span]', $thisSearchResult);
					$content .= '[div class={###QUOT###}searchResultItem{###QUOT###}]' . $displaySearchResult . '[/div]';
				}
			}
			if($dbConnection->db_displayErrors() != "") {
				$errorMessage .= $dbConnection->db_displayErrors(). '<br />';
			}

			if($dbConnection) {
				$dbConnection->db_close();
			}
		}

		else if($_GET["type"] == "searchCustomerNumberDatas") {
			$dbConnection = new DB_Connection();
			$db_open = $dbConnection->db_connect();

			$where = "";
			if(preg_match("/^[a-zA-Z]/", $thisSearchString)) {
				$where = " `dataCustomer`.`customersFirmenname` LIKE '%" . $thisSearchString . "%' ";
				// $sql .= " MATCH(`customersFirmenname`) AGAINST '" . $thisSearchString . "' ";
			}
			if(preg_match("/^[0-9]/", $thisSearchString)) {
				$where = " `dataCustomer`.`customersKundennummer` LIKE '" . $thisSearchString . "%' ";
			}
			if(preg_match("/^[A-Z]{2}[\-]/", $thisSearchString)) {
				$where = " `dataCustomer`.`customersKundennummer` LIKE '" . $thisSearchString . "%' ";
			}
			if(preg_match("/^[A-Z]{3}[0-9]/", $thisSearchString)) {
				$where = " `dataCustomer`.`customersKundennummer` LIKE '" . $thisSearchString . "%' ";
			}

			$sql_x = "SELECT
						CONCAT(
							'[span class={###QUOT###}customerNumber{###QUOT###}]', `customersKundennummer`, '[/span]', ': ',
							'[span class={###QUOT###}customerName{###QUOT###}]', `customersFirmenname`, '[/span]', ' - ',
							'[span class={###QUOT###}customerZipCode{###QUOT###}]', `customersCompanyPLZ`, '[/span]', ' ',
							'[span class={###QUOT###}customerCity{###QUOT###}]', `customersCompanyOrt`, '[/span]', ' ',
							'[span class={###QUOT###}customerLand{###QUOT###}]', `customersCompanyCountry`, '[/span]', ' ',
							'[span class={###QUOT###}salesmanName{###QUOT###}]', `salesmansFirmenname`, '[/span]', ' ',
							'[span class={###QUOT###}customerMail{###QUOT###}]', `customersMail`, '[/span]'
						) AS `searchResult`

						FROM (

							SELECT
								`dataCustomer`.`customersKundennummer` AS `customersKundennummer`,
								`dataCustomer`.`customersFirmenname` AS `customersFirmenname`,
								`dataCustomer`.`customersCompanyPLZ` AS `customersCompanyPLZ`,
								`dataCustomer`.`customersCompanyOrt` AS `customersCompanyOrt`,

								`dataSalesman`.`customersFirmenname` AS `salesmansFirmenname`,

								IF(`dataSalesman`.`customersMail1` != '',
									`dataSalesman`.`customersMail1`,

									IF(`dataSalesman`.`customersMail2` != '',
										`dataSalesman`.`customersMail2`,
										IF(`dataCustomer`.`customersMail1` != '',
											`dataCustomer`.`customersMail1`,

											IF(`dataCustomer`.`customersMail2` != '',
												`dataCustomer`.`customersMail2`,
												''
											)
										)
									)
								)  AS `customersMail`

								FROM `" . TABLE_CUSTOMERS . "` AS `dataCustomer`
								LEFT JOIN `" . TABLE_CUSTOMERS . "` AS `dataSalesman`
								ON(`dataCustomer`.`customersVertreterID` = `dataSalesman`.`customersID`)

								WHERE  " . $where . "
									AND `dataCustomer`.`customersActive` = '1'

						) AS `tempTable`
			";

			$sql = "SELECT

							CONCAT('[span class={###QUOT###}customerNumber{###QUOT###}]', `tempTable`.`customersKundennummer`, '[/span]', ': ') AS `searchResult1`,
							CONCAT('[span class={###QUOT###}customerName{###QUOT###}]', `tempTable`.`customersFirmenname`, '[/span]', ' - ') AS `searchResult2`,
							CONCAT('[span class={###QUOT###}customerZipCode{###QUOT###}]', `tempTable`.`customersCompanyPLZ`, '[/span]', ' ') AS `searchResult3`,
							CONCAT('[span class={###QUOT###}customerCity{###QUOT###}]', `tempTable`.`customersCompanyOrt`, '[/span]', ' # ') AS `searchResult4`,
							CONCAT('[span class={###QUOT###}customerCountry{###QUOT###}]', `tempTable`.`customersCompanyCountry`, '[/span]', ' # ') AS `searchResult8`,
							CONCAT('[span class={###QUOT###}salesmanName{###QUOT###}]', `tempTable`.`salesmansFirmenname`, '[/span]', ' : ') AS `searchResult5`,
							CONCAT('[span class={###QUOT###}salesmanID{###QUOT###}]', `tempTable`.`salesmansID`, '[/span]', ' - ') AS `searchResult6`,
							CONCAT('[span class={###QUOT###}customerMail{###QUOT###}]', `tempTable`.`customersMail`, '[/span]' ) AS `searchResult7`


						FROM (
							SELECT
								`dataCustomer`.`customersKundennummer` AS `customersKundennummer`,
								`dataCustomer`.`customersFirmenname` AS `customersFirmenname`,
								`dataCustomer`.`customersCompanyPLZ` AS `customersCompanyPLZ`,
								`dataCustomer`.`customersCompanyOrt` AS `customersCompanyOrt`,
								`dataCustomer`.`customersCompanyCountry` AS `customersCompanyCountry`,
								`dataSalesman`.`customersFirmenname` AS `salesmansFirmenname`,
								`dataSalesman`.`customersID` AS `salesmansID`,
								IF(`dataSalesman`.`customersMail1` != '',
									`dataSalesman`.`customersMail1`,
									IF(`dataSalesman`.`customersMail2` != '',
										`dataSalesman`.`customersMail2`,
										IF(`dataCustomer`.`customersMail1` != '',
											`dataCustomer`.`customersMail1`,
											IF(`dataCustomer`.`customersMail2` != '',
												`dataCustomer`.`customersMail2`, ''
											)
										)
									)
								) AS `customersMail`

								FROM `" . TABLE_CUSTOMERS . "` AS `dataCustomer`
								LEFT JOIN `" . TABLE_CUSTOMERS . "` AS `dataSalesman`
								ON(`dataCustomer`.`customersVertreterID` = `dataSalesman`.`customersID`)
								WHERE " . $where . "
									AND `dataCustomer`.`customersActive` = '1'

						) AS `tempTable`
			";
			#echo $sql;

			$sql_y = "
				SELECT
					CONCAT(
						'[span class={###QUOT###}customerNumber{###QUOT###}]', `customersKundennummer`, '[/span]', ': ',
						'[span class={###QUOT###}customerName{###QUOT###}]', `customersFirmenname`, '[/span]', ' - ',
						'[span class={###QUOT###}customerZipCode{###QUOT###}]', `customersCompanyPLZ`, '[/span]', ' ',
						'[span class={###QUOT###}customerCity{###QUOT###}]', `customersCompanyOrt`, '[/span]'
					) AS `searchResult`

					FROM `" . TABLE_CUSTOMERS . "` AS `dataCustomer`

					FORCE INDEX (`customersKundennummer`, `customersFirmenname`)

					WHERE " . $where . "
						AND `dataCustomer`.`customersActive` = '1'
			";

			$sql .= "
					LIMIT " . MAX_SEARCH_SUGGESTIONS . "
			";

			$rs = $dbConnection->db_query($sql);

			while($ds = mysqli_fetch_assoc($rs)) {
				#$arrSearchResults[] = $ds["searchResult"];
				$arrSearchResults[] = $ds["searchResult1"] . $ds["searchResult2"] . $ds["searchResult3"] . $ds["searchResult4"] . $ds["searchResult8"] . $ds["searchResult5"] . $ds["searchResult6"] . $ds["searchResult7"];
			}

			if(!empty($arrSearchResults)) {

				#for($i = 0; $i < MAX_SEARCH_SUGGESTIONS ; $i++) {
				for($i = 0; $i < count($arrSearchResults) ; $i++) {
					$thisSearchResult = $arrSearchResults[$i];
					$thisSearchString = $thisSearchString;
					$displaySearchResult = preg_replace('/^'.$thisSearchString.'/ism', '[span class={###QUOT###}foundString{###QUOT###}]'.ucfirst($thisSearchString).'[/span]', $thisSearchResult);
					$content .= '[div class={###QUOT###}searchResultItem{###QUOT###}]' . $displaySearchResult . '[/div]';
				}
			}
			if($dbConnection->db_displayErrors() != "") {
				$errorMessage .= $dbConnection->db_displayErrors(). '<br />';
			}
			if($dbConnection) {
				$dbConnection->db_close();
			}
		}
		else if($_GET["type"] == "searchProduct") {

			if(LOAD_EXTERNAL_SHOP_DATAS == true){
				$dbConnection_Extern = new DB_Connection(DB_HOST_EXTERN_SHOP, '', DB_NAME_EXTERN_SHOP, DB_USER_EXTERN_SHOP, DB_PASSWORD_EXTERN_SHOP);
				$db_openExtern = $dbConnection_Extern->db_connect();
			}
			else {
				$dbConnection_Extern = new DB_Connection();
				$db_openExtern = $dbConnection_Extern->db_connect();
			}

			$sql = "SET NAMES latin1";
			$rs = $dbConnection_Extern->db_query($sql);

			$sql = "";
			if(preg_match("/^[a-zA-Z]/", $thisSearchString)) {
				$sql = "
					SELECT
						`products_name` AS `searchResult`

						FROM `" . TABLE_PRODUCTS_DESCRIPTION . "`
						LEFT JOIN `" . TABLE_PRODUCTS . "`
						ON(`" . TABLE_PRODUCTS_DESCRIPTION . "`.`products_id` = `" . TABLE_PRODUCTS . "`.`products_id`)

						WHERE `products_name` LIKE '" . ($thisSearchString) . "%'
							/* AND `products_status` = 1 */

						LIMIT " . MAX_SEARCH_SUGGESTIONS . "";

			}
			// $sql = " UNION ";
			else if(preg_match("/^[0-9]/", $thisSearchString)) {
				$sql = "
					SELECT
						`products_model` AS `searchResult`

						FROM `" . TABLE_PRODUCTS . "`

						WHERE `products_model` LIKE '" . ($thisSearchString) . "%'
							/* AND `products_status` = 1 */

						LIMIT " . MAX_SEARCH_SUGGESTIONS . "
				";
			}

			if($dbConnection_Extern) {
				$dbConnection_Extern->db_close();
			}

			while($ds = mysqli_fetch_assoc($rs)) {
				$arrSearchResults[] = $ds["searchResult"];
			}

			if(!empty($arrSearchResults)) {
				#for($i = 0; $i < MAX_SEARCH_SUGGESTIONS ; $i++) {
				for($i = 0; $i < count($arrSearchResults) ; $i++) {
					$thisSearchResult = $arrSearchResults[$i];
					$thisSearchString = $thisSearchString;
					$displaySearchResult = preg_replace('/^'.$thisSearchString.'/ism', '[span class={###QUOT###}foundString{###QUOT###}]'.ucfirst($thisSearchString).'[/span]', $thisSearchResult);
					$content .= '[div class={###QUOT###}searchResultItem{###QUOT###}]' . utf8_decode($displaySearchResult) . '[/div]';
				}
			}
			if($dbConnection_Extern->db_displayErrors() != "") {
				$errorMessage .= $dbConnection_Extern->db_displayErrors(). '<br />';
			}
			$dbConnection_Extern->db_close();
		}

		else if($_GET["type"] == "searchProductNumber") {

			if(LOAD_EXTERNAL_SHOP_DATAS == true){
				$dbConnection_Extern = new DB_Connection(DB_HOST_EXTERN_SHOP, '', DB_NAME_EXTERN_SHOP, DB_USER_EXTERN_SHOP, DB_PASSWORD_EXTERN_SHOP);
				$db_openExtern = $dbConnection_Extern->db_connect();
			}
			else {
				$dbConnection_Extern = new DB_Connection();
				$db_openExtern = $dbConnection_Extern->db_connect();
			}

			$sql = "SET NAMES latin1";
			$rs = $dbConnection_Extern->db_query($sql);			
			
			$sql = "
					SELECT 
						CONCAT(
							'[span class={###QUOT###}productNumber{###QUOT###}]', `" . TABLE_STOCK_PRODUCT_CATEGORIES . "`.`stockProductCategoriesProductNumber`, '[/span] - ',
							'[span class={###QUOT###}productName{###QUOT###}]', `" . TABLE_STOCK_PRODUCT_CATEGORIES . "`.`stockProductCategoriesName`, '[/span] # ',
							'[span class={###QUOT###}productID{###QUOT###}]', `" . TABLE_STOCK_PRODUCT_CATEGORIES . "`.`stockProductCategoriesID`, '[/span] # ',
							'[span class={###QUOT###}productCategoryID{###QUOT###}]', `" . TABLE_STOCK_PRODUCT_CATEGORIES . "`.`stockProductCategoriesLevelID`, '[/span]'
						) AS `searchResult`
						
						FROM `" . TABLE_STOCK_PRODUCT_CATEGORIES . "`
						
						WHERE 1
							AND `" . TABLE_STOCK_PRODUCT_CATEGORIES . "`.`stockProductCategoriesProductNumber` LIKE '" . utf8_decode($thisSearchString) . "%'
							AND `" . TABLE_STOCK_PRODUCT_CATEGORIES . "`.`stockProductCategoriesActive` = '1'
				";

			$rs = $dbConnection_Extern->db_query($sql);

			while($ds = mysqli_fetch_assoc($rs)) {
				$arrSearchResults[] = $ds["searchResult"];
			}

			if(!empty($arrSearchResults)) {
				#for($i = 0; $i < MAX_SEARCH_SUGGESTIONS ; $i++) {
				for($i = 0; $i < count($arrSearchResults) ; $i++) {
					$thisSearchResult = $arrSearchResults[$i];
					$thisSearchString = $thisSearchString;
					$displaySearchResult = preg_replace('/^'.$thisSearchString.'/ism', '[span class={###QUOT###}foundString{###QUOT###}]'.ucfirst($thisSearchString).'[/span]', $thisSearchResult);
					$content .= '[div class={###QUOT###}searchResultItem{###QUOT###}]' . ($displaySearchResult) . '[/div]';
				}
				$content = utf8_encode($content);
			}
			if($dbConnection_Extern->db_displayErrors() != "") {
				$errorMessage .= $dbConnection_Extern->db_displayErrors(). '<br />';
			}
			if($dbConnection_Extern) {
				$dbConnection_Extern->db_close();
			}
		}

		else if($_GET["type"] == "searchProductName") {
			if(LOAD_EXTERNAL_SHOP_DATAS == true){
				$dbConnection_Extern = new DB_Connection(DB_HOST_EXTERN_SHOP, '', DB_NAME_EXTERN_SHOP, DB_USER_EXTERN_SHOP, DB_PASSWORD_EXTERN_SHOP);
				$db_openExtern = $dbConnection_Extern->db_connect();
			}
			else {
				$dbConnection_Extern = new DB_Connection();
				$db_openExtern = $dbConnection_Extern->db_connect();
			}

			$sql = "SET NAMES latin1";
			$rs = $dbConnection_Extern->db_query($sql);

			$sql = "
					SELECT 
						CONCAT(
							'[span class={###QUOT###}productNumber{###QUOT###}]', `" . TABLE_STOCK_PRODUCT_CATEGORIES . "`.`stockProductCategoriesProductNumber`, '[/span] - ',
							'[span class={###QUOT###}productName{###QUOT###}]', `" . TABLE_STOCK_PRODUCT_CATEGORIES . "`.`stockProductCategoriesName`, '[/span] # ',
							'[span class={###QUOT###}productID{###QUOT###}]', `" . TABLE_STOCK_PRODUCT_CATEGORIES . "`.`stockProductCategoriesID`, '[/span] # ',
							'[span class={###QUOT###}productCategoryID{###QUOT###}]', `" . TABLE_STOCK_PRODUCT_CATEGORIES . "`.`stockProductCategoriesLevelID`, '[/span]'
						) AS `searchResult`
						
						FROM `" . TABLE_STOCK_PRODUCT_CATEGORIES . "`
						
						WHERE 1
							AND `" . TABLE_STOCK_PRODUCT_CATEGORIES . "`.`stockProductCategoriesName` LIKE '%" . utf8_decode($thisSearchString) . "%'
							AND `" . TABLE_STOCK_PRODUCT_CATEGORIES . "`.`stockProductCategoriesActive` = '1'
						
						ORDER BY `stockProductCategoriesName`
				";
			
			$rs = $dbConnection_Extern->db_query($sql);

			while($ds = mysqli_fetch_assoc($rs)) {
				$arrSearchResults[] = $ds["searchResult"];
			}

			if(!empty($arrSearchResults)) {
				#for($i = 0; $i < MAX_SEARCH_SUGGESTIONS ; $i++) {
				for($i = 0; $i < count($arrSearchResults) ; $i++) {
					$thisSearchResult = $arrSearchResults[$i];
					$thisSearchString = $thisSearchString;
					$displaySearchResult = preg_replace('/^'.$thisSearchString.'/ism', '[span class={###QUOT###}foundString{###QUOT###}]'.ucfirst($thisSearchString).'[/span]', $thisSearchResult);
					$content .= '[div class={###QUOT###}searchResultItem{###QUOT###}]' . $displaySearchResult . '[/div]';
				}
				$content = utf8_encode($content);
			}
			if($dbConnection_Extern->db_displayErrors() != "") {
				$errorMessage .= $dbConnection_Extern->db_displayErrors(). '<br />';
			}
			if($dbConnection_Extern) {
				$dbConnection_Extern->db_close();
			}
		}
		else if($_GET["type"] == "searchCustomerNumber" || $_GET["type"] == "searchCustomerNumberAndID") {
			$dbConnection = new DB_Connection();
			$db_open = $dbConnection->db_connect();

			$sql = "
				SELECT
					CONCAT(
							'[span class={###QUOT###}customerNumber{###QUOT###}]', `" . TABLE_CUSTOMERS . "`.`customersKundennummer`, '[/span] - ',
							'[span class={###QUOT###}customerName{###QUOT###}]', `" . TABLE_CUSTOMERS . "`.`customersFirmenname`, '[/span] # ',
							'[span class={###QUOT###}customerID{###QUOT###}]', `" . TABLE_CUSTOMERS . "`.`customersID`, '[/span]'
						) AS `searchResult`

					FROM `" . TABLE_CUSTOMERS . "`

					FORCE INDEX (`customersKundennummer`)

					WHERE `customersKundennummer` LIKE '" . $thisSearchString . "%'
						AND `" . TABLE_CUSTOMERS . "`.`customersActive` = '1'

					LIMIT " . MAX_SEARCH_SUGGESTIONS . "
			";

			$rs = $dbConnection->db_query($sql);

			while($ds = mysqli_fetch_assoc($rs)) {
				$arrSearchResults[] = ($ds["searchResult"]);
			}

			if(!empty($arrSearchResults)) {
				#for($i = 0; $i < MAX_SEARCH_SUGGESTIONS ; $i++) {
				for($i = 0; $i < count($arrSearchResults) ; $i++) {
					$thisSearchResult = $arrSearchResults[$i];
					$thisSearchString = $thisSearchString;
					$displaySearchResult = preg_replace('/^'.$thisSearchString.'/ism', '[span class="foundString"]'.ucfirst($thisSearchString).'[/span]', $thisSearchResult);
					$content .= '[div class={###QUOT###}searchResultItem{###QUOT###}]' . $displaySearchResult . '[/div]';
				}
			}
			if($dbConnection->db_displayErrors() != "") {
				$errorMessage .= $dbConnection->db_displayErrors(). '<br />';
			}
			if($dbConnection) {
				$dbConnection->db_close();
			}
		}
		else if($_GET["type"] == "searchCustomerName") {
			$dbConnection = new DB_Connection();
			$db_open = $dbConnection->db_connect();

			$sql = "
				SELECT
					CONCAT(
							'[span class={###QUOT###}customerNumber{###QUOT###}]', `" . TABLE_CUSTOMERS . "`.`customersKundennummer`, '[/span] - ',
							'[span class={###QUOT###}customerName{###QUOT###}]', `" . TABLE_CUSTOMERS . "`.`customersFirmenname`, '[/span] # ',
							'[span class={###QUOT###}customerID{###QUOT###}]', `" . TABLE_CUSTOMERS . "`.`customersID`, '[/span]'
						) AS `searchResult`

					FROM `" . TABLE_CUSTOMERS . "`

					FORCE INDEX (`customersFirmenname`)

					WHERE `customersFirmenname` LIKE '%" . $thisSearchString . "%'
						AND `" . TABLE_CUSTOMERS . "`.`customersActive` = '1'

					LIMIT " . MAX_SEARCH_SUGGESTIONS . "
			";

			// WHERE `customersFirmenname` LIKE '" . $thisSearchString . "%'

			$rs = $dbConnection->db_query($sql);

			while($ds = mysqli_fetch_assoc($rs)) {
				$arrSearchResults[] = $ds["searchResult"];
			}

			if(!empty($arrSearchResults)) {
				#for($i = 0; $i < MAX_SEARCH_SUGGESTIONS ; $i++) {
				for($i = 0; $i < count($arrSearchResults) ; $i++) {
					$thisSearchResult = $arrSearchResults[$i];
					$thisSearchString = $thisSearchString;
					$displaySearchResult = preg_replace('/^'.$thisSearchString.'/ism', '[span class="foundString"]'.ucfirst($thisSearchString).'[/span]', $thisSearchResult);
					$content .= '[div class={###QUOT###}searchResultItem{###QUOT###}]' . $displaySearchResult . '[/div]';
				}
			}
			if($dbConnection->db_displayErrors() != "") {
				$errorMessage .= $dbConnection->db_displayErrors(). '<br />';
			}
			if($dbConnection) {
				$dbConnection->db_close();
			}
		}
		else if($_GET["type"] == "searchSupplierNumber") {

			$dbConnection = new DB_Connection();
			$db_open = $dbConnection->db_connect();

			$thisSearchString = PREFIX_SUPPLIER_NUMBER . preg_replace("/" . PREFIX_SUPPLIER_NUMBER . "/", "", $thisSearchString);

			$sql = "
				SELECT
					CONCAT(
							'[span class={###QUOT###}supplierNumber{###QUOT###}]', `" . TABLE_SUPPLIERS . "`.`suppliersKundennummer`, '[/span] - ',
							'[span class={###QUOT###}supplierName{###QUOT###}]', `" . TABLE_SUPPLIERS . "`.`suppliersFirmenname`, '[/span] # ',
							'[span class={###QUOT###}supplierID{###QUOT###}]', `" . TABLE_SUPPLIERS . "`.`suppliersID`, '[/span]'
						) AS `searchResult`

					FROM `" . TABLE_SUPPLIERS . "`

					FORCE INDEX (`suppliersKundennummer`)

					WHERE `suppliersKundennummer` LIKE '" . $thisSearchString . "%'

					LIMIT " . MAX_SEARCH_SUGGESTIONS . "
			";

			$rs = $dbConnection->db_query($sql);

			while($ds = mysqli_fetch_assoc($rs)) {
				$arrSearchResults[] = $ds["searchResult"];
			}

			if(!empty($arrSearchResults)) {
				#for($i = 0; $i < MAX_SEARCH_SUGGESTIONS ; $i++) {
				for($i = 0; $i < count($arrSearchResults) ; $i++) {
					$thisSearchResult = $arrSearchResults[$i];
					$thisSearchString = $thisSearchString;
					$displaySearchResult = preg_replace('/^'.$thisSearchString.'/ism', '[span class="foundString"]'.ucfirst($thisSearchString).'[/span]', $thisSearchResult);
					$content .= '[div class={###QUOT###}searchResultItem{###QUOT###}]' . $displaySearchResult . '[/div]';
				}
			}
			if($dbConnection->db_displayErrors() != "") {
				$errorMessage .= $dbConnection->db_displayErrors(). '<br />';
			}
			if($dbConnection) {
				$dbConnection->db_close();
			}
		}
		else if($_GET["type"] == "searchSupplierName") {
			$dbConnection = new DB_Connection();
			$db_open = $dbConnection->db_connect();

			$sql = "
				SELECT
					CONCAT(
							'[span class={###QUOT###}supplierNumber{###QUOT###}]', `" . TABLE_SUPPLIERS . "`.`suppliersKundennummer`, '[/span] - ',
							'[span class={###QUOT###}supplierName{###QUOT###}]', `" . TABLE_SUPPLIERS . "`.`suppliersFirmenname`, '[/span] # ',
							'[span class={###QUOT###}supplierID{###QUOT###}]', `" . TABLE_SUPPLIERS . "`.`suppliersID`, '[/span]'
						) AS `searchResult`

					FROM `" . TABLE_SUPPLIERS . "`

					FORCE INDEX (`suppliersFirmenname`)

					WHERE `suppliersFirmenname` LIKE '%" . $thisSearchString . "%'

					LIMIT " . MAX_SEARCH_SUGGESTIONS . "
			";

			// WHERE `suppliersFirmenname` LIKE '" . $thisSearchString . "%'

			$rs = $dbConnection->db_query($sql);

			while($ds = mysqli_fetch_assoc($rs)) {
				$arrSearchResults[] = $ds["searchResult"];
			}

			if(!empty($arrSearchResults)) {
				#for($i = 0; $i < MAX_SEARCH_SUGGESTIONS ; $i++) {
				for($i = 0; $i < count($arrSearchResults) ; $i++) {
					$thisSearchResult = $arrSearchResults[$i];
					$thisSearchString = $thisSearchString;
					$displaySearchResult = preg_replace('/^'.$thisSearchString.'/ism', '[span class="foundString"]'.ucfirst($thisSearchString).'[/span]', $thisSearchResult);
					$content .= '[div class={###QUOT###}searchResultItem{###QUOT###}]' . $displaySearchResult . '[/div]';
				}
			}
			if($dbConnection->db_displayErrors() != "") {
				$errorMessage .= $dbConnection->db_displayErrors(). '<br />';
			}
			if($dbConnection) {
				$dbConnection->db_close();
			}
		}
		else if($_GET["type"] == "searchSalesmen") {
			$dbConnection = new DB_Connection();
			$db_open = $dbConnection->db_connect();

				// BOF READ CUSTOMER TYPES
					$arrCustomerTypeDatas = getCustomerTypes();
				// EOF READ CUSTOMER TYPES

			$sql = "SELECT

					CONCAT(
							'[span class={###QUOT###}customerNumber{###QUOT###}]', `" . TABLE_CUSTOMERS . "`.`customersKundennummer`, '[/span] - ',
							'[span class={###QUOT###}customerName{###QUOT###}]', `" . TABLE_CUSTOMERS . "`.`customersFirmenname`, '[/span] # ',
							'[span class={###QUOT###}customerID{###QUOT###}]', `" . TABLE_CUSTOMERS . "`.`customersID`, '[/span]'
						) AS `searchResult`

					FROM `" . TABLE_CUSTOMERS . "`
					WHERE (
						`customersTyp` = 2
						OR `customersTyp` = 3
						OR `customersTyp` = 4
						OR `customersTyp` = 9
						OR `customersTyp` = 11
						) AND (
							`customersKundennummer` LIKE '%" . $thisSearchString . "%'
							OR `customersFirmenname` LIKE '%" . $thisSearchString . "%'
						)
						AND `" . TABLE_CUSTOMERS . "`.`customersActive` = '1'
					ORDER BY `customersFirmenname`
			";

			$rs = $dbConnection->db_query($sql);

			while($ds = mysqli_fetch_assoc($rs)) {
				$arrSearchResults[] = $ds["searchResult"];
			}

			if(!empty($arrSearchResults)) {
				#for($i = 0; $i < MAX_SEARCH_SUGGESTIONS ; $i++) {
				for($i = 0; $i < count($arrSearchResults) ; $i++) {
					$thisSearchResult = $arrSearchResults[$i];
					$thisSearchString = $thisSearchString;
					$displaySearchResult = preg_replace('/^'.$thisSearchString.'/ism', '[span class="foundString"]'.ucfirst($thisSearchString).'[/span]', $thisSearchResult);
					$content .= '[div class={###QUOT###}searchResultItem{###QUOT###}]' . $displaySearchResult . '[/div]';
				}
			}

			if($dbConnection->db_displayErrors() != "") {
				$errorMessage .= $dbConnection->db_displayErrors(). '<br />';
			}
			if($dbConnection) {
				$dbConnection->db_close();
			}
		}
		else if($_GET["type"] == "searchPlzCity" || $_GET["type"] == "searchPLZ") {
			$dbConnection = new DB_Connection();
			$db_open = $dbConnection->db_connect();

			$sql = "SELECT
						CONCAT(
							'[span class={###QUOT###}cityZipCode{###QUOT###}]', `" . TABLE_ZIPCODES_CITIES . "`.`postal_code`, '[/span] - ',
							'[span class={###QUOT###}cityName{###QUOT###}]', `" . TABLE_ZIPCODES_CITIES . "`.`place_name`, '[/span] # ',
							'[span class={###QUOT###}cityState{###QUOT###}]', `" . TABLE_ZIPCODES_CITIES . "`.`firstOrderSubdivision_stateName`, '[/span]'
						) AS `searchResult`

					FROM `" . TABLE_ZIPCODES_CITIES . "`
					WHERE `postal_code` LIKE '" . $thisSearchString . "%'
			";

			$rs = $dbConnection->db_query($sql);

			while($ds = mysqli_fetch_assoc($rs)) {
				$arrSearchResults[] = $ds["searchResult"];
			}

			if(!empty($arrSearchResults)) {
				#for($i = 0; $i < MAX_SEARCH_SUGGESTIONS ; $i++) {
				for($i = 0; $i < count($arrSearchResults) ; $i++) {
					$thisSearchResult = $arrSearchResults[$i];
					$thisSearchString = $thisSearchString;
					$displaySearchResult = preg_replace('/^'.$thisSearchString.'/ism', '[span class="foundString"]'.ucfirst($thisSearchString).'[/span]', $thisSearchResult);
					$content .= '[div class={###QUOT###}searchResultItem{###QUOT###}]' . $displaySearchResult . '[/div]';
				}
			}
			if($dbConnection->db_displayErrors() != "") {
				$errorMessage .= $dbConnection->db_displayErrors(). '<br />';
			}
			if($dbConnection) {
				$dbConnection->db_close();
			}
		}
		else if($_GET["type"] == "searchCityPlz" || $_GET["type"] == "searchCity") {
			$dbConnection = new DB_Connection();
			$db_open = $dbConnection->db_connect();

			$sql = "SELECT
						CONCAT(
							'[span class={###QUOT###}cityZipCode{###QUOT###}]', `" . TABLE_ZIPCODES_CITIES . "`.`postal_code`, '[/span] - ',
							'[span class={###QUOT###}cityName{###QUOT###}]', `" . TABLE_ZIPCODES_CITIES . "`.`place_name`, '[/span] # ',
							'[span class={###QUOT###}cityState{###QUOT###}]', `" . TABLE_ZIPCODES_CITIES . "`.`firstOrderSubdivision_stateName`, '[/span]'
						) AS `searchResult`

					FROM `" . TABLE_ZIPCODES_CITIES . "`
					WHERE `place_name` LIKE '" . $thisSearchString . "%'
			";

			$rs = $dbConnection->db_query($sql);

			while($ds = mysqli_fetch_assoc($rs)) {
				$arrSearchResults[] = $ds["searchResult"];
			}

			if(!empty($arrSearchResults)) {
				#for($i = 0; $i < MAX_SEARCH_SUGGESTIONS ; $i++) {
				for($i = 0; $i < count($arrSearchResults) ; $i++) {
					$thisSearchResult = $arrSearchResults[$i];
					$thisSearchString = $thisSearchString;
					$displaySearchResult = preg_replace('/^'.$thisSearchString.'/ism', '[span class="foundString"]'.ucfirst($thisSearchString).'[/span]', $thisSearchResult);
					$content .= '[div class={###QUOT###}searchResultItem{###QUOT###}]' . $displaySearchResult . '[/div]';
				}
			}

			if($dbConnection->db_displayErrors() != "") {
				$errorMessage .= $dbConnection->db_displayErrors(). '<br />';
			}
			if($dbConnection) {
				$dbConnection->db_close();
			}
		}
		else if($_GET["type"] == "searchSalesmanName") {

			$dbConnection = new DB_Connection();
			$db_open = $dbConnection->db_connect();
			$sql = "
				SELECT
					`ordersVertreter`,
					CONCAT(
						'[span class={###QUOT###}SalesmanName{###QUOT###}]', `" . TABLE_ORDERS . "`.`ordersVertreter`, '[/span]'
					) AS `searchResult`

					FROM `" . TABLE_ORDERS . "`

					WHERE `ordersVertreter` LIKE '" . $thisSearchString . "%'

					GROUP BY `ordersVertreter`

					LIMIT " . MAX_SEARCH_SUGGESTIONS . "
			";

			$rs = $dbConnection->db_query($sql);

			while($ds = mysqli_fetch_assoc($rs)) {
				$arrSearchResults[] = $ds["searchResult"];
			}

			if(!empty($arrSearchResults)) {
				#for($i = 0; $i < MAX_SEARCH_SUGGESTIONS ; $i++) {
				for($i = 0; $i < count($arrSearchResults) ; $i++) {
					$thisSearchResult = $arrSearchResults[$i];
					$thisSearchString = $thisSearchString;
					$displaySearchResult = preg_replace('/'.$thisSearchString.'/ism', '[span class={###QUOT###}foundString{###QUOT###}]'.ucfirst($thisSearchString).'[/span]', $thisSearchResult);
					$content .= '[div class={###QUOT###}searchResultItem{###QUOT###}]' . $displaySearchResult . '[/div]';
				}
			}
			if($dbConnection->db_displayErrors() != "") {
				$errorMessage .= $dbConnection->db_displayErrors(). '<br />';
			}
			if($dbConnection) {
				$dbConnection->db_close();
			}
		}
		else if($_GET["type"] == "searchStreetName") {
			$dbConnection = new DB_Connection();
			$db_open = $dbConnection->db_connect();
			$thisSearchString = rawurlencode($thisSearchString);
			$sql = "
				SELECT
					`" . TABLE_GEODB_STREET_NAMES . "`.`name` AS `searchResult`

					FROM `" . TABLE_GEODB_STREET_NAMES . "`

					WHERE `name` LIKE '" . $thisSearchString . "%'

					GROUP BY `name`

					LIMIT " . MAX_SEARCH_SUGGESTIONS . "
				";

			$rs = $dbConnection->db_query($sql);

			while($ds = mysqli_fetch_assoc($rs)) {
				$arrSearchResults[] = $ds["searchResult"];
			}

			if(!empty($arrSearchResults)) {
				#for($i = 0; $i < MAX_SEARCH_SUGGESTIONS ; $i++) {
				for($i = 0; $i < count($arrSearchResults) ; $i++) {
					$thisSearchResult = $arrSearchResults[$i];
					$thisSearchString = $thisSearchString;
					$displaySearchResult = preg_replace('/'.$thisSearchString.'/ism', '[span class={###QUOT###}foundString{###QUOT###}]'.ucfirst($thisSearchString).'[/span]', $thisSearchResult);
					$content .= '[div class={###QUOT###}searchResultItem{###QUOT###}]' . $displaySearchResult . '[/div]';
				}
			}
			if($dbConnection->db_displayErrors() != "") {
				$errorMessage .= $dbConnection->db_displayErrors(). '<br />';
			}
			if($dbConnection) {
				$dbConnection->db_close();
			}
		}

		// $content = $sql;
		// $content = htmlentities($content);

		// $content = utf8_decode($content);
		// $content = htmlentities($content);
		$content = preg_replace("/\[span(.*)\](.*)\[\/span\]/ismU", "<span $1>$2</span>", $content);

		$content = preg_replace('/\[div id={###QUOT###}searchResultHeader{###QUOT###}\](.*)\[\/div\]/ismU', '<div id="searchResultHeader">$1</div>', $content);
		$content = preg_replace('/\[div class={###QUOT###}searchResultItem{###QUOT###}\](.*)\[\/div\]/ismU', '<div class="searchResultItem">$1</div>', $content);
		$content = preg_replace('/\[div id={###QUOT###}searchResultFooter{###QUOT###}\](.*)\[\/div\]/ismU', '<div id="searchResultFooter">$1</div>', $content);
		$content = preg_replace('/\[div id={###QUOT###}searchResultContents{###QUOT###}](.*)\[\/div\]/ismU', '<div id="searchResultContents">$1</div>', $content);

		$content = preg_replace('/\[div(.*)\](.*)\[\/div\]/ismU', '<div $1>$2</div>', $content);
		$content = preg_replace('/{###QUOT###}/ismU', '"', $content);
		$content = preg_replace('/{###CLOSE_ICON###}/ismU', '<img src="layout/windowClose.gif" class="iconClose" width="14" height="14" alt="" title="Liste schließen">', $content);

		#$content = $sql;
		echo $content;

		#$fp = fopen('sqlExternTest.sql', 'a');
		#fwrite($fp, $sql);
		#fclose($fp);
	}
?>