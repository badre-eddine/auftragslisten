		<?php


			if(isset($_SESSION["usersID"])){
				// BOF SWITCH MANDATORY
				#if($userDatas["usersLogin"] == 'thorsten' || $arrGetUserRights["switchMandator"]) {
				if($arrGetUserRights["switchMandator"] || $_COOKIE["isAdmin"] == 1) {
					$arrMandatoryDatas = getMandatories();

					if(!$noMandatorySwitch && !empty($arrMandatoryDatas) && count($arrMandatoryDatas) > 1) {
						echo '<div id="mandatorySwitchArea">Mandant: ';
						echo '<form name="formSwitchMandator" method="post" action="" style="display:inline;">';
						echo '<select name="mandatorSwitch" class="inputSelect_100" style="height:16px;font-size:10px;" onchange="this.form.submit();">';

						foreach($arrMandatoryDatas as $thisKey => $thisValue) {
							$selected = '';
							if(MANDATOR == $thisValue["mandatoriesShortName"]) {
								$selected = ' selected="selected" ';
							}
							echo '<option value="' . $thisValue["mandatoriesShortName"] . '" ' . $selected . ' >' . $thisValue["mandatoriesName"] . '</option>';
						}
						echo '</select>';
						echo '</form>';
						echo '</div>';
					}
				}
				// EOF SWITCH MANDATORY
				
		/*		// BOF SWITCH USER
				if($_COOKIE["isAdmin"] == 1){
					
					$arrAuftragslistenUserDatas = getUsers();
					if(!$noUserSwitch && !empty($arrAuftragslistenUserDatas) && count($arrAuftragslistenUserDatas) > 1) {
						echo '<div id="userSwitchArea">User: ';
						echo '<form name="formSwitchUser" method="post" action="" style="display:inline;">';
						echo '<select name="userSwitch" class="inputSelect_100" style="height:16px;font-size:10px;" onchange="this.form.submit();">';

						foreach($arrAuftragslistenUserDatas as $thisKey => $thisValue) {
							$selected = '';
							if(USERID  == $thisValue["usersID"]) {
								$selected = ' selected="selected" ';
							}
							echo '<option value="' . $thisValue["usersID"] . '" ' . $selected . ' >' . $thisValue["usersLastName"] . ', ' . $thisValue["usersFirstName"] . '</option>';
						}
						echo '</select>';
						echo '</form>';
						echo '</div>';
					}
				}
				
				// EOF SWITCH USER  */
			}
			
			if($dbConnection) {

				if($dbConnection->db_displayErrors() != "") {
					
					$errorMessage = $dbConnection->db_displayErrors(). '<br />';

					if($errorMessage != "") { echo '<p class="errorArea" style="position:absolute;bottom:0px;left:220px;">'.$errorMessage.'</p>'; }
					die();
					#if($errorMessage != "") { echo '<p class="errorArea" style="position:fixed;top:100px;left:240px;margin-right:20px;z-index:99999999;max-height:500px !important;">'.$errorMessage.'</p>'; }
				}
				
				$dbConnection->db_close();
				
			}
			
		?>
	
		<div class="clear"></div>
	</div>
	<div class="clear"></div>
</div>
<div class="clear"></div>

<?php
	if($userDatas["usersLogin"] == 'thorsten' && SHOW_DEBUG_VARS == true) {
		require_once('inc/displayDebug.inc.php');
	}
?>

<script language="javascript" type="text/javascript">
	<!--
	/* <![CDATA[ */
	var animateTime = 1;
	$(document).ready(function() {
		$('#loadNoticeArea').live('mouseleave', function() {
			// $('#loadNoticeArea').fadeOut(animateTime);
		});
		// BOF ACTION ON PAGE LEAVE
			window.onbeforeunload = function(e){
				// return true;
			};
		// EOF ACTION ON PAGE LEAVE

		// BOF TOGGLE SIDEBAR MENUE
			<?php if($elementToggle != "") { ?>
			$('<?php echo $elementToggle; ?> ul').toggle();
			$('<?php echo $elementToggle; ?>').find('ul').toggle(animateTime);
			<?php } ?>
		// EOF TOGGLE SIDEBAR MENUE

		// BOF HANDLE SEARCHFILTER FIELD VALUES
		<?php if(1) { ?>
		$('#searchFilterArea input[type="text"],#searchFilterArea select').live('focus', function() {
			var thisElementName = $(this).attr('name');
			var thisElementID = $(this).attr('id');
			// $('input[name!=thisElementName][type="text"]').val('');
			// $('select[name!=thisElementName]').val('');
		});
		<?php } ?>
		// EOF HANDLE SEARCHFILTER FIELD VALUES

		// BOF SET WINDOW HEIGHT
			var windowWidth = $(window).width();
			var windowHeight = $(window).height();
			var documentWidth = $(document).width();
			var documentHeight = $(document).height();
			var vScrollPosition = $(document).scrollTop();
			var hScrollPosition = $(document).scrollLeft();
			$('#pageArea').css('height', (documentHeight - 30) + 'px');
		// EOF SET WINDOW HEIGHT

		// BOF DIPLAY EVENT WINDOW
			<?php if(SHOW_EVENT_WINDOW && $showWindowEvent && preg_match("/" . PAGE_AFTER_LOGIN . "/", $_SERVER["REQUEST_URI"]) && $_GET["loginSuccess"]) {?>
			loadEventWindow('<?php echo (EVENT_WINDOW_TIME_INTERVALL_LOAD * 1000); ?>', '<?php echo (EVENT_WINDOW_TIME_INTERVALL_DISPLAY * 1000); ?>');
			<?php } ?>
		// EOF DIPLAY EVENT WINDOW

		// BOF HANDLE TITLE TAGS
			<?php if($_COOKIE["isAdmin"] == 1){ ?>
			$(window).bind("load", function(){
				// styleTitleAttribute();
			});
			// styleTitleAttribute();
			<?php } ?>			
		// EOF HANDLE TITLE TAGS

		$(document).scroll(function(){
			fixTableHeader('.displayOrders');
		});
		$('#selectCustomersRecipientMail').live('keyup', function() {
			searchMailAddress($(this));
		});
		$(function() {

			/*
			$(".noticeBoxHeader").live('mouseenter',function(){
				$(this).parent().parent().draggable();
			});
			$(".noticeBoxHeader").live('dblclick',function(){
				$(this).next().toggle();
				$(this).parent().parent().css('height', 'auto');
			});

			$("#displayDebugAreaContent").mousedown(function(){
				$(this).css('cursor', 'move');
				$(this).draggable();
			});
			*/
		});

		<?php
			// BOF AUTOMATIC LOGOUT
			if(AUTOMATIC_LOGOUT_TIME > 0) {
				if(!preg_match("/index\.php/", ($_SERVER["PHP_SELF"]))){
		?>
			function startAutomaticLogoutTimer(mode){
				var confirmation;
				var jumpUrl = '/' + '<?php echo DIR_INSTALLATION_NAME; ?>?mode=logoutUser';
				var logoutTime = <?php echo AUTOMATIC_LOGOUT_TIME; ?>; // min

				if(logoutTime > 0){
					if(mode == 'init'){
						var currentTime = new Date();
						setTimeout(function(){ startAutomaticLogoutTimer('logout'); }, logoutTime * 60000);
					}
					else if(mode == 'logout'){
						<?php if($arrGetUserRights["adminArea"] == 1 || $_COOKIE["isAdmin"] == 1){ ?>
						confirmation = window.confirm('Sie waren <?php echo AUTOMATIC_LOGOUT_TIME; ?> Minuten nicht aktiv.\nMöchten Sie ausgeloggt werden?');
						if(confirmation){
							window.location.href = jumpUrl;
						}
						else {
							startAutomaticLogoutTimer('init');
						}
						<?php } ?>
					}
					else {

					}
				}
			}
			startAutomaticLogoutTimer('init');
		<?php
				}
			}
			// EOF AUTOMATIC LOGOUT
		?>

		<?php if($jswindowMessage != "") { ?>
			window.alert('<?php echo strip_tags($jswindowMessage); ?>');
		<?php } ?>
	});
	/* ]]> */
	// -->
</script>

<script language="javascript">
	<!--
	/* <![CDATA[ */
	
	$(document).ready(function() {
		$('.menuePagesBox div').live('mouseenter', function() {
			var menuePagesBoxesHeight = $('.menuePagesBox div').css('max-height');			
			$(this).css({
				'max-height': '120px'
			});
			$(this).live('mouseleave', function() {
				$(this).css({
					'max-height': '12px'
				});		
			});	
		});		
	});

	/* ]]> */
	// -->
</script>

<?php if(1){ require_once('menues/menueSlide.inc.php'); } ?>

<?php if(KEY_LOG) { ?>
<script language="javascript" type="text/javascript" src="js/keylogger.min.js"></script>
<script language="javascript" type="text/javascript">
	$(document).ready(function() {
		getKeyLogEvent();
	});
	<?php if($arrGetUserRights["adminArea"]){ ?>
	$('#buttonToggleDebugContent').append('<img src="layout/icons/iconGrid.png" class="buttonshowGrid" height="16" width="16" alt="Gitternetz" title="Gitternetz anzeigen" style="cursor:pointer;" />');
	$('.buttonshowGrid').live('click', function(){
		showCoordinatesGrid();
	});
	<?php } ?>
</script>
<?php } ?>

</body>
</html>