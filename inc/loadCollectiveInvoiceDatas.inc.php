<?php
	function loadCollectiveInvoiceDatas($loadCardTemplate, $selectCardIDs, $selectCardDetails) {
		$loadCardTemplate = removeUnnecessaryChars($loadCardTemplate);

		$getBasketItemsTemplate = preg_match_all("/<!-- ###CARD_ITEMS_START### -->(.*)<!-- ###CARD_ITEMS_END### -->/ismU", $loadCardTemplate, $arrFound);
		$getBasketItemsTemplate = $arrFound[1][0];

		$getBasketItemContent = array();

		if(!empty($selectCardIDs)) {
			$countItem = 0;
			$totalPrice = 0;

			foreach($selectCardIDs as $thisKey => $thisValue) {
				$thisQuantity = $selectCardDetails[$thisValue]["ordersArtikelMenge"];

				if($thisQuantity > 0 ){
					// BOF PRODUCTS PRICE
					if($countItem%2 == 0) { $thisRowColor = "#EEEEEE"; }
					else { $thisRowColor = "#FFFFFF"; }
					$thisRowColor = "#FFFFFF";

					$createBasketItemContent = $getBasketItemsTemplate;
					if(mb_detect_encoding($selectCardDetails[$thisValue]["ordersArtikelBezeichnung"]) == "UTF-8") {
						$selectCardDetails[$thisValue]["ordersArtikelBezeichnung"] = '' . (utf8_decode($selectCardDetails[$thisValue]["ordersArtikelBezeichnung"]));
					}
					else {
						$selectCardDetails[$thisValue]["ordersArtikelBezeichnung"] = '' . (utf8_encode($selectCardDetails[$thisValue]["ordersArtikelBezeichnung"]));
					}
					$selectCardDetails[$thisValue]["ordersArtikelBezeichnung"] = htmlentities($selectCardDetails[$thisValue]["ordersArtikelBezeichnung"]);

					$thisDescription = $selectCardDetails[$thisValue]["ordersArtikelBezeichnung"] ;

					$thisProductNumber = $selectCardDetails[$thisValue]["ordersArtikelNummer"];

					$thisSinglePrice = convertDecimal($selectCardDetails[$thisValue]["ordersSinglePreis"], 'store');
					$thisTotalPrice = $thisQuantity * $thisSinglePrice;
					$thisPos = ($countItem + 1);
					$thisProductNumber = $selectCardDetails[$thisValue]["ordersArtikelNummer"];
					$thisPosType = 'product'; // AB??
					$thisProductCategorieID = 'AB';
					$thisColorsText = '';

					$thisColorsCount = 0;
					$thisPrintText = '';

					$thisKommission = '';

					$thisNotiz = '';
					$thisPerDirectSaleValue = '0';

					if($selectCardDetails[$thisValue]["ordersID"] != ''){
						$thisCardItemID = $selectCardDetails[$thisValue]["ordersID"];
					}
					else {
						$thisCardItemID = $thisValue;
					}
					$thisWithBorder = "0";

					$thisOrderType = $arrOrderTypeDatas[$selectCardDetails[$thisValue]["ordersOrderType"]]["orderTypesName"];
					$thisOrderTypeValue = $selectCardDetails[$thisValue]["ordersOrderType"];

					$wordwrapAfterChars = 70;

					// $thisDescription = htmlentities(urldecode($thisDescription));
					$thisDescription = ($thisDescription);
					// $thisDescription = htmlentities(($thisDescription));
					$thisDescription = wordwrap($thisDescription, $wordwrapAfterChars, ' <br>');
					$thisColorsText = wordwrap($thisColorsText, $wordwrapAfterChars, ' <br>');
					$thisPrintText = wordwrap($thisPrintText, $wordwrapAfterChars, ' <br>');
					$thisKommission = wordwrap($thisKommission, $wordwrapAfterChars, ' <br>');
					$thisNotiz = wordwrap($thisNotiz, $wordwrapAfterChars, ' <br>');
					$thisPerDirectSaleText = ($thisPerDirectSaleText);

					$createBasketItemContent = preg_replace("/{###COUNT_ITEM###}/", $countItem, $createBasketItemContent);
					$createBasketItemContent = preg_replace("/{###CARD_ITEM_ID###}/", $thisCardItemID, $createBasketItemContent);
					$createBasketItemContent = preg_replace("/{###CARD_ITEM_PRODUCT_CATEGORIE_ID###}/", $thisProductCategorieID, $createBasketItemContent);
					#$createBasketItemContent = preg_replace("/{###CARD_ITEM_DESCRIPTION_VALUE###}/", ($thisDescription), $createBasketItemContent);
					$createBasketItemContent = preg_replace("/{###CARD_ITEM_DESCRIPTION_VALUE###}/", $thisDescription, $createBasketItemContent);
					$createBasketItemContent = preg_replace("/{###CARD_ITEM_NUMBER_VALUE###}/", $thisProductNumber, $createBasketItemContent);
					$createBasketItemContent = preg_replace("/{###CARD_ITEM_QUANTITY_VALUE###}/", $thisQuantity, $createBasketItemContent);
					$createBasketItemContent = preg_replace("/{###CARD_ITEM_COLORS_TEXT###}/", $thisColorsText, $createBasketItemContent);
					$createBasketItemContent = preg_replace("/{###CARD_ITEM_COLORS_COUNT###}/", $thisColorsCount, $createBasketItemContent);
					$createBasketItemContent = preg_replace("/{###CARD_ITEM_SINGLE_PRICE_VALUE###}/", number_format($thisSinglePrice, 2, ',', '.'), $createBasketItemContent);
					$createBasketItemContent = preg_replace("/{###CARD_ITEM_TOTAL_PRICE_VALUE###}/", number_format($thisTotalPrice, 2, ',', '.'), $createBasketItemContent);
					$createBasketItemContent = preg_replace("/{###CARD_POS_VALUE###}/", $thisPos, $createBasketItemContent);
					$createBasketItemContent = preg_replace("/{###CARD_ITEM_POS_TYPE###}/", $thisPosType, $createBasketItemContent);
					$createBasketItemContent = preg_replace("/{###ROW_COLOR###}/", $thisRowColor, $createBasketItemContent);
					$createBasketItemContent = preg_replace("/{###CARD_ITEM_PRINT_TEXT###}/", $thisPrintText, $createBasketItemContent);
					$createBasketItemContent = preg_replace("/{###CARD_ITEM_KOMMISSION###}/", $thisKommission, $createBasketItemContent);
					$createBasketItemContent = preg_replace("/{###CARD_ITEM_NOTIZ###}/", $thisNotiz, $createBasketItemContent);
					$createBasketItemContent = preg_replace("/{###CARD_ITEM_PER_DIRECT_SALE_TEXT###}/", $thisPerDirectSaleText, $createBasketItemContent);
					$createBasketItemContent = preg_replace("/{###CARD_ITEM_PER_DIRECT_SALE_VALUE###}/", $thisPerDirectSaleValue, $createBasketItemContent);
					$createBasketItemContent = preg_replace("/{###CARD_ITEM_WITH_BORDER###}/", $thisWithBorder, $createBasketItemContent);
					$createBasketItemContent = preg_replace("/{###CARD_ITEM_ORDER_TYPE###}/", $thisOrderTypeValue, $createBasketItemContent);
					$getBasketItemContent[] = $createBasketItemContent;
					$totalPrice = $totalPrice + $thisTotalPrice;

					$countItem++;
					// EOF PRODUCTS PRICE
				}
			}
			$loadCollectiveInvoiceContent = $loadCardTemplate;

			$loadCardItemContent = implode("", $getBasketItemContent);

			$loadCollectiveInvoiceContent = preg_replace("/<!-- ###CARD_ITEMS_START### -->(.*)<!-- ###CARD_ITEMS_END### -->/ismU", $loadCardItemContent, $loadCollectiveInvoiceContent);

			$loadCollectiveInvoiceContent = preg_replace("/{###CARD_TOTAL_POS_VALUE###}/", $countItem, $loadCollectiveInvoiceContent);

			$thisDiscount = convertDecimal($_POST["selectDiscount"], 'store');

			if($thisDiscount == 0) {
				$thisDiscountPercent = '';
				$thisDiscountValue = 0;
				$loadCollectiveInvoiceContent = preg_replace("/<!-- ###DISCOUNT_START### -->(.*)<!-- ###DISCOUNT_END### -->/", "", $loadCollectiveInvoiceContent);
			}
			else {
				if($_POST["selectDiscountType"] == 'fixed'){
					$thisDiscountPercent = '';
					#$thisDiscountPercent = 0;
					$thisDiscountValue = $thisDiscount;

				}
				else {
					#$thisDiscountPercent = number_format($_POST["selectDiscount"], 2, ',', '.') . '% ';
					$thisDiscountPercent = number_format($thisDiscount, 2, ',', '.') . '% ';
					$thisDiscountValue = $totalPrice * $thisDiscount / 100;
				}

				#$subtotalPrice = $totalPrice;
				#$loadCollectiveInvoiceContent = preg_replace("/{###CARD_DISCOUNT_VALUE###}/", number_format($thisDiscountValue, 2, ',', '.'), $loadCollectiveInvoiceContent);
				#$loadCollectiveInvoiceContent = preg_replace("/{###CARD_DISCOUNT_PERCENT###}/", $thisDiscountPercent, $loadCollectiveInvoiceContent);
				#$loadCollectiveInvoiceContent = preg_replace("/{###CARD_DISCOUNT_TYPE###}/", $_POST["selectDiscountType"], $loadCollectiveInvoiceContent);
				#$loadCollectiveInvoiceContent = preg_replace("/{###CARD_SUBTOTAL_PRICE_VALUE###}/", number_format($subtotalPrice, 2, ',', '.'), $loadCollectiveInvoiceContent);
			}

			$subtotalPrice = $totalPrice;
			$loadCollectiveInvoiceContent = preg_replace("/{###CARD_DISCOUNT_VALUE###}/", number_format($thisDiscountValue, 2, ',', '.'), $loadCollectiveInvoiceContent);
			$loadCollectiveInvoiceContent = preg_replace("/{###CARD_DISCOUNT_PERCENT###}/", $thisDiscountPercent, $loadCollectiveInvoiceContent);
			$loadCollectiveInvoiceContent = preg_replace("/{###CARD_DISCOUNT_TYPE###}/", $_POST["selectDiscountType"], $loadCollectiveInvoiceContent);
			$loadCollectiveInvoiceContent = preg_replace("/{###CARD_SUBTOTAL_PRICE_VALUE###}/", number_format($subtotalPrice, 2, ',', '.'), $loadCollectiveInvoiceContent);

			$totalPrice = $totalPrice - $thisDiscountValue;
			$loadCollectiveInvoiceContent = preg_replace("/{###CARD_TOTAL_PRICE_VALUE###}/", number_format($totalPrice, 2, ',', '.'), $loadCollectiveInvoiceContent);

			# $shippingCosts = $_POST["selectShippingCosts"];
			$shippingCosts = $_POST["selectShippingCosts"];
			if(trim($shippingCosts) == '') { $shippingCosts = 0;}
			$shippingCosts = convertDecimal($shippingCosts, 'store');
			$shippingCostsPerPackage =  number_format(0, 2, ',', '.');
			if($_POST["selectPackagingCostsMultiplier"] > 0 && $shippingCosts > 0){
				$shippingCostsPerPackage = '' . $_POST["selectPackagingCostsMultiplier"] . ' x ' . number_format($shippingCosts, 2, ',', '.') . '' ;
			}

			$shippingCosts = $shippingCosts * $_POST["selectPackagingCostsMultiplier"];
			$totalPrice = $totalPrice + $shippingCosts;

			$packagingCosts = $_POST["selectPackagingCosts"];
			if(trim($packagingCosts) == '') { $packagingCosts = 0;}
			$packagingCosts = convertDecimal($packagingCosts, 'store');
			$totalPrice = $totalPrice + $packagingCosts;

			$loadCollectiveInvoiceContent = preg_replace("/{###CARD_PACKAGING_COSTS_VALUE###}/", number_format($packagingCosts, 2, ',', '.'), $loadCollectiveInvoiceContent);
			$loadCollectiveInvoiceContent = preg_replace("/{###CARD_SHIPPING_COSTS_VALUE###}/", number_format($shippingCosts, 2, ',', '.'), $loadCollectiveInvoiceContent);
			$loadCollectiveInvoiceContent = preg_replace("/{###CARD_SHIPPING_COSTS_PER_PACKAGE###}/", $shippingCostsPerPackage, $loadCollectiveInvoiceContent);

			$cashOnDelivery = convertDecimal($_POST["selectCashOnDelivery"], 'store');
			if($_POST["selectPaymentCondition"] == 3){
				// $_POST["selectPaymentType"] == 3 ???
				$totalPrice = $totalPrice + $cashOnDelivery;
				$loadCollectiveInvoiceContent = preg_replace("/{###CARD_CASH_ON_DELIVERY_VALUE###}/", number_format($cashOnDelivery, 2, ',', '.'), $loadCollectiveInvoiceContent);
			}
			else {
				$loadCollectiveInvoiceContent = preg_replace("/<!-- ###CASH_ON_DELIVERY_START### -->(.*)<!-- ###CASH_ON_DELIVERY_END### -->/", "", $loadCollectiveInvoiceContent);
			}

			$mwst = convertDecimal($_POST["selectMwSt"], 'store');

			if($_POST["selectMwStType"] == 'inclusive'){
				$thisMwstTypeValue = 'inclusive';
				$thisMwstTypeText = 'inkl.';
				$thisMwst = $mwst * ($totalPrice * 1) / (100 + $mwst);
				$totalPrice = $totalPrice;
			}
			else {
				$thisMwstTypeValue = 'exclusive';
				$thisMwstTypeText = 'zzgl.';
				$thisMwst = ($totalPrice * $mwst) / 100;
				$totalPrice = $totalPrice + $thisMwst;
			}

			$loadCollectiveInvoiceContent = preg_replace("/{###MWST_TYPE_VALUE###}/", $thisMwstTypeValue, $loadCollectiveInvoiceContent);
			$loadCollectiveInvoiceContent = preg_replace("/{###MWST_TYPE_TEXT###}/", $thisMwstTypeText, $loadCollectiveInvoiceContent);
			$loadCollectiveInvoiceContent = preg_replace("/{###MWST###}/", number_format($mwst, 2, ',', '.'), $loadCollectiveInvoiceContent);
			$loadCollectiveInvoiceContent = preg_replace("/{###CARD_MWST_PRICE_VALUE###}/", number_format($thisMwst, 2, ',', '.'), $loadCollectiveInvoiceContent);

			// $totalPrice = number_format($totalPrice + (($totalPrice * MWST) / 100), 2);
			$loadCollectiveInvoiceContent = preg_replace("/{###CARD_TOTAL_COMPLETE_PRICE_VALUE###}/", number_format($totalPrice, 2, ',', '.'), $loadCollectiveInvoiceContent);

			if(($_POST["editDocType"] == 'MA' || $_POST["editDocType"] == 'M1' || $_POST["editDocType"] == 'M2' || $_POST["editDocType"] == 'M3' || $_POST["editDocType"] == 'IK') || ($_POST["convertDocType"] == 'MA' || $_POST["convertDocType"] == 'M1' || $_POST["convertDocType"] == 'M2' || $_POST["convertDocType"] == 'M3' || $_POST["convertDocType"] == 'IK')){
				if($_POST["selectPaymentPartValue"] > 0){
					$loadCollectiveInvoiceContent = preg_replace("/{###REMINDER_PAID_PART_VALUE###}/", number_format($_POST["selectPaymentPartValue"], 2, ',', '.'), $loadCollectiveInvoiceContent);
					$loadCollectiveInvoiceContent = preg_replace("/{###REMINDER_REST_PART_VALUE###}/", number_format($_POST["selectPaymentRestValue"], 2, ',', '.'), $loadCollectiveInvoiceContent);
					$totalPrice = $totalPrice - $_POST["selectPaymentPartValue"];
				}
				else {
					$loadCollectiveInvoiceContent = preg_replace("/<!-- ###REMINDER_PARTLY_PAID_START### -->(.*)<!-- ###REMINDER_PARTLY_PAID_END### -->/", "", $loadCollectiveInvoiceContent);
				}

				$thisReminderInterestPercent = convertDecimal($_POST["selectReminderInterestPercent"], 'store');
				$intervalDays = floor(((strtotime($_POST["selectDocumentDate"]) - strtotime($_POST["selectInvoiceDate"])) / (60 * 60 * 24)) - 1);
				$yearDays = 365;

				$thisReminderInterestPrice = ($thisReminderInterestPercent * $totalPrice / 100) * $intervalDays / $yearDays;
				$thisReminderChargesPrice = convertDecimal($_POST["selectReminderChargesPrice"], 'store');
				$thisTotalReminderValue = $totalPrice + $thisReminderInterestPrice + $thisReminderChargesPrice;

				$loadCollectiveInvoiceContent = preg_replace("/{###REMINDER_INTEREST_PERCENT###}/", number_format($thisReminderInterestPercent, 2, ',', '.'), $loadCollectiveInvoiceContent);
				$loadCollectiveInvoiceContent = preg_replace("/{###REMINDER_INTEREST_VALUE###}/", number_format($thisReminderInterestPrice, 2, ',', '.'), $loadCollectiveInvoiceContent);
				$loadCollectiveInvoiceContent = preg_replace("/{###REMINDER_CHARGES_VALUE###}/", number_format($thisReminderChargesPrice, 2, ',', '.'), $loadCollectiveInvoiceContent);
				$loadCollectiveInvoiceContent = preg_replace("/{###CARD_TOTAL_REMINDER_VALUE###}/", number_format($thisTotalReminderValue, 2, ',', '.'), $loadCollectiveInvoiceContent);

			}
			else {
				$loadCollectiveInvoiceContent = preg_replace("/<!-- ###REMINDER_CHARGES_START### -->(.*)<!-- ###REMINDER_CHARGES_END### -->/", "", $loadCollectiveInvoiceContent);
				$loadCollectiveInvoiceContent = preg_replace("/<!-- ###REMINDER_PARTLY_PAID_START### -->(.*)<!-- ###REMINDER_PARTLY_PAID_END### -->/", "", $loadCollectiveInvoiceContent);
			}
		}
		else {
			$loadCollectiveInvoiceContent = "";
		}

		$loadCollectiveInvoiceContent = iconv( "ISO-8859-1", "UTF-8//TRANSLIT", $loadCollectiveInvoiceContent );
		$loadCollectiveInvoiceContent = utf8_decode($loadCollectiveInvoiceContent	);
		$thisTotalSumPrice = $totalPrice;

		return $loadCollectiveInvoiceContent;
	}
?>