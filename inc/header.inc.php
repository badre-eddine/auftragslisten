<?php
	session_start();

	if(!defined("MANDATOR")) { DEFINE(MANDATOR, $_SESSION["mandator"]); }
	if($_POST["userSwitch"] != '') {
		DEFINE(USERID, $_POST["userSwitch"]);
		$_SESSION["usersID"] = $_POST["userSwitch"];
		header('location: ./');
	}
	if(!defined("USERID")) { DEFINE(USERID, $_SESSION["usersID"]); }

	$arrGetUserRights = readUserRights();
	$showWindowEvent = true;

	// BOF TRIM SEARCH VALUES
		if(!empty($_POST)){
			foreach($_POST as $thisKey => $thisValue){
				if(preg_match("/^search/", $thisKey)){
					$_POST[$thisKey] = trim($thisValue);
				}
			}
		}
		if(!empty($_REQUEST)){
			foreach($_REQUEST as $thisKey => $thisValue){
				if(preg_match("/^search/", $thisKey)){
					$_REQUEST[$thisKey] = trim($thisValue);
				}
			}
		}
	// EOF TRIM SEARCH VALUES
?>