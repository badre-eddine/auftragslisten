<?php
	function loadOrderConfirmationsForm($mode='add', $arrOrderConfirmationsSelectedDatas=array(), $useCheckbox=0, $classRow='row0', $count=0, $autoCheckFirst=0) {
		global $userDatas, $dbConnection, $arrGetUserRights, $customersData, $_POST, $_GET, $_REQUEST, $_SESSION;

		$thisCustomerID = $customersData["customersID"];
		$thisCustomerKNR = $_REQUEST["editID"];
		$thisOriginDocumentType = substr($_POST["originDocumentNumber"], 0, 2);

		// BOF READ COUNTRIES
			$arrCountryTypeDatas = getCountryTypes();
		// EOF READ COUNTRIES

		$addConfirmationsForm = '';
		#$addConfirmationsForm .= '<h3>Sammelrechnungen erstellen</h3>';

		$addConfirmationsForm .= '<div class="adminEditArea">';
		if($mode == 'add'){
			$addConfirmationsForm .= '<p class="infoArea">Bitte w&auml;hlen Sie die zu &uuml;bernehmenden Auftragsbest&auml;tigungen aus.</p>';
			$addConfirmationsForm .= '<p class="warningArea">Nicht mehr als 10 ABs nehmen!!! (<span id="displayCountSelectedItems">0</span> ausgew&auml;hlt)</p>';
		}
		else {
			$addConfirmationsForm .= '<p class="infoArea">Ausgew&auml;hlte Auftragsbest&auml;tigungen.</p>';
		}

		#$addConfirmationsForm .= '<fieldset>';
		if($mode == 'add'){
			$addConfirmationsForm .= '<legend>Auftragsbest&auml;tigungen f&uuml;r Sammelrechnung</legend>';
		}
		else {
			$addConfirmationsForm .= '<legend>Auftragsbest&auml;tigungen aus Sammelrechnung</legend>';
		}


		#echo 'mode: ' . $mode . '<br />';

		if($mode != ''){
			// BOF GET USED CONFIRMATIONS
			if($mode == 'select' || $mode == 'edit' || $mode == 'convert') {
				$arrUsedConfirmations = array();
				if($_POST["originDocumentNumber"] != ""){

					$_old_sql_getUsedConfirmations = "
							SELECT
									`" . TABLE_ORDER_INVOICES . "`.`orderDocumentsID`,
									`" . TABLE_ORDER_INVOICES . "`.`orderDocumentsNumber`,

									`" . TABLE_ORDER_INVOICES_DETAILS . "`.`orderDocumentDetailProductNumber`
								FROM `" . TABLE_ORDER_INVOICES . "`

								LEFT JOIN `" . TABLE_ORDER_INVOICES_DETAILS . "`
								ON(`" . TABLE_ORDER_INVOICES . "`.`orderDocumentsID` = `" . TABLE_ORDER_INVOICES_DETAILS . "`.`orderDocumentDetailDocumentID`)

								WHERE 1
									AND `" . TABLE_ORDER_INVOICES . "`.`orderDocumentsNumber` = '" . $_POST["originDocumentNumber"] . "'
						";


					$sql_getUsedConfirmations = "
							SELECT
									`" . constant('TABLE_ORDER_' . $thisOriginDocumentType) . "`.`orderDocumentsID`,
									`" . constant('TABLE_ORDER_' . $thisOriginDocumentType) . "`.`orderDocumentsNumber`,

									`" . constant('TABLE_ORDER_' . $thisOriginDocumentType . '_DETAILS') . "`.`orderDocumentDetailProductNumber`
								FROM `" . constant('TABLE_ORDER_' . $thisOriginDocumentType) . "`

								LEFT JOIN `" . constant('TABLE_ORDER_' . $thisOriginDocumentType . '_DETAILS') . "`
								ON(`" . constant('TABLE_ORDER_' . $thisOriginDocumentType) . "`.`orderDocumentsID` = `" . constant('TABLE_ORDER_' . $thisOriginDocumentType . '_DETAILS') . "`.`orderDocumentDetailDocumentID`)

								WHERE 1
									AND `" . constant('TABLE_ORDER_' . $thisOriginDocumentType) . "`.`orderDocumentsNumber` = '" . $_POST["originDocumentNumber"] . "'
						";



					$rs_getUsedConfirmations = $dbConnection->db_query($sql_getUsedConfirmations);
					while($ds_getUsedConfirmations = mysqli_fetch_assoc($rs_getUsedConfirmations)){
						$arrUsedConfirmations[] = $ds_getUsedConfirmations["orderDocumentDetailProductNumber"];
					}
				}
			}

			if($_COOKIE["isAdmin"]){
				# echo $sql_getUsedConfirmations;
			}
			$where_usedConfirmations = "";
			if(!empty($arrUsedConfirmations)){
				$where_usedConfirmations = "
					OR (
							`" . TABLE_ORDER_CONFIRMATIONS . "`.`orderDocumentsNumber` = '" . implode("' OR `" . TABLE_ORDER_CONFIRMATIONS . "`.`orderDocumentsNumber` = '", $arrUsedConfirmations) . "'
						)
					";
			}

			// EOF GET USED CONFIRMATIONS


			// BOF GET ALL ABs WITH NO INVOICE OR USED CONFIRMATIONS
			$sql = "
				SELECT
					`" . TABLE_ORDER_CONFIRMATIONS . "`.`orderDocumentsID`,
					`" . TABLE_ORDER_CONFIRMATIONS . "`.`orderDocumentsNumber`,
					`" . TABLE_ORDER_CONFIRMATIONS . "`.`orderDocumentsType`,
					`" . TABLE_ORDER_CONFIRMATIONS . "`.`orderDocumentsOrdersIDs`,
					`" . TABLE_ORDER_CONFIRMATIONS . "`.`orderDocumentsCustomerNumber`,
					`" . TABLE_ORDER_CONFIRMATIONS . "`.`orderDocumentsCustomersOrderNumber`,
					`" . TABLE_ORDER_CONFIRMATIONS . "`.`orderDocumentsProcessingDate`,
					`" . TABLE_ORDER_CONFIRMATIONS . "`.`orderDocumentsDocumentDate`,
					`" . TABLE_ORDER_CONFIRMATIONS . "`.`orderDocumentsOrderDate`,
					`" . TABLE_ORDER_CONFIRMATIONS . "`.`orderDocumentsInvoiceDate`,
					`" . TABLE_ORDER_CONFIRMATIONS . "`.`orderDocumentsCreditDate`,
					`" . TABLE_ORDER_CONFIRMATIONS . "`.`orderDocumentsDeliveryDate`,
					`" . TABLE_ORDER_CONFIRMATIONS . "`.`orderDocumentsBindingDate`,
					`" . TABLE_ORDER_CONFIRMATIONS . "`.`orderDocumentsDebitingDate`,
					`" . TABLE_ORDER_CONFIRMATIONS . "`.`orderDocumentsSalesman`,
					`" . TABLE_ORDER_CONFIRMATIONS . "`.`orderDocumentsKommission`,
					`" . TABLE_ORDER_CONFIRMATIONS . "`.`orderDocumentsAddressCompanyCustomerNumber`,
					`" . TABLE_ORDER_CONFIRMATIONS . "`.`orderDocumentsAddressCompany`,
					`" . TABLE_ORDER_CONFIRMATIONS . "`.`orderDocumentsAddressName`,
					`" . TABLE_ORDER_CONFIRMATIONS . "`.`orderDocumentsAddressStreet`,
					`" . TABLE_ORDER_CONFIRMATIONS . "`.`orderDocumentsAddressStreetNumber`,
					`" . TABLE_ORDER_CONFIRMATIONS . "`.`orderDocumentsAddressZipcode`,
					`" . TABLE_ORDER_CONFIRMATIONS . "`.`orderDocumentsAddressCity`,
					`" . TABLE_ORDER_CONFIRMATIONS . "`.`orderDocumentsAddressCountry`,
					`" . TABLE_ORDER_CONFIRMATIONS . "`.`orderDocumentsAddressMail`,
					`" . TABLE_ORDER_CONFIRMATIONS . "`.`orderDocumentsSumPrice`,
					`" . TABLE_ORDER_CONFIRMATIONS . "`.`orderDocumentsDiscount`,
					`" . TABLE_ORDER_CONFIRMATIONS . "`.`orderDocumentsDiscountType`,
					`" . TABLE_ORDER_CONFIRMATIONS . "`.`orderDocumentsDiscountPercent`,
					`" . TABLE_ORDER_CONFIRMATIONS . "`.`orderDocumentsMwst`,
					`" . TABLE_ORDER_CONFIRMATIONS . "`.`orderDocumentsMwstPrice`,
					`" . TABLE_ORDER_CONFIRMATIONS . "`.`orderDocumentsMwstType`,
					`" . TABLE_ORDER_CONFIRMATIONS . "`.`orderDocumentsShippingCosts`,
					`" . TABLE_ORDER_CONFIRMATIONS . "`.`orderDocumentsShippingCostsPackages`,
					`" . TABLE_ORDER_CONFIRMATIONS . "`.`orderDocumentsShippingCostsPerPackage`,
					`" . TABLE_ORDER_CONFIRMATIONS . "`.`orderDocumentsPackagingCosts`,
					`" . TABLE_ORDER_CONFIRMATIONS . "`.`orderDocumentsCashOnDelivery`,
					`" . TABLE_ORDER_CONFIRMATIONS . "`.`orderDocumentsTotalPrice`,
					`" . TABLE_ORDER_CONFIRMATIONS . "`.`orderDocumentsBankAccount`,
					`" . TABLE_ORDER_CONFIRMATIONS . "`.`orderDocumentsPaymentCondition`,
					`" . TABLE_ORDER_CONFIRMATIONS . "`.`orderDocumentsPaymentType`,
					`" . TABLE_ORDER_CONFIRMATIONS . "`.`orderDocumentsSkonto`,
					`" . TABLE_ORDER_CONFIRMATIONS . "`.`orderDocumentsDeliveryType`,
					`" . TABLE_ORDER_CONFIRMATIONS . "`.`orderDocumentsDeliveryTypeNumber`,
					`" . TABLE_ORDER_CONFIRMATIONS . "`.`orderDocumentsSubject`,
					`" . TABLE_ORDER_CONFIRMATIONS . "`.`orderDocumentsContent`,
					`" . TABLE_ORDER_CONFIRMATIONS . "`.`orderDocumentsDocumentPath`,
					`" . TABLE_ORDER_CONFIRMATIONS . "`.`orderDocumentsTimestamp`,
					`" . TABLE_ORDER_CONFIRMATIONS . "`.`orderDocumentsSendMail`,
					`" . TABLE_ORDER_CONFIRMATIONS . "`.`orderDocumentsInterestPercent`,
					`" . TABLE_ORDER_CONFIRMATIONS . "`.`orderDocumentsInterestPrice`,
					`" . TABLE_ORDER_CONFIRMATIONS . "`.`orderDocumentsChargesPrice`,
					`" . TABLE_ORDER_CONFIRMATIONS . "`.`orderDocumentsDeadlineDate`,
					`" . TABLE_ORDER_CONFIRMATIONS . "`.`orderDocumentsCustomerGroupID`,

					GROUP_CONCAT(`" . TABLE_ORDER_CONFIRMATIONS_DETAILS . "`.`orderDocumentDetailKommission`) AS `orderDocumentDetailKommission`,

					`" . TABLE_ORDER_CONFIRMATIONS . "`.`orderDocumentsStatus`,
					`" . TABLE_PAYMENT_STATUS_TYPES . "`.`paymentStatusTypesName`,

					`" . TABLE_RELATED_DOCUMENTS . "`.`relatedDocuments_AB`,
					`" . TABLE_RELATED_DOCUMENTS . "`.`relatedDocuments_RE`,

					`customers`.`customersKundennummer`,
					`customers`.`customersFirmenname`,
					`customers`.`customersCompanyStrasse`,
					`customers`.`customersCompanyHausnummer`,
					`customers`.`customersCompanyPLZ`,
					`customers`.`customersCompanyOrt`,
					`customers`.`customersCompanyCountry`,

					`customers`.`customersLieferadresseFirmenname`,
					`customers`.`customersLieferadresseFirmennameZusatz`,
					`customers`.`customersLieferadresseStrasse`,
					`customers`.`customersLieferadresseHausnummer`,
					`customers`.`customersLieferadressePLZ`,
					`customers`.`customersLieferadresseOrt`,
					`customers`.`customersLieferadresseLand`,

					`customers`.`customersRechnungsadresseFirmenname`,
					`customers`.`customersRechnungsadresseFirmennameZusatz`,
					`customers`.`customersRechnungsadresseStrasse`,
					`customers`.`customersRechnungsadresseHausnummer`,
					`customers`.`customersRechnungsadressePLZ`,
					`customers`.`customersRechnungsadresseOrt`,
					`customers`.`customersRechnungsadresseLand`,

					`customers`.`customersUseSalesmanDeliveryAdress`,
					`customers`.`customersUseSalesmanInvoiceAdress`,

					`salesmen`.`customersKundennummer` AS `salesmenKundennummer`,
					`salesmen`.`customersFirmenname` AS `salesmenFirmenname`,
					`salesmen`.`customersCompanyStrasse` AS `salesmenCompanyStrasse`,
					`salesmen`.`customersCompanyHausnummer` AS `salesmenCompanyHausnummer`,
					`salesmen`.`customersCompanyPLZ` AS `salesmenCompanyPLZ`,
					`salesmen`.`customersCompanyOrt` AS `salesmenCompanyOrt`,
					`salesmen`.`customersCompanyCountry` AS `salesmenCompanyCountry`

					FROM `" . TABLE_ORDER_CONFIRMATIONS . "`

					LEFT JOIN `" . TABLE_ORDER_CONFIRMATIONS_DETAILS . "`
					ON(`" . TABLE_ORDER_CONFIRMATIONS . "`.`orderDocumentsID` = `" . TABLE_ORDER_CONFIRMATIONS_DETAILS . "`.`orderDocumentDetailDocumentID`)

					LEFT JOIN `" . TABLE_RELATED_DOCUMENTS . "`
					ON(`" . TABLE_ORDER_CONFIRMATIONS . "`.`orderDocumentsNumber` = `" . TABLE_RELATED_DOCUMENTS . "`.`relatedDocuments_AB`)

					LEFT JOIN `" . TABLE_PAYMENT_STATUS_TYPES . "`
					ON(`" . TABLE_ORDER_CONFIRMATIONS . "`.`orderDocumentsStatus` = `" . TABLE_PAYMENT_STATUS_TYPES . "`.`paymentStatusTypesID`)

					LEFT JOIN `" . TABLE_CUSTOMERS . "` AS `customers`
					ON(`" . TABLE_ORDER_CONFIRMATIONS . "`.`orderDocumentsCustomerNumber` = `customers`.`customersKundennummer`)

					LEFT JOIN `" . TABLE_CUSTOMERS . "` AS `salesmen`
					ON(`" . TABLE_ORDER_CONFIRMATIONS . "`.`orderDocumentsSalesman` = `salesmen`.`customersID`)

					WHERE 1
						AND (
							 (
								(
									`" . TABLE_ORDER_CONFIRMATIONS . "`.`orderDocumentsCustomerNumber` = '" . $thisCustomerKNR . "'
									OR
									`" . TABLE_ORDER_CONFIRMATIONS . "`.`orderDocumentsSalesman` = " . $thisCustomerID . "
								)
								AND `" . TABLE_ORDER_CONFIRMATIONS . "`.`orderDocumentsStatus` != 3
								AND (
									`" . TABLE_RELATED_DOCUMENTS . "`.`relatedDocuments_RE` IS NULL
									OR
									`" . TABLE_RELATED_DOCUMENTS . "`.`relatedDocuments_RE` = ''
								)
							)
						)
						" . $where_usedConfirmations . "

					GROUP BY `" . TABLE_ORDER_CONFIRMATIONS . "`.`orderDocumentsNumber`
					ORDER BY `" . TABLE_RELATED_DOCUMENTS . "`.`relatedDocuments_AB`
			";

			#echo '<pre>';
			#print($sql);
			#echo '</pre>';
			// BOF GET ALL ABs WITH NO INVOICE OR USED CONFIRMATIONS
		}


		$rs = $dbConnection->db_query($sql);

		#$addConfirmationsForm .= '<div style="cursor:pointer;margin: 0 0 6px 0; font-size:10px;color:#0000FF;font-weight:bold;">';
		#$addConfirmationsForm .= '<span class="buttonCheckAll">Alle markieren</span>';
		#$addConfirmationsForm .= ' | ';
		#$addConfirmationsForm .= '<span class="buttonUncheckAll">Alle unmarkieren</span>';
		#$addConfirmationsForm .= '</div>';

		$addConfirmationsForm .= '<table border="1" cellpadding="0" cellspacing="0" width="100%" class="displayOrders">';

		$addConfirmationsForm .= '<tr>';
			$addConfirmationsForm .= '<th style="width:15px;">#</th>';
			$addConfirmationsForm .= '<th style="width:15px;"><img src="layout/icons/iconUse.png" width="16" height="16" alt="Auswahl" /></th>';
			$addConfirmationsForm .= '<th style="width:100px;">AB-Nummer</th>';
			$addConfirmationsForm .= '<th style="width:90px;">Datum</th>';
			$addConfirmationsForm .= '<th style="width:200px;">AB-Empf&auml;nger</th>';
			$addConfirmationsForm .= '<th style="width:200px;">RE-Empf&auml;nger</th>';
			$addConfirmationsForm .= '<th style="width:200px;">Kunde</th>';
			$addConfirmationsForm .= '<th style="width:90px;">Summe (netto)</th>';
			$addConfirmationsForm .= '<th style="width:90px;">Summe (brutto)</th>';
			$addConfirmationsForm .= '<th style="width:90px;">Status</th>';

			$addConfirmationsForm .= '<th style="width:50px;">Info</th>';
		$addConfirmationsForm .= '</tr>';

		$countRow = 0;
		while($ds = mysqli_fetch_assoc($rs )){
			#echo '<pre>';
			#print_r($ds);
			#echo '</pre>';
			$displayRow = false;

			if($mode == 'add'){
				if($ds["orderDocumentsAddressCompanyCustomerNumber"] == $thisCustomerKNR){
					$displayRow = true;
				}
			}
			else {
				if(in_array($ds["orderDocumentsNumber"], $arrUsedConfirmations)){
					$displayRow = true;
				}
			}

			if($displayRow){
				#if($countRow ==  0){ dd('ds'); }

				if($countRow%2 == 0){ $rowClass = 'row1'; }
				else { $rowClass = 'row4'; }

				$addConfirmationsForm .= '<tr class="' . $rowClass . '">';

				$addConfirmationsForm .= '<td style="text-align:right;">';
				$addConfirmationsForm .= '<b>' . ($countRow + 1) . '.</b>';
				$addConfirmationsForm .= '</td>';

				$addConfirmationsForm .= '<td style="text-align:center;">';
				if($mode == 'add'){
					$checked = '';
					if(in_array($ds["orderDocumentsNumber"], $arrOrderConfirmationsSelectedDatas)){
						$checked = ' checked="checked" ';
					}
					$addConfirmationsForm .= '<input type="checkbox" name="selectConfirmationRow[' . $countRow . ']" class="selectConfirmation" value="1" '. $checked . ' title="' . $ds["orderDocumentsNumber"] . ' zur Sammelrechnung hinzuf&uuml;gen?" />';
				}
				else {
					$addConfirmationsForm .= 'x<input type="hidden" name="selectConfirmationRow[' . $countRow . ']" class="selectConfirmation" value="1" />';
				}
				$addConfirmationsForm .= '</td>';

				$addConfirmationsForm .= '<td>';
				$addConfirmationsForm .= '<b>' . $ds["orderDocumentsNumber"] . '</b>';
				$addConfirmationsForm .= '</td>';

				$addConfirmationsForm .= '<td>';
				$addConfirmationsForm .= formatDate($ds["orderDocumentsDocumentDate"], "display");
				$addConfirmationsForm .= '</td>';

				$addConfirmationsForm .= '<td style="white-space:nowrap;">';
					$addConfirmationsForm .= '<span style="line-height:12px;">';
					$addConfirmationsForm .= '<a href="' . PAGE_EDIT_CUSTOMER . '?editCustomerNumber=' . $ds["orderDocumentsAddressCompanyCustomerNumber"] . '" target="_blank">';
					$addConfirmationsForm .= $ds["orderDocumentsAddressCompanyCustomerNumber"];
					$addConfirmationsForm .= '</a>';
					$addConfirmationsForm .= '</span>';
					$addConfirmationsForm .= '<br />';
					$addConfirmationsForm .= '<span style="font-size:10px;line-height:11px;">';
					$addConfirmationsForm .= $ds["orderDocumentsAddressCompany"];
					$addConfirmationsForm .= '<br />';
					$addConfirmationsForm .= $ds["orderDocumentsAddressStreet"] . ' ' . $ds["orderDocumentsAddressStreetNumber"];
					$addConfirmationsForm .= '<br />';
					$addConfirmationsForm .= $ds["orderDocumentsAddressZipcode"] . ' ' . $ds["orderDocumentsAddressCity"];
					if($ds["orderDocumentsAddressCountry"] != 81) {
						$addConfirmationsForm .= '<br />';
						#$addConfirmationsForm .= $arrCountryTypeDatas[$ds["orderDocumentsAddressCountry"]]["countries_name_DE"];
						$addConfirmationsForm .= $arrCountryTypeDatas[$ds["orderDocumentsAddressCountry"]]["countries_iso_code_3"];
					}
					/*
					$addConfirmationsForm .= '<hr />';
					$addConfirmationsForm .= $ds["salesmenKundennummer"];
					$addConfirmationsForm .= '<br />';
					$addConfirmationsForm .= $ds["salesmenFirmenname"];
					$addConfirmationsForm .= '<br />';
					$addConfirmationsForm .= $ds["salesmenCompanyStrasse"] . ' ' . $ds["salesmenCompanyHausnummer"];
					$addConfirmationsForm .= '<br />';
					$addConfirmationsForm .= $ds["salesmenCompanyPLZ"] . ' ' . $ds["salesmenCompanyOrt"];
					if($ds["salesmenCompanyCountry"] != 81) {
						#$addConfirmationsForm .= '<br />';
						##$addConfirmationsForm .= $arrCountryTypeDatas[$ds["salesmenCompanyCountry"]]["countries_name_DE"];
						#$addConfirmationsForm .= $arrCountryTypeDatas[$ds["salesmenCompanyCountry"]]["countries_iso_code_3"];
					}
					*/
					$addConfirmationsForm .= '</span>';
				$addConfirmationsForm .= '</td>';

				$addConfirmationsForm .= '<td style="font-size:10px;font-weight:bold;">';
				if($ds["customersUseSalesmanInvoiceAdress"] == '1'){
					$addConfirmationsForm .= '<span><img src="layout/icons/iconOk.png" width="16" height="16" alt="ok" title="ok"> VERTRETER</span>';
				}
				else {
					$addConfirmationsForm .= '<span><img src="layout/icons/iconNotOk.png" width="16" height="16" alt="ok" title="ok"> KUNDE</span>';
				}

				$addConfirmationsForm .= '</td>';

				$addConfirmationsForm .= '<td style="white-space:nowrap;">';

					#$addConfirmationsForm .= $ds["orderDocumentsCustomerNumber"];
					$addConfirmationsForm .= '<span style="line-height:12px;">';
					$addConfirmationsForm .= '<a href="' . PAGE_EDIT_CUSTOMER . '?editCustomerNumber=' . $ds["customersKundennummer"] . '" target="_blank">';
					$addConfirmationsForm .= $ds["customersKundennummer"];
					$addConfirmationsForm .= '</a>';
					$addConfirmationsForm .= '</span>';

					$addConfirmationsForm .= '<br />';

					$addConfirmationsForm .= '<span style="font-size:10px;line-height:11px;">';
					$addConfirmationsForm .= $ds["customersFirmenname"];

					if(
						$ds["orderDocumentsAddressCompany"] != $ds["customersFirmenname"]
						&& $ds["orderDocumentsAddressStreet"] != $ds["customersCompanyStrasse"]
						&& $ds["orderDocumentsAddressStreetNumber"] != $ds["customersCompanyHausnummer"]
						&& $ds["orderDocumentsAddressZipcode"] != $ds["customersCompanyPLZ"]
					){
						$addConfirmationsForm .= '<br />';
						$addConfirmationsForm .= $ds["customersCompanyStrasse"] . ' ' . $ds["customersCompanyHausnummer"];
						$addConfirmationsForm .= '<br />';
						$addConfirmationsForm .= $ds["customersCompanyPLZ"] . ' ' . $ds["customersCompanyOrt"];
						if($ds["customersCompanyCountry"] != 81) {
							$addConfirmationsForm .= '<br />';
							#$addConfirmationsForm .= $arrCountryTypeDatas[$ds["customersCompanyCountry"]]["countries_name_DE"];
							$addConfirmationsForm .= $arrCountryTypeDatas[$ds["customersCompanyCountry"]]["countries_iso_code_3"];
						}
					}
					else if($ds["orderDocumentDetailKommission"] != ""){
						$addConfirmationsForm .= '<br />';
						$addConfirmationsForm .= '';
						$addConfirmationsForm .= $ds["orderDocumentDetailKommission"];
					}

					$addConfirmationsForm .= '</span>';

					$thisCustomerData = 'KNR ' . $ds["customersKundennummer"] . ' - ' . $ds["customersFirmenname"];

					if(
						$ds["orderDocumentsAddressCompany"] != $ds["customersFirmenname"]
						&& $ds["orderDocumentsAddressStreet"] != $ds["customersCompanyStrasse"]
						&& $ds["orderDocumentsAddressStreetNumber"] != $ds["customersCompanyHausnummer"]
						&& $ds["orderDocumentsAddressZipcode"] != $ds["customersCompanyPLZ"]
					){
						$thisCustomerData .= ', ' . $ds["customersCompanyPLZ"] . ' ' . $ds["customersCompanyOrt"];
					}

					else if($ds["orderDocumentDetailKommission"] != ""){
						$thisCustomerData .= ' - '. trim($ds["orderDocumentDetailKommission"]);
					}


				$addConfirmationsForm .= '</td>';

				$addConfirmationsForm .= '<td style="text-align:right;background-color:#FEFFAF;">';
				$addConfirmationsForm .= '<b>';
				$thisOrderDocumentsTotalPriceNetto = $ds["orderDocumentsTotalPrice"] - $ds["orderDocumentsMwstPrice"];
				$addConfirmationsForm .= number_format($thisOrderDocumentsTotalPriceNetto, 2, ",", ".");
				$addConfirmationsForm .= ' &euro; </b>';
				$addConfirmationsForm .= '</td>';

				$addConfirmationsForm .= '<td style="text-align:right;">';
				$addConfirmationsForm .= number_format($ds["orderDocumentsTotalPrice"], 2, ",", ".");
				$addConfirmationsForm .= '</td>';

				$addConfirmationsForm .= '<td>';
				$addConfirmationsForm .= $ds["paymentStatusTypesName"] . ' (' . $ds["orderDocumentsStatus"] . ')';

				$addConfirmationsForm .= '<input type="hidden" name="selectConfirmationDocumentNumber[' . $countRow . ']" value="' . $ds["orderDocumentsNumber"] . '" />';
				$addConfirmationsForm .= '<input type="hidden" name="selectConfirmationQuantity[' . $countRow . ']" value="1" />';
				$addConfirmationsForm .= '<input type="hidden" name="selectConfirmationTotalSumNetto[' . $countRow . ']" value="' . $thisOrderDocumentsTotalPriceNetto . '" />';
				$addConfirmationsForm .= '<input type="hidden" name="selectConfirmationTotalSumBrutto[' . $countRow . ']" value="' . $ds["orderDocumentsTotalPrice"] . '" />';
				$addConfirmationsForm .= '<input type="hidden" name="selectConfirmationCustomer[' . $countRow . ']" value="' . $thisCustomerData . '" />';

				$addConfirmationsForm .= '</td>';

				$addConfirmationsForm .= '<td>';
				$addConfirmationsForm .= '<span class="toolItem">';
				#$addConfirmationsForm .= '<a href="' . $_SERVER["PHP_SELF"] . '?downloadFile=' . urlencode(basename($ds["orderDocumentsDocumentPath"])) . '&documentType=' . $ds["orderDocumentsType"] . '">' . '<img src="layout/icons/icon' . getFileType(basename($ds["orderDocumentsDocumentPath"])) . '.gif" width="16" height="16" title="Dokument herunterladen" alt="Download" /></a>';
				$addConfirmationsForm .= '<a href="' . $ds["orderDocumentsDocumentPath"] . '" targe="_blank">' . '<img src="layout/icons/icon' . getFileType(basename($ds["orderDocumentsDocumentPath"])) . '.gif" width="16" height="16" title="Dokument herunterladen" alt="Download" /></a>';
				$addConfirmationsForm .= '</span>';
				$addConfirmationsForm .= '</td>';

				$addConfirmationsForm .= '</tr>';
				$countRow++;
			}
		}

		$addConfirmationsForm .= '</table>';

		#$addConfirmationsForm .= '</fieldset>';

		$addConfirmationsForm .= '</div>';

		$addConfirmationsForm .= '
			<script language="javascript" type="text/javascript">

				function countSelectedItems(){
					var countSelectedItems = ($(".selectConfirmation:checked").length);
					$("#displayCountSelectedItems").html(countSelectedItems);
				}

				var maxSelectedItems = 10;
				$(document).ready(function() {
					$(".selectConfirmation").live("click", function(){
						var thisSelectedConfirmationValue = $(this).attr("checked");
						if(thisSelectedConfirmationValue == "checked"){
							$(this).parent().parent().find("td").css("background-color", "#A0FF9F");
						}
						else {
							$(this).parent().parent().find("td").css("background-color", "");
						}
						if($(".selectConfirmation:checked").length > maxSelectedItems){
							window.alert("Bitte nicht mehr als " + maxSelectedItems + "ABs nehmen!!");
						}
						countSelectedItems();
					});

					$(".buttonCheckAll").live("click", function() {
						$(".selectConfirmation").attr("checked", true);
						$(".selectConfirmation").parent().parent().find("td").css("background-color", "#A0FF9F");
						countSelectedItems();
					});

					$(".buttonUncheckAll").live("click", function() {
						$(".selectConfirmation").attr("checked", false);
						$(".selectConfirmation").parent().parent().find("td").css("background-color", "");
						countSelectedItems();
					});
					$(".selectConfirmation:checked").parent().parent().find("td").css("background-color", "#A0FF9F");

				});
			</script>
		';

		// EOF GET ALL ABs WITH NO INVOICE
		#################################

		$addConfirmationsForm = removeUnnecessaryChars($addConfirmationsForm);
		return $addConfirmationsForm;
	}
?>