<?php
	session_start();
	// if(($_SESSION["usersID"] == "" && $_COOKIE["usersID"] == "") || $_COOKIE["mandator"] == "") {
	if(($_SESSION["usersID"] == '' || $_SESSION["mandator"] == '')) {
		session_unset();
		header('location: ./');
		exit;
	}
	

	require_once('config/configMandator.inc.php');
	require_once('config/configParams_' . strtolower(MANDATOR) . '.inc.php');
	require_once('config/configBasic.inc.php');
	require_once('config/configFiles.inc.php');

	require_once('config/configTables.inc.php');
	require_once('inc/functions.inc.php');
	require_once('inc/header.inc.php');

	if(!empty($_POST)) {
		foreach($_POST as $thisKey => $thisValue){
			if(!is_array($thisValue)){
				$_POST[$thisKey] = trim($thisValue);
			}
		}
	}

	// BOF CHECK WEB CONNECTION
	$isWebConnection = true;
	if($arrGetUserRights["adminArea"]){
		#$isWebConnection = checkWebConnection();
	}
	// EOF CHECK WEB CONNECTION

	// BOF TEST EXTERNAL DB_CONNECTION
	$isExternalDBConnection = true;
	if($arrGetUserRights["adminArea"]){
		if($isWebConnection){
			#$isExternalDBConnection = checkExternalDbConnection();
		}
	}
	// BOF TEST EXTERNAL DB_CONNECTION

?>