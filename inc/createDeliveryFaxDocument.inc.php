<?php
	$createPdfContentFax = "";
	$loadContentTemplate = implode('', file(DIRECTORY_PDF_TEMPLATES . '/template_BASIC.html'));

	$loadContentTemplate = preg_replace("/{###DOCUMENT_DATAS###}/", $arrDocumentDatas["VB"], $loadContentTemplate);
	$loadContentTemplate = preg_replace("/({###ADRESS_COMPANY###})/", "<b>FAX-NR: {###ADRESS_FAX###}</b><hr>$1", $loadContentTemplate);

	$createPdfContentFax = $loadContentTemplate;
	$createPdfContentFax = preg_replace("/{###CARD_DATAS###}/", $loadCardDatas, $createPdfContentFax);
	$createPdfContentFax = preg_replace("/<!-- BOF PRICE_DATAS -->(.*)<!-- EOF PRICE_DATAS -->/ismU", "", $createPdfContentFax);

	// STYLES
	$loadTemplateStyles = implode('', file(DIRECTORY_PDF_TEMPLATES . 'template_STYLES.html'));
	$createPdfContentFax = preg_replace("/{###STYLES###}/", $loadTemplateStyles, $createPdfContentFax);

	$loadSubject = 'Versandinfo &uuml;ber Ihre Bestellung Nr.: ' . '{###DOCUMENT_NUMBER_AB###}' . '<br>';
	$createPdfContentFax = preg_replace("/{###SUBJECT###}/", $loadSubject, $createPdfContentFax);

	if(!empty($loadedAdressDatas)) {
		foreach($loadedAdressDatas as $thisKey => $thisValue) {
			if($thisValue != "") {
				$createPdfContentFax = preg_replace("/{###".$thisKey."###}/", $thisValue . "<br />", $createPdfContentFax);
			}
			else {
				$createPdfContentFax = preg_replace("/{###".$thisKey."###}/", "", $createPdfContentFax);
			}
		}
	}

	if(!empty($loadedDocumentDatas)) {
		$createPdfContentFax = preg_replace("/{###BANK_ACCOUNT_WARNING###}/", "", $createPdfContentFax);

		foreach($arrContentDatas["VB"] as $thisKey => $thisValue) {
			$createPdfContentFax = preg_replace("/{###".$thisKey."###}/", $thisValue, $createPdfContentFax);
			if(trim($thisValue) == '') {
				$createPdfContentFax = preg_replace("/<!-- BOF_".$thisKey." -->(.*)<!-- EOF_".$thisKey." -->/", '', $createPdfContentFax);
			}
		}

		foreach($loadedDocumentDatas as $thisKey => $thisValue) {
			$createPdfContentFax = preg_replace("/{###".$thisKey."###}/", $thisValue, $createPdfContentFax);
			if(trim($thisValue) == '') {
				$createPdfContentFax = preg_replace("/<!-- BOF_".$thisKey." -->(.*)<!-- EOF_".$thisKey." -->/", '', $createPdfContentFax);
			}
		}
	}

	$createPdfContentFax = preg_replace("/{###CARD_TOTAL_COMPLETE_PRICE_VALUE###}/", number_format($thisTotalSumPrice, 2, ",", ""), $createPdfContentFax);
	$createPdfContentFax = removeUnnecessaryChars($createPdfContentFax);

	$displayPdfContentFax = $createPdfContentFax;

	if($_POST["editDocType"] != "") {
		$displayPdfContentFax = preg_replace("/{###DOCUMENT_NUMBER###}/", $_POST["originDocumentNumber"], $displayPdfContentFax);
	}
	else {
		$displayPdfContentFax = preg_replace("/{###DOCUMENT_NUMBER###}/", constant("BASIC_NUMBER_" . $_REQUEST["documentType"]), $displayPdfContentFax);
	}
	$displayPdfContentFax = preg_replace("/{###DOCUMENT_NUMBER###}/", constant("BASIC_NUMBER_" . $_REQUEST["documentType"]), $displayPdfContentFax);
	$displayPdfContentFax = $createPdfContentFax;

	$displayPdfContentFax = preg_replace('/<input (.*) \/>/ismU', '', $displayPdfContentFax);
	$createPdfContentFax = preg_replace('/<input (.*) \/>/ismU', '', $createPdfContentFax);

	// BOF INCLUDE PDF SEPARATOR
	if($userDatas["usersLogin"] == "thorsten"){
		$createPdfContentFax = '<!-- BOF DOCUMENT_FAX -->' . $createPdfContentFax . '<!-- EOF DOCUMENT_FAX -->';
	}
	// EOF INCLUDE PDF SEPARATOR

	#echo '<div id="displayCreateDocument" style="padding:10px; background-color:#FFF;">';
	# echo htmlentities($displayPdfContentFax);
	#echo $displayPdfContentFax;
	#echo '</div>';
	#$pdfCreateContent .= $displayPdfContentFax;
	$pdfCreateContent .= $createPdfContentFax;

	#dd($pdfCreateContent);

	#$pdfTestTempName = 'testVB.pdf.html';
	#$pdfTestTempFile = fopen($pdfTestTempName, 'w');
	#fwrite($pdfTestTempFile, $pdfCreateContent);
	#fclose($pdfTestTempFile);

	#require_once(DIRECTORY_HTML2PDF . 'html2pdf.class.php');

	#$testhtml2pdf = new HTML2PDF('P', 'A4', 'de', true, 'UTF-8', array(22, 5, 5, 5));

	#ob_start();
	#echo $pdfCreateContentTest;
	#$contentPDFTest = ob_get_clean();

	#$testhtml2pdf->writeHTML($contentPDFTest, isset($_GET['vuehtml']));
	#$testhtml2pdf->Output("testVB.pdf", 'F', '');


	#if(file_exists($pdfTestTempName)){
		#unlink($pdfTestTempName);
	#}
?>