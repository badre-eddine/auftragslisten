<?php

	$sql_getRelatedDocumentsData = '';

	$arrGetRelatedDocumentsTypes = array('AB', 'MA', 'M1', 'M2', 'M3');
	$arrSql_getRelatedDocumentsData = array();
	$arrSql_getRelatedDocumentsData['FIELDS'] = "
			`{###THIS_TABLE_ORDER###}`.`orderDocumentsID` AS `{###THIS_TABLE_DOCUMENT_TYPE###}_DocumentsID`,
			`{###THIS_TABLE_ORDER###}`.`orderDocumentsNumber` AS `{###THIS_TABLE_DOCUMENT_TYPE###}_DocumentsNumber`,
			`{###THIS_TABLE_ORDER###}`.`orderDocumentsType` AS `{###THIS_TABLE_DOCUMENT_TYPE###}_DocumentsType`,

			`{###THIS_TABLE_ORDER###}`.`orderDocumentsCustomerNumber` AS `{###THIS_TABLE_DOCUMENT_TYPE###}_DocumentsCustomerNumber`,

			`{###THIS_TABLE_ORDER###}`.`orderDocumentsProcessingDate` AS `{###THIS_TABLE_DOCUMENT_TYPE###}_DocumentsProcessingDate`,
			`{###THIS_TABLE_ORDER###}`.`orderDocumentsDocumentDate` AS `{###THIS_TABLE_DOCUMENT_TYPE###}_DocumentsDocumentDate`,
			`{###THIS_TABLE_ORDER###}`.`orderDocumentsOrderDate` AS `{###THIS_TABLE_DOCUMENT_TYPE###}_DocumentsOrderDate`,
			`{###THIS_TABLE_ORDER###}`.`orderDocumentsInvoiceDate` AS `{###THIS_TABLE_DOCUMENT_TYPE###}_DocumentsInvoiceDate`,
			`{###THIS_TABLE_ORDER###}`.`orderDocumentsCreditDate` AS `{###THIS_TABLE_DOCUMENT_TYPE###}_DocumentsCreditDate`,
			`{###THIS_TABLE_ORDER###}`.`orderDocumentsDeliveryDate` AS `{###THIS_TABLE_DOCUMENT_TYPE###}_DocumentsDeliveryDate`,
			`{###THIS_TABLE_ORDER###}`.`orderDocumentsBindingDate` AS `{###THIS_TABLE_DOCUMENT_TYPE###}_DocumentsBindingDate`,
			`{###THIS_TABLE_ORDER###}`.`orderDocumentsDebitingDate` AS `{###THIS_TABLE_DOCUMENT_TYPE###}_DocumentsDebitingDate`,

			`{###THIS_TABLE_ORDER###}`.`orderDocumentsSumPrice` AS `{###THIS_TABLE_DOCUMENT_TYPE###}_DocumentsSumPrice`,
			`{###THIS_TABLE_ORDER###}`.`orderDocumentsDiscount` AS `{###THIS_TABLE_DOCUMENT_TYPE###}_DocumentsDiscount`,
			`{###THIS_TABLE_ORDER###}`.`orderDocumentsDiscountType` AS `{###THIS_TABLE_DOCUMENT_TYPE###}_DocumentsDiscountType`,
			`{###THIS_TABLE_ORDER###}`.`orderDocumentsDiscountPercent` AS `{###THIS_TABLE_DOCUMENT_TYPE###}_DocumentsDiscountPercent`,
			`{###THIS_TABLE_ORDER###}`.`orderDocumentsMwst` AS `{###THIS_TABLE_DOCUMENT_TYPE###}_DocumentsMwst`,
			`{###THIS_TABLE_ORDER###}`.`orderDocumentsMwstPrice` AS `{###THIS_TABLE_DOCUMENT_TYPE###}_DocumentsMwstPrice`,
			`{###THIS_TABLE_ORDER###}`.`orderDocumentsMwstType` AS `{###THIS_TABLE_DOCUMENT_TYPE###}_DocumentsMwstType`,
			`{###THIS_TABLE_ORDER###}`.`orderDocumentsShippingCosts` AS `{###THIS_TABLE_DOCUMENT_TYPE###}_DocumentsShippingCosts`,
			`{###THIS_TABLE_ORDER###}`.`orderDocumentsShippingCostsPackages` AS `{###THIS_TABLE_DOCUMENT_TYPE###}_DocumentsShippingCostsPackages`,
			`{###THIS_TABLE_ORDER###}`.`orderDocumentsShippingCostsPerPackage` AS `{###THIS_TABLE_DOCUMENT_TYPE###}_DocumentsShippingCostsPerPackage`,
			`{###THIS_TABLE_ORDER###}`.`orderDocumentsPackagingCosts` AS `{###THIS_TABLE_DOCUMENT_TYPE###}_DocumentsPackagingCosts`,
			`{###THIS_TABLE_ORDER###}`.`orderDocumentsCashOnDelivery` AS `{###THIS_TABLE_DOCUMENT_TYPE###}_DocumentsCashOnDelivery`,
			`{###THIS_TABLE_ORDER###}`.`orderDocumentsTotalPrice` AS `{###THIS_TABLE_DOCUMENT_TYPE###}_DocumentsTotalPrice`,
			`{###THIS_TABLE_ORDER###}`.`orderDocumentsBankAccount` AS `{###THIS_TABLE_DOCUMENT_TYPE###}_DocumentsBankAccount`,
			`{###THIS_TABLE_ORDER###}`.`orderDocumentsPaymentCondition` AS `{###THIS_TABLE_DOCUMENT_TYPE###}_DocumentsPaymentCondition`,
			`{###THIS_TABLE_ORDER###}`.`orderDocumentsPaymentType` AS `{###THIS_TABLE_DOCUMENT_TYPE###}_DocumentsPaymentType`,
			`{###THIS_TABLE_ORDER###}`.`orderDocumentsSkonto` AS `{###THIS_TABLE_DOCUMENT_TYPE###}_DocumentsSkonto`,

			`{###THIS_TABLE_ORDER###}`.`orderDocumentsDocumentPath` AS `{###THIS_TABLE_DOCUMENT_TYPE###}_DocumentsDocumentPath`,

			`{###THIS_TABLE_ORDER###}`.`orderDocumentsStatus` AS `{###THIS_TABLE_DOCUMENT_TYPE###}_DocumentsStatus`,

			`{###THIS_TABLE_ORDER###}`.`orderDocumentsInterestPercent` AS `{###THIS_TABLE_DOCUMENT_TYPE###}_DocumentsInterestPercent`,
			`{###THIS_TABLE_ORDER###}`.`orderDocumentsInterestPrice` AS `{###THIS_TABLE_DOCUMENT_TYPE###}_DocumentsInterestPrice`,
			`{###THIS_TABLE_ORDER###}`.`orderDocumentsChargesPrice` AS `{###THIS_TABLE_DOCUMENT_TYPE###}_DocumentsChargesPrice`,
			`{###THIS_TABLE_ORDER###}`.`orderDocumentsDeadlineDate` AS `{###THIS_TABLE_DOCUMENT_TYPE###}_DocumentsDeadlineDate`,
			`{###THIS_TABLE_ORDER###}`.`orderDocumentsCustomerGroupID` AS `{###THIS_TABLE_DOCUMENT_TYPE###}_DocumentsCustomerGroupID`,
			`{###THIS_TABLE_ORDER###}`.`orderDocumentsIsCollectiveInvoice` AS `{###THIS_TABLE_DOCUMENT_TYPE###}_DocumentsIsCollectiveInvoice`
		";
	$arrSql_getRelatedDocumentsData['JOINS'] = "
			LEFT JOIN `{###THIS_TABLE_ORDER###}`
			ON(`" . TABLE_RELATED_DOCUMENTS . "`.`relatedDocuments_{###THIS_TABLE_DOCUMENT_TYPE###}` = `{###THIS_TABLE_ORDER###}`.`orderDocumentsNumber`)
		";

	$sql_getRelatedDocumentsData = "
			SELECT
				`" . TABLE_RELATED_DOCUMENTS . "`.`relatedDocuments_AN`,
				`" . TABLE_RELATED_DOCUMENTS . "`.`relatedDocuments_AB`,
				`" . TABLE_RELATED_DOCUMENTS . "`.`relatedDocuments_RE`,
				`" . TABLE_RELATED_DOCUMENTS . "`.`relatedDocuments_LS`,
				`" . TABLE_RELATED_DOCUMENTS . "`.`relatedDocuments_GU`,
				`" . TABLE_RELATED_DOCUMENTS . "`.`relatedDocuments_MA`,
				`" . TABLE_RELATED_DOCUMENTS . "`.`relatedDocuments_M1`,
				`" . TABLE_RELATED_DOCUMENTS . "`.`relatedDocuments_M2`,
				`" . TABLE_RELATED_DOCUMENTS . "`.`relatedDocuments_M3`,
				`" . TABLE_RELATED_DOCUMENTS . "`.`relatedDocuments_IK`,
				`" . TABLE_RELATED_DOCUMENTS . "`.`relatedDocuments_collectiveABs`
		";

	if(!empty($arrGetRelatedDocumentsTypes)){
		foreach($arrGetRelatedDocumentsTypes as $thisGetRelatedDocumentsType){
			$sql_getRelatedDocumentsData .= ", ";
			$thisDocumentTable = constant('TABLE_ORDER_' . $thisGetRelatedDocumentsType);
			$sql_getRelatedDocumentsData .= str_replace("{###THIS_TABLE_DOCUMENT_TYPE###}", $thisGetRelatedDocumentsType, str_replace("{###THIS_TABLE_ORDER###}", $thisDocumentTable, $arrSql_getRelatedDocumentsData['FIELDS']));
		}
	}

	$sql_getRelatedDocumentsData .= "

			FROM `" . TABLE_ORDER_RE . "`

			LEFT JOIN `bctr_relatedDocuments`
			ON(`" . TABLE_ORDER_RE . "`.`orderDocumentsNumber` = `" . TABLE_RELATED_DOCUMENTS . "`.`relatedDocuments_RE`)
		";

	if(!empty($arrGetRelatedDocumentsTypes)){
		foreach($arrGetRelatedDocumentsTypes as $thisGetRelatedDocumentsType){
			$thisDocumentTable = constant('TABLE_ORDER_' . $thisGetRelatedDocumentsType);
			$sql_getRelatedDocumentsData .= str_replace("{###THIS_TABLE_DOCUMENT_TYPE###}", $thisGetRelatedDocumentsType, str_replace("{###THIS_TABLE_ORDER###}", $thisDocumentTable, $arrSql_getRelatedDocumentsData['JOINS']));
		}
	}

	$sql_getRelatedDocumentsData .= "
			WHERE 1
				AND `relatedDocuments_RE` IS NOT NULL
				AND `relatedDocuments_RE` != ''

			LIMIT 10

		";
		echo '<pre>';
		echo $sql_getRelatedDocumentsData;
		echo '</pre>';
?>