 <?php
	// $nowDate = date("d.m.Y");
	$nowDate = date("d.m.Y, H:i:s");

	$arrAttachmentFiles = array (
		// "general-terms-and-conditions_".$_SESSION["lang"].".pdf" => str_replace(" ", "_", myUnHtmlEntities($main_32c)).".pdf",

		// "general-terms-and-conditions_de.pdf" => str_replace(" ", "_", myUnHtmlEntities("Allgemeine Gesch&auml;ftsbedingungen und Lieferbedingungen")).".pdf"

		DIRECTORY_CREATED_DOCUMENTS_CUSTOMERS . $thisCreatedPdfName => str_replace(" ", "_", myUnHtmlEntities($thisCreatedPdfName))
		// DIRECTORY_ATTACHMENT_DOCUMENTS
	);
	$arrAttachmentFiles = array_unique($arrAttachmentFiles);

	// BOF CREATE HTML MAILS

		// BOF CREATE HTML MAIL TO CUSTOMER
			$mailToClient_HTML = $mailTemplate["html"];

			$mailToClient_HTML = preg_replace('/{###MAIL_HEADER_IMAGE###}/',$arrContentDatas[$_REQUEST["documentType"]]["MAIL_HEADER_IMAGE"] , $mailToClient_HTML);

			$mailToClient_HTML = preg_replace('/{###MAIL_TITLE###}/',$arrContentDatas[$_REQUEST["documentType"]]["MAIL_TITLE"], $mailToClient_HTML);
			$mailToClient_HTML = preg_replace('/{###MAIL_DATE_VALUE###}/', $nowDate, $mailToClient_HTML);

			$mailToClient_HTML = preg_replace('/{###MAIL_SALUTATION###}/', $arrContentDatas[$_REQUEST["documentType"]]["MAIL_SALUTATION"], $mailToClient_HTML);
			$mailToClient_HTML = preg_replace('/{###MAIL_CONTENT###}/', $arrContentDatas[$_REQUEST["documentType"]]["MAIL_CONTENT"], $mailToClient_HTML);
			$mailToClient_HTML = preg_replace('/{###DOCUMENT_NUMBER###}/', $loadedDocumentDatas["DOCUMENT_NUMBER"], $mailToClient_HTML);
			$mailToClient_HTML = preg_replace('/{###MAIL_REGARDS###}/', $arrContentDatas[$_REQUEST["documentType"]]["MAIL_REGARDS"], $mailToClient_HTML);
			$mailToClient_HTML = preg_replace('/{###MAIL_SIGNATURE###}/', $arrContentDatas[$_REQUEST["documentType"]]["MAIL_SIGNATURE"], $mailToClient_HTML);
			$mailToClient_HTML = preg_replace('/{###MAIL_COPYRIGHT###}/', $arrContentDatas[$_REQUEST["documentType"]]["MAIL_COPYRIGHT"], $mailToClient_HTML);
		// EOF CREATE HTML MAIL TO CUSTOMER


		// BOF CREATE HTML MAIL TO BURHAN
			$mailToUs_HTML = $mailTemplate["html"];

			$mailToUs_HTML = preg_replace('/{###MAIL_HEADER_IMAGE###}/', $arrContentDatas[$_REQUEST["documentType"]]["MAIL_HEADER_IMAGE"], $mailToUs_HTML);

			$mailToUs_HTML = preg_replace('/{###MAIL_TITLE###}/', $arrContentDatas[$_REQUEST["documentType"]]["MAIL_TITLE"], $mailToUs_HTML);
			$mailToUs_HTML = preg_replace('/{###MAIL_DATE_VALUE###}/', $nowDate, $mailToUs_HTML);

			$mailToUs_HTML = preg_replace('/{###MAIL_SALUTATION###}/', $arrContentDatas[$_REQUEST["documentType"]]["MAIL_SALUTATION"], $mailToUs_HTML);
			$mailToUs_HTML = preg_replace('/{###MAIL_CONTENT###}/', $arrContentDatas[$_REQUEST["documentType"]]["MAIL_CONTENT"], $mailToUs_HTML);
			$mailToUs_HTML = preg_replace('/{###DOCUMENT_NUMBER###}/', $loadedDocumentDatas["DOCUMENT_NUMBER"], $mailToUs_HTML);
			$mailToUs_HTML = preg_replace('/{###MAIL_REGARDS###}/', $arrContentDatas[$_REQUEST["documentType"]]["MAIL_REGARDS"], $mailToUs_HTML);
			$mailToUs_HTML = preg_replace('/{###MAIL_SIGNATURE###}/', $arrContentDatas[$_REQUEST["documentType"]]["MAIL_SIGNATURE"], $mailToUs_HTML);
			$mailToUs_HTML = preg_replace('/{###MAIL_COPYRIGHT###}/', $arrContentDatas[$_REQUEST["documentType"]]["MAIL_COPYRIGHT"], $mailToUs_HTML);

			// $mailToUs_HTML = preg_replace('/{###FIRST_NAME_TITLE###}/', 'Vorname', $mailToUs_HTML);
			// $mailToUs_HTML = preg_replace('/{###FIRST_NAME_VALUE###}/', $Vorname, $mailToUs_HTML);
		// EOF CREATE HTML MAIL TO BURHAN

		// BOF REMOVE UNNECESSARY CHARS
			$mailToClient_HTML = removeUnnecessaryChars($mailToClient_HTML);
			$mailToUs_HTML = removeUnnecessaryChars($mailToUs_HTML);
		// EOF REMOVE UNNECESSARY CHARS

	// EOF CREATE HTML MAILS

	// BOF CREATE TEXT MAIL
		$mailToClient_TEXT = $mailToClient_HTML;
		$mailToClient_TEXT = COMPANY_NAME_LONG . "<hr />" . $mailToClient_TEXT;
		$mailToClient_TEXT = "" . convertHtmlToText($mailToClient_TEXT);

		$mailToUs_TEXT = $mailToUs_HTML;
		$mailToUs_TEXT = COMPANY_NAME_LONG . "<hr />" . $mailToUs_TEXT;
		$mailToUs_TEXT = "" . convertHtmlToText($mailToUs_TEXT);
	// EOF CREATE TEXT MAIL



	// BOF SEND BASKET PER MAIL:

		// BOF TO BURHAN
			// $headersToUs = '"' . MAIL_ADDRESS_FROM_NAME . '" <' . MAIL_ADDRESS_FROM . '>' . "\r\n" . 'Reply-To: ' . MAIL_ADDRESS_REPLY_TO . '' . "\r\n" . 'Errors-To: ' . MAIL_ERRORS_TO;
			// $headersToUs = 'From: ' . MAIL_ADDRESS_FROM . "\r\n" . 'Reply-To: ' . MAIL_ADDRESS_REPLY_TO . '' . "\r\n" . 'Errors-To: ' . MAIL_ERRORS_TO;
			$mailToUs_Subject = preg_replace('/{###DOCUMENT_NUMBER###}/', $generatedDocumentNumber, $_POST["selectSubject"]).' (KOPIE)';
			//$sendMailToUs = mail($orderMailDatas["SendTo"], $mail_16, $mail_text, $headersToUs);

			$mailToUs = new htmlMimeMail();
			$mailToUs->setTextCharset('utf-8');
			$mailToUs->setHtmlCharset('utf-8');
			$mailToUs->setTextEncoding('7bit');
			$mailToUs->setHtmlEncoding('quoted-printable');
			$mailToUs->setHeadCharset('utf-8');
			// $mailToUs->setFrom($headersToUs);
			if(MAIL_ADDRESS_BCC != '') { $mailToUs->setBcc(MAIL_ADDRESS_BCC); }
			$mailToUs->setHeader('From', '"' . MAIL_ADDRESS_FROM_NAME . '" <' . MAIL_ADDRESS_FROM . '>');
			$mailToUs->setHeader('Reply-To', MAIL_ADDRESS_REPLY_TO);
			$mailToUs->setHeader('Errors-To', MAIL_ERRORS_TO);
			$mailToUs->setHeader('Subject', $mailToUs_Subject);
			$mailToUs->setHeader('Date', date('D, d M y H:i:s O'));
			// $mailToUs->setHeader($name, $value);
			// $mailToUs->setSubject($mailToUs_Subject);

			#if(DEBUG) {
				#$tempDisplayMailToUsHTML = $mailToUs_HTML;
				#$tempDisplayMailToUsTEXT = $mailToUs_TEXT;
			#}

			$mailToUs->setHtml($mailToUs_HTML, $mailToUs_TEXT, DIRECTORY_MAIL_IMAGES);

			if(!empty($arrAttachmentFiles)) {
				foreach($arrAttachmentFiles as $thisFile => $thisFileName) {
					$mailToUs->addAttachment($mailToUs->getFile($thisFile), $thisFileName, 'application/pdf');
				}
			}

			$mailToUs_arrRecipients = explode(";", MAIL_ADDRESS_FROM); // HAS TO BE AN ARRAY
			#if(DEBUG) { $mailToUs_arrRecipients = array(MAIL_ADDRESS_BCC, DEBUG_MAIL_TO_ADDRESS); }

			if(MAIL_SEND_TYPE == 'mail') {
				$sendMailToUs = $mailToUs->send($mailToUs_arrRecipients, 'mail');
			}
			else if(MAIL_SEND_TYPE == 'smtp') {
				$mailToUs->setSMTPParams(MAIL_ACCOUNT_SMTP, MAIL_ACCOUNT_PORT, MAIL_ACCOUNT_HELO, MAIL_ACCOUNT_AUTH, MAIL_ACCOUNT_USER, MAIL_ACCOUNT_PASSWORD);
				// function setSMTPParams($host = null, $port = null, $helo = null, $auth = null, $user = null, $pass = null)
				$sendMailToUs = $mailToUs->send($mailToUs_arrRecipients, 'smtp');
				if(!$sendMailToUs) {
					/*
					echo '<pre>';
					print_r($mailToUs->errors);
					echo '</pre>';

					echo '<pre>';
					print_r($mailToUs_arrRecipients);
					echo '</pre>';
					*/
				}
			}
			else {
				$sendMailToUs = false;
			}
		// EOF TO BURHAN


		// BOF TO CUSTOMER
			$mailToClient_recipient = $_POST["selectCustomersRecipientMail"];
			$checkCustomerEmail = checkEmailStructure($mailToClient_recipient);

			if($checkCustomerEmail) {
				//$headersToClient = "FROM:"'.$orderMailDatas["SendName"].'" <'.$orderMailDatas["SendTo"].'>';
				$headersToClient = '"'.$orderMailDatas["SendName"].'" <'.$orderMailDatas["SendTo"].'>' . "\r\n" . 'Reply-To: '.$orderMailDatas["SendTo"].'' . "\r\n" .'Errors-To: '.$orderMailDatas["SendTo"];
				if(DEBUG) { $headersToClient = '"'.$debugOrderMailDatas["SendName"].'" <'.$debugOrderMailDatas["SendTo"].'>' . "\r\n" . 'Reply-To: '.$debugOrderMailDatas["SendTo"].'' . "\r\n" .'Errors-To: '.$debugOrderMailDatas["SendTo"]; }

				$mailToClient_Subject = preg_replace('/{###DOCUMENT_NUMBER###}/', $generatedDocumentNumber, $_POST["selectSubject"]).'';

				$mailToClient = new htmlMimeMail();
				$mailToClient->setTextCharset('utf-8');
				$mailToClient->setHtmlCharset("utf-8");
				$mailToClient->setTextEncoding('7bit');
				$mailToClient->setHtmlEncoding('quoted-printable');
				$mailToClient->setHeadCharset('utf-8');
				//$mailToClient->setFrom($headersToClient);
				$mailToClient->setHeader('From', '"' . MAIL_ADDRESS_FROM_NAME . '" <' . MAIL_ADDRESS_FROM . '>');
				$mailToClient->setHeader('Reply-To', MAIL_ADDRESS_REPLY_TO);
				$mailToClient->setHeader('Errors-To', MAIL_ERRORS_TO);
				$mailToClient->setHeader('Subject', $mailToClient_Subject);
				$mailToClient->setHeader('Date', date('D, d M y H:i:s O'));
				// $mailToClient->setHeader($name, $value);
				// $mailToClient->setSubject($mailToClient_Subject);

				if(DEBUG) {
					$tempDisplayMailToClientHTML = $mailToClient_HTML;
					$tempDisplayMailToClientTEXT = $mailToClient_TEXT;
				}

				$mailToClient->setHtml($mailToClient_HTML, $mailToClient_TEXT, DIRECTORY_MAIL_IMAGES);

				if(!empty($arrAttachmentFiles)){
					foreach($arrAttachmentFiles as $thisFile => $thisFileName) {
						$mailToClient->addAttachment($mailToClient->getFile($thisFile), $thisFileName, 'application/pdf');
					}
				}
				if(file_exists(DIRECTORY_ATTACHMENT_DOCUMENTS.'contact.vcf')) {
					$mailToClient->addAttachment($mailToClient->getFile(DIRECTORY_ATTACHMENT_DOCUMENTS.'contact.vcf'), 'Kontakt_vCard.vcf', 'application/text');
				}
				clearstatcache();

				$mailToClient_arrRecipients = array($mailToClient_recipient); // HAS TO BE AN ARRAY
				# if(DEBUG) { $mailToUs_arrRecipients = array(MAIL_ADDRESS_BCC, DEBUG_MAIL_TO_ADDRESS); }

				if(MAIL_SEND_TYPE == 'mail') {
					$sendMailToClient = $mailToClient->send($mailToClient_arrRecipients, 'mail');
				}
				else if(MAIL_SEND_TYPE == 'smtp') {
					$mailToClient->setSMTPParams(MAIL_ACCOUNT_SMTP, MAIL_ACCOUNT_PORT, MAIL_ACCOUNT_HELO, MAIL_ACCOUNT_AUTH, MAIL_ACCOUNT_USER, MAIL_ACCOUNT_PASSWORD);
					$sendMailToClient = $mailToClient->send($mailToClient_arrRecipients, 'smtp');
					if(!$sendMailToClient) {
						/*
						echo '<pre>';
						print_r($mailToClient->errors);
						echo '</pre>';

						echo '<pre>';
						print_r($mailToClient);
						echo '</pre>';
						*/
					}
				}
			}
			else
			{
				$sendMailToClient = false;
			}
		// EOF TO CUSTOMER
?>