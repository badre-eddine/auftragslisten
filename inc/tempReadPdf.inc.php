<?php

echo '<h2>PDF lesen</h2>';

require_once(PATH_PDF2TEXT);

// BOF DEFINE ARRAY PATH
$arrProvisionPdfPaths = array (
	"documents_bctr/documentsCreated/salesmen/gerhard_fueller.pdf",
	"documents_bctr/documentsCreated/salesmen/kai_friedrichsen.pdf",
	"documents_bctr/documentsCreated/salesmen/document.pdf",
	"documents_bctr/documentsCreated/salesmen/Provision_2013-03-22_autonorm_antz_gmbh_29012.pdf",
	"documents_bctr/documentsCreated/salesmen/Provision_2013-03-25_autonorm_antz_gmbh_29012.pdf",
	"documents_bctr/documentsCreated/salesmen/Provision_2013-03-25_ali_ekber_erkut_13074.pdf",
	"documents_bctr/documentsCreated/salesmen/Provision_2013-03-26_autonorm_antz_gmbh_29012.pdf",
	"documents_bctr/documentsCreated/salesmen/gerhard_fueller_handelsvertretung.pdf",
	"documents_bctr/documentsCreated/salesmen/Provision_2013-03-28_kersting_gmbh_59027.pdf",
	"documents_bctr/documentsCreated/salesmen/ali_ekber_erkut.pdf",
	"documents_bctr/documentsCreated/salesmen/Provision_2013-04-04_ali_ekber_erkut_13074.pdf",
	"documents_bctr/documentsCreated/salesmen/Provision_2013-03-22_autonorm_antz_gmbh_29012.pdf",
	"documents_bctr/documentsCreated/salesmen/Provision_2013-04-04_ali_ekber_erkut_13074.pdf",
	"documents_bctr/documentsCreated/salesmen/Provision_2013-03-26_autonorm_antz_gmbh_29012.pdf",
	"documents_bctr/documentsCreated/salesmen/Provision_2013-03-25_autonorm_antz_gmbh_29012.pdf",
	"documents_bctr/documentsCreated/salesmen/Provision_2013-03-28_kersting_gmbh_59027.pdf",
	"documents_bctr/documentsCreated/salesmen/Provision_2013-03-25_ali_ekber_erkut_13074.pdf",
	"documents_bctr/documentsCreated/salesmen/Provision_2013-04-15_autonorm_antz_gmbh_29012.pdf",
	"documents_bctr/documentsCreated/salesmen/Provision_2013-04-18_autonorm_antz_gmbh_29012.pdf",
	"documents_bctr/documentsCreated/salesmen/Provision_2013-04-19_autonorm_antz_gmbh_29012.pdf",
	"documents_bctr/documentsCreated/salesmen/Provision_2013-04-19_autotechnik_domin_ug_26196.pdf",
	"documents_bctr/documentsCreated/salesmen/Provision_2013-04-19_carsten_herzberg_27073.pdf",
	"documents_bctr/documentsCreated/salesmen/Provision_2013-04-19_eduard_chojnacki_09011.pdf",
	"documents_bctr/documentsCreated/salesmen/Provision_2013-04-19_faseler_marketing_74002.pdf",
	"documents_bctr/documentsCreated/salesmen/Provision_2013-04-19_gerhard_fueller_handelsvertretung_72014.pdf",
	"documents_bctr/documentsCreated/salesmen/Provision_2013-04-19_jens_buhmeier_78039.pdf",
	"documents_bctr/documentsCreated/salesmen/Provision_2013-04-19_kai_friedrichsen_24196.pdf",
	"documents_bctr/documentsCreated/salesmen/Provision_2013-04-19_p_consulting_erika_peter_98033.pdf",
	"documents_bctr/documentsCreated/salesmen/Provision_2013-04-19_u._lorchheim,_handelsvertretung_26001.pdf",
	"documents_bctr/documentsCreated/salesmen/Provision_2013-04-19_wolfgang_goetz_65105.pdf",
	"documents_bctr/documentsCreated/salesmen/Provision_2013-04-22_autonorm_antz_gmbh_29012.pdf",
	"documents_bctr/documentsCreated/salesmen/Provision_2013-04-22_gerhard_fueller_handelsvertretung_72014.pdf",
	"documents_bctr/documentsCreated/salesmen/Provision_2013-04-22_kersting_gmbh_59027.pdf",
	"documents_bctr/documentsCreated/salesmen/Provision_2013-04-23_gerhard_fueller_handelsvertretung_72014.pdf",
	"documents_bctr/documentsCreated/salesmen/autonorm_antz_gmbh.pdf",
	"documents_bctr/documentsCreated/salesmen/Provision_2013-05-10_ali_ekber_erkut_13074.pdf",
	"documents_bctr/documentsCreated/salesmen/Provision_2013-05-10_autonorm_antz_gmbh_29012.pdf",
	"documents_bctr/documentsCreated/salesmen/Provision_2013-05-10_autotechnik_koehring_01073.pdf",
	"documents_bctr/documentsCreated/salesmen/Provision_2013-05-10_carsten_herzberg_handelsvertretung_27073.pdf",
	"documents_bctr/documentsCreated/salesmen/Provision_2013-05-10_faseler_marketing_74002.pdf",
	"documents_bctr/documentsCreated/salesmen/Provision_2013-05-10_gerhard_fueller_handelsvertretung_72014.pdf",
	"documents_bctr/documentsCreated/salesmen/Provision_2013-05-10_jens_buhmeier_78039.pdf",
	"documents_bctr/documentsCreated/salesmen/Provision_2013-05-10_kai_friedrichsen_24196.pdf",
	"documents_bctr/documentsCreated/salesmen/Provision_2013-05-10_norbert_federmann_78030.pdf",
	"documents_bctr/documentsCreated/salesmen/Provision_2013-05-10_u._lorchheim,_handelsvertretung_26001.pdf",
	"documents_bctr/documentsCreated/salesmen/Provision_2013-05-10_ulrich_merz_01055.pdf",
	"documents_bctr/documentsCreated/salesmen/Provision_2013-05-10_wolfgang_goetz_65105.pdf",
	"documents_bctr/documentsCreated/salesmen/Provision_2013-05-13_kersting_gmbh_59027.pdf",
	"documents_bctr/documentsCreated/salesmen/Provision_2013-05-14_kersting_gmbh_59027.pdf",
	"documents_bctr/documentsCreated/salesmen/Provision_2013-05-21_kersting_gmbh_59027.pdf",
	"documents_bctr/documentsCreated/salesmen/Interne-Gutschrift_2013-06-14_autonorm_antz_gmbh_29012.pdf",
	"documents_bctr/documentsCreated/salesmen/Interne-Gutschrift_2013-06-18_autonorm_antz_gmbh_29012.pdf",
	"documents_bctr/documentsCreated/salesmen/Interne-Gutschrift_2013-06-18_autohandel_mario_duesel_06024.pdf",
	"documents_bctr/documentsCreated/salesmen/Interne-Gutschrift_2013-06-18_autotechnik_domin_ug_26196.pdf",
	"documents_bctr/documentsCreated/salesmen/Interne-Gutschrift_2013-06-18_autotechnik_koehring_01073.pdf",
	"documents_bctr/documentsCreated/salesmen/Interne-Gutschrift_2013-06-18_faseler_marketing_74002.pdf",
	"documents_bctr/documentsCreated/salesmen/Interne-Gutschrift_2013-06-18_jens_buhmeier_78039.pdf",
	"documents_bctr/documentsCreated/salesmen/Interne-Gutschrift_2013-06-18_kai_friedrichsen_24196.pdf",
	"documents_bctr/documentsCreated/salesmen/Interne-Gutschrift_2013-06-18_u._lorchheim,_handelsvertretung_26001.pdf",
	"documents_bctr/documentsCreated/salesmen/Interne-Gutschrift_2013-06-18_norbert_federmann_78030.pdf",
	"documents_bctr/documentsCreated/salesmen/Interne-Gutschrift_2013-06-18_wolfgang_goetz_65105.pdf",
	"documents_bctr/documentsCreated/salesmen/Interne-Gutschrift_2013-06-18_gerhard_fueller_handelsvertretung_72014.pdf",
	"documents_bctr/documentsCreated/salesmen/_Interne-Gutschrift_2013-06-19_kersting_gmbh_59027.pdf",
	"documents_bctr/documentsCreated/salesmen/Interne-Gutschrift_2013-06-19_gerhard_fueller_handelsvertretung_72014.pdf",
	"documents_bctr/documentsCreated/salesmen/Interne-Gutschrift_2013-06-19_kersting_gmbh_59027.pdf",
	"documents_bctr/documentsCreated/salesmen/Interne-Gutschrift_2013-07-12_autonorm_antz_gmbh_29012.pdf",
	"documents_bctr/documentsCreated/salesmen/Interne-Gutschrift_2013-07-15_autonorm_antz_gmbh_29012.pdf",
	"documents_bctr/documentsCreated/salesmen/Interne-Gutschrift_2013-07-15_jens_buhmeier_78039.pdf",
	"documents_bctr/documentsCreated/salesmen/Interne-Gutschrift_2013-07-16_kersting_gmbh_59027.pdf",
	"documents_bctr/documentsCreated/salesmen/Interne-Gutschrift_2013-07-18_autonorm_antz_gmbh_29012.pdf",
	"documents_bctr/documentsCreated/salesmen/Interne-Gutschrift_2013-07-18_autotechnik_domin_ug_26196.pdf",
	"documents_bctr/documentsCreated/salesmen/Interne-Gutschrift_2013-07-18_autotechnik_koehring_01073.pdf",
	"documents_bctr/documentsCreated/salesmen/Interne-Gutschrift_2013-07-18_faseler_marketing_74002.pdf",
	"documents_bctr/documentsCreated/salesmen/Interne-Gutschrift_2013-07-18_jens_buhmeier_78039.pdf",
	"documents_bctr/documentsCreated/salesmen/Interne-Gutschrift_2013-07-18_mecon_dieter_ebner_67228.pdf",
	"documents_bctr/documentsCreated/salesmen/Interne-Gutschrift_2013-07-18_u._lorchheim,_handelsvertretung_26001.pdf",
	"documents_bctr/documentsCreated/salesmen/Interne-Gutschrift_2013-07-18_wolfgang_goetz_65105.pdf",
	"documents_bctr/documentsCreated/salesmen/Interne-Gutschrift_2013-07-19_kersting_gmbh_59027.pdf",
	"documents_bctr/documentsCreated/salesmen/Interne-Gutschrift_2013-08-23_carsten_herzberg_handelsvertretung_27073.pdf",
	"documents_bctr/documentsCreated/salesmen/Interne-Gutschrift_2013-08-24_autonorm_antz_gmbh_29012.pdf",
	"documents_bctr/documentsCreated/salesmen/Interne-Gutschrift_2013-08-24_autotechnik_domin_ug_26196.pdf",
	"documents_bctr/documentsCreated/salesmen/Interne-Gutschrift_2013-08-24_autotechnik_koehring_01073.pdf",
	"documents_bctr/documentsCreated/salesmen/Interne-Gutschrift_2013-08-24_faseler_marketing_74002.pdf",
	"documents_bctr/documentsCreated/salesmen/Interne-Gutschrift_2013-08-24_jens_buhmeier_78039.pdf",
	"documents_bctr/documentsCreated/salesmen/Interne-Gutschrift_2013-08-24_kai_friedrichsen_24196.pdf",
	"documents_bctr/documentsCreated/salesmen/Interne-Gutschrift_2013-08-24_norbert_federmann_78030.pdf",
	"documents_bctr/documentsCreated/salesmen/Interne-Gutschrift_2013-08-24_p_consulting_erika_peter_98033.pdf",
	"documents_bctr/documentsCreated/salesmen/Interne-Gutschrift_2013-08-24_u._lorchheim,_handelsvertretung_26001.pdf",
	"documents_bctr/documentsCreated/salesmen/Interne-Gutschrift_2013-08-24_ulrich_merz_01055.pdf",
	"documents_bctr/documentsCreated/salesmen/Interne-Gutschrift_2013-08-24_kersting_gmbh_59027.pdf",
	"documents_bctr/documentsCreated/salesmen/Interne-Gutschrift_2013-09-04_autonorm_antz_gmbh_29012.pdf",
	"documents_bctr/documentsCreated/salesmen/Interne-Gutschrift_2013-09-16_autotechnik_domin_ug_26196.pdf",
	"documents_bctr/documentsCreated/salesmen/Interne-Gutschrift_2013-09-20_gerhard_fueller_handelsvertretung_72014.pdf",
	"documents_bctr/documentsCreated/salesmen/Interne-Gutschrift_2013-09-24_autohandel_mario_duesel_06024.pdf",
	"documents_bctr/documentsCreated/salesmen/Interne-Gutschrift_2013-09-24_autonorm_antz_gmbh_29012.pdf",
	"documents_bctr/documentsCreated/salesmen/Interne-Gutschrift_2013-09-24_autotechnik_domin_ug_26196.pdf",
	"documents_bctr/documentsCreated/salesmen/Interne-Gutschrift_2013-09-24_carsten_herzberg_handelsvertretung_27073.pdf",
	"documents_bctr/documentsCreated/salesmen/Interne-Gutschrift_2013-09-24_kersting_gmbh_59027.pdf",
	"documents_bctr/documentsCreated/salesmen/Interne-Gutschrift_2013-09-25_faseler_marketing_74002.pdf",
	"documents_bctr/documentsCreated/salesmen/Interne-Gutschrift_2013-09-25_jens_buhmeier_78039.pdf",
	"documents_bctr/documentsCreated/salesmen/Interne-Gutschrift_2013-09-25_norbert_federmann_78030.pdf",
	"documents_bctr/documentsCreated/salesmen/Interne-Gutschrift_2013-09-25_u._lorchheim,_handelsvertretung_26001.pdf",
	"documents_bctr/documentsCreated/salesmen/Interne-Gutschrift_2013-09-27_kersting_gmbh_59027.pdf"
);
// EOF DEFINE ARRAY PATH

#dd('arrProvisionPdfPaths');


if(!empty($arrProvisionPdfPaths)){
	$arrDocumentDatas = array();
	foreach($arrProvisionPdfPaths as $thisPdfPathKey => $thisPdfPathValue){

		$resultText = pdf2text($thisPdfPathValue);

		$resultText = utf8_encode($resultText);

		$resultText = preg_replace("/EUR/", "EUR<br />", ($resultText));
		$resultText = preg_replace("/\\\r/", " ", ($resultText));
		$resultText = preg_replace("/\\\n/", " ", ($resultText));

		$resultSearch = preg_match("/Datum:.*([0-9]{2}\.[0-9]{2}\.[0-9]{4})/", $resultText, $arrFoundDocumentDate);
		$resultSearch = preg_match("/Kundennummer:.*([0-9]{5})/ismU", $resultText, $arrFoundDocumentSalesmanNumber);
		$resultSearch = preg_match("/Provisionsnummer:.*(PR-.*) /ismU", $resultText, $arrFoundDocumentDocumentNumber);

		$resultSearch = preg_match("/Zwischensumme:(.*)(EUR)/ismU", $resultText, $arrFoundDocumentSum);
		$resultSearch = preg_match("/Gesamt:(.*) EUR/ismU", $resultText, $arrFoundDocumentTotalSum);
		$resultSearch = preg_match("/MwSt\. \((.*) %\): (.*)EUR/ismU", $resultText, $arrFoundDocumentMWST);

		$thisDocumentNumber = trim($arrFoundDocumentDocumentNumber[1]);
		$thisDocumentDate = formatDate($arrFoundDocumentDate[1], "store");
		$doFillArray = false;
		if($thisDocumentNumber != ''){
			if(!is_array($arrDocumentDatas[$thisDocumentNumber])){
				$doFillArray = true;
			}
			else if($arrDocumentDatas[$thisDocumentNumber]["documentDate"] < $thisDocumentDate) {
				$doFillArray = true;
			}
		}
		if($doFillArray) {
			$arrDocumentDatas[$thisDocumentNumber] = array();

			$arrDocumentDatas[$thisDocumentNumber]["documentDate"] = $thisDocumentDate;
			$arrDocumentDatas[$thisDocumentNumber]["documentNumber"] = $thisDocumentNumber;
			$arrDocumentDatas[$thisDocumentNumber]["documentPdfPath"] = $arrProvisionPdfPaths[$thisPdfPathKey];
			$arrDocumentDatas[$thisDocumentNumber]["documentSalesmanNumber"] = $arrFoundDocumentSalesmanNumber[1];
			$arrDocumentDatas[$thisDocumentNumber]["documentSum"] = str_replace(",", ".", trim($arrFoundDocumentSum[1]));
			$arrDocumentDatas[$thisDocumentNumber]["documentTotalSum"] = str_replace(",", ".", trim($arrFoundDocumentTotalSum[1]));
			$arrDocumentDatas[$thisDocumentNumber]["documentMwstPercent"] = str_replace(",", ".", trim($arrFoundDocumentMWST[1]));
			$arrDocumentDatas[$thisDocumentNumber]["documentMwstValue"] = str_replace(",", ".", trim($arrFoundDocumentMWST[2]));
			#echo $resultText;
		}

		$resultText = '';
	}
	dd('arrDocumentDatas');
	exit;
	if(!empty($arrDocumentDatas)){
		$sql_updateCreatedDocuments = '';
		foreach($arrDocumentDatas as $thisDocumentNumber => $thisDocumentNumberDatas){
			// BOF CREATED DOCUMENTS
				$sql_checkCreatedDocuments = "SELECT `createdDocumentsNumber` FROM `bctr_createddocuments` WHERE 1 AND `createdDocumentsNumber` = '" . $thisDocumentNumber . "' ";
				$rs_checkCreatedDocuments = $dbConnection->db_query($sql_checkCreatedDocuments);
				$entryExists = $dbConnection->db_getMysqlNumRows($rs_checkCreatedDocuments);
				if($entryExists == 0){
					// INSERT
					$sql_updateCreatedDocuments = "
							INSERT INTO
								`bctr_createddocuments` (
									`createdDocumentsID`,
									`createdDocumentsType`,
									`createdDocumentsNumber`,
									`createdDocumentsCustomerNumber`,
									`createdDocumentsTitle`,
									`createdDocumentsFilename`,
									`createdDocumentsUserID`,
									`createdDocumentsTimeCreated`
								)
								VALUES (
									'%',
									'PR',
									'" . $thisDocumentNumber . "',
									'" . $thisDocumentNumberDatas["documentSalesmanNumber"] . "',
									'" . $thisDocumentNumberDatas["documentNumber"] . "',
									'" . $thisDocumentNumberDatas["documentPdfPath"] . "',
									'1',
									'" . $thisDocumentNumberDatas["documentDate"] . " 17:00'
								)
						";
				}
				else {
					// UPDATE
					$sql_updateCreatedDocuments = "
						UPDATE
							`bctr_createddocuments`
								SET
									`createdDocumentsFilename` = '" . $thisDocumentNumberDatas["documentPdfPath"] . "',
									`createdDocumentsTimeCreated` = '" . $thisDocumentNumberDatas["documentDate"] . "'
							WHERE 1
								AND `createdDocumentsNumber` = '" . $thisDocumentNumber . "'
						";
				}

				$rs_updateCreatedDocuments = $dbConnection->db_query($sql_updateCreatedDocuments);
			// EOF CREATED DOCUMENTS

			// BOF SALESMAN_PROVISIONS
			$sql_checkSalesmanProvisions = "SELECT `salesmenProvisionsDocumentNumber` FROM `common_salesmenprovisions` WHERE 1 AND `salesmenProvisionsDocumentNumber` = '" . $thisDocumentNumber . "' ";
			$rs_checkSalesmanProvisions = $dbConnection->db_query($sql_checkSalesmanProvisions);
			$entryExists = $dbConnection->db_getMysqlNumRows($rs_checkSalesmanProvisions);
			dd('entryExists');

			if($entryExists == 0){
				// INSERT
				$sql_updateSalesmanProvisions = "
						INSERT INTO
							`common_salesmenprovisions` (
								`salesmenProvisionsID`,
								`salesmenProvisionsDocumentNumber`,
								`salesmenProvisionsSalesmanID`,
								`salesmenProvisionsDocumentDate`,
								`salesmenProvisionsSum`,
								`salesmenProvisionsMwst`,
								`salesmenProvisionsMwstValue`,
								`salesmenProvisionsTotal`,
								`salesmenProvisionsStatus`,
								`salesmenProvisionsDocumentPath`,
								`salesmenProvisionsContentDocumentNumber`
							)
							VALUES (
								'%',
								'" . $thisDocumentNumberDatas["documentNumber"] . "',
								`salesmenProvisionsSalesmanID`,
								'" . $thisDocumentNumberDatas["documentDate"] . "',
								'" . $thisDocumentNumberDatas["documentSum"] . "',
								'" . $thisDocumentNumberDatas["documentMwstPercent"] . "',
								'" . $thisDocumentNumberDatas["documentMwstValue"] . "',
								'" . $thisDocumentNumberDatas["documentTotalSum"] . "',
								'1',
								'" . $thisDocumentNumberDatas["documentPdfPath"] . "',
								''
							)
					";
			}
			else {
				// UPDATE
				$sql_updateSalesmanProvisions = "
						UPDATE
							`common_salesmenprovisions`
								SET
									`salesmenProvisionsDocumentDate` = '" . $thisDocumentNumberDatas["documentDate"] . "',
									`salesmenProvisionsSum` = '" . $thisDocumentNumberDatas["documentSum"] . "',
									`salesmenProvisionsMwst` = '" . $thisDocumentNumberDatas["documentMwstPercent"] . "',
									`salesmenProvisionsMwstValue` = '" . $thisDocumentNumberDatas["documentMwstValue"] . "',
									`salesmenProvisionsTotal` = '" . $thisDocumentNumberDatas["documentTotalSum"] . "',
									`salesmenProvisionsDocumentPath`= '" . $thisDocumentNumberDatas["documentPdfPath"] . "'
							WHERE 1
								AND `salesmenProvisionsDocumentNumber` = '" . $thisDocumentNumber . "'
						";
			}
			$rs_updateSalesmanProvisions = $dbConnection->db_query($sql_updateSalesmanProvisions);
			echo mysqli_error();
			// EOF SALESMAN_PROVISIONS


# createdDocumentsID Absteigend 	createdDocumentsType 	createdDocumentsNumber 	createdDocumentsCustomerNumber 	createdDocumentsTitle 	createdDocumentsFilename 	createdDocumentsContent 	createdDocumentsUserID 	createdDocumentsTimeCreated 	createdDocumentsOrderIDs





		}
	}
}
?>