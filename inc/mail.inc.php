 <?php
	// $nowDate = date("d.m.Y");
	$nowDate = date("d.m.Y, H:i:s");

	if($_REQUEST["documentType"] == 'KA') {
		$pathDocumentFolder = DIRECTORY_UPLOAD_FILES;
		$thisCreatedPdfName = utf8_decode($thisCreatedPdfName);
	}
	else if($_REQUEST["documentType"] == 'EX') {
		$pathDocumentFolder = DIRECTORY_EXPORT_FILES;
		$thisCreatedPdfName = utf8_decode($thisCreatedPdfName);
	}
	else if($_REQUEST["documentType"] == 'SX') {
		$pathDocumentFolder = DIRECTORY_CREATED_DOCUMENTS_SALESMEN;
		$thisCreatedPdfName = utf8_decode($thisCreatedPdfName);
	}
	else {
		$pathDocumentFolder = DIRECTORY_CREATED_DOCUMENTS_CUSTOMERS;
	}

	$arrAttachmentFiles = array (
		$pathDocumentFolder . $thisCreatedPdfName => str_replace(" ", "_", myUnHtmlEntities($thisCreatedPdfName))
		// DIRECTORY_ATTACHMENT_DOCUMENTS
	);
	if(file_exists($arrAttachmentFiles[0])){
		#echo 'ja<br>';
	}
	else {
		#echo 'nein<br>';
	}

	// BOF CREATE HTML MAILS

		// BOF CREATE HTML MAIL TO CUSTOMER
			$mailToClient_HTML = $mailTemplate["html"];

			$mailToClient_HTML = preg_replace('/{###MAIL_HEADER_IMAGE###}/',$arrMailContentDatas[$_REQUEST["documentType"]]["MAIL_HEADER_IMAGE"] , $mailToClient_HTML);

			#$mailToClient_HTML = preg_replace('/{###MAIL_TITLE###}/',$arrMailContentDatas[$_REQUEST["documentType"]]["MAIL_TITLE"], $mailToClient_HTML);
			$mailToClient_HTML = preg_replace('/{###MAIL_TITLE###}/',$arrMailContentDatas[$_REQUEST["documentType"]]["MAIL_TITLE"], $mailToClient_HTML);
			$mailToClient_HTML = preg_replace('/{###MAIL_DATE_VALUE###}/', $nowDate, $mailToClient_HTML);

			#$mailToClient_HTML = preg_replace('/{###MAIL_SALUTATION###}/', $arrMailContentDatas[$_REQUEST["documentType"]]["MAIL_SALUTATION"], $mailToClient_HTML);

			if($_POST["sendAttachedDocument"] == '1' && $_POST["sendAttachedMailText"] != '') {
				$mailToClient_HTML = preg_replace('/{###MAIL_CONTENT###}/', nl2br($_POST["sendAttachedMailText"]), $mailToClient_HTML);
				$mailToClient_HTML = preg_replace('/{###MAIL_SALUTATION###}/', '', $mailToClient_HTML);
			}
			else {
				if($_POST["selectAdditionalMailtext"] != "") {
					$mailToClient_HTML = preg_replace('/{###MAIL_CONTENT###}/', ($_POST["selectAdditionalMailtext"]), $mailToClient_HTML);
					$mailToClient_HTML = preg_replace('/{###MAIL_SALUTATION###}/', '', $mailToClient_HTML);
					$mailToClient_HTML = preg_replace('/{###MAIL_REGARDS###}/', '', $mailToClient_HTML);
				}
				$mailToClient_HTML = preg_replace('/{###MAIL_CONTENT###}/', $arrMailContentDatas[$_REQUEST["documentType"]]["MAIL_CONTENT"], $mailToClient_HTML);
				$mailToClient_HTML = preg_replace('/{###MAIL_SALUTATION###}/', $arrMailContentDatas[$_REQUEST["documentType"]]["MAIL_SALUTATION"], $mailToClient_HTML);
			}
			$mailToClient_HTML = preg_replace('/{###MAIL_REGARDS###}/', $arrMailContentDatas[$_REQUEST["documentType"]]["MAIL_REGARDS"], $mailToClient_HTML);
			$mailToClient_HTML = preg_replace('/{###DOCUMENT_NUMBER###}/', $generatedDocumentNumber, $mailToClient_HTML);
			$mailToClient_HTML = preg_replace('/{###MAIL_SIGNATURE###}/', $arrMailContentDatas[$_REQUEST["documentType"]]["MAIL_SIGNATURE"], $mailToClient_HTML);
			$mailToClient_HTML = preg_replace('/{###MAIL_COPYRIGHT###}/', $arrMailContentDatas[$_REQUEST["documentType"]]["MAIL_COPYRIGHT"], $mailToClient_HTML);
		// EOF CREATE HTML MAIL TO CUSTOMER


		// BOF CREATE HTML MAIL TO BURHAN
			$mailToUs_HTML = $mailTemplate["html"];

			$mailToUs_HTML = preg_replace('/{###MAIL_HEADER_IMAGE###}/', $arrMailContentDatas[$_REQUEST["documentType"]]["MAIL_HEADER_IMAGE"], $mailToUs_HTML);

			#$mailToUs_HTML = preg_replace('/{###MAIL_TITLE###}/', $arrMailContentDatas[$_REQUEST["documentType"]]["MAIL_TITLE"], $mailToUs_HTML);
			$mailToUs_HTML = preg_replace('/{###MAIL_TITLE###}/', $arrMailContentDatas[$_REQUEST["documentType"]]["MAIL_TITLE"], $mailToUs_HTML);
			$mailToUs_HTML = preg_replace('/{###MAIL_DATE_VALUE###}/', $nowDate, $mailToUs_HTML);

			$mailToUs_HTML = preg_replace('/{###MAIL_SALUTATION###}/', $arrMailContentDatas[$_REQUEST["documentType"]]["MAIL_SALUTATION"], $mailToUs_HTML);
			$mailToUs_HTML = preg_replace('/{###MAIL_CONTENT###}/', $arrMailContentDatas[$_REQUEST["documentType"]]["MAIL_CONTENT"], $mailToUs_HTML);
			$mailToUs_HTML = preg_replace('/{###DOCUMENT_NUMBER###}/', $generatedDocumentNumber, $mailToUs_HTML);
			$mailToUs_HTML = preg_replace('/{###MAIL_REGARDS###}/', $arrMailContentDatas[$_REQUEST["documentType"]]["MAIL_REGARDS"], $mailToUs_HTML);
			$mailToUs_HTML = preg_replace('/{###MAIL_SIGNATURE###}/', $arrMailContentDatas[$_REQUEST["documentType"]]["MAIL_SIGNATURE"], $mailToUs_HTML);
			$mailToUs_HTML = preg_replace('/{###MAIL_COPYRIGHT###}/', $arrMailContentDatas[$_REQUEST["documentType"]]["MAIL_COPYRIGHT"], $mailToUs_HTML);

			// $mailToUs_HTML = preg_replace('/{###FIRST_NAME_TITLE###}/', 'Vorname', $mailToUs_HTML);
			// $mailToUs_HTML = preg_replace('/{###FIRST_NAME_VALUE###}/', $Vorname, $mailToUs_HTML);
		// EOF CREATE HTML MAIL TO BURHAN

		// BOF REMOVE UNNECESSARY CHARS
			$mailToClient_HTML = removeUnnecessaryChars($mailToClient_HTML);
			$mailToUs_HTML = removeUnnecessaryChars($mailToUs_HTML);
		// EOF REMOVE UNNECESSARY CHARS

	// EOF CREATE HTML MAILS

	// BOF CREATE TEXT MAIL
		$mailToClient_TEXT = $mailToClient_HTML;
		$mailToClient_TEXT = COMPANY_NAME_LONG . "<hr />" . $mailToClient_TEXT;
		$mailToClient_TEXT = "" . convertHtmlToText($mailToClient_TEXT);

		$mailToUs_TEXT = $mailToUs_HTML;
		$mailToUs_TEXT = COMPANY_NAME_LONG . "<hr />" . $mailToUs_TEXT;
		$mailToUs_TEXT = "" . convertHtmlToText($mailToUs_TEXT);
	// EOF CREATE TEXT MAIL



	// BOF SEND BASKET PER MAIL:

		// BOF TO BURHAN
			// $headersToUs = '"' . MAIL_ADDRESS_FROM_NAME . '" <' . MAIL_ADDRESS_FROM . '>' . "\r\n" . 'Reply-To: ' . MAIL_ADDRESS_REPLY_TO . '' . "\r\n" . 'Errors-To: ' . MAIL_ERRORS_TO;
			// $headersToUs = 'From: ' . MAIL_ADDRESS_FROM . "\r\n" . 'Reply-To: ' . MAIL_ADDRESS_REPLY_TO . '' . "\r\n" . 'Errors-To: ' . MAIL_ERRORS_TO;
			$mailToUs_Subject = preg_replace('/{###DOCUMENT_NUMBER###}/', $generatedDocumentNumber, $_POST["selectSubject"]).' (KOPIE)';
			//$sendMailToUs = mail($orderMailDatas["SendTo"], $mail_16, $mail_text, $headersToUs);

			$mailToUs = new htmlMimeMail();
			$mailToUs->setTextCharset('utf-8');
			$mailToUs->setHtmlCharset('utf-8');
			$mailToUs->setTextEncoding('7bit');
			$mailToUs->setHtmlEncoding('quoted-printable');
			$mailToUs->setHeadCharset('utf-8');
			// $mailToUs->setFrom($headersToUs);
			#if(MAIL_ADDRESS_BCC != '') { $mailToUs->setBcc(MAIL_ADDRESS_BCC); }
			$mailToUs->setHeader('From', '"' . MAIL_ADDRESS_FROM_NAME . '" <' . MAIL_ADDRESS_FROM . '>');
			$mailToUs->setHeader('Reply-To', MAIL_ADDRESS_REPLY_TO);
			$mailToUs->setHeader('Errors-To', MAIL_ERRORS_TO);
			$mailToUs->setHeader('Return-Path', MAIL_ERRORS_TO);
			$mailToUs->setHeader('Subject', $mailToUs_Subject);
			$mailToUs->setHeader('Date', date('D, d M y H:i:s O'));
			// $mailToUs->setHeader($name, $value);
			// $mailToUs->setSubject($mailToUs_Subject);

			#if(DEBUG) {
				#$tempDisplayMailToUsHTML = $mailToUs_HTML;
				#$tempDisplayMailToUsTEXT = $mailToUs_TEXT;
			#}

			$mailToUs->setHtml($mailToUs_HTML, $mailToUs_TEXT, DIRECTORY_MAIL_IMAGES);

			if(!empty($arrAttachmentFiles)) {
				foreach($arrAttachmentFiles as $thisFile => $thisFileName) {
					// $mailToUs->addAttachment($mailToUs->getFile($thisFile), $thisFileName, 'application/pdf');
					$mailToUs->addAttachment($mailToUs->getFile($thisFile), $thisFileName, '');
				}
			}

			if(isset($_POST["sendAttachedDocument"]) && $_POST["sendAttachedDocument"] != '1') {
				$mailToUs_arrRecipients = explode(";", MAIL_ADDRESS_FROM); // HAS TO BE AN ARRAY
				#$mailToUs_arrRecipients[] = MAIL_ADDRESS_BCC;
				#$mailToUs_arrRecipients[] = DEBUG_MAIL_TO_ADDRESS;
				if($_POST["selectCustomersRecipientMailCopy"] != '') {
					#$mailToUs_arrRecipients[] = $_POST["selectCustomersRecipientMailCopy"];

					if(preg_match("/;/", $_POST["selectCustomersRecipientMailCopy"])){
						$arrTemp = explode(";", $_POST["selectCustomersRecipientMailCopy"]);
						$mailToUs_arrRecipients = array_merge($mailToUs_arrRecipients, $arrTemp);
					}
					else if(preg_match("/,/", $_POST["selectCustomersRecipientMailCopy"])){
						$arrTemp = explode(",", $_POST["selectCustomersRecipientMailCopy"]);
						$mailToUs_arrRecipients = array_merge($mailToUs_arrRecipients, $arrTemp);
					}
					else {
						$mailToUs_arrRecipients[] = $_POST["selectCustomersRecipientMailCopy"];
					}
					$mailToUs_arrRecipients = array_unique($mailToUs_arrRecipients);
				}

				if(!empty($mailToUs_arrRecipients)){
					if(MAIL_SEND_TYPE == 'mail') {
						$sendMailToUs = $mailToUs->send($mailToUs_arrRecipients, 'mail');
					}
					else if(MAIL_SEND_TYPE == 'smtp') {
						$mailToUs->setSMTPParams(MAIL_ACCOUNT_SMTP, MAIL_ACCOUNT_PORT, MAIL_ACCOUNT_HELO, MAIL_ACCOUNT_AUTH, MAIL_ACCOUNT_USER, MAIL_ACCOUNT_PASSWORD);
						// function setSMTPParams($host = null, $port = null, $helo = null, $auth = null, $user = null, $pass = null)
						$sendMailToUs = $mailToUs->send($mailToUs_arrRecipients, 'smtp');
						if(!$sendMailToUs) {
							/*
							echo '<pre>';
							print_r($mailToUs->errors);
							echo '</pre>';

							echo '<pre>';
							print_r($mailToUs_arrRecipients);
							echo '</pre>';
							*/
						}
					}
					else {
						$sendMailToUs = false;
					}
					logSendedMails(implode(';',$mailToUs_arrRecipients), $mailToUs_Subject, $sendMailToUs, $_REQUEST["documentType"], $generatedDocumentNumber, $mailToUs_HTML);
				}
				else {
					unset($sendMailToUs);
				}
			}
		// EOF TO BURHAN


		// BOF TO CUSTOMER
			$mailToClient_arrRecipients = array();
			if(preg_match("/;/", $_POST["selectCustomersRecipientMail"])){
				$arrTemp = explode(";", $_POST["selectCustomersRecipientMail"]);
				$mailToClient_arrRecipients = array_merge($mailToClient_arrRecipients, $arrTemp);
			}
			else if(preg_match("/,/", $_POST["selectCustomersRecipientMail"])){
				$arrTemp = explode(",", $_POST["selectCustomersRecipientMail"]);
				$mailToClient_arrRecipients = array_merge($mailToClient_arrRecipients, $arrTemp);
			}
			else {
				$mailToClient_recipient = $_POST["selectCustomersRecipientMail"];
				$mailToClient_arrRecipients = array($mailToClient_recipient); // HAS TO BE AN ARRAY
				# if(DEBUG) { $mailToClient_arrRecipients = array(MAIL_ADDRESS_BCC, DEBUG_MAIL_TO_ADDRESS); }
			}
			$mailToClient_arrRecipients = array_unique($mailToClient_arrRecipients);
			$checkCustomerEmail = false;
			if(!empty($mailToClient_arrRecipients)){
				foreach($mailToClient_arrRecipients as $thisMailKey => $thisMailValue){
					$checkCustomerEmail = checkEmailStructure($thisMailValue);
					if($checkCustomerEmail == false){
						unset($mailToClient_arrRecipients[$thisMailKey]);
					}
				}
				if(!empty($mailToClient_arrRecipients)){
					$checkCustomerEmail = true;
				}
			}
			#$mailToClient_arrRecipients = explode(";", MAIL_ADDRESS_FROM); // HAS TO BE AN ARRAY

			if($checkCustomerEmail) {
				//$headersToClient = "FROM:"'.$orderMailDatas["SendName"].'" <'.$orderMailDatas["SendTo"].'>';
				$headersToClient = '"'.$orderMailDatas["SendName"].'" <'.$orderMailDatas["SendTo"].'>' . "\r\n" . 'Reply-To: '.$orderMailDatas["SendTo"].'' . "\r\n" .'Errors-To: '.$orderMailDatas["SendTo"];
				if(DEBUG) { $headersToClient = '"'.$debugOrderMailDatas["SendName"].'" <'.$debugOrderMailDatas["SendTo"].'>' . "\r\n" . 'Reply-To: '.$debugOrderMailDatas["SendTo"].'' . "\r\n" .'Errors-To: '.$debugOrderMailDatas["SendTo"]; }

				$mailToClient_Subject = preg_replace('/{###DOCUMENT_NUMBER###}/', $generatedDocumentNumber, $_POST["selectSubject"]).'';

				if($_POST["sendAttachedDocument"] == '1' && $_POST["sendAttachedMailSubject"] != '') {
					$mailToClient_Subject = $_POST["sendAttachedMailSubject"] . ', ' . $mailToClient_Subject;
				}

				$mailToClient = new htmlMimeMail();
				$mailToClient->setTextCharset('utf-8');
				$mailToClient->setHtmlCharset("utf-8");
				$mailToClient->setTextEncoding('7bit');
				$mailToClient->setHtmlEncoding('quoted-printable');
				$mailToClient->setHeadCharset('utf-8');
				//$mailToClient->setFrom($headersToClient);
				$mailToClient->setHeader('From', '"' . MAIL_ADDRESS_FROM_NAME . '" <' . MAIL_ADDRESS_FROM . '>');
				$mailToClient->setHeader('Reply-To', MAIL_ADDRESS_REPLY_TO);
				$mailToClient->setHeader('Errors-To', MAIL_ERRORS_TO);
				$mailToClient->setHeader('Return-Path', MAIL_ERRORS_TO);
				$mailToClient->setHeader('Subject', $mailToClient_Subject);
				$mailToClient->setHeader('Date', date('D, d M y H:i:s O'));
				// $mailToClient->setHeader($name, $value);
				// $mailToClient->setSubject($mailToClient_Subject);

				if(DEBUG) {
					$tempDisplayMailToClientHTML = $mailToClient_HTML;
					$tempDisplayMailToClientTEXT = $mailToClient_TEXT;
				}

				$mailToClient->setHtml($mailToClient_HTML, $mailToClient_TEXT, DIRECTORY_MAIL_IMAGES);

				if(!empty($arrAttachmentFiles)){
					foreach($arrAttachmentFiles as $thisFile => $thisFileName) {
						// $mailToClient->addAttachment($mailToClient->getFile($thisFile), $thisFileName, 'application/pdf');
						$mailToClient->addAttachment($mailToClient->getFile($thisFile), $thisFileName, '');
					}
				}
				if(file_exists(PATH_VCARD)) {
					$mailToClient->addAttachment($mailToClient->getFile(PATH_VCARD), 'Kontakt_vCard.vcf', 'application/text');
				}
				if(file_exists(PATH_AGB_PDF)) {
					$mailToClient->addAttachment($mailToClient->getFile(PATH_AGB_PDF), 'AGB.pdf', 'application/pdf');
				}
				clearstatcache();

				if(MAIL_SEND_TYPE == 'mail') {
					$sendMailToClient = $mailToClient->send($mailToClient_arrRecipients, 'mail');
				}
				else if(MAIL_SEND_TYPE == 'smtp') {
					$mailToClient->setSMTPParams(MAIL_ACCOUNT_SMTP, MAIL_ACCOUNT_PORT, MAIL_ACCOUNT_HELO, MAIL_ACCOUNT_AUTH, MAIL_ACCOUNT_USER, MAIL_ACCOUNT_PASSWORD);
					$sendMailToClient = $mailToClient->send($mailToClient_arrRecipients, 'smtp');
					if(!$sendMailToClient) {
						/*
						echo '<pre>';
						print_r($mailToClient->errors);
						echo '</pre>';

						echo '<pre>';
						print_r($mailToClient);
						echo '</pre>';
						*/
					}
				}
				logSendedMails(implode(';',$mailToClient_arrRecipients), $mailToClient_Subject, $sendMailToClient, $_REQUEST["documentType"], $generatedDocumentNumber, $mailToClient_HTML);
			}
			else {
				$sendMailToClient = false;
			}
		// EOF TO CUSTOMER
?>