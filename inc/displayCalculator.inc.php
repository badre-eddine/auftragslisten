<form name="formCalculator" action="#" method="get" onsubmit="return false;">
	<div style="margin:0;padding: 4px 4px 4px 4px;background-color:#CCC;width:530px;">
		<table border="2" cellpadding="2" cellspacing="2" class="calculatorTable">
			<tr>
			<td colspan="7"><input type="text" class="calculatorDisplay" name="calculatorDisplay" value="0" readonly="readonly" /></td>
			</tr>
			<tr>
				<td><input type="button" class="calculatorButton" value="cos" onclick="addSpecialFunction(this.value)" /></td>
				<td><input type="button" class="calculatorButton" value="sin" onclick="addSpecialFunction(this.value)" /></td>
				<td><input type="button" class="calculatorButton" value="tan" onclick="addSpecialFunction(this.value)" /></td>
				<td><input type="button" class="calculatorButton" value="acos" onclick="addSpecialFunction(this.value)" /></td>
				<td><input type="button" class="calculatorButton" value="asin" onclick="addSpecialFunction(this.value)" /></td>
				<td><input type="button" class="calculatorButton" value="atan" onclick="addSpecialFunction(this.value)" /></td>
				<td><input type="button" class="calculatorButton" value="abs" onclick="addSpecialFunction(this.value)" /></td>
			</tr>
			<tr>
				<td><input type="button" class="calculatorButton" value="exp" onclick="addSpecialFunction(this.value)" /></td>
				<td class="numberBlock"><input type="button" class="calculatorButton" value="7" onclick="addNumberAndOperators(this.value)" /></td>
				<td class="numberBlock"><input type="button" class="calculatorButton" value="8" onclick="addNumberAndOperators(this.value)" /></td>
				<td class="numberBlock"><input type="button" class="calculatorButton" value="9" onclick="addNumberAndOperators(this.value)" /></td>
				<td><input type="button" class="calculatorButton" value="+" onclick="addNumberAndOperators(this.value)" /></td>
				<td><input type="button" class="calculatorButton" value="round" onclick="addSpecialFunction(this.value)" /></td>
				<td><input type="button" class="calculatorButton" value="random" onclick="addSpecialFunction(this.value)" /></td>
			</tr>
			<tr>
				<td><input type="button" class="calculatorButton" value="&radic;" onclick="addSpecialFunction('sqrt')" /></td>
				<td class="numberBlock"><input type="button" class="calculatorButton" value="4" onclick="addNumberAndOperators(this.value)" /></td>
				<td class="numberBlock"><input type="button" class="calculatorButton" value="5" onclick="addNumberAndOperators(this.value)" /></td>
				<td class="numberBlock"><input type="button" class="calculatorButton" value="6" onclick="addNumberAndOperators(this.value)" /></td>
				<td><input type="button" class="calculatorButton" value="-" onclick="addNumberAndOperators(this.value)" /></td>
				<td><input type="button" class="calculatorButton" value="&pi;" onclick="addSpecialNumbers('pi')" /></td>
				<td><input type="button" class="calculatorButton" value="e" onclick="addSpecialNumbers(this.value)" /></td>
			</tr>
			<tr>
				<td><input type="button" class="calculatorButton" value="x&sup2;" onclick="addSpecialFunction('pow')" /></td>
				<td class="numberBlock"><input type="button" class="calculatorButton" value="1" onclick="addNumberAndOperators(this.value)" /></td>
				<td class="numberBlock"><input type="button" class="calculatorButton" value="2" onclick="addNumberAndOperators(this.value)" /></td>
				<td class="numberBlock"><input type="button" class="calculatorButton" value="3" onclick="addNumberAndOperators(this.value)" /></td>
				<td><input type="button" class="calculatorButton" value="*" onclick="addNumberAndOperators(this.value)" /></td>
				<td colspan="2"><input type="button" class="calculatorButton2" value="C" onclick="addSpecialFunction(this.value)" /></td>
			</tr>
			<tr>
				<td><input type="button" class="calculatorButton" value="ln" onclick="addSpecialFunction(this.value)" /></td>
				<td class="numberBlock"><input type="button" class="calculatorButton" value="0" onclick="addNumberAndOperators(this.value)" /></td>
				<td class="numberBlock"><input type="button" class="calculatorButton" value="." onclick="addNumberAndOperators(this.value)" /></td>
				<td class="numberBlock"><input type="button" class="calculatorButton" value="+/-" onclick="addSpecialFunction(this.value)" /></td>
				<td><input type="button" class="calculatorButton" value="/" onclick="addNumberAndOperators(this.value)" /></td>
				<td colspan="2"><input type="button" class="calculatorButton2" value="EXE" onclick="getResult(this.value)" /></td>
			</tr>
		</table>	
	</div>
</form>	

<script type="text/javascript" language="javascript">
	<!--
	function checkValue(number) {
		var validChars = "0123456789[]()-+*%/.";
		for(i = 0 ; i < number.length ; i++) {
			if (validChars.indexOf(number.charAt(i)) < 0) {
				return false;
			}
		}
		return true;
	}
	function addNumberAndOperators(number) {
		// var checkResult = checkValue(number);
		var form = window.document.forms["formCalculator"];
		var displayValue = form.elements["calculatorDisplay"].value;

		if(number == ".") {
			var checkSign = true;;
			for(i = 0 ; i < displayValue.length ; i++) {
				if(number.indexOf(displayValue.charAt(i)) >= 0)
				{
					checkSign = false;
					break;
				}
			}
			if(checkSign) {
				form.elements["calculatorDisplay"].value = displayValue + number;
			}
		}
		else {
			if(displayValue == "0") { displayValue = ""; }
			form.elements["calculatorDisplay"].value = displayValue + number;
		}
	}
	function getResult(operator) {
		if(operator == "EXE") {
			var form = window.document.forms["formCalculator"];
			var displayValue = form.elements["calculatorDisplay"].value;
			var result;
			try {
				result = eval(displayValue);
				displayCalculatedValue(result);
			}
			catch (e) {

			}
		}
	}

	function addSpecialNumbers(type) {
		var form = window.document.forms["formCalculator"];
		var displayValue = form.elements["calculatorDisplay"].value;

		var lastChar = displayValue.substr((displayValue.length-1), 1);
		var checkSign = true;
		if(displayValue != 0) {
			for(i = 0 ; i < 10 ; i++) {
				if(lastChar == i)
				{
					checkSign = false;
					break;
				}
			}
		}
		if(checkSign) {
			var number;
			if(displayValue == "0") { displayValue = ""; }
			if(type == 'e') { number = Math.E; }
			else if(type == 'pi') { number = Math.PI; }
			form.elements["calculatorDisplay"].value = displayValue + number;
		}
	}

	function addSpecialFunction(functionType) {
		var form = window.document.forms["formCalculator"];
		var displayValue = form.elements["calculatorDisplay"].value;
		var result;
		try {
			if(functionType == "C") {
				result = "0";
			}
			else if(functionType == "sqrt") {
				result = Math.sqrt(eval(displayValue));
			}
			else if(functionType == "pow") {
				result = (eval(displayValue) * eval(displayValue));
			}
			else if(functionType == "ln") {
				result = Math.log(eval(displayValue));
			}
			else if(functionType == "cos") {
				result = Math.cos(eval(displayValue));
			}
			else if(functionType == "sin") {
				result = Math.sin(eval(displayValue));
			}
			else if(functionType == "tan") {
				result = Math.tan(eval(displayValue));
			}
			else if(functionType == "acos") {
				result = Math.acos(eval(displayValue));
			}
			else if(functionType == "asin") {
				result = Math.asin(eval(displayValue));
			}
			else if(functionType == "atan") {
				result = Math.atan(eval(displayValue));
			}
			else if(functionType == "round") {
				result = Math.round(eval(displayValue));
			}
			else if(functionType == "random") {
				result = Math.random(eval(displayValue));
			}
			else if(functionType == "exp") {
				result = Math.exp(eval(displayValue));
			}
			else if(functionType == "abs") {
				result = Math.abs(eval(displayValue));
			}
			else if(functionType == "+/-") {
				result = (eval(displayValue)) *(-1);
			}
		}
		catch (e) {

		}
		displayCalculatedValue (result);
	}

	function displayCalculatedValue (result) {
		var form = window.document.forms["formCalculator"];
		if (!isFinite(result)) {
			form.elements["calculatorDisplay"].value = "FEHLER: Die Zahl ist nicht zu verarbeiten.";
		}
		else if(isNaN(result)) {
			form.elements["calculatorDisplay"].value = "FEHLER: Das Ergebnis ist keine Zahl.";
		}
		else {
			form.elements["calculatorDisplay"].value = result;
		}
	}

	function checkPressedKey(event) {
		var keycode;
		var keyvalue;
		if(!event) { event = window.event; }

		if(event.which) { keycode = event.which; }
		else { keycode = event.keyCode; }
		// document.write(keycode);


		if((keycode > 45 && keycode < 58) || (keycode > 95 && keycode < 112) || (keycode > 186 || keycode < 191 )) {
			switch(keycode) {
				case 48:
				keyvalue = "0";
				break;
				case 49:
				keyvalue = "1";
				break;
				case 50:
				keyvalue = "2";
				break;
				case 51:
				keyvalue = "3";
				break;
				case 52:
				keyvalue = "4";
				break;
				case 53:
				keyvalue = "5";
				break;
				case 54:
				keyvalue = "6";
				break;
				case 55:
				keyvalue = "7";
				break;
				case 56:
				keyvalue = "8";
				break;
				case 57:
				keyvalue = "9";
				break;
				case 96:
				keyvalue = "0";
				break;
				case 97:
				keyvalue = "1";
				break;
				case 98:
				keyvalue = "2";
				break;
				case 99:
				keyvalue = "3";
				break;
				case 100:
				keyvalue = "4";
				break;
				case 101:
				keyvalue = "5";
				break;
				case 102:
				keyvalue = "6";
				break;
				case 103:
				keyvalue = "7";
				break;
				case 104:
				keyvalue = "8";
				break;
				case 105:
				keyvalue = "9";
				break;
				case 110:
				keyvalue = ".";
				break;
				case 188:
				keyvalue = ".";
				break;
				case 190:
				keyvalue = ".";
				break;
				case 111:
				keyvalue = "/";
				break;
				case 106:
				keyvalue = "*";
				break;
				case 109:
				keyvalue = "-";
				break;
				case 107:
				keyvalue = "+";
				break;
				case 189:
				keyvalue = "-";
				break;
				case 187:
				keyvalue = "+";
				break;
				default:
				keyvalue = "";
				break;
			}
			if(keyvalue != "") {
				addNumberAndOperators(keyvalue);
			}
		}
		if(keycode == 13) {
			getResult('EXE');
		}
		if(keycode == 46 || keycode == 67) {
			addSpecialFunction('C');
		}
	}
	document.onkeydown = checkPressedKey;
	//-->
</script>