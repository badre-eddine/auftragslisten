<?php
	function formatDate($date, $mode) {
		// if(preg_match('/[0-9]{4}-[0-9]{2}-[0-9]{2} [0-9]{2}:[0-9]{2}:[0-9]{2}/', $date) {
			if(1){
			$arrTemp = explode(" ", $date);
			$date = $arrTemp[0];
			$time = $arrTemp[1];
		}
		else {
			$time = '';
		}

		if($mode == "store") {
			$arrDate = explode(".", $date);
			$newDate = $arrDate[2]."-".$arrDate[1]."-".$arrDate[0];
			if($arrDate[2] == '' OR $arrDate[1] == '' OR $arrDate[2] == '') {
				$newDate = "0000-00-00";
			}
		}
		else if($mode == "display") {
			$arrDate = explode("-", $date);
			$newDate = trim($arrDate[2]).".".trim($arrDate[1]).".".trim($arrDate[0]);
			if($newDate == "00.00.0000" || $newDate == "..") {
				$newDate = '';
			}
		}
		else if($mode == "split") {
			$arrDate = explode("-", $date);
			$newDate["day"] = $arrDate[2];
			$newDate["month"] = $arrDate[1];
			$newDate["year"] = $arrDate[0];
		}
		else if($mode == "time") {

		}
		if(!is_array($newDate)){
			$newDate = preg_replace('/^\.\./', '', $newDate);
			$newDate .= ' ' .$time;
			$newDate = trim($newDate);
		}

		return $newDate;
	}

	// ---------------------------------

	function readUserRights() {
		global $_COOKIE, $_SESSION;

		$dbConnection = new DB_Connection();
		$db_open = $dbConnection->db_connect();
		if($dbConnection->db_displayErrors() != "") {
			$errorMessage .= $dbConnection->db_displayErrors(). '<br />';
		}

		if ($db_open) {
			$sql = "SELECT
						`usersRights`

						FROM `" . TABLE_USERS . "`
						WHERE 1
							AND `usersID` = '".$_SESSION["usersID"]."'
							AND `usersActive` = '1'
						LIMIT 1
					";

			$rs = $dbConnection->db_query($sql);

			list($userRights) = mysqli_fetch_array($rs);

			if($dbConnection) {
				#$dbConnection->db_close($db_open);
			}
			$arrUserRights = unserialize($userRights);
		 
			if($arrUserRights["adminArea"]) {
				foreach($arrUserRights as $thisKey => $thisValue) {
					if($thisKey != "adminArea") {
						//die("true");
						$arrUserRights[$thisKey] = 1;
					}
				}
			}

			return $arrUserRights;
		}
	}

	// ---------------------------------

	function getUserDatas() {
		global $_COOKIE, $_SESSION;

		$dbConnection = new DB_Connection();
		$db_open = $dbConnection->db_connect();
		if($dbConnection->db_displayErrors() != "") {
			$errorMessage .= $dbConnection->db_displayErrors(). '<br />';
		}

		if ($db_open) {

			$sql = "SELECT
						`usersLogin`,
						`usersFirstName`,
						`usersLastName`,
						`userMailDatas`,
						`usersUserToPersonnel`

						FROM `" . TABLE_USERS . "`
						WHERE 1
							AND `usersID` = '".$_SESSION["usersID"]."'
							AND `usersActive` = '1'
						LIMIT 1
					";

			$rs = $dbConnection->db_query($sql);

			list(
				$userDatas["usersLogin"],
				$userDatas["usersFirstName"],
				$userDatas["usersLastName"],
				$userDatas["userMailDatas"],
				$userDatas["usersUserToPersonnel"]

			) = mysqli_fetch_array($rs);

			if($dbConnection) {
				#$dbConnection->db_close($db_open);
			}
			return $userDatas;
		}
	}

	// ---------------------------------

	function getPrinters() {
		global $dbConnection;

		$sql = "SELECT
					`personnelID` AS `printersID`,
					`personnelFirstName` AS `printersFirstName`,
					`personnelLastName` AS `printersLastName`

					FROM `" . TABLE_PERSONNEL . "`

					WHERE 1
						AND `personnelActive` = '1'
						AND `personnelGroup` = 4

					ORDER BY `printersLastName`
		";

		$rs = $dbConnection->db_query($sql);

		while($ds = mysqli_fetch_assoc($rs)) {
			foreach(array_keys($ds) as $field) {
				$arrPrinterDatas[$ds["printersID"]][$field] = $ds[$field];
			}
		}
		return $arrPrinterDatas;
	}

	// ---------------------------------

	function getAgents() {
		global $dbConnection;

		$sql = "SELECT
						`salesmenID` AS `customersID`,
						`salesmenKundennummer` AS `customersKundennummer`,
						`salesmenFirmenname` AS `customersFirmenname`,
						`salesmenTypesID` AS `customersTypesID`,
						`salesmenTypesName` AS `customersTypesName`
					FROM `" . TABLE_SALESMEN . "`

					WHERE 1
					ORDER BY `salesmenFirmenname`
			";
		$rs = $dbConnection->db_query($sql);
		while($ds = mysqli_fetch_assoc($rs)) {
			foreach(array_keys($ds) as $field) {
				$arrAgentDatas[$ds["customersID"]][$field] = $ds[$field];
			}
		}
		return $arrAgentDatas;
	}

	function getAgents2() {
		global $dbConnection;

		$sql = "SELECT
					`customersID`,
					`customersKundennummer`,
					`customersFirmenname`,
					`customersTyp`,
					`customersVertreterID` AS `customersVertreterID`,
					`customersPriceListsID`,
					IF(`customersMail1` != '', `customersMail1`,
						IF(`customersMail2` != '', `customersMail2`, '')
					) AS `customersMail`,

					`customersGroupCompanyNumber`

					FROM `" . TABLE_CUSTOMERS . "`
					WHERE 1
						AND (
							`customersTyp` = 2
							OR `customersTyp` = 3
							OR `customersTyp` = 4
							OR `customersTyp` = 9
							OR `customersTyp` = 11
						)
					ORDER BY `customersFirmenname`
			";
		$rs = $dbConnection->db_query($sql);

		while($ds = mysqli_fetch_assoc($rs)) {
			foreach(array_keys($ds) as $field) {
				$arrAgentDatas[$ds["customersID"]][$field] = $ds[$field];
			}
		}

		return $arrAgentDatas;
	}

	// ---------------------------------
	function getCustomerTypeMandators() {
		global $dbConnection;

		$sql = "SELECT
					`customersID`,
					`customersKundennummer`,
					`customersFirmenname`,
					`customersTyp`,
					`customersVertreterID` AS `customersVertreterID`

					FROM `" . TABLE_CUSTOMERS . "`
					WHERE 1
						AND `customersTyp` = 11
					ORDER BY `customersFirmenname`
			";
		$rs = $dbConnection->db_query($sql);
		$arrCustomerTypeMandators = array();
		while($ds = mysqli_fetch_assoc($rs)) {
			foreach(array_keys($ds) as $field) {
				$arrCustomerTypeMandators[$ds["customersID"]][$field] = $ds[$field];
			}
		}

		return $arrCustomerTypeMandators;
	}
	// ---------------------------------

	function getPaymentTypes() {
		global $dbConnection;
		$sql = "SELECT
					`paymentTypesID`,
					`paymentTypesShortName`,
					`paymentTypesName`,
					`paymentTypesActive`

					FROM `" . TABLE_PAYMENT_TYPES . "`

					WHERE 1
						AND `paymentTypesActive` = '1'

					ORDER BY
						`paymentTypesSort`,
						`paymentTypesName`
		";

		$rs = $dbConnection->db_query($sql);

		while($ds = mysqli_fetch_assoc($rs)) {
			foreach(array_keys($ds) as $field) {
				$arrPaymentTypeDatas[$ds["paymentTypesID"]][$field] = $ds[$field];
			}
		}
		return $arrPaymentTypeDatas;
	}

	// ---------------------------------

	function getPaymentStatut() {
		global $dbConnection;
		$sql = "SELECT
					`paymentStatusTypesID`,
					`paymentStatusTypesShortName`,
					`paymentStatusTypesName`,
					`paymentStatusTypesActive`

					FROM `" . TABLE_PAYMENT_STATUS_TYPES . "`

					WHERE 1
						AND `paymentStatusTypesActive` = '1'

					ORDER BY
						`paymentStatusTypesShortName`,
						`paymentStatusTypesName`
		";

		$rs = $dbConnection->db_query($sql);

		while($ds = mysqli_fetch_assoc($rs)) {
			foreach(array_keys($ds) as $field) {
				$arrPaymentStatutDatas[$ds["paymentStatusTypesID"]][$field] = $ds[$field];
			}
		}
		return $arrPaymentStatutDatas;
	}
	// ---------------------------------




	// ---------------------------------
	function getPaymentConditions() {
		global $dbConnection;
		$sql = "SELECT

					`paymentConditionsID`,
					`paymentConditionsShortName`,
					`paymentConditionsName`,
					`paymentConditionsSkonto`,
					`paymentConditionsSkontoDaysLimit`,
					`paymentConditionsDaysLimit`,
					`paymentConditionsActive`

					FROM `" . TABLE_PAYMENT_CONDITIONS . "`

					WHERE 1
						AND `paymentConditionsActive` = '1'

					ORDER BY `paymentConditionsName`
		";

		$rs = $dbConnection->db_query($sql);

		while($ds = mysqli_fetch_assoc($rs)) {
			foreach(array_keys($ds) as $field) {
				$arrPaymentConditionDatas[$ds["paymentConditionsID"]][$field] = $ds[$field];
			}
		}
		return $arrPaymentConditionDatas;
	}

	// ---------------------------------

	function getPaymentStatusTypes() {
		global $dbConnection;
		$sql = "SELECT
					`paymentStatusTypesID`,
					`paymentStatusTypesShortName`,
					`paymentStatusTypesName`,
					`paymentStatusTypesSort`

					FROM `" . TABLE_PAYMENT_STATUS_TYPES . "`

					WHERE 1
						AND `paymentStatusTypesActive` = '1'

					ORDER BY `paymentStatusTypesSort`
		";

		$rs = $dbConnection->db_query($sql);

		while($ds = mysqli_fetch_assoc($rs)) {
			foreach(array_keys($ds) as $field) {
				$arrPaymentStatusTypeDatas[$ds["paymentStatusTypesID"]][$field] = $ds[$field];
			}
		}
		return $arrPaymentStatusTypeDatas;
	}

	// ---------------------------------

	function getOrderTypes() {
		global $dbConnection;
		$sql = "SELECT
					`orderTypesID`,
					`orderTypesShortName`,
					`orderTypesName`

					FROM `" . TABLE_ORDER_TYPES . "`

					WHERE 1

					ORDER BY `orderTypesName`
		";

		$rs = $dbConnection->db_query($sql);

		while($ds = mysqli_fetch_assoc($rs)) {
			foreach(array_keys($ds) as $field) {
				$arrOrderTypeDatas[$ds["orderTypesID"]][$field] = $ds[$field];
			}
		}
		return $arrOrderTypeDatas;
	}

	// ---------------------------------

	function getOrderStatusTypes() {
		global $dbConnection;
		$sql = "SELECT
					`orderStatusTypesID`,
					`orderStatusTypesShortName`,
					`orderStatusTypesName`

					FROM `" . TABLE_ORDER_STATUS_TYPES . "`

					WHERE 1
						AND `orderStatusTypesActive` = '1'

					ORDER BY
						`orderStatusTypesSort`,
						`orderStatusTypesName`
		";

		$rs = $dbConnection->db_query($sql);

		while($ds = mysqli_fetch_assoc($rs)) {
			foreach(array_keys($ds) as $field) {
				$arrOrderStatusTypeDatas[$ds["orderStatusTypesID"]][$field] = $ds[$field];
			}
		}
		return $arrOrderStatusTypeDatas;
	}

	// ---------------------------------
	function getOrderStatusTypesExtern() {
		global $dbConnection;
		$sql = "SELECT
					`orderStatusTypesID`,
					`orderStatusTypesShortName`,
					`orderStatusTypesName`

					FROM `" . TABLE_ORDER_STATUS_TYPES_EXTERN . "`

					WHERE 1
						AND `orderStatusTypesActive` = '1'

					ORDER BY `orderStatusTypesName`
		";

		$rs = $dbConnection->db_query($sql);

		while($ds = mysqli_fetch_assoc($rs)) {
			foreach(array_keys($ds) as $field) {
				$arrOrderStatusTypeDatas[$ds["orderStatusTypesID"]][$field] = $ds[$field];
			}
		}
		return $arrOrderStatusTypeDatas;
	}
	// ---------------------------------

	function getSalutationTypes() {
		global $dbConnection;
		$sql = "SELECT
					`salutationTypesID`,
					`salutationTypesShortName`,
					`salutationTypesName`

					FROM `" . TABLE_SALUTATION_TYPES . "`

					WHERE 1

					ORDER BY `salutationTypesName` DESC
		";

		$rs = $dbConnection->db_query($sql);

		while($ds = mysqli_fetch_assoc($rs)) {
			foreach(array_keys($ds) as $field) {
				$arrSalutationTypeDatas[$ds["salutationTypesID"]][$field] = $ds[$field];
			}
		}
		return $arrSalutationTypeDatas;
	}

	// ---------------------------------

	function getCountryTypes() {
		global $dbConnection;
		$sql = "SELECT
					`status`,
					`countries_name_DE`,
					`countries_id`,
					`countries_name_DE` AS `countries_name`,
					`countries_iso_code_2`,
					`countries_iso_code_3`,
					`isEU`,

					`countriesUnionsCountryIsoCode`,
					`countriesUnionsCountryName`,
					`countriesUnionsUnionType`

					FROM `" . TABLE_COUNTRIES . "`

					LEFT JOIN `" . TABLE_COUNTRIES_UNIONS . "`
					ON(`" . TABLE_COUNTRIES . "`.`countries_iso_code_2` = `" . TABLE_COUNTRIES_UNIONS . "`.`countriesUnionsCountryIsoCode`)

					WHERE 1
						AND `status` = 1

					ORDER BY `countries_name_DE`
			";

		$rs = $dbConnection->db_query($sql);

		while($ds = mysqli_fetch_assoc($rs)) {
			foreach(array_keys($ds) as $field) {
				$arrCountyTypeDatas[$ds["countries_id"]][$field] = $ds[$field];
			}
		}
		return $arrCountyTypeDatas;
	}

	// ---------------------------------
	function getSalesmen() {
		global $dbConnection;

		$sql = "SELECT
					`" . TABLE_CUSTOMERS . "`.`customersID`,
					`" . TABLE_CUSTOMERS . "`.`customersKundennummer`,
					`" . TABLE_CUSTOMERS . "`.`customersFirmenname`,
					`" . TABLE_CUSTOMERS . "`.`customersTyp`,
					`" . TABLE_CUSTOMERS . "`.`customersVertreterID`,
					`" . TABLE_CUSTOMERS . "`.`customersActive`,
					`" . TABLE_CUSTOMERS . "`.`customersMail1`,
					`" . TABLE_CUSTOMERS . "`.`customersMail2`,

					`" . TABLE_SALESMEN . "`.`salesmenID`,
					`" . TABLE_SALESMEN . "`.`salesmenKundennummer`,
					`" . TABLE_SALESMEN . "`.`salesmenGetProvision`	,
					`" . TABLE_SALESMEN . "`.`salesmenExportDatas`,
					`" . TABLE_SALESMEN . "`.`salesmenAreas`,
					`" . TABLE_SALESMEN . "`.`salesmenAreasActive`,
					`" . TABLE_SALESMEN . "`.`salesmenAreaProtection`,
					`" . TABLE_SALESMEN . "`.`salesmenPersonnelID`,
					`" . TABLE_SALESMEN . "`.`salesmenActive`

					FROM `" . TABLE_CUSTOMERS . "`

					LEFT JOIN `" . TABLE_SALESMEN . "`
					ON(`" . TABLE_CUSTOMERS . "`.`customersKundennummer` = `" . TABLE_SALESMEN . "`.`salesmenKundennummer`)

					WHERE 1
						AND (
							`customersTyp` = 2 /* Vertreter */
							OR
							`customersTyp` = 4 /* Handelsvertreter */
							OR
							`customersTyp` = 9 /* Außendienst-Mitarbeiter */
							OR
							`customersTyp` = 10 /* EX-Außendienst-Mitarbeiter */
							OR
							`customersTyp` = 3 /* Wiederverkäufer */
						)
						AND `" . TABLE_CUSTOMERS . "`.`customersActive` = '1'
					ORDER BY
						/* `customersTyp`, */
						`customersFirmenname`
			";
		$rs = $dbConnection->db_query($sql);

		while($ds = mysqli_fetch_assoc($rs)) {
			#if($ds["salesmenExportDatas"] == '1'){
				foreach(array_keys($ds) as $field) {
					$arrSalesmenDatas[$ds["customersID"]][$field] = $ds[$field];
				}
			#}
		}

		return $arrSalesmenDatas;
	}

	// ---------------------------------
	function getAquisitionUsers($indexUser='usersAuftragslistenCustomerID') {
		global $dbConnection_ExternAcqisition;

		$sql = "SELECT
				`usersID`,
				`usersFirstName`,
				`usersLastName`,
				`usersAddressStreet`,
				`usersAddressStreetNumber`,
				`usersAddressZipcode`,
				`usersAddressCity`,
				`usersAuftragslistenCustomerID`,
				`usersLogin`,
				`usersActive`,
				`usersRights`,
				`userMailDatas`,
				`usersAreas`,
				`usersActive`,

				`usersGroupID`,
				`usersStartDate`,
				`usersSelectable`,
				`usersLocation`

				FROM `" . TABLE_ACQUISATION_USERS . "`

				WHERE 1
					/* AND `usersAuftragslistenCustomerID` > 0 */

				ORDER BY `usersLastName`, `usersFirstName`
		";
		$rs = $dbConnection_ExternAcqisition->db_query($sql);

		while($ds = mysqli_fetch_assoc($rs)) {
			foreach(array_keys($ds) as $field) {
				#$arrUserDatas[$ds["usersAuftragslistenCustomerID"]][$field] = utf8_decode($ds[$field]);
				$arrUserDatas[$ds[$indexUser]][$field] = utf8_decode($ds[$field]);
			}
		}

		return $arrUserDatas;
	}

	// ---------------------------------
	function getSalesmen2() {
		global $dbConnection;

		$sql = "SELECT
					`" . TABLE_SALESMEN . "`.`salesmenID`,
					`" . TABLE_SALESMEN . "`.`salesmenKundennummer`,
					`" . TABLE_SALESMEN . "`.`salesmenFirmenname`,
					`" . TABLE_SALESMEN . "`.`salesmenAreas`,
					`" . TABLE_SALESMEN . "`.`salesmenAreasActive`,
					`" . TABLE_SALESMEN . "`.`salesmenAreaProtection`,

					`" . TABLE_CUSTOMERS . "`.`customersID`,
					`" . TABLE_CUSTOMERS . "`.`customersKundennummer`,
					`" . TABLE_CUSTOMERS . "`.`customersFirmenname`,
					`" . TABLE_CUSTOMERS . "`.`customersFirmenInhaberVorname`,
					`" . TABLE_CUSTOMERS . "`.`customersFirmenInhaberNachname`,
					`" . TABLE_CUSTOMERS . "`.`customersTyp`,
					`" . TABLE_CUSTOMERS . "`.`customersVertreterID`,
					`" . TABLE_CUSTOMERS . "`.`customersActive`

					FROM `" . TABLE_SALESMEN . "`

					LEFT JOIN `" . TABLE_CUSTOMERS . "`
					ON(`" . TABLE_CUSTOMERS . "`.`customersKundennummer` = `" . TABLE_SALESMEN . "`.`salesmenKundennummer`)

					WHERE 1
						AND `" . TABLE_SALESMEN . "`.`salesmenActive` = '1'
						AND `" . TABLE_SALESMEN . "`.`salesmenAreas` != ''
						/*AND `" . TABLE_CUSTOMERS . "`.`customersTyp` = 9*/
					ORDER BY `" . TABLE_SALESMEN . "`.`salesmenFirmenname`
			";
		$rs = $dbConnection->db_query($sql);
		$arrSalesmenDatas = array();
		while($ds = mysqli_fetch_assoc($rs)) {
			foreach(array_keys($ds) as $field) {
				$arrSalesmenDatas[$ds["salesmenID"]][$field] = $ds[$field];
			}
		}

		return $arrSalesmenDatas;
	}

	// ---------------------------------
	function getSalesmenWithActiveAreas(){
		global $dbConnection;

		$sql = "SELECT
					`" . TABLE_SALESMEN . "`.`salesmenID`,
					`" . TABLE_SALESMEN . "`.`salesmenKundennummer`,
					`" . TABLE_SALESMEN . "`.`salesmenFirmenname`,
					`" . TABLE_SALESMEN . "`.`salesmenAreas`,
					`" . TABLE_SALESMEN . "`.`salesmenAreasActive`,
					`" . TABLE_SALESMEN . "`.`salesmenAreaProtection`,

					`" . TABLE_CUSTOMERS . "`.`customersID`,
					`" . TABLE_CUSTOMERS . "`.`customersKundennummer`,
					`" . TABLE_CUSTOMERS . "`.`customersFirmenname`,
					`" . TABLE_CUSTOMERS . "`.`customersFirmenInhaberVorname`,
					`" . TABLE_CUSTOMERS . "`.`customersFirmenInhaberNachname`,
					`" . TABLE_CUSTOMERS . "`.`customersTyp`,
					`" . TABLE_CUSTOMERS . "`.`customersVertreterID`,
					`" . TABLE_CUSTOMERS . "`.`customersActive`

					FROM `" . TABLE_SALESMEN . "`

					LEFT JOIN `" . TABLE_CUSTOMERS . "`
					ON(`" . TABLE_CUSTOMERS . "`.`customersKundennummer` = `" . TABLE_SALESMEN . "`.`salesmenKundennummer`)

					WHERE 1
						AND `" . TABLE_SALESMEN . "`.`salesmenActive` = '1'
						AND `" . TABLE_SALESMEN . "`.`salesmenAreasActive` = '1'
						AND `" . TABLE_SALESMEN . "`.`salesmenAreas` != ''
						/*AND `" . TABLE_CUSTOMERS . "`.`customersTyp` = 9*/
					ORDER BY `" . TABLE_SALESMEN . "`.`salesmenFirmenname`
			";
		$rs = $dbConnection->db_query($sql);
		$arrSalesmenWithActiveAreasDatas = array();
		while($ds = mysqli_fetch_assoc($rs)) {
			foreach(array_keys($ds) as $field) {
				$arrSalesmenWithActiveAreasDatas[$ds["salesmenID"]][$field] = $ds[$field];
			}
		}

		return $arrSalesmenWithActiveAreasDatas;
	}
	// ---------------------------------

	function getSalesmenTypes() {
		global $dbConnection;
		$sql = "SELECT
					`customerTypesID` AS `salesmenTypesID`,
					`customerTypesName` AS `salesmenTypesName`,
					`customerTypesShortName` AS `salesmenTypesShortName`

					FROM `" . TABLE_CUSTOMER_TYPES . "`

					WHERE 1

					ORDER BY `salesmenTypesID`
		";

		$rs = $dbConnection->db_query($sql);

		while($ds = mysqli_fetch_assoc($rs)) {
			foreach(array_keys($ds) as $field) {
				$arrSalesmenTypeDatas[$ds["salesmenTypesID"]][$field] = $ds[$field];
			}
		}
		return $arrSalesmenTypeDatas;
	}
	// ---------------------------------
	function getSalesmenGroups() {

	}

	// ---------------------------------

	function getCustomerTypes() {
		global $dbConnection;
		$sql = "SELECT
					`customerTypesID`,
					`customerTypesName`,
					`customerTypesShortName`,
					`customersTypesActive`,
					`customerTypesHasPricelist`

					FROM `" . TABLE_CUSTOMER_TYPES . "`

					WHERE 1

					ORDER BY `customerTypesID`
		";

		$rs = $dbConnection->db_query($sql);

		while($ds = mysqli_fetch_assoc($rs)) {
			foreach(array_keys($ds) as $field) {
				$arrCustomerTypeDatas[$ds["customerTypesID"]][$field] = $ds[$field];
			}
		}
		return $arrCustomerTypeDatas;
	}

	// ---------------------------------
		function getCustomersPriceLists() {
			global $dbConnection;
			$sql = "
				SELECT
						`customersPriceListsID`,
						`customersPriceListsName`,
						`customersPriceListsShortName`,
						`customerPriceListsDescription`,
						`customersPriceListsActive`,
						`customersPriceListsSort`,
						`customersPriceListsIsStandard`

						FROM `" . TABLE_CUSTOMERS_PRICE_LISTS . "`

						WHERE 1
							AND `customersPriceListsActive` = '1'
							ORDER BY `customersPriceListsSort`
			";
			$rs = $dbConnection->db_query($sql);
			$arrCustomersPriceListDatas = array();
			while($ds = mysqli_fetch_assoc($rs)) {
				foreach(array_keys($ds) as $field) {
					$arrCustomersPriceListDatas[$ds["customersPriceListsID"]][$field] = $ds[$field];
				}
			}
			return $arrCustomersPriceListDatas;
		}

	// ---------------------------------
	function getCustomerGroups($groupID='') {
		global $dbConnection;

		$where = "";
		if($groupID != ''){
			$where = " AND `customerGroupsID` = '" . $groupID . "' ";
		}
		$sql = "SELECT
					`customerGroupsID`,
					`customerGroupsName`,
					`customerGroupsActive`,
					`customerGroupsHasPricelist`,
					`customerGroupsMailRecipient_AB`,
					`customerGroupsMailRecipient_RE`,
					`customerGroupsMailRecipient_RE` AS `customerGroupsMailRecipient_LS`,
					`customerGroupsMailRecipient_RE` AS `customerGroupsMailRecipient_GU`,
					`customerGroupsMailRecipient_RE` AS `customerGroupsMailRecipient_MA`,
					`customerGroupsMailRecipient_RE` AS `customerGroupsMailRecipient_M1`,
					`customerGroupsMailRecipient_RE` AS `customerGroupsMailRecipient_M2`,
					`customerGroupsMailRecipient_RE` AS `customerGroupsMailRecipient_M3`,
					`customerGroupsMailRecipient_COPY`

					FROM `" . TABLE_CUSTOMER_GROUPS . "`

					WHERE 1
						" . $where . "

					ORDER BY `customerGroupsID`
		";

		$rs = $dbConnection->db_query($sql);
		$arrCustomerGroupDatas = array();
		while($ds = mysqli_fetch_assoc($rs)) {
			foreach(array_keys($ds) as $field) {
				$arrCustomerGroupDatas[$ds["customerGroupsID"]][$field] = $ds[$field];
			}
		}
		return $arrCustomerGroupDatas;
	}
	// ---------------------------------
	function getContactTypes() {
		global $dbConnection;
		$sql = "SELECT
					`contactTypeID`,
					`contactTypeName`,
					`contactTypeNameShort`,
					`contactTypeSort`,
					`contactTypeActive`

					FROM `" . TABLE_CONTACT_TYPES . "`

					WHERE 1

					ORDER BY `contactTypeSort`
		";

		$rs = $dbConnection->db_query($sql);

		while($ds = mysqli_fetch_assoc($rs)) {
			foreach(array_keys($ds) as $field) {
				$arrContactTypeDatas[$ds["contactTypeID"]][$field] = $ds[$field];
			}
		}
		return $arrContactTypeDatas;
	}
	// ---------------------------------
	function getPrintColors () {
		$arrPrintColors = array();
		$arrPrintColors[0] = array("printColorName" => "Blau / dunkel", "printColorValue" => "dunkelblau", "printColorCode" => "Pröll 523");
		$arrPrintColors[1] = array("printColorName" => "Blau / hell", "printColorValue" => "hellblau", "printColorCode" => "Pröll 518");
		$arrPrintColors[2] = array("printColorName" => "Blau / mittel", "printColorValue" => "mittelblau", "printColorCode" => "Pröll 521");
		$arrPrintColors[3] = array("printColorName" => "Gelb", "printColorValue" => "gelb", "printColorCode" => "Pröll 104");
		$arrPrintColors[4] = array("printColorName" => "Gold", "printColorValue" => "gold", "printColorCode" => "Pröll 861");
		$arrPrintColors[17] = array("printColorName" => "Grau", "printColorValue" => "grau", "printColorCode" => "");
		$arrPrintColors[5] = array("printColorName" => "Grün / dunkel", "printColorValue" => "dunkelgrün", "printColorCode" => "Pröll 615");
		$arrPrintColors[6] = array("printColorName" => "Grün / hell", "printColorValue" => "hellgrün", "printColorCode" => "Pröll 628");
		$arrPrintColors[7] = array("printColorName" => "Orange", "printColorValue" => "orange", "printColorCode" => "Pröll 209");
		$arrPrintColors[8] = array("printColorName" => "Rot", "printColorValue" => "rot", "printColorCode" => "Pröll 315");
		$arrPrintColors[9] = array("printColorName" => "Schwarz", "printColorValue" => "schwarz", "printColorCode" => "Pröll 948");
		$arrPrintColors[10] = array("printColorName" => "Silber", "printColorValue" => "silber", "printColorCode" => "Pröll 733");
		$arrPrintColors[19] = array("printColorName" => "Türkis", "printColorValue" => "türkis", "printColorCode" => "");
		$arrPrintColors[11] = array("printColorName" => "Violett", "printColorValue" => "violett", "printColorCode" => "Pröll 417");
		$arrPrintColors[12] = array("printColorName" => "Weiss", "printColorValue" => "weiss", "printColorCode" => "Pröll 945");
		$arrPrintColors[13] = array("printColorName" => "chrom", "printColorValue" => "chrom", "printColorCode" => "");
		$arrPrintColors[14] = array("printColorName" => "unbedruckt", "printColorValue" => "unbedruckt", "printColorCode" => "");
		$arrPrintColors[15] = array("printColorName" => "mehrfarbig", "printColorValue" => "mehrfarbig", "printColorCode" => "");
		$arrPrintColors[16] = array("printColorName" => "sonstige", "printColorValue" => "sonstige", "printColorCode" => "");
		$arrPrintColors[18] = array("printColorName" => "gelasert", "printColorValue" => "gelasert", "printColorCode" => "");
		$arrPrintColors[20] = array("printColorName" => "geprägt", "printColorValue" => "geprägt", "printColorCode" => "");
		return $arrPrintColors;
	}

	// ---------------------------------
	function getSupplierGroups() {
		/*
		global $dbConnection;
		$sql = "SELECT
					`supplierGroupsID`,
					`customerGroupsName`

					FROM `" . TABLE_SUPPLIER_GROUPS . "`

					WHERE 1

					ORDER BY `supplierGroupsID`
		";

		$rs = $dbConnection->db_query($sql);

		while($ds = mysqli_fetch_assoc($rs)) {
			foreach(array_keys($ds) as $field) {
				$arrCustomerGroupDatas[$ds["supplierGroupsID"]][$field] = $ds[$field];
			}
		}
		*/
		return $arrSupplierGroupDatas;
	}
	// ---------------------------------
	function getPersonnelTypes() {
		global $dbConnection;
		$sql = "SELECT
					`personnelTypesID`,
					`personnelTypesShortName`,
					`personnelTypesName`

					FROM `" . TABLE_PERSONNEL_TYPES . "`

					WHERE 1

					ORDER BY `personnelTypesName`
		";

		$rs = $dbConnection->db_query($sql);

		while($ds = mysqli_fetch_assoc($rs)) {
			foreach(array_keys($ds) as $field) {
				$arrPersonnelTypeDatas[$ds["personnelTypesID"]][$field] = $ds[$field];
			}
		}
		return $arrPersonnelTypeDatas;
	}
	 // ---------------------------------

	function getSupplierTypes() {
		global $dbConnection;
		$sql = "SELECT
					`supplierTypesID`,
					`supplierTypesShortName`,
					`supplierTypesName`

					FROM `" . TABLE_SUPPLIER_TYPES . "`

					WHERE 1

					ORDER BY `supplierTypesShortName`
		";

		$rs = $dbConnection->db_query($sql);

		while($ds = mysqli_fetch_assoc($rs)) {
			foreach(array_keys($ds) as $field) {
				$arrSupplierTypeDatas[$ds["supplierTypesID"]][$field] = $ds[$field];
			}
		}
		return $arrSupplierTypeDatas;
	}

	// ---------------------------------

	function getSuppliers() {
		global $dbConnection;
		$sql = "SELECT
					`" . TABLE_SUPPLIERS . "`.`suppliersID`,
					`" . TABLE_SUPPLIERS . "`.`suppliersKundennummer`,
					`" . TABLE_SUPPLIERS . "`.`suppliersFirmenname`,
					`" . TABLE_SUPPLIERS . "`.`suppliersTyp`,
					`" . TABLE_SUPPLIER_TYPES . "`.`supplierTypesName`,
					`" . TABLE_SUPPLIERS . "`.`suppliersFirmaArtikelnummerKurzform`

					FROM `" . TABLE_SUPPLIERS . "`

					LEFT JOIN `" . TABLE_SUPPLIER_TYPES . "`
					ON(`" . TABLE_SUPPLIERS . "`.`suppliersTyp` = `" . TABLE_SUPPLIER_TYPES . "`.`supplierTypesID`)

					WHERE 1

					ORDER BY `suppliersFirmenname`
		";

		$rs = $dbConnection->db_query($sql);

		while($ds = mysqli_fetch_assoc($rs)) {
			foreach(array_keys($ds) as $field) {
				$arrSupplierDatas[$ds["suppliersID"]][$field] = $ds[$field];
			}
		}
		return $arrSupplierDatas;
	}

	// ---------------------------------

	function getProductSuppliers() {
		global $dbConnection;
		$sql = "SELECT
					`" . TABLE_SUPPLIERS . "`.`suppliersID`,
					`" . TABLE_SUPPLIERS . "`.`suppliersFirmenname`,
					`" . TABLE_SUPPLIERS . "`.`suppliersTyp`,
					`" . TABLE_SUPPLIER_TYPES . "`.`supplierTypesName`,
					`" . TABLE_SUPPLIERS . "`.`suppliersFirmaArtikelnummerKurzform`

					FROM `" . TABLE_SUPPLIERS . "`

					LEFT JOIN `" . TABLE_SUPPLIER_TYPES . "`
					ON(`" . TABLE_SUPPLIERS . "`.`suppliersTyp` = `" . TABLE_SUPPLIER_TYPES . "`.`supplierTypesID`)

					WHERE 1
						AND `" . TABLE_SUPPLIERS . "`.`suppliersFirmaArtikelnummerKurzform` != ''

					ORDER BY `suppliersFirmenname`
		";

		$rs = $dbConnection->db_query($sql);

		while($ds = mysqli_fetch_assoc($rs)) {
			foreach(array_keys($ds) as $field) {
				$arrProductSupplierDatas[$ds["suppliersFirmaArtikelnummerKurzform"]][$field] = $ds[$field];
			}
		}
		return $arrProductSupplierDatas;
	}
	// ---------------------------------

	function getCustomerGroupsProducts($groupID=0, $mode='products') {
		global $dbConnection, $_COOKIE;

		$whereAdd = "";
		if($mode == 'products'){
			$whereAdd = " AND `productsCategoriesID` != '' ";
		}
		else if($mode == 'all'){
			$whereAdd = "";
		}
		else if($mode == 'non-products'){
			$whereAdd = " AND `productsCategoriesID` = '' ";
		}

		$arrCustomerGroupsProducts = array();
		$sql_getCustomerGroupProducts = "
				SELECT
					`customerGroupsProductNumbersID`,
					`customerGroupsProductNumbersGroupID`,
					`customerGroupsProductNumber`,
					`customerGroupsProductNumber2`,
					`productsModelNumber`,
					`productsCategoriesID`

				FROM `" . TABLE_CUSTOMER_GROUPS_PRODUCTS . "`

				WHERE 1
					AND `customerGroupsProductNumbersGroupID` = " . $groupID . "
					" . $whereAdd . "

		";
		$rs_getCustomerGroupProducts = $dbConnection->db_query($sql_getCustomerGroupProducts);

		$arrCustomerGroupProductNumbers = array();
		$arrCustomerGroupProductParentCategoryIDs = array();
		while($ds_getCustomerGroupProducts = mysqli_fetch_assoc($rs_getCustomerGroupProducts)){
			$arrCustomerGroupProductNumbers[$ds_getCustomerGroupProducts["productsModelNumber"]] = $ds_getCustomerGroupProducts["customerGroupsProductNumber"];

			$thisParentCategoryIDs = substr($ds_getCustomerGroupProducts["productsCategoriesID"], 0, 3);
			$arrCustomerGroupProductParentCategoryIDs[] = $thisParentCategoryIDs;
		}

		$arrCustomerGroupProductParentCategoryIDs = array_unique($arrCustomerGroupProductParentCategoryIDs);

		$arrCustomerGroupsProducts = array(
			"arrCustomerGroupProductNumbers" => $arrCustomerGroupProductNumbers,
			"arrCustomerGroupProductParentCategoryIDs" => $arrCustomerGroupProductParentCategoryIDs
		);
		return $arrCustomerGroupsProducts;
	}
	// ---------------------------------

	function getOrderProducts($catID='all', $mandator='', $groupID='0') {

		global $dbConnection, $_COOKIE;

		if($mandator == '') {
			$mandator = $_COOKIE["mandator"];
		}

		$sql_addFields = "";
		$sql_join = "";

		if($catID == 'all'){
			$where = "";
		}
		else {
			$where = " AND `categories`.`stockProductCategoriesLevelID` = '" . $catID . "' ";
		}

		// $sql_addFields .= ", `products`.`stockProductCategoriesProductNumber_" . strtolower($mandator) . "` AS `productsProductNumberUse` ";
		$sql_addFields .= ", `products`.`stockProductCategoriesProductNumber` AS `productsProductNumberUse` ";

		if($groupID > 0){
			$sql_addFields .= ", `customerGroupsProductNumber` AS `productsGroupsProductNumber` ";

			$sql_join = "
						LEFT JOIN `common_customerGroupsProducts`
						ON(
							`products`.`stockProductCategoriesProductNumber` = `common_customerGroupsProducts`.`productsModelNumber`
							AND `common_customerGroupsProducts`.`customerGroupsProductNumbersGroupID` = '" . $groupID . "'
						)
				";

		}

		$sql_getProducts = "
				SELECT
					`categories`.`stockProductCategoriesID` AS `categoriesID`,
					`categories`.`stockProductCategoriesLevelID` AS `categoriesLevelID`,
					`categories`.`stockProductCategoriesParentID` AS `categoriesParentID`,
					`categories`.`stockProductCategoriesRelationID` AS `categoriesRelationID`,
					`categories`.`stockProductCategoriesName` AS `categoriesName`,
					`categories`.`stockProductCategoriesText` AS `categoriesText`,
					`categories`.`stockProductCategoriesShortName` AS `categoriesShortName`,
					`categories`.`stockProductCategoriesProductNumber` AS `categoriesProductNumber`,
					`categories`.`stockProductCategoriesProductNumber_bctr` AS `categoriesProductNumber_bctr`,
					`categories`.`stockProductCategoriesProductNumber_b3` AS `categoriesProductNumber_b3`,
					`categories`.`stockProductCategoriesSort` AS `categoriesSort`,
					`categories`.`stockProductCategoriesActive` AS `categoriesActive`,
					`categories`.`stockProductCategoriesTempID` AS `categoriesTempID`,
					`categories`.`stockProductCategoriesGewischtProMenge` AS `categoriesGewischtProMenge`,

					`products`.`stockProductCategoriesID` AS `productsID`,
					`products`.`stockProductCategoriesLevelID` AS `productsLevelID`,
					`products`.`stockProductCategoriesParentID` AS `productsParentID`,
					`products`.`stockProductCategoriesRelationID` AS `productsRelationID`,
					`products`.`stockProductCategoriesName` AS `productsName`,
					`products`.`stockProductCategoriesText` AS `productsText`,
					`products`.`stockProductCategoriesShortName` AS `productsShortName`,
					`products`.`stockProductCategoriesProductNumber` AS `productsProductNumber`,
					`products`.`stockProductCategoriesProductNumber_bctr` AS `productsProductNumber_bctr`,
					`products`.`stockProductCategoriesProductNumber_b3` AS `productsProductNumber_b3`,
					`products`.`stockProductCategoriesSort` AS `productsSort`,
					`products`.`stockProductCategoriesActive` AS `productsActive`,
					`products`.`stockProductCategoriesTempID` AS `productsTempID`,
					`products`.`productweight` AS `productsGewicht`

					" . $sql_addFields . "

				FROM `" . TABLE_STOCK_PRODUCT_CATEGORIES . "` AS `categories`

				LEFT JOIN `" . TABLE_STOCK_PRODUCT_CATEGORIES . "` AS `products`
				ON(`categories`.`stockProductCategoriesLevelID` = `products`.`stockProductCategoriesParentID`)
				
				LEFT JOIN `" . TABLE_STOCK_PRODUCT_DETAIL_DATA . "` AS `products_detail`
								ON(`products_detail`.`stockProductDetailDataProductID` = `products`.`stockProductCategoriesID`)

				" . $sql_join . "

				WHERE 1
					AND `categories`.`stockProductCategoriesParentID` = '000'
					AND `categories`.`stockProductCategoriesActive` = '1'
					AND `products`.`stockProductCategoriesActive` = '1'
					#AND `products_detail`.`stockProductDetailDataOptionID` = '14'

					" . $where . "

				ORDER BY
					`categories`.`stockProductCategoriesSort` ASC,
					`categories`.`stockProductCategoriesName` ASC,
					`products`.`stockProductCategoriesSort` ASC,
					`products`.`stockProductCategoriesName` ASC

			";
// echo $sql_getProducts;exit();
		$rs_getProducts = $dbConnection->db_query($sql_getProducts);

		$arrProductsData = array();

		while($ds_getProducts = mysqli_fetch_assoc($rs_getProducts)){
			foreach(array_keys($ds_getProducts) as $field){
				if(preg_match("/^categories/", $field)){
					$arrProductsData[$ds_getProducts["categoriesLevelID"]][$field] = $ds_getProducts[$field];
				}
				if(preg_match("/^products/", $field)){
					$arrProductsData[$ds_getProducts["categoriesLevelID"]]["products"][$ds_getProducts["productsLevelID"]][$field] = $ds_getProducts[$field];
				}

				if($groupID > 0){
					if($ds_getProducts["productsGroupsProductNumber"] != ""){
						$arrProductsData[$ds_getProducts["categoriesLevelID"]]["products"][$ds_getProducts["productsLevelID"]]["productsName"] = $ds_getProducts["productsGroupsProductNumber"] . " - " . $ds_getProducts["productsName"];
					}
				}
			}
		}
		// print_r($arrProductsData);exit();
		return $arrProductsData;
	}

	function getOrderCategories($catID='all', $mandator='') {
		global $dbConnection, $_COOKIE;

		$useInStock = '0';

		// BOF REMOVE PARENT PRODUCT IF ATTRIBUTED PRODUCT EXISTS
			$removeParentProductIfAttributedProductExists = true; // true | false
		// EOF REMOVE PARENT PRODUCT IF ATTRIBUTED PRODUCT EXISTS

		if($mandator == '') {
			$mandator = $_COOKIE["mandator"];
		}

		// $useOnlyProductsFromBCTR = true; // true | false
		$useOnlyProductsFromBCTR = USE_ONLY_PRODUCTS_FROM_BCTR;

		if($catID == 'all'){
			$where = "";
		}
		else {
			$where = " AND `" . TABLE_PRODUCTS . "`.`orderCategoriesParentID` = '" . $catID . "' ";
		}

		$orderBy = "ORDER BY
						`" . TABLE_PRODUCTS . "`.`orderCategoriesSort`
						, `" . TABLE_PRODUCTS . "`.`orderCategoriesLevelID`
						, `parentProducts`.`orderCategoriesName`

						, `" . TABLE_PRODUCTS . "`.`orderCategoriesSort2`
						, `" . TABLE_PRODUCTS . "`.`orderCategoriesName`
			";

		$sql = "SELECT
					`" . TABLE_PRODUCTS . "`.`orderCategoriesActive`,
					`" . TABLE_PRODUCTS . "`.`orderCategoriesID`,
					`" . TABLE_PRODUCTS . "`.`orderCategoriesParentID`,

					`" . TABLE_PRODUCTS . "`.`orderCategoriesLevelID`,
					`" . TABLE_PRODUCTS . "`.`orderCategoriesRelationID`,

					`" . TABLE_PRODUCTS . "`.`orderCategoriesShortName`,
					`" . TABLE_PRODUCTS . "`.`orderCategoriesName`,
					`" . TABLE_PRODUCTS . "`.`orderCategoriesPartNumber`,
					`" . TABLE_PRODUCTS . "`.`orderCategoriesName_bctr`,
					`" . TABLE_PRODUCTS . "`.`orderCategoriesPartNumber_bctr`,
					`" . TABLE_PRODUCTS . "`.`orderCategoriesName_b3`,
					`" . TABLE_PRODUCTS . "`.`orderCategoriesPartNumber_b3`,
					`" . TABLE_PRODUCTS . "`.`orderCategoriesSort`,
					IF(LENGTH(`" . TABLE_PRODUCTS . "`.`orderCategoriesID`) > 3, 'level_2', 'level_1') AS `orderCategoriesClass`,

					`" . TABLE_PRODUCTS . "`.`orderCategoriesUseInStock`,

					`parentProducts`.`orderCategoriesID` AS `parentProductCategoriesID`,
					`parentProducts`.`orderCategoriesParentID` AS `parentProductCategoriesParentID`,
					`parentProducts`.`orderCategoriesLevelID` AS `parentProductCategoriesLevelID`,
					`parentProducts`.`orderCategoriesRelationID` AS `parentProductCategoriesRelationID`,

					SUBSTRING(`parentProducts`.`orderCategoriesRelationID`, 1, 3) AS `parentProductCategoriesRelationParentID`,

					`parentProducts`.`orderCategoriesShortName` AS `parentProductCategoriesShortName`,
					`parentProducts`.`orderCategoriesName` AS `parentProductCategoriesName`,
					`parentProducts`.`orderCategoriesPartNumber` AS `parentProductCategoriesPartNumber`,
					`parentProducts`.`orderCategoriesName_bctr` AS `parentProductCategoriesName_bctr`,
					`parentProducts`.`orderCategoriesPartNumber_bctr` AS `parentProductCategoriesPartNumber_bctr`,
					`parentProducts`.`orderCategoriesName_b3` AS `parentProductCategoriesName_b3`,
					`parentProducts`.`orderCategoriesPartNumber_b3` AS `parentProductCategoriesPartNumber_b3`,
					`parentProducts`.`orderCategoriesSort` AS `parentProductCategoriesSort`,
					`parentProducts`.`orderCategoriesActive` AS `parentProductCategoriesActive`,
					`parentProducts`.`orderCategoriesUseInStock` AS `parentProductCategoriesUseInStock`,

					'Auftragslisten' AS `productSource`


					FROM `" . TABLE_ORDER_CATEGORIES . "` AS `" . TABLE_PRODUCTS . "`

					LEFT JOIN `" . TABLE_ORDER_CATEGORIES . "` AS `parentProducts`
					ON(`" . TABLE_PRODUCTS . "`.`orderCategoriesRelationID` = `parentProducts`.`orderCategoriesID`)

					WHERE 1
						AND `" . TABLE_PRODUCTS . "`.`orderCategoriesActive` = '1'
						" . $where . "
						" . $orderBy . "

			";

		// ORDER BY SUBSTRING(`orderCategoriesID`, 1, 3), `orderCategoriesName`

		$rs = $dbConnection->db_query($sql);

		$arrMainCategories = array();
		$arrMainToSubCategories = array();
		while($ds = mysqli_fetch_assoc($rs)) {
			foreach(array_keys($ds) as $field) {
				$arrOrderCategoriesDatas[$ds["orderCategoriesID"]][$field] = $ds[$field];
			}
			if(strlen($ds["orderCategoriesID"]) == 6){
				$arrMainToSubCategories[substr($ds["orderCategoriesID"], 0, 3)][] = $ds["orderCategoriesID"];
			}
			else if(strlen($ds["orderCategoriesID"]) == 3){
				$arrMainCategories[] = $ds["orderCategoriesID"];
			}
		}
		#if($catID == 'all' && $_COOKIE["isAdmin"] == '1'){
		if($catID == 'all'){
			if($_COOKIE["isAdmin"] == '1'){
				#echo 'con:' . $dbOpen_ExternShop;
				#echo '<pre style="background-color:#FFF;">';
				#print_r($arrOrderCategoriesDatas);
				#echo '</pre>';
			}
			$arrMainWithoutSubCategories = array();

			if(($_COOKIE["mandator"] == "bctr" && $mandator == $_COOKIE["mandator"]) || $useOnlyProductsFromBCTR == true){
				if(!empty($arrMainCategories)){
					foreach($arrMainCategories as $thisMainCategories){
						if(empty($arrMainToSubCategories[$thisMainCategories])){
							$arrMainWithoutSubCategories[] = $thisMainCategories;
						}
					}
				}
			}

			if(!empty($arrMainWithoutSubCategories)){
				if(LOAD_EXTERNAL_SHOP_DATAS){
					$dbConnection_ExternShop = new DB_Connection(DB_HOST_EXTERN_SHOP, '', DB_NAME_EXTERN_SHOP, DB_USER_EXTERN_SHOP, DB_PASSWORD_EXTERN_SHOP);
					$dbOpen_ExternShop = $dbConnection_ExternShop->db_connect();
				}
				else {
					$dbConnection_ExternShop = $dbConnection;
				}

				$sql_extern = '';
				$arrSql_extern = array();

				$arrSql_extern[] = "
					SELECT
							'1' AS `orderCategoriesActive`,
							CONCAT(`products_group`, 'xxx') AS `orderCategoriesID`,
							CONCAT(`products_group`, `" . TABLE_PRODUCTS . "`.`products_id`) AS `orderProductsID`,
							`products_group` AS `orderCategoriesParentID`,

							`products_group` AS `orderCategoriesLevelID`,
							`products_group` AS `orderCategoriesRelationID`,

							'' AS `orderCategoriesShortName`,
							`products_name` AS `orderCategoriesName`,
							`products_model` AS `orderCategoriesPartNumber`,
							`products_name` AS `orderCategoriesName_bctr`,
							`products_model` AS `orderCategoriesPartNumber_bctr`,
							`products_name` AS `orderCategoriesName_b3`,
							`products_model` AS `orderCategoriesPartNumber_b3`,
							'0' AS `orderCategoriesSort`,
							'level_2' AS `orderCategoriesClass`,

							'" . $useInStock . "' AS `orderCategoriesUseInStock`,

							'' AS `parentProductCategoriesID`,
							'' AS `parentProductCategoriesParentID`,
							'' AS `parentProductCategoriesLevelID`,
							'' AS `parentProductCategoriesRelationID`,

							CONCAT(`products_group`, 'yyy') AS `parentProductCategoriesRelationParentID`,

							'' AS `parentProductCategoriesShortName`,

							`" . TABLE_PRODUCTS_DESCRIPTION . "`.`products_name` AS `parentProductCategoriesName`,
							`" . TABLE_PRODUCTS . "`.`products_model` AS `parentProductCategoriesPartNumber`,

							'' AS `parentProductCategoriesName_bctr`,
							'' AS `parentProductCategoriesPartNumber_bctr`,
							'' AS `parentProductCategoriesName_b3`,
							'' AS `parentProductCategoriesPartNumber_b3`,
							'' AS `parentProductCategoriesSort`,
							'' AS `parentProductCategoriesActive`,
							'" . $useInStock . "' AS `parentProductCategoriesUseInStock`,

							'SHOP' AS `productSource`

						FROM `" . TABLE_PRODUCTS . "`
						LEFT JOIN `" . TABLE_PRODUCTS_DESCRIPTION . "`
						ON(`" . TABLE_PRODUCTS . "`.`products_id` = `" . TABLE_PRODUCTS_DESCRIPTION . "`.`products_id`)
							WHERE 1
								AND (
									`products_group` = '" . implode("' OR `products_group` = '", $arrMainWithoutSubCategories) . "'
								)
								AND `products_status` = '1'
						/* ORDER BY `products_name` */
					";

				$arrSql_extern[] = "
					SELECT
							'1' AS `orderCategoriesActive`,
							CONCAT(`products_group`, 'xxx') AS `orderCategoriesID`,
							CONCAT(`products_group`, `" . TABLE_PRODUCTS . "`.`products_id`, '_', IF(`" . TABLE_PRODUCTS_OPTIONS_VALUES . "`.`products_options_values_id` IS NULL, 'x', `" . TABLE_PRODUCTS_OPTIONS_VALUES . "`.`products_options_values_id`)) AS `orderProductsID`,

							`products_group` AS `orderCategoriesParentID`,

							`products_group` AS `orderCategoriesLevelID`,
							`products_group` AS `orderCategoriesRelationID`,

							'' AS `orderCategoriesShortName`,
							CONCAT(
								`" . TABLE_PRODUCTS_DESCRIPTION . "`.`products_name`,
								': ',
								`" . TABLE_PRODUCTS_OPTIONS . "`.`products_options_name`,
								' ',
								IF(`" . TABLE_PRODUCTS_OPTIONS_VALUES . "`.`products_options_values_name` IS NULL, '', `" . TABLE_PRODUCTS_OPTIONS_VALUES . "`.`products_options_values_name`)
							) AS `orderCategoriesName`,
							`" . TABLE_PRODUCTS_ATTRIBUTES . "`.`attributes_model` AS `orderCategoriesPartNumber`,
							CONCAT(
								`" . TABLE_PRODUCTS_DESCRIPTION . "`.`products_name`,
								': ',
								`" . TABLE_PRODUCTS_OPTIONS . "`.`products_options_name`,
								' ',
								IF(`" . TABLE_PRODUCTS_OPTIONS_VALUES . "`.`products_options_values_name` IS NULL, '', `" . TABLE_PRODUCTS_OPTIONS_VALUES . "`.`products_options_values_name`)
							) AS `orderCategoriesName_bctr`,
							`" . TABLE_PRODUCTS_ATTRIBUTES . "`.`attributes_model` AS `orderCategoriesPartNumber_bctr`,
							CONCAT(
								`" . TABLE_PRODUCTS_DESCRIPTION . "`.`products_name`,
								': ',
								`" . TABLE_PRODUCTS_OPTIONS . "`.`products_options_name`,
								' ',
								IF(`" . TABLE_PRODUCTS_OPTIONS_VALUES . "`.`products_options_values_name` IS NULL, '', `" . TABLE_PRODUCTS_OPTIONS_VALUES . "`.`products_options_values_name`)
							) AS `orderCategoriesName_b3`,
							`" . TABLE_PRODUCTS_ATTRIBUTES . "`.`attributes_model` AS `orderCategoriesPartNumber_b3`,
							'0' AS `orderCategoriesSort`,
							'level_2' AS `orderCategoriesClass`,

							'" . $useInStock . "' AS `orderCategoriesUseInStock`,

							'' AS `parentProductCategoriesID`,
							'' AS `parentProductCategoriesParentID`,
							'' AS `parentProductCategoriesLevelID`,
							'' AS `parentProductCategoriesRelationID`,

							CONCAT(`products_group`, 'yyy') AS `parentProductCategoriesRelationParentID`,

							'' AS `parentProductCategoriesShortName`,
							`" . TABLE_PRODUCTS_DESCRIPTION . "`.`products_name` AS `parentProductCategoriesName`,
							`" . TABLE_PRODUCTS . "`.`products_model` AS `parentProductCategoriesPartNumber`,
							'' AS `parentProductCategoriesName_bctr`,
							'' AS `parentProductCategoriesPartNumber_bctr`,
							'' AS `parentProductCategoriesName_b3`,
							'' AS `parentProductCategoriesPartNumber_b3`,
							'' AS `parentProductCategoriesSort`,
							'' AS `parentProductCategoriesActive`,
							'" . $useInStock . "' AS `parentProductCategoriesUseInStock`,

							'SHOP' AS `productSource`

						FROM `" . TABLE_PRODUCTS_ATTRIBUTES . "`

						LEFT JOIN `" . TABLE_PRODUCTS . "`
						ON(`" . TABLE_PRODUCTS_ATTRIBUTES . "`.`products_id` = `" . TABLE_PRODUCTS . "`.`products_id`)

						LEFT JOIN `" . TABLE_PRODUCTS_DESCRIPTION . "`
						ON(`" . TABLE_PRODUCTS_ATTRIBUTES . "`.`products_id` = `" . TABLE_PRODUCTS_DESCRIPTION . "`.`products_id`)

						LEFT JOIN `" . TABLE_PRODUCTS_OPTIONS . "`
						ON(`" . TABLE_PRODUCTS_ATTRIBUTES . "`.`options_id` = `" . TABLE_PRODUCTS_OPTIONS . "`.`products_options_id`)

						LEFT JOIN `" . TABLE_PRODUCTS_OPTIONS_VALUES . "`
						ON(`" . TABLE_PRODUCTS_ATTRIBUTES . "`.`options_values_id` = `" . TABLE_PRODUCTS_OPTIONS_VALUES . "`.`products_options_values_id`)

						WHERE 1
							AND (
								`products_group` = '" . implode("' OR `products_group` = '", $arrMainWithoutSubCategories) . "'
							)
							AND `products_status` = '1'
							AND `" . TABLE_PRODUCTS_ATTRIBUTES . "`.`attributes_model` != ''

					";

				$sql_extern = "
						SELECT
							*
						FROM (" . implode(' UNION ', $arrSql_extern) . ") AS `tempTable`
						ORDER BY `orderCategoriesName`
					";

if($useOnlyProductsFromBCTR == true){
	$sql_extern = preg_replace("/`b3_/", "`bctr_", $sql_extern);
}

				$rs_extern = $dbConnection_ExternShop->db_query($sql_extern);

				$count = 0;
				$arrAddMissingSubCategories = array();
				$arrRelationProductNumberCategoryID = array();
				while($ds_extern = mysqli_fetch_assoc($rs_extern)){
					$ds_extern["orderCategoriesShortName"] = $arrOrderCategoriesDatas[$ds_extern["orderCategoriesParentID"]]["orderCategoriesShortName"];

					// BOF USE COUNTER FOR orderCategoriesID
						// $ds_extern["orderCategoriesID"] = str_replace("xxx", str_repeat("0", (3-strlen($count))) . $count, $ds_extern["orderCategoriesID"]);
					// EOF USE COUNTER FOR orderCategoriesID

					// BOF USE products_id FOR orderCategoriesID
						if(1 || $_COOKIE["isAdmin"] == '1'){
							$ds_extern["orderCategoriesID"] = $ds_extern["orderProductsID"];
						}
					// EOF USE products_id FOR orderCategoriesID

					$arrAddMissingSubCategories[$ds_extern["orderCategoriesParentID"]][$ds_extern["orderCategoriesID"]] = $ds_extern;
					$count++;
					if(!isset($arrRelationProductNumberCategoryID[$ds_extern["parentProductCategoriesPartNumber"]])){
						$arrRelationProductNumberCategoryID[$ds_extern["parentProductCategoriesPartNumber"]] = $ds_extern["orderCategoriesID"];
					}

				}

				$arrTemp = array();
				foreach($arrOrderCategoriesDatas as $thisOrderCategoriesDatasKey => $thisOrderCategoriesDatasValue){
					$arrTemp[$thisOrderCategoriesDatasKey] = $thisOrderCategoriesDatasValue;
					if(strlen($thisOrderCategoriesDatasKey) == 3){
						if(!empty($arrAddMissingSubCategories[$thisOrderCategoriesDatasKey])){
							foreach($arrAddMissingSubCategories[$thisOrderCategoriesDatasKey] as $arrThisMissingSubCategory){
								$arrTemp[$arrThisMissingSubCategory["orderCategoriesID"]] = $arrThisMissingSubCategory;
								$arrTemp[$arrThisMissingSubCategory["orderCategoriesID"]]["parentProductCategoriesRelationParentID"] = $arrRelationProductNumberCategoryID[$arrTemp[$arrThisMissingSubCategory["orderCategoriesID"]]["parentProductCategoriesPartNumber"]];
							}
						}
					}
				}
				if(!empty($arrTemp)){
					$arrOrderCategoriesDatas = $arrTemp;
				}
				if(LOAD_EXTERNAL_SHOP_DATAS){
					if($dbConnection_ExternShop) {
						$dbConnection_ExternShop->db_close($dbOpen_ExternShop);
					}
				}
			}
		}
		if($_COOKIE["isAdmin"] == '1'){
			#echo 'con:' . $dbOpen_ExternShop;
			#echo '<pre style="background-color:#FFF;">';
			#print_r($arrOrderCategoriesDatas);
			#echo $sql_extern;
			#echo '</pre>';
		}

		// BOF REMOVE PARENT PRODUCT IF ATTRIBUTED PRODUCT EXISTS
			if($removeParentProductIfAttributedProductExists){
				if(!empty($arrOrderCategoriesDatas)){
					foreach($arrOrderCategoriesDatas as $thisCategoriesKey => $thisCategoriesValue){
						if(preg_match("/_/", $thisCategoriesKey)){
							$thisCategoriesKeyParentKey = preg_replace("/_.*$/", "", $thisCategoriesKey);
							if(isset($arrOrderCategoriesDatas[$thisCategoriesKeyParentKey])){
								unset($arrOrderCategoriesDatas[$thisCategoriesKeyParentKey]);
							}
						}
					}
				}
			}
		// EOF REMOVE PARENT PRODUCT IF ATTRIBUTED PRODUCT EXISTS

		return $arrOrderCategoriesDatas;
	}

	// ---------------------------------
	// ---------------------------------

	function getAdditionalCosts() {
		global $dbConnection;
		$sql = "SELECT
					`additionalCostsID`,
					`additionalCostsShortName`,
					`additionalCostsName`,
					`additionalCostsPrice`

					FROM `" . TABLE_ADDITIONAL_COSTS . "`

					WHERE 1

					ORDER BY `additionalCostsID`
		";

		$rs = $dbConnection->db_query($sql);

		while($ds = mysqli_fetch_assoc($rs)) {
			foreach(array_keys($ds) as $field) {
				$arrAdditionalCostsDatas[$ds["additionalCostsID"]][$field] = $ds[$field];
			}
		}
		return $arrAdditionalCostsDatas;
	}
	// ---------------------------------

	function getPrintTypes() {
		global $dbConnection;
		$sql = "SELECT
					`printTypesID`,
					`printTypesShortName`,
					`printTypesName`,
					`printTypesSort`,
					`printTypesActive`
					`printTypesCostsPrice`,
					`printTypesCostsDeliveryMaxTime`

					FROM `" . TABLE_PRINT_TYPES . "`

					WHERE 1

					ORDER BY `printTypesSort`
		";

		$rs = $dbConnection->db_query($sql);

		while($ds = mysqli_fetch_assoc($rs)) {
			foreach(array_keys($ds) as $field) {
				$arrPrintTypesDatas[$ds["printTypesID"]][$field] = $ds[$field];
			}
		}
		return $arrPrintTypesDatas;
	}

	// ---------------------------------

	function getBankAccountTypes($mode='') {
		global $dbConnection;

		$where = "";
		if($mode != 'all'){
			$where .= " AND `bankAccountTypesActive` = '1' ";
		}

		$sql = "SELECT
					`bankAccountTypesID`,
					`bankAccountTypesShortName`,
					`bankAccountTypesName`,
					`bankAccountTypesAccountNumber`,
					`bankAccountTypesBankCode`,
					`bankAccountTypesIBAN`,
					`bankAccountTypesBIC`,
					`bankAccountTypesShowWarning`,
					`bankAccountTypesActive`,
					`bankAccountTypesSort`,
					`bankAccountTypesDefault`,
					`bankAccountTypesNotice`

					FROM `" . TABLE_BANK_ACCOUNT_TYPES . "`

					WHERE 1
						" . $where . "

					ORDER BY `bankAccountTypesName`
		";

		$rs = $dbConnection->db_query($sql);

		while($ds = mysqli_fetch_assoc($rs)) {
			foreach(array_keys($ds) as $field) {
				$arrBankAccountTypeDatas[$ds["bankAccountTypesShortName"]][$field] = $ds[$field];
			}
		}
		return $arrBankAccountTypeDatas;
	}

	// ---------------------------------
	function getOrdersSourceTypes() {
		global $dbConnection;
		$sql = "SELECT
					`ordersSourceTypesID`,
					`ordersSourceTypesShortName`,
					`ordersSourceTypesName`

					FROM `" . TABLE_ORDERS_SOURCE_TYPES . "`

					WHERE 1

					ORDER BY `ordersSourceTypesID`
		";

		$rs = $dbConnection->db_query($sql);

		while($ds = mysqli_fetch_assoc($rs)) {
			foreach(array_keys($ds) as $field) {
				$arrOrdersSourceTypeDatas[$ds["ordersSourceTypesID"]][$field] = $ds[$field];
			}
		}
		return $arrOrdersSourceTypeDatas;
	}
	// ---------------------------------
	function getDocumentTypes() {
		global $dbConnection;

		$sql = "SELECT
					`createdDocumentsTypesID`,
					`createdDocumentsTypesName`,
					`createdDocumentsTypesShortName`,
					`createdDocumentsTypesName2`,
					`createdDocumentsTypesName3`

					FROM `" . TABLE_CREATED_DOCUMENTS_TYPES . "`
					WHERE 1

					ORDER BY `createdDocumentsSort`
		";

		$rs = $dbConnection->db_query($sql);

		while($ds = mysqli_fetch_assoc($rs)) {
			foreach(array_keys($ds) as $field) {
				$arrDocumentTypeDatas[$ds["createdDocumentsTypesShortName"]][$field] = $ds[$field];
			}
		}

		return $arrDocumentTypeDatas;
	}

	// ---------------------------------
	function generateDocumentNumber($basicNumber, $documentType, $date, $table='') {
		global $dbConnection;

		if($table != '') {
			$prefix = $documentType . '-' . $date;

			$sql = "
				SELECT
					(MAX(SUBSTRING(`orderDocumentsNumber`," . (strlen($prefix) + 1) . ",LENGTH(`orderDocumentsNumber`))) + 1) AS `searchResult`
					FROM  `" . $table . "`

					WHERE 1
						AND `orderDocumentsNumber` LIKE '" . substr($prefix, 0, -2) . "%'
				";

			$rs = $dbConnection->db_query($sql);
			list($searchResult) = mysqli_fetch_array($rs);

			if($searchResult == null || $searchResult == "" || $searchResult == "0") { $searchResult = 1;}

			$searchResult = str_repeat(0, (DOCUMENT_NUMBER_LENGTH - strlen($searchResult))).$searchResult;
			$searchResult = $documentType.'-'.$date.$searchResult;
		}

		// BOF DOKUMENT NUMBERS TEST
			if($_COOKIE["isAdmin"] == '1'){
				$arrTestDocumentNumbers = array(
					"AN" => "AN-0000000001",
					"AB" => "AB-0000000001",
					#"AB" => "AB-1701000592",
					"RE" => "RE-0000000001",
					"LS" => "LS-0000000001",
					"MA" => "MA-0000000001",
					"M1" => "M1-0000000001",
					"M2" => "M2-0000000001",
					"M3" => "M3-0000000001"
				);
				// $searchResult = $arrTestDocumentNumbers[$documentType];
			}
		// EOF DOKUMENT NUMBERS TEST

		return $searchResult;
	}
	// ---------------------------------
	function checkFormDutyFields($arrFormDutyFields) {
		$doAction = true;

		if(!empty($arrFormDutyFields)) {
			foreach ($arrFormDutyFields as $thisFormDutyFieldKey => $thisFormDutyFieldValue) {
				$thisFormDutyFieldKey = preg_replace("/\[\]/", "", $thisFormDutyFieldKey);
				$thisFormDutyFieldType = gettype($_POST[$thisFormDutyFieldKey]);

				if($thisFormDutyFieldType == "array") {
					if(empty($_POST[$thisFormDutyFieldKey])) {
						$doAction = false;
						break;
					}
				}
				else if($thisFormDutyFieldType == "string") {
					if(trim($_POST[$thisFormDutyFieldKey]) == "") {
						$doAction = false;
						break;
					}
				}
			}
		}
		return $doAction;
		}
	// ---------------------------------
	function createJson($arrData) {
		$arrJSON = array();
		$strJSON = "";
		foreach($arrData as $thisKey => $thisValue) {
			$arrJSON[] = "{'fieldName': '" . $thisKey . "', 'fieldLabel': '" . $thisValue . "'}";
		}
		if(!empty($arrJSON)) {
			$strJSON = "[" . implode(", ", $arrJSON) . "]";
		}
		return $strJSON;
	}
	function createJson2($arrData) {
		$arrJSON = array();
		$strJSON = "";
		foreach($arrData as $thisKey => $thisValue) {
			if(!is_array($thisValue)){
				$arrJSON[] = "{'fieldName': '" . $thisKey . "', 'fieldLabel': '" . $thisValue . "'}";
			}
			else {
				createJson2($thisValue);
			}
		}
		if(!empty($arrJSON)) {
			$strJSON = "[" . implode(", ", $arrJSON) . "]";
		}
		return $strJSON;
	}
	// ---------------------------------

	function checkEmailStructure($mailString) {
		$returnValue = false;
		//// $pattern = "/^[_a-zA-Z0-9-öäüÖÄÜ]+(.[_a-zA-Z0-9-öäüÖÄÜ]+)*@([a-zA-Z0-9-öäüÖÄÜ]+.)+([a-zA-Z]{2,4})$/";
		//   $pattern = "/^[_a-zA-Z0-9-]+(.[_a-zA-Z0-9-]+)*@[_a-zA-Z0-9-]+(\.[_a-zA-Z0-9-]+)?(\.[_a-zA-Z0-9-]{2,3})$/";
		// $checkMail = preg_match($pattern, $mailString);
		// $returnValue = $checkMail;

		if(filter_var($mailString, FILTER_VALIDATE_EMAIL)){
			$returnValue = true;
		}
		/*
		if (eregi("	^[a-zA-Z0-9_]+@[a-zA-Z0-9\-]+\.[a-zA-Z0-9\-\.]+$]", $mailString)) {
			$returnValue = true;
		}
		else {
			$returnValue = true;
		}
		*/

		if($returnValue == false){			
			//// $pattern = "/^[_a-zA-Z0-9-öäüÖÄÜ]+(.[_a-zA-Z0-9-öäüÖÄÜ]+)*@([a-zA-Z0-9-öäüÖÄÜ]+.)+([a-zA-Z]{2,4})$/";
			#$pattern = "/^[_a-zA-Z0-9-]+(.[_a-zA-Z0-9-]+)*@[_a-zA-Z0-9-]+(\.[_a-zA-Z0-9-]+)?(\.[_a-zA-Z0-9-]{2,4})$/";
			$pattern = "/^[_a-zA-Z0-9\-]+(.[_a-zA-Z0-9\-]+)*@[_a-zA-Z0-9\-]+(\.[_a-zA-Z0-9\-]+)?(\.[_a-zA-Z0-9\-]{2,})$/";
			$checkMail = preg_match($pattern, $mailString);
			$returnValue = $checkMail;
		}

		#if(preg_match("/[,;]/", $mailString)){
		if(preg_match("/[,]/", $mailString)){
			$returnValue = false;
		}
		
		if(preg_match("/@.*@/", $mailString)){
			$returnValue = false;
		}

		return $returnValue;
	}
	//----------------------------------
	function dd($varName, $removeFormat=0, $mode=''){
		displayDebug($varName, $removeFormat, $mode);
	}
	function displayDebug($varName, $removeFormat=0, $mode='') {
		global $arrGetUserRights;

		if($arrGetUserRights["adminArea"] == 1 || $_COOKIE["isAdmin"] == 1){
			global $$varName;

			if(preg_match("/\[.*\]/", $varName)){
				$arrTemp = preg_match_all("/(.*)\[(.*)\]{1,}/ismU", $varName, $arrFound);
				$strTemp = $arrFound[1][0];
				global $$strTemp;
				$checkVar = $$strTemp;
				foreach($arrFound[2] as $thisKey => $thisValue){
					$thisValue = preg_replace("/[\"']/", "", $thisValue);
					$checkVar = $checkVar[$thisValue];
				}
			}
			else {
				global $$varName;
				$checkVar = $$varName;

				// BOF HANDLE CONSTANTS
				if(defined($varName) && preg_match("/^[A-Z_]{1,}$/ismU", $varName)){
					#echo 'ctype_upper: ' . ctype_upper(preg_replace("/_/", "", $varName));
					$checkVar = constant($varName);
				}
				// EOF HANDLE CONSTANTS
			}

			echo '<div class="debugVarsArea" style="font-size:10px;border:2px solid #FF0000; background-color:#FFFFFF;line-height:10px;">';
			if(is_array($checkVar)) {
				echo '<b>' . htmlentities($varName).':</b><br />';
				if(!$removeFormat) { echo '<pre>&nbsp;'; }
				if($mode != 'decodeHtml'){
					print_r($checkVar);
					#echo '<hr />';
				}
				else {
					#var_dump($checkVar);
					array_walk_recursive($checkVar, function(&$value, &$key) {
						$value = htmlentities($value);
						echo '[' . $key . '] => ' . $value . '<br />';
					});
				}
				if(!$removeFormat) { echo '</pre>'; }
			}
			else {
				if(!$removeFormat) { echo '<pre>&nbsp;'; }
				$checkVar = preg_replace("/\\t{3,}/", "", $checkVar);

				echo '<b>' . htmlentities($varName).':</b> '.htmlentities($checkVar).'<br />';
				if(!$removeFormat) { echo '</pre>'; }
			}
			echo '</div>';
			#echo '<hr />';
		}
	}

	//----------------------------------

	function removeUnnecessaryChars($string) {
		$newString = $string;

		$newString = preg_replace("/\\n/", "", $newString);
		$newString = preg_replace("/\\r/", "", $newString);
		$newString = preg_replace("/\\t/", "", $newString);
		// $newString = preg_replace("/<!--(.*)-->/ismU", "", $newString);
		$newString = preg_replace("/<thead>(.*)<\/thead>/ismU", "$1", $newString);
		$newString = preg_replace("/<tfoot>(.*)<\/tfoot>/ismU", "$1", $newString);
		$newString = preg_replace("/<tbody>(.*)<\/tbody>/ismU", "$1", $newString);

		return $newString;
	}
	function removeUnnecessaryChars2($string) {
		$newString = $string;

		$newString = preg_replace("/\\n/", "", $newString);
		$newString = preg_replace("/\\r/", "", $newString);
		$newString = preg_replace("/\\t/", "", $newString);

		return $newString;
	}
	//----------------------------------
	function cleanSQL($string) {
		$newString = $string;

		$newString = preg_replace("/\\n/", " ", $newString);
		$newString = preg_replace("/\\r/", " ", $newString);
		$newString = preg_replace("/\\t/", " ", $newString);
		$newString = preg_replace("/[ ]{2,}/", " ", $newString);

		return $newString;
	}

	//----------------------------------
	function formatVatID($string){
		$newString = $string;
		$newString = preg_replace("/ /", "", $newString);
		return $newString;
	}
	//----------------------------------

	function convertHtmlToText($string)
		{
			$tab = "\t";
			$lineBreak = "\r\n";
			$mail_line =  $lineBreak."-----------------------------------------------------------".$lineBreak;
			$mail_line2 = $lineBreak."+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++".$lineBreak;

			$string = preg_replace('/\\n/', '', $string);
			$string = preg_replace('/\\r/', '', $string);
			// $string = preg_replace('/</td><td/', '</td>|<td', $string);
			$string = preg_replace('/<\/td><td/', '</td>'.$tab.'<td', $string);
			$string = preg_replace('/<\/th><th/', '</th>'.$tab.'<th', $string);
			$string = preg_replace('/<\/tr>/', '</tr>'.$lineBreak, $string);
			$string = preg_replace('/<\/p>/', '</p>'.$lineBreak.$lineBreak, $string);
			$string = preg_replace('/<\/h(1|2|3|4|5|6)>/', '</h$1>'.$lineBreak.$lineBreak, $string);
			$string = preg_replace('/<table/', $lineBreak.$lineBreak.'<table', $string);
			$string = preg_replace('/<\/table>/', '</table>'.$lineBreak.$lineBreak, $string);
			$string = preg_replace('/<div/', $lineBreak.$lineBreak.'<div', $string);
			$string = preg_replace('/<\/div>/', '</div>'.$lineBreak.$lineBreak, $string);
			$string = preg_replace('/<br \/>/', '<br />'.$lineBreak, $string);
			$string = preg_replace('/<hr (.*) \/>/', '<hr />'.$mail_line, $string);

			$string = strip_tags($string);

			$string = myUnHtmlEntities($string);

			return $string;
		}

	//----------------------------------

	function myUnHtmlEntities($string) {
		$returnString = $string;

		$mode = 0; // 0 - 1

		if($mode == 0) {
			$returnString = html_entity_decode($returnString);
		}
		else {
			/*
			// replace numeric entities
			$returnString = preg_replace('~&#x([0-9a-f]+);~ei', 'chr(hexdec("\\1"))', $returnString);
			$returnString = preg_replace('~&#([0-9]+);~e', 'chr("\\1")', $returnString);
			// replace literal entities
			$trans_tbl = get_html_translation_table(HTML_ENTITIES);
			$trans_tbl = array_flip($trans_tbl);
			$returnString = strtr($returnString, $trans_tbl);
			*/
		}
		return $returnString;
	}

	// ---------------------------------
	function formatCSV($string, $fieldSeperator='\t') {
		$string = preg_replace('/&nbsp;/', " ", $string);
		$string = preg_replace('/\\n/', "", $string);
		$string = preg_replace('/\\r/', "", $string);
		$string = preg_replace('/\\t/', "", $string);
		$string = preg_replace('/<\/td>/', $fieldSeperator, $string);
		$string = preg_replace('/<\/th>/', $fieldSeperator, $string);
		$string = preg_replace('/<\/tr>/', "\n", $string);
		$string = strip_tags($string);
		return $string;
	}
	// ---------------------------------

	function renameUploadesFiles(){

	}
	function convertChars($string) {
		$arrCheckChars = array(
			array("convertFrom" => "ä", "convertTo" => "ae"),
			array("convertFrom" => "ö", "convertTo" => "oe"),
			array("convertFrom" => "ü", "convertTo" => "ue"),
			array("convertFrom" => "ß", "convertTo" => "ss"),
			
			array("convertFrom" => "Ä", "convertTo" => "ae"),
			array("convertFrom" => "Ö", "convertTo" => "oe"),
			array("convertFrom" => "Ü", "convertTo" => "ue"),

			array("convertFrom" => " ", "convertTo" => "_"),
			array("convertFrom" => "'", "convertTo" => "-"),
			array("convertFrom" => "\+", "convertTo" => "-"),
			array("convertFrom" => "&", "convertTo" => "-"),
		);
		$string = utf8_decode($string);
		$string = strtolower($string);
		$string = ($string);

		if(!empty($arrCheckChars)) {
			foreach($arrCheckChars as $thisKey) {
				//$string = preg_replace("/".$thisKey["convertFrom"]."/", $thisKey["convertTo"], $string);
				$string = preg_replace("/".utf8_decode($thisKey["convertFrom"])."/", $thisKey["convertTo"], $string);
			}
		}
		return $string;
	}
	// ---------------------------------
	function getUrlImageName($string) {
		$string = preg_replace("/(https|http):\/\//", "", $string);
		$string = preg_replace("/(www\.)/", "", $string);
		$string = preg_replace("/\//", "_", $string);
		$string = preg_replace("/\./", "-", $string);
		$string = preg_replace("/\?/", "_", $string);
		$string = preg_replace("/\=/", "_", $string);
		$string = preg_replace("/:/", "_", $string);
		$string = preg_replace("/:/", "_", $string);
		$string .= '.jpg';
		return $string;
	}
	// ---------------------------------
	function writeConfigFile() {
		global $dbConnection;
		$pathConfig = 'config/configParams_' . MANDATOR . '.inc.php';

		if(file_exists($pathConfig)){
			if(copy($pathConfig, "backupFiles/" . "_copy_" . date("Y-m-d_H_i_s") . "_" . basename($pathConfig))) {
				unlink($pathConfig);
			}
		}
		clearstatcache();

		$fp = fopen($pathConfig, "a");
		$fileContent = "<?php" . "\n";
		fwrite($fp, $fileContent);

		$fileContent = "// FILE GENERATED: " . date("Y-m-d H:i:s") . "\n";
		fwrite($fp, $fileContent);

		// BOF READ ALL DATAS
		$sql = "SELECT

					`configDatasID`,
					`configDatasCategorie`,
					`configDatasGroupID`,
					`configDatasParam`,
					`configDatasName`,
					`configDatasDescription`,
					`configDatasValue`,
					`configDatasSetValueFieldType`,
					`configDatasSetValueFieldValues`

				FROM `" . TABLE_CONFIG_DATAS . "`

				WHERE 1

				ORDER BY `configDatasCategorie`, `configDatasGroupID`
		";
		$rs = $dbConnection->db_query($sql);
		$markerCategorie = "";
		$markerGroup = "";
		while($ds = mysqli_fetch_assoc($rs)) {
			if($markerCategorie != $ds["configDatasCategorie"]) {
				$fileContent = "### " .$ds["configDatasCategorie"] .  "\n";
				fwrite($fp, $fileContent);
				$markerCategorie = $ds["configDatasCategorie"];
			}
			if($markerGroup != $ds["configDatasGroupID"]) {
				$fileContent = "\t" . "### " .$ds["configDatasGroupID"] .  "\n";
				fwrite($fp, $fileContent);
				$markerGroup = $ds["configDatasGroupID"];
			}
			$thisEnclose = "'";
			// $ds["configDatasValue"] = preg_replace("/^'(.*)'$/", "$1", $ds["configDatasValue"]);
			$ds["configDatasValue"] = preg_replace("/'/", "\"", $ds["configDatasValue"]);
			if($ds["configDatasValue"] == 'null' || $ds["configDatasValue"] == 'true' || $ds["configDatasValue"] == 'false') {
				$thisEnclose = "";
			}
			$fileContent = "\t" . "DEFINE('" . $ds["configDatasParam"] . "', " . $thisEnclose . $ds["configDatasValue"] . $thisEnclose . ");" . "\n";
			fwrite($fp, $fileContent);
		}
		// EOF READ ALL DATAS
		$fileContent = "?>" . "\n";
		fwrite($fp, $fileContent);

		fclose($fp);
	}
	// ---------------------------------
	function createConfigInputField($configDataValue, $configCategoryKey, $configGroupKey, $configDatasId ='editConfigDatas', $configDatasName='arrEditConfigDatas', $configDatasClass='_260') {
		global $arrGetUserRights;

		$fieldContent = "";

		if($configDataValue["configDatasSetValueFieldType"] == '') { $configDataValue["configDatasSetValueFieldType"] = 'input'; }
		if($configDataValue["configDatasSetValueFieldType"] == 'input') {
			$fieldContent = '<input type="text" id="' . $configDatasId . '_' . $configCategoryKey . '_' . $configGroupKey . '_' . $configDataValue["configDatasParam"] . '" name="' . $configDatasName . '[' . $configCategoryKey . '][' . $configGroupKey . '][' . $configDataValue["configDatasParam"] . ']" class="inputField' . $configDatasClass . '" value="' . htmlentities(utf8_decode($configDataValue["configDatasValue"])) . '" />';
		}
		else if($configDataValue["configDatasSetValueFieldType"] == 'select') {
			#$fieldContent = '<input type="text" id="' . $configDatasId . '_' . $configCategoryKey . '_' . $configGroupKey . '_' . $configDataValue["configDatasParam"] . '" name="' . $configDatasName . '[' . $configCategoryKey . '][' . $configGroupKey . '][' . $configDataValue["configDatasParam"] . ']" class="inputField' . $configDatasClass . '" value="' . $configDataValue["configDatasValue"] . '" />';
			$arrOptionFieldValues = json_decode($configDataValue["configDatasSetValueFieldValues"]);
			if(!empty($arrOptionFieldValues)){
				$fieldContent = '<select id="' . $configDatasId . '_' . $configCategoryKey . '_' . $configGroupKey . '_' . $configDataValue["configDatasParam"] . '" name="' . $configDatasName . '[' . $configCategoryKey . '][' . $configGroupKey . '][' . $configDataValue["configDatasParam"] . ']" class="inputSelect' . $configDatasClass . '">';
		  		foreach($arrOptionFieldValues as $thisFieldValue){
					$selected = '';
					if($configDataValue["configDatasValue"] == $thisFieldValue) {
						$selected = ' selected="selected" ';
					}
					$fieldContent .= '<option value="' . $thisFieldValue . '" ' . $selected . ' >' . $thisFieldValue . '</option>';
				}
				$fieldContent .= '</select>';
			}
		}
		else if($configDataValue["configDatasSetValueFieldType"] == 'boolean') {
			#$fieldContent = '<input type="text" id="' . $configDatasId . '_' . $configCategoryKey . '_' . $configGroupKey . '_' . $configDataValue["configDatasParam"] . '" name="' . $configDatasName . '[' . $configCategoryKey . '][' . $configGroupKey . '][' . $configDataValue["configDatasParam"] . ']" class="inputField' . $configDatasClass . '" value="' . $configDataValue["configDatasValue"] . '" />';
			$arrOptionFieldValues = json_decode($configDataValue["configDatasSetValueFieldValues"]);
			if(!empty($arrOptionFieldValues)){
				$fieldContent = '<select id="' . $configDatasId . '_' . $configCategoryKey . '_' . $configGroupKey . '_' . $configDataValue["configDatasParam"] . '" name="' . $configDatasName . '[' . $configCategoryKey . '][' . $configGroupKey . '][' . $configDataValue["configDatasParam"] . ']" class="inputSelect' . $configDatasClass . '">';
		  		foreach($arrOptionFieldValues as $thisFieldValue){
					$selected = '';
					if($configDataValue["configDatasValue"] == $thisFieldValue) {
						$selected = ' selected="selected" ';
					}
					$fieldContent .= '<option value="' . $thisFieldValue . '" ' . $selected . ' >' . $thisFieldValue . '</option>';
				}
				$fieldContent .= '</select>';
			}
		}
		if($arrGetUserRights["adminArea"]) {
			$fieldContent .= ' <span class="paramName">' . $configDataValue["configDatasParam"] . '</span>';
		}

		return $fieldContent;
	}
	// ---------------------------------
	function getFileType($file) {
		$arrTemp = explode(".", $file);
		$fileType = strtolower($arrTemp[(count($arrTemp) - 1)]);
		$fileType = strtoupper($fileType);
		return $fileType;
	}
	function getFilename($string) {
		$filename = basename($string);
		$arrTemp = explode("_", $filename);
		$filename = $arrTemp[(count($arrTemp) - 2)] . '_' . $arrTemp[(count($arrTemp) - 1)];
		return $filename;
	}
	// ---------------------------------
	function getCategories() {
		$arrDatas = array (
			"3" => "Automobil-Verbände >> TECHNO",
			"4" => "Automobil-Verbände >> PSI",
			"5" => "Automobil-Verbände >> MAPFRE",
			"6" => "Automobil-Verbände >> MOBILE.DE",
			"7" => "Automobil-Verbände >> ANAG",
			"8" => "Automobil-Verbände >> BVFK",
			"9" => "Automobil-Verbände >> AGO",
			"10" => "Werkstattsarten >> Vertragswerkstatt",
			"11" => "Werkstattsarten >> freie Werkstatt"
		);
		return $arrDatas;
	}
	// ---------------------------------
	function getCountDays($dateStart, $dateEnd) {
		/*
		$startdatum= mktime(0, 0, 0, 2, 23, 2005);  // Startdatum
		$heute= time ();                          // Heute
		$diff= $heute - $startdatum;
		$anzahl_tage = round ($diff / 86400);
		echo $anzahl_tage;
		*/
		$timeStart = strtotime($dateStart);
		$timeEnd = strtotime($dateEnd);
		$timeDiff = $timeEnd - $timeStart;
		$countDays = round ($timeDiff / 86400) + 1;
		return $countDays;
	}

	// ---------------------------------
	function getTimeNames($key, $type, $mode='short') {
		$arrMonthNames = array();
			$arrMonthNames["01"] = array('name' => 'Januar', 'nameShort' => 'Jan');
			$arrMonthNames["02"] = array('name' => 'Februar', 'nameShort' => 'Feb');
			$arrMonthNames["03"] = array('name' => 'März', 'nameShort' => 'Mrz');
			$arrMonthNames["04"] = array('name' => 'April', 'nameShort' => 'Apr');
			$arrMonthNames["05"] = array('name' => 'Mai', 'nameShort' => 'Mai');
			$arrMonthNames["06"] = array('name' => 'Juni', 'nameShort' => 'Jun');
			$arrMonthNames["07"] = array('name' => 'Juli', 'nameShort' => 'Jul');
			$arrMonthNames["08"] = array('name' => 'August', 'nameShort' => 'Aug');
			$arrMonthNames["09"] = array('name' => 'September', 'nameShort' => 'Sep');
			$arrMonthNames["10"] = array('name' => 'Oktober', 'nameShort' => 'Okt');
			$arrMonthNames["11"] = array('name' => 'November', 'nameShort' => 'Nov');
			$arrMonthNames["12"] = array('name' => 'Dezember', 'nameShort' => 'Dez');

		$arrDayNames = array();
			$arrDayNames[0] = array('name' => 'Sonntag', 'nameShort' => 'Son');
			$arrDayNames[1] = array('name' => 'Montag', 'nameShort' => 'Mon');
			$arrDayNames[2] = array('name' => 'Dienstag', 'nameShort' => 'Die');
			$arrDayNames[3] = array('name' => 'Mittwoch', 'nameShort' => 'Mit');
			$arrDayNames[4] = array('name' => 'Donnerstag', 'nameShort' => 'Don');
			$arrDayNames[5] = array('name' => 'Freitag', 'nameShort' => 'Fre');
			$arrDayNames[6] = array('name' => 'Samstag', 'nameShort' => 'Sam');

		if($mode == 'short') {
			$nameType = 'nameShort';
		}
		else {
			$nameType = 'name';
		}

		if($type == 'day') {
			$timeName = $arrDayNames[$key][$nameType];
		}
		else if($type == 'month') {
			$timeName = $arrMonthNames[$key][$nameType];
		}

		return $timeName;
	}

	// ---------------------------------
	function getParamTitels($string, $type) {
		global $arrGetUserRights;

		$arrParamTitels = array(
				'ADMIN_CONFIG' => 'Admin-Einstellungen',
				'BANK_DATAS' => 'Firma-Bank-Daten',
				'BASIC_CONFIG' => 'Grund-Konfiguration',
				'COMPANY_DATAS' => 'Firma-Daten',
				'DB_CONFIG' => 'Datenbank-Konfiguration',
				'FTP_CONFIG' => 'FTP-Konfiguration',
				'MAIL_CONFIG' => 'Mail-Konfiguration',
				'COMPANY_BANK_ACCOUNT_KSK' => 'KSK-Bank-Daten',
				'COMPANY_BANK_ACCOUNT_TEBA' => 'Teba-Bank-Daten',
				'COMPANY_DATAS_ADDRESS' => 'Firma-Kontakt-Daten',
				'COMPANY_DATAS_IMPRINT' => 'Firma-Impressum',
				'CONFIG_BOXES' => 'Info-Boxen',
				'CONFIG_SHIPPING' => 'Versand etc.',
				'CONFIG_SOUND' => 'Klangwiedergabe',
				'CONFIG_EVENT' => 'Ereignis-Fenster',
				'CONFIG_ICONS' => 'Icons-Anzeige',
				'CONFIG_ORDERS' => 'Bestellungen-Konfiguration',
				'CONFIG_Auftragslisten' => 'Auftragslisten-Konfiguration',
				'DB_EXTERN_ACQUISITION' => 'Datenbank Kundenakquise',
				'DB_EXTERN_ADM_REPORTS' => 'ADM Kundenakquise',
				'DB_EXTERN_SHOP' => 'Datenbank Online-Shop',
				'DB_EXTERN_DISTRIBUTION' => 'Datenbank Vertrieb',
				'DB_EXTERN_PRODUCTION' => 'Datenbank TR-Produktionen',
				'DB_LOKAL' => 'Datenbank Auftragslisten',
				'DEBUG_MODE' => 'Debug-Modus',
				'FILES_TYPES' => 'Datei-Typen',
				'LOG_FILES' => 'LOG-Dateien',
				'MAIL_CONFIG_ACCOUNT' => 'Mail-Konto',
				'MAIL_CONFIG_DEBUG' => 'Mail-Debug',
				'PHP_INI' => 'Php-Ini',
				'CONFIG_DOCUMENT_TYPE_NUMBERS' => 'Dokument-Nummern',
		);

		if($type == 'group') {
			$seperator = '';
		}

		if($type == 'categorie') {
			$seperator = ' / ';
		}
		$string = $arrParamTitels[$string] . $seperator;
		if(!$arrGetUserRights["adminArea"] && $type == 'categorie') {
			$string = '';
		}

		return $string;
	}

	// ---------------------------------

	function getUrlQueryString($string) {
		$arrTemp = explode('?', $string);
		if($arrTemp[1] != '') {
			$arrTemp = explode('&', $arrTemp[1]);
			if(!empty($arrTemp)) {
				foreach($arrTemp as $thisVarString){
					$arrTempVar = explode('=', $thisVarString);
					$arrReturn[$arrTempVar[0]] = $arrTempVar[1];
				}
			}
		}
		return $arrReturn;
	}

	// ---------------------------------

	function read_dir($dir) {
		if(preg_match("/\/$/", $dir)) {
			$dir = substr($dir, 0, (strlen($dir) - 1));
		}
		$array = array();
		$d = dir($dir);
		while (false !== ($entry = $d->read())) {
			if($entry!='.' && $entry!='..')
			{
				$entry = $dir.'/'.$entry;

				$filePath = addslashes($entry);
				$fileName = basename(stripslashes($filePath));

				// echo "1:filePath: ".$filePath."<br>";
				// echo "fileName: ".$fileName."<br>";

				if(is_dir($entry))
				{
					$array[] = $entry;
					$array = array_merge($array, read_dir($entry));
				}
				else
				{
					$array[] = $entry;
				}
				clearstatcache();
			}
		}
		$d->close();
		return $array;
	}

	// ---------------------------------

	function delete_dir($dir) {
		$array = array();
		$d = dir($dir);
		while (false !== ($entry = $d->read())) {
			if($entry!='.' && $entry!='..')
			{
				$entry = $dir.'/'.$entry;

				$filePath = addslashes($entry);
				$fileName = basename(stripslashes($filePath));

				// echo "1:filePath: ".$filePath."<br>";
				// echo "fileName: ".$fileName."<br>";

				if(is_dir($entry))
				{
					$array[] = $entry;
					$array = array_merge($array, read_dir($entry));
				}
				else
				{
					$array[] = $entry;
				}
				clearstatcache();
			}
		}
		$d->close();
		return $array;
	}

	// ---------------------------------
	function createPhoneCallLink($phoneNumber) {
		$thisPhoneCallLinkMethod = "skype"; // skype || callto | tel
		$arrPhoneCallLinkMethodData = array(
			"skype"		=> array("icon" => "iconPhone.png", "title" => "Den Kunden &uuml;ber Skype anrufen", "link" => $phoneNumber . "?call"),
			"callto"	=> array("icon" => "iconPhone.png", "title" => "Den Kunden anrufen", "link" =>  $phoneNumber . ""),
			"tel"		=> array("icon" => "iconPhone.png", "title" => "Den Kunden anrufen", "link" =>  $phoneNumber . ""),
		);
		$newString = ' <a href="' . $thisPhoneCallLinkMethod . ':' . $arrPhoneCallLinkMethodData[$thisPhoneCallLinkMethod]["link"] . '">
							<img src="layout/icons/' . $arrPhoneCallLinkMethodData[$thisPhoneCallLinkMethod]["icon"] . '" width="16" height="14" alt="Telefon-Kontakt" title="' . $arrPhoneCallLinkMethodData[$thisPhoneCallLinkMethod]["title"] . '" />
						</a>
			';
		return $newString;
	}
	// ---------------------------------

	function formatPhoneNumber($strPhone, $arrPhoneCodes=array()) {
		$newString = cleanPhoneNumbers($strPhone);

		if(preg_match("/^(\+49)/", $newString)){
			$newString = preg_replace("/(\+49)/", "$1(0) ", $newString);
		}
		else if(preg_match("/^(\+)/", $newString)){
			$newString = preg_replace("/(\+)/", "00", $newString);
		}

		if(!empty($arrPhoneCodes)){
			#$newString = preg_replace("/(" . implode("|", $arrPhoneCodes) . ")/", "$1 / ", $newString);
			#$newString = preg_replace("/(+49(0) 2922)/", "$1xxx", $newString);
			foreach($arrPhoneCodes as $thisKey => $thisValue){
				if($thisValue != ''){
					#$newString = preg_replace("/(" . $thisValue . ")/", "$1 / ", $newString);
					$newString = preg_replace("/(" . $thisValue . ")/", "$1 / ", $newString, 1);
				}
			}
		}

		if(preg_match("/[0-9]{3}/", $newString)){
			$newString = preg_replace("/([0-9]{2})/", "$1 ", $newString);
		}
		#$newString = preg_replace("/  /", " ", $newString);
		$newString = preg_replace("/[ ]{2,}/", " ", $newString);
		$newString = preg_replace("/\/[ ]{1}\//", "/", $newString);

		return trim($newString);
	}

	// ---------------------------------

	function cleanPhoneNumbers($string, $countryID='') {
		$clean = trim($string);
		$prefix = '';

		$clean = preg_replace('/\(0\)/', '', $clean);

		if(preg_match("/^\+/", $clean)) {
			$clean = preg_replace('/[a-z ()\/\[\]#\-,\–]/', '', $clean);
		}
		else {
			$clean = preg_replace('/[a-z ()\/\[\]#\-\+,\–]/', '', $clean);
			if($countryID != ''){
				$arrCountryPhoneCode = getCountryPhoneCode($countryID);
				$clean = preg_replace('/^0([1-9])/', $arrCountryPhoneCode["phoneAreaCodesPhoneCode"] . '$1', $clean);
			}
			$clean = preg_replace('/^00/', '+', $clean);
			$clean = $prefix . $clean;
		}
		return $clean;
	}

	function cleanChars($string) {
		$clean = trim($string);
		$clean = preg_replace('/ /', '', $clean);
		return $clean;
	}
	// ---------------------------------
	function embedSoundFile($file, $name='Alarm', $border='0', $width=150, $height=20, $autostart='true', $delay=0, $volume=100, $loop='false', $controls='smallconsole', $hidden='true') {
		$codeHTML = '';
		if(EMBED_SOUND_FILES) {
			// <bgsound src="background.mid" loop="infinite">
			$codeHTML .= '<embed ';
			$codeHTML .= ' name="' . $name . '_' . time() . '"';
			$codeHTML .= ' id="' . $name . '_' . time() . '"';
			$codeHTML .= ' class="' . $name . '" ';
			$codeHTML .= ' src="' . $file . '" ';
			$codeHTML .= ' border="' . $border . '" ';
			$codeHTML .= ' width="' . $width . '" ';
			$codeHTML .= ' height="' . $height . '" ';
			$codeHTML .= ' autostart="' . $autostart . '" ';
			$codeHTML .= ' delay="' . $delay . '" ';
			$codeHTML .= ' volume="' . $volume . '" ';
			$codeHTML .= ' loop="' . $loop . '" ';
			$codeHTML .= ' controls="' . $controls . '" ';
			$codeHTML .= ' hidden="' . $hidden . '" ';
			$codeHTML .= ' ></embed>';

			/*
			$codeHTML = '';

			$codeHTML .= '<object ';
			$codeHTML .= ' name="' . $name . '_' . time() . '"';
			$codeHTML .= ' id="' . $name . '_' . time() . '"';
			$codeHTML .= ' class="' . $name . '" ';

			$codeHTML .= ' border="' . $border . '" ';
			$codeHTML .= ' width="' . $width . '" ';
			$codeHTML .= ' height="' . $height . '" ';
			$codeHTML .= ' autostart="' . $autostart . '" ';
			$codeHTML .= ' data="' . $file . '" ';
			$codeHTML .= ' ></object>';
			*/
		}
		return $codeHTML;
	}
	// ---------------------------------
	function getTodayPersonnelVacations($date) {
		global $dbConnection;
		if($date == '') { $date = date('Y-m-d'); }
		$sql = "SELECT
					`personnelCalendarID`,
					`personnelCalendarPersonnelID`,

					`personnelCalendarStart`,
					`personnelCalendarEnd`,
					`personnelCalendarWorkDays`,
					`personnelCalendarTotalDays`,
					`personnelCalendarReason`,


					`personnelID`,
					`personnelFirstName`,
					`personnelLastName`

					FROM `" . TABLE_PERSONNEL_CALENDAR . "`
					LEFT JOIN `" . TABLE_PERSONNEL . "`
					ON (`" . TABLE_PERSONNEL_CALENDAR . "`.`personnelCalendarPersonnelID` = `" . TABLE_PERSONNEL . "`.`personnelID`)

					WHERE 1
						AND ('" . $date . "' BETWEEN `personnelCalendarStart` AND `personnelCalendarEnd`)
						AND `personnelCalendarReason` = 'urlaub'
						AND `personnelActive` = '1'
					ORDER BY `personnelGroup`, `personnelLastName`
		";

		$rs = $dbConnection->db_query($sql);
		$getVacationDatas = array();
		while($ds = mysqli_fetch_assoc($rs)) {
			foreach(array_keys($ds) as $field) {
				$getVacationDatas[$ds["personnelID"]][$field] = $ds[$field];
			}
		}
		if(empty($getVacationDatas)) { $getVacationDatas[0]["personnelFirstName"] = 'niemand'; }
		return $getVacationDatas;
	}
	// ---------------------------------
	function getTodayPersonnelDiseases($date) {
		global $dbConnection;
		if($date == '') { $date = date('Y-m-d'); }
		$sql = "SELECT
					`personnelCalendarID`,
					`personnelCalendarPersonnelID`,

					`personnelCalendarStart`,
					`personnelCalendarEnd`,
					`personnelCalendarWorkDays`,
					`personnelCalendarTotalDays`,
					`personnelCalendarReason`,

					`personnelID`,
					`personnelFirstName`,
					`personnelLastName`,

					`personnelActive`

					FROM `" . TABLE_PERSONNEL_CALENDAR . "`
					LEFT JOIN `" . TABLE_PERSONNEL . "`
					ON (`" . TABLE_PERSONNEL_CALENDAR . "`.`personnelCalendarPersonnelID` = `" . TABLE_PERSONNEL . "`.`personnelID`)

					WHERE 1
						AND `personnelActive` = '1'
						AND ('" . $date . "' BETWEEN `personnelCalendarStart` AND `personnelCalendarEnd`)
						AND `personnelCalendarReason` = 'krank'
		";

		$rs = $dbConnection->db_query($sql);
		$getVacationDatas = array();
		while($ds = mysqli_fetch_assoc($rs)) {
			foreach(array_keys($ds) as $field) {
				$getVacationDatas[$ds["personnelID"]][$field] = $ds[$field];
			}
		}
		if(empty($getVacationDatas)) { $getVacationDatas[0]["personnelFirstName"] = 'niemand'; }
		return $getVacationDatas;
	}
	// ---------------------------------
	function getCurrentPersonnelBirthdays($date) {
		global $dbConnection;

		if($date == '') { $date = date('m-d'); }
		$todayDate = date('m-d');
		$tomorrowDate = date('m-d', mktime(0,0,0, date('m'), (date('d') + 1), date('Y')));
		$yesterdayDate = date('m-d', mktime(0,0,0, date('m'), (date('d') - 0), date('Y')));
		$nextWeekDate = date('m-d', mktime(0,0,0, date('m'), (date('d') + 14), date('Y')));

		$sql = "SELECT
					`personnelID`,
					`personnelFirstName`,
					`personnelLastName`,
					`personnelBirthday`,
					`personnelActive`,
					CONCAT(DATE_FORMAT(`personnelBirthday`, '%m'), '-', DATE_FORMAT(`personnelBirthday`, '%d')) AS `personnelBirthdayDate`

				FROM `" . TABLE_PERSONNEL . "`
				WHERE 1
					AND `personnelActive` = '1'
					/*AND DATE_FORMAT(`personnelBirthday`, '%m-%d') = '" . $date . "'*/
					AND DATE_FORMAT(`personnelBirthday`, '%m-%d') >= '" . $yesterdayDate . "'
					AND DATE_FORMAT(`personnelBirthday`, '%m-%d') <= '" . $nextWeekDate . "'
				ORDER BY `personnelBirthdayDate`
			";

		$rs = $dbConnection->db_query($sql);

		$getBirthdayDatas = array();
		$countItems = 0;
		while($ds = mysqli_fetch_assoc($rs)) {
			foreach(array_keys($ds) as $field) {
				$getBirthdayDatas[$ds["personnelBirthdayDate"]][$ds["personnelID"]][$field] = $ds[$field];
			}
			$countItems++;
		}

		return $getBirthdayDatas;
	}
	// ---------------------------------
	function getTodayPersonnelBirthdays($date) {
		global $dbConnection;

		if($date == '') { $date = date('m-d'); }

		$sql = "SELECT
					`personnelID`,
					`personnelFirstName`,
					`personnelLastName`,
					`personnelBirthday`,
					`personnelActive`,
					CONCAT(DATE_FORMAT(`personnelBirthday`, '%m'), '-', DATE_FORMAT(`personnelBirthday`, '%d')) AS `personnelBirthdayDate`

				FROM `" . TABLE_PERSONNEL . "`
				WHERE 1
					AND `personnelActive` = '1'
					/*AND DATE_FORMAT(`personnelBirthday`, '%m-%d') = '" . $date . "'*/
					AND (
						`personnelBirthday` LIKE '%" . $date . "'
					)

			";

		$rs = $dbConnection->db_query($sql);

		$getBirthdayDatas = array();
		while($ds = mysqli_fetch_assoc($rs)) {
			foreach(array_keys($ds) as $field) {
				$getBirthdayDatas[$ds["personnelID"]][$field] = $ds[$field];
			}
		}

		if(empty($getBirthdayDatas)) { $getBirthdayDatas[0]["personnelFirstName"] = 'niemand'; }

		return $getBirthdayDatas;
	}
	// ---------------------------------
	function getTodayMessages($date, $mode=''){
		global $dbConnection;
		if($date == '') { $date = date('Y-m-d'); }
		$where = '';

		if($mode == ''){
			$where = " AND ('" . $date . "' BETWEEN `newsDateStart` AND `newsDateEnd`) ";
		}
		$sql = "SELECT
						`newsID`,
						`newsDateEntry`,
						`newsDateStart`,
						`newsDateEnd`,
						`newsTitle`,
						`newsText`,
						`newsType`,
						`newsRecipient`,
						`newsActive`

					FROM `" . TABLE_MESSAGES . "`

					WHERE 1
						AND `newsActive` = '1'
						" . $where . "
					ORDER BY `newsDateEntry` DESC
		";

		$rs = $dbConnection->db_query($sql);
		$getMessageDatas = array();
		while($ds = mysqli_fetch_assoc($rs)) {
			foreach(array_keys($ds) as $field) {
				$getMessageDatas[$ds["newsID"]][$field] = $ds[$field];
			}
		}
		return $getMessageDatas;
	}
	// ---------------------------------
	function getNewMails($mode='0') {
		if($mode == '1'){
			$getNewMailDatas = getNewMails_imapFunction();
		}
		else {
			$getNewMailDatas = getNewMails_imapClass();
		}
		return $getNewMailDatas;
	}

	function getNewMails_imapFunction() {
		global $dbConnection, $userDatas, $errorMessage;
		$getNewMailDatas = array();

		$arrMailDatas = unserialize($userDatas["userMailDatas"]);
		$arrMailDatas["serverInbox"] = '{' . $arrMailDatas["serverInboxName"] . ':' . $arrMailDatas["serverInboxPort"] . '/pop3}INBOX';

		if($arrMailDatas["serverInboxName"] != '' && $arrMailDatas["serverInboxLogin"] != '' && $arrMailDatas["serverInboxPassword"] != '') {

			$mailConnection = imap_open($arrMailDatas["serverInbox"], $arrMailDatas["serverInboxLogin"], $arrMailDatas["serverInboxPassword"]);
			$folders = imap_listmailbox($mailConnection, $arrMailDatas["serverInbox"], "*");

			if($mailConnection){
				if($opjTemp) { unset($opjTemp); }
				$opjTemp = new stdClass();
				$opjTemp->stream = $mailConnection;
				$opjTemp->is_connected = 1;
				$getNewMailDatas["CONNECTION"]		= $opjTemp;

				if($opjTemp) { unset($opjTemp); }
				$opjTemp = new stdClass();
				$objImapCheck = imap_check($mailConnection);
				$opjTemp->Unread	= imap_num_msg($mailConnection);
				$opjTemp->Deleted	= 0;
				$opjTemp->Nmsgs		= $objImapCheck->Nmsgs;
				$opjTemp->Size		= 1;
				$opjTemp->Date		= $objImapCheck->Date;
				$opjTemp->Driver	= $objImapCheck->Driver;
				$opjTemp->Mailbox	= $objImapCheck->Mailbox;
				$opjTemp->Recent	= $objImapCheck->Recent;
				#$opjTemp->Recent	= imap_num_recent($mailConnection);
				$getNewMailDatas["INFOS"]			= $opjTemp;

				// $getNewMailDatas["FOLDERS"]		= imap_listmailbox($mailConnection);

				if($opjTemp) { unset($opjTemp); }
				$opjTemp = array();
				$objImapHeaders = imap_headers($mailConnection);
				$countImapHeaders = count($objImapHeaders);
				if($countImapHeaders >= MAX_DISPLAY_NEW_MAILS){
					$countImapHeaders = MAX_DISPLAY_NEW_MAILS;
				}

				$objImapHeaders = imap_fetch_overview($mailConnection, "1:" . $countImapHeaders, 0);

				if(!empty($objImapHeaders)){
					foreach($objImapHeaders as $thisObjImapHeaderKey => $thisObjImapHeaderValue){

						$opjTemp[$thisObjImapHeaderKey] = array();
						$opjTemp[$thisObjImapHeaderKey]["date"]				= $thisObjImapHeaderValue->date;
						$opjTemp[$thisObjImapHeaderKey]["subject"]			= $thisObjImapHeaderValue->subject;
						$opjTemp[$thisObjImapHeaderKey]["to"]				= $thisObjImapHeaderValue->to;
						$opjTemp[$thisObjImapHeaderKey]["message_id"]		= $thisObjImapHeaderValue->message_id;
						$opjTemp[$thisObjImapHeaderKey]["from"]				= $thisObjImapHeaderValue->from;
						$opjTemp[$thisObjImapHeaderKey]["sender"]			= $thisObjImapHeaderValue->from;
						$opjTemp[$thisObjImapHeaderKey]["reply_toaddress"]	= $thisObjImapHeaderValue->from;
						$opjTemp[$thisObjImapHeaderKey]["size"]				= $thisObjImapHeaderValue->size;
						$opjTemp[$thisObjImapHeaderKey]["msgno"]			= $thisObjImapHeaderValue->msgno;
						#$opjTemp[$thisObjImapHeaderKey]["status"]			= $thisObjImapHeaderValue->seen;
						if($thisObjImapHeaderValue->recent == 1){
							$opjTemp[$thisObjImapHeaderKey]["status"]		= "recent";
						}
						if($thisObjImapHeaderValue->flagged == 1){
							$opjTemp[$thisObjImapHeaderKey]["status"]		= "flagged";
						}
						if($thisObjImapHeaderValue->answered == 1){
							$opjTemp[$thisObjImapHeaderKey]["status"]		= "answered";
						}
						if($thisObjImapHeaderValue->deleted == 1){
							$opjTemp[$thisObjImapHeaderKey]["status"]		= "deleted";
						}
						if($thisObjImapHeaderValue->seen == 1){
							$opjTemp[$thisObjImapHeaderKey]["status"]		= "seen";
						}
						if($thisObjImapHeaderValue->draft == 1){
							$opjTemp[$thisObjImapHeaderKey]["status"]		= "draft";
						}
						$opjTemp[$thisObjImapHeaderKey]["strtotime"]		= $thisObjImapHeaderValue->udate;
					}
				}
				$getNewMailDatas["HEADERS"]			= $opjTemp;

				// $getNewMailDatas["MESSAGES"]		= '';

				imap_close($mailConnection);
			}
			else {
				$errorMessage .= 'Keine Verbindung zum Mailserver!' . '<br />';
				$errorMessage .= imap_errors() . '<br />';
			}

		}
		else {
			$errorMessage .= 'Bitte tragen Sie Ihre Mailkonto-Daten ein!' . '<br />';
		}
		return $getNewMailDatas;
	}

	function getNewMails_imapClass() {
		global $dbConnection, $userDatas, $errorMessage;
		$getNewMailDatas = array();

		$arrMailDatas = unserialize($userDatas["userMailDatas"]);
		#$arrMailDatas["serverInbox"] = '{' . $arrMailDatas["serverInboxName"] . ':' . $arrMailDatas["serverInboxPort"] . '/pop3}INBOX';
		$arrMailDatas["serverInbox"] = '' . $arrMailDatas["serverInboxName"] . '';

		// $mailConnection = new Imap($arrMailDatas["serverOutboxName"], $arrMailDatas["serverOutboxLogin"], $arrMailDatas["serverOutboxPassword"], 'INBOX', 110, 'tls' );

		if($arrMailDatas["serverInboxName"] != '' && $arrMailDatas["serverInboxLogin"] != '' && $arrMailDatas["serverInboxPassword"] != '') {
			$mailConnection = new Imap($arrMailDatas["serverInbox"], $arrMailDatas["serverInboxLogin"], $arrMailDatas["serverInboxPassword"]);
			// $mailConnection = new Imap($arrMailDatas["serverOutboxName"], $arrMailDatas["serverOutboxLogin"], $arrMailDatas["serverOutboxPassword"], 'OP_READONLY', 1);

			if($mailConnection->get_is_connected()) {
				$getNewMailDatas["CONNECTION"]		= $mailConnection;
				$getNewMailDatas["INFOS"]			= $mailConnection->returnImapMailBoxmMsgInfoObj();
				// $getNewMailDatas["FOLDERS"]		= $mailConnection->returnMailboxListArr();
				$getNewMailDatas["HEADERS"]			= $mailConnection->returnMailBoxHeaderArr();
				#$getNewMailDatas["MESSAGES"]		= $mailConnection->returnEmailMessageArr(1);
			}
			else {
				$errorMessage .= 'Keine Verbindung zum Mailserver!' . '<br />';
			}
		}
		else {
			$errorMessage .= 'Bitte tragen Sie Ihre Mailkonto-Daten ein!' . '<br />';
		}
		return $getNewMailDatas;
	}

	// ---------------------------------
	function getLatestOnlineOrders($startDate, $endDate) {
		// echo DB_HOST_EXTERN_SHOP.", '', ".DB_NAME_EXTERN_SHOP.", ".DB_USER_EXTERN_SHOP.", ".DB_PASSWORD_EXTERN_SHOP;
		// exit();
		
		$dbConnection_ExternShop = new DB_Connection(DB_HOST_EXTERN_SHOP, '', DB_NAME_EXTERN_SHOP, DB_USER_EXTERN_SHOP, DB_PASSWORD_EXTERN_SHOP);
		$dbOpen_ExternShop = $dbConnection_ExternShop->db_connect();

		$sql = "SELECT
						SUBSTRING(`date_purchased`, 1, 10) AS `date_purchased`,
						COUNT(`orders_id`) AS `sumOnlineOrders`

					FROM `" . TABLE_SHOP_ORDERS . "`

					WHERE `date_purchased` LIKE '" . $startDate. "%'
						OR `date_purchased` LIKE '" . $endDate. "%'

					GROUP BY(SUBSTRING(`date_purchased`, 1, 10))

					ORDER BY `date_purchased` DESC
		";

		$rs = $dbConnection_ExternShop->db_query($sql);

		while($ds = mysqli_fetch_assoc($rs)){
			$getOnlineOrdersDatas[$ds["date_purchased"]] = $ds["sumOnlineOrders"];
		}

		if($dbConnection_ExternShop) {
			$dbConnection_ExternShop->db_close($dbOpen_ExternShop);
		}

		if($getOnlineOrdersDatas[$startDate] == '') { $getOnlineOrdersDatas[$startDate] = 0; }
		if($getOnlineOrdersDatas[$endDate] == '') { $getOnlineOrdersDatas[$endDate] = 0; }

		return $getOnlineOrdersDatas;
	}
	// ---------------------------------
	function getOnlineShopRegistrations() {
        $dbConnection_ExternShop = new DB_Connection(DB_HOST_EXTERN_SHOP, '', DB_NAME_EXTERN_SHOP, DB_USER_EXTERN_SHOP, DB_PASSWORD_EXTERN_SHOP);
        $dbOpen_ExternShop = $dbConnection_ExternShop->db_connect();
        $getOnlineShopRegistrationDatas = array();
        $sql = "SELECT
                    COUNT(`customers_id`) AS `countNewRegistrations`
                    FROM `" . TABLE_SHOP_CUSTOMERS . "`
                    WHERE `customers_status` = '2'
        ";

        $rs = $dbConnection_ExternShop->db_query($sql);

        list($getOnlineShopRegistrationDatas["countNewRegistrations"]) = mysqli_fetch_array($rs);

        if($getOnlineShopRegistrationDatas["countNewRegistrations"] == '') { $getOnlineShopRegistrationDatas["countNewRegistrations"] = 0; }

        $dbConnection_ExternShop->db_close();
        return $getOnlineShopRegistrationDatas;
    }

	// ---------------------------------
	function writeVcard(){
		global $_POST, $errorMessage, $successMessage;
		// BOF WRITE VCARD
			if(file_exists(DIRECTORY_ATTACHMENT_DOCUMENTS . 'contact.vcf')) {
				unlink(DIRECTORY_ATTACHMENT_DOCUMENTS . 'contact.vcf');
			}
			clearstatcache();
			if(file_exists(PATH_VCARD_TEMPLATE)) {
				$fileVcardContent = implode("", file(PATH_VCARD_TEMPLATE));

				$fileVcardContent = preg_replace("/{###COMPANY_CITY###}/", $_POST["arrEditConfigDatas"]["COMPANY_DATAS"]["COMPANY_DATAS_ADDRESS"]["COMPANY_CITY"], $fileVcardContent);
				$fileVcardContent = preg_replace("/{###COMPANY_COUNTRY###}/", $_POST["arrEditConfigDatas"]["COMPANY_DATAS"]["COMPANY_DATAS_ADDRESS"]["COMPANY_COUNTRY"], $fileVcardContent);
				$fileVcardContent = preg_replace("/{###COMPANY_FAX###}/", cleanPhoneNumbers($_POST["arrEditConfigDatas"]["COMPANY_DATAS"]["COMPANY_DATAS_ADDRESS"]["COMPANY_FAX"]), $fileVcardContent);
				$fileVcardContent = preg_replace("/{###COMPANY_INTERNET###}/", "http://" . preg_replace("/http:\/\//", "", $_POST["arrEditConfigDatas"]["COMPANY_DATAS"]["COMPANY_DATAS_ADDRESS"]["COMPANY_INTERNET"]), $fileVcardContent);
				$fileVcardContent = preg_replace("/{###COMPANY_MAIL###}/", $_POST["arrEditConfigDatas"]["COMPANY_DATAS"]["COMPANY_DATAS_ADDRESS"]["COMPANY_MAIL"], $fileVcardContent);
				$fileVcardContent = preg_replace("/{###COMPANY_NAME_LONG###}/", $_POST["arrEditConfigDatas"]["COMPANY_DATAS"]["COMPANY_DATAS_ADDRESS"]["COMPANY_NAME_LONG"], $fileVcardContent);
				$fileVcardContent = preg_replace("/{###COMPANY_NAME_SHORT###}/", $_POST["arrEditConfigDatas"]["COMPANY_DATAS"]["COMPANY_DATAS_ADDRESS"]["COMPANY_NAME_SHORT"], $fileVcardContent);
				$fileVcardContent = preg_replace("/{###COMPANY_NOTE###}/", $_POST["arrEditConfigDatas"]["COMPANY_DATAS"]["COMPANY_DATAS_ADDRESS"]["COMPANY_NOTE"], $fileVcardContent);
				$fileVcardContent = preg_replace("/{###COMPANY_PHONE###}/", cleanPhoneNumbers($_POST["arrEditConfigDatas"]["COMPANY_DATAS"]["COMPANY_DATAS_ADDRESS"]["COMPANY_PHONE"]), $fileVcardContent);
				$fileVcardContent = preg_replace("/{###COMPANY_STREET_NAME###}/", $_POST["arrEditConfigDatas"]["COMPANY_DATAS"]["COMPANY_DATAS_ADDRESS"]["COMPANY_STREET_NAME"], $fileVcardContent);
				$fileVcardContent = preg_replace("/{###COMPANY_STREET_NUMBER###}/", $_POST["arrEditConfigDatas"]["COMPANY_DATAS"]["COMPANY_DATAS_ADDRESS"]["COMPANY_STREET_NUMBER"], $fileVcardContent);
				$fileVcardContent = preg_replace("/{###COMPANY_ZIPCODE###}/", $_POST["arrEditConfigDatas"]["COMPANY_DATAS"]["COMPANY_DATAS_ADDRESS"]["COMPANY_ZIPCODE"], $fileVcardContent);
				$fileVcardContent = preg_replace("/{###CREATION_DATE###}/", date("Ymd"), $fileVcardContent);
				$fileVcardContent = preg_replace("/{###CREATION_TIME###}/", date("His"), $fileVcardContent);

				$fp = fopen(DIRECTORY_ATTACHMENT_DOCUMENTS . 'contact.vcf', 'w');
				$rs = fwrite($fp, $fileVcardContent);
				fclose($fp);
				if($rs){
					$successMessage .= ' Die vCard-Datei wurde neu generiert.' . '<br />';
				}
				else {
					$errorMessage .= ' Die vCard-Datei konnte nicht neu generiert werden.' . '<br />';
				}
			}
			else {
				$errorMessage .= ' Die vCard-Template-Datei konnte nicht gefunden werden.' . '<br />';
			}
			clearstatcache();

			// DIRECTORY_ATTACHMENT_DOCUMENTS . 'contact.vcf'
		// EOF WRITE VCARD
	}
	// ---------------------------------
	function cleanSpaces($string){
		$newString = $string;
		$newString = preg_replace("/[ ]{2,}/", " ", $newString);
		$newString = preg_replace("/ , /", ", ", $newString);
		return $newString;
	}
	// ---------------------------------
	function convertDecimal($value, $type="display", $test='') {
		$returnValue = $value;
		if($type == "display") {
			$returnValue = number_format($returnValue, 2, ',', '');
		}
		else if($type == "store") {
			if(preg_match('/,/', $returnValue)) {
				$returnValue = preg_replace('/\./', '', $returnValue);
			}
			$returnValue = preg_replace('/,/', '.', $returnValue);
			$returnValue = floatval($returnValue);
			$returnValue = number_format($returnValue, 2, '.', '');
		}
		else {
			// $returnValue
		}
		return $returnValue;
	}
	// ---------------------------------
	function createSelectDocTypes ($doctype, $mode, $arrAddForbiddenDocTypes='', $docStatusType='') {
		global $arrDocumentTypeDatas;
		$content = '';
		$arrForbiddenDocTypes = array();

		if($mode == 'convert') {
			$arrForbiddenDocTypes[] = 'KA';
			$arrForbiddenDocTypes[] = 'BR';
			$arrForbiddenDocTypes[] = 'PR';
			if($doctype == 'AN') { 
				$arrForbiddenDocTypes[] = 'AN';
				$arrForbiddenDocTypes[] = 'AB';
				$arrForbiddenDocTypes[] = 'RE';
				$arrForbiddenDocTypes[] = 'LS';
				$arrForbiddenDocTypes[] = 'MA';
				$arrForbiddenDocTypes[] = 'M1';
				$arrForbiddenDocTypes[] = 'M2';
				$arrForbiddenDocTypes[] = 'GU';
			}
			if($doctype == 'AB') {
				  $arrForbiddenDocTypes[] = 'AN';
				  $arrForbiddenDocTypes[] = 'AB';
				  $arrForbiddenDocTypes[] = 'MA';
				  $arrForbiddenDocTypes[] = 'M1';
				  $arrForbiddenDocTypes[] = 'M2';
				  //$arrForbiddenDocTypes[] = 'RK';
				}
			if($doctype == 'RE') { 
				$arrForbiddenDocTypes[] = 'AN';
				$arrForbiddenDocTypes[] = 'AB';
				$arrForbiddenDocTypes[] = 'RE';
				$arrForbiddenDocTypes[] = 'M1';
				$arrForbiddenDocTypes[] = 'M2';
				//$arrForbiddenDocTypes[] = 'RK';
			}  
			if($doctype == 'GU') { $arrForbiddenDocTypes[] = 'AN';
				 $arrForbiddenDocTypes[] = 'AB';
				 $arrForbiddenDocTypes[] = 'RE';
				 $arrForbiddenDocTypes[] = 'LS';
				 $arrForbiddenDocTypes[] = 'GU';
				 $arrForbiddenDocTypes[] = 'MA';
				 $arrForbiddenDocTypes[] = 'M1';
				 $arrForbiddenDocTypes[] = 'M2';
			}
			if($doctype == 'MA') { $arrForbiddenDocTypes[] = 'AN';
				 $arrForbiddenDocTypes[] = 'AB';
				 $arrForbiddenDocTypes[] = 'RE';
				 $arrForbiddenDocTypes[] = 'LS';
				 $arrForbiddenDocTypes[] = 'MA';
				 $arrForbiddenDocTypes[] = 'M2';
			}
			if($doctype == 'M1') { 
				 $arrForbiddenDocTypes[] = 'AN';
				 $arrForbiddenDocTypes[] = 'AB';
				 $arrForbiddenDocTypes[] = 'RE';
				 $arrForbiddenDocTypes[] = 'LS';
				 $arrForbiddenDocTypes[] = 'MA';
				 $arrForbiddenDocTypes[] = 'M1';
			}
			if($doctype == 'M2') { 
				 $arrForbiddenDocTypes[] = 'AN';
				 $arrForbiddenDocTypes[] = 'AB';
				 $arrForbiddenDocTypes[] = 'RE';
				 $arrForbiddenDocTypes[] = 'LS';
				 $arrForbiddenDocTypes[] = 'MA';
				 $arrForbiddenDocTypes[] = 'M1';
				 $arrForbiddenDocTypes[] = 'M2';
			}
			if($doctype == 'LS') { 
				 $arrForbiddenDocTypes[] = 'RK';
				 $arrForbiddenDocTypes[] = 'AN';
				 $arrForbiddenDocTypes[] = 'AB';
				 $arrForbiddenDocTypes[] = 'M1';
				 $arrForbiddenDocTypes[] = 'M2';
				 $arrForbiddenDocTypes[] = 'RK';
			 }
			if($doctype == 'RK') { 
				 $arrForbiddenDocTypes[] = 'RK';
				 $arrForbiddenDocTypes[] = 'AN';
				 $arrForbiddenDocTypes[] = 'AB';
				 $arrForbiddenDocTypes[] = 'M1';
				 $arrForbiddenDocTypes[] = 'M2';
				 $arrForbiddenDocTypes[] = 'RK';
			 }
			/* if($doctype == 'RK') { 
				 $arrForbiddenDocTypes[] = 'GU';
				 $arrForbiddenDocTypes[] = 'LS'; 
			 } */
			if($docStatusType == '2'){
				$arrForbiddenDocTypes[] = 'MA'; $arrForbiddenDocTypes[] = 'M1'; $arrForbiddenDocTypes[] = 'M2'; $arrForbiddenDocTypes[] = 'M3'; $arrForbiddenDocTypes[] = 'IK';
			}

			if(is_array($arrAddForbiddenDocTypes)) {
				$arrForbiddenDocTypes = array_merge($arrForbiddenDocTypes, $arrAddForbiddenDocTypes);
				$arrForbiddenDocTypes = array_unique($arrForbiddenDocTypes);
			}

			if($doctype == "LS"){
				$keyTemp = array_search("LS", $arrForbiddenDocTypes);
				if($keyTemp != ""){
					unset($arrForbiddenDocTypes[$keyTemp]);
				}
				$keyTemp = array_search("", $arrForbiddenDocTypes);
				if($keyTemp != ""){
					unset($arrForbiddenDocTypes[$keyTemp]);
				}
			}
		}
		else if($mode == 'copy') {
			$arrForbiddenDocTypes[] = 'KA';
			$arrForbiddenDocTypes[] = 'BR';
			$arrForbiddenDocTypes[] = 'GU';
			$arrForbiddenDocTypes[] = 'MA';
			$arrForbiddenDocTypes[] = 'LS';
			$arrForbiddenDocTypes[] = 'M1';
			$arrForbiddenDocTypes[] = 'M2';
			$arrForbiddenDocTypes[] = 'RK';
		}

		#$arrForbiddenDocTypes = array_diff($arrForbiddenDocTypes, array('LS'));
		if(!empty($arrDocumentTypeDatas)) {
			foreach($arrDocumentTypeDatas as $thisDocTypeKey => $thisDocTypeValue){
				#if(!in_array($thisDocTypeKey, $arrForbiddenDocTypes)) {
				if(!in_array($thisDocTypeKey, $arrForbiddenDocTypes)) {
					$content .= '<option value="' . $thisDocTypeKey . '" >' . $thisDocTypeValue["createdDocumentsTypesName"] . ' (' . $thisDocTypeValue["createdDocumentsTypesShortName"] . ')</option>';
				}
			}
		}

		if($_COOKIE["isAdmin"] == 1){
			#echo '<pre>';
			#print_r($docStatusType);
			#echo '</pre>';
		}

		return $content;
	}
	// ---------------------------------

	function getMandatories() {
		global $dbConnection;
		$sql = "
				SELECT
					`mandatoriesID`,
					`mandatoriesShortName`,
					`mandatoriesName`,
					`mandatoriesActive`,
					`mandatoriesCustomerNumber`

				FROM `" . TABLE_MANDATORIES . "`

				WHERE 1
					AND `mandatoriesActive` = '1'
		";

		$rs = $dbConnection->db_query($sql);

		$getMandatoryDatas = array();
		while($ds = mysqli_fetch_assoc($rs)) {
			foreach(array_keys($ds) as $field) {
				$getMandatoryDatas[$ds["mandatoriesID"]][$field] = $ds[$field];
			}
		}
		return $getMandatoryDatas;
	}

	// ---------------------------------
	function getMandatoriesShort() {
		global $dbConnection;
		$sql = "
				SELECT
					`mandatoriesID`,
					`mandatoriesShortName`,
					`mandatoriesName`,
					`mandatoriesActive`,
					`mandatoriesCustomerNumber`

				FROM `" . TABLE_MANDATORIES . "`

				WHERE 1
					AND `mandatoriesActive` = '1'
		";

		$rs = $dbConnection->db_query($sql);

		$getMandatoryDatas = array();
		while($ds = mysqli_fetch_assoc($rs)) {
			foreach(array_keys($ds) as $field) {
				$getMandatoryDatas[$ds["mandatoriesShortName"]][$field] = $ds[$field];
			}
		}
		return $getMandatoryDatas;
	}
	// ---------------------------------
	function getAllMandatoryCompanyDatas(){
		global $dbConnection;

		$getAllMandatoryCompanyDatas = array();

		$arrMandatories = getMandatories();

		if(!empty($arrMandatories)){
			foreach($arrMandatories as $thisMandatoryKey => $thisMandatoryValue){
				$thisConfigDB = preg_replace("/{###MANDATORY###}/", $thisMandatoryValue["mandatoriesShortName"], TABLE_CONFIG_DATAS_ALL_MANDATORIES);

				$sql = "
					SELECT
						`configDatasGroupID`,
						`configDatasParam`,
						`configDatasName`,
						`configDatasDescription`,
						`configDatasValue`

						FROM `" . $thisConfigDB . "`

						WHERE 1
							AND `configDatasCategorie` = 'COMPANY_DATAS'

				";

				$rs = $dbConnection->db_query($sql);

				while($ds = mysqli_fetch_assoc($rs)) {
					$getAllMandatoryCompanyDatas[$thisMandatoryValue["mandatoriesShortName"]][$ds["configDatasParam"]] = $ds["configDatasValue"];
				}
			}
		}
		return $getAllMandatoryCompanyDatas;
	}
	// ---------------------------------

	function getUsers(){
		global $dbConnection;
		$sql = "SELECT
					`usersID`,
					`usersLogin`,
					`usersPassword`,
					`usersFirstName`,
					`usersLastName`,
					`userMailDatas`,
					`usersRights`,
					`usersActive`,
					`usersUserToPersonnel`

					FROM `" . TABLE_USERS . "`

					WHERE 1
						AND `usersActive` = '1'

			";

		$rs = $dbConnection->db_query($sql);

		$getUserDatas = array();
		while($ds = mysqli_fetch_assoc($rs)) {
			foreach(array_keys($ds) as $field) {
				$getUserDatas[$ds["usersID"]][$field] = $ds[$field];
			}
		}
		return $getUserDatas;
	}

	// ---------------------------------
	function getRelationSalesmenZipcode($zipcode='', $mode='') {
		global $dbConnection;
		$where = " AND `salesmenAreasActive` = '1' ";
		if($mode == 'SALESMEN_ALL'){
			$where = '';
		}
		$sql = "SELECT
					`" . TABLE_SALESMEN . "`.`salesmenID`,
					`" . TABLE_SALESMEN . "`.`salesmenKundennummer`,
					`" . TABLE_SALESMEN . "`.`salesmenFirmenname`,
					`" . TABLE_SALESMEN . "`.`salesmenAreas`,
					`" . TABLE_SALESMEN . "`.`salesmenAreasActive`,
					`" . TABLE_SALESMEN . "`.`salesmenAreaProtection`,
					`" . TABLE_SALESMEN . "`.`salesmenActive`,

					`" . TABLE_CUSTOMERS . "`.`customersID`,
					`" . TABLE_CUSTOMERS . "`.`customersKundennummer`

					FROM `" . TABLE_SALESMEN . "`
					LEFT JOIN `" . TABLE_CUSTOMERS . "`
					ON(`" . TABLE_SALESMEN . "`.`salesmenKundennummer` = `" . TABLE_CUSTOMERS . "`.`customersKundennummer`)
					WHERE 1
						AND `salesmenAreas` != ''
						AND `salesmenActive` = '1'
						" . $where . "
			";

		$rs = $dbConnection->db_query($sql);

		$getRelationSalesmenZipcodeDatas = array();
		while($ds = mysqli_fetch_assoc($rs)) {
			$arrTemp = explode(';', $ds["salesmenAreas"]);

			if(!empty($arrTemp)){
				foreach($arrTemp as $thisPLZ) {
					if(preg_match("/\-/", $thisPLZ)){
						$arrTemp2 = explode('-', $thisPLZ);
						for($i = $arrTemp2[0] ;  $i < ($arrTemp2[1] + 1) ; $i++) {
							$thisPLZ2 = $i;
							if($i <	10){
								$thisPLZ2 = "0" . $i;
							}
							$getRelationSalesmenZipcodeDatas[$thisPLZ2]["kundenname"] = $ds["salesmenFirmenname"];
							$getRelationSalesmenZipcodeDatas[$thisPLZ2]["kundennummer"] = $ds["salesmenKundennummer"];
							$getRelationSalesmenZipcodeDatas[$thisPLZ2]["kundenID"] = $ds["customersID"];
							$getRelationSalesmenZipcodeDatas[$thisPLZ2]["kundenPLZ"] = $thisPLZ2;
							$getRelationSalesmenZipcodeDatas[$thisPLZ2]["kundenPLZaktiv"] = $ds["salesmenAreasActive"];
						}
					}
					else {
						$getRelationSalesmenZipcodeDatas[$thisPLZ]["kundenname"] = $ds["salesmenFirmenname"];
						$getRelationSalesmenZipcodeDatas[$thisPLZ]["kundennummer"] = $ds["salesmenKundennummer"];
						$getRelationSalesmenZipcodeDatas[$thisPLZ]["kundenID"] = $ds["customersID"];
						$getRelationSalesmenZipcodeDatas[$thisPLZ]["kundenPLZ"] = $thisPLZ;
						$getRelationSalesmenZipcodeDatas[$thisPLZ]["kundenPLZaktiv"] = $ds["salesmenAreasActive"];
					}
				}
			}
		}

		if($zipcode != ''){
			$arrTemp = $getRelationSalesmenZipcodeDatas[$zipcode];
			$getRelationSalesmenZipcodeDatas = array();
			$getRelationSalesmenZipcodeDatas[$zipcode] = $arrTemp;
		}
		ksort($getRelationSalesmenZipcodeDatas);
		#print_r($getRelationSalesmenZipcodeDatas);
		return $getRelationSalesmenZipcodeDatas;
	}

	// ---------------------------------
	function getRelationSalesmenZipcode2($zipcode='', $mode='') {
		global $dbConnection;
		$where = " AND `salesmenAreasActive` = '1' ";
		if($mode == 'SALESMEN_ALL'){
			$where = '';
		}
		$sql = "SELECT
					`" . TABLE_SALESMEN . "`.`salesmenID`,
					`" . TABLE_SALESMEN . "`.`salesmenKundennummer`,
					`" . TABLE_SALESMEN . "`.`salesmenFirmenname`,
					`" . TABLE_SALESMEN . "`.`salesmenAreas`,
					`" . TABLE_SALESMEN . "`.`salesmenAreasActive`,
					`" . TABLE_SALESMEN . "`.`salesmenAreaProtection`,
					`" . TABLE_SALESMEN . "`.`salesmenActive`,

					`" . TABLE_CUSTOMERS . "`.`customersID`,
					`" . TABLE_CUSTOMERS . "`.`customersKundennummer`,

					`" . TABLE_CUSTOMERS . "`.`customersFirmenname`,
					
					`" . TABLE_CUSTOMERS . "`.`customersFirmennameZusatz`,
					`" . TABLE_CUSTOMERS . "`.`customersCompanyPLZ`,
					`" . TABLE_CUSTOMERS . "`.`customersCompanyOrt`,
					`" . TABLE_CUSTOMERS . "`.`customersCompanyStrasse`,
					`" . TABLE_CUSTOMERS . "`.`customersCompanyHausnummer`,
					
					`" . TABLE_CUSTOMERS . "`.`customersTelefon1`,
					`" . TABLE_CUSTOMERS . "`.`customersTelefon2`,
					`" . TABLE_CUSTOMERS . "`.`customersMobil1`,
					`" . TABLE_CUSTOMERS . "`.`customersMobil2`,
					`" . TABLE_CUSTOMERS . "`.`customersFax1`,
					`" . TABLE_CUSTOMERS . "`.`customersFax2`,
					`" . TABLE_CUSTOMERS . "`.`customersMail1`,
					`" . TABLE_CUSTOMERS . "`.`customersMail2`

					FROM `" . TABLE_SALESMEN . "`
					LEFT JOIN `" . TABLE_CUSTOMERS . "`
					ON(`" . TABLE_SALESMEN . "`.`salesmenKundennummer` = `" . TABLE_CUSTOMERS . "`.`customersKundennummer`)
					WHERE 1
						AND `salesmenAreas` != ''
						AND `salesmenActive` = '1'
						" . $where . "
			";

		$rs = $dbConnection->db_query($sql);

		$getRelationSalesmenZipcodeDatas = array();
		while($ds = mysqli_fetch_assoc($rs)) {
			$arrTemp = explode(';', $ds["salesmenAreas"]);

			if(!empty($arrTemp)){
				foreach($arrTemp as $thisPLZ) {
					if(preg_match("/\-/", $thisPLZ)){
						$arrTemp2 = explode('-', $thisPLZ);
						for($i = $arrTemp2[0] ;  $i < ($arrTemp2[1] + 1) ; $i++) {
							$thisPLZ2 = $i;
							if($i <	10){
								$thisPLZ2 = "0" . $i;
							}
							$getRelationSalesmenZipcodeDatas[$thisPLZ2][$ds["customersID"]]["kundenname"] = $ds["salesmenFirmenname"];
							$getRelationSalesmenZipcodeDatas[$thisPLZ2][$ds["customersID"]]["kundennummer"] = $ds["salesmenKundennummer"];
							$getRelationSalesmenZipcodeDatas[$thisPLZ2][$ds["customersID"]]["kundenID"] = $ds["customersID"];
							$getRelationSalesmenZipcodeDatas[$thisPLZ2][$ds["customersID"]]["kundenPLZ"] = $thisPLZ2;
							$getRelationSalesmenZipcodeDatas[$thisPLZ2][$ds["customersID"]]["kundenPLZaktiv"] = $ds["salesmenAreasActive"];
							
							$getRelationSalesmenZipcodeDatas[$thisPLZ2][$ds["customersID"]]["kundenPLZ2"] = $ds["customersCompanyPLZ"];
							$getRelationSalesmenZipcodeDatas[$thisPLZ2][$ds["customersID"]]["kundenOrt"] = $ds["customersCompanyOrt"];
						}
					}
					else {
						$getRelationSalesmenZipcodeDatas[$thisPLZ][$ds["customersID"]]["kundenname"] = $ds["salesmenFirmenname"];
						$getRelationSalesmenZipcodeDatas[$thisPLZ][$ds["customersID"]]["kundennummer"] = $ds["salesmenKundennummer"];
						$getRelationSalesmenZipcodeDatas[$thisPLZ][$ds["customersID"]]["kundenID"] = $ds["customersID"];
						$getRelationSalesmenZipcodeDatas[$thisPLZ][$ds["customersID"]]["kundenPLZ"] = $thisPLZ;
						$getRelationSalesmenZipcodeDatas[$thisPLZ][$ds["customersID"]]["kundenPLZaktiv"] = $ds["salesmenAreasActive"];
						
						$getRelationSalesmenZipcodeDatas[$thisPLZ][$ds["customersID"]]["kundenPLZ2"] = $ds["customersCompanyPLZ"];
						$getRelationSalesmenZipcodeDatas[$thisPLZ][$ds["customersID"]]["kundenOrt"] = $ds["customersCompanyOrt"];
					}
				}
			}
		}

		if($zipcode != ''){
			$arrTemp = $getRelationSalesmenZipcodeDatas[$zipcode];
			$getRelationSalesmenZipcodeDatas = array();
			$getRelationSalesmenZipcodeDatas[$zipcode] = $arrTemp;
		}
		ksort($getRelationSalesmenZipcodeDatas);
		#print_r($getRelationSalesmenZipcodeDatas);
		return $getRelationSalesmenZipcodeDatas;
	}
	// ---------------------------------

	function getOpenInvoices() {
		global $dbConnection;
		$sql_xxx = "SELECT
					IF(COUNT(`orderDocumentsID`) != 0, COUNT(`orderDocumentsID`), 0) AS `statusCount`,
					IF(SUM(`orderDocumentsTotalPrice`) != 0, SUM(`orderDocumentsTotalPrice`), 0) AS `statusTotalSum`,
					IF(`orderDocumentsStatus` = 0, `orderDocumentsStatus`, 1) AS `orderDocumentsStatus`

					FROM `" . TABLE_ORDER_INVOICES . "`

					WHERE `orderDocumentsStatus` = '1'

				UNION

				SELECT
					IF(COUNT(`orderDocumentsID`) != 0, COUNT(`orderDocumentsID`), 0) AS `statusCount`,
					IF(SUM(`orderDocumentsTotalPrice`) != 0, SUM(`orderDocumentsTotalPrice`), 0) AS `statusTotalSum`,
					IF(`orderDocumentsStatus` = 0, `orderDocumentsStatus`, 4) AS `orderDocumentsStatus`

					FROM `" . TABLE_ORDER_INVOICES . "`

					WHERE `orderDocumentsStatus` = '4'

			";

		$sql = "SELECT
					IF(COUNT(`orderDocumentsID`) != 0, COUNT(`orderDocumentsID`), 0) AS `statusCount`,
					IF(SUM(`orderDocumentsTotalPrice`) != 0, SUM(`orderDocumentsTotalPrice`), 0) AS `statusTotalSum`,
					`orderDocumentsStatus`

					FROM `" . TABLE_ORDER_INVOICES . "`

					WHERE 1
						AND (
							`orderDocumentsStatus` = '1'
							OR
							`orderDocumentsStatus` = '4'
							OR
							`orderDocumentsStatus` = '6'
							OR
							`orderDocumentsStatus` = '7'
							OR
							`orderDocumentsStatus` = '8'
							OR
							`orderDocumentsStatus` = '9'
							OR
							`orderDocumentsStatus` = '11'
							OR
							`orderDocumentsStatus` = '12'
						)

				UNION

					SELECT
						IF(COUNT(`orderDocumentsID`) != 0, COUNT(`orderDocumentsID`), 0) AS `statusCount`,
						IF(SUM(`orderDocumentsTotalPrice`) != 0, SUM(`orderDocumentsTotalPrice`), 0) AS `statusTotalSum`,
						IF(`orderDocumentsStatus` = 0, `orderDocumentsStatus`, 4) AS `orderDocumentsStatus`

						FROM `" . TABLE_ORDER_INVOICES . "`

						WHERE `orderDocumentsStatus` = '4'
			";

		$rs = $dbConnection->db_query($sql);

		$getOpenInvoiceDatas = array();
		while($ds = mysqli_fetch_assoc($rs)) {
			foreach(array_keys($ds) as $field) {
				$getOpenInvoiceDatas[$ds["orderDocumentsStatus"]][$field] = $ds[$field];
			}
		}
		return $getOpenInvoiceDatas;
	}

	// ---------------------------------

	function deleteFile($deleteFile='', $thisDocumentType='', $thisDocumentNumber='', $tableDocumentsMain='', $tableDocumentsDetails=''){
		global $dbConnection, $jswindowMessage, $warningMessage, $errorMessage, $successMessage, $infoMessage;

		$sql_1 = "DELETE
					`" . $tableDocumentsMain . "`,
					`" .  $tableDocumentsDetails . "`,
					`" . TABLE_CREATED_DOCUMENTS . "`


					FROM `" . $tableDocumentsMain . "`
					LEFT JOIN `" .  $tableDocumentsDetails . "`
					ON(`" . $tableDocumentsMain . "`.`orderDocumentsID` = `" .  $tableDocumentsDetails . "`.`orderDocumentDetailDocumentID`)
					LEFT JOIN `" . TABLE_CREATED_DOCUMENTS . "`
					ON(`" . $tableDocumentsMain . "`.`orderDocumentsNumber` = `" .  $tableDocumentsDetails . "`.`createdDocumentsNumber`)

					WHERE `" . $tableDocumentsMain . "`.`orderDocumentsNumber` = '" . $thisDocumentNumber . "'
			";
			echo $sql_1 . '<br />';
			// $rs_1 = $dbConnection->db_query($sql_1);

			$sql_2 = "DELETE FROM `" . TABLE_DOCUMENTS_TO_DOCUMENTS . "` WHERE `documentsToDocumentsCreatedDocumentNumber` = '" . $thisDocumentNumber . "'";
			echo $sql_2 . '<br />';
			// $rs_2 = $dbConnection->db_query($sql_2);
	}

	// ---------------------------------

	function getNewCustomerTaxAccountNumber($customerCountryID=81, $customerCountryCode, $customerType) {
		global $dbConnection, $arrCustomerTypeDatas;
		// KUNDEN-INLAND	:	10000 - 59999
		// KUNDEN-AUSLAND	:	60000 - 69999
		// KUNDEN-AMAZON	: 	eine Nummer (Inland) / customerType:  7 :10000
		// LAND-ID-INLAND	: 81
		// LAND-ID-AUSLAND	: !81

		DEFINE('CUSTOMER_TAX_ACCOUNT_NUMBER_HOME_COUNTRY_MIN', 10001);
		DEFINE('CUSTOMER_TAX_ACCOUNT_NUMBER_HOME_COUNTRY_MAX', 59999);

		DEFINE('CUSTOMER_TAX_ACCOUNT_NUMBER_FOREIGN_COUNTRY_MIN', 60000);
		DEFINE('CUSTOMER_TAX_ACCOUNT_NUMBER_FOREIGN_COUNTRY_MAX', 69999);

		DEFINE('CUSTOMER_TAX_ACCOUNT_NUMBER_HOME_AMZ', 10000);
		DEFINE('CUSTOMER_TAX_ACCOUNT_NUMBER_HOME_EBY', 10000);
		DEFINE('CUSTOMER_TAX_ACCOUNT_NUMBER_HOME_PRV', 10000);

		// if($customerType == '7'){
		if(in_array($customerType, array(7, 8, 12))){
			$thisCustomerNumberPrefix = $arrCustomerTypeDatas[$customerType]["customerTypesShortName"];
			$sql = "SELECT
							`customersTaxAccountID` AS `newCustomerTaxAccountNumber`
							FROM `" . TABLE_CUSTOMERS . "`
							WHERE `customersTyp` = 7
							LIMIT 1
					";
			#$rs = $dbConnection->db_query($sql);
			list($getNewCustomerTaxAccountNumber) = mysqli_fetch_array($rs);
			$getNewCustomerTaxAccountNumber = constant("CUSTOMER_TAX_ACCOUNT_NUMBER_HOME_" . $thisCustomerNumberPrefix);
		}
		else {
			if($customerCountryID == 81){
				// BOF FIND GAP
					$sql_findGap = "
							SELECT
								/*
								`customers_1`.`customersTaxAccountID` AS `customersTaxAccountID_beforeGap`,
								`customers_1`.`customersTaxAccountID` + 1 AS `customersTaxAccountID_fillGap`
								*/
								`customers_1`.`customersTaxAccountID` AS `newCustomerTaxAccountNumber`
							FROM `" . TABLE_CUSTOMERS . "` AS `customers_1`
							WHERE 1
								AND `customersCompanyCountry` = '81'
								AND (
									SELECT
										`customers_2`.`customersTaxAccountID`
										FROM `" . TABLE_CUSTOMERS . "` AS `customers_2`
										WHERE `customers_2`.`customersTaxAccountID` = `customers_1`.`customersTaxAccountID` + 1
								) IS NULL
							ORDER BY `customers_1`.`customersTaxAccountID`
							LIMIT 1;
						";
					$rs_findGap = $dbConnection->db_query($sql_findGap);
					list($getNewCustomerTaxAccountNumber) = mysqli_fetch_array($rs_findGap);
				// EOF FIND GAP

				// BOF GET MAX
					if($getNewCustomerTaxAccountNumber == ''){
						$sql = "SELECT
									MAX(`customersTaxAccountID`) AS `newCustomerTaxAccountNumber`
									FROM `" . TABLE_CUSTOMERS . "`
									WHERE `customersCompanyCountry` = '81'
							";
						$rs = $dbConnection->db_query($sql);
						list($getNewCustomerTaxAccountNumber) = mysqli_fetch_array($rs);
					}
				// EOF GET MAX

				if($getNewCustomerTaxAccountNumber == '0' || $getNewCustomerTaxAccountNumber == '') {
					$getNewCustomerTaxAccountNumber = CUSTOMER_TAX_ACCOUNT_NUMBER_HOME_COUNTRY_MIN;
				}
				else {
					$getNewCustomerTaxAccountNumber++;
				}
			}
			else {
				$sql = "SELECT
							MAX(`customersTaxAccountID`) AS `newCustomerTaxAccountNumber`
							FROM `" . TABLE_CUSTOMERS . "`
							WHERE `customersCompanyCountry` != '81'
					";
				$rs = $dbConnection->db_query($sql);
				list($getNewCustomerTaxAccountNumber) = mysqli_fetch_array($rs);

				if($getNewCustomerTaxAccountNumber == '0') {
					$getNewCustomerTaxAccountNumber = CUSTOMER_TAX_ACCOUNT_NUMBER_FOREIGN_COUNTRY_MIN;
				}
				else {
					$getNewCustomerTaxAccountNumber++;
				}
			}
		}
		return $getNewCustomerTaxAccountNumber;
	}

	// ---------------------------------

	function specialSubstr($string, $length=10) {
		$newString = $string;
		$newString = wordwrap($newString, $length, "###", false);
		$arrTemp = explode("###", $newString);
		$newString = $arrTemp[0];
		if(strlen($newString) < strlen($string)){
			$newString .= ' [...]';
		}
		return $newString;
	}

	// ---------------------------------

	function getRelatedDocuments($arrSearchDocumentNumbers, $arrRelatedDocumentNumbers=array()) {
		global $dbConnection, $arrRelatedDocumentNumbers, $arrGetUserRights;
		$thisDoctype = substr($arrSearchDocumentNumbers[0], 0, 2);
		if($thisDoctype == 'AN' || $thisDoctype == 'AB' || $thisDoctype == 'RE' || $thisDoctype == 'LS' || $thisDoctype == 'GU' || $thisDoctype == 'MA' || $thisDoctype == 'M1' || $thisDoctype == 'M2' || $thisDoctype == 'M3'){
			$sql = "
						SELECT
							`relatedDocuments_AN`,
							`relatedDocuments_AB`,
							`relatedDocuments_RE`,
							`relatedDocuments_LS`,
							`relatedDocuments_GU`,
							`relatedDocuments_MA`,
							`relatedDocuments_M1`,
							`relatedDocuments_M2`,
							`relatedDocuments_M3`,
							`relatedDocuments_collectiveABs`,

							`relatedDocuments_comment`,
							`relatedDocuments_SperrbetragFAKT`


							FROM `" . TABLE_RELATED_DOCUMENTS . "`

							WHERE 1
								AND `relatedDocuments_" . $thisDoctype. "` LIKE '%" . $arrSearchDocumentNumbers[0] . "%'
							LIMIT 1

			";

			/* `relatedDocuments_IK` */
			#echo $sql;
			$rs = $dbConnection->db_query($sql);
			list(
				$arrRelatedDocumentNumbers["AN"],
				$arrRelatedDocumentNumbers["AB"],
				$arrRelatedDocumentNumbers["RE"],
				$arrRelatedDocumentNumbers["LS"],
				$arrRelatedDocumentNumbers["GU"],
				$arrRelatedDocumentNumbers["MA"],
				$arrRelatedDocumentNumbers["M1"],
				$arrRelatedDocumentNumbers["M2"],
				$arrRelatedDocumentNumbers["M3"],
				$arrRelatedDocumentNumbers["collectiveABs"],

				$arrRelatedDocumentNumbers["comment"],
				$arrRelatedDocumentNumbers["SperrbetragFAKT"]
			) = mysqli_fetch_array($rs);

			/* $arrRelatedDocumentNumbers["IK"], */
			$arrIsFilled = false;
			foreach($arrRelatedDocumentNumbers as $thisKey => $thisValue){
				if($thisValue != ''){ $arrIsFilled = true; }
			}
			if(!$arrIsFilled){ $arrReturn = null;}
			else {$arrReturn = $arrRelatedDocumentNumbers; }
			return $arrReturn;
		}
	}

	function getRelatedDocumentsComment($arrSearchDocumentNumbers, $arrRelatedDocumentNumbers=array()) {
		global $dbConnection, $arrRelatedDocumentNumbers, $arrGetUserRights;
		$thisDoctype = substr($arrSearchDocumentNumbers[0], 0, 2);
		if($thisDoctype == 'AN' || $thisDoctype == 'AB' || $thisDoctype == 'RE' || $thisDoctype == 'LS' || $thisDoctype == 'GU' || $thisDoctype == 'MA' || $thisDoctype == 'M1' || $thisDoctype == 'M2' || $thisDoctype == 'M3'){
			$sql = "
						SELECT
							IF(`relatedDocuments_comment` IS NULL, '', `relatedDocuments_comment`) AS `relatedDocuments_comment`

							FROM `" . TABLE_RELATED_DOCUMENTS . "`

							WHERE 1
								AND `relatedDocuments_" . $thisDoctype. "` LIKE '%" . $arrSearchDocumentNumbers[0] . "%'
							LIMIT 1

			";

			/* `relatedDocuments_IK` */
			#echo $sql;
			$rs = $dbConnection->db_query($sql);
			list(
				$relatedDocumentsComment
			) = mysqli_fetch_array($rs);

			return $relatedDocumentsComment;
		}
	}

	function xxx_getRelatedDocuments($arrSearchDocumentNumbers, $arrRelatedDocumentNumbers=array()) {
		global $dbConnection, $arrRelatedDocumentNumbers;

		if(empty($arrRelatedDocumentNumbers)) {
			foreach($arrSearchDocumentNumbers as $thisSearchDocumentNumber) {
				$arrRelatedDocumentNumbers[substr($thisSearchDocumentNumber, 0, 2)] = $thisSearchDocumentNumber;
			}
		}

		if(!empty($arrSearchDocumentNumbers)){
			foreach($arrSearchDocumentNumbers as $thisSearchDocumentNumber) {
				$sql = "SELECT
							`documentsToDocumentsCreatedDocumentNumber` AS `foundDocumentNumber`
							FROM `" . TABLE_DOCUMENTS_TO_DOCUMENTS . "`
							WHERE `documentsToDocumentsOriginDocumentNumber` = '" . $thisSearchDocumentNumber . "'
								AND `documentsToDocumentsRelationType` = 'convert'

						UNION

						SELECT
							`documentsToDocumentsOriginDocumentNumber` AS `foundDocumentNumber`
							FROM `" . TABLE_DOCUMENTS_TO_DOCUMENTS . "`
							WHERE `documentsToDocumentsCreatedDocumentNumber` = '" . $thisSearchDocumentNumber . "'
								AND `documentsToDocumentsRelationType` = 'convert'
					";
				$rs = $dbConnection->db_query($sql);

				$arrTempDocumentNumbers = array();
				if($dbConnection->db_getMysqlNumRows($rs) > 0) {
					while($ds = mysqli_fetch_array($rs)){
						if(!in_array($ds["foundDocumentNumber"], $arrRelatedDocumentNumbers)){
							$arrTempDocumentNumbers[substr($ds["foundDocumentNumber"], 0, 2)] = $ds["foundDocumentNumber"];
						}
					}

					if(!empty($arrTempDocumentNumbers)){
						$arrTempDocumentNumbers = array_unique($arrTempDocumentNumbers);
						$arrRelatedDocumentNumbers = array_merge($arrRelatedDocumentNumbers, $arrTempDocumentNumbers);
						$arrRelatedDocumentNumbers = array_unique($arrRelatedDocumentNumbers);
						getRelatedDocuments($arrTempDocumentNumbers, $arrRelatedDocumentNumbers);
					}
					else {
						return $arrRelatedDocumentNumbers;
					}
				}
				else {
					return $arrRelatedDocumentNumbers;
				}
			}
		}
		return $arrRelatedDocumentNumbers;
	}

	// ---------------------------------

	function logSendedMails($sendedMailsRecipient, $sendedMailsSubject, $sendedMailsIsSended, $sendedMailsDocumentType, $sendedMailsDocumentNumber, $sendedMailsSender, $sendedMailsContent='') {
		global $dbConnection, $_SESSION, $_COOKIE;

		$sql = "
				INSERT INTO `" . TABLE_SENDED_MAILS . "` (
						
						`sendedMailsMandatory`,
						`sendedMailsRecipient`,
						`sendedMailsSender`,
						`sendedMailsSendTime`,
						`sendedMailsSubject`,
						`sendedMailsContent`,
						`sendedMailsDocumentType`,
						`sendedMailsDocumentNumber`,
						`sendedMailsUser`,
						`sendedMailsIsSended`
					)
					VALUES (
						
						'" . MANDATOR . "',
						'".$sendedMailsRecipient."',
						'".$sendedMailsSender."',
						NOW(),
						'".addslashes($sendedMailsSubject)."',
						'".addslashes($sendedMailsContent)."',
						'".$sendedMailsDocumentType."',
						'".$sendedMailsDocumentNumber."',
						'".$_SESSION["usersID"]."',
						'".$sendedMailsIsSended."'
					)
				";

		$rs = $dbConnection->db_query($sql);
	}

	// ---------------------------------

	function valueSign($num){
		return $sign = $num < 0 ? -1 : ( $num > 0 ? 1 : 0 );
		//or
		#return $sign = $num ? $num / abs($num) : 0;
	}
	// ---------------------------------
	function formatLoadedDeliveryDatas($strDatas, $separator) {
		if($separator == '#'){
			$arrUseContent = explode("#", $strDatas);
			$arrUseDeliveryData = array();
			foreach($arrUseContent as $thisUseContent){
				if(preg_match("/[0-9]{4}-[0-9]{2}-[0-9]{2}/", $thisUseContent)){
					$arrUseDeliveryData["DATE"] = $thisUseContent;
				}
				if(preg_match("/Zustellung an: /", $thisUseContent)){
					$arrUseDeliveryData["NAME"] = preg_replace("/Zustellung an: /", "", $thisUseContent);
				}
				else if(preg_match("/Paketabholung im DPD Paket-Shop/", $thisUseContent)){
					$arrUseDeliveryData["NAME"] = "Paketabholung im DPD Paket-Shop";
				}
				else if(preg_match("/Paketzustellung im DPD Paket-Shop/", $thisUseContent)){
					$arrUseDeliveryData["NAME"] = "Paketzustellung im DPD Paket-Shop";
				}
				else if(preg_match("/Zustellung/", $thisUseContent)){
					$arrUseDeliveryData["NAME"] = "Zustellung ohne Name";
				}
			}
			$formattedDeliveryData = implode("#", $arrUseDeliveryData);
		}
		else if($separator == '|'){
			$arrUseContent = explode("|", $strDatas);
			$arrUseDeliveryData["DATE"] = $arrUseContent[0];
			$arrUseDeliveryData["NAME"] = $arrUseContent[1];
			$formattedDeliveryData = implode("#", $arrUseDeliveryData);
		}

		return $formattedDeliveryData;
	}
	// ---------------------------------
	function formatStreet($string){
		$newString = $string;

		$newString = preg_replace("/\./", ". ", $newString);
		$newString = preg_replace("/\. $/", ".", $newString);
		$newString = preg_replace("/  /", " ", $newString);

		$newString = preg_replace("/ - /", "-", $newString);
		$newString = preg_replace("/- /", "-", $newString);
		$newString = preg_replace("/ -/", "-", $newString);
		$newString = preg_replace("/ Strasse$/", " Str.", $newString);
		$newString = preg_replace("/ Straße$/", "-Str.", $newString);
		$newString = preg_replace("/-Strasse$/", "-Str.", $newString);
		$newString = preg_replace("/-Straße$/", " Str.", $newString);
		$newString = preg_replace("/strasse$/", "str.", $newString);
		$newString = preg_replace("/straße$/", "str.", $newString);
		$newString = preg_replace("/(Str|str)$/", "$1.", $newString);

		return $newString;
	}

	// ---------------------------------
	function getPhoneCodes($arrZipcodes){
		global $dbConnection, $_SESSION, $_COOKIE;

		$arrPhoneCodes = array();
		if(!empty($arrZipcodes)){

			$arrWhere = array();
			foreach($arrZipcodes as $thisZipcodeKey => $thisZipcodeData){
				if($thisZipcodeData["countryID"] == '81'){
					$arrWhere[] = " `" . TABLE_CITIES_PHONECODE . "`.`zip` = '" . $thisZipcodeData["zipCode"] . "' ";
				}
			}
			if(!empty($arrWhere)){
				$sql = "
						SELECT
							`" . TABLE_CITIES_PHONECODE . "`.`zip`,
							`" . TABLE_CITIES_PHONECODE . "`.`phonecode`
							FROM
							`" . TABLE_CITIES_PHONECODE . "`
							WHERE 1
								AND ( " . implode(" OR ", $arrWhere) . " )
					";
				$rs = $dbConnection->db_query($sql);
				while($ds = mysqli_fetch_assoc($rs)){
					$arrPhoneCodes[$ds["zip"]] = preg_replace('/^0/', '', $ds["phonecode"]);
				}
			}
		}
		return $arrPhoneCodes;
	}
	// ---------------------------------
	function getCountryPhoneCode($countryID){
		global $dbConnection, $_SESSION, $_COOKIE;
		$arrCountryPhoneCode = array();

		$sql = "SELECT
						`" . TABLE_COUNTRIES . "`.`countries_id`,
						`" . TABLE_COUNTRIES . "`.`countries_name_EN`,
						`" . TABLE_COUNTRIES . "`.`countries_name_DE`,
						`" . TABLE_COUNTRIES . "`.`countries_iso_code_2`,
						`" . TABLE_COUNTRIES . "`.`countries_iso_code_3`,
						`" . TABLE_COUNTRIES . "`.`address_format_id`,
						`" . TABLE_COUNTRIES . "`.`status`,
						`" . TABLE_COUNTRIES . "`.`isEU`,

						`" . TABLE_PHONE_AREA_CODES . "`.`phoneAreaCodesID`,
						`" . TABLE_PHONE_AREA_CODES . "`.`phoneAreaCodesCountry`,
						`" . TABLE_PHONE_AREA_CODES . "`.`phoneAreaCodesCountryCode`,
						`" . TABLE_PHONE_AREA_CODES . "`.`phoneAreaCodesPhoneCode`,
						`" . TABLE_PHONE_AREA_CODES . "`.`phoneAreaCodesTopLevelDomain`

					FROM `" . TABLE_COUNTRIES . "`

					LEFT JOIN  `" . TABLE_PHONE_AREA_CODES . "`
					ON(`" . TABLE_COUNTRIES . "`.`countries_iso_code_2` = `" . TABLE_PHONE_AREA_CODES . "`.`phoneAreaCodesTopLevelDomain` )

					WHERE 1
						AND `" . TABLE_COUNTRIES . "`. `countries_id` = " . $countryID . "

					LIMIT 1
			";

		$rs = $dbConnection->db_query($sql);
		while($ds = mysqli_fetch_assoc($rs)){
			foreach(array_keys($ds) as $field){
				$arrCountryPhoneCode[$field] = $ds[$field];
			}
		}

		return $arrCountryPhoneCode;
	}
	// ---------------------------------

	function getEkPrice($productID, $productQuantity){
		global $dbConnection, $dbConnection_Extern, $errorMessage;

		// BOF GET PRODUCT EK-PRICES
				$productEkPreis = 0;
				$sql_getEkPrice = "
						SELECT
							`" . TABLE_PERSONAL_OFFERS_BY_CUSTOMERS . COST_PRICE_GROUP . "`.`personal_offer` AS `productEkPreis`

							FROM (
								SELECT
									`" . TABLE_PRODUCTS . "`.`products_id` AS `products_id`
									FROM `" . TABLE_PRODUCTS . "`
									WHERE `" . TABLE_PRODUCTS . "`.`products_model` = '" . $productID . "'
										AND `" . TABLE_PRODUCTS . "`.`products_status` = 1

									GROUP BY `" . TABLE_PRODUCTS . "`.`products_id`

								UNION

								SELECT
									`" . TABLE_PRODUCTS_ATTRIBUTES . "`.`products_id`
									FROM `" . TABLE_PRODUCTS_ATTRIBUTES . "`
									LEFT JOIN `" . TABLE_PRODUCTS . "`
									ON (`" . TABLE_PRODUCTS_ATTRIBUTES . "`.`products_id` = `" . TABLE_PRODUCTS . "`.`products_id`)
									WHERE `" . TABLE_PRODUCTS_ATTRIBUTES . "`.`attributes_model` = '" . $productID . "'
										AND `" . TABLE_PRODUCTS . "`.`products_status` = 1

									GROUP BY `" . TABLE_PRODUCTS_ATTRIBUTES . "`.`products_id`
							) AS `temp`

							LEFT JOIN `" . TABLE_PERSONAL_OFFERS_BY_CUSTOMERS . COST_PRICE_GROUP . "`
							ON(`temp`.`products_id` = `" . TABLE_PERSONAL_OFFERS_BY_CUSTOMERS . COST_PRICE_GROUP . "`.`products_id`)

							WHERE  `" . TABLE_PERSONAL_OFFERS_BY_CUSTOMERS . COST_PRICE_GROUP . "`.`quantity` <= '" . $productQuantity . "'
								AND `" . TABLE_PERSONAL_OFFERS_BY_CUSTOMERS . COST_PRICE_GROUP . "`.`quantity` > 0
								AND `" . TABLE_PERSONAL_OFFERS_BY_CUSTOMERS . COST_PRICE_GROUP . "`.`personal_offer` > 0

							LIMIT 1
					";
				$rs_getEkPrice = $dbConnection_Extern->db_query($sql_getEkPrice);

				list($productEkPreis) = mysqli_fetch_array($rs_getEkPrice);
				if($productEkPreis == '' || $productEkPreis == NULL){$productEkPreis = 0;}

				if($dbConnection_Extern->db_displayErrors() != "") {
					$errorMessage .= $dbConnection_Extern->db_displayErrors(). '<br />';
				}

				return $productEkPreis;
			// EOF GET PRODUCT EK-PRICES
		}
	// ---------------------------------
	function getDoubleZipcodes($mode='1') {
		global $dbConnection, $_SESSION, $_COOKIE;

		$where = "";
		if($mode == '1' || $mode == ''){
			$where = " AND `zipcodesDoubleActive` = '1' ";
		}
		else if($mode == '0'){
			$where = " AND `zipcodesDoubleActive` != '1' ";
		}
		else if($mode == 'ALL'){
			$where = "";
		}

		$arrDoubleZipcodes = array();

		$sql = "
				SELECT
					 `zipcodesDoubleZipcode`,
					 `zipcodesDoubleDescription`

				FROM `" . TABLE_ZIPCODES_DOUBLE . "`

				WHERE 1
					" . $where . "
				ORDER BY `zipcodesDoubleZipcode`
			";
		//echo $sql;
		$rs = $dbConnection->db_query($sql);
		while($ds = mysqli_fetch_assoc($rs)){
			foreach(array_keys($ds) as $field){
				$arrDoubleZipcodes[$ds["zipcodesDoubleZipcode"]][$field] = $ds[$field];
			}
		}

		return $arrDoubleZipcodes;
	}
	// ---------------------------------

	####
	/*
	// BOF GET SALESMAN
		$sql = "
			SELECT
				`personnelFirstName`,
				`personnelLastName`

				FROM `" . TABLE_PERSONNEL . "`

				WHERE 1
					AND `personnelGroup` = 7
		";

		$rs = $dbConnection->db_query($sql);

		$arrSalesmanDatas = array();
		while($ds = mysqli_fetch_assoc($rs)) {
			$arrSalesmanDatas[] = $ds["personnelFirstName"] . " " . $ds["personnelLastName"];
		}
		// EOF GET SALESMAN
	*/
	####

	function getPersonnelDatas($personnelGroup=''){
		global $dbConnection;

		$where = "";
		if((int)$personnelGroup > 0){
			$where .= " AND `personnelGroup` = " . $personnelGroup . "";
		}

		$sql = "SELECT
					`" . TABLE_PERSONNEL . "`.`personnelID`,
					`" . TABLE_PERSONNEL . "`.`personnelFirstName`,
					`" . TABLE_PERSONNEL . "`.`personnelLastName`,
					`" . TABLE_PERSONNEL . "`.`personnelGroup`,
					`" . TABLE_PERSONNEL . "`.`personnelActive`,
				IF(`personnelActive` = '1', 'aktiv', 'nicht aktiv') AS `personnelActiveText`

				FROM `" . TABLE_PERSONNEL . "`

				LEFT JOIN `" . TABLE_PERSONNEL_TYPES . "`
				ON(`" . TABLE_PERSONNEL . "`.`personnelGroup` = `" . TABLE_PERSONNEL_TYPES . "`.`personnelTypesID`)

				WHERE 1

				" . $where . "

				ORDER BY
					`" . TABLE_PERSONNEL . "`.`personnelActive` DESC,
					`" . TABLE_PERSONNEL_TYPES . "`.`personnelTypesSort`,
					`" . TABLE_PERSONNEL . "`.`personnelLastName`,
					`" . TABLE_PERSONNEL . "`.`personnelFirstName`
		";
		$rs = $dbConnection->db_query($sql);
		$arrGetPersonnelDatas = array();
		while($ds = mysqli_fetch_assoc($rs)) {
			foreach(array_keys($ds) as $field) {
				$arrGetPersonnelDatas[$ds["personnelID"]][$field] = $ds[$field];
			}
		}

		return $arrGetPersonnelDatas;
	}

	// ---------------------------------

	function displayMessages(){
		global $warningMessage, $errorMessage, $successMessage, $infoMessage;
		if($warningMessage != "") {	echo '<p class="warningArea">'.$warningMessage.'</p>'; }
		if($errorMessage != "") {	echo '<p class="errorArea">'.$errorMessage.'</p>'; }
		if($successMessage != "") {	echo '<p class="successArea">'.$successMessage.'</p>'; }
		if($infoMessage != "") {	echo '<p class="infoArea">'.$infoMessage.'</p>'; }
		$warningMessage = '';
		$errorMessage = '';
		$successMessage = '';
		$infoMessage = '';
	}
	// ---------------------------------

	function generateCustomerNumber($circleType, $customersType, $customersCountry, $customersPLZ){
		global $dbConnection, $arrCountryTypeDatas, $arrCustomerTypeDatas;
		$circleTypeDatasTableName = '';
		$generatedCustomerNumber = "";
		#echo '<hr />test: '.$circleType.', '.$customersType.', '.$customersCountry.', '.$customersPLZ . '<hr />';
		$circleTypeDatasTableName = constant("TABLE_" . strtoupper($circleType));

		$where = "";
		$thisNumberPrefix = "";

		if($circleType == 'customers' || $circleType == 'suppliers'){
			if($circleType == 'customers'){
				// BOF CUSTOMERS
					// BOF NON AMAZON / EBAY - CUSTOMERS
					// if($customersType != 7 && $customersType != 8) {
					if(!in_array($customersType, array(7, 8, 12))) {
						// BOF GERMAN CUSTOMERS
						if($customersCountry == '81') {
							$where .= " AND `possibleCustomerNumber` LIKE '" . substr($customersPLZ, 0, 2) . "%' ";
							$thisNumberPrefix = "";
						}
						// EOF GERMAN CUSTOMERS

						// BOF NON GERMAN CUSTOMERS
						else if($customersCountry != '81') {
							$where .= "";
							$thisNumberPrefix = $arrCountryTypeDatas[$customersCountry]["countries_iso_code_2"] . '-';
						}
						// EOF NON GERMAN CUSTOMERS
					}
					// EOF NON AMAZON / EBAY - CUSTOMERS

					// BOF AMAZON / EBAY - CUSTOMERS
					else {
						$where .= "";
						$thisNumberPrefix = $arrCustomerTypeDatas[$customersType]["customerTypesShortName"];
					}
					// EOF AMAZON / EBAY - CUSTOMERS

				// EOF CUSTOMERS
			}
			else if($circleType == 'suppliers'){
				// BOF SUPPLIERS
				$where .= " AND `possibleCustomerNumber`  LIKE '" . substr($customersPLZ, 0, 2) . "%' ";
				$thisNumberPrefix = PREFIX_SUPPLIER_NUMBER;
				// EOF SUPPLIERS
			}

			$sql = "
				SELECT
					MIN(`tempTable`.`possibleCustomerNumber`) AS `searchResult`
					FROM (
						SELECT
							`" . TABLE_POSSIBLE_CUSTOMER_NUMBERS . "`.`possibleCustomerNumber`,
							`" . $circleTypeDatasTableName . "`.`" . strtolower($circleType) . "Kundennummer`

							FROM `" . TABLE_POSSIBLE_CUSTOMER_NUMBERS . "`

							LEFT JOIN `" . $circleTypeDatasTableName . "`
							ON (CONCAT('" . $thisNumberPrefix . "', `" . TABLE_POSSIBLE_CUSTOMER_NUMBERS . "`.`possibleCustomerNumber`) = `" . $circleTypeDatasTableName . "`.`" . strtolower($circleType) . "Kundennummer`)

							WHERE 1 " . $where . "

							HAVING	`" . $circleTypeDatasTableName . "`.`" . strtolower($circleType) . "Kundennummer` IS NULL
					) AS `tempTable`
				";
			# /* ORDER BY `" . TABLE_POSSIBLE_CUSTOMER_NUMBERS . "`.`possibleCustomerNumber` */

			#echo $sql;
			$rs = $dbConnection->db_query($sql);
			list($searchResult) = mysqli_fetch_array($rs);
			$generatedCustomerNumber = $thisNumberPrefix . $searchResult;
		}
		return $generatedCustomerNumber;
	}

	function getCustomerCalendarDates($date){
		global $dbConnection;
		$arrCustomerCalendarDatesDatas = array();
		$dayDiff = 2;

		$sql = "SELECT
					`customersCalendarID`,
					`customersCalendarCustomersID`,
					`customersCalendarCustomersNumber`,
					`customersCalendarDate`,
					`customersCalendarDateRepeat`,
					`customersCalendarRepeatType`,
					`customersCalendarRepeatValue`,
					`customersCalendarDescription`,
					`customersCalendarReminderMailSended`,
					`customersCalendarUserID`

				FROM `" . TABLE_CUSTOMERS_CALENDAR . "`

				WHERE 1
					AND '" . $date . "' < DATE_ADD(`customersCalendarDate`,INTERVAL " . $dayDiff . " DAY)
					AND '" . $date . "' > DATE_SUB(`customersCalendarDate`,INTERVAL " . $dayDiff . " DAY)

					ORDER BY `customersCalendarDate` DESC
			";

		$rs = $dbConnection->db_query($sql);
		while($ds = mysqli_fetch_assoc($rs)){
			foreach(array_keys($ds) as $field){
				$arrCustomerCalendarDatesDatas[$ds["customersCalendarID"]][$field] = $ds[$field];
			}
		}

		return $arrCustomerCalendarDatesDatas;
	}

	function substringChars($string, $stringStart, $stringEnd, $mode=0){
		$newsString = $string;
		$encodingString = mb_detect_encoding($newsString);
		if($encodingString == "UTF-8"){
			$stringEnd = $stringEnd + 1;
		}
		$mode = 0;
		if($mode == 1){
			if($encodingString == "UTF-8"){
				$newsString = utf8_encode($newsString);
			}
			// utf8_decode
			$newsString = html_entity_decode($newsString);
			$newsString = substr($newsString, $stringStart, $stringEnd);
		}
		else {
			if (preg_match('!^([^&]|&(?:.*?;)){' . $stringStart . ',' . $stringEnd .'}!s', $newsString, $match)) {
				$newsString = $match[0];
			}
		}
		return $newsString;
	}

	function getDownloadFileSubCategories($documentType){
		global $dbConnection;
		$arrDownloadFileSubCategories = array();

		$sql = "SELECT
					`downloadFilesSubCategoriesID`,
					`downloadFilesSubCategoriesName`,
					`downloadFilesSubCategoriesActive`,
					`downloadFilesSubCategoriesSort`,
					`downloadFilesSubCategoriesCategory`

					FROM `" . TABLE_DOWNLOAD_FILES_SUBCATEGORIES . "`

					WHERE 1
						AND `downloadFilesSubCategoriesActive` = '1'
						AND `downloadFilesSubCategoriesCategory` = '" . $documentType . "'

					ORDER BY `downloadFilesSubCategoriesSort`

			";
		$rs = $dbConnection->db_query($sql);
		while($ds = mysqli_fetch_assoc($rs)){
			foreach(array_keys($ds) as $field){
				$arrDownloadFileSubCategories[$ds["downloadFilesSubCategoriesID"]][$field] = $ds[$field];
			}
		}

		return $arrDownloadFileSubCategories;
	}

	function sendMailStatusChanges($customerNumber, $changeStatusID, $storedStatusID, $orderID){
		global $dbConnection, $errorMessage, $warningMessage, $infoMessage, $successMessage, $arrGetUserRights;

		// BOF GET ORDER DATAS FROM CHANGED STATUS
		$sql = "SELECT
					`" . TABLE_ORDERS . "`.`ordersID`,
					`" . TABLE_ORDERS . "`.`ordersBestellDatum`,
					`" . TABLE_ORDERS . "`.`ordersKundennummer`,
					`" . TABLE_ORDERS . "`.`ordersKundenName`,
					`" . TABLE_ORDERS . "`.`ordersKommission`,
					`" . TABLE_ORDERS . "`.`ordersInfo`,
					`" . TABLE_ORDERS . "`.`ordersPLZ`,
					`" . TABLE_ORDERS . "`.`ordersOrt`,
					`" . TABLE_ORDERS . "`.`ordersLand`,
					`" . TABLE_ORDERS . "`.`ordersArtikelID`,
					`" . TABLE_ORDERS . "`.`ordersArtikelNummer`,
					`" . TABLE_ORDERS . "`.`ordersArtikelBezeichnung`,
					`" . TABLE_ORDERS . "`.`ordersArtikelKategorieID`,
					`" . TABLE_ORDERS . "`.`ordersArtikelMenge`,
					`" . TABLE_ORDERS . "`.`ordersArtikelPrintColorsCount`,
					`" . TABLE_ORDERS . "`.`ordersArtikelWithBorder`,
					`" . TABLE_ORDERS . "`.`ordersAufdruck`,
					`" . TABLE_ORDERS . "`.`ordersAdditionalArtikelID`,
					`" . TABLE_ORDERS . "`.`ordersAdditionalArtikelNummer`,
					`" . TABLE_ORDERS . "`.`ordersAdditionalArtikelBezeichnung`,
					`" . TABLE_ORDERS . "`.`ordersAdditionalArtikelKategorieID`,
					`" . TABLE_ORDERS . "`.`ordersAdditionalArtikelMenge`,
					`" . TABLE_ORDERS . "`.`ordersDruckFarbe`,
					`" . TABLE_ORDERS . "`.`ordersAdditionalCosts`,
					`" . TABLE_ORDERS . "`.`ordersAdditionalCostsID`,
					`" . TABLE_ORDERS . "`.`ordersVertreter`,
					`" . TABLE_ORDERS . "`.`ordersVertreterID`,
					`" . TABLE_ORDERS . "`.`ordersMailAdress`,
					`" . TABLE_ORDERS . "`.`ordersMandant`,
					`" . TABLE_ORDERS . "`.`ordersPerExpress`,
					`" . TABLE_ORDERS . "`.`ordersNotizen`,
					`" . TABLE_ORDERS . "`.`ordersUserID`,
					`" . TABLE_ORDERS . "`.`ordersDirectSale`,
					`" . TABLE_ORDERS . "`.`ordersReklamationsgrund`,

					`" . TABLE_CUSTOMERS . "`.`customersID`,
					`" . TABLE_CUSTOMERS . "`.`customersKundennummer`,
					`" . TABLE_CUSTOMERS . "`.`customersFirmenname`,
					`" . TABLE_CUSTOMERS . "`.`customersFirmennameZusatz`,
					`" . TABLE_CUSTOMERS . "`.`customersMail1`,
					`" . TABLE_CUSTOMERS . "`.`customersMail2`,
					`" . TABLE_CUSTOMERS . "`.`customersTyp`

				FROM `" . TABLE_ORDERS . "`

				LEFT JOIN `" . TABLE_CUSTOMERS . "`
				ON(`" . TABLE_ORDERS . "`.`ordersVertreterID` = `" . TABLE_CUSTOMERS . "`.`customersID`)

				LEFT JOIN `" . TABLE_SALESMEN . "`
				ON(`" . TABLE_CUSTOMERS . "`.`customersKundennummer` = `" . TABLE_SALESMEN . "`.`salesmenKundennummer`)

				WHERE 1
					AND `" . TABLE_ORDERS . "`.`ordersID` = '" . $orderID . "'
					AND `" . TABLE_CUSTOMERS . "`.`customersTyp` = '9'
					AND `" . TABLE_SALESMEN . "`.`salesmenActive` = '1'
			";

		$rs = $dbConnection->db_query($sql);
		$arrOrderChangedStatusDatas = array();
		$arrMailRecipients = array();
		while($ds = mysqli_fetch_assoc($rs)){
			if($ds["customersMail1"] != ""){ $arrMailRecipients[] = $ds["customersMail1"]; }
			if($ds["customersMail2"] != ""){ $arrMailRecipients[] = $ds["customersMail2"]; }

			foreach(array_keys($ds) as $field){
				$arrOrderChangedStatusDatas[$field] = $ds[$field];
			}
		}
		// EOF GET ORDER DATAS FROM CHANGED STATUS

		// BOF GET STATUS TYPES
		$arrOrderStatusTypData = getOrderStatusTypes();
		// BOF GET STATUS TYPES

		#$arrMailRecipients[] = "webmaster@burhan-ctr.de";

		// BOF SEND CHANGED STATUS-NOTICE
		require_once(DIRECTORY_HTML_MIME_MAIL . 'htmlMimeMail.php');
		require_once(DIRECTORY_MAIL_TEMPLATES . 'template_BASIC.inc.php');
		require_once(DIRECTORY_MAIL_TEMPLATES . 'template_TEXT_DATAS.inc.php');
		require_once("classes/createMail.class.php");
		if(!empty($arrOrderChangedStatusDatas) && !empty($arrMailRecipients)){
			// BOF SEND MAIL TO CUSTOMER
			$thisMailSubject = 'Statusänderung' . ' ' . $arrOrderChangedStatusDatas["ordersKundennummer"] . ' - ' . $arrOrderChangedStatusDatas["ordersKundenName"];

			$thisMailRecipients = implode(";", $arrMailRecipients);

			$thisMailAdditionalText = '';
			$thisMailAdditionalText = '
				<p>
				Der Status
				vom Kunden ' . $arrOrderChangedStatusDatas["ordersKundenName"] . ' ' .
				'(Kundennummer ' . $arrOrderChangedStatusDatas["ordersKundennummer"] . ') ' .
				'wurde von ' . $arrOrderStatusTypData[$storedStatusID]["orderStatusTypesName"] . ' ' .
				'auf ' . $arrOrderStatusTypData[$changeStatusID]["orderStatusTypesName"] . ' ' .
				'gesetzt.</p>
				<p>
					Bestelldatum: ' . formatDate($arrOrderChangedStatusDatas["ordersBestellDatum"], "display") . '<br />
					Artikel: ' . $arrOrderChangedStatusDatas["ordersArtikelBezeichnung"] . '<br />
					Menge: ' . $arrOrderChangedStatusDatas["ordersArtikelMenge"] . '<br />
				</p>
			';

			unset($createMail);
			$createMail = new createMail(
								$arrMailContentDatas["SC"]["MAIL_TITLE"] . ' ' . $thisMailSubject,			// TEXT:	MAIL_SUBJECT
								'SC',																		// STRING:	DOCUMENT TYPE
								$generatedDocumentNumber,													// STRING:	DOCUMENT NUMBER
								null,																		// ARRAY:	ATTACHMENT FILES
								$thisMailRecipients,	 													// STRING:	RECIPIENTS
								$arrMailContentDatas["SC"],													// MAIL_TEXT_TEMPLATE
								$mailTemplate["html"],														// MAIL_BODY_TEMPLATE
								$thisMailAdditionalText,																			// STRING:	ADDITIONAL TEXT
								'',																			// STRING:	SEND ATTACHED TEXT
								false,																		// BOOLEAN:	SEND ATTACHED DOCUMENT
								DIRECTORY_MAIL_IMAGES														// DIRECTORY_MAIL_IMAGES
							);

			$createMailResult = $createMail->returnResult();
			$sendMailStatusChangeToAgent = $createMailResult;

			if(isset($sendMailStatusChangeToAgent)) {
				if(!$sendMailStatusChangeToAgent) { $errorMessage .= ' Die Status-&Auml;nderung konnte nicht per Mail an ' . $thisMailRecipients . ' gesendet werden.' .'<br />'; }
				else { $successMessage .= ' Die Status-&Auml;nderung wurde per Mail an ' . $thisMailRecipients . ' gesendet.' .'<br />'; }
			}
			// EOF SEND MAIL TO CUSTOMER
		}
		// EOF SEND CHANGED STATUS-NOTICE
	}

	function getRomanNumerals($num){
		$n = intval($num);
		$res = '';

		/*** roman_numerals array  ***/
		$roman_numerals = array(
			'M'  => 1000,
			'CM' => 900,
			'D'  => 500,
			'CD' => 400,
			'C'  => 100,
			'XC' => 90,
			'L'  => 50,
			'XL' => 40,
			'X'  => 10,
			'IX' => 9,
			'V'  => 5,
			'IV' => 4,
			'I'  => 1
		);

		foreach ($roman_numerals as $roman => $number){
			/*** divide to get  matches ***/
			$matches = intval($n / $number);

			/*** assign the roman char * $matches ***/
			$res .= str_repeat($roman, $matches);

			/*** substract from the number ***/
			$n = $n % $number;
		}

		/*** return the res ***/
		return $res;
	}
	function getLayoutFiles($arrCustomerNumbers=array()){
		global $dbConnection, $errorMessage, $warningMessage, $infoMessage, $successMessage;

		$arrLayoutPdfDatas = array();

		if(!empty($arrCustomerNumbers)){
			$arrSql = array();
			foreach($arrCustomerNumbers as $thisCustomerNumbers){
				$arrSql[] = "
						SELECT
							`" . TABLE_CREATED_DOCUMENTS . "`.`createdDocumentsID`,
							`" . TABLE_CREATED_DOCUMENTS . "`.`createdDocumentsType`,
							`" . TABLE_CREATED_DOCUMENTS . "`.`createdDocumentsNumber`,
							`" . TABLE_CREATED_DOCUMENTS . "`.`createdDocumentsCustomerNumber`,
							`" . TABLE_CREATED_DOCUMENTS . "`.`createdDocumentsFilename`,
							`" . TABLE_CREATED_DOCUMENTS . "`.`createdDocumentsTimeCreated`,

							`" . TABLE_CREATED_DOCUMENTS_TYPES . "`.`createdDocumentsTypesID`,
							`" . TABLE_CREATED_DOCUMENTS_TYPES . "`.`createdDocumentsTypesName`,
							`" . TABLE_CREATED_DOCUMENTS_TYPES . "`.`createdDocumentsTypesShortName`

							FROM `" . TABLE_CREATED_DOCUMENTS . "`

							LEFT JOIN `" . TABLE_CREATED_DOCUMENTS_TYPES . "`
							ON(`" . TABLE_CREATED_DOCUMENTS . "`.`createdDocumentsType` = `" . TABLE_CREATED_DOCUMENTS_TYPES . "`.`createdDocumentsTypesShortName`)

							WHERE 1
								AND `" . TABLE_CREATED_DOCUMENTS . "`.`createdDocumentsCustomerNumber` = '".$thisCustomerNumbers."'
								AND `" . TABLE_CREATED_DOCUMENTS . "`.`createdDocumentsType` = 'KA'

					";
			}

			$sql = implode(" UNION ", $arrSql) . " ORDER BY `createdDocumentsNumber` DESC ";

			$rs = $dbConnection->db_query($sql);

			while($ds = mysqli_fetch_assoc($rs)) {
				foreach(array_keys($ds) as $field) {
					$arrLayoutPdfDatas[$ds["createdDocumentsID"]][$field] = $ds[$field];
				}
			}

		}
		return $arrLayoutPdfDatas;
	}
	// ---------------------------------
	function formatTime($string, $mode){
		$newString = $string;
		if($newString == ""){
			$newString = "00:00:00";
		}
		if($mode == 'store'){

		}
		else if($mode == 'display'){
			$newString = substr($newString, 0, -3);
		}
		return $newString;
	}
	// ---------------------------------
	function convertTime($time, $mode="minutes"){
		$newTime = 0;
		if($mode == "minutes"){
			$time = formatTime($time, "display");
			$arrTemp = explode(":", $time);
			$newTime = ($arrTemp[0] * 60) + $arrTemp[1];
		}
		else if($mode == "hours"){
			$newTimeMinutes = $time%60;
			$newTimeHours = ($time - $newTimeMinutes) / 60;
			if($newTimeMinutes < 10){ $newTimeMinutes = "0" . $newTimeMinutes; }
			if($newTimeHours < 10){ $newTimeHours = "0" . $newTimeHours; }
			$newTime = $newTimeHours . ":" . $newTimeMinutes;
		}
		return $newTime;
	}
	// ---------------------------------
	function wordunwrap($string){
		$newString = $string;
		$newString = preg_replace('/<br>|<br \/>|[\r\n]+/', ' ', $newString);
		return $newString;
	}
	// ---------------------------------
	function getSalesmanInZipcodeArea($arrRelationSalesmenZipcodeDatas, $zipcode){
		global $dbConnection, $errorMessage, $warningMessage, $infoMessage, $successMessage;
		$arrThisAreaSalesman = array();

		if($zipcode != ""){
			for($i = 2 ; $i < (strlen($zipcode) + 1) ; $i++){
				$checkZipcode = substr($zipcode, 0, $i);
				if(!empty($arrRelationSalesmenZipcodeDatas[$checkZipcode])){
					$arrThisAreaSalesman = $arrRelationSalesmenZipcodeDatas[$checkZipcode];
					#echo $checkZipcode . '<br />';
					break;
				}
			}
		}

		return $arrThisAreaSalesman;
	}
	// ---------------------------------
	function getSalesmanInZipcodeArea2($arrRelationSalesmenZipcodeDatas, $zipcode){
		global $dbConnection, $errorMessage, $warningMessage, $infoMessage, $successMessage;
		$arrThisAreaSalesman = array();

		if($zipcode != ""){
			for($i = 2 ; $i < (strlen($zipcode) + 1) ; $i++){
				$checkZipcode = substr($zipcode, 0, $i);
				if(!empty($arrRelationSalesmenZipcodeDatas[$checkZipcode])){
					foreach($arrRelationSalesmenZipcodeDatas[$checkZipcode] as $thisRelationSalesmenZipcodeDatas){
						$arrThisAreaSalesman[] = $thisRelationSalesmenZipcodeDatas;
					}
					break;
				}
			}
		}

		return $arrThisAreaSalesman;
	}
	// ---------------------------------
	function getTrackingUrl($service, $trackingNumber){
		$thisTrackingUrl = '';
		$trackingNumber = urlencode($trackingNumber);
		/*
		$arrTrackingUrls = array(
			"dpd" => "https://tracking.dpd.de/cgi-bin/delistrack?pknr={###TRACKING_NUMBER###}&amp;typ=1&amp;lang=de",
			"post" => "https://www.deutschepost.de/sendung/simpleQueryResult.html",
			"dhl" => "http://nolp.dhl.de/nextt-online-public/set_identcodes.do?idc={###TRACKING_NUMBER###}",
			"gls" => "http://gls-group.eu/DE/de/paketverfolgung?match={###TRACKING_NUMBER###}",
		);
		*/
		$thisTrackingUrl = $arrTrackingUrls[$service];


		$thisTrackingUrl = constant("TRACKING_URL_" . strtoupper($service));
		if($thisTrackingUrl != ''){
			$thisTrackingUrl = preg_replace("/{###TRACKING_NUMBER###}/", $trackingNumber, $thisTrackingUrl);
		}
		return $thisTrackingUrl;
	}
	// ---------------------------------
	function getPrintProductionPlace($trackingNumber, $mode="flag"){
		global $arrDeliveryTrackingBasicNumbers;
		$thisPrintProductionPlace = "";

		if(!empty($arrDeliveryTrackingBasicNumbers)){

			foreach($arrDeliveryTrackingBasicNumbers as $thisDeliveryTrackingBasicNumberKey => $thisDeliveryTrackingBasicNumberValue){
				if(preg_match("/^" . $thisDeliveryTrackingBasicNumberValue . "/", $trackingNumber)){
					$thisPrintProductionPlace = $thisDeliveryTrackingBasicNumberKey;
				}
			}
			if($thisPrintProductionPlace != "" && $mode == "flag"){
				$thisPrintProductionPlace = ' <img src="layout/flags/flag_' . $thisPrintProductionPlace . '.gif" width="14" height="10" style="border:1px solid #999;" alt="' . $thisPrintProductionPlace . '" title="' . $thisPrintProductionPlace . '" />';
			}
		}
		return $thisPrintProductionPlace;
	}
	// ---------------------------------
	function searchDownloadFile($path, $documentType){
		global $dbConnection;
		$thisDownloadFile = '';

		$arrTemp = explode("_", basename($path));
		$thisSearchFileDocumentNumber = $arrTemp[0];

		if($thisSearchFileDocumentNumber != ""){
			$sql_seachFile = "
				SELECT
					`orderDocumentsDocumentPath`
					FROM `" . constant("TABLE_ORDER_" . $documentType) . "`
					WHERE 1
						AND `orderDocumentsNumber` = '" . $thisSearchFileDocumentNumber . "'
					LIMIT 1
			";
			$rs_seachFile = $dbConnection->db_query($sql_seachFile);
			list($thisDownloadFile) = mysqli_fetch_array($rs_seachFile);
		}

		if($thisDownloadFile == ""){
			$thisDownloadFile = $path;
		}
		$thisDownloadFile = basename($thisDownloadFile);
		return $thisDownloadFile;
	}
	// ---------------------------------

	function checkWebConnection(){
		$checkTimeout = 2;
		$checkDomain = "www.google.de";
		$isWebConnection = true;

		$arrayPorts = array("80", "443");
		if(!empty($arrayPorts)){
			foreach($arrayPorts as $thisPort){
				$fp_sock = @fsockopen($checkDomain, $thisPort, $errorNumber, $errorString, $checkTimeout);
				if(!$fp_sock){
					$isWebConnection = false;
					break;
				}
			}
		}
		return $isWebConnection;
	}

	// ---------------------------------
	function checkExternalDbConnection(){
		$isExternalDBConnection = true;
		if(defined("DB_HOST_EXTERN_SHOP")){
			$dbConnection_ExternShop = new DB_Connection(DB_HOST_EXTERN_SHOP, '', DB_NAME_EXTERN_SHOP, DB_USER_EXTERN_SHOP, DB_PASSWORD_EXTERN_SHOP);
			$db_open_ExternShop = $dbConnection_ExternShop->db_connect();
			if(!$db_open_ExternShop){
				$isExternalDBConnection = false;
			}
		}
		return $isExternalDBConnection;
	}

	function getCustomersSourceTableFromCustomerNumber($string){
		$customersSource = PAGE_EDIT_CUSTOMER;
		if(preg_match("/^LF-/", $string)){
			$customersSource = PAGE_EDIT_SUPPLIERS;
		}
		return($customersSource);
	}

	function checkIfCustomerAlreadyExists($arrCompanyData = array()){
		/*
		global $dbConnection;

		$arrFoundDatas		= array ();

		$checkStreet		= $arrCompanyData["street"];
		$checkZipcode		= $arrCompanyData["zipcode"];
		$checkZipcodePart	= substr($arrCompanyData["zipcode"], 0, 2);
		$checkCountryID		= $arrCompanyData["countryID"];
		$checkMail1			= $arrCompanyData["mail1"];
		$checkMail2			= $arrCompanyData["mail2"];

		$sqlWhere = "";

		if($checkStreetName != ""){
			$sqlWhere .= " AND (`" . TABLE_CUSTOMERS . "`.`customersCompanyStrasse` = '" . $checkStreetName . "') ";
		}
		if($checkStreetNumber != ""){
			$sqlWhere .= " AND (`" . TABLE_CUSTOMERS . "`.`customersCompanyHausnummer` = '" . $checkStreetNumber . "') ";
		}
		if($checkZipcode != ""){
			$sqlWhere .= " AND (`" . TABLE_CUSTOMERS . "`.`customersCompanyPLZ` = '" . $checkZipcode . "') ";
		}
		if($checkCountryID != ""){
			$sqlWhere .= " AND (`" . TABLE_CUSTOMERS . "`.`customersCompanyCountry` = '" . $checkCountryID . "') ";
		}
		if($checkMail1 != "" || $checkMail2 != ""){
			$sqlWhere .= " AND ( ";
			if($checkMail1 != ""){
				$sqlWhere .= "
								`" . TABLE_CUSTOMERS . "`.`customersMail1` = '" . $checkMail1 . "'
								OR
								`" . TABLE_CUSTOMERS . "`.`customersMail2` = '" . $checkMail1 . "'
					";
			}
			if($checkMail1 != "" && $checkMail2 != ""){
				$sqlWhere .= " OR ";
			}
			if($checkMail2 != ""){
				$sqlWhere .= "
								`" . TABLE_CUSTOMERS . "`.`customersMail1` = '" . $checkMail2 . "'
								OR
								`" . TABLE_CUSTOMERS . "`.`customersMail2` = '" . $checkMail2 . "'
					";
			}
		}

		$sql = "
			SELECT
					`" . TABLE_CUSTOMERS . "`.`customersID`,
					`" . TABLE_CUSTOMERS . "`.`customersKundennummer`,
					`" . TABLE_CUSTOMERS . "`.`customersFirmenname`,
					`" . TABLE_CUSTOMERS . "`.`customersFirmennameZusatz`,

					`" . TABLE_CUSTOMERS . "`.`customersFax1`,
					`" . TABLE_CUSTOMERS . "`.`customersFax2`,

					`" . TABLE_CUSTOMERS . "`.`customersMail1`,
					`" . TABLE_CUSTOMERS . "`.`customersMail2`,

					`" . TABLE_CUSTOMERS . "`.`customersHomepage`,

					`" . TABLE_CUSTOMERS . "`.`customersCompanyStrasse`,
					`" . TABLE_CUSTOMERS . "`.`customersCompanyHausnummer`,

					`" . TABLE_CUSTOMERS . "`.`customersCompanyPLZ`,
					`" . TABLE_CUSTOMERS . "`.`customersCompanyOrt`,

					`" . TABLE_CUSTOMERS . "`.`customersCompanyCountry`



				FROM `" . TABLE_CUSTOMERS . "`
				WHERE 1
					AND
				" . $sqlWhere . "

				ORDER BY `" . TABLE_CUSTOMERS . "`.`customersKundennummer`
		";



		return $arrFoundDatas;
		*/
	}

	// ---------------------------------

	function checkIfDocumentIsEditable($docType, $docNumber){
		$isDocEditable = true;

		if(in_array($docType, array('RE'))){
			$thisDocEditableDateLimitDays = 10;
			$thisDocNumberDateYear = substr($docNumber, 3, 2);
			$thisDocNumberDateMonth = substr($docNumber, 5, 2);
			$thisDocNumberDateDay = date('t', mktime(1, 1, 1, $thisDocNumberDateMonth, 1, $thisDocNumberDateYear));
			$thisDocNumberDate = $thisDocNumberDateYear . '-' . $thisDocNumberDateMonth . '-' . $thisDocNumberDateDay;
			$thisDocEditableDateLimit = date('y-m-d', mktime(1, 1, 1, ($thisDocNumberDateMonth + 1), $thisDocEditableDateLimitDays, $thisDocNumberDateYear));
			$todayDate = date('y-m-d');

			if($todayDate >= $thisDocEditableDateLimit){
				$isDocEditable = false;
			}
			else {
				$isDocEditable = true;
			}
		}
		$isDocEditable = true; // ALWAYS EDITABLE
		return $isDocEditable;
	}

	// ---------------------------------

	function cleanUrl($string) {
		$string = preg_replace("/^http:\/\//", "", $string);
		$string = preg_replace("/\/$/", "", $string);
		return $string;
	}

	// ---------------------------------

	function cleanMail($string) {
		$string = trim($string);
		$string = preg_replace("/\(at\)/i", "@", $string);
		$string = preg_replace("/\[at\]/i", "@", $string);
		return $string;
	}

	// ---------------------------------

	function getGoogleLocationDatas($arrDatas){
		$arrLocationDatas = array();
		#if($arrDatas["customersCompanyPLZ"] != "" && $arrDatas["customersCompanyOrt"] != "" && $arrDatas["customersCompanyStrasse"] != "" && $arrDatas["customersCompanyHausnummer"] != ""){
		if($arrDatas["customersCompanyPLZ"] != "" && $arrDatas["customersCompanyOrt"] != "" && $arrDatas["customersCompanyStrasse"] != ""){

			if(!empty($arrDatas)){
				$arrTemp = explode(" ", $arrDatas["customersCompanyOrt"]);
				$arrDatas["customersCompanyOrt"] = $arrTemp[0];
				if(strlen($arrDatas["customersCompanyOrt"] < 4 && $arrTemp[1] != '')){
					$arrDatas["customersCompanyOrt"] = $arrTemp[0] . ' ' . $arrTemp[1];
				}

				#$arrDatas["customersCompanyHausnummer"] = preg_replace("/\//", " ", $arrDatas["customersCompanyHausnummer"]);

				foreach($arrDatas as $thisDataKey => $thisDataValue){
					#$thisDataValue = utf8_decode($thisDataValue);
					$thisDataValue = rawurlencode($thisDataValue);
					$arrDatas[$thisDataKey] = $thisDataValue;
				}
			}
			# $thisSearchString = "48145 Münster Johanniterstr. 15";
			$thisSearchString = '';
			$thisSearchString .= $arrDatas["customersCompanyPLZ"];
			$thisSearchString .= '+';
			$thisSearchString .= $arrDatas["customersCompanyOrt"];
			$thisSearchString .= '+';
			$thisSearchString .= $arrDatas["customersCompanyStrasse"];
			if($arrDatas["customersCompanyHausnummer"] != ""){
				$thisSearchString .= '+';
				$thisSearchString .= $arrDatas["customersCompanyHausnummer"];
			}
			if($arrDatas["customersCompanyCountry"] != ""){
				$thisSearchString .= '+';
				$thisSearchString .= $arrDatas["customersCompanyCountry"];
			}
			$thisSearchStringORIG = $thisSearchString;
			#$thisSearchString = preg_replace("/ /", "+", $thisSearchString);
			#$thisSearchString = rawurlencode(utf8_decode($thisSearchString));

			#$thisSearchString = (rawurlencode($thisSearchString));
			#print_r($thisSearchString);
			$getGoogleDataUrL_Points = 'https://maps.googleapis.com/maps/api/geocode/json?address=' . $thisSearchString . '&sensor=false&language=de';

			$thisGetJson = implode("", file($getGoogleDataUrL_Points));
			$thisObjJson = json_decode($thisGetJson);

			$address_components		= $thisObjJson->results[0]->address_components;
			$formatted_address		= $thisObjJson->results[0]->formatted_address;
			$geometry_location		= $thisObjJson->results[0]->geometry->location;
			$error_message			= $thisObjJson->error_message;
			$status					= $thisObjJson->status;
			$thisLNG				= $thisObjJson->results[0]->geometry->location->lng;
			$thisLAT				= $thisObjJson->results[0]->geometry->location->lat;

			$arrLocationDatas = array(
				"LNG" => $thisLNG,
				"LAT" => $thisLAT,
				"SEARCHSTRING" => $thisSearchString,
				"SEARCHSTRING_ENCODING" => mb_detect_encoding($thisSearchString),
				"SEARCHSTRING2" => $thisSearchStringORIG,
				"SEARCHSTRING2_ENCODING" => mb_detect_encoding($thisSearchStringORIG),
				"RESULT_JSON" => $thisGetJson,
				"STATUS" => $status,
				"ERROR_MESSAGE" => $error_message,
				"GET_URL" => $getGoogleDataUrL_Points
			);
		}
		return $arrLocationDatas;
	}

	// ---------------------------------

	function getAllZipcodes(){
		global $dbConnection;

		$arrAllZipCodes = array();
		$arrAllDoubleZipCodes = array();
		$arrAllTripleZipCodes = array();

		$sql = "
			SELECT
				`zipcodesDoubleZipcode`,
				`zipcodesDoubleDescription`

			FROM `" . TABLE_ZIPCODES_DOUBLE . "`
			WHERE 1
				AND `zipcodesDoubleActive` = '1'
			ORDER BY `zipcodesDoubleZipcode`
		";
		$rs = $dbConnection->db_query($sql);

		while($ds = mysqli_fetch_assoc($rs)){
			foreach(array_keys($ds) as $field){
				$arrAllDoubleZipCodes[$ds["zipcodesDoubleZipcode"]][$field] = $ds[$field];
				for($i = 0 ; $i < 10 ; $i++){
					$zipCodeTriple = $ds["zipcodesDoubleZipcode"] . $i;
					$arrAllTripleZipCodes[$zipCodeTriple][$field] = $ds[$field];
				}
			}
		}
		$arrAllZipCodes['double'] = $arrAllDoubleZipCodes;
		$arrAllZipCodes['triple'] = $arrAllTripleZipCodes;
		return $arrAllZipCodes;
	}

	// ---------------------------------

	function getNotUsedZipcodeAreas($arrSalesmenWithActiveAreasData){
		$arrAllZipcodes = getAllZipcodes();
		$arrAllDoubleZipCodes = $arrAllZipcodes['double'];
		$arrAllTripleZipCodes = $arrAllZipcodes['triple'];
		$arrNotUsedZipCodeAreas = array();

		$arrUsedDoubleZipCodeAreas = array();
		$arrUsedTripleZipCodeAreas = array();
		if(!empty($arrSalesmenWithActiveAreasData)){
			foreach($arrSalesmenWithActiveAreasData as $thisSalesmenWithActiveAreasData){
				$arrTempZipCodes = explode(";", $thisSalesmenWithActiveAreasData["salesmenAreas"]);
				if(!empty($arrTempZipCodes)){
					foreach($arrTempZipCodes as $thisTempZipCodes){
						if(strlen($thisTempZipCodes) == 2){
							$arrUsedDoubleZipCodeAreas[] = $thisTempZipCodes;
						}
						else if(strlen($thisTempZipCodes) == 3){
							$arrUsedTripleZipCodeAreas[] = $thisTempZipCodes;
						}
					}
				}
			}
		}

		$arrNotUsedDoubleZipCodes = array_diff(array_keys($arrAllDoubleZipCodes), $arrUsedDoubleZipCodeAreas);
		$arrNotUsedTripleZipCodes = array_diff(array_keys($arrAllTripleZipCodes), $arrUsedTripleZipCodeAreas);

		// BOF COUNT TRIPLE ZIP CODE PER DOUBLE ZIP CODE
			$arrCountTripleZipCodesPerDoubleZipCode = array();
			if(!empty($arrNotUsedTripleZipCodes)){
				foreach($arrNotUsedTripleZipCodes as $thisTripleZipCode){
					$thisDoubleZipCode = substr($thisTripleZipCode, 0, 2);
					if(empty($arrCountTripleZipCodesPerDoubleZipCode[$thisDoubleZipCode])){
						$arrCountTripleZipCodesPerDoubleZipCode[$thisDoubleZipCode] = 0;
					}
					$arrCountTripleZipCodesPerDoubleZipCode[$thisDoubleZipCode]++;
				}
			}
		// EOF COUNT TRIPLE ZIP CODE PER DOUBLE ZIP CODE

		if(!empty($arrCountTripleZipCodesPerDoubleZipCode)){
			foreach($arrCountTripleZipCodesPerDoubleZipCode as $thisCountTripleZipCodesPerDoubleZipCodeKey => $thisCountTripleZipCodesPerDoubleZipCodeValue){
				if($thisCountTripleZipCodesPerDoubleZipCodeValue == 10){
					foreach($arrNotUsedTripleZipCodes as $thisNotUsedTripleZipCodes){
						if(substr($thisNotUsedTripleZipCodes, 0, 2) == $thisCountTripleZipCodesPerDoubleZipCodeKey){
							unset($arrNotUsedTripleZipCodes[array_search($thisNotUsedTripleZipCodes, $arrNotUsedTripleZipCodes)]);
						}
					}
				}
				else {
					unset($arrNotUsedDoubleZipCodes[array_search($thisCountTripleZipCodesPerDoubleZipCodeKey, $arrNotUsedDoubleZipCodes)]);
				}
			}
		}

		$arrNotUsedZipCodeAreas = array_merge($arrNotUsedDoubleZipCodes, $arrNotUsedTripleZipCodes);

		return $arrNotUsedZipCodeAreas;
	}

	// ---------------------------------

	function transliterateTurkishChars($string, $convert=true) {
		$string = trim($string);
		$search  = array('ç', 'Ç', 'ğ', 'Ğ', 'ı', 'İ', 'ö', 'Ö', 'ş', 'Ş', 'ü', 'Ü');
		#$replace = array('c', 'C', 'g', 'G', 'i', 'I', 'o', 'O', 's', 'S', 'u', 'U');
		$replace = array('c', 'C', 'g', 'G', 'i', 'I', 'ö', 'Ö', 's', 'S', 'ü', 'Ü');
		$output = str_replace($search, $replace, $string);

		if($convert == true){
			$output = utf8_decode($output);
			$output = htmlentities($output);
			$output = strtolower($output);
			$output = ucwords($output);
		}
		return $output;
	}

	// ---------------------------------

	function formatFilesizeAuto($filesize){
		global $countLoop;
		$unitBaseFaktor = 1024;
		$arrUnits = array(
			0 => "B",
			1 => "KB",
			2 => "MB",
			3 => "GB",
			4 => "TB",
			5 => "PB",
			6 => "EB",
			7 => "ZB",
			8 => "YB"
		);
		$countLoop = 0;
		while($filesize > $unitBaseFaktor){
			if(($countLoop + 1) < count($arrUnits)){
				$filesize = $filesize / $unitBaseFaktor;
				$countLoop++;
			}
			else {
				break;
			}
		}
		$filesizeUnit = $arrUnits[$countLoop];
		$formattedFilesize = number_format($filesize, "2", ",", ".");
		$formattedFilesize .= " " . $filesizeUnit;
		return $formattedFilesize;
	}

	function formatFilesize($filesize, $unitOrigin, $unitNew){
		$unitBaseFaktor = 1024;
		$arrUnits = array(
			0 => "B",
			1 => "KB",
			2 => "MB",
			3 => "GB",
			4 => "TB",
			5 => "PB",
			6 => "EB",
			7 => "ZB",
			8 => "YB"
		);

		$unitOriginIndex = array_search($unitOrigin, $arrUnits);
		$unitNewIndex = array_search($unitNew, $arrUnits);
		$unitFormatExp = ($unitOriginIndex - $unitNewIndex);
		$unitFormatFaktor = pow($unitBaseFaktor, $unitFormatExp);
		if($unitFormatExp > 0){

		}
		else if($unitFormatExp < 0){

		}
		else if($unitFormatExp == 0){

		}

		$formattedFilesize = $filesize * $unitFormatFaktor;
		$formattedFilesize = number_format($formattedFilesize, "2", ",", ".");
		$formattedFilesize .= " " . $unitNew;
		return $formattedFilesize;
	}

	// ---------------------------------

	function getGroupCustomerData($groupID){
		global $dbConnection;
		$arrGroupCustomerData = array();
		if($groupID != ''){
			$sql = "
					SELECT
						`customersID`,
						`customersKundennummer`,
						`customersFirmenname`,
						`customersFirmennameZusatz`,
						`customersGroupCompanyNumber`,
						`customersCompanyPLZ`,
						`customersCompanyOrt`

					FROM `" . TABLE_CUSTOMERS . "`
					WHERE 1
						AND `customersTyp` = '3'
						AND `customersGroupCompanyNumber` != ''
						AND ( FIND_IN_SET('" . $groupID . "', REPLACE(`customersGruppe`, ';', ',')) > 0 )

					ORDER BY `customersGroupCompanyNumber`
				";

			$rs = $dbConnection->db_query($sql);

			while($ds = mysqli_fetch_assoc($rs)){
				foreach(array_keys($ds) as $field){
					$arrGroupCustomerData[$ds["customersID"]][$field] = $ds[$field];
				}
			}
		}
		return $arrGroupCustomerData;
	}

	//

	// ---------------------------------

	function checkIfDocumentWasExportedToDATEV($documentType, $documentNumber){
		global $dbConnection;
		$documentWasExportedToDATEV = false;
		if($documentType != '' && $documentNumber != ''){
			$sql = "
					SELECT
						`BELEGNR`

					FROM `" . TABLE_EXPORT_DATEV_DATA . "`
					WHERE 1
						AND `DOC_TYPE` = '" . $documentType . "'
						AND `DOC_NR` = '" . $documentNumber . "'

					LIMIT 1
				";

			$rs = $dbConnection->db_query($sql);
			$countResult = $dbConnection->db_getMysqlNumRows($rs);
			#echo 'countResult: ' . $countResult . '<br />';
			#echo 'sql: ' . $sql . '<br />';
			if($countResult > 0){
				$documentWasExportedToDATEV = true;
			}
		}
		return $documentWasExportedToDATEV;
	}

	// ---------------------------------

	function in_array_r($needle, $haystack, $strict=false) {
		foreach ($haystack as $item) {
			if(($strict ? $item === $needle : $item == $needle) || (is_array($item) && in_array_r($needle, $item, $strict))) {
				return true;
			}
		}
		return false;
	}

	// ---------------------------------

	function getTransferTransportTypes(){
		$arrTransportTypes = array(
			"LKW"		=> 7,	// DAYS
			"CONTAINER" => 21	// DAYS
		);
		return $arrTransportTypes;
	}

	// ---------------------------------
	function checkActiveParcelNumbers($parcelNumber, $parcelDate){
		$thisPrintProductionPlace = getPrintProductionPlace($parcelNumber, '');

		if($thisPrintProductionPlace == 'TR'){
			$dateAddInterval = '+50 days';
		}
		else {
			$dateAddInterval = '+12 days';
		}
		$checkDate = date("Y-m-d", strtotime($parcelDate . $dateAddInterval));

		return $checkDate;
	}
	// ---------------------------------
	function getDeliveryTimeKW($date, $printType){
		global $dbConnection;
		$deliveryTimeKW = 'date: ' . $date . ' printType: ' . $printType;
		$sql = "
				SELECT
					@num:=CAST(`" . TABLE_ADDITIONAL_COSTS . "`.`additionalCostsDeliveryMaxTime` AS UNSIGNED) AS `num`,
					@p  :=SUBSTR(`" . TABLE_ADDITIONAL_COSTS . "`.`additionalCostsDeliveryMaxTime`, CHAR_LENGTH(@num)+2) AS `p`,

					CASE
						WHEN @p='YEAR' THEN DATE_FORMAT(DATE_ADD('" . $date . "', INTERVAL @num YEAR), '%u')
						WHEN @p='MONTH' THEN DATE_FORMAT(DATE_ADD('" . $date . "', INTERVAL @num MONTH), '%u')
						WHEN @p='DAY' THEN DATE_FORMAT(DATE_ADD('" . $date . "', INTERVAL @num DAY), '%u')
						WHEN @p='WEEK' THEN DATE_FORMAT(DATE_ADD('" . $date . "', INTERVAL @num WEEK), '%u')
					END AS `orderCircaDeliveryKW`

					FROM `" . TABLE_ADDITIONAL_COSTS . "`

					WHERE 1
						AND `" . TABLE_ADDITIONAL_COSTS . "`.`additionalCostsID` = '" . $printType . "'

					LIMIT 1
			";
		$rs = $dbConnection->db_query($sql);

		list($num, $p, $orderCircaDeliveryKW) = mysqli_fetch_array($rs);
		$deliveryTimeKW = $orderCircaDeliveryKW;

		if($_COOKIE["isAdmin"] == '1'){
			#echo 'num: ' . $num . '<br />';
			#echo 'p: ' . $p . '<br />';
			#echo 'deliveryTimeKW: ' . $deliveryTimeKW . '<br />';
		}

		return $deliveryTimeKW;
	}
	// ---------------------------------
	function translateDeliveryData($string, $langSource, $langConvert){
		$translatedString = $string;
		$arrWords = array(
			array("TR" => "Çýta", "DE" => "Leisten"),
			array("TR" => "Çýta", "DE" => "Leisten"),
			array("TR" => "ÇITA", "DE" => "Leisten"),
			array("TR" => "çýta", "DE" => "Leisten"),
			array("TR" => "LEÝSTEN", "DE" => "Leisten"),
			array("TR" => "LESÝTEN", "DE" => "Leisten"),

			array("TR" => "MÝNÝLETTER", "DE" => "Miniletter"),
			array("TR" => "PS PLATE", "DE" => "Miniletter"),
			array("TR" => "PS Plate", "DE" => "Miniletter"),

			array("TR" => "Paspas", "DE" => "Papierfussmatten")
		);

		if(!empty($arrWords)){
			foreach($arrWords as $thisKey => $thisValue){
				if($thisValue[$langSource] != "" && $thisValue[$langConvert] != ""){
					$translatedString = preg_replace("/" . $thisValue[$langSource] . "/", $thisValue[$langConvert], $translatedString);
				}
			}
		}

		return $translatedString;
	}
	// ---------------------------------

	function getProductsVPE(){
		global $dbConnection;
		$arrProductsVPE = array();
		$sql = "
				SELECT
					`" . TABLE_PRODUCT_VPE . "`.`productVpeID`,
					`" . TABLE_PRODUCT_VPE . "`.`productVpeName`,
					`" . TABLE_PRODUCT_VPE . "`.`productVpeSort`,
					`" . TABLE_PRODUCT_VPE . "`.`productVpeActive`,
					`" . TABLE_PRODUCT_VPE . "`.`productVpeFactor`

				FROM `" . TABLE_PRODUCT_VPE . "`
				WHERE 1
					AND `" . TABLE_PRODUCT_VPE . "`.`productVpeActive` = '1'
				ORDER BY
					`productVpeSort`,
					`productVpeName`
			";

		$rs = $dbConnection->db_query($sql);

		while($ds = mysqli_fetch_assoc($rs)){
			foreach(array_keys($ds) as $field){
				$arrProductsVPE[$ds["productVpeID"]][$field] = $ds[$field];
			}
		}
		return $arrProductsVPE;
	}
// Mounir
	if (!function_exists('array_key_first')) {
	    function array_key_first(array $arr) {
	        foreach($arr as $key => $unused) {
	            return $key;
	        }
	        return NULL;
	    }
	}

?>