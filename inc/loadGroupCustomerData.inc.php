<?php
	session_start();

	require_once('../config/configMandator.inc.php');
	require_once('../config/configParams_' . strtolower(MANDATOR) . '.inc.php');
	require_once('../config/configBasic.inc.php');
	require_once('../config/configFiles.inc.php');
	require_once('../config/configTables.inc.php');
	require_once('../inc/functions.inc.php');
	##require_once('header.inc.php');

	#$arrUrlData = parse_url(urldecode($_SERVER["REQUEST_URI"]));
	$arrUrlData = parse_url(($_SERVER["REQUEST_URI"]));
	$strUrlParams = ($arrUrlData["query"]);
	$arrUrlParams = explode('&', $strUrlParams);
	if(!empty($arrUrlParams)){
		foreach($arrUrlParams as $thisParam){
			$arrTemp = explode('=', $thisParam);
			$_GET[$arrTemp[0]] = urldecode(preg_replace("/xxxxxx/", " ", $arrTemp[1]));
		}
	}

	$warningMessage = '';
	$errorMessage = '';
	$successMessage = '';
	$infoMessage = '';

	$groupID = trim($_GET["groupID"]);
	$groupCompanyNumber = trim($_GET["groupCompanyNumber"]);
	$mode = trim($_GET["mode"]);

	$content = '';

	if((int)$groupID > 0){

		$dbConnection = new DB_Connection();
		$db_open = $dbConnection->db_connect();

		$arrGroupCustomerData = getGroupCustomerData($groupID);

		if(!empty($arrGroupCustomerData)){
			// BOF GET VZ-NUMBER DEPENDING ON groupCompanyNumber
				if($groupCompanyNumber != ''){
					$arrGroupCompanyNumbers = explode("_", $groupCompanyNumber);
					$depot = '';
					if(!empty($arrGroupCompanyNumbers)){

						$sql = "
								SELECT
									`DEPOT`
								FROM `" . TABLE_CUSTOMER_GROUPS_RELATION_DATA . "`
								WHERE 1
							";

						foreach($arrGroupCompanyNumbers as $thisGroupCompanyNumbers){
							if(strlen($thisGroupCompanyNumbers) == 3){
								$sql .= " AND `" . TABLE_CUSTOMER_GROUPS_RELATION_DATA . "`.`PREFIX` = '" . $thisGroupCompanyNumbers . "' ";
							}
							else if(strlen($thisGroupCompanyNumbers) == 5){
								$sql .= " AND `" . TABLE_CUSTOMER_GROUPS_RELATION_DATA . "`.`HDLNR` = '" . $thisGroupCompanyNumbers . "' ";
							}
						}
						$sql .= "
							LIMIT 1
						";

						$rs = $dbConnection->db_query($sql);

						list($depot) = mysqli_fetch_array($rs);
					}
				}
				if($mode == 'this'){
					$content .= '<b> - zugeh&ouml;rige VZ-Nummer: ' . $depot . '</b>';
				}
			// EOF GET VZ-NUMBER DEPENDING ON groupCompanyNumber



			// BOF GET ALL VZ-NUMBERS
				if($mode == 'all'){
					$count = 0;

					$thisGroupMarker = '';

					foreach($arrGroupCustomerData as $thisGroupCustomerDataKey => $thisGroupCustomerDataValue){
						if($count%2 == 0){ $rowClass = 'row0'; }
						else { $rowClass = 'row1'; }
						$thisStyle = '';
						if($thisGroupMarker != $thisGroupCustomerDataKey){
							$thisGroupMarker = $thisGroupCustomerDataKey;
							$thisStyle = ' border-top:2px solid #F00; ';
						}

						if($depot != '' && $depot == $thisGroupCustomerDataValue["customersGroupCompanyNumber"]){
							$thisStyle = ' border-left:4px solid #090; ';
						}

						$content .= '<div style="width:400px !important;' . $thisStyle . '" class="sideInfoItem ' . $rowClass . '" id="cid_' . $thisGroupCustomerDataValue["customersID"] . '_' . $thisGroupCustomerDataKey . '" rel="' . $thisGroupCustomerDataValue["customersID"] . '" title="Dieses Vertriebszentrum per Klick als Vertreter &uuml;bernehmen" >';
							$content .= '<div class="sideInfoTitle" style="float:left;width:16px;">' . $thisGroupCustomerDataValue["customersGroupCompanyNumber"] . '</div>';
							$content .= '<div class="sideInfoText" style="float:left;white-space:nowrap;font-size:10px;">' .
											'KNR: ' . $thisGroupCustomerDataValue["customersKundennummer"] .
											' <br />' .
											$thisGroupCustomerDataValue["customersFirmenname"] .
											' <br />' .
											$thisGroupCustomerDataValue["customersFirmennameZusatz"] .
											' <br />' .
											$thisGroupCustomerDataValue["customersCompanyPLZ"] .
											' ' .
											$thisGroupCustomerDataValue["customersCompanyOrt"] .
										'</div>'
								;
							$content .= '<div class="clear"></div>';
						$content .= '</div>';
						$count++;
					}
				}
			// EOF GET ALL VZ-NUMBERS
		}

		if($dbConnection->db_displayErrors() != "") {
			$errorMessage .= $dbConnection->db_displayErrors(). '<br />';
		}
		if($dbConnection) {
			$dbConnection->db_close();
		}
	}

	echo $content;
?>