<?php
	session_start();

	require_once('../config/configMandator.inc.php');
	require_once('../config/configParams_' . strtolower($_COOKIE["mandator"]) . '.inc.php');
	require_once('../config/configBasic.inc.php');
	require_once('../config/configFiles.inc.php');
	require_once('../config/configTables.inc.php');
	require_once('../inc/functions.inc.php');
	##require_once('header.inc.php');
	require_once('../' . PATH_MAILBOX_CLASS);

	$userDatas = getUserDatas();

	$content = '';

	$warningMessage = '';
	$errorMessage = '';
	$successMessage = '';
	$infoMessage = '';

	$dbConnection = new DB_Connection();
	$db_open = $dbConnection->db_connect();

	$useMandator = $_GET["mandator"];
	$useMandator = 'bctr';

	$customerGroupID = $_GET["customerGroupID"];	

	$arrLoadedProducts = getOrderProducts($_GET["catID"], $useMandator, $customerGroupID);
	$content .= '<option value="0">' . ' --- Bitte w&auml;hlen --- ' . '</option>';

	$thisOptionMarker = '';
	
	if(!empty($arrLoadedProducts)){
		foreach($arrLoadedProducts as $thisCategoryKey => $thisCategoryData){								
			$content .= '<option class="level_1" value="' . $thisCategoryKey . '" relation="'. htmlentities($thisCategoryData["categoriesGewischtProMenge"]) .'">' . htmlentities($thisCategoryData["categoriesName"]) . ' [' . $thisCategoryKey . ']' .'</option>';
			
			if(!empty($thisCategoryData["products"])){
				$thisDistance = " &bull; ";
				foreach($thisCategoryData["products"] as $thisProductKey => $thisProductValue){
					// print_r($thisProductValue);exit();
					$selected = '';
					if(
						$thisProductKey == $orderDatas["ordersArtikelID"]						
						|| $thisProductKey == $orderDatas["ordersArtikelKategorieID"]						
						|| $thisProductKey == $_GET["selectedKey"]
					){
						$selected = ' selected= "" ';
					}
					$content .= '<option class="level_2" data-gewicht="'.$thisProductValue["productsGewicht"].'" value="' . $thisProductKey . '" ' . $selected . ' >' . $thisDistance . htmlentities($thisProductValue["productsName"]) . ' / ' . htmlentities($thisCategoryData["categoriesName"]) . ' / ' . $thisProductValue["productsProductNumberUse"] . ' / ' . $thisProductValue["productsID"] . '</option>';
				}
			}									
		}
	}
	

	if($dbConnection) {
		$dbConnection->db_close();
	}

	echo $content;
?>