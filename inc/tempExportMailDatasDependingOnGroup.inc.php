<?php
if($thisAction == 'EXPORT_MAIL_DATAS_DEPENDING_ON_GROUP'){
	#error_reporting(E_ALL);

	echo '<h3>Mail exportieren</h3>';

	// BOF GET GROUPS
	$arrCustomerGroups = getCustomerGroups();
	// EOF GET GROUPS


	if(empty($arrCustomerGroups)){
		$arrCustomerGroups = array();
	}
	$arrCustomerGroups[0] = array("customerGroupsID" => 7,  "customerGroupsName" => "NO_GROUP");
	dd('arrCustomerGroups');
	echo "bp: ". BASEPATH;
	$thisStoreDir = DIRECTORY_EXPORT_FILES . "exportedMailAdressPerGroup/";
	if(!is_dir($thisStoreDir)){ mkdir($thisStoreDir); }

	foreach($arrCustomerGroups as $thisCustomerGroupKey => $thisCustomerGroupValue){
		$thisExportFile = $thisStoreDir . $thisCustomerGroupValue["customerGroupsName"] . ".csv";
		if(file_exists($thisExportFile)){
			unlink($thisExportFile);
		}
		$thisWhere = "";
		if($thisCustomerGroupKey > 0){
			$thisWhere = " AND (
								`common_customers`.`customersGruppe` = '" . $thisCustomerGroupKey . "' OR
								`common_customers`.`customersGruppe` LIKE '" . $thisCustomerGroupKey . ";%' OR
								`common_customers`.`customersGruppe` LIKE '%;" . $thisCustomerGroupKey . "' OR
								`common_customers`.`customersGruppe` LIKE '%;" . $thisCustomerGroupKey . ";%'
							) ";
		}
		else {
			$thisWhere = "
					AND (
						(`customersGruppe` = '' OR `customersGruppe` = 0)
						AND (
							`customersTyp` = 1
							OR
							`customersTyp` = 5
							OR
							`customersTyp` = 6
						)
					)
			";
		}
		dd('thisExportFile');
		$sql = "
			SELECT
				`tempTable`.`customersMail`,
				`tempTable`.`customersGruppe`,
				'" . $thisCustomerGroupValue["customerGroupsName"] . "'

				INTO OUTFILE '" . $thisExportFile . "' FIELDS TERMINATED BY ';' LINES TERMINATED BY '\n'
			FROM (
				SELECT
						`common_customers`.`customersMail1` AS `customersMail`,
						`common_customers`.`customersGruppe`

					FROM `common_customers`
					WHERE 1
						" . $thisWhere . "
						AND `common_customers`.`customersMail1`  != ''

				UNION

				SELECT
						`common_customers`.`customersMail2` AS `customersMail`,
						`common_customers`.`customersGruppe`
					FROM `common_customers`
					WHERE 1
						" . $thisWhere . "
						AND `common_customers`.`customersMail2`  != ''
			) AS `tempTable`
		";
		$rs = $dbConnection->db_query($sql);
	}
}

	/*
		SELECT
			`AuftragslistenCustomerMailAdress`,
			COUNT(`AuftragslistenCustomerGroupID`) AS `countID`
			FROM `_AuftragslistenCustomerMailExport`
			WHERE 1
			GROUP BY `AuftragslistenCustomerMailAdress`
		HAVING `countID` > 1

		-- -----------------------

		SELECT
			*
			FROM `_AuftragslistenCustomerMailExport`
		-> 7375

		-- -----------------------
		SELECT
			*
			FROM `_AuftragslistenCustomerMailExport`
			LEFT JOIN `newsletter_recipients`
			ON(`_AuftragslistenCustomerMailExport`.`AuftragslistenCustomerMailAdress` = `newsletter_recipients`.`customers_email_address`)
		-> 6758
		-- -----------------------
		SELECT
			*
			FROM `_AuftragslistenCustomerMailExport`
			LEFT JOIN `newsletter_recipients`
			ON(`_AuftragslistenCustomerMailExport`.`AuftragslistenCustomerMailAdress` = `newsletter_recipients`.`customers_email_address`)


			HAVING `newsletter_recipients`.`customers_email_address` IS NULL
		-- -----------------------

		SELECT
			*
			FROM `_AuftragslistenCustomerMailExport`
			WHERE `AuftragslistenCustomerMailAdress`
			NOT IN (
				SELECT
					`customers_email_address`
					FROM `newsletter_recipients`
					WHERE 1
			)
		-- -----------------------
		DELETE
			`_AuftragslistenCustomerMailExport`
			FROM `_AuftragslistenCustomerMailExport`
			INNER JOIN `newsletter_recipients`
			ON(`_AuftragslistenCustomerMailExport`.`AuftragslistenCustomerMailAdress` = `newsletter_recipients`.`customers_email_address`)


	-- -----------------------
	-- ANAG		`AuftragslistenCustomerGroupID`:3 -> shopGROUPID: 6
	-- NO_GROUP `AuftragslistenCustomerGroupID`:0 -> shopGROUPID: 3
	-- AGN `AuftragslistenCustomerGroupID`:7 ->  shopGROUPID: 17
	-- AGN `AuftragslistenCustomerGroupID`:1 ->  shopGROUPID: 15
	SELECT `AuftragslistenCustomerMailAdress` FROM `_AuftragslistenCustomerMailExport` WHERE 1 AND `AuftragslistenCustomerGroupID` = 3

	--
	SELECT
		*
		FROM `newsletter_recipients`
		INNER JOIN `_AuftragslistenCustomerMailExport`
		ON(`newsletter_recipients`.`customers_email_address` = `_AuftragslistenCustomerMailExport`.`AuftragslistenCustomerMailAdress`)

	-- -----------------------

	UPDATE
		`newsletter_recipients`
		INNER JOIN `_AuftragslistenCustomerMailExport`
		ON(`newsletter_recipients`.`customers_email_address` = `_AuftragslistenCustomerMailExport`.`AuftragslistenCustomerMailAdress`)

		SET `customers_status` =
			IF(`AuftragslistenCustomerGroupID` = 3, 6,
				IF(`AuftragslistenCustomerGroupID` = 7, 17,
					IF(`AuftragslistenCustomerGroupID` = 1, 15,
						IF(`AuftragslistenCustomerGroupID` = 0, 3,
							`customers_status`
						)
					)
				)
			)
	-- -----------------------
	UPDATE
		`newsletter_recipients`
		INNER JOIN `_AuftragslistenCustomerMailExport`
		ON(`newsletter_recipients`.`customers_email_address` = `_AuftragslistenCustomerMailExport`.`AuftragslistenCustomerMailAdress`)

		SET `customers_status` = CASE
			WHEN `AuftragslistenCustomerGroupID` = 3 THEN 6
			WHEN `AuftragslistenCustomerGroupID` = 7 THEN 17
			WHEN `AuftragslistenCustomerGroupID` = 1 THEN 15
			WHEN `AuftragslistenCustomerGroupID` = 0 THEN 3
			ELSE `customers_status`
		END
	*/
?>