<?php
	session_start();

	require_once('../config/configMandator.inc.php');
	require_once('../config/configParams_' . strtolower(MANDATOR) . '.inc.php');
	require_once('../config/configBasic.inc.php');
	require_once('../config/configFiles.inc.php');
	require_once('../config/configTables.inc.php');
	require_once('../inc/functions.inc.php');
	##require_once('header.inc.php');

	$thisSearchString = (trim($_GET["searchString"]));

	$content = '';

	$warningMessage = '';
	$errorMessage = '';
	$successMessage = '';
	$infoMessage = '';

	$useUf8Encode = false;

	if($thisSearchString != "") {

		$dbConnection = new DB_Connection();
		$db_open = $dbConnection->db_connect();

		$sql = "
			SELECT
				`orderCategoriesID`	 AS `searchResult`

				FROM `" . TABLE_ORDER_CATEGORIES . "`

				WHERE
					`orderCategoriesRelationID` = '" . $thisSearchString . "'
					AND
					`orderCategoriesID` LIKE '002%'
				LIMIT 1
		";

		$rs = $dbConnection->db_query($sql);

		list($searchResult) = mysqli_fetch_array($rs);

		$content = $searchResult;

		if($dbConnection->db_displayErrors() != "") {
			$errorMessage .= $dbConnection->db_displayErrors(). '<br />';
		}

		if($dbConnection) {
			$dbConnection->db_close();
		}
	}
	echo $content;
?>