<?php
	function loadAddProductForm($mode='add', $arrThisOrderDetailDatas=array(), $useCheckbox=0, $classRow='row0', $count=0, $autoCheckFirst=0) {
		global $userDatas, $arrOrderCategoriesTypeDatas, $arrOrderDetailDatas, $arrAdditionalCostsDatas, $arrGetUserRights, $arrOrderTypeDatas;

		$addProductForm = '';
			/*
			if($_COOKIE["isAdmin"] == '1'){
				echo '<pre>arrOrderDetailDatas';
				print_r($arrOrderDetailDatas);
				echo '</pre>';
				
				echo '<pre>arrThisOrderDetailDatas';
				print_r($arrThisOrderDetailDatas);
				echo '</pre>';
			}
			*/	

		if($mode == 'add') {

			$count = '';
			#$arrThisOrderDetailDatas["product"]["orderDocumentDetailProductName"] = $arrThisOrderDetailDatas["product"]["orderDocumentDetailProductName"]
			$addProductForm .= '
				<div class="addProductForm">
					<div class="addProductFormItem">
					<table border="1" cellpadding="0" cellspacing="0" width="95%" class="displayOrders">
						<thead>
							<tr>
								<th style="width:20px;text-align:center;"><img src="layout/icons/iconUse.png" title="Diesen Eintrag &uuml;bernehmen" alt="" /></th>
								<th>Artikel-Daten</th>
								<th>Menge</th>
								<th>Einzelpreis</th>
								<th>Farben</th>
								<th>Druckart</th>
								<th>Druckkosten</th>
								<th>Details</th>
							</tr>
						</thead>
				';

			$addProductForm .= '
						<tbody>
							<tr class="' . $classRow . '">
								<td><input type="checkbox" class="addProductsCountUse" name="xxx" disabled="disabled" checked="checked" value="1" /></td>
								<td style="text-align:left;">
									<input type="hidden" name="addProductsCount[]" value="1" />
									<input type="hidden" name="addProductDetails[ordersID][]" value="' . $arrThisOrderDetailDatas["product"]["orderDocumentDetailOrderID"] . '" />
									<table border="0" cellpadding="0" cellspacing="0" width="100%" class="noBorder">
										<tr>
											<td style="width: 120px;"><b>Art.-Kategorie:</b></td>
											<td>
												<select name="addProductDetails[ordersArtikelKategorieID][]" class="inputField_260" onchange="insertProductDatas2(this.form.name, $(this), \'{ordersArtikelNummer,ordersArtikelBezeichnung,ordersArtikelID]\');">
													<option value="0"></option>
													';
													
													if(!empty($arrOrderCategoriesTypeDatas)){
														foreach($arrOrderCategoriesTypeDatas as $thisCategoryKey => $thisCategoryData){
															$addProductForm .= '<option class="level_1" value="' . $thisCategoryKey . '">' . htmlentities($thisCategoryData["categoriesName"]) . '</option>';

															if(!empty($thisCategoryData["products"])){
																foreach($thisCategoryData["products"] as $thisProductKey => $thisProductValue){
																	$selected = '';
																	if($arrThisOrderDetailDatas["product"]["orderDocumentDetailProductKategorieID"] == $thisProductKey){
																		$selected = ' selected="selected" ';
																	}
																	$addProductForm .= '<option class="level_2" value="' . $thisProductKey . '" ' . $selected . ' >' . htmlentities($thisProductValue["productsName"]) . ' / ' . htmlentities($thisCategoryData["categoriesName"]) . ' / ' . $thisProductValue["productsProductNumberUse"] . ' / ' . $thisProductValue["productsID"] . '</option>';
																}
															}
														}
													}


			$addProductForm .= '				</select>
											</td>
										</tr>
										<tr>
											<td style="width: 110px;"><b>Art.-Nummer:</b></td>
											<td>
												<input type="text" class="inputField_260" name="addProductDetails[ordersArtikelNummer][]" value="' . $arrThisOrderDetailDatas["product"]["orderDocumentDetailProductNumber"] . '" onkeyup="loadProductDatas(this.form.name, $(this), \'{ordersArtikelNummer,ordersArtikelBezeichnung,ordersArtikelID,ordersArtikelKategorieID]\');" " /><!--readonly="readonly-->
												<input type="hidden" name="addProductDetails[ordersArtikelID][]" value="' . ($arrThisOrderDetailDatas["product"]["orderDocumentDetailProductID"]) . '" class="inputField_260" readonly="readonly" />
											</td>
										</tr>
										<tr>
											<td><b>Art.-Name:</b></td>
											<td><input type="text" class="inputField_260" name="addProductDetails[ordersArtikelBezeichnung][]" value="' . ((preg_replace("/\[[a-zA-Z]{0,}\]$/", "", preg_replace("/, ohne Druck/", "", preg_replace("/,[ ]{0,1}[0-9]{1,}\-farbiger Druck/", "", htmlentities(($arrThisOrderDetailDatas["product"]["orderDocumentDetailProductName"]))))))) . '" onkeyup="loadProductDatas(this.form.name, $(this), \'{ordersArtikelNummer,ordersArtikelBezeichnung,ordersArtikelID,ordersArtikelKategorieID]\');" readonly="readonly" /></td>
										</tr>
										<tr>
											<td><b>Farbnamen:</b></td>
											<td><textarea name="addProductDetails[ordersArtikelBezeichnungZusatz][]" class="inputTextarea_260x40">' . preg_replace("/Druckfarben: /", "", $arrThisOrderDetailDatas["product"]["orderDocumentDetailProductColorsText"]) . '</textarea></td>
										</tr>
				';

			$thisChecked = '';
			if($arrThisOrderDetailDatas["product"]["orderDocumentDetailsWithBorder"] == "1") {
				$thisChecked = ' checked="checked" ';
			}

			$addProductForm .= '
										<tr>
											<td><b>Umrandung:</b></td>
											<td><input type="checkbox" name="addProductDetails[ordersArtikelMitUmrandung][]" value="1" ' . $thisChecked . ' /> mit Umrandung ( plus ' . number_format(PRINT_WITH_BORDER_PER_PIECE, 2, ',', '.'). ' &euro; pro St&uuml;ck)</td>
										</tr>
				';

if(1) {
			$thisChecked = '';
			if($arrThisOrderDetailDatas["product"]["orderDocumentDetailsWithClearPaint"] == "1") {
				$thisChecked = ' checked="checked" ';
			}

			$addProductForm .= '
										<tr>
											<td><b>Klarlack:</b></td>
											<td><input type="checkbox" name="addProductDetails[ordersArtikelMitKlarlack][]" value="1" ' . $thisChecked . ' /> mit Klarlack (plus ' . number_format(PRINT_WITH_CLEARPAINT_PER_PIECE, 2, ',', '.'). ' &euro; pro St&uuml;ck)</td>
										</tr>
				';
}
			$addProductForm .= '
										<tr>
											<td><b>Aufdruck:</b></td>
											<td><textarea name="addProductDetails[ordersAufdruck][]" class="inputTextarea_260x40">' . preg_replace("/Aufdruck: /", "", $arrThisOrderDetailDatas["product"]["orderDocumentDetailPrintText"]) . '</textarea></td>
										</tr>
										<tr>
											<td><b>Kommission:</b></td>
											<td><textarea name="addProductDetails[ordersKommission][]" class="inputTextarea_260x40">' . preg_replace("/Kommission: /", "", $arrThisOrderDetailDatas["product"]["orderDocumentDetailKommission"]) . '</textarea></td>
										</tr>
										<tr>
											<td><b>Notiz:</b></td>
											<td><textarea name="addProductDetails[ordersNotiz][]" class="inputTextarea_260x40">' . preg_replace("/Notiz: /", "", $arrThisOrderDetailDatas["product"]["orderDocumentDetailNotiz"]) . '</textarea></td>
										</tr>
										<!--
										<tr>
											<td><b>+ Zusatzleisten:</b></td>
											<td>
												<input type="text" name="addProductDetails[ordersAdditionalArtikelMenge][]" class="inputField_50" value="' . $arrThisOrderDetailDatas["additionalProducts"]["orderDocumentDetailProductQuantity"] . '" />
												<select name="addProductDetails[ordersAdditionalArtikelKategorieID][]" class="inputSelect_200">
													<option value="0"></option>
													';

													if(!empty($arrOrderCategoriesTypeDatas)) {
														foreach($arrOrderCategoriesTypeDatas as $thisKey => $thisValue){
															if($thisValue["orderCategoriesParentID"] == '002') {
																$selected = '';
																if($arrThisOrderDetailDatas["additionalProducts"]["orderDocumentDetailProductKategorieID"] == $thisKey) { $selected = ' selected="selected" '; }
																if(strlen($thisKey) > 3) { $thisDistance = " &bull; "; }
																$addProductForm .= '<option class="'.$thisValue["orderCategoriesClass"].'" value="'.$thisKey.'" '.$selected.'>'.$thisValue["orderCategoriesName_" . $_COOKIE["mandator"]].'</option>';
															}
														}
													}

			$addProductForm .= '				</select>
											</td>
										</tr>
										-->
										<tr style="height:40px; border-top:1px dotted #333333;">
				';

			if($arrThisOrderDetailDatas["ordersPerExpress"] == '1') {
				$thisChecked = ' checked="checked" ';
			}
			else {
				$thisChecked = '';
			}

			$addProductForm .= '
											<td><b>per Express:</b></td>
											<td><input type="checkbox" name="addProductDetails[orderPerShippingExpress][]" class="selectShippingExpress" value="1" ' . $thisChecked . '/> <span class="expressCostsArea" style="display:none; white-space:nowrap;"><input type="text" name="addProductDetails[orderShippingExpressValue][]" class="inputField_50" value="' . number_format(EXPRESS_COSTS_PER_PIECE, 2, ",", "") . '" /> &euro; pro St&uuml;ck</span></td>
										</tr>
										<tr style="height:40px; border-top:1px dotted #333333;">
											<td><b>Bestellart:</b></td>
											<td>
												<select name="addProductDetails[ordersOrderType][]" class="inputSelect_200">
				';
											if(!empty($arrOrderTypeDatas)) {
												foreach($arrOrderTypeDatas as $thisKey => $thisValue) {
													$selected = '';
													if($thisKey == $arrThisOrderDetailDatas["product"]["orderDocumentDetailOrderType"]) {
														$selected = ' selected="selected" ';
													}
													$addProductForm .= '<option value="' . $thisKey . '" ' . $selected . '>' . $arrOrderTypeDatas[$thisKey]["orderTypesName"] . '</option>';
												}
											}

			$addProductForm .= '

												</select>
											</td>
										</tr>
				';


			$thisChecked = '';
			if($arrThisOrderDetailDatas["product"]["orderDocumentDetailDirectSale"] == "1") {
				$thisChecked = ' checked="checked" ';
			}
			$addProductForm .= '<tr style="height:40px; border-top:1px dotted #333333;">
									<td><b>per Direkt-Verkauf:</b></td>
									<td>
										<input type="checkbox" name="addProductDetails[orderPerDirectSale][]" class="selectDirectSale" value="1" ' . $thisChecked . '/>
										Direktverkauf durch Vertreter?
									</td>
								</tr>
			';

			$addProductForm .= '
									</table>
								</td>
								<td style="text-align:right;"><input type="text" class="inputField_50" name="addProductDetails[ordersArtikelMenge][]" value="' . $arrThisOrderDetailDatas["product"]["orderDocumentDetailProductQuantity"] . '" /></td>
								<td style="text-align:right;">
									<input type="text" class="inputField_50" name="addProductDetails[ordersSinglePreis][]" value="' . number_format(preg_replace("/,/", ".", $arrThisOrderDetailDatas["product"]["orderDocumentDetailProductSinglePrice"]), 2, ',', '') . '" /> &euro;

								</td>
								<td style="text-align:left;">
									<select name="addProductDetails[ordersArtikelPrintColorsCount][]" class="inputSelect_40">
										';
										for($i = 0 ; $i < 10 ; $i++) {
											$selected = '';
											if($i == $arrThisOrderDetailDatas["product"]["orderDocumentDetailProductColorsCount"]) {
												$selected = ' selected="selected" ';
											}
											if($i == 0){ $thisText = '0 / unbedruckt'; }
											else { $thisText = $i; }
											$addProductForm .= '<option value="' . $i . '" '.$selected.' >' . $thisText . '</option>';
										}

			$addProductForm .= '	</select>
								</td>
								<td>
									<select name="addProductDetails[ordersAdditionalCostsID][]" class="inputSelect_100">
				';

										// // onchange="insertAdditionalCosts('xxx','yyy','zzz');"
										foreach($arrAdditionalCostsDatas as $thisKey => $thisValue) {
											$selected = '';

											$addProductForm .= '<option value="' . $thisKey . '" title="' . $arrAdditionalCostsDatas[$thisKey]["additionalCostsName"] . ' - ' . number_format($arrAdditionalCostsDatas[$thisKey]["additionalCostsPrice"], 2, ',', '') . '" '.$selected . '>' . ($arrAdditionalCostsDatas[$thisKey]["additionalCostsName"]) . '</option>';
											// echo 'editOrdersAdditionalCostsPrice['.$thisKey.']" class="inputField_50" value="'.number_format($arrAdditionalCostsDatas[$thisKey]["additionalCostsPrice"], 2).'" '.$checked . $readonly . ' /> &euro;
										}

			$addProductForm .= '	</select>
								</td>
								<td style="text-align:right; white-space:nowrap;"><input type="text" name="addProductDetails[ordersAdditionalCosts][]" class="inputField_50" value="' . number_format($arrThisOrderDetailDatas["printCosts"]["orderDocumentDetailProductSinglePrice"], 2, ',', '') . '" /> &euro;<br>pro Farbe</td>
								<td></td>
							</tr>
						</tbody>
					</table>
					<div class="actionButtonsArea"><img src="layout/icons/iconDelete.png" class="buttonDeleteProduct" width="16" height="16" title="Element entfernen" alt="Element entfernen" /></div>

			';
		}
		else if($mode == 'select' || $mode == 'edit' || $mode == 'convert') {
			if($mode == 'select') {
				$arrThisOrderDetailDatas["product"]["orderDocumentDetailProductName"] = htmlentities(($arrThisOrderDetailDatas["product"]["orderDocumentDetailProductName"]));
			}
			else if($mode == 'edit'){
				$arrThisOrderDetailDatas["product"]["orderDocumentDetailProductName"] = htmlentities(($arrThisOrderDetailDatas["product"]["orderDocumentDetailProductName"]));
			}
			else if($mode == 'convert'){
				$arrThisOrderDetailDatas["product"]["orderDocumentDetailProductName"] = htmlentities(($arrThisOrderDetailDatas["product"]["orderDocumentDetailProductName"]));
			}
			$addProductForm .= '
				<div class="addProductForm">
					<div class="addProductFormItem">
					<table border="1" cellpadding="0" cellspacing="0" width="95%" class="displayOrders">
						<thead>
							<tr>
								<th style="width:20px;text-align:center;"><img src="layout/icons/iconUse.png" title="Diesen Eintrag &uuml;bernehmen" alt="" /></th>
								<th>Artikel-Daten</th>
								<th>Menge</th>
								<th>Einzelpreis</th>
								<th>Farben</th>
								<th>Druckart</th>
								<th>Satzkosten <img src="layout/menueIcons/menueSidebar/help.png" class="buttonShowFilmCosts" width="12" height="12" alt="Info" title="Film- / Satzkosten anzeigen"/></th>
								<th>Details</th>
							</tr>
						</thead>
				';
			$thisItemChecked = '';
			if($arrThisOrderDetailDatas["itemChecked"] == "1") {
				$thisItemChecked = ' checked="checked" ';
			}
			else if($count == 0) {
				#if($autoCheckFirst) { $thisItemChecked = ' checked="checked" '; }
			}
			$addProductForm .= '
						<tbody>
							<tr class="' . $classRow . '">
								<td style="text-align:center;">
				';

			if($mode == 'select'){
				$addProductForm .= '
										<input type="checkbox" class="selectOrderID" title="&Uuml;bernehmen" id="selectOrdersIDs['.$count.']" name="selectOrdersIDs['.$count.']" value="'.$count.'" ' . $thisItemChecked . ' />
					';
			}
			else if($mode == 'edit' || $mode == 'convert') {
				if(($_POST["editDocType"] == 'AB' || $_POST["editDocType"] == 'AN')){
					$addProductForm .= '<input type="checkbox" id="selectOrdersIDs_'.$count.'" name="selectOrdersIDs['.$count.']" checked="checked" value="'.$count.'" />';
				}
				else {
					if($_COOKIE["isAdmin"] == '1'){
						$addProductForm .= '<input type="checkbox" id="selectOrdersIDs_'.$count.'" name="selectOrdersIDs['.$count.']" checked="checked" value="'.$count.'" />';
					}
					else{
						$addProductForm .= '
											<input type="checkbox" name="dummy" disabled="disabled" checked="checked" value="1" />
											<input type="hidden" id="selectOrdersIDs_'.$count.'" name="selectOrdersIDs['.$count.']" value="'.$count.'" />
						';
					}
				}
			}
			$addProductForm .= '
									</td>
									<td style="text-align:left;">
										<table border="0" cellpadding="0" cellspacing="0" width="100%" class="noBorder">
				';

			if($mode == 'select') {

				$addProductForm .= '
											<tr>
												<td style="width: 120px;"><b>Eingang am:</b></td>
												<td>
													<input type="hidden" name="selectOrdersDetails['.$count.'][ordersBestellDatum]" value="' . ($arrThisOrderDetailDatas["product"]["orderDocumentBestellDatum"]) . '" />' . '
													<span class="ordersBestellDatum">' .
													(formatDate($arrThisOrderDetailDatas["product"]["orderDocumentBestellDatum"], 'display')) . '
													</span>
												</td>
											</tr>
											<tr>
												<td style="width: 120px;"><b>Vertreter:</b></td>
												<td><b style="color:#990000;">' . $arrThisOrderDetailDatas["product"]["ordersVertreterName"] . '</b></td>
											</tr>
					';


					if($arrThisOrderDetailDatas["product"]["orderDocumentFreigabeDatum"] > 0) {
						$addProductForm .= '
											<tr>
												<td><b>Freigabe am:</b></td>
												<td>
													<input type="hidden" name="selectOrdersDetails['.$count.'][ordersFreigabeDatum]" value="' . ($arrThisOrderDetailDatas["product"]["orderDocumentFreigabeDatum"] ) . '" />' . '
													<span class="ordersFreigabeDatum">' .
														(formatDate($arrThisOrderDetailDatas["product"]["orderDocumentFreigabeDatum"], 'display')) . '
													</span>
												</td>
											</tr>
							';
					}
			}
			$addProductForm .= '

											<tr style="height:40px; border-top:1px dotted #333333;">
												<td style="width: 120px;"><b>Art.-Kategorie:</b></td>
												<td>
													<input type="hidden" name="selectOrdersDetails['.$count.'][ordersID]" value="' . ($arrThisOrderDetailDatas["product"]["orderDocumentDetailOrderID"]) . '" />' .'
													<select name="selectOrdersDetails['.$count.'][ordersArtikelKategorieID]" class="inputField_260" onchange="insertProductDatas2(this.form.name, $(this), \'{ordersArtikelNummer,ordersArtikelBezeichnung,ordersArtikelID]\');">
													<option value="0"></option>
													';
													
													if(!empty($arrOrderCategoriesTypeDatas)){
														foreach($arrOrderCategoriesTypeDatas as $thisCategoryKey => $thisCategoryData){
															$addProductForm .= '<option class="level_1" value="' . $thisCategoryKey . '">' . htmlentities($thisCategoryData["categoriesName"]) . '</option>';

															if(!empty($thisCategoryData["products"])){
																foreach($thisCategoryData["products"] as $thisProductKey => $thisProductValue){
																	$selected = '';
																	if(
																		$arrThisOrderDetailDatas["product"]["orderDocumentDetailProductKategorieID"] == $thisProductKey																		
																	){
																		$selected = ' selected="selected" ';
																	}
																	$addProductForm .= '<option class="level_2" value="' . $thisProductKey . '" ' . $selected . ' >' . htmlentities($thisProductValue["productsName"]) . ' / ' . htmlentities($thisCategoryData["categoriesName"]) . ' / ' . $thisProductValue["productsProductNumberUse"] . ' / ' . $thisProductValue["productsID"] . '</option>';
																}
															}
														}
													}
													
			$addProductForm .= '				</select>
												</td>
											</tr>
											<tr>
												<td style="width: 110px;"><b>Art.-Nummer:</b></td>
												<td>
													<input type="text" name="selectOrdersDetails['.$count.'][ordersArtikelNummer]" value="' . ($arrThisOrderDetailDatas["product"]["orderDocumentDetailProductNumber"]) . '" class="inputField_260" readonly="readonly" /><!--readonly="readonly-->
													<input type="hidden" name="selectOrdersDetails['.$count.'][ordersArtikelID]" value="' . ($arrThisOrderDetailDatas["product"]["orderDocumentDetailProductID"]) . '" class="inputField_260" readonly="readonly" />
												</td>
											</tr>
											<tr>
												<td><b>Art.-Name:</b></td>
												<td><input type="text" name="selectOrdersDetails['.$count.'][ordersArtikelBezeichnung]" value="' . addslashes(preg_replace("/[,][ ]{1,}$/", "", $arrThisOrderDetailDatas["product"]["orderDocumentDetailProductName"])) . '" class="inputField_260" readonly="readonly" /></td>
											</tr>
											<tr>
												<td><b>Farbnamen:</b></td>
				';
			// weiss|schwarz|silber
			#$thisColorNames = preg_replace("/\[Pröll [0-9]{1,5}\][ ]?/", "", $arrThisOrderDetailDatas["product"]["orderDocumentDetailProductColorsText"]);
			$thisColorNames = $arrThisOrderDetailDatas["product"]["orderDocumentDetailProductColorsText"];
			// BOF FILTER COLOR CODES
			#dd('thisColorNames');

			$arrTempColors = explode(' / ', $thisColorNames);
			$arrNewColors = array();
			foreach($arrTempColors as $thisColorName){
				$thisColorName = trim($thisColorName);
				if(preg_match("/(weiss)|(schwarz)|(silber)/", $thisColorName)){
					$thisColorName = preg_replace("/\[Pröll [0-9]{1,5}\]/", "", $thisColorName);
				}
				$arrNewColors[] = $thisColorName;
			}
			if(!empty($arrNewColors)){ $thisColorNames = implode(" / ", $arrNewColors); }

			// EOF FILTER COLOR CODES
			$addProductForm .= '
												<td><textarea name="selectOrdersDetails['.$count.'][ordersArtikelBezeichnungZusatz]" class="inputTextarea_260x24">' . $thisColorNames . '</textarea></td>
											</tr>
				';

			$thisChecked = '';
			if($arrThisOrderDetailDatas["product"]["orderDocumentDetailsWithBorder"] == "1") {
				$thisChecked = ' checked="checked" ';
			}

			$addProductForm .= '
											<tr>
												<td><b>Umrandung:</b></td>
												<td><input type="checkbox" name="selectOrdersDetails['.$count.'][ordersArtikelMitUmrandung]" value="1" ' . $thisChecked . ' /> mit Umrandung (plus ' . number_format(PRINT_WITH_BORDER_PER_PIECE, 2, ',', '.'). ' &euro; pro St&uuml;ck)</td>
											</tr>
				';

if(1) {
			$thisChecked = '';
			if($arrThisOrderDetailDatas["product"]["orderDocumentDetailsWithClearPaint"] == "1") {
				$thisChecked = ' checked="checked" ';
			}

			$addProductForm .= '
										<tr>
											<td><b>Klarlack:</b></td>
											<td><input type="checkbox" name="selectOrdersDetails['.$count.'][ordersArtikelMitKlarlack]" value="1" ' . $thisChecked . ' /> mit Klarlack (plus ' . number_format(PRINT_WITH_CLEARPAINT_PER_PIECE, 2, ',', '.'). ' &euro; pro St&uuml;ck)</td>
										</tr>
				';
}
			$addProductForm .= '
											<tr>
												<td><b>Aufdruck:</b></td>
												<td><textarea name="selectOrdersDetails['.$count.'][ordersAufdruck]" class="inputTextarea_260x24">' . $arrThisOrderDetailDatas["product"]["orderDocumentDetailPrintText"]. '</textarea></td>
											</tr>
											<tr>
												<td><b>Kommission:</b></td>
												<td><textarea name="selectOrdersDetails['.$count.'][ordersKommission]" class="inputTextarea_260x24">' . $arrThisOrderDetailDatas["product"]["orderDocumentDetailKommission"]. '</textarea></td>
											</tr>
											<tr>
												<td><b>Notiz:</b></td>
												<td><textarea name="selectOrdersDetails['.$count.'][ordersNotiz]" class="inputTextarea_260x24">' . $arrThisOrderDetailDatas["product"]["orderDocumentDetailNotiz"]. '</textarea></td>
											</tr>
											<!--
											<tr>
												<td><b>+ Zusatzleisten:</b></td>
												<td>
													<input type="text" name="selectOrdersDetails['.$count.'][ordersAdditionalArtikelMenge]" class="inputField_50" value="' . ($arrThisOrderDetailDatas["additionalProducts"]["orderDocumentDetailProductQuantity"]) . '" />
													<select name="selectOrdersDetails['.$count.'][ordersAdditionalArtikelKategorieID]" class="inputSelect_200">
														<option value="0"></option>
				';
														if(!empty($arrOrderCategoriesTypeDatas)) {
															foreach($arrOrderCategoriesTypeDatas as $thisKey => $thisValue){
																if($thisValue["orderCategoriesParentID"] == '002') {
																	$selected = '';
																	if($thisKey	== $arrThisOrderDetailDatas["additionalProducts"]["orderDocumentDetailProductKategorieID"]) {
																		$selected = ' selected="selected" ';
																	}
																	if(strlen($thisKey) > 3) { $thisDistance = " &bull; "; }
																	$addProductForm .= '<option class="'.$thisValue["orderCategoriesClass"].'" value="'.$thisKey.'" '.$selected.'>'.$thisValue["orderCategoriesName_" . strtolower(MANDATOR)].'</option>';
																}
															}
														}
			$addProductForm .= '					</select>
												</td>
											</tr>
											-->
											<tr style="height:40px; border-top:1px dotted #333333;">
												<td><b>per Express:</b></td>
												<td>
				';


			if($_COOKIE["isAdmin"] == '1'){
				#echo '<pre>';
				#print_r($arrThisOrderDetailDatas);
				#echo '</pre>';
				#echo $mode;
			}

			$arrThisOrderDetailDatas["expressCosts"]["orderDocumentDetailProductSinglePrice"] = convertDecimal($arrThisOrderDetailDatas["expressCosts"]["orderDocumentDetailProductSinglePrice"], 'store');
			if($arrThisOrderDetailDatas["expressCosts"]["orderDocumentDetailProductSinglePrice"] > 0) {
				$thisChecked = ' checked="checked" ';
				$thisDisplay = 'inline';

			}
			else if($arrThisOrderDetailDatas["expressCosts"]["orderDocumentPerExpress"] == '1') {
			#else if($arrThisOrderDetailDatas["expressCosts"]["orderDocumentPerExpress"] == '1' || !empty($arrThisOrderDetailDatas["expressCosts"])) {
				$thisChecked = ' checked="checked" ';
				$thisDisplay = 'inline';
				if($mode == 'select'){
					$arrThisOrderDetailDatas["expressCosts"]["orderDocumentDetailProductSinglePrice"] = EXPRESS_COSTS_PER_PIECE;
				}

			}
			else {
				$thisChecked = '';
				$thisDisplay = 'none';
				if($mode == 'select'){
					$arrThisOrderDetailDatas["expressCosts"]["orderDocumentDetailProductSinglePrice"] = EXPRESS_COSTS_PER_PIECE;
				}
			}

			$addProductForm .= '
													<input type="checkbox" name="selectOrdersDetails['.$count.'][orderPerShippingExpress]" class="selectShippingExpress" value="1" ' . $thisChecked . ' />
													<span class="expressCostsArea" style="display:' . $thisDisplay . '; white-space:nowrap;"><input type="text" name="selectOrdersDetails['.$count.'][orderShippingExpressValue]" class="inputField_50" value="' . number_format($arrThisOrderDetailDatas["expressCosts"]["orderDocumentDetailProductSinglePrice"], 2, ",", "") . '" /> &euro; pro St&uuml;ck</span>
												</td>
											</tr>
											<tr style="height:40px; border-top:1px dotted #333333;">
												<td><b>Bestellart:</b></td>
												<td>
													<select name="selectOrdersDetails['.$count.'][ordersOrderType]" class="inputSelect_200">
				';
												if(!empty($arrOrderTypeDatas)) {
													foreach($arrOrderTypeDatas as $thisKey => $thisValue) {
														$selected = '';
														if($thisKey == $arrThisOrderDetailDatas["product"]["orderDocumentDetailOrderType"]) {
															$selected = ' selected="selected" ';
														}
														$addProductForm .= '<option value="' . $thisKey . '" ' . $selected . '>' . $arrOrderTypeDatas[$thisKey]["orderTypesName"] . '</option>';
													}
												}

			$addProductForm .= '
													</select>
												</td>
											</tr>
				';



			$thisChecked = '';
			if($arrThisOrderDetailDatas["product"]["orderDocumentDetailDirectSale"] == "1") {
				$thisChecked = ' checked="checked" ';
			}

			$addProductForm .= '		<tr style="height:40px; border-top:1px dotted #333333;">
											<td><b>per Direkt-Verkauf:</b></td>
											<td>
												<input type="checkbox" name="selectOrdersDetails['.$count.'][orderPerDirectSale]" class="selectDirectSale" value="1" ' . $thisChecked . '/>
												Direktverkauf durch Vertreter?
											</td>
										</tr>
				';

			$addProductForm .= '
										</table>
									</td>
									<td style="text-align:right;">
										<input type="text" class="inputField_50" name="selectOrdersDetails['.$count.'][ordersArtikelMenge]" value="' . ($arrThisOrderDetailDatas["product"]["orderDocumentDetailProductQuantity"]). '" />
									</td>
									<td style="text-align:right; white-space:nowrap;">
										<input type="text" class="inputField_50" name="selectOrdersDetails['.$count.'][ordersSinglePreis]" value="' . number_format(preg_replace("/,/", ".", $arrThisOrderDetailDatas["product"]["orderDocumentDetailProductSinglePrice"]), 2, ',', '') . '" /> &euro;
										<hr />
										<div>
											<div style="text-align:left;">
												<b>EK-Preis:</b>
											</div>
											<div>
												<span style="color:#FF0000;background-color:#EEE;padding:2px; line-height: 18px;font-size:16px;font-weight:bold;" class="displayEkPrice inputField_70;">
													' . number_format(preg_replace("/,/", ".", $arrThisOrderDetailDatas["product"]["productEkPrice"]), 2, ',', '') . '
												</span> &euro;
											</div>
										</div>
									</td>
									<td style="text-align:left;">
										<select name="selectOrdersDetails['.$count.'][ordersArtikelPrintColorsCount]" class="inputSelect_40">
				';
										for($i = 0 ; $i < 10 ; $i++) {
											$selected = '';
											if($arrThisOrderDetailDatas["product"]["orderDocumentDetailProductColorsCount"] == '' && $i == 0) {
												$selected = ' selected="selected" ';
											}
											else if($i == $arrThisOrderDetailDatas["product"]["orderDocumentDetailProductColorsCount"]) {
												$selected = ' selected="selected" ';
											}
											if($i == 0){ $thisText = '0 / unbedruckt'; }
											else { $thisText = $i; }
											$addProductForm .= '<option value="' . $i . '" '.$selected.' >' . $thisText . '</option>';
										}
			$addProductForm .= '		</select>
									</td>
									<td>
										<select name="selectOrdersDetails['.$count.'][ordersAdditionalCostsID]" class="inputSelect_100">
				';
									#$thisAdditionalCosts = $arrAdditionalCostsDatas[$arrThisOrderDetailDatas["ordersAdditionalCostsID"]]["additionalCostsPrice"];
									#if($arrThisOrderDetailDatas["ordersAdditionalCosts"] == 0) {
										#$arrThisOrderDetailDatas["ordersAdditionalCosts"] = $thisAdditionalCosts;
									#}
									foreach($arrAdditionalCostsDatas as $thisKey => $thisValue) {
										$selected = '';
										if($thisKey == $arrThisOrderDetailDatas["printCosts"]["ordersAdditionalCostsID"]) {
											$selected = ' selected="selected" ';
										}
										if($thisKey == $arrThisOrderDetailDatas["product"]["orderDocumentDetailProductPrintType"]) {
											$selected = ' selected="selected" ';
										}
										// <option value="' . $thisKey . '" title="' . $arrAdditionalCostsDatas[$thisKey]["additionalCostsName"] . '" '.$selected . '>' . ($arrAdditionalCostsDatas[$thisKey]["additionalCostsShortName"]) . ' - ' . number_format($arrAdditionalCostsDatas[$thisKey]["additionalCostsPrice"], 2, ',', '') . ' &euro;</option>';
										$addProductForm .= '<option value="' . $thisKey . '" title="'. $arrAdditionalCostsDatas[$thisKey]["additionalCostsName"] . ' - ' . number_format($arrAdditionalCostsDatas[$thisKey]["additionalCostsPrice"], 2, ',', '') . '" '.$selected . '>' . ($arrAdditionalCostsDatas[$thisKey]["additionalCostsName"]) . '</option>';
										// editOrdersAdditionalCostsPrice['.$thisKey.']" class="inputField_50" value="'.number_format($arrAdditionalCostsDatas[$thisKey]["additionalCostsPrice"], 2).'" '.$checked . $readonly . ' /> &euro;

									}
			$addProductForm .= '
										</select>
									</td>
									<td style="text-align:right; white-space:nowrap;">
										<input type="text" name="selectOrdersDetails['.$count.'][ordersAdditionalCosts]" id="ordersAdditionalCosts_' . $count . '" class="inputField_50" value="' . number_format($arrThisOrderDetailDatas["printCosts"]["orderDocumentDetailProductSinglePrice"], 2, ',', '') . '" /> &euro;<br>pro Farbe
									</td>
									<td style="white-space:nowrap;">';

										if($arrThisOrderDetailDatas["ordersNotizen"] != "") {
											$addProductForm .= '<span class="toolItem"><img src="layout/icons/iconRtf.gif" class="buttonNotice" width="16" height="16" alt="Bemerkung: '.$arrThisOrderDetailDatas["ordersNotizen"].'" /></span>';
										}
										else {
											$addProductForm .= '<span class="toolItem"><img src="layout/spacer.gif" width="16" height="16" alt="" /></span>';
										}
										if($arrGetUserRights["editOrders"]) {
											$arrPathinfo = preg_replace("/([\?|\&].*){1,}/", "", pathinfo($_SERVER["REQUEST_URI"]));

											#$arrPathinfo = pathinfo($_SERVER["QUERY_STRING"]);
											$redirectURL = $arrPathinfo["dirname"] . '/' . $arrPathinfo["basename"];
											$addProductForm .= '<span class="toolItem"><a href="'.PAGE_EDIT_PROCESS.'?editID=' . $arrThisOrderDetailDatas["product"]["orderDocumentDetailOrderID"] . '&redirect='.$redirectURL.'&documentType='.$_REQUEST["documentType"].'"><img src="layout/icons/iconEdit.gif" width="16" height="16" title="Vorgang bearbeiten (Datensatz ' . $arrThisOrderDetailDatas["product"]["orderDocumentDetailOrderID"] . ')" alt="Bearbeiten" /></a></span>';
											#$addProductForm .= '<span class="toolItem"><a href="'.PAGE_EDIT_PROCESS.'?editID=' . $arrThisOrderDetailDatas["product"]["orderDocumentDetailOrderID"] . '&redirect='.$_SERVER["REQUEST_URI"].'&documentType='.$_REQUEST["documentType"].'"><img src="layout/icons/iconEdit.gif" width="16" height="16" title="Vorgang bearbeiten (Datensatz ' . $arrThisOrderDetailDatas["product"]["orderDocumentDetailOrderID"] . ')" alt="Bearbeiten" /></a></span>';
											#$addProductForm .= '<span class="toolItem"><a href="'.PAGE_EDIT_PROCESS.'?editID=' . $arrThisOrderDetailDatas["product"]["orderDocumentDetailOrderID"] . '&redirect='.$_SERVER["PHP_SELF"].'&documentType='.$_REQUEST["documentType"].'"><img src="layout/icons/iconEdit.gif" width="16" height="16" title="Vorgang bearbeiten (Datensatz ' . $arrThisOrderDetailDatas["product"]["orderDocumentDetailOrderID"] . ')" alt="Bearbeiten" /></a></span>';
										}
										else {
											$addProductForm .= '<span class="toolItem"><img src="layout/spacer.gif" width="16" height="16" alt="" /></span>';
										}
										if($ds["ordersMailAdress"] != "") {
											$addProductForm .= '<span class="toolItem"><a href="mailto:'.$ds["ordersMailAdress"].'"><img src="layout/icons/iconMail.gif" width="16" height="14" title="Kunde &quot;'.($ds["ordersKundenName"]). '&quot; (KNR: '.$ds["ordersKundennummer"].') per Mail kontaktieren" alt="Bearbeiten" /></a></span>';
										}
										else {
											$addProductForm .= '<span class="toolItem"><img src="layout/spacer.gif" width="16" height="16" alt="" /></span>';
										}
			$addProductForm .= '	</td>
								</tr>
							</tbody>
						</table>
			';
		}

		$addProductForm .= '
						<hr />
					</div>
				</div>
		';

		$addProductForm = removeUnnecessaryChars($addProductForm);
		return $addProductForm;
	}
?>