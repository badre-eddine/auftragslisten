<?php
	function loadCardDatas($loadCardTemplate, $selectCardIDs, $selectCardDetails) {
		global $arrAdditionalCostsDatas, $loadedDocumentDatas, $arrOrderCategoriesTypeDatas, $arrOrderTypeDatas, $userDatas, $thisTotalSumPrice, $isOTLG;

		if($isOTLG > 0){
			#$arrCustomerGroupsProducts = getCustomerGroupsProducts($isOTLG, 'all');
			##$arrCustomerGroupsProducts = getCustomerGroupsProducts($isOTLG, 'non-products');
			#$arrCustomerGroupProductNumbers = 			$arrCustomerGroupsProducts["arrCustomerGroupProductNumbers"];
			#$arrCustomerGroupProductParentCategoryIDs = $arrCustomerGroupsProducts["arrCustomerGroupProductParentCategoryIDs"];
		}

/*
if($_COOKIE["isAdmin"] == '1'){
	echo 'selectCardDetails<pre>';
	print_r($selectCardDetails);
	echo '</pre>';
}
*/
		// BOF DEFINE ARRAY FOR EXCLUSIVE KZH FOR SATZK MODEL-NUMBER
			$arrProductsWerkzeugkosten = array(
								"101-LE 52",
								"101-LE 52_LEI",
								"101-LED 52",
								"101-LED 52_LEI",
								"101-MKE 52",
								"101-MKE 52_LEI",
								"101-MKED 52",
								"101-MKED 52_LEI",
								"101-NPE 52",
								"101-NPE 52_LEI",
								"101-NPED 52",
								"101-NPED 52_LEI",
								"101-TNE 52",
								"101-TNE 52_LEI",
								"101-TNED 52",
								"101-TNED 52_LEI",
								"102-500.02",
								"102-500.02_LEI",
								"102-600.02",
								"102-600.02_LEI",
								"108-500.02",
								"108-500.02_LEI",
								"108-600.02",
								"108-600.02_LEI",
								"109-500.02",
								"109-500.02_LEI",
								"109-600.02",
								"109-600.02_LEI",
								"110-500.02",
								"110-500.02_LEI",
								"110-600.02",
								"110-600.02_LEI"
				);
		// EOF DEFINE ARRAY FOR EXCLUSIVE KZH FOR SATZK MODEL-NUMBER

		$loadCardTemplate = removeUnnecessaryChars($loadCardTemplate);
		$getBasketItemsTemplate = preg_match_all("/<!-- ###CARD_ITEMS_START### -->(.*)<!-- ###CARD_ITEMS_END### -->/ismU", $loadCardTemplate, $arrFound);
		$getBasketItemsTemplate = $arrFound[1][0];

		$getBasketItemContent = array();
		if(!empty($selectCardIDs)) {
			$countItem = 0;
			$totalPrice = 0;

			foreach($selectCardIDs as $thisKey => $thisValue) {
				/*
				if($_COOKIE["isAdmin"] == '1'){
					echo 'selectCardDetails<pre>';
					print_r($selectCardDetails);
					echo '</pre>';
				}
				*/
				$thisQuantity = $selectCardDetails[$thisValue]["ordersArtikelMenge"];
				if($thisQuantity > 0 ){
					// BOF PRODUCTS PRICE
					if($countItem%2 == 0) { $thisRowColor = "#EEEEEE"; }
					else { $thisRowColor = "#FFFFFF"; }
					$thisRowColor = "#FFFFFF";

					$createBasketItemContent = $getBasketItemsTemplate;
					#echo mb_detect_encoding($selectCardDetails[$thisValue]["ordersArtikelBezeichnung"]);
					if(mb_detect_encoding($selectCardDetails[$thisValue]["ordersArtikelBezeichnung"]) == "UTF-8") {
						// $selectCardDetails[$thisValue]["ordersArtikelBezeichnung"] = ($selectCardDetails[$thisValue]["ordersArtikelBezeichnung"]);
						#$selectCardDetails[$thisValue]["ordersArtikelBezeichnung"] = ($selectCardDetails[$thisValue]["ordersArtikelBezeichnung"]);
						#echo $selectCardDetails[$thisValue]["ordersArtikelBezeichnung"].': '.mb_detect_encoding($selectCardDetails[$thisValue]["ordersArtikelBezeichnung"]) . '<br />';
						#$selectCardDetails[$thisValue]["ordersArtikelBezeichnung"] = '' .  htmlentities(($selectCardDetails[$thisValue]["ordersArtikelBezeichnung"]));
						$selectCardDetails[$thisValue]["ordersArtikelBezeichnung"] = '' .  (($selectCardDetails[$thisValue]["ordersArtikelBezeichnung"]));
					}
					else {						
						#$selectCardDetails[$thisValue]["ordersArtikelBezeichnung"] = '' .  htmlentities(($selectCardDetails[$thisValue]["ordersArtikelBezeichnung"]));
						$selectCardDetails[$thisValue]["ordersArtikelBezeichnung"] = '' .  (($selectCardDetails[$thisValue]["ordersArtikelBezeichnung"]));
					}
					#echo $selectCardDetails[$thisValue]["ordersArtikelBezeichnung"];
					$selectCardDetails[$thisValue]["ordersArtikelBezeichnung"] = htmlentities($selectCardDetails[$thisValue]["ordersArtikelBezeichnung"]);

					if($selectCardDetails[$thisValue]["ordersArtikelPrintColorsCount"] > 0) {
						$thisDescription = preg_replace('/, ' . $selectCardDetails[$thisValue]["ordersArtikelPrintColorsCount"] . '-farbiger Druck/', '', $selectCardDetails[$thisValue]["ordersArtikelPrintColorsCount"]);
						$thisDescription = ($selectCardDetails[$thisValue]["ordersArtikelBezeichnung"]) . ', ' . $selectCardDetails[$thisValue]["ordersArtikelPrintColorsCount"] . '-farbiger Druck';
					}
					else {
						$thisDescription = preg_replace('/, ' . 'ohne Druck/', '', $selectCardDetails[$thisValue]["ordersArtikelPrintColorsCount"]);
						// $thisDescription = $selectCardDetails[$thisValue]["ordersArtikelBezeichnung"] . ', ' . 'ohne Druck';
						$thisDescription = $selectCardDetails[$thisValue]["ordersArtikelBezeichnung"];
					}

					if($selectCardDetails[$thisValue]["ordersArtikelBezeichnungZusatz"] != '') {
						#$thisDescription .= '<br>' . addslashes($selectCardDetails[$thisValue]["ordersArtikelBezeichnungZusatz"]);
					}

					$thisDescription = preg_replace("/[ ]{1,},/", ",", $thisDescription);

					$thisProductNumber = $selectCardDetails[$thisValue]["ordersArtikelNummer"];
					$thisProductID = $selectCardDetails[$thisValue]["ordersArtikelID"];
					
					if($isOTLG > 0){
						#$thisDescription = $arrCustomerGroupProductNumbers[$thisProductNumber] . ' - ' . $thisDescription;
					}


					$thisSinglePrice = convertDecimal($selectCardDetails[$thisValue]["ordersSinglePreis"], 'store');
					$thisTotalPrice = $thisQuantity * $thisSinglePrice;
					$thisPos = ($countItem + 1);
					$thisProductNumber = $selectCardDetails[$thisValue]["ordersArtikelNummer"];
					$thisProductID = $selectCardDetails[$thisValue]["ordersArtikelID"];
					
					$thisPosType = 'product';
					$thisProductCategorieID = $selectCardDetails[$thisValue]["ordersArtikelKategorieID"];
					if($selectCardDetails[$thisValue]["ordersArtikelBezeichnungZusatz"] != '') {
						$thisColorsText = '<br><b>Druckfarben:</b> ' . addslashes(htmlentities(preg_replace("/Druckfarben:/", "", $selectCardDetails[$thisValue]["ordersArtikelBezeichnungZusatz"])));
					}
					else { $thisColorsText = ''; }

					$thisColorsCount = $selectCardDetails[$thisValue]["ordersArtikelPrintColorsCount"];
					if($selectCardDetails[$thisValue]["ordersAufdruck"] != '') {
						$thisPrintText = '<br><b>Aufdruck:</b> ' . addslashes(htmlentities($selectCardDetails[$thisValue]["ordersAufdruck"]));
					}
					else { $thisPrintText = ''; }

					$thisPrintType = $selectCardDetails[$thisValue]["ordersAdditionalCostsID"];
					if($thisPrintType == ''){
						$thisPrintType = 0;
					}

					if($selectCardDetails[$thisValue]["ordersKommission"] != '') {
						$thisKommission = '<br><b>Kommission:</b> ' . addslashes(htmlentities($selectCardDetails[$thisValue]["ordersKommission"]));
					}
					else { $thisKommission = ''; }

					if($selectCardDetails[$thisValue]["ordersNotiz"] != '') {
						#$thisNotiz = '<br><span class=important><b>Notiz:</b> ' . addslashes($selectCardDetails[$thisValue]["ordersNotiz"]) . '</span>';
						$thisNotiz = '<br><b>Notiz:</b> ' . addslashes(htmlentities($selectCardDetails[$thisValue]["ordersNotiz"])) . '';
					}
					else { $thisNotiz = ''; }

					if($selectCardDetails[$thisValue]["orderPerDirectSale"] == '1') {
						$thisPerDirectSaleText = ('<br><span class=important>Direktverkauf durch Vertreter</span>');
						$thisPerDirectSaleValue = '1';
					}
					else {
						$thisPerDirectSaleText = '';
						$thisPerDirectSaleValue = '0';
					}

					if($selectCardDetails[$thisValue]["ordersID"] != ''){
						$thisCardItemID = $selectCardDetails[$thisValue]["ordersID"];
					}
					else {
						$thisCardItemID = $thisValue;
					}
					if($selectCardDetails[$thisValue]["ordersArtikelMitUmrandung"] == '1') {
						$thisWithBorder = "1";
						$thisPrintText .= ' - Mit Umrandung ';
					}
					else {
						$thisWithBorder = "0";
					}
					if($selectCardDetails[$thisValue]["ordersArtikelMitKlarlack"] == '1') {
						$thisWithClearPaint = "1";
						$thisPrintText .= ' - Mit Klarlack ';
					}
					else {
						$thisWithClearPaint = "0";
					}

					$thisOrderType = $arrOrderTypeDatas[$selectCardDetails[$thisValue]["ordersOrderType"]]["orderTypesName"];
					$thisOrderTypeValue = $selectCardDetails[$thisValue]["ordersOrderType"];



					$thisDescription .= ' [' . $thisOrderType . ']';
					// $thisDescription = htmlentities(urldecode($thisDescription));
					$thisDescription = ($thisDescription);
					// $thisDescription = htmlentities(($thisDescription));

					$wordwrapAfterChars = 70; // 70
					if($wordwrapAfterChars > 0){
						$thisDescription = wordwrap($thisDescription, $wordwrapAfterChars, ' <br>');
						$thisColorsText = wordwrap($thisColorsText, $wordwrapAfterChars, ' <br>');
						$thisPrintText = wordwrap($thisPrintText, $wordwrapAfterChars, ' <br>');
						$thisKommission = wordwrap($thisKommission, $wordwrapAfterChars, ' <br>');
						$thisNotiz = wordwrap($thisNotiz, $wordwrapAfterChars, ' <br>');
					}
					#$thisDescription = preg_replace("/,/", ",<br>", $thisDescription);

					if($thisNotiz != ''){
						$thisNotiz = '<span class=important>' . $thisNotiz . '</span>';
					}
					$thisPerDirectSaleText = ($thisPerDirectSaleText);

					$createBasketItemContent = preg_replace("/{###COUNT_ITEM###}/", $countItem, $createBasketItemContent);
					$createBasketItemContent = preg_replace("/{###CARD_ITEM_ID###}/", $thisCardItemID, $createBasketItemContent);
					$createBasketItemContent = preg_replace("/{###CARD_ITEM_PRODUCT_CATEGORIE_ID###}/", $thisProductCategorieID, $createBasketItemContent);
					#$createBasketItemContent = preg_replace("/{###CARD_ITEM_DESCRIPTION_VALUE###}/", ($thisDescription), $createBasketItemContent);
					$createBasketItemContent = preg_replace("/{###CARD_ITEM_DESCRIPTION_VALUE###}/", $thisDescription, $createBasketItemContent);
					$createBasketItemContent = preg_replace("/{###CARD_ITEM_NUMBER_VALUE###}/", $thisProductNumber, $createBasketItemContent);
					$createBasketItemContent = preg_replace("/{###CARD_ITEM_PRODUCT_ID_VALUE###}/", $thisProductID, $createBasketItemContent);
					$createBasketItemContent = preg_replace("/{###CARD_ITEM_QUANTITY_VALUE###}/", $thisQuantity, $createBasketItemContent);
					$createBasketItemContent = preg_replace("/{###CARD_ITEM_COLORS_TEXT###}/", $thisColorsText, $createBasketItemContent);
					$createBasketItemContent = preg_replace("/{###CARD_ITEM_COLORS_COUNT###}/", $thisColorsCount, $createBasketItemContent);
					$createBasketItemContent = preg_replace("/{###CARD_ITEM_PRINT_TYPE###}/", $thisPrintType, $createBasketItemContent);
					$createBasketItemContent = preg_replace("/{###CARD_ITEM_SINGLE_PRICE_VALUE###}/", number_format($thisSinglePrice, 2, ',', '.'), $createBasketItemContent);
					$createBasketItemContent = preg_replace("/{###CARD_ITEM_TOTAL_PRICE_VALUE###}/", number_format($thisTotalPrice, 2, ',', '.'), $createBasketItemContent);
					$createBasketItemContent = preg_replace("/{###CARD_POS_VALUE###}/", $thisPos, $createBasketItemContent);
					$createBasketItemContent = preg_replace("/{###CARD_ITEM_POS_TYPE###}/", $thisPosType, $createBasketItemContent);
					$createBasketItemContent = preg_replace("/{###ROW_COLOR###}/", $thisRowColor, $createBasketItemContent);
					$createBasketItemContent = preg_replace("/{###CARD_ITEM_PRINT_TEXT###}/", $thisPrintText, $createBasketItemContent);
					$createBasketItemContent = preg_replace("/{###CARD_ITEM_KOMMISSION###}/", $thisKommission, $createBasketItemContent);
					$createBasketItemContent = preg_replace("/{###CARD_ITEM_NOTIZ###}/", $thisNotiz, $createBasketItemContent);
					$createBasketItemContent = preg_replace("/{###CARD_ITEM_PER_DIRECT_SALE_TEXT###}/", $thisPerDirectSaleText, $createBasketItemContent);
					$createBasketItemContent = preg_replace("/{###CARD_ITEM_PER_DIRECT_SALE_VALUE###}/", $thisPerDirectSaleValue, $createBasketItemContent);
					$createBasketItemContent = preg_replace("/{###CARD_ITEM_WITH_BORDER###}/", $thisWithBorder, $createBasketItemContent);
					$createBasketItemContent = preg_replace("/{###CARD_ITEM_WITH_CLEARPAINT###}/", $thisWithClearPaint, $createBasketItemContent);
					$createBasketItemContent = preg_replace("/{###CARD_ITEM_ORDER_TYPE###}/", $thisOrderTypeValue, $createBasketItemContent);
					$getBasketItemContent[] = $createBasketItemContent;
					$totalPrice = $totalPrice + $thisTotalPrice;

					$countItem++;
					// EOF PRODUCTS PRICE

					// BOF EXPRESS COSTS FOR PRODUCT
					if($selectCardDetails[$thisValue]["orderPerShippingExpress"] == '1') {
						$thisPos = ($countItem + 1);

						$thisSinglePrice = $selectCardDetails[$thisValue]["orderShippingExpressValue"];
						$thisSinglePrice = convertDecimal($thisSinglePrice, 'store');
						$thisTotalPrice = $thisQuantity * $thisSinglePrice;
						$thisDescription = "Expressaufschlag";

						$thisProductNumber = "EXP";
						$thisProductID = "";
						$thisPosType = 'expressCosts';
						$thisProductCategorieID = '';
						$thisColorsText = '';
						$thisColorsCount = '';
						$thisPrintType = '';
						$thisPrintText = '';
						$thisKommission = '';
						$thisNotiz = '';
						$thisPerDirectSaleText = '';
						$thisPerDirectSaleValue = '0';
						if($selectCardDetails[$thisValue]["ordersID"] != ''){
							$thisCardItemID = $selectCardDetails[$thisValue]["ordersID"];
						}
						else {
							$thisCardItemID = $thisValue;
						}

						$thisWithBorder = "0";
						$thisWithClearPaint = "0";

						$thisOrderType = '';

						if($wordwrapAfterChars > 0){
							$thisDescription = wordwrap($thisDescription, $wordwrapAfterChars, ' <br>');
							$thisColorsText = wordwrap($thisColorsText, $wordwrapAfterChars, ' <br>');
							$thisPrintText = wordwrap($thisPrintText, $wordwrapAfterChars, ' <br>');
						}

						$createBasketItemContent = $getBasketItemsTemplate;
						$createBasketItemContent = preg_replace("/{###COUNT_ITEM###}/", $countItem, $createBasketItemContent);
						$createBasketItemContent = preg_replace("/{###CARD_ITEM_ID###}/", $thisCardItemID, $createBasketItemContent);
						$createBasketItemContent = preg_replace("/{###CARD_ITEM_PRODUCT_CATEGORIE_ID###}/", $thisProductCategorieID, $createBasketItemContent);
						$createBasketItemContent = preg_replace("/{###CARD_ITEM_DESCRIPTION_VALUE###}/", $thisDescription, $createBasketItemContent);
						$createBasketItemContent = preg_replace("/{###CARD_ITEM_NUMBER_VALUE###}/", $thisProductNumber, $createBasketItemContent);
						$createBasketItemContent = preg_replace("/{###CARD_ITEM_PRODUCT_ID_VALUE###}/", $thisProductID, $createBasketItemContent);
						$createBasketItemContent = preg_replace("/{###CARD_ITEM_QUANTITY_VALUE###}/", $thisQuantity, $createBasketItemContent);
						$createBasketItemContent = preg_replace("/{###CARD_ITEM_COLORS_TEXT###}/", $thisColorsText, $createBasketItemContent);
						$createBasketItemContent = preg_replace("/{###CARD_ITEM_COLORS_COUNT###}/", $thisColorsCount, $createBasketItemContent);
						$createBasketItemContent = preg_replace("/{###CARD_ITEM_PRINT_TYPE###}/", $thisPrintType, $createBasketItemContent);
						$createBasketItemContent = preg_replace("/{###CARD_ITEM_SINGLE_PRICE_VALUE###}/", number_format($thisSinglePrice, 2, ',', '.'), $createBasketItemContent);
						$createBasketItemContent = preg_replace("/{###CARD_ITEM_TOTAL_PRICE_VALUE###}/", number_format($thisTotalPrice, 2, ',', '.'), $createBasketItemContent);
						$createBasketItemContent = preg_replace("/{###CARD_POS_VALUE###}/", $thisPos, $createBasketItemContent);
						$createBasketItemContent = preg_replace("/{###CARD_ITEM_POS_TYPE###}/", $thisPosType, $createBasketItemContent);
						$createBasketItemContent = preg_replace("/{###ROW_COLOR###}/", $thisRowColor, $createBasketItemContent);
						$createBasketItemContent = preg_replace("/{###CARD_ITEM_PRINT_TEXT###}/", $thisPrintText, $createBasketItemContent);
						$createBasketItemContent = preg_replace("/{###CARD_ITEM_KOMMISSION###}/", $thisKommission, $createBasketItemContent);
						$createBasketItemContent = preg_replace("/{###CARD_ITEM_NOTIZ###}/", $thisNotiz, $createBasketItemContent);
						$createBasketItemContent = preg_replace("/{###CARD_ITEM_PER_DIRECT_SALE_TEXT###}/", $thisPerDirectSaleText, $createBasketItemContent);
						$createBasketItemContent = preg_replace("/{###CARD_ITEM_PER_DIRECT_SALE_VALUE###}/", $thisPerDirectSaleValue, $createBasketItemContent);
						$createBasketItemContent = preg_replace("/{###CARD_ITEM_WITH_BORDER###}/", $thisWithBorder, $createBasketItemContent);
						$createBasketItemContent = preg_replace("/{###CARD_ITEM_WITH_CLEARPAINT###}/", $thisWithClearPaint, $createBasketItemContent);
						$createBasketItemContent = preg_replace("/{###CARD_ITEM_ORDER_TYPE###}/", $thisOrderType, $createBasketItemContent);
						$getBasketItemContent[] = $createBasketItemContent;
						$totalPrice = $totalPrice + $thisTotalPrice;

						$countItem++;
					}
					// EOF EXPRESS COSTS FOR PRODUCT

					// BOF ADDITIONAL ITEMS
					if($selectCardDetails[$thisValue]["ordersAdditionalArtikelMenge"] > 0) {
						$thisDescription = 'plus ' . $arrOrderCategoriesTypeDatas[$selectCardDetails[$thisValue]["ordersAdditionalArtikelKategorieID"]]['orderCategoriesName_' . strtolower(MANDATOR)]. '<br>(Zusatzleisten werden bei Bestellung der Grundplatten berechnet)';

						$thisSinglePrice = convertDecimal(0, 'store');
						$thisTotalPrice = $selectCardDetails[$thisValue]["ordersAdditionalArtikelMenge"] * $thisSinglePrice;
						$thisPos = ($countItem + 1);
						$thisProductNumber = $selectCardDetails[$thisValue]["ordersArtikelNummer"];
						$thisProductID = $selectCardDetails[$thisValue]["ordersArtikelID"];
						

						if($countItem%2 == 0) { $thisRowColor = "#EEEEEE"; }
						else { $thisRowColor = "#FFFFFF"; }
						$thisRowColor = "#FFFFFF";
						$thisPosType = 'additionalProducts';
						$thisProductCategorieID = $selectCardDetails[$thisValue]["ordersAdditionalArtikelKategorieID"];
						$thisColorsText = '';
						$thisColorsCount = '';
						$thisPrintType = '';
						$thisPrintText = '';
						$thisKommission = '';
						$thisNotiz = '';
						$thisPerDirectSaleText = '';
						$thisPerDirectSaleValue = '0';
						if($selectCardDetails[$thisValue]["ordersID"] != ''){
							$thisCardItemID = $selectCardDetails[$thisValue]["ordersID"];
						}
						else {
							$thisCardItemID = $thisValue;
						}

						$thisWithBorder = "0";
						$thisWithClearPaint = "0";

						$thisOrderType = '';

						if($wordwrapAfterChars > 0){
							#$thisDescription = wordwrap($thisDescription, $wordwrapAfterChars, ' <br>');
							$thisColorsText = wordwrap($thisColorsText, $wordwrapAfterChars, ' <br>');
							$thisPrintText = wordwrap($thisPrintText, $wordwrapAfterChars, ' <br>');
						}

						$createBasketItemContent = $getBasketItemsTemplate;
						$createBasketItemContent = preg_replace("/{###COUNT_ITEM###}/", $countItem, $createBasketItemContent);
						$createBasketItemContent = preg_replace("/{###CARD_ITEM_ID###}/", $thisCardItemID, $createBasketItemContent);
						$createBasketItemContent = preg_replace("/{###CARD_ITEM_PRODUCT_CATEGORIE_ID###}/", $thisProductCategorieID, $createBasketItemContent);
						$createBasketItemContent = preg_replace("/{###CARD_ITEM_DESCRIPTION_VALUE###}/", $thisDescription, $createBasketItemContent);
						$createBasketItemContent = preg_replace("/{###CARD_ITEM_NUMBER_VALUE###}/", $thisProductNumber, $createBasketItemContent);
						$createBasketItemContent = preg_replace("/{###CARD_ITEM_PRODUCT_ID_VALUE###}/", $thisProductID, $createBasketItemContent);
						$createBasketItemContent = preg_replace("/{###CARD_ITEM_QUANTITY_VALUE###}/", $selectCardDetails[$thisValue]["ordersAdditionalArtikelMenge"], $createBasketItemContent);
						$createBasketItemContent = preg_replace("/{###CARD_ITEM_COLORS_TEXT###}/", $thisColorsText, $createBasketItemContent);
						$createBasketItemContent = preg_replace("/{###CARD_ITEM_COLORS_COUNT###}/", $thisColorsCount, $createBasketItemContent);
						$createBasketItemContent = preg_replace("/{###CARD_ITEM_PRINT_TYPE###}/", $thisPrintType, $createBasketItemContent);
						$createBasketItemContent = preg_replace("/{###CARD_ITEM_SINGLE_PRICE_VALUE###}/", number_format($thisSinglePrice, 2, ',', '.'), $createBasketItemContent);
						$createBasketItemContent = preg_replace("/{###CARD_ITEM_TOTAL_PRICE_VALUE###}/", number_format($thisTotalPrice, 2, ',', '.'), $createBasketItemContent);
						$createBasketItemContent = preg_replace("/{###CARD_POS_VALUE###}/", $thisPos, $createBasketItemContent);
						$createBasketItemContent = preg_replace("/{###CARD_ITEM_POS_TYPE###}/", $thisPosType, $createBasketItemContent);
						$createBasketItemContent = preg_replace("/{###ROW_COLOR###}/", $thisRowColor, $createBasketItemContent);
						$createBasketItemContent = preg_replace("/{###CARD_ITEM_PRINT_TEXT###}/", $thisPrintText, $createBasketItemContent);
						$createBasketItemContent = preg_replace("/{###CARD_ITEM_KOMMISSION###}/", $thisKommission, $createBasketItemContent);
						$createBasketItemContent = preg_replace("/{###CARD_ITEM_NOTIZ###}/", $thisNotiz, $createBasketItemContent);
						$createBasketItemContent = preg_replace("/{###CARD_ITEM_PER_DIRECT_SALE_TEXT###}/", $thisPerDirectSaleText, $createBasketItemContent);
						$createBasketItemContent = preg_replace("/{###CARD_ITEM_PER_DIRECT_SALE_VALUE###}/", $thisPerDirectSaleValue, $createBasketItemContent);
						$createBasketItemContent = preg_replace("/{###CARD_ITEM_WITH_BORDER###}/", $thisWithBorder, $createBasketItemContent);
						$createBasketItemContent = preg_replace("/{###CARD_ITEM_WITH_CLEARPAINT###}/", $thisWithClearPaint, $createBasketItemContent);
						$createBasketItemContent = preg_replace("/{###CARD_ITEM_ORDER_TYPE###}/", $thisOrderType, $createBasketItemContent);
						$getBasketItemContent[] = $createBasketItemContent;
						$totalPrice = $totalPrice + $thisTotalPrice;

						$countItem++;
					}
					// EOF ADDITIONAL ITEMS

					// BOF ADDITIONAL COSTS
					if($selectCardDetails[$thisValue]["ordersAdditionalCosts"] > 0) {

						$thisProductNumber = 'SATZK';
						$thisProductID = '';

						$thisDescription = 'einmalige Satzkosten pro Farbe ' . ' (entfallen bei Nachbestellung)';
						// . $arrAdditionalCostsDatas[$selectCardDetails[$thisValue]["ordersAdditionalCostsID"]]["additionalCostsName"] .

						if(!empty($arrProductsWerkzeugkosten)){
							if(in_array($selectCardDetails[$thisValue]["ordersArtikelNummer"], $arrProductsWerkzeugkosten)){
								$thisProductNumber = 'WERKZEUG';
								$thisDescription = 'einmalige Werkzeugkosten ' . ' (entfallen bei Nachbestellung)';
								$thisProductID = '';
							}
						}

						if($isOTLG > 0){
							#$thisDescription = $arrCustomerGroupProductNumbers[$thisProductNumber] . ' - ' . $thisDescription;
						}

						$thisSinglePrice = convertDecimal($selectCardDetails[$thisValue]["ordersAdditionalCosts"], 'store');
						$thisTotalPrice = $selectCardDetails[$thisValue]["ordersArtikelPrintColorsCount"] * $thisSinglePrice;
						$thisPos = ($countItem + 1);
						// $thisProductNumber = $arrAdditionalCostsDatas[$selectCardDetails[$thisValue]["ordersAdditionalCostsID"]]["additionalCostsShortName"];


						if($countItem%2 == 0) { $thisRowColor = "#EEEEEE"; }
						else { $thisRowColor = "#FFFFFF"; }
						$thisRowColor = "#FFFFFF";
						$thisPosType = 'printCosts';
						$thisProductCategorieID = '';
						$thisColorsText = '';
						$thisColorsCount = '';
						$thisPrintType = '';
						$thisPrintText = '';
						$thisKommission = '';
						$thisNotiz = '';
						$thisPerDirectSaleText = '';
						$thisPerDirectSaleValue = '0';
						if($selectCardDetails[$thisValue]["ordersID"] != ''){
							$thisCardItemID = $selectCardDetails[$thisValue]["ordersID"];
						}
						else {
							$thisCardItemID = $thisValue;
						}

						$thisWithBorder = "0";
						$thisWithClearPaint = "0";

						$thisOrderType = '';

						if($wordwrapAfterChars > 0){
							$thisDescription = wordwrap($thisDescription, $wordwrapAfterChars, ' <br>');
							$thisColorsText = wordwrap($thisColorsText, $wordwrapAfterChars, ' <br>');
							$thisPrintText = wordwrap($thisPrintText, $wordwrapAfterChars, ' <br>');
						}

						$createBasketItemContent = $getBasketItemsTemplate;
						$createBasketItemContent = preg_replace("/{###COUNT_ITEM###}/", $countItem, $createBasketItemContent);
						$createBasketItemContent = preg_replace("/{###CARD_ITEM_ID###}/", $thisCardItemID, $createBasketItemContent);
						$createBasketItemContent = preg_replace("/{###CARD_ITEM_PRODUCT_CATEGORIE_ID###}/", $thisProductCategorieID, $createBasketItemContent);
						$createBasketItemContent = preg_replace("/{###CARD_ITEM_DESCRIPTION_VALUE###}/", $thisDescription, $createBasketItemContent);
						$createBasketItemContent = preg_replace("/{###CARD_ITEM_NUMBER_VALUE###}/", $thisProductNumber, $createBasketItemContent);
						$createBasketItemContent = preg_replace("/{###CARD_ITEM_PRODUCT_ID_VALUE###}/", $thisProductID, $createBasketItemContent);
						$createBasketItemContent = preg_replace("/{###CARD_ITEM_QUANTITY_VALUE###}/", $selectCardDetails[$thisValue]["ordersArtikelPrintColorsCount"], $createBasketItemContent);
						$createBasketItemContent = preg_replace("/{###CARD_ITEM_COLORS_TEXT###}/", $thisColorsText, $createBasketItemContent);
						$createBasketItemContent = preg_replace("/{###CARD_ITEM_COLORS_COUNT###}/", $thisColorsCount, $createBasketItemContent);
						$createBasketItemContent = preg_replace("/{###CARD_ITEM_PRINT_TYPE###}/", $thisPrintType, $createBasketItemContent);
						$createBasketItemContent = preg_replace("/{###CARD_ITEM_SINGLE_PRICE_VALUE###}/", number_format($thisSinglePrice, 2, ',', '.'), $createBasketItemContent);
						$createBasketItemContent = preg_replace("/{###CARD_ITEM_TOTAL_PRICE_VALUE###}/", number_format($thisTotalPrice, 2, ',', '.'), $createBasketItemContent);
						$createBasketItemContent = preg_replace("/{###CARD_POS_VALUE###}/", $thisPos, $createBasketItemContent);
						$createBasketItemContent = preg_replace("/{###CARD_ITEM_POS_TYPE###}/", $thisPosType, $createBasketItemContent);
						$createBasketItemContent = preg_replace("/{###ROW_COLOR###}/", $thisRowColor, $createBasketItemContent);
						$createBasketItemContent = preg_replace("/{###CARD_ITEM_PRINT_TEXT###}/", $thisPrintText, $createBasketItemContent);
						$createBasketItemContent = preg_replace("/{###CARD_ITEM_KOMMISSION###}/", $thisKommission, $createBasketItemContent);
						$createBasketItemContent = preg_replace("/{###CARD_ITEM_NOTIZ###}/", $thisNotiz, $createBasketItemContent);
						$createBasketItemContent = preg_replace("/{###CARD_ITEM_PER_DIRECT_SALE_TEXT###}/", $thisPerDirectSaleText, $createBasketItemContent);
						$createBasketItemContent = preg_replace("/{###CARD_ITEM_PER_DIRECT_SALE_VALUE###}/", $thisPerDirectSaleValue, $createBasketItemContent);
						$createBasketItemContent = preg_replace("/{###CARD_ITEM_WITH_BORDER###}/", $thisWithBorder, $createBasketItemContent);
						$createBasketItemContent = preg_replace("/{###CARD_ITEM_WITH_CLEARPAINT###}/", $thisWithClearPaint, $createBasketItemContent);
						$createBasketItemContent = preg_replace("/{###CARD_ITEM_ORDER_TYPE###}/", $thisOrderType, $createBasketItemContent);
						$getBasketItemContent[] = $createBasketItemContent;
						$totalPrice = $totalPrice + $thisTotalPrice;

						$countItem++;
					}
					// EOF ADDITIONAL COSTS
				}
			}

			$loadCardContent = $loadCardTemplate;

			$loadCardItemContent = implode("", $getBasketItemContent);

			$loadCardContent = preg_replace("/<!-- ###CARD_ITEMS_START### -->(.*)<!-- ###CARD_ITEMS_END### -->/ismU", $loadCardItemContent, $loadCardContent);

			$loadCardContent = preg_replace("/{###CARD_TOTAL_POS_VALUE###}/", $countItem, $loadCardContent);

			$thisDiscount = convertDecimal($_POST["selectDiscount"], 'store');
			if($thisDiscount == 0) {
				$thisDiscountPercent = '';
				$thisDiscountValue = 0;
				$loadCardContent = preg_replace("/<!-- ###DISCOUNT_START### -->(.*)<!-- ###DISCOUNT_END### -->/", "", $loadCardContent);
			}
			else {
				if($_POST["selectDiscountType"] == 'fixed'){
					$thisDiscountPercent = '';
					#$thisDiscountPercent = 0;
					$thisDiscountValue = $thisDiscount;

				}
				else {
					#$thisDiscountPercent = number_format($_POST["selectDiscount"], 2, ',', '.') . '% ';
					$thisDiscountPercent = number_format($thisDiscount, 2, ',', '.') . '% ';
					$thisDiscountValue = $totalPrice * $thisDiscount / 100;
				}

				#$subtotalPrice = $totalPrice;
				#$loadCardContent = preg_replace("/{###CARD_DISCOUNT_VALUE###}/", number_format($thisDiscountValue, 2, ',', '.'), $loadCardContent);
				#$loadCardContent = preg_replace("/{###CARD_DISCOUNT_PERCENT###}/", $thisDiscountPercent, $loadCardContent);
				#$loadCardContent = preg_replace("/{###CARD_DISCOUNT_TYPE###}/", $_POST["selectDiscountType"], $loadCardContent);
				#$loadCardContent = preg_replace("/{###CARD_SUBTOTAL_PRICE_VALUE###}/", number_format($subtotalPrice, 2, ',', '.'), $loadCardContent);
			}

			$subtotalPrice = $totalPrice;
			$loadCardContent = preg_replace("/{###CARD_DISCOUNT_VALUE###}/", number_format($thisDiscountValue, 2, ',', '.'), $loadCardContent);
			$loadCardContent = preg_replace("/{###CARD_DISCOUNT_PERCENT###}/", $thisDiscountPercent, $loadCardContent);
			$loadCardContent = preg_replace("/{###CARD_DISCOUNT_TYPE###}/", $_POST["selectDiscountType"], $loadCardContent);
			$loadCardContent = preg_replace("/{###CARD_SUBTOTAL_PRICE_VALUE###}/", number_format($subtotalPrice, 2, ',', '.'), $loadCardContent);

			$totalPrice = $totalPrice - $thisDiscountValue;
			$loadCardContent = preg_replace("/{###CARD_TOTAL_PRICE_VALUE###}/", number_format($totalPrice, 2, ',', '.'), $loadCardContent);

			# $shippingCosts = $_POST["selectShippingCosts"];
			$shippingCosts = $_POST["selectShippingCosts"];
			if(trim($shippingCosts) == '') { $shippingCosts = 0;}
			$shippingCosts = convertDecimal($shippingCosts, 'store');
			$shippingCostsPerPackage = number_format(0, 2, ',', '.');
			if($_POST["selectPackagingCostsMultiplier"] > 0 && $shippingCosts > 0){
				$shippingCostsPerPackage = '' . $_POST["selectPackagingCostsMultiplier"] . ' x ' . number_format($shippingCosts, 2, ',', '.') . '' ;
			}

			$shippingCosts = $shippingCosts * $_POST["selectPackagingCostsMultiplier"];
			$totalPrice = $totalPrice + $shippingCosts;

			$packagingCosts = $_POST["selectPackagingCosts"];
			if(trim($packagingCosts) == '') { $packagingCosts = 0;}
			$packagingCosts = convertDecimal($packagingCosts, 'store');
			$totalPrice = $totalPrice + $packagingCosts;

			$loadCardContent = preg_replace("/{###CARD_PACKAGING_COSTS_VALUE###}/", number_format($packagingCosts, 2, ',', '.'), $loadCardContent);
			$loadCardContent = preg_replace("/{###CARD_SHIPPING_COSTS_VALUE###}/", number_format($shippingCosts, 2, ',', '.'), $loadCardContent);
			$loadCardContent = preg_replace("/{###CARD_SHIPPING_COSTS_PER_PACKAGE###}/", $shippingCostsPerPackage, $loadCardContent);

			$cashOnDelivery = convertDecimal($_POST["selectCashOnDelivery"], 'store');
			if($_POST["selectPaymentCondition"] == 3){
				// $_POST["selectPaymentType"] == 3 ???
				$totalPrice = $totalPrice + $cashOnDelivery;
				$loadCardContent = preg_replace("/{###CARD_CASH_ON_DELIVERY_VALUE###}/", number_format($cashOnDelivery, 2, ',', '.'), $loadCardContent);
			}
			else {
				$loadCardContent = preg_replace("/<!-- ###CASH_ON_DELIVERY_START### -->(.*)<!-- ###CASH_ON_DELIVERY_END### -->/", "", $loadCardContent);
			}

			$mwst = convertDecimal($_POST["selectMwSt"], 'store');

			if($_POST["selectMwStType"] == 'inclusive'){
				$thisMwstTypeValue = 'inclusive';
				$thisMwstTypeText = 'inkl.';
				$thisMwst = $mwst * ($totalPrice * 1) / (100 + $mwst);

				$thisMwst = round($thisMwst, 2);

				$totalPrice = $totalPrice;
			}
			else {
				$thisMwstTypeValue = 'exclusive';
				$thisMwstTypeText = 'zzgl.';
				$thisMwst = ($totalPrice * $mwst) / 100;

				$thisMwst = round($thisMwst, 2);

				$totalPrice = $totalPrice + $thisMwst;
			}

			$loadCardContent = preg_replace("/{###MWST_TYPE_VALUE###}/", $thisMwstTypeValue, $loadCardContent);
			$loadCardContent = preg_replace("/{###MWST_TYPE_TEXT###}/", $thisMwstTypeText, $loadCardContent);
			$loadCardContent = preg_replace("/{###MWST###}/", number_format($mwst, 2, ',', '.'), $loadCardContent);
			$loadCardContent = preg_replace("/{###CARD_MWST_PRICE_VALUE###}/", number_format($thisMwst, 2, ',', '.'), $loadCardContent);

			// $totalPrice = number_format($totalPrice + (($totalPrice * MWST) / 100), 2);
			$loadCardContent = preg_replace("/{###CARD_TOTAL_COMPLETE_PRICE_VALUE###}/", number_format($totalPrice, 2, ',', '.'), $loadCardContent);

			if(($_POST["editDocType"] == 'MA' || $_POST["editDocType"] == 'M1' || $_POST["editDocType"] == 'M2' || $_POST["editDocType"] == 'M3' || $_POST["editDocType"] == 'IK') || ($_POST["convertDocType"] == 'MA' || $_POST["convertDocType"] == 'M1' || $_POST["convertDocType"] == 'M2' || $_POST["convertDocType"] == 'M3' || $_POST["convertDocType"] == 'IK')){
				if($_POST["selectPaymentPartValue"] > 0){
					$loadCardContent = preg_replace("/{###REMINDER_PAID_PART_VALUE###}/", number_format($_POST["selectPaymentPartValue"], 2, ',', '.'), $loadCardContent);
					$loadCardContent = preg_replace("/{###REMINDER_REST_PART_VALUE###}/", number_format($_POST["selectPaymentRestValue"], 2, ',', '.'), $loadCardContent);
					$totalPrice = $totalPrice - $_POST["selectPaymentPartValue"];
				}
				else {
					$loadCardContent = preg_replace("/<!-- ###REMINDER_PARTLY_PAID_START### -->(.*)<!-- ###REMINDER_PARTLY_PAID_END### -->/", "", $loadCardContent);
				}

				$thisReminderInterestPercent = convertDecimal($_POST["selectReminderInterestPercent"], 'store');
				$intervalDays = floor(((strtotime($_POST["selectDocumentDate"]) - strtotime($_POST["selectInvoiceDate"])) / (60 * 60 * 24)) - 1);
				$yearDays = 365;

				$thisReminderInterestPrice = ($thisReminderInterestPercent * $totalPrice / 100) * $intervalDays / $yearDays;

				$thisReminderInterestPrice = round($thisReminderInterestPrice, 2);

				$thisReminderChargesPrice = convertDecimal($_POST["selectReminderChargesPrice"], 'store');
				$thisTotalReminderValue = $totalPrice + $thisReminderInterestPrice + $thisReminderChargesPrice;

				$loadCardContent = preg_replace("/{###REMINDER_INTEREST_PERCENT###}/", number_format($thisReminderInterestPercent, 2, ',', '.'), $loadCardContent);
				$loadCardContent = preg_replace("/{###REMINDER_INTEREST_VALUE###}/", number_format($thisReminderInterestPrice, 2, ',', '.'), $loadCardContent);
				$loadCardContent = preg_replace("/{###REMINDER_CHARGES_VALUE###}/", number_format($thisReminderChargesPrice, 2, ',', '.'), $loadCardContent);
				$loadCardContent = preg_replace("/{###CARD_TOTAL_REMINDER_VALUE###}/", number_format($thisTotalReminderValue, 2, ',', '.'), $loadCardContent);

			}
			else {
				$loadCardContent = preg_replace("/<!-- ###REMINDER_CHARGES_START### -->(.*)<!-- ###REMINDER_CHARGES_END### -->/", "", $loadCardContent);
				$loadCardContent = preg_replace("/<!-- ###REMINDER_PARTLY_PAID_START### -->(.*)<!-- ###REMINDER_PARTLY_PAID_END### -->/", "", $loadCardContent);
			}
		}
		else {
			$loadCardContent = "";
		}

		$loadCardContent = iconv( "ISO-8859-1", "UTF-8//TRANSLIT", $loadCardContent );
		$loadCardContent = ($loadCardContent	);
		$thisTotalSumPrice = $totalPrice;

		return $loadCardContent;
	}
?>