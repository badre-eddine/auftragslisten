
				SELECT
					CONCAT(
							'[span class={###QUOT###}customerNumber{###QUOT###}]', `common_customers`.`customersKundennummer`, '[/span] - ',
							'[span class={###QUOT###}customerName{###QUOT###}]', `common_customers`.`customersFirmenname`, '[/span] # ',
							'[span class={###QUOT###}customerID{###QUOT###}]', `common_customers`.`customersID`, '[/span]'
						) AS `searchResult`

					FROM `common_customers`

					FORCE INDEX (`customersKundennummer`)

					WHERE `customersKundennummer` LIKE '4%'
						AND `common_customers`.`customersActive` = '1'

					LIMIT 10

				SELECT
					CONCAT(
							'[span class={###QUOT###}customerNumber{###QUOT###}]', `common_customers`.`customersKundennummer`, '[/span] - ',
							'[span class={###QUOT###}customerName{###QUOT###}]', `common_customers`.`customersFirmenname`, '[/span] # ',
							'[span class={###QUOT###}customerID{###QUOT###}]', `common_customers`.`customersID`, '[/span]'
						) AS `searchResult`

					FROM `common_customers`

					FORCE INDEX (`customersKundennummer`)

					WHERE `customersKundennummer` LIKE '44%'
						AND `common_customers`.`customersActive` = '1'

					LIMIT 10

				SELECT
					CONCAT(
							'[span class={###QUOT###}customerNumber{###QUOT###}]', `common_customers`.`customersKundennummer`, '[/span] - ',
							'[span class={###QUOT###}customerName{###QUOT###}]', `common_customers`.`customersFirmenname`, '[/span] # ',
							'[span class={###QUOT###}customerID{###QUOT###}]', `common_customers`.`customersID`, '[/span]'
						) AS `searchResult`

					FROM `common_customers`

					FORCE INDEX (`customersKundennummer`)

					WHERE `customersKundennummer` LIKE '444%'
						AND `common_customers`.`customersActive` = '1'

					LIMIT 10

				SELECT
					CONCAT(
							'[span class={###QUOT###}customerNumber{###QUOT###}]', `common_customers`.`customersKundennummer`, '[/span] - ',
							'[span class={###QUOT###}customerName{###QUOT###}]', `common_customers`.`customersFirmenname`, '[/span] # ',
							'[span class={###QUOT###}customerID{###QUOT###}]', `common_customers`.`customersID`, '[/span]'
						) AS `searchResult`

					FROM `common_customers`

					FORCE INDEX (`customersKundennummer`)

					WHERE `customersKundennummer` LIKE '4446%'
						AND `common_customers`.`customersActive` = '1'

					LIMIT 10

				SELECT
					CONCAT(
							'[span class={###QUOT###}customerNumber{###QUOT###}]', `common_customers`.`customersKundennummer`, '[/span] - ',
							'[span class={###QUOT###}customerName{###QUOT###}]', `common_customers`.`customersFirmenname`, '[/span] # ',
							'[span class={###QUOT###}customerID{###QUOT###}]', `common_customers`.`customersID`, '[/span]'
						) AS `searchResult`

					FROM `common_customers`

					FORCE INDEX (`customersKundennummer`)

					WHERE `customersKundennummer` LIKE '44463%'
						AND `common_customers`.`customersActive` = '1'

					LIMIT 10

				SELECT
					CONCAT(
							'[span class={###QUOT###}customerNumber{###QUOT###}]', `common_customers`.`customersKundennummer`, '[/span] - ',
							'[span class={###QUOT###}customerName{###QUOT###}]', `common_customers`.`customersFirmenname`, '[/span] # ',
							'[span class={###QUOT###}customerID{###QUOT###}]', `common_customers`.`customersID`, '[/span]'
						) AS `searchResult`

					FROM `common_customers`

					FORCE INDEX (`customersKundennummer`)

					WHERE `customersKundennummer` LIKE '44463%'
						AND `common_customers`.`customersActive` = '1'

					LIMIT 10

				SELECT
					CONCAT(
							'[span class={###QUOT###}productNumber{###QUOT###}]', `bctr_products`.`products_model`, '[/span] - ',
							'[span class={###QUOT###}productName{###QUOT###}]', `bctr_products_description`.`products_name`, '[/span] # ',
							'[span class={###QUOT###}productID{###QUOT###}]', `bctr_products`.`products_id`, '[/span]'
						) AS `searchResult`

					FROM `bctr_products`
					LEFT JOIN `bctr_products_description`
					ON(`bctr_products`.`products_id` = `bctr_products_description`.`products_id`)

					WHERE `bctr_products`.`products_model` LIKE 'a%'
						/* AND `bctr_products`.`products_status` = 1 */

					LIMIT 10

				UNION

				SELECT
					CONCAT(
						'[span class={###QUOT###}productNumber{###QUOT###}]', `products_attributes`.`attributes_model`, '[/span] - ',
						'[span class={###QUOT###}productName{###QUOT###}]',
							CONCAT(
								`products_description`.`products_name`,
								': ',
								`products_options`.`products_options_name`,
								' ',
								if(`products_options_values`.`products_options_values_name` IS NULL, '', `products_options_values`.`products_options_values_name`)
							), '[/span] # ',
						'[span class={###QUOT###}productID{###QUOT###}]', `products_attributes`.`products_id`, '[/span]'
					) AS `searchResult`

					FROM `products_attributes`

					LEFT JOIN `products`
					ON(`products_attributes`.`products_id` = `products`.`products_id`)

					LEFT JOIN `products_description`
					ON(`products_attributes`.`products_id` = `products_description`.`products_id`)

					LEFT JOIN `products_options`
					ON(`products_attributes`.`options_id` = `products_options`.`products_options_id`)

					LEFT JOIN `products_options_values`
					ON(`products_attributes`.`options_values_id` = `products_options_values`.`products_options_values_id`)

					WHERE `products_attributes`.`attributes_model` LIKE 'a%'
						/* AND `products`.`products_status` = 1 */

					LIMIT 10


				SELECT
					CONCAT(
							'[span class={###QUOT###}productNumber{###QUOT###}]', `bctr_products`.`products_model`, '[/span] - ',
							'[span class={###QUOT###}productName{###QUOT###}]', `bctr_products_description`.`products_name`, '[/span] # ',
							'[span class={###QUOT###}productID{###QUOT###}]', `bctr_products`.`products_id`, '[/span]'
						) AS `searchResult`

					FROM `bctr_products`
					LEFT JOIN `bctr_products_description`
					ON(`bctr_products`.`products_id` = `bctr_products_description`.`products_id`)

					WHERE `bctr_products`.`products_model` LIKE '1%'
						/* AND `bctr_products`.`products_status` = 1 */

					LIMIT 10

				UNION

				SELECT
					CONCAT(
						'[span class={###QUOT###}productNumber{###QUOT###}]', `products_attributes`.`attributes_model`, '[/span] - ',
						'[span class={###QUOT###}productName{###QUOT###}]',
							CONCAT(
								`products_description`.`products_name`,
								': ',
								`products_options`.`products_options_name`,
								' ',
								if(`products_options_values`.`products_options_values_name` IS NULL, '', `products_options_values`.`products_options_values_name`)
							), '[/span] # ',
						'[span class={###QUOT###}productID{###QUOT###}]', `products_attributes`.`products_id`, '[/span]'
					) AS `searchResult`

					FROM `products_attributes`

					LEFT JOIN `products`
					ON(`products_attributes`.`products_id` = `products`.`products_id`)

					LEFT JOIN `products_description`
					ON(`products_attributes`.`products_id` = `products_description`.`products_id`)

					LEFT JOIN `products_options`
					ON(`products_attributes`.`options_id` = `products_options`.`products_options_id`)

					LEFT JOIN `products_options_values`
					ON(`products_attributes`.`options_values_id` = `products_options_values`.`products_options_values_id`)

					WHERE `products_attributes`.`attributes_model` LIKE '1%'
						/* AND `products`.`products_status` = 1 */

					LIMIT 10


				SELECT
					CONCAT(
							'[span class={###QUOT###}productNumber{###QUOT###}]', `bctr_products`.`products_model`, '[/span] - ',
							'[span class={###QUOT###}productName{###QUOT###}]', `bctr_products_description`.`products_name`, '[/span] # ',
							'[span class={###QUOT###}productID{###QUOT###}]', `bctr_products`.`products_id`, '[/span]'
						) AS `searchResult`

					FROM `bctr_products_description`
					LEFT JOIN `bctr_products`
					ON(`bctr_products_description`.`products_id` = `bctr_products`.`products_id`)

					WHERE `products_name` LIKE '%m%'
						/* AND `products_status` = 1 */

					LIMIT 10

				SELECT
					CONCAT(
							'[span class={###QUOT###}productNumber{###QUOT###}]', `bctr_products`.`products_model`, '[/span] - ',
							'[span class={###QUOT###}productName{###QUOT###}]', `bctr_products_description`.`products_name`, '[/span] # ',
							'[span class={###QUOT###}productID{###QUOT###}]', `bctr_products`.`products_id`, '[/span]'
						) AS `searchResult`

					FROM `bctr_products_description`
					LEFT JOIN `bctr_products`
					ON(`bctr_products_description`.`products_id` = `bctr_products`.`products_id`)

					WHERE `products_name` LIKE '%mi%'
						/* AND `products_status` = 1 */

					LIMIT 10

				SELECT
					CONCAT(
							'[span class={###QUOT###}productNumber{###QUOT###}]', `bctr_products`.`products_model`, '[/span] - ',
							'[span class={###QUOT###}productName{###QUOT###}]', `bctr_products_description`.`products_name`, '[/span] # ',
							'[span class={###QUOT###}productID{###QUOT###}]', `bctr_products`.`products_id`, '[/span]'
						) AS `searchResult`

					FROM `bctr_products_description`
					LEFT JOIN `bctr_products`
					ON(`bctr_products_description`.`products_id` = `bctr_products`.`products_id`)

					WHERE `products_name` LIKE '%minb%'
						/* AND `products_status` = 1 */

					LIMIT 10

				SELECT
					CONCAT(
							'[span class={###QUOT###}productNumber{###QUOT###}]', `bctr_products`.`products_model`, '[/span] - ',
							'[span class={###QUOT###}productName{###QUOT###}]', `bctr_products_description`.`products_name`, '[/span] # ',
							'[span class={###QUOT###}productID{###QUOT###}]', `bctr_products`.`products_id`, '[/span]'
						) AS `searchResult`

					FROM `bctr_products_description`
					LEFT JOIN `bctr_products`
					ON(`bctr_products_description`.`products_id` = `bctr_products`.`products_id`)

					WHERE `products_name` LIKE '%min%'
						/* AND `products_status` = 1 */

					LIMIT 10

				SELECT
					CONCAT(
							'[span class={###QUOT###}productNumber{###QUOT###}]', `bctr_products`.`products_model`, '[/span] - ',
							'[span class={###QUOT###}productName{###QUOT###}]', `bctr_products_description`.`products_name`, '[/span] # ',
							'[span class={###QUOT###}productID{###QUOT###}]', `bctr_products`.`products_id`, '[/span]'
						) AS `searchResult`

					FROM `bctr_products_description`
					LEFT JOIN `bctr_products`
					ON(`bctr_products_description`.`products_id` = `bctr_products`.`products_id`)

					WHERE `products_name` LIKE '%minb%'
						/* AND `products_status` = 1 */

					LIMIT 10

				SELECT
					CONCAT(
							'[span class={###QUOT###}productNumber{###QUOT###}]', `bctr_products`.`products_model`, '[/span] - ',
							'[span class={###QUOT###}productName{###QUOT###}]', `bctr_products_description`.`products_name`, '[/span] # ',
							'[span class={###QUOT###}productID{###QUOT###}]', `bctr_products`.`products_id`, '[/span]'
						) AS `searchResult`

					FROM `bctr_products_description`
					LEFT JOIN `bctr_products`
					ON(`bctr_products_description`.`products_id` = `bctr_products`.`products_id`)

					WHERE `products_name` LIKE '%mini%'
						/* AND `products_status` = 1 */

					LIMIT 10
			SELECT

					CONCAT(
							'[span class={###QUOT###}customerNumber{###QUOT###}]', `common_customers`.`customersKundennummer`, '[/span] - ',
							'[span class={###QUOT###}customerName{###QUOT###}]', `common_customers`.`customersFirmenname`, '[/span] # ',
							'[span class={###QUOT###}customerID{###QUOT###}]', `common_customers`.`customersID`, '[/span]'
						) AS `searchResult`

					FROM `common_customers`
					WHERE (
						`customersTyp` = 2
						OR `customersTyp` = 3
						OR `customersTyp` = 4
						OR `customersTyp` = 9
						OR `customersTyp` = 11
						) AND (
							`customersKundennummer` LIKE '%K%'
							OR `customersFirmenname` LIKE '%K%'
						)
						AND `common_customers`.`customersActive` = '1'
					ORDER BY `customersFirmenname`
			SELECT

					CONCAT(
							'[span class={###QUOT###}customerNumber{###QUOT###}]', `common_customers`.`customersKundennummer`, '[/span] - ',
							'[span class={###QUOT###}customerName{###QUOT###}]', `common_customers`.`customersFirmenname`, '[/span] # ',
							'[span class={###QUOT###}customerID{###QUOT###}]', `common_customers`.`customersID`, '[/span]'
						) AS `searchResult`

					FROM `common_customers`
					WHERE (
						`customersTyp` = 2
						OR `customersTyp` = 3
						OR `customersTyp` = 4
						OR `customersTyp` = 9
						OR `customersTyp` = 11
						) AND (
							`customersKundennummer` LIKE '%K%'
							OR `customersFirmenname` LIKE '%K%'
						)
						AND `common_customers`.`customersActive` = '1'
					ORDER BY `customersFirmenname`
			SELECT

					CONCAT(
							'[span class={###QUOT###}customerNumber{###QUOT###}]', `common_customers`.`customersKundennummer`, '[/span] - ',
							'[span class={###QUOT###}customerName{###QUOT###}]', `common_customers`.`customersFirmenname`, '[/span] # ',
							'[span class={###QUOT###}customerID{###QUOT###}]', `common_customers`.`customersID`, '[/span]'
						) AS `searchResult`

					FROM `common_customers`
					WHERE (
						`customersTyp` = 2
						OR `customersTyp` = 3
						OR `customersTyp` = 4
						OR `customersTyp` = 9
						OR `customersTyp` = 11
						) AND (
							`customersKundennummer` LIKE '%F%'
							OR `customersFirmenname` LIKE '%F%'
						)
						AND `common_customers`.`customersActive` = '1'
					ORDER BY `customersFirmenname`
			SELECT

					CONCAT(
							'[span class={###QUOT###}customerNumber{###QUOT###}]', `common_customers`.`customersKundennummer`, '[/span] - ',
							'[span class={###QUOT###}customerName{###QUOT###}]', `common_customers`.`customersFirmenname`, '[/span] # ',
							'[span class={###QUOT###}customerID{###QUOT###}]', `common_customers`.`customersID`, '[/span]'
						) AS `searchResult`

					FROM `common_customers`
					WHERE (
						`customersTyp` = 2
						OR `customersTyp` = 3
						OR `customersTyp` = 4
						OR `customersTyp` = 9
						OR `customersTyp` = 11
						) AND (
							`customersKundennummer` LIKE '%F%'
							OR `customersFirmenname` LIKE '%F%'
						)
						AND `common_customers`.`customersActive` = '1'
					ORDER BY `customersFirmenname`
			SELECT

					CONCAT(
							'[span class={###QUOT###}customerNumber{###QUOT###}]', `common_customers`.`customersKundennummer`, '[/span] - ',
							'[span class={###QUOT###}customerName{###QUOT###}]', `common_customers`.`customersFirmenname`, '[/span] # ',
							'[span class={###QUOT###}customerID{###QUOT###}]', `common_customers`.`customersID`, '[/span]'
						) AS `searchResult`

					FROM `common_customers`
					WHERE (
						`customersTyp` = 2
						OR `customersTyp` = 3
						OR `customersTyp` = 4
						OR `customersTyp` = 9
						OR `customersTyp` = 11
						) AND (
							`customersKundennummer` LIKE '%Fa%'
							OR `customersFirmenname` LIKE '%Fa%'
						)
						AND `common_customers`.`customersActive` = '1'
					ORDER BY `customersFirmenname`
			SELECT

					CONCAT(
							'[span class={###QUOT###}customerNumber{###QUOT###}]', `common_customers`.`customersKundennummer`, '[/span] - ',
							'[span class={###QUOT###}customerName{###QUOT###}]', `common_customers`.`customersFirmenname`, '[/span] # ',
							'[span class={###QUOT###}customerID{###QUOT###}]', `common_customers`.`customersID`, '[/span]'
						) AS `searchResult`

					FROM `common_customers`
					WHERE (
						`customersTyp` = 2
						OR `customersTyp` = 3
						OR `customersTyp` = 4
						OR `customersTyp` = 9
						OR `customersTyp` = 11
						) AND (
							`customersKundennummer` LIKE '%Fas%'
							OR `customersFirmenname` LIKE '%Fas%'
						)
						AND `common_customers`.`customersActive` = '1'
					ORDER BY `customersFirmenname`
			SELECT

					CONCAT(
							'[span class={###QUOT###}customerNumber{###QUOT###}]', `common_customers`.`customersKundennummer`, '[/span] - ',
							'[span class={###QUOT###}customerName{###QUOT###}]', `common_customers`.`customersFirmenname`, '[/span] # ',
							'[span class={###QUOT###}customerID{###QUOT###}]', `common_customers`.`customersID`, '[/span]'
						) AS `searchResult`

					FROM `common_customers`
					WHERE (
						`customersTyp` = 2
						OR `customersTyp` = 3
						OR `customersTyp` = 4
						OR `customersTyp` = 9
						OR `customersTyp` = 11
						) AND (
							`customersKundennummer` LIKE '%Fase%'
							OR `customersFirmenname` LIKE '%Fase%'
						)
						AND `common_customers`.`customersActive` = '1'
					ORDER BY `customersFirmenname`
			SELECT

					CONCAT(
							'[span class={###QUOT###}customerNumber{###QUOT###}]', `common_customers`.`customersKundennummer`, '[/span] - ',
							'[span class={###QUOT###}customerName{###QUOT###}]', `common_customers`.`customersFirmenname`, '[/span] # ',
							'[span class={###QUOT###}customerID{###QUOT###}]', `common_customers`.`customersID`, '[/span]'
						) AS `searchResult`

					FROM `common_customers`
					WHERE (
						`customersTyp` = 2
						OR `customersTyp` = 3
						OR `customersTyp` = 4
						OR `customersTyp` = 9
						OR `customersTyp` = 11
						) AND (
							`customersKundennummer` LIKE '%Fasel%'
							OR `customersFirmenname` LIKE '%Fasel%'
						)
						AND `common_customers`.`customersActive` = '1'
					ORDER BY `customersFirmenname`
			SELECT

					CONCAT(
							'[span class={###QUOT###}customerNumber{###QUOT###}]', `common_customers`.`customersKundennummer`, '[/span] - ',
							'[span class={###QUOT###}customerName{###QUOT###}]', `common_customers`.`customersFirmenname`, '[/span] # ',
							'[span class={###QUOT###}customerID{###QUOT###}]', `common_customers`.`customersID`, '[/span]'
						) AS `searchResult`

					FROM `common_customers`
					WHERE (
						`customersTyp` = 2
						OR `customersTyp` = 3
						OR `customersTyp` = 4
						OR `customersTyp` = 9
						OR `customersTyp` = 11
						) AND (
							`customersKundennummer` LIKE '%Faseler%'
							OR `customersFirmenname` LIKE '%Faseler%'
						)
						AND `common_customers`.`customersActive` = '1'
					ORDER BY `customersFirmenname`
			SELECT

					CONCAT(
							'[span class={###QUOT###}customerNumber{###QUOT###}]', `common_customers`.`customersKundennummer`, '[/span] - ',
							'[span class={###QUOT###}customerName{###QUOT###}]', `common_customers`.`customersFirmenname`, '[/span] # ',
							'[span class={###QUOT###}customerID{###QUOT###}]', `common_customers`.`customersID`, '[/span]'
						) AS `searchResult`

					FROM `common_customers`
					WHERE (
						`customersTyp` = 2
						OR `customersTyp` = 3
						OR `customersTyp` = 4
						OR `customersTyp` = 9
						OR `customersTyp` = 11
						) AND (
							`customersKundennummer` LIKE '%Faseler%'
							OR `customersFirmenname` LIKE '%Faseler%'
						)
						AND `common_customers`.`customersActive` = '1'
					ORDER BY `customersFirmenname`

				SELECT
					CONCAT(
							'[span class={###QUOT###}productNumber{###QUOT###}]', `products`.`products_model`, '[/span] - ',
							'[span class={###QUOT###}productName{###QUOT###}]', `products_description`.`products_name`, '[/span] # ',
							'[span class={###QUOT###}productID{###QUOT###}]', `products`.`products_id`, '[/span]'
						) AS `searchResult`

					FROM `products`
					LEFT JOIN `products_description`
					ON(`products`.`products_id` = `products_description`.`products_id`)

					WHERE `products`.`products_model` LIKE '1%'
						/* AND `products`.`products_status` = 1 */

					LIMIT 10

				UNION

				SELECT
					CONCAT(
						'[span class={###QUOT###}productNumber{###QUOT###}]', `products_attributes`.`attributes_model`, '[/span] - ',
						'[span class={###QUOT###}productName{###QUOT###}]',
							CONCAT(
								`products_description`.`products_name`,
								': ',
								`products_options`.`products_options_name`,
								' ',
								if(`products_options_values`.`products_options_values_name` IS NULL, '', `products_options_values`.`products_options_values_name`)
							), '[/span] # ',
						'[span class={###QUOT###}productID{###QUOT###}]', `products_attributes`.`products_id`, '[/span]'
					) AS `searchResult`

					FROM `products_attributes`

					LEFT JOIN `products`
					ON(`products_attributes`.`products_id` = `products`.`products_id`)

					LEFT JOIN `products_description`
					ON(`products_attributes`.`products_id` = `products_description`.`products_id`)

					LEFT JOIN `products_options`
					ON(`products_attributes`.`options_id` = `products_options`.`products_options_id`)

					LEFT JOIN `products_options_values`
					ON(`products_attributes`.`options_values_id` = `products_options_values`.`products_options_values_id`)

					WHERE `products_attributes`.`attributes_model` LIKE '1%'
						/* AND `products`.`products_status` = 1 */

					LIMIT 10


				SELECT
					CONCAT(
							'[span class={###QUOT###}productNumber{###QUOT###}]', `products`.`products_model`, '[/span] - ',
							'[span class={###QUOT###}productName{###QUOT###}]', `products_description`.`products_name`, '[/span] # ',
							'[span class={###QUOT###}productID{###QUOT###}]', `products`.`products_id`, '[/span]'
						) AS `searchResult`

					FROM `products`
					LEFT JOIN `products_description`
					ON(`products`.`products_id` = `products_description`.`products_id`)

					WHERE `products`.`products_model` LIKE '10%'
						/* AND `products`.`products_status` = 1 */

					LIMIT 10

				UNION

				SELECT
					CONCAT(
						'[span class={###QUOT###}productNumber{###QUOT###}]', `products_attributes`.`attributes_model`, '[/span] - ',
						'[span class={###QUOT###}productName{###QUOT###}]',
							CONCAT(
								`products_description`.`products_name`,
								': ',
								`products_options`.`products_options_name`,
								' ',
								if(`products_options_values`.`products_options_values_name` IS NULL, '', `products_options_values`.`products_options_values_name`)
							), '[/span] # ',
						'[span class={###QUOT###}productID{###QUOT###}]', `products_attributes`.`products_id`, '[/span]'
					) AS `searchResult`

					FROM `products_attributes`

					LEFT JOIN `products`
					ON(`products_attributes`.`products_id` = `products`.`products_id`)

					LEFT JOIN `products_description`
					ON(`products_attributes`.`products_id` = `products_description`.`products_id`)

					LEFT JOIN `products_options`
					ON(`products_attributes`.`options_id` = `products_options`.`products_options_id`)

					LEFT JOIN `products_options_values`
					ON(`products_attributes`.`options_values_id` = `products_options_values`.`products_options_values_id`)

					WHERE `products_attributes`.`attributes_model` LIKE '10%'
						/* AND `products`.`products_status` = 1 */

					LIMIT 10


				SELECT
					CONCAT(
							'[span class={###QUOT###}productNumber{###QUOT###}]', `products`.`products_model`, '[/span] - ',
							'[span class={###QUOT###}productName{###QUOT###}]', `products_description`.`products_name`, '[/span] # ',
							'[span class={###QUOT###}productID{###QUOT###}]', `products`.`products_id`, '[/span]'
						) AS `searchResult`

					FROM `products`
					LEFT JOIN `products_description`
					ON(`products`.`products_id` = `products_description`.`products_id`)

					WHERE `products`.`products_model` LIKE '101%'
						/* AND `products`.`products_status` = 1 */

					LIMIT 10

				UNION

				SELECT
					CONCAT(
						'[span class={###QUOT###}productNumber{###QUOT###}]', `products_attributes`.`attributes_model`, '[/span] - ',
						'[span class={###QUOT###}productName{###QUOT###}]',
							CONCAT(
								`products_description`.`products_name`,
								': ',
								`products_options`.`products_options_name`,
								' ',
								if(`products_options_values`.`products_options_values_name` IS NULL, '', `products_options_values`.`products_options_values_name`)
							), '[/span] # ',
						'[span class={###QUOT###}productID{###QUOT###}]', `products_attributes`.`products_id`, '[/span]'
					) AS `searchResult`

					FROM `products_attributes`

					LEFT JOIN `products`
					ON(`products_attributes`.`products_id` = `products`.`products_id`)

					LEFT JOIN `products_description`
					ON(`products_attributes`.`products_id` = `products_description`.`products_id`)

					LEFT JOIN `products_options`
					ON(`products_attributes`.`options_id` = `products_options`.`products_options_id`)

					LEFT JOIN `products_options_values`
					ON(`products_attributes`.`options_values_id` = `products_options_values`.`products_options_values_id`)

					WHERE `products_attributes`.`attributes_model` LIKE '101%'
						/* AND `products`.`products_status` = 1 */

					LIMIT 10


				SELECT
					CONCAT(
							'[span class={###QUOT###}productNumber{###QUOT###}]', `bctr_products`.`products_model`, '[/span] - ',
							'[span class={###QUOT###}productName{###QUOT###}]', `bctr_products_description`.`products_name`, '[/span] # ',
							'[span class={###QUOT###}productID{###QUOT###}]', `bctr_products`.`products_id`, '[/span]'
						) AS `searchResult`

					FROM `bctr_products`
					LEFT JOIN `bctr_products_description`
					ON(`bctr_products`.`products_id` = `bctr_products_description`.`products_id`)

					WHERE `bctr_products`.`products_model` LIKE 'q%'
						/* AND `bctr_products`.`products_status` = 1 */

					LIMIT 10

				UNION

				SELECT
					CONCAT(
						'[span class={###QUOT###}productNumber{###QUOT###}]', `products_attributes`.`attributes_model`, '[/span] - ',
						'[span class={###QUOT###}productName{###QUOT###}]',
							CONCAT(
								`products_description`.`products_name`,
								': ',
								`products_options`.`products_options_name`,
								' ',
								if(`products_options_values`.`products_options_values_name` IS NULL, '', `products_options_values`.`products_options_values_name`)
							), '[/span] # ',
						'[span class={###QUOT###}productID{###QUOT###}]', `products_attributes`.`products_id`, '[/span]'
					) AS `searchResult`

					FROM `products_attributes`

					LEFT JOIN `products`
					ON(`products_attributes`.`products_id` = `products`.`products_id`)

					LEFT JOIN `products_description`
					ON(`products_attributes`.`products_id` = `products_description`.`products_id`)

					LEFT JOIN `products_options`
					ON(`products_attributes`.`options_id` = `products_options`.`products_options_id`)

					LEFT JOIN `products_options_values`
					ON(`products_attributes`.`options_values_id` = `products_options_values`.`products_options_values_id`)

					WHERE `products_attributes`.`attributes_model` LIKE 'q%'
						/* AND `products`.`products_status` = 1 */

					LIMIT 10


				SELECT
					CONCAT(
							'[span class={###QUOT###}productNumber{###QUOT###}]', `bctr_products`.`products_model`, '[/span] - ',
							'[span class={###QUOT###}productName{###QUOT###}]', `bctr_products_description`.`products_name`, '[/span] # ',
							'[span class={###QUOT###}productID{###QUOT###}]', `bctr_products`.`products_id`, '[/span]'
						) AS `searchResult`

					FROM `bctr_products`
					LEFT JOIN `bctr_products_description`
					ON(`bctr_products`.`products_id` = `bctr_products_description`.`products_id`)

					WHERE `bctr_products`.`products_model` LIKE '1%'
						/* AND `bctr_products`.`products_status` = 1 */

					LIMIT 10

				UNION

				SELECT
					CONCAT(
						'[span class={###QUOT###}productNumber{###QUOT###}]', `products_attributes`.`attributes_model`, '[/span] - ',
						'[span class={###QUOT###}productName{###QUOT###}]',
							CONCAT(
								`products_description`.`products_name`,
								': ',
								`products_options`.`products_options_name`,
								' ',
								if(`products_options_values`.`products_options_values_name` IS NULL, '', `products_options_values`.`products_options_values_name`)
							), '[/span] # ',
						'[span class={###QUOT###}productID{###QUOT###}]', `products_attributes`.`products_id`, '[/span]'
					) AS `searchResult`

					FROM `products_attributes`

					LEFT JOIN `products`
					ON(`products_attributes`.`products_id` = `products`.`products_id`)

					LEFT JOIN `products_description`
					ON(`products_attributes`.`products_id` = `products_description`.`products_id`)

					LEFT JOIN `products_options`
					ON(`products_attributes`.`options_id` = `products_options`.`products_options_id`)

					LEFT JOIN `products_options_values`
					ON(`products_attributes`.`options_values_id` = `products_options_values`.`products_options_values_id`)

					WHERE `products_attributes`.`attributes_model` LIKE '1%'
						/* AND `products`.`products_status` = 1 */

					LIMIT 10


				SELECT
					CONCAT(
							'[span class={###QUOT###}productNumber{###QUOT###}]', `bctr_products`.`products_model`, '[/span] - ',
							'[span class={###QUOT###}productName{###QUOT###}]', `bctr_products_description`.`products_name`, '[/span] # ',
							'[span class={###QUOT###}productID{###QUOT###}]', `bctr_products`.`products_id`, '[/span]'
						) AS `searchResult`

					FROM `bctr_products`
					LEFT JOIN `bctr_products_description`
					ON(`bctr_products`.`products_id` = `bctr_products_description`.`products_id`)

					WHERE `bctr_products`.`products_model` LIKE '10%'
						/* AND `bctr_products`.`products_status` = 1 */

					LIMIT 10

				UNION

				SELECT
					CONCAT(
						'[span class={###QUOT###}productNumber{###QUOT###}]', `products_attributes`.`attributes_model`, '[/span] - ',
						'[span class={###QUOT###}productName{###QUOT###}]',
							CONCAT(
								`products_description`.`products_name`,
								': ',
								`products_options`.`products_options_name`,
								' ',
								if(`products_options_values`.`products_options_values_name` IS NULL, '', `products_options_values`.`products_options_values_name`)
							), '[/span] # ',
						'[span class={###QUOT###}productID{###QUOT###}]', `products_attributes`.`products_id`, '[/span]'
					) AS `searchResult`

					FROM `products_attributes`

					LEFT JOIN `products`
					ON(`products_attributes`.`products_id` = `products`.`products_id`)

					LEFT JOIN `products_description`
					ON(`products_attributes`.`products_id` = `products_description`.`products_id`)

					LEFT JOIN `products_options`
					ON(`products_attributes`.`options_id` = `products_options`.`products_options_id`)

					LEFT JOIN `products_options_values`
					ON(`products_attributes`.`options_values_id` = `products_options_values`.`products_options_values_id`)

					WHERE `products_attributes`.`attributes_model` LIKE '10%'
						/* AND `products`.`products_status` = 1 */

					LIMIT 10


				SELECT
					CONCAT(
							'[span class={###QUOT###}productNumber{###QUOT###}]', `bctr_products`.`products_model`, '[/span] - ',
							'[span class={###QUOT###}productName{###QUOT###}]', `bctr_products_description`.`products_name`, '[/span] # ',
							'[span class={###QUOT###}productID{###QUOT###}]', `bctr_products`.`products_id`, '[/span]'
						) AS `searchResult`

					FROM `bctr_products`
					LEFT JOIN `bctr_products_description`
					ON(`bctr_products`.`products_id` = `bctr_products_description`.`products_id`)

					WHERE `bctr_products`.`products_model` LIKE '108%'
						/* AND `bctr_products`.`products_status` = 1 */

					LIMIT 10

				UNION

				SELECT
					CONCAT(
						'[span class={###QUOT###}productNumber{###QUOT###}]', `products_attributes`.`attributes_model`, '[/span] - ',
						'[span class={###QUOT###}productName{###QUOT###}]',
							CONCAT(
								`products_description`.`products_name`,
								': ',
								`products_options`.`products_options_name`,
								' ',
								if(`products_options_values`.`products_options_values_name` IS NULL, '', `products_options_values`.`products_options_values_name`)
							), '[/span] # ',
						'[span class={###QUOT###}productID{###QUOT###}]', `products_attributes`.`products_id`, '[/span]'
					) AS `searchResult`

					FROM `products_attributes`

					LEFT JOIN `products`
					ON(`products_attributes`.`products_id` = `products`.`products_id`)

					LEFT JOIN `products_description`
					ON(`products_attributes`.`products_id` = `products_description`.`products_id`)

					LEFT JOIN `products_options`
					ON(`products_attributes`.`options_id` = `products_options`.`products_options_id`)

					LEFT JOIN `products_options_values`
					ON(`products_attributes`.`options_values_id` = `products_options_values`.`products_options_values_id`)

					WHERE `products_attributes`.`attributes_model` LIKE '108%'
						/* AND `products`.`products_status` = 1 */

					LIMIT 10

			SELECT

					CONCAT(
							'[span class={###QUOT###}customerNumber{###QUOT###}]', `common_customers`.`customersKundennummer`, '[/span] - ',
							'[span class={###QUOT###}customerName{###QUOT###}]', `common_customers`.`customersFirmenname`, '[/span] # ',
							'[span class={###QUOT###}customerID{###QUOT###}]', `common_customers`.`customersID`, '[/span]'
						) AS `searchResult`

					FROM `common_customers`
					WHERE (
						`customersTyp` = 2
						OR `customersTyp` = 3
						OR `customersTyp` = 4
						OR `customersTyp` = 9
						OR `customersTyp` = 11
						) AND (
							`customersKundennummer` LIKE '%B%'
							OR `customersFirmenname` LIKE '%B%'
						)
						AND `common_customers`.`customersActive` = '1'
					ORDER BY `customersFirmenname`
			SELECT

					CONCAT(
							'[span class={###QUOT###}customerNumber{###QUOT###}]', `common_customers`.`customersKundennummer`, '[/span] - ',
							'[span class={###QUOT###}customerName{###QUOT###}]', `common_customers`.`customersFirmenname`, '[/span] # ',
							'[span class={###QUOT###}customerID{###QUOT###}]', `common_customers`.`customersID`, '[/span]'
						) AS `searchResult`

					FROM `common_customers`
					WHERE (
						`customersTyp` = 2
						OR `customersTyp` = 3
						OR `customersTyp` = 4
						OR `customersTyp` = 9
						OR `customersTyp` = 11
						) AND (
							`customersKundennummer` LIKE '%B%'
							OR `customersFirmenname` LIKE '%B%'
						)
						AND `common_customers`.`customersActive` = '1'
					ORDER BY `customersFirmenname`
			SELECT

					CONCAT(
							'[span class={###QUOT###}customerNumber{###QUOT###}]', `common_customers`.`customersKundennummer`, '[/span] - ',
							'[span class={###QUOT###}customerName{###QUOT###}]', `common_customers`.`customersFirmenname`, '[/span] # ',
							'[span class={###QUOT###}customerID{###QUOT###}]', `common_customers`.`customersID`, '[/span]'
						) AS `searchResult`

					FROM `common_customers`
					WHERE (
						`customersTyp` = 2
						OR `customersTyp` = 3
						OR `customersTyp` = 4
						OR `customersTyp` = 9
						OR `customersTyp` = 11
						) AND (
							`customersKundennummer` LIKE '%Bur%'
							OR `customersFirmenname` LIKE '%Bur%'
						)
						AND `common_customers`.`customersActive` = '1'
					ORDER BY `customersFirmenname`
			SELECT

					CONCAT(
							'[span class={###QUOT###}customerNumber{###QUOT###}]', `common_customers`.`customersKundennummer`, '[/span] - ',
							'[span class={###QUOT###}customerName{###QUOT###}]', `common_customers`.`customersFirmenname`, '[/span] # ',
							'[span class={###QUOT###}customerID{###QUOT###}]', `common_customers`.`customersID`, '[/span]'
						) AS `searchResult`

					FROM `common_customers`
					WHERE (
						`customersTyp` = 2
						OR `customersTyp` = 3
						OR `customersTyp` = 4
						OR `customersTyp` = 9
						OR `customersTyp` = 11
						) AND (
							`customersKundennummer` LIKE '%Bur%'
							OR `customersFirmenname` LIKE '%Bur%'
						)
						AND `common_customers`.`customersActive` = '1'
					ORDER BY `customersFirmenname`
			SELECT

					CONCAT(
							'[span class={###QUOT###}customerNumber{###QUOT###}]', `common_customers`.`customersKundennummer`, '[/span] - ',
							'[span class={###QUOT###}customerName{###QUOT###}]', `common_customers`.`customersFirmenname`, '[/span] # ',
							'[span class={###QUOT###}customerID{###QUOT###}]', `common_customers`.`customersID`, '[/span]'
						) AS `searchResult`

					FROM `common_customers`
					WHERE (
						`customersTyp` = 2
						OR `customersTyp` = 3
						OR `customersTyp` = 4
						OR `customersTyp` = 9
						OR `customersTyp` = 11
						) AND (
							`customersKundennummer` LIKE '%Burha%'
							OR `customersFirmenname` LIKE '%Burha%'
						)
						AND `common_customers`.`customersActive` = '1'
					ORDER BY `customersFirmenname`
			SELECT

					CONCAT(
							'[span class={###QUOT###}customerNumber{###QUOT###}]', `common_customers`.`customersKundennummer`, '[/span] - ',
							'[span class={###QUOT###}customerName{###QUOT###}]', `common_customers`.`customersFirmenname`, '[/span] # ',
							'[span class={###QUOT###}customerID{###QUOT###}]', `common_customers`.`customersID`, '[/span]'
						) AS `searchResult`

					FROM `common_customers`
					WHERE (
						`customersTyp` = 2
						OR `customersTyp` = 3
						OR `customersTyp` = 4
						OR `customersTyp` = 9
						OR `customersTyp` = 11
						) AND (
							`customersKundennummer` LIKE '%Burhan%'
							OR `customersFirmenname` LIKE '%Burhan%'
						)
						AND `common_customers`.`customersActive` = '1'
					ORDER BY `customersFirmenname`
			SELECT

					CONCAT(
							'[span class={###QUOT###}customerNumber{###QUOT###}]', `common_customers`.`customersKundennummer`, '[/span] - ',
							'[span class={###QUOT###}customerName{###QUOT###}]', `common_customers`.`customersFirmenname`, '[/span] # ',
							'[span class={###QUOT###}customerID{###QUOT###}]', `common_customers`.`customersID`, '[/span]'
						) AS `searchResult`

					FROM `common_customers`
					WHERE (
						`customersTyp` = 2
						OR `customersTyp` = 3
						OR `customersTyp` = 4
						OR `customersTyp` = 9
						OR `customersTyp` = 11
						) AND (
							`customersKundennummer` LIKE '%Burha%'
							OR `customersFirmenname` LIKE '%Burha%'
						)
						AND `common_customers`.`customersActive` = '1'
					ORDER BY `customersFirmenname`

				SELECT
					CONCAT(
							'[span class={###QUOT###}productNumber{###QUOT###}]', `products`.`products_model`, '[/span] - ',
							'[span class={###QUOT###}productName{###QUOT###}]', `products_description`.`products_name`, '[/span] # ',
							'[span class={###QUOT###}productID{###QUOT###}]', `products`.`products_id`, '[/span]'
						) AS `searchResult`

					FROM `products`
					LEFT JOIN `products_description`
					ON(`products`.`products_id` = `products_description`.`products_id`)

					WHERE `products`.`products_model` LIKE '1%'
						/* AND `products`.`products_status` = 1 */

					LIMIT 10

				UNION

				SELECT
					CONCAT(
						'[span class={###QUOT###}productNumber{###QUOT###}]', `products_attributes`.`attributes_model`, '[/span] - ',
						'[span class={###QUOT###}productName{###QUOT###}]',
							CONCAT(
								`products_description`.`products_name`,
								': ',
								`products_options`.`products_options_name`,
								' ',
								if(`products_options_values`.`products_options_values_name` IS NULL, '', `products_options_values`.`products_options_values_name`)
							), '[/span] # ',
						'[span class={###QUOT###}productID{###QUOT###}]', `products_attributes`.`products_id`, '[/span]'
					) AS `searchResult`

					FROM `products_attributes`

					LEFT JOIN `products`
					ON(`products_attributes`.`products_id` = `products`.`products_id`)

					LEFT JOIN `products_description`
					ON(`products_attributes`.`products_id` = `products_description`.`products_id`)

					LEFT JOIN `products_options`
					ON(`products_attributes`.`options_id` = `products_options`.`products_options_id`)

					LEFT JOIN `products_options_values`
					ON(`products_attributes`.`options_values_id` = `products_options_values`.`products_options_values_id`)

					WHERE `products_attributes`.`attributes_model` LIKE '1%'
						/* AND `products`.`products_status` = 1 */

					LIMIT 10


				SELECT
					CONCAT(
							'[span class={###QUOT###}productNumber{###QUOT###}]', `bctr_products`.`products_model`, '[/span] - ',
							'[span class={###QUOT###}productName{###QUOT###}]', `bctr_products_description`.`products_name`, '[/span] # ',
							'[span class={###QUOT###}productID{###QUOT###}]', `bctr_products`.`products_id`, '[/span]'
						) AS `searchResult`

					FROM `bctr_products_description`
					LEFT JOIN `bctr_products`
					ON(`bctr_products_description`.`products_id` = `bctr_products`.`products_id`)

					WHERE `products_name` LIKE '%1%'
						/* AND `products_status` = 1 */

					LIMIT 10

				SELECT
					CONCAT(
							'[span class={###QUOT###}productNumber{###QUOT###}]', `bctr_products`.`products_model`, '[/span] - ',
							'[span class={###QUOT###}productName{###QUOT###}]', `bctr_products_description`.`products_name`, '[/span] # ',
							'[span class={###QUOT###}productID{###QUOT###}]', `bctr_products`.`products_id`, '[/span]'
						) AS `searchResult`

					FROM `bctr_products`
					LEFT JOIN `bctr_products_description`
					ON(`bctr_products`.`products_id` = `bctr_products_description`.`products_id`)

					WHERE `bctr_products`.`products_model` LIKE '3%'
						/* AND `bctr_products`.`products_status` = 1 */

					LIMIT 10

				UNION

				SELECT
					CONCAT(
						'[span class={###QUOT###}productNumber{###QUOT###}]', `bctr_products_attributes`.`attributes_model`, '[/span] - ',
						'[span class={###QUOT###}productName{###QUOT###}]',
							CONCAT(
								`bctr_products_description`.`products_name`,
								': ',
								`bctr_products_options`.`products_options_name`,
								' ',
								if(`bctr_products_options_values`.`products_options_values_name` IS NULL, '', `bctr_products_options_values`.`products_options_values_name`)
							), '[/span] # ',
						'[span class={###QUOT###}productID{###QUOT###}]', `bctr_products_attributes`.`products_id`, '[/span]'
					) AS `searchResult`

					FROM `bctr_products_attributes`

					LEFT JOIN `bctr_products`
					ON(`bctr_products_attributes`.`products_id` = `bctr_products`.`products_id`)

					LEFT JOIN `bctr_products_description`
					ON(`bctr_products_attributes`.`products_id` = `bctr_products_description`.`products_id`)

					LEFT JOIN `bctr_products_options`
					ON(`bctr_products_attributes`.`options_id` = `bctr_products_options`.`products_options_id`)

					LEFT JOIN `bctr_products_options_values`
					ON(`bctr_products_attributes`.`options_values_id` = `bctr_products_options_values`.`products_options_values_id`)

					WHERE `bctr_products_attributes`.`attributes_model` LIKE '3%'
						/* AND `bctr_products`.`products_status` = 1 */

					LIMIT 10


				SELECT
					CONCAT(
							'[span class={###QUOT###}productNumber{###QUOT###}]', `bctr_products`.`products_model`, '[/span] - ',
							'[span class={###QUOT###}productName{###QUOT###}]', `bctr_products_description`.`products_name`, '[/span] # ',
							'[span class={###QUOT###}productID{###QUOT###}]', `bctr_products`.`products_id`, '[/span]'
						) AS `searchResult`

					FROM `bctr_products`
					LEFT JOIN `bctr_products_description`
					ON(`bctr_products`.`products_id` = `bctr_products_description`.`products_id`)

					WHERE `bctr_products`.`products_model` LIKE '1%'
						/* AND `bctr_products`.`products_status` = 1 */

					LIMIT 10

				UNION

				SELECT
					CONCAT(
						'[span class={###QUOT###}productNumber{###QUOT###}]', `bctr_products_attributes`.`attributes_model`, '[/span] - ',
						'[span class={###QUOT###}productName{###QUOT###}]',
							CONCAT(
								`bctr_products_description`.`products_name`,
								': ',
								`bctr_products_options`.`products_options_name`,
								' ',
								if(`bctr_products_options_values`.`products_options_values_name` IS NULL, '', `bctr_products_options_values`.`products_options_values_name`)
							), '[/span] # ',
						'[span class={###QUOT###}productID{###QUOT###}]', `bctr_products_attributes`.`products_id`, '[/span]'
					) AS `searchResult`

					FROM `bctr_products_attributes`

					LEFT JOIN `bctr_products`
					ON(`bctr_products_attributes`.`products_id` = `bctr_products`.`products_id`)

					LEFT JOIN `bctr_products_description`
					ON(`bctr_products_attributes`.`products_id` = `bctr_products_description`.`products_id`)

					LEFT JOIN `bctr_products_options`
					ON(`bctr_products_attributes`.`options_id` = `bctr_products_options`.`products_options_id`)

					LEFT JOIN `bctr_products_options_values`
					ON(`bctr_products_attributes`.`options_values_id` = `bctr_products_options_values`.`products_options_values_id`)

					WHERE `bctr_products_attributes`.`attributes_model` LIKE '1%'
						/* AND `bctr_products`.`products_status` = 1 */

					LIMIT 10


				SELECT
					CONCAT(
							'[span class={###QUOT###}customerNumber{###QUOT###}]', `common_customers`.`customersKundennummer`, '[/span] - ',
							'[span class={###QUOT###}customerName{###QUOT###}]', `common_customers`.`customersFirmenname`, '[/span] # ',
							'[span class={###QUOT###}customerID{###QUOT###}]', `common_customers`.`customersID`, '[/span]'
						) AS `searchResult`

					FROM `common_customers`

					FORCE INDEX (`customersKundennummer`)

					WHERE `customersKundennummer` LIKE '7%'
						AND `common_customers`.`customersActive` = '1'

					LIMIT 10

				SELECT
					CONCAT(
							'[span class={###QUOT###}customerNumber{###QUOT###}]', `common_customers`.`customersKundennummer`, '[/span] - ',
							'[span class={###QUOT###}customerName{###QUOT###}]', `common_customers`.`customersFirmenname`, '[/span] # ',
							'[span class={###QUOT###}customerID{###QUOT###}]', `common_customers`.`customersID`, '[/span]'
						) AS `searchResult`

					FROM `common_customers`

					FORCE INDEX (`customersKundennummer`)

					WHERE `customersKundennummer` LIKE '73%'
						AND `common_customers`.`customersActive` = '1'

					LIMIT 10

				SELECT
					CONCAT(
							'[span class={###QUOT###}customerNumber{###QUOT###}]', `common_customers`.`customersKundennummer`, '[/span] - ',
							'[span class={###QUOT###}customerName{###QUOT###}]', `common_customers`.`customersFirmenname`, '[/span] # ',
							'[span class={###QUOT###}customerID{###QUOT###}]', `common_customers`.`customersID`, '[/span]'
						) AS `searchResult`

					FROM `common_customers`

					FORCE INDEX (`customersKundennummer`)

					WHERE `customersKundennummer` LIKE '731%'
						AND `common_customers`.`customersActive` = '1'

					LIMIT 10

				SELECT
					CONCAT(
							'[span class={###QUOT###}customerNumber{###QUOT###}]', `common_customers`.`customersKundennummer`, '[/span] - ',
							'[span class={###QUOT###}customerName{###QUOT###}]', `common_customers`.`customersFirmenname`, '[/span] # ',
							'[span class={###QUOT###}customerID{###QUOT###}]', `common_customers`.`customersID`, '[/span]'
						) AS `searchResult`

					FROM `common_customers`

					FORCE INDEX (`customersKundennummer`)

					WHERE `customersKundennummer` LIKE '7312%'
						AND `common_customers`.`customersActive` = '1'

					LIMIT 10

				SELECT
					CONCAT(
							'[span class={###QUOT###}customerNumber{###QUOT###}]', `common_customers`.`customersKundennummer`, '[/span] - ',
							'[span class={###QUOT###}customerName{###QUOT###}]', `common_customers`.`customersFirmenname`, '[/span] # ',
							'[span class={###QUOT###}customerID{###QUOT###}]', `common_customers`.`customersID`, '[/span]'
						) AS `searchResult`

					FROM `common_customers`

					FORCE INDEX (`customersKundennummer`)

					WHERE `customersKundennummer` LIKE '73127%'
						AND `common_customers`.`customersActive` = '1'

					LIMIT 10

				SELECT
					CONCAT(
							'[span class={###QUOT###}customerNumber{###QUOT###}]', `common_customers`.`customersKundennummer`, '[/span] - ',
							'[span class={###QUOT###}customerName{###QUOT###}]', `common_customers`.`customersFirmenname`, '[/span] # ',
							'[span class={###QUOT###}customerID{###QUOT###}]', `common_customers`.`customersID`, '[/span]'
						) AS `searchResult`

					FROM `common_customers`

					FORCE INDEX (`customersKundennummer`)

					WHERE `customersKundennummer` LIKE '73127%'
						AND `common_customers`.`customersActive` = '1'

					LIMIT 10

				SELECT
					CONCAT(
							'[span class={###QUOT###}productNumber{###QUOT###}]', `bctr_products`.`products_model`, '[/span] - ',
							'[span class={###QUOT###}productName{###QUOT###}]', `bctr_products_description`.`products_name`, '[/span] # ',
							'[span class={###QUOT###}productID{###QUOT###}]', `bctr_products`.`products_id`, '[/span]'
						) AS `searchResult`

					FROM `bctr_products`
					LEFT JOIN `bctr_products_description`
					ON(`bctr_products`.`products_id` = `bctr_products_description`.`products_id`)

					WHERE `bctr_products`.`products_model` LIKE '1%'
						/* AND `bctr_products`.`products_status` = 1 */

					LIMIT 10

				UNION

				SELECT
					CONCAT(
						'[span class={###QUOT###}productNumber{###QUOT###}]', `bctr_products_attributes`.`attributes_model`, '[/span] - ',
						'[span class={###QUOT###}productName{###QUOT###}]',
							CONCAT(
								`bctr_products_description`.`products_name`,
								': ',
								`bctr_products_options`.`products_options_name`,
								' ',
								if(`bctr_products_options_values`.`products_options_values_name` IS NULL, '', `bctr_products_options_values`.`products_options_values_name`)
							), '[/span] # ',
						'[span class={###QUOT###}productID{###QUOT###}]', `bctr_products_attributes`.`products_id`, '[/span]'
					) AS `searchResult`

					FROM `bctr_products_attributes`

					LEFT JOIN `bctr_products`
					ON(`bctr_products_attributes`.`products_id` = `bctr_products`.`products_id`)

					LEFT JOIN `bctr_products_description`
					ON(`bctr_products_attributes`.`products_id` = `bctr_products_description`.`products_id`)

					LEFT JOIN `bctr_products_options`
					ON(`bctr_products_attributes`.`options_id` = `bctr_products_options`.`products_options_id`)

					LEFT JOIN `bctr_products_options_values`
					ON(`bctr_products_attributes`.`options_values_id` = `bctr_products_options_values`.`products_options_values_id`)

					WHERE `bctr_products_attributes`.`attributes_model` LIKE '1%'
						/* AND `bctr_products`.`products_status` = 1 */

					LIMIT 10


				SELECT
					CONCAT(
							'[span class={###QUOT###}productNumber{###QUOT###}]', `bctr_products`.`products_model`, '[/span] - ',
							'[span class={###QUOT###}productName{###QUOT###}]', `bctr_products_description`.`products_name`, '[/span] # ',
							'[span class={###QUOT###}productID{###QUOT###}]', `bctr_products`.`products_id`, '[/span]'
						) AS `searchResult`

					FROM `bctr_products_description`
					LEFT JOIN `bctr_products`
					ON(`bctr_products_description`.`products_id` = `bctr_products`.`products_id`)

					WHERE `products_name` LIKE '%m%'
						/* AND `products_status` = 1 */

					LIMIT 10

				SELECT
					CONCAT(
							'[span class={###QUOT###}productNumber{###QUOT###}]', `bctr_products`.`products_model`, '[/span] - ',
							'[span class={###QUOT###}productName{###QUOT###}]', `bctr_products_description`.`products_name`, '[/span] # ',
							'[span class={###QUOT###}productID{###QUOT###}]', `bctr_products`.`products_id`, '[/span]'
						) AS `searchResult`

					FROM `bctr_products_description`
					LEFT JOIN `bctr_products`
					ON(`bctr_products_description`.`products_id` = `bctr_products`.`products_id`)

					WHERE `products_name` LIKE '%mi%'
						/* AND `products_status` = 1 */

					LIMIT 10

				SELECT
					CONCAT(
							'[span class={###QUOT###}productNumber{###QUOT###}]', `bctr_products`.`products_model`, '[/span] - ',
							'[span class={###QUOT###}productName{###QUOT###}]', `bctr_products_description`.`products_name`, '[/span] # ',
							'[span class={###QUOT###}productID{###QUOT###}]', `bctr_products`.`products_id`, '[/span]'
						) AS `searchResult`

					FROM `bctr_products_description`
					LEFT JOIN `bctr_products`
					ON(`bctr_products_description`.`products_id` = `bctr_products`.`products_id`)

					WHERE `products_name` LIKE '%min%'
						/* AND `products_status` = 1 */

					LIMIT 10

				SELECT
					CONCAT(
							'[span class={###QUOT###}productNumber{###QUOT###}]', `bctr_products`.`products_model`, '[/span] - ',
							'[span class={###QUOT###}productName{###QUOT###}]', `bctr_products_description`.`products_name`, '[/span] # ',
							'[span class={###QUOT###}productID{###QUOT###}]', `bctr_products`.`products_id`, '[/span]'
						) AS `searchResult`

					FROM `bctr_products_description`
					LEFT JOIN `bctr_products`
					ON(`bctr_products_description`.`products_id` = `bctr_products`.`products_id`)

					WHERE `products_name` LIKE '%mini%'
						/* AND `products_status` = 1 */

					LIMIT 10

				SELECT
					CONCAT(
							'[span class={###QUOT###}productNumber{###QUOT###}]', `bctr_products`.`products_model`, '[/span] - ',
							'[span class={###QUOT###}productName{###QUOT###}]', `bctr_products_description`.`products_name`, '[/span] # ',
							'[span class={###QUOT###}productID{###QUOT###}]', `bctr_products`.`products_id`, '[/span]'
						) AS `searchResult`

					FROM `bctr_products_description`
					LEFT JOIN `bctr_products`
					ON(`bctr_products_description`.`products_id` = `bctr_products`.`products_id`)

					WHERE `products_name` LIKE '%minil%'
						/* AND `products_status` = 1 */

					LIMIT 10

				SELECT
					CONCAT(
							'[span class={###QUOT###}productNumber{###QUOT###}]', `bctr_products`.`products_model`, '[/span] - ',
							'[span class={###QUOT###}productName{###QUOT###}]', `bctr_products_description`.`products_name`, '[/span] # ',
							'[span class={###QUOT###}productID{###QUOT###}]', `bctr_products`.`products_id`, '[/span]'
						) AS `searchResult`

					FROM `bctr_products_description`
					LEFT JOIN `bctr_products`
					ON(`bctr_products_description`.`products_id` = `bctr_products`.`products_id`)

					WHERE `products_name` LIKE '%minile%'
						/* AND `products_status` = 1 */

					LIMIT 10

				SELECT
					CONCAT(
							'[span class={###QUOT###}productNumber{###QUOT###}]', `bctr_products`.`products_model`, '[/span] - ',
							'[span class={###QUOT###}productName{###QUOT###}]', `bctr_products_description`.`products_name`, '[/span] # ',
							'[span class={###QUOT###}productID{###QUOT###}]', `bctr_products`.`products_id`, '[/span]'
						) AS `searchResult`

					FROM `bctr_products_description`
					LEFT JOIN `bctr_products`
					ON(`bctr_products_description`.`products_id` = `bctr_products`.`products_id`)

					WHERE `products_name` LIKE '%minilet%'
						/* AND `products_status` = 1 */

					LIMIT 10

				SELECT
					CONCAT(
							'[span class={###QUOT###}productNumber{###QUOT###}]', `bctr_products`.`products_model`, '[/span] - ',
							'[span class={###QUOT###}productName{###QUOT###}]', `bctr_products_description`.`products_name`, '[/span] # ',
							'[span class={###QUOT###}productID{###QUOT###}]', `bctr_products`.`products_id`, '[/span]'
						) AS `searchResult`

					FROM `bctr_products_description`
					LEFT JOIN `bctr_products`
					ON(`bctr_products_description`.`products_id` = `bctr_products`.`products_id`)

					WHERE `products_name` LIKE '%minilett%'
						/* AND `products_status` = 1 */

					LIMIT 10

				SELECT
					CONCAT(
							'[span class={###QUOT###}productNumber{###QUOT###}]', `bctr_products`.`products_model`, '[/span] - ',
							'[span class={###QUOT###}productName{###QUOT###}]', `bctr_products_description`.`products_name`, '[/span] # ',
							'[span class={###QUOT###}productID{###QUOT###}]', `bctr_products`.`products_id`, '[/span]'
						) AS `searchResult`

					FROM `bctr_products_description`
					LEFT JOIN `bctr_products`
					ON(`bctr_products_description`.`products_id` = `bctr_products`.`products_id`)

					WHERE `products_name` LIKE '%miniletter%'
						/* AND `products_status` = 1 */

					LIMIT 10

				SELECT
					CONCAT(
							'[span class={###QUOT###}productNumber{###QUOT###}]', `bctr_products`.`products_model`, '[/span] - ',
							'[span class={###QUOT###}productName{###QUOT###}]', `bctr_products_description`.`products_name`, '[/span] # ',
							'[span class={###QUOT###}productID{###QUOT###}]', `bctr_products`.`products_id`, '[/span]'
						) AS `searchResult`

					FROM `bctr_products_description`
					LEFT JOIN `bctr_products`
					ON(`bctr_products_description`.`products_id` = `bctr_products`.`products_id`)

					WHERE `products_name` LIKE '%miniletter%'
						/* AND `products_status` = 1 */

					LIMIT 10
