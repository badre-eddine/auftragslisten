<?php
	require_once('../config/configMandator.inc.php');
	require_once('../config/configParams_' . strtolower(MANDATOR) . '.inc.php');
	require_once('../config/configBasic.inc.php');
	require_once('../config/configFiles.inc.php');
	require_once('../config/configTables.inc.php');
	require_once('../inc/functions.inc.php');
	require_once('header.inc.php');
	// $_SERVER["DOCUMENT_ROOT"]."/includes/configure.php");

	session_start();

	$dbConnection_Extern = new DB_Connection();
	$db_openExtern = $dbConnection_Extern->db_connect();

	$dateStart	= trim($_GET["dateStart"]);
	$dateEnd	= trim($_GET["dateEnd"]);

	#$dateStart  =  formatDate($dateStart, "store");
	#$dateEnd  =  formatDate($dateEnd, "store");

	$content = '';

	$warningMessage = '';
	$errorMessage = '';
	$successMessage = '';

	if($dateStart != '' && $dateEnd != '') {
		$sql = "

			SELECT

				CONCAT(
					`holidaysDate`,
					':',
					`holidaysType`,
					':',
					`holidaysHolydayName`,
					':',
					`holidaysHolydayNameType`,
					':',
					`holidaysCountry`
				) AS `searchResult`


				FROM `" . TABLE_OFFICIAL_HOLIDAYS . "`

				WHERE  1
					AND `holidaysDate` BETWEEN '" . $dateStart . "' AND '" . $dateEnd . "'
					AND `holidaysActive` = '1'
		";

		$rs = $dbConnection_Extern->db_query($sql);
		$searchResult = '';
		$arrSearchResult = array();
		while($ds = mysqli_fetch_assoc($rs)){
			$arrSearchResult[] = $ds["searchResult"];
		}
		$searchResult = implode(";", $arrSearchResult);

		$content = $searchResult;

		if($dbConnection_Extern->db_displayErrors() != "") {
			$errorMessage .= $dbConnection_Extern->db_displayErrors(). '<br />';
		}
		$dbConnection_Extern->db_close($db_openExtern);
		#$content = $sql;
	}
	echo $content;
?>