<?php
	// BOF CREATE_FACTORING_ATTACHMENT
	$createPdfContentFaktoringInfo = "";
	$loadContentTemplate = implode('', file(DIRECTORY_PDF_TEMPLATES . '/template_BASIC.html'));

	/*
	#$arrBankAccountTypeDatas[$_POST["selectBankAccount"]]
	dd('arrBankAccountTypeDatas');
	[FAKT] => Array
        (
            [bankAccountTypesID] => 6
            [bankAccountTypesShortName] => FAKT
            [bankAccountTypesName] => Kreissparkasse Steinfurt
            [bankAccountTypesAccountNumber] => 73730889
            [bankAccountTypesBankCode] => 40351060
            [bankAccountTypesIBAN] => DE23403510600073730889
            [bankAccountTypesBIC] => WELADED1STF
            [bankAccountTypesShowWarning] => 1
            [bankAccountTypesActive] => 1
            [bankAccountTypesSort] => 0
            [bankAccountTypesDefault] =>
            [bankAccountTypesNotice] => Faktoring-Konto

			Universal Factoring GmbH, Kreuzerkamp 7-11, 40878 Ratingen

	*/

	// DEFINE('FAKTORING_COMPANY_ADDRESS', '<b>Universal Factoring GmbH, Kreuzerkamp 7-11, 40878 Ratingen</b>');
	DEFINE('FAKTORING_COMPANY_ADDRESS', '<b>Deutsche Factoring Bank GmbH &amp; Co. KG, Kreuzerkamp 7-11, 40878 Ratingen</b>');
	DEFINE('FAKTORING_BANK_ACCOUNT', $arrBankAccountTypeDatas[$_POST["selectBankAccount"]]["bankAccountTypesName"] . '<br>Konto-Nr: ' . $arrBankAccountTypeDatas[$_POST["selectBankAccount"]]["bankAccountTypesAccountNumber"] . '<br>IBAN: ' . $arrBankAccountTypeDatas[$_POST["selectBankAccount"]]["bankAccountTypesIBAN"] . '<br>BIC: ' . $arrBankAccountTypeDatas[$_POST["selectBankAccount"]]["bankAccountTypesBIC"]);

	DEFINE('FAKTORING_START_DATE_B3', '26.02.2016');
	DEFINE('FAKTORING_START_DATE_BCTR', '19.10.2015');
	DEFINE('FAKTORING_START_DATE', constant("FAKTORING_START_DATE_". strtoupper(MANDATOR)));

	// BOF FK
		$arrContentDatas['FK'] = array(
			'SALUTATION_TEXT' => '<p>Sehr geehrte Kundin, sehr geehrter Kunde,</p>',
			'INTRO_TEXT' => '
				<p>im Zuge unseres anhaltenden Wachstums setzen wir das Factoring als umsatzkonformes Finanzierungsinstrument ein. Dadurch ist es uns m&ouml;glich, uns noch mehr auf unsere Kunden und unser Kerngesch&auml;ft zu konzentrieren.<br><br>
				Unser Factoring-Dienstleister ist die <br>{###FAKTORING_COMPANY_ADDRESS###},<br>die als Mitglied der Sparkassen Finanzgruppe ein finanzstarker und solider Partner ist.<br>Sie &uuml;berwacht <b>seit dem {###FAKTORING_START_DATE###}</b> den Zahlungseingang und die mit Ihnen vereinbarten F&auml;lligkeiten und entlastet uns somit von einem Teil unserer Verwaltungsarbeit.<br><br>
				<b>Bitte leisten Sie zuk&uuml;nftige Zahlungen unmittelbar an die Universal Factoring GmbH auf das nachfolgende Konto unter Angabe Ihrer Abnehmer- und Rechnungsnummer</b>:<br><br></p>
				<div style="font-size:16pt;font-weight:bold;border:2pt solid #CC0000 !important;padding:4pt !important;">{###FAKTORING_BANK_ACCOUNT###}</div><br><br>
				<p>Liefertermine und etwaige R&uuml;ckfragen zu gelieferten Waren bitten wir weiterhin mit uns direkt abzustimmen.<br><br>Wir bedanken uns f&uuml;r Ihre Kooperation und freuen uns auf eine weiterhin erfolgreiche Zusammenarbeit.</p>',
			'EDITOR_TEXT' => '',
			'PAYMENT_CONDITIONS' => '',
			'OUTRO_TEXT' => '',
			'REGARDS_TEXT' => '<p>Mit freundlichen Gr&uuml;&szlig;en,<br><b><i>Ihr Burhan CTR Team</i></b></p>',
			'BANK_ACCOUNT_WARNING' => '' . $arrWarningBankAccountText . '',
			'FOOTER_NOTICE' => '',
		);

		$arrDocumentDatas['FK'] = '
			<!-- BOF_DOCUMENT_NUMBER --><!-- EOF_DOCUMENT_NUMBER -->
			<!-- BOF_CUSTOMER_NUMBER --><!-- EOF_CUSTOMER_NUMBER -->
			<!-- BOF_CUSTOMER_VAT_ID --><!-- EOF_CUSTOMER_VAT_ID -->
			<!-- BOF_CREATION_DATE --><tr><td align="left"><b>Datum:&nbsp;</b></td><td align="left">{###CREATION_DATE###}</td></tr><!-- EOF_CREATION_DATE -->
			<!-- BOF_CUSTOMERS_SALESMAN --><!-- EOF_CUSTOMERS_SALESMAN -->
			<!-- BOF_SALESMANS_CUSTOMER --><!-- EOF_SALESMANS_CUSTOMER -->
		';
	// EOF FK

	$loadContentTemplate = preg_replace("/{###DOCUMENT_DATAS###}/", $arrDocumentDatas["FK"], $loadContentTemplate);

	$createPdfContentFaktoringInfo = $loadContentTemplate;
	$createPdfContentFaktoringInfo = preg_replace("/{###CARD_DATAS###}/", "", $createPdfContentFaktoringInfo);
	$createPdfContentFaktoringInfo = preg_replace("/<!-- BOF PRICE_DATAS -->(.*)<!-- EOF PRICE_DATAS -->/ismU", "", $createPdfContentFaktoringInfo);

	// STYLES
	$loadTemplateStyles = implode('', file(DIRECTORY_PDF_TEMPLATES . 'template_STYLES.html'));
	$createPdfContentFaktoringInfo = preg_replace("/{###STYLES###}/", $loadTemplateStyles, $createPdfContentFaktoringInfo);

	$loadSubject = 'Neuer Factoring-Partner' . '<br><br>';
	$createPdfContentFaktoringInfo = preg_replace("/{###SUBJECT###}/", $loadSubject, $createPdfContentFaktoringInfo);

	$createPdfContentFaktoringInfo = preg_replace("/{###ADDRESS_INVOICE###}/", '', $createPdfContentFaktoringInfo);
	$createPdfContentFaktoringInfo = preg_replace("/{###ADDRESS_DELIVERY###}/", '', $createPdfContentFaktoringInfo);

	if($arrBankAccountTypeDatas[$_POST["selectBankAccount"]]["bankAccountTypesShowWarning"] == '1') {
		#$createPdfContentFaktoringInfo = preg_replace("/{###BANK_ACCOUNT_WARNING###}/", $arrContentDatas[$_REQUEST["documentType"]]['BANK_ACCOUNT_WARNING'], $createPdfContentFaktoringInfo);
	}

	if(!empty($loadedAdressDatas)) {
		foreach($loadedAdressDatas as $thisKey => $thisValue) {
			if($thisValue != "") {
				$createPdfContentFaktoringInfo = preg_replace("/{###".$thisKey."###}/", $thisValue . "<br />", $createPdfContentFaktoringInfo);
			}
			else {
				$createPdfContentFaktoringInfo = preg_replace("/{###".$thisKey."###}/", "", $createPdfContentFaktoringInfo);
			}
		}
	}

	if(!empty($loadedDocumentDatas)) {
		$createPdfContentFaktoringInfo = preg_replace("/{###BANK_ACCOUNT_WARNING###}/", "", $createPdfContentFaktoringInfo);

		foreach($arrContentDatas["FK"] as $thisKey => $thisValue) {
			$createPdfContentFaktoringInfo = preg_replace("/{###".$thisKey."###}/", $thisValue, $createPdfContentFaktoringInfo);
			if(trim($thisValue) == '') {
				$createPdfContentFaktoringInfo = preg_replace("/<!-- BOF_".$thisKey." -->(.*)<!-- EOF_".$thisKey." -->/", '', $createPdfContentFaktoringInfo);
			}
		}

		foreach($loadedDocumentDatas as $thisKey => $thisValue) {
			$createPdfContentFaktoringInfo = preg_replace("/{###".$thisKey."###}/", $thisValue, $createPdfContentFaktoringInfo);
			if(trim($thisValue) == '') {
				$createPdfContentFaktoringInfo = preg_replace("/<!-- BOF_".$thisKey." -->(.*)<!-- EOF_".$thisKey." -->/", '', $createPdfContentFaktoringInfo);
			}
		}
	}

	$createPdfContentFaktoringInfo = preg_replace("/{###CARD_TOTAL_COMPLETE_PRICE_VALUE###}/", '', $createPdfContentFaktoringInfo);

	#$createPdfContentFaktoringInfo = preg_replace("/{###FAKTORING_START_DATE###}/", '19.10.2015', $createPdfContentFaktoringInfo);
	$createPdfContentFaktoringInfo = preg_replace("/{###FAKTORING_START_DATE###}/", FAKTORING_START_DATE, $createPdfContentFaktoringInfo);
	$createPdfContentFaktoringInfo = preg_replace("/{###FAKTORING_COMPANY_ADDRESS###}/", FAKTORING_COMPANY_ADDRESS, $createPdfContentFaktoringInfo);
	$createPdfContentFaktoringInfo = preg_replace("/{###FAKTORING_BANK_ACCOUNT###}/", FAKTORING_BANK_ACCOUNT, $createPdfContentFaktoringInfo);

	$createPdfContentFaktoringInfo = removeUnnecessaryChars($createPdfContentFaktoringInfo);

	$displayPdfContentFaktoringInfo = $createPdfContentFaktoringInfo;

	if($_POST["editDocType"] != "") {
		$displayPdfContentFaktoringInfo = preg_replace("/{###DOCUMENT_NUMBER###}/", '', $displayPdfContentFaktoringInfo);
	}
	else {
		$displayPdfContentFaktoringInfo = preg_replace("/{###DOCUMENT_NUMBER###}/", '', $displayPdfContentFaktoringInfo);
	}
	$displayPdfContentFaktoringInfo = preg_replace("/{###DOCUMENT_NUMBER###}/", '', $displayPdfContentFaktoringInfo);
	$displayPdfContentFaktoringInfo = $createPdfContentFaktoringInfo;

	$displayPdfContentFaktoringInfo = preg_replace('/<input (.*) \/>/ismU', '', $displayPdfContentFaktoringInfo);
	$createPdfContentFaktoringInfo = preg_replace('/<input (.*) \/>/ismU', '', $createPdfContentFaktoringInfo);

	// BOF INCLUDE PDF SEPARATOR
	if($userDatas["usersLogin"] == "thorsten"){
		$createPdfContentFaktoringInfo = '<!-- BOF DOCUMENT_FAKTORING_INFO -->' . $createPdfContentFaktoringInfo . '<!-- EOF DOCUMENT_FAKTORING_INFO -->';
	}
	// EOF INCLUDE PDF SEPARATOR

	#$pdfCreateContent .= $createPdfContentFaktoringInfo;
	$pdfCreateContent = $createPdfContentFaktoringInfo . $pdfCreateContent;

	// EOF CREATE_FACTORING_ATTACHMENT
?>