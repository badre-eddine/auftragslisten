<?php
	require_once('../config/configMandator.inc.php');
	require_once('../config/configParams_' . strtolower(MANDATOR) . '.inc.php');
	require_once('../config/configBasic.inc.php');
	require_once('../config/configFiles.inc.php');
	require_once('../config/configTables.inc.php');
	require_once('../inc/functions.inc.php');
	require_once('header.inc.php');
	// $_SERVER["DOCUMENT_ROOT"]."/includes/configure.php");

	session_start();

	$elementMenge = trim($_GET["elementMenge"]);
	$elementArtikelID = trim($_GET["elementArtikelID"]);

	$content = '';

	$warningMessage = '';
	$errorMessage = '';
	$successMessage = '';

	// elementMenge, formElementSinglePreis, formElementTotalPreis, elementArtikelID

	if($elementMenge != "" && $elementArtikelID != "") {
		if(LOAD_EXTERNAL_SHOP_DATAS == true){
			$dbConnection_Extern = new DB_Connection(DB_HOST_EXTERN_SHOP, '', DB_NAME_EXTERN_SHOP, DB_USER_EXTERN_SHOP, DB_PASSWORD_EXTERN_SHOP);
			$db_openExtern = $dbConnection_Extern->db_connect();
		}
		else {
			$dbConnection_Extern = new DB_Connection();
			$db_openExtern = $dbConnection_Extern->db_connect();
		}

		$sql_xxx = "
			SELECT

				`" . TABLE_PERSONAL_OFFERS_BY_CUSTOMERS . DEFAULT_PRICE_GROUP . "`.`personal_offer`

				FROM `" . TABLE_PRODUCTS . "`

				LEFT JOIN `" . TABLE_PERSONAL_OFFERS_BY_CUSTOMERS . DEFAULT_PRICE_GROUP . "`
				ON(`" . TABLE_PRODUCTS . "`.`products_id` = `" . TABLE_PERSONAL_OFFERS_BY_CUSTOMERS . DEFAULT_PRICE_GROUP . "`.`products_id`)

				WHERE `" . TABLE_PRODUCTS . "`.`products_model` = '" . $elementArtikelID . "'
					AND `" . TABLE_PERSONAL_OFFERS_BY_CUSTOMERS . DEFAULT_PRICE_GROUP . "`.`quantity` <= '" . $elementMenge . "'
					AND `" . TABLE_PERSONAL_OFFERS_BY_CUSTOMERS . DEFAULT_PRICE_GROUP . "`.`quantity` > 0
					AND `" . TABLE_PERSONAL_OFFERS_BY_CUSTOMERS . DEFAULT_PRICE_GROUP . "`.`personal_offer` > 0

				LIMIT 1
		";

		$sql = "

			SELECT
				`" . TABLE_PERSONAL_OFFERS_BY_CUSTOMERS . DEFAULT_PRICE_GROUP . "`.`personal_offer`

				FROM (
					SELECT
						`" . TABLE_PRODUCTS . "`.`products_id` AS `products_id`
						FROM `" . TABLE_PRODUCTS . "`
						WHERE `" . TABLE_PRODUCTS . "`.`products_model` = '" . $elementArtikelID . "'
							AND `" . TABLE_PRODUCTS . "`.`products_status` = 1

						GROUP BY `" . TABLE_PRODUCTS . "`.`products_id`

					UNION

					SELECT
						`" . TABLE_PRODUCTS_ATTRIBUTES . "`.`products_id`
						FROM `" . TABLE_PRODUCTS_ATTRIBUTES . "`
						LEFT JOIN `" . TABLE_PRODUCTS . "`
						ON (`" . TABLE_PRODUCTS_ATTRIBUTES . "`.`products_id` = `" . TABLE_PRODUCTS . "`.`products_id`)
						WHERE `" . TABLE_PRODUCTS_ATTRIBUTES . "`.`attributes_model` = '" . $elementArtikelID . "'
							AND `" . TABLE_PRODUCTS . "`.`products_status` = 1

						GROUP BY `" . TABLE_PRODUCTS_ATTRIBUTES . "`.`products_id`
				) AS `temp`

				LEFT JOIN `" . TABLE_PERSONAL_OFFERS_BY_CUSTOMERS . DEFAULT_PRICE_GROUP . "`
				ON(`temp`.`products_id` = `" . TABLE_PERSONAL_OFFERS_BY_CUSTOMERS . DEFAULT_PRICE_GROUP . "`.`products_id`)

				WHERE  `" . TABLE_PERSONAL_OFFERS_BY_CUSTOMERS . DEFAULT_PRICE_GROUP . "`.`quantity` <= '" . $elementMenge . "'
					AND `" . TABLE_PERSONAL_OFFERS_BY_CUSTOMERS . DEFAULT_PRICE_GROUP . "`.`quantity` > 0
					AND `" . TABLE_PERSONAL_OFFERS_BY_CUSTOMERS . DEFAULT_PRICE_GROUP . "`.`personal_offer` > 0

				LIMIT 1
		";

		$rs = $dbConnection_Extern->db_query($sql);
		list($searchResult) = mysqli_fetch_array($rs);

		// $content = number_format($searchResult, 2, ',', '');
		$content = $searchResult;

		if($dbConnection_Extern->db_displayErrors() != "") {
			$errorMessage .= $dbConnection_Extern->db_displayErrors(). '<br />';
		}
		$dbConnection_Extern->db_close($db_openExtern);
		// $content = $sql;
	}
	echo $content;
?>