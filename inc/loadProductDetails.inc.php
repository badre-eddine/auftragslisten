<?php
	session_start();

	require_once('../config/configMandator.inc.php');
	require_once('../config/configParams_' . strtolower(MANDATOR) . '.inc.php');
	require_once('../config/configBasic.inc.php');
	require_once('../config/configFiles.inc.php');
	require_once('../config/configTables.inc.php');
	require_once('../inc/functions.inc.php');
	##require_once('header.inc.php');

	$thisSearchString = (trim($_GET["productID"]));

	$content = '';

	$warningMessage = '';
	$errorMessage = '';
	$successMessage = '';
	$infoMessage = '';

	$useUf8Encode = false;

	$dbConnection = new DB_Connection();
	$db_open = $dbConnection->db_connect();

	if($thisSearchString != "") {

		if(LOAD_EXTERNAL_SHOP_DATAS){
			$dbConnection_ExternShop = new DB_Connection(DB_HOST_EXTERN_SHOP, '', DB_NAME_EXTERN_SHOP, DB_USER_EXTERN_SHOP, DB_PASSWORD_EXTERN_SHOP);
			$dbOpen_ExternShop = $dbConnection_ExternShop->db_connect();
		}
		else {
			$dbConnection_ExternShop = $dbConnection;
		}

		$sql = "SELECT
						`products_options_id`,
						`language_id`,
						`products_options_name`,
						`products_options_sortorder`

						FROM `" . TABLE_PRODUCTS_OPTIONS . "`

						WHERE 1
			";

		$rs = $dbConnection_ExternShop->db_query($sql);

		while($ds = mysqli_fetch_assoc($rs)) {
			foreach(array_keys($ds) as $field) {
				$arrProductsOptions[$ds["products_options_id"]][$field] = (strip_tags($ds[$field]));
			}
		}

		$sql = "SELECT
					`products_options_values_id`,
					`language_id`,
					`products_options_values_name`

					FROM `" . TABLE_PRODUCTS_OPTIONS_VALUES . "`

					WHERE 1
		";

		$rs = $dbConnection_ExternShop->db_query($sql);

		while($ds = mysqli_fetch_assoc($rs)) {
			foreach(array_keys($ds) as $field) {
				$arrProductsOptionsValues[$ds["products_options_values_id"]][$field] = (strip_tags($ds[$field]));
			}
		}

		$sql = "
			SELECT
				`" . TABLE_PRODUCTS . "`.`products_id`,
				`" . TABLE_PRODUCTS . "`.`products_model`,
				CONCAT(
					'http://www.burhan-ctr.de/images/product_images/thumbnail_images/',
					`" . TABLE_PRODUCTS . "`.`products_image`
				) AS `products_image`,

				`" . TABLE_PRODUCTS_DESCRIPTION . "`.`products_name`,
				`" . TABLE_PRODUCTS_DESCRIPTION . "`.`products_description`,

				GROUP_CONCAT(
					DISTINCT
					`" . TABLE_PERSONAL_OFFERS_BY_CUSTOMERS . DEFAULT_PRICE_GROUP . "`.`quantity`,
					':',
					`" . TABLE_PERSONAL_OFFERS_BY_CUSTOMERS . DEFAULT_PRICE_GROUP . "`.`personal_offer`
					ORDER BY `" . TABLE_PERSONAL_OFFERS_BY_CUSTOMERS . DEFAULT_PRICE_GROUP . "`.`quantity`
					SEPARATOR '#'
				) AS `prices_3`,

				GROUP_CONCAT(`" . TABLE_PRODUCTS_ATTRIBUTES . "`.`options_id`,
					':',
					`" . TABLE_PRODUCTS_ATTRIBUTES . "`.`options_values_id`,
					':',
					`" . TABLE_PRODUCTS_ATTRIBUTES . "`.`options_values_price`,
					':',
					`" . TABLE_PRODUCTS_ATTRIBUTES . "`.`price_prefix`,
					':',
					`" . TABLE_PRODUCTS_ATTRIBUTES . "`.`attributes_model`
					ORDER BY `" . TABLE_PERSONAL_OFFERS_BY_CUSTOMERS . DEFAULT_PRICE_GROUP . "`.`quantity`
					SEPARATOR '#'
				) AS `products_option`,

				`" . TABLE_PRODUCTS_VPE . "`.`products_vpe_name`


				FROM `" . TABLE_PRODUCTS . "`

				LEFT JOIN `" . TABLE_PRODUCTS_DESCRIPTION . "`
				ON(`" . TABLE_PRODUCTS . "`.`products_id` = `" . TABLE_PRODUCTS_DESCRIPTION . "`.`products_id`)

				LEFT JOIN `" . TABLE_PERSONAL_OFFERS_BY_CUSTOMERS . DEFAULT_PRICE_GROUP . "`
				ON(`" . TABLE_PRODUCTS . "`.`products_id` = `" . TABLE_PERSONAL_OFFERS_BY_CUSTOMERS . DEFAULT_PRICE_GROUP . "`.`products_id`)

				LEFT JOIN `" . TABLE_PRODUCTS_ATTRIBUTES . "`
				ON(`" . TABLE_PRODUCTS . "`.`products_id` = `" . TABLE_PRODUCTS_ATTRIBUTES . "`.`products_id`)

				LEFT JOIN `" . TABLE_PRODUCTS_VPE . "`
				ON(`" . TABLE_PRODUCTS . "`.`products_vpe` = `" . TABLE_PRODUCTS_VPE . "`.`products_vpe_id`)

				WHERE
					`" . TABLE_PRODUCTS . "`.`products_id` = '" . $thisSearchString . "'
					/* AND `products_status` = 1 */
					/* AND `" . TABLE_PERSONAL_OFFERS_BY_CUSTOMERS . DEFAULT_PRICE_GROUP . "`.`personal_offer` > 0 */
					/* AND `" . TABLE_PRODUCTS_DESCRIPTION . "`.`language_id` = '2' */
					/* AND `" . TABLE_PRODUCTS_VPE . "`.`language_id` = '2' */

				GROUP BY `" . TABLE_PRODUCTS . "`.`products_id`
		";

		$rs = $dbConnection_ExternShop->db_query($sql);
echo mysqli_error();
		list(
				$products_id,
				$products_model,
				$products_image,
				$products_name,
				$products_description,
				$prices_3,
				$products_option,
				$products_vpe
			) = mysqli_fetch_array($rs);

		if($products_id != ''){
			$content .=  '<h2>'.($products_name).'</h2>';

			$content .=  '<table border="0" cellpadding="0" cellspacing="0" width="" class="noBorder">';
			$content .=  '<tr>';
			$content .=  '<td><b>Bezeichnung:</b></td><td>' . ($products_name) . '</td>';
			$content .=  '</tr>';
			$content .=  '<tr>';
			$content .=  '<td><b>Art.-Nummer:</b></td><td>' . $products_model . '</td>';
			$content .=  '</tr>';

			$arrGetSupplier = explode("/", $products_model);

			$sql = "SELECT
						`suppliersID`,
						`suppliersKundennummer`,
						`suppliersFirmenname`,
						`suppliersFirmennameZusatz`

						FROM `" . TABLE_SUPPLIERS . "`

						WHERE `suppliersFirmaArtikelnummerKurzform` = '" . $arrGetSupplier[(count($arrGetSupplier) - 1)] . "'
			";

			$rs = $dbConnection->db_query($sql);
			list(
				$suppliersID,
				$suppliersKundennummer,
				$suppliersFirmenname,
				$suppliersFirmennameZusatz
			) = mysqli_fetch_array($rs);

			$content .=  '<tr>';
			$content .=  '<td><b>Lieferant:</b></td><td><a href="' . PAGE_EDIT_DISTRIBUTORS. '?editID=' . $suppliersID . '">' . $suppliersFirmenname . '</a></td>';
			$content .=  '</tr>';

			$content .=  '<tr>';
			if($products_vpe == ''){
				$products_vpe = 'Stück';
			}
			$content .=  '<td><b>Verpackungseinheit:</b></td><td>pro ' . $products_vpe . '</td>';
			$content .=  '</tr>';
			$content .=  '</table>';

			$content .=  '<table border="0" cellpadding="0" cellspacing="0" width="100%" class="displayOrders">
					<tr>

						<th>Bild</th>
						<th>Beschreibung</th>
						<th style="width:140px;">Preise</th>
						<th style="width:160px;">Farben / Art.-Nummer</th>
					</tr>
					<tr>
						<td style="text-align:center;"><img src="'.($products_image).'" /></td>
			';

			$content .=  '
						<td>';
							$thisProductsDescription = preg_replace("/<(.*)>/ismU", "[$1]", $products_description);
							// $thisProductsDescription = ($thisProductsDescription);
							$thisProductsDescription = preg_replace("/\[(.*)\]/ismU", "<$1>", $thisProductsDescription);
							$content .=  ($thisProductsDescription);
					$content .=  '</td>';
						// <td><pre>'.strip_tags(preg_replace("/<li>|<br \/>|<\/p>/", "\n", $products_description)).'</pre></td>
			$content .=  '<td>';

			$arrPrices_3 = explode('#', $prices_3);
			$arrPrices_3 = array_unique($arrPrices_3);
			$arrPrices = array();
			$content .=  '<table border="0 cellpadding="0" cellspacing="0" width="100%" class="border">';
			$content .=  '<tr>
					<th style="text-align:center;" width="50%">Menge</th>
					<th style="text-align:center;" width="50%">Preis</th>
				</tr>';
			if(!empty($arrPrices_3)) {
				foreach($arrPrices_3 as $thisKey => $thisValue) {
					$arrThisValue = explode(":", $thisValue);
					if($arrThisValue[1] > 0) {
						$content .=  '
							<tr>
								<td style="text-align:right;white-space:nowrap">ab '.$arrThisValue[0].(" St&uuml;ck").'</td>
								<td style="text-align:right;"> je '.number_format($arrThisValue[1], 2, ',', '.').' &euro; </td>
							</tr>';
					}
					else if(count($arrPrices_3) == 1){
						$content .=  '
							<tr>
								<td colspan="2">Preis auf Anfrage</td>
							</tr>';
					}
				}
			}
			$content .=  '</table><br />';
			$content .=  '<p><b>Preise pro ' . $products_vpe . '</b></p>';
			$content .=  '</td>';
			$content .=  '<td>';


			$arrProducts_option = explode('#', $products_option);
			if(!empty($arrProducts_option)) {
				$arrProducts_option = array_unique($arrProducts_option);
					$content .=  '<table border="0 cellpadding="0" cellspacing="0" width="100%" class="border">';
				$content .=  '<tr>
						<th style="text-align:center;" width="50%">Farbe</th>
						<th style="text-align:center;" width="50%">Art.-Nr.</th>
					</tr>';

				foreach($arrProducts_option as $thisKey => $thisValue) {
					$arrThisValue = explode(":", $thisValue);
					if($arrThisValue[0] == 1) {

						$content .=  '<tr>
										<td>'.$arrProductsOptionsValues[$arrThisValue[1]]["products_options_values_name"].'</td>
										<td style="white-space:nowrap;">'.$arrThisValue[4].'</td>
									</tr>
					';

					}
				}
				$content .=  '</table >';

			}

			$content .=  '		</td>
					</tr>
			';
			$content .= '</table>';
		}
		else {
			$content .= '<p class="warningArea">Es wurden keine Daten gefunden!</p>';
		}

		if(LOAD_EXTERNAL_SHOP_DATAS){
			if($dbConnection_ExternShop) {
				$dbConnection_ExternShop->db_close($dbOpen_ExternShop);
			}
		}
	}
	$content = removeUnnecessaryChars($content);
	echo $content;
?>