<?php
srand ( (double)microtime () * 1000000 );
$rand = rand();
$rand = date('YmdH');
#$rand = date('Ymd');
$addRand = "?" . $rand;
$addRand = "";
$userDatas = getUserDatas();

# $thisDocType = '';
$thisDocType = '<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">';
if($_COOKIE["isAdmin"] == 'x1'){
	#$thisDocType = '<!DOCTYPE html>';
	#$thisDocType = '<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd" [<!ATTLIST img rel CDATA #IMPLIED>]>';
}

$headerHTML = '';

$headerHTML .= $thisDocType;
$headerHTML .= '

		<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="de" lang="de">
		<head>
			<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
			<title>' . MANDATOR . ' | Warenwirtschaft - {###TITLE###}</title>
			<meta name="keywords" content="" />
			<meta name="description" content="" />
			<meta name="robots" content="NOINDEX,NOFOLLOW" />
			<meta name="author" content="Thorsten Hoff" />
			<meta http-equiv="imagetoolbar" content="no" />
			<!-- <meta http-equiv="content-type" content="text/html; utf-8" /> -->
			<meta http-equiv="Content-Script-Type" content="text/javascript" />
			<meta http-equiv="Content-Style-Type" content="text/css" />
			<meta name="revisit-after" content="1 days" />
			<meta http-equiv="content-language" content="de" />
			<meta http-equiv="cache-control" content="no-cache" />
			<link rel="shortcut icon" href="' . FILE_FAVICON . '" />
			<link rel="icon" href="' . FILE_FAVICON . '" type="image/x-icon" />

			<link rel="stylesheet" type="text/css" href="' . FILE_LAYOUT_CSS . $addRand . '" media="screen,print" charset="utf-8" />
			<link rel="stylesheet" type="text/css" href="' . FILE_PRINT_CSS . $addRand . '" media="print" charset="utf-8" />
			<script language="javascript" type="text/javascript">
				/* <![CDATA[ */
				document.write(\'<link rel="stylesheet" type="text/css" href="' . FILE_BORDERS_CSS . $addRand . '" media="all" charset="utf-8" />\');
				/* ]]> */
			</script>
			<script src="' . FILE_FUNCTIONS_JS . $addRand . '" type="text/javascript" charset="utf-8"></script>
			<script src="' . FILE_JQUERY_JS . '" type="text/javascript"></script>
			<script src="' . FILE_JQUERY_POWERTABLE . '" type="text/javascript"></script>
			<script src="' . FILE_JQUERY_UI_JS . '" type="text/javascript"></script>
			<script src="' . FILE_JQUERY_DATEPICKER_DE_JS . '" type="text/javascript"></script>
			<script src="' . FILE_JQUERY_TIMEPICKER_DE_JS . '" type="text/javascript"></script>
			<script src="' . FILE_JQUERY_MEDIA_JS . '" type="text/javascript"></script>
			<script src="' . FILE_JQUERY_LAZYLOAD_JS . '" type="text/javascript"></script>			
			
			<!--
			<link rel="stylesheet" type="text/css" href="' . FILE_JAVASCRIPT_ALERT_CSS . '" media="screen" />
			<script src="' . FILE_JAVASCRIPT_ALERT_JS . '" type="text/javascript"></script>
			-->

			<!--
			<link rel="stylesheet" type="text/css" href="' . FILE_JAVASCRIPT_CLUSTERIZE_CSS . '" media="screen" />
			<script src="' . FILE_JAVASCRIPT_CLUSTERIZE_JS . '" type="text/javascript"></script>
			-->

			<!--
			<link rel="stylesheet" type="text/css" href="' . FILE_JQUERY_CONFIRM_CSS . '" media="screen" />
			<script src="' . FILE_JQUERY_CONFIRM_JS . '" type="text/javascript"></script>
			-->

			<!--
			<script type="text/javascript" src="' . PATH_JQUERY_SELECT . 'select2.js"></script>
			<link rel="stylesheet" type="text/css" href="' . PATH_JQUERY_SELECT . 'select2.css">
			-->

			<link rel="stylesheet" type="text/css" href="' . FILE_JQUERY_UI_CSS . '" media="screen" charset="utf-8" />
			<link rel="stylesheet" type="text/css" href="' . FILE_JQUERY_TIMEPICKER_DE_CSS . '" media="screen" charset="utf-8" />
		</head>
		<body>
		<div id="pageArea">
			<div id="documentArea">
				<div id="headerArea">
					<div id="headerAreaContent" style="background-image:url(layout/headerBG_' . MANDATOR . '.png);" >
						<div id="headerAreaLogo" style="background-image:url(layout/headerLogo_' . MANDATOR . '.png);" >
							<div id="headerAreaTitle">Warenwirtschaft</div>
							<div class="clear"></div>
						</div>
						<div id="headerAreaAdress">
	';
if(defined("COMPANY_PHONE") && defined("COMPANY_FAX")){
	$headerHTML .= '
							<span class="headerAreaAdressContent_' . MANDATOR . '">&bull; <b>Tel.: </b>' . COMPANY_PHONE . ' &bull; <b>Fax: </b>' . COMPANY_FAX . '</span>
		';
}

$headerHTML .= '
						</div>
						<div id="infoArea">
							<div id="infoAreaContent">
								<table border="0" cellpadding="0" cellspacing="0" width="100%">
									<tr>
	';
								if(isset($_SESSION["usersID"]) && $_SESSION["usersID"] != "") {
									if(empty($userDatas)) { $userDatas = getUserDatas(); }
									$headerHTML .='<td><span style="color:#009900;padding-right:20px; line-height:10px;"><b>Eingeloggt als:</b> '.$userDatas["usersFirstName"].' '.$userDatas["usersLastName"].'</span></td>';
									// $headerHTML .=' | ';
									// $headerHTML .='<span style="color:#CC0000;"><b><a href="./?mode=logoutUser" style="color:#CC0000;text-decoration:none;">Logout</a></b></span>';
									$headerHTML .='<td width="20"><a href="./?mode=logoutUser" style="color:#CC0000;text-decoration:none;"><img src="layout/icons/logout.png" alt="" title="Logout" width="14" height="14"/></a></td>';

								}
								else {
									$headerHTML .='<td><span style="color:#CC0000; padding-right:20px;"><b>Nicht eingeloggt</b> </span></td>';
									$headerHTML .='<td width="20"> <a href="./?mode=logoutUser" style="color:#CC0000;text-decoration:none;"><img src="layout/spacer.gif" alt="" title="" width="14" height="14"/></a></td>';
								}
								$headerHTML .='<td width="20"><a href="javascript:void(0);" onclick="window.print();" style="color:#CC0000;text-decoration:none;"><img src="layout/icons/printer.png" title="Diese Seite drucken" alt="drucken" width="14" height="14"/></a></td>';


$headerHTML .= '		 			</tr>
								</table>
								<hr /><div class="headerDate"><a href="' . PAGE_DISPLAY_CALENDAR . '" title="Kalender anzeigen"><b>Datum:</b> '.date("d.m.Y").' - <b>KW '.date("W").'</b></a></div>
							</div>
						</div>
						<div class="clear"></div>
					</div>
				</div>
	';

if(isset($isWebConnection) && !$isWebConnection){
	$headerHTML .= '
		<div class="errorArea">
			Achtung! Es ist keine Internetverbindung vorhanden! Die Warenwirtschaft ist nur eingeschr&auml;nkt nutzbar.
		</div>
	';
}

if(isset($isExternalDBConnection) && !$isExternalDBConnection){
	$headerHTML .= '
		<div class="errorArea">
			Achtung! Es ist keine Verbindung zur Online-Datenbank vorhanden! Die Warenwirtschaft ist nur eingeschr&auml;nkt nutzbar.
		</div>
	';
}

$headerHTML .= '
				<noscript>
					<div class="errorArea">
						Achtung! Um alle Funktionen nutzen zu k&ouml;nnen, m&uuml;ssen Sie Javascript aktivieren!
					</div>
				</noscript>

				<script language="javascript" type="text/javascript">
					<!--
					/* <![CDATA[ */
					if(!navigator.cookieEnabled) {
						document.write(\'<div class="errorArea">\');
							document.write(\'Achtung! Um alle Funktionen nutzen zu k&ouml;nnen, m&uuml;ssen Sie Cookies zulassen!\');
						document.write(\'<\/div>\');
					}
					/* ]]> */
					-->
				</script>
	';
?>