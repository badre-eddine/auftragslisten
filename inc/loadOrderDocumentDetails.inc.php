<?php
	session_start();

	require_once('../config/configMandator.inc.php');
	require_once('../config/configParams_' . strtolower(MANDATOR) . '.inc.php');
	require_once('../config/configBasic.inc.php');
	require_once('../config/configFiles.inc.php');
	require_once('../config/configTables.inc.php');
	require_once('../inc/functions.inc.php');
	##require_once('header.inc.php');

	$thisDocumentType = (trim($_GET["documentType"]));
	$thisSearchString = (trim($_GET["documentID"]));

	$content = '';

	$warningMessage = '';
	$errorMessage = '';
	$successMessage = '';
	$infoMessage = '';

	$useUf8Encode = false;
if(1){
	//if($thisSearchString != "") {

		// BOF DEFINE DOCUMENT TABLES DEPENDING ON DOCUMENT TYPE
		DEFINE('TABLE_ORDER_THIS', constant('TABLE_ORDER_' . $thisDocumentType));
		DEFINE('TABLE_ORDER_THIS_DETAILS', constant('TABLE_ORDER_' . $thisDocumentType.'_DETAILS'));
		// EOF DEFINE DOCUMENT TABLES DEPENDING ON DOCUMENT TYPE

		$dbConnection = new DB_Connection();
		$db_open = $dbConnection->db_connect();

		// BOF READ COUNTRIES
			$arrCountryTypeDatas = getCountryTypes();
		// EOF READ COUNTRIES

		$sql = "SELECT

				`" . TABLE_ORDER_THIS . "`.`orderDocumentsID`,
				`" . TABLE_ORDER_THIS . "`.`orderDocumentsNumber`,
				`" . TABLE_ORDER_THIS . "`.`orderDocumentsType`,
				`" . TABLE_ORDER_THIS . "`.`orderDocumentsOrdersIDs`,
				`" . TABLE_ORDER_THIS . "`.`orderDocumentsCustomerNumber`,
				`" . TABLE_ORDER_THIS . "`.`orderDocumentsCustomersOrderNumber`,
				`" . TABLE_ORDER_THIS . "`.`orderDocumentsProcessingDate`,
				`" . TABLE_ORDER_THIS . "`.`orderDocumentsDeliveryDate`,
				`" . TABLE_ORDER_THIS . "`.`orderDocumentsBindingDate`,
				`" . TABLE_ORDER_THIS . "`.`orderDocumentsSalesman`,
				`" . TABLE_ORDER_THIS . "`.`orderDocumentsAddressCompany`,
				`" . TABLE_ORDER_THIS . "`.`orderDocumentsAddressName`,
				`" . TABLE_ORDER_THIS . "`.`orderDocumentsAddressStreet`,
				`" . TABLE_ORDER_THIS . "`.`orderDocumentsAddressStreetNumber`,
				`" . TABLE_ORDER_THIS . "`.`orderDocumentsAddressZipcode`,
				`" . TABLE_ORDER_THIS . "`.`orderDocumentsAddressCity`,
				`" . TABLE_ORDER_THIS . "`.`orderDocumentsAddressCountry`,
				`" . TABLE_ORDER_THIS . "`.`orderDocumentsAddressMail`,
				`" . TABLE_ORDER_THIS . "`.`orderDocumentsSumPrice`,
				`" . TABLE_ORDER_THIS . "`.`orderDocumentsDiscount`,
				`" . TABLE_ORDER_THIS . "`.`orderDocumentsDiscountType`,
				`" . TABLE_ORDER_THIS . "`.`orderDocumentsDiscountPercent`,
				`" . TABLE_ORDER_THIS . "`.`orderDocumentsMwst`,
				`" . TABLE_ORDER_THIS . "`.`orderDocumentsMwstPrice`,
				`" . TABLE_ORDER_THIS . "`.`orderDocumentsShippingCosts`,
				`" . TABLE_ORDER_THIS . "`.`orderDocumentsPackagingCosts`,
				`" . TABLE_ORDER_THIS . "`.`orderDocumentsCashOnDelivery`,
				`" . TABLE_ORDER_THIS . "`.`orderDocumentsTotalPrice`,
				`" . TABLE_ORDER_THIS . "`.`orderDocumentsBankAccount`,
				`" . TABLE_ORDER_THIS . "`.`orderDocumentsPaymentCondition`,
				`" . TABLE_ORDER_THIS . "`.`orderDocumentsPaymentType`,
				`" . TABLE_ORDER_THIS . "`.`orderDocumentsDeliveryType`,
				`" . TABLE_ORDER_THIS . "`.`orderDocumentsDeliveryTypeNumber`,
				`" . TABLE_ORDER_THIS . "`.`orderDocumentsSubject`,
				`" . TABLE_ORDER_THIS . "`.`orderDocumentsContent`,
				`" . TABLE_ORDER_THIS . "`.`orderDocumentsDocumentPath`,
				`" . TABLE_ORDER_THIS . "`.`orderDocumentsTimestamp`,
				`" . TABLE_ORDER_THIS . "`.`orderDocumentsStatus`,
				`" . TABLE_ORDER_THIS . "`.`orderDocumentsSendMail`,

				CONCAT(
					'<b>Kunde: </b>',
					`tableCustomers`.`customersFirmenname`,
					`tableCustomers`.`customersFirmennameZusatz`,
					' (',
					`" . TABLE_ORDER_THIS . "`.`orderDocumentsCustomerNumber`,
					')'
				) AS `orderDocumentsAddressCompany2`,

				`" . TABLE_ORDER_THIS_DETAILS . "`.`orderDocumentDetailID`,
				`" . TABLE_ORDER_THIS_DETAILS . "`.`orderDocumentDetailDocumentID`,
				`" . TABLE_ORDER_THIS_DETAILS . "`.`orderDocumentDetailOrderID`,
				`" . TABLE_ORDER_THIS_DETAILS . "`.`orderDocumentDetailOrderType`,
				`" . TABLE_ORDER_THIS_DETAILS . "`.`orderDocumentDetailPos`,
				`" . TABLE_ORDER_THIS_DETAILS . "`.`orderDocumentDetailProductNumber`,
				`" . TABLE_ORDER_THIS_DETAILS . "`.`orderDocumentDetailProductName`,
				`" . TABLE_ORDER_THIS_DETAILS . "`.`orderDocumentDetailProductKategorieID`,
				`" . TABLE_ORDER_THIS_DETAILS . "`.`orderDocumentDetailProductQuantity`,
				`" . TABLE_ORDER_THIS_DETAILS . "`.`orderDocumentDetailProductColorsText`,
				`" . TABLE_ORDER_THIS_DETAILS . "`.`orderDocumentDetailProductColorsCount`,
				`" . TABLE_ORDER_THIS_DETAILS . "`.`orderDocumentDetailsWithBorder`,
				`" . TABLE_ORDER_THIS_DETAILS . "`.`orderDocumentDetailKommission`,
				`" . TABLE_ORDER_THIS_DETAILS . "`.`orderDocumentDetailPrintText`,
				`" . TABLE_ORDER_THIS_DETAILS . "`.`orderDocumentDetailNotiz`,
				`" . TABLE_ORDER_THIS_DETAILS . "`.`orderDocumentDetailProductSinglePrice`,
				`" . TABLE_ORDER_THIS_DETAILS . "`.`orderDocumentDetailProductTotalPrice`,
				`" . TABLE_ORDER_THIS_DETAILS . "`.`orderDocumentDetailMwst`,
				`" . TABLE_ORDER_THIS_DETAILS . "`.`orderDocumentDetailDiscount`,
				`" . TABLE_ORDER_THIS_DETAILS . "`.`orderDocumentDetailDirectSale`,
				`" . TABLE_ORDER_THIS_DETAILS . "`.`orderDocumentDetailPosType`,
				`" . TABLE_ORDER_THIS_DETAILS . "`.`orderDocumentDetailGroupingID`,

				`tableSalesman`.`customersID` AS `salesmanID`,
				`tableSalesman`.`customersKundennummer` AS `salesmanKundennummer`,
				`tableSalesman`.`customersFirmenname` AS `salesmanFirmenname`,

				`tableCustomers`.`customersID` AS `customersID`,
				`tableCustomers`.`customersKundennummer` AS `customersKundennummer`,
				`tableCustomers`.`customersFirmenname` AS `customersFirmenname`

			FROM `" . TABLE_ORDER_THIS . "`

			LEFT JOIN `" . TABLE_ORDER_THIS_DETAILS . "`
			ON(`" . TABLE_ORDER_THIS . "`.`orderDocumentsID` = `" . TABLE_ORDER_THIS_DETAILS . "`.`orderDocumentDetailDocumentID`)

			LEFT JOIN `" . TABLE_CUSTOMERS . "` AS `tableSalesman`
			ON(`" . TABLE_ORDER_THIS . "`.`orderDocumentsSalesman` = `tableSalesman`.`customersID`)

			LEFT JOIN `" . TABLE_CUSTOMERS . "` AS `tableCustomers`
			ON(`" . TABLE_ORDER_THIS . "`.`orderDocumentsCustomerNumber` = `tableCustomers`.`customersKundennummer`)

			WHERE 1
				AND `" . TABLE_ORDER_THIS . "`.`orderDocumentsID`= '" . $thisSearchString . "'
		";

		$rs = $dbConnection->db_query($sql);
		if(mysqli_error()){
			echo mysqli_error();

		}
		while($ds = mysqli_fetch_assoc($rs)) {
			foreach(array_keys($ds) as $field){
				if(preg_match("/^orderDocumentDetail/", $field)) {
					$arrDocumentDetailDatas[$ds["orderDocumentDetailID"]][$field] = $ds[$field];
				}
				else {
					$arrDocumentDatas[$field] = $ds[$field];
				}
			}
		}



		$content .= '<h2>'.($arrDocumentDatas["orderDocumentsSubject"]).'</h2>';

		$content .= '<table border="0" cellpadding="0" cellspacing="0" width="" class="noBorder">';
		$content .= '<tr>';
		$content .= '<td><b>Dokument-Nummer:</b></td>';
		$content .= '<td>'.($arrDocumentDatas["orderDocumentsNumber"]).'</td>';
		$content .= '</tr>';

		$content .= '<tr>';
		$content .= '<td><b>Vertreter:</b></td>';
		if($arrDocumentDatas["orderDocumentsSalesman"] == '0') {
			$thisOrderDocumentsSalesman = '-';
		}
		else {
			$thisOrderDocumentsSalesman = $arrDocumentDatas["salesmanFirmenname"] . ' (KNR: ' . $arrDocumentDatas["salesmanKundennummer"] . ')';
		}
		$content .= '<td>'.($thisOrderDocumentsSalesman).'</td>';
		$content .= '</tr>';
		$content .= '<tr>';
		$content .= '<td><b>Dokument-Empf&auml;nger:</b></td>';
		$content .= '<td>';
		$content .= ''.($arrDocumentDatas["orderDocumentsAddressCompany"]).'<br />';
		$content .= ''.($arrDocumentDatas["orderDocumentsAddressName"]).'<br />';
		$content .= ''.($arrDocumentDatas["orderDocumentsAddressStreet"]).' '.($arrDocumentDatas["orderDocumentsAddressStreetNumber"]).'<br />';
		$content .= ''.($arrDocumentDatas["orderDocumentsAddressZipcode"]).' '.($arrDocumentDatas["orderDocumentsAddressCity"]).'<br />';
		$content .= ''.($arrCountryTypeDatas[$arrDocumentDatas["orderDocumentsAddressCountry"]]["countries_name"]).'';
		$content .= '</td>';
		$content .= '</tr>';
		$content .= '<tr>';
		$content .= '<td><b>E-Mail:</b></td>';
		if($arrDocumentDatas["orderDocumentsAddressMail"] == '') {
			$arrDocumentDatas["orderDocumentsAddressMail"] = '-';
		}
		$content .= '<td>'.($arrDocumentDatas["orderDocumentsAddressMail"]).'</td>';
		$content .= '</tr>';
		$content .= '</table>';

		$content .= '<hr />';

		if($arrDocumentDatas["orderDocumentsAddressCompany2"] != ""){
			$content .= '<p class="infoArea">';
			$content .= $arrDocumentDatas["orderDocumentsAddressCompany2"];
			$content .= '<hr />';
			$content .= '</p>';
		}



		$content .= '<table border="0" cellpadding="0" cellspacing="0" width="100%" class="border">';
		$content .= '<tr>';
		$content .= '<th style="width:45px;text-align:right;">#</th>';
		$content .= '<th>Art.-Nr</th>';
		$content .= '<th>Artikel</th>';
		$content .= '<th>Menge</th>';
		$content .= '<th>Einzelpreis</th>';
		$content .= '<th>Gesamtpreis</th>';
		$content .= '</tr>';

		$count = 0;

		foreach($arrDocumentDetailDatas as $thisKey => $thisValue) {
			if($count%2 == 0){ $rowClass = 'row0'; }
			else { $rowClass = 'row1'; }

			$content .= '<tr class="'.$rowClass.'">';

			$content .= '<td style="text-align:right;">';
			$content .= '<b>' . ($count + 1) . '.</b>';
			$content .= '</td>';

			$content .= '<td>';
			$content .= $thisValue["orderDocumentDetailProductNumber"];
			$content .= '</td>';

			$content .= '<td>';
			$content .= $thisValue["orderDocumentDetailProductName"];

			if($thisValue["orderDocumentDetailKommission"] != ""){
				$content .= '<br /><span class="remarksArea"><b>Kommission:</b> ';
				$content .= $thisValue["orderDocumentDetailKommission"];
				$content .= '</span>';
			}
			if($thisValue["orderDocumentDetailPrintText"] != ""){
				$content .= '<br /><span class="remarksArea"><b>Aufdruck: </b>';
				$content .= $thisValue["orderDocumentDetailPrintText"];
				$content .= '</span>';
			}
			$content .= '</td>';

			$content .= '<td style="text-align:right;">';
			$content .= $thisValue["orderDocumentDetailProductQuantity"];
			$content .= '</td>';

			$content .= '<td style="text-align:right;">';
			$content .= convertDecimal($thisValue["orderDocumentDetailProductSinglePrice"], 'display') . ' &euro;';
			$content .= '</td>';

			$content .= '<td style="text-align:right;">';
			$content .= convertDecimal($thisValue["orderDocumentDetailProductTotalPrice"], 'display') . ' &euro;';
			$content .= '</td>';

			$content .= '</tr>';
			$count++;
		}
		$content .= '</table>';

		$content .= '<hr />';

		$content .= '<table align="right" border="0" cellpadding="0" cellspacing="0" width="" class="noBorder">';
		$content .= '<tr>';
		if($arrDocumentDatas["orderDocumentsDiscountType"] == 'fixed'){
			$thisDiscountPercentValue = '';
		}
		else {
			$thisDiscountPercentValue = '(' . convertDecimal($arrDocumentDatas["orderDocumentsDiscountPercent"]) . ' %)';
		}
		$content .= '<td><b>Rabatt '.$thisDiscountPercentValue.':</b></td>';
		$content .= '<td style="text-align:right;">'.convertDecimal($arrDocumentDatas["orderDocumentsDiscount"]).' &euro;</td>';
		$content .= '</tr>';
		$content .= '<tr>';
		$content .= '<td><b>Summe:</b></td>';
		$content .= '<td style="text-align:right;">'.convertDecimal($arrDocumentDatas["orderDocumentsSumPrice"]).' &euro;</td>';
		$content .= '</tr>';
		$content .= '<tr>';
		$content .= '<td><b>MwSt. (' . $arrDocumentDatas["orderDocumentsMwst"] . '%):</b></td>';
		$content .= '<td style="text-align:right;">'.convertDecimal($arrDocumentDatas["orderDocumentsMwstPrice"]).' &euro;</td>';
		$content .= '</tr>';
		$content .= '<tr>';
		$content .= '<td><b>Versandkosten:</b></td>';
		$content .= '<td style="text-align:right;">'.convertDecimal($arrDocumentDatas["orderDocumentsShippingCosts"]).' &euro;</td>';
		$content .= '</tr>';
		$content .= '<tr>';
		$content .= '<td><b>Verpackungskosten:</b></td>';
		$content .= '<td style="text-align:right;">'.convertDecimal($arrDocumentDatas["orderDocumentsPackagingCosts"]).' &euro;</td>';
		$content .= '</tr>';

		if($arrDocumentDatas["orderDocumentsPaymentType"] == 3){
			$content .= '<tr>';
			$content .= '<td><b>Nachnamegeb&uuml;hr:</b></td>';
			$content .= '<td style="text-align:right;">'.convertDecimal($arrDocumentDatas["orderDocumentsCashOnDelivery"]).' &euro;</td>';
			$content .= '</tr>';
		}

		$content .= '<tr>';
		$content .= '<td style="border-top: 1px solid #000000 !important;"><b>Gesamtpreis:</b></td>';
		$content .= '<td style="border-top: 1px solid #000000 !important; text-align:right;">'.convertDecimal($arrDocumentDatas["orderDocumentsTotalPrice"]).' &euro;</td>';
		$content .= '</tr>';


		$content .= '</table>';

		#$arrGetSupplier = explode("/", $products_model);
		#$thisSupplier = $arrSupplierTypeDatas[$arrGetSupplier[(count($arrGetSupplier) - 1)]]["supplierTypesName"];
		#$content .= $thisSupplier;

		if($dbConnection) {
			$dbConnection->db_close();
		}
	}
	$content = removeUnnecessaryChars($content);
	echo $content;
?>