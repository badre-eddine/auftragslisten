<?php
	require_once('../config/configMandator.inc.php');
	require_once('../config/configParams_' . strtolower(MANDATOR) . '.inc.php');
	require_once('../config/configParams.inc.php');
	require_once('../config/configBasic.inc.php');
	require_once('../config/configFiles.inc.php');
	require_once('../config/configTables.inc.php');
	require_once('../inc/functions.inc.php');
	require_once('header.inc.php');
	// $_SERVER["DOCUMENT_ROOT"]."/includes/configure.php");

	$thisSearchString = substr(trim($_GET["searchString"]), 0, 2);

	$content = "";
	if($thisSearchString != "") {
		$dbConnection = new DB_Connection();
		$db_open = $dbConnection->db_connect();

		// SELECT `customersID`, `customersKundennummer`, `customersRechnungsadressePLZ` FROM `bcw_customers` WHERE 1
		$sql = "SELECT
					(MAX(`customersKundennummer`) + 1) AS `searchResult`

					FROM `".TABLE_CUSTOMERS . "`

					WHERE `customersKundennummer` LIKE '".$thisSearchString."%'
		";
		$rs = $dbConnection->db_query($sql);

		list($searchResult) = mysqli_fetch_array($rs);

		$content = $searchResult;

		if($dbConnection->db_displayErrors() != "") {
			$errorMessage .= $dbConnection->db_displayErrors(). '<br />';
		}
		echo $content;
	}
?>