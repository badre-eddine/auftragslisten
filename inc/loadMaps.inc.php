<?php
	session_start();

	require_once('../config/configMandator.inc.php');
	require_once('../config/configParams_' . strtolower(MANDATOR) . '.inc.php');
	require_once('../config/configBasic.inc.php');
	require_once('../config/configFiles.inc.php');
	require_once('../config/configTables.inc.php');
	require_once('../inc/functions.inc.php');
	##require_once('header.inc.php');

	$thisSearchString = (trim($_GET["salesmanID"]));

	$content = '';

	$warningMessage = '';
	$errorMessage = '';
	$successMessage = '';
	$infoMessage = '';

	$useUf8Encode = false;

	if($thisSearchString != "") {

		$dbConnection = new DB_Connection(DB_HOST, '', DB_NAME, DB_USER, DB_PASSWORD);
		$dbOpen = $dbConnection->db_connect();

		$sql = "SELECT
					`" . TABLE_SALESMEN . "`.`salesmenID`,
					`" . TABLE_SALESMEN . "`.`salesmenKundennummer` AS `salesmenKundennummer2`,
					`" . TABLE_SALESMEN . "`.`salesmenFirmenname` AS `salesmenFirmenname2`,
					`" . TABLE_SALESMEN . "`.`salesmenAreas`,
					`" . TABLE_SALESMEN . "`.`salesmenTypesID`,
					`" . TABLE_SALESMEN . "`.`salesmenTypesName`,
					`" . TABLE_SALESMEN . "`.`salesmenActive`,

					`" . TABLE_CUSTOMERS . "`.`customersID`,
					`" . TABLE_CUSTOMERS . "`.`customersKundennummer` AS `salesmenKundennummer`,
					`" . TABLE_CUSTOMERS . "`.`customersMail1` AS `salesmenMail1`,
					`" . TABLE_CUSTOMERS . "`.`customersMail2` AS `salesmenMail2`,
					`" . TABLE_CUSTOMERS . "`.`customersTelefon1` AS `salesmenTelefon1`,
					`" . TABLE_CUSTOMERS . "`.`customersTelefon2` AS `salesmenTelefon2`,
					`" . TABLE_CUSTOMERS . "`.`customersFax1` AS `salesmenFax1`,
					`" . TABLE_CUSTOMERS . "`.`customersFax2` AS `salesmenFax2`,
					`" . TABLE_CUSTOMERS . "`.`customersMobil1` AS `salesmenMobile1`,
					`" . TABLE_CUSTOMERS . "`.`customersMobil2` AS `salesmenMobile2`,

					`" . TABLE_CUSTOMERS . "`.`customersFirmenname` AS `salesmenFirmenname`,
					`" . TABLE_CUSTOMERS . "`.`customersFirmenInhaberVorname` AS `salesmenFirmenInhaberVorname`,
					`" . TABLE_CUSTOMERS . "`.`customersFirmenInhaberNachname` AS `salesmenFirmenInhaberNachname`,
					`" . TABLE_CUSTOMERS . "`.`customersCompanyPLZ` AS `salesmenCompanyPLZ`,
					`" . TABLE_CUSTOMERS . "`.`customersCompanyOrt` AS `salesmenCompanyOrt`,
					`" . TABLE_CUSTOMERS . "`.`customersCompanyCountry` AS `salesmenCompanyCountry`,
					`" . TABLE_CUSTOMERS . "`.`customersTyp` AS `salesmenTyp`

					FROM `" . TABLE_SALESMEN . "`

					LEFT JOIN `" . TABLE_CUSTOMERS . "`
					ON (`" . TABLE_SALESMEN . "`.`salesmenKundennummer` = `" . TABLE_CUSTOMERS . "`.`customersKundennummer`)

					WHERE 1
						AND `" . TABLE_SALESMEN . "`.`salesmenAreas` != ''
						AND `" . TABLE_SALESMEN . "`.`salesmenActive` = '1'
				";
				$rs = $dbConnection->db_query($sql);

				$arrSalesmenDatas = array();
				$arrSalesmanAreas = array();
				while($ds = mysqli_fetch_assoc($rs)) {
					foreach(array_keys($ds) as $field) {
						$arrSalesmenDatas[$ds["salesmenID"]][$field] = $ds[$field];
					}
					$arrSalesmanAreas[$ds["salesmenID"]] = explode(";", $ds["salesmenAreas"]);
				}

		// BOF GET GOOGLE MAPS POINTS

			$arrSql = array();
			if(!empty($arrSalesmenDatas)) {
				foreach($arrSalesmenDatas as $thisKey => $thisValue) {
					$where = " AND 1 ";
					if(!empty($arrSalesmanAreas)) {
						$where .= " AND (";
						$arrWhere = array();
						foreach($arrSalesmanAreas[$thisKey] as $thisZipcode) {
							if(preg_match("/-/", $thisZipcode)) {
								$arrZipcodes = explode("-", $thisZipcode);
								$arrWhere[] = " (`" . TABLE_GEODB_TEXTDATA . "`.`text_val` BETWEEN " . $arrZipcodes[0] . str_repeat("0", (5 - strlen($arrZipcodes[0]))) . " AND " . $arrZipcodes[1] . str_repeat("9", (5 - strlen($arrZipcodes[1]))) . ") ";
							}
							else {
								$arrWhere[] = " `" . TABLE_GEODB_TEXTDATA . "`.`text_val` LIKE '" . $thisZipcode . "%' ";
							}
						}
						$where .= implode (" OR ", $arrWhere);
						$where .= ") ";
					}

					$arrSql[] = "
								SELECT
									`" . TABLE_GEODB_COORDINATES . "`.`lat` AS `latitude`,
									`" . TABLE_GEODB_COORDINATES . "`.`lon` AS `longitude`,

									'" . $thisKey . "' AS `salesmanID`,
									`" . TABLE_GEODB_TEXTDATA . "`.`text_val`,
									`geodb_textdata2`.`text_val` AS `text_val2`,
									`" . TABLE_ZIPCODES_CITIES . "`.`place_name`,
									`" . TABLE_ZIPCODES_CITIES . "`.`postal_code`,
									`" . TABLE_ZIPCODES_CITIES . "`.`firstOrderSubdivision_stateName` AS `region_1`,
									`" . TABLE_ZIPCODES_CITIES . "`.`secondOrderSubdivision_stateName` AS `region_2`,
									`" . TABLE_ZIPCODES_CITIES . "`.`thirdOrderSubdivision_stateName` AS `region_3`

								FROM `" . TABLE_GEODB_TEXTDATA . "`

								LEFT JOIN `" . TABLE_GEODB_TEXTDATA . "` AS `geodb_textdata2`
								ON(`" . TABLE_GEODB_TEXTDATA . "`.`loc_id` = `geodb_textdata2`.`loc_id`)

								LEFT JOIN `" . TABLE_GEODB_LOCATIONS . "`
								ON (`" . TABLE_GEODB_TEXTDATA . "`.`loc_id` = `" . TABLE_GEODB_LOCATIONS . "`.`loc_id`)


								LEFT JOIN `" . TABLE_GEODB_COORDINATES . "`
								ON(`" . TABLE_GEODB_TEXTDATA . "`.`loc_id` = `" . TABLE_GEODB_COORDINATES . "`.`loc_id`)

								LEFT JOIN `" . TABLE_ZIPCODES_CITIES . "`
								ON(`" . TABLE_GEODB_TEXTDATA . "`.`text_val` = `" . TABLE_ZIPCODES_CITIES . "`.`postal_code`)

								WHERE `" . TABLE_GEODB_TEXTDATA . "`.`text_type` = '500300000'
									AND `geodb_textdata2`.`text_type` = '500100000'
									" . $where . "

								GROUP BY CONCAT(`latitude`, '-', `longitude`)

								LIMIT 30
						";
				}
			}
			$sql = implode(' UNION ', $arrSql);


			$rs = $dbConnection->db_query($sql);

			$strGoogleMapPoints = "";
			$arrGoogleMapZipcodeAreas = array();
			$countPoints = 1;
			while($ds = mysqli_fetch_assoc($rs)) {
				$arrPoints[] = array("latitude" => $ds["latitude"], "longitude" => $ds["longitude"], "text_val" =>  $ds["text_val"], "placeName" =>  $ds["place_name"]);

				// $arrGoogleMapZipcodeAreas[$salesmenDatas["salesmenFirmenname"]][] = 'new google.maps.LatLng(' . $ds["latitude"] . ', ' . $ds["longitude"] . ')';

				$strGoogleMapPoints .= 	'myPointDatas[' . $countPoints . '] = new Array();' . "\n";
				$strGoogleMapPoints .= 	'myPointDatas[' . $countPoints . ']["latitude"] = "' . $ds["latitude"] . '";' . "\n";
				$strGoogleMapPoints .= 	'myPointDatas[' . $countPoints . ']["longitude"] = "' . $ds["longitude"] . '";' . "\n";
				$strGoogleMapPoints .= 	'myPointDatas[' . $countPoints . ']["title"] = "' . $ds["text_val"] . ' ' . $ds["place_name"];
				if($ds["text_val2"] != $ds["place_name"]) { $strGoogleMapPoints .= ' / ' . $ds["text_val2"]; }
				$strGoogleMapPoints .= 	'";' . "\n";
				$strGoogleMapPoints .= 	'myPointDatas[' . $countPoints . ']["header"] = "' . $ds["text_val"] . ' ' . $ds["place_name"];
				if($ds["text_val2"] != $ds["place_name"]) { $strGoogleMapPoints .= ' / ' . $ds["text_val2"]; }
				$strGoogleMapPoints .= 	'";' . "\n";
				$strGoogleMapPoints .= 	'myPointDatas[' . $countPoints . ']["text"] = "<b>Vertreter:</b> ' . $salesmenDatas["salesmenFirmenname"] . ' (' . $salesmenDatas["salesmenKundennummer"] . ')";' . "\n";
				$strGoogleMapPoints .= 	'myPointDatas[' . $countPoints . ']["city"] = "<b>PLZ:</b> ' . $ds["text_val"] . '";' . "\n";
				$strGoogleMapPoints .= 	'myPointDatas[' . $countPoints . ']["region_1"] = "<b>Bundesland:</b> ' . $ds["region_1"] . '";' . "\n";
				$strGoogleMapPoints .= 	'myPointDatas[' . $countPoints . ']["region_2"] = "<b>Region:</b> ' . $ds["region_2"] . '";' . "\n";
				$strGoogleMapPoints .= 	'myPointDatas[' . $countPoints . ']["region_3"] = "<b>Kreis:</b> ' . $ds["region_3"] . '";' . "\n";
				$strGoogleMapPoints .= 	'myPointDatas[' . $countPoints . ']["imagePath"] = "";' . "\n";
				$countPoints ++;
			}
		// EOF GET GOOGLE MAPS POINTS

		// BOF GET GOOGLE MAPS AREAS
			$sql = "SELECT
						`lat` AS `latitude`,
						`lon` AS `longitude`,
						`country`,
						`subcountry`,
						IF(`subcountry` != '' , CONCAT(`country`, '_', `subcountry`), `country`) AS `area`

						FROM `" . TABLE_GEODB_AREAS . "`

						WHERE 1 AND `subcountry` = '' AND `country` = 'DE'
			";

			$rs = $dbConnection->db_query($sql);

			$arrGoogleMapAreas = array();
			while($ds = mysqli_fetch_assoc($rs)) {
				$arrGoogleMapAreas[$ds["area"]][] = 'new google.maps.LatLng(' . $ds["latitude"] . ', ' . $ds["longitude"] . ')';
			}
		// ROF GET GOOGLE MAPS AREAS

		$rs = $dbConnection->db_query($sql);

		if($dbConnection) {
			$dbConnection->db_close();
		}

		$content .= '<div id="myGoogleMapsArea">
						<div id="myGmapCanvas" style="width:900px;height:600px;"> </div>
					</div>
		';

		$content .= '<!-- GOOGLE MAPS START -->';
		if($strGoogleMapPoints != ""){
			// $content .= '<script type="text/javascript" src="http://maps.google.com/maps/api/js?sensor=true"></script>';
			$content .= '<script type="text/javascript" language="javascript">';
			$content .= 'try {';
			$content .= 'function loadScript() {
							var script = document.createElement("script");
							script.type = "text/javascript";
							script.src = "http://maps.google.com/maps/api/js?sensor=true&callback=initialize";
							document.body.appendChild(script);
						}
				';

			$content .= 'function setGmapMarkers(map, i) {
				var thisMarkerImage;
				if(i == 0) {
					thisMarkerImage = "bctr_googleMarkerFlag.png";
					thisMarkerShadowImage = "bctr_googleMarkerFlagShadow.png";
					thisMarkerSize = new Array(81, 84);
					thisMarkerShadowSize = new Array(81, 84);
					thisZindex = 999999;
				}
				else {
					thisMarkerImage = "googleMapsIcon.png";
					thisMarkerShadowImage = "googleMapsIconShadow.png";
					thisMarkerSize = new Array(28, 20);
					thisMarkerShadowSize = new Array(28, 20);
					thisZindex = i;
				}

				var image = new google.maps.MarkerImage(
							"layout/icons/" + thisMarkerImage,
							new google.maps.Size(thisMarkerSize[0], thisMarkerSize[1]),
							new google.maps.Point(0,0),
							new google.maps.Point(0, thisMarkerSize[1])
				);

				var shadow = new google.maps.MarkerImage(
							"layout/icons/" + thisMarkerShadowImage,
							new google.maps.Size(thisMarkerShadowSize[0], thisMarkerShadowSize[1]),
							new google.maps.Point(0, 0),
							new google.maps.Point(0, thisMarkerShadowSize[1])
				);

				var shape = {
					/* coord: [1, (thisMarkerSize[1] + 1 - 20), 1, thisMarkerSize[1], thisMarkerSize[0], thisMarkerSize[1], thisMarkerSize[0] , (thisMarkerSize[1] + 1 - 20)], */
					coord: [1, 1, 1, 28, 20, 28, 20 , 1],
					type: "poly"
				};

				var myLatLng = new google.maps.LatLng(myPointDatas[i]["latitude"], myPointDatas[i]["longitude"]);
				marker[i] = new google.maps.Marker({
					position: myLatLng,
					map: map,
					shadow: shadow,
					icon: image,
					shape: shape,
					title: myPointDatas[i]["title"],
					zIndex: thisZindex
				});

				/* DEFINE INFO WINDOW     */
					var html		= new Array();
					var imageExists	= false;
					html[i] = "";
					html[i] += "<div class=\"myGmapInfo\">";
					/* html[i] += "  <p class=\'headline\'>" + myPointDatas[i]["title"] + "</p>"; */
					html[i] += "  <p class=\'headline\'>" + myPointDatas[i]["header"] + "</p>";
					if(myPointDatas[i]["imagePath"] != "") {
					  imageExists = true;
					  html[i] += \'  <img src="\' + myPointDatas[i]["imagePath"] + \'" + myPointDatas[i]["imageDimension"] + alt="\' + myPointDatas[i]["title"] + \'"/>\';
					}
					if(myPointDatas[i]["text"] != "") {
						/* html[i] += "  <p class=\'text\'>" + myPointDatas[i]["text"].replace(/, /g, ",<br />") + "</p>"; */
						html[i] += "  <p class=\'text\'>" + myPointDatas[i]["text"] + "</p>";
					}
					html[i] += "<div class=\'clear\'></div>";

					if(myPointDatas[i]["city"] != "") {
					  html[i] += "  <span class=\'text\'>" + myPointDatas[i]["city"] + "</span><br />";
					}

					if(myPointDatas[i]["region_1"] != "") {
					  html[i] += "  <span class=\'text\'>" + myPointDatas[i]["region_1"] + "</span><br />";
					}

					if(myPointDatas[i]["region_2"] != "") {
					  html[i] += "  <span class=\'text\'>" + myPointDatas[i]["region_2"] + "</span><br />";
					}

					if(myPointDatas[i]["region_3"] != "") {
					  html[i] += "  <span class=\'text\'>" + myPointDatas[i]["region_3"] + "</span><br />";
					}


					/* html[i] += "  <p class=\'text\'>"; */
					/* html[i] += "    <b>Longitude: </b>" + myPointDatas[i]["longitude"]; */
					/* html[i] += "    <br />"; */
					/* html[i] += "    <b>Latitude: </b>" + myPointDatas[i]["latitude"]; */
					/* html[i] += "  </p>"; */

					html[i] += "<div class=\'clear\'></div>";
					html[i] += "</div>";

					infowindow[i] = new google.maps.InfoWindow({
						content: html[i]
					});
				/* END DEFINE INFO WINDOW */

				/* SHOW INFO WINDOW	 */
					google.maps.event.addListener(marker[i], "click", function() {
						if(activeMarker > -1) {
							infowindow[activeMarker].close();
						}
						infowindow[i].open(map,marker[i]);
						activeMarker = i;
					});
					/* createMarkerLinkInGmapSidebar(i, myPointDatas[i]["title"], marker[i], imageExists); */
				/* END SHOW INFO WINDOW	 */
				}
			';

			$content .= '
				/* BOF DEFINE DEFAULT VALUES				 */
					var myPointDatas	= new Array();

					var elementId = "myGmapCanvas";
					var default_arrayKey	= 0;
					var default_lat			= "52.103138";
					var default_lon			= "7.622817";
					var default_zoom		= 5;

					var sidebarHtml		= "";              			/* HTML-Code für die gesamte Sidebar */
					var sidebarMarkers	= [];              			/* Array für die Marker */
					var sidebarCount	= 0;               			/* Zähler für die Marker (= Zeile in der Sidebar) */
					var marker			= new Array();
					var activeMarker	= -1;
					var infowindow		= new Array();
				/* EOF DEFINE DEFAULT VALUES			 */

				/* DEFINE POINT DATAS */
					myPointDatas[0] = new Array();
					myPointDatas[0]["latitude"] = default_lat;
					myPointDatas[0]["longitude"] = default_lon;
					myPointDatas[0]["title"] = "BURHAN CTR";
					myPointDatas[0]["header"] = "BURHAN CTR";
					myPointDatas[0]["text"] = "";
					myPointDatas[0]["city"] = "";
					myPointDatas[0]["region_1"] = "";
					myPointDatas[0]["region_2"] = "";
					myPointDatas[0]["region_3"] = "";
					myPointDatas[0]["imagePath"] = "";
			';

			if($strGoogleMapPoints != '') {
				$content .= $strGoogleMapPoints;
			}

			$content .= '
				/* END DEFINE POINT DATAS */

				/* BOF POLYGON DATAS */
			';

			if(!empty($arrGoogleMapAreas)){
				foreach($arrGoogleMapAreas as $thisKey => $thisValue) {
					$content .= 'var coords_' . $thisKey . ' = [' . implode(", ", $thisValue) . '];';
				}
			}

			if(!empty($arrGoogleMapZipcodeAreas)){
				foreach($arrGoogleMapZipcodeAreas as $thisKey => $thisValue) {
					$content .= 'var coords_' . $thisKey . ' = [' . implode(", ", $thisValue) . '];';
				}
			}
		$content .= '
			/* EOF POLYGON DATAS */

			/* BOF CREATE GOOGLE MAP */
				function initialize() {
					if (!document.getElementById(elementId)) {
						alert("Fehler: das Element mit der id "+ elementId+ " konnte nicht auf dieser Webseite gefunden werden!");
						return false;
					}
					else {

					}
				}
			';

		if($strGoogleMapPoints != '') {
			$content .= 'var latlng = new google.maps.LatLng(myPointDatas[default_arrayKey]["latitude"], myPointDatas[default_arrayKey]["longitude"]);';
		}

		$content .= '
			var myOptions = {
				zoom: default_zoom,
				center: latlng,
				panControl: true,
				zoomControl: true,
				navigationControl: true,
				mapTypeControl: true,
				scaleControl: true,
				overviewMapControl: true,
				streetViewControl: true,
				mapTypeId: google.maps.MapTypeId.TERRAIN
				/* ROADMAP zeigt die normalen Standard-2D-Kacheln von Google Maps an. */
				/* SATELLITE zeigt Fotokacheln an. */
				/* HYBRID zeigt eine Mischung aus Fotokacheln und eine Kachelebene für markante Merkmale an (Straßen, Ortsnamen). */
				/* TERRAIN zeigt Reliefkacheln für Erhebungen und GewÃ¤sser (Berge, Flüsse usw.) an. */
			};
			var map = new google.maps.Map(document.getElementById(elementId), myOptions);
		';


		if(!empty($arrGoogleMapAreas)){
			foreach($arrGoogleMapAreas as $thisKey => $thisValue) {
				$content .= 'var area_' . $thisKey . ' = new google.maps.Polygon({
						paths: coords_' . $thisKey . ',
						strokeColor: "#FF0000",
						strokeOpacity: 0.8,
						strokeWeight: 2,
						fillColor: "#FFFF00",
						fillOpacity: 0.1
						});
					area_' . $thisKey . '.setMap(map);
				';
			}
		}

		if(!empty($arrGoogleMapZipcodeAreas)){
			foreach($arrGoogleMapZipcodeAreas as $thisKey => $thisValue) {
				$content .= 'var area_' . $thisKey . ' = new google.maps.Polygon({
						paths: coords_' . $thisKey . ',
						strokeColor: "#00FF00",
						strokeOpacity: 0.8,
						strokeWeight: 2,
						fillColor: "transparent",
						fillOpacity: 0.1
						});
					area_' . $thisKey . '.setMap(map);
				';
			}
		}

		if($strGoogleMapPoints != '') {
			$content .= '
				if(myPointDatas.length > 0) {
					for(i = 0 ; i < myPointDatas.length ; i++) {
						setGmapMarkers(map, i);
					}
				}
			';
		}

		$content .= 'setGmapMarkers(map, i);';



		$content .= '} catch(err) { alert(err); } finally {} ';
		$content .= '
			$(document).ready(function() {
				loadScript();
				initialize();
			});
			</script>
			<!-- GOOGLE MAPS END -->
		';
		}
	}
	// $content = removeUnnecessaryChars($content);
	// echo htmlentities(utf8_decode($content));
	// $content = preg_replace('/{/', '###OPEN_BRACKET###', $content);
	// $content = preg_replace('/}/', '###CLOSE_BRACKET###', $content);
	// $content = addslashes(utf8_encode($content));
	$content = (utf8_decode($content));

	echo $content;
?>