<?php
	session_start();

	require_once('../config/configMandator.inc.php');
	require_once('../config/configParams_' . strtolower(MANDATOR) . '.inc.php');
	require_once('../config/configBasic.inc.php');
	require_once('../config/configFiles.inc.php');
	require_once('../config/configTables.inc.php');
	require_once('../inc/functions.inc.php');
	##require_once('header.inc.php');

	$content = '';

	$objCompareData = ($_GET["objCompareData"]);
	$objCompareData = urldecode($objCompareData);
	$jsonCompareData = json_decode($objCompareData);

	$arrCompareData = array();

	if(!empty($jsonCompareData)){
		foreach($jsonCompareData as $thisJsonKey => $thisJsonValue){
			$arrCompareData[$thisJsonKey] = trim($thisJsonValue);
		}
	}

	DEFINE('CHECK_GEO_LOCATION', true);

	$arrSearchResults = array();

	$warningMessage = '';
	$errorMessage = '';
	$successMessage = '';
	$infoMessage = '';

	if(!empty($arrCompareData)){
		/*
		searchCompanyName =>
		searchCompanyZipcode => 12345
		searchCompanyStreet =>
		searchCompanyMail1 =>
		searchCompanyMail2 =>
		searchCompanyPhone1 =>
		searchCompanyPhone2 =>
		searchCompanyFax1 =>
		searchCompanyFax2 =>
		searchCompanyMobil1 =>
		searchCompanyMobil2 =>
		*/

		if(
			(($arrCompareData["searchCompanyZipcode"]) != '' && ($arrCompareData["searchCompanyStreet"]) != '')
			|| (($arrCompareData["searchCompanyMail1"]) != '' || ($arrCompareData["searchCompanyMail2"]) != '')
			|| (($arrCompareData["searchCompanyPhone1"]) != '' || ($arrCompareData["searchCompanyPhone2"]) != '' || ($arrCompareData["searchCompanyFax1"]) != '' || ($arrCompareData["searchCompanyFax2"]) != '' || ($arrCompareData["searchCompanyMobil1"]) != '' || ($arrCompareData["searchCompanyMobil2"]) != '')
		) {

			$dbConnection = new DB_Connection();
			$db_open = $dbConnection->db_connect();

			// BOF READ COUNTRIES
				$arrCountryTypeDatas = getCountryTypes();
			// EOF READ COUNTRIES

			$where = "";

			// BOF COMPARE ZIPCODE AND STREET
				if($arrCompareData["searchCompanyZipcode"] != '' && ($arrCompareData["searchCompanyStreet"]) != ''){
					$where .= "
						(
							(
								`customersCompanyPLZ` = '" . $arrCompareData["searchCompanyZipcode"] . "'
								OR
								`customersCompanyOrt` = '" . $arrCompareData["searchCompanyCity"] . "'
							)
							AND (
								REPLACE(CONCAT(`customersCompanyStrasse`, `customersCompanyHausnummer`), ' ', '') = '" . preg_replace("/ /", "", formatStreet($arrCompareData["searchCompanyStreet"]) . $arrCompareData["searchCompanyStreetNumber"]) . "'
								OR
								SOUNDEX(REPLACE(CONCAT(`customersCompanyStrasse`, `customersCompanyHausnummer`), ' ', '')) = SOUNDEX('" . preg_replace("/ /", "", formatStreet($arrCompareData["searchCompanyStreet"]) . $arrCompareData["searchCompanyStreetNumber"]) . "')
							)
						)
					";
				}
			// EOF COMPARE ZIPCODE AND STREET

			// BOF COMPARE MAIL ADRESSES
				if(($arrCompareData["searchCompanyMail1"]) != '' || ($arrCompareData["searchCompanyMail2"]) != ''){
					if($where != ''){
						$where .= " OR ";
					}

					$where .= " ( ";
					if(($arrCompareData["searchCompanyMail1"]) != ''){
						$where .= "
								`customersMail1` = '" . $arrCompareData["searchCompanyMail1"] . "'
								OR
								`customersMail2` = '" . $arrCompareData["searchCompanyMail1"] . "'
							";
					}
					if(($arrCompareData["searchCompanyMail1"]) != '' && ($arrCompareData["searchCompanyMail2"]) != ''){
						$where .= " OR ";
					}

					if(($arrCompareData["searchCompanyMail2"]) != ''){
						$where .= "
								`customersMail1` = '" . $arrCompareData["searchCompanyMail2"] . "'
								OR
								`customersMail2` = '" . $arrCompareData["searchCompanyMail2"] . "'
							";
					}
					$where .= " ) ";
				}
			// EOF COMPARE MAIL ADRESSES

			// BOF COMPARE PHONE NUMBERS
				if(($arrCompareData["searchCompanyPhone1"]) != '' || ($arrCompareData["searchCompanyPhone2"]) != '' || ($arrCompareData["searchCompanyFax1"]) != '' || ($arrCompareData["searchCompanyFax2"]) != '' || ($arrCompareData["searchCompanyMobil1"]) != '' || ($arrCompareData["searchCompanyMobil2"]) != ''){
					if($where != ''){
						$where .= " OR ";
					}
					$where .= " ( ";

					$arrWherePhone = array();

					if($arrCompareData["searchCompanyPhone1"] != ''){
						$arrWherePhone[] = "
									`customersTelefon1` = '" . cleanPhoneNumbers($arrCompareData["searchCompanyPhone1"], $arrCompareData["searchCompanyCountry"]) . "'
									OR
									`customersTelefon2` = '" . cleanPhoneNumbers($arrCompareData["searchCompanyPhone1"], $arrCompareData["searchCompanyCountry"]) . "'
									OR
									`customersMobil1` = '" . cleanPhoneNumbers($arrCompareData["searchCompanyPhone1"], $arrCompareData["searchCompanyCountry"]) . "'
									OR
									`customersMobil2` = '" . cleanPhoneNumbers($arrCompareData["searchCompanyPhone1"], $arrCompareData["searchCompanyCountry"]) . "'
									OR
									`customersFax1` = '" . cleanPhoneNumbers($arrCompareData["searchCompanyPhone1"], $arrCompareData["searchCompanyCountry"]) . "'
									OR
									`customersFax2` = '" . cleanPhoneNumbers($arrCompareData["searchCompanyPhone1"], $arrCompareData["searchCompanyCountry"]) . "'

									OR
									`customersTelefon1` = '" . formatPhoneNumber($arrCompareData["searchCompanyPhone1"]) . "'
									OR
									`customersTelefon2` = '" . formatPhoneNumber($arrCompareData["searchCompanyPhone1"]) . "'
									OR
									`customersMobil1` = '" . formatPhoneNumber($arrCompareData["searchCompanyPhone1"]) . "'
									OR
									`customersMobil2` = '" . formatPhoneNumber($arrCompareData["searchCompanyPhone1"]) . "'
									OR
									`customersFax1` = '" . formatPhoneNumber($arrCompareData["searchCompanyPhone1"]) . "'
									OR
									`customersFax2` = '" . formatPhoneNumber($arrCompareData["searchCompanyPhone1"]) . "'
							";
					}
					if($arrCompareData["searchCompanyPhone2"] != ''){
						$arrWherePhone[] = "
									`customersTelefon1` = '" . cleanPhoneNumbers($arrCompareData["searchCompanyPhone2"], $arrCompareData["searchCompanyCountry"]) . "'
									OR
									`customersTelefon2` = '" . cleanPhoneNumbers($arrCompareData["searchCompanyPhone2"], $arrCompareData["searchCompanyCountry"]) . "'
									OR
									`customersMobil1` = '" . cleanPhoneNumbers($arrCompareData["searchCompanyPhone2"], $arrCompareData["searchCompanyCountry"]) . "'
									OR
									`customersMobil2` = '" . cleanPhoneNumbers($arrCompareData["searchCompanyPhone2"], $arrCompareData["searchCompanyCountry"]) . "'
									OR
									`customersFax1` = '" . cleanPhoneNumbers($arrCompareData["searchCompanyPhone2"], $arrCompareData["searchCompanyCountry"]) . "'
									OR
									`customersFax2` = '" . cleanPhoneNumbers($arrCompareData["searchCompanyPhone2"], $arrCompareData["searchCompanyCountry"]) . "'

									OR
									`customersTelefon1` = '" . formatPhoneNumber($arrCompareData["searchCompanyPhone2"]) . "'
									OR
									`customersTelefon2` = '" . formatPhoneNumber($arrCompareData["searchCompanyPhone2"]) . "'
									OR
									`customersMobil1` = '" . formatPhoneNumber($arrCompareData["searchCompanyPhone2"]) . "'
									OR
									`customersMobil2` = '" . formatPhoneNumber($arrCompareData["searchCompanyPhone2"]) . "'
									OR
									`customersFax1` = '" . formatPhoneNumber($arrCompareData["searchCompanyPhone2"]) . "'
									OR
									`customersFax2` = '" . formatPhoneNumber($arrCompareData["searchCompanyPhone2"]) . "'
							";
					}
					if($arrCompareData["searchCompanyFax1"] != ''){
						$arrWherePhone[] = "
									`customersTelefon1` = '" . cleanPhoneNumbers($arrCompareData["searchCompanyFax1"], $arrCompareData["searchCompanyCountry"]) . "'
									OR
									`customersTelefon2` = '" . cleanPhoneNumbers($arrCompareData["searchCompanyFax1"], $arrCompareData["searchCompanyCountry"]) . "'
									OR
									`customersMobil1` = '" . cleanPhoneNumbers($arrCompareData["searchCompanyFax1"], $arrCompareData["searchCompanyCountry"]) . "'
									OR
									`customersMobil2` = '" . cleanPhoneNumbers($arrCompareData["searchCompanyFax1"], $arrCompareData["searchCompanyCountry"]) . "'
									OR
									`customersFax1` = '" . cleanPhoneNumbers($arrCompareData["searchCompanyFax1"], $arrCompareData["searchCompanyCountry"]) . "'
									OR
									`customersFax2` = '" . cleanPhoneNumbers($arrCompareData["searchCompanyFax1"], $arrCompareData["searchCompanyCountry"]) . "'

									OR
									`customersTelefon1` = '" . formatPhoneNumber($arrCompareData["searchCompanyFax1"]) . "'
									OR
									`customersTelefon2` = '" . formatPhoneNumber($arrCompareData["searchCompanyFax1"]) . "'
									OR
									`customersMobil1` = '" . formatPhoneNumber($arrCompareData["searchCompanyFax1"]) . "'
									OR
									`customersMobil2` = '" . formatPhoneNumber($arrCompareData["searchCompanyFax1"]) . "'
									OR
									`customersFax1` = '" . formatPhoneNumber($arrCompareData["searchCompanyFax1"]) . "'
									OR
									`customersFax2` = '" . formatPhoneNumber($arrCompareData["searchCompanyFax1"]) . "'
							";
					}
					if($arrCompareData["searchCompanyFax2"] != ''){
						$arrWherePhone[] = "
									`customersTelefon1` = '" . cleanPhoneNumbers($arrCompareData["searchCompanyFax2"], $arrCompareData["searchCompanyCountry"]) . "'
									OR
									`customersTelefon2` = '" . cleanPhoneNumbers($arrCompareData["searchCompanyFax2"], $arrCompareData["searchCompanyCountry"]) . "'
									OR
									`customersMobil1` = '" . cleanPhoneNumbers($arrCompareData["searchCompanyFax2"], $arrCompareData["searchCompanyCountry"]) . "'
									OR
									`customersMobil2` = '" . cleanPhoneNumbers($arrCompareData["searchCompanyFax2"], $arrCompareData["searchCompanyCountry"]) . "'
									OR
									`customersFax1` = '" . cleanPhoneNumbers($arrCompareData["searchCompanyFax2"], $arrCompareData["searchCompanyCountry"]) . "'
									OR
									`customersFax2` = '" . cleanPhoneNumbers($arrCompareData["searchCompanyFax2"], $arrCompareData["searchCompanyCountry"]) . "'

									OR
									`customersTelefon1` = '" . formatPhoneNumber($arrCompareData["searchCompanyFax2"]) . "'
									OR
									`customersTelefon2` = '" . formatPhoneNumber($arrCompareData["searchCompanyFax2"]) . "'
									OR
									`customersMobil1` = '" . formatPhoneNumber($arrCompareData["searchCompanyFax2"]) . "'
									OR
									`customersMobil2` = '" . formatPhoneNumber($arrCompareData["searchCompanyFax2"]) . "'
									OR
									`customersFax1` = '" . formatPhoneNumber($arrCompareData["searchCompanyFax2"]) . "'
									OR
									`customersFax2` = '" . formatPhoneNumber($arrCompareData["searchCompanyFax2"]) . "'
							";
					}
					if($arrCompareData["searchCompanyMobil1"] != ''){
						$arrWherePhone[] = "
									`customersTelefon1` = '" . cleanPhoneNumbers($arrCompareData["searchCompanyMobil1"], $arrCompareData["searchCompanyCountry"]) . "'
									OR
									`customersTelefon2` = '" . cleanPhoneNumbers($arrCompareData["searchCompanyMobil1"], $arrCompareData["searchCompanyCountry"]) . "'
									OR
									`customersMobil1` = '" . cleanPhoneNumbers($arrCompareData["searchCompanyMobil1"], $arrCompareData["searchCompanyCountry"]) . "'
									OR
									`customersMobil2` = '" . cleanPhoneNumbers($arrCompareData["searchCompanyMobil1"], $arrCompareData["searchCompanyCountry"]) . "'
									OR
									`customersFax1` = '" . cleanPhoneNumbers($arrCompareData["searchCompanyMobil1"], $arrCompareData["searchCompanyCountry"]) . "'
									OR
									`customersFax2` = '" . cleanPhoneNumbers($arrCompareData["searchCompanyMobil1"], $arrCompareData["searchCompanyCountry"]) . "'

									OR
									`customersTelefon1` = '" . formatPhoneNumber($arrCompareData["searchCompanyMobil1"]) . "'
									OR
									`customersTelefon2` = '" . formatPhoneNumber($arrCompareData["searchCompanyMobil1"]) . "'
									OR
									`customersMobil1` = '" . formatPhoneNumber($arrCompareData["searchCompanyMobil1"]) . "'
									OR
									`customersMobil2` = '" . formatPhoneNumber($arrCompareData["searchCompanyMobil1"]) . "'
									OR
									`customersFax1` = '" . formatPhoneNumber($arrCompareData["searchCompanyMobil1"]) . "'
									OR
									`customersFax2` = '" . formatPhoneNumber($arrCompareData["searchCompanyMobil1"]) . "'
							";
					}
					if($arrCompareData["searchCompanyMobil2"] != ''){
						$arrWherePhone[] = "
									`customersTelefon1` = '" . cleanPhoneNumbers($arrCompareData["searchCompanyMobil2"], $arrCompareData["searchCompanyCountry"]) . "'
									OR
									`customersTelefon2` = '" . cleanPhoneNumbers($arrCompareData["searchCompanyMobil2"], $arrCompareData["searchCompanyCountry"]) . "'
									OR
									`customersMobil1` = '" . cleanPhoneNumbers($arrCompareData["searchCompanyMobil2"], $arrCompareData["searchCompanyCountry"]) . "'
									OR
									`customersMobil2` = '" . cleanPhoneNumbers($arrCompareData["searchCompanyMobil2"], $arrCompareData["searchCompanyCountry"]) . "'
									OR
									`customersFax1` = '" . cleanPhoneNumbers($arrCompareData["searchCompanyMobil2"], $arrCompareData["searchCompanyCountry"]) . "'
									OR
									`customersFax2` = '" . cleanPhoneNumbers($arrCompareData["searchCompanyMobil2"], $arrCompareData["searchCompanyCountry"]) . "'

									OR
									`customersTelefon1` = '" . formatPhoneNumber($arrCompareData["searchCompanyMobil2"]) . "'
									OR
									`customersTelefon2` = '" . formatPhoneNumber($arrCompareData["searchCompanyMobil2"]) . "'
									OR
									`customersMobil1` = '" . formatPhoneNumber($arrCompareData["searchCompanyMobil2"]) . "'
									OR
									`customersMobil2` = '" . formatPhoneNumber($arrCompareData["searchCompanyMobil2"]) . "'
									OR
									`customersFax1` = '" . formatPhoneNumber($arrCompareData["searchCompanyMobil2"]) . "'
									OR
									`customersFax2` = '" . formatPhoneNumber($arrCompareData["searchCompanyMobil2"]) . "'
							";
					}

					if(!empty($arrWherePhone)){
						$where .= implode(" OR ", $arrWherePhone);
					}

					$where .= " ) ";
				}
			// EOF COMPARE PHONE NUMBERS

			// BOF COMPARE GEO LOCATIONS
				if(CHECK_GEO_LOCATION == true){
					$arrCheckGeoLocationDatas = array();
					$arrCheckGeoLocationDatas["customersCompanyPLZ"]		= $arrCompareData["searchCompanyZipcode"];
					$arrCheckGeoLocationDatas["customersCompanyOrt"]		= $arrCompareData["searchCompanyCity"];
					$arrCheckGeoLocationDatas["customersCompanyStrasse"]	= formatStreet($arrCompareData["searchCompanyStreet"]);
					$arrCheckGeoLocationDatas["customersCompanyHausnummer"] = $arrCompareData["searchCompanyStreetNumber"];
					$arrCheckGeoLocationDatas["customersCompanyCountry"]	= $arrCountryTypeDatas[$arrCompareData["searchCompanyCountry"]]["countries_name"];

					$arrLocationDatas = getGoogleLocationDatas($arrCheckGeoLocationDatas);

					if($arrLocationDatas["LAT"] != "" && $arrLocationDatas["LNG"] != ""){
						$arrCompareData["searchCompanyLocation"] = $arrLocationDatas["LAT"] . ":" . $arrLocationDatas["LNG"];
						$where .= " OR `customersCompanyLocation` = '" . $arrCompareData["searchCompanyLocation"] . "' ";
					}
				}
			// EOF COMPARE GEO LOCATIONS

			$sql = "
				SELECT
					`customersID`,
					`customersKundennummer`,
					`customersFirmenname`,
					`customersFirmennameZusatz`,

					`customersTelefon1`,
					`customersTelefon2`,
					`customersMobil1`,
					`customersMobil2`,
					`customersFax1`,
					`customersFax2`,
					`customersMail1`,
					`customersMail2`,
					`customersHomepage`,
					CONCAT(`customersCompanyStrasse`, ' ', `customersCompanyHausnummer`) AS `customersCompanyStrasse`,
					`customersCompanyCountry`,
					`customersCompanyPLZ`,
					`customersCompanyOrt`,

					`customersCompanyLocation`

					FROM `" . TABLE_CUSTOMERS . "`

					WHERE 1
						AND (" . $where . ")

			";

			$rs = $dbConnection->db_query($sql);
			$countResults = mysqli_num_rows($rs);

			if($countResults > 0){
				$content .= '<p class="warningArea">';
				$content .= 'Ist der Neukunde eventuell schon vorhanden? Bitte pr&uuml;fen!';
				$content .= '</p>';
				$content .= '<div style="height:400px;overflow:auto;">';
				$content .= '<table border="0" cellpadding="0" cellspacing="0" width="90%" class="displayOrders">';

				$content .= '<tr>';
				$content .= '<th>#</th>';
				$content .= '<th>KNR</th>';
				$content .= '<th>NAME</th>';
				$content .= '<th>PLZ</th>';
				$content .= '<th>ORT</th>';
				$content .= '<th>LAND</th>';
				$content .= '<th>STR.</th>';
				$content .= '<th>MAIL</th>';
				$content .= '<th>GEO</th>';
				$content .= '</tr>';

				$countRow = 0;
				while($ds = mysqli_fetch_assoc($rs)) {
					if($countRow%2 == 0){ $rowClass = 'row0'; }
					else { $rowClass = 'row1'; }

					$content .= '<tr class="'.$rowClass.'">';

					$content .= '<td>' . ($countRow + 1). '</td>';
					$content .= '<td><b><a href="' . PAGE_EDIT_CUSTOMER . '?searchCustomerNumber=' . $ds["customersKundennummer"] . '">' . $ds["customersKundennummer"]. '</b></td>';
					$content .= '<td>' . $ds["customersFirmenname"]. ' - ' . $ds["customersFirmennameZusatz"]. '</td>';
					$content .= '<td>' . $ds["customersCompanyPLZ"]. '</td>';
					$content .= '<td>' . $ds["customersCompanyOrt"]. '</td>';
					$content .= '<td>' . $arrCountryTypeDatas[$ds["customersCompanyCountry"]]["countries_name"]. '</td>';
					$content .= '<td>' . $ds["customersCompanyStrasse"]. '</td>';
					$content .= '<td>' . $ds["customersMail1"]. '; ' . $ds["customersMail2"]. '</td>';
					$content .= '<td>' . $ds["customersCompanyLocation"]. '</td>';

					$content .= '</tr>';
					$countRow++;
				}

				$content .= '</table>';
				$content .= '</div>';

				$content .= '<hr />';
				$content .= '<div class="actionButtonsArea">';
				$content .= '<input type="button" name="resetDatas2" class="inputButton1 inputButtonOrange" value="Abbrechen" onclick="$(\'#searchResultArea\').remove();"> ';
				$content .= '<input type="submit" name="storeDatas2" class="inputButton1 inputButtonGreen" value="Trotzdem speichern">';
				$content .= '</div>';

			}
		}
	}
	echo $content;
?>