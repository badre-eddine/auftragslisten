<?php
	session_start();

	require_once('../config/configMandator.inc.php');
	require_once('../config/configParams_' . strtolower(MANDATOR) . '.inc.php');
	require_once('../config/configBasic.inc.php');
	require_once('../config/configFiles.inc.php');
	require_once('../config/configTables.inc.php');
	require_once('../inc/functions.inc.php');
	##require_once('header.inc.php');

	#$arrUrlData = parse_url(urldecode($_SERVER["REQUEST_URI"]));
	$arrUrlData = parse_url(($_SERVER["REQUEST_URI"]));
	$strUrlParams = ($arrUrlData["query"]);
	$arrUrlParams = explode('&', $strUrlParams);
	if(!empty($arrUrlParams)){
		foreach($arrUrlParams as $thisParam){
			$arrTemp = explode('=', $thisParam);
			$_GET[$arrTemp[0]] = urldecode(preg_replace("/xxxxxx/", " ", $arrTemp[1]));
		}
	}

	$thisSqlString = trim($_GET["sqlString"]);

	$content = '';

	$warningMessage = '';
	$errorMessage = '';
	$successMessage = '';
	$infoMessage = '';

	$useUf8Encode = false;

	if($thisSqlString != "") {

		$dbConnection = new DB_Connection();
		$db_open = $dbConnection->db_connect();
		$sql = $thisSqlString;

		$rs = $dbConnection->db_query($sql);

		if($rs){
			$content = 1;
		}
		else {
			$content = 0;
			$fp_checkExecuteSQL = fopen('checkExecuteSQL.txt', 'a');
			fwrite($fp_checkExecuteSQL, date('Y-m-d H:i:s') . '<br />' . $sql . '<br />' . mysqli_error($db_open) . '<hr />');
			fclose($fp_checkExecuteSQL);
		}

		if($dbConnection) {
			$dbConnection->db_close();
		}
	}
	echo $content;
?>