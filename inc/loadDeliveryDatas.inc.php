<?php
	session_start();

	require_once('../config/configMandator.inc.php');
	require_once('../config/configParams_' . strtolower(MANDATOR) . '.inc.php');
	require_once('../config/configBasic.inc.php');
	require_once('../config/configFiles.inc.php');
	require_once('../config/configTables.inc.php');
	require_once('../inc/functions.inc.php');
	##require_once('header.inc.php');

	$thisSearchTrackingID = trim($_GET["searchTrackingID"]);
	$thisSearchTrackingDate = trim($_GET["searchTrackingDate"]);
	$thisSearchTrackingZipcode = trim($_GET["searchTrackingZipcode"]);
	$thisSearchTrackingCustomerNumber = trim($_GET["searchTrackingCustomerNumber"]);
	
	#$thisSearchTrackingID = trim(urldecode($_GET["searchTrackingID"]));
	#$thisSearchTrackingID = preg_replace("/[ ]{1,}$/", "", $thisSearchTrackingID);
	#$thisSearchTrackingID = preg_replace("/ $/", "", $thisSearchTrackingID);

	$arrDpdScanCodes = array(
		"01" => "Konsolidierung/Dekonsolidierung",
		"02" => "Eingangsscannung",
		"03" => "Ausrollung",
		"04" => "UN-Retoure",
		"05" => "Einrollung",
		"06" => "Systemretoure",
		"07" => "Ausgang",
		"08" => "Lager",
		"09" => "Eingang-Differenz",
		"10" => "Hauptdepotdurchlauf",
		"12" => "In Verzollung",
		"13" => "Zugestellt Scann",
		"14" => "Keine Zustellung",
		"15" => "Abhol Scan",
		"17" => "Verzollt",
		"18" => "Infoscan",
		"20" => "Beladescan",
		"77" => "Rueckholung",
		"97" => "Dekonsolidierung"
	);

	$content = '';
	$sql = '';

	$warningMessage = '';
	$errorMessage = '';
	$successMessage = '';
	$infoMessage = '';

	$useUf8Encode = false;

	// BOF READ FROM ONLINE PAGE
		/*
		if($thisSearchTrackingID != "") {
			#$thisGetFileUrl = "https://tracking.dpd.de/cgi-bin/delistrack?pknr={###TRACKING_NUMBER###}&typ=1&lang=de";
			$thisGetFileUrl = "https://tracking.dpd.de/parcelstatus?query={###TRACKING_NUMBER###}&locale=de_DE";
			$thisGetFileUrl = preg_replace("/{###TRACKING_NUMBER###}/", $thisSearchTrackingID, $thisGetFileUrl);

			$thisGetFileContent = implode("", file($thisGetFileUrl));

			if($thisSearchTrackingID == '01485004296746'){
				$testFP = fopen('dpdTest.html', 'w');
				fwrite($testFP, $thisGetFileContent);
				fclose($testFP);
			}

			$thisGetFileContent = preg_replace("/[\r\n]{1,}/", "", $thisGetFileContent);

			$pattern = "<tr.*>(.*)<\/tr>";
			$thisMatchResult = preg_match_all("/" . $pattern . "/ismU", $thisGetFileContent, $arrThisFileContent);

			for($i = 0 ; $i < count($arrThisFileContent[1]) ; $i++){

				$thisMatchResult = preg_match("/Zustellung an/ism", $arrThisFileContent[1][$i]);
				if($thisMatchResult) {
					$useContent = $arrThisFileContent[1][$i];
					$usePattern = "/([0-9]{2}\.[0-9]{2}\.[0-9]{4}) <br>([0-9]{2}\:[0-9]{2}).*Zustellung an: (.*)&nbsp;<\/td><td.*$/ism";
				}
				else {
					$thisMatchResult = preg_match("/Paketabholung im DPD Paket-Shop/ism", $arrThisFileContent[1][$i]);
					if($thisMatchResult) {
						$useContent = $arrThisFileContent[1][$i];
						$usePattern = "/([0-9]{2}\.[0-9]{2}\.[0-9]{4}) <br>([0-9]{2}\:[0-9]{2}).*(Paketabholung im DPD Paket-Shop) durch Empf.*<\/td>/ism";
					}
					else {
						$thisMatchResult = preg_match("/<br>Zustellung&nbsp;<\/td>/ism", $arrThisFileContent[1][$i]);
						if($thisMatchResult) {
							$useContent = $arrThisFileContent[1][$i];
							$usePattern = "/<br>Zustellung&nbsp;<\/td><td.*$/ism";
						}
						else {
							$thisMatchResult = preg_match("/>Paketzustellung im DPD Paket-Shop<br>/ism", $arrThisFileContent[1][$i]);
							if($thisMatchResult) {
								$useContent = $arrThisFileContent[1][$i];
								$usePattern = "/>Paketzustellung im DPD Paket-Shop<br>/ism";
							}
						}
					}
				}
			}

			#$thisMatchResult = preg_match("/([0-9]{2}\.[0-9]{2}\.[0-9]{4}) <br>([0-9]{2}\:[0-9]{2}).*(Paketabholung im DPD Paket-Shop) durch Empf.*<\/td>/ism", $useContent, $arrFound);
			#echo 'tR: ' . $thisMatchResult . '<br>';
			#echo 'useContent: ' . $useContent . '<br>';
			#$thisMatchResult = preg_match("/([0-9]{2}\.[0-9]{2}\.[0-9]{4}) <br>([0-9]{2}\:[0-9]{2}).*Zustellung an: (.*)&nbsp;<\/td><td.*$/ism", $useContent, $arrFound);

			$thisMatchResult = preg_match($usePattern, $useContent, $arrFound);

			if($thisMatchResult){
				$pattern = "([0-9]{2})\.([0-9]{2})\.([0-9]{4})";
				$replace = "$3-$2-$1";
				$useContent = preg_replace("/" . $pattern . "/", $replace, $useContent);

				$pattern = "#[ ]{1,}|[ ]{1,}#|##|#\\n|\\r|<\/td>|&nbsp;|&bull;";
				$replace = "#";
				while(preg_match("/" . $pattern . "/", $useContent)){
					$useContent = preg_replace("/" . $pattern . "/", $replace, $useContent);
				}

				$useContent = strip_tags($useContent);

				$pattern = "##";
				$replace = "#";
				while(preg_match("/" . $pattern . "/", $useContent)){
					$useContent = preg_replace("/" . $pattern . "/", $replace, $useContent);
				}

				$pattern = "#$|^#";
				$replace = "";
				$useContent = preg_replace("/" . $pattern . "/", $replace, $useContent);

				$pattern = "#Zusatz-Information";
				$replace = "";
				$useContent = preg_replace("/" . $pattern . "/", $replace, $useContent);

				$thisDeliveryData = formatLoadedDeliveryDatas($useContent, '#');

				$arrUpdateDatas = array("DATA" => trim($useContent), "TRACKING_NUMBER" => $thisSearchTrackingID);
				$rowClass = 'row3';
				flush();
				sleep(5);
			}
			else {
				$thisDeliveryData = '- ?#- ?';
			}
			#$content = $thisDeliveryData;


			if(!empty($arrUpdateDatas)){
				$dbConnection = new DB_Connection();
				$db_open = $dbConnection->db_connect();
				$sql = "UPDATE `" . TABLE_DELIVERY_DATAS . "`
							SET `" . TABLE_DELIVERY_DATAS . "`.`Zustellung` = '" . addslashes($arrUpdateDatas["DATA"]) . "'
							WHERE
								`" . TABLE_DELIVERY_DATAS . "`.`Paketnummer` = '" . $arrUpdateDatas["TRACKING_NUMBER"] . "'
								AND (
									`" . TABLE_DELIVERY_DATAS . "`.`Zustellung` = ''
									OR
									`" . TABLE_DELIVERY_DATAS . "`.`Zustellung` IS NULL
								)
					";

				$rs = $dbConnection->db_query($sql);
				if($dbConnection) {
					if($dbConnection->db_displayErrors() != "") {
						$errorMessage = $dbConnection->db_displayErrors(). '<br />';
						if($errorMessage != "") {	echo '<p class="errorArea" style="position:absolute;bottom:0px;left:220px;">'.$errorMessage.'</p>'; }
					}
					$dbConnection->db_close();
				}
			}
		}
		*/
	// EOF READ FROM ONLINE PAGE

	// BOF READ FROM INTERFACE DATAS
		if($thisSearchTrackingID != "" && $thisSearchTrackingDate != '' && $thisSearchTrackingZipcode != '') {
			// EVENT_DATE_TIME:			20150615153032
			// thisSearchTrackingDate:	2015-06-15
			$intervalMonth = '5';
			$arrTempTrackingDate = explode('-', $thisSearchTrackingDate);
			$thisSearchTrackingDateLimit = date('Ymd', mktime(1, 1, 1, ($arrTempTrackingDate["1"] + $intervalMonth), $arrTempTrackingDate["2"], $arrTempTrackingDate[0])) . '000000';
			$thisSearchTrackingDate = preg_replace("/-/", "", $thisSearchTrackingDate) . '000000';
			$dbConnection = new DB_Connection();
			$db_open = $dbConnection->db_connect();

			$sql = "
					SELECT
						`" . TABLE_DELIVERY_DATAS_STATUS_IMPORT . "`.`PARCELNO`,
						`" . TABLE_DELIVERY_DATAS_STATUS_IMPORT . "`.`SCAN_CODE`,
						`" . TABLE_DELIVERY_DATAS_STATUS_IMPORT . "`.`DEPOT_CODE`,
						`" . TABLE_DELIVERY_DATAS_STATUS_IMPORT . "`.`DEPOTNAME`,
						`" . TABLE_DELIVERY_DATAS_STATUS_IMPORT . "`.`EVENT_DATE_TIME`,
						`" . TABLE_DELIVERY_DATAS_STATUS_IMPORT . "`.`ROUTE`,
						`" . TABLE_DELIVERY_DATAS_STATUS_IMPORT . "`.`TOUR`,
						`" . TABLE_DELIVERY_DATAS_STATUS_IMPORT . "`.`PCODE`,
						`" . TABLE_DELIVERY_DATAS_STATUS_IMPORT . "`.`SERVICE`,
						`" . TABLE_DELIVERY_DATAS_STATUS_IMPORT . "`.`CONSIGNEE_COUNTRY_CODE`,
						`" . TABLE_DELIVERY_DATAS_STATUS_IMPORT . "`.`CONSIGNEE_ZIP`,
						`" . TABLE_DELIVERY_DATAS_STATUS_IMPORT . "`.`ADD_SERVICE_1`,
						`" . TABLE_DELIVERY_DATAS_STATUS_IMPORT . "`.`ADD_SERVICE_2`,
						`" . TABLE_DELIVERY_DATAS_STATUS_IMPORT . "`.`ADD_SERVICE_3`,
						`" . TABLE_DELIVERY_DATAS_STATUS_IMPORT . "`.`WEIGHT`,
						`" . TABLE_DELIVERY_DATAS_STATUS_IMPORT . "`.`CUSTOMER_REFERENCE`,
						`" . TABLE_DELIVERY_DATAS_STATUS_IMPORT . "`.`POD_IMAGE_REF`,
						`" . TABLE_DELIVERY_DATAS_STATUS_IMPORT . "`.`RECEIVER_NAME`,
						`" . TABLE_DELIVERY_DATAS_STATUS_IMPORT . "`.`INFO_TEXT`,
						`" . TABLE_DELIVERY_DATAS_STATUS_IMPORT . "`.`LOCATION`,
						`" . TABLE_DELIVERY_DATAS_STATUS_IMPORT . "`.`ADDITIONAL`
					FROM `" . TABLE_DELIVERY_DATAS_STATUS_IMPORT . "`
					WHERE 1
						AND `" . TABLE_DELIVERY_DATAS_STATUS_IMPORT . "`.`PARCELNO` = '" . $thisSearchTrackingID . "'
						AND `" . TABLE_DELIVERY_DATAS_STATUS_IMPORT . "`.`EVENT_DATE_TIME` >= '" . $thisSearchTrackingDate . "'
						AND `" . TABLE_DELIVERY_DATAS_STATUS_IMPORT . "`.`EVENT_DATE_TIME` < '" . $thisSearchTrackingDateLimit . "'
						AND (
							`" . TABLE_DELIVERY_DATAS_STATUS_IMPORT . "`.`SCAN_CODE` = '13'
							OR (`" . TABLE_DELIVERY_DATAS_STATUS_IMPORT . "`.`SCAN_CODE` = '23' AND `" . TABLE_DELIVERY_DATAS_STATUS_IMPORT . "`.`RECEIVER_NAME` != '')
						)
						LIMIT 1
				";

			$useContent = "";
			$rs = $dbConnection->db_query($sql);

			while($ds = mysqli_fetch_assoc($rs)){
				// 2012-12-17 10:22#0171#Ludwigsburg (DE)#29022#Zustellung an: REISCFICH#DE#70191#0171#308#101
				$useContent = "";
				// if($ds["RECEIVER_NAME"] != ''){
				if(1){
					$thisDateTime = substr($ds["EVENT_DATE_TIME"], 0, 4) . '-' . substr($ds["EVENT_DATE_TIME"], 4, 2) . '-' . substr($ds["EVENT_DATE_TIME"], 6, 2) . ' ' . substr($ds["EVENT_DATE_TIME"], 8, 2) . ':' . substr($ds["EVENT_DATE_TIME"], 10, 2);
					$useContent .= $thisDateTime . '|' ;
					$useContent .= '' . $ds["RECEIVER_NAME"] . '' ;
					if($ds["SCAN_CODE"] == '23'){
						$useContent .= ' - Zustellung im Paket-Shop' ;
					}
				}
			}
			$useContent = trim($useContent);

			// BOF WORKAROUND DPD-PORTAL SHOWS DELIVERED BUT INTERFACE-DATAS NOT INCLUDING DELIVERY NAME PARCELNUMBER AND ZIPCODE
				#if($_COOKIE["isAdmin"] == '1'){
				if(1){
					if($useContent == ''){
						$sql_workaround = "
								SELECT
									`deliveryDatasStatusImport_2`.`SOURCE_FILE`,
									`deliveryDatasStatusImport_2`.`PARCELNO`,
									`deliveryDatasStatusImport_2`.`SCAN_CODE`,
									`deliveryDatasStatusImport_2`.`DEPOT_CODE`,
									`deliveryDatasStatusImport_2`.`DEPOTNAME`,
									`deliveryDatasStatusImport_2`.`EVENT_DATE_TIME`,
									`deliveryDatasStatusImport_2`.`ROUTE`,
									`deliveryDatasStatusImport_2`.`TOUR`,
									`deliveryDatasStatusImport_2`.`PCODE`,
									`deliveryDatasStatusImport_2`.`SERVICE`,
									`deliveryDatasStatusImport_2`.`CONSIGNEE_COUNTRY_CODE`,
									`deliveryDatasStatusImport_2`.`CONSIGNEE_ZIP`,
									`deliveryDatasStatusImport_2`.`ADD_SERVICE_1`,
									`deliveryDatasStatusImport_2`.`ADD_SERVICE_2`,
									`deliveryDatasStatusImport_2`.`ADD_SERVICE_3`,
									`deliveryDatasStatusImport_2`.`WEIGHT`,
									`deliveryDatasStatusImport_2`.`CUSTOMER_REFERENCE`,
									`deliveryDatasStatusImport_2`.`POD_IMAGE_REF`,
									`deliveryDatasStatusImport_2`.`RECEIVER_NAME`,
									`deliveryDatasStatusImport_2`.`INFO_TEXT`,
									`deliveryDatasStatusImport_2`.`LOCATION`,
									`deliveryDatasStatusImport_2`.`ADDITIONAL`

								FROM `" . TABLE_DELIVERY_DATAS_STATUS_IMPORT . "` AS `deliveryDatasStatusImport_1`

								LEFT JOIN `" . TABLE_DELIVERY_DATAS_STATUS_IMPORT . "` AS `deliveryDatasStatusImport_2`
								ON(
									`deliveryDatasStatusImport_1`.`PARCELNO` = `deliveryDatasStatusImport_2`.`PARCELNO`
									AND (
										`deliveryDatasStatusImport_1`.`SOURCE_FILE` = `deliveryDatasStatusImport_2`.`SOURCE_FILE`
										/*
										OR DATE_ADD(DATE_FORMAT(SUBSTRING(`deliveryDatasStatusImport_1`.`SOURCE_FILE`, -15), '%Y-%m-%d'), INTERVAL " . DPD_CHECK_EVENT_DATE_INTERVAL . ") <= DATE_FORMAT(SUBSTRING(`deliveryDatasStatusImport_2`.`SOURCE_FILE`, -15), '%Y-%m-%d')
										OR DATE_ADD(DATE_FORMAT(SUBSTRING(`deliveryDatasStatusImport_2`.`SOURCE_FILE`, -15), '%Y-%m-%d'), INTERVAL " . DPD_CHECK_EVENT_DATE_INTERVAL . ") <= DATE_FORMAT(SUBSTRING(`deliveryDatasStatusImport_1`.`SOURCE_FILE`, -15), '%Y-%m-%d')
										*/
										OR DATE_FORMAT(`deliveryDatasStatusImport_1`.`EVENT_DATE_TIME`, '%Y-%m-%d') = DATE_FORMAT(`deliveryDatasStatusImport_2`.`EVENT_DATE_TIME`, '%Y-%m-%d')

										OR DATE_ADD(DATE_FORMAT(`deliveryDatasStatusImport_1`.`EVENT_DATE_TIME`, '%Y-%m-%d'), INTERVAL " . DPD_CHECK_EVENT_DATE_INTERVAL . ") <= DATE_FORMAT(`deliveryDatasStatusImport_2`.`EVENT_DATE_TIME`, '%Y-%m-%d')
										OR DATE_ADD(DATE_FORMAT(`deliveryDatasStatusImport_2`.`EVENT_DATE_TIME`, '%Y-%m-%d'), INTERVAL " . DPD_CHECK_EVENT_DATE_INTERVAL . ") <= DATE_FORMAT(`deliveryDatasStatusImport_1`.`EVENT_DATE_TIME`, '%Y-%m-%d')
									)
									AND `deliveryDatasStatusImport_2`.`CONSIGNEE_ZIP` = ''
								)

								WHERE 1
									AND `deliveryDatasStatusImport_1`.`PARCELNO` = '" . $thisSearchTrackingID . "'
									AND (
										`deliveryDatasStatusImport_1`.`CONSIGNEE_ZIP` = '" . $thisSearchTrackingZipcode . "'
										/*
										OR
										`deliveryDatasStatusImport_1`.`CUSTOMER_REFERENCE` = '" . $thisSearchTrackingCustomerNumber . "'
										*/
									)
									AND (
										`deliveryDatasStatusImport_1`.`SCAN_CODE` = '13'
										OR `deliveryDatasStatusImport_2`.`SCAN_CODE` = '13'
										OR (`deliveryDatasStatusImport_1`.`SCAN_CODE` = '23' AND `deliveryDatasStatusImport_1`.`RECEIVER_NAME` != '')
										OR (`deliveryDatasStatusImport_2`.`SCAN_CODE` = '23' AND `deliveryDatasStatusImport_2`.`RECEIVER_NAME` != '')
									)

									LIMIT 1
							";
						$useContent_workaround = "";
						$rs_workaround = $dbConnection->db_query($sql_workaround);

						while($ds_workaround = mysqli_fetch_assoc($rs_workaround)){
							$useContent_workaround = "";

							$useContent = "";
							// if($ds_workaround["RECEIVER_NAME"] != ''){
							if(1){
								$thisDateTime = substr($ds_workaround["EVENT_DATE_TIME"], 0, 4) . '-' . substr($ds_workaround["EVENT_DATE_TIME"], 4, 2) . '-' . substr($ds_workaround["EVENT_DATE_TIME"], 6, 2) . ' ' . substr($ds_workaround["EVENT_DATE_TIME"], 8, 2) . ':' . substr($ds_workaround["EVENT_DATE_TIME"], 10, 2);
								$useContent_workaround .= $thisDateTime . '|' ;
								$useContent_workaround .= '' . $ds_workaround["RECEIVER_NAME"] . '' ;
								if($ds_workaround["SCAN_CODE"] == '23'){
									$useContent_workaround .= ' - Zustellung im Paket-Shop' ;
								}
							}
						}
						if($useContent_workaround != ''){
							#$thisDeliveryData_workaround = formatLoadedDeliveryDatas($useContent_workaround, '|');
						}
						#echo $thisDeliveryData_workaround;
						$useContent = $useContent_workaround;
					}
				}
			// EOF WORKAROUND DPD-PORTAL SHOWS DELIVERED BUT INTERFACE-DATAS NOT INCLUDING DELIVERY NAME PARCELNUMBER AND ZIPCODE


			// BOF WORKAROUND DPD-PORTAL SHOWS DELIVERED BUT INTERFACE-DATAS NOT INCLUDING DELIVERY NAME USING SCAN_CODE, INFO_TEXT
				if($useContent == ''){
					$sql_workaround = "
							SELECT
								`" . TABLE_DELIVERY_DATAS_STATUS_IMPORT . "`.`PARCELNO`,
								`" . TABLE_DELIVERY_DATAS_STATUS_IMPORT . "`.`SCAN_CODE`,
								`" . TABLE_DELIVERY_DATAS_STATUS_IMPORT . "`.`DEPOT_CODE`,
								`" . TABLE_DELIVERY_DATAS_STATUS_IMPORT . "`.`DEPOTNAME`,
								`" . TABLE_DELIVERY_DATAS_STATUS_IMPORT . "`.`EVENT_DATE_TIME`,
								`" . TABLE_DELIVERY_DATAS_STATUS_IMPORT . "`.`ROUTE`,
								`" . TABLE_DELIVERY_DATAS_STATUS_IMPORT . "`.`TOUR`,
								`" . TABLE_DELIVERY_DATAS_STATUS_IMPORT . "`.`PCODE`,
								`" . TABLE_DELIVERY_DATAS_STATUS_IMPORT . "`.`SERVICE`,
								`" . TABLE_DELIVERY_DATAS_STATUS_IMPORT . "`.`CONSIGNEE_COUNTRY_CODE`,
								`" . TABLE_DELIVERY_DATAS_STATUS_IMPORT . "`.`CONSIGNEE_ZIP`,
								`" . TABLE_DELIVERY_DATAS_STATUS_IMPORT . "`.`ADD_SERVICE_1`,
								`" . TABLE_DELIVERY_DATAS_STATUS_IMPORT . "`.`ADD_SERVICE_2`,
								`" . TABLE_DELIVERY_DATAS_STATUS_IMPORT . "`.`ADD_SERVICE_3`,
								`" . TABLE_DELIVERY_DATAS_STATUS_IMPORT . "`.`WEIGHT`,
								`" . TABLE_DELIVERY_DATAS_STATUS_IMPORT . "`.`CUSTOMER_REFERENCE`,
								`" . TABLE_DELIVERY_DATAS_STATUS_IMPORT . "`.`POD_IMAGE_REF`,
								`" . TABLE_DELIVERY_DATAS_STATUS_IMPORT . "`.`RECEIVER_NAME`,
								`" . TABLE_DELIVERY_DATAS_STATUS_IMPORT . "`.`INFO_TEXT`,
								`" . TABLE_DELIVERY_DATAS_STATUS_IMPORT . "`.`LOCATION`,
								`" . TABLE_DELIVERY_DATAS_STATUS_IMPORT . "`.`ADDITIONAL`
							FROM `" . TABLE_DELIVERY_DATAS_STATUS_IMPORT . "`
							WHERE 1
								AND `" . TABLE_DELIVERY_DATAS_STATUS_IMPORT . "`.`PARCELNO` = '" . $thisSearchTrackingID . "'
								AND `" . TABLE_DELIVERY_DATAS_STATUS_IMPORT . "`.`EVENT_DATE_TIME` >= '" . $thisSearchTrackingDate . "'
								AND `" . TABLE_DELIVERY_DATAS_STATUS_IMPORT . "`.`EVENT_DATE_TIME` < '" . $thisSearchTrackingDateLimit . "'
								AND `" . TABLE_DELIVERY_DATAS_STATUS_IMPORT . "`.`SCAN_CODE` = '18'
								AND `" . TABLE_DELIVERY_DATAS_STATUS_IMPORT . "`.`INFO_TEXT` LIKE '101:2:%'
								/* AND `" . TABLE_DELIVERY_DATAS_STATUS_IMPORT . "`.`POD_IMAGE_REF` != '' */
								AND LENGTH(`" . TABLE_DELIVERY_DATAS_STATUS_IMPORT . "`.`INFO_TEXT`) = 14

								LIMIT 1
						";
					$useContent_workaround = "";
					$rs_workaround = $dbConnection->db_query($sql_workaround);

					while($ds_workaround = mysqli_fetch_assoc($rs_workaround)){
						$useContent_workaround = "";

						$thisDateTime = substr($ds_workaround["EVENT_DATE_TIME"], 0, 4) . '-' . substr($ds_workaround["EVENT_DATE_TIME"], 4, 2) . '-' . substr($ds_workaround["EVENT_DATE_TIME"], 6, 2) . ' ' . substr($ds_workaround["EVENT_DATE_TIME"], 8, 2) . ':' . substr($ds_workaround["EVENT_DATE_TIME"], 10, 2);
						$arrGetDateTime = explode(":", $ds_workaround["INFO_TEXT"]);
						$thisGetDateTime = trim($arrGetDateTime[2]);

						$thisDateTime = substr($thisGetDateTime, 0, 4) . '-' . substr($thisGetDateTime, 4, 2) . '-' . substr($thisGetDateTime, 6, 2);

						$useContent_workaround .= $thisDateTime . '|' ;
						$useContent_workaround .= '[Kein Zustell-Scan]' ;
					}
					if($useContent_workaround != ''){
						#$thisDeliveryData_workaround = formatLoadedDeliveryDatas($useContent_workaround, '|');
					}
					#echo $thisDeliveryData_workaround;
					$useContent = $useContent_workaround;
				}
			// EOF WORKAROUND DPD-PORTAL SHOWS DELIVERED BUT INTERFACE-DATAS NOT INCLUDING DELIVERY NAME USING SCAN_CODE, INFO_TEXT



			if($useContent != ''){
				$thisDeliveryData = formatLoadedDeliveryDatas($useContent, '|');

				$arrUpdateDatas = array("DATA" => trim($useContent), "TRACKING_NUMBER" => $thisSearchTrackingID);

				if(!empty($arrUpdateDatas)){
					// BOF UPDATE ONLY IF NOT `Zustellung`-DATE < `Versanddatum`-DATE
						/*
							// BOF DELETE WHERE `Zustellung`-DATE < `Versanddatum`-DATE
								UPDATE
									`common_deliveryDatas`

									SET `common_deliveryDatas`.`Zustellung` = ''

									WHERE 1
										AND `common_deliveryDatas`.`Zustellung` != ''
										AND SUBSTRING(`common_deliveryDatas`.`Zustellung`, 1, 10) < `common_deliveryDatas`.`Versanddatum`
										AND DATE_FORMAT(`common_deliveryDatas`.`Versanddatum`, '%Y') = '2016'
							// EOF DELETE WHERE `Zustellung`-DATE < `Versanddatum`-DATE
						*/


						$thisDeliveryDate = substr(trim($arrUpdateDatas["DATA"]), 0, 10);
						$thisSearchTrackingDateFormatted = date("Y-m-d", strtotime($thisSearchTrackingDate));

						/*
						$fp_test = fopen('testLogDPD.txt', 'a+');
						fwrite($fp_test, 'thisDeliveryDate: ' . $thisDeliveryDate . "\n");
						fwrite($fp_test, 'thisSearchTrackingDate: ' . $thisSearchTrackingDate . "\n");
						fwrite($fp_test, 'thisSearchTrackingDateFormatted: ' . $thisSearchTrackingDateFormatted . "\n");
						fwrite($fp_test, 'preg_match: ' . preg_match("/^[0-9]{4}-[0-9]{2}-[0-9]{2}/", $thisDeliveryDate) . "\n");
						fwrite($fp_test, "\n" . '---------------------------------------------------' . "\n");
						*/

						if(preg_match("/^[0-9]{4}-[0-9]{2}-[0-9]{2}/", $thisDeliveryDate) && ($thisDeliveryDate > $thisSearchTrackingDateFormatted)){
							$dbConnection = new DB_Connection();
							$db_open = $dbConnection->db_connect();
							$sql = "UPDATE `" . TABLE_DELIVERY_DATAS . "`
										SET `" . TABLE_DELIVERY_DATAS . "`.`Zustellung` = '" . addslashes($arrUpdateDatas["DATA"]) . "'
										WHERE
											`" . TABLE_DELIVERY_DATAS . "`.`Paketnummer` = '" . $arrUpdateDatas["TRACKING_NUMBER"] . "'
											AND (
												`" . TABLE_DELIVERY_DATAS . "`.`Zustellung` = ''
												OR
												`" . TABLE_DELIVERY_DATAS . "`.`Zustellung` IS NULL
											)

											/* AND `Versanddatum` = '" . $thisSearchTrackingDate . "' */
											AND `Versanddatum` = '" . $thisSearchTrackingDateFormatted . "'
								";
							#fwrite($fp_test, 'sql: ' . $sql . "\n");
							$rs = $dbConnection->db_query($sql);
							if($dbConnection) {
								if($dbConnection->db_displayErrors() != "") {
									$errorMessage = $dbConnection->db_displayErrors(). '<br />';
									if($errorMessage != "") {	echo '<p class="errorArea" style="position:absolute;bottom:0px;left:220px;">'.$errorMessage.'</p>'; }
								}
								$dbConnection->db_close();
							}
						}
						else {
							$thisDeliveryData = '- ?#- ?';
						}

						#fclose($fp_test);
					// EOF UPDATE ONLY IF NOT `Zustellung`-DATE < `Versanddatum`-DATE
				}
			}
			else {
				$thisDeliveryData = '- ?#- ?';
			}
		}
	// EOF READ FROM INTERFACE DATAS
	
	$content = $thisDeliveryData;
	$content = trim($content);

	echo $content;
?>
