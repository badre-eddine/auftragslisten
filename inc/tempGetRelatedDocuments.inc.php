<?php

if($thisAction == 'GET_CONVERTED_DOCUMENTS'){

	echo '<h3>Konvertierte Dokumente holen</h3>';

	$thisTempTable = "_SICH" . date("YmdHis") . "_" . TABLE_RELATED_DOCUMENTS . "";

	$sql = " CREATE TABLE `" . $thisTempTable . "` LIKE `" . TABLE_RELATED_DOCUMENTS . "`; ";
	$rs = $dbConnection->db_query($sql);

	$sql = " INSERT INTO `" . $thisTempTable . "` SELECT * FROM `" . TABLE_RELATED_DOCUMENTS . "`; ";
	$rs = $dbConnection->db_query($sql);

	$sql = " DROP TABLE IF EXISTS `tempDocumentRelations` ";
	$rs = $dbConnection->db_query($sql);

	$sql = "
		CREATE TABLE IF NOT EXISTS `tempDocumentRelations` (
			`t1_documentsToDocumentsOriginDocumentNumber` varchar(20) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
			`t2_documentsToDocumentsOriginDocumentNumber` varchar(20) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
			`t3_documentsToDocumentsOriginDocumentNumber` varchar(20) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
			`t4_documentsToDocumentsOriginDocumentNumber` varchar(20) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
			`t5_documentsToDocumentsOriginDocumentNumber` varchar(20) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
			`t6_documentsToDocumentsOriginDocumentNumber` varchar(20) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
			`t7_documentsToDocumentsOriginDocumentNumber` varchar(20) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
			`t8_documentsToDocumentsOriginDocumentNumber` varchar(20) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
			`t9_documentsToDocumentsOriginDocumentNumber` varchar(20) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
			`t10_documentsToDocumentsOriginDocumentNumber` varchar(20) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
			`t11_documentsToDocumentsOriginDocumentNumber` varchar(20) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
			`t12_documentsToDocumentsOriginDocumentNumber` varchar(20) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
			`t13_documentsToDocumentsOriginDocumentNumber` varchar(20) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
			`t14_documentsToDocumentsOriginDocumentNumber` varchar(20) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
			`t15_documentsToDocumentsOriginDocumentNumber` varchar(20) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
			`t16_documentsToDocumentsOriginDocumentNumber` varchar(20) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
			`t17_documentsToDocumentsOriginDocumentNumber` varchar(20) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,

			`t1_documentsToDocumentsCreatedDocumentNumber` varchar(20) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
			`t2_documentsToDocumentsCreatedDocumentNumber` varchar(20) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
			`t3_documentsToDocumentsCreatedDocumentNumber` varchar(20) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
			`t4_documentsToDocumentsCreatedDocumentNumber` varchar(20) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
			`t5_documentsToDocumentsCreatedDocumentNumber` varchar(20) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
			`t6_documentsToDocumentsCreatedDocumentNumber` varchar(20) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
			`t7_documentsToDocumentsCreatedDocumentNumber` varchar(20) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
			`t8_documentsToDocumentsCreatedDocumentNumber` varchar(20) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
			`t9_documentsToDocumentsCreatedDocumentNumber` varchar(20) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
			`t10_documentsToDocumentsCreatedDocumentNumber` varchar(20) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
			`t11_documentsToDocumentsCreatedDocumentNumber` varchar(20) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
			`t12_documentsToDocumentsCreatedDocumentNumber` varchar(20) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
			`t13_documentsToDocumentsCreatedDocumentNumber` varchar(20) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
			`t14_documentsToDocumentsCreatedDocumentNumber` varchar(20) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
			`t15_documentsToDocumentsCreatedDocumentNumber` varchar(20) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
			`t16_documentsToDocumentsCreatedDocumentNumber` varchar(20) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
			`t17_documentsToDocumentsCreatedDocumentNumber` varchar(20) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,

			KEY `t1_documentsToDocumentsOriginDocumentNumber` (`t1_documentsToDocumentsOriginDocumentNumber`),
			KEY `t2_documentsToDocumentsOriginDocumentNumber` (`t2_documentsToDocumentsOriginDocumentNumber`),
			KEY `t3_documentsToDocumentsOriginDocumentNumber` (`t3_documentsToDocumentsOriginDocumentNumber`),
			KEY `t4_documentsToDocumentsOriginDocumentNumber` (`t4_documentsToDocumentsOriginDocumentNumber`),
			KEY `t5_documentsToDocumentsOriginDocumentNumber` (`t5_documentsToDocumentsOriginDocumentNumber`),
			KEY `t6_documentsToDocumentsOriginDocumentNumber` (`t6_documentsToDocumentsOriginDocumentNumber`),
			KEY `t7_documentsToDocumentsOriginDocumentNumber` (`t7_documentsToDocumentsOriginDocumentNumber`),
			KEY `t8_documentsToDocumentsOriginDocumentNumber` (`t8_documentsToDocumentsOriginDocumentNumber`),
			KEY `t9_documentsToDocumentsOriginDocumentNumber` (`t9_documentsToDocumentsOriginDocumentNumber`),
			KEY `t10_documentsToDocumentsOriginDocumentNumber` (`t10_documentsToDocumentsOriginDocumentNumber`),
			KEY `t11_documentsToDocumentsOriginDocumentNumber` (`t11_documentsToDocumentsOriginDocumentNumber`),
			KEY `t12_documentsToDocumentsOriginDocumentNumber` (`t12_documentsToDocumentsOriginDocumentNumber`),
			KEY `t13_documentsToDocumentsOriginDocumentNumber` (`t13_documentsToDocumentsOriginDocumentNumber`),
			KEY `t14_documentsToDocumentsOriginDocumentNumber` (`t14_documentsToDocumentsOriginDocumentNumber`),
			KEY `t15_documentsToDocumentsOriginDocumentNumber` (`t15_documentsToDocumentsOriginDocumentNumber`),
			KEY `t16_documentsToDocumentsOriginDocumentNumber` (`t16_documentsToDocumentsOriginDocumentNumber`),
			KEY `t17_documentsToDocumentsOriginDocumentNumber` (`t17_documentsToDocumentsOriginDocumentNumber`),

			KEY `t1_documentsToDocumentsCreatedDocumentNumber` (`t1_documentsToDocumentsCreatedDocumentNumber`),
			KEY `t2_documentsToDocumentsCreatedDocumentNumber` (`t2_documentsToDocumentsCreatedDocumentNumber`),
			KEY `t3_documentsToDocumentsCreatedDocumentNumber` (`t3_documentsToDocumentsCreatedDocumentNumber`),
			KEY `t4_documentsToDocumentsCreatedDocumentNumber` (`t4_documentsToDocumentsCreatedDocumentNumber`),
			KEY `t5_documentsToDocumentsCreatedDocumentNumber` (`t5_documentsToDocumentsCreatedDocumentNumber`),
			KEY `t6_documentsToDocumentsCreatedDocumentNumber` (`t6_documentsToDocumentsCreatedDocumentNumber`),
			KEY `t7_documentsToDocumentsCreatedDocumentNumber` (`t7_documentsToDocumentsCreatedDocumentNumber`),
			KEY `t8_documentsToDocumentsCreatedDocumentNumber` (`t8_documentsToDocumentsCreatedDocumentNumber`),
			KEY `t9_documentsToDocumentsCreatedDocumentNumber` (`t9_documentsToDocumentsCreatedDocumentNumber`),
			KEY `t10_documentsToDocumentsCreatedDocumentNumber` (`t10_documentsToDocumentsCreatedDocumentNumber`),
			KEY `t11_documentsToDocumentsCreatedDocumentNumber` (`t11_documentsToDocumentsCreatedDocumentNumber`),
			KEY `t12_documentsToDocumentsCreatedDocumentNumber` (`t12_documentsToDocumentsCreatedDocumentNumber`),
			KEY `t13_documentsToDocumentsCreatedDocumentNumber` (`t13_documentsToDocumentsCreatedDocumentNumber`),
			KEY `t14_documentsToDocumentsCreatedDocumentNumber` (`t14_documentsToDocumentsCreatedDocumentNumber`),
			KEY `t15_documentsToDocumentsCreatedDocumentNumber` (`t15_documentsToDocumentsCreatedDocumentNumber`),
			KEY `t16_documentsToDocumentsCreatedDocumentNumber` (`t16_documentsToDocumentsCreatedDocumentNumber`),
			KEY `t17_documentsToDocumentsCreatedDocumentNumber` (`t16_documentsToDocumentsCreatedDocumentNumber`)
		) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='Dokument-Verknüpfungen';

		";
	$rs = $dbConnection->db_query($sql);

	$sql = " TRUNCATE TABLE `tempDocumentRelations`; ";
	$rs = $dbConnection->db_query($sql);

	$sql = "
		INSERT INTO `tempDocumentRelations`

			SELECT
				`t1`.`documentsToDocumentsOriginDocumentNumber`,
				`t2`.`documentsToDocumentsOriginDocumentNumber`,
				`t3`.`documentsToDocumentsOriginDocumentNumber`,
				`t4`.`documentsToDocumentsOriginDocumentNumber`,
				`t5`.`documentsToDocumentsOriginDocumentNumber`,
				`t6`.`documentsToDocumentsOriginDocumentNumber`,
				`t7`.`documentsToDocumentsOriginDocumentNumber`,
				`t8`.`documentsToDocumentsOriginDocumentNumber`,
				`t9`.`documentsToDocumentsOriginDocumentNumber`,
				`t10`.`documentsToDocumentsOriginDocumentNumber`,
				`t11`.`documentsToDocumentsOriginDocumentNumber`,
				`t12`.`documentsToDocumentsOriginDocumentNumber`,
				`t13`.`documentsToDocumentsOriginDocumentNumber`,
				`t14`.`documentsToDocumentsOriginDocumentNumber`,
				`t15`.`documentsToDocumentsOriginDocumentNumber`,
				`t16`.`documentsToDocumentsOriginDocumentNumber`,
				`t17`.`documentsToDocumentsOriginDocumentNumber`,

				`t1`.`documentsToDocumentsCreatedDocumentNumber`,
				`t2`.`documentsToDocumentsCreatedDocumentNumber`,
				`t3`.`documentsToDocumentsCreatedDocumentNumber`,
				`t4`.`documentsToDocumentsCreatedDocumentNumber`,
				`t5`.`documentsToDocumentsCreatedDocumentNumber`,
				`t6`.`documentsToDocumentsCreatedDocumentNumber`,
				`t7`.`documentsToDocumentsCreatedDocumentNumber`,
				`t8`.`documentsToDocumentsCreatedDocumentNumber`,
				`t9`.`documentsToDocumentsCreatedDocumentNumber`,
				`t10`.`documentsToDocumentsCreatedDocumentNumber`,
				`t11`.`documentsToDocumentsCreatedDocumentNumber`,
				`t12`.`documentsToDocumentsCreatedDocumentNumber`,
				`t13`.`documentsToDocumentsCreatedDocumentNumber`,
				`t14`.`documentsToDocumentsCreatedDocumentNumber`,
				`t15`.`documentsToDocumentsCreatedDocumentNumber`,
				`t16`.`documentsToDocumentsCreatedDocumentNumber`,
				`t17`.`documentsToDocumentsCreatedDocumentNumber`


		FROM `" .TABLE_DOCUMENTS_TO_DOCUMENTS . "` AS `t1`

		LEFT JOIN `" .TABLE_DOCUMENTS_TO_DOCUMENTS . "` AS `t2`
		ON(`t1`.`documentsToDocumentsCreatedDocumentNumber` = `t2`.`documentsToDocumentsOriginDocumentNumber` AND `t2`.`documentsToDocumentsRelationType` = 'convert' )
		LEFT JOIN `" .TABLE_DOCUMENTS_TO_DOCUMENTS . "` AS `t3`
		ON(`t2`.`documentsToDocumentsCreatedDocumentNumber` = `t3`.`documentsToDocumentsOriginDocumentNumber` AND `t3`.`documentsToDocumentsRelationType` = 'convert' )
		LEFT JOIN `" .TABLE_DOCUMENTS_TO_DOCUMENTS . "` AS `t4`
		ON(`t3`.`documentsToDocumentsCreatedDocumentNumber` = `t4`.`documentsToDocumentsOriginDocumentNumber` AND `t4`.`documentsToDocumentsRelationType` = 'convert' )
		LEFT JOIN `" .TABLE_DOCUMENTS_TO_DOCUMENTS . "` AS `t5`
		ON(`t4`.`documentsToDocumentsCreatedDocumentNumber` = `t5`.`documentsToDocumentsOriginDocumentNumber` AND `t5`.`documentsToDocumentsRelationType` = 'convert' )
		LEFT JOIN `" .TABLE_DOCUMENTS_TO_DOCUMENTS . "` AS `t6`
		ON(`t5`.`documentsToDocumentsCreatedDocumentNumber` = `t6`.`documentsToDocumentsOriginDocumentNumber` AND `t6`.`documentsToDocumentsRelationType` = 'convert' )
		LEFT JOIN `" .TABLE_DOCUMENTS_TO_DOCUMENTS . "` AS `t7`
		ON(`t6`.`documentsToDocumentsCreatedDocumentNumber` = `t7`.`documentsToDocumentsOriginDocumentNumber` AND `t7`.`documentsToDocumentsRelationType` = 'convert' )
		LEFT JOIN `" .TABLE_DOCUMENTS_TO_DOCUMENTS . "` AS `t8`
		ON(`t7`.`documentsToDocumentsCreatedDocumentNumber` = `t8`.`documentsToDocumentsOriginDocumentNumber` AND `t8`.`documentsToDocumentsRelationType` = 'convert' )
		LEFT JOIN `" .TABLE_DOCUMENTS_TO_DOCUMENTS . "` AS `t9`
		ON(`t8`.`documentsToDocumentsCreatedDocumentNumber` = `t9`.`documentsToDocumentsOriginDocumentNumber` AND `t9`.`documentsToDocumentsRelationType` = 'convert' )

		LEFT JOIN `" .TABLE_DOCUMENTS_TO_DOCUMENTS . "` AS `t10`
		ON(`t1`.`documentsToDocumentsOriginDocumentNumber` = `t10`.`documentsToDocumentsCreatedDocumentNumber` AND `t10`.`documentsToDocumentsRelationType` = 'convert' )
		LEFT JOIN `" .TABLE_DOCUMENTS_TO_DOCUMENTS . "` AS `t11`
		ON(`t10`.`documentsToDocumentsOriginDocumentNumber` = `t11`.`documentsToDocumentsCreatedDocumentNumber` AND `t11`.`documentsToDocumentsRelationType` = 'convert' )
		LEFT JOIN `" .TABLE_DOCUMENTS_TO_DOCUMENTS . "` AS `t12`
		ON(`t11`.`documentsToDocumentsOriginDocumentNumber` = `t12`.`documentsToDocumentsCreatedDocumentNumber` AND `t12`.`documentsToDocumentsRelationType` = 'convert' )
		LEFT JOIN `" .TABLE_DOCUMENTS_TO_DOCUMENTS . "` AS `t13`
		ON(`t12`.`documentsToDocumentsOriginDocumentNumber` = `t13`.`documentsToDocumentsCreatedDocumentNumber` AND `t13`.`documentsToDocumentsRelationType` = 'convert' )
		LEFT JOIN `" .TABLE_DOCUMENTS_TO_DOCUMENTS . "` AS `t14`
		ON(`t13`.`documentsToDocumentsOriginDocumentNumber` = `t14`.`documentsToDocumentsCreatedDocumentNumber` AND `t14`.`documentsToDocumentsRelationType` = 'convert' )
		LEFT JOIN `" .TABLE_DOCUMENTS_TO_DOCUMENTS . "` AS `t15`
		ON(`t14`.`documentsToDocumentsOriginDocumentNumber` = `t15`.`documentsToDocumentsCreatedDocumentNumber` AND `t15`.`documentsToDocumentsRelationType` = 'convert' )
		LEFT JOIN `" .TABLE_DOCUMENTS_TO_DOCUMENTS . "` AS `t16`
		ON(`t15`.`documentsToDocumentsOriginDocumentNumber` = `t16`.`documentsToDocumentsCreatedDocumentNumber` AND `t16`.`documentsToDocumentsRelationType` = 'convert' )
		LEFT JOIN `" .TABLE_DOCUMENTS_TO_DOCUMENTS . "` AS `t17`
		ON(`t16`.`documentsToDocumentsOriginDocumentNumber` = `t17`.`documentsToDocumentsCreatedDocumentNumber` AND `t17`.`documentsToDocumentsRelationType` = 'convert' )


		WHERE 1
			AND `t1`.`documentsToDocumentsRelationType` = 'convert'
	";

	$rs = $dbConnection->db_query($sql);


	$sql = " SELECT * FROM `tempDocumentRelations` ";
	$rs = $dbConnection->db_query($sql);

	echo '<h2>Dokument-Beziehungen holen</h2>';

	echo '<pre>';
	while ($ds = mysqli_fetch_assoc($rs)){
		$arrRelatedDocuments = array();
		$insertSql = '';
		foreach(array_keys($ds) as $field){
			if($ds[$field] != "") {
				$arrRelatedDocuments[substr($ds[$field], 0, 2)] = $ds[$field];
			}
		}
		#dd('arrRelatedDocuments');
		if(!empty($arrRelatedDocuments)){
			$insertSql = " INSERT INTO `" . TABLE_RELATED_DOCUMENTS . "` (`relatedDocuments_AN`, `relatedDocuments_AB`, `relatedDocuments_RE`, `relatedDocuments_LS`, `relatedDocuments_GU`, `relatedDocuments_MA`, `relatedDocuments_M1`, `relatedDocuments_M2`, `relatedDocuments_M3` ) VALUES ('" . $arrRelatedDocuments["AN"] . "', '" . $arrRelatedDocuments["AB"] . "', '" . $arrRelatedDocuments["RE"] . "', '" . $arrRelatedDocuments["LS"] . "', '" . $arrRelatedDocuments["GU"] . "', '" . $arrRelatedDocuments["MA"] . "', '" . $arrRelatedDocuments["M1"] . "', '" . $arrRelatedDocuments["M2"] . "', '" . $arrRelatedDocuments["M3"] . "'); ";
			$insertSql = preg_replace("/''/", "NULL", $insertSql);
echo $insertSql . "\n";
		}
	}
	echo '</pre>';
}
	############################################################
if($thisAction == 'GET_NONCONVERTED_DOCUMENTS'){

	echo '<h3>Unkonvertierte Dokumente holen</h3>';

	$sql = "
		SELECT
			CONCAT(
				'INSERT INTO `THIS_TABLE` (',
				'`relatedDocuments_',
				SUBSTRING(`tempTable`.`orderDocumentsNumber`, 1, 2),
				'` )',
				'VALUES (\'',
				`tempTable`.`orderDocumentsNumber`,
				'\')'

			) AS `insertDatas`,
			`tempTable`.`orderDocumentsNumber`,

			`tempTable`.`relatedDocuments_AN`,
			`tempTable`.`relatedDocuments_AB`,
			`tempTable`.`relatedDocuments_RE`,
			`tempTable`.`relatedDocuments_LS`,
			`tempTable`.`relatedDocuments_GU`,
			`tempTable`.`relatedDocuments_MA`,
			`tempTable`.`relatedDocuments_M1`,
			`tempTable`.`relatedDocuments_M2`,
			`tempTable`.`relatedDocuments_M3`

		FROM (

			SELECT
				`" . TABLE_ORDER_OFFERS . "`.`orderDocumentsNumber`,

				`" . TABLE_RELATED_DOCUMENTS . "`.`relatedDocuments_AN`,
				`" . TABLE_RELATED_DOCUMENTS . "`.`relatedDocuments_AB`,
				`" . TABLE_RELATED_DOCUMENTS . "`.`relatedDocuments_RE`,
				`" . TABLE_RELATED_DOCUMENTS . "`.`relatedDocuments_LS`,
				`" . TABLE_RELATED_DOCUMENTS . "`.`relatedDocuments_GU`,
				`" . TABLE_RELATED_DOCUMENTS . "`.`relatedDocuments_MA`,
				`" . TABLE_RELATED_DOCUMENTS . "`.`relatedDocuments_M1`,
				`" . TABLE_RELATED_DOCUMENTS . "`.`relatedDocuments_M2`,
				`" . TABLE_RELATED_DOCUMENTS . "`.`relatedDocuments_M3`
				FROM `" . TABLE_ORDER_OFFERS . "`
				LEFT JOIN `" . TABLE_RELATED_DOCUMENTS . "`
				ON(`" . TABLE_ORDER_OFFERS . "`.`orderDocumentsNumber` = `" . TABLE_RELATED_DOCUMENTS . "`.`relatedDocuments_AN`)
				WHERE 1

			UNION

			SELECT
				`" .TABLE_ORDER_CONFIRMATIONS . "`.`orderDocumentsNumber`,

				`" . TABLE_RELATED_DOCUMENTS . "`.`relatedDocuments_AN`,
				`" . TABLE_RELATED_DOCUMENTS . "`.`relatedDocuments_AB`,
				`" . TABLE_RELATED_DOCUMENTS . "`.`relatedDocuments_RE`,
				`" . TABLE_RELATED_DOCUMENTS . "`.`relatedDocuments_LS`,
				`" . TABLE_RELATED_DOCUMENTS . "`.`relatedDocuments_GU`,
				`" . TABLE_RELATED_DOCUMENTS . "`.`relatedDocuments_MA`,
				`" . TABLE_RELATED_DOCUMENTS . "`.`relatedDocuments_M1`,
				`" . TABLE_RELATED_DOCUMENTS . "`.`relatedDocuments_M2`,
				`" . TABLE_RELATED_DOCUMENTS . "`.`relatedDocuments_M3`
				FROM `" .TABLE_ORDER_CONFIRMATIONS . "`
				LEFT JOIN `" . TABLE_RELATED_DOCUMENTS . "`
				ON(`" .TABLE_ORDER_CONFIRMATIONS . "`.`orderDocumentsNumber` = `" . TABLE_RELATED_DOCUMENTS . "`.`relatedDocuments_AB`)
				WHERE 1

			UNION

			SELECT
				`" . TABLE_ORDER_DELIVERIES . "`.`orderDocumentsNumber`,

				`" . TABLE_RELATED_DOCUMENTS . "`.`relatedDocuments_AN`,
				`" . TABLE_RELATED_DOCUMENTS . "`.`relatedDocuments_AB`,
				`" . TABLE_RELATED_DOCUMENTS . "`.`relatedDocuments_RE`,
				`" . TABLE_RELATED_DOCUMENTS . "`.`relatedDocuments_LS`,
				`" . TABLE_RELATED_DOCUMENTS . "`.`relatedDocuments_GU`,
				`" . TABLE_RELATED_DOCUMENTS . "`.`relatedDocuments_MA`,
				`" . TABLE_RELATED_DOCUMENTS . "`.`relatedDocuments_M1`,
				`" . TABLE_RELATED_DOCUMENTS . "`.`relatedDocuments_M2`,
				`" . TABLE_RELATED_DOCUMENTS . "`.`relatedDocuments_M3`
				FROM `" . TABLE_ORDER_DELIVERIES . "`
				LEFT JOIN `" . TABLE_RELATED_DOCUMENTS . "`
				ON(`" . TABLE_ORDER_DELIVERIES . "`.`orderDocumentsNumber` = `" . TABLE_RELATED_DOCUMENTS . "`.`relatedDocuments_LS`)
				WHERE 1

			UNION

			SELECT
				`" . TABLE_ORDER_INVOICES . "`.`orderDocumentsNumber`,

				`" . TABLE_RELATED_DOCUMENTS . "`.`relatedDocuments_AN`,
				`" . TABLE_RELATED_DOCUMENTS . "`.`relatedDocuments_AB`,
				`" . TABLE_RELATED_DOCUMENTS . "`.`relatedDocuments_RE`,
				`" . TABLE_RELATED_DOCUMENTS . "`.`relatedDocuments_LS`,
				`" . TABLE_RELATED_DOCUMENTS . "`.`relatedDocuments_GU`,
				`" . TABLE_RELATED_DOCUMENTS . "`.`relatedDocuments_MA`,
				`" . TABLE_RELATED_DOCUMENTS . "`.`relatedDocuments_M1`,
				`" . TABLE_RELATED_DOCUMENTS . "`.`relatedDocuments_M2`,
				`" . TABLE_RELATED_DOCUMENTS . "`.`relatedDocuments_M3`
				FROM `" . TABLE_ORDER_INVOICES . "`
				LEFT JOIN `" . TABLE_RELATED_DOCUMENTS . "`
				ON(`" . TABLE_ORDER_INVOICES . "`.`orderDocumentsNumber` = `" . TABLE_RELATED_DOCUMENTS . "`.`relatedDocuments_RE`)
				WHERE 1

			UNION

			SELECT
				`" .TABLE_ORDER_CREDITS . "`.`orderDocumentsNumber`,

				`" . TABLE_RELATED_DOCUMENTS . "`.`relatedDocuments_AN`,
				`" . TABLE_RELATED_DOCUMENTS . "`.`relatedDocuments_AB`,
				`" . TABLE_RELATED_DOCUMENTS . "`.`relatedDocuments_RE`,
				`" . TABLE_RELATED_DOCUMENTS . "`.`relatedDocuments_LS`,
				`" . TABLE_RELATED_DOCUMENTS . "`.`relatedDocuments_GU`,
				`" . TABLE_RELATED_DOCUMENTS . "`.`relatedDocuments_MA`,
				`" . TABLE_RELATED_DOCUMENTS . "`.`relatedDocuments_M1`,
				`" . TABLE_RELATED_DOCUMENTS . "`.`relatedDocuments_M2`,
				`" . TABLE_RELATED_DOCUMENTS . "`.`relatedDocuments_M3`
				FROM `" .TABLE_ORDER_CREDITS . "`
				LEFT JOIN `" . TABLE_RELATED_DOCUMENTS . "`
				ON(`" .TABLE_ORDER_CREDITS . "`.`orderDocumentsNumber` = `" . TABLE_RELATED_DOCUMENTS . "`.`relatedDocuments_GU`)
				WHERE 1
			UNION

			SELECT
				`" . TABLE_ORDER_REMINDERS . "`.`orderDocumentsNumber`,

				`" . TABLE_RELATED_DOCUMENTS . "`.`relatedDocuments_AN`,
				`" . TABLE_RELATED_DOCUMENTS . "`.`relatedDocuments_AB`,
				`" . TABLE_RELATED_DOCUMENTS . "`.`relatedDocuments_RE`,
				`" . TABLE_RELATED_DOCUMENTS . "`.`relatedDocuments_LS`,
				`" . TABLE_RELATED_DOCUMENTS . "`.`relatedDocuments_GU`,
				`" . TABLE_RELATED_DOCUMENTS . "`.`relatedDocuments_MA`,
				`" . TABLE_RELATED_DOCUMENTS . "`.`relatedDocuments_M1`,
				`" . TABLE_RELATED_DOCUMENTS . "`.`relatedDocuments_M2`,
				`" . TABLE_RELATED_DOCUMENTS . "`.`relatedDocuments_M3`
				FROM `" . TABLE_ORDER_REMINDERS . "`
				LEFT JOIN `" . TABLE_RELATED_DOCUMENTS . "`
				ON(`" . TABLE_ORDER_REMINDERS . "`.`orderDocumentsNumber` = `" . TABLE_RELATED_DOCUMENTS . "`.`relatedDocuments_MA`)
				WHERE 1

			UNION

			SELECT
				`" . TABLE_ORDER_FIRST_DEMANDS . "`.`orderDocumentsNumber`,

				`" . TABLE_RELATED_DOCUMENTS . "`.`relatedDocuments_AN`,
				`" . TABLE_RELATED_DOCUMENTS . "`.`relatedDocuments_AB`,
				`" . TABLE_RELATED_DOCUMENTS . "`.`relatedDocuments_RE`,
				`" . TABLE_RELATED_DOCUMENTS . "`.`relatedDocuments_LS`,
				`" . TABLE_RELATED_DOCUMENTS . "`.`relatedDocuments_GU`,
				`" . TABLE_RELATED_DOCUMENTS . "`.`relatedDocuments_MA`,
				`" . TABLE_RELATED_DOCUMENTS . "`.`relatedDocuments_M1`,
				`" . TABLE_RELATED_DOCUMENTS . "`.`relatedDocuments_M2`,
				`" . TABLE_RELATED_DOCUMENTS . "`.`relatedDocuments_M3`
				FROM `" . TABLE_ORDER_FIRST_DEMANDS . "`
				LEFT JOIN `" . TABLE_RELATED_DOCUMENTS . "`
				ON(`" . TABLE_ORDER_FIRST_DEMANDS . "`.`orderDocumentsNumber` = `" . TABLE_RELATED_DOCUMENTS . "`.`relatedDocuments_M1`)
				WHERE 1

			UNION

			SELECT
				`" . TABLE_ORDER_SECOND_DEMANDS . "`.`orderDocumentsNumber`,

				`" . TABLE_RELATED_DOCUMENTS . "`.`relatedDocuments_AN`,
				`" . TABLE_RELATED_DOCUMENTS . "`.`relatedDocuments_AB`,
				`" . TABLE_RELATED_DOCUMENTS . "`.`relatedDocuments_RE`,
				`" . TABLE_RELATED_DOCUMENTS . "`.`relatedDocuments_LS`,
				`" . TABLE_RELATED_DOCUMENTS . "`.`relatedDocuments_GU`,
				`" . TABLE_RELATED_DOCUMENTS . "`.`relatedDocuments_MA`,
				`" . TABLE_RELATED_DOCUMENTS . "`.`relatedDocuments_M1`,
				`" . TABLE_RELATED_DOCUMENTS . "`.`relatedDocuments_M2`,
				`" . TABLE_RELATED_DOCUMENTS . "`.`relatedDocuments_M3`
				FROM `" . TABLE_ORDER_SECOND_DEMANDS . "`
				LEFT JOIN `" . TABLE_RELATED_DOCUMENTS . "`
				ON(`" . TABLE_ORDER_SECOND_DEMANDS . "`.`orderDocumentsNumber` = `" . TABLE_RELATED_DOCUMENTS . "`.`relatedDocuments_M2`)
				WHERE 1

			UNION

			SELECT
				`" . TABLE_ORDER_THIRD_DEMANDS . "`.`orderDocumentsNumber`,

				`" . TABLE_RELATED_DOCUMENTS . "`.`relatedDocuments_AN`,
				`" . TABLE_RELATED_DOCUMENTS . "`.`relatedDocuments_AB`,
				`" . TABLE_RELATED_DOCUMENTS . "`.`relatedDocuments_RE`,
				`" . TABLE_RELATED_DOCUMENTS . "`.`relatedDocuments_LS`,
				`" . TABLE_RELATED_DOCUMENTS . "`.`relatedDocuments_GU`,
				`" . TABLE_RELATED_DOCUMENTS . "`.`relatedDocuments_MA`,
				`" . TABLE_RELATED_DOCUMENTS . "`.`relatedDocuments_M1`,
				`" . TABLE_RELATED_DOCUMENTS . "`.`relatedDocuments_M2`,
				`" . TABLE_RELATED_DOCUMENTS . "`.`relatedDocuments_M3`
				FROM `" . TABLE_ORDER_THIRD_DEMANDS . "`
				LEFT JOIN `" . TABLE_RELATED_DOCUMENTS . "`
				ON(`" . TABLE_ORDER_THIRD_DEMANDS . "`.`orderDocumentsNumber` = `" . TABLE_RELATED_DOCUMENTS . "`.`relatedDocuments_M3`)
				WHERE 1

		) AS `tempTable`

		WHERE 1
			AND `tempTable`.`relatedDocuments_AN` IS NULL
			AND `tempTable`.`relatedDocuments_AB` IS NULL
			AND `tempTable`.`relatedDocuments_RE` IS NULL
			AND `tempTable`.`relatedDocuments_LS` IS NULL
			AND `tempTable`.`relatedDocuments_GU` IS NULL
			AND `tempTable`.`relatedDocuments_MA` IS NULL
			AND `tempTable`.`relatedDocuments_M1` IS NULL
			AND `tempTable`.`relatedDocuments_M2` IS NULL
			AND `tempTable`.`relatedDocuments_M3` IS NULL
	";

	$rs = $dbConnection->db_query($sql);

	echo '<pre>';
	while($ds=mysqli_fetch_assoc($rs)){
		$arrRelatedDocuments = array();
		$insertSql = '';

		$arrRelatedDocuments[substr($ds["orderDocumentsNumber"], 0, 2)] = $ds["orderDocumentsNumber"];

		if(!empty($arrRelatedDocuments)){
			$insertSql = " INSERT INTO `" . TABLE_RELATED_DOCUMENTS . "` (`relatedDocuments_AN`, `relatedDocuments_AB`, `relatedDocuments_RE`, `relatedDocuments_LS`, `relatedDocuments_GU`, `relatedDocuments_MA`, `relatedDocuments_M1`, `relatedDocuments_M2`, `relatedDocuments_M3` ) VALUES ('" . $arrRelatedDocuments["AN"] . "', '" . $arrRelatedDocuments["AB"] . "', '" . $arrRelatedDocuments["RE"] . "', '" . $arrRelatedDocuments["LS"] . "', '" . $arrRelatedDocuments["GU"] . "', '" . $arrRelatedDocuments["MA"] . "', '" . $arrRelatedDocuments["M1"] . "', '" . $arrRelatedDocuments["M2"] . "', '" . $arrRelatedDocuments["M3"] . "'); ";
			$insertSql = preg_replace("/''/", "NULL", $insertSql);
echo $insertSql . "\n";
		}
	}
	echo '</pre>';

}
?>