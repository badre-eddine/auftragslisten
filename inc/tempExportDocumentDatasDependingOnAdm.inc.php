<?php
	$sql = "
			SELECT
				`bctr_orderConfirmations`.`orderDocumentsID`,
				`bctr_orderConfirmations`.`orderDocumentsNumber`,
				`bctr_orderConfirmations`.`orderDocumentsType`,
				`bctr_orderConfirmations`.`orderDocumentsOrdersIDs`,
				`bctr_orderConfirmations`.`orderDocumentsCustomerNumber`,
				`bctr_orderConfirmations`.`orderDocumentsCustomersOrderNumber`,
				`bctr_orderConfirmations`.`orderDocumentsProcessingDate`,
				`bctr_orderConfirmations`.`orderDocumentsDocumentDate`,
				`bctr_orderConfirmations`.`orderDocumentsOrderDate`,
				`bctr_orderConfirmations`.`orderDocumentsInvoiceDate`,
				`bctr_orderConfirmations`.`orderDocumentsCreditDate`,
				`bctr_orderConfirmations`.`orderDocumentsDeliveryDate`,
				`bctr_orderConfirmations`.`orderDocumentsBindingDate`,
				`bctr_orderConfirmations`.`orderDocumentsDebitingDate`,
				`bctr_orderConfirmations`.`orderDocumentsSalesman`,
				`bctr_orderConfirmations`.`orderDocumentsKommission`,
				`bctr_orderConfirmations`.`orderDocumentsAddressCompany`,
				`bctr_orderConfirmations`.`orderDocumentsAddressName`,
				`bctr_orderConfirmations`.`orderDocumentsAddressStreet`,
				`bctr_orderConfirmations`.`orderDocumentsAddressStreetNumber`,
				`bctr_orderConfirmations`.`orderDocumentsAddressZipcode`,
				`bctr_orderConfirmations`.`orderDocumentsAddressCity`,
				`bctr_orderConfirmations`.`orderDocumentsAddressCountry`,
				`bctr_orderConfirmations`.`orderDocumentsAddressMail`,
				`bctr_orderConfirmations`.`orderDocumentsSumPrice`,
				`bctr_orderConfirmations`.`orderDocumentsDiscount`,
				`bctr_orderConfirmations`.`orderDocumentsDiscountType`,
				`bctr_orderConfirmations`.`orderDocumentsDiscountPercent`,
				`bctr_orderConfirmations`.`orderDocumentsMwst`,
				`bctr_orderConfirmations`.`orderDocumentsMwstPrice`,
				`bctr_orderConfirmations`.`orderDocumentsMwstType`,
				`bctr_orderConfirmations`.`orderDocumentsShippingCosts`,
				`bctr_orderConfirmations`.`orderDocumentsShippingCostsPackages`,
				`bctr_orderConfirmations`.`orderDocumentsShippingCostsPerPackage`,
				`bctr_orderConfirmations`.`orderDocumentsPackagingCosts`,
				`bctr_orderConfirmations`.`orderDocumentsCashOnDelivery`,
				`bctr_orderConfirmations`.`orderDocumentsTotalPrice`,
				`bctr_orderConfirmations`.`orderDocumentsBankAccount`,
				`bctr_orderConfirmations`.`orderDocumentsPaymentCondition`,
				`bctr_orderConfirmations`.`orderDocumentsPaymentType`,
				`bctr_orderConfirmations`.`orderDocumentsSkonto`,
				`bctr_orderConfirmations`.`orderDocumentsDeliveryType`,
				`bctr_orderConfirmations`.`orderDocumentsDeliveryTypeNumber`,
				`bctr_orderConfirmations`.`orderDocumentsSubject`,
				`bctr_orderConfirmations`.`orderDocumentsContent`,
				`bctr_orderConfirmations`.`orderDocumentsDocumentPath`,
				`bctr_orderConfirmations`.`orderDocumentsTimestamp`,
				`bctr_orderConfirmations`.`orderDocumentsStatus`,
				`bctr_orderConfirmations`.`orderDocumentsSendMail`,
				`bctr_orderConfirmations`.`orderDocumentsInterestPercent`,
				`bctr_orderConfirmations`.`orderDocumentsInterestPrice`,
				`bctr_orderConfirmations`.`orderDocumentsChargesPrice`,
				`bctr_orderConfirmations`.`orderDocumentsDeadlineDate`,
				`bctr_orderConfirmations`.`orderDocumentsCustomerGroupID`,

				`bctr_orderConfirmationsDetails`.`orderDocumentDetailID`,
				`bctr_orderConfirmationsDetails`.`orderDocumentDetailDocumentID`,
				`bctr_orderConfirmationsDetails`.`orderDocumentDetailOrderID`,
				`bctr_orderConfirmationsDetails`.`orderDocumentDetailOrderType`,
				`bctr_orderConfirmationsDetails`.`orderDocumentDetailPos`,
				`bctr_orderConfirmationsDetails`.`orderDocumentDetailProductNumber`,
				`bctr_orderConfirmationsDetails`.`orderDocumentDetailProductName`,
				`bctr_orderConfirmationsDetails`.`orderDocumentDetailProductKategorieID`,
				`bctr_orderConfirmationsDetails`.`orderDocumentDetailProductQuantity`,
				`bctr_orderConfirmationsDetails`.`orderDocumentDetailProductColorsText`,
				`bctr_orderConfirmationsDetails`.`orderDocumentDetailProductColorsCount`,
				`bctr_orderConfirmationsDetails`.`orderDocumentDetailsWithBorder`,
				`bctr_orderConfirmationsDetails`.`orderDocumentDetailKommission`,
				`bctr_orderConfirmationsDetails`.`orderDocumentDetailPrintText`,
				`bctr_orderConfirmationsDetails`.`orderDocumentDetailNotiz`,
				`bctr_orderConfirmationsDetails`.`orderDocumentDetailProductSinglePrice`,
				`bctr_orderConfirmationsDetails`.`orderDocumentDetailProductTotalPrice`,
				`bctr_orderConfirmationsDetails`.`orderDocumentDetailMwst`,
				`bctr_orderConfirmationsDetails`.`orderDocumentDetailDiscount`,
				`bctr_orderConfirmationsDetails`.`orderDocumentDetailDirectSale`,
				`bctr_orderConfirmationsDetails`.`orderDocumentDetailPosType`,
				`bctr_orderConfirmationsDetails`.`orderDocumentDetailGroupingID`

			FROM `bctr_orderConfirmations`

			LEFT JOIN `bctr_orderConfirmationsDetails`
			ON(`bctr_orderConfirmations`.`orderDocumentsID` = `bctr_orderConfirmationsDetails`.`orderDocumentDetailDocumentID`)

			WHERE 1
				AND `bctr_orderConfirmations`.`orderDocumentsSalesman` = '5145'
				/*AND `bctr_orderConfirmationsDetails`.`orderDocumentDetailPosType` = 'product'*/

			ORDER BY
				`bctr_orderConfirmations`.`orderDocumentsNumber` ASC,
				`bctr_orderConfirmationsDetails`.`orderDocumentDetailGroupingID`,
				FIELD(`bctr_orderConfirmationsDetails`.`orderDocumentDetailPosType`, 'product', 'additionalProducts', 'printCosts', 'expressCosts')
LIMIT 20
		";
	$rs = $dbConnection->db_query($sql, $db_open);

	$marker = '';

	$fileContentCsv = '';

	$countRows = 0;
	$thisFileName = "5145_test.csv";
	if(file_exists($thisFileName)){
		unlink($thisFileName);
	}

	$fp = fopen($thisFileName, "a+");
	$fileContentCsv .= 'DATUM';
	$fileContentCsv .= "\t";
	$fileContentCsv .= "DOK-NR";
	$fileContentCsv .= "\t";
	$fileContentCsv .= "K-NR";
	$fileContentCsv .= "\t";
	$fileContentCsv .= "FIRMA";
	$fileContentCsv .= "\t";
	$fileContentCsv .= "NAME";
	$fileContentCsv .= "\t";
	$fileContentCsv .= "STRASSE";
	$fileContentCsv .= "\t";
	$fileContentCsv .= "PLZ";
	$fileContentCsv .= "\t";
	$fileContentCsv .= "ORT";
	$fileContentCsv .= "\t";


	$fileContentCsv .= "SUMME";
	$fileContentCsv .= "\t";
	$fileContentCsv .= "RABATT";
	$fileContentCsv .= "\t";
	$fileContentCsv .= "RABATT-ART";
	$fileContentCsv .= "\t";
	$fileContentCsv .= "RABATT-%";
	$fileContentCsv .= "\t";
	$fileContentCsv .= "MWST-%";
	$fileContentCsv .= "\t";
	$fileContentCsv .= "MWST";
	$fileContentCsv .= "\t";
	$fileContentCsv .= "MWST-TYP";
	$fileContentCsv .= "\t";
	$fileContentCsv .= "VERSAND";
	$fileContentCsv .= "\t";
	$fileContentCsv .= "PAKETE";
	$fileContentCsv .= "\t";
	$fileContentCsv .= "VERSAND PRO PAKET";
	$fileContentCsv .= "\t";
	$fileContentCsv .= "VERPACKUNG";
	$fileContentCsv .= "\t";
	$fileContentCsv .= "NACHNAHME";
	$fileContentCsv .= "\t";
	$fileContentCsv .= "TOTAL";
	$fileContentCsv .= "\t";


	$fileContentCsv .= "MENGE";
	$fileContentCsv .= "\t";
	$fileContentCsv .= "ART-NR";
	$fileContentCsv .= "\t";
	$fileContentCsv .= "ARTIKEL";
	$fileContentCsv .= "\t";
	$fileContentCsv .= "STCKPREIS";
	$fileContentCsv .= "\t";
	$fileContentCsv .= "GES-PREIS";
	$fileContentCsv .= "\t";
	$fileContentCsv .= "DV";
	$fileContentCsv .= "\t";


	$fileContentCsv .= "DR-MENGE";
	$fileContentCsv .= "\t";
	$fileContentCsv .= "DR-ART-NR";
	$fileContentCsv .= "\t";
	$fileContentCsv .= "ARTIKEL";
	$fileContentCsv .= "\t";
	$fileContentCsv .= "STCKPREIS";
	$fileContentCsv .= "\t";
	$fileContentCsv .= "GES-PREIS";
	$fileContentCsv .= "\t";

	fwrite($fp, $fileContentCsv);
	#$fileContentCsv = '';

	while($ds = mysqli_fetch_assoc($rs)){
		#$fileContentCsv = '';
		if($ds["orderDocumentDetailGroupingID"] != $marker){
			$marker = $ds["orderDocumentDetailGroupingID"];
			if($countRows > 0){ $fileContentCsv .= "\n"; }
			$fileContentCsv .= formatDate($ds["orderDocumentsDocumentDate"], "display");
			$fileContentCsv .= "\t";
			$fileContentCsv .= $ds["orderDocumentsNumber"];
			$fileContentCsv .= "\t";
			$fileContentCsv .= $ds["orderDocumentsCustomerNumber"];
			$fileContentCsv .= "\t";
			$fileContentCsv .= $ds["orderDocumentsAddressCompany"];
			$fileContentCsv .= "\t";
			$fileContentCsv .= $ds["orderDocumentsAddressName"];
			$fileContentCsv .= "\t";
			$fileContentCsv .= $ds["orderDocumentsAddressStreet"] . ' ' . $ds["orderDocumentsAddressStreetNumber"];
			$fileContentCsv .= "\t";
			$fileContentCsv .= $ds["orderDocumentsAddressZipcode"];
			$fileContentCsv .= "\t";
			$fileContentCsv .= $ds["orderDocumentsAddressCity"];
			$fileContentCsv .= "\t";


			$fileContentCsv .= $ds["orderDocumentsSumPrice"];
			$fileContentCsv .= "\t";
			$fileContentCsv .= $ds["orderDocumentsDiscount"];
			$fileContentCsv .= "\t";
			$fileContentCsv .= $ds["orderDocumentsDiscountType"];
			$fileContentCsv .= "\t";
			$fileContentCsv .= $ds["orderDocumentsDiscountPercent"];
			$fileContentCsv .= "\t";
			$fileContentCsv .= $ds["orderDocumentsMwst"];
			$fileContentCsv .= "\t";
			$fileContentCsv .= $ds["orderDocumentsMwstPrice"];
			$fileContentCsv .= "\t";
			$fileContentCsv .= $ds["orderDocumentsMwstType"];
			$fileContentCsv .= "\t";
			$fileContentCsv .= $ds["orderDocumentsShippingCosts"];
			$fileContentCsv .= "\t";
			$fileContentCsv .= $ds["orderDocumentsShippingCostsPackages"];
			$fileContentCsv .= "\t";
			$fileContentCsv .= $ds["orderDocumentsShippingCostsPerPackage"];
			$fileContentCsv .= "\t";
			$fileContentCsv .= $ds["orderDocumentsPackagingCosts"];
			$fileContentCsv .= "\t";
			$fileContentCsv .= $ds["orderDocumentsCashOnDelivery"];
			$fileContentCsv .= "\t";
			$fileContentCsv .= $ds["orderDocumentsTotalPrice"];
			$fileContentCsv .= "\t";


			$fileContentCsv .= $ds["orderDocumentDetailProductQuantity"];
			$fileContentCsv .= "\t";
			$fileContentCsv .= $ds["orderDocumentDetailProductNumber"];
			$fileContentCsv .= "\t";
			$fileContentCsv .= $ds["orderDocumentDetailProductName"];
			$fileContentCsv .= "\t";
			$fileContentCsv .= $ds["orderDocumentDetailProductSinglePrice"];
			$fileContentCsv .= "\t";
			$fileContentCsv .= $ds["orderDocumentDetailProductTotalPrice"];
			$fileContentCsv .= "\t";
			$fileContentCsv .= $ds["orderDocumentDetailDirectSale"];
			$fileContentCsv .= "\t";

		}
		else {
			$fileContentCsv .= $ds["orderDocumentDetailProductQuantity"];
			$fileContentCsv .= "\t";
			$fileContentCsv .= $ds["orderDocumentDetailProductNumber"];
			$fileContentCsv .= "\t";
			$fileContentCsv .= $ds["orderDocumentDetailProductName"];
			$fileContentCsv .= "\t";
			$fileContentCsv .= $ds["orderDocumentDetailProductSinglePrice"];
			$fileContentCsv .= "\t";
			$fileContentCsv .= $ds["orderDocumentDetailProductTotalPrice"];
			$fileContentCsv .= "\t";
		}
		fwrite($fp, $fileContentCsv);
		#$fileContentCsv = '';
		$countRows ++;
	}
	fclose($fp);

	dd('fileContentCsv');
?>