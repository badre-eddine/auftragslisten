<?php
	// http://evatr.bff-online.de/eVatR/xmlrpc/schnittstelle
	// $pathServer = "https://evatr.bff-online.de/";

	/*
	$UstId_1    = 'DE123456789';
	$UstId_2    = 'AB123456789012';
	$Firmenname = 'Firmenname einschl. Rechtsform';
	$Ort        = 'Ort';
	$PLZ        = '1234567';
	$Strasse    = 'Strasse und Hausnummer';
	$Druck      = 'nein';
	*/

	/*
	Parameter	Datentyp	Case Sensitive	Beschreibung
	UstId_1		String		ja				Ihre deutsche USt-IdNr. 	X 	X
	UstId_2 	String		ja				Anzufragende ausländische USt-IdNr. 	X 	X
	Firmenname 	String		nein			Name der anzufragenden Firma einschl. Rechtsform 	  	X
	Ort			String		nein			Ort der anzufragenden Firma 	  	X
	PLZ			String		nein			Postleitzahl der anzufragenden Firma 	  	  	X
	Strasse 	String		nein			Strasse und Hausnummer der anzufragenden Firma 	  	  	X
	Druck		String		nein			ja = mit amtlicher Bestätigungsmitteilung 	  	  	X
	*/

	function checkForeignCountryVatID($mode, $UstId_1, $UstId_2, $Firmenname, $Ort, $PLZ, $Strasse, $Druck){
		global $_POST, $errorMessage, $successMessage;
		// $mode : 'IXR' | 'HTTPS'
		$pathServer = PAGE_CHECK_VAT_ID;

		$arrStatusMessages = array(
					"200" => "Die angefragte USt-IdNr. ist gültig.",
					"201" => "Die angefragte USt-IdNr. ist ungültig.",
					"202" => "Die angefragte USt-IdNr. ist ungültig. Sie ist nicht in der Unternehmerdatei des betreffenden EU-Mitgliedstaates registriert. Hinweis: Ihr Geschäftspartner kann seine gültige USt-IdNr. bei der für ihn zuständigen Finanzbehörde in Erfahrung bringen. Möglicherweise muss er einen Antrag stellen, damit seine USt-IdNr. in die Datenbank aufgenommen wird.",
					"203" => "Die angefragte USt-IdNr. ist ungültig. Sie ist erst ab dem ... gültig (siehe Feld 'Gueltig_ab').",
					"204" => "Die angefragte USt-IdNr. ist ungültig. Sie war im Zeitraum von ... bis ... gültig (siehe Feld 'Gueltig_ab' und 'Gueltig_bis').",
					"205" => "Ihre Anfrage kann derzeit durch den angefragten EU-Mitgliedstaat oder aus anderen Gründen nicht beantwortet werden. Bitte versuchen Sie es später noch einmal. Bei wiederholten Problemen wenden Sie sich bitte an das Bundeszentralamt für Steuern - Dienstsitz Saarlouis.",
					"206" => "Ihre deutsche USt-IdNr. ist ungültig. Eine Bestätigungsanfrage ist daher nicht möglich. Den Grund hierfür können Sie beim Bundeszentralamt für Steuern - Dienstsitz Saarlouis - erfragen.",
					"207" => "Ihnen wurde die deutsche USt-IdNr. ausschliesslich zu Zwecken der Besteuerung des innergemeinschaftlichen Erwerbs erteilt. Sie sind somit nicht berechtigt, Bestätigungsanfragen zu stellen.",
					"208" => "Für die von Ihnen angefragte USt-IdNr. läuft gerade eine Anfrage von einem anderen Nutzer. Eine Bearbeitung ist daher nicht möglich. Bitte versuchen Sie es später noch einmal.",
					"209" => "Die angefragte USt-IdNr. ist ungültig. Sie entspricht nicht dem Aufbau der für diesen EU-Mitgliedstaat gilt. ( Aufbau der USt-IdNr. aller EU-Länder)",
					"210" => "Die angefragte USt-IdNr. ist ungültig. Sie entspricht nicht den Prüfziffernregeln die für diesen EU-Mitgliedstaat gelten.",
					"211" => "Die angefragte USt-IdNr. ist ungültig. Sie enthält unzulässige Zeichen (wie z.B. Leerzeichen oder Punkt oder Bindestrich usw.).",
					"212" => "Die angefragte USt-IdNr. ist ungültig. Sie enthält ein unzulässiges Länderkennzeichen.",
					"213" => "Die Abfrage einer deutschen USt-IdNr. ist nicht möglich.",
					"214" => "Ihre deutsche USt-IdNr. ist fehlerhaft. Sie beginnt mit 'DE' gefolgt von 9 Ziffern.",
					"215" => "Ihre Anfrage enthält nicht alle notwendigen Angaben für eine einfache Bestätigungsanfrage (Ihre deutsche USt-IdNr. und die ausl. USt-IdNr.). Ihre Anfrage kann deshalb nicht bearbeitet werden.",
					"216" => "Ihre Anfrage enthält nicht alle notwendigen Angaben für eine qualifizierte Bestätigungsanfrage (Ihre deutsche USt-IdNr., die ausl. USt-IdNr., Firmenname einschl. Rechtsform und Ort). Es wurde eine einfache Bestätigungsanfrage durchgeführt mit folgenden Ergebnis: Die angefragte USt-IdNr. ist gültig.",
					"217" => "Bei der Verarbeitung der Daten aus dem angefragten EU-Mitgliedstaat ist ein Fehler aufgetreten. Ihre Anfrage kann deshalb nicht bearbeitet werden.",
					"218" => "Eine qualifizierte Bestätigung ist zur Zeit nicht möglich. Es wurde eine einfache Bestätigungsanfrage mit folgendem Ergebnis durchgeführt: Die angefragte USt-IdNr. ist gültig.",
					"219" => "Bei der Durchführung der qualifizierten Bestätigungsanfrage ist ein Fehler aufgetreten. Es wurde eine einfache Bestätigungsanfrage mit folgendem Ergebnis durchgeführt:Die angefragte USt-IdNr. ist gültig.",
					"220" => "Bei der Anforderung der amtlichen Bestätigungsmitteilung ist ein Fehler aufgetreten. Sie werden kein Schreiben erhalten.",
					"221" => "Die Anfragedaten enthalten nicht alle notwendigen Parameter oder einen ungültigen Datentyp. Weitere Informationen erhalten Sie bei den Hinweisen zum Schnittstelle - Aufruf.",
					"999" => "Eine Bearbeitung Ihrer Anfrage ist zurzeit nicht möglich. Bitte versuchen Sie es später noch einmal. ",
		);


		$UstId_1 = formatVatID($UstId_1);
		$UstId_2 = formatVatID($UstId_2);

		if($UstId_1 != '' && $UstId_2 != ''){
			if($mode == 'HTTPS'){
				$getUrl = $pathServer . "evatrRPC?UstId_1={###USTID_1###}&UstId_2={###USTID_2###}&Firmenname={###FIRMENNAME###}&Ort={###ORT###}&PLZ={###PLZ###}&Strasse={###STRASSE###}&Druck={###DRUCK###}";

				$pattern = '/{###USTID_1###}/';
				$replace = $UstId_1;
				$getUrl = preg_replace($pattern, $replace, $getUrl);

				$pattern = '/{###USTID_2###}/';
				$replace = $UstId_2;
				$getUrl = preg_replace($pattern, $replace, $getUrl);

				$pattern = '/{###FIRMENNAME###}/';
				$replace = $Firmenname;
				$getUrl = preg_replace($pattern, $replace, $getUrl);

				$pattern = '/{###ORT###}/';
				$replace = $Ort;
				$getUrl = preg_replace($pattern, $replace, $getUrl);

				$pattern = '/{###PLZ###}/';
				$replace = $PLZ;
				$getUrl = preg_replace($pattern, $replace, $getUrl);

				$pattern = '/{###STRASSE###}/';
				$replace = $Strasse;
				$getUrl = preg_replace($pattern, $replace, $getUrl);

				$pattern = '/{###DRUCK###}/';
				$replace = $Druck;
				$getUrl = preg_replace($pattern, $replace, $getUrl);

				$outString = implode("", file($getUrl));

			}
			else if($mode == "IXR"){
				echo 'mode: ' . $mode . '<br>';
				require_once(PATH_PHP_INCUTIO_XML_RPC);
				echo 'path: ' . file_exists(PATH_PHP_INCUTIO_XML_RPC) . '<br>';

				$client = new IXR_Client($pathServer);

				if(!$client->query('evatrRPC',
						$UstId_1,
						$UstId_2,
						$Firmenname,
						$Ort,
						$PLZ,
						$Strasse,
						$Druck)) {
							die('Ein Fehler ist aufgetreten - '.$client->getErrorCode().":".$client->getErrorMessage());
				}

				$outString = $client->getResponse();
			}
		}
		else {
			echo 'Ihre UstID und die UstID des Kunden sind Pflichtfelder!';
			$outString = '';
		}

		// BOF RESULT TO ARRAY
			$arrOutputData = array();
			if($outString != ''){
				$arrResultData = array();

				$rs_match = preg_match_all("/<param>(.*)<\/param>/ismU", $outString, $arrFound);
				$arrResultData = $arrFound[1];

				if($rs_match){
					foreach($arrResultData as $thisResultData){
						$rs_match = preg_match_all("/<array>(.*)<\/array>/ismU", $thisResultData, $arrFound);
						$strTemp = $arrFound[1][0];
						$rs_match = preg_match_all("/<value>(.*)<\/value>/ismU", $strTemp, $arrFound);
						$arrTemp = $arrFound[1];
						$arrOutputData[trim(strip_tags($arrTemp[0]))] = trim(strip_tags($arrTemp[1]));
					}
				}

				// BOF PARSE XML
				/*
				$xmpParser = xml_parser_create();
				xml_parser_set_option($xmpParser, XML_OPTION_CASE_FOLDING, 0);
				xml_parser_set_option($xmpParser, XML_OPTION_SKIP_WHITE, 1);
				xml_parse_into_struct($xmpParser, $outString, $vals, $index);
				xml_parser_free($xmpParser);

				#echo '<pre>';
				#print_r(($index));
				#echo '</pre><hr />';

				#echo '<pre>';
				#print_r(($vals));
				#echo '</pre><hr />';
				*/
				// EOF PARSE XML
			}
		// EOF RESULT TO ARRAY

		if($arrOutputData["ErrorCode"] == "200"){
			$arrOutputData["Status"] = "Gepr&uuml;ft / OK";
		}
		else {
			$arrOutputData["Status"] = "Gepr&uuml;ft / Fehlerhaft";
		}
		$arrOutputData["StatusMessage"] = $arrStatusMessages[$arrOutputData["ErrorCode"]];

		return $arrOutputData;
	}
?>