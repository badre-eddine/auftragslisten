<?php
	session_start();

	require_once('../config/configMandator.inc.php');
	require_once('../config/configParams_' . strtolower($_COOKIE["mandator"]) . '.inc.php');
	require_once('../config/configBasic.inc.php');
	require_once('../config/configFiles.inc.php');
	require_once('../config/configTables.inc.php');
	require_once('../inc/functions.inc.php');
	##require_once('header.inc.php');
	require_once('../' . PATH_MAILBOX_CLASS);

	$userDatas = getUserDatas();

	$thisSearchString = (trim($_GET["searchString"]));

	$content = '';

	$warningMessage = '';
	$errorMessage = '';
	$successMessage = '';
	$infoMessage = '';

	$dbConnection = new DB_Connection();
	$db_open = $dbConnection->db_connect();

	$modeSound = 'play';

	if(EMBED_SOUND_FILES) {
		if($_COOKIE["modeSound"] == 'play'){
			$modeSound = 'stop';
		}
		else {
			$modeSound = 'play';
		}
	}

	$playAlarm = false;

	// BOF EVENT MAILS
	if(SHOW_EVENT_WINDOW_MAILS){
		if(empty($arrNewMails)) {
			$arrNewMails = getNewMails();
		}
		if(!empty($arrNewMails)) {
			if($arrNewMails["INFOS"]->Unread > 0) {
				$playAlarm = false;
				$content .= $arrNewMails["INFOS"]->Unread . ' neue Mail';
				if($arrNewMails["INFOS"]->Unread > 1) {$content .= 's'; }

				$content .= ' im Posteingang... ';
			}
		}
	}
	// EOF EVENT MAILS

	// BOF EVENT DISEASES
		if(SHOW_EVENT_WINDOW_DISEASES){
			if(empty($arrTodayDiseases)) {
				$arrTodayDiseases = getTodayPersonnelDiseases(date('Y-m-d'));
			}
			if(!empty($arrTodayDiseases)  && $arrTodayDiseases[0]["personnelFirstName"] != 'niemand'){
				$playAlarm = true;
				$content .= '<div class="eventWindowContentElement">';
				$content .= '<h3><img src="' . PATH_ICONS_MENUE_TITLES . 'firstAid.png" alt="" width="16" height="16" /> Mitarbeiter-Krankmeldungen</h3>';
				$content .= '<div class="eventWindowContentElementContent">';
				$content .= '<ul>';
				foreach($arrTodayDiseases as $thisTodayDiseaseKey => $thisTodayDiseaseValue){
					$content .= '<li>';
					if($arrGetUserRights["displayCalendar"] || $arrGetUserRights["editCalendar"]) {
						$content .= '<a href="' . PAGE_DISPLAY_CALENDAR . '">';
					}
					if($thisTodayDiseaseKey > 0) {
						$thisTitle = 'Krank bis zum ' . formatDate($thisTodayDiseaseValue["personnelCalendarEnd"], 'display') . ' einschlie&szlig;lich';
					}
					else {
						$thisTitle .= 'zum Kalender';
					}
					$content .= '<span title="' . $thisTitle . '"> ' . $thisTodayDiseaseValue["personnelFirstName"] . ' ' . $thisTodayDiseaseValue["personnelLastName"] . '</span>';
					if($arrGetUserRights["displayCalendar"] || $arrGetUserRights["editCalendar"]) {
						$content .= '</a>';
					}
					$content .= '</li>';
				}
				$content .= '</ul>';
				$content .= '</div>';
				$content .= '</div>';
			}
		}
	// EOF EVENT DISEASES

	// BOF EVENT BIRTHDAYS
		if(SHOW_EVENT_WINDOW_BIRTHDAYS){
			if(empty($arrTodayBirthdays)){
				$arrTodayBirthdays = getTodayPersonnelBirthdays(date('m-d'));
			}
			if(!empty($arrTodayBirthdays)  && $arrTodayBirthdays[0]["personnelFirstName"] != 'niemand'){
				$playAlarm = true;
				$content .= '<div class="eventWindowContentElement">';
				$content .= '<h3><img src="' . PATH_ICONS_MENUE_TITLES . 'birthday.png" alt="" width="16" height="16" /> Mitarbeiter-Geburtstage</h3>';
				$content .= '<ul>';
				foreach($arrTodayBirthdays as $thisTodayBirthdayKey => $thisTodayBirthdayValue){
					$content .= '<li>';
					if($arrGetUserRights["displayCalendar"] || $arrGetUserRights["editCalendar"]) {
						$content .= '<a href="' . PAGE_DISPLAY_CALENDAR . '">';
					}
					if($thisTodayBirthdayKey > 0) {
						$thisTitle = '*' . formatDate($thisTodayBirthdayValue["personnelBirthday"], 'display') . '';
					}
					else {
						$thisTitle = 'zum Kalender';
					}
					$content .= '<span title="' . $thisTitle . '"> ' . $thisTodayBirthdayValue["personnelFirstName"] . ' ' . $thisTodayBirthdayValue["personnelLastName"] . '</span>';
					if($arrGetUserRights["displayCalendar"] || $arrGetUserRights["editCalendar"]) {
						$content .= '</a>';
					}
					$content .= '</li>';
				}
				$content .= '</ul>';
				$content .= '</div>';
			}
		}
	// EOF EVENT BIRTHDAYS

	// BOF EVENT VACATIONS
		if(SHOW_EVENT_WINDOW_VACATIONS){
			if(empty($arrTodayVacations)){
				$arrTodayVacations = getTodayPersonnelVacations(date('Y-m-d'));
			}

			if(!empty($arrTodayVacations) && $arrTodayVacations[0]["personnelFirstName"] != 'niemand'){
				$playAlarm = true;
				$content .= '<div class="eventWindowContentElement">';
				$content .= '<h3><img src="' . PATH_ICONS_MENUE_TITLES . 'suitcase.png" alt="" width="16" height="16" /> Mitarbeiter-Urlaube</h3>';
				$content .= '<ul>';

				foreach($arrTodayVacations as $thisTodayVacationKey => $thisTodayVacationValue){
					$content .= '<li>';
					if($arrGetUserRights["displayCalendar"] || $arrGetUserRights["editCalendar"]) {
						$content .= '<a href="' . PAGE_DISPLAY_CALENDAR . '">';
					}
					if($thisTodayVacationKey > 0) {
						$thisTitle = 'Urlaub bis zum ' . formatDate($thisTodayVacationValue["personnelCalendarEnd"], 'display') . ' einschlie&szlig;lich';
					}
					else {
						$thisTitle = 'zum Kalender';
					}
					$content .= '<span title="' . $thisTitle . '">' . $thisTodayVacationValue["personnelFirstName"] . ' ' . $thisTodayVacationValue["personnelLastName"] . '</span>';
					if($arrGetUserRights["displayCalendar"] || $arrGetUserRights["editCalendar"]) {
						$content .= '</a>';
					}
					$content .= '</li>';
				}
				$content .= '</ul>';
				$content .= '</div>';
			}
		}
	// EOF EVENT VACATIONS

	// BOF EVENT MESSAGES
		if(SHOW_EVENT_WINDOW_MESSAGES){
			if(empty($arrTodayMessages)){
				$arrTodayMessages = getTodayMessages(date('Y-m-d'));
			}

			if(!empty($arrTodayMessages)){
				$playAlarm = true;
				$content .= '<div class="eventWindowContentElement">';
				$content .= '<h3><img src="' . PATH_ICONS_MENUE_TITLES . 'messages.png" alt="" width="16" height="16" /> Infos</h3>';
				$content .= '<ul>';

				foreach($arrTodayMessages as $thisTodayMessageKey => $thisTodayMessageValue){
					$content .= '<li>';
					$content .= '<b>';
					if($arrGetUserRights["displayInternalMessages"] || $arrGetUserRights["editInternalMessages"]) {
						$content .= '<a href="' . PAGE_EDIT_INTERNAL_MESSAGES . '">';
					}
					if($thisTodayMessageKey > 0) {
						$thisTitle = '' . $thisTodayMessageValue["newsText"] . '';
					}
					else {
						$thisTitle = 'zu den Infos';
					}
					$content .= '<span title="' . $thisTitle . '">' . $thisTodayMessageValue["newsTitle"] . '</span>';
					if($arrGetUserRights["displayInternalMessages"] || $arrGetUserRights["editInternalMessages"]) {
						$content .= '</a>';
					}
					$content .= '</b>';
					$content .= '<br /><span style="color:#666">' . nl2br($thisTodayMessageValue["newsText"]) . '</span>';
					$content .= '</li>';
				}
				$content .= '</ul>';
				$content .= '</div>';
			}
		}
	// EOF EVENT MESSAGES

	// BOF EVENT ACCOUNTING
		if(SHOW_EVENT_WINDOW_ACCOUNTING){
			if(empty($arrOpenInvoiceDatas)){
				$arrOpenInvoiceDatas = getOpenInvoices();
			}
			if(!empty($arrOpenInvoiceDatas)){
				$playAlarm = true;
			}
		}
	// EOF EVENT ACCOUNTING

	// BOF EVENT ONLINE ORDERS
		if(SHOW_EVENT_WINDOW_ONLINE_ORDERS){
			if(empty($arrTodayOnlineOrders)){
				$thisOrderDateToday = date("Y-m-d");
				$thisOrderDateYesterday = date("Y-m-d", mktime(0, 0, 0, date('m'), (date('d') - 1), date('Y')));
				$arrTodayOnlineOrders = getLatestOnlineOrders($thisOrderDateYesterday, $thisOrderDateToday);
			}
			if(!empty($arrTodayOnlineOrders)){
				$playAlarm = true;
			}
		}
	// EOF EVENT ONLINE_ORDERS

	// BOF EVENT CUSTOMER CALENDAR
		if(SHOW_EVENT_WINDOW_CUSTOMER_CALENDAR){
			if(empty($arrCustomerCalendarDates)){
				$arrCustomerCalendarDates = getCustomerCalendarDates(date('Y-m-d'));
			}
			if(!empty($arrCustomerCalendarDates)){
				$playAlarm = true;
				$content .= '<div class="eventWindowContentElement">';
				$content .= '<h3><img src="' . PATH_ICONS_MENUE_TITLES . 'calendar.png" alt="" width="16" height="16" /> Kunden-Termine</h3>';
				$content .= '<ul>';
				foreach($arrCustomerCalendarDates as $thisCalendarDateKey => $thisCalendarDateValue){
					$thisTitle = "Kundendaten anzeigen";
					if($thisCalendarDateValue["customersCalendarDate"] == date('Y-m-d')){
						$content .= '<li title="' . $thisTitle . '" style="background-color:#DBFFCF;  border-color:#28CF00;">';
					}
					else {
						$content .= '<li title="' . $thisTitle . '" >';
					}
					$content .= '<b>' . formatDate($thisCalendarDateValue["customersCalendarDate"], 'display') . ':</b> <br />';
					$content .= '<a href="' . PAGE_EDIT_CUSTOMER. '?searchBoxCustomer=' . $thisCalendarDateValue["customersCalendarCustomersNumber"] . '#tabs-10">Kunde &quot;' . $thisCalendarDateValue["customersCalendarCustomersNumber"] . '&quot; - ' . ($thisCalendarDateValue["customersCalendarDescription"]) . '</a>';
					$content .= '</li>';

					// BOF SEND INFO MAIL
					if($thisCalendarDateValue["customersCalendarReminderMailSended"] == 0){
						if($thisCalendarDateValue["customersCalendarDate"] <= date("Y-m-d")){
							$recipient	= 'grafik@burhan-ctr.de';
							#$recipient	= 'thorsten.hoff@web.de';
							$sender		= 'webmaster@burhan-ctr.de';
							$reply		= 'webmaster@burhan-ctr.de';
							$subject	= 'Kalender-Benachrichtigung ' . formatDate($thisCalendarDateValue["customersCalendarDate"], 'display') . ' - KNR ' . $thisCalendarDateValue["customersCalendarCustomersNumber"];
							$message	= 'Kalender-Benachrichtigung ' . formatDate($thisCalendarDateValue["customersCalendarDate"], 'display') . ' - KNR ' . $thisCalendarDateValue["customersCalendarCustomersNumber"] . ': ' . $thisCalendarDateValue["customersCalendarDescription"];


							require_once('../' . DIRECTORY_HTML_MIME_MAIL . 'htmlMimeMail.php');
							require_once("../classes/createMail.class.php");
							unset($eventMail);

							$eventMail = new createMail(
								$subject,					// TEXT:	MAIL_SUBJECT
								'SX',						// STRING:	DOCUMENT TYPE
								'',							// STRING:	DOCUMENT NUMBER
								null,						// ARRAY:	ATTACHMENT FILES
								$recipient,					// STRING:	RECIPIENTS
								$arrMailContentDatas['SX'],	// MAIL_TEXT_TEMPLATE
								$mailTemplate["html"],		// MAIL_BODY_TEMPLATE
								$message,					// STRING:	ADDITIONAL TEXT
								'',							// STRING:	SEND ATTACHED TEXT
								false,						// BOOLEAN:	SEND ATTACHED DOCUMENT
								DIRECTORY_MAIL_IMAGES,		// DIRECTORY_MAIL_IMAGES
								$sender						// STRING:	SENDER
							);
							$eventMailResult = $eventMail->returnResult();
							if(isset($eventMailResult)) {
								if(!$eventMailResult) {
									$errorMessage .= ' Die Kalender-Benachrichtigung konnte nicht an ' . $recipient . ' versendet werden! '.'<br />';
								}
								else {
									$successMessage .= ' Die Kalender-Benachrichtigung wurde an ' . $recipient . ' versendet! '.'<br />';
									$sql = "UPDATE
												`" . TABLE_CUSTOMERS_CALENDAR . "`
												SET `customersCalendarReminderMailSended` = '1'
												WHERE 1
													AND `customersCalendarID` = '" . $thisCalendarDateValue["customersCalendarID"] . "'
													AND `customersCalendarCustomersID` = '" . $thisCalendarDateValue["customersCalendarCustomersID"] . "'
													AND `customersCalendarCustomersNumber` = '" . $thisCalendarDateValue["customersCalendarCustomersNumber"] . "'
										";
									$rs = $dbConnection->db_query($sql);
								}
							}
						}
					}
					// EOF SEND INFO MAIL
				}
				$content .= '</ul>';
				$content .= '</div>';
			}
		}
	// EOF EVENT CUSTOMER CALENDAR

	// BOF EVENT CUSTOMER NEW ONLINE REGISTRATIONS
		if(SHOW_EVENT_WINDOW_NEW_ONLINE_REGISTRATIONS){
			if(empty($arrOnlineShopRegistrations)){
				$arrOnlineShopRegistrations = getOnlineShopRegistrations();
			}
			if(!empty($arrOnlineShopRegistrations)){
				$playAlarm = true;
			}
		}
	// EOF EVENT CUSTOMER NEW ONLINE REGISTRATIONS

	if($dbConnection) {
		$dbConnection->db_close();
	}

	if($content != ''){
		$content .= '<div class="clear"></div>';
	}
	if($_COOKIE["modeSound"] == 'play' && $playAlarm) {
		$codeSound = embedSoundFile(DIRECTORY_SOUND_FILES . 'klingel_laut.mp3', 'notifications');
		$content .= ($codeSound);
	}
	echo $content;
	displayMessages();
?>