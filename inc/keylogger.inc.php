<?php
	session_start();

	require_once('../config/configMandator.inc.php');
	require_once('../config/configParams_' . strtolower(MANDATOR) . '.inc.php');
	require_once('../config/configBasic.inc.php');
	require_once('../config/configFiles.inc.php');
	require_once('../config/configTables.inc.php');
	require_once('../inc/functions.inc.php');

	error_reporting(0);

	if(KEY_LOG_IN_DB || KEY_LOG_IN_FILE){
		$dataSeparator = "\n";
		$dataKeyLogTime = date("H:i:s");

		$thisStoreDateDir = date('Y-m-d');
		$thisStorLogDir = '../' . PATH_LOGFILES . 'keylog/' . $thisStoreDateDir . '/';
		if(!is_dir($thisStorLogDir)){
			mkdir($thisStorLogDir);
		}

		if($_GET["eventType"] == "keydown" || $_GET["eventType"] == "keyup" || $_GET["keypress"] == "keypress"){
			$dataKeyLog = chr($_GET["eventData"]);
		}
		else if($_GET["eventType"] == "mousemove" || $_GET["eventType"] == "mouseclick" || $_GET["eventType"] == "contextmenu" || $_GET["eventType"] == "dblclick"){
			$dataKeyLog = ($_GET["eventData"]);
		}

		if(KEY_LOG_IN_FILE){
			$dataLogContent = date("Y-m-d") . "\t" . $dataKeyLogTime . "\t" . $_SESSION["usersID"] . "\t" . $_GET["eventType"] . "\t" . $dataKeyLog . "\t" . $_SERVER["HTTP_REFERER"] . "\t" . $_SERVER["REQUEST_URI"] . "\t" . $_SERVER["REMOTE_ADDR"];

			$logfileName = date("Y-m-d") . '_' . 'UID-' . $_SESSION["usersID"] . '.txt';
			$logfilePath = $thisStorLogDir . $logfileName;

			$fp = fopen($logfilePath, "a");
			$frs = fwrite($fp, $dataLogContent . $dataSeparator);
			fclose($fp);
		}

		if(KEY_LOG_IN_DB){
			$dbConnection = new DB_Connection();
			$db_open = $dbConnection->db_connect();

			$sql = "INSERT DELAYED INTO `" . TABLE_USERS_KEYLOG . "`(
											`keylogID`,
											`keylogDate`,
											`keylogTime`,
											`keylogUserID`,
											`keylogEvent`,
											`keylogKey`,
											`keylogReferrer`,
											`keylogRequestUri`,
											`keylogIP`
										)
										VALUES (
											'%',
											'" . date("Y-m-d") . "',
											'" . $dataKeyLogTime . "',
											'" . $_SESSION["usersID"] . "',
											'" . $_GET["eventType"] . "',
											'" . $dataKeyLog . "',
											'" . $_SERVER["HTTP_REFERER"] . "',
											'" . $_SERVER["REQUEST_URI"] . "',
											'" . $_SERVER["REMOTE_ADDR"] . "'
										)
				";

			$rs = $dbConnection->db_query($sql);

			if($db_open){
				$dbConnection->db_close();
			}
		}
	}
?>