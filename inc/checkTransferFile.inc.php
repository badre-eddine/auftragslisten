<?php
	session_start();

	require_once('../config/configMandator.inc.php');
	require_once('../config/configParams_' . strtolower(MANDATOR) . '.inc.php');
	require_once('../config/configBasic.inc.php');
	require_once('../config/configFiles.inc.php');
	require_once('../config/configTables.inc.php');
	require_once('../inc/functions.inc.php');
	##require_once('header.inc.php');

	$thisProduktName = (trim($_GET["thisProduktName"]));
	$thisTransferFileName = (trim($_GET["thisTransferFileName"]));

	$content = '';

	$warningMessage = '';
	$errorMessage = '';
	$successMessage = '';
	$infoMessage = '';

	$useUf8Encode = false;

	if($thisProduktName != "" && $thisTransferFileName != "") {
		$dbConnection = new DB_Connection();
		$db_open = $dbConnection->db_connect();

		$sql = "
				SELECT
					CONCAT (
						`orderCategoriesShortName`,
						' ',
						`orderCategoriesName`
					) AS `productName`
					FROM `common_productcategories`
					WHERE 1
						/*
							AND `orderCategoriesLevelID` = '001'
							AND `orderCategoriesRelationID` != ''
						*/
						AND (
							`orderCategoriesName_bctr` = '" . $thisProduktName . "'
							OR `orderCategoriesName_b3` = '" . $thisProduktName . "'
						)
					LIMIT 1
			";

		$rs = $dbConnection->db_query($sql);
		$thisNumRows = $dbConnection->db_getMysqlNumRows($rs);

		$checkSum = 0;
		if($thisNumRows > 0) {
			list($checkResult) = mysqli_fetch_array($rs);
			if($checkResult != ''){
				$arrCheckResult = explode(" ", $checkResult);
				foreach($arrCheckResult as $thisCheckResult){
					if(preg_match("/" . $thisCheckResult . "/", $thisTransferFileName)){
						$checkSum++;
					}
				}
				$content = $checkSum;
			}
		}
		else {
			$content = '';
		}

		if($dbConnection->db_displayErrors() != "") {
			$errorMessage .= $dbConnection->db_displayErrors(). '<br />';
		}

		$dbConnection->db_close();
	}
	echo $content;

?>