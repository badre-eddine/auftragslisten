<?php
	session_start();

	require_once('../config/configMandator.inc.php');
	require_once('../config/configParams_' . strtolower(MANDATOR) . '.inc.php');
	require_once('../config/configBasic.inc.php');
	require_once('../config/configFiles.inc.php');
	require_once('../config/configTables.inc.php');
	require_once('../inc/functions.inc.php');
	##require_once('header.inc.php');

	$thisSearchString = (trim($_GET["searchString"]));

	$content = '';

	$warningMessage = '';
	$errorMessage = '';
	$successMessage = '';
	$infoMessage = '';

	$useUf8Encode = false;

	if($thisSearchString != "") {

		// BOF DEFINE DOCUMENT TABLES DEPENDING ON DOCUMENT TYPE
			$thisDocumentType = 'AB';
			DEFINE('TABLE_ORDER_THIS', constant('TABLE_ORDER_' . $thisDocumentType));
			DEFINE('TABLE_ORDER_THIS_DETAILS', constant('TABLE_ORDER_' . $thisDocumentType.'_DETAILS'));
		// EOF DEFINE DOCUMENT TABLES DEPENDING ON DOCUMENT TYPE


		$dbConnection = new DB_Connection();
		$db_open = $dbConnection->db_connect();

		// BOF READ PRINTTYPES
			$arrPrintTypeDatas = getPrintTypes();
		// EOF READ PRINTTYPES

		$sql = "
				SELECT
					`tempTable`.`orderDocumentsID`,
					`tempTable`.`orderDocumentsNumber`,
					`tempTable`.`orderDocumentsOrdersIDs`,
					`tempTable`.`orderDocumentsCustomerNumber`,
					`tempTable`.`orderDocumentsCustomersOrderNumber`,

					`tempTable`.`orderDocumentDetailID`,
					`tempTable`.`orderDocumentDetailDocumentID`,
					`tempTable`.`orderDocumentDetailOrderID`,
					`tempTable`.`orderDocumentDetailOrderType`,
					`tempTable`.`orderDocumentDetailPos`,
					`tempTable`.`orderDocumentDetailProductNumber`,
					`tempTable`.`orderDocumentDetailProductName`,
					`tempTable`.`orderDocumentDetailProductKategorieID`,
					`tempTable`.`orderDocumentDetailProductQuantity`,
					`tempTable`.`orderDocumentDetailProductColorsText`,
					`tempTable`.`orderDocumentDetailProductColorsCount`,
					`tempTable`.`orderDocumentDetailsWithBorder`,
					`tempTable`.`orderDocumentDetailKommission`,
					`tempTable`.`orderDocumentDetailPrintText`,

					 `" . TABLE_ORDERS . "`.`ordersID`,
					 `" . TABLE_ORDERS . "`.`ordersKundennummer`,
					 `" . TABLE_ORDERS . "`.`ordersKundenName`,
					 `" . TABLE_ORDERS . "`.`ordersKommission`,
					 `" . TABLE_ORDERS . "`.`ordersInfo`,

					 `" . TABLE_ORDERS . "`.`ordersArtikelID`,
					 `" . TABLE_ORDERS . "`.`ordersArtikelNummer`,
					 `" . TABLE_ORDERS . "`.`ordersArtikelBezeichnung`,
					 `" . TABLE_ORDERS . "`.`ordersArtikelKategorieID`,
					 `" . TABLE_ORDERS . "`.`ordersArtikelMenge`,
					 `" . TABLE_ORDERS . "`.`ordersArtikelPrintColorsCount`,
					 `" . TABLE_ORDERS . "`.`ordersArtikelWithBorder`,
					 `" . TABLE_ORDERS . "`.`ordersArtikelWithClearPaint`,
					 `" . TABLE_ORDERS . "`.`ordersAufdruck`,

					 `" . TABLE_ORDERS . "`.`ordersDruckFarbe`,
					 `" . TABLE_ORDERS . "`.`ordersAdditionalCostsID`,
					 `" . TABLE_ORDERS . "`.`ordersNotizen`,
					 `" . TABLE_ORDERS . "`.`ordersDruckerName`,
					 `" . TABLE_ORDERS . "`.`ordersPrintPlateNumber`

					FROM (
							SELECT

								`bctr_orderConfirmations`.`orderDocumentsID`,
								`bctr_orderConfirmations`.`orderDocumentsNumber`,
								`bctr_orderConfirmations`.`orderDocumentsOrdersIDs`,
								`bctr_orderConfirmations`.`orderDocumentsCustomerNumber`,
								`bctr_orderConfirmations`.`orderDocumentsCustomersOrderNumber`,

								`bctr_orderConfirmationsDetails`.`orderDocumentDetailID`,
								`bctr_orderConfirmationsDetails`.`orderDocumentDetailDocumentID`,
								`bctr_orderConfirmationsDetails`.`orderDocumentDetailOrderID`,
								`bctr_orderConfirmationsDetails`.`orderDocumentDetailOrderType`,
								`bctr_orderConfirmationsDetails`.`orderDocumentDetailPos`,
								`bctr_orderConfirmationsDetails`.`orderDocumentDetailProductNumber`,
								`bctr_orderConfirmationsDetails`.`orderDocumentDetailProductName`,
								`bctr_orderConfirmationsDetails`.`orderDocumentDetailProductKategorieID`,
								`bctr_orderConfirmationsDetails`.`orderDocumentDetailProductQuantity`,
								`bctr_orderConfirmationsDetails`.`orderDocumentDetailProductColorsText`,
								`bctr_orderConfirmationsDetails`.`orderDocumentDetailProductColorsCount`,
								`bctr_orderConfirmationsDetails`.`orderDocumentDetailsWithBorder`,
								`bctr_orderConfirmationsDetails`.`orderDocumentDetailKommission`,
								`bctr_orderConfirmationsDetails`.`orderDocumentDetailPrintText`

							FROM `bctr_orderConfirmations`

							LEFT JOIN `bctr_orderConfirmationsDetails`
							ON(`bctr_orderConfirmations`.`orderDocumentsID` = `bctr_orderConfirmationsDetails`.`orderDocumentDetailDocumentID`)

							WHERE 1
								AND `bctr_orderConfirmations`.`orderDocumentsNumber`= '" . $thisSearchString . "'
								AND `bctr_orderConfirmationsDetails`.`orderDocumentDetailPosType`= 'product'

							UNION

							SELECT

								`b3_orderConfirmations`.`orderDocumentsID`,
								`b3_orderConfirmations`.`orderDocumentsNumber`,
								`b3_orderConfirmations`.`orderDocumentsOrdersIDs`,
								`b3_orderConfirmations`.`orderDocumentsCustomerNumber`,
								`b3_orderConfirmations`.`orderDocumentsCustomersOrderNumber`,

								`b3_orderConfirmationsDetails`.`orderDocumentDetailID`,
								`b3_orderConfirmationsDetails`.`orderDocumentDetailDocumentID`,
								`b3_orderConfirmationsDetails`.`orderDocumentDetailOrderID`,
								`b3_orderConfirmationsDetails`.`orderDocumentDetailOrderType`,
								`b3_orderConfirmationsDetails`.`orderDocumentDetailPos`,
								`b3_orderConfirmationsDetails`.`orderDocumentDetailProductNumber`,
								`b3_orderConfirmationsDetails`.`orderDocumentDetailProductName`,
								`b3_orderConfirmationsDetails`.`orderDocumentDetailProductKategorieID`,
								`b3_orderConfirmationsDetails`.`orderDocumentDetailProductQuantity`,
								`b3_orderConfirmationsDetails`.`orderDocumentDetailProductColorsText`,
								`b3_orderConfirmationsDetails`.`orderDocumentDetailProductColorsCount`,
								`b3_orderConfirmationsDetails`.`orderDocumentDetailsWithBorder`,
								`b3_orderConfirmationsDetails`.`orderDocumentDetailKommission`,
								`b3_orderConfirmationsDetails`.`orderDocumentDetailPrintText`

							FROM `b3_orderConfirmations`

							LEFT JOIN `b3_orderConfirmationsDetails`
							ON(`b3_orderConfirmations`.`orderDocumentsID` = `b3_orderConfirmationsDetails`.`orderDocumentDetailDocumentID`)

							WHERE 1
								AND `b3_orderConfirmations`.`orderDocumentsNumber`= '" . $thisSearchString . "'
								AND `b3_orderConfirmationsDetails`.`orderDocumentDetailPosType`= 'product'
						) AS `tempTable`

					LEFT JOIN `" . TABLE_ORDERS . "`
					ON(`tempTable`.`orderDocumentDetailOrderID` = `" . TABLE_ORDERS . "`.`ordersID`)
			";

		#echo $sql;

		$rs = $dbConnection->db_query($sql);
		if(mysqli_error()){
			echo mysqli_error();

		}
		while($ds = mysqli_fetch_assoc($rs)) {
			foreach(array_keys($ds) as $field){
				if(preg_match("/^orderDocumentDetail/", $field) || preg_match("/^orders/", $field)) {
					$arrDocumentDetailDatas[$ds["orderDocumentDetailID"]][$field] = $ds[$field];
				}
				else {
					$arrDocumentDatas[$field] = $ds[$field];
				}				
			}
		}


		$content .= '<table border="0" cellpadding="0" cellspacing="0" width="" class="noBorder">';
		$content .= '<tr>';
		$content .= '<td><b>Dokument-Nummer:</b></td>';
		$content .= '<td>'.($arrDocumentDatas["orderDocumentsNumber"]).'</td>';		
		$content .= '</tr>';
		$content .= '</table>';

		$content .= '<hr />';

		$content .= '<table border="0" cellpadding="0" cellspacing="0" width="100%" class="border">';
		$content .= '<tr>';
		$content .= '<th style="width:45px;text-align:right;">#</th>';
		$content .= '<th>Aktion</th>';
		$content .= '<th colspan="2">Kunde</th>';
		$content .= '<th>Art.-Nr</th>';
		$content .= '<th>Artikel</th>';
		$content .= '<th>Menge</th>';
		$content .= '<th>Anz. Farben</th>';
		$content .= '<th>Klarlack</th>';
		$content .= '<th>Ges. Farben</th>';
		$content .= '<th>Druckart</th>';
		$content .= '</tr>';

		$count = 0;

		foreach($arrDocumentDetailDatas as $thisKey => $thisValue) {
			if($count%2 == 0){ $rowClass = 'row0'; }
			else { $rowClass = 'row1'; }

			$content .= '<tr class="'.$rowClass.'">';

			$content .= '<td style="text-align:right;">';
			$content .= '<b>' . ($count + 1) . '.</b>';
			$content .= '<span class="productsOrderID" style="visibility:hidden">';
			$content .= $thisValue["ordersID"];
			$content .= '</span>';
			$content .= '</td>';

			$content .= '<td>';
			$content .= '<img src="layout/icons/iconCopy.png" width="14" height="14" class="buttonUseData" style="cursor:pointer;" alt="verwenden" title="Diese Daten &uuml;bernehmen" />';
			$content .= '</td>';
			
			$content .= '<td>';
			$content .= '<b>' . $thisValue["ordersKundennummer"] . '</b>';
			$content .= '</td>';
			
			$content .= '<td>';
			$content .= htmlentities($thisValue["ordersKundenName"]);
			$content .= '</td>';

			$content .= '<td>';
			$content .= $thisValue["orderDocumentDetailProductNumber"];
			$content .= '<span class="productsProductID" style="visibility:hidden">';
			$content .= $thisValue["ordersArtikelID"];
			$content .= '</span>';
			$content .= '</td>';
			
			$content .= '<td>';
			$content .= $thisValue["orderDocumentDetailProductName"];

			if($thisValue["orderDocumentDetailKommission"] != ""){
				$content .= '<br /><span class="remarksArea"><b>Kommission:</b> ';
				$content .= $thisValue["orderDocumentDetailKommission"];
				$content .= '</span>';
			}
			else if($thisValue["ordersKommission"] != ""){
				$content .= '<br /><span class="remarksArea"><b>Kommission:</b> ';
				$content .= $thisValue["ordersKommission"];
				$content .= '</span>';
			}
			
			if($thisValue["orderDocumentDetailPrintText"] != ""){
				$content .= '<br /><span class="remarksArea"><b>Aufdruck: </b>';
				$content .= $thisValue["orderDocumentDetailPrintText"];
				$content .= '</span>';
			}
			else if($thisValue["ordersAufdruck"] != ""){
				$content .= '<br /><span class="remarksArea"><b>Aufdruck: </b>';
				$content .= $thisValue["ordersAufdruck"];
				$content .= '</span>';
			}
			$content .= '</td>';

			$content .= '<td style="text-align:right;">';
			$content .= '<span class="productsQuantity">';
			$content .= $thisValue["orderDocumentDetailProductQuantity"];
			$content .= '</span>';
			$content .= '</td>';

			$content .= '<td style="text-align:right;">';
			$content .= $thisValue["orderDocumentDetailProductColorsCount"];
			$content .= '</td>';

			$content .= '<td style="text-align:right;">';
			$content .= $thisValue["ordersArtikelWithClearPaint"];
			$content .= '</td>';

			$content .= '<td style="text-align:right;">';
			$content .= '<span class="productsTotalColors">';
			$content .= ($thisValue["orderDocumentDetailProductColorsCount"] + $thisValue["ordersArtikelWithClearPaint"]);
			$content .= '</span>';
			$content .= '</td>';

			$content .= '<td style="text-align:right;">';
			$content .= $arrPrintTypeDatas[$thisValue["ordersAdditionalCostsID"]]["printTypesName"];
			$content .= ' [<span class="productsPrintTypeID">';
			$content .= $thisValue["ordersAdditionalCostsID"];
			$content .= '</span>]';
			$content .= '</td>';			

			$content .= '</tr>';
			$count++;
		}
		$content .= '</table>';

		if($dbConnection) {
			$dbConnection->db_close();
		}
	}
	$content = removeUnnecessaryChars($content);
	echo $content;
?>