<?php
	ini_set('display_errors', 1);
	ini_set('display_startup_errors', 1);
	error_reporting(E_ALL);
	require_once('inc/requires.inc.php');

	if(!$arrGetUserRights["displayOrdersOverview"] && !$arrGetUserRights["displayOrders"] && !$arrGetUserRights["editOrders"]) {
		header('location: ' . PAGE_EXIT_LOCATION);
		exit;
	}

	if($_REQUEST["jsonReturnParams"] != "") {
		$arrJsonReturnParams = json_decode($_REQUEST["jsonReturnParams"]);
		foreach($arrJsonReturnParams as $thisKey => $thisValue){
			$_REQUEST[$thisKey] = $thisValue;
		}
	}

	if($_COOKIE["isAdmin"] == '1'){
		DEFINE('SHOW_PRINT_PRODUCTION_FILES', false); // false | true
	}
	else {
		DEFINE('SHOW_PRINT_PRODUCTION_FILES', false); // false | true
	}

	$jswindowMessage = "";
	$warningMessage = '';
	$errorMessage = '';
	$successMessage = '';
	$infoMessage = '';

	$dbConnection = new DB_Connection();
	$db_open = $dbConnection->db_connect();

	if($_REQUEST["searchProductDateSort"] == ''){
		$_REQUEST["searchProductDateSort"] = 'DESC';
	}

	// BOF DOWNLOAD FILE
	

		if($_REQUEST["downloadFile"] != "") {
			
			require_once('classes/downloadFile.class.php');
			$thisDownloadFile = $_REQUEST["downloadFile"];
			if($_REQUEST["thisDocumentType"] = "PP"){
				$fileDirectory = DIRECTORY_PRINT_PRODUCTION_FILES;
			}
			
			$thisDownload = new downloadFile($fileDirectory, $thisDownloadFile);
			
			$errorMessage .= $thisDownload->startDownload();
		}
		
	// EOF DOWNLOAD FILE
	
	if($_POST["sendAttachedDocument"] == '1' && $_POST["orderID"] > 0 && $_FILES["sendAttachedMailFile"]["name"] != "" && $_POST["selectCustomersRecipientMail"] != ""){
		require_once("inc/submitPrintProductionSheet.inc.php");

		$_POST["searchCustomerNumber"] = trim($_POST["editCustomerNumber"]);
		$_REQUEST["searchCustomerNumber"] = trim($_POST["editCustomerNumber"]);
		
		$resultOrderStatusChangeInformAgent = sendMailStatusChanges(
			$_POST["searchCustomerNumber"],
			$_POST["editStatusIDChange"],
			$_POST["editStatusIDStored"],
			$_POST["orderID"]
		);
		
	}
	

	else if($_POST["sendAttachedDocument"] == '1' && ($_FILES["sendAttachedMailFile"]["name"] == "" || $_POST["selectCustomersRecipientMail"] == "")){
		
		$_POST["searchCustomerNumber"] = trim($_POST["editCustomerNumber"]);
		$_REQUEST["searchCustomerNumber"] = trim($_POST["editCustomerNumber"]);
		if($_FILES["sendAttachedMailFile"]["name"] == ""){
			$errorMessage .= ' Es wurde keine Datei ausgew&auml;hlt!' . '<br />';
		}
		if($_POST["selectCustomersRecipientMail"] == ""){
			$errorMessage .= ' Es wurde keine Mail-Adresse eingegeben!' . '<br />';
		}

	}
	
	// BOF CHANGE STATUS
		if(!empty($_POST["changeStatus"])) {
			foreach($_POST["changeStatus"] as $thisKey => $thisValue) {

				// BOF SEND INFORMATION STATUS CHANGE
					#if($_POST["changeStatus"] != $_POST["storedStatus"]) {
					if($thisValue != $_POST["storedStatus"]) {
						$resultOrderStatusChangeInformAgent = sendMailStatusChanges(
							$_POST["searchCustomerNumber"],
							$thisValue,
							$_POST["storedStatus"],
							$_POST["dataRow"]
						);
					}
				// EOF SEND INFORMATION STATUS CHANGE

				$sql_updateStatus = "
					UPDATE
						`" . TABLE_ORDERS . "`
							SET
								`" . TABLE_ORDERS . "`.`ordersStatus` = '".$thisValue."',
								`" . TABLE_ORDERS . "`.`ordersProductionPrintingPlant` = 'DE'
				";
				if($thisValue == '3') {
					$sql_updateStatus .= ", `" . TABLE_ORDERS . "`.`ordersBelichtungsDatum` = NOW() ";
				}
				else if($thisValue == '7') {
					$sql_updateStatus .= ", `" . TABLE_ORDERS . "`.`ordersProduktionsDatum` = NOW() ";
				}
				else if($thisValue == '2') {
					// $sql_updateStatus .= ", `" . TABLE_ORDERS . "`.`ordersBestellDatum` = NOW() ";
					$sql_updateStatus .= ", `" . TABLE_ORDERS . "`.`ordersFreigabeDatum` = NOW() ";
					// BOF SET DELIVERY KW ON STATUS CHANGE
						$newOrdersLieferwoche = date('W') + MIN_SHIPPING_TIME;

						// BOF GET DELIVERY TIME DEPENDING ON FreigabeDatum AND PRINT-TYPE
							$newOrdersLieferwoche = getDeliveryTimeKW(date("Y-m-d"), $_POST["storedAdditionalCostsID"]);
						// EOF GET DELIVERY TIME DEPENDING ON FreigabeDatum AND PRINT-TYPE

						$sql_updateStatus .= ", `" . TABLE_ORDERS . "`.`ordersLieferwoche` = '" . $newOrdersLieferwoche . "' ";
					// EOF SET DELIVERY KW ON STATUS CHANGE
				}
				else if($thisValue == '4') {
					$sql_updateStatus .= ", `" . TABLE_ORDERS . "`.`ordersAuslieferungsDatum` = NOW() ";
					$sql_updateStatus .= ", `" . TABLE_ORDERS . "`.`ordersLieferwoche` = '" . date("W", mktime(0, 0, 0, date('m'), date('d'), date('Y'))) . "'";
				}
				else if($thisValue == '6') {
					$sql_updateStatus .= ", `" . TABLE_ORDERS . "`.`ordersStornoDatum` = NOW() ";

					// BOF STORNO DOCUMENTS
						$sql_getAssociatedDocuments = "
								SELECT
										`" . TABLE_ORDER_CONFIRMATIONS . "`.`orderDocumentsNumber`,
										`" . TABLE_ORDER_CONFIRMATIONS . "`.`orderDocumentsOrdersIDs`,
										'AB' AS `orderDocumentsType`,

										`" . TABLE_RELATED_DOCUMENTS . "`.`relatedDocuments_AN`,
										`" . TABLE_RELATED_DOCUMENTS . "`.`relatedDocuments_AB`,
										`" . TABLE_RELATED_DOCUMENTS . "`.`relatedDocuments_RE`,
										`" . TABLE_RELATED_DOCUMENTS . "`.`relatedDocuments_LS`,
										`" . TABLE_RELATED_DOCUMENTS . "`.`relatedDocuments_GU`,
										`" . TABLE_RELATED_DOCUMENTS . "`.`relatedDocuments_MA`,
										`" . TABLE_RELATED_DOCUMENTS . "`.`relatedDocuments_M1`,
										`" . TABLE_RELATED_DOCUMENTS . "`.`relatedDocuments_M2`,
										`" . TABLE_RELATED_DOCUMENTS . "`.`relatedDocuments_M3`,
										`" . TABLE_RELATED_DOCUMENTS . "`.`relatedDocuments_IK`,
										`" . TABLE_RELATED_DOCUMENTS . "`.`relatedDocuments_collectiveABs`

									FROM `" . TABLE_ORDER_CONFIRMATIONS . "`

									LEFT JOIN `" . TABLE_ORDER_CONFIRMATIONS_DETAILS . "`
									ON(`" . TABLE_ORDER_CONFIRMATIONS_DETAILS . "`.`orderDocumentDetailDocumentID` = `" . TABLE_ORDER_CONFIRMATIONS . "`.`orderDocumentsID`)

									LEFT JOIN `" . TABLE_RELATED_DOCUMENTS. "`
									ON(`" . TABLE_ORDER_CONFIRMATIONS . "`.`orderDocumentsNumber` = `" . TABLE_RELATED_DOCUMENTS . "`.`relatedDocuments_AB`)

									WHERE 1
										AND `" . TABLE_ORDER_CONFIRMATIONS_DETAILS . "`.`orderDocumentDetailOrderID` = '".$thisKey."'

							";

						$rs_getAssociatedDocuments = $dbConnection->db_query($sql_getAssociatedDocuments);
						$countDocuments = 0;
						while ($ds = mysqli_fetch_assoc($rs_getAssociatedDocuments)){
							$countDocuments++;
							$arrAssociatedDocumentNumbers[$ds["orderDocumentsType"]] = $ds["orderDocumentsNumber"];
							$arrAssociatedDocumentNumbers["relatedDocuments_LS"] = $ds["relatedDocuments_LS"];
							$arrAssociatedDocumentDatas[$ds["orderDocumentsNumber"]]["orderPosition"] = $ds["orderPosition"];
							$arrAssociatedDocumentDatas[$ds["orderDocumentsNumber"]]["orderIDs"] = explode(";", $ds["orderDocumentsOrdersIDs"]);
							$arrAssociatedDocumentDatas[$ds["orderDocumentsNumber"]]["orderIDsCount"] = count(explode(";", $ds["orderDocumentsOrdersIDs"]));
						}

						if($countDocuments > 0) {
							// BOF PRÜFEN OB RE VORLIEGT?
							// DANN NICHT STORNIEREN
							#if($arrAssociatedDocumentNumbers["relatedDocuments_RE"] != ''){

							#}
							$sql_updateAssociatedDocuments = "
									UPDATE
										`" . TABLE_ORDER_CONFIRMATIONS_DETAILS . "`
										LEFT JOIN `" . TABLE_ORDER_CONFIRMATIONS . "`
										ON(`" . TABLE_ORDER_CONFIRMATIONS_DETAILS . "`.`orderDocumentDetailDocumentID` = `" . TABLE_ORDER_CONFIRMATIONS . "`.`orderDocumentsID`)
										SET `" . TABLE_ORDER_CONFIRMATIONS . "`.`orderDocumentsStatus` = '3'

									WHERE 1
										AND `" . TABLE_ORDER_CONFIRMATIONS_DETAILS . "`.`orderDocumentDetailOrderID` = '".$thisKey."'
								";
							$rs_updateAssociatedDocuments = $dbConnection->db_query($sql_updateAssociatedDocuments);

							if($rs_updateAssociatedDocuments) {
								$successMessage .= ' Die zur Produktion geh&ouml;rigen Auftragsbest&auml;tigung (&quot;' . $arrAssociatedDocumentNumbers["AB"] . '&quot;) wurden auf <b>STORNIERT</b> gesetzt.' .'<br />';
							}
							else {
								$errorMessage .= ' Die zur Produktion geh&ouml;rigen Auftragsbest&auml;tigung (&quot;' . $arrAssociatedDocumentNumbers["AB"] . '&quot;) konnten nicht auf <b>STORNIERT</b> gesetzt werden.' . '<br />';
							}

							$sql_updateAssociatedDocuments = "
									UPDATE
										`" . TABLE_ORDER_DELIVERIES_DETAILS . "`
										LEFT JOIN `" . TABLE_ORDER_DELIVERIES . "`
										ON(`" . TABLE_ORDER_DELIVERIES_DETAILS . "`.`orderDocumentDetailDocumentID` = `" . TABLE_ORDER_DELIVERIES . "`.`orderDocumentsID`)
										SET `" . TABLE_ORDER_DELIVERIES . "`.`orderDocumentsStatus` = '3'

									WHERE 1
										AND `" . TABLE_ORDER_DELIVERIES_DETAILS . "`.`orderDocumentDetailOrderID` = '".$thisKey."'
								";
							$rs_updateAssociatedDocuments = $dbConnection->db_query($sql_updateAssociatedDocuments);

							if($rs_updateAssociatedDocuments) {
								$successMessage .= ' Der zur Produktion geh&ouml;rigen Lieferschein (&quot;' . $arrAssociatedDocumentNumbers["relatedDocuments_LS"] . '&quot;) wurden auf <b>STORNIERT</b> gesetzt.' .'<br />';
							}
							else {
								$errorMessage .= ' Der zur Produktion geh&ouml;rigen Lieferschein (&quot;' . $arrAssociatedDocumentNumbers["relatedDocuments_LS"] . '&quot;) konnten nicht auf <b>STORNIERT</b> gesetzt werden.' . '<br />';
							}
						}
						else {
							$infoMessage .= ' Es wurden keine zur Produktion geh&ouml;rigen Auftragsbest&auml;tigungen gefunden. ';
						}
					// EOF STORNO DOCUMENTS
				}

				$sql_updateStatus .= "
							WHERE `" . TABLE_ORDERS . "`.`ordersID` =  '".$thisKey."'
				";

				$rs_updateStatus = $dbConnection->db_query($sql_updateStatus);
			}
		}
	// EOF CHANGE STATUS

	$defaultSortField = "ordersBestellDatum";
	$defaultSortDirection = "ASC";

	if($_REQUEST["sortField"] != "") {
		$sqlSortField = "`" . $_REQUEST["sortField"] . "`";
	}
	else {
		$sqlSortField = $defaultSortField;
	}

	if($_REQUEST["sortDirection"] != "") {
		$sqlSortDirection = $_REQUEST["sortDirection"];
	}
	else {
		$sqlSortDirection = $defaultSortDirection;
	}
	if($sqlSortDirection == "DESC") { $sqlSortDirection = "ASC"; }
	else { $sqlSortDirection = "DESC"; }

	if($_REQUEST["searchProductDateSort"] != ''){
		#$sqlSortField = " ORDER BY `" . TABLE_ORDERS . "`.`ordersBestellDatum` DESC ";
		#$sqlSortField = " ORDER BY `" . TABLE_ORDERS . "`.`ordersBestellDatum` " . $_REQUEST["searchProductDateSort"] . " ";
		#$sqlSortDirection = "";
		$sqlSortField = "`" . TABLE_ORDERS . "`.`ordersBestellDatum`";
		$sqlSortDirection = " " . $_REQUEST["searchProductDateSort"] . " ";
	}

	// BOF READ PRINTERS
		$arrPrinterDatas = getPrinters();
	// EOF READ PRINTERS

	// BOF READ PRINTTYPES
		$arrPrintTypeDatas = getPrintTypes();
	// EOF READ PRINTTYPES

	// BOF READ VERTRETER
		$arrAgentDatas = getAgents();
		$arrAgentDatas2 = getAgents2();
	// EOF READ VERTRETER

	// BOF READ PAYMENT TYPES
		$arrPaymentTypeDatas = getPaymentTypes();
	// EOF READ PAYMENT TYPES

	// BOF READ PAYMENT STATUS TYPES
		$arrPaymentStatusTypeDatas = getPaymentStatusTypes();
	// EOF READ PAYMENT STATUS TYPES

	// BOF READ BESTELL ARTEN
		$arrOrderTypeDatas = getOrderTypes();
	// EOF READ BESTELL ARTEN

	// BOF READ BESTELL STATUS TYPEN
		$arrOrderStatusTypeDatas = getOrderStatusTypes();
		$arrOrderStatusTypeDatasExtern = getOrderStatusTypesExtern();
	// EOF READ BESTELL STATUS TYPEN

	// BOF READ BANK TYPES
		$arrBankAccountTypeDatas = getBankAccountTypes();
	// EOF READ BANK TYPES

	// BOF READ ORDER CATEGORIES
		#$arrOrderCategoriesTypeDatas = getOrderCategories();
		$arrOrderCategoriesTypeDatas = getOrderProducts();
	// EOF READ ORDER CATEGORIES

	// BOF READ ORDER CATEGORIES
		$arrOrderSourceTypes = getOrdersSourceTypes();
	// EOF READ ORDER CATEGORIES

	// BOF READ MANDATORIES
		$arrMandatories = getAllMandatoryCompanyDatas();
		$arrMandatories = array_keys($arrMandatories);
	// EOF READ MANDATORIES

	// BOF COUNT STATUS TYPE
		$sql_countStatusType = "SELECT
					`" . TABLE_ORDER_STATUS_TYPES . "`.`orderStatusTypesID`,
					count(`" . TABLE_ORDERS . "`.`ordersID`) AS `ordersStatusCount`,
					`orderStatusTypesShortName`,
					`orderStatusTypesName`

					FROM `" . TABLE_ORDER_STATUS_TYPES . "`

					LEFT JOIN `" . TABLE_ORDERS . "`
					ON(`" . TABLE_ORDER_STATUS_TYPES . "`.`orderStatusTypesID` = `" . TABLE_ORDERS . "`.`ordersStatus`)

					WHERE 1  /* AND `ordersArchive` != '1' */

					GROUP BY `" . TABLE_ORDER_STATUS_TYPES . "`.`orderStatusTypesID`

					ORDER BY `" . TABLE_ORDER_STATUS_TYPES . "`.`orderStatusTypesID`
			";

		$rs_countStatusType = $dbConnection->db_query($sql_countStatusType);

		$countStatusTypesTotal = 0;
		while($ds_countStatusType = mysqli_fetch_assoc($rs_countStatusType)) {
			$countStatusTypesTotal += $ds_countStatusType["ordersStatusCount"];
			foreach(array_keys($ds_countStatusType) as $field) {
				$arrCountStatusTypes[$ds_countStatusType["orderStatusTypesID"]][$field] = $ds_countStatusType[$field];
			}
		}

	// EOF COUNT STATUS TYPE

?>
<?php
	require_once('inc/headerHTML.inc.php');
	$thisTitle = "&Uuml;bersicht Vorg&auml;nge";

	if($_POST["searchCustomerNumber"] != "") {
		$thisTitle .= " - Kundennummer ".$_POST["searchCustomerNumber"];
	}
	if($_POST["searchCustomerName"] != "") {
		$thisTitle .= " - Kunde ".$_POST["searchCustomerName"];
	}
	if($_POST["searchPLZ"] != "") {
		$thisTitle .= " - PLZ ".$_POST["searchPLZ"];
	}
	if($_POST["searchSalesmanName"] != "") {
		$thisTitle .= " - Vertreter ".$_POST["searchSalesmanName"];
	}
	if($_POST["searchWord"] != "") {
		$thisTitle .= " - Suchbegriff ".$_POST["searchWord"];
	}
	if($_REQUEST["displayOrderStatus"] != "") {
		$thisTitle .= " - Status ".$arrOrderStatusTypeDatas[$_REQUEST["displayOrderStatus"]]["orderStatusTypesName"];
	}
	if($_REQUEST["searchProductionStatus"] != '') {
		$thisTitle .= " - Status ".$arrOrderStatusTypeDatas[$_REQUEST["searchProductionStatus"]]["orderStatusTypesName"];
	}

	$headerHTML = preg_replace("/{###TITLE###}/", strip_tags($thisTitle), $headerHTML);
	echo $headerHTML;

	require_once(FILE_MENUE_TOP);

	// BOF CREATE SQL FOR DISPLAY ORDERS
	$sqlWhere = "";
	$sqlJoin = "";
	$sqlGroup = "";
	$sqlFields = "";

	if($_REQUEST["searchPLZ"] != "") {
		$sqlWhere = " AND `ordersPLZ` LIKE '" . $_REQUEST["searchPLZ"] . "%' ";
	}
	else if($_REQUEST["searchCustomerNumber"] != "") {
		// $sqlWhere = " AND `ordersKundennummer` = '" . $_REQUEST["searchCustomerNumber"] . "' ";

		$sqlWhere = "
					AND (
						`ordersKundennummer` = '" . $_REQUEST["searchCustomerNumber"] . "'
						OR `salesmen`.`customersKundennummer` = '" . $_REQUEST["searchCustomerNumber"] . "'
					)
				";

		$sqlJoin = "
				LEFT JOIN `" . TABLE_CUSTOMERS . "` AS `salesmen`
				ON(`" . TABLE_ORDERS . "`.`ordersVertreterID` = `salesmen`.`customersID`)

			";
	}
	else if($_REQUEST["searchCustomerName"] != "") {
		$sqlWhere = " AND (`ordersKundenName` LIKE '%" . $_REQUEST["searchCustomerName"] . "%' OR `ordersKommission` LIKE '%" . $_REQUEST["searchCustomerName"] . "%') ";
		// $sqlWhere = " AND MATCH(`ordersKundenName`) AGAINST ('" . $_REQUEST["searchCustomerName"] . "') ";
	}
	else if($_REQUEST["searchSalesmanName"] != "") {
		$sqlWhere = " AND `searchSalesmanName` LIKE '" . $_REQUEST["searchSalesmanName"] . "%' ";
		// $sqlWhere = " AND MATCH(`ordersVertreter`) AGAINST ('" . $_REQUEST["searchSalesmanName"] . "') ";
	}
	else if($_REQUEST["searchSalesmanData"] != "") {
		$arrTemp = explode("###", $_REQUEST["searchSalesmanData"]);
		$sqlWhere = " AND (
						`ordersVertreter` = '" . $arrTemp[0] . "'
						OR `ordersVertreterID` = '" . $arrTemp[0] . "'
					)
			";
	}
	else if($_REQUEST["searchDocumentNumber"] != "") {
		
		foreach($arrMandatories as $thisMandatory){
			$thisTableConfirmations = preg_replace("/^" . strtolower(MANDATOR) . "_/", strtolower($thisMandatory) . "_", TABLE_ORDER_CONFIRMATIONS);
			$thisTableConfirmationPayments = preg_replace("/^" . strtolower(MANDATOR) . "_/", strtolower($thisMandatory) . "_", TABLE_ORDER_CONFIRMATION_PAYMENTS);
			$thisTableConfirmationDetails = preg_replace("/^" . strtolower(MANDATOR) . "_/", strtolower($thisMandatory) . "_", TABLE_ORDER_CONFIRMATIONS_DETAILS);

			$sqlJoin .= "
					LEFT JOIN `" . $thisTableConfirmationDetails . "`
					ON(`" . TABLE_ORDERS . "`.`ordersID` = `" . $thisTableConfirmationDetails . "`.`orderDocumentDetailOrderID` AND `" . $thisTableConfirmationDetails . "`.`orderDocumentDetailPosType` = 'product')

					LEFT JOIN `" . $thisTableConfirmations . "`
					ON(`" . $thisTableConfirmationDetails . "`.`orderDocumentDetailDocumentID` = `" . $thisTableConfirmations . "`.`orderDocumentsID`)
				";
		}

		$sqlGroup = " GROUP BY `" . TABLE_ORDERS . "`.`ordersID` ";

		$sqlWhere = "
				AND (
					`bctr_orderConfirmations`.`orderDocumentsNumber` LIKE '%" . $_REQUEST["searchDocumentNumber"] . "%'
					OR `bctr_orderConfirmations`.`orderDocumentsCustomersOrderNumber` LIKE '%" . $_REQUEST["searchDocumentNumber"] . "%'
					OR `b3_orderConfirmations`.`orderDocumentsNumber` LIKE '" . $_REQUEST["searchDocumentNumber"] . "%'
					OR `b3_orderConfirmations`.`orderDocumentsCustomersOrderNumber` LIKE '%" . $_REQUEST["searchDocumentNumber"] . "%'
				)
			";

		/*
		$sqlGroup = " GROUP BY `" . TABLE_ORDERS . "`.`ordersID` ";

		$sqlWhere = "
				AND (
					`" . VIEW_ORDERS_TO_CONFIRMATIONS . "`.`orderDocumentNumber` LIKE '%" . $_REQUEST["searchDocumentNumber"] . "%'
					OR
					`" . VIEW_ORDERS_TO_CONFIRMATIONS . "`.`orderCustomersOrderNumber` LIKE '" . $_REQUEST["searchDocumentNumber"] . "%'
				)
			";
		*/
	}
	else if($_REQUEST["searchWord"] != "") {
		$sqlWhere = " AND (
						`" . TABLE_ORDERS . "`.`ordersPLZ` LIKE '" . $_REQUEST["searchWord"] . "%'
						OR `" . TABLE_ORDERS . "`.`ordersKundennummer` LIKE '" . $_REQUEST["searchWord"] . "%'
						OR `" . TABLE_ORDERS . "`.`ordersKundenName` LIKE '%" . $_REQUEST["searchWord"] . "%'
						OR `" . TABLE_ORDERS . "`.`ordersKommission` LIKE '%" . $_REQUEST["searchWord"] . "%'
						OR `" . TABLE_ORDERS . "`.`ordersAufdruck` LIKE '%" . $_REQUEST["searchWord"] . "%'
						OR `" . TABLE_ORDERS . "`.`ordersOrt` LIKE '%" . $_REQUEST["searchWord"] . "%'
						OR `" . TABLE_ORDERS . "`.`ordersVertreter` LIKE '%" . $_REQUEST["searchWord"] . "%'
						OR `" . TABLE_ORDERS . "`.`ordersArtikelBezeichnung` LIKE '%" . $_REQUEST["searchWord"] . "%'
						OR `" . TABLE_ORDERS . "`.`ordersArtikelNummer` LIKE '%" . $_REQUEST["searchWord"] . "%'
						OR `" . TABLE_ORDERS . "`.`ordersDruckFarbe` LIKE '%" . $_REQUEST["searchWord"] . "%'
						/*
						OR `" . VIEW_ORDERS_TO_CONFIRMATIONS . "`.`orderDocumentNumber` LIKE '%" . $_REQUEST["searchWord"] . "%'
						OR `" . VIEW_ORDERS_TO_CONFIRMATIONS . "`.`orderCustomersOrderNumber` LIKE '%" . $_REQUEST["searchWord"] . "%'
						*/
					)
			";

	}
	else if($_GET["searchBoxProduction"] != ""){
		$sqlJoin = "";
		
		foreach($arrMandatories as $thisMandatory){
			$thisTableConfirmations = preg_replace("/^" . strtolower(MANDATOR) . "_/", strtolower($thisMandatory) . "_", TABLE_ORDER_CONFIRMATIONS);
			$thisTableConfirmationPayments = preg_replace("/^" . strtolower(MANDATOR) . "_/", strtolower($thisMandatory) . "_", TABLE_ORDER_CONFIRMATION_PAYMENTS);
			$thisTableConfirmationDetails = preg_replace("/^" . strtolower(MANDATOR) . "_/", strtolower($thisMandatory) . "_", TABLE_ORDER_CONFIRMATIONS_DETAILS);

			$sqlJoin .= "
					LEFT JOIN `" . $thisTableConfirmationDetails . "`
					ON(`" . TABLE_ORDERS . "`.`ordersID` = `" . $thisTableConfirmationDetails . "`.`orderDocumentDetailOrderID` AND `" . $thisTableConfirmationDetails . "`.`orderDocumentDetailPosType` = 'product')

					LEFT JOIN `" . $thisTableConfirmations . "`
					ON(`" . $thisTableConfirmationDetails . "`.`orderDocumentDetailDocumentID` = `" . $thisTableConfirmations . "`.`orderDocumentsID`)
				";
		}		

		$sqlGroup = " GROUP BY `" . TABLE_ORDERS . "`.`ordersID` ";

		$sqlWhere = " AND (
						`" . TABLE_ORDERS . "`.`ordersPLZ` LIKE '" . addslashes($_GET["searchBoxProduction"]) . "%'
						OR `" . TABLE_ORDERS . "`.`ordersKundennummer` LIKE '" . addslashes($_GET["searchBoxProduction"]) . "%'
						OR `" . TABLE_ORDERS . "`.`ordersKundenName` LIKE '%" . addslashes($_GET["searchBoxProduction"]) . "%'
						OR `" . TABLE_ORDERS . "`.`ordersKommission` LIKE '%" . addslashes($_GET["searchBoxProduction"]) . "%'						
						OR `" . TABLE_ORDERS . "`.`ordersOrt` LIKE '%" . addslashes($_GET["searchBoxProduction"]) . "%'						
						OR `" . TABLE_ORDERS . "`.`ordersVertreter` LIKE '%" . addslashes($_GET["searchBoxProduction"]) . "%'						
						OR `" . TABLE_ORDERS . "`.`ordersArtikelBezeichnung` LIKE '%" . addslashes($_GET["searchBoxProduction"]) . "%'
						OR `" . TABLE_ORDERS . "`.`ordersArtikelNummer` LIKE '%" . addslashes($_GET["searchBoxProduction"]) . "%'
						OR `" . TABLE_ORDERS . "`.`ordersDruckFarbe` LIKE '%" . addslashes($_GET["searchBoxProduction"]) . "%'
												
						OR `bctr_orderConfirmations`.`orderDocumentsNumber` = '" . addslashes($_GET["searchBoxProduction"]) . "'
						OR `bctr_orderConfirmations`.`orderDocumentsCustomersOrderNumber` = '" . addslashes($_GET["searchBoxProduction"]) . "'
						OR `b3_orderConfirmations`.`orderDocumentsNumber` = '" . addslashes($_GET["searchBoxProduction"]) . "'
						OR `b3_orderConfirmations`.`orderDocumentsCustomersOrderNumber` = '" . addslashes($_GET["searchBoxProduction"]) . "'
						
						/*
						OR `" . VIEW_ORDERS_TO_CONFIRMATIONS . "`.`orderDocumentNumber` LIKE '%" . addslashes($_GET["searchBoxProduction"]) . "%'
						OR `" . VIEW_ORDERS_TO_CONFIRMATIONS . "`.`orderCustomersOrderNumber` LIKE '%" . addslashes($_GET["searchBoxProduction"]) . "%'
						*/
					)
			";
	}
	else if($_REQUEST["searchOverdueProductions2"] == "1") {
		$sqlWhere = "
				AND (
					`" . TABLE_ORDERS . "`.`ordersStatus` = '2'
					AND `" . TABLE_ORDERS . "`.`ordersArtikelPrintColorsCount` = '0'
					AND `" . TABLE_ORDERS . "`.`ordersMandant` = '" . strtolower(MANDATOR) . "'
					AND(
						IF(
							`" . TABLE_ORDERS . "`.`ordersFreigabeDatum` > 0, DATE_ADD(`" . TABLE_ORDERS . "`.`ordersFreigabeDatum`, INTERVAL 30 DAY),
							DATE_ADD(`" . TABLE_ORDERS . "`.`ordersBestellDatum`, INTERVAL 5 DAY)
						) < DATE_FORMAT(NOW(), '%Y-%m-%d')
					)
				)
			";
	}
	else if($_REQUEST["displayOrderStatus"] != "") {
		$sqlWhere = " AND `ordersStatus` = '" . $_REQUEST["displayOrderStatus"] . "' ";
	}
	else if($_REQUEST["searchOrdersID"] != "") {
		$sqlWhere = " AND `ordersID` = '" . $_REQUEST["searchOrdersID"] . "' ";
	}
	else if($_REQUEST["searchProductionStatus"] != '') {
		$sqlWhere .= " AND `ordersStatus` = '" . $_REQUEST["searchProductionStatus"] . "' ";
	}
	else {
		$sqlWhere = "";
	}

	if(!empty($_REQUEST["arrSearchProductionStatus"])){
		$sqlWhere .= " AND `" . TABLE_ORDERS . "`.`ordersStatus` IN(" . implode(", ", array_keys($_REQUEST["arrSearchProductionStatus"])) . ") ";
	}

	if($_REQUEST["searchOverdueProductions"] == "1" || $_REQUEST["searchUpcomingProductions"] == "1" || $_REQUEST["searchTimeframeProductions"] == "1") {
		$sqlWhere_ProductionTime = array();

		if(!empty($arrMandatories)){
			foreach($arrMandatories as $thisMandatory){
				$thisTableConfirmations = preg_replace("/^" . strtolower(MANDATOR) . "_/", strtolower($thisMandatory) . "_", TABLE_ORDER_CONFIRMATIONS);
				$thisTableConfirmationPayments = preg_replace("/^" . strtolower(MANDATOR) . "_/", strtolower($thisMandatory) . "_", TABLE_ORDER_CONFIRMATION_PAYMENTS);
				$thisTableConfirmationDetails = preg_replace("/^" . strtolower(MANDATOR) . "_/", strtolower($thisMandatory) . "_", TABLE_ORDER_CONFIRMATIONS_DETAILS);

				$sqlFields .= "
						,
						`" . $thisTableConfirmationPayments . "`.`orderPaymentOrderNumber` AS `" . $thisMandatory . "_orderPaymentOrderNumber`,
						`" . $thisTableConfirmationPayments . "`.`orderPaymentDate` AS `" . $thisMandatory . "_orderPaymentDate`,
						`" . $thisTableConfirmations . "`.`orderDocumentsStatus` AS `" . $thisMandatory . "_orderDocumentsStatus`,

						`" . $thisTableConfirmations . "`.`orderDocumentsCustomerNumber` AS `" . $thisMandatory . "_orderDocumentsCustomerNumber`,
						`" . $thisTableConfirmations . "`.`orderDocumentsCustomersOrderNumber` AS `" . $thisMandatory . "_orderDocumentsCustomersOrderNumber`,
						`" . $thisTableConfirmations . "`.`orderDocumentsDocumentDate` AS `" . $thisMandatory . "_orderDocumentsDocumentDate`,

						`" . $thisTableConfirmations . "`.`orderDocumentsPaymentCondition` AS `" . $thisMandatory . "_orderDocumentsPaymentCondition`,
						`" . $thisTableConfirmations . "`.`orderDocumentsPaymentType` AS `" . $thisMandatory . "_orderDocumentsPaymentType`
					";

				$sqlJoin .= "
						LEFT JOIN `" . $thisTableConfirmationDetails . "`
						ON(`" . TABLE_ORDERS . "`.`ordersID` = `" . $thisTableConfirmationDetails . "`.`orderDocumentDetailOrderID` AND `" . $thisTableConfirmationDetails . "`.`orderDocumentDetailPosType` = 'product')

						LEFT JOIN `" . $thisTableConfirmations . "`
						ON(`" . $thisTableConfirmationDetails . "`.`orderDocumentDetailDocumentID` = `" . $thisTableConfirmations . "`.`orderDocumentsID`)

						LEFT JOIN `" . $thisTableConfirmationPayments . "`
						ON(`" . $thisTableConfirmations . "`.`orderDocumentsNumber` = `" . $thisTableConfirmationPayments . "`.`orderPaymentOrderNumber`)
					";
			}
		}

		if($_REQUEST["searchOverdueProductions"] == "1") {
			$sqlWhere_ProductionTime[] = "
					CONCAT(
							IF(`" . TABLE_ORDERS . "`.`ordersFreigabeDatum` != '0000-00-00', DATE_FORMAT(`" . TABLE_ORDERS . "`.`ordersFreigabeDatum`, '%Y'),
								DATE_FORMAT(`" . TABLE_ORDERS . "`.`ordersBestellDatum`, '%Y')
							),
							'-',
							`" . TABLE_ORDERS . "`.`ordersLieferwoche`
						) < '" . date('Y-W') . "'
				";
		}
		if($_REQUEST["searchTimeframeProductions"] == "1") {
			$sqlWhere_ProductionTime[] = "
					CONCAT(
							IF(`" . TABLE_ORDERS . "`.`ordersFreigabeDatum` != '0000-00-00', DATE_FORMAT(`" . TABLE_ORDERS . "`.`ordersFreigabeDatum`, '%Y'),
								DATE_FORMAT(`" . TABLE_ORDERS . "`.`ordersBestellDatum`, '%Y')
							),
							'-',
							`" . TABLE_ORDERS . "`.`ordersLieferwoche`
						) BETWEEN '" . date('Y') . "-" . date('W') . "' AND '" . date('Y') . "-" . (date('W') + 2) . "'
				";
		}
		if($_REQUEST["searchUpcomingProductions"] == "1") {
			$sqlWhere_ProductionTime[] = "
					CONCAT(
							IF(`" . TABLE_ORDERS . "`.`ordersFreigabeDatum` != '0000-00-00', DATE_FORMAT(`" . TABLE_ORDERS . "`.`ordersFreigabeDatum`, '%Y'),
								DATE_FORMAT(`" . TABLE_ORDERS . "`.`ordersBestellDatum`, '%Y')
							),
							'-',
							`" . TABLE_ORDERS . "`.`ordersLieferwoche`
						) BETWEEN '" . date('Y') . "-" . (date('W') + 2) . "' AND '" . date('Y') . "-" . (date('W') + 3) . "'
				";
		}
		if(!empty($sqlWhere_ProductionTime)){
			$sqlWhere .= "
					AND (
						`" . TABLE_ORDERS . "`.`ordersStatus` = '2'
						/*
						AND(
							IF(
								`" . TABLE_ORDERS . "`.`ordersFreigabeDatum` != '0000-00-00', DATE_ADD(`" . TABLE_ORDERS . "`.`ordersFreigabeDatum`, INTERVAL 30 DAY),
								DATE_ADD(`" . TABLE_ORDERS . "`.`ordersBestellDatum`, INTERVAL 30 DAY)
							) < DATE_FORMAT(NOW(), '%Y-%m-%d')
						)
						*/
						AND (
							" . implode(" OR ", $sqlWhere_ProductionTime) . "
						)
					)
				";

			$sqlWhere .= "
					AND (
							(`bctr_orderConfirmations`.`orderDocumentsStatus` = '2' AND `bctr_orderConfirmations`.`orderDocumentsPaymentType` = '2')
							OR (`b3_orderConfirmations`.`orderDocumentsStatus` = '2' AND `b3_orderConfirmations`.`orderDocumentsPaymentType` = '2')
							OR (`bctr_orderConfirmations`.`orderDocumentsStatus` = '1' AND `bctr_orderConfirmations`.`orderDocumentsPaymentType` != '2')
							OR (`b3_orderConfirmations`.`orderDocumentsStatus` = '1' AND `b3_orderConfirmations`.`orderDocumentsPaymentType` != '2')
					)
				";
		}
	}

	if($_REQUEST["searchProductionPrintType"] != "" || $_REQUEST["searchProductCategory"] != "") {
		$sqlWhere_PrintType = "";
		$sqlWhere_ProductCategory = "";

		if($_REQUEST["searchProductionPrintType"] != "") {
			$sqlWhere_PrintType = " AND `ordersAdditionalCostsID` = '" . $_REQUEST["searchProductionPrintType"] . "' ";
		}

		if($_REQUEST["searchProductCategory"] != "") {
			$arrSearchProductCategories = array(
				"KZH" => array("001", "002", "005"),
				"MIN" => array("003"),
				"AWT" => array("006")
			);
			$arrMergedSearchProductCategories = array_unique(call_user_func_array('array_merge', $arrSearchProductCategories));

			if($_REQUEST["searchProductCategory"] != "OTHERS"){
				$sqlWhere_ProductCategory = " AND SUBSTRING(`ordersArtikelKategorieID`, 1, 3) IN (" . implode(", ", $arrSearchProductCategories[$_REQUEST["searchProductCategory"]]) . ") ";
			}
			else if($_REQUEST["searchProductCategory"] == "OTHERS"){
				$sqlWhere_ProductCategory = "
					AND SUBSTRING(`ordersArtikelKategorieID`, 1, 3) NOT IN (" . implode(", ", $arrMergedSearchProductCategories) . ")
				";
			}
		}
		$sqlWhere .= $sqlWhere_PrintType  . $sqlWhere_ProductCategory;
	}

	$sqlSortField = "ORDER BY " . $sqlSortField . " ";

	$sqlGetPrintProductionFilesLeftJoin = "";
	$sqlGetPrintProductionFilesFields = "";

	if(SHOW_PRINT_PRODUCTION_FILES){
		/*
		$xxx_sqlGetPrintProductionFilesLeftJoin = "
				LEFT JOIN (
					SELECT
						`". TABLE_PRINT_PRODUCTION_FILES . "`.`printProductionFilesFileDate`,
						GROUP_CONCAT(IF(`". TABLE_PRINT_PRODUCTION_FILES . "`.`printProductionFilesFileName` IS NULL, '', `". TABLE_PRINT_PRODUCTION_FILES . "`.`printProductionFilesFileName`)) AS `printProductionFilesFileNames`
					FROM `". TABLE_PRINT_PRODUCTION_FILES . "`
					GROUP BY `". TABLE_PRINT_PRODUCTION_FILES . "`.`printProductionFilesFileDate`
				) AS `tempPrintProductionFiles`

				ON(
					`" . TABLE_ORDERS . "`.`ordersProduktionsDatum` = `tempPrintProductionFiles`.`printProductionFilesFileDate`
					OR
					`" . TABLE_ORDERS . "`.`ordersBelichtungsDatum` = `tempPrintProductionFiles`.`printProductionFilesFileDate`
				)
			";
		$sqlGetPrintProductionFilesFields = "
				,
				`tempPrintProductionFiles`.`printProductionFilesFileNames`,
				`tempPrintProductionFiles`.`printProductionFilesFileDate`
			";
		*/


		$sqlGetPrintProductionFilesLeftJoin = "
				LEFT JOIN `". TABLE_PRINT_PRODUCTION_FILES . "`
				ON(
					IF(`" . TABLE_ORDERS . "`.`ordersBelichtungsDatum` != '0000-00-00',
						`" . TABLE_ORDERS . "`.`ordersBelichtungsDatum`,
						`" . TABLE_ORDERS . "`.`ordersProduktionsDatum`
					)  = `". TABLE_PRINT_PRODUCTION_FILES . "`.`printProductionFilesFileDate`
				)
			";

		$sqlGetPrintProductionFilesFields = "
				,
				`". TABLE_PRINT_PRODUCTION_FILES . "`.`printProductionFilesFileDate`,
				GROUP_CONCAT(IF(`". TABLE_PRINT_PRODUCTION_FILES . "`.`printProductionFilesFileName` IS NULL, '', `". TABLE_PRINT_PRODUCTION_FILES . "`.`printProductionFilesFileName`)) AS `printProductionFilesFileNames`
			";


		$sqlGetPrintProductionFilesGroup = " GROUP BY `" . TABLE_ORDERS . "`.`ordersID` ";
	}

	if(SHOW_PRINT_PRODUCTION_FILES) {
		$sqlGroup = " GROUP BY `" . TABLE_ORDERS . "`.`ordersID` ";
	}

	$sql = "SELECT
				/* SQL_CALC_FOUND_ROWS */

				`" . TABLE_ORDERS . "`.`ordersID`,
				`" . TABLE_ORDERS . "`.`ordersBestellDatum`,
				`" . TABLE_ORDERS . "`.`ordersStornoDatum`,
				`" . TABLE_ORDERS . "`.`ordersFreigabeDatum`,
				`" . TABLE_ORDERS . "`.`ordersBelichtungsDatum`,
				`" . TABLE_ORDERS . "`.`ordersProduktionsDatum`,
				`" . TABLE_ORDERS . "`.`ordersAuslieferungsDatum`,
				`" . TABLE_ORDERS . "`.`ordersKundennummer`,
				`" . TABLE_ORDERS . "`.`ordersKundenName`,
				`" . TABLE_ORDERS . "`.`ordersKommission`,
				`" . TABLE_ORDERS . "`.`ordersInfo`,
				`" . TABLE_ORDERS . "`.`ordersPLZ`,
				`" . TABLE_ORDERS . "`.`ordersOrt`,
				`" . TABLE_ORDERS . "`.`ordersArtikelMenge`,
				`" . TABLE_ORDERS . "`.`ordersArtikelPrintColorsCount`,
				`" . TABLE_ORDERS . "`.`ordersArtikelWithBorder`,
				`" . TABLE_ORDERS . "`.`ordersArtikelWithClearPaint`,
				`" . TABLE_ORDERS . "`.`ordersAufdruck`,
				`" . TABLE_ORDERS . "`.`ordersSinglePreis`,
				`" . TABLE_ORDERS . "`.`ordersTotalPreis`,
				`" . TABLE_ORDERS . "`.`ordersArtikelBezeichnung`,
				`" . TABLE_ORDERS . "`.`ordersArtikelID`,
				`" . TABLE_ORDERS . "`.`ordersArtikelNummer`,
				`" . TABLE_ORDERS . "`.`ordersArtikelKategorieID`,

				`" . TABLE_ORDERS . "`.`ordersAdditionalArtikelKategorieID`,
				`" . TABLE_ORDERS . "`.`ordersAdditionalArtikelMenge`,

				`" . TABLE_ORDERS . "`.`ordersDruckFarbe`,
				`" . TABLE_ORDERS . "`.`ordersAdditionalCosts`,
				`" . TABLE_ORDERS . "`.`ordersAdditionalCostsID`,
				`" . TABLE_ORDERS . "`.`ordersVertreter`,
				`" . TABLE_ORDERS . "`.`ordersVertreterID`,
				`" . TABLE_ORDERS . "`.`ordersMailAdress`,
				`" . TABLE_ORDERS . "`.`ordersContactOthers`,
				`" . TABLE_ORDERS . "`.`ordersMandant`,
				`" . TABLE_ORDERS . "`.`ordersLieferwoche`,
				`" . TABLE_ORDERS . "`.`ordersLieferDatum`,
				`" . TABLE_ORDERS . "`.`ordersDruckerName`,
				`" . TABLE_ORDERS . "`.`ordersNotizen`,
				`" . TABLE_ORDERS . "`.`ordersStatus`,
				`" . TABLE_ORDERS . "`.`ordersOrderType`,
				`" . TABLE_ORDERS . "`.`ordersSourceName`,
				`" . TABLE_ORDERS . "`.`ordersSourceType`,
				`" . TABLE_ORDERS . "`.`ordersPerExpress`,
				`" . TABLE_ORDERS . "`.`ordersBankAccountType`,
				`" . TABLE_ORDERS . "`.`ordersPaymentType`,
				`" . TABLE_ORDERS . "`.`ordersPaymentStatusType`,
				`" . TABLE_ORDERS . "`.`ordersUserID`,
				`" . TABLE_ORDERS . "`.`ordersTimeCreated`,
				`" . TABLE_ORDERS . "`.`ordersDirectSale`,
				`" . TABLE_ORDERS . "`.`ordersReklamationsgrund`,

				`" . TABLE_ORDERS . "`.`ordersProductionPrintingPlant`,
				`" . TABLE_ORDERS . "`.`ordersPrintPlateNumber`,

				`" . TABLE_SUPPLIER_ORDERS . "`.`supplierOrdersID`,
				`" . TABLE_SUPPLIER_ORDERS . "`.`supplierOrdersAmount`,
				`" . TABLE_SUPPLIER_ORDERS . "`.`supplierOrdersProductNumber`,
				`" . TABLE_SUPPLIER_ORDERS . "`.`supplierOrdersProductName`,
				`" . TABLE_SUPPLIER_ORDERS . "`.`supplierOrdersOrderDate`,
				`" . TABLE_SUPPLIER_ORDERS . "`.`supplierOrdersDeliveryDate`,

				`" . TABLE_SUPPLIERS . "`.`suppliersFirmenname`,

				`" . TABLE_PRINT_PRODUCTION_ORDERS . "`.`printProductionOrdersID`,
				`" . TABLE_PRINT_PRODUCTION_ORDERS . "`.`printProductionOrdersProductDatas`,

				`" . TABLE_PRODUCTIONS_TRANSFER . "`.`ordersProductionsTransferID`,

				`" . TABLE_PRODUCTIONS_TRANSFER . "`.`ordersProductionsTransferOrdersID`,

				`" . TABLE_PRODUCTIONS_TRANSFER . "`.`ordersProductionsTransferDeliveryRedDot`,
				`" . TABLE_PRODUCTIONS_TRANSFER . "`.`ordersProductionsTransferDeliveryNoDPD`,
				`" . TABLE_PRODUCTIONS_TRANSFER . "`.`ordersProductionsTransferDeliveryContainerDate`,

				`" . TABLE_PRODUCTIONS_TRANSFER . "`.`ordersProductionsTransferMailSubmitDateTime`,
				`" . TABLE_PRODUCTIONS_TRANSFER . "`.`ordersProductionsTransferDeliveryPrintDate`,

				`" . TABLE_PRODUCTIONS_TRANSFER . "`.`ordersProductionsTransferOrdersProductQuantity`,
				`" . TABLE_PRODUCTIONS_TRANSFER . "`.`ordersProductionsTransferOrdersProductOriginalQuantity`,

				`" . TABLE_PRODUCTIONS_TRANSFER . "`.`ordersProductionsTransferProductionStatus`,
				`" . TABLE_PRODUCTIONS_TRANSFER . "`.`ordersProductionsTransferProductionTrackingNumbers`,
				`" . TABLE_PRODUCTIONS_TRANSFER . "`.`ordersProductionsTransferProductionModified`,
				`" . TABLE_PRODUCTIONS_TRANSFER . "`.`ordersProductionsTransferUseNoNeutralPacking`

				/*
				`" . VIEW_ORDERS_TO_CONFIRMATIONS . "`.`orderDocumentNumber`,
				`" . VIEW_ORDERS_TO_CONFIRMATIONS . "`.`orderCustomersOrderNumber`
				*/

				" . $sqlGetPrintProductionFilesFields . "
				" . $sqlFields . "

				FROM `" . TABLE_ORDERS . "`

				LEFT JOIN `" . TABLE_SUPPLIER_ORDERS . "`
				ON(`" . TABLE_ORDERS . "`.`ordersID` = `" . TABLE_SUPPLIER_ORDERS . "`.`supplierOrdersOrderProcessID`)

				LEFT JOIN `" . TABLE_SUPPLIERS . "`
				ON(`" . TABLE_SUPPLIER_ORDERS . "`.`supplierOrdersSupplierID` = `" . TABLE_SUPPLIERS . "`.`suppliersID`)

				LEFT JOIN `" . TABLE_PRODUCTIONS_TRANSFER . "`
				ON(`" . TABLE_ORDERS . "`.`ordersID` = `" . TABLE_PRODUCTIONS_TRANSFER . "`.`ordersProductionsTransferOrdersID`)

				LEFT JOIN `" . TABLE_PRINT_PRODUCTION_ORDERS . "`
				ON(`" . TABLE_ORDERS . "`.`ordersID` = `" . TABLE_PRINT_PRODUCTION_ORDERS . "`.`printProductionOrdersOrderID`)

				/*
				LEFT JOIN `" . VIEW_ORDERS_TO_CONFIRMATIONS . "`
				ON(`" . TABLE_ORDERS . "`.`ordersID` = `" . VIEW_ORDERS_TO_CONFIRMATIONS . "`.`orderID`)
				*/

				" . $sqlJoin . "
				" . $sqlGetPrintProductionFilesLeftJoin . "

				WHERE 1 /* AND `ordersArchive` != '1' */
					" . $sqlWhere . "
					" . $sqlGroup . "
					". $sqlSortField . "
					". $sqlSortDirection . "
			";
	// EOF CREATE SQL FOR DISPLAY ORDERS	

?>
<div id="menueSidebarToggleArea">
	<img src="layout/icons/iconToggle.png" class="buttonToggleSidebarMenue" width="10" height="10" alt="" title="Seitennavigation ein-/ausblenden" />
	<div id="menueSidebarToggleContent">
	<?php require_once(FILE_MENUE_SIDEBAR); ?>
	<div class="clear"></div>
	</div>
</div>
<?php require_once(FILE_MENUE_SIDEBAR); ?>
<div id="contentArea2">
	<a name="top"></a>
	<div id="contentAreaElements">
		<h1><?php if(SHOW_PAGE_TITLE_ICONS) { echo '<img src="' . PATH_ICONS_MENUE_TITLES . 'ordersoverview.png" alt="" />'; } ?> <?php echo $thisTitle; ?></h1>

		<div id="displayStatusCountArea">
			<span class="displayStatusCount"><b><a href="<?php echo PAGE_DISPLAY_ORDERS_OVERVIEW; ?>" title="Alle Vorg&auml;nge anzeigen">Insgesamt</a>:</b> <?php echo $countStatusTypesTotal; ?></span>
			<?php
				if(!empty($arrCountStatusTypes)) {
					foreach($arrCountStatusTypes as $thisKey => $thisValue) {
						$thisStyle = "";
						if($thisValue["orderStatusTypesID"] == $_REQUEST["displayOrderStatus"]) {
							$thisStyle = "text-decoration:underline";
						}
						echo ' &bull; <span class="displayStatusCount"><b><a href="?displayOrderStatus='.$thisValue["orderStatusTypesID"].'" style="' . $thisStyle . '" title="Als &quot;'.$thisValue["orderStatusTypesName"].'&quot; markierte Vorg&auml;nge anzeigen">'.$thisValue["orderStatusTypesName"].'</a>:</b> '.$thisValue["ordersStatusCount"].'</span>';
					}
				}
			?>
			<span class="displayStatusCount">
				<?php
					$processlinkAdd = '';
					if($_REQUEST["searchCustomerNumber"] != ''){
						$processLinkAdd = '&amp;editCustomersNumber=' . $_REQUEST["searchCustomerNumber"];
					}
				?>
				<a href="<?php echo PAGE_EDIT_PROCESS; ?>?editID=NEW<?php echo $processLinkAdd; ?>" class="linkButton inputButtonGreen" title="Neue Produktion anlegen" ><span>Neue Produktion anlegen</span></a>
			</span>
		</div>
		<div id="searchFilterArea">
			<form name="formFilterSearch" method="post" action="<?php echo $_SERVER["PHP_SELF"]; ?>">
			<table border="0" cellpadding="0" cellspacing="0" class="searchFilterContent">
				<tr>
					<td>
						<label for="searchCustomerNumber">Kundennr.:</label>
						<input type="text" name="searchCustomerNumber" id="searchCustomerNumber" class="inputField_70" value="<?php echo $_REQUEST["searchCustomerNumber"]; ?>" />
					</td>
					<td>
						<label for="searchCustomerName">Kundenname:</label>
						<input type="text" name="searchCustomerName" id="searchCustomerName" class="inputField_120" value="<?php echo $_REQUEST["searchCustomerName"]; ?>" />
					</td>
					<td>
						<label for="searchPLZ">PLZ:</label>
						<input type="text" name="searchPLZ" id="searchPLZ" class="inputField_40" value="<?php echo $_REQUEST["searchPLZ"]; ?>" />
					</td>
					<?php if(!1){ ?>
					<!--
					<td>
						<label for="searchSalesmanID">Vertreter:</label>
						<!--
						<input type="text" name="searchSalesmanName" id="searchSalesmanName" class="inputField_160" />
						-->
						<!--
						<select name="searchSalesmanID" class="inputSelect_120">
							<option value=""> </option>
							<?php
								if(!empty($arrAgentDatas2)) {
									foreach($arrAgentDatas2 as $thisKey => $thisValue) {
										$selected = '';
										// if($arrAgentDatas2[$thisKey]["customersFirmenname"] == $_REQUEST["searchSalesmanName"]) {
										if($thisKey == $_REQUEST["searchSalesmanID"]) {
											$selected = ' selected="selected" ';
										}
										// echo '<option value="' . $thisKey . '" '.$selected.' >' . ($arrAgentDatas[$thisKey]["customersFirmenname"]). ' | '.$arrAgentDatas[$thisKey]["customersKundennummer"].'</option>';
										// echo '<option value="' . $arrAgentDatas2[$thisKey]["customersFirmenname"] . '" '.$selected.' >' . ($arrAgentDatas2[$thisKey]["customersFirmenname"]). ' | '.$arrAgentDatas2[$thisKey]["customersKundennummer"].'</option>';
										echo '<option value="' . $thisKey. '" '.$selected.' >' . htmlentities($arrAgentDatas2[$thisKey]["customersFirmenname"]). ' | '.$arrAgentDatas2[$thisKey]["customersKundennummer"].'</option>';
									}
								}
							?>
						</select>
					</td>
					-->
					<?php } ?>
					<td>
						<label for="searchWord">AB oder Auftragsnr:</label>
						<input type="text" name="searchDocumentNumber" id="searchDocumentNumber" class="inputField_120" value="<?php echo $_REQUEST["searchDocumentNumber"]; ?>" />
					</td>
					<td>
						<label for="searchSalesmanData">Vertreter:</label>
						<select name="searchSalesmanData" id="searchSalesmanData" class="inputSelect_120">
							<option value=""> </option>
							<?php
								if(!empty($arrAgentDatas2)) {
									foreach($arrAgentDatas2 as $thisKey => $thisValue) {
										$selected = '';
										if($thisKey == $_REQUEST["searchSalesmanData"]) {
											$selected = ' selected="selected" ';
										}
										// echo '<option value="' . $thisKey . '" '.$selected.' >' . ($arrAgentDatas[$thisKey]["customersFirmenname"]). ' | '.$arrAgentDatas[$thisKey]["customersKundennummer"].'</option>';
										// echo '<option value="' . $arrAgentDatas2[$thisKey]["customersFirmenname"] . '" '.$selected.' >' . ($arrAgentDatas2[$thisKey]["customersFirmenname"]). ' | '.$arrAgentDatas2[$thisKey]["customersKundennummer"].'</option>';
										echo '<option value="' . $thisKey. '###' . $arrAgentDatas2[$thisKey]["customersKundennummer"] . '" '.$selected.' >' . htmlentities($arrAgentDatas2[$thisKey]["customersFirmenname"]). ' | '.$arrAgentDatas2[$thisKey]["customersKundennummer"].'</option>';
									}
								}
							?>
						</select>
					</td>

					<!--
					<td>
						<label for="searchProductionStatus">Status:</label>
						<select name="searchProductionStatus" id="searchProductionStatus" class="inputSelect_100">
						<option value=""> </option>
						<?php
							if(!empty($arrOrderStatusTypeDatas)) {
								foreach($arrOrderStatusTypeDatas as $thisKey => $thisValue) {
									$selected = '';
									if($thisKey == $_REQUEST["searchProductionStatus"]) {
										$selected = ' selected="selected" ';
									}
									echo '<option value="' . $thisKey. '" '.$selected.' >' . htmlentities($arrOrderStatusTypeDatas[$thisKey]["orderStatusTypesName"]) . '</option>';
								}
							}
						?>
					</td>
					-->

					<td class="legendSeperator"></td>

					<td>
						<label for="searchProductionPrintType">Druckart:</label>
						<select name="searchProductionPrintType" id="searchProductionPrintType" class="inputField_100">
							<option value=""></option>
							<?php
								if(!empty($arrPrintTypeDatas)) {
									foreach($arrPrintTypeDatas as $thisKey => $thisValue) {
										$selected = '';
										if($thisKey == $_REQUEST["searchProductionPrintType"]){
											$selected = ' selected="selected" ';
										}
										echo '<option value="' . $thisKey . '" ' . $selected . ' > ' . $thisValue["printTypesName"] . '</option>';
									}
								}
							?>
						</select>
					</td>

					<td>
						<label for="searchProductCategory">Kategorie:</label>
						<select name="searchProductCategory" id="searchProductCategory" class="inputField_100">
							<option value=""></option>
							<option value="KZH" <?php if($_REQUEST["searchProductCategory"] == 'KZH'){ echo ' selected="selected" '; } ?> >Kennzeichenhalter</option>
							<option value="MIN" <?php if($_REQUEST["searchProductCategory"] == 'MIN'){ echo ' selected="selected" '; } ?> >Miniletter</option>
							<option value="AWT" <?php if($_REQUEST["searchProductCategory"] == 'AWT'){ echo ' selected="selected" '; } ?> >Ausweistaschen &amp; F&uuml;hrerscheintaschen</option>
							<option value="OTHERS" <?php if($_REQUEST["searchProductCategory"] == 'OTHERS'){ echo ' selected="selected" '; } ?> >andere</option>
						</select>
					</td>

					<td>
						<label for="searchProductDateSort">Datum-Sortierung:</label>
						<select name="searchProductDateSort" id="searchProductDateSort" class="inputField_100">
							<option value="DESC" <?php if($_REQUEST["searchProductDateSort"] == 'DESC'){ echo ' selected="selected" '; } ?> >absteigend</option>
							<option value="ASC" <?php if($_REQUEST["searchProductDateSort"] == 'ASC'){ echo ' selected="selected" '; } ?> >aufsteigend</option>
						</select>
					</td>

					<td>
						<label for="searchWord">Suchbegriff:</label>
						<input type="text" name="searchWord" id="searchWord" class="inputField_70" value="<?php echo $_REQUEST["searchWord"]; ?>" />
					</td>
					
					<td>
						<input type="hidden" name="editID" id="editID" value="" />
						<input type="submit" name="submitSearch" class="inputButton0" value="Suchen" />
					</td>
					<!--
					<td>
						<div style="width:120px;text-align:right; background-color:#990000;height:20px;" >
							<a href="<?php echo PAGE_EDIT_PROCESS; ?>?editID=NEW" class="linkButton inputButtonGreen" title="Neue Produktion anlegen" style="width: 70px;float:right;"><span>Neue Produktion</span></a>
						</div>
					</td>
					-->
				</tr>
			</table>
			<input type="hidden" name="sortDirection" id="inputSortDirection" value="<?php echo $_REQUEST["sortDirection"]; ?>" />
			<input type="hidden" name="sortRow" id="inputSortRow" value="<?php echo $_REQUEST["sortRow"]; ?>" />
			<input type="hidden" name="sortField" id="inputSortField" value="<?php echo $_REQUEST["sortField"]; ?>" />
			<input type="hidden" name="displayType" id="inputDisplayType" value="<?php echo $_REQUEST["displayType"]; ?>" />
			<input type="hidden" name="submitSearchForm" id="inputSubmitSearchForm" value="1" />
			<input type="hidden" name="displayOrderStatus" value="<?php echo $_REQUEST["displayOrderStatus"]; ?>" />

			<div class="colorsLegend">
				<!--
				<div class="legendItem">
					<b>F&auml;rbung:</b>
				</div>
				-->

				<div class="legendItem">
					<b>Zeilen:</b>
				</div>

				<!--
				<div class="legendItem">
					<span class="legendColorField" id="colorField4"></span>
					<span class="legendDescription">Aktuelle Zeile</span>
					<div class="clear"></div>
				</div>
				<div class="legendSeperator">&nbsp;</div>
				-->

				<?php
					if(!empty($arrOrderStatusTypeDatas)) {
						foreach($arrOrderStatusTypeDatas as $thisKey => $thisValue) {
							if(in_array($thisKey, array(1, 2, 3, 4, 6, 7, 8))){
								$checked = '';
								if(!empty($_REQUEST["arrSearchProductionStatus"])){
									if(in_array($thisKey, array_keys($_REQUEST["arrSearchProductionStatus"]))){
										$checked = ' checked="checked" ';
									}
								}
								$thisClass = '';
								$thisID = '';
								$thisColorfield = '';
								if($thisKey == '1'){
									$thisColorfield = 'colorField0';
								}
								else if($thisKey == '2'){
									$thisColorfield = 'colorField0';
								}
								else if($thisKey == '3'){
									$thisColorfield = 'colorField8';
								}
								else if($thisKey == '4'){
									$thisColorfield = 'colorField3';
								}
								else if($thisKey == '6'){
									$thisColorfield = 'colorField0';
									$thisClass = 'row6';
								}
								else if($thisKey == '7'){
									$thisColorfield = 'colorField1';
								}
								else if($thisKey == '8'){
									$thisColorfield = 'colorField9';
								}

								$checked = '';
								if($_REQUEST["arrSearchProductionStatus"][$thisKey] == '1'){
									$checked = ' checked="" ';
								}

								echo '<div class="legendItem">';
									echo '<input type="checkbox" name="arrSearchProductionStatus[' . $thisKey . ']" value="1" ' . $checked . ' />';
									echo '<span class="legendColorField ' . $thisColorfield . '"></span> ';
									echo '<span class="legendDescription ' . $thisClass . '"> ' . ($thisValue["orderStatusTypesName"]) . '</span>';
									echo '<div class="clear"></div>';
								echo '</div>';
								echo '<div class="legendSeperator">&nbsp;</div>';
							}
						}
					}
				?>

				<div class="legendSeperator">&nbsp;</div>

				<div class="legendItem">
					<b>KW-Spalte:</b>
				</div>

				<div class="legendItem">
					<span class="legendColorField" id="colorField6"></span>
					<?php
						$checked = '';
						if($_REQUEST["searchOverdueProductions"] == '1'){
							$checked = ' checked="checked" ';
						}
					?>
					<input type="checkbox" name="searchOverdueProductions" id="searchOverdueProductions" class="" value="1" <?php echo $checked ; ?> />
					<span class="legendDescription">Liefertermin &uuml;berf&auml;llig</span>
					<div class="clear"></div>
				</div>

				<div class="legendItem">
					<span class="legendColorField" id="colorField2"></span>
					<?php
						$checked = '';
						if($_REQUEST["searchUpcomingProductions"] == '1'){
							$checked = ' checked="checked" ';
						}
					?>
					<input type="checkbox" name="searchUpcomingProductions" id="searchUpcomingProductions" class="" value="1" <?php echo $checked ; ?> />
					<span class="legendDescription">Liefertermin bevorstehend</span>
					<div class="clear"></div>
				</div>

				<div class="legendItem">
					<span class="legendColorField" id="colorField5"></span>
					<?php
						$checked = '';
						if($_REQUEST["searchTimeframeProductions"] == '1'){
							$checked = ' checked="checked" ';
						}
					?>
					<input type="checkbox" name="searchTimeframeProductions" id="searchTimeframeProductions" class="" value="1" <?php echo $checked ; ?>/>
					<span class="legendDescription">Liefertermin im Zeitrahmen</span>
					<div class="clear"></div>
				</div>

				<div class="legendSeperator">&nbsp;</div>

				<div class="clear"></div>
			</div>

			</form>
		</div>

		<?php displayMessages(); ?>

		<?php

			// BOF GET COUNT ALL ROWS
				$sql_getAllRows = "
						SELECT

							COUNT(`" . TABLE_ORDERS . "`.`ordersID`) AS `countAllRows`

						FROM `" . TABLE_ORDERS . "`

						" . $sqlJoin . "
						" . $sqlGetPrintProductionFilesLeftJoin . "

						/*
						LEFT JOIN `" . VIEW_ORDERS_TO_CONFIRMATIONS . "`
						ON(`" . TABLE_ORDERS . "`.`ordersID` = `" . VIEW_ORDERS_TO_CONFIRMATIONS . "`.`orderID`)
						*/

						WHERE 1
							" . $sqlWhere . "
							" . $sqlGroup . "
					";

				$rs_getAllRows = $dbConnection->db_query($sql_getAllRows);
				list(
					$countAllRows
				) = mysqli_fetch_array($rs_getAllRows);
			// EOF GET COUNT ALL ROWS

			if($_REQUEST["page"] == "" || $_REQUEST["page"] == 0) {
				$_REQUEST["page"] = 1;
			}
			if(MAX_ORDERS_PER_PAGE > 0) {
				$sql .= " LIMIT " . (($_REQUEST["page"] - 1) * MAX_ORDERS_PER_PAGE) . ", " . MAX_ORDERS_PER_PAGE." ";
			} 
			//fix erro 
			$rs = mysqli_query($db_open, $sql);
			// OLD BOF GET ALL ROWS
				#$sql_totalRows = "SELECT FOUND_ROWS() AS `countRows`";
				#$rs_totalRows = $dbConnection->db_query($sql_totalRows);
				#list($countRows) = mysqli_fetch_array($rs_totalRows);
			// OLD EOF GET ALL ROWS

			$countRows = $countAllRows;

			$pagesCount = ceil($countRows / MAX_ORDERS_PER_PAGE);
			if($countRows > 0) {
		?>

		<table border="0" cellpadding="0" cellspacing="0" width="100%" class="menueToggle">
			<tr>
				<td>
					<span class="buttonShowAllColumns">Alle Spalten zeigen</span>
					<span class="buttonHideAllColumns">Alle Spalten ausblenden</span>
				</td>
				<td style="text-align:right; font-size:10px;">
					Anzahl Zeilen insgesamt: <?php echo $countRows; ?>
				</td>
			</tr>
		</table>

		<?php
			if($pagesCount > 1) {
				include(FILE_MENUE_PAGES);
			}
		?>

		<?php if(!1) { ?>
		<!--
		<div class="colorsLegend">
			<div class="legendItem">
				<b>F&auml;rbung:</b>
			</div>

			<div class="legendItem">
				<b>Zeilen:</b>
			</div>
			<div class="legendItem">
				<span class="legendColorField" id="colorField4"></span>
				<span class="legendDescription">Aktuelle Zeile</span>
				<div class="clear"></div>
			</div>
			<div class="legendSeperator">&nbsp;</div>

			<x!--
			<div class="legendItem">
				<span class="legendColorField" id="colorField7"></span>
				<span class="legendDescription">Freigegeben</span>
				<div class="clear"></div>
			</div>
			--x>

			<div class="legendItem">
				<span class="legendColorField" id="colorField8"></span>
				<span class="legendDescription">Belichtet</span>
				<div class="clear"></div>
			</div>
			<div class="legendItem">
				<span class="legendColorField" id="colorField1"></span>
				<span class="legendDescription">in Produktion</span>
				<div class="clear"></div>
			</div>
			<div class="legendItem">
				<span class="legendColorField" id="colorField3"></span>
				<span class="legendDescription">Versendet</span>
				<div class="clear"></div>
			</div>

			<div class="legendSeperator">&nbsp;</div>

			<div class="legendItem">
				<b>KW-Spalte:</b>
			</div>

			<div class="legendItem">
				<span class="legendColorField" id="colorField6"></span>
				<span class="legendDescription">Liefertermin &uuml;berf&auml;llig</span>
				<div class="clear"></div>
			</div>

			<div class="legendItem">
				<span class="legendColorField" id="colorField2"></span>
				<span class="legendDescription">Liefertermin bevorstehend</span>
				<div class="clear"></div>
			</div>

			<div class="legendItem">
				<span class="legendColorField" id="colorField5"></span>
				<span class="legendDescription">Liefertermin im Zeitrahmen</span>
				<div class="clear"></div>
			</div>

			<div class="legendSeperator">&nbsp;</div>

			<div class="clear"></div>
		</div>
		-->
		<?php } ?>

		<div class="contentDisplay">
			<table border="0" cellpadding="0" cellspacing="0" width="100%" class="displayOrders">
				<colgroup>
					<col />
					<col />
					<col />
					<col />
					<col />
					<col />
					<col />
					<col />
					<col />
					<col />
					<col />
					<col />
					<col />
					<col />
					<col />
					<col />
					<?php if($_COOKIE["isAdmin"] == "xxx1") { ?>
					<col />
					<?php } ?>
					<col />
					<col />
					<col />
					<col />
					<col />
					<!--<col />-->
					<!--<col />-->
					<col />
					<col />
					<!--<col />-->
					<col />
					<col />
					<col />
				</colgroup>
				<thead>
					<tr>
						<th style="width:45px;text-align:right;">#</th>
						<th class="sortColumn">Eingang</th>
						<th class="sortColumn">Freigabe</th>
						<th class="sortColumn">Belichtung</th>
						<th class="sortColumn">Druck</th>
						<th class="sortColumn">Liefer-KW</th>
						<th class="sortColumn">Auslieferung</th>
						<th class="sortColumn">Status DE</th>
						<th class="">Status TR</th>
						<th>Storno</th>
						<th class="sortColumn">K-NR</th>
						<th style="min-width:200px;">Kunde</th>
						<th class="sortColumn">PLZ</th>
						<th>Ort</th>
						<th>Art.-Nr</th>
						<th style="min-width:200px;">Art.-Bezeichnung</th>
						<?php if($_COOKIE["isAdmin"] == "xxx1") { ?>
						<th>Kategorie</th>
						<?php } ?>
						<th>Menge</th>
						<th>Anz. Farbe(n)</th>
						<th>Druckfarbe(n)</th>
						<th>Druckart</th>
						<th>Klischee-Nr.</th>
						<th class="sortColumn">Vertreter</th>
						<th>Quelle</th>
						<!--<th>IDM</th>-->
						<!--<th>Vorlagendatei</th>-->
						<th class="sortColumn">Bestell-Art</th>
						<th class="sortColumn">Zahlart</th>
						<!--<th class="sortColumn">Bank</th>-->
						<th class="sortColumn">Drucker</th>
						<th class="sortColumn">Mandant</th>
						<th style="width:50px;">Infos</th>
					</tr>
				</thead>

				<tbody>
			<?php
				$count = 0;
				while($ds = mysqli_fetch_assoc($rs)) {

					if($count%2 == 0){ $rowClass = 'row0'; }
					else { $rowClass = 'row1'; }

					if($ds["ordersStatus"] == "1") {
						// $rowClass = 'row5';
					}
					else if($ds["ordersStatus"] == "3") {
						#$rowClass = 'row2';
						$rowClass = 'row9';
					}
					else if($ds["ordersStatus"] == "4") {
						$rowClass = 'row3';
					}
					else if($ds["ordersStatus"] == "6") {
						$rowClass = 'row6';
					}
					else if($ds["ordersStatus"] == "7") {
						#$rowClass = 'row9';
						$rowClass = 'row2';
					}

					echo '<tr class="'.$rowClass.'">';

					echo '<td style="text-align:right;">';
					if($ds["ordersID"] == $_POST["dataRow"]){
						echo '<img src="layout/marker.gif" width="6" height="12" class="markerSavedItem" alt="" title="Gespeicherter Datensatz" />';
					}
					echo '<a name="' . $ds["ordersID"] . '"></a><span style="font-weight:bold;cursor:pointer;" title="Datensatz-ID: '.($ds["ordersID"]).'">'.($count + 1).'.</span>';
					echo '</td>';

					echo '<td>';
					echo formatDate($ds["ordersBestellDatum"], "display");
					echo '<br />';

					$thisIconImage = '';
					$thisIconClass = '';
					if($ds["ordersOrderType"] == 3){
						$thisIconImage = '<img src="layout/icons/iconAttention.png" width="14" height="14" alt="" /> ';
						$thisIconClass = 'class="iconAttention"';
					}
					else if($ds["ordersOrderType"] == 1){
						$thisIconImage = '<img src="layout/icons/iconNewOrder.png" width="14" height="14" alt="" /> ';
						$thisIconClass = '';
					}
					echo '<span style="font-size:11px;white-space:nowrap;" ' . $thisIconClass . ' >';
					echo $thisIconImage;

					echo $arrOrderTypeDatas[$ds["ordersOrderType"]]["orderTypesName"];
					echo '</span>';

					echo '</td>';

					echo '<td>' . formatDate($ds["ordersFreigabeDatum"], "display") . '</td>';
					echo '<td>';
					// BOF PRINT PRODUCTION FILES
						if(SHOW_PRINT_PRODUCTION_FILES) {
							if($ds["printProductionFilesFileNames"] != "") {
								echo '<img src="layout/icons/lightbulb.png" class="buttonPrintProductionFile" width="16" height="16" title="Belichtungsbogen" alt="Belichtungsbogen" />';

								echo '<div class="displayNoticeArea">';
								$arrThisPrintProductionFiles = explode(",", $ds["printProductionFilesFileNames"]);
								if(!empty($arrThisPrintProductionFiles)){
									echo '<ul>';
									foreach($arrThisPrintProductionFiles as $thisPrintProductionFile){
										echo '<li title="Belichtungsbogen vom ' . formatDate($ds["printProductionFilesFileDate"], "display") . '">';
										echo '<a href="?downloadFile=' . basename($thisPrintProductionFile) . '&amp;thisDocumentType=PP">';
										echo '<img src="layout/icons/iconPDF.gif" height="16" width="16" alt="PDF" /> ';
										echo $thisPrintProductionFile;
										echo '</a>';
										echo '</li>';
									}
									echo '</ul>';
								}
								echo '</div>';
							}
						}
					// EOF PRINT PRODUCTION FILES
					echo formatDate($ds["ordersBelichtungsDatum"], "display");
					echo '</td>';
					echo '<td style="white-space:nowrap;text-align:right;">';
					/*
					if($ds["printProductionOrdersID"] > 0 && !in_array($ds["ordersProductionsTransferProductionStatus"], array(6))){
						echo ' <span class="countryTR" title="Produktion in der T&uuml;rkei">TR</span>';
					}
					*/

					#if($ds["ordersProductionPrintingPlant"] != "DE" || $ds["ordersProductionsTransferOrdersID"] != ""){
					#if($ds["ordersProductionsTransferOrdersID"] != "" || $ds["ordersProductionPrintingPlant"] == "TR"){
					if(!in_array($ds["ordersProductionsTransferProductionStatus"], array(6)) && ($ds["ordersProductionsTransferOrdersID"] != "" || $ds["ordersProductionPrintingPlant"] == "TR")){
						echo '<div>';
						if($ds["ordersProductionsTransferOrdersID"] != ""){
							echo '<a href="' . PAGE_DISPLAY_EXTERNAL_PRODUCTIONS . '?searchCustomerNumber=' . $ds["ordersKundennummer"] . '"><span class="countryTR" title="Produktion in der T&uuml;rkei">TR</span></a> ';
							echo substr(formatDate($ds["ordersProductionsTransferMailSubmitDateTime"], "display"), 0, 10);
						} else if($ds["ordersProductionPrintingPlant"] == "TR"){
							echo '<a href="' . PAGE_DISPLAY_EXTERNAL_PRODUCTIONS . '?searchCustomerNumber=' . $ds["ordersKundennummer"] . '"><span class="countryTR" title="Produktion in der T&uuml;rkei">TR</span></a> ';
							echo substr(formatDate($ds["ordersProduktionsDatum"], "display"), 0, 10);
						}
						echo '</div>';
					}

					#if($arrGetUserRights["transferPrintProduction"] || (in_array($ds["ordersStatus"], array(2, 3, 7)) && $arrGetUserRights["transferPrintProduction"] && $ds["ordersProductionsTransferOrdersID"] == '') || $_COOKIE["isAdmin"] == '1'){
					if(!in_array($ds["ordersStatus"], array(4)) && ($arrGetUserRights["transferPrintProduction"] || (in_array($ds["ordersStatus"], array(2, 3, 7)) && $arrGetUserRights["transferPrintProduction"] && $ds["ordersProductionsTransferOrdersID"] == ''))){
						#if($ds["ordersStatus"] == 2 && $ds["printProductionOrdersID"] == '' && $arrGetUserRights["transferPrintProduction"]){
						# if($ds["ordersStatus"] == 2 && $arrGetUserRights["transferPrintProduction"]){
						// FELD ordersIsExported
						$newExportStatusID = 7;
						$thisRel = '';
						$thisRel .= $ds["ordersID"]; // 0
						$thisRel .= '|';
						$thisRel .= $ds["ordersKundennummer"]; // 1
						$thisRel .= '|';
						$thisRel .= $ds["ordersStatus"]; // 2
						$thisRel .= '|';
						$thisRel .= $newExportStatusID; // 3

						$thisRel .= '|';
						$thisRel .= $ds["ordersArtikelMenge"]; // 4

						$thisRel .= '|';
						$thisRel .= $ds["ordersMandant"]; // 5

						// BOF LKW-HINTEN - VORKASSE | NACHNAHME
							$thisRel .= '|';
							$isRedDot = 0;
							#if(preg_match("/(Vorkasse|Nachnahme)/", $ds["ordersInfo"])){
							if(preg_match("/(Nachnahme)/", $ds["ordersInfo"])){
								$isRedDot = 1;
							}
							$thisRel .= $isRedDot; // 6
						// EOF LKW-HINTEN - VORKASSE | NACHNAHME

						// BOF PRODUKT DATA
							$thisRel .= '|';
							$thisRel .= htmlentities(($ds["ordersArtikelBezeichnung"])); // 7
							if($ds["ordersKommission"] != '') {
								$thisRel .= ' ' . htmlentities(($ds["ordersKommission"]));
							}
							if($ds["ordersAufdruck"] != '') {
								$thisRel .= ' ' . htmlentities(($ds["ordersAufdruck"]));
							}
						// EOF PRODUKT DATA

						// BOF PRINT PLATE NUMBER
							$thisRel .= '|';
							$thisRel .= ' ' . htmlentities(($ds["ordersPrintPlateNumber"]));  // 8
						// EOF PRINT PLATE NUMBER

						echo '<img src="layout/icons/exportOrder.png" style="cursor:pointer;" class="buttonExportOrder" width="16" height="16" rel="' . $thisRel . '" alt="Export" title="Produktion in die T&uuml;rkei transferieren" />';
					}

					$thisOrdersProduktionsDatum = formatDate($ds["ordersProduktionsDatum"], "display");
					#if($thisOrdersProduktionsDatum != '' && $ds["ordersProductionsTransferOrdersProductQuantity"] != $ds["ordersArtikelMenge"] && $ds["ordersProductionPrintingPlant"] == "DE"){
					if(in_array($ds["ordersProductionsTransferProductionStatus"], array(6)) || ($thisOrdersProduktionsDatum != '' && $ds["ordersProductionsTransferOrdersProductQuantity"] != $ds["ordersArtikelMenge"] && $ds["ordersProductionPrintingPlant"] == "DE")){
					#if($thisOrdersProduktionsDatum != ''){
						echo '<div>';
						echo '<span class="countryDE" title="Produktion in Deutschland">DE</span> ';
						echo $thisOrdersProduktionsDatum;
						echo '</div>';
					}

					echo '</td>';

					$arrThisOrdersBestellDatum = formatDate($ds["ordersBestellDatum"], "split");
					$thisOrderKW = date('W', mktime(0,0,0, $arrThisOrdersBestellDatum["month"], $arrThisOrdersBestellDatum["day"], $arrThisOrdersBestellDatum["year"]));

					/*
					if($arrThisOrdersBestellDatum["year"] == date("Y") && $ds["ordersStatus"] != "4") {
						if($ds["ordersLieferwoche"] > (date("W") + 3)) {
							$thisStyle = 'background-color:#70FF6F;text-decoration:none;';
						}
						else if($ds["ordersLieferwoche"] > (date("W") + 2)) {
							$thisStyle = 'background-color:#FFFF00;text-decoration:none;';
						}
						else {
							$thisStyle = 'background-color:#FFBFBF;';
						}
					}
					else {
						$thisStyle = '';
					}
					*/

					$thisStyle = '';
					if($ds["ordersStatus"] < 4) {

						// BOF NEW
							$totalWeeksInYear = date('W', mktime(0, 0, 0, 12, 31, $arrThisOrdersBestellDatum["year"]) );

							if($ds["ordersFreigabeDatum"] > 0){
								$thisProductionStartDate = $ds["ordersFreigabeDatum"];
							}
							else if($ds["ordersBestellDatum"]){
								$thisProductionStartDate = $ds["ordersBestellDatum"];
							}
							$thisProductionStartYear = substr($thisProductionStartDate, 0, 4);
							$useAsDateForKW = date("Y-m-d", mktime(0, 0, 0, 1, 1, $thisProductionStartYear));

							$timeForMaxDeliveryDate = strtotime($useAsDateForKW);
							$secondsPerWeek = 1 * 7 * 24 * 60 * 60; // SECONDS PER WEEK;
							$timeForMaxDeliveryDate += $ds["ordersLieferwoche"] * $secondsPerWeek; // SECONDS PER WEEK
							$thisProductionMaxDeliveryDate = date("Y-m-d", $timeForMaxDeliveryDate);

							if($thisProductionMaxDeliveryDate < $thisProductionStartDate){
								$thisProductionMaxDeliveryDate = (substr($thisProductionMaxDeliveryDate, 0, 4) + 1) . substr($thisProductionMaxDeliveryDate, 4, strlen($thisProductionMaxDeliveryDate));
							}
							$thisProductionMaxDeliveryWeek = date("W", strtotime($thisProductionMaxDeliveryDate));
							$todayDate = date("Y-m-d");
							if($todayDate >= $thisProductionMaxDeliveryDate){
								$thisStyle = 'background-color:#FF558A;text-decoration:none;';
							}
							else {
								if((strtotime($thisProductionMaxDeliveryDate) - strtotime($todayDate)) <= 1 * $secondsPerWeek){
									$thisStyle = 'background-color:#FFFF00;text-decoration:none;';
								}
								else if((strtotime($thisProductionMaxDeliveryDate) - strtotime($todayDate)) <= 3 * $secondsPerWeek){
									$thisStyle = 'background-color:#70FF6F;text-decoration:none;';
								}
							}
						// EOF NEW

						// BOF OLD
							/*
							if(date("W") == $ds["ordersLieferwoche"] || (date("W") > $ds["ordersLieferwoche"] && $thisOrderKW - $ds["ordersLieferwoche"] < 0)) {
								$thisStyle = 'background-color:#FF558A;text-decoration:none;';
							}
							else if(date("W") + 1 >= $ds["ordersLieferwoche"] && ($thisOrderKW - $ds["ordersLieferwoche"] < 0)) {
								$thisStyle = 'background-color:#FFFF00;text-decoration:none;';
							}
							else if(date("W") + 3 >= $ds["ordersLieferwoche"] && ($thisOrderKW - $ds["ordersLieferwoche"] < 0)) {
								$thisStyle = 'background-color:#70FF6F;text-decoration:none;';
							}
							*/
						// EOF OLD

						/*
						if($arrThisOrdersBestellDatum["year"] == date("Y") && $ds["ordersStatus"] != "4") {
							if($ds["ordersLieferwoche"] > (date("W") + 3)) {
								$thisStyle = 'background-color:#70FF6F;text-decoration:none;';
							}
							else if($ds["ordersLieferwoche"] > (date("W") + 2)) {
								$thisStyle = 'background-color:#FFFF00;text-decoration:none;';
							}
							else {
								$thisStyle = 'background-color:#FFBFBF;';
							}
						}
						else
						#####
						if(date("W") > $ds["ordersLieferwoche"] && ($thisOrderKW - $ds["ordersLieferwoche"]) < 0 ) {
							if(date("Y") > $arrThisOrdersBestellDatum["year"]) {
								// $thisStyle = 'background-color:#000000;color:#FFFFFF;text-decoration:none;';
							}
							else if(date("Y") == $arrThisOrdersBestellDatum["year"]) {
								$thisStyle = 'background-color:#FFBFBF;';
							}
							else {
								$thisStyle = 'background-color:#70FF6F;text-decoration:none;'; ;
							}
						}
						else if(date("W") == $ds["ordersLieferwoche"]) {
							if(date("Y") > $arrThisOrdersBestellDatum["year"]) {
								// $thisStyle = 'background-color:#000000;color:#FFFFFF;text-decoration:none;';
							}
							else if(date("Y") == $arrThisOrdersBestellDatum["year"]) {
								$thisStyle = 'background-color:#FFBFBF;';
							}
							else {
								$thisStyle = 'background-color:#70FF6F;';
							}
						}
						else if(date("W") < ($ds["ordersLieferwoche"] + 2) && ($thisOrderKW - $ds["ordersLieferwoche"]) < 0 ) {
							if(date("Y") > $arrThisOrdersBestellDatum["year"]) {
								$thisStyle = 'background-color:#70FF6F;text-decoration:none;'; ;
							}
							else if(date("Y") == $arrThisOrdersBestellDatum["year"]) {
								$thisStyle = 'background-color:#70FF6F;text-decoration:none;'; ;
							}
							else {
								// $thisStyle = 'background-color:#70FF6F;text-decoration:none;'; ;
							}
						}
						*/
					}
					echo '<td style="font-weight:bold;'.$thisStyle.'">';

					if($ds["ordersLieferwoche"] > 0 && $ds["ordersStatus"] != 1) {
						$thisLieferKW = '~ ';
						if($ds["ordersLieferwoche"] < 10) { $thisLieferKW .= '0';}
						$thisLieferKW .= ($ds["ordersLieferwoche"]) . '. KW';
						echo $thisLieferKW;
					}

					echo'</td>';
					echo '<td>'.formatDate($ds["ordersAuslieferungsDatum"], "display").'</td>';


					// BOF CHANGE STATUS
						echo '<td style="cursor:pointer;white-space: nowrap;" title="'.($arrOrderStatusTypeDatas[$ds["ordersStatus"]]["orderStatusTypesName"]).'">';
							// echo($arrOrderStatusTypeDatas[$ds["ordersStatus"]]["orderStatusTypesShortName"]);

							if($arrGetUserRights["editOrders"]) {
								echo '<form name="formStoreStatus" method="post" action="'.$_SERVER["PHP_SELF"].'#' . $ds["ordersID"] . '">';
								echo '<select class="selectStatus" name="changeStatus['.$ds["ordersID"].']">';
								if(!empty($arrOrderStatusTypeDatas)) {
									foreach($arrOrderStatusTypeDatas as $thisKey => $thisValue) {
										$selected = "";
										if(($ds["ordersStatus"] == "" || $ds["ordersStatus"] == "0") && $thisKey == 1) {
											$selected = ' selected="selected" ';
										}
										else if($ds["ordersStatus"] != "" && $thisKey == $ds["ordersStatus"]) {
											$selected = ' selected="selected" ';
										}
										echo '<option title="'.$arrOrderStatusTypeDatas[$thisKey]["orderStatusTypesName"].'" value="'.$thisKey.'" '.$selected.' >'.$arrOrderStatusTypeDatas[$thisKey]["orderStatusTypesShortName"].'</option>';
									}
								}

								echo '</select>';
								echo '<input type="hidden" name="storedStatus" value="' . $ds["ordersStatus"] . '" />';
								echo '<input type="hidden" name="searchCustomerName" value="' . $_REQUEST["searchCustomerName"] . '" />';
								#echo '<input type="hidden" name="searchCustomerNumber" value="' . $_REQUEST["searchCustomerNumber"] . '" />';
								echo '<input type="hidden" name="searchCustomerNumber" value="' . $ds["ordersKundennummer"] . '" />';
								echo '<input type="hidden" name="searchPLZ" value="' . $_REQUEST["searchPLZ"] . '" />';
								echo '<input type="hidden" name="searchSalesmanName" />';
								echo '<input type="hidden" name="searchWord" value="' . $_REQUEST["searchWord"] . '" />';

								echo '<input type="hidden" name="displayOrderStatus" value="' . $_REQUEST["displayOrderStatus"] . '" />';
								echo '<input type="hidden" name="page" value="' . $_REQUEST["page"] . '" />';
								echo '<input type="hidden" name="dataRow" value="' . $ds["ordersID"] . '" />';
								echo '<input type="hidden" name="storedAdditionalCostsID" value="' . $ds["ordersAdditionalCostsID"] . '" />';
								echo '<input type="image" class="buttonSaveStatus" src="layout/icons/iconSave.png" style="width:16px;height:16px;" title="Status speichern" alt="Status speichern" />';
								echo '</form>';
							}
							else {
								echo $arrOrderStatusTypeDatas[$ds["ordersStatus"]]["orderStatusTypesShortName"];
							}
						echo '</td>';
					// EOF CHANGE STATUS

					echo '<td style="font-size:11px;white-space:nowrap;">';

					$contentStatusTR = '';

					if($ds["ordersProductionsTransferDeliveryRedDot"] == '1' || $ds["ordersProductionsTransferUseNoNeutralPacking"] == '1'){
						$contentStatusTR .= '<table border="0" cellpadding="0" cellspacing="0" class="noBorder">';
						if($ds["ordersProductionsTransferDeliveryRedDot"] == '1'){
							$contentStatusTR .= '<tr><td style="font-size:10px;">LKW-POS:</td><td><img width="10" height="10" title="Hinten LKW (Vorkasse / Nachnahme)" alt="Hinten LKW (Vorkasse / Nachnahme)" src="layout/icons/redDot.png" /></td></tr>';
						}
						if($ds["ordersProductionsTransferUseNoNeutralPacking"] == '1'){
							$contentStatusTR .= '<tr><td style="font-size:10px;">Verpackung:</td><td><img width="10" height="10" title="Verpackung mit Werbung (BURHAN)" alt="Verpackung mit Werbung (BURHAN)" src="layout/icons/greenDot.png" /></td></tr>';
						}
						$contentStatusTR .= '</table>';
					}
					echo '<b>' . $arrOrderStatusTypeDatasExtern[$ds["ordersProductionsTransferProductionStatus"]]["orderStatusTypesName"] . '</b>';
					echo $contentStatusTR;
					echo '</td>';

					echo '<td>'.formatDate($ds["ordersStornoDatum"], "display").'</td>';

					$thisCellStyle = '';
					if($_REQUEST["searchBoxProduction"] == $ds["ordersKundennummer"]){
						$thisCellStyle = 'background-color:#0F0;font-weight:bold;';
					}
					echo '<td style="' . $thisCellStyle . '">';
					if($arrGetUserRights["editCustomers"]) {
						echo '<a';
					}
					else {
						echo '<span';
					}
					echo ' href="' . PAGE_EDIT_CUSTOMER . '?editCustomerNumber='.$ds["ordersKundennummer"].'" title="Kunde &quot;'.htmlentities(($ds["ordersKundenName"])). '&quot; (KNR: '.$ds["ordersKundennummer"].') anzeigen">'.($ds["ordersKundennummer"]).'</';
					if($arrGetUserRights["editCustomers"]) {
						echo 'a>';
					}
					else {
						echo 'span>';
					}
					echo '</td>';
					echo '<td>';
					echo htmlentities((preg_replace("/\//", " / ", $ds["ordersKundenName"])));
					if($ds["ordersKommission"] != '') {
						echo '<div class="remarksArea">';
						echo '<b>Kommission:</b> ' . htmlentities(($ds["ordersKommission"]));
						echo '</div>';
					}
					if($ds["ordersAufdruck"] != '') {
						echo '<div class="remarksArea">';
						echo '<b>Aufdruck:</b> ' . htmlentities(($ds["ordersAufdruck"]));
						echo '</div>';
					}
					if($ds["ordersPerExpress"] == '1') {
						echo '<div class="remarksArea">';
						echo '<b style="color:#FF0000;">als Express-Bestellung</b>';
						echo '</div>';
					}
					if($ds["ordersInfo"] != '') {
						echo '<div class="remarksArea" style="color:#FF0000;">';
						echo '<b>Info:</b> ' . htmlentities(($ds["ordersInfo"]));
						echo '</div>';
					}

					if($ds["ordersNotizen"] != "") {
						echo '<div class="remarksArea">';
						echo '<b>Bemerkung:</b> ' .htmlentities(($ds["ordersNotizen"]));
						echo '</div>';
					}

					if($ds["ordersDirectSale"] == '1') {
						echo '<div class="remarksArea">';
						echo '<b style="color:#FF0000;">Direktverkauf durch den Vertreter</b>';
						echo '</div>';
					}

					// BOF COMPLAINT
					if($ds["ordersReklamationsgrund"] != '') {
						echo '<div class="remarksArea">';
						echo '<b style="color:#FF0000;">Reklamiert:</b> ' . htmlentities(($ds["ordersReklamationsgrund"]));
						echo '</div>';
					}
					// EOF COMPLAINT

					echo '</td>';

					echo '<td>'.($ds["ordersPLZ"]).'</td>';

					echo '<td style="white-space:nowrap;">'.($ds["ordersOrt"]).'</td>';

					echo '<td>';
					echo '<a href="' . PAGE_DISPLAY_PRODUCTS . '?editOrdersArtikelID='.$ds["ordersArtikelID"].'">';
					echo ($ds["ordersArtikelNummer"]);
					echo '</a>';
					echo '</td>';

					echo '<td>';
					echo '<a href="' . PAGE_DISPLAY_PRODUCTS . '?editOrdersArtikelID='.$ds["ordersArtikelID"].'">';
					$thisOrdersArtikelBezeichnung = $ds["ordersArtikelBezeichnung"];
					$thisOrdersArtikelBezeichnung = (preg_replace("/\//", " / ", $thisOrdersArtikelBezeichnung));
					$thisOrdersArtikelBezeichnung = (preg_replace('/(Exclusiv mit erhabener Chrom-Schrift)/', '<b style="color:#FF0000;">$1</b>', $thisOrdersArtikelBezeichnung));
					echo '<span class="productName">' . ($thisOrdersArtikelBezeichnung) . '</span>';
					echo '</a>';
					if($ds["ordersAdditionalArtikelMenge"] > 0) {
						echo '<br /> (+ '. $ds["ordersAdditionalArtikelMenge"] . ' ' . htmlentities($arrOrderCategoriesTypeDatas[$ds["ordersAdditionalArtikelKategorieID"]]["orderCategoriesName"]).')';
					}

					// BOF EXTERNAL PRODUCT ORDER
						if($ds["supplierOrdersID"] > 0){
							echo '';
							echo '<span class="remarksArea" style="color:#FF0000;">';
							echo '<br />';
							echo '<b>Ware (' .  $ds["supplierOrdersAmount"] . ' St&uuml;ck) am ' .  formatDate($ds["supplierOrdersOrderDate"], 'display') . ' bestellt bei ' .  $ds["suppliersFirmenname"] . '</b>';
							echo '</span>';
							if($ds["supplierOrdersDeliveryDate"] > 0){
								echo '<span class="remarksArea" style="color:#000099;">';
								echo '<br />';
								echo '<b>Ware geliefert am ' .   formatDate($ds["supplierOrdersDeliveryDate"], 'display') . '.</b>';
								echo '</span>';
							}
						}
					// EOF EXTERNAL PRODUCT ORDER

					echo '</td>';

					if($_COOKIE["isAdmin"] == "xxxx1") {
						#dd('arrOrderCategoriesTypeDatas');
						#echo '<td style="cursor:pointer;"><span title="'.htmlentities($arrOrderCategoriesTypeDatas[$ds["ordersArtikelKategorieID"]]["orderCategoriesName"]).'">'.($arrOrderCategoriesTypeDatas[$ds["ordersArtikelKategorieID"]]["orderCategoriesShortName"]).'</span></td>';
						echo '<td style="cursor:pointer;"><span title="'.htmlentities($arrOrderCategoriesTypeDatas[substr($ds["ordersArtikelKategorieID"], 0, 3)]["products"][$ds["ordersArtikelKategorieID"]]["productsProductNumber"]).'">'.($arrOrderCategoriesTypeDatas[substr($ds["ordersArtikelKategorieID"], 0, 3)]["products"][$ds["ordersArtikelKategorieID"]]["productsProductNumber"]).'</span></td>';
					}

					echo '<td style="white-space:nowrap;text-align:right;">';
					if($ds["printProductionOrdersProductDatas"] != ""){
						$arrPrintProductionOrdersProductDatas = unserialize($ds["printProductionOrdersProductDatas"]);
						$ds["ordersProductionsTransferOrdersProductQuantity"] = $arrPrintProductionOrdersProductDatas["ordersArtikelMenge"];
					}
					echo '<b>' . ($ds["ordersArtikelMenge"]) . '</b>';
					if(!in_array($ds["ordersStatus"], array(1, 2, 6))){
						// BOF OLD 05.01.2017
							/*
							if($ds["ordersProductionsTransferOrdersProductQuantity"] > 0){
								echo '<br /><a href="' . PAGE_DISPLAY_EXTERNAL_PRODUCTIONS . '?searchCustomerNumber=' . $ds["ordersKundennummer"] . '"><span class="countryTR">TR:</span></a> '.($ds["ordersProductionsTransferOrdersProductQuantity"]).'';
							}
							if(($ds["ordersArtikelMenge"] - $ds["ordersProductionsTransferOrdersProductQuantity"]) > 0){
								echo '<br /><span class="countryDE">DE:</span> '.($ds["ordersArtikelMenge"] - $ds["ordersProductionsTransferOrdersProductQuantity"]).'';
							}
							*/
						// EOF OLD 05.01.2017

						// BOF NEW
							echo '<span style="display:block;margin:2px 0 0 0;padding:2px 0 0 0;border-top:1px dotted #666;">';

							if($ds["ordersProductionsTransferOrdersProductQuantity"] > 0){
								#echo '<br />';
								echo '<a href="' . PAGE_DISPLAY_EXTERNAL_PRODUCTIONS . '?searchCustomerNumber=' . $ds["ordersKundennummer"] . '"><span class="countryTR">TR:</span></a> '.($ds["ordersProductionsTransferOrdersProductQuantity"]).'';

								if(($ds["ordersArtikelMenge"] - $ds["ordersProductionsTransferOrdersProductQuantity"]) > 0){
									echo '<br />';
									echo '<span class="countryDE">DE:</span> '.($ds["ordersArtikelMenge"] - $ds["ordersProductionsTransferOrdersProductQuantity"]).'';
								}
							}
							else if($ds["ordersProductionsTransferOrdersProductQuantity"] == 0){
								#echo '<br />';
								echo '<span class="country' . $ds["ordersProductionPrintingPlant"] . '">' . $ds["ordersProductionPrintingPlant"] . ':</span> '.($ds["ordersArtikelMenge"] - $ds["ordersProductionsTransferOrdersProductQuantity"]).'';
							}
							echo '</span>';

						// BOF NEW
					}
					echo '</td>';

					echo '<td>';
					echo $ds["ordersArtikelPrintColorsCount"];
					echo '</td>';

					echo '<td style="white-space:nowrap;">';
					#echo (preg_replace("/\//", " / ", $ds["ordersDruckFarbe"]));
					echo (preg_replace("/\//", "<br />", $ds["ordersDruckFarbe"]));
					if($ds["ordersArtikelWithBorder"] == '1') {
						if($ds["ordersDruckFarbe"] != '') {
							echo '<br />';
						}
						echo '<b style="color:#FF0000;">mit Umrandung</b>';
					}
					if($ds["ordersArtikelWithClearPaint"] == '1') {
						if($ds["ordersDruckFarbe"] != '') {
							echo '<br />';
						}
						echo '<b style="color:#FF0000;">mit Klarlack</b>';
					}
					echo '</td>';
					
					echo '<td>';					
					echo '' . htmlentities($arrPrintTypeDatas[$ds["ordersAdditionalCostsID"]]["printTypesName"]) . '';
					echo '</td>';

					echo '<td>';
					echo '<b>' . $ds["ordersPrintPlateNumber"] . '</b>';
					echo '</td>';

					echo '<td>';
					echo (preg_replace("/\//", " / ", $ds["ordersVertreter"]));
					echo '</td>';

					echo '<td style="white-space:nowrap;">';
					echo ($arrOrderSourceTypes[$ds["ordersSourceType"]]["ordersSourceTypesName"]);
					echo ' - ';
					echo ($ds["ordersSourceName"]);
					echo '</td>';

					echo '<td style="cursor:pointer;">';
					echo '<span title="'.($arrOrderTypeDatas[$ds["ordersOrderType"]]["orderTypesName"]).'" ' . $thisIconClass . ' >';
					echo $thisIconImage;
					echo ($arrOrderTypeDatas[$ds["ordersOrderType"]]["orderTypesShortName"]);
					echo '</span>';
					echo '</td>';

					echo '<td style="cursor:pointer;"><span title="'.($arrPaymentTypeDatas[$ds["ordersPaymentType"]]["paymentTypesName"]).'">';
					echo ($arrPaymentTypeDatas[$ds["ordersPaymentType"]]["paymentTypesShortName"]);
					echo '</span></td>';
					// echo '<td style="cursor:pointer;" title="'.($arrBankAccountTypeDatas[$ds["ordersBankAccountType"]]["bankAccountTypesName"]).'">'.($arrBankAccountTypeDatas[$ds["ordersBankAccountType"]]["bankAccountTypesShortName"]).'</td>';
					echo '<td>'.($arrPrinterDatas[$ds["ordersDruckerName"]]["printersLastName"]).'</td>';
					echo '<td>'.($ds["ordersMandant"]).'</td>';
					echo '<td style="white-space:nowrap;">';

					// BOF NOTICE
					if($ds["ordersNotizen"] != "") {
						echo '<span class="toolItem">';
						echo '<img src="layout/icons/iconRTF.gif" class="buttonNotice" width="16" height="16" alt="Bemerkung / Notiz" />';

						echo '<div class="displayNoticeArea">';
						echo wordwrap($ds["ordersNotizen"], 75, "<br />", true);
						echo '</div>';

						echo '</span>';
					}
					else {
						echo '<span class="toolItem"><img src="layout/spacer.gif" width="16" height="16" alt="" /></span>';
					}
					// EOF NOTICE

					// BOF EDIT
					if($arrGetUserRights["editOrders"] || $arrGetUserRights["displayOrders"]) {
						$arrReturnParams = array();
						$jsonReturnParams = '';
						if($_REQUEST["page"] != '') { $arrReturnParams["page"] = $_REQUEST["page"]; }
						if($_REQUEST["displayOrderStatus"] != '') { $arrReturnParams["displayOrderStatus"] = $_REQUEST["displayOrderStatus"]; }
						if($_REQUEST["displayType"] != '') { $arrReturnParams["displayType"] = $_REQUEST["displayType"]; }
						if($_REQUEST["searchCustomerName"] != '') { $arrReturnParams["searchCustomerName"] = $_REQUEST["searchCustomerName"]; }
						if($_REQUEST["searchCustomerNumber"] != '') { $arrReturnParams["searchCustomerNumber"] = $_REQUEST["searchCustomerNumber"]; }
						if($_REQUEST["searchPLZ"] != '') { $arrReturnParams["searchPLZ"] = $_REQUEST["searchPLZ"]; }
						if($_REQUEST["searchSalesmanName"] != '') { $arrReturnParams["searchSalesmanName"] = $_REQUEST["searchSalesmanName"]; }
						if($_REQUEST["searchWord"] != '') { $arrReturnParams["searchWord"] = $_REQUEST["searchWord"]; }
						if($_REQUEST["sortDirection"] != '') { $arrReturnParams["sortDirection"] = $_REQUEST["sortDirection"]; }
						if($_REQUEST["sortField"] != '') { $arrReturnParams["sortField"] = $_REQUEST["sortField"]; }
						if($_REQUEST["sortRow"] != '') { $arrReturnParams["sortRow"] = $_REQUEST["sortRow"]; }
						if(!empty($arrReturnParams)) {
							$jsonReturnParams = '&amp;';
							$jsonReturnParams .= 'jsonReturnParams=' . rawurlencode(json_encode($arrReturnParams));
						}
						if($arrGetUserRights["editOrders"]) {
							echo '<span class="toolItem"><a href="' . PAGE_EDIT_PROCESS . '?editID='.$ds["ordersID"]. $jsonReturnParams . '"><img src="layout/icons/iconEdit.gif" width="16" height="16" title="Produktion bearbeiten (Datensatz '.$ds["ordersID"].')" alt="Bearbeiten" /></a></span>';
						}
						else if($arrGetUserRights["displayOrders"]) {
							echo '<span class="toolItem"><a href="' . PAGE_EDIT_PROCESS . '?editID='.$ds["ordersID"]. $jsonReturnParams . '"><img src="layout/icons/iconView.png" width="16" height="16" title="Produktion ansehen (Datensatz '.$ds["ordersID"].')" alt="Bearbeiten" /></a></span>';
						}
					}
					else {
						echo '<span class="toolItem"><img src="layout/spacer.gif" width="16" height="16" alt="" /></span>';
					}
					// EOF EDIT

					// BOF MAIL
					if($ds["ordersMailAdress"] != "") {
						echo '<span class="toolItem"><a href="mailto:'.$ds["ordersMailAdress"].'"><img src="layout/icons/iconMail.gif" width="16" height="14" title="Kunde &quot;'.htmlentities($ds["ordersKundenName"]). '&quot; (KNR: '.$ds["ordersKundennummer"].') per Mail kontaktieren" alt="Kontakt per Mail" /></a></span>';
					}
					else {
						echo '<span class="toolItem"><img src="layout/spacer.gif" width="16" height="16" alt="" /></span>';
					}
					// EOF MAIL

					// BOF OTHER CONTACTS
					if($ds["ordersContactOthers"] != "") {
						echo '<span class="toolItem"><img src="layout/icons/iconPhone.png" width="16" height="14" title="Kunde &quot;'.htmlentities($ds["ordersKundenName"]). '&quot; (KNR: '.$ds["ordersKundennummer"].') Kontakt per '.$ds["ordersContactOthers"].'" alt="Kontakt" /></span>';
					}
					else {
						echo '<span class="toolItem"><img src="layout/spacer.gif" width="16" height="16" alt="" /></span>';
					}
					// EOF OTHER CONTACTS
					echo '</td>';
					echo '</tr>';
					$count++;
				}
			?>
				</tbody>
			</table>
		</div>
		<?php
			if($pagesCount > 1) {
				include(FILE_MENUE_PAGES);
			}
		?>
		<div style="text-align:right;" class="menueToTop">
			<a href="#top">nach oben</a>
		</div>
		<?php
			}
			else {
				echo '<p class="warningArea">Es wurden keine Daten gefunden!</p>';
			}
		?>
		<div class="clear"></div>
	</div>
	<div class="clear"></div>
</div>
<script language="javascript" type="text/javascript">
	<!--
	/* <![CDATA[ */
	$(document).ready(function() {
		function sortResult() {
			$('.sortColumn').css('font-weight', 'bold');

			var allth=jQuery('table.displayOrders tr th');
			jQuery(allth).each( function(index,value){
				var container = '<span class="tableheader">'+jQuery(this).html()+'<\/span>';
				jQuery(this).html(container);
			});

			// var objSortFields = $.ajax({ 'PLZ':{displayHeader:'PLZ', tableField:''}, 'BESTELLUNG':{displayHeader:'Bestellung', tableField:''}, 'FREIGABE':{displayHeader:'Freigabe', tableField:''}, 'KW':{displayHeader:'Lief-KW', tableField:''}, 'STATUS':{displayHeader:'Status', tableField:''}, 'KNR':{displayHeader:'K-NR', tableField:''} });
			// var objSortFields = '{"displayHeader":"PLZ", "tableField":""}';var objSortFields = '{"displayHeader":"PLZ", "tableField":""}';

			var objSortFields = '';
			objSortFields += '{';
			objSortFields += '"PLZ":"ordersPLZ"';
			objSortFields += ', "Eingang":"ordersBestellDatum"';
			objSortFields += ', "Belichtung":"ordersBelichtungsDatum"';
			objSortFields += ', "Druck":"ordersProduktionsDatum"';
			objSortFields += ', "Freigabe":"ordersFreigabeDatum"';
			objSortFields += ', "Liefer-KW":"ordersLieferwoche"';
			objSortFields += ', "Auslieferung":"ordersAuslieferungsDatum"';
			objSortFields += ', "Vertreter":"ordersVertreter"';
			objSortFields += ', "Status":"ordersStatus"';
			objSortFields += ', "K-NR":"ordersKundennummer"';
			objSortFields += ', "Bestell-Art":"ordersOrderType"';
			objSortFields += ', "Zahlart":"ordersPaymentType"';
			objSortFields += ', "Bank":"ordersBankAccountType"';
			objSortFields += ', "Mandant":"ordersMandant"';

			objSortFields += '}';

			jsonSortFields = JSON.parse(objSortFields);

			var	iconPath = 'layout/icons/';
			var iconSortUp = 'iconSortUp.gif';
			var iconSortDown = 'iconSortDown.gif';
			var iconSortEmpty = 'iconSortEmpty.gif';
			$('.sortColumn').each(function(index) {
				var thisText = $(this).text();
				var thisAlt = 'Sortieren nach ' + thisText;
				var	newHtml = '<span class="sortFieldButton">' + thisText + '<\/span>';
				var thisImage = iconSortEmpty;
				$(this).css('color', '');
				if('<?php echo $_REQUEST["sortRow"]; ?>' == thisText) {
					if('<?php echo $_REQUEST["sortDirection"]; ?>' == 'DESC') {
						thisImage = iconSortUp;
					}
					else if('<?php echo $_REQUEST["sortDirection"]; ?>' == 'ASC') {
						thisImage = iconSortDown;
					}
					$(this).css('color', '#FFFFFF');
				}
				newHtml += '<img src="' + iconPath + thisImage + '" width="9" height="14" class="sortDirectionButton" title="' + thisAlt + '" \/>';
				$(this).html(newHtml);
				$(this).css('text-decoration', 'none');
			});
			$('.sortDirectionButton').css('padding', '0 4px 0 4px');
			$('.sortDirectionButton').css('cursor', 'pointer');

			$('.sortDirectionButton').click(function() {
				$('.sortDirectionButton').each(function(index) {
					$(this).attr('src', iconPath + iconSortEmpty);
				});

				var sortDirection = $('#inputSortDirection').val();
				var sortField = $('#inputSortField').val();
				var sortRow = $('#inputSortRow').val();

				if(sortDirection == '') {
					sortDirection = 'DESC';
					$(this).attr('src', iconPath + iconSortUp);
				}
				else if(sortDirection == 'DESC') {
					sortDirection = 'ASC';
					$(this).attr('src', iconPath + iconSortDown);
				}
				else if(sortDirection == 'ASC') {
					sortDirection = 'DESC';
					$(this).attr('src', iconPath + iconSortUp);
				}
				$('#inputSortDirection').attr('value', sortDirection);
				$('#inputSortField').attr('value', jsonSortFields[$(this).parent().text()]);
				$('#inputSortRow').attr('value', $(this).parent().text());
				$('form[name=formFilterSearch]').submit();
			});
		}

		sortResult();
		setFocus('formFilterSearch', 'searchWord');
		colorRowMouseOver('.displayOrders tbody tr');
		// $('.displayOrders thead').css('position', 'fixed');
		toggleColums('table.displayOrders', 'tableDisplayOrders');
		// setTimeout("toggleColums('table.displayOrders', 'tableDisplayOrders')", 10);
		getToggleCookie('table.displayOrders', 'tableDisplayOrders');
		// setTimeout("getToggleCookie('table.displayOrders', 'tableDisplayOrders')", 10);

		$('#searchCustomerNumber').keyup(function () {
			// loadSuggestions('searchCustomerNumber', [{'triggerElement': '#searchCustomerNumber', 'fieldNumber': '#searchCustomerNumber', 'fieldName': '#searchCustomerName', 'fieldCity': '', 'fieldZipCode': ''}], 1);
		});
		$('#searchCustomerName').keyup(function () {
			// loadSuggestions('searchCustomerName', [{'triggerElement': '#searchCustomerName', 'fieldNumber': '#searchCustomerNumber', 'fieldName': '#searchCustomerName', 'fieldCity': '', 'fieldZipCode': ''}], 1);
		});
		$('#searchSalesmanName').keyup(function () {
			// loadSuggestions('searchSalesmanName', [{'triggerElement': '#searchSalesmanName', 'fieldName': '#searchSalesmanName'}], 1);
		});
		$('#searchPLZ').keyup(function () {
			// loadSuggestions('searchPLZ', [{'triggerElement': '#searchPLZ', 'fieldZipCode': '#searchPLZ'}], 1);
		});

		$('.buttonNotice').click(function () {
			loadNotice($(this));
		});
		$('.buttonPrintProductionFile').click(function () {
			loadNotice($(this));
		});

		var thisProduktName = '';
		$('.buttonExportOrder').click(function () {
			thisProduktName = $(this).parent().parent().parent().find('span.productName').text();
			var thisMailAddress = 'production@burhan-ctr.de';

			<?php if($_COOKIE["isAdmin"] == '1'){ ?>
			// thisMailAddress = 'production@burhan-ctr.de;webmaster@burhan-ctr.de';
			<?php } ?>

			$('tr[id="activeRow"]').removeAttr('id');
			$(this).parent().parent().parent().attr('id', 'activeRow');
			$('.iconClose').live('click', function(){
				$('tr[id="activeRow"]').removeAttr('id');
			});

			loadFormSubmitPrintProduction($(this), '<?php echo $_SERVER["PHP_SELF"]; ?>', thisMailAddress);
		});

		$('.buttonToggleSidebarMenue').click(function() {
			$('#menueSidebarToggleContent').toggle();
		});

		<?php if($arrGetUserRights["transferPrintProduction"] == '1'){ ?>
			function checkTransferFile(element, thisProduktName){
				var thisTransferFileName = element.val();
				var thisTansferFileType =  thisTransferFileName.substr(thisTransferFileName.lastIndexOf('.')+1)
				var thisTransferCustomerNumber = $('input[name="editCustomerNumber"]').val();
				var customerNumberInFileName = thisTransferFileName.indexOf(thisTransferCustomerNumber);

				if(customerNumberInFileName == '-1'){
					alert('Prüfen: Gehört diese Datei zu dem ausgewählten Kunden??');
				}
				if(thisTansferFileType != 'ai'){
					alert('Prüfen: Diese Datei ist keine AI-Datei!');
				}

				if(thisProduktName != ''){
					if(thisProduktName.indexOf('Miniletter') >= 0){
						if(thisTransferFileName.indexOf('Miniletter') < 0){
							alert('Prüfen: Diese Datei ist keine Miniletter-Datei! \n Produkt ist aber Miniletter!');
						}
					}
					else {
						if(thisTransferFileName.indexOf('Miniletter') >= 0){
							alert('Prüfen: Diese Datei ist eine Miniletter-Datei! \n Produkt ist aber kein Miniletter!');
						}
						else {
							$.get('inc/checkTransferFile.inc.php?thisProduktName=' + thisProduktName + '&thisTransferFileName=' + thisTransferFileName, function(result) {
								if(result == 0){
									alert('Prüfen: Ist die richtige AI-Datei ausgewählt?');
								}
							});
						}
					}
				}
			}
			$('#sendAttachedMailFile').live('change', function(){
				checkTransferFile($(this), thisProduktName);
			});
		<?php } ?>

		blink('.iconAttention', 1000, 0.1, 400);
	});

	/* ]]> */
	// -->
</script>

<?php require_once('inc/footerHTML.inc.php'); ?>