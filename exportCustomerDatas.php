<?php
	require_once('inc/requires.inc.php');
	ini_set('memory_limit', '512M');


	if(!$arrGetUserRights["exportCustomers"]) {
		header('location: ' . PAGE_EXIT_LOCATION);
		exit;
	}

	$jswindowMessage = "";
	$warningMessage = '';
	$errorMessage = '';
	$successMessage = '';
	$infoMessage = '';

	$dbConnection = new DB_Connection();
	$db_open = $dbConnection->db_connect();

	$userDatas = getUserDatas();
	$arrExportFileTypes = array(
		"txt",
		"csv",
		"xls",
		// "xlsx"
	);

	$includeLastOrder = false; // true, false
	$defaultStoreDir = '__Ohne-Vertreter/';

	// BOF READ SALESMEN WITH ZIP CODES
		$arrSalesmenWithActiveAreasData = getSalesmenWithActiveAreas();
	// EOF READ SALESMEN WITH ZIP CODES

	// BOF GET SALESMEN
		#$arrSalesmenZipcodeDatas = getRelationSalesmenZipcode();
		$arrSalesmenZipcodeDatas = getRelationSalesmenZipcode2();
	// EOF GET SALESMEN

	// BOF READ MANDATORIES
		$arrMandatoryDatas = getMandatories();
		if(!empty($arrMandatoryDatas)){
			foreach($arrMandatoryDatas as $thisKey => $thisValue){
				$arrSearchMandantory[] = $thisValue["mandatoriesShortName"];
			}
		}
	// EOF READ MANDATORIES

	$excelFileExcelVersion = 'Excel5';
	#$excelFileExcelVersion = 'Excel2007';

	$zipFileName = "kundenExport_PLZ";
	$downloadFolder = "kundenExport_PLZ/";

	$downloadFilePath = DIRECTORY_EXPORT_FILES . $zipFileName.'.zip';

	// BOF CREATE ZIPCODES FOR EXPORT
	$arrZipCodes = array();
	for($i = 0; $i < 100 ; $i++){
		$zipcodePart = $i;
		if($zipcodePart < 10){
			$zipcodePart = "0" . $zipcodePart;
		}
		$arrZipCodes[] = $zipcodePart;
		for($k = 0; $k < 10 ; $k++){
			$arrZipCodes[] = $zipcodePart . $k;
		}
	}
	// EOF CREATE ZIPCODES FOR EXPORT

	// BOF DOWNLOAD FILE
		if($_GET["downloadFile"] != "") {
			require_once('classes/downloadFile.class.php');
			$thisDownload = new downloadFile(DIRECTORY_EXPORT_FILES, $_GET["downloadFile"]);
			$errorMessage .= $thisDownload->startDownload();
		}
	// EOF DOWNLOAD FILE

	if($_POST["submitFormExportDatas"] != "" && $_POST["selectExportFormat"]){
		if(!is_dir(DIRECTORY_EXPORT_FILES . $downloadFolder)){
			mkdir(DIRECTORY_EXPORT_FILES . $downloadFolder);
		}

		$exportCountryID = 81;
		#$exportFileType = 'xlsx'; // txt | csv || xls | xlsx
		$exportFileType = $_POST["selectExportFormat"];
		#$exportFileName = "customersExport_{###ZIP_CODE###}." . $exportFileType;
		$exportFileName = "kundenExport_PLZ-{###ZIP_CODE###}." . $exportFileType;

		$exportFieldSeperator = "\t";
		$exportLineSeperator = "\r\n";

		$arrFileNames = array();
		#for($i = 1 ; $i < 100 ; $i++){
		#$partPLZ = $i;
		#if($partPLZ < 10){ $partPLZ = "0" . $partPLZ;}

		foreach($arrZipCodes as $partPLZ){

			$thisExportName = preg_replace("/{###ZIP_CODE###}/", $partPLZ, $exportFileName);
			$arrFileNames[] = $thisExportName;

			/*
			if(isset($arrSalesmenZipcodeDatas[$partPLZ])){
				$thisStoreDir = convertChars($arrSalesmenZipcodeDatas[$partPLZ]["kundenname"]) . '/';
			}
			else {
				$thisStoreDir = convertChars($defaultStoreDir) . '/';
			}
			*/
			$thisStoreDir = convertChars($defaultStoreDir) . '/';
			$thisStoreDir = DIRECTORY_EXPORT_FILES . $downloadFolder . $thisStoreDir;

			if(!is_dir($thisStoreDir)){
				mkdir($thisStoreDir);
			}

			if(file_exists($thisStoreDir . $thisExportName)){
				unlink($thisStoreDir . $thisExportName);
			}

			clearstatcache();
			if($exportFileType == 'txt' || $exportFileType == 'csv') {
				$sql = "SET NAMES 'latin1'";
				$rs = $dbConnection->db_query($sql);
			}

			$sql = "
					SELECT
							'K-NR' AS `customersKundennummer`,
							'FIRMENNAME' AS `customersFirmenname`,

							'TELEFON1' AS `customersTelefon1`,
							'TELEFON2' AS `customersTelefon2`,
							'MOBIL1' AS `customersMobil1`,
							'MOBIL2' AS `customersMobil2`,

							'STRASSE' AS `customersCompanyStrasse`,

							'PLZ' AS `customersCompanyPLZ`,
							'ORT' AS `customersCompanyOrt`

							/*
							,
							'VERTRETERNAME' AS `salesmanName`,
							'VERBAND' AS `customersGruppe`
							*/
				";
			if($includeLastOrder) {
				$sql .= " , 'LETZTE BESTELLUNG' AS `lastOrderData` ";
			}

			$sql .= "
						UNION

						SELECT
							`tempTableCustomers`.`customersKundennummer`,

							`tempTableCustomers`.`customersFirmenname`,

							`tempTableCustomers`.`customersTelefon1`,
							`tempTableCustomers`.`customersTelefon2`,
							`tempTableCustomers`.`customersMobil1`,
							`tempTableCustomers`.`customersMobil2`,

							`tempTableCustomers`.`customersCompanyStrasse`,

							`tempTableCustomers`.`customersCompanyPLZ`,
							`tempTableCustomers`.`customersCompanyOrt`

							/*
							,
							`tempTableCustomers`.`salesmanName`,
							`tempTableCustomers`.`customerGroupsName`
							*/
				";

				if($includeLastOrder) {
					$sql .= "
							,
							CONCAT(
								`tempTableConfirmations`.`orderDocumentsDocumentDate`,
								': ',
								`tempTableConfirmations`.`orderDocumentDetailProductQuantity`,
								' x ',
								`tempTableConfirmations`.`orderCategoriesName`,
								' à ',
								`tempTableConfirmations`.`orderDocumentDetailProductSinglePrice`,
								' EUR'

							) AS `lastOrderData`
						";
				}
				$sql .= "

							FROM (

								SELECT
									`" . TABLE_CUSTOMERS . "`.`customersKundennummer`,
									`" . TABLE_CUSTOMERS . "`.`customersCompanyPLZ`,

									`" . TABLE_CUSTOMERS . "`.`customersFirmenname`,

									REPLACE(`" . TABLE_CUSTOMERS . "`.`customersTelefon1`, '+49', '0') AS `customersTelefon1`,
									REPLACE(`" . TABLE_CUSTOMERS . "`.`customersTelefon2`, '+49', '0') AS `customersTelefon2`,
									REPLACE(`" . TABLE_CUSTOMERS . "`.`customersMobil1`, '+49', '0') AS `customersMobil1`,
									REPLACE(`" . TABLE_CUSTOMERS . "`.`customersMobil2`, '+49', '0') AS `customersMobil2`,

									CONCAT(
										`" . TABLE_CUSTOMERS . "`.`customersCompanyStrasse`,
										' ',
										`" . TABLE_CUSTOMERS . "`.`customersCompanyHausnummer`
									) AS `customersCompanyStrasse`,

									`" . TABLE_CUSTOMERS . "`.`customersCompanyOrt`,

									`tableSalesmen`.`customersFirmenname` AS `salesmanName`,
									`common_customerGroups`.`customerGroupsName`

								FROM `" . TABLE_CUSTOMERS . "`

								LEFT JOIN `common_customerTypes`
								ON(`" . TABLE_CUSTOMERS . "`.`customersTyp` = `common_customerTypes`.`customerTypesID`)

								LEFT JOIN `common_salutationTypes` AS `salutationtypes_1`
								ON(`" . TABLE_CUSTOMERS . "`.`customersFirmenInhaberAnrede` = `salutationtypes_1`.`salutationTypesID`)

								LEFT JOIN `common_salutationTypes` AS `salutationtypes_2`
								ON(`" . TABLE_CUSTOMERS . "`.`customersFirmenInhaber2Anrede` = `salutationtypes_2`.`salutationTypesID`)

								LEFT JOIN `common_salutationTypes` AS `salutationtypes_3`
								ON(`" . TABLE_CUSTOMERS . "`.`customersAnsprechpartner1Anrede` = `salutationtypes_3`.`salutationTypesID`)

								LEFT JOIN `common_salutationTypes` AS `salutationtypes_4`
								ON(`" . TABLE_CUSTOMERS . "`.`customersAnsprechpartner2Anrede` = `salutationtypes_4`.`salutationTypesID`)

								LEFT JOIN `common_customerGroups`
								ON(`" . TABLE_CUSTOMERS . "`.`customersGruppe` = `common_customerGroups`.`customerGroupsID`)

								LEFT JOIN `" . TABLE_CUSTOMERS . "` AS `tableSalesmen`
								ON(`" . TABLE_CUSTOMERS . "`.`customersVertreterID` = `tableSalesmen`.`customersID`)


								WHERE 1
									AND `" . TABLE_CUSTOMERS . "`.`customersCompanyPLZ` LIKE '" . $partPLZ . "%'
									AND `" . TABLE_CUSTOMERS . "`.`customersCompanyCountry` = " . $exportCountryID . "
									AND `" . TABLE_CUSTOMERS . "`.`customersKundennummer` NOT LIKE 'AMZ%'
									AND `" . TABLE_CUSTOMERS . "`.`customersKundennummer` NOT LIKE 'EBY%'

								ORDER BY
									`" . TABLE_CUSTOMERS . "`.`customersCompanyPLZ` ASC,
									`" . TABLE_CUSTOMERS . "`.`customersKundennummer` ASC

							) AS `tempTableCustomers`
					";
				if($includeLastOrder) {
					$sql .= "
							LEFT JOIN (
								SELECT
										`" . TABLE_ORDER_CONFIRMATIONS . "`.`orderDocumentsNumber`,
										`" . TABLE_ORDER_CONFIRMATIONS . "`.`orderDocumentsCustomerNumber`,
										`" . TABLE_ORDER_CONFIRMATIONS . "`.`orderDocumentsDocumentDate`,

										`" . TABLE_ORDER_CONFIRMATIONS_DETAILS . "`.`orderDocumentDetailProductName`,
										`" . TABLE_ORDER_CONFIRMATIONS_DETAILS . "`.`orderDocumentDetailProductQuantity`,
										`" . TABLE_ORDER_CONFIRMATIONS_DETAILS . "`.`orderDocumentDetailProductSinglePrice`,
										`" .TABLE_ORDER_CATEGORIES . "`.`orderCategoriesName`

									FROM `" . TABLE_ORDER_CONFIRMATIONS . "`
									LEFT JOIN `" . TABLE_ORDER_CONFIRMATIONS_DETAILS . "`
									ON( `" . TABLE_ORDER_CONFIRMATIONS . "`.`orderDocumentsID` = `" . TABLE_ORDER_CONFIRMATIONS_DETAILS . "`.`orderDocumentDetailDocumentID`)

									LEFT JOIN `" .TABLE_ORDER_CATEGORIES . "`
									ON(`" . TABLE_ORDER_CONFIRMATIONS_DETAILS . "`.`orderDocumentDetailProductKategorieID` COLLATE utf8_unicode_ci = `" .TABLE_ORDER_CATEGORIES . "`.`orderCategoriesID`)

									WHERE 1
										AND `" . TABLE_ORDER_CONFIRMATIONS_DETAILS . "`.`orderDocumentDetailProductKategorieID` LIKE '001%'

									ORDER BY `" . TABLE_ORDER_CONFIRMATIONS . "`.`orderDocumentsDocumentDate` DESC

							) AS `tempTableConfirmations`
							ON(`tempTableCustomers`.`customersKundennummer` = `tempTableConfirmations`.`orderDocumentsCustomerNumber`)
					";
				}
			$sql .= "
							GROUP BY `tempTableCustomers`.`customersKundennummer`

							ORDER BY
								LENGTH(`customersCompanyPLZ`) ASC,
								`customersCompanyPLZ` ASC
			";

			if($exportFileType == 'txt' || $exportFileType == 'csv') {
				$sql .= "
					INTO OUTFILE '" . $thisStoreDir . $thisExportName . "'
						FIELDS TERMINATED BY '" . addslashes($exportFieldSeperator). "'
						LINES TERMINATED BY '" . addslashes($exportLineSeperator). "'


				";
			}

			$sql = preg_replace("/\/\*.*\*\//ismU", "", $sql);
#dd('sql');

			// BOF READ LASTOKZH-ORDER
/*
			$arrSql_Mandantory = array();
			if(!empty($arrSearchMandantory)){
				foreach($arrSearchMandantory as $thisMandatory){
					$thisOrderTable = preg_replace("/" . MANDATOR. "/","", TABLE_ORDER_CONFIRMATIONS);
					$thisOrderTableDetails = preg_replace("/" . MANDATOR. "/","", TABLE_ORDER_CONFIRMATIONS_DETAILS);
					$arrSql_Mandantory[] = "
						SELECT
							SUBSTRING(`orderDocumentsAddressZipcode`, 1, 2) AS `PLZ-Gebiete`,
							" . $dateField . ",
							`orderDocumentsAddressCountry` AS `PLZ-Land`,
							SUM(`orderDocumentDetailProductQuantity`) AS `countItems`

							FROM `" . $thisMandatory . $thisOrderTable . "`
							LEFT JOIN `" . $thisMandatory . $thisOrderTableDetails . "`
							ON( `" . $thisMandatory . $thisOrderTable . "`.`orderDocumentsID` = `" . $thisMandatory . $thisOrderTableDetails . "`.`orderDocumentDetailDocumentID`)
							WHERE 1
								AND `orderDocumentDetailProductKategorieID` LIKE '001%'
								" . $where . "
							GROUP BY CONCAT(SUBSTRING(`orderDocumentsAddressZipcode`, 1, 2), '#', `interval`)
						";
				}
			}
*/
			// EOF READ LAST OKZH-ORDER

			#FIELDS ESCAPED BY '\\\\'
			#LINES TERMINATED BY '\\n'
			#FIELDS ENCLOSED BY ''
			$rs = $dbConnection->db_query($sql);

			if($exportFileType == 'xls' || $exportFileType == 'xlsx') {
				$arrAlphabet = range('A', 'Z');
				$countFields = 0;
				$countRows = 1;

				define('EOL',(PHP_SAPI == 'cli') ? PHP_EOL : '<br />');

				require_once (PATH_PHP_EXCEL . 'Classes/PHPExcel.php');

				// Create new PHPExcel object
				$objPHPExcel = new PHPExcel();

				$xxxarrHeader = array(
					'type'       => PHPExcel_Style_Fill::FILL_SOLID,
					'startcolor' => array('rgb' => 'FEFF6F'),
					'endcolor'   => array('rgb' => 'FEFF6F')
				);

				$arrHeader = array(
					'fill' => array(
							'type' => PHPExcel_Style_Fill::FILL_SOLID,
							'color' => array('rgb'=>'FEFF6F'),
						),
					'font' => array(
							'bold' => true,
						)
					);

				// Set document properties
				$objPHPExcel->getProperties()->setCreator("File generated automatically")
											 ->setLastModifiedBy("File generated automatically")
											 ->setTitle($thisExportName)
											 ->setSubject($thisExportName)
											 ->setDescription($thisExportName)
											 ->setKeywords($thisExportName)
											 ->setCategory($thisExportName);

				while($ds = mysqli_fetch_assoc($rs)){
					$countFields = 0;
					foreach(array_keys($ds) as $field){
						// BOF ADD DATA
						$thisFieldAlphabetIndexPart1 = -1;
						$thisFieldExcelNamePart1 = "";
						if(($countFields + 1) > count($arrAlphabet)){
							$thisFieldAlphabetIndexPart1 = (floor(($countFields) / count($arrAlphabet)) - 1);
							$thisFieldExcelNamePart1 = $arrAlphabet[$thisFieldAlphabetIndexPart1];
						}
						$thisFieldAlphabetIndexPart2 = $countFields % count($arrAlphabet);
						$thisFieldExcelNamePart2 = $arrAlphabet[$thisFieldAlphabetIndexPart2];

						$thisFieldExcelName = $thisFieldExcelNamePart1 . $thisFieldExcelNamePart2 . $countRows;

						/*
						$objPHPExcel->setActiveSheetIndex(0)
									->setCellValue($thisFieldExcelName, (($ds[$field])))
								;
						*/
						$objPHPExcel->setActiveSheetIndex(0);
						/*
						$objPHPExcel->getDefaultStyle()
									->getNumberFormat()
									->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_TEXT)
							;
						*/
						$objPHPExcelActiveSheet = $objPHPExcel->getActiveSheet();
						#$objPHPExcelActiveSheet->setCellValue($thisFieldExcelName, (('"' . $ds[$field] . '"')));
						/*
						getStyle('L3:N2048')
                              ->getNumberFormat()->setFormatCode('0000');
							  */
						#$objPHPExcelActiveSheet->getStyle($thisFieldExcelName)->getAlignment()->setWrapText(true);
						$objPHPExcelActiveSheet->getCell($thisFieldExcelName)->setValueExplicit(preg_replace("/###/", " \\\\\\ \r\n", $ds[$field]), PHPExcel_Cell_DataType::TYPE_STRING);
						/*
						$objPHPExcelActiveSheet->getStyle($thisFieldExcelName)
												->getAlignment()
												->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT)
							;
						*/

						// EOF ADD DATA

						if($countRows == 1){
							#$objPHPExcel->getActiveSheet()->getStyle($thisFieldExcelName)->getFill()->applyFromArray($arrHeader);
							$objPHPExcel->getActiveSheet()->getStyle($thisFieldExcelName)->applyFromArray($arrHeader);
						}

						$countFields++;
					}

					flush();
					$countRows++;
				}

				// Set active sheet index to the first sheet, so Excel opens this as the first sheet
				$objPHPExcel->setActiveSheetIndex(0);
				foreach(range('A','Z') as $columnID) {
					$objPHPExcel->getActiveSheet()->getColumnDimension($columnID)
								->setAutoSize(true);
				}


				// Save Excel 2007 file
				#$successMessage .= date('H:i:s') . ' ' . $excelFileExcelVersion . '-Format wird-geschrieben. ' . '<br />';
				$callStartTime = microtime(true);

				$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, $excelFileExcelVersion);
				$objWriter->save($thisStoreDir . $thisExportName);
				$callEndTime = microtime(true);
				$callTime = $callEndTime - $callStartTime;

				#$successMessage .= 'Die Datei &quot;' . DIRECTORY_EXPORT_FILES . $thisExportName . '&quot; wurde erzeugt.';

				// Echo memory usage
				#echo date('H:i:s') , ' Current memory usage: ' , (memory_get_usage(true) / 1024 / 1024) , " MB" , EOL;

				// Echo memory peak usage
				#echo date('H:i:s') , " Peak memory usage: " , (memory_get_peak_usage(true) / 1024 / 1024) , " MB" , EOL;

				// Echo done
				#echo date('H:i:s') , " Done writing files" , EOL;
				#echo 'Files have been created in ' , getcwd() , EOL;

				displayMessages();
				clearstatcache();
				$objPHPExcel->disconnectWorksheets();
				unset($objPHPExcel);
			}
		}

		/*
		if(!empty($arrFileNames)){
			foreach($arrFileNames as $thisFileName) {
				if(file_exists(DIRECTORY_EXPORT_FILES . $thisFileName)){
					$createCommand = ZIP_PATH.' a -tzip '.DIRECTORY_EXPORT_FILES . $zipFileName.'.zip '.DIRECTORY_EXPORT_FILES.$thisFileName.'';
					$arrExecResults[] = $createCommand;
					$arrExecResults[] = exec($createCommand, $error);
					if(!empty($error)) {
						$arrExecResults[] = array('1. ZIP: error', $error);
					}
					unlink(DIRECTORY_EXPORT_FILES . $thisFileName);
				}
			}
		}
		*/

		// BOF COPY FILES TO SPECIAL DIR
			/*
			$thisStoreDir = convertChars($defaultStoreDir) . '/';
			$thisStoreDir = DIRECTORY_EXPORT_FILES . $downloadFolder . $thisStoreDir;

			if(!is_dir($thisStoreDir)){
				mkdir($thisStoreDir);
			}
			*/
			$arrFilesToDelete = array();
			if(!empty($arrSalesmenZipcodeDatas)){
				foreach($arrSalesmenZipcodeDatas as $thisZipcodeKey => $thisZipcodeData){
					foreach($thisZipcodeData as $thisZipcodeDatasSalesmen){
						$thisCopyDir = convertChars($thisZipcodeDatasSalesmen["kundenname"]) . '/';
						$thisCopyDir = DIRECTORY_EXPORT_FILES . $downloadFolder . $thisCopyDir;

						$thisOriginDir = DIRECTORY_EXPORT_FILES . $downloadFolder . $defaultStoreDir;

						if(!is_dir($thisCopyDir)){
							mkdir($thisCopyDir);
						}
						$thisCopyFile = preg_replace("/{###ZIP_CODE###}/", $thisZipcodeKey, $exportFileName);

						if(file_exists($thisOriginDir . $thisCopyFile)){
							$copyResult = copy($thisOriginDir . $thisCopyFile, $thisCopyDir . $thisCopyFile);
							if($copyResult) {
								$arrFilesToDelete[] = $thisOriginDir . $thisCopyFile;
							}
						}
						clearstatcache();
					}
				}
				if(!empty($arrFilesToDelete)){
					$arrFilesToDelete = array_unique($arrFilesToDelete);
					foreach($arrFilesToDelete as $thisFilesToDelete){
						if(file_exists($thisFilesToDelete)){
							unlink($thisFilesToDelete);
						}
						clearstatcache();
					}
				}
			}
		// EOF COPY FILES TO SPECIAL DIR

		if(file_exists(DIRECTORY_EXPORT_FILES . $zipFileName . '.zip')){
			unlink(DIRECTORY_EXPORT_FILES . $zipFileName . '.zip');
		}

		$createCommand = ZIP_PATH.' a '.DIRECTORY_EXPORT_FILES . $zipFileName.'.zip ' . DIRECTORY_EXPORT_FILES . $downloadFolder . '';
		$arrExecResults[] = $createCommand;
		$arrExecResults[] = exec($createCommand, $error);
		if(!empty($error)) {
			$arrExecResults[] = array('1. ZIP: error', $error);
		}
		if(DIRECTORY_EXPORT_FILES . $downloadFolder != ''){
			if(is_dir(DIRECTORY_EXPORT_FILES . $downloadFolder)){
				$createCommand = 'rmdir "' . DIRECTORY_EXPORT_FILES . $downloadFolder . '" /s /q';
				$arrExecResults[] = $createCommand;
				$arrExecResults[] = exec($createCommand, $error);
				if(!empty($error)) {
					$arrExecResults[] = array('2. DEL: error', $error);
				}
			}
		}
	}
?>

<?php
	require_once('inc/headerHTML.inc.php');
	$thisTitle = "Kunden-Export nach PLZ gefiltert";
	$headerHTML = preg_replace("/{###TITLE###}/", strip_tags($thisTitle), $headerHTML);
	echo $headerHTML;

	require_once(FILE_MENUE_TOP);
?>
<div id="mainArea">
	<div id="mainContent">
		<?php require_once(FILE_MENUE_SIDEBAR); ?>
		<div id="contentArea">
			<div id="contentAreaElements">
				<h1><?php if(SHOW_PAGE_TITLE_ICONS) { echo '<img src="' . PATH_ICONS_MENUE_TITLES . 'export.png" alt="" />'; } ?> <?php echo $thisTitle; ?></h1>

				<div class="contentDisplay">
					<div id="searchFilterArea">
					<form name="formExportDatas" method="post" action="<?php echo $_SERVER["REQUEST_URI"]; ?>" enctype="multipart/form-data">
						<table border="0" cellpadding="0" cellspacing="0" class="searchFilterContent">
							<tr>
								<td><b>Datei-Format:</b></td>
								<td>
									<select class="inputSelect_427" name="selectExportFormat">
										<option value=""> - Bitte w&auml;hlen - </option>
										<?php
											if(!empty($arrExportFileTypes)){
												asort($arrExportFileTypes);
												foreach($arrExportFileTypes as $thisFileType) {
													$selected = '';
													if($_POST["selectExportFormat"] == $thisFileType){
														$selected = ' selected="selected" ';
													}
													echo '<option value="' . $thisFileType . '" ' . $selected . ' >' . $thisFileType . '-Format</option>';
												}
											}
										?>
									</select>
								</td>
								<!--
								<td style="width:200px;"><input type="checkbox" name="searchCustomerProducts" <?php if($_POST["searchCustomerProducts"] == '1'){ echo ' checked="checked" '; } ?> value="1" /> <b>inkl. Kunden-Produkte?</b></td>
								<td>&nbsp;</td>

								<td style="width:200px;"><input type="checkbox" name="searchCustomerLastOrder" <?php if($_POST["searchCustomerLastOrder"] == '1'){ echo ' checked="checked" '; } ?> value="1" /> <b>inkl. letzte KZH-Bestellung?</b></td>
								<td>&nbsp;</td>
								-->
								<td><input type="submit" name="submitFormExportDatas" class="inputButton1" value="Export starten" /></td>
							</tr>
						</table>
					</form>
					</div>
					<?php displayMessages(); ?>
					<?php
						if(file_exists($downloadFilePath)) {
							$downloadPath = str_replace(BASEPATH, "", $downloadFilePath);
							$thisDownloadLink = '<p class="dataTableContent"><b>DOWNLOAD-LINK: </b><img src="layout/icons/icon' . getFileType(basename($downloadPath)) . '.gif" height="16" width="16" alt="" />
													<a href="' . $_SERVER["REQUEST_URI"] . '?documentType=&amp;downloadFile=' . basename($downloadPath) . '">' . basename($downloadPath) . '</a> (Stand: ' . date("d.m.Y H:i:s", filemtime($downloadPath)) . ')
												</p>';
							echo $thisDownloadLink;
						}
						clearstatcache();

						if(!empty($arrExecResults)){
							dd('arrExecResults');
						}
					?>
				</div>
			</div>
		</div>
		<div class="clear"></div>
	</div>
</div>
<?php
	if($arrGetUserRights["editPersonnel"]) {
?>
<script language="javascript" type="text/javascript">
	$(document).ready(function() {
		$(function() {
			$( "#datepicker" ).datepicker();
			$.datepicker.setDefaults($.datepicker.regional[""]);
			$('#editPersonnelVacationsStart').datepicker($.datepicker.regional["de"]);
		});
		$(function() {
			$('#tabs').tabs();
		});
		setFormDutyFields(<?php echo $jsonFormDutyFields; ?>);
		colorRowMouseOver('.displayOrders tbody tr');
	});
</script>
<?php
	}
?>
<?php require_once('inc/footerHTML.inc.php'); ?>