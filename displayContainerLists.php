<?php
	require_once('inc/requires.inc.php');

	if(!$arrGetUserRights["displayTracking"]) {
		header('location: ' . PAGE_EXIT_LOCATION);
		exit;
	}

	$jswindowMessage = "";
	$warningMessage = '';
	$errorMessage = '';
	$successMessage = '';
	$infoMessage = '';

	// BOF DOWNLOAD FILE
	if($_REQUEST["downloadFile"] != "") {
		$thisDownloadFile = basename($_REQUEST["downloadFile"]);
		$thisDownloadFolder = DIRECTORY_DOWNLOAD_CONTAINER_LISTS;

		require_once('classes/downloadFile.class.php');
		$thisDownload = new downloadFile($thisDownloadFolder, $thisDownloadFile);
		$errorMessage .= $thisDownload->startDownload();
	}
	// BOF DOWNLOAD FILE

	$dbConnection = new DB_Connection();
	$db_open = $dbConnection->db_connect();

	// BOF GET ALL PRODUCTION START DATES
		/*
		$sql = "
				SELECT
						`containerListsProductionsDateStart`
					FROM `" . TABLE_CONTAINER_LISTS . "`
					WHERE 1
					GROUP BY `containerListsProductionsDateStart`
					ORDER BY `containerListsProductionsDateStart` DESC
			";
		$rs = $dbConnection->db_query($sql, $db_open);
		$arrContainerListsProductionsDateStartDatas = array();
		while($ds = mysqli_fetch_assoc($rs)){
			$arrContainerListsProductionsDateStartDatas[] = $ds["containerListsProductionsDateStart"];
		}
		*/
	// EOF GET ALL PRODUCTION START DATES

	// BOF GET ALL PRODUCTION END DATES
		$sql = "
				SELECT
						`containerListsProductionsDateEnd`
					FROM `" . TABLE_CONTAINER_LISTS . "`
					WHERE 1
					GROUP BY `containerListsProductionsDateEnd`
					ORDER BY `containerListsProductionsDateEnd` DESC
			";
		$rs = $dbConnection->db_query($sql, $db_open);
		$arrContainerListsProductionsDateEndDatas = array();
		while($ds = mysqli_fetch_assoc($rs)){
			$arrContainerListsProductionsDateEndDatas[] = $ds["containerListsProductionsDateEnd"];
		}
	// EOF GET ALL PRODUCTION END DATES

	// BOF GET ALL PRODUCTION START AND END DATES
		/*
		$sql = "
				SELECT
						CONCAT(`containerListsProductionsDateStart`, ' - ', `containerListsProductionsDateEnd`) AS `containerListsProductionsDate`
					FROM `" . TABLE_CONTAINER_LISTS . "`
					WHERE 1
					GROUP BY CONCAT(`containerListsProductionsDateStart`, ' - ', `containerListsProductionsDateEnd`)
					ORDER BY `containerListsProductionsDateStart` DESC
			";
		$rs = $dbConnection->db_query($sql, $db_open);
		$arrContainerListsProductionsDates = array();
		while($ds = mysqli_fetch_assoc($rs)){
			$arrContainerListsProductionsDates[] = $ds["containerListsProductionsDate"];
		}
		*/
	// EOF GET ALL PRODUCTION START AND END DATES


	if($_REQUEST["searchCustomerNumber"] != ""){
		$sqlWhere = " AND `containerListsCustomerNumber` = '" . $_REQUEST["searchCustomerNumber"] . "' ";
	}
	else if($_REQUEST["searchProductionDate"] != ""){
		$sqlWhere = " AND `containerListsProductionsDateEnd` = '" . $_REQUEST["searchProductionDate"] . "' ";
	}
	else if($_REQUEST["searchWord"] != ""){
		$sqlWhere = "
			AND (
				`containerListsCustomerNumber` LIKE  '%" . $_REQUEST["searchWord"] . "%'
				OR `containerListsCustomerName` LIKE '%" . $_REQUEST["searchWord"] . "%'
				OR `containerListsCustomerKommission` LIKE '%" . $_REQUEST["searchWord"] . "%'
				OR `customersFirmenname` LIKE '%" . $_REQUEST["searchWord"] . "%'
			)";
	}
	else if($_REQUEST["searchCheck"] == '1') {
		$sqlWhere = " AND (
						SOUNDEX(`" . TABLE_CONTAINER_LISTS . "`.`containerListsCustomerName`) != SOUNDEX(`" . TABLE_CUSTOMERS . "`.`customersFirmenname`)
						AND (
							`" . TABLE_CONTAINER_LISTS . "`.`containerListsCustomerName` NOT LIKE CONCAT('%', `" . TABLE_CUSTOMERS . "`.`customersFirmenname`, '%')
							AND `" . TABLE_CUSTOMERS . "`.`customersFirmenname` NOT LIKE CONCAT('%', `" . TABLE_CONTAINER_LISTS . "`.`containerListsCustomerName`, '%')
						)
					)
			";
	}

	$sql = "
		SELECT
				/* SQL_CALC_FOUND_ROWS */

				`" . TABLE_CONTAINER_LISTS . "`.`containerListsID`,
				`" . TABLE_CONTAINER_LISTS . "`.`containerListsFilename`,
				`" . TABLE_CONTAINER_LISTS . "`.`containerListsProductionsDateStart`,
				`" . TABLE_CONTAINER_LISTS . "`.`containerListsProductionsDateEnd`,
				`" . TABLE_CONTAINER_LISTS . "`.`containerListsDepartureKW`,
				`" . TABLE_CONTAINER_LISTS . "`.`containerListsArrivalKW`,
				`" . TABLE_CONTAINER_LISTS . "`.`containerListsDeliveryKW`,
				`" . TABLE_CONTAINER_LISTS . "`.`containerListsCustomerNumber`,
				`" . TABLE_CONTAINER_LISTS . "`.`containerListsCustomerName`,
				`" . TABLE_CONTAINER_LISTS . "`.`containerListsCustomerKommission`,
				`" . TABLE_CONTAINER_LISTS . "`.`containerListsSalesmanNumber`,
				`" . TABLE_CONTAINER_LISTS . "`.`containerListsSalesmanName`,
				`" . TABLE_CONTAINER_LISTS . "`.`containerListsProduct`,
				`" . TABLE_CONTAINER_LISTS . "`.`containerListsQuantity`,
				`" . TABLE_CONTAINER_LISTS . "`.`containerListsTirArkasi`,
				`" . TABLE_CONTAINER_LISTS . "`.`containerListsCita`,
				`" . TABLE_CONTAINER_LISTS . "`.`containerListsExtra`,

				`" . TABLE_CUSTOMERS . "`.`customersKundennummer`,
				`" . TABLE_CUSTOMERS . "`.`customersFirmenname`



			FROM `" . TABLE_CONTAINER_LISTS . "`

			LEFT JOIN `" . TABLE_CUSTOMERS . "`
			ON(`" . TABLE_CONTAINER_LISTS . "`.`containerListsCustomerNumber` = `" . TABLE_CUSTOMERS . "`.`customersKundennummer`)

			WHERE 1
			" . $sqlWhere . "

			ORDER BY
				`containerListsProductionsDateStart` DESC,
				`containerListsCustomerNumber`
		";



?>
<?php
	require_once('inc/headerHTML.inc.php');
	$thisTitle = "TR-Container-Listen";

	$headerHTML = preg_replace("/{###TITLE###}/", strip_tags($thisTitle), $headerHTML);
	echo $headerHTML;

	require_once(FILE_MENUE_TOP);


?>

<div id="mainArea">
	<div id="mainContent">
		<?php require_once(FILE_MENUE_SIDEBAR); ?>
		<div id="contentArea">
			<a name="top"></a>
			<div id="contentAreaElements">
				<h1><?php if(SHOW_PAGE_TITLE_ICONS) { echo '<img src="' . PATH_ICONS_MENUE_TITLES . 'shipping.png" alt="" />'; } ?> <?php echo $thisTitle; ?></h1>
				<p class="warningArea">Achtung: In den TR-Listen treten manchmal Fehler in den Kundennummern auf (z. B. Zahlendreher) und Fehler in den Namensschreibweisen!<br />Zum Vergleich werden die zur Listen-Kundennummer geh&ouml;rigen Kundennamen angezeigt.</p>

				<div style="text-align:right;"><a href="<?php echo PAGE_DOWNLOAD_CONTAINER_LISTS; ?>">Download der Container-Listen als Excel-Datei</a></div>
				<div id="searchFilterArea">
					<form name="formFilterSearch" method="post" action="<?php echo $_SERVER["PHP_SELF"]; ?>">
					<table border="0" cellpadding="0" cellspacing="0" class="searchFilterContent">
						<tr>
							<td>
								<label for="searchCustomerNumber">K-NR:</label>
								<input type="text" name="searchCustomerNumber" id="searchCustomerNumber" class="inputField_70" value="<?php echo $_REQUEST["searchCustomerNumber"]; ?>" />
							</td>
							<td>
								<label for="searchProductionDate">Datum:</label>
								<select name="searchProductionDate" class="inputField_200">
									<option value=""></option>
									<?php
										if(!empty($arrContainerListsProductionsDateEndDatas)){
											foreach($arrContainerListsProductionsDateEndDatas as $thisContainerListsProductionsDateEnd){
												$selected = '';
												if($thisContainerListsProductionsDateEnd == $_REQUEST["searchProductionDate"]){
													$selected = 'selected="selected" ';
												}
												echo '<option value="' . $thisContainerListsProductionsDateEnd . '" ' . $selected . ' >' . formatDate($thisContainerListsProductionsDateEnd, "display"). '</option>';
											}
										}
									?>
								</select>
							</td>
							<td>
								<label for="searchWord">Suchbegriff:</label>
								<input type="text" name="searchWord" id="searchWord" class="inputField_70" value="<?php echo $_REQUEST["searchWord"]; ?>" />
							</td>
							<td>
								<label for="searchCheck">Check:</label>
								<input type="checkbox" name="searchCheck" id="searchCheck" class="inputField_70" value="1" <?php if( $_REQUEST["searchCheck"] == '1') { echo ' checked="checked" '; } ?> />
							</td>
							<td>
								<input type="hidden" name="editID" id="editID" value="" />
								<input type="submit" name="submitSearch" class="inputButton0" value="Suchen" />
							</td>
						</tr>
					</table>
					<input type="hidden" name="sortDirection" id="inputSortDirection" value="<?php echo $_REQUEST["sortDirection"]; ?>" />
					<input type="hidden" name="sortRow" id="inputSortRow" value="<?php echo $_REQUEST["sortRow"]; ?>" />
					<input type="hidden" name="sortField" id="inputSortField" value="<?php echo $_REQUEST["sortField"]; ?>" />
					<input type="hidden" name="displayType" id="inputDisplayType" value="<?php echo $_REQUEST["displayType"]; ?>" />
					<input type="hidden" name="submitSearchForm" id="inputSubmitSearchForm" value="1" />
					<input type="hidden" name="displayOrderStatus" value="<?php echo $_REQUEST["displayOrderStatus"]; ?>" />
					</form>
				</div>

				<?php displayMessages(); ?>
				
				<?php
				
					// BOF GET COUNT ALL ROWS
						$sql_getAllRows = "
								SELECT

									COUNT(`" . TABLE_CONTAINER_LISTS . "`.`containerListsID`) AS `countAllRows`

								FROM `" . TABLE_CONTAINER_LISTS . "`

								WHERE 1
									" . $sqlWhere . "
							";

						$rs_getAllRows = $dbConnection->db_query($sql_getAllRows);
						list(
							$countAllRows
						) = mysqli_fetch_array($rs_getAllRows);
					// EOF GET COUNT ALL ROWS

					if($_REQUEST["page"] == "" || $_REQUEST["page"] == 0) {
						$_REQUEST["page"] = 1;
					}

					if(MAX_DELIVERIES_PER_PAGE > 0) {
						$sql .= " LIMIT " . (($_REQUEST["page"] - 1) * MAX_DELIVERIES_PER_PAGE) . ", " . MAX_DELIVERIES_PER_PAGE." ";
					}

					$rs = $dbConnection->db_query($sql, $db_open);

					// OLD BOF GET ALL ROWS
						#$sql_totalRows = "SELECT FOUND_ROWS() AS `countRows`";
						#$rs_totalRows = $dbConnection->db_query($sql_totalRows);
						#list($countRows) = mysqli_fetch_array($rs_totalRows);
					// OLD EOF GET ALL ROWS

					$countRows = $countAllRows;
					
					$pagesCount = ceil($countRows / MAX_DELIVERIES_PER_PAGE);

					if($countRows > 0) {
				?>
				<?php
					if($pagesCount > 1 && $_GET["loadAll"] != "true") {
						include(FILE_MENUE_PAGES);
					}
				?>
				<div class="contentDisplay">
					<table border="0" cellpadding="0" cellspacing="0" width="100%" class="displayOrders">
						<thead>
							<tr>
								<th style="width:45px;text-align:right;">#</th>
								<th class="sortColumn">K-Nr</th>
								<th class="sortColumn">Kunde-Liste</th>
								<th class="sortColumn">Kunde-Auftragslisten</th>
								<th class="sortColumn">Menge</th>
								<th class="sortColumn">Produkt</th>
								<th class="sortColumn" colspan="2">Produktion von - bis</th>
								<th class="sortColumn">Abfahrt</th>
								<th class="sortColumn">Ankunft</th>
								<th class="sortColumn">Versand</th>
								<th class="sortColumn">Transport</th>
								<th class="sortColumn">Check</th>
								<th class="sortColumn">Info</th>

								<!--
								<th class="sortColumn">Liste</th>
								-->

							</tr>
						</thead>

						<tbody>
						<?php
							$count = 0;

							$thisMarker = '';
							while($ds = mysqli_fetch_assoc($rs)){
								#dd('ds');
								if($count%2 == 0){ $rowClass = 'row0'; }
								else { $rowClass = 'row1'; }

								if($thisMarker != $ds["containerListsProductionsDateEnd"]){
									$thisMarker = $ds["containerListsProductionsDateEnd"];
									echo '<tr class="tableRowTitle1">';
									echo '<td colspan="14">';
									echo 'PRODUKTION: ' . formatDate($ds["containerListsProductionsDateEnd"], "display");

									if($_REQUEST["searchProductionDate"] != ""){
										$thisContainerList = $ds["containerListsFilename"];
										$arrFileInfo = pathinfo($thisContainerList);
										$thisContainerListExcelPath = DIRECTORY_DOWNLOAD_CONTAINER_LISTS . convertChars($arrFileInfo["filename"]) . '.ods';

										echo '<a href="?downloadFile=' . $thisContainerListExcelPath . '" style="float:right;line-height:12px;letter-spacing:0px;font-size:11px;">';
										echo '<img src="layout/icons/icon' . getFileType(basename($thisContainerListExcelPath)) . '.gif" width="10" height="10" title="Dokument herunterladen" alt="Download" /> ';
										echo 'Download Excel-Datei &quot;' . basename($thisContainerListExcelPath) . '&quot;';
										echo '</a>';
									}
									echo '</td>';
									echo '</tr>';
								}

								echo '<tr class="'.$rowClass.'">';

									echo '<td style="text-align:right;"><b>' . ($count + 1) .'.</b></td>';

									echo '<td style="font-weight:bold;">';
									$thisContainerListsCustomerNumber = $ds["containerListsCustomerNumber"];
									echo '<a href="' . PAGE_EDIT_CUSTOMER . '?searchCustomerNumber=' . $thisContainerListsCustomerNumber . '">' . $thisContainerListsCustomerNumber . '</a>';
									echo '</td>';

									echo '<td style="white-space:nowrap;">';
									$thisContainerListsCustomerName = htmlentities($ds["containerListsCustomerName"]);
									#$thisContainerListsCustomerName = transliterateTurkishChars($thisContainerListsCustomerName, true);

									$thisContainerListsCustomerKommission = $ds["containerListsCustomerKommission"];
									$thisContainerListsCustomerKommission = transliterateTurkishChars($thisContainerListsCustomerKommission, true);

									echo $thisContainerListsCustomerName;
									if($thisContainerListsCustomerKommission != '' && $thisContainerListsCustomerKommission != $thisContainerListsCustomerName){
										echo '<br /><b>Kommission:</b> ';
										echo $thisContainerListsCustomerKommission;
									}
									echo '</td>';

									echo '<td style="white-space:nowrap;">';
									echo  htmlentities($ds["customersFirmenname"]);
									echo '</td>';

									echo '<td style="text-align:right;font-weight:bold;">';
									echo $ds["containerListsQuantity"];
									echo '</td>';

									echo '<td style="white-space:nowrap;">';
									$thisContainerListProduct = $ds["containerListsProduct"];
									$thisContainerListProduct = translateDeliveryData($thisContainerListProduct, "TR", "DE");
									$thisContainerListProduct = transliterateTurkishChars($thisContainerListProduct, false);

									$thisContainerListProductAdd = trim($ds["containerListsCita"]);
									$thisContainerListProductAdd = translateDeliveryData($thisContainerListProductAdd, "TR", "DE");
									$thisContainerListProductAdd = transliterateTurkishChars($thisContainerListProductAdd, false);

									echo $thisContainerListProduct;
									if($thisContainerListProductAdd != ''){
										echo ' (' . $thisContainerListProductAdd . ')';
									}
									echo '</td>';

									echo '<td style="font-size:10px;">';
									echo formatDate($ds["containerListsProductionsDateStart"], "display");
									echo '</td>';

									echo '<td style="font-size:10px;">';
									echo formatDate($ds["containerListsProductionsDateEnd"], "display");
									echo '</td>';

									echo '<td style="white-space:nowrap;font-size:10px;">';
									$thisContainerListsDepartureKW = $ds["containerListsDepartureKW"];
									if(strlen($thisContainerListsDepartureKW) == 1){
										$thisContainerListsDepartureKW = '0'.$thisContainerListsDepartureKW;
									}
									echo 'ca. ' . $thisContainerListsDepartureKW . '. KW';
									echo '</td>';

									echo '<td style="white-space:nowrap;font-size:10px;">';
									$thisContainerListsArrivalKW = $ds["containerListsArrivalKW"];
									if(strlen($thisContainerListsArrivalKW) == 1){
										$thisContainerListsArrivalKW = '0'.$thisContainerListsArrivalKW;
									}
									echo 'ca. ' . $thisContainerListsArrivalKW . '. KW';
									echo '</td>';

									echo '<td style="white-space:nowrap;font-weight:bold;font-size:10px;">';
									$thisContainerListsDeliveryKW = $ds["containerListsDeliveryKW"];
									if(strlen($thisContainerListsDeliveryKW) == 1){
										$thisContainerListsDeliveryKW = '0'.$thisContainerListsDeliveryKW;
									}
									echo 'ca. ' . $thisContainerListsDeliveryKW . '. KW';
									echo '</td>';

									echo '<td style="white-space:nowrap;text-align:center;">';
									if($ds["containerListsTirArkasi"] != ''){
										echo '<img width="12" height="12" src="layout/icons/redDot.png" alt="Hinten LKW (Vorkasse / Nachnahme)" title="Hinten LKW (Vorkasse / Nachnahme)" />';
									}
									echo '</td>';

									echo '<td style="font-size:10px;">';
									$checkCustomername = false;
									$soundexContainerListsCustomerName = soundex($ds["containerListsCustomerName"]);
									$soundexCustomersFirmenname = soundex($ds["customersFirmenname"]);
									if(($soundexCustomersFirmenname == $soundexContainerListsCustomerName)
										|| (preg_match("/" . $ds["containerListsCustomerName"] . "/", $ds["customersFirmenname"]))
										|| (preg_match("/" . $ds["customersFirmenname"] . "/", $ds["containerListsCustomerName"]))
									){
										$checkCustomername = true;
									}
									else {
										$checkCustomername = false;
									}

									if($checkCustomername){
										echo '<img src="layout/icons/iconOk.png" width="14" height=""14 alt="OK" title="Namen stimmen &uuml;berein" />';
									}
									else {
										echo '<img src="layout/icons/iconNotOk.png" width="14" height=""14 alt="Nicht OK" title="Namen stimmen nicht &uuml;berein" />';
									}
									echo '</td>';

									echo '<td style="white-space:nowrap;text-align:center;">';
									echo '<span class="toolItem">';
									echo '<a href="' . PAGE_DISPLAY_ORDERS_OVERVIEW . '?searchCustomerNumber=' . $ds["containerListsCustomerNumber"] . '">' . '<img src="layout/menueIcons/menueSidebar/ordersoverview.png" width="16" height="16" title="In Produktionsansicht anzeigen" alt="Zur Produktionsansicht" /></a>';
									echo '</span>';
									echo '</td>';

									#echo '<td style="white-space:nowrap">';
									#echo $ds["containerListsFilename"];
									#echo '</td>';

								echo '</tr>';
								$count++;
							}
						?>
						</tbody>
					</table>
				</div>
				<?php
					if($pagesCount > 1 && $_GET["loadAll"] != "true") {
						include(FILE_MENUE_PAGES);
					}
				?>
				<div style="text-align:right;" class="menueToTop">
					<a href="#top">nach oben</a>
				</div>
				<?php
					}
					else {
						echo '<p class="warningArea">Es wurden keine Daten gefunden!</p>';
					}

				?>
				<div class="clear"></div>
			</div>
			<div class="clear"></div>
		</div>
	</div>
</div>

<script language="javascript" type="text/javascript">
	<!--
	/* <![CDATA[ */
		$(document).ready(function() {

		});
	/* ]]> */
	// -->
</script>

<?php require_once('inc/footerHTML.inc.php'); ?>