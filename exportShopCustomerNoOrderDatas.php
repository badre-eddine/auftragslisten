<?php
	require_once('inc/requires.inc.php');
	ini_set('memory_limit', '512M');

	if(!$arrGetUserRights["exportCustomers"]) {
		header('location: ' . PAGE_EXIT_LOCATION);
		exit;
	}

	$jswindowMessage = "";
	$warningMessage = '';
	$errorMessage = '';
	$successMessage = '';
	$infoMessage = '';

	$dbConnection = new DB_Connection();
	$db_open = $dbConnection->db_connect();

	$userDatas = getUserDatas();
	$arrExportFileTypes = array("txt", "csv", "xls", "xlsx");

	$excelFileExcelVersion = 'Excel5';
	#$excelFileExcelVersion = 'Excel2007';

	$zipFileName = "exportNichtBestellendeShopKundenExport_PLZ";
	$downloadFilePath = DIRECTORY_EXPORT_FILES . $zipFileName.'.zip';

	// BOF DOWNLOAD FILE
	if($_GET["downloadFile"] != "") {
		require_once('classes/downloadFile.class.php');
		$thisDownload = new downloadFile(DIRECTORY_EXPORT_FILES, $_GET["downloadFile"]);
		$errorMessage .= $thisDownload->startDownload();
	}
	// EOF DOWNLOAD FILE

	if($_POST["submitFormExportDatas"] != "" && $_POST["selectExportFormat"]){
		$exportCountryID = 81;
		#$exportFileType = 'xlsx'; // txt | csv || xls | xlsx
		$exportFileType = $_POST["selectExportFormat"];
		#$exportFileName = "customersExport_{###ZIP_CODE###}." . $exportFileType;
		$exportFileName = "exportNichtBestellendeShopKundenExport_PLZ-{###ZIP_CODE###}." . $exportFileType;

		if(file_exists(DIRECTORY_EXPORT_FILES . $zipFileName . '.zip')){
			unlink(DIRECTORY_EXPORT_FILES . $zipFileName . '.zip');
		}
		$exportFieldSeperator = "\t";
		$exportLineSeperator = "\r\n";

		$dbConnection_ExternShop = new DB_Connection(DB_HOST_EXTERN_SHOP, '', DB_NAME_EXTERN_SHOP, DB_USER_EXTERN_SHOP, DB_PASSWORD_EXTERN_SHOP);
		$dbOpen_ExternShop = $dbConnection_ExternShop->db_connect();

		$arrFileNames = array();
		for($i = 1 ; $i < 100 ; $i++){
			$partPLZ = $i;
			if($partPLZ < 10){ $partPLZ = "0" . $partPLZ;}
			$thisExportName = preg_replace("/{###ZIP_CODE###}/", $partPLZ, $exportFileName);
			$arrFileNames[] = $thisExportName;
			if(file_exists(DIRECTORY_EXPORT_FILES . $thisExportName)){
				unlink(DIRECTORY_EXPORT_FILES . $thisExportName);
			}

			clearstatcache();
			if($exportFileType == 'txt' || $exportFileType == 'csv') {
				$sql_ExternShop = "SET NAMES 'latin1'";
				$rs_ExternShop = $dbConnection->db_query($sql_ExternShop);
			}

			$sql_ExternShop = "
						SELECT

								'FIRMA' AS `entry_company`,
								'INHABER' AS `customers_manager`,
								'VORNAME' AS `entry_firstname`,
								'NACHNAME' AS `entry_lastname`,
								'STRASSE' AS `entry_street_address`,
								'PLZ' AS `entry_postcode`,
								'ORT' AS `entry_city`,
								'KONTAKT-VORNAME' AS `customers_firstname`,
								'KONTAKT-NACHNAME' AS `customers_lastname`,
								'MAIL' AS `customers_email_address`,
								'TELEFON' AS `customers_telephone`,
								'FAX' AS `customers_fax`,
								'TYP' AS `customers_type`

							UNION

							SELECT

								`tempTable`.`entry_company`,
								`tempTable`.`customers_manager`,
								`tempTable`.`entry_firstname`,
								`tempTable`.`entry_lastname`,
								`tempTable`.`entry_street_address`,
								`tempTable`.`entry_postcode`,
								`tempTable`.`entry_city`,
								`tempTable`.`customers_firstname`,
								`tempTable`.`customers_lastname`,
								`tempTable`.`customers_email_address`,
								`tempTable`.`customers_telephone`,
								`tempTable`.`customers_fax`,
								`tempTable`.`customers_type`


								FROM (

									SELECT
										`" . TABLE_SHOP_ADDRESS_BOOK . "`.`entry_company`,
										`" .TABLE_SHOP_CUSTOMERS . "`.`customers_manager`,
										`" . TABLE_SHOP_ADDRESS_BOOK . "`.`entry_firstname`,
										`" . TABLE_SHOP_ADDRESS_BOOK . "`.`entry_lastname`,
										`" . TABLE_SHOP_ADDRESS_BOOK . "`.`entry_street_address`,
										`" . TABLE_SHOP_ADDRESS_BOOK . "`.`entry_postcode`,
										`" . TABLE_SHOP_ADDRESS_BOOK . "`.`entry_city`,
										`" .TABLE_SHOP_CUSTOMERS . "`.`customers_firstname`,
										`" .TABLE_SHOP_CUSTOMERS . "`.`customers_lastname`,
										`" .TABLE_SHOP_CUSTOMERS . "`.`customers_email_address`,
										`" .TABLE_SHOP_CUSTOMERS . "`.`customers_telephone`,
										`" .TABLE_SHOP_CUSTOMERS . "`.`customers_fax`,
										`" .TABLE_SHOP_CUSTOMERS . "`.`customers_type`

									FROM `" .TABLE_SHOP_CUSTOMERS . "`
									LEFT JOIN `" . TABLE_SHOP_ADDRESS_BOOK . "`
									ON(`" .TABLE_SHOP_CUSTOMERS . "`.`customers_default_address_id` = `" . TABLE_SHOP_ADDRESS_BOOK . "`.`address_book_id`)
									WHERE 1
										AND (`customers_cid` = ''
											OR `customers_cid` IS NULL
										)
										AND `" . TABLE_SHOP_ADDRESS_BOOK . "`.`entry_postcode` LIKE '" . $partPLZ . "%'
										AND `" . TABLE_SHOP_ADDRESS_BOOK . "`.`entry_country_id` = " . $exportCountryID . "

									ORDER BY `" . TABLE_SHOP_ADDRESS_BOOK . "`.`entry_postcode` ASC
								) AS `tempTable`
				";
			if($exportFileType == 'txt' || $exportFileType == 'csv') {
				$sql_ExternShop .= "
					INTO OUTFILE '" . DIRECTORY_EXPORT_FILES . $thisExportName . "'
						FIELDS TERMINATED BY '" . addslashes($exportFieldSeperator). "'
						LINES TERMINATED BY '" . addslashes($exportLineSeperator). "'


				";
			}
			#dd('sql_ExternShop');
			#FIELDS ESCAPED BY '\\\\'
			#LINES TERMINATED BY '\\n'
			#FIELDS ENCLOSED BY ''
			$rs_ExternShop = $dbConnection_ExternShop->db_query($sql_ExternShop);

			if($exportFileType == 'xls' || $exportFileType == 'xlsx') {
				$arrAlphabet = range('A', 'Z');
				$countFields = 0;
				$countRows = 1;

				define('EOL',(PHP_SAPI == 'cli') ? PHP_EOL : '<br />');

				require_once (PATH_PHP_EXCEL . 'Classes/PHPExcel.php');

				// Create new PHPExcel object
				$objPHPExcel = new PHPExcel();

				$xxxarrHeader = array(
					'type'       => PHPExcel_Style_Fill::FILL_SOLID,
					'startcolor' => array('rgb' => 'FEFF6F'),
					'endcolor'   => array('rgb' => 'FEFF6F')
				);

				$arrHeader = array(
					'fill' => array(
							'type' => PHPExcel_Style_Fill::FILL_SOLID,
							'color' => array('rgb'=>'FEFF6F'),
						),
					'font' => array(
							'bold' => true,
						)
					);

				// Set document properties
				$objPHPExcel->getProperties()->setCreator("File generated automatically")
											 ->setLastModifiedBy("File generated automatically")
											 ->setTitle($thisExportName)
											 ->setSubject($thisExportName)
											 ->setDescription($thisExportName)
											 ->setKeywords($thisExportName)
											 ->setCategory($thisExportName);

				while($ds = mysqli_fetch_assoc($rs_ExternShop)){
					$countFields = 0;
					foreach(array_keys($ds) as $field){
						// BOF ADD DATA
						$thisFieldAlphabetIndexPart1 = -1;
						$thisFieldExcelNamePart1 = "";
						if(($countFields + 1) > count($arrAlphabet)){
							$thisFieldAlphabetIndexPart1 = (floor(($countFields) / count($arrAlphabet)) - 1);
							$thisFieldExcelNamePart1 = $arrAlphabet[$thisFieldAlphabetIndexPart1];
						}
						$thisFieldAlphabetIndexPart2 = $countFields % count($arrAlphabet);
						$thisFieldExcelNamePart2 = $arrAlphabet[$thisFieldAlphabetIndexPart2];

						$thisFieldExcelName = $thisFieldExcelNamePart1 . $thisFieldExcelNamePart2 . $countRows;

						/*
						$objPHPExcel->setActiveSheetIndex(0)
									->setCellValue($thisFieldExcelName, (($ds[$field])))
								;
						*/
						$objPHPExcel->setActiveSheetIndex(0);
						/*
						$objPHPExcel->getDefaultStyle()
									->getNumberFormat()
									->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_TEXT)
							;
						*/
						$objPHPExcelActiveSheet = $objPHPExcel->getActiveSheet();
						#$objPHPExcelActiveSheet->setCellValue($thisFieldExcelName, (('"' . $ds[$field] . '"')));
						/*
						getStyle('L3:N2048')
                              ->getNumberFormat()->setFormatCode('0000');
							  */
						$objPHPExcelActiveSheet->getCell($thisFieldExcelName)->setValueExplicit($ds[$field], PHPExcel_Cell_DataType::TYPE_STRING);
						/*
						$objPHPExcelActiveSheet->getStyle($thisFieldExcelName)
												->getAlignment()
												->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT)
							;
						*/

						// EOF ADD DATA

						if($countRows == 1){
							#$objPHPExcel->getActiveSheet()->getStyle($thisFieldExcelName)->getFill()->applyFromArray($arrHeader);
							$objPHPExcel->getActiveSheet()->getStyle($thisFieldExcelName)->applyFromArray($arrHeader);
						}

						$countFields++;
					}

					flush();
					$countRows++;
				}

				// Set active sheet index to the first sheet, so Excel opens this as the first sheet
				$objPHPExcel->setActiveSheetIndex(0);
				foreach(range('A','Z') as $columnID) {
					$objPHPExcel->getActiveSheet()->getColumnDimension($columnID)
								->setAutoSize(true);
				}


				// Save Excel 2007 file
				#$successMessage .= date('H:i:s') . ' ' . $excelFileExcelVersion . '-Format wird-geschrieben. ' . '<br />';
				$callStartTime = microtime(true);

				$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, $excelFileExcelVersion);
				$objWriter->save(DIRECTORY_EXPORT_FILES . $thisExportName);
				$callEndTime = microtime(true);
				$callTime = $callEndTime - $callStartTime;

				#$successMessage .= 'Die Datei &quot;' . DIRECTORY_EXPORT_FILES . $thisExportName . '&quot; wurde erzeugt.';

				// Echo memory usage
				#echo date('H:i:s') , ' Current memory usage: ' , (memory_get_usage(true) / 1024 / 1024) , " MB" , EOL;

				// Echo memory peak usage
				#echo date('H:i:s') , " Peak memory usage: " , (memory_get_peak_usage(true) / 1024 / 1024) , " MB" , EOL;

				// Echo done
				#echo date('H:i:s') , " Done writing files" , EOL;
				#echo 'Files have been created in ' , getcwd() , EOL;

				#displayMessages();#
				clearstatcache();
				$objPHPExcel->disconnectWorksheets();
				unset($objPHPExcel);
			}
		}

		if(!empty($arrFileNames)){
			foreach($arrFileNames as $thisFileName) {
				if(file_exists(DIRECTORY_EXPORT_FILES . $thisFileName)){
					$createCommand = ZIP_PATH.' a -tzip '.DIRECTORY_EXPORT_FILES . $zipFileName.'.zip '.DIRECTORY_EXPORT_FILES.$thisFileName.'';
					$arrExecResults[] = $createCommand;
					$arrExecResults[] = exec($createCommand, $error);
					if(!empty($error)) {
						$arrExecResults[] = array('1. ZIP: error', $error);
					}
					unlink(DIRECTORY_EXPORT_FILES . $thisFileName);
				}
			}
		}
	}
?>

<?php
	require_once('inc/headerHTML.inc.php');
	$thisTitle = "Nicht bestellende Shop-Kunden-Export nach PLZ gefiltert";
	$headerHTML = preg_replace("/{###TITLE###}/", strip_tags($thisTitle), $headerHTML);
	echo $headerHTML;

	require_once(FILE_MENUE_TOP);
?>
<div id="mainArea">
	<div id="mainContent">
		<?php require_once(FILE_MENUE_SIDEBAR); ?>
		<div id="contentArea">
			<div id="contentAreaElements">
				<h1><?php if(SHOW_PAGE_TITLE_ICONS) { echo '<img src="' . PATH_ICONS_MENUE_TITLES . 'export.png" alt="" />'; } ?> <?php echo $thisTitle; ?></h1>

				<div class="contentDisplay">
					<div class="adminEditArea">
					<form name="formExportDatas" method="post" action="<?php echo $_SERVER["REQUEST_URI"]; ?>" enctype="multipart/form-data">
						<fieldset>
						<table border="0" cellpadding="0" cellspacing="0" width="100%" class="">
							<tr>
								<td style="width:200px;"><b>Datei-Format:</b></td>
								<td>
									<select class="inputSelect_427" name="selectExportFormat">
										<option value=""> - Bitte w&auml;hlen - </option>
										<?php
											if(!empty($arrExportFileTypes)){
												asort($arrExportFileTypes);
												foreach($arrExportFileTypes as $thisFileType) {
													$selected = '';
													if($_POST["selectExportFormat"] == $thisFileType){
														$selected = ' selected="selected" ';
													}
													echo '<option value="' . $thisFileType . '" ' . $selected . ' >' . $thisFileType . '-Format</option>';
												}
											}
										?>
									</select>
								</td>
							</tr>
						</table>
						</fieldset>
						<div class="actionButtonsArea">
							<input type="submit" name="submitFormExportDatas" class="inputButton1" value="Export starten" />
						</div>
					</form>
					</div>

					<?php displayMessages(); ?>

					<?php
						if(file_exists($downloadFilePath)) {
							$downloadPath = str_replace(BASEPATH, "", $downloadFilePath);
							$thisDownloadLink = '<p class="dataTableContent"><b>DOWNLOAD-LINK: </b><img src="layout/icons/icon' . getFileType(basename($downloadPath)) . '.gif" height="16" width="16" alt="" />
													<a href="' . $_SERVER["REQUEST_URI"] . '?documentType=&downloadFile=' . basename($downloadPath) . '">' . basename($downloadPath) . '</a>
												</p>';
							echo $thisDownloadLink;
						}
						clearstatcache();

						if(!empty($arrExecResults)){
							dd('arrExecResults');
						}
					?>
				</div>
			</div>
		</div>
		<div class="clear"></div>
	</div>
</div>
<?php
	if($arrGetUserRights["editPersonnel"]) {
?>
<script language="javascript" type="text/javascript">
	$(document).ready(function() {
		$(function() {
			$( "#datepicker" ).datepicker();
			$.datepicker.setDefaults($.datepicker.regional[""]);
			$('#editPersonnelVacationsStart').datepicker($.datepicker.regional["de"]);
		});
		$(function() {
			$('#tabs').tabs();
		});
		setFormDutyFields(<?php echo $jsonFormDutyFields; ?>);
		colorRowMouseOver('.displayOrders tbody tr');
	});
</script>
<?php
	}
?>
<?php require_once('inc/footerHTML.inc.php'); ?>