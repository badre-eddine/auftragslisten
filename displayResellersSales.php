<?php
	require_once('inc/requires.inc.php');

	if(!$arrGetUserRights["displayDistribution"]) {
		header('location: ' . PAGE_EXIT_LOCATION);
		exit;
	}

	$jswindowMessage = '';
	$warningMessage = '';
	$errorMessage = '';
	$successMessage = '';
	$infoMessage = '';

	$dbConnection = new DB_Connection();
	$db_open = $dbConnection->db_connect();

	// BOF READ ORDER TYPES
		$arrMandatoriesDatas = getMandatories();
		if($_POST["searchMandatory"] == ''){
			$_POST["searchMandatory"] = MANDATOR;
		}
	// EOF READ ORDER TYPES

	// BOF READ PAYMENT TYPES
		$arrPaymentTypeDatas = getPaymentTypes();
	// EOF READ PAYMENT TYPES

	// BOF READ PAYMENT NAMES
	$arrPaymentStatutDatas = getPaymentStatut();
	// EOF READ PAYMENT NAMES

	$defaultORDER = "orderDocumentsDocumentDate";
	$defaultSORT = "ASC";

	if($_POST["searchInterval"] == ""){
		$_POST["searchInterval"] = "YEAR";
	}

	$todayYear = date("Y");
	if($_POST["searchYear"] == ""){
		$_POST["searchYear"] = $todayYear;
	}
?>
<?php
	require_once('inc/headerHTML.inc.php');
	$thisTitle = "Wiederverk&auml;ufer-Umsatz";

	$headerHTML = preg_replace("/{###TITLE###}/", strip_tags($thisTitle), $headerHTML);
	echo $headerHTML;

	require_once(FILE_MENUE_TOP);

	$sqlSortField = "";
	$sqlSortDirection = "";
	$sqlWhere = "";
?>

		<div id="menueSidebarToggleArea">
			<img src="layout/icons/iconToggle.png" class="buttonToggleSidebarMenue" width="10" height="10" alt="" title="Seitennavigation ein-/ausblenden" />
			<div id="menueSidebarToggleContent">
			<?php require_once(FILE_MENUE_SIDEBAR); ?>
			<div class="clear"></div>
			</div>
		</div>
		<div id="contentArea2">
			<a name="top"></a>
			<div id="contentAreaElements">
				<h1><?php if(SHOW_PAGE_TITLE_ICONS) { echo '<img src="' . PATH_ICONS_MENUE_TITLES . 'statistics.png" alt="" />'; } ?> <?php echo $thisTitle; ?></h1>

				<p class="infoArea">Angezeigt werden nur KZH-St&uuml;ckzahlen aus Rechnungen!</p>

				<div id="searchFilterArea">
					<form name="formSearchDatas" method="post" action="<?php echo $_SERVER["PHP_SELF"]; ?>">
					<table border="0" cellpadding="0" cellspacing="0" class="searchFilterContent">
						<tr>
							<td>
								<label for="searchInterval">Zeitraum:</label>
								<select name="searchInterval" id="searchInterval" class="inputField_130">
									<option value="WEEK" <?php if($_POST["searchInterval"] == 'WEEK'){ echo ' selected="selected" '; } ?> >pro Woche</option>
									<option value="MONTH" <?php if($_POST["searchInterval"] == 'MONTH'){ echo ' selected="selected" '; } ?> >pro Monat</option>
									<option value="QUARTER" <?php if($_POST["searchInterval"] == 'QUARTER'){ echo ' selected="selected" '; } ?> >pro Vierteljahr</option>
									<option value="YEAR" <?php if($_POST["searchInterval"] == 'YEAR'){ echo ' selected="selected" '; } ?> >pro Jahr</option>
								 </select>
							</td>

							<td>
								<label for="searchInterval">Jahr:</label>
								<select name="searchYear" id="searchYear" class="inputField_130">
									<!-- <option value="ALL" <?php if($_POST["searchYear"] == "ALL"){ echo ' selected="selected" '; } ?>>Alle Jahre</option> -->
									<?php
										for($i = $todayYear ; $i > 2012 ; $i--){
											$selected = '';
											if($i == $_POST["searchYear"]){
												$selected = ' selected="selected" ';
											}
											echo '<option value="' . $i . '" ' . $selected . ' >' . $i . '</option>';
										}
									?>
								</select>
							</td>

							<td>
								<label for="searchCustomerNumber">KNR:</label>
								<input type="text" name="searchCustomerNumber" id="searchCustomerNumber" class="inputField_70" value="<?php echo $_REQUEST["searchCustomerNumber"]; ?>"/>
							</td>

							<td>
								<label for="searchCustomerPaymentType">Kunden-Zahlart:</label>
								<select name="searchCustomerPaymentType" id="searchCustomerPaymentType" class="inputSelect_170">
									<option value=""> ALLE </option>
									<?php
										if(!empty($arrPaymentTypeDatas)) {
											foreach($arrPaymentTypeDatas as $thisKey => $thisValue) {
												$selected = '';
												if($thisKey == $_REQUEST["searchCustomerPaymentType"]) {
													$selected = ' selected="selected" ';
												}
												echo '
													<option value="' . $thisKey . '" '.$selected.' >' . htmlentities($arrPaymentTypeDatas[$thisKey]["paymentTypesName"]). '</option>
												';
											}
										}
									?>
								</select>
							</td>

							<td>
								<label for="searchCustomerPaymentStatut">Kunden-ZahlStatut:</label>
								<select name="searchCustomerPaymentStatut" id="searchCustomerPaymentStatut" class="inputSelect_170">
									<option value=""> ALLE </option>
									<?php
										if(!empty($arrPaymentStatutDatas)) {
											foreach($arrPaymentStatutDatas as $thisKey => $thisValue) {
												$selected = '';
												if($thisKey == $_REQUEST["searchCustomerPaymentStatut"]) {
													$selected = ' selected="selected" ';
												}
												echo '
													<option value="' . $thisKey . '" '.$selected.' >' . htmlentities($arrPaymentStatutDatas[$thisKey]["paymentStatusTypesName"]). '</option>
												';
											}
										}
									?>
								</select>
							</td>

							<td>
								<input type="hidden" name="editID" id="editID" value="" />
								<input type="submit" name="submitSearch" class="inputButton0" value="Anzeigen" />
							</td>
						</tr>
					</table>
					<input type="hidden" name="submitSearchForm" id="submitSearchForm" value="1" />
					</form>
				</div>

				<div class="contentDisplay">
					<?php

						// EOF CONSTRUCT WHERE CLAUSE DEPENDING ON SELECTED DATA
						$arrWhereInvoices = array();
						$dateFieldInvoices = "";
						$whereInvoices = "";
						$orderInvoices = "";

						if($_POST["searchInterval"] == "WEEK"){
							$dateFieldInvoices = "
								CONCAT(
									IF(DATE_FORMAT(`" . TABLE_ORDER_INVOICES . "`.`orderDocumentsDocumentDate`, '%m') = '12' AND DATE_FORMAT(`" . TABLE_ORDER_INVOICES . "`.`orderDocumentsDocumentDate`, '%v') = '01', (DATE_FORMAT(`" . TABLE_ORDER_INVOICES . "`.`orderDocumentsDocumentDate`, '%Y') + 1),  DATE_FORMAT(`" . TABLE_ORDER_INVOICES . "`.`orderDocumentsDocumentDate`, '%Y')),
									'#',
									DATE_FORMAT(`" . TABLE_ORDER_INVOICES . "`.`orderDocumentsDocumentDate`, '%v')
								) AS `interval`
							";
							$orderInvoices = " ORDER BY `customersFirmenname` ";
						}
						else if($_POST["searchInterval"] == "MONTH"){
							$dateFieldInvoices = "
								CONCAT(
									DATE_FORMAT(`" . TABLE_ORDER_INVOICES . "`.`orderDocumentsDocumentDate`, '%Y'),
									'#',
									DATE_FORMAT(`" . TABLE_ORDER_INVOICES . "`.`orderDocumentsDocumentDate`, '%m')
								) AS `interval`
							";
							$orderInvoices = " ORDER BY `customersFirmenname` ";
						}
						else if($_POST["searchInterval"] == "QUARTER"){
							$dateFieldInvoices = "
								CONCAT(
									DATE_FORMAT(`" . TABLE_ORDER_INVOICES . "`.`orderDocumentsDocumentDate`, '%Y'),
									'#',
									IF(DATE_FORMAT(`" . TABLE_ORDER_INVOICES . "`.`orderDocumentsDocumentDate`, '%m') = '01' OR DATE_FORMAT(`" . TABLE_ORDER_INVOICES . "`.`orderDocumentsDocumentDate`, '%m') = '02' OR DATE_FORMAT(`" . TABLE_ORDER_INVOICES . "`.`orderDocumentsDocumentDate`, '%m') = '03', '1. Quartal',
										IF(DATE_FORMAT(`" . TABLE_ORDER_INVOICES . "`.`orderDocumentsDocumentDate`, '%m') = '04' OR DATE_FORMAT(`" . TABLE_ORDER_INVOICES . "`.`orderDocumentsDocumentDate`, '%m') = '05' OR DATE_FORMAT(`" . TABLE_ORDER_INVOICES . "`.`orderDocumentsDocumentDate`, '%m') = '06', '2. Quartal',
											IF(DATE_FORMAT(`" . TABLE_ORDER_INVOICES . "`.`orderDocumentsDocumentDate`, '%m') = '07' OR DATE_FORMAT(`" . TABLE_ORDER_INVOICES . "`.`orderDocumentsDocumentDate`, '%m') = '08' OR DATE_FORMAT(`" . TABLE_ORDER_INVOICES . "`.`orderDocumentsDocumentDate`, '%m') = '09', '3. Quartal',
												IF(DATE_FORMAT(`" . TABLE_ORDER_INVOICES . "`.`orderDocumentsDocumentDate`, '%m') = '10' OR DATE_FORMAT(`" . TABLE_ORDER_INVOICES . "`.`orderDocumentsDocumentDate`, '%m') = '11' OR DATE_FORMAT(`" . TABLE_ORDER_INVOICES . "`.`orderDocumentsDocumentDate`, '%m') = '12', '4. Quartal',
													''
												)
											)
										)
									)
								) AS `interval`
							";
							$orderInvoices = " ORDER BY `customersFirmenname` ";
						}
						else if($_POST["searchInterval"] == "YEAR"){
							$dateFieldInvoices = "
								CONCAT(
									DATE_FORMAT(`" . TABLE_ORDER_INVOICES . "`.`orderDocumentsDocumentDate`, '%Y'),
									'#',
									DATE_FORMAT(`" . TABLE_ORDER_INVOICES . "`.`orderDocumentsDocumentDate`, '%Y')
								) AS `interval`
							";


							$orderInvoices = " ORDER BY `sumOrderDocumentDetailProductQuantity` DESC, `customersFirmenname` ASC";
						}

						if($_POST["searchYear"] != "" && $_POST["searchYear"] != "ALL"){
							$whereInvoices = "
								AND DATE_FORMAT(`" . TABLE_ORDER_INVOICES . "`.`orderDocumentsDocumentDate`, '%Y') = '" . $_POST["searchYear"] . "'
							";
						}

						if($_REQUEST["searchCustomerNumber"] != "") {
							$whereInvoices .= " AND (
										`" . TABLE_CUSTOMERS . "`.`customersKundennummer` = '" . $_REQUEST["searchCustomerNumber"] . "'
									)
								";
						}
						else if($_REQUEST["searchCustomerPaymentType"] != "") {
							$whereInvoices .= " AND `" . TABLE_CUSTOMERS . "`.`customersBezahlart` = '" . $_REQUEST["searchCustomerPaymentType"] . "'";
						}

						else if($_REQUEST["searchCustomerPaymentStatut"] != "") {
							$whereInvoices .= " AND `" . TABLE_PAYMENT_STATUS_TYPES . "`.`paymentStatusTypesName` = '" . $_REQUEST["searchCustomerPaymentStatut"] . "'";
						}
	
					$orderInvoices = " ORDER BY `sumOrderDocumentDetailProductQuantity` DESC, `customersFirmenname` ASC";
					// EOF CONSTRUCT WHERE CLAUSE DEPENDING ON SELECTED DATA

					$arrIntervals = array();
					$arrDatas = array();

					// BOF GET INVOICES DATA
						$sqlInvoicesSingle = "
									SELECT
										" . $dateFieldInvoices . ",
										`" . TABLE_CUSTOMER_TYPES . "`.`customerTypesID`,
										`" . TABLE_CUSTOMER_TYPES . "`.`customerTypesName`,
										`" . TABLE_CUSTOMER_TYPES . "`.`customerTypesShortName`,

										`" . TABLE_CUSTOMERS . "`.`customersID`,
										`" . TABLE_CUSTOMERS . "`.`customersTyp`,
										`" . TABLE_CUSTOMERS . "`.`customersKundennummer`,
										`" . TABLE_CUSTOMERS . "`.`customersFirmenname`,

										`" . TABLE_CUSTOMERS . "`.`customersBezahlart`,
									

										/*
										COUNT(DISTINCT `" . TABLE_ORDER_INVOICES . "`.`orderDocumentsID`) AS `countOrderDocumentsID`,
										`" . TABLE_ORDER_INVOICES . "`.`orderDocumentsDocumentDate`,
										IF(`" . TABLE_ORDER_INVOICES . "`.`orderDocumentsTotalPrice` IS NULL, 0, `" . TABLE_ORDER_INVOICES . "`.`orderDocumentsTotalPrice`) AS `orderDocumentsTotalPrice`,
										*/

										`" . TABLE_ORDER_INVOICES . "`.`orderDocumentsIsCollectiveInvoice`,

										'' AS `relatedDocuments_AB`,

										/*
										GROUP_CONCAT(DISTINCT IF(`" . TABLE_ORDER_INVOICES . "`.`orderDocumentsNumber` IS NULL, '', `" . TABLE_ORDER_INVOICES . "`.`orderDocumentsNumber`) SEPARATOR ';') AS `sumOrderDocumentsNumbers`,
										*/

										SUM(IF(`" . TABLE_ORDER_INVOICES_DETAILS . "`.`orderDocumentDetailProductQuantity` IS NULL, 0, `" . TABLE_ORDER_INVOICES_DETAILS . "`.`orderDocumentDetailProductQuantity`)) AS `sumOrderDocumentDetailProductQuantity`,
										SUM(IF(`" . TABLE_ORDER_INVOICES_DETAILS . "`.`orderDocumentDetailProductSinglePrice` IS NULL, 0, `" . TABLE_ORDER_INVOICES_DETAILS . "`.`orderDocumentDetailProductSinglePrice`)) AS `sumOrderDocumentDetailProductSinglePrice`,
										SUM(IF(`" . TABLE_ORDER_INVOICES_DETAILS . "`.`orderDocumentDetailProductTotalPrice` IS NULL, 0, `" . TABLE_ORDER_INVOICES_DETAILS . "`.`orderDocumentDetailProductTotalPrice`)) AS `sumOrderDocumentDetailProductTotalPrice`,

										'RE' AS `dataType`

									FROM `" . TABLE_CUSTOMER_TYPES . "`

									LEFT JOIN `" . TABLE_CUSTOMERS . "`
									ON(`" . TABLE_CUSTOMER_TYPES . "`.`customerTypesID` = `" . TABLE_CUSTOMERS . "`.`customersTyp`)

									LEFT JOIN `" . TABLE_ORDER_INVOICES . "`
									ON(
										/*
										`" . TABLE_CUSTOMERS . "`.`customersKundennummer` = `" . TABLE_ORDER_INVOICES . "`.`orderDocumentsCustomerNumber`
										OR
										*/
										`" . TABLE_CUSTOMERS . "`.`customersKundennummer` = `" . TABLE_ORDER_INVOICES . "`.`orderDocumentsAddressCompanyCustomerNumber`
										/*
										OR
										`" . TABLE_CUSTOMERS . "`.`customersID` = `" . TABLE_ORDER_INVOICES . "`.`orderDocumentsSalesman`
										*/
									)

									LEFT JOIN `" . TABLE_ORDER_INVOICES_DETAILS . "`
									ON(`"
									 . TABLE_ORDER_INVOICES . "`.`orderDocumentsID` = `" . TABLE_ORDER_INVOICES_DETAILS . "`.`orderDocumentDetailDocumentID`)

									WHERE 1
									" . $whereInvoices . "
										AND (
											`" . TABLE_CUSTOMER_TYPES . "`.`customerTypesID` = 2
											OR
											`" . TABLE_CUSTOMER_TYPES . "`.`customerTypesID` = 3
											OR
											`" . TABLE_CUSTOMER_TYPES . "`.`customerTypesID` = 4
											OR
											`" . TABLE_CUSTOMER_TYPES . "`.`customerTypesID` = 11
											
										)
										AND `" . TABLE_ORDER_INVOICES_DETAILS . "`.`orderDocumentDetailPosType` = 'product'
										 
										#AND SUBSTRING(`" . TABLE_ORDER_INVOICES_DETAILS . "`.`orderDocumentDetailProductKategorieID`, 1, 3) = '001'
										 
										AND `" . TABLE_ORDER_INVOICES . "`.`orderDocumentsIsCollectiveInvoice` != '1'

										GROUP BY CONCAT(`" . TABLE_CUSTOMERS . "`.`customersKundennummer`, '#', `interval`, `" . TABLE_ORDER_INVOICES . "`.`orderDocumentsNumber`)

							";
							//echo "<hr>".$sqlInvoicesSingle."<hr>";
							//die($sqlInvoicesSingle);
					// EOF GET INVOICES DATA

					// BOF GET ABs FROM COLLECTIVE INVOICES
						$sqlInvoicesCollective = "
								SELECT
									" . $dateFieldInvoices . ",
									`" . TABLE_CUSTOMER_TYPES . "`.`customerTypesID`,
									`" . TABLE_CUSTOMER_TYPES . "`.`customerTypesName`,
									`" . TABLE_CUSTOMER_TYPES . "`.`customerTypesShortName`,

									`" . TABLE_CUSTOMERS . "`.`customersID`,
									`" . TABLE_CUSTOMERS . "`.`customersTyp`,
									`" . TABLE_CUSTOMERS . "`.`customersKundennummer`,
									`" . TABLE_CUSTOMERS . "`.`customersFirmenname`,

									`" . TABLE_CUSTOMERS . "`.`customersBezahlart`,
									

									/*
									COUNT(DISTINCT `" . TABLE_ORDER_INVOICES . "`.`orderDocumentsID`) AS `countOrderDocumentsID`,
									`" . TABLE_ORDER_INVOICES . "`.`orderDocumentsDocumentDate`,
									IF(`" . TABLE_ORDER_INVOICES . "`.`orderDocumentsTotalPrice` IS NULL, 0, `" . TABLE_ORDER_INVOICES . "`.`orderDocumentsTotalPrice`) AS `orderDocumentsTotalPrice`,
									*/

									`" . TABLE_ORDER_INVOICES . "`.`orderDocumentsIsCollectiveInvoice`,

									`" . TABLE_RELATED_DOCUMENTS . "`.`relatedDocuments_AB`,

									/*
									GROUP_CONCAT(DISTINCT IF(`" . TABLE_ORDER_INVOICES . "`.`orderDocumentsNumber` IS NULL, '', `" . TABLE_ORDER_INVOICES . "`.`orderDocumentsNumber`) SEPARATOR ';') AS `sumOrderDocumentsNumbers`,
									*/

									SUM(IF(`" . TABLE_ORDER_CONFIRMATIONS_DETAILS . "`.`orderDocumentDetailProductQuantity` IS NULL, 0, `" . TABLE_ORDER_CONFIRMATIONS_DETAILS . "`.`orderDocumentDetailProductQuantity`)) AS `sumOrderDocumentDetailProductQuantity`,
									SUM(IF(`" . TABLE_ORDER_CONFIRMATIONS_DETAILS . "`.`orderDocumentDetailProductSinglePrice` IS NULL, 0, `" . TABLE_ORDER_CONFIRMATIONS_DETAILS . "`.`orderDocumentDetailProductSinglePrice`)) AS `sumOrderDocumentDetailProductSinglePrice`,
									SUM(IF(`" . TABLE_ORDER_CONFIRMATIONS_DETAILS . "`.`orderDocumentDetailProductTotalPrice` IS NULL, 0, `" . TABLE_ORDER_CONFIRMATIONS_DETAILS . "`.`orderDocumentDetailProductTotalPrice`)) AS `sumOrderDocumentDetailProductTotalPrice`,

									'AB' AS `dataType`

								FROM `" . TABLE_CUSTOMER_TYPES . "`

								LEFT JOIN `" . TABLE_CUSTOMERS . "`
								ON(`" . TABLE_CUSTOMER_TYPES . "`.`customerTypesID` = `" . TABLE_CUSTOMERS . "`.`customersTyp`)

								LEFT JOIN `" . TABLE_ORDER_INVOICES . "`
								ON(
									/*
									`" . TABLE_CUSTOMERS . "`.`customersKundennummer` = `" . TABLE_ORDER_INVOICES . "`.`orderDocumentsCustomerNumber`
									OR
									*/
									`" . TABLE_CUSTOMERS . "`.`customersKundennummer` = `" . TABLE_ORDER_INVOICES . "`.`orderDocumentsAddressCompanyCustomerNumber`
									/*
									OR
									`" . TABLE_CUSTOMERS . "`.`customersID` = `" . TABLE_ORDER_INVOICES . "`.`orderDocumentsSalesman`
									*/
								)

								LEFT JOIN `" . TABLE_RELATED_DOCUMENTS . "`
								ON(`" . TABLE_ORDER_INVOICES . "`.`orderDocumentsNumber` = `" . TABLE_RELATED_DOCUMENTS . "`.`relatedDocuments_RE`)

								LEFT JOIN `" . TABLE_ORDER_CONFIRMATIONS . "`
								ON(`" . TABLE_RELATED_DOCUMENTS . "`.`relatedDocuments_AB` = `" . TABLE_ORDER_CONFIRMATIONS . "`.`orderDocumentsNumber`)

								LEFT JOIN `" . TABLE_ORDER_CONFIRMATIONS_DETAILS . "`
								ON(`" . TABLE_ORDER_CONFIRMATIONS . "`.`orderDocumentsID` = `" . TABLE_ORDER_CONFIRMATIONS_DETAILS . "`.`orderDocumentDetailDocumentID`)

								WHERE 1
									" . $whereInvoices . "
									AND (
										`" . TABLE_CUSTOMER_TYPES . "`.`customerTypesID` = 2
										OR
										`" . TABLE_CUSTOMER_TYPES . "`.`customerTypesID` = 3
										OR
										`" . TABLE_CUSTOMER_TYPES . "`.`customerTypesID` = 4
										OR
										`" . TABLE_CUSTOMER_TYPES . "`.`customerTypesID` = 11
									)
									AND `" . TABLE_ORDER_CONFIRMATIONS_DETAILS . "`.`orderDocumentDetailPosType` = 'product'
									AND SUBSTRING(`" . TABLE_ORDER_CONFIRMATIONS_DETAILS . "`.`orderDocumentDetailProductKategorieID`, 1, 3) = '001'
									AND `" . TABLE_ORDER_INVOICES . "`.`orderDocumentsIsCollectiveInvoice` = '1'

									GROUP BY CONCAT(`" . TABLE_CUSTOMERS . "`.`customersKundennummer`, '#', `interval`, `" . TABLE_ORDER_INVOICES . "`.`orderDocumentsNumber`)
						";

					// EOF GET ABs FROm COLLECTIVE INVOICES

					// BOF GET INVOICES DATA DEPENDING ON KZH
						$sqlInvoices = $sqlInvoicesSingle . " UNION " . $sqlInvoicesCollective;

						$sqlInvoices = "
								SELECT

									`tempTable`.`interval`,
									`tempTable`.`customerTypesID`,
									`tempTable`.`customerTypesName`,
									`tempTable`.`customerTypesShortName`,

									`tempTable`.`customersID`,
									`tempTable`.`customersTyp`,
									`tempTable`.`customersKundennummer`,
									`tempTable`.`customersFirmenname`,

									`tempTable`.`customersBezahlart`,
									

									/*
									SUM(`tempTable`.`countOrderDocumentsID`) AS `countOrderDocumentsID`,
									`tempTable`.`orderDocumentsDocumentDate`,
									SUM(`tempTable`.`orderDocumentsTotalPrice`) AS `orderDocumentsTotalPrice`,
									*/

									`tempTable`.`orderDocumentsIsCollectiveInvoice`,

									`tempTable`.`relatedDocuments_AB`,

									/*
									GROUP_CONCAT(DISTINCT `tempTable`.`sumOrderDocumentsNumbers` SEPARATOR ';') AS `sumOrderDocumentsNumbers`,
									*/

									SUM(`tempTable`.`sumOrderDocumentDetailProductQuantity`) AS `sumOrderDocumentDetailProductQuantity`,
									SUM(`tempTable`.`sumOrderDocumentDetailProductSinglePrice`) AS `sumOrderDocumentDetailProductSinglePrice`,
									SUM(`tempTable`.`sumOrderDocumentDetailProductTotalPrice`) AS `sumOrderDocumentDetailProductTotalPrice`,

									'RE+AB' AS `dataType`

								FROM (
								" . $sqlInvoices . "
								) AS `tempTable`

								GROUP BY CONCAT(`tempTable`.`customersKundennummer`, '#', `interval`)
							";


						$sqlInvoices .= "" . $orderInvoices . "";
	# dd('sqlInvoices');  
						$rsInvoices = $dbConnection->db_query($sqlInvoices);

						$countTotalRowsInvoices = $dbConnection->db_getMysqlNumRows($rsInvoices);

						$arrSortCustomers = array();
						$countItem = 0;

						while($ds = mysqli_fetch_assoc($rsInvoices)) {
							
							if(!in_array($ds["customersKundennummer"], $arrSortCustomers)){
								$arrSortCustomers[$countItem] = $ds["customersKundennummer"];
								$countItem++;
							}

							foreach(array_keys($ds) as $field){
								if(preg_match("/^customer/", $field)){
									$arrCustomerDatas[$ds["customersKundennummer"]][$field] = $ds[$field];
								}
								else {
									$arrInvoiceDatas[$ds["customersKundennummer"]][$ds["interval"]][$field] = $ds[$field];
								}
							}
						}
						 
					// EOF GET INVOICES DATA DEPENDING ON KZH

					// BOF GET TOTAL SUM OF ALL INVOICES
						$sqlInvoicesAll = "
								SELECT
										" . $dateFieldInvoices . ",
										`" . TABLE_CUSTOMER_TYPES . "`.`customerTypesID`,
										`" . TABLE_CUSTOMER_TYPES . "`.`customerTypesName`,
										`" . TABLE_CUSTOMER_TYPES . "`.`customerTypesShortName`,

										`" . TABLE_CUSTOMERS . "`.`customersID`,
										`" . TABLE_CUSTOMERS . "`.`customersTyp`,
										`" . TABLE_CUSTOMERS . "`.`customersKundennummer`,
										`" . TABLE_CUSTOMERS . "`.`customersFirmenname`,

										COUNT(DISTINCT `" . TABLE_ORDER_INVOICES . "`.`orderDocumentsID`) AS `countOrderDocumentsID`,

										SUM(IF(`" . TABLE_ORDER_INVOICES . "`.`orderDocumentsTotalPrice` IS NULL, 0, `" . TABLE_ORDER_INVOICES . "`.`orderDocumentsTotalPrice`)) AS `orderDocumentsTotalPrice`

									FROM `" . TABLE_CUSTOMER_TYPES . "`

									LEFT JOIN `" . TABLE_CUSTOMERS . "`
									ON(`" . TABLE_CUSTOMER_TYPES . "`.`customerTypesID` = `" . TABLE_CUSTOMERS . "`.`customersTyp`)

									LEFT JOIN `" . TABLE_ORDER_INVOICES . "`
									ON(
										/*
										`" . TABLE_CUSTOMERS . "`.`customersKundennummer` = `" . TABLE_ORDER_INVOICES . "`.`orderDocumentsCustomerNumber`
										OR
										*/
										`" . TABLE_CUSTOMERS . "`.`customersKundennummer` = `" . TABLE_ORDER_INVOICES . "`.`orderDocumentsAddressCompanyCustomerNumber`
									)
									WHERE 1
											" . $whereInvoices . "
											AND (
												`" . TABLE_CUSTOMER_TYPES . "`.`customerTypesID` = 2
												OR
												`" . TABLE_CUSTOMER_TYPES . "`.`customerTypesID` = 3
												OR
												`" . TABLE_CUSTOMER_TYPES . "`.`customerTypesID` = 4
												OR
												`" . TABLE_CUSTOMER_TYPES . "`.`customerTypesID` = 11
											)

											GROUP BY CONCAT(`" . TABLE_CUSTOMERS . "`.`customersKundennummer`, '#', `interval`)
							";

					 
						$rsInvoicesAll = $dbConnection->db_query($sqlInvoicesAll);
					 
 
						$arrInvoiceDatasAll = array();

						while($ds = mysqli_fetch_assoc($rsInvoicesAll)) {
							
							if(!in_array($ds["customersKundennummer"], $arrSortCustomers)){
								$arrSortCustomers[$countItem] = $ds["customersKundennummer"];

								foreach(array_keys($ds) as $field){
									if(preg_match("/^customer/", $field)){
										$arrCustomerDatas[$ds["customersKundennummer"]][$field] = $ds[$field];
									}
								}
								$countItem++;
							}
							foreach(array_keys($ds) as $field){
								$arrInvoiceDatasAll[$ds["customersKundennummer"]][$ds["interval"]][$field] = $ds[$field];
							}
						}
					// EOF GET TOTAL SUM OF ALL INVOICES

					// BOF SET INTERVALS
						$arrIntervals = array();

						if($_POST["searchInterval"] == "WEEK"){
							for($i = 1 ; $i < 53; $i++){
								$arrIntervals[$i] = $_POST["searchYear"] . '#' . str_repeat("0", (2 - strlen($i))) . $i;
							}
						}
						else if($_POST["searchInterval"] == "MONTH"){
							for($i = 1 ; $i < 13; $i++){
								$arrIntervals[$i] = $_POST["searchYear"] . '#' . str_repeat("0", (2 - strlen($i))) . $i;
							}
						}
						else if($_POST["searchInterval"] == "QUARTER"){
							for($i = 1 ; $i < 5; $i++){
								$arrIntervals[$i] = $_POST["searchYear"] . '#' . $i . '. Quartal';
							}
						}
						else if($_POST["searchInterval"] == "YEAR"){
							if($_POST["searchYear"] != "" && $_POST["searchYear"] != "ALL"){
								$arrIntervals[1] = $_POST["searchYear"] . '#' . $_POST["searchYear"];
							}
							else {
								for($i = $todayYear ; $i > 2012 ; $i--){
									$arrIntervals[$i] = $i . '#' . $i;
								}
							}
						}
					// EOF SET INTERVALS
				 
						if(!empty($arrInvoiceDatas)){
							echo '<table width="100%" cellpadding="0" cellspacing="0" class="displayOrders">';

							echo '<thead>';

							echo '<tr>';
							echo '<th style="width:45px"rowspan="2">#</th>';
							echo '<th style="width:80px" rowspan="2">Firma</th>';
							echo '<th style="width:40px" rowspan="2">KNR</th>';
							echo '<th style="width:40px" rowspan="2">Zahlart</th>';
							
							echo '<th class="spacer" rowspan="2"></th>';
							if(!empty($arrIntervals)){
								foreach($arrIntervals as $thisInterval){
									$arrTemp = explode('#', $thisInterval);
									if($_POST["searchInterval"] == "WEEK"){
										$thisIntervalHeader = $arrTemp[0] . ' - KW ' . $arrTemp[1];
									}
									else if($_POST["searchInterval"] == "MONTH"){
										$thisIntervalHeader = $arrTemp[0] . ' - ' . $arrTemp[1];
									}
									else if($_POST["searchInterval"] == "QUARTER"){
										$thisIntervalHeader = $arrTemp[0] . ' - ' . $arrTemp[1];
									}
									else if($_POST["searchInterval"] == "YEAR"){
										$thisIntervalHeader = $arrTemp[0];
									}
									echo '<th style="width:200px;text-align:center;" colspan="3">' . $thisIntervalHeader . '</th>';
									echo '<th class="spacer" rowspan="2"></th>';
								}
							}

							echo '<th style="width:200px" colspan="3">Gesamt</th>';
							echo '</tr>';

							echo '<tr>';

							if(!empty($arrIntervals)){
								foreach($arrIntervals as $thisInterval){
									echo '<th>KZH St&uuml;ck</th>';
									echo '<th>KZH Euro</th>';
									echo '<th>RE-Summe</th>';
								}
							}

							echo '<th>KZH St&uuml;ck</th>';
							echo '<th>KZH Euro</th>';
							echo '<th>RE-Summe</th>';
							echo '</tr>';

							echo '</thead>';

							echo '<tbody>';

							$countRow = 0;
							$thisMarker = "";

							$totalPrice = 0;
							$totalInvoicePrice = 0;
							$totalQuantity = 0;
							$totalCountInvoices = 0;

							#foreach($arrCustomerDatas as $thisCustomerDatasKey => $thisCustomerDatasValue) {
							foreach($arrSortCustomers as $thisCustomerDatasKey) {
								$rowClass = 'row0';
								if($countRow%2 == 0) {
									$rowClass = 'row1';
								}

								if(in_array($arrCustomerDatas[$thisCustomerDatasKey]["customersBezahlart"], array(2, 3))){
									$rowClass = 'row9';
								}

								echo '<tr style="border-bottom: 2px solid #003300;" class="'.$rowClass.'">';

								echo '<td style="text-align:right;"><b>';
								echo ($countRow + 1);
								echo '.</b></td>';

								echo '<td style="font-weight:bold;white-space:nowrap;">';
								echo $arrCustomerDatas[$thisCustomerDatasKey]["customersFirmenname"];
								echo '</td>';

								echo '<td>';
								echo '<b><a href="' . PAGE_EDIT_CUSTOMER. '?editCustomerNumber=' . $arrCustomerDatas[$thisCustomerDatasKey]["customersKundennummer"] . '">';
								echo $arrCustomerDatas[$thisCustomerDatasKey]["customersKundennummer"];
								echo '</a></b>';
								echo '</td>';

								echo '<td style="white-space:nowrap;">';
								echo $arrPaymentTypeDatas[$arrCustomerDatas[$thisCustomerDatasKey]["customersBezahlart"]]["paymentTypesName"];
								echo '</td>';

								echo '<td class="spacer"></td>';

								$thisIntervalTotalPrice = 0;
								$thisIntervalTotalInvoicePrice = 0;
								$thisIntervalTotalQuantity = 0;
								if(!empty($arrIntervals)){
									foreach($arrIntervals as $thisInterval){

										 
										$totalCountInvoices += $arrInvoiceDatasAll[$thisCustomerDatasKey][$thisInterval]["countOrderDocumentsID"];
										echo '<td style="text-align:right;white-space:nowrap;">';

										$thisSumOrderDocumentDetailProductQuantity = $arrInvoiceDatas[$thisCustomerDatasKey][$thisInterval]["sumOrderDocumentDetailProductQuantity"];
										if($thisSumOrderDocumentDetailProductQuantity == '') {
											$thisSumOrderDocumentDetailProductQuantity = 0;
										}
										$thisIntervalTotalQuantity += $thisSumOrderDocumentDetailProductQuantity;
										if($thisSumOrderDocumentDetailProductQuantity > 0) {
											echo number_format($thisSumOrderDocumentDetailProductQuantity, 0, ',', '.')  . ' KZH';
										}
										echo '</td>';

										echo '<td style="text-align:right;white-space:nowrap;">';
										$thisSumOrderDocumentDetailProductTotalPrice = $arrInvoiceDatas[$thisCustomerDatasKey][$thisInterval]["sumOrderDocumentDetailProductTotalPrice"];
										if($thisSumOrderDocumentDetailProductTotalPrice == '') {
											$thisSumOrderDocumentDetailProductTotalPrice = 0;
										}
										$thisIntervalTotalPrice += $thisSumOrderDocumentDetailProductTotalPrice;
										if($thisSumOrderDocumentDetailProductTotalPrice > 0) {
											echo number_format($thisSumOrderDocumentDetailProductTotalPrice, 2, ',', '.') . ' EUR';
										}

										#echo '<br />' . $arrInvoiceDatas[$thisCustomerDatasKey][$thisInterval]["sumOrderDocumentsNumbers"] . '';
										echo '</td>';

										echo '<td style="text-align:right;white-space:nowrap;">';
										$thisSumOrderDocumentsTotalPrice = $arrInvoiceDatasAll[$thisCustomerDatasKey][$thisInterval]["orderDocumentsTotalPrice"];
										if($thisSumOrderDocumentsTotalPrice == '') {
											$thisSumOrderDocumentsTotalPrice = 0;
										}
										$thisIntervalTotalInvoicePrice += $thisSumOrderDocumentsTotalPrice;
										if($thisSumOrderDocumentsTotalPrice > 0) {
											echo number_format($thisSumOrderDocumentsTotalPrice, 2, ',', '.') . ' EUR';
										}
										echo '</td>';

										echo '<td class="spacer"></td>';
									}
								}

								$totalQuantity += $thisIntervalTotalQuantity;
								$totalPrice += $thisIntervalTotalPrice;
								$totalInvoicePrice += $thisIntervalTotalInvoicePrice;

								echo '<td style="text-align:right;white-space:nowrap;background-color:#FEFFAF;">';
								if($thisIntervalTotalQuantity > 0){
									echo number_format($thisIntervalTotalQuantity, 0, ',', '.')  . ' KZH';
								}
								echo '</td>';

								echo '<td style="text-align:right;white-space:nowrap;background-color:#FEFFAF;">';
								if($thisIntervalTotalPrice > 0){
									echo number_format($thisIntervalTotalPrice, 2, ',', '.') . ' EUR';
								}
								echo '</td>';

								echo '<td style="text-align:right;white-space:nowrap;background-color:#FEFFAF;">';
								if($thisIntervalTotalInvoicePrice > 0){
									echo number_format($thisIntervalTotalInvoicePrice, 2, ',', '.') . ' EUR';
								}
								echo '</td>';

								echo '</tr>';
								$countRow++;
							}
							echo '<tr><td colspan="' . (8 + (count($arrIntervals) * 4)) . '"><hr /></td></tr>';

							echo '<tr>';

							echo '<td>';
							echo '<b>Insgesamt:</b>';
							echo '</td>';

							echo '<td style="white-space:nowrap;">';
							echo '<b>' . $countRow . ' Wiederverk&auml;ufer</b>';
							echo '</td>';

							echo '<td colspan="' . (3 + (count($arrIntervals) * 4)) . '">';
							echo '<b>' . $totalCountInvoices . ' Rechnungen' . '</b>';
							echo '</td>';

							echo '<td style="font-weight:bold;text-align:right;white-space:nowrap;background-color:#FEFFAF;">';
							if($totalQuantity > 0){
								echo number_format($totalQuantity, 0, ',', '.')  . ' KZH';
							}
							echo '</td>';

							echo '<td style="font-weight:bold;text-align:right;white-space:nowrap;background-color:#FEFFAF;">';
							if($totalPrice > 0){
								echo number_format($totalPrice, 2, ',', '.') . ' EUR';
							}
							echo '</td>';

							echo '<td style="font-weight:bold;text-align:right;white-space:nowrap;background-color:#FEFFAF;">';
							if($totalInvoicePrice > 0){
								echo number_format($totalInvoicePrice, 2, ',', '.') . ' EUR';
							}
							echo '</td>';

							echo '</tr>';

							echo '</tbody>';

							echo '</table>';
						}
						else {
							$infoMessage .= ' Es liegen keine Daten vor!' . '<br />';
						}
						displayMessages();
					?>
					<?php displayMessages(); ?>
				</div>

			</div>
		</div>

<script language="javascript" type="text/javascript">
	$(document).ready(function() {
		$(function() {
			colorRowMouseOver('.displayOrders tbody tr');
			$('.buttonShopOrderInfo').css('cursor', 'pointer');
			$('.buttonShopOrderInfo').click(function () {
				loadOrderDetails($(this), '<?php echo BASEPATH; ?>', 'Details der Bestellung');
			});
		});

		$('.buttonToggleSidebarMenue').click(function() {
			$('#menueSidebarToggleContent').toggle();
		});

		$('.blink').parent().parent().parent('tr').css('background-color', '#FFBFC8 !important');
		$('.blink').parent().parent().parent().find('td').css('background-color', '#FFBFC8 !important');

		$(function() {
			$('#tabs').tabs();
		});
	});
</script>
<?php require_once('inc/footerHTML.inc.php'); ?>