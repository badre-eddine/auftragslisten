<?php

	// TIMEZONE / PHP_INI
	if(!defined("SET_DATE_DEFAULT_TIMEZONE")) { DEFINE('SET_DATE_DEFAULT_TIMEZONE', 'Europe/Berlin'); }
	if(!defined("LC_ALL")) { DEFINE('LC_ALL', '"de_DE@euro", "de_DE", "de", "ge"'); }

	// INI
	if(!defined("TRACK_ERRORS")) { DEFINE('TRACK_ERRORS', '1'); }
	if(!defined("AUTO_DETECT_LINE_ENDINGS")) { DEFINE('AUTO_DETECT_LINE_ENDINGS', false); }
	if(!defined("DEFAULT_SOCKET_TIMEOUT")) { DEFINE('DEFAULT_SOCKET_TIMEOUT', '7200'); }
	if(!defined("MEMORY_LIMIT")) { DEFINE('MEMORY_LIMIT', '256M'); }
	if(!defined("SENDMAIL_FROM")) { DEFINE('SENDMAIL_FROM', ''); }
	if(!defined("SESSION.GC_MAXLIFETIME")) { DEFINE('SESSION.GC_MAXLIFETIME', '7200'); }
	if(!defined("SMTP")) { DEFINE('SMTP', ''); }
	if(!defined("SMTP_PORT")) { DEFINE('SMTP_PORT', ''); }
	if(!defined("ZLIB.OUTPUT_COMPRESSION")) { DEFINE('ZLIB.OUTPUT_COMPRESSION', '5'); }
	if(!defined("SET_TIME_LIMIT")) { DEFINE('SET_TIME_LIMIT', '0'); }
	if(!defined("POST_MAX_SIZE")) { DEFINE('POST_MAX_SIZE', '5000M'); }
	if(!defined("UPLOAD_MAX_FILESIZE")) { DEFINE('UPLOAD_MAX_FILESIZE', '5000M'); }
	if(!defined("ERROR_REPORTING")) { DEFINE('ERROR_REPORTING', 0); }

	date_default_timezone_set(SET_DATE_DEFAULT_TIMEZONE);
	setlocale(LC_ALL, constant("LC_ALL"));
	setlocale(LC_TIME, constant("LC_ALL"));
	ini_set('memory_limit', MEMORY_LIMIT);
	set_time_limit(SET_TIME_LIMIT);
	ini_set('post_max_size', POST_MAX_SIZE);
	ini_set('upload_max_filesize', UPLOAD_MAX_FILESIZE);
	// error_reporting(constant("ERROR_REPORTING"));
	if(preg_replace("/[0-9]/", "", constant("ERROR_REPORTING")) != "") {
		error_reporting(constant(constant("ERROR_REPORTING")));
	}
	else {
		error_reporting(intval(constant("ERROR_REPORTING")));
	}

	require_once($_SERVER["DOCUMENT_ROOT"]. '/Auftragslisten/classes/DB_Connection.class.php');
	DEFINE('DB_HOST', 'localhost');
	DEFINE('DB_PORT', '80');
	DEFINE('DB_USER', 'burhan');
	DEFINE('DB_PASSWORD', 'burhan');
	DEFINE('DB_NAME', 'burhanctrAuftragslisten');

	$jswindowMessage = "";
	$warningMessage = '';
	$errorMessage = '';
	$successMessage = '';
	$infoMessage = '';

	$dbConnection = new DB_Connection();
	$db_open = $dbConnection->db_connect();

	// BOF GET CONNECTED DOCUMENTS
	$sql = "SELECT

			`tempTable`.`orderDocumentsID`,
			`tempTable`.`orderDocumentsCustomerNumber`,
			`tempTable`.`orderDocumentsNumber`,
			`tempTable`.`orderDocumentsType`,
			`tempTable`.`orderDocumentsOrdersIDs`,
			`tempTable`.`nameTable`,
			`tempTable`.`orderDocumentsStatus`,

			`b3_documentstodocuments`.`documentsToDocumentsOriginDocumentNumber`,
			`b3_documentstodocuments`.`documentsToDocumentsCreatedDocumentNumber`,
			`b3_documentstodocuments`.`documentsToDocumentsRelationType`

			FROM (

				SELECT

						`orderDocumentsID`,
						`orderDocumentsCustomerNumber`,
						`orderDocumentsNumber`,
						`orderDocumentsType`,
						`orderDocumentsOrdersIDs`,
						`orderDocumentsStatus`,
						'orderoffers' AS `nameTable`

					FROM `b3_orderoffers`

					WHERE `orderDocumentsCustomerNumber` = '32007'

				UNION

				SELECT

						`orderDocumentsID`,
						`orderDocumentsCustomerNumber`,
						`orderDocumentsNumber`,
						`orderDocumentsType`,
						`orderDocumentsOrdersIDs`,
						`orderDocumentsStatus`,
						'orderconfirmations' AS `nameTable`

					FROM `b3_orderconfirmations`

					WHERE `orderDocumentsCustomerNumber` = '32007'

				UNION

				SELECT

						`orderDocumentsID`,
						`orderDocumentsCustomerNumber`,
						`orderDocumentsNumber`,
						`orderDocumentsType`,
						`orderDocumentsOrdersIDs`,
						`orderDocumentsStatus`,
						'orderdeliveries' AS `nameTable`

					FROM `b3_orderdeliveries`

					WHERE `orderDocumentsCustomerNumber` = '32007'

				UNION

				SELECT

						`orderDocumentsID`,
						`orderDocumentsCustomerNumber`,
						`orderDocumentsNumber`,
						`orderDocumentsType`,
						`orderDocumentsOrdersIDs`,
						`orderDocumentsStatus`,
						'orderinvoices' AS `nameTable`

					FROM `b3_orderinvoices`

					WHERE `orderDocumentsCustomerNumber` = '32007'

				UNION

				SELECT

						`orderDocumentsID`,
						`orderDocumentsCustomerNumber`,
						`orderDocumentsNumber`,
						`orderDocumentsType`,
						`orderDocumentsOrdersIDs`,
						`orderDocumentsStatus`,
						'orderreminders' AS `nameTable`

					FROM `b3_orderreminders`

					WHERE `orderDocumentsCustomerNumber` = '32007'

				UNION

				SELECT

						`orderDocumentsID`,
						`orderDocumentsCustomerNumber`,
						`orderDocumentsNumber`,
						`orderDocumentsType`,
						`orderDocumentsOrdersIDs`,
						`orderDocumentsStatus`,
						'ordercredits' AS `nameTable`

					FROM `b3_ordercredits`

					WHERE `orderDocumentsCustomerNumber` = '32007'

				UNION

				SELECT

						`orderDocumentsID`,
						`orderDocumentsCustomerNumber`,
						`orderDocumentsNumber`,
						`orderDocumentsType`,
						`orderDocumentsOrdersIDs`,
						`orderDocumentsStatus`,
						'orderletters' AS `nameTable`

					FROM `b3_orderletters`

					WHERE `orderDocumentsCustomerNumber` = '32007'
				) AS `tempTable`

				LEFT JOIN `b3_documentstodocuments`
				ON(`tempTable`.`orderDocumentsNumber` = `b3_documentstodocuments`.`documentsToDocumentsOriginDocumentNumber`)

				";

		// GROUP BY CONCAT(`tempTable`.`orderDocumentsNumber`, ';', `b3_documentstodocuments`.`documentsToDocumentsCreatedDocumentNumber`)

	$rs = $dbConnection->db_query($sql);

	$arrConnectedDocumentIds = array();
	while($ds = mysqli_fetch_assoc($rs)) {
		$arrTemp = explode(";", $ds["orderDocumentsOrdersIDs"]);
		$arrTemp = array_unique($arrTemp);
		$thisProductionKey = implode("#", $arrTemp);
		$arrConnectedDocumentIds[$ds["orderDocumentsNumber"]] = array(
					"table" => $ds["nameTable"],
					"documentID" => $ds["orderDocumentsID"],
					"relationType" => $ds["documentsToDocumentsRelationType"],
					"productionIDs" => $ds["orderDocumentsOrdersIDs"],
					"documentsType" => $ds["orderDocumentsType"],
					"documentsStatus" => $ds["orderDocumentsStatus"]


			);
		foreach(array_keys($ds) as $field){
			$arrResultDocumentNumbers[$ds["nameTable"]][$thisProductionKey][$ds["orderDocumentsID"]][$field] = $ds[$field];
		}
	}

	if(!empty($arrResultDocumentNumbers)){
		$arrRelationProductionToDocumentNumbers = array();
		foreach($arrResultDocumentNumbers as $thisDocumentTableKey => $thisDocumentTableValue){
			foreach($thisDocumentTableValue as $thisProductionIds => $thisResultDocumentNumberValue){
				if(!empty($thisResultDocumentNumberValue)){
					foreach($thisResultDocumentNumberValue as $thisResultDocumentNumberValueKey => $thisResultDocumentNumberValueValue){
						if($thisResultDocumentNumberValueValue["orderDocumentsNumber"] != "") { $arrRelationProductionToDocumentNumbers[$thisProductionIds][] = $thisResultDocumentNumberValueValue["orderDocumentsNumber"]; }
						if($thisResultDocumentNumberValueValue["documentsToDocumentsOriginDocumentNumber"] != "") { $arrRelationProductionToDocumentNumbers[$thisProductionIds][] = $thisResultDocumentNumberValueValue["documentsToDocumentsOriginDocumentNumber"]; }
						if($thisResultDocumentNumberValueValue["documentsToDocumentsCreatedDocumentNumber"] != "") { $arrRelationProductionToDocumentNumbers[$thisProductionIds][] = $thisResultDocumentNumberValueValue["documentsToDocumentsCreatedDocumentNumber"]; }
					}
				}
				$arrRelationProductionToDocumentNumbers[$thisProductionIds] = array_unique($arrRelationProductionToDocumentNumbers[$thisProductionIds]);
			}
		}
	}

	#$arrConnectedDocumentIds = array_unique($arrConnectedDocumentIds);

	echo 'arrResultDocumentNumbers<pre>';
	print_r($arrResultDocumentNumbers);
	echo '</pre>';

	echo 'arrRelationProductionToDocumentNumbers<pre>';
	print_r($arrRelationProductionToDocumentNumbers);
	echo '</pre>';

	echo 'arrConnectedDocumentIds<pre>';
	print_r($arrConnectedDocumentIds);
	echo '</pre>';

	// EOF GET CONNECTED DOCUMENTS

	if(!empty($arrRelationProductionToDocumentNumbers)){
		echo '<table border="1" cellpadding="0" cellspacing="0" width="0" class="displayOrders">';
		echo '<tr>';
		echo '<th>Produktions-ID</th>';
		echo '<th>Dokumente</th>';
		echo '<th>xxxx</th>';
		echo '<th>xxxx</th>';
		echo '</tr>';
		foreach($arrRelationProductionToDocumentNumbers as $thisProductionID => $thisDocumentNumbers){
			echo '<tr>';
			echo '<td>' . $thisProductionID . '</td>';
			echo '<td>';
			echo '<table border="1" cellpadding="0" cellspacing="0" width="0" class="displayOrders">';
			echo '<tr>';
			echo '<th>Nr</th>';
			echo '<th>DB-ID</th>';
			echo '<th>docType</th>';
			echo '<th>Status</th>';
			foreach($thisDocumentNumbers as $thisDocumentKey => $thisDocumentNumber){
				echo '<tr>';
				echo '<td>';
				echo $thisDocumentNumber;
				echo '</td>';
				echo '<td>';
				echo $arrConnectedDocumentIds[$thisDocumentNumber]["documentID"];
				echo '</td>';
				echo '<td>';
				echo $arrConnectedDocumentIds[$thisDocumentNumber]["documentsType"];
				echo '</td>';
				echo '<td>';
				echo $arrConnectedDocumentIds[$thisDocumentNumber]["documentsStatus"];
				echo '</td>';
				echo '<tr>';
			}
			echo '</table>';
			echo '</td>';
			echo '<td>xxxx</td>';
			echo '<td>xxxx</td>';
			echo '</tr>';
		}
		echo '</table>';
	}
?>



