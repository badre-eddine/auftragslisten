<?php

	require_once('inc/requires.inc.php');

	if(!$arrGetUserRights["displayStocks"] && !$arrGetUserRights["editStocks"]) {
		header('location: ' . PAGE_EXIT_LOCATION);
		exit;
	}
	
	DEFINE('MAX_STOCK_PRICE_ADDITIONAL_ROWS', 2);

	$jswindowMessage = "";
	$warningMessage = '';
	$errorMessage = '';
	$successMessage = '';
	$infoMessage = '';
	
	
	if($_REQUEST["tab"] == ''){
		$_REQUEST["tab"] = "tabs-1";
	}

	$dbConnection = new DB_Connection();
	$db_open = $dbConnection->db_connect();

	// BOF READ VERTRETER
		$arrAgentDatas = getAgents();
		$arrAgentDatas2 = getAgents2();
	// EOF READ VERTRETER

	// BOF READ PAYMENT TYPES
		$arrPaymentTypeDatas = getPaymentTypes();
	// EOF READ PAYMENT TYPES

	// BOF READ CUSTOMER GROUP DATAS
		$arrCustomerGroupDatas = getCustomerGroups();
	// EOF READ CUSTOMER GROUP DATAS

	// BOF READ CUSTOMER TYPES
		$arrCustomerTypeDatas = getCustomerTypes();
	// EOF READ CUSTOMER TYPES	
	
	// BOF READ ORDER CATEGORIES		
		$arrAllProductDatas = getOrderProducts();		
	// EOF READ ORDER CATEGORIES
	
	// BOF READ PRINT COLORS
		$arrSupplierDatas = getSuppliers();
	// EOF READ PRINT COLORS
	
	// BOF GET CUSTOMER GROUPS
		$arrCustomerGroups = getCustomerGroups();		
		// BOF ONLY USE CUSTOMER GROUPS HAVING PRICELIST
		foreach($arrCustomerGroups as $thisCustomerGroupKey => $thisCustomerGroupValue){
			if($thisCustomerGroupValue["customerGroupsHasPricelist"] != '1'){
				unset($arrCustomerGroups[$thisCustomerGroupKey]);
			}
		}
	// EOF GET CUSTOMER GROUPS

	// BOF GET CUSTOMER TYPES
		$arrCustomerTypes = getCustomerTypes();
		// BOF ONLY USE CUSTOMER TYPES HAVING PRICELIST
		foreach($arrCustomerTypes as $thisCustomerTypeKey => $thisCustomerTypeValue){
			if($thisCustomerTypeValue["customerTypesHasPricelist"] != '1'){
				unset($arrCustomerTypes[$thisCustomerTypeKey]);
			}
		}
	// EOF GET CUSTOMER GROUPS
	
	// BOF STORE STOCK PRODUCT PRICES
		if($_POST["storeProductPrices"] != ""){
			$arrSqlInsertPrices = array();

			// BOF STORE CUSTOMER GROUP PRICES
				if(!empty($_POST["editCustomerGroupProductQuantity"])){
					foreach($_POST["editCustomerGroupProductQuantity"] as $thisRowKey => $thisQuantityValue){

						if($thisQuantityValue > 0) {

							if(!empty($_POST["editCustomerGroupProductPrice"][$thisRowKey])){
								foreach($_POST["editCustomerGroupProductPrice"][$thisRowKey] as $thisGroupKey => $thisGroupPrice){
									if($thisGroupPrice > 0){
										$arrSqlInsertPrices[] = "
												(
													'" . $_POST["editCustomerGroupPriceID"][$thisRowKey][$thisGroupKey]. "',
													'" . $_POST["editID"]. "',
													'" . $_POST["productID"]. "',
													'" . $_POST["productNumber"]. "',
													0,
													'" . $thisGroupKey . "',
													'" . $thisGroupPrice. "',
													'" . $thisQuantityValue. "',
													'customerGroup'
												)
											";
									}
								}
							}
						}

					}
				}
			// EOF STORE CUSTOMER GROUP PRICES

			// BOF STORE CUSTOMER TYPE PRICES
				if(!empty($_POST["editCustomerTypeProductQuantity"])){
					foreach($_POST["editCustomerTypeProductQuantity"] as $thisRowKey => $thisQuantityValue){

						if($thisQuantityValue > 0){

							if(!empty($_POST["editCustomerTypeProductPrice"][$thisRowKey])){
								foreach($_POST["editCustomerTypeProductPrice"][$thisRowKey] as $thisTypeKey => $thisTypePrice){
									if($thisTypePrice > 0){
										$arrSqlInsertPrices[] = "
												(
													'" . $_POST["editCustomerTypePriceID"][$thisRowKey][$thisTypeKey]. "',
													'" . $_POST["editID"]. "',
													'" . $_POST["productID"]. "',
													'" . $_POST["productNumber"]. "',
													'" . $thisTypeKey . "',
													0,
													'" . $thisTypePrice. "',
													'" . $thisQuantityValue. "',
													'customerType'
												)
											";

									}
								}
							}
						}
					}
				}
			// EOF STORE CUSTOMER TYPE PRICES

			if(!empty($arrSqlInsertPrices)){
				$sql_storeStockPrices = "
						REPLACE
							INTO `" . TABLE_STOCK_PRODUCT_PRICES . "` (
								`stockProductPricesID`,
								`stockProductPricesProductID`,
								`stockProductPricesProductDataID`,
								`stockProductPricesProductNumber`,
								`stockProductPricesCustomerTypeID`,
								`stockProductPricesCustomerGroupID`,
								`stockProductPricesPrice`,
								`stockProductPricesQuantity`,
								`stockProductPricesPriceType`
							) VALUES

							" . implode(", ", $arrSqlInsertPrices) . "
					";
					//echo $sql_storeStockPrices;
				$query_delete ="delete from `" . TABLE_STOCK_PRODUCT_PRICES . "`   where  stockProductPricesProductID='" . $_POST["editID"]. "'";
				$dbConnection->db_query($query_delete);
				$rs_storeStockPrices = $dbConnection->db_query($sql_storeStockPrices);

				if($rs_storeStockPrices){
					$successMessage .= 'Die Preise wurde gespeichert.' . '<br />';
				}
				else {
					$errorMessage .= 'Die Preise konnten nicht gespeichert werden.' . '<br />';
				}
			}
		}
	// EOF STORE STOCK PRODUCT PRICES
	
	// BOF GET SELECTED PRODUCT PRICES
		if($_REQUEST["tab"] == "tabs-3") {
			$sql_getProductsPrices = "
					SELECT
							`stockProductPricesID`,
							`stockProductPricesProductID`,
							`stockProductPricesProductDataID`,
							`stockProductPricesProductNumber`,
							IF(`stockProductPricesPriceType` = 'customerType', `stockProductPricesCustomerTypeID`,
								IF(`stockProductPricesPriceType` = 'customerGroup', `stockProductPricesCustomerGroupID`,
									'xxx'
								)
							) AS `stockProductPricesCustomerGroupTypeID`,
							`stockProductPricesPrice`,
							`stockProductPricesQuantity`,
							`stockProductPricesPriceType`

						FROM `" . TABLE_STOCK_PRODUCT_PRICES . "`

						WHERE 1
							AND `stockProductPricesProductID` = '" . $_REQUEST["editID"] . "'

						ORDER BY
							`stockProductPricesPriceType` ASC,
							`stockProductPricesQuantity` ASC

				";

			$rs_getProductsPrices = $dbConnection->db_query($sql_getProductsPrices);

			$arrProductsPrices = array();
			while($ds_getProductsPrices = mysqli_fetch_assoc($rs_getProductsPrices)){
				$arrProductsPrices[$ds_getProductsPrices["stockProductPricesPriceType"]][$ds_getProductsPrices["stockProductPricesQuantity"]][$ds_getProductsPrices["stockProductPricesCustomerGroupTypeID"]] = $ds_getProductsPrices;
			}
		}
	// EOF GET SELECTED PRODUCT PRICES
	
	// BOF STORE PRODUCT
		if($_POST["storeDatas"] != ""){			
			// BOF STORE PRODUCTS CAT
				$thisStoreID = $_POST["editID"];
				// BOF CREATE LEVEL ID
					if($thisStoreID == 'NEW' || $_POST["editProductsCategoryParentID"] != $_POST["originalProductsCategoryParentID"]){
						if($_POST["editProductsCategoryParentID"] == '000'){
							// BOF FIND LEVEL_ID
								// BOF GET GAP LEVEL ID 3 CHARS
									$thisCatLength = 3;
									$where = " AND LENGTH(`stockproductcategories_1`.`stockProductCategoriesLevelID`) = " . $thisCatLength . " ";
								// EOF GET GAP LEVEL ID 3 CHARS
							// EOF FIND LEVEL_ID
						}
						else if($_POST["editProductsCategoryParentID"] != '000'){
							// BOF GET GAP LEVEL ID 6 CHARS
								$thisCatLength = 6;
								$where = "
										AND LENGTH(`stockproductcategories_1`.`stockProductCategoriesLevelID`) = " . $thisCatLength . "
										AND `stockproductcategories_1`.`stockProductCategoriesLevelID` LIKE '" . $_POST["editProductsCategoryParentID"] . "%'
									";
							// EOF GET GAP LEVEL ID 6 CHARS
						}

						$sql_getLevel_ID = "
								SELECT
									/*
									`tempTable`.`stockProductCategoriesLevelID_beforeGap`,
									`tempTable`.`stockProductCategoriesLevelID_fillGap`,
									*/

									CONCAT(REPEAT('0', (" . $thisCatLength . " - LENGTH(`tempTable`.`newStockProductCategoriesLevelID`))), `tempTable`.`newStockProductCategoriesLevelID`)  AS `newStockProductCategoriesLevelID`

									FROM (

										SELECT

											CONVERT(`stockproductcategories_1`.`stockProductCategoriesLevelID`, SIGNED INTEGER) AS `stockProductCategoriesLevelID_beforeGap`,
											(CONVERT(`stockproductcategories_1`.`stockProductCategoriesLevelID`, SIGNED INTEGER) + 1) AS `stockProductCategoriesLevelID_fillGap`,

											(CONVERT(`stockproductcategories_1`.`stockProductCategoriesLevelID`, SIGNED INTEGER) + 1) AS `newStockProductCategoriesLevelID`

										FROM `common_stockproductcategories` AS `stockproductcategories_1`
										WHERE 1
											" . $where . "
											AND (
												SELECT
													CONVERT(`stockproductcategories_2`.`stockProductCategoriesLevelID`, SIGNED INTEGER) AS `stockProductCategoriesLevelID`
													FROM `common_stockproductcategories` AS `stockproductcategories_2`
													WHERE 1
														AND CONVERT(`stockproductcategories_2`.`stockProductCategoriesLevelID`, SIGNED INTEGER) = (CONVERT(`stockproductcategories_1`.`stockProductCategoriesLevelID`, SIGNED INTEGER) + 1)
											) IS NULL
										ORDER BY `stockproductcategories_1`.`stockProductCategoriesLevelID`
										LIMIT 1
									) AS `tempTable`
							";
						#dd('sql_getLevel_ID');
						$rs_getLevel_ID = $dbConnection->db_query($sql_getLevel_ID);
						list($getNewStockProductCategoriesLevelID) = mysqli_fetch_array($rs_getLevel_ID);

						if($getNewStockProductCategoriesLevelID == ''){
							$getNewStockProductCategoriesLevelID = "1";
							$getNewStockProductCategoriesLevelID = str_repeat("0", (3 - strlen($getNewStockProductCategoriesLevelID))) . $getNewStockProductCategoriesLevelID;

							if($thisCatLength == 3){

							}
							else if($thisCatLength == 6){
								$getNewStockProductCategoriesLevelID = $_POST["editProductsCategoryParentID"] . $getNewStockProductCategoriesLevelID;
							}
						}

						$thisStockProductCategoriesLevelID = $getNewStockProductCategoriesLevelID;
						$thisStockProductCategoriesParentID = $_POST["editProductsCategoryParentID"];
						$thisStockProductCategoriesRelationID = $_POST["editProductCategoriesRelationID"];
					}
					else {
						$thisStockProductCategoriesLevelID = $_POST["editProductsCategoryLevelID"];
						$thisStockProductCategoriesParentID = $_POST["editProductsCategoryParentID"];
						$thisStockProductCategoriesRelationID = $_POST["editProductCategoriesRelationID"];
					}
				// EOF CREATE LEVEL ID

				if($thisStoreID == 'NEW'){
					$thisStoreID = '%';
				}
				$editProductCategoriesProductIsForeign = ($_POST["editProductCategoriesProductIsForeign"]!='')?$_POST["editProductCategoriesProductIsForeign"]:'0';
				$editProductCategoriesProductSupplier = ($_POST["editProductCategoriesProductSupplier"]!='')?$_POST["editProductCategoriesProductSupplier"]:'0';
				$sql_storeCat = "
						REPLACE INTO `" . TABLE_STOCK_PRODUCT_CATEGORIES . "` (
								
								`stockProductCategoriesLevelID`,
								`stockProductCategoriesParentID`,
								`stockProductCategoriesRelationID`,
								`stockProductCategoriesName`,
								`stockProductCategoriesName_TR`,
								`stockProductCategoriesText`,
								`stockProductCategoriesShortName`,
								`stockProductCategoriesProductNumber`,
								`stockProductCategoriesProductNumber_bctr`,
								`stockProductCategoriesProductNumber_b3`,
								`stockProductCategoriesSort`,
								`stockProductCategoriesActive`,
								`stockProductCategoriesProductIsForeign`,
								`stockProductCategoriesProductSupplier`,
								`productweight`,
								`stockProductCategoriesProductSupplierProductNumber`
						)
						VALUES (
								
								'" . $thisStockProductCategoriesLevelID . "',
								'" . $thisStockProductCategoriesParentID . "',
								'" . $thisStockProductCategoriesRelationID . "',
								'" . $_POST["editProductsCategoryName"] . "',
								'" . $_POST["editProductsCategoryName_TR"] . "',
								'" . $_POST["editProductsCategoryText"] . "',
								'" . $_POST["editProductsCategoryShortName"] . "',
								'" . $_POST["editProductCategoriesProductNumber"] . "',
								'" . $_POST["editProductCategoriesProductNumber_bctr"] . "',
								'" . $_POST["editProductCategoriesProductNumber_b3"] . "',
								'" . $_POST["editProductsCategorySort"] . "',
								'" . $_POST["editProductsCategoryStatus"] . "',
								'" . $editProductCategoriesProductIsForeign . "',
								'" . $editProductCategoriesProductSupplier . "',
								'" . $_POST["productweight"] . "',
								'" . $_POST["editProductCategoriesProductSupplierProductNumber"] . "'
						)
					";
					 
				$rs_storeCat = $dbConnection->db_query($sql_storeCat);
				if($thisStoreID == '%')
					$thisStoreID = mysqli_insert_id();
				#dd('thisStoreID');
				if($rs_storeCat){
					$successMessage .= 'Das Produkt wurde gespeichert.' . '<br />';
				}
				else {
					$errorMessage .= 'Das Produkt konnte nicht gespeichert werden.' . '<br />';
				}			
			// EOF STORE PRODUCTS CAT
			
			// BOF STORE PRODUCTS DETAIL DATA				
				if(!empty($_REQUEST["stockProductsOptions"])){
					
					// BOF DELETE ATTRIBUTES BEFORE STORING NEW
						$sql_deleteAttributes = "
								DELETE 
									FROM `" . TABLE_STOCK_PRODUCT_DETAIL_DATA. "` 
									
									WHERE 1
										AND `stockProductDetailDataProductID` = '" . $thisStoreID . "'
							";
						$rs_deleteAttributes = $dbConnection->db_query($sql_deleteAttributes);
					// EOF DELETE ATTRIBUTES BEFORE STORING NEW
					
					
					foreach($_REQUEST["stockProductsOptions"] as $thisOptionKey => $thisOptionData){
						$arrSqlStoreAttributes = array();
						foreach($_REQUEST["stockProductsOptions"][$thisOptionKey] as $thisAttributeKey => $thisAttributeData){
							$arrSqlStoreAttributes[] = "
									(
										'" . $thisStoreID . "',
										'" . $thisOptionKey . "',
										'" . $thisAttributeData . "'
									)
								";
						}
						if(!empty($arrSqlStoreAttributes)){
							$sql_storeAttributes = "
									REPLACE
										INTO `" . TABLE_STOCK_PRODUCT_DETAIL_DATA. "` 
										VALUES " . implode(", ", $arrSqlStoreAttributes) . "
								";				
								// echo $sql_storeAttributes;			
							$rs_storeAttributes = $dbConnection->db_query($sql_storeAttributes);
						}
					}					
				}
				// exit();
			// EOF STORE PRODUCTS DETAIL DATA
			
			if(!empty($_REQUEST["stockProductsGroupProductsData"])){
				foreach($_REQUEST["stockProductsGroupProductsData"] as $thisGroupProductsDataKey => $thisGroupProductsDataValue){

					// BOF DELETE BEFORE STORING
						$sql_deleteGroupProductData = "
								DELETE FROM `" . TABLE_CUSTOMER_GROUPS_PRODUCTS . "`
									WHERE 1
										AND `productsProductID` = '" . $thisStoreID . "'
										AND `customerGroupsProductNumbersGroupID` = '" . $thisGroupProductsDataKey . "'										
							";
						$rs_deleteGroupProductData = $dbConnection->db_query($sql_deleteGroupProductData);						
					// EOF DELETE BEFORE STORING
				
					if($thisGroupProductsDataValue["customerGroupsProductNumber"] != ''){						
						$sql_storeGroupProductData = "
								REPLACE INTO `" . TABLE_CUSTOMER_GROUPS_PRODUCTS . "` (
											`customerGroupsProductNumbersID`,
											`productsProductID`,
											`customerGroupsProductNumbersGroupID`,
											`customerGroupsProductNumber`,
											`customerGroupsProductNumber2`,
											`productsModelNumber`,
											`productsCategoriesID`,
											`customerGroupsCompanyID`
										)
										VALUES (
											'" . $thisGroupProductsDataValue["customerGroupsProductNumbersID"] . "',
											'" . $thisStoreID . "',
											'" . $thisGroupProductsDataValue["customerGroupsID"] . "',
											'" . $thisGroupProductsDataValue["customerGroupsProductNumber"] . "',
											'" . $thisGroupProductsDataValue["customerGroupsProductNumber2"] . "',
											'" . $_REQUEST["editProductCategoriesProductNumber"] . "',
											'" . $thisGroupProductsDataValue["stockProductsLevelID"] . "',
											'" . $thisGroupProductsDataValue["customerGroupsCompanyID"] . "'
										)
							";						
						$rs_storeGroupProductData = $dbConnection->db_query($sql_storeGroupProductData);
					}
				}
			}
			
			// unset($_REQUEST["editID"]);
		}
	// EOF STORE PRODUCT

	// BOF CANCEL PRODUCTS_CAT
		if($_POST["cancelDatas"] != ""){
			unset($_REQUEST["editID"]);
		}
	// EOF CANCEL PRODUCTS_CAT

	// BOF DELETE PRODUCTS_CAT
		if($_POST["deleteDatas"] != ""){
			// BOF DELETE PRODUCT AND PRODUCT ATTRIBUTES
				$sql_deleteProduct = "
						DELETE
							`" . TABLE_STOCK_PRODUCT_CATEGORIES . "`,
							`" . TABLE_STOCK_PRODUCT_DETAIL_DATA. "`
							
							FROM `" . TABLE_STOCK_PRODUCT_CATEGORIES . "`
							
							LEFT JOIN `" . TABLE_STOCK_PRODUCT_DETAIL_DATA. "`
							ON(`" . TABLE_STOCK_PRODUCT_CATEGORIES . "`.`stockProductCategoriesID` = `" . TABLE_STOCK_PRODUCT_DETAIL_DATA . "`.`stockProductDetailDataProductID`)
							
							WHERE 1
								AND `" . TABLE_STOCK_PRODUCT_CATEGORIES . "`.`stockProductCategoriesID` = '" . $_POST["editID"] . "'
					";
				$rs_deleteProduct = $dbConnection->db_query($sql_deleteProduct);
				if($rs_deleteProduct){
					$successMessage .= 'Das Produkt und die zugehörigen Attribute wurden entfernt.' . '<br />';
				}
				else {
					$errorMessage .= 'Das Produkt und die zugehörigen Attribute konnten nicht entfernt werden.' . '<br />';
				}
			// EOF DELETE PRODUCT AND PRODUCT ATTRIBUTES
			
			unset($_REQUEST["editID"]);
		}
	// EOF DELETE PRODUCTS_CAT
	
	// BOF GET ALL PRODUCTS OPTIONS AND ATTRIBUTES	
		if($_REQUEST["editID"] != ""){
			$sql_getOption = "
					SELECT
							`stockProductOptionsID`,
							`stockProductOptionsName`,
							`stockProductOptionsParentID`,
							`stockProductOptionsSort`,
							`stockProductOptionsActive`,
							`stockProductOptionsSelectable`,
							IF(`stockProductOptionsParentID` = '0', 'option', 'attribute') AS `stockProductOptionsType`
						FROM `" . TABLE_STOCK_PRODUCT_OPTIONS . "`
						WHERE 1										

						ORDER BY
							`stockProductOptionsParentID`,
							`stockProductOptionsSort`,						
							`stockProductOptionsName`
							
				";
				// echo $sql_getOption;exit();
			$rs_getOption = $dbConnection->db_query($sql_getOption);

			$arrProductsOptionData = array();
			$arrProductsAttributeData = array();

			while($ds_getOption = mysqli_fetch_assoc($rs_getOption)){
				foreach(array_keys($ds_getOption) as $field){
					if($ds_getOption["stockProductOptionsType"] == 'option'){
						$arrProductsOptionData[$ds_getOption["stockProductOptionsID"]][$field] = $ds_getOption[$field];
					}
					else if($ds_getOption["stockProductOptionsType"] == 'attribute'){
						$arrProductsAttributeData[$ds_getOption["stockProductOptionsParentID"]][$ds_getOption["stockProductOptionsID"]][$field] = $ds_getOption[$field];
					}

				}
			}		
		}
	// BOF GET ALL PRODUCTS OPTIONS AND ATTRIBUTES

	// BOF GET ALL PRODUCT CATEGORIES
		$sql_getCat = "
				SELECT
						`stockProductCategoriesID`,
						`stockProductCategoriesLevelID`,
						`stockProductCategoriesParentID`,
						`stockProductCategoriesRelationID`,						
						`stockProductCategoriesName`,
						`stockProductCategoriesName_TR`,
						`stockProductCategoriesText`,
						`stockProductCategoriesShortName`,
						`stockProductCategoriesProductNumber`,
						`stockProductCategoriesProductNumber_bctr`,
						`stockProductCategoriesProductNumber_b3`,
						`stockProductCategoriesSort`,
						`stockProductCategoriesActive`,
						`stockProductCategoriesProductIsForeign`,
						`stockProductCategoriesProductSupplier`,
						`productweight`,
						`stockProductCategoriesProductSupplierProductNumber`
						
					FROM `" . TABLE_STOCK_PRODUCT_CATEGORIES . "`
					WHERE 1
						AND `stockProductCategoriesParentID` = '000'

					ORDER BY
						`stockProductCategoriesSort`,
						`stockProductCategoriesName`
						
						/*,
						`stockProductCategoriesParentID`,
						`stockProductCategoriesLevelID`		
						*/						
						
			";
		$rs_getCat = $dbConnection->db_query($sql_getCat);

		$arrProductsCategoryData = array();

		while($ds_getCat = mysqli_fetch_assoc($rs_getCat)){
			foreach(array_keys($ds_getCat) as $field){
				$arrProductsCategoryData[$ds_getCat["stockProductCategoriesLevelID"]][$field] = $ds_getCat[$field];
			}
		}
	// EOF GET ALL PRODUCT CATEGORIES

	// BOF GET ALL PRODUCTS
	
		$sqlWhere = "";
		
		if($_REQUEST["searchProductCategory"] != ""){
			$sqlWhere = " AND `productCategories`.`stockProductCategoriesParentID` = '" . $_REQUEST["searchProductCategory"] . "' ";
		}
		else if($_REQUEST["searchProductNumber"] != ""){
			$sqlWhere = " 
					AND (
						`productCategories`.`stockProductCategoriesProductNumber` = '" . $_REQUEST["searchProductNumber"] . "' 
						OR `productCategories`.`stockProductCategoriesProductNumber_bctr` = '" . $_REQUEST["searchProductNumber"] . "' 
						OR `productCategories`.`stockProductCategoriesProductNumber_b3` = '" . $_REQUEST["searchProductNumber"] . "' 
					)
				";
		}
		else if($_REQUEST["searchProductName"] != ""){
			$sqlWhere = " 
					AND (
						`productCategories`.`stockProductCategoriesName` LIKE '%" . $_REQUEST["searchProductName"] . "%' 
						OR `productCategories`.`stockProductCategoriesText` LIKE '%" . $_REQUEST["searchProductName"] . "%' 
					)
				";
		}
		else if($_REQUEST["searchWord"] != ""){
			$sqlWhere = " 
					AND (
						`productCategories`.`stockProductCategoriesName` LIKE '%" . $_REQUEST["searchWord"] . "%' 
						OR `productCategories`.`stockProductCategoriesText` LIKE '%" . $_REQUEST["searchWord"] . "%' 
						OR `productCategories`.`stockProductCategoriesProductNumber` LIKE '%" . $_REQUEST["searchWord"] . "%' 
						OR `productCategories`.`stockProductCategoriesProductNumber_bctr` LIKE '%" . $_REQUEST["searchWord"] . "%' 
						OR `productCategories`.`stockProductCategoriesProductNumber_b3` LIKE '%" . $_REQUEST["searchWord"] . "%' 
					)
				";
		}
		
		if($_REQUEST["searchDeactivatedProducts"] != "1"){
			$sqlWhere .= " AND `productCategories`.`stockProductCategoriesActive` = '1' ";
		}
		
		
		
		$sql_getCat = "
				SELECT
						`productCategories`.`stockProductCategoriesID` AS `stockProductsID`,
						`productCategories`.`stockProductCategoriesLevelID` AS `stockProductsLevelID`,
						`productCategories`.`stockProductCategoriesParentID` AS `stockProductsParentID`,
						`productCategories`.`stockProductCategoriesRelationID` AS `stockProductsRelationID`,
						`productCategories`.`stockProductCategoriesName` AS `stockProductsName`,
						`productCategories`.`stockProductCategoriesName_TR` AS `stockProductsName_TR`,
						`productCategories`.`stockProductCategoriesText` AS `stockProductsText`,
						`productCategories`.`stockProductCategoriesShortName` AS `stockProductsShortName`,
						`productCategories`.`stockProductCategoriesProductNumber` AS `stockProductsProductNumber`,
						`productCategories`.`stockProductCategoriesProductNumber_bctr` AS `stockProductsProductNumber_bctr`,
						`productCategories`.`stockProductCategoriesProductNumber_b3` AS `stockProductsProductNumber_b3`,
						`productCategories`.`stockProductCategoriesSort` AS `stockProductsSort`,
						`productCategories`.`stockProductCategoriesActive` AS `stockProductsActive`,
						`productCategories`.`stockProductCategoriesContainsStockProducts` AS `stockProductsContainsStockProducts`,
						`productCategories`.`stockProductCategoriesProductIsForeign` AS `stockProductsIsForeign`,
						`productCategories`.`stockProductCategoriesProductSupplier` AS `stockProductsSupplier`,
						`productCategories`.`stockProductCategoriesProductSupplierProductNumber` AS `stockProductsSupplierProductNumber`,
						
						`parentCategories`.`stockProductCategoriesID` AS `stockParentCategoriesID`,
						`parentCategories`.`stockProductCategoriesLevelID` AS `stockParentCategoriesLevelID`,
						`parentCategories`.`stockProductCategoriesParentID` AS `stockParentCategoriesParentID`,
						`parentCategories`.`stockProductCategoriesRelationID` AS `stockParentCategoriesRelationID`,
						`parentCategories`.`stockProductCategoriesName` AS `stockParentCategoriesName`,
						`parentCategories`.`stockProductCategoriesName_TR` AS `stockParentCategoriesName_TR`,
						`parentCategories`.`stockProductCategoriesText` AS `stockParentCategoriesText`,
						`parentCategories`.`stockProductCategoriesShortName` AS `stockParentCategoriesShortName`,
						`parentCategories`.`stockProductCategoriesProductNumber` AS `stockParentCategoriesProductNumber`,
						`parentCategories`.`stockProductCategoriesProductNumber_bctr` AS `stockParentCategoriesProductNumber_bctr`,
						`parentCategories`.`stockProductCategoriesProductNumber_b3` AS `stockParentCategoriesProductNumber_b3`,
						`parentCategories`.`stockProductCategoriesSort` AS `stockParentCategoriesSort`,
						`parentCategories`.`stockProductCategoriesActive` AS `stockParentCategoriesActive`,
						`parentCategories`.`stockProductCategoriesContainsStockProducts` AS `stockParentCategoriesContainsStockProducts`,
						`parentCategories`.`stockProductCategoriesProductIsForeign` AS `stockParentCategoriesIsForeign`,
						`parentCategories`.`stockProductCategoriesProductSupplier` AS `stockParentCategoriesSupplier`,
						`parentCategories`.`stockProductCategoriesProductSupplierProductNumber` AS `stockParentCategoriesSupplierProductNumber`,

						GROUP_CONCAT(		
							DISTINCT
							`productOptions`.`stockProductOptionsName`,
							'|',							
							`productAttributes`.`stockProductOptionsName`
							ORDER BY `productOptions`.`stockProductOptionsSort` ASC, `productAttributes`.`stockProductOptionsName` ASC
							SEPARATOR '###'
						) AS `stockProductData`						
						
						/*
						`" . TABLE_STOCK_PRODUCT_DETAIL_DATA . "`.`stockProductDetailDataProductID`,
						`" . TABLE_STOCK_PRODUCT_DETAIL_DATA . "`.`stockProductDetailDataOptionID`,
						`" . TABLE_STOCK_PRODUCT_DETAIL_DATA . "`.`stockProductDetailDataAttributeID`,	
						
						`productOptions`.`stockProductOptionsID`,
						`productOptions`.`stockProductOptionsName`,						
						`productOptions`.`stockProductOptionsParentID`,
						`productOptions`.`stockProductOptionsSort`,
						`productOptions`.`stockProductOptionsActive`,
						`productOptions`.`stockProductOptionsSelectable`,
						
						`productAttributes`.`stockProductOptionsID`,
						`productAttributes`.`stockProductOptionsName`,
						`productAttributes`.`stockProductOptionsParentID`,
						`productAttributes`.`stockProductOptionsSort`,
						`productAttributes`.`stockProductOptionsActive`,
						`productAttributes`.`stockProductOptionsSelectable`	
						
						
						
						FROM `" . TABLE_STOCK_PRODUCT_CATEGORIES . "` AS `parentCategories`
						*/						
						
					FROM `" . TABLE_STOCK_PRODUCT_CATEGORIES . "` AS `productCategories`
					
					LEFT JOIN `" . TABLE_STOCK_PRODUCT_CATEGORIES . "` AS `parentCategories`
					ON(`productCategories`.`stockProductCategoriesParentID` = `parentCategories`.`stockProductCategoriesLevelID`)				
	
					LEFT JOIN `" . TABLE_STOCK_PRODUCT_DETAIL_DATA . "`
					ON(`productCategories`.`stockProductCategoriesID` = `" . TABLE_STOCK_PRODUCT_DETAIL_DATA . "`.`stockProductDetailDataProductID`)
					
					LEFT JOIN `" . TABLE_STOCK_PRODUCT_OPTIONS . "` AS `productOptions`
					ON(`" . TABLE_STOCK_PRODUCT_DETAIL_DATA . "`.`stockProductDetailDataOptionID` = `productOptions`.`stockProductOptionsID`)
					
					LEFT JOIN `" . TABLE_STOCK_PRODUCT_OPTIONS . "` AS `productAttributes`
					ON(`" . TABLE_STOCK_PRODUCT_DETAIL_DATA . "`.`stockProductDetailDataAttributeID` = `productAttributes`.`stockProductOptionsID`)					
					
					WHERE 1
						AND `productCategories`.`stockProductCategoriesParentID` != '000'
						
						" . $sqlWhere . "

					GROUP BY
						`productCategories`.`stockProductCategoriesID`
						
					ORDER BY
						`parentCategories`.`stockProductCategoriesSort`,
						
						`productCategories`.`stockProductCategoriesSort` ASC,
						`productCategories`.`stockProductCategoriesName` ASC,
						`productCategories`.`stockProductCategoriesParentID` ASC
						/*,
						`productCategories`.`stockProductCategoriesLevelID`	
						*/					
			";
			
		
		$rs_getCat = $dbConnection->db_query($sql_getCat);

		$arrProductsData = array();

		while($ds_getCat = mysqli_fetch_assoc($rs_getCat)){
			foreach(array_keys($ds_getCat) as $field){
				$arrProductsData[$ds_getCat["stockProductsParentID"]][$ds_getCat["stockProductsID"]][$field] = $ds_getCat[$field];
			}
		}
	// EOF GET ALL PRODUCTS

	// BOF GET SELECTED PRODUCT
		if($_REQUEST["editID"] != '' && $_REQUEST["editID"] != 'NEW'){
			$sql_getCat = "
					SELECT
							`productCategories`.`stockProductCategoriesID` AS `stockProductsID`,
							`productCategories`.`stockProductCategoriesLevelID` AS `stockProductsLevelID`,
							`productCategories`.`stockProductCategoriesParentID` AS `stockProductsParentID`,
							`productCategories`.`stockProductCategoriesRelationID` AS `stockProductsRelationID`,
							`productCategories`.`stockProductCategoriesName` AS `stockProductsName`,
							`productCategories`.`stockProductCategoriesName_TR` AS `stockProductsName_TR`,
							`productCategories`.`stockProductCategoriesText` AS `stockProductsText`,
							`productCategories`.`stockProductCategoriesShortName` AS `stockProductsShortName`,
							`productCategories`.`stockProductCategoriesProductNumber` AS `stockProductsProductNumber`,
							`productCategories`.`stockProductCategoriesProductNumber_bctr` AS `stockProductsProductNumber_bctr`,
							`productCategories`.`stockProductCategoriesProductNumber_b3` AS `stockProductsProductNumber_b3`,
							`productCategories`.`stockProductCategoriesSort` AS `stockProductsSort`,
							`productCategories`.`stockProductCategoriesActive` AS `stockProductsActive`,
							`productCategories`.`stockProductCategoriesContainsStockProducts` AS `stockProductsContainsStockProducts`,
							`productCategories`.`stockProductCategoriesProductIsForeign` AS `stockProductsIsForeign`,							
							`productCategories`.`stockProductCategoriesProductSupplier` AS `stockProductsSupplier`,
							`productCategories`.`stockProductCategoriesProductSupplierProductNumber` AS `stockProductsSupplierProductNumber`,
							`productCategories`.`productweight` AS `productweight`,
							
							`parentCategories`.`stockProductCategoriesID` AS `stockParentCategoriesID`,
							`parentCategories`.`stockProductCategoriesLevelID` AS `stockParentCategoriesLevelID`,
							`parentCategories`.`stockProductCategoriesParentID` AS `stockParentCategoriesParentID`,
							`parentCategories`.`stockProductCategoriesRelationID` AS `stockParentCategoriesRelationID`,
							`parentCategories`.`stockProductCategoriesName` AS `stockParentCategoriesName`,
							`parentCategories`.`stockProductCategoriesName_TR` AS `stockParentCategoriesName_TR`,
							`parentCategories`.`stockProductCategoriesText` AS `stockParentCategoriesText`,
							`parentCategories`.`stockProductCategoriesShortName` AS `stockParentCategoriesShortName`,
							`parentCategories`.`stockProductCategoriesProductNumber` AS `stockParentCategoriesProductNumber`,
							`parentCategories`.`stockProductCategoriesProductNumber_bctr` AS `stockParentCategoriesProductNumber_bctr`,
							`parentCategories`.`stockProductCategoriesProductNumber_b3` AS `stockParentCategoriesProductNumber_b3`,
							`parentCategories`.`stockProductCategoriesSort` AS `stockParentCategoriesSort`,
							`parentCategories`.`stockProductCategoriesActive` AS `stockParentCategoriesActive`,
							`parentCategories`.`stockProductCategoriesContainsStockProducts` AS `stockParentCategoriesContainsStockProducts`,
							`parentCategories`.`stockProductCategoriesProductIsForeign` AS `stockParentCategoriesIsForeign`,
							`parentCategories`.`stockProductCategoriesProductSupplier` AS `stockParentCategoriesSupplier`,
							`parentCategories`.`stockProductCategoriesProductSupplierProductNumber` AS `stockParentCategoriesSupplierProductNumber`
							
						FROM `" . TABLE_STOCK_PRODUCT_CATEGORIES . "` AS `productCategories`
						
						LEFT JOIN `" . TABLE_STOCK_PRODUCT_CATEGORIES . "` AS `parentCategories`
						ON(`productCategories`.`stockProductCategoriesParentID` = `parentCategories`.`stockProductCategoriesLevelID`)						

						WHERE 1
							 AND `productCategories`.`stockProductCategoriesID` = '" . $_REQUEST["editID"] . "'

						ORDER BY
							`productCategories`.`stockProductCategoriesParentID`,
							`productCategories`.`stockProductCategoriesLevelID`,
							`productCategories`.`stockProductCategoriesSort`,
							`productCategories`.`stockProductCategoriesName`
				";
			$rs_getCat = $dbConnection->db_query($sql_getCat);

			$arrSelectedProductsCategory = array();
			while($ds_getCat = mysqli_fetch_assoc($rs_getCat)){
				foreach(array_keys($ds_getCat) as $field){
					$arrSelectedProductsCategory[$field] = $ds_getCat[$field];
				}
			}
			#dd('sql_getCat');
			#dd('arrSelectedProductsCategory');
		}
	// EOF GET SELECTED PRODUCT
	
	// BOF GET SELECTED PRODUCTS ATTRIBUTES
		if($_REQUEST["editID"] != '' && $_REQUEST["editID"] != 'NEW'){
			$sgl_getProductsAttributes = "
					SELECT							
							`stockProductDetailDataProductID`,
							`stockProductDetailDataOptionID`,
							`stockProductDetailDataAttributeID`
						FROM `" . TABLE_STOCK_PRODUCT_DETAIL_DATA. "`
						
						WHERE 1
							AND `stockProductDetailDataProductID` = '" . $_REQUEST["editID"] . "'
							
						ORDER BY 
							`stockProductDetailDataOptionID` ASC,
							`stockProductDetailDataAttributeID` ASC
			
				";
			$sgl_getProductsAttributes = $dbConnection->db_query($sgl_getProductsAttributes);

			$arrSelectedProductsDetailData = array();
			while($ds_getProductsAttributes = mysqli_fetch_assoc($sgl_getProductsAttributes)){
				foreach(array_keys($ds_getProductsAttributes) as $field){
					$arrSelectedProductsDetailData[$ds_getProductsAttributes["stockProductDetailDataOptionID"]][$ds_getProductsAttributes["stockProductDetailDataAttributeID"]][$field] = $ds_getProductsAttributes[$field];
				}
			}			
		}
	// BOF GET SELECTED PRODUCTS ATTRIBUTES
?>
<?php
	require_once('inc/headerHTML.inc.php');
	$thisTitle = "Auftragslisten-Artikel";
	
	if($_REQUEST["editID"] != ""){
		if($_REQUEST["editID"] == "NEW"){
			$thisTitle .= ': <span class="headerSelectedEntry">Neues Produkt</span>';
		}
		else {
			$thisTitle .= ': <span class="headerSelectedEntry">' . htmlentities($arrSelectedProductsCategory["stockProductsName"]) . ' &bull; ' . htmlentities($arrSelectedProductsCategory["stockProductsProductNumber"]) . '</span>';
		}
	}

	$headerHTML = preg_replace("/{###TITLE###}/", strip_tags($thisTitle), $headerHTML);
	echo $headerHTML;

	require_once(FILE_MENUE_TOP);

?>

<?php require_once(FILE_MENUE_SIDEBAR); ?>
<div id="contentArea">
	<a name="top"></a>
	<div id="contentAreaElements">
		<h1><?php if(SHOW_PAGE_TITLE_ICONS) { echo '<img src="' . PATH_ICONS_MENUE_TITLES . 'shipping.png" alt="" />'; } ?> <?php echo $thisTitle; ?></h1>

		<?php displayMessages(); ?>
		<?php if($_REQUEST["editID"] == "" && $arrGetUserRights["editStocks"] == '1') { ?>
			<div class="menueActionsArea">
				<a href="<?php echo PAGE_PRODUCTS_STOCK_PRODUCTS; ?>?editID=NEW" class="linkButton">Neues Produkt anlegen</a>
				<div class="clear"></div>
			</div>
		<?php } ?>

		<div class="adminInfo">
			Datensatz-ID: <?php echo $_REQUEST["editID"]; ?>
			Level-ID: <?php echo $arrSelectedProductsCategory["stockProductsLevelID"]; ?>
		</div>		

		<div class="contentDisplay">
			<?php if($_REQUEST["editID"] == ''){ ?>
			<div id="searchFilterArea">
				<form name="formFilterSearch" method="post" action="<?php echo $_SERVER["PHP_SELF"]; ?>">
				<table border="0" cellpadding="0" cellspacing="0" class="searchFilterContent">
					<tr>
						<td>
							<label for="searchProductNumber">Artikelnummer:</label>
							<input type="text" name="searchProductNumber" id="searchProductNumber" class="inputField_70" />
						</td>
						<td>
							<label for="searchProductName">Artikelname:</label>
							<input type="text" name="searchProductName" id="searchProductName" class="inputField_120" />
						</td>
						<td>								
							<label for="searchProductCategory">Artikelkategorie:</label>
							<select name="searchProductCategory" id="searchProductCategory" class="inputField_120" >
								<option value=""></option>
								<?php
									if(!empty($arrProductsCategoryData)){
										foreach($arrProductsCategoryData as $thisKey => $thisValue){
											echo '<option value="' . $thisValue["stockProductCategoriesLevelID"] . '">' . htmlentities($thisValue["stockProductCategoriesName"]) . ' [' . $thisValue["stockProductCategoriesLevelID"] . ']</option>';
										}
									}
								?>
							</select>
						</td>
						<td>
							<label for="searchWord">Suchbegriff:</label>
							<input type="text" name="searchWord" id="searchWord" class="inputField_70" />
						</td>
						<td>
							<label for="searchDeactivatedProducts">Deaktivierte anzeigen?:</label>
							<input type="checkbox" name="searchDeactivatedProducts" id="searchDeactivatedProducts" value="1" />
						</td>
						<td>
							<input type="submit" name="submitformFilterSearch" id="submitformFilterSearch" class="inputButton0" value="Suchen" />
						</td>
					</tr>
				</table>
				</form>
			</div>
			<?php } ?>
		
			<div class="adminEditArea">
			<?php			
			
				if($_REQUEST["editID"] == ''){
					$countRowLevel1 = 0;
					$countRowLevel2 = 0;
# dd('arrProductsCategoryData');
					if(!empty($arrProductsData)){
						echo '<table cellpadding="0" cellspacing="0" border="0" width="100%" class="displayOrders">
							<colgroup>								
								<col />
								<col />
								<col />
								<col />								
								<col />
								<col />
								<col />
								<col />
							';
						if($_COOKIE["isAdmin"] == '1'){
							echo '
									<col />
									<col />
									<col />
									<col />
								';
						}
						echo '
								<col />
							</colgroup>
						
							<tr>								
								<th style="width:45px;text-align:right;">#</th>
								<th>Bezeichnung</th>
								<th>Kurz</th>
								<th>Art.Nr.</th>
								<th>Art.Nr. B3</th>
								<th>Sortierung</th>
								<th>Status</th>
								<th>Lieferant</th>
							';
						if($_COOKIE["isAdmin"] == '1'){
							echo '		
								<th>ID</th>
								<th>LID</th>
								<th>PID</th>
								<th>RID</th>
							';
						}
						echo '
								<th>Aktion</th>
							</tr>
						';

						foreach($arrProductsData as $thisCategoryKey => $thisCategoryValue){							
							
							// BOF CATEGORIES
								echo '<tr style="font-weight:bold;" class="row2">';	

								echo '<td style="text-align:left;" title="ID: ' . $arrProductsCategoryData[$thisCategoryKey]["stockProductCategoriesID"] . "\n" . 'LID: ' . $arrProductsCategoryData[$thisCategoryKey]["stockProductCategoriesLevelID"] . "\n" . 'PID: ' . $arrProductsCategoryData[$thisCategoryKey]["stockProductCategoriesParentID"] . "\n" . 'RID: ' . $arrProductsCategoryData[$thisCategoryKey]["stockProductCategoriesRelationID"] . '">';
								echo '<b>' . ($countRowLevel1 + 1). '.</b>';
								echo '</td>';

								echo '<td>';
								echo '&#9632; ' . htmlentities($arrProductsCategoryData[$thisCategoryKey]["stockProductCategoriesName"]);
								echo '</td>';

								echo '<td>';
								echo htmlentities($arrProductsCategoryData[$thisCategoryKey]["stockProductCategoriesShortName"]);
								echo '</td>';
								
								echo '<td style="white-space:nowrap;">';
								echo htmlentities($arrProductsCategoryData[$thisCategoryKey]["stockProductCategoriesProductNumber"]);
								echo '</td>';
								
								echo '<td style="white-space:nowrap;">';
								echo htmlentities($arrProductsCategoryData[$thisCategoryKey]["stockProductCategoriesProductNumber_b3"]);
								echo '</td>';

								echo '<td>';
								echo $arrProductsCategoryData[$thisCategoryKey]["stockProductCategoriesSort"];
								echo '</td>';

								echo '<td>';
								// echo $arrProductsCategoryData[$thisCategoryKey]["stockProductCategoriesActive"];
								if($arrProductsCategoryData[$thisCategoryKey]["stockProductCategoriesActive"] == '1'){
									$thisStatusImage = 'iconOk.png';
									$thisStatusTitle = 'aktiv';
								}
								else {
									$thisStatusImage = 'iconNotOk.png';
									$thisStatusTitle = 'nicht aktiv';
								}

								echo '<img src="layout/icons/' . $thisStatusImage . '" width="16" height="16" alt="' . $thisStatusTitle . '" title="' . $thisStatusTitle . '" />';
								echo '</td>';
								
								echo '<td>';
								echo '</td>';
								
								if($_COOKIE["isAdmin"] == '1'){								
									echo '<td>';
									echo $arrProductsCategoryData[$thisCategoryKey]["stockProductCategoriesID"];
									echo '</td>';								

									echo '<td>';
									echo $arrProductsCategoryData[$thisCategoryKey]["stockProductCategoriesLevelID"];
									echo '</td>';

									echo '<td>';
									echo $arrProductsCategoryData[$thisCategoryKey]["stockProductCategoriesParentID"];
									echo '</td>';
									
									echo '<td>';
									echo $arrProductsCategoryData[$thisCategoryKey]["stockProductCategoriesRelationID"];
									echo '</td>';
								}

								echo '<td>';
								echo '</td>';
								
								echo '</tr>';

								$countRowLevel1++;
							// EOF CATEGORIES	

							// BOF PRODUCTS
								$countRowLevel2 = 0;
								foreach($thisCategoryValue as $thisProductsKey => $thisProductsValue){
									if($countRowLevel1%2 == 0){ $rowClass = 'row0'; }
									else { $rowClass = 'row1'; }
									
									if($arrProductsCategoryData[$thisCategoryKey]["stockProductCategoriesActive"] != '1'){
										$rowClass = 'row6';
									}
																
									if($countRowLevel2%2 == 0){ $rowClass = 'row0'; }
									else { $rowClass = 'row1'; }
									
									if($thisProductsValue["stockProductsActive"] != '1'){
										$rowClass = 'row6';
									}

									echo '<tr class="'.$rowClass.'">';	
									
									echo '<td style="text-align:right;" title="ID: ' . $thisProductsValue["stockProductsID"] . "\n" . 'LID: ' . $thisProductsValue["stockProductsLevelID"] . "\n" . 'PID: ' . $thisProductsValue["stockProductsParentID"] . "\n" . 'RID: ' . $thisProductsValue["stockProductsRelationID"] . '">';
									echo '<b>' . ($countRowLevel1). '_' . ($countRowLevel2 + 1). '.</b>';
									echo '</td>';

									echo '<td>';
									echo '<span style="display:block;float:left;width:20px;">';
									echo '<img src="layout/icons/subCat.png" width="19" height="11" alt="" /> ';
									echo '</span>';
									
									echo '<span style="display:block;margin-left:24px;">';						
									echo htmlentities($thisProductsValue["stockProductsName"]);
									if(!empty($thisProductsValue["stockProductData"])){										
										echo ' <img src="layout/icons/iconInfo.png" class="buttonShowProductDetails" width="12" height="12" alt="Optionen anzeigen" title="Optionen anzeigen" />';
										echo '<div class="productDetails" style="display:none;position:absolute;top:auto;left:auto;background-color:#FFF;padding:10px;border:1px solid #333;box-shadow: 6px 6px 8px #333;">';
										
										$arrThisStockProductOptions = explode("###", $thisProductsValue["stockProductData"]);
										$thisOptionMarker = '';
										
										foreach($arrThisStockProductOptions as $thisOptionToAttribute){		
											
											$arrOptionToAttribute = explode("|", $thisOptionToAttribute);
											
											if($thisOptionMarker != $arrOptionToAttribute[0]){
												echo '<b>' . $arrOptionToAttribute[0] . '</b><br />';
												$thisOptionMarker = $arrOptionToAttribute[0];
											}
											echo '<span style="padding-left:20px;white-space:nowrap;line-height:20px;">&bull; ' . $arrOptionToAttribute[1] . '</span><br />';													
										}												
										echo '</div>';
									}
									echo '</span>';
									echo '</td>';

									echo '<td>';
									echo htmlentities($thisProductsValue["stockProductsShortName"]);
									echo '</td>';
									
									echo '<td style="white-space:nowrap;">';
									echo htmlentities($thisProductsValue["stockProductsProductNumber"]);
									echo '</td>';
									
									echo '<td style="white-space:nowrap;">';
									echo htmlentities($thisProductsValue["stockProductsProductNumber_b3"]);
									echo '</td>';

									echo '<td>';
									echo htmlentities($thisProductsValue["stockProductsSort"]);
									echo '</td>';

									echo '<td>';
									// echo $thisProductsValue["stockProductsActive"];
									if($thisProductsValue["stockProductsActive"] == '1'){
										$thisStatusImage = 'iconOk.png';
										$thisStatusTitle = 'aktiv';
									}
									else {
										$thisStatusImage = 'iconNotOk.png';
										$thisStatusTitle = 'nicht aktiv';
									}

									echo '<img src="layout/icons/' . $thisStatusImage . '" width="16" height="16" alt="' . $thisStatusTitle . '" title="' . $thisStatusTitle . '" />';
									echo '</td>';

									echo '<td>';
									$thisProductsIsForeignText = '';
									if($thisProductsValue["stockProductsIsForeign"] == '1'){
										$thisProductsIsForeignText = 'Fremdartikel';
										$thisProductsIsForeignText = $arrSupplierDatas[$thisProductsValue["stockProductsSupplier"]]["suppliersFirmenname"];
										echo htmlentities($thisProductsIsForeignText);										
										if($thisProductsValue["stockProductsSupplierProductNumber"] != ''){	
											#echo $thisProductsValue["stockProductsSupplierProductNumber"];
										}
									}
									echo '</td>';
									
									if($_COOKIE["isAdmin"] == '1'){
										echo '<td>';
										echo $thisProductsValue["stockProductsID"];
										echo '</td>';
										
										echo '<td>';
										echo $thisProductsValue["stockProductsLevelID"];
										echo '</td>';

										echo '<td>';
										echo $thisProductsValue["stockProductsParentID"];
										echo '</td>';
										
										echo '<td>';
										echo $thisProductsValue["stockProductsRelationID"];
										echo '</td>';
									}
									
									echo '<td>';
									if($arrGetUserRights["editStocks"]) {
										echo '<span class="toolItem"><a href="' . PAGE_PRODUCTS_STOCK_PRODUCTS . '?editID=' . $thisProductsValue["stockProductsID"] . '"><img src="layout/icons/iconEdit.gif" width="16" height="16" title="Artikel bearbeiten" alt="Bearbeiten" /></a></span>';
									}
									else {
										echo '<span class="toolItem"><img src="layout/spacer.gif" width="16" height="16" alt="" /></span>';
									}
									echo '</td>';

									echo '</tr>';

									$countRowLevel2++;
								}
							// EOF PRODUCTS
						}	

						echo '</table>';
					}
					else {
						echo '<p class="infoArea">Es wurden keine Daten gefunden!</p>';
					}
				}
				else {

					/*
						[21] => Array
						(
							[stockProductCategoriesID] => 21
							[stockProductCategoriesLevelID] => 001007
							[stockProductCategoriesParentID] => 001
							[stockProductCategoriesName] => Pluto 520s
							[stockProductCategoriesShortName] => PL52
							[stockProductCategoriesProductNumber] => xxx
							[stockProductCategoriesSort] => 0
							[stockProductCategoriesActive] => 1
						)
						*/
					?>							
					
					<div id="tabs" class="ui-tabs ui-widget ui-widget-content ui-corner-all">
						<?php
							$thisPageUrl = $_SERVER["PHP_SELF"] . '?editID=' . $_REQUEST["editID"];
						?>
						<ul class="ui-tabs-nav ui-helper-reset ui-helper-clearfix ui-widget-header ui-corner-all">
							<li class="ui-state-default ui-corner-top"><a href="<?php echo $thisPageUrl; ?>&amp;tab=tabs-1#tabs-1">Produkt-Daten</a></li>
							<?php if($_REQUEST["editID"] != 'NEW' && $_REQUEST["editID"] != ''){ ?>
							<!--<li class="ui-state-default ui-corner-top"><a href="<?php echo $thisPageUrl; ?>&amp;tab=tabs-2#tabs-2">Neuer Wareneingang</a></li>-->
							<!--<li class="ui-state-default ui-corner-top"><a href="<?php echo $thisPageUrl; ?>&amp;tab=tabs-5#tabs-5">Wareneing&auml;nge</a></li>-->
							<li class="ui-state-default ui-corner-top"><a href="<?php echo $thisPageUrl; ?>&amp;tab=tabs-3#tabs-3">Preise</a></li>
							<!--<li class="ui-state-default ui-corner-top"><a href="<?php echo $thisPageUrl; ?>&amp;tab=tabs-4#tabs-4">Verbrauch</a></li>-->
							<?php } ?>
						</ul>														

						<?php if($_REQUEST["tab"] == "tabs-1"){ ?>
						
						<div id="tabs-1">
							<form name="formEditProductsCategory" id="formEditProductsCategory" method="post" action="<?php echo $_SERVER["PHP_SELF"]; ?>#tabs-1" enctype="multipart/form-data" >
								<input type="hidden" name="editID" value="<?php echo $_REQUEST["editID"]; ?>" />
								<input type="hidden" name="originalProductsCategoryLevelID" class="inputField_70" value="<?php echo $arrSelectedProductsCategory["stockProductsLevelID"]; ?>" readonly="readonly" />
								<input type="hidden" name="editProductsCategoryLevelID" class="inputField_70" value="<?php echo $arrSelectedProductsCategory["stockProductsLevelID"]; ?>" readonly="readonly" />
								<input type="hidden" name="originalProductsCategoryParentID" class="inputField_70" value="<?php echo $arrSelectedProductsCategory["stockProductsParentID"]; ?>" readonly="readonly" />
								<input type="hidden" name="originalProductCategoriesRelationID" class="inputField_70" value="<?php echo $arrSelectedProductsCategory["stockProductsRelationID"]; ?>" readonly="readonly" />
								
								<fieldset class="colored1">
									<legend>Artikel-Daten</legend>									
									<table border="0" width="100%" cellspacing="0" cellpadding="0">										
										<tr>
											<td style="width:200px;"><b>Artikel-Name [DE]:</b></td>
											<td><input type="text" name="editProductsCategoryName" class="inputField_510" value="<?php echo htmlentities(($arrSelectedProductsCategory["stockProductsName"])); ?>" /></td>
										</tr>
										<tr>
											<td style="width:200px;"><b>Artikel-Name [TR]:</b></td>
											<td><input type="text" name="editProductsCategoryName_TR" class="inputField_510" value="<?php echo htmlentities(($arrSelectedProductsCategory["stockProductsName_TR"])); ?>" /></td>
										</tr>
										<tr>
											<td style="width:200px;"><b>Artikel-Kurzname:</b></td>
											<td><input type="text" name="editProductsCategoryShortName" class="inputField_70" value="<?php echo $arrSelectedProductsCategory["stockProductsShortName"]; ?>" /></td>
										</tr>
										<tr>
											<td style="width:200px;"><b>Artikel-Text:</b></td>
											<td><input type="text" name="editProductsCategoryText" class="inputField_510" value="<?php echo htmlentities(($arrSelectedProductsCategory["stockProductsText"])); ?>" /></td>
										</tr>
										<tr>
											<td style="width:200px;"><b>Artikel-Nr:</b></td>
											<td><input type="text" name="editProductCategoriesProductNumber" class="inputField_510" value="<?php echo $arrSelectedProductsCategory["stockProductsProductNumber"]; ?>" /></td>
										</tr>
										<tr>
											<td style="width:200px;"><b>Artikel-Nr [BCTR]:</b></td>
											<td><input type="text" name="editProductCategoriesProductNumber_bctr" class="inputField_510" value="<?php echo $arrSelectedProductsCategory["stockProductsProductNumber_bctr"]; ?>" /></td>
										</tr>
										<tr>
											<td style="width:200px;"><b>Artikel-Nr [B3]:</b></td>
											<td><input type="text" name="editProductCategoriesProductNumber_b3" class="inputField_510" value="<?php echo $arrSelectedProductsCategory["stockProductsProductNumber_b3"]; ?>" /></td>
										</tr>
										<tr>
											<td style="width:200px;"><b>Artikel-Sortierung:</b></td>
											<td><input type="text" name="editProductsCategorySort" class="inputField_70" value="<?php echo $arrSelectedProductsCategory["stockProductsSort"]; ?>" /></td>
										</tr>										
										<tr>
											<td style="width:200px;"><b>Artikel-Status:</b></td>
											<td>
												<?php
													$checked = '';
													if($arrSelectedProductsCategory["stockProductsActive"] == '1'){
														$checked = ' checked="checked" ';

													}
												?>
												<input type="checkbox" name="editProductsCategoryStatus" value="1" <?php echo $checked; ?> /> Aktiv?
											</td>
										</tr>
										<tr>
											<td style="width:200px;"><b>Oberkategorie: </b></td>
											<td>																										
												<select name="editProductsCategoryParentID" class="inputSelect_510">
													<?php
														$selected = '';
														if($arrSelectedProductsCategory["stockProductsParentID"] == '000'){
															$selected = ' selected="selected" ';
														}
													?>													
													<?php
														if(!empty($arrProductsCategoryData)){
															foreach($arrProductsCategoryData as $thisProductsCategoryKey => $thisProductsCategoryValue){
																if($thisProductsCategoryValue["stockProductCategoriesActive"] == '1'){
																	$selected = '';
																	if($arrSelectedProductsCategory["stockProductsParentID"] == $thisProductsCategoryValue["stockProductCategoriesLevelID"]){
																		$selected = ' selected="selected" ';
																	}
																	echo '<option value="' .  $thisProductsCategoryValue["stockProductCategoriesLevelID"] . '" ' . $selected . '>' .  htmlentities($thisProductsCategoryValue["stockProductCategoriesName"]) . ' [LID ' .  $thisProductsCategoryValue["stockProductCategoriesLevelID"] . ']</option>';
																}
															}
														}
													?>
												</select>
											</td>
										</tr>
										<?php
											#dd('arrSelectedProductsCategory');
											#dd('arrAllProductDatas');
										?>
										<tr>
											<td style="width:200px;"><b>Verkn&uuml;pftes Produkt: </b></td>
											<td>																									
												<select name="editProductCategoriesRelationID" id="editProductCategoriesRelationID" class="inputSelect_510">
													<?php
														$selected = '';
														if($arrSelectedProductsCategory["editProductCategoriesRelationID"] == ''){
															$selected = ' selected="selected" ';
														}
													?>
													<option value="" <?php echo $selected; ?>>Keine Verkn&uuml;pfung</option>
													<?php
														if(!empty($arrAllProductDatas)){
															foreach($arrAllProductDatas as $thisProductsCategoryKey => $thisProductsCategoryData){
																if($thisProductsCategoryKey == $arrSelectedProductsCategory["stockProductsParentID"]){
																	echo '<option class="level_1" value="">' .  $thisProductsCategoryData["stockProductCategoriesName"] . ' [LID ' .  $thisProductsCategoryData["categoriesLevelID"] . ']</option>';
																	foreach($thisProductsCategoryData["products"] as $thisProductsKey => $thisProductsData){
																		if($thisProductsData["productsActive"] == '1'){
																			$selected = '';
																			if($arrSelectedProductsCategory["stockProductsRelationID"] == $thisProductsData["productsLevelID"]){
																				$selected = ' selected="selected" ';
																			}
																			echo '<option class="level_2" value="' .  $thisProductsData["productsLevelID"] . '" ' . $selected . '>' . htmlentities($thisProductsData["productsName"]) . ' [LID ' .  $thisProductsData["productsLevelID"] . ']</option>';
																		}
																	}
																}
															}
														}
													?>
												</select>
											</td>
										</tr>										
									</table>
								</fieldset>
	
								<fieldset>
									<legend>Lieferant / Herkunft</legend>
									<table border="0" width="100%" cellspacing="0" cellpadding="0">
										<tr>
											<td style="width:200px;"><b>Fremdartikel:</b></td>
											<td>
												<?php
													$checked = '';
													if($arrSelectedProductsCategory["stockProductsIsForeign"] == '1'){
														$checked = ' checked="checked" ';
													}
												?>
												<input type="checkbox" name="editProductCategoriesProductIsForeign" value="1" <?php echo $checked; ?> />
												Artikel ist Fremdware?
											</td>
										</tr>
										<tr>
											<td style="width:200px;"><b>Lieferant:</b></td>
											<td>
												<select name="editProductCategoriesProductSupplier" class="inputSelect_510">
													<option value=""> - </option>
													<?php
														if(!empty($arrSupplierDatas)){
															foreach($arrSupplierDatas as $thisSupplierKey => $thisSupplierValue){
																$selected = '';
																if($thisSupplierKey == $arrSelectedProductsCategory["stockProductsSupplier"]){
																	$selected = ' selected="selected" ';
																}
																echo '<option value="' . $thisSupplierKey . '" ' . $selected . ' >' . htmlentities($thisSupplierValue["suppliersFirmenname"]) . ' [' . htmlentities($thisSupplierValue["suppliersKundennummer"]) . ']</option>';
															}
														}
													?>
												</select>
												
												<?php if((int)$arrSelectedProductsCategory["stockProductsSupplier"] > 0){ ?>												
												 <a href="<?php echo PAGE_EDIT_SUPPLIERS; ?>?editID=<?php echo $arrSelectedProductsCategory["stockProductsSupplier"]; ?>"> <img src="layout/menueIcons/menueSidebar/suppliers.png" width="16" height="16" alt="Lieferanten anzeigen" /></a>														
												<?php } ?>
											</td>
										</tr>
										<tr>
											<td style="width:200px;"><b>Lieferanten-Produktnr.:</b></td>
											<td>
												<input type="text" name="editProductCategoriesProductSupplierProductNumber" class="inputField_510" value="<?php echo htmlentities($arrSelectedProductsCategory["stockProductsSupplierProductNumber"]); ?>"  />
											</td>
										</tr>										
									</table>
									
								</fieldset>
								
								<fieldset class="colored1">
									<legend>Artikel-Attribute</legend>
									<p class="infoArea">Mehrfachauswahl möglich bei gedr&uuml;ckter Shift-Taste!</p>
									<table border="0" width="100%" cellspacing="0" cellpadding="0">
										<?php
											if(!empty($arrProductsOptionData)){
												foreach($arrProductsOptionData as $thisOptionKey => $thisOptionValue){
													if(!empty($arrProductsAttributeData[$thisOptionKey])){
														if($thisOptionValue["stockProductOptionsSelectable"] == '1'){
															echo '<tr>';
															echo '<td style="width:200px;"><b>' . htmlentities($thisOptionValue["stockProductOptionsName"]) . ' [' . $thisOptionValue["stockProductOptionsID"] . ']:</b></td>';
															echo '<td>';
															
															echo '<select name="stockProductsOptions[' . $thisOptionValue["stockProductOptionsID"] . '][]" class="inputSelect_510" style="height:100%;" multiple="multiple">';
															echo '<option value="">-</option>';
															
															foreach($arrProductsAttributeData[$thisOptionKey] as $thisAttributeKey => $thisAttributeValue){
																$selected = '';
																if(!empty($arrSelectedProductsDetailData[$thisOptionKey][$thisAttributeValue["stockProductOptionsID"]])){
																	$selected = ' selected="selected" ';
																}
																
																echo '<option value="' . $thisAttributeValue["stockProductOptionsID"] . '" ' . $selected . ' >' . htmlentities($thisAttributeValue["stockProductOptionsName"]) . '</option>';
															}
															
															echo '</select>';
															
															echo '</td>';
															echo '</tr>';
														}
													}
												}
											}
										?>											

									<tr>
											<td style="width:200px;"><b>Gewischt [14] :</b></td>
											<td><input type="text" name="productweight"  value="<?php echo 
											htmlentities(($arrSelectedProductsCategory["productweight"]));?>">
											<!--<input type="text" name="stockProductsOptions[14][]"  value="<?php echo 
											array_key_first($arrSelectedProductsDetailData[14]);?>">--></td>
										</tr>
										
									</table>
									<?php //echo "<pre>";print_r($arrSelectedProductsDetailData);exit();?>
								</fieldset>
								
								<fieldset class="colored1">
								
								<?php
								
								?>
								<?php
									// BOF GET CUSTOMER GROUPS
										$sql_getCustomerGroups = "
												SELECT 
														`" . TABLE_CUSTOMER_GROUPS . "`.`customerGroupsID`,
														`" . TABLE_CUSTOMER_GROUPS . "`.`customerGroupsName`											
													
													FROM `" . TABLE_CUSTOMER_GROUPS . "`
													
													WHERE 1													
														AND `" . TABLE_CUSTOMER_GROUPS . "`.`customerGroupsActive` = '1'
													
													ORDER BY 
														`" . TABLE_CUSTOMER_GROUPS . "`.`customerGroupsName` ASC
											";	
										$rs_getCustomerGroups = $dbConnection->db_query($sql_getCustomerGroups);
											
										$arrCustomerGroupsData = array();
										while($ds_getCustomerGroups = mysqli_fetch_assoc($rs_getCustomerGroups)){
											foreach(array_keys($ds_getCustomerGroups) as $field){
												$arrCustomerGroupsData[$ds_getCustomerGroups["customerGroupsID"]][$field] = $ds_getCustomerGroups[$field];
											}
										}
									// EOF GET CUSTOMER GROUPS
									
									// BOF GET CUSTOMER GROUPS PRODUCT DATA
										$sql_getCustomerGroupsProductData = "
												SELECT 								
														
														`" . TABLE_CUSTOMER_GROUPS_PRODUCTS . "`.`customerGroupsProductNumbersID`,
														`" . TABLE_CUSTOMER_GROUPS_PRODUCTS . "`.`productsProductID`,
														`" . TABLE_CUSTOMER_GROUPS_PRODUCTS . "`.`customerGroupsProductNumbersGroupID`,
														`" . TABLE_CUSTOMER_GROUPS_PRODUCTS . "`.`customerGroupsProductNumber`,
														`" . TABLE_CUSTOMER_GROUPS_PRODUCTS . "`.`customerGroupsProductNumber2`,
														`" . TABLE_CUSTOMER_GROUPS_PRODUCTS . "`.`productsModelNumber`,
														`" . TABLE_CUSTOMER_GROUPS_PRODUCTS . "`.`productsCategoriesID`,
														`" . TABLE_CUSTOMER_GROUPS_PRODUCTS . "`.`customerGroupsCompanyID`												
													
													FROM `" . TABLE_CUSTOMER_GROUPS_PRODUCTS . "`												
													
													WHERE 1
														AND `" . TABLE_CUSTOMER_GROUPS_PRODUCTS . "`.`productsModelNumber` = '" . $arrSelectedProductsCategory["stockProductsProductNumber"] . "'																									
													
											";	
										$rs_getCustomerGroupsProductData = $dbConnection->db_query($sql_getCustomerGroupsProductData);
										
										$arrCustomerGroupsProductData = array();
										while($ds_getCustomerGroupsProductData = mysqli_fetch_assoc($rs_getCustomerGroupsProductData)){
											foreach(array_keys($ds_getCustomerGroupsProductData) as $field){
												$arrCustomerGroupsProductData[$ds_getCustomerGroupsProductData["customerGroupsProductNumbersGroupID"]][$field] = $ds_getCustomerGroupsProductData[$field];
											}
										}	
									// EOF GET CUSTOMER GROUPS PRODUCT DATA
									
									#dd('sql_getCustomerGroupsProductData');
									#dd('arrSelectedProductsCategory');
									#dd('arrCustomerGroupsData');
									#dd('arrCustomerGroupsProductData');
									#dd('arrSelectedProductsCategory');
								?>								
									<legend>Artikel-Nummern von Verb&auml;nden</legend>											
									<table border="0" width="100%" cellspacing="0" cellpadding="0">
										<tr>
											<th>Gruppe</th>
											<th>Artikel-Nummer</th>
										</tr>
										<?php foreach($arrCustomerGroupsData as $thisCustomerGroupsKey => $thisCustomerGroupsValue){ ?>										
										<tr>
											<td style="width:200px;"><b><?php echo htmlentities($thisCustomerGroupsValue["customerGroupsName"]); ?>: </b></td>
											<td>
												<input type="text" name="stockProductsGroupProductsData[<?php echo $thisCustomerGroupsValue["customerGroupsID"]; ?>][customerGroupsProductNumber]" class="inputField_510" value="<?php echo htmlentities($arrCustomerGroupsProductData[$thisCustomerGroupsKey]["customerGroupsProductNumber"]); ?>" />
												
												<input type="hidden" name="stockProductsGroupProductsData[<?php echo $thisCustomerGroupsValue["customerGroupsID"]; ?>][customerGroupsID]" value="<?php echo $thisCustomerGroupsValue["customerGroupsID"]; ?>" />
												<input type="hidden" name="stockProductsGroupProductsData[<?php echo $thisCustomerGroupsValue["customerGroupsID"]; ?>][customerGroupsProductNumbersGroupID]" value="<?php echo $arrCustomerGroupsProductData[$thisCustomerGroupsKey]["customerGroupsProductNumbersGroupID"]; ?>" />
												<input type="hidden" name="stockProductsGroupProductsData[<?php echo $thisCustomerGroupsValue["customerGroupsID"]; ?>][customerGroupsProductNumber2]" value="<?php echo htmlentities($arrCustomerGroupsProductData[$thisCustomerGroupsKey]["customerGroupsProductNumber2"]); ?>" />
												<input type="hidden" name="stockProductsGroupProductsData[<?php echo $thisCustomerGroupsValue["customerGroupsID"]; ?>][productsModelNumber]" value="<?php echo htmlentities($arrSelectedProductsCategory["stockProductsLevelID"]); ?>" />
												<input type="hidden" name="stockProductsGroupProductsData[<?php echo $thisCustomerGroupsValue["customerGroupsID"]; ?>][productsCategoriesID]" value="<?php echo htmlentities($arrCustomerGroupsProductData[$thisCustomerGroupsKey]["productsCategoriesID"]); ?>" />
												<input type="hidden" name="stockProductsGroupProductsData[<?php echo $thisCustomerGroupsValue["customerGroupsID"]; ?>][customerGroupsCompanyID]" value="<?php echo htmlentities($arrCustomerGroupsProductData[$thisCustomerGroupsKey]["customerGroupsCompanyID"]); ?>" />
												<input type="hidden" name="stockProductsGroupProductsData[<?php echo $thisCustomerGroupsValue["customerGroupsID"]; ?>][customerGroupsProductNumbersID]" value="<?php echo htmlentities($arrCustomerGroupsProductData[$thisCustomerGroupsKey]["customerGroupsProductNumbersID"]); ?>" />												
												<input type="hidden" name="stockProductsGroupProductsData[<?php echo $thisCustomerGroupsValue["customerGroupsID"]; ?>][stockProductsLevelID]" value="<?php echo htmlentities($arrSelectedProductsCategory["stockProductsLevelID"]); ?>" />												
											</td>
										</tr>
										<?php } ?>
									</table>
								</fieldset>


								<div class="actionButtonsArea">
									<?php if($arrGetUserRights["editStocks"] == '1') { ?>
									<input type="submit" class="inputButton1 inputButtonGreen" name="storeDatas" value="Speichern" />
									&nbsp;
									<input type="submit" class="inputButton1 inputButtonRed" name="deleteDatas" value="Endg&uuml;ltig l&ouml;schen" onclick="return showWarning('Wollen Sie diesen Eintrag wirklich endgültig entfernen???\nOder soll der Eintrag nur deaktiviert werden?');" />
									&nbsp;
									<input type="submit" class="inputButton1 inputButtonOrange" name="cancelDatas" value="Abbrechen" onclick="return showWarning('Wollen Sie diesen Vorgang wirklich abbrechen??? ');" />
									<?php } ?>
								</div>
							</form>
						</div>
						<?php } ?>
						
						<?php if($_REQUEST["tab"] == "tabs-2") { ?>
						
						</div>
						<?php } ?>
						
						<?php if($_REQUEST["tab"] == "tabs-3"){ ?>
						<div id="tabs-3">

							<form name="formEditProductsPrice" method="post" action="<?php echo $_SERVER["PHP_SELF"]; ?>#tabs-3" enctype="multipart/form-data" >
								<input type="hidden" name="tab" value="<?php echo $_REQUEST["tab"]; ?>" />
								<input type="hidden" name="editID" value="<?php echo $arrSelectedProductsCategory["stockProductsID"]; ?>" />										
								<input type="hidden" name="productLevelID" value="<?php echo $arrSelectedProductsCategory["stockProductsLevelID"]; ?>" />
								<input type="hidden" name="productArtikelNumber" value="<?php echo $arrSelectedProductsCategory["stockProductsProductNumber"]; ?>" />

							

								<hr />

								<fieldset>
									<legend>Preise f&uuml;r Kunden-Typen</legend>

									<table width="100%" cellspacing="0" cellpadding="0" class="displayOrders">
										<tr>
											<th style="width:100px;">Staffelung</th>
											<th class="spacer"></th>

											<?php
												if(!empty($arrCustomerTypes)){													
													foreach($arrCustomerTypes as $thisCustomerTypesKey => $thisCustomerTypesValue){
														if(1 | $thisCustomerTypesValue["customerTypesHasPricelist"] == '1') {
															?>
															<th><?php echo $thisCustomerTypesValue["customerTypesName"]; ?></th>
															<?php
														}
													}
												}
											?>
										</tr>

										<?php
											$countCustomerTypesRow = 0;

											// BOF DISPLAY STORED CUSTOMER TYPE PRICES
												if(!empty($arrProductsPrices["customerType"])){
													$countRow = 0;
													foreach($arrProductsPrices["customerType"] as $thisCustomerTypePricesQuantity => $thisCustomerTypePricesValue) {
														if($countRow%2 == 0){ $rowClass = 'row0'; }
														else { $rowClass = 'row1'; }
														?>
															<tr class="<?php echo $rowClass; ?>">
																<td><input type="text" name="editCustomerTypeProductQuantity[<?php echo $countCustomerTypesRow; ?>]" class="inputField_70" value="<?php echo $thisCustomerTypePricesQuantity; ?>" style="text-align:right;" /></td>
																<td class="spacer"></td>
																<?php
																	if(!empty($arrCustomerTypes)){
																		foreach($arrCustomerTypes as $thisCustomerTypesKey => $thisCustomerTypesValue){
																			?>
																			<td>
																				<input type="hidden" name="editCustomerTypePriceID[<?php echo $countCustomerTypesRow; ?>][<?php echo $thisCustomerTypesKey; ?>]" class="inputField_70" value="<?php echo $thisCustomerTypePricesValue[$thisCustomerTypesKey]["stockProductPricesID"]; ?>" />
																				<input type="text" name="editCustomerTypeProductPrice[<?php echo $countCustomerTypesRow; ?>][<?php echo $thisCustomerTypesKey; ?>]" class="inputField_70" value="<?php echo $thisCustomerTypePricesValue[$thisCustomerTypesKey]["stockProductPricesPrice"]; ?>" style="text-align:right;" />  &euro;
																			</td>
																			<?php
																		}
																	}
																?>
															</tr>
														<?php
															$countCustomerTypesRow++;
															$countRow++;
													}
													?>
													<tr>
														<td colspan="<?php echo(count(array_keys($arrCustomerTypes)) + 2); ?>"><br /><b>Staffelungen hinzuf&uuml;gen:</b></td>
													</tr>
													<?php
												}
											// EOF DISPLAY STORED CUSTOMER TYPE PRICES

											// BOF DISPLAY ADD CUSTOMER TYPE PRICES
												for($i = 0 ; $i < MAX_STOCK_PRICE_ADDITIONAL_ROWS ; $i++){
													?>
														<tr>
															<td><input type="text" name="editCustomerTypeProductQuantity[<?php echo $countCustomerTypesRow; ?>]" class="inputField_70" value="" style="text-align:right;" /></td>
															<td class="spacer"></td>
															<?php
																if(!empty($arrCustomerTypes)){
																	foreach($arrCustomerTypes as $thisCustomerTypesKey => $thisCustomerTypesValue){
																		?>
																		<td>
																			<input type="hidden" name="editCustomerTypePriceID[<?php echo $countCustomerTypesRow; ?>][<?php echo $thisCustomerTypesKey; ?>]" class="inputField_70" value="%" />
																			<input type="text" name="editCustomerTypeProductPrice[<?php echo $countCustomerTypesRow; ?>][<?php echo $thisCustomerTypesKey; ?>]" class="inputField_70" value="" style="text-align:right;" />  &euro;
																		</td>
																		<?php
																	}
																}
															?>
														</tr>
													<?php
													$countCustomerTypesRow++;
												}
											// EOF DISPLAY ADD CUSTOMER TYPE PRICES
										?>
									</table>
								</fieldset>

								<div class="actionButtonsArea">
									<input type="submit" class="inputButton1 inputButtonGreen" name="storeProductPrices" value="Speichern" />
									<input type="submit" class="inputButton1 inputButtonOrange" name="cancelProductPrices" value="Abbrechen" />
								</div>
							</form>
						</div>						
						<?php } ?>
						
						<?php if($_REQUEST["tab"] == "tabs-4"){ ?>
						<div id="tabs-4">
							4
						</div>
						<?php } ?>
						
						<?php if($_REQUEST["tab"] == "tabs-5"){ ?>
						<div id="tabs-5">
							5
						</div>
						<?php } ?>
					</div>	
			<?php } ?>
		</div>
		</div>
		</div>
	<div class="clear"></div>
</div>

<script language="javascript" type="text/javascript">	
	<!--
	/* <![CDATA[ */
	$(document).ready(function() {
		$('.buttonToggleSidebarMenue').click(function() {
			$('#menueSidebarToggleContent').toggle();
		});

		$('.buttonSendDocument').click(function() {
			var mailDocumentDatas = $(this).attr('rel');
			var mailAdress = '';
			sendAttachedDocument($(this), mailDocumentDatas, '<?php echo $thisDocumentValue[$thisKey]["orderDocumentsCustomerNumber"]; ?>', '<?php echo $_REQUEST["editID"]; ?>', '', '<?php echo $_SERVER["PHP_SELF"]; ?>', mailAdress);
		});

		colorRowMouseOver('.displayOrders tbody tr');
		
		$('#editProductCategoriesRelationID').live('change', function() {
			if($(this).val() == ''){
				alert('Sie haben eine Kategorie gewählt. Es ist hier nur die Auswahl eines Produkts möglich!');
				$('#editProductCategoriesRelationID option:first').attr('selected', 'selected');
				
			}
		});
				
		$('.buttonShowProductDetails').css('cursor', 'pointer');
		$('.buttonShowProductDetails').click(function() {
			$('.productDetails').not($(this).next('.productDetails')).hide();
			$(this).next('.productDetails').toggle();
		});
		
	});
	/* ]]> */
	// -->
</script>
<?php require_once('inc/footerHTML.inc.php'); ?>