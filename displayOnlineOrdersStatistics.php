<?php
	require_once('inc/requires.inc.php');

	if(!$arrGetUserRights["displayOnlineOrders"] && !$arrGetUserRights["importOnlineOrders"]) {
		header('location: ' . PAGE_EXIT_LOCATION);
		exit;
	}

	if($_POST["displayOrdersCount"] !="") {
		$displayOrdersCount = $_POST["displayOrdersCount"];
	}
	else {
		$displayOrdersCount = DEFAULT_ONLINE_ORDERS_MONTHS;
	}

	/*
	$arrPaymentTypes = array (
		"banktransfer" => ("banktransfer"),
		"cash" => ("cash"),
		"cc" => ("cc"),
		"cod" => ("cod"),
		"invoice" => ("invoice"),
		"moneyorder" => ("moneyorder"),
	);
	*/

	$jswindowMessage = "";
	$warningMessage = '';
	$errorMessage = '';
	$successMessage = '';
	$infoMessage = '';

	$dbConnection = new DB_Connection();
	$db_open = $dbConnection->db_connect();

	$dbConnection_ExternShop = new DB_Connection(DB_HOST_EXTERN_SHOP, '', DB_NAME_EXTERN_SHOP, DB_USER_EXTERN_SHOP, DB_PASSWORD_EXTERN_SHOP);
	$dbOpen_ExternShop = $dbConnection_ExternShop->db_connect();

	$defaultORDER = "date_purchased";
	$defaultSORT = "ASC";

	if($_POST["searchInterval"] == ""){
		$_POST["searchInterval"] = "MONTH";
	}
?>

<?php
	require_once('inc/headerHTML.inc.php');
	$thisTitle = "Statistik Online-Bestellungen";

	if($_POST["searchInterval"] == 'WEEK') {
		$thisTitle .= ': <span class="headerSelectedEntry">pro Woche</span>';
	}
	else if($_POST["searchInterval"] == 'MONTH') {
		$thisTitle .= ': <span class="headerSelectedEntry">pro Monat</span>';
	}
	else if($_POST["searchInterval"] == 'QUARTER') {
		$thisTitle .= ': <span class="headerSelectedEntry">pro Vierteljahr</span>';
	}
	else if($_POST["searchInterval"] == 'YEAR') {
		$thisTitle .= ': <span class="headerSelectedEntry">pro Jahr</span>';
	}

	$headerHTML = preg_replace("/{###TITLE###}/", strip_tags($thisTitle), $headerHTML);
	echo $headerHTML;

	require_once(FILE_MENUE_TOP);
?>
<div id="mainArea">
	<div id="mainContent">
		<?php require_once(FILE_MENUE_SIDEBAR); ?>
		<div id="contentArea">
			<div id="contentAreaElements">
				<h1><?php if(SHOW_PAGE_TITLE_ICONS) { echo '<img src="' . PATH_ICONS_MENUE_TITLES . 'statistics.png" alt="" />'; } ?> <?php echo $thisTitle; ?></h1>

				<div id="searchFilterArea">
					<form name="formDisplayOrders" action="<?php echo $_SERVER["PHP_SELF"]; ?>" method="post">
						<table class="noborder" cellspacing="0" cellpadding="0">
							<tr>
								<td>
									<label for="searchInterval">Zeitraum:</label>
									<select name="searchInterval" id="searchInterval" class="inputField_130">
										<option value=""></option>
										<option value="WEEK" <?php if($_POST["searchInterval"] == 'WEEK'){ echo ' selected="selected" '; } ?> >pro Woche</option>
										<option value="MONTH" <?php if($_POST["searchInterval"] == 'MONTH'){ echo ' selected="selected" '; } ?> >pro Monat</option>
										<option value="QUARTER" <?php if($_POST["searchInterval"] == 'QUARTER'){ echo ' selected="selected" '; } ?> >pro Vierteljahr</option>
										<option value="YEAR" <?php if($_POST["searchInterval"] == 'YEAR'){ echo ' selected="selected" '; } ?> >pro Jahr</option>
									 </select>
								</td>
								<td>
									<input type="hidden" name="editID" id="editID" value="" />
									<input type="submit" name="submitSearch" class="inputButton0" value="Anzeigen" />
								</td>
							</tr>
						</table>
					</form>
				</div>

				<div class="contentDisplay">
					<?php
						if(1) {
							// BOF GET DATAS
							$where = "";
							$dateField = "";

							if($_POST["searchInterval"] == "WEEK"){
								$where .= "";
								$dateField = "
									CONCAT(
										DATE_FORMAT(`" . TABLE_SHOP_ORDERS . "`.`date_purchased`, '%Y'),
										'#',
										DATE_FORMAT(`" . TABLE_SHOP_ORDERS . "`.`date_purchased`, '%u')
									) AS `interval`
								";
							}
							else if($_POST["searchInterval"] == "MONTH"){
								// DATE_FORMAT(`" . TABLE_ORDER_INVOICES . "`.`date_purchased`, '%M') AS `interval`,
								$where .= "";

								$dateField = "
									CONCAT(
										DATE_FORMAT(`" . TABLE_SHOP_ORDERS . "`.`date_purchased`, '%Y'),
										'#',
										DATE_FORMAT(`" . TABLE_SHOP_ORDERS . "`.`date_purchased`, '%m')
									) AS `interval`
								";
							}
							else if($_POST["searchInterval"] == "QUARTER"){
								$where .= "";

								$dateField = "
									CONCAT(
										DATE_FORMAT(`" . TABLE_SHOP_ORDERS . "`.`date_purchased`, '%Y'),
										'#',
										IF(DATE_FORMAT(`" . TABLE_SHOP_ORDERS . "`.`date_purchased`, '%m') = '01' OR DATE_FORMAT(`" . TABLE_SHOP_ORDERS . "`.`date_purchased`, '%m') = '02' OR DATE_FORMAT(`" . TABLE_SHOP_ORDERS . "`.`date_purchased`, '%m') = '03', '1. Quartal',
											IF(DATE_FORMAT(`" . TABLE_SHOP_ORDERS . "`.`date_purchased`, '%m') = '04' OR DATE_FORMAT(`" . TABLE_SHOP_ORDERS . "`.`date_purchased`, '%m') = '05' OR DATE_FORMAT(`" . TABLE_SHOP_ORDERS . "`.`date_purchased`, '%m') = '06', '2. Quartal',
												IF(DATE_FORMAT(`" . TABLE_SHOP_ORDERS . "`.`date_purchased`, '%m') = '07' OR DATE_FORMAT(`" . TABLE_SHOP_ORDERS . "`.`date_purchased`, '%m') = '08' OR DATE_FORMAT(`" . TABLE_SHOP_ORDERS . "`.`date_purchased`, '%m') = '09', '3. Quartal',
													IF(DATE_FORMAT(`" . TABLE_SHOP_ORDERS . "`.`date_purchased`, '%m') = '10' OR DATE_FORMAT(`" . TABLE_SHOP_ORDERS . "`.`date_purchased`, '%m') = '11' OR DATE_FORMAT(`" . TABLE_SHOP_ORDERS . "`.`date_purchased`, '%m') = '12', '4. Quartal',
														''
													)
												)
											)
										)
									) AS `interval`
								";

							}
							else if($_POST["searchInterval"] == "YEAR"){
								$where .= "";

								$dateField = "
									CONCAT(
										DATE_FORMAT(`" . TABLE_SHOP_ORDERS . "`.`date_purchased`, '%Y'),
										'#',
										DATE_FORMAT(`" . TABLE_SHOP_ORDERS . "`.`date_purchased`, '%Y')
									) AS `interval`
								";

							}

							$arrIntervals = array();
							$arrDatas = array();

							$sql = "
								SELECT
									`tempTable2`.`interval`,

									SUM(`tempTable2`.`value`) AS `totalPrice`,

									SUM(`tempTable2`.`subtotal`) AS `subtotal`,
									SUM(`tempTable2`.`total`) AS `total`,

									COUNT(`tempTable2`.`date_purchased`) AS `countItems`
								";
							$x_sql .= "
									/*
									,
									GROUP_CONCAT(`products_options` SEPARATOR '###') AS `products_options`
									*/
								";
							$sql .= "
									FROM (
											SELECT
												`interval`,
												`tempTable1`.`value`,

												SUM(`tempTable1`.`subtotal`) AS `subtotal`,
												SUM(`tempTable1`.`total`) AS `total`,

												`tempTable1`.`date_purchased`
								";
							$x_sql .= "
												/*
												,
												GROUP_CONCAT(
													DISTINCT
													`tempTable1`.`orders_id`,
													':',
													IF(`tempTable1`.`orderType` IS NULL, '', `tempTable1`.`orderType`),
													':',
													IF(`tempTable1`.`countColors` IS NULL || `tempTable1`.`countColors` = '', 0, TRIM(`tempTable1`.`countColors`)),
													':',
													IF(`tempTable1`.`orderFilmCosts` IS NULL || `tempTable1`.`orderFilmCosts` = '', 0, TRIM(`tempTable1`.`orderFilmCosts`))
													SEPARATOR '#'
												) AS `products_options`
												*/
								";
							$sql .= "
												FROM (

													SELECT
														" . $dateField . ",

														IF(`" . TABLE_SHOP_ORDERS_TOTAL . "`.`class` = 'ot_subtotal', `" . TABLE_SHOP_ORDERS_TOTAL . "`.`value`, 0 ) AS `subtotal`,
														IF(`" . TABLE_SHOP_ORDERS_TOTAL . "`.`class` = 'ot_total', `" . TABLE_SHOP_ORDERS_TOTAL . "`.`value`, 0 ) AS `total`,

														`" . TABLE_SHOP_ORDERS_TOTAL . "`.`value`,
														`" . TABLE_SHOP_ORDERS . "`.`date_purchased`,
								";
							$x_sql .= "
														/*
														REPLACE(`" . TABLE_SHOP_ORDERS_PRODUCTS_ATTRIBUTES . "_1`.`products_options_values`, '-farbig', '') AS `countColors`,

														IF(
															`" . TABLE_SHOP_ORDERS_PRODUCTS_ATTRIBUTES . "_2`.`products_options_values` LIKE 'Nachbestellung%',
															'N',
															IF(
																`" . TABLE_SHOP_ORDERS_PRODUCTS_ATTRIBUTES . "_2`.`products_options_values` LIKE 'Erstbestellung%',
																'E',
																'N'
															)
														) AS `orderType`,

														IF(
															`" . TABLE_SHOP_ORDERS_PRODUCTS_ATTRIBUTES . "_3`.`products_options_values` LIKE '%zzgl%',
															REPLACE(
																REPLACE(
																	REPLACE(
																		REPLACE(
																			`" . TABLE_SHOP_ORDERS_PRODUCTS_ATTRIBUTES . "_3`.`products_options_values`,
																			SUBSTRING(
																				`" . TABLE_SHOP_ORDERS_PRODUCTS_ATTRIBUTES . "_3`.`products_options_values`,
																				1,
																				(LOCATE('zzgl', `" . TABLE_SHOP_ORDERS_PRODUCTS_ATTRIBUTES . "_3`.`products_options_values`) + LENGTH('zzgl'))
																			),
																			''
																		),
																		SUBSTRING(
																			`" . TABLE_SHOP_ORDERS_PRODUCTS_ATTRIBUTES . "_3`.`products_options_values`,
																			(LOCATE('EUR', `" . TABLE_SHOP_ORDERS_PRODUCTS_ATTRIBUTES . "_3`.`products_options_values`)),
																			LENGTH(`" . TABLE_SHOP_ORDERS_PRODUCTS_ATTRIBUTES . "_3`.`products_options_values`)
																		),
																		''
																	),
																	',',
																	'.'
																),
																'Filmkosten ',
																''
															),
															0
														) AS `orderFilmCosts`,
														*/
								";
							$sql .= "
														`" . TABLE_SHOP_ORDERS_TOTAL . "`.`orders_id`


													FROM `" . TABLE_SHOP_ORDERS_TOTAL . "`

													LEFT JOIN `" . TABLE_SHOP_ORDERS . "`
													ON (`" . TABLE_SHOP_ORDERS_TOTAL . "`.`orders_id` = `" . TABLE_SHOP_ORDERS . "`.`orders_id`)
								";
							$x_sql .= "
													/*
													LEFT JOIN `" . TABLE_SHOP_ORDERS_PRODUCTS_ATTRIBUTES . "` AS `" . TABLE_SHOP_ORDERS_PRODUCTS_ATTRIBUTES . "_1`
													ON (
														`" . TABLE_SHOP_ORDERS_TOTAL . "`.`orders_id` = `" . TABLE_SHOP_ORDERS_PRODUCTS_ATTRIBUTES . "_1`.`orders_id`
														AND (
															`" . TABLE_SHOP_ORDERS_PRODUCTS_ATTRIBUTES . "_1`.`products_options` = 'Farbinformation'
															OR
															`" . TABLE_SHOP_ORDERS_PRODUCTS_ATTRIBUTES . "_1`.`products_options` = 'Farb-Anzahl'
														)
													)

													LEFT JOIN `" . TABLE_SHOP_ORDERS_PRODUCTS_ATTRIBUTES . "` AS `" . TABLE_SHOP_ORDERS_PRODUCTS_ATTRIBUTES . "_2`
													ON (
														`" . TABLE_SHOP_ORDERS_TOTAL . "`.`orders_id` = `" . TABLE_SHOP_ORDERS_PRODUCTS_ATTRIBUTES . "_2`.`orders_id`
														AND `" . TABLE_SHOP_ORDERS_PRODUCTS_ATTRIBUTES . "_2`.`products_options` = 'Bestellart'
													)

													LEFT JOIN `" . TABLE_SHOP_ORDERS_PRODUCTS_ATTRIBUTES . "` AS `" . TABLE_SHOP_ORDERS_PRODUCTS_ATTRIBUTES . "_3`
													ON (
														`" . TABLE_SHOP_ORDERS_TOTAL . "`.`orders_id` = `" . TABLE_SHOP_ORDERS_PRODUCTS_ATTRIBUTES . "_3`.`orders_id`
														AND `" . TABLE_SHOP_ORDERS_PRODUCTS_ATTRIBUTES . "_3`.`products_options` = 'Druckart'
													)
													*/
									";
							$sql .= "
													WHERE 1
														AND (
															`" . TABLE_SHOP_ORDERS_TOTAL . "`.`class` = 'ot_subtotal'
															OR
															`" . TABLE_SHOP_ORDERS_TOTAL . "`.`class` = 'ot_total'
														)
								";
							$sql .= "
													/*GROUP BY `" . TABLE_SHOP_ORDERS_TOTAL . "`.`orders_id`*/
								";
							$sql .= "
												) AS `tempTable1`

										GROUP BY `tempTable1`.`orders_id`
									) AS `tempTable2`

									GROUP BY `interval`

									ORDER BY `interval` DESC
								";

							$rs = $dbConnection_ExternShop->db_query($sql);
#dd('sql');
							$countTotalRows = $dbConnection_ExternShop->db_getMysqlNumRows($rs);
							$arrProductsOptionsAddTotal = array();
							while($ds = mysqli_fetch_assoc($rs)) {
								$arrIntervals[] = $ds["interval"];
								$arrLabelsGraph[$ds["interval"]] = $ds["interval"];
								$arrDatasGraph["countItems"][$ds["interval"]] = $ds["countItems"];
								$arrDatasGraph["totalPrice"][$ds["interval"]] = $ds["total"];
								$arrDatasGraph["subtotalPrice"][$ds["interval"]] = $ds["subtotal"];


								$arrProductsOptionsDataAll = explode('###',  $ds["products_options"]);
								#dd('arrProductsOptionsDataAll');
								if(!empty($arrProductsOptionsDataAll)){
									foreach($arrProductsOptionsDataAll as $thisProductsOptionsDataAll){
										$arrProductsOptionsData = explode('#',  $thisProductsOptionsDataAll);
										if(!empty($arrProductsOptionsData)){
											sort($arrProductsOptionsData);
										}

										foreach($arrProductsOptionsData as $thisProductsOptionsData){
											#dd('thisProductsOptionsData');
											$arrTemp = explode(":", $thisProductsOptionsData);
											if($arrProductsOptionsAddTotal[$ds["interval"]] == ''){
												$arrProductsOptionsAddTotal[$ds["interval"]] = 0;
											}
											if($arrTemp[1] == 'E'){
												$arrProductsOptionsAddTotal[$ds["interval"]] += (intval($arrTemp[2]) * intval($arrTemp[3]));
											}
										}
									}
								}
								#$arrDatasGraph["totalPrice"][$ds["interval"]] += $arrProductsOptionsAddTotal[$ds["interval"]];
								foreach(array_keys($ds) as $field){
									$arrDatas[$ds["interval"]][$field] = $ds[$field];
								}
							}

							if(!empty($arrIntervals)){
								$arrIntervals = array_unique($arrIntervals);
								arsort($arrIntervals);
							}

							if($countTotalRows > 0) {
							?>
								<!-- BOF GRAPH ELEMENTS -->
								<script type="text/javascript" language="javascript" src="<?php echo PATH_RGRAPH; ?>libraries/RGraph.common.core.js" ></script>
								<script type="text/javascript" language="javascript" src="<?php echo PATH_RGRAPH; ?>libraries/RGraph.bar.js" ></script>
								<script type="text/javascript" language="javascript" src="<?php echo PATH_RGRAPH; ?>libraries/RGraph.line.js" ></script>

								<div id="tabs">
									<ul>
										<li><a href="#tabs-1">Bestellungseing&auml;nge</a></li>
										<li><a href="#tabs-2">Bestellwert ohne MwSt. und Versand</a></li>
										<li><a href="#tabs-3">Bestellsumme </a></li>
									</ul>
									<div id="tabs-1">
										<div class="adminEditArea">
											<h2>Bestellungseing&auml;nge</h2>
											<canvas id="cvs_dataCountItems" width="900" height="250">[No canvas support]</canvas>
										</div>
									</div>
									<div id="tabs-2">
										<div class="adminEditArea">
											<h2>Bestellwert ohne MwSt. und Versand</h2>
											<canvas id="cvs_dataTotalPrice" width="900" height="250">[No canvas support]</canvas>
										</div>
									</div>
									<div id="tabs-3">
										<div class="adminEditArea">
											<h2>Bestellsumme</h2>
											<canvas id="cvs_dataSubtotalPrice" width="900" height="250">[No canvas support]</canvas>
										</div>
									</div>
								</div>
								<!-- EOF GRAPH ELEMENTS -->
							<?php
								echo '<table cellspacing="0" cellpadding="0" width="100%" class="displayOrders">';

								echo '<tr>';
								echo '<th>Zeitraum</th>';
								echo '<th>Anzahl der Bestellungen</th>';
								echo '<th>Netto-Gesamtbetrag ohne Versand</th>';
								echo '<th>Brutto-Gesamtbetrag</th>';
								echo '</tr>';

								$countRow = 0;
								$thisMarker = "";

								foreach($arrIntervals as $thisIntervalKey => $thisIntervalValue) {
									if($arrDatas[$thisIntervalValue]['countItems'] > 0){
										if($countRow == 0) {

										}

										$rowClass = 'row0';
										if($countRow%2 == 0) {
											$rowClass = 'row1';
										}
										#dd('thisIntervalValue');
										$arrTemp = explode('#', $thisIntervalValue);
										if($thisMarker != $arrTemp[0]) {
											$thisMarker = $arrTemp[0];
											echo '<tr><td colspan="8" class="tableRowTitle1">' . $arrTemp[0] . '</td></tr>';
										}

										echo '<tr class="' . $rowClass . '">';

										echo '<td style="text-align:right;">';
										if($_POST["searchInterval"] == "MONTH"){
											$thisMonth = getTimeNames($arrTemp[1], 'month', 'long');
											echo $thisMonth;
										}
										else {
											if($_POST["searchInterval"] == "WEEK"){ echo 'KW '; }
											echo $arrTemp[1];
										}
										echo '</td>';
										echo '<td style="text-align:right;">'.$arrDatas[$thisIntervalValue]["countItems"].'</td>';

										echo '<td style="text-align:right;">';
										$thisSubTotalPrice = $arrDatas[$thisIntervalValue]["subtotal"];
										$thisTotalPrice = $arrDatas[$thisIntervalValue]["total"];
										#$thisTotalPrice += $arrProductsOptionsAddTotal[$thisIntervalValue];

										echo number_format($thisSubTotalPrice, 2, ',', '.');
										echo ' &euro;';
										echo '</td>';

										echo '<td style="text-align:right;">';
										echo number_format($thisTotalPrice , 2, ',', '.');
										echo ' &euro;';
										echo '</td>';

										echo "</tr>";
										$totalOrders += $arrDatas[$thisIntervalValue]["countItems"];
										$totalPrice += $arrDatas[$thisIntervalValue]["total"];
										$subtotalPrice += $arrDatas[$thisIntervalValue]["subtotal"];
										if($countRow == 0) { $timeInterval = htmlentities(utf8_decode($thisMonth)).' ' . $arrDatas[$thisIntervalValue]["year"]; }
										if($countRow == ($totalRows  - 1) && $countRow > 0) { $timeInterval = htmlentities(utf8_decode($thisMonth)).' ' . $arrDatas[$thisIntervalValue]["year"] . ' - ' . $timeInterval; }
										$countRow++;
									}
								}

								echo '<tr>';
								echo '<td colspan="4"><hr /></td>';
								echo '</tr>';

								echo '<tr class="row2">';
								echo '<td style="text-align:left;"><b>Insgesamt:</b></td>';
								echo '<td style="text-align:right;"><b>' . $totalOrders . '</b></td>';
								echo '<td style="text-align:right;"><b>' . number_format($subtotalPrice, 2, ',', '.') . ' &euro;</b></td>';
								echo '<td style="text-align:right;"><b>' . number_format($totalPrice, 2, ',', '.') . ' &euro;</b></td>';
								echo '</tr>';
								echo '</table>';
							}
							else {
								$warningMessage .= 'F&uuml;r den gew&auml;hlten Zeitraum wurden keine Daten gefunden.';
							}
							// EOF GET DATAS
						}
					?>
					<?php displayMessages(); ?>

				</div>
			</div>
		</div>
		<div class="clear"></div>
	</div>
</div>

<script language="javascript" type="text/javascript">
	$(document).ready(function() {
		$('.buttonShopOrderInfo').css('cursor', 'pointer');
		$('.buttonShopOrderInfo').click(function () {
			loadOrderDetails($(this), '<?php echo BASEPATH; ?>');
		});
		colorRowMouseOver('.displayOrders tbody tr');
		// toggleAreas();

		// BOF GET DATAS FOR GRAPH
			<?php
				if(!empty($arrDatasGraph["countItems"])){
					$arrDatasGraph["countItems"] = array_reverse($arrDatasGraph["countItems"], true);
				}
				if(!empty($arrDatasGraph["totalPrice"])){
					$arrDatasGraph["totalPrice"] = array_reverse($arrDatasGraph["totalPrice"], true);
				}
				if(!empty($arrDatasGraph["subtotalPrice"])){
					$arrDatasGraph["subtotalPrice"] = array_reverse($arrDatasGraph["subtotalPrice"], true);
				}
			?>

			createGraph('cvs_dataCountItems', [<?php echo implode(",", array_values($arrDatasGraph["countItems"])); ?>], [<?php echo "'" . implode("','", array_keys($arrDatasGraph["countItems"])) . "'"; ?>], 'Line', [['green','red']]);
			createGraph('cvs_dataTotalPrice', [<?php echo implode(",", array_values($arrDatasGraph["totalPrice"])); ?>], [<?php echo "'" . implode("','", array_keys($arrDatasGraph["totalPrice"])) . "'"; ?>], 'Line', [['green','red']]);
			createGraph('cvs_dataSubtotalPrice', [<?php echo implode(",", array_values($arrDatasGraph["subtotalPrice"])); ?>], [<?php echo "'" . implode("','", array_keys($arrDatasGraph["subtotalPrice"])) . "'"; ?>], 'Line', [['green','red']]);
		// EOF GET DATAS FOR GRAPH

		$(function() {
			$('#tabs').tabs();
		});
	});
</script>

<?php
	$dbConnection_ExternShop->db_close();
?>

<?php require_once('inc/footerHTML.inc.php'); ?>
