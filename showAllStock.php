<?php

	require_once('inc/requires.inc.php');

	if(!$arrGetUserRights["displayStocks"] && !$arrGetUserRights["editStocks"]) {
		header('location: ' . PAGE_EXIT_LOCATION);
		exit;
	}
	
	DEFINE('MAX_STOCK_PRICE_ADDITIONAL_ROWS', 2);

	$jswindowMessage = "";
	$warningMessage = '';
	$errorMessage = '';
	$successMessage = '';
	$infoMessage = '';
	
	
	if($_REQUEST["tab"] == ''){
		$_REQUEST["tab"] = "tabs-1";
	}

	$dbConnection = new DB_Connection();
	$db_open = $dbConnection->db_connect();

	// BOF READ VERTRETER
		$arrAgentDatas = getAgents();
		$arrAgentDatas2 = getAgents2();
		
	// EOF READ VERTRETER

	// BOF READ PAYMENT TYPES
		$arrPaymentTypeDatas = getPaymentTypes();
	// EOF READ PAYMENT TYPES

	// BOF READ CUSTOMER GROUP DATAS
		$arrCustomerGroupDatas = getCustomerGroups();
	// EOF READ CUSTOMER GROUP DATAS

	// BOF READ CUSTOMER TYPES
		$arrCustomerTypeDatas = getCustomerTypes();
	// EOF READ CUSTOMER TYPES	
	
	// BOF READ ORDER CATEGORIES		
		$arrAllProductDatas = getOrderProducts();		
	// EOF READ ORDER CATEGORIES
	
	
		$arrSupplierDatas = getSuppliers();
	
	


	
		$arrCustomerTypes = getCustomerTypes();
		// BOF ONLY USE CUSTOMER TYPES HAVING PRICELIST
		foreach($arrCustomerTypes as $thisCustomerTypeKey => $thisCustomerTypeValue){
			if($thisCustomerTypeValue["customerTypesHasPricelist"] != '1'){
				unset($arrCustomerTypes[$thisCustomerTypeKey]);
			}
		}
	// EOF GET CUSTOMER GROUPS
	
	
		
	// EOF STORE STOCK PRODUCT PRICES
	
	
	
	
		
	// BOF SEARCH


		$sqlWhere = "";
		
		if($_REQUEST["searchProductCategory"] != ""){
			$sqlWhere = " AND `productCategories`.`stockProductCategoriesParentID` = '" . $_REQUEST["searchProductCategory"] . "' ";
		}
		else if($_REQUEST["searchProductNumber"] != ""){
			$sqlWhere = " 
					AND (
						`productCategories`.`stockProductCategoriesProductNumber` = '" . $_REQUEST["searchProductNumber"] . "' 
						OR `productCategories`.`stockProductCategoriesProductNumber_bctr` = '" . $_REQUEST["searchProductNumber"] . "' 
						OR `productCategories`.`stockProductCategoriesProductNumber_b3` = '" . $_REQUEST["searchProductNumber"] . "' 
					)
				";
		}
		else if($_REQUEST["searchProductName"] != ""){
			$sqlWhere = " 
					AND (
						`productCategories`.`stockProductCategoriesName` LIKE '%" . $_REQUEST["searchProductName"] . "%' 
						OR `productCategories`.`stockProductCategoriesText` LIKE '%" . $_REQUEST["searchProductName"] . "%' 
					)
				";
		}
		else if($_REQUEST["searchWord"] != ""){
			$sqlWhere = " 
					AND (
						`productCategories`.`stockProductCategoriesName` LIKE '%" . $_REQUEST["searchWord"] . "%' 
						OR `productCategories`.`stockProductCategoriesText` LIKE '%" . $_REQUEST["searchWord"] . "%' 
						OR `productCategories`.`stockProductCategoriesProductNumber` LIKE '%" . $_REQUEST["searchWord"] . "%' 
						OR `productCategories`.`stockProductCategoriesProductNumber_bctr` LIKE '%" . $_REQUEST["searchWord"] . "%' 
						OR `productCategories`.`stockProductCategoriesProductNumber_b3` LIKE '%" . $_REQUEST["searchWord"] . "%' 
					)
				";
		}
		
		if($_REQUEST["searchDeactivatedProducts"] != "1"){
			$sqlWhere .= " AND `productCategories`.`stockProductCategoriesActive` = '1' ";
		}
		
		
		
		$sql_getCat = "
				SELECT
						`productCategories`.`stockProductCategoriesID` AS `stockProductsID`,
						

						GROUP_CONCAT(		
							DISTINCT
							`productOptions`.`stockProductOptionsName`,
							'|',							
							`productAttributes`.`stockProductOptionsName`
							ORDER BY `productOptions`.`stockProductOptionsSort` ASC, `productAttributes`.`stockProductOptionsName` ASC
							SEPARATOR '###'
						) AS `stockProductData`						
						
						/*
						`" . TABLE_STOCK_PRODUCT_DETAIL_DATA . "`.`stockProductDetailDataProductID`,
						`" . TABLE_STOCK_PRODUCT_DETAIL_DATA . "`.`stockProductDetailDataOptionID`,
						`" . TABLE_STOCK_PRODUCT_DETAIL_DATA . "`.`stockProductDetailDataAttributeID`,	
						
						`productOptions`.`stockProductOptionsID`,
						`productOptions`.`stockProductOptionsName`,						
						`productOptions`.`stockProductOptionsParentID`,
						`productOptions`.`stockProductOptionsSort`,
						`productOptions`.`stockProductOptionsActive`,
						`productOptions`.`stockProductOptionsSelectable`,
						
						`productAttributes`.`stockProductOptionsID`,
						`productAttributes`.`stockProductOptionsName`,
						`productAttributes`.`stockProductOptionsParentID`,
						`productAttributes`.`stockProductOptionsSort`,
						`productAttributes`.`stockProductOptionsActive`,
						`productAttributes`.`stockProductOptionsSelectable`	
						
						
						
						FROM `" . TABLE_STOCK_PRODUCT_CATEGORIES . "` AS `parentCategories`
						*/						
						
					FROM `" . TABLE_STOCK_PRODUCT_CATEGORIES . "` AS `productCategories`
					
					LEFT JOIN `" . TABLE_STOCK_PRODUCT_CATEGORIES . "` AS `parentCategories`
					ON(`productCategories`.`stockProductCategoriesParentID` = `parentCategories`.`stockProductCategoriesLevelID`)				
	
					LEFT JOIN `" . TABLE_STOCK_PRODUCT_DETAIL_DATA . "`
					ON(`productCategories`.`stockProductCategoriesID` = `" . TABLE_STOCK_PRODUCT_DETAIL_DATA . "`.`stockProductDetailDataProductID`)
					
					LEFT JOIN `" . TABLE_STOCK_PRODUCT_OPTIONS . "` AS `productOptions`
					ON(`" . TABLE_STOCK_PRODUCT_DETAIL_DATA . "`.`stockProductDetailDataOptionID` = `productOptions`.`stockProductOptionsID`)
					
					LEFT JOIN `" . TABLE_STOCK_PRODUCT_OPTIONS . "` AS `productAttributes`
					ON(`" . TABLE_STOCK_PRODUCT_DETAIL_DATA . "`.`stockProductDetailDataAttributeID` = `productAttributes`.`stockProductOptionsID`)					
					
					WHERE 1
						AND `productCategories`.`stockProductCategoriesParentID` != '000'
						
						" . $sqlWhere . "

					GROUP BY
						`productCategories`.`stockProductCategoriesID`
						
					ORDER BY
						`parentCategories`.`stockProductCategoriesSort`,
						
						`productCategories`.`stockProductCategoriesSort` ASC,
						`productCategories`.`stockProductCategoriesName` ASC,
						`productCategories`.`stockProductCategoriesParentID` ASC
						/*,
						`productCategories`.`stockProductCategoriesLevelID`	
						*/					
			";
			
		
		$rs_getCat = $dbConnection->db_query($sql_getCat);

		$arrProductsData = array();

		while($ds_getCat = mysqli_fetch_assoc($rs_getCat)){
			foreach(array_keys($ds_getCat) as $field){
				$arrProductsData[$ds_getCat["stockProductsParentID"]][$ds_getCat["stockProductsID"]][$field] = $ds_getCat[$field];
			}
		}
	// EOF GET ALL PRODUCTS
 //Delete Item 
 if($_GET['removeID'] != '')
 {
	 $sql = "DELETE FROM common_stock WHERE id_article='" . $_GET['removeID'] . "' ";
	   $dbConnection->db_query($sql);
	   
 }
	// BOF GET SELECTED PRODUCT
	
	
		
	// BOF lager
?>
<?php
	require_once('inc/headerHTML.inc.php');
	
	switch ($_GET["stockcountry"]) {
		case 'de':
			$thisTitle = "Lager Details DE";
			$queryWhere = "where stockCountry = 'de'";
			break;
		case 'tr':
			$thisTitle = "Lager Details TR";
			$queryWhere = "where stockCountry = 'tr'";
			break;
		
		default:
		$thisTitle = "Lager Details DE + TR";
		$queryWhere = "";
			break;
	}
	
	if($_REQUEST["editID"] != ""){
		if($_REQUEST["editID"] == "NEW"){
			$thisTitle .= ': <span class="headerSelectedEntry">Neues Produkt</span>';
		}
		else {
			$thisTitle .= ': <span class="headerSelectedEntry">' . htmlentities($arrSelectedProductsCategory["stockProductsName"]) . ' &bull; ' . htmlentities($arrSelectedProductsCategory["stockProductsProductNumber"]) . '</span>';
		}
	}

	$headerHTML = preg_replace("/{###TITLE###}/", strip_tags($thisTitle), $headerHTML);
	echo $headerHTML;

	require_once(FILE_MENUE_TOP);

?>

<?php require_once(FILE_MENUE_SIDEBAR); ?>
<div id="contentArea">
	<a name="top"></a>
	<div id="contentAreaElements">
		<h1><?php if(SHOW_PAGE_TITLE_ICONS) { echo '<img src="' . PATH_ICONS_MENUE_TITLES . 'shipping.png" alt="" />'; } ?> <?php echo $thisTitle; ?></h1>

		<?php displayMessages(); ?>
		<?php if($_REQUEST["editID"] == "" && $arrGetUserRights["editStocks"] == '1') { ?>
			<div class="menueActionsArea">
				<a href="addStock.php" class="linkButton">Neu Ware anlegen </a>
				<a href="showAllStock.php?stockcountry=all" class="linkButton">Lager DE + TR </a>
				<a href="showAllStock.php?stockcountry=de" class="linkButton">Lager DE </a>
				<a href="showAllStock.php?stockcountry=tr" class="linkButton">Lager TR </a>
				<div class="clear"></div>
			</div>
		<?php } ?>

	<!-- 	<div class="adminInfo">
			Datensatz-ID: <?php echo $_REQUEST["editID"]; ?>
			Level-ID: <?php echo $arrSelectedProductsCategory["stockProductsLevelID"]; ?>
		</div> -->		

		<div class="contentDisplay">
			<?php if($_REQUEST["editID"] == ''){ ?>
	<!-- 		<div id="searchFilterArea">
				<form name="formFilterSearch" method="post" action="<?php echo $_SERVER["PHP_SELF"]; ?>">
				<table border="0" cellpadding="0" cellspacing="0" class="searchFilterContent">
					<tr>
						<td>
							<label for="searchProductNumber">Artikelnummer:</label>
							<input type="text" name="searchProductNumber" id="searchProductNumber" class="inputField_70" />
						</td>
						<td>
							<label for="searchProductName">Artikelname:</label>
							<input type="text" name="searchProductName" id="searchProductName" class="inputField_120" />
						</td>
						<td>								
							<label for="searchProductCategory">Artikelkategorie:</label>
							<select name="searchProductCategory" id="searchProductCategory" class="inputField_120" >
								<option value=""></option>
								<?php
									if(!empty($arrProductsCategoryData)){
										foreach($arrProductsCategoryData as $thisKey => $thisValue){
											echo '<option value="' . $thisValue["stockProductCategoriesLevelID"] . '">' . htmlentities($thisValue["stockProductCategoriesName"]) . ' [' . $thisValue["stockProductCategoriesLevelID"] . ']</option>';
										}
									}
								?>
							</select>
						</td>
						<td>
							<label for="searchWord">Suchbegriff:</label>
							<input type="text" name="searchWord" id="searchWord" class="inputField_70" />
						</td>
						<td>
							<label for="searchDeactivatedProducts">Deaktivierte anzeigen?:</label>
							<input type="checkbox" name="searchDeactivatedProducts" id="searchDeactivatedProducts" value="1" />
						</td>
						<td>
							<input type="submit" name="submitformFilterSearch" id="submitformFilterSearch" class="inputButton0" value="Suchen" />
						</td>
					</tr>
				</table>
				</form>
			</div> -->
			<?php } ?>
			<!--BOF SEARCH-->

			<div id="searchFilterArea">
						<form name="formFilterSearch" method="POST" action="/Auftragslisten/showAllStock.php">
							<table border="0" cellpadding="0" cellspacing="0" class="searchFilterContent">
								<tbody><tr>
									<td>
										<label for="searchCustomerNumber">Suchen:</label>
										<input type="text" name="searchField" id="searchCustomerNumber" class="inputField_300" value="">
									</td> 
												<td>
												<input type="submit" name="submitSearch" class="inputButton0" value="Suchen">												</td>
																				<td>
										
									</td>
								</tr>
							</tbody></table>
						</form>
					</div>
			<div class="adminEditArea">
			
			<?php			
			
				if($_REQUEST["editID"] == ''){
					$countRowLevel1 = 0;
					$countRowLevel2 = 0;
# dd('arrProductsCategoryData');
					if(!empty($arrProductsData)){
						//echo "<input type='text' style=' width: 95%; margin-bottom: 6px; margin-top: 6px;' name='search'><input type='submit' value='search'>";
						echo '<table cellpadding="0" cellspacing="0" border="0" width="100%" class="displayOrders">
							<colgroup>								
								<col />
								<col />
								<col />
								<col />								
								<col />
								<col />
								<col />
								<col />
							';
						if($_COOKIE["isAdmin"] == '1'){
							echo '
									<col />
									<col />
									<col />
									<col />
								';
						}
						echo '
								<col />
							</colgroup>
						
							<tr>								
								<th style="width:45px;text-align:right;">#</th>
								<th>Artikel</th>
								<th>Lieferant</th>
								<th>Lagerort</th>
								<th>Menge</th>
								<th>Notizen</th>
								<th>Eingegangen am-</th>
								<th>Aktualiziert am-</th> 
								<th>Änderen</th> 
								<th>Sachbearbeiter</th> 
							';
					/* 	if($_COOKIE["isAdmin"] == '1'){
							echo '		
								<th>ID</th>
								<th>LID</th>
								<th>PID</th>
								<th>RID</th>
							';
						} */
						echo '
							 
							</tr>
						';
						// BOF SEARCH
						if(isset($_POST['submitSearch']))
						{
							$searchField = $_POST['searchField'];
							$stock_query = "SELECT * FROM `common_stock` WHERE 
							id_article LIKE '%".$searchField."%' 
							OR
							article LIKE '%".$searchField."%' 
							OR
							fournisseur LIKE '%".$searchField."%' 
							OR
							stockCountry LIKE '%".$searchField."%' 
							OR
							qty LIKE '%".$searchField."%' 
							OR
							note LIKE '%".$searchField."%' 
							OR
							date_recp LIKE '%".$searchField."%' 
							OR
							created_at LIKE '%".$searchField."%' 
							OR
							bearbeiter LIKE '%".$searchField."%' 
							order by id_article desc
							";

						}else{
						$stock_query = "SELECT * FROM `common_stock`". $queryWhere. " order by id_article desc";
						}
						 
						$rs_getCat = $dbConnection->db_query($stock_query);

					
						//ordersStatus
						//ordersArtikelBezeichnung

						$my_i = 0 ;
						while($ds_getCat = mysqli_fetch_assoc($rs_getCat))
						{ 		
							$x = $y = $z = 0 ;
 
							$my_i++;
							if($my_i%2 == 0){ $rowClass = 'row0'; }
							else { $rowClass = 'row1'; }


						/* 	while($ds_getCat = mysqli_fetch_assoc($rs_getCat))
							{  */ 
						 
 
							
							// BOF CATEGORIES
								echo '<tr style="font-weight:bold;" class="'.$rowClass.'">';	

								echo '<td style="text-align:left;" title="ID: ' .  $ds_getCat['id_article']   . '">';
						
								// echo  $ds_getCat['id_article'] ;
								echo '</td>';

								echo '<td>'; 
								echo "&#9632; ".$ds_getCat['article']."<a>";
								echo '</td>';

								echo '<td>';
								echo  $ds_getCat['fournisseur'] ;
								echo '</td>';

								echo '<td>';
								echo  strtoupper($ds_getCat['stockCountry'] );
								echo '</td>';

								echo '<td>';
								echo  $ds_getCat['qty'] ;
								echo '</td>';


								echo '<td>';
								echo  $ds_getCat['note'] ;
								echo '</td>';

								echo '<td>';
								echo  $ds_getCat['date_recp'] ;
								echo '</td>';
						
								echo '<td>';
								echo  $ds_getCat['created_at'] ;
							  
								echo '</td>';
							  
								 echo '<td>';
								 
								echo '<span class="toolItem"><a href="addStock.php?editID=' . $ds_getCat['id_article']. '"><img src="layout/icons/iconEdit.gif" width="16" height="16" title="lager  bearbeiten" alt="Bearbeiten" /></a></span>';
								echo '<span class="toolItem"><a href="showAllStock.php?removeID=' . $ds_getCat["id_article"] . '"><img src="layout/icons/iconDelete.png" width="16" height="16" title="Artikel bearbeiten" alt="Bearbeiten" /></a></span>';
								
								echo '</td>'; 
								echo '<td>';
								echo  $ds_getCat['bearbeiter'] ;
								echo '</td>';
								
								echo '</tr>';

					 

						 
						}	 
						echo '</table>';
					}
					else {
						echo '<p class="infoArea">Es wurden keine Daten gefunden!</p>';
					}
				}
				else {

				
					?>							
					
		
						
																			

						<?php if($_REQUEST["tab"] == "tabs-1"){ ?>
						
						
						<?php } ?>
						
						<?php if($_REQUEST["tab"] == "tabs-2") { ?>
						
						</div>
						<?php } ?>
						
						<?php if($_REQUEST["tab"] == "tabs-3"){ ?>
												
						<?php } ?>
						
						<?php if($_REQUEST["tab"] == "tabs-4"){ ?>
						
						<?php } ?>
						
						<?php if($_REQUEST["tab"] == "tabs-5"){ ?>
						
						<?php } ?>
					</div>	
			<?php } ?>
		</div>
		</div>
		</div>
	<div class="clear"></div>
</div>

<script language="javascript" type="text/javascript">	
	<!--
	/* <![CDATA[ */
	$(document).ready(function() {
		$('.buttonToggleSidebarMenue').click(function() {
			$('#menueSidebarToggleContent').toggle();
		});

		$('.buttonSendDocument').click(function() {
			var mailDocumentDatas = $(this).attr('rel');
			var mailAdress = '';
			sendAttachedDocument($(this), mailDocumentDatas, '<?php echo $thisDocumentValue[$thisKey]["orderDocumentsCustomerNumber"]; ?>', '<?php echo $_REQUEST["editID"]; ?>', '', '<?php echo $_SERVER["PHP_SELF"]; ?>', mailAdress);
		});

		colorRowMouseOver('.displayOrders tbody tr');
		
		$('#editProductCategoriesRelationID').live('change', function() {
			if($(this).val() == ''){
				alert('Sie haben eine Kategorie gewählt. Es ist hier nur die Auswahl eines Produkts möglich!');
				$('#editProductCategoriesRelationID option:first').attr('selected', 'selected');
				
			}
		});
				
		$('.buttonShowProductDetails').css('cursor', 'pointer');
		$('.buttonShowProductDetails').click(function() {
			$('.productDetails').not($(this).next('.productDetails')).hide();
			$(this).next('.productDetails').toggle();
		});
		
	});
	/* ]]> */
	// -->
</script>
<?php require_once('inc/footerHTML.inc.php'); ?>