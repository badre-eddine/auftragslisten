<?php
	require_once('inc/requires.inc.php');

	if(!$arrGetUserRights["editSalesmen"] && !$arrGetUserRights["displaySalesmen"]) {
		#header('location: ' . PAGE_EXIT_LOCATION);
		#exit;
	}

	$jswindowMessage = '';
	$warningMessage = '';
	$errorMessage = '';
	$successMessage = '';
	$infoMessage = '';

	$dbConnection = new DB_Connection();
	$db_open = $dbConnection->db_connect();

	$fileXML = PATH_XML_FILE_SALESMEN;

	// BOF GET DOUBLE ZIP CODES
		$arrDoubleZipcodes = getDoubleZipcodes();
		$arrUnusedDoubleZipcodes = getDoubleZipcodes('0');
	// BOF GET DOUBLE ZIP CODES

	// BOF DEFINE AREA BG-COLORS
		$arrColorsPLZ = array(
				'#FFF200',
				'#FDB913',
				'#F58220',
				'#E31D3C',
				'#F58F98',
				'#9A258F',
				'#0094C9',
				'#00C0E8',
				'#008060',
				'#23AA4A',
				'#80C342',
				'#905501',

				#'#FFCCFF',
				#'#FFCC99',
				#'#CC6600',
				#'#99CCCC',
				#'#66FF33',
				#'#FFFF33',
				#'#9acd32',
				#'#ee82ee',
				#'#d2b48c',
				#'#66cdaa',
				#'#98fb98',
				#'#00bfff',
				#'#f0b488',
			);
	// EOF DEFINE AREA BG-COLORS
?>
<?php
	require_once('inc/headerHTML.inc.php');
	$thisTitle = 'Vertreter PLZ-Karte';
	$thisIcon = 'iconMapPLZ.png';
	$headerHTML = preg_replace("/{###TITLE###}/", strip_tags($thisTitle), $headerHTML);

	$headerHTML = preg_replace('/<body/ism', '<body id="bodyFrame" onload="initialize();"', $headerHTML);

	echo $headerHTML;

	require_once(FILE_MENUE_TOP);
?>

<div id="mainArea">
	<div id="mainContent">
		<?php require_once(FILE_MENUE_SIDEBAR); ?>
		<div id="contentArea">
			<div id="contentAreaElements">
				<h1><?php if(SHOW_PAGE_TITLE_ICONS) { echo '<img src="' . PATH_ICONS_MENUE_TITLES . $thisIcon . '" alt="" />'; } ?> <?php echo $thisTitle; ?></h1>

				<?php
					if(!empty($arrUnusedDoubleZipcodes)){
						$arrTemp = array();
						foreach($arrUnusedDoubleZipcodes as $thisUnusedDoubleZipcodeKey => $thisUnusedDoubleZipcodeValue){
							$arrTemp[] = $thisUnusedDoubleZipcodeKey;
						}
						$unusedZipcodes = implode(', ', $arrTemp);
				?>
				<p class="infoArea">Folgende PLZ sind nicht in Gebrauch: <?php echo $unusedZipcodes; ?></p>
				<?php
					}
				?>
				<div class="contentDisplay">
					<!--- -->
					<?php
						// BOF GET SALESMAN TO ZIPCODE
							$arrRelationSalesmenZipcodeDatas = getRelationSalesmenZipcode();
							// BOF NEW SORT OF ZIPCODES
								$arrRelationSalesmenZipcodeDatasSorted = array();
								// BOF ADD BURHAN TO NON ADM-AREAS
									$arrRelationSalesmenZipcodeDatasSorted["00000"]["00"] = array(
														"kundenname" => "BURHAN CTR e. K.",
														"kundennummer" => "48464",
														"kundenID" => 13301,
														"kundenPLZ" => "00",
														"kundenPLZaktiv" => 0
													);
								// EOF ADD BURHAN TO NON ADM-AREAS

								if(!empty($arrRelationSalesmenZipcodeDatas)){
									foreach($arrRelationSalesmenZipcodeDatas as $thisKey => $thisValue){
										$thisKeyNew = $thisKey . str_repeat("0", (5 - strlen($thisKey)));
										$arrRelationSalesmenZipcodeDatasSorted[$thisKeyNew][$thisKey] = $arrRelationSalesmenZipcodeDatas[$thisKey];
									}
									ksort($arrRelationSalesmenZipcodeDatasSorted);
								}
							// EOF NEW SORT OF ZIPCODES
						// EOF GET SALESMAN TO ZIPCODE
					?>


					<div>
					<?php
						// BOF SET ZIP CODE ARRAY DATA
							$arrZipCodes = array();
							$arrZipCodesPerLength = array();
							$arrZipCodesToSalesman = array();
							function setZipCodes($arrRelationSalesmenZipcodeDatas){
								global $arrZipCodes, $arrZipCodesPerLength, $arrZipCodesToSalesman;
								if(!empty($arrRelationSalesmenZipcodeDatas)){
									foreach($arrRelationSalesmenZipcodeDatas as $thisKey => $thisValue){
										$arrZipCodes[] = $thisKey;
										$arrZipCodesPerLength[strlen($thisKey)][] = $thisKey;
										$arrZipCodesToSalesman[$thisKey] = $thisValue["kundenID"];
									}
								}
							}
							setZipCodes($arrRelationSalesmenZipcodeDatas);
						// EOF SET ZIP CODE ARRAY DATA

						// BOF SET SALESMAN DATA
							function getSalesmenData(){
								global $arrRelationSalesmenZipcodeDatas;
								$content = '';
								$content .= 'var arrSalesmanDatas = new Array();' . "\n";
								if(!empty($arrRelationSalesmenZipcodeDatas)){
									foreach($arrRelationSalesmenZipcodeDatas as $thisKey => $thisValue){
										// $content .= 'arrSalesmanDatas["' . $thisKey . '"] = new Array(); '. "\n";
										$thisTempID = $arrRelationSalesmenZipcodeDatas[$thisKey]["kundenID"];
										$content .= 'arrSalesmanDatas["' . $thisTempID . '"] = new Array();' . "\n";
										foreach($thisValue as $thisValueKey => $thisValueData){
											$content .= 'arrSalesmanDatas["' . $thisTempID . '"]["' . $thisValueKey . '"] = "' . $thisValueData . '";' . "\n";
										}
									}
								}
								return $content;
							}
							$getSalesmenData = getSalesmenData();
						// EOF SET SALESMAN DATA

						// BOF GET GOOGLE MAPS COUNTRY AREAS
							function getCountryBordersData(){
								global $dbConnection;
								$arrGoogleMapCountryAreas = array();
								$sql = "SELECT
											`lat` AS `latitude`,
											`lon` AS `longitude`,
											`country`,
											`subcountry`,
											IF(`subcountry` != '' , CONCAT(`country`, '_', `subcountry`), `country`) AS `area`

											FROM `" . TABLE_GEODB_AREAS . "`

											WHERE 1
												AND `subcountry` = ''
												AND `country` = 'DE'
									";
								$rs = $dbConnection->db_query($sql);

								while($ds = mysqli_fetch_assoc($rs)) {
									$arrGoogleMapCountryAreas[$ds["area"]][] = 'new google.maps.LatLng(' . $ds["latitude"] . ', ' . $ds["longitude"] . ')';
								}

								// BOF COUNTRY BORDERS
								if(!empty($arrGoogleMapCountryAreas)){
									$content = '';

									$countItem = 0;
									foreach($arrGoogleMapCountryAreas as $thisKey => $thisValue) {
										$content .= 'arrMyCountryAreaPoints[' . $countItem . '] = [' . implode(", ", $thisValue) . '];';
										$countItem++;
									}
									$content .= '
											function showCountryBorders(map){
												if(arrMyCountryAreaPoints.length > 0){
													var arrMyCountryAreaPolygon = new Array();
													for(i = 0 ; i < arrMyCountryAreaPoints.length ; i++){
														arrMyCountryAreaPolygon[i] = new google.maps.Polygon({
																paths: arrMyCountryAreaPoints[i],
																strokeColor: "#00FF3A",
																strokeOpacity: 0.8,
																strokeWeight: 2,
																fillColor: "transparent",
																fillOpacity: 0.0
																});
														arrMyCountryAreaPolygon[i].setMap(map);
													}
												}
											}
										';
								}
								// EOF COUNTRY BORDERS
								return $content;
							}
							$getCountryBordersData = getCountryBordersData();
						// EOF GET GOOGLE MAPS COUNTRY AREAS

						// BOF GET GOOGLE MAPS ZIPCODE AREAS
							function getGoogleZipcodeAreas(){
								global $dbConnection, $arrColorsPLZ, $arrZipCodes, $arrZipCodesPerLength, $arrZipCodesToSalesman;
								$arrSQL = array();

								if(!empty($arrZipCodesPerLength)){
									foreach($arrZipCodesPerLength as $thisKey => $thisValue){
										$thisKeyLength = $thisKey;
										if($thisKeyLength < 3){
											if($thisKeyLength == 2){
												$arrSQL[] = "
														SELECT
															`zipcodeDoubleDigit`,
															`coordinates`,
															`zipcodeDoubleDigit` AS `salesmanAreaKey`

															FROM `" . TABLE_GEODB_DOUBLEDIGIT_AREAS . "`

															WHERE 1
																AND (
																	`zipcodeDoubleDigit` = '" . implode("' OR `zipcodeDoubleDigit` = '", $thisValue) . "'
																)
													";
											}
											else if($thisKeyLength == 1){
												$arrSQL[] = "
														SELECT
															`zipcodeDoubleDigit`,
															`coordinates`,
															SUBSTRING(`zipcodeDoubleDigit`, 1, 1) AS `salesmanAreaKey`

															FROM `" . TABLE_GEODB_DOUBLEDIGIT_AREAS . "`

															WHERE 1
																AND (
																	`zipcodeDoubleDigit` LIKE '" . implode("%' OR `zipcodeDoubleDigit` LIKE '", $thisValue) . "%'
																)
													";
											}
										}
										else if($thisKeyLength < 6){
											if($thisKeyLength == 5){
												$arrSQL[] = "
														SELECT
															`zipcode` AS `zipcodeDoubleDigit`,
															`coordinates`,
															`zipcode` AS `salesmanAreaKey`
															FROM `" . TABLE_GEODB_ZIPCODE_AREAS . "`
															WHERE 1
																AND (
																	`zipcode` = '" . implode("' OR `zipcode` = '", $thisValue) . "'
																)
													";
											}
											else if($thisKeyLength == 4){
												$arrSQL[] = "
														SELECT
															`zipcode` AS `zipcodeDoubleDigit`,
															`coordinates`,
															SUBSTRING(`zipcode`, 1, 4) AS `salesmanAreaKey`
															FROM `" . TABLE_GEODB_ZIPCODE_AREAS . "`
															WHERE 1
																AND (
																	`zipcode` LIKE '" . implode("%' OR `zipcode` LIKE '", $thisValue) . "%'
																)
													";
											}
											else if($thisKeyLength == 3){
												$arrSQL[] = "
														SELECT
															`zipcode` AS `zipcodeDoubleDigit`,
															`coordinates`,
															SUBSTRING(`zipcode`, 1, 3) AS `salesmanAreaKey`
															FROM `" . TABLE_GEODB_ZIPCODE_AREAS . "`
															WHERE 1
																AND (
																	`zipcode` LIKE '" . implode("%' OR `zipcode` LIKE '", $thisValue) . "%'
																)
													";
											}
										}
									}
								}

								if(!empty($arrSQL)){
									$sql = implode(" UNION ", $arrSQL);

									$rs = $dbConnection->db_query($sql);
									$arrGoogleMapDoubleDigitZipCodeAreas = array();
									while($ds = mysqli_fetch_assoc($rs)) {
										$thisCoordinates = $ds["coordinates"];
										$thisSalesmanID = $arrZipCodesToSalesman[$ds["salesmanAreaKey"]];

										if(strlen($ds["zipcodeDoubleDigit"]) < 3){
											$searchPattern = "/(\[)([0-9\.\-]*),([0-9\.\-]*)(\])/ismU";
											$replace = "new google.maps.LatLng($2, $3)";
										}
										else if(strlen($ds["zipcodeDoubleDigit"]) > 2){
											$searchPattern = "/(\[)([0-9\.\-]*),([0-9\.\-]*)(\])/ismU";
											$replace = "new google.maps.LatLng($3, $2)";
										}
										$thisCoordinates = preg_replace($searchPattern, $replace, $thisCoordinates);
										$arrGoogleMapDoubleDigitZipCodeAreas[$thisSalesmanID][$ds["zipcodeDoubleDigit"]] = $thisCoordinates;
									}
									$arrGoogleMapZipCodeAreas = $arrGoogleMapDoubleDigitZipCodeAreas;
								}

								$content = '';
								if(!empty($arrGoogleMapZipCodeAreas)){
									foreach($arrGoogleMapZipCodeAreas as $thisSalesmanID => $thisSalesmanGoogleMapZipCodeAreas) {
										$content .= 'arrMyZipcodeAreaPoints[' . $thisSalesmanID . '] = new Array();';
										$countItem = 0;
										foreach($thisSalesmanGoogleMapZipCodeAreas as $thisKey => $thisValue) {
											$content .= 'arrMyZipcodeAreaPoints[' . $thisSalesmanID . '][' . $countItem . '] = [' . $thisValue . '];';
											#$content .= 'arrMyZipcodeAreaPoints[' . $countItem . '] = [' . $thisValue . '];';
											$countItem++;
										}
									}

									$content .= '
											function getColors(index){
												var getColor = "";
												var arrColors = new Array("' . implode('", "', $arrColorsPLZ) . '");
												getColor = arrColors[index];

												return getColor;
											}

											function showZipCodeAreas(map){
												var count = 0;
												if(arrMyZipcodeAreaPoints.length > 0){
													var arrMyZipcodeAreaPolygon = new Array();
													for(i in arrMyZipcodeAreaPoints){
														var thisColor = getColors(count);
														arrMyZipcodeAreaPolygon[i] = new Array();
														for(k = 0 ; k < arrMyZipcodeAreaPoints[i].length ; k++){
															infWindowKey = i+"_"+k;

															// BOF GET CENTER OF POLYGON
																var bounds = new google.maps.LatLngBounds();
																for (c = 0; c < arrMyZipcodeAreaPoints[i][k].length; c++) {
																	bounds.extend(arrMyZipcodeAreaPoints[i][k][c]);
																}
																var thisPolygonCenter = bounds.getCenter();
															// EOF GET CENTER OF POLYGON

															// DEFINE INFO WINDOW
																var html		= new Array();
																var imageExists	= false;
																html[infWindowKey] = "";
																html[infWindowKey] += "<div class=\"myGmapInfo\">";

																html[infWindowKey] += "  <p class=\"headline\">" + "PLZ-Gebiet: " + arrSalesmanDatas[i]["kundenPLZ"] + "</p>";

																html[infWindowKey] += "<table border=\"0\" width=\"\" cellpadding=\"0\" cellspacing=\"0\">";

																// html[infWindowKey] += "  <tr><td><b>PLZ-Gebiet:</b></td><td>" + arrSalesmanDatas[i]["kundenPLZ"] + "</td></tr>";
																html[infWindowKey] += "  <tr><td><b>ADM/Vertreter:</b></td><td>" + arrSalesmanDatas[i]["kundenname"] + "</td></tr>";
																html[infWindowKey] += "  <tr><td><b>KNR:</b></td><td>" + arrSalesmanDatas[i]["kundennummer"] + "</td></tr>";

																html[infWindowKey] += "</table>";

																html[infWindowKey] += "<a href=\"' . PAGE_EDIT_CUSTOMER . '?searchBoxCustomer=" + arrSalesmanDatas[i]["kundennummer"] + "\" class=\"linkButton\">Details anzeigen</a>";

																html[infWindowKey] += "<div class=\"clear\"</div>";
																html[infWindowKey] += "</div>";

																infoWindowArea = new google.maps.InfoWindow({
																	content: "lade....."
																});
															// END DEFINE INFO WINDOW

															// BOF SET MARKER IN CENTER OF POLYGON
																/*
																var thisMarkerImage;
																thisMarkerImage = "googleMapsIcon_" + count + ".png";
																thisMarkerShadowImage = "googleMapsIconShadow.png";
																thisMarkerSize = new Array(28, 20);
																thisMarkerShadowSize = new Array(28, 20);
																thisZindex = 9;

																var image = new google.maps.MarkerImage(
																			"layout/icons/" + thisMarkerImage,
																			new google.maps.Size(thisMarkerSize[0], thisMarkerSize[1]),
																			new google.maps.Point(0,0),
																			new google.maps.Point(0, thisMarkerSize[1])
																);

																var shadow = new google.maps.MarkerImage(
																			"layout/icons/" + thisMarkerShadowImage,
																			new google.maps.Size(thisMarkerShadowSize[0], thisMarkerShadowSize[1]),
																			new google.maps.Point(0, 0),
																			new google.maps.Point(0, thisMarkerShadowSize[1])
																);

																var shape = {
																	coord: [1, 1, 1, 28, 20, 28, 20 , 1],
																	type: "poly"
																};

																var myLatLng = thisPolygonCenter;

																markerPolygonCenter[infWindowKey] = new google.maps.Marker({
																	position: myLatLng,
																	map: map,
																	shadow: shadow,
																	icon: image,
																	shape: shape,
																	title: "PLZ-Gebiet: " + arrSalesmanDatas[i]["kundenPLZ"] + " | ADM: " + arrSalesmanDatas[i]["kundenname"],
																	title: "testTitle",
																	zIndex: thisZindex,
																	html: html[infWindowKey],
																	polCenter: thisPolygonCenter
																});

																markerPolygonCenterShadow[infWindowKey] = new google.maps.Marker({
																	position: myLatLng,
																	map: map,
																	icon: shadow,
																	shape: shape,
																	zIndex: thisZindex
																});
																*/
															// EOF SET MARKER IN CENTER OF POLYGON

															// BOF SET POLYGON AREA
																arrMyZipcodeAreaPolygon[infWindowKey] = new google.maps.Polygon({
																		paths: arrMyZipcodeAreaPoints[i][k],
																		strokeColor: thisColor,
																		strokeOpacity: 1,
																		strokeWeight: 0.2,
																		fillColor: thisColor,
																		fillOpacity: 0.5,
																		zIndex:1,
																		html: html[infWindowKey],
																		polCenter: thisPolygonCenter
																	});
																arrMyZipcodeAreaPolygon[infWindowKey].setMap(map);
															// EOF SET POLYGON AREA

															// SHOW INFO WINDOW
																google.maps.event.addListener(arrMyZipcodeAreaPolygon[infWindowKey], "click", function() {
																	infoWindowArea.close();
																	try{
																		infoWindowArea.setContent(this.html);
																		infoWindowArea.setPosition(this.polCenter);
																		infoWindowArea.open(map);
																	}
																	catch(e){
																		alert("error: " + e + " | i: " + i + " | k: " + k);
																	}
																});

																// createMarkerLinkInGmapSidebar(i, myPointDatas[i][k]["title"], marker[infWindowKey], imageExists);
															// END SHOW INFO WINDOW

														}
														count++;
													}
												}
											}
										';
								}
								return $content;
							}
							$getGoogleZipcodeAreasData = getGoogleZipcodeAreas();
						// EOF GET GOOGLE MAPS ZIPCODE AREAS
					?>
					<div class="clear"></div>
					<div id="contentAreaElements">
						<div id="myGoogleMapsArea" style="width:810px;">
							<div id="myGoogleMapCanvas">
								<span class="loadingArea"><img class="loader" src="layout/ajax-loader.gif" /> Bitte haben Sie ein wenig Geduld. Die Daten werden geladen...</span>
							</div>
							<div style="width:200px;float:right;">
								<?php
									if(!empty($arrRelationSalesmenZipcodeDatas)){
										echo '<div class="xxxsideInfo" style="position:relative;">';
											echo '<div class="sideInfoHeader">PLZ | Au&szlig;endienst-Mitarbeiter</div>';
											echo '<div class="sideInfoContent">';
											$count = 0;

											foreach($arrRelationSalesmenZipcodeDatasSorted as $arrRelationSalesmenZipcodeDatas){
												foreach($arrRelationSalesmenZipcodeDatas as $thisKey => $thisValue){
													if($count%2 == 0){ $rowClass = 'row0'; }
													else { $rowClass = 'row1'; }

													echo '<div class="sideInfoItem ' . $rowClass . '" id="' . $thisValue["kundenID"] . '" title="Diesen Au&szlig;endienst-Mitarbeiter per Klick als Vertreter &uuml;bernehmen" >';
														echo '<div class="sideInfoTitle" style="float:left;width:40px;">' . $thisKey . '</div>';
														echo '<div class="sideInfoText" style="white-space:nowrap;">' . $thisValue["kundenname"] . '</div>';
														echo '<div class="clear"></div>';
													echo '</div>';
													$count++;
												}
											}
											echo '</div>';
										echo '</div>';
									}
								?>
							</div>
							<div class="clear"></div>
						</div>
					</div>
					<script language="javascript" type="text/javascript">
						$('#myGoogleMapCanvas').css({
							'float': 'left',
							'width': '600px',
							'height': ($(window).height() - 140) + 'px'
						});
						$('.sideInfoContent').css({
							'height': ($(window).height() - 150) + 'px'
						});
					</script>

					<!-- GOOGLE MAPS START -->
					<script type="text/javascript" src="<?php echo PATH_GOOGLE_MAPS_API; ?>"></script>
					<script type="text/javascript" language="javascript">
						var elementId = "myGoogleMapCanvas";
						function initialize() {
							if (!document.getElementById(elementId)) {
								alert("Fehler: das Element mit der id " + elementId + " konnte nicht auf dieser Webseite gefunden werden!");
								return false;
							}
							else {
								/* BOF DEFINE DEFAULT VALUES */
									var myPointDatas	= new Array();


									var default_arrayKey	= 0;
									//var default_lat			= "52.103138";
									//var default_lon			= "7.622817";

									var default_lat			= "50.182568";
									var default_lon			= "10.695819";
									var default_zoom		= 6;

									var sidebarHtml		= "";              			/* HTML-Code für die gesamte Sidebar */
									var sidebarMarkers	= [];              			/* Array für die Marker */
									var sidebarCount	= 0;               			/* Zähler für die Marker (= Zeile in der Sidebar) */
									var marker				= new Array();
									var markerPolygonCenter	= new Array();
									var markerPolygonCenterShadow	= new Array();
									var activeMarker	= "";
									var activeMarkerPolygonCenter	= "";
									var infoWindowPoint			= new Array();
									infoWindowPolygon		= new Array();
									var infoWindowArea			= new Array();
									activeMarkerPolygon = "";

									var arrMyCountryAreaPoints = new Array();
									var arrMyZipcodeAreaPoints = new Array();
								/* EOF DEFINE DEFAULT VALUES */

								/* BOF DEFINE POINT DATAS */
									myPointDatas[0] = new Array();
									myPointDatas[0][0] = new Array();
									myPointDatas[0][0]["latitude"] = default_lat;
									myPointDatas[0][0]["longitude"] = default_lon;
									myPointDatas[0][0]["title"] = "BURHAN CTR";
									myPointDatas[0][0]["header"] = "BURHAN CTR";
									myPointDatas[0][0]["text"] = "";
									myPointDatas[0][0]["city"] = "";
									myPointDatas[0][0]["region_1"] = "";
									myPointDatas[0][0]["region_2"] = "";
									myPointDatas[0][0]["region_3"] = "";
									myPointDatas[0][0]["imagePath"] = "";
								/* EOF DEFINE POINT DATAS */

								var latlng = new google.maps.LatLng(myPointDatas[default_arrayKey][default_arrayKey]["latitude"], myPointDatas[default_arrayKey][default_arrayKey]["longitude"]);

								var myOptions = {
									zoom: default_zoom,
									center: latlng,
									panControl: true,
									zoomControl: true,
									navigationControl: true,
									mapTypeControl: true,
									scaleControl: true,
									overviewMapControl: true,
									streetViewControl: true,
									mapTypeId: google.maps.MapTypeId.TERRAIN
									/* ROADMAP zeigt die normalen Standard-2D-Kacheln von Google Maps an. */
									/* SATELLITE zeigt Fotokacheln an. */
									/* HYBRID zeigt eine Mischung aus Fotokacheln und eine Kachelebene für markante Merkmale an (Straßen, Ortsnamen). */
									/* TERRAIN zeigt Reliefkacheln für Erhebungen und GewÃ¤sser (Berge, Flüsse usw.) an. */
								};
								var map = new google.maps.Map(document.getElementById(elementId), myOptions);

								<?php echo $getSalesmenData; ?>

								<?php echo $getCountryBordersData; ?>
								if(arrMyCountryAreaPoints.length > 0){
									showCountryBorders(map);
								}

								<?php echo $getGoogleZipcodeAreasData; ?>
								if(arrMyZipcodeAreaPoints.length > 0){
									showZipCodeAreas(map);
								}

								// BOF SET POINTS
									function setGmapMarkers(map, i) {
										var thisMarkerImage;
										thisMarkerImage = 'bctr_googleMarkerFlag.png';
										thisMarkerShadowImage = 'bctr_googleMarkerFlagShadow.png';
										thisMarkerSize = new Array(81, 84);
										thisMarkerShadowSize = new Array(81, 84);
										thisZindex = (myPointDatas.length + 100000);

										var image = new google.maps.MarkerImage(
													'layout/icons/' + thisMarkerImage,
													new google.maps.Size(thisMarkerSize[0], thisMarkerSize[1]),
													new google.maps.Point(0,0),
													new google.maps.Point(0, thisMarkerSize[1])
										);

										var shadow = new google.maps.MarkerImage(
														'layout/icons/' + thisMarkerShadowImage,
														new google.maps.Size(thisMarkerShadowSize[0], thisMarkerShadowSize[1]),
														new google.maps.Point(0, 0),
														new google.maps.Point(0, thisMarkerShadowSize[1])
										);

										var shape = {
											// coord: [1, (thisMarkerSize[1] + 1 - 20), 1, thisMarkerSize[1], thisMarkerSize[0], thisMarkerSize[1], thisMarkerSize[0] , (thisMarkerSize[1] + 1 - 20)],
											coord: [1, 1, 1, 28, 20, 28, 20 , 1],
											type: 'poly'
										};

										var myLatLng = new google.maps.LatLng(myPointDatas[i]["latitude"], myPointDatas[i]["longitude"]);
										marker[i] = new google.maps.Marker({
											position: myLatLng,
											map: map,
											shadow: shadow,
											icon: image,
											shape: shape,
											title: myPointDatas[i]["title"],
											zIndex: thisZindex
										});

										shadow[i] = new google.maps.Marker({
											position: myLatLng,
											map: map,
											icon: shadow,
											shape: shape,
											title: myPointDatas[i]["title"],
											zIndex: thisZindex
										});

										// DEFINE INFO WINDOW
											/*
											var html		= new Array();
											var imageExists	= false;
											html[i] = "";
											html[i] += "<div class='myGmapInfo'>";
											// html[i] += "  <p class='headline'>" + myPointDatas[i]["title"] + "</p>";
											html[i] += "  <p class='headline'>" + myPointDatas[i]["header"] + "</p>";

											if(myPointDatas[i]["imagePath"] != "") {
											  imageExists = true;
											  html[i] += '  <img src="' + myPointDatas[i]["imagePath"] + '" + myPointDatas[i]["imageDimension"] + alt="' + myPointDatas[i]["title"] + '"/>';
											}

											html[i] += '<table border="0" width="" cellpadding="0" cellspacing="0">';

											if(myPointDatas[i]["text"] != "") {
											  // html[i] += "  <p class='text'>" + myPointDatas[i]["text"].replace(/, /g, ",<br />") + "</p>";
											  html[i] += "  <tr><td><b>Vertreter:</b></td><td>" + myPointDatas[i]["text"] + "</td></tr>";
											}

											if(myPointDatas[i]["city"] != "") {
											  // html[i] += "  <tr><td><b>PLZ:</b></td><td>" + myPointDatas[i]["city"] + "</td></tr>";
											}

											if(myPointDatas[i]["region_1"] != "") {
											  html[i] += "  <tr><td><b>Bundesland:</b></td><td>" + myPointDatas[i]["region_1"] + "</td></tr>";
											}

											if(myPointDatas[i]["region_2"] != "") {
											  html[i] += "  <tr><td><b>Region:</b></td><td>" + myPointDatas[i]["region_2"] + "</td></tr>";
											}

											if(myPointDatas[i]["region_3"] != "") {
											  html[i] += "  <tr><td><b>Kreis:</b></td><td>" + myPointDatas[i]["region_3"] + "</td></tr>";
											}

											html[i] += "  <tr><td><b>Longitude:</b></td><td>" + myPointDatas[i]["longitude"] + "</td></tr>";
											html[i] += "  <tr><td><b>Latitude: </b></td><td>" + myPointDatas[i]["latitude"] + "</td></tr>";

											html[i] += '</table>';

											html[i] += "<div class='clear'></div>";
											html[i] += "</div>";

											infowindow[i] = new google.maps.InfoWindow({
												content: html[i]
											});
											*/
										// END DEFINE INFO WINDOW

										// SHOW INFO WINDOW
											/*
											google.maps.event.addListener(marker[i], 'click', function() {
												if(activeMarker > -1) {
													infowindow[activeMarker].close();
												}
												infowindow[i].open(map,marker[i]);
												activeMarker = i;
											});
											*/
											// createMarkerLinkInGmapSidebar(i, myPointDatas[i]["title"], marker[i], imageExists);
										// END SHOW INFO WINDOW
									}
									for (i = 0 ; i < myPointDatas.length ; i++) {
										if(myPointDatas[i]){
											setGmapMarkers(map, i);
										}
									}
								// EOF SET POINTS
							}
						}
					</script>
					<!--- -->
				</div>
			</div>
		</div>

	</div>
</div>


<script language="javascript" type="text/javascript">
	$(document).ready(function() {
		<?php if($_REQUEST["editID"] != '' && $_REQUEST["editID"] != 'NEW'){ ?>
		$('body').attr('onload', 'initialize();');
		<?php } ?>

		$('.buttonLoadMap').click(function() {
			loadGoogleMapSalesmen('4');
		});
		<?php if($_REQUEST["openMap"] == true){ ?>
			loadGoogleMapSalesmen('4');
		<?php } ?>

		<?php if($_REQUEST["editID"] != "NEW"){ ?>
		$(function() {
			$('#tabs').tabs();
		});
		<?php } ?>
		colorRowMouseOver('.displayOrders tbody tr');
		$('.buttonNotice').click(function () {
			loadNotice($(this));
		});
		setFormDutyFields(<?php echo $jsonFormDutyFields; ?>);

	});
</script>

<?php require_once('inc/footerHTML.inc.php'); ?>