<?php
	require_once('inc/requires.inc.php');

	DEFINE ('KZH_EK_PRICE', 0.59);

	if(!$arrGetUserRights["editProvisionDatas"] && !$arrGetUserRights["displayProvisionDatas"]) {
		header('location: ' . PAGE_EXIT_LOCATION);
		exit;
	}

	if($_GET["downloadFile"] != "") {

		$thisDownloadFolder = DIRECTORY_CREATED_DOCUMENTS_SALESMEN;
		if($_GET["thisDocumentType"] == 'AB' || $_GET["thisDocumentType"] == 'RE'){
			$thisDownloadFolder = DIRECTORY_CREATED_DOCUMENTS_CUSTOMERS;
		}
		if(file_exists($thisDownloadFolder . $_GET["downloadFile"])) {
			header('Content-Description: File Transfer');
			header('Content-Type: application/octet-stream');
			header('Content-Disposition: attachment; filename='.basename($thisDownloadFolder . date("Y-m-d") .'_' . $_GET["downloadFile"]));
			header('Content-Transfer-Encoding: binary');
			header('Expires: 0');
			header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
			header('Pragma: public');
			header('Content-Length: ' . filesize(($thisDownloadFolder . $_GET["downloadFile"])));
			header('Connection: Close');
			readfile($thisDownloadFolder . $_GET["downloadFile"]);
			exit;
		}
		else { exit; }
		clearstatcache();
	}

	$jswindowMessage = '';
	$warningMessage = '';
	$errorMessage = '';
	$successMessage = '';
	$infoMessage = '';

	$dbConnection = new DB_Connection();
	$db_open = $dbConnection->db_connect();

	// BOF READ VERTRETER
		$arrSalesmenDatas = getSalesmen();
	// EOF READ VERTRETER

	// BOF READ BANK TYPES
		$arrBankAccountTypeDatas = getBankAccountTypes();
	// EOF READ BANK TYPES

	// BOF READ BESTELL ARTEN
		$arrOrderTypeDatas = getOrderTypes();
	// EOF READ BESTELL ARTEN

	// BOF READ BESTELL STATUS TYPEN
		$arrOrderStatusTypeDatas = getOrderStatusTypes();
	// EOF READ BESTELL STATUS TYPEN

	// BOF READ DOCUMENT TYPE DATAS
		$arrPaymentStatusTypeDatas = getPaymentStatusTypes();
	// EOF READ DOCUMENT TYPE DATAS

	// DEFINE('TABLE_ORDER_THIS', TABLE_ORDER_INVOICES);
	// DEFINE('TABLE_ORDER_THIS_DETAILS', TABLE_ORDER_INVOICES_DETAILS);

	// DEFINE('TABLE_ORDER_THIS', TABLE_ORDER_CONFIRMATIONS);
	// DEFINE('TABLE_ORDER_THIS_DETAILS', TABLE_ORDER_CONFIRMATIONS_DETAILS);


	// BOF SALESMAN DATAS
		$sql = "SELECT

					`" . TABLE_CUSTOMERS. "`.`customersID`,
					`" . TABLE_CUSTOMERS. "`.`customersKundennummer`,
					`" . TABLE_CUSTOMERS. "`.`customersFirmenname`,
					`" . TABLE_CUSTOMERS. "`.`customersFirmennameZusatz`,
					`" . TABLE_CUSTOMERS. "`.`customersFirmenInhaberVorname`,
					`" . TABLE_CUSTOMERS. "`.`customersFirmenInhaberNachname`,
					`" . TABLE_CUSTOMERS. "`.`customersFirmenInhaberAnrede`,
					`" . TABLE_CUSTOMERS. "`.`customersFirmenInhaber2Vorname`,
					`" . TABLE_CUSTOMERS. "`.`customersFirmenInhaber2Nachname`,
					`" . TABLE_CUSTOMERS. "`.`customersFirmenInhaber2Anrede`,
					`" . TABLE_CUSTOMERS. "`.`customersAnsprechpartner1Vorname`,
					`" . TABLE_CUSTOMERS. "`.`customersAnsprechpartner1Nachname`,
					`" . TABLE_CUSTOMERS. "`.`customersAnsprechpartner1Anrede`,
					`" . TABLE_CUSTOMERS. "`.`customersAnsprechpartner2Vorname`,
					`" . TABLE_CUSTOMERS. "`.`customersAnsprechpartner2Nachname`,
					`" . TABLE_CUSTOMERS. "`.`customersAnsprechpartner2Anrede`,
					`" . TABLE_CUSTOMERS. "`.`customersTelefon1`,
					`" . TABLE_CUSTOMERS. "`.`customersTelefon2`,
					`" . TABLE_CUSTOMERS. "`.`customersMobil1`,
					`" . TABLE_CUSTOMERS. "`.`customersMobil2`,
					`" . TABLE_CUSTOMERS. "`.`customersFax1`,
					`" . TABLE_CUSTOMERS. "`.`customersFax2`,
					`" . TABLE_CUSTOMERS. "`.`customersMail1`,
					`" . TABLE_CUSTOMERS. "`.`customersMail2`,
					`" . TABLE_CUSTOMERS. "`.`customersHomepage`,
					`" . TABLE_CUSTOMERS. "`.`customersCompanyStrasse`,
					`" . TABLE_CUSTOMERS. "`.`customersCompanyHausnummer`,
					`" . TABLE_CUSTOMERS. "`.`customersCompanyCountry`,
					`" . TABLE_CUSTOMERS. "`.`customersCompanyPLZ`,
					`" . TABLE_CUSTOMERS. "`.`customersCompanyOrt`,
					`" . TABLE_CUSTOMERS. "`.`customersLieferadresseFirmenname`,
					`" . TABLE_CUSTOMERS. "`.`customersLieferadresseStrasse`,
					`" . TABLE_CUSTOMERS. "`.`customersLieferadresseHausnummer`,
					`" . TABLE_CUSTOMERS. "`.`customersLieferadressePLZ`,
					`" . TABLE_CUSTOMERS. "`.`customersLieferadresseOrt`,
					`" . TABLE_CUSTOMERS. "`.`customersLieferadresseLand`,
					`" . TABLE_CUSTOMERS. "`.`customersRechnungsadresseFirmenname`,
					`" . TABLE_CUSTOMERS. "`.`customersRechnungsadresseStrasse`,
					`" . TABLE_CUSTOMERS. "`.`customersRechnungsadresseHausnummer`,
					`" . TABLE_CUSTOMERS. "`.`customersRechnungsadressePLZ`,
					`" . TABLE_CUSTOMERS. "`.`customersRechnungsadresseOrt`,
					`" . TABLE_CUSTOMERS. "`.`customersRechnungsadresseLand`,
					`" . TABLE_CUSTOMERS. "`.`customersBankName`,
					`" . TABLE_CUSTOMERS. "`.`customersKontoinhaber`,
					`" . TABLE_CUSTOMERS. "`.`customersBankKontonummer`,
					`" . TABLE_CUSTOMERS. "`.`customersBankLeitzahl`,
					`" . TABLE_CUSTOMERS. "`.`customersBankIBAN`,
					`" . TABLE_CUSTOMERS. "`.`customersBankBIC`,
					`" . TABLE_CUSTOMERS. "`.`customersBezahlart`,
					`" . TABLE_CUSTOMERS. "`.`customersZahlungskondition`,
					`" . TABLE_CUSTOMERS. "`.`customersRabatt`,
					`" . TABLE_CUSTOMERS. "`.`customersUseProductMwst`,
					`" . TABLE_CUSTOMERS. "`.`customersUseProductDiscount`,
					`" . TABLE_CUSTOMERS. "`.`customersUstID`,
					`" . TABLE_CUSTOMERS. "`.`customersVertreterID`,
					`" . TABLE_CUSTOMERS. "`.`customersVertreterName`,
					`" . TABLE_CUSTOMERS. "`.`customersUseSalesmanDeliveryAdress`,
					`" . TABLE_CUSTOMERS. "`.`customersUseSalesmanInvoiceAdress`,
					`" . TABLE_CUSTOMERS. "`.`customersTyp`,
					`" . TABLE_CUSTOMERS. "`.`customersGruppe`,
					`" . TABLE_CUSTOMERS. "`.`customersNotiz`,
					`" . TABLE_CUSTOMERS. "`.`customersUserID`,
					`" . TABLE_CUSTOMERS. "`.`customersTimeCreated`,
					`" . TABLE_CUSTOMERS. "`.`customersActive`,
					`" . TABLE_CUSTOMERS. "`.`customersDatasUpdated`,
					`" . TABLE_CUSTOMERS. "`.`customersTaxAccountID`,

					`" . TABLE_SALESMEN . "`.`salesmenID`,
					`" . TABLE_SALESMEN . "`.`salesmenKundennummer`,
					`" . TABLE_SALESMEN . "`.`salesmenFirmenname`,
					`" . TABLE_SALESMEN . "`.`salesmenSubSalesmanID`,
					`" . TABLE_SALESMEN . "`.`salesmenAreas`,
					`" . TABLE_SALESMEN . "`.`salesmenProvision`,
					`" . TABLE_SALESMEN . "`.`salesmenTypesID`,
					`" . TABLE_SALESMEN . "`.`salesmenTypesName`,
					`" . TABLE_SALESMEN . "`.`salesmenTimeCreated`,
					`" . TABLE_SALESMEN . "`.`salesmenActive`

				FROM `" . TABLE_CUSTOMERS. "`

				LEFT JOIN `" . TABLE_SALESMEN . "`
				ON(`" . TABLE_CUSTOMERS. "`.`customersKundennummer` = `" . TABLE_SALESMEN . "`.`salesmenKundennummer`)

				WHERE `" . TABLE_CUSTOMERS. "`.`customersID` = '" . $_POST["exportOrdersVertreterID"] . "'

				LIMIT 1
		";

		$rs = $dbConnection->db_query($sql);

		while($ds = mysqli_fetch_assoc($rs)) {
			foreach(array_keys($ds) as $field){
				$arrSalesmanDatas[$field] = $ds[$field];
			}
		}
	// EOF SALESMAN DATAS

	if(($_POST["submitFormProvision"] != '' || $_POST["createDocumentProvision"] != '') && $_POST["exportOrdersVertreterID"] > 0 && !empty($_POST["arrProvisionValues"])) {
		if(file_exists(DIRECTORY_CREATED_DOCUMENTS_SALESMEN . $_POST["tempHtmlFileName"])) {
			#$successMessage .= 'Die tempor&auml;re Datei &quot;' . basename(DIRECTORY_CREATED_DOCUMENTS_SALESMEN . $_POST["tempHtmlFileName"]) . '&quot; wurde gefunden.' . '<br />';
		}
		else  {
			#$errorMessage .= 'Die tempor&auml;re Datei &quot;' . basename(DIRECTORY_CREATED_DOCUMENTS_SALESMEN . $_POST["tempHtmlFileName"]) . '&quot; konnte nicht gefunden werden.' . '<br />';
		}

		$thisCreatedDocumentNumber = "PR-".date('ym') . '_' . $arrSalesmanDatas["customersKundennummer"];

		if($_POST["createDocumentProvision"] != '') {

			$fp = fopen(DIRECTORY_CREATED_DOCUMENTS_SALESMEN . $_POST["tempHtmlFileName"], 'r');
			$contentPDF = fread($fp, filesize(DIRECTORY_CREATED_DOCUMENTS_SALESMEN . $_POST["tempHtmlFileName"]));
			fclose($fp);

			$valueSum = 0;
			$valueMwst = 0;
			$valueTotal = 0;

			$contentPDF = preg_replace('/<input(.*)\/>/ismU', '', $contentPDF);

			foreach($_POST["arrProvisionValues"] as $thisKey => $thisValue) {
				$valueSum += str_replace(",", ".", $thisValue["salesmanProvisionValue"]);

				$contentPDF = preg_replace('/<!-- BOF VK_PRICE_'.$thisKey.' -->(.*)<!-- EOF VK_PRICE_'.$thisKey.' -->/ismU', $thisValue["salesmanProvisionProductVkprice"], $contentPDF);
				$contentPDF = preg_replace('/<!-- BOF PROVISION_PRICE_'.$thisKey.' -->(.*)<!-- EOF PROVISION_PRICE_'.$thisKey.' -->/ismU', $thisValue["salesmanProvisionValue"], $contentPDF);
			}

			$contentCSV = formatCSV($contentPDF);
			$contentPDF = preg_replace('/<!-- BOF DOCUMENT_NUMBER_ORDER -->(.*)<!-- EOF DOCUMENT_NUMBER_ORDER -->/ismU', '', $contentPDF);

			$valueProvision = 0;
			if($arrSalesmanDatas["salesmenProvision"] > 0){
				$valueProvision = $valueSum * ($arrSalesmanDatas["salesmenProvision"]/100);
			}
			$valueMwst = ($valueSum + $valueProvision) * MWST/100;
			$valueTotal = $valueSum + $valueProvision + $valueMwst;

			$contentPDF = preg_replace('/<!-- BOF SUM_VALUE --><!-- EOF SUM_VALUE -->/ismU', number_format($valueSum, 2, ',', ''), $contentPDF);
			$contentPDF = preg_replace('/<!-- BOF PROVISION_VALUE --><!-- EOF PROVISION_VALUE -->/ismU',  number_format($valueProvision, 2, ',', ''), $contentPDF);
			$contentPDF = preg_replace('/<!-- BOF MWST_VALUE --><!-- EOF MWST_VALUE -->/ismU',  number_format($valueMwst, 2, ',', ''), $contentPDF);
			$contentPDF = preg_replace('/<!-- BOF TOTAL_VALUE --><!-- EOF TOTAL_VALUE -->/ismU', number_format($valueTotal, 2, ',', ''), $contentPDF);

			$contentPDF = preg_replace('/ style="background-color:#FEFFAF;"/', '', $contentPDF);
			$contentPDF = preg_replace('/background-color:#FEFFAF;/', '', $contentPDF);
			$contentPDF = preg_replace('/<tbody>/ismU', '', $contentPDF);
			$contentPDF = preg_replace('/<\/tbody>/ismU', '', $contentPDF);
			$contentPDF = preg_replace('/<thead>/ismU', '', $contentPDF);
			$contentPDF = preg_replace('/<\/thead>/ismU', '', $contentPDF);
			$contentPDF = preg_replace('/ row[0-9]/ismU', '', $contentPDF);

			// BOF STORE CSV
			if(file_exists(DIRECTORY_CREATED_DOCUMENTS_SALESMEN . $_POST["thisCreatedCsvName"])) {
				// chmod(DIRECTORY_CREATED_DOCUMENTS_SALESMEN . $_POST["thisCreatedCsvName"] , "0777");
				unlink(DIRECTORY_CREATED_DOCUMENTS_SALESMEN . $_POST["thisCreatedCsvName"]);
			}
			clearstatcache();
			$fp = fopen(DIRECTORY_CREATED_DOCUMENTS_SALESMEN . $_POST["thisCreatedCsvName"], 'w');
			fwrite($fp, stripslashes($contentCSV));
			fclose($fp);
			// EOF STORE CSV

			$loadPdfContent = "";
			$loadContentTemplate = implode('', file(DIRECTORY_PDF_TEMPLATES . '/template_BASIC.html'));
			require_once(DIRECTORY_PDF_TEMPLATES . 'template_TEXT_DATAS.inc.php');

			$loadPdfContent = $loadContentTemplate;
			$loadPdfContent = preg_replace("/{###CARD_DATAS###}/", $contentPDF, $loadPdfContent);

			// STYLES
			$loadTemplateStyles = implode('', file(DIRECTORY_PDF_TEMPLATES . 'template_STYLES.html'));
			$loadPdfContent = preg_replace("/{###STYLES###}/", $loadTemplateStyles, $loadPdfContent);

			$loadPdfContent = preg_replace("/{###DOCUMENT_DATAS###}/", $arrDocumentDatas["PR"], $loadPdfContent);

			$loadPdfContent = preg_replace('/<a href="(.*)">(.*)<\/a>/ismU', '$2', $loadPdfContent);
			// DOKUMENT-TYP PROVISION anlegen

			$loadedAdressDatas = array(
				"ADRESS_COMPANY" => $arrSalesmanDatas["customersFirmenname"],
				"ADRESS_MANAGER" => "",
				"ADRESS_CONTACT" => $arrSalesmanDatas["customersFirmenInhaberVorname"] . " " . $arrSalesmanDatas["customersFirmenInhaberNachname"],
				"ADRESS_STREET" => $arrSalesmanDatas["customersCompanyStrasse"].' '.$arrSalesmanDatas["customersCompanyHausnummer"],
				"ADRESS_CITY" => $arrSalesmanDatas["customersCompanyPLZ"].' '.$arrSalesmanDatas["customersCompanyOrt"],
				"ADRESS_COUNTRY" => $arrCountryTypeDatas[$arrSalesmanDatas["customersCompanyCountry"]]["countries_name"],
				"ADRESS_MAIL" => $arrSalesmanDatas["customersMail1"],
			);

			$loadedDocumentDatas = array(
				"DOCUMENT_NUMBER_RE" => "",
				"CUSTOMERS_SALESMAN" => "",
				"CUSTOMER_NUMBER" => $arrSalesmanDatas["customersKundennummer"],
				"CUSTOMERS_TAX_ACCOUNT" => $arrSalesmanDatas["customersTaxAccountID"],
				"CUSTOMER_PHONE" => "",
				"CUSTOMER_MAIL" => "",
				"CUSTOMERS_ORDER_NUMBER" => "",

				"SHIPPING_COSTS" => "",
				"SHIPPING_TYPE" => "",
				"SHIPPING_TYPE_NUMBER" => "",
				"PACKAGING_COSTS" => "",
				"CARD_CASH_ON_DELIVERY_VALUE" => "",
				"SHIPPING_DATE" => "",
				"BINDING_DATE" => "",

				"COMPANY_HEADER_IMAGE" => DIRECTORY_PDF_IMAGES . "briefkopf.jpg",
				"COMPANY_NAME_LONG" => COMPANY_NAME_LONG,
				"COMPANY_PHONE" => COMPANY_PHONE,
				"COMPANY_FAX" => COMPANY_FAX,
				"COMPANY_INTERNET" => COMPANY_INTERNET,
				"COMPANY_MANAGER" => COMPANY_MANAGER,
				"COMPANY_MAIL" => COMPANY_MAIL,
				"COMPANY_STREET_NAME" => COMPANY_STREET_NAME,
				"COMPANY_STREET_NUMBER" => COMPANY_STREET_NUMBER,
				"COMPANY_ZIPCODE" => COMPANY_ZIPCODE,
				"COMPANY_CITY" => COMPANY_CITY,
				"COMPANY_TAX_NUMBER" => COMPANY_TAX_NUMBER,
				"COMPANY_UID_NUMBER" => COMPANY_UID_NUMBER,
				"COMPANY_VENUE" => COMPANY_VENUE,

				"BANK_ACCOUNT_TYPE" => $arrBankAccountTypeDatas['KSK']["bankAccountTypesShortName"],
				"BANK_ACCOUNT_CODE" => $arrBankAccountTypeDatas['KSK']["bankAccountTypesBankCode"],
				"BANK_ACCOUNT_NAME" => $arrBankAccountTypeDatas['KSK']["bankAccountTypesName"],
				"BANK_ACCOUNT_NUMBER" => $arrBankAccountTypeDatas['KSK']["bankAccountTypesAccountNumber"],
				"BANK_ACCOUNT_IBAN" => $arrBankAccountTypeDatas['KSK']["bankAccountTypesIBAN"],
				"BANK_ACCOUNT_BIC" => $arrBankAccountTypeDatas['KSK']["bankAccountTypesBIC"],

				"INTRO_TEXT" => $arrContentDatas["PR"]["INTRO_TEXT"],
				"OUTRO_TEXT" => $arrContentDatas["PR"]["OUTRO_TEXT"],
				"EDITOR_TEXT" => $arrContentDatas["PR"]["EDITOR_TEXT"],
				"REGARDS_TEXT" => $arrContentDatas["PR"]["REGARDS_TEXT"],
				"SALUTATION_TEXT" => $arrContentDatas["PR"]["SALUTATION_TEXT"],
				"PAYMENT_CONDITIONS" => $arrContentDatas["PR"]["PAYMENT_CONDITIONS"],
				"PAYMENT_CONDITION" => $arrPaymentConditionDatas["PR"]["paymentConditionsName"],
				"PAYMENT_TYPE" => "",
			);

			$loadedDocumentDatas["ADDRESS_INVOICE"] = "";
			$loadedDocumentDatas["ADDRESS_DELIVERY"] = "";
			$loadedDocumentDatas["INVOICE_SKONTO"] = "";

			$loadedDocumentDatas["WARNING_INVOICE_IS_PAID"] = "";

			$loadedDocumentDatas["CREATION_DATE"] = formatDate(date('Y-m-d'), 'display');

			$loadedDocumentDatas["DOCUMENT_NUMBER"] = $thisCreatedDocumentNumber;

			$loadSubjectText = 'Interne Provisionsgutschrift - Nr.: ' . '{###DOCUMENT_NUMBER###}';

			$loadSubject = ''.$loadSubjectText. '';

			$loadPdfContent = preg_replace("/{###SUBJECT###}/", $loadSubject, $loadPdfContent);

			if(!empty($loadedAdressDatas)) {
				foreach($loadedAdressDatas as $thisKey => $thisValue) {
					if($thisValue != "") {
						$loadPdfContent = preg_replace("/{###".$thisKey."###}/", $thisValue . "<br />", $loadPdfContent);
					}
					else {
						$loadPdfContent = preg_replace("/{###".$thisKey."###}/", "", $loadPdfContent);
					}
				}
			}

			if(!empty($loadedDocumentDatas)) {
				// if(constant('BANK_ACCOUNT_' . $_POST["selectBankAccount"]. '_SHOW_WARNING')){
				$loadPdfContent = preg_replace("/{###BANK_ACCOUNT_WARNING###}/", "", $loadPdfContent);
				foreach($loadedDocumentDatas as $thisKey => $thisValue) {
					$loadPdfContent = preg_replace("/{###".$thisKey."###}/", $thisValue, $loadPdfContent);
					if(trim($thisValue) == '') {
						$loadPdfContent = preg_replace("/<!-- BOF_".$thisKey." -->(.*)<!-- EOF_".$thisKey." -->/", '', $loadPdfContent);
					}
				}
			}

			$loadPdfContent = preg_replace('/class="displayOrders"/', '', $loadPdfContent);
			$loadPdfContent = removeUnnecessaryChars($loadPdfContent);

			#$loadPdfContent = preg_replace('/<\/page>/', '</page><page pageset="old">NEUE SEITE</page>', $loadPdfContent);

			$loadPdfContent = preg_replace('/pageset="old"/', '', $loadPdfContent);

			$pdfContentPage = $loadPdfContent;

			// BOF createPDF
			#require_once(DIRECTORY_HTML2PDF . 'html2pdf.class.php');
			require_once(DIRECTORY_HTML2PDF . 'html2pdf.class.php');

			/*
			if(file_exists(DIRECTORY_CREATED_DOCUMENTS_SALESMEN . $thisCreatedPdfName)) {
				try {
					chmod(DIRECTORY_CREATED_DOCUMENTS_SALESMEN . $thisCreatedPdfName , "0777");
					unlink(DIRECTORY_CREATED_DOCUMENTS_SALESMEN . $thisCreatedPdfName);
				}
				catch (Exception $e) {}
			}
			clearstatcache();
			*/

			try {
				// $html2pdf = new HTML2PDF('P', 'A4', 'de', true, 'UTF-8', array(0, 0, 0, 0));
				// $html2pdf = new HTML2PDF('L', 'A4', 'de', true, 'UTF-8', array(22, 5, 5, 5));
				$html2pdf = new HTML2PDF('P', 'A4', 'de', true, 'UTF-8', array(20, 10, 10, 10));
				#$html2pdf = new HTML2PDF('P', 'A4', 'de', true, 'UTF-8');
				// $html2pdf = new HTML2PDF('P', 'A4', 'de');
				// $html2pdf = new HTML2PDF('P', 'A4', 'de', true, 'UTF-8');
				// $html2pdf->setModeDebug();
				// $html2pdf->setDefaultFont('Arial');
				// $html2pdf->createIndex('Inhaltsverzeichnis', 12, 8, false, true, 2);

				if(file_exists(DIRECTORY_CREATED_DOCUMENTS_SALESMEN . $_POST["thisCreatedPdfName"])) {
					// chmod(DIRECTORY_CREATED_DOCUMENTS_SALESMEN . $_POST["thisCreatedPdfName"] , "0777");
					unlink(DIRECTORY_CREATED_DOCUMENTS_SALESMEN . $_POST["thisCreatedPdfName"]);
				}
				clearstatcache();

				ob_start();
				echo $pdfContentPage;
				$contentPDF = ob_get_clean();
				#$_GET['vuehtml'] = 1;
				$html2pdf->writeHTML($contentPDF, isset($_GET['vuehtml']));
				$html2pdf->Output($_POST["thisCreatedPdfName"], 'F', DIRECTORY_CREATED_DOCUMENTS_SALESMEN);

				if(file_exists(DIRECTORY_CREATED_DOCUMENTS_SALESMEN . $_POST["thisCreatedPdfName"])) {
					if(file_exists($_POST["thisCreatedPdfName"])) {
						unlink($_POST["thisCreatedPdfName"]);
					}

					/*
					$sql_update = array();
					foreach($thisDocumentOrdersIDs as $thisKey => $thisValue) {
						$sql_update[] = " `ordersToDocumentsOrderID` = '" . $thisValue . "' ";
					}

					$sql = "UPDATE `" . TABLE_ORDERS_TO_DOCUMENTS . "`
								SET `ordersToDocumentsStatus` = 'created'
								WHERE ". implode(" OR ", $sql_update) . "
					";

					$rs = $dbConnection->db_query($sql);
					*/
					}
				clearstatcache();
			}
			catch(HTML2PDF_exception $e) {
				echo $e;
				exit;
			}
			if(file_exists(DIRECTORY_CREATED_DOCUMENTS_SALESMEN . $_POST["thisCreatedPdfName"])) {

				$sql = "REPLACE INTO `" . TABLE_CREATED_DOCUMENTS . "` (
								`createdDocumentsID`,
								`createdDocumentsType`,
								`createdDocumentsNumber`,
								`createdDocumentsCustomerNumber`,
								`createdDocumentsTitle`,
								`createdDocumentsFilename`,
								`createdDocumentsContent`,
								`createdDocumentsUserID`,
								`createdDocumentsTimeCreated`,
								`createdDocumentsOrderIDs`
							)
							VALUES (
								'%',
								'PR',
								'" . $thisCreatedDocumentNumber . "',
								'" . $arrSalesmanDatas["customersKundennummer"] . "',
								'" . $thisCreatedDocumentNumber . "',
								'" . DIRECTORY_CREATED_DOCUMENTS_SALESMEN . $_POST["thisCreatedPdfName"] . "',
								'',
								'" . $_SESSION["usersID"] . "',
								NOW(),
								'" . $arrDocumentContentDocumentNumbers . "'
							)
					";

				// '".$_POST["documentOrdersIDs"] . "'

				$rs = $dbConnection->db_query($sql);

				$successMessage .= 'Die PDF-Datei &quot;' . $_POST["thisCreatedPdfName"] . '&quot wurde generiert. ' . '<br />';
				$showPdfDownloadLink = true;
			}
			else {
				$errorMessage .= 'Die PDF-Datei &quot;' . $_POST["thisCreatedPdfName"] . '&quot konnte nicht generiert werden. ' . '<br />';
				$showPdfDownloadLink = false;
			}

			clearstatcache();
			// EOF createPDF
		}

		// BOF STORE DATAS
			$arrTemp = array();
			$deleteWhere = '';
			foreach($_POST["arrProvisionValues"] as $thisKey => $thisValue) {
				$arrTemp[] = $thisValue["orderDocumentsNumber"];
			}
			if(!empty($arrTemp)){
				$arrTemp = array_unique($arrTemp);
				$arrDocumentContentDocumentNumbers = implode(";",$arrTemp);
				foreach($arrTemp as $thisKey => $thisValue) {
					$deleteWhere .= " OR `salesmenProvisionsContentDocumentNumber` LIKE '%" . $thisValue . "%'";
				}
			}

			$sql = "DELETE
				`" . TABLE_SALESMEN_PROVISIONS . "`,
				`" . TABLE_SALESMEN_PROVISIONS_DETAILS . "`

				FROM `" . TABLE_SALESMEN_PROVISIONS . "`

				LEFT JOIN `" . TABLE_SALESMEN_PROVISIONS_DETAILS . "`
				ON(`" . TABLE_SALESMEN_PROVISIONS . "`.`salesmenProvisionsID` = `" . TABLE_SALESMEN_PROVISIONS_DETAILS . "`.`salesmenProvisionsDetailsProvisionsID`)

				WHERE
					`" . TABLE_SALESMEN_PROVISIONS . "`.`salesmenProvisionsDocumentNumber` = '" . $thisCreatedDocumentNumber . "'
					" . $deleteWhere . "
			";

			$rs = $dbConnection->db_query($sql);

			$sql = "INSERT INTO `" . TABLE_SALESMEN_PROVISIONS . "` (

						`salesmenProvisionsID`,
						`salesmenProvisionsDocumentNumber`,
						`salesmenProvisionsSalesmanID`,
						`salesmenProvisionsDocumentDate`,
						`salesmenProvisionsSum`,
						`salesmenProvisionsMwst`,
						`salesmenProvisionsMwstValue`,
						`salesmenProvisionsTotal`,
						`salesmenProvisionsStatus`,
						`salesmenProvisionsDocumentPath`,
						`salesmenProvisionsContentDocumentNumber`
					)
					VALUES (
						'%',
						'" . $thisCreatedDocumentNumber . "',
						'" . $arrSalesmanDatas["customersID"] . "',
						'" . $loadedDocumentDatas["CREATION_DATE"] . "',
						'" . $valueSum . "',
						'" . MWST . "',
						'" . $valueMwst . "',
						'" . $valueTotal . "',
						'1',
						'" . DIRECTORY_CREATED_DOCUMENTS_SALESMEN . $_POST["thisCreatedPdfName"] . "',
						'" . $arrDocumentContentDocumentNumbers . "'
					)
				";
			$rs = $dbConnection->db_query($sql);

			if($rs) {
				$successMessage .= ' Der Datensatz wurde gespeichert.' .'<br />';
				$thisInsertID = mysqli_insert_id();
			}
			else {
				$errorMessage .= ' Der Datensatz konnte nicht gespeichert werden.' .'<br />';
			}

			$sql = "INSERT IGNORE INTO `" . TABLE_SALESMEN_PROVISIONS_DETAILS . "` (
						`salesmenProvisionsDetailsID`,
						`salesmenProvisionsDetailsProvisionsID`,
						`salesmenProvisionsDetailsDocumentsDetailID`,
						`salesmenProvisionsDetailsDocumentNumber`,
						`salesmenProvisionsDetailsProductOrderDate`,
						`salesmenProvisionsDetailsProductAmount`,
						`salesmenProvisionsDetailsProductNumber`,
						`salesmenProvisionsDetailsProductName`,
						`salesmenProvisionsDetailsProductRecipientCustomerNumber`,
						`salesmenProvisionsDetailsProductRecipientName`,
						`salesmenProvisionsDetailsProductRecipientZipcode`,
						`salesmenProvisionsDetailsProductRecipientCity`,
						`salesmenProvisionsDetailsProductVkPreis`,
						`salesmenProvisionsDetailsProductProvisionPrice`,
						`salesmenProvisionsDetailsDisabled`,
						`salesmenProvisionsDetailsType`
					)
					VALUES
			";



			foreach($_POST["arrProvisionValues"] as $thisKey => $thisValue) {

				$arrSQL[] = " (
							'%',
							'" . $thisInsertID . "',
							'" . $thisValue["orderDocumentDetailID"] . "',
							'" . $thisValue["orderDocumentsNumber"] . "',
							'" . ($thisValue["orderDocumentsOrderDate"]) . "',
							'" . $thisValue["orderDocumentDetailProductQuantity"] . "',
							'" . $thisValue["orderDocumentDetailProductNumber"] . "',
							'" . addslashes($thisValue["orderDocumentDetailProductName"]) . "',
							'" . $thisValue["orderDocumentsCustomerNumber"] . "',
							'" . addslashes($thisValue["orderDocumentsAddressCompany"]) . "',
							'" . $thisValue["orderDocumentsAddressZipcode"] . "',
							'" . $thisValue["orderDocumentsAddressCity"] . "',
							'" . str_replace(',', '.', $thisValue["salesmanProvisionProductVkprice"]) . "',
							'" . str_replace(',', '.', $thisValue["salesmanProvisionValue"]) . "',

							'" . $thisValue["salesmenProvisionsDetailsDisabled"] . "',
							'" . $thisValue["salesmenProvisionsDetailsType"] . "'
						)
					";
			}

			if(!empty($arrSQL)){
				$sql = $sql . implode(", ", $arrSQL);
				$rs = $dbConnection->db_query($sql);
			}

			$_POST["exportOrdersVertreterID"] = '';
			// BOF DELETE TEMP HTML FILE
			if(file_exists(DIRECTORY_CREATED_DOCUMENTS_SALESMEN . $_POST["tempHtmlFileName"])) {
				unlink(DIRECTORY_CREATED_DOCUMENTS_SALESMEN . $_POST["tempHtmlFileName"]);
			}
			clearstatcache();
			// EOF DELETE TEMP HTML FILE

		// BOF STORE DATAS

	}
?>
<?php
	require_once('inc/headerHTML.inc.php');
	$thisTitle = "Berechnung der Vertreter-Provision (Interne Gutschriften)";
	if($_POST["exportOrdersVertreterID"] != "") {
		$thisTitle .= ' - <span class="headerSelectedEntry">Vertreter '.$arrSalesmenDatas[$_POST["exportOrdersVertreterID"]]["customersFirmenname"] . '</span>';
	}
	$headerHTML = preg_replace("/{###TITLE###}/", strip_tags($thisTitle), $headerHTML);
	echo $headerHTML;

	require_once(FILE_MENUE_TOP);
?>

<div id="mainArea">
	<div id="mainContent">
		<?php require_once(FILE_MENUE_SIDEBAR); ?>
		<div id="contentArea">
			<a name="top"></a>
			<div id="contentAreaElements">
				<h1><?php if(SHOW_PAGE_TITLE_ICONS) { echo '<img src="' . PATH_ICONS_MENUE_TITLES . 'export.png" alt="" />'; } ?> <?php echo $thisTitle; ?></h1>

				<div id="searchFilterArea">
					<form name="formSearchDatas" method="post" action="<?php echo $_SERVER["PHP_SELF"]; ?>">
					<table border="0" cellpadding="0" cellspacing="0" width="" class="searchFilterContent">
						<tr>
							<td>
								<label for="exportOrdersVertreterID">Vertreter:</label>
								<select id="exportOrdersVertreterID" name="exportOrdersVertreterID" class="inputSelect_300">
									<option value=""> - </option>
									<?php
										if(!empty($arrSalesmenDatas)) {
											foreach($arrSalesmenDatas as $thisKey => $thisValue) {
												$selected = '';
												if($thisKey == $_POST["exportOrdersVertreterID"]) {
													$selected = ' selected ';
												}
												echo '
													<option value="' . $thisKey . '" '.$selected.' >' . ($arrSalesmenDatas[$thisKey]["customersFirmenname"]). ' | K-NR: '.$arrSalesmenDatas[$thisKey]["customersKundennummer"].'</option>';
											}
										}
									?>
								</select>
							</td>
							<td> von
								<?php
									if($_POST["exportOrdersDateFrom"] != '') {
										$fromDate = $_POST["exportOrdersDateFrom"];
									}
									else {
										$fromDate = formatDate(date("Y-m-d", mktime(1, 1, 1, (date("m") - 1), (1), date("Y"))), "display");
									}
								?>
								<input type="text" name="exportOrdersDateFrom" id="exportOrdersDateFrom" maxlength="2" class="inputField_70" readonly value="<?php echo $fromDate; ?>" />
								bis
								<?php
									if($_POST["exportOrdersDateTo"] != '') {
										$toDate = $_POST["exportOrdersDateTo"];
									}
									else {
										$toDate = formatDate(date("Y-m-d", mktime(1, 1, 1, date("m"), (0), date("Y"))), "display");
									}
								?>
								<input type="text" name="exportOrdersDateTo" id="exportOrdersDateTo" maxlength="2" class="inputField_70" readonly value="<?php echo $toDate; ?>" />
							</td>
							<td>
								<input type="checkbox" name="displayUnExportedLastMonth" value="1" <?php if($_POST["displayUnExportedLastMonth"] == '1'){ echo ' checked="checked" '; } ?> /> <span class="infotext">Unberechnete Provisionen des Vormonats anzeigen?</span>
							</td>
							<td>
								<input type="hidden" name="editID" id="editID" value="" />
								<input type="submit" name="submitExport" class="inputButton0" value="Vertreter-Auftr&auml;ge anzeigen" />
							</td>
						</tr>
					</table>
					<input type="hidden" name="submitExportForm" id="submitExportForm" value="1" />
					</form>
				</div>

				<div class="contentDisplay">
					<?php
						if($_POST["createDocumentProvision"] != '') {
							if($showPdfDownloadLink) {
								// echo '<p class="dataTableContent"><b>DOWNLOAD-LINK: </b><a href="'.DIRECTORY_CREATED_DOCUMENTS_SALESMEN . $thisCreatedPdfName.'" target="_blank">'.$thisCreatedPdfName.'</a></p>';
								echo '<p class="dataTableContent"><b>DOWNLOAD-LINK: </b><img src="layout/icons/iconPDF.gif" height="16" width="16" alt="" /> <a href="?downloadFile=' . $_POST["thisCreatedPdfName"].'">'.utf8_decode($_POST["thisCreatedPdfName"]).'</a></p>';
								#echo '<p class="dataTableContent"><b>DOWNLOAD-LINK: </b><img src="layout/icons/iconEXCEL.gif" height="16" width="16" alt="" /> <a href="?downloadFile=' . $_POST["thisCreatedCsvName"].'">'.utf8_decode($_POST["thisCreatedCsvName"]).'</a></p>';
							}
							echo '<hr />';
							// $_POST["editID"] = "";
						}
					?>
					<?php
						if($_POST["submitExportForm"] == 1 && $_POST["exportOrdersVertreterID"] != "" && $_POST["submitFormProvision"] == '') {
							echo '<table cellpadding="0" cellspacing="0" width="">';
							echo '<tr>';
							echo '<td><b>Vertreter-Name: </b></td>';
							echo '<td>';
							echo $arrSalesmanDatas["salesmenFirmenname"] . ' [' . $arrSalesmanDatas["customersKundennummer"] . ']';
							echo '</td>';
							echo '</tr>';

							if($arrSalesmanDatas["salesmenProvision"] > 0){
								echo '<tr>';
								echo '<td><b>Vertreter-Provision: </b></td>';
								echo '<td>' . number_format($arrSalesmanDatas["salesmenProvision"], 2, ',', '') . ' % vom Netto-Preis</td>';
								echo '</tr>';
							}

							echo '<tr>';
							echo '<td><b>Vertreter-Gebiete: </b></td>';
							echo '<td>';
							echo preg_replace("/;/", " / ", $arrSalesmanDatas["salesmenAreas"]);
							if(SALESMEN_TERRITORY_PROTECTION) {
								echo '<br /><p class="infotext">Es werden nur Auftr&auml;ge innerhalb des Vertretergebietes angezeigt.</p>';
							}
							echo '</td>';
							echo '</tr>';
							echo '</table>';

							echo '<div>';
							echo '<span title="Zu den Kundendaten von ' . $arrSalesmanDatas["salesmenFirmenname"] . '"><a href="' . PAGE_EDIT_CUSTOMER. '?searchCustomerNumber=' . $arrSalesmanDatas["customersKundennummer"] . '" class="linkButton" onclick="return showWarning(\'Wollen Sie diese Seite wirklich verlassen?\nNicht gespeicherte Daten gehen verloren.\');">Zu den Kundendaten</a></span>';
							echo '<span title="Zu den Vertreterdaten von ' . $arrSalesmanDatas["salesmenFirmenname"] . '"><a href="' . PAGE_EDIT_SALESMEN. '?editID=' . $arrSalesmanDatas["salesmenID"] . '" class="linkButton" class="linkButton" onclick="return showWarning(\'Wollen Sie diese Seite wirklich verlassen?\nNicht gespeicherte Daten gehen verloren.\');">Zu den Vertreterdaten</a></span>';
							echo '<div class="clear"></div>';
							echo '</div>';
							echo '<hr />';

							// BOF CREATE SQL FOR PROVISIONS FROM INVOICES DEPENDING ON SALESMAN AREAS

							$arrAreas = explode(';', $arrSalesmanDatas["salesmenAreas"]);
							$where = "";

							if(SALESMEN_TERRITORY_PROTECTION) {
								if(!empty($arrAreas)) {
									$where .= " AND (";
									$arrWhere = array();

									foreach($arrAreas as $thisZipcode) {
										if(preg_match("/-/", $thisZipcode)) {
											$arrZipcodes = explode("-", $thisZipcode);
											$arrWhere[] = " (`" . "{###THIS_TABLE###}" . "`.`orderDocumentsAddressZipcode` BETWEEN " . $arrZipcodes[0] . str_repeat("0", (5 - strlen($arrZipcodes[0]))) . " AND " . $arrZipcodes[1] . str_repeat("9", (5 - strlen($arrZipcodes[1]))) . ") ";
										}
										else {
											$arrWhere[] = " `" . "{###THIS_TABLE###}" . "`.`orderDocumentsAddressZipcode` LIKE '" . $thisZipcode . "%' ";
										}
									}
									$where .= implode (" OR ", $arrWhere);
									$where .= ") ";
								}
							}

							$sql_basic = "
									SELECT
										`tempTable`.`salesmenProvisionsDetailsID`,
										`tempTable`.`salesmenProvisionsDetailsProvisionsID`,
										`tempTable`.`salesmenProvisionsDetailsDocumentsDetailID`,
										`tempTable`.`salesmenProvisionsDetailsDocumentNumber`,
										`tempTable`.`salesmenProvisionsDetailsProductOrderDate`,
										`tempTable`.`salesmenProvisionsDetailsProductAmount`,
										`tempTable`.`salesmenProvisionsDetailsProductNumber`,
										`tempTable`.`salesmenProvisionsDetailsProductName`,
										`tempTable`.`salesmenProvisionsDetailsProductRecipientCustomerNumber`,
										`tempTable`.`salesmenProvisionsDetailsProductRecipientName`,
										`tempTable`.`salesmenProvisionsDetailsProductRecipientZipcode`,
										`tempTable`.`salesmenProvisionsDetailsProductRecipientCity`,
										`tempTable`.`salesmenProvisionsDetailsProductVkPreis`,
										`tempTable`.`salesmenProvisionsDetailsProductProvisionPrice`,
										`tempTable`.`salesmenProvisionsDetailsDisabled`,
										`tempTable`.`salesmenProvisionsDetailsType`,

										`tempTable`.`orderDocumentsID`,
										`tempTable`.`orderDocumentsNumber`,
										`tempTable`.`orderDocumentsType`,
										`tempTable`.`orderDocumentsOrdersIDs`,
										`tempTable`.`orderDocumentsCustomerNumber`,
										`tempTable`.`orderDocumentsCustomersOrderNumber`,
										`tempTable`.`orderDocumentsProcessingDate`,
										`tempTable`.`orderDocumentsDocumentDate`,
										`tempTable`.`orderDocumentsOrderDate`,
										`tempTable`.`orderDocumentsInvoiceDate`,
										`tempTable`.`orderDocumentsCreditDate`,
										`tempTable`.`orderDocumentsDeliveryDate`,
										`tempTable`.`orderDocumentsBindingDate`,
										`tempTable`.`orderDocumentsSalesman`,
										`tempTable`.`orderDocumentsKommission`,
										`tempTable`.`orderDocumentsAddressCompany`,
										`tempTable`.`orderDocumentsAddressName`,
										`tempTable`.`orderDocumentsAddressStreet`,
										`tempTable`.`orderDocumentsAddressStreetNumber`,
										`tempTable`.`orderDocumentsAddressZipcode`,
										`tempTable`.`orderDocumentsAddressCity`,
										`tempTable`.`orderDocumentsAddressCountry`,
										`tempTable`.`orderDocumentsAddressMail`,
										`tempTable`.`orderDocumentsSumPrice`,
										`tempTable`.`orderDocumentsDiscount`,
										`tempTable`.`orderDocumentsDiscountType`,
										`tempTable`.`orderDocumentsDiscountPercent`,
										`tempTable`.`orderDocumentsMwst`,
										`tempTable`.`orderDocumentsMwstPrice`,
										`tempTable`.`orderDocumentsShippingCosts`,
										`tempTable`.`orderDocumentsShippingCostsPackages`,
										`tempTable`.`orderDocumentsShippingCostsPerPackage`,
										`tempTable`.`orderDocumentsPackagingCosts`,
										`tempTable`.`orderDocumentsCashOnDelivery`,
										`tempTable`.`orderDocumentsTotalPrice`,
										`tempTable`.`orderDocumentsBankAccount`,
										`tempTable`.`orderDocumentsPaymentCondition`,
										`tempTable`.`orderDocumentsPaymentType`,
										`tempTable`.`orderDocumentsSkonto`,
										`tempTable`.`orderDocumentsDeliveryType`,
										`tempTable`.`orderDocumentsDeliveryTypeNumber`,
										`tempTable`.`orderDocumentsSubject`,
										`tempTable`.`orderDocumentsContent`,
										`tempTable`.`orderDocumentsDocumentPath`,
										`tempTable`.`orderDocumentsTimestamp`,
										`tempTable`.`orderDocumentsStatus`,
										`tempTable`.`orderDocumentsSendMail`,

										`tempTable`.`orderDocumentDetailOrderID`,
										`tempTable`.`orderDocumentDetailDocumentID`,
										`tempTable`.`orderDocumentDetailID`,
										`tempTable`.`orderDocumentDetailOrderType`,
										`tempTable`.`orderDocumentDetailProductNumber`,
										`tempTable`.`orderDocumentDetailProductName`,
										`tempTable`.`orderDocumentDetailProductQuantity`,
										`tempTable`.`orderDocumentDetailProductSinglePrice`,
										`tempTable`.`orderDocumentDetailPosType`,
										`tempTable`.`orderDocumentDetailProductKategorieID`,

										`tempTable`.`documentsToDocumentsCreatedDocumentNumber`,

										`tempTable`.`concatGroup`

										FROM (

												SELECT
													`" . TABLE_SALESMEN_PROVISIONS_DETAILS . "`.`salesmenProvisionsDetailsID`,
													`" . TABLE_SALESMEN_PROVISIONS_DETAILS . "`.`salesmenProvisionsDetailsProvisionsID`,
													`" . TABLE_SALESMEN_PROVISIONS_DETAILS . "`.`salesmenProvisionsDetailsDocumentsDetailID`,
													`" . TABLE_SALESMEN_PROVISIONS_DETAILS . "`.`salesmenProvisionsDetailsDocumentNumber`,
													`" . TABLE_SALESMEN_PROVISIONS_DETAILS . "`.`salesmenProvisionsDetailsProductOrderDate`,
													`" . TABLE_SALESMEN_PROVISIONS_DETAILS . "`.`salesmenProvisionsDetailsProductAmount`,
													`" . TABLE_SALESMEN_PROVISIONS_DETAILS . "`.`salesmenProvisionsDetailsProductNumber`,
													`" . TABLE_SALESMEN_PROVISIONS_DETAILS . "`.`salesmenProvisionsDetailsProductName`,
													`" . TABLE_SALESMEN_PROVISIONS_DETAILS . "`.`salesmenProvisionsDetailsProductRecipientCustomerNumber`,
													`" . TABLE_SALESMEN_PROVISIONS_DETAILS . "`.`salesmenProvisionsDetailsProductRecipientName`,
													`" . TABLE_SALESMEN_PROVISIONS_DETAILS . "`.`salesmenProvisionsDetailsProductRecipientZipcode`,
													`" . TABLE_SALESMEN_PROVISIONS_DETAILS . "`.`salesmenProvisionsDetailsProductRecipientCity`,
													`" . TABLE_SALESMEN_PROVISIONS_DETAILS . "`.`salesmenProvisionsDetailsProductVkPreis`,
													`" . TABLE_SALESMEN_PROVISIONS_DETAILS . "`.`salesmenProvisionsDetailsProductProvisionPrice`,
													`" . TABLE_SALESMEN_PROVISIONS_DETAILS . "`.`salesmenProvisionsDetailsDisabled`,
													`" . TABLE_SALESMEN_PROVISIONS_DETAILS . "`.`salesmenProvisionsDetailsType`,

													`" . TABLE_ORDER_CONFIRMATIONS . "`.`orderDocumentsID`,
													`" . TABLE_ORDER_CONFIRMATIONS . "`.`orderDocumentsNumber`,
													`" . TABLE_ORDER_CONFIRMATIONS . "`.`orderDocumentsType`,
													`" . TABLE_ORDER_CONFIRMATIONS . "`.`orderDocumentsOrdersIDs`,
													`" . TABLE_ORDER_CONFIRMATIONS . "`.`orderDocumentsCustomerNumber`,
													`" . TABLE_ORDER_CONFIRMATIONS . "`.`orderDocumentsCustomersOrderNumber`,
													`" . TABLE_ORDER_CONFIRMATIONS . "`.`orderDocumentsProcessingDate`,
													`" . TABLE_ORDER_CONFIRMATIONS . "`.`orderDocumentsDocumentDate`,
													`" . TABLE_ORDER_CONFIRMATIONS . "`.`orderDocumentsOrderDate`,
													`" . TABLE_ORDER_CONFIRMATIONS . "`.`orderDocumentsInvoiceDate`,
													`" . TABLE_ORDER_CONFIRMATIONS . "`.`orderDocumentsCreditDate`,
													`" . TABLE_ORDER_CONFIRMATIONS . "`.`orderDocumentsDeliveryDate`,
													`" . TABLE_ORDER_CONFIRMATIONS . "`.`orderDocumentsBindingDate`,
													`" . TABLE_ORDER_CONFIRMATIONS . "`.`orderDocumentsSalesman`,
													`" . TABLE_ORDER_CONFIRMATIONS . "`.`orderDocumentsKommission`,
													`" . TABLE_ORDER_CONFIRMATIONS . "`.`orderDocumentsAddressCompany`,
													`" . TABLE_ORDER_CONFIRMATIONS . "`.`orderDocumentsAddressName`,
													`" . TABLE_ORDER_CONFIRMATIONS . "`.`orderDocumentsAddressStreet`,
													`" . TABLE_ORDER_CONFIRMATIONS . "`.`orderDocumentsAddressStreetNumber`,
													`" . TABLE_ORDER_CONFIRMATIONS . "`.`orderDocumentsAddressZipcode`,
													`" . TABLE_ORDER_CONFIRMATIONS . "`.`orderDocumentsAddressCity`,
													`" . TABLE_ORDER_CONFIRMATIONS . "`.`orderDocumentsAddressCountry`,
													`" . TABLE_ORDER_CONFIRMATIONS . "`.`orderDocumentsAddressMail`,
													`" . TABLE_ORDER_CONFIRMATIONS . "`.`orderDocumentsSumPrice`,
													`" . TABLE_ORDER_CONFIRMATIONS . "`.`orderDocumentsDiscount`,
													`" . TABLE_ORDER_CONFIRMATIONS . "`.`orderDocumentsDiscountType`,
													`" . TABLE_ORDER_CONFIRMATIONS . "`.`orderDocumentsDiscountPercent`,
													`" . TABLE_ORDER_CONFIRMATIONS . "`.`orderDocumentsMwst`,
													`" . TABLE_ORDER_CONFIRMATIONS . "`.`orderDocumentsMwstPrice`,
													`" . TABLE_ORDER_CONFIRMATIONS . "`.`orderDocumentsShippingCosts`,
													`" . TABLE_ORDER_CONFIRMATIONS . "`.`orderDocumentsShippingCostsPackages`,
													`" . TABLE_ORDER_CONFIRMATIONS . "`.`orderDocumentsShippingCostsPerPackage`,
													`" . TABLE_ORDER_CONFIRMATIONS . "`.`orderDocumentsPackagingCosts`,
													`" . TABLE_ORDER_CONFIRMATIONS . "`.`orderDocumentsCashOnDelivery`,
													`" . TABLE_ORDER_CONFIRMATIONS . "`.`orderDocumentsTotalPrice`,
													`" . TABLE_ORDER_CONFIRMATIONS . "`.`orderDocumentsBankAccount`,
													`" . TABLE_ORDER_CONFIRMATIONS . "`.`orderDocumentsPaymentCondition`,
													`" . TABLE_ORDER_CONFIRMATIONS . "`.`orderDocumentsPaymentType`,
													`" . TABLE_ORDER_CONFIRMATIONS . "`.`orderDocumentsSkonto`,
													`" . TABLE_ORDER_CONFIRMATIONS . "`.`orderDocumentsDeliveryType`,
													`" . TABLE_ORDER_CONFIRMATIONS . "`.`orderDocumentsDeliveryTypeNumber`,
													`" . TABLE_ORDER_CONFIRMATIONS . "`.`orderDocumentsSubject`,
													`" . TABLE_ORDER_CONFIRMATIONS . "`.`orderDocumentsContent`,
													`" . TABLE_ORDER_CONFIRMATIONS . "`.`orderDocumentsDocumentPath`,
													`" . TABLE_ORDER_CONFIRMATIONS . "`.`orderDocumentsTimestamp`,
													`" . TABLE_ORDER_CONFIRMATIONS . "`.`orderDocumentsStatus`,
													`" . TABLE_ORDER_CONFIRMATIONS . "`.`orderDocumentsSendMail`,

													`" . TABLE_ORDER_CONFIRMATIONS_DETAILS . "`.`orderDocumentDetailID`,
													`" . TABLE_ORDER_CONFIRMATIONS_DETAILS . "`.`orderDocumentDetailDocumentID`,
													`" . TABLE_ORDER_CONFIRMATIONS_DETAILS . "`.`orderDocumentDetailOrderID`,
													`" . TABLE_ORDER_CONFIRMATIONS_DETAILS . "`.`orderDocumentDetailOrderType`,
													`" . TABLE_ORDER_CONFIRMATIONS_DETAILS . "`.`orderDocumentDetailProductNumber`,
													`" . TABLE_ORDER_CONFIRMATIONS_DETAILS . "`.`orderDocumentDetailProductName`,
													`" . TABLE_ORDER_CONFIRMATIONS_DETAILS . "`.`orderDocumentDetailProductKategorieID`,
													`" . TABLE_ORDER_CONFIRMATIONS_DETAILS . "`.`orderDocumentDetailProductQuantity`,
													`" . TABLE_ORDER_CONFIRMATIONS_DETAILS . "`.`orderDocumentDetailProductSinglePrice`,
													`" . TABLE_ORDER_CONFIRMATIONS_DETAILS . "`.`orderDocumentDetailPosType`,
													`" . TABLE_ORDER_CONFIRMATIONS_DETAILS . "`.`orderDocumentDetailGroupingID`,

													'' AS `documentsToDocumentsCreatedDocumentNumber`,

													CONCAT(`" . TABLE_ORDER_CONFIRMATIONS . "`.`orderDocumentsNumber`, '-', `" . TABLE_ORDER_CONFIRMATIONS_DETAILS . "`.`orderDocumentDetailOrderID`) AS `concatGroup`

													FROM `" . TABLE_ORDER_CONFIRMATIONS . "`

													LEFT JOIN `" . TABLE_ORDER_CONFIRMATIONS_DETAILS . "`
													ON(`" . TABLE_ORDER_CONFIRMATIONS . "`.`orderDocumentsID` = `" . TABLE_ORDER_CONFIRMATIONS_DETAILS . "`.`orderDocumentDetailDocumentID` )

													LEFT JOIN `" . TABLE_SALESMEN_PROVISIONS_DETAILS . "`
													ON(
														`" . TABLE_ORDER_CONFIRMATIONS . "`.`orderDocumentsNumber` = `" . TABLE_SALESMEN_PROVISIONS_DETAILS . "`.`salesmenProvisionsDetailsDocumentNumber`
														/*
														AND
														`" . TABLE_ORDER_CONFIRMATIONS_DETAILS . "`.`orderDocumentDetailProductNumber` = `" . TABLE_SALESMEN_PROVISIONS_DETAILS . "`.`salesmenProvisionsDetailsProductNumber`
														*/
														AND
														`" . TABLE_ORDER_CONFIRMATIONS_DETAILS . "`.`orderDocumentDetailID` = `" . TABLE_SALESMEN_PROVISIONS_DETAILS . "`.`salesmenProvisionsDetailsDocumentsDetailID`
													)

													WHERE 1
														AND (
															`" . TABLE_SALESMEN_PROVISIONS_DETAILS . "`.`salesmenProvisionsDetailsType` != 'additional'
															OR
															`" . TABLE_SALESMEN_PROVISIONS_DETAILS . "`.`salesmenProvisionsDetailsType` IS NULL
														)
														AND (
															`" . TABLE_SALESMEN_PROVISIONS_DETAILS . "`.`salesmenProvisionsDetailsDisabled` != '1'
															OR
															`" . TABLE_SALESMEN_PROVISIONS_DETAILS . "`.`salesmenProvisionsDetailsDisabled` IS NULL
														)
														AND (
															`" . TABLE_ORDER_CONFIRMATIONS . "`.`orderDocumentsSalesman` = '" . $_POST["exportOrdersVertreterID"] . "'
											";

											if($arrSalesmanDatas["salesmenSubSalesmanID"] > 0){

												$sql_basic .= "
															OR
															`" . TABLE_ORDER_CONFIRMATIONS . "`.`orderDocumentsSalesman` = '" .  $arrSalesmanDatas["salesmenSubSalesmanID"] . "'
												";
											}

											$sql_basic .= "
														)
														AND `" . TABLE_ORDER_CONFIRMATIONS_DETAILS . "`.`orderDocumentDetailPosType` = 'product'

														/*
														AND (
															`" . TABLE_SALESMEN_PROVISIONS_DETAILS . "`.`salesmenProvisionsDetailsDocumentNumber` = ''
															OR
															`" . TABLE_SALESMEN_PROVISIONS_DETAILS . "`.`salesmenProvisionsDetailsDocumentNumber` IS NULL
														)
														*/

														" . str_replace("{###THIS_TABLE###}", TABLE_ORDER_CONFIRMATIONS, $where) . "

													HAVING (
														`orderDocumentsProcessingDate` BETWEEN '{###DATE_EXPORT_START###}' AND '{###DATE_EXPORT_END###}'
													)

												UNION

												SELECT
													`" . TABLE_SALESMEN_PROVISIONS_DETAILS . "`.`salesmenProvisionsDetailsID`,
													`" . TABLE_SALESMEN_PROVISIONS_DETAILS . "`.`salesmenProvisionsDetailsProvisionsID`,
													`" . TABLE_SALESMEN_PROVISIONS_DETAILS . "`.`salesmenProvisionsDetailsDocumentsDetailID`,
													`" . TABLE_SALESMEN_PROVISIONS_DETAILS . "`.`salesmenProvisionsDetailsDocumentNumber`,
													`" . TABLE_SALESMEN_PROVISIONS_DETAILS . "`.`salesmenProvisionsDetailsProductOrderDate`,
													`" . TABLE_SALESMEN_PROVISIONS_DETAILS . "`.`salesmenProvisionsDetailsProductAmount`,
													`" . TABLE_SALESMEN_PROVISIONS_DETAILS . "`.`salesmenProvisionsDetailsProductNumber`,
													`" . TABLE_SALESMEN_PROVISIONS_DETAILS . "`.`salesmenProvisionsDetailsProductName`,
													`" . TABLE_SALESMEN_PROVISIONS_DETAILS . "`.`salesmenProvisionsDetailsProductRecipientCustomerNumber`,
													`" . TABLE_SALESMEN_PROVISIONS_DETAILS . "`.`salesmenProvisionsDetailsProductRecipientName`,
													`" . TABLE_SALESMEN_PROVISIONS_DETAILS . "`.`salesmenProvisionsDetailsProductRecipientZipcode`,
													`" . TABLE_SALESMEN_PROVISIONS_DETAILS . "`.`salesmenProvisionsDetailsProductRecipientCity`,
													`" . TABLE_SALESMEN_PROVISIONS_DETAILS . "`.`salesmenProvisionsDetailsProductVkPreis`,
													`" . TABLE_SALESMEN_PROVISIONS_DETAILS . "`.`salesmenProvisionsDetailsProductProvisionPrice`,
													`" . TABLE_SALESMEN_PROVISIONS_DETAILS . "`.`salesmenProvisionsDetailsDisabled`,
													`" . TABLE_SALESMEN_PROVISIONS_DETAILS . "`.`salesmenProvisionsDetailsType`,

													`" . TABLE_ORDER_INVOICES . "`.`orderDocumentsID`,
													`" . TABLE_ORDER_INVOICES . "`.`orderDocumentsNumber`,
													`" . TABLE_ORDER_INVOICES . "`.`orderDocumentsType`,
													`" . TABLE_ORDER_INVOICES . "`.`orderDocumentsOrdersIDs`,
													`" . TABLE_ORDER_INVOICES . "`.`orderDocumentsCustomerNumber`,
													`" . TABLE_ORDER_INVOICES . "`.`orderDocumentsCustomersOrderNumber`,
													`" . TABLE_ORDER_INVOICES . "`.`orderDocumentsProcessingDate`,
													`" . TABLE_ORDER_INVOICES . "`.`orderDocumentsDocumentDate`,
													`" . TABLE_ORDER_INVOICES . "`.`orderDocumentsOrderDate`,
													`" . TABLE_ORDER_INVOICES . "`.`orderDocumentsInvoiceDate`,
													`" . TABLE_ORDER_INVOICES . "`.`orderDocumentsCreditDate`,
													`" . TABLE_ORDER_INVOICES . "`.`orderDocumentsDeliveryDate`,
													`" . TABLE_ORDER_INVOICES . "`.`orderDocumentsBindingDate`,
													`" . TABLE_ORDER_INVOICES . "`.`orderDocumentsSalesman`,
													`" . TABLE_ORDER_INVOICES . "`.`orderDocumentsKommission`,
													`" . TABLE_ORDER_INVOICES . "`.`orderDocumentsAddressCompany`,
													`" . TABLE_ORDER_INVOICES . "`.`orderDocumentsAddressName`,
													`" . TABLE_ORDER_INVOICES . "`.`orderDocumentsAddressStreet`,
													`" . TABLE_ORDER_INVOICES . "`.`orderDocumentsAddressStreetNumber`,
													`" . TABLE_ORDER_INVOICES . "`.`orderDocumentsAddressZipcode`,
													`" . TABLE_ORDER_INVOICES . "`.`orderDocumentsAddressCity`,
													`" . TABLE_ORDER_INVOICES . "`.`orderDocumentsAddressCountry`,
													`" . TABLE_ORDER_INVOICES . "`.`orderDocumentsAddressMail`,
													`" . TABLE_ORDER_INVOICES . "`.`orderDocumentsSumPrice`,
													`" . TABLE_ORDER_INVOICES . "`.`orderDocumentsDiscount`,
													`" . TABLE_ORDER_INVOICES . "`.`orderDocumentsDiscountType`,
													`" . TABLE_ORDER_INVOICES . "`.`orderDocumentsDiscountPercent`,
													`" . TABLE_ORDER_INVOICES . "`.`orderDocumentsMwst`,
													`" . TABLE_ORDER_INVOICES . "`.`orderDocumentsMwstPrice`,
													`" . TABLE_ORDER_INVOICES . "`.`orderDocumentsShippingCosts`,
													`" . TABLE_ORDER_INVOICES . "`.`orderDocumentsShippingCostsPackages`,
													`" . TABLE_ORDER_INVOICES . "`.`orderDocumentsShippingCostsPerPackage`,
													`" . TABLE_ORDER_INVOICES . "`.`orderDocumentsPackagingCosts`,
													`" . TABLE_ORDER_INVOICES . "`.`orderDocumentsCashOnDelivery`,
													`" . TABLE_ORDER_INVOICES . "`.`orderDocumentsTotalPrice`,
													`" . TABLE_ORDER_INVOICES . "`.`orderDocumentsBankAccount`,
													`" . TABLE_ORDER_INVOICES . "`.`orderDocumentsPaymentCondition`,
													`" . TABLE_ORDER_INVOICES . "`.`orderDocumentsPaymentType`,
													`" . TABLE_ORDER_INVOICES . "`.`orderDocumentsSkonto`,
													`" . TABLE_ORDER_INVOICES . "`.`orderDocumentsDeliveryType`,
													`" . TABLE_ORDER_INVOICES . "`.`orderDocumentsDeliveryTypeNumber`,
													`" . TABLE_ORDER_INVOICES . "`.`orderDocumentsSubject`,
													`" . TABLE_ORDER_INVOICES . "`.`orderDocumentsContent`,
													`" . TABLE_ORDER_INVOICES . "`.`orderDocumentsDocumentPath`,
													`" . TABLE_ORDER_INVOICES . "`.`orderDocumentsTimestamp`,
													`" . TABLE_ORDER_INVOICES . "`.`orderDocumentsStatus`,
													`" . TABLE_ORDER_INVOICES . "`.`orderDocumentsSendMail`,

													`" . TABLE_ORDER_INVOICES_DETAILS . "`.`orderDocumentDetailID`,
													`" . TABLE_ORDER_INVOICES_DETAILS . "`.`orderDocumentDetailDocumentID`,
													`" . TABLE_ORDER_INVOICES_DETAILS . "`.`orderDocumentDetailOrderID`,
													`" . TABLE_ORDER_INVOICES_DETAILS . "`.`orderDocumentDetailOrderType`,
													`" . TABLE_ORDER_INVOICES_DETAILS . "`.`orderDocumentDetailProductNumber`,
													`" . TABLE_ORDER_INVOICES_DETAILS . "`.`orderDocumentDetailProductName`,
													`" . TABLE_ORDER_INVOICES_DETAILS . "`.`orderDocumentDetailProductKategorieID`,
													`" . TABLE_ORDER_INVOICES_DETAILS . "`.`orderDocumentDetailProductQuantity`,
													`" . TABLE_ORDER_INVOICES_DETAILS . "`.`orderDocumentDetailProductSinglePrice`,
													`" . TABLE_ORDER_INVOICES_DETAILS . "`.`orderDocumentDetailPosType`,
													`" . TABLE_ORDER_INVOICES_DETAILS . "`.`orderDocumentDetailGroupingID`,

													`" . TABLE_DOCUMENTS_TO_DOCUMENTS . "`.`documentsToDocumentsCreatedDocumentNumber`,

													CONCAT(`" . TABLE_ORDER_INVOICES . "`.`orderDocumentsNumber`, '-', `" . TABLE_ORDER_INVOICES_DETAILS . "`.`orderDocumentDetailOrderID`) AS `concatGroup`

												FROM `" . TABLE_ORDER_INVOICES . "`

												LEFT JOIN `" . TABLE_DOCUMENTS_TO_DOCUMENTS . "`
												ON(`" . TABLE_ORDER_INVOICES . "`.`orderDocumentsNumber` = `" . TABLE_DOCUMENTS_TO_DOCUMENTS . "`.`documentsToDocumentsCreatedDocumentNumber`)

												LEFT JOIN `" . TABLE_ORDER_INVOICES_DETAILS . "`
												ON(`" . TABLE_ORDER_INVOICES . "`.`orderDocumentsID` = `" . TABLE_ORDER_INVOICES_DETAILS . "`.`orderDocumentDetailDocumentID` )

												LEFT JOIN `" . TABLE_SALESMEN_PROVISIONS_DETAILS . "`
												ON(
													`" . TABLE_ORDER_INVOICES . "`.`orderDocumentsNumber` = `" . TABLE_SALESMEN_PROVISIONS_DETAILS . "`.`salesmenProvisionsDetailsDocumentNumber`
													/*
													AND
													`" . TABLE_ORDER_INVOICES_DETAILS . "`.`orderDocumentDetailProductNumber` = `" . TABLE_SALESMEN_PROVISIONS_DETAILS . "`.`salesmenProvisionsDetailsProductNumber`
													*/
													AND
													`" . TABLE_ORDER_INVOICES_DETAILS . "`.`orderDocumentDetailID` = `" . TABLE_SALESMEN_PROVISIONS_DETAILS . "`.`salesmenProvisionsDetailsDocumentsDetailID`
												)

												WHERE 1
													AND (
															`" . TABLE_SALESMEN_PROVISIONS_DETAILS . "`.`salesmenProvisionsDetailsType` != 'additional'
															OR
															`" . TABLE_SALESMEN_PROVISIONS_DETAILS . "`.`salesmenProvisionsDetailsType` IS NULL
													)
													AND (
														`" . TABLE_SALESMEN_PROVISIONS_DETAILS . "`.`salesmenProvisionsDetailsDisabled` != '1'
														OR
														`" . TABLE_SALESMEN_PROVISIONS_DETAILS . "`.`salesmenProvisionsDetailsDisabled` IS NULL
													)
													AND (
														`" . TABLE_ORDER_INVOICES . "`.`orderDocumentsSalesman` = '" . $_POST["exportOrdersVertreterID"] . "'
											";
									if($arrSalesmanDatas["salesmenSubSalesmanID"] > 0){
										$sql_basic .= "
													OR
													`" . TABLE_ORDER_INVOICES . "`.`orderDocumentsSalesman` = '" .  $arrSalesmanDatas["salesmenSubSalesmanID"] . "'
											";
									}
									$sql_basic .= "
													)
													AND `" . TABLE_ORDER_INVOICES_DETAILS . "`.`orderDocumentDetailPosType` = 'product'

													/*
													AND (
														`" . TABLE_SALESMEN_PROVISIONS_DETAILS . "`.`salesmenProvisionsDetailsDocumentNumber` = ''
														OR `" . TABLE_SALESMEN_PROVISIONS_DETAILS . "`.`salesmenProvisionsDetailsDocumentNumber` IS NULL
													)
													*/

													" . str_replace("{###THIS_TABLE###}", TABLE_ORDER_INVOICES, $where) . "
												GROUP BY CONCAT(`orderDocumentsNumber`, '-', `orderDocumentDetailOrderID`)
												HAVING (
														`orderDocumentsProcessingDate` BETWEEN '{###DATE_EXPORT_START###}' AND '{###DATE_EXPORT_END###}'
														 AND `documentsToDocumentsCreatedDocumentNumber` IS NULL
												)
												/* ################################################################ */

											UNION

												SELECT
													`" . TABLE_SALESMEN_PROVISIONS_DETAILS . "`.`salesmenProvisionsDetailsID`,
													`" . TABLE_SALESMEN_PROVISIONS_DETAILS . "`.`salesmenProvisionsDetailsProvisionsID`,
													`" . TABLE_SALESMEN_PROVISIONS_DETAILS . "`.`salesmenProvisionsDetailsDocumentsDetailID`,
													`" . TABLE_SALESMEN_PROVISIONS_DETAILS . "`.`salesmenProvisionsDetailsDocumentNumber`,
													`" . TABLE_SALESMEN_PROVISIONS_DETAILS . "`.`salesmenProvisionsDetailsProductOrderDate`,
													`" . TABLE_SALESMEN_PROVISIONS_DETAILS . "`.`salesmenProvisionsDetailsProductAmount`,
													`" . TABLE_SALESMEN_PROVISIONS_DETAILS . "`.`salesmenProvisionsDetailsProductNumber`,
													`" . TABLE_SALESMEN_PROVISIONS_DETAILS . "`.`salesmenProvisionsDetailsProductName`,
													`" . TABLE_SALESMEN_PROVISIONS_DETAILS . "`.`salesmenProvisionsDetailsProductRecipientCustomerNumber`,
													`" . TABLE_SALESMEN_PROVISIONS_DETAILS . "`.`salesmenProvisionsDetailsProductRecipientName`,
													`" . TABLE_SALESMEN_PROVISIONS_DETAILS . "`.`salesmenProvisionsDetailsProductRecipientZipcode`,
													`" . TABLE_SALESMEN_PROVISIONS_DETAILS . "`.`salesmenProvisionsDetailsProductRecipientCity`,
													`" . TABLE_SALESMEN_PROVISIONS_DETAILS . "`.`salesmenProvisionsDetailsProductVkPreis`,
													`" . TABLE_SALESMEN_PROVISIONS_DETAILS . "`.`salesmenProvisionsDetailsProductProvisionPrice`,
													`" . TABLE_SALESMEN_PROVISIONS_DETAILS . "`.`salesmenProvisionsDetailsDisabled`,
													`" . TABLE_SALESMEN_PROVISIONS_DETAILS . "`.`salesmenProvisionsDetailsType`,

													`" . TABLE_ORDER_CREDITS . "`.`orderDocumentsID`,
													`" . TABLE_ORDER_CREDITS . "`.`orderDocumentsNumber`,
													`" . TABLE_ORDER_CREDITS . "`.`orderDocumentsType`,
													`" . TABLE_ORDER_CREDITS . "`.`orderDocumentsOrdersIDs`,
													`" . TABLE_ORDER_CREDITS . "`.`orderDocumentsCustomerNumber`,
													`" . TABLE_ORDER_CREDITS . "`.`orderDocumentsCustomersOrderNumber`,
													`" . TABLE_ORDER_CREDITS . "`.`orderDocumentsProcessingDate`,
													`" . TABLE_ORDER_CREDITS . "`.`orderDocumentsDocumentDate`,
													`" . TABLE_ORDER_CREDITS . "`.`orderDocumentsOrderDate`,
													`" . TABLE_ORDER_CREDITS . "`.`orderDocumentsInvoiceDate`,
													`" . TABLE_ORDER_CREDITS . "`.`orderDocumentsCreditDate`,
													`" . TABLE_ORDER_CREDITS . "`.`orderDocumentsDeliveryDate`,
													`" . TABLE_ORDER_CREDITS . "`.`orderDocumentsBindingDate`,
													`" . TABLE_ORDER_CREDITS . "`.`orderDocumentsSalesman`,
													`" . TABLE_ORDER_CREDITS . "`.`orderDocumentsKommission`,
													`" . TABLE_ORDER_CREDITS . "`.`orderDocumentsAddressCompany`,
													`" . TABLE_ORDER_CREDITS . "`.`orderDocumentsAddressName`,
													`" . TABLE_ORDER_CREDITS . "`.`orderDocumentsAddressStreet`,
													`" . TABLE_ORDER_CREDITS . "`.`orderDocumentsAddressStreetNumber`,
													`" . TABLE_ORDER_CREDITS . "`.`orderDocumentsAddressZipcode`,
													`" . TABLE_ORDER_CREDITS . "`.`orderDocumentsAddressCity`,
													`" . TABLE_ORDER_CREDITS . "`.`orderDocumentsAddressCountry`,
													`" . TABLE_ORDER_CREDITS . "`.`orderDocumentsAddressMail`,
													`" . TABLE_ORDER_CREDITS . "`.`orderDocumentsSumPrice`,
													`" . TABLE_ORDER_CREDITS . "`.`orderDocumentsDiscount`,
													`" . TABLE_ORDER_CREDITS . "`.`orderDocumentsDiscountType`,
													`" . TABLE_ORDER_CREDITS . "`.`orderDocumentsDiscountPercent`,
													`" . TABLE_ORDER_CREDITS . "`.`orderDocumentsMwst`,
													`" . TABLE_ORDER_CREDITS . "`.`orderDocumentsMwstPrice`,
													`" . TABLE_ORDER_CREDITS . "`.`orderDocumentsShippingCosts`,
													`" . TABLE_ORDER_CREDITS . "`.`orderDocumentsShippingCostsPackages`,
													`" . TABLE_ORDER_CREDITS . "`.`orderDocumentsShippingCostsPerPackage`,
													`" . TABLE_ORDER_CREDITS . "`.`orderDocumentsPackagingCosts`,
													`" . TABLE_ORDER_CREDITS . "`.`orderDocumentsCashOnDelivery`,
													`" . TABLE_ORDER_CREDITS . "`.`orderDocumentsTotalPrice`,
													`" . TABLE_ORDER_CREDITS . "`.`orderDocumentsBankAccount`,
													`" . TABLE_ORDER_CREDITS . "`.`orderDocumentsPaymentCondition`,
													`" . TABLE_ORDER_CREDITS . "`.`orderDocumentsPaymentType`,
													`" . TABLE_ORDER_CREDITS . "`.`orderDocumentsSkonto`,
													`" . TABLE_ORDER_CREDITS . "`.`orderDocumentsDeliveryType`,
													`" . TABLE_ORDER_CREDITS . "`.`orderDocumentsDeliveryTypeNumber`,
													`" . TABLE_ORDER_CREDITS . "`.`orderDocumentsSubject`,
													`" . TABLE_ORDER_CREDITS . "`.`orderDocumentsContent`,
													`" . TABLE_ORDER_CREDITS . "`.`orderDocumentsDocumentPath`,
													`" . TABLE_ORDER_CREDITS . "`.`orderDocumentsTimestamp`,
													`" . TABLE_ORDER_CREDITS . "`.`orderDocumentsStatus`,
													`" . TABLE_ORDER_CREDITS . "`.`orderDocumentsSendMail`,

													`" . TABLE_ORDER_CREDITS_DETAILS . "`.`orderDocumentDetailID`,
													`" . TABLE_ORDER_CREDITS_DETAILS . "`.`orderDocumentDetailDocumentID`,
													`" . TABLE_ORDER_CREDITS_DETAILS . "`.`orderDocumentDetailOrderID`,
													`" . TABLE_ORDER_CREDITS_DETAILS . "`.`orderDocumentDetailOrderType`,
													`" . TABLE_ORDER_CREDITS_DETAILS . "`.`orderDocumentDetailProductNumber`,
													`" . TABLE_ORDER_CREDITS_DETAILS . "`.`orderDocumentDetailProductName`,
													`" . TABLE_ORDER_CREDITS_DETAILS . "`.`orderDocumentDetailProductKategorieID`,
													`" . TABLE_ORDER_CREDITS_DETAILS . "`.`orderDocumentDetailProductQuantity`,
													(`" . TABLE_ORDER_CREDITS_DETAILS . "`.`orderDocumentDetailProductSinglePrice`) * (-1),
													`" . TABLE_ORDER_CREDITS_DETAILS . "`.`orderDocumentDetailPosType`,
													`" . TABLE_ORDER_CREDITS_DETAILS . "`.`orderDocumentDetailGroupingID`,

													'' AS `documentsToDocumentsCreatedDocumentNumber`,

													CONCAT(`" . TABLE_ORDER_CREDITS . "`.`orderDocumentsNumber`, '-', `" . TABLE_ORDER_CREDITS_DETAILS . "`.`orderDocumentDetailOrderID`) AS `concatGroup`

													FROM `" . TABLE_ORDER_CREDITS . "`

													LEFT JOIN `" . TABLE_ORDER_CREDITS_DETAILS . "`
													ON(`" . TABLE_ORDER_CREDITS . "`.`orderDocumentsID` = `" . TABLE_ORDER_CREDITS_DETAILS . "`.`orderDocumentDetailDocumentID` )

													LEFT JOIN `" . TABLE_SALESMEN_PROVISIONS_DETAILS . "`
													ON(
														`" . TABLE_ORDER_CREDITS . "`.`orderDocumentsNumber` = `" . TABLE_SALESMEN_PROVISIONS_DETAILS . "`.`salesmenProvisionsDetailsDocumentNumber`
														/*
														AND
														`" . TABLE_ORDER_CREDITS_DETAILS . "`.`orderDocumentDetailProductNumber` = `" . TABLE_SALESMEN_PROVISIONS_DETAILS . "`.`salesmenProvisionsDetailsProductNumber`
														*/
														AND
														`" . TABLE_ORDER_CREDITS_DETAILS . "`.`orderDocumentDetailID` = `" . TABLE_SALESMEN_PROVISIONS_DETAILS . "`.`salesmenProvisionsDetailsDocumentsDetailID`
													)

													WHERE 1
														AND (
															`" . TABLE_SALESMEN_PROVISIONS_DETAILS . "`.`salesmenProvisionsDetailsType` != 'additional'
															OR
															`" . TABLE_SALESMEN_PROVISIONS_DETAILS . "`.`salesmenProvisionsDetailsType` IS NULL
														)
														AND (
															`" . TABLE_SALESMEN_PROVISIONS_DETAILS . "`.`salesmenProvisionsDetailsDisabled` != '1'
															OR
															`" . TABLE_SALESMEN_PROVISIONS_DETAILS . "`.`salesmenProvisionsDetailsDisabled` IS NULL
														)
														AND (
															`" . TABLE_ORDER_CREDITS . "`.`orderDocumentsSalesman` = '" . $_POST["exportOrdersVertreterID"] . "'
											";

											if($arrSalesmanDatas["salesmenSubSalesmanID"] > 0){

												$sql_basic .= "
															OR
															`" . TABLE_ORDER_CREDITS . "`.`orderDocumentsSalesman` = '" .  $arrSalesmanDatas["salesmenSubSalesmanID"] . "'
												";
											}

											$sql_basic .= "
														)
														AND `" . TABLE_ORDER_CREDITS_DETAILS . "`.`orderDocumentDetailPosType` = 'product'

														/*
														AND (
															`" . TABLE_SALESMEN_PROVISIONS_DETAILS . "`.`salesmenProvisionsDetailsDocumentNumber` = ''
															OR
															`" . TABLE_SALESMEN_PROVISIONS_DETAILS . "`.`salesmenProvisionsDetailsDocumentNumber` IS NULL
														)
														*/

														" . str_replace("{###THIS_TABLE###}", TABLE_ORDER_CREDITS, $where) . "

													HAVING (
														`orderDocumentsProcessingDate` BETWEEN '{###DATE_EXPORT_START###}' AND '{###DATE_EXPORT_END###}'
													)
												/* ################################################################ */

										) AS `tempTable`

									GROUP BY `tempTable`.`concatGroup`
							";
$sql_order = "
									ORDER BY FIELD(`orderDocumentsType`, 'GU', 'AB', 'RE'), `orderDocumentsDocumentDate` ASC, `orderDocumentsNumber` ASC

							";

						// EOF CREATE SQL FOR PROVISIONS FROM INVOICES DEPENDING ON SALESMAN AREAS

					/*
					WHERE `" . TABLE_ORDER_CONFIRMATIONS . "`.`orderDocumentsProcessingDate`
											BETWEEN '" . formatDate($_POST["exportDateStart"], "store") . "' AND '" . formatDate($_POST["exportDateEnd"], "store")."'

					*/
							$sql = $sql_basic;
							$dateStart = formatDate($_POST["exportOrdersDateFrom"], "store");
							$dateEnd = formatDate($_POST["exportOrdersDateTo"], "store");
							$sql = preg_replace("/{###DATE_EXPORT_START###}/", $dateStart, $sql);
							$sql = preg_replace("/{###DATE_EXPORT_END###}/", $dateEnd, $sql);
							$sqlAdd = '';


							if($_POST["displayUnExportedLastMonth"] == '1') {
								$sqlAdd = $sql_basic;
								$dateStartLastMonth = date("Y-m-d", mktime(0, 0, 0, (date("m", strtotime($dateStart)) - 1), 1, date("Y", strtotime($dateStart))));
								$dateEndLastMonth = date("Y-m-d", mktime(0, 0, 0, (date("m", strtotime($dateStart)) - 0), 0, date("Y", strtotime($dateStart))));
								$sqlAdd = preg_replace("/{###DATE_EXPORT_START###}/", $dateStartLastMonth, $sqlAdd);
								$sqlAdd = preg_replace("/{###DATE_EXPORT_END###}/", $dateEndLastMonth, $sqlAdd);
								$sql .= ' UNION ' . $sqlAdd . ' HAVING `tempTable`.`salesmenProvisionsDetailsID` IS NULL ';
							}

							$sql .= $sql_order;
#dd('sql');
							$rs = $dbConnection->db_query($sql);

							$countTotalRows = $dbConnection->db_getMysqlNumRows($rs);

							$contentExportPDF = '';
							$contentExportCSV = '';

							if($countTotalRows > 0) {
								$contentExportPDF_HEAD = '';
								$contentExportPDF_HEAD .= '<thead>';
								$contentExportPDF_HEAD .= '<tr>';
								$contentExportPDF_HEAD .= '<th>Pos</th>';
								$contentExportPDF_HEAD .= '<!-- BOF DOCUMENT_NUMBER_ORDER -->';
								$contentExportPDF_HEAD .= '<th>Auftragsnummer</th>';
								#$contentExportPDF_HEAD .= '<th>Produktionsstatus</th>';
								$contentExportPDF_HEAD .= '<!-- EOF DOCUMENT_NUMBER_ORDER -->';
								$contentExportPDF_HEAD .= '<th>Datum</th>';
								$contentExportPDF_HEAD .= '<th>K-Nr.</th>';
								$contentExportPDF_HEAD .= '<th>Firma</th>';
								$contentExportPDF_HEAD .= '<th>Ort</th>';
								$contentExportPDF_HEAD .= '<th>Artikel</th>';
								$contentExportPDF_HEAD .= '<th>Menge</th>';
								$contentExportPDF_HEAD .= '<th>VK-Preis</th>';
								if($arrSalesmanDatas["salesmenProvision"] > 0){
									$contentExportPDF_HEAD .= '<th>Gesamt</th>';
								}
								else {
									$contentExportPDF_HEAD .= '<th>Gutschrift</th>';
								}
								$contentExportPDF_HEAD .= '</tr>';
								$contentExportPDF_HEAD .= '</thead>';

								$contentExportPDF_TABLE = '<table border="0" cellpadding="0" cellspacing="0" width="0" class="displayOrders" id="displayCardDatas" >';
								#$contentExportPDF_TABLE = '<table border="0" cellpadding="0" cellspacing="0" width="0">';

								echo '<form name="formProvision" action="' . $_SERVER["PHP_SELF"]. '" method="post" enctype="multipart/form-data">';
								echo '<input type="hidden" name="exportOrdersVertreterID" value="' . $_REQUEST["exportOrdersVertreterID"] . '" />';
								$contentExportPDF .= $contentExportPDF_TABLE;

								$contentExportPDF .= $contentExportPDF_HEAD;

								$contentExportPDF .= '<tbody>';

								$countRow = 0;
								$displayTotalSum = 0;
								$thisProvisionID = 0;
								$thisParsedUrl = parse_url($_SERVER["REQUEST_URI"]);
								while($ds = mysqli_fetch_assoc($rs)){

									if($thisProvisionID == 0){ $thisProvisionID = $ds["salesmenProvisionsDetailsProvisionsID"]; }
									if($countRow%2 == 0){ $rowClass = 'row0'; }
									else { $rowClass = 'row1'; }
									//  '.$rowClass.'
									$contentExportPDF .= '<tr class="displayCardDatasItemRows">';
									$contentExportPDF .= '<td style="text-align:right;">';
									$contentExportPDF .= '<b>' . ($countRow + 1) . '.</b>';
									$contentExportPDF .= '<input type="hidden" name="arrProvisionValues['.$countRow.'][orderDocumentDetailID]" value="' . $ds["orderDocumentDetailID"] . '" />';
									$contentExportPDF .= '<input type="hidden" name="arrProvisionValues['.$countRow.'][salesmenProvisionsDetailsDisabled]" value="0" />';
									$contentExportPDF .= '<input type="hidden" name="arrProvisionValues['.$countRow.'][salesmenProvisionsDetailsType]" value="standard" />';
									$contentExportPDF .= '</td>';
									$contentExportPDF .= '<!-- BOF DOCUMENT_NUMBER_ORDER -->';
									$contentExportPDF .= '<td style="background-color:#FEFFAF; white-space:nowrap;">';
									$contentExportPDF .= '' . $ds["orderDocumentsNumber"] . '';
									#$contentExportPDF .= '<a href="' . PAGE_DOWNLOAD_DOCUMENTS . '?searchBoxFile=' . $ds["orderDocumentsNumber"] . '">' . '<b>' . $ds["orderDocumentsNumber"] . '</b>' . '</a>';
									#$contentExportPDF .= '<a href="' . PAGE_DOWNLOAD_DOCUMENTS . '?searchBoxFile= . ' . $ds["orderDocumentsNumber"] . '">' . '<img src="layout/icons/iconPDF.gif" width="16" height="16" title="Dokument &quot;' . $ds["orderDocumentsNumber"]  . '&quot; herunterladen" alt="Download" /></a>';
									$contentExportPDF .= '<a href="' . $thisParsedUrl["path"] . '?downloadFile=' . basename($ds["orderDocumentsDocumentPath"]) . '&thisDocumentType=' . $ds["orderDocumentsType"] . '">' . '<img src="layout/icons/icon' . getFileType(basename($ds["orderDocumentsDocumentPath"])) . '.gif" width="16" height="16" title="Dokument &quot;' . $ds["orderDocumentsNumber"] . '&quot; herunterladen" alt="Download" /></a>';
									$contentExportPDF .= '<input type="hidden" name="arrProvisionValues['.$countRow.'][orderDocumentsNumber]" value="' . $ds["orderDocumentsNumber"] . '" />';
									$contentExportPDF .= '</td>';
									#$contentExportPDF .= '<td>';
									#$contentExportPDF .= $arrPaymentStatusTypeDatas[$ds["orderDocumentsStatus"]]["paymentStatusTypesName"];
									#$contentExportPDF .= '</td>';
									$contentExportPDF .= '<!-- EOF DOCUMENT_NUMBER_ORDER -->';
									$contentExportPDF .= '<td>';
									if($ds["orderDocumentsOrderDate"] > 0) {
										$thisDate = $ds["orderDocumentsOrderDate"];
									}
									else {
										$thisDate = $ds["orderDocumentsProcessingDate"];
									}
									$thisDate = $ds["orderDocumentsProcessingDate"];
									$contentExportPDF .= formatDate($thisDate, 'display');
									$contentExportPDF .= '<input type="hidden" name="arrProvisionValues['.$countRow.'][orderDocumentsOrderDate]" value="' . $thisDate . '" />';
									$contentExportPDF .= '</td>';
									$contentExportPDF .= '<td style="background-color:#FEFFAF;">';
									#$contentExportPDF .= '<a href="' . PAGE_EDIT_CUSTOMER . '?editCustomerNumber=' . $ds["orderDocumentsCustomerNumber"]. '"><b title="Zu den Kundendaten">' . $ds["orderDocumentsCustomerNumber"]. '</b></a>';
									$contentExportPDF .= '' . $ds["orderDocumentsCustomerNumber"]. '';
									$contentExportPDF .= '<input type="hidden" name="arrProvisionValues['.$countRow.'][orderDocumentsCustomerNumber]" value="' . $ds["orderDocumentsCustomerNumber"] . '" />';
									$contentExportPDF .= '</td>';
									$contentExportPDF .= '<td>';
									$contentExportPDF .= wordwrap($ds["orderDocumentsAddressCompany"], 20, "<br>", false);
									$contentExportPDF .= '<input type="hidden" name="arrProvisionValues['.$countRow.'][orderDocumentsAddressCompany]" value="' . $ds["orderDocumentsAddressCompany"] . '" />';
									$contentExportPDF .= '</td>';
									$contentExportPDF .= '<td>';
									$contentExportPDF .= $ds["orderDocumentsAddressZipcode"]. ' ' . $ds["orderDocumentsAddressCity"];
									$contentExportPDF .= '<input type="hidden" name="arrProvisionValues['.$countRow.'][orderDocumentsAddressZipcode]" value="' . $ds["orderDocumentsAddressZipcode"] . '" />';
									$contentExportPDF .= '<input type="hidden" name="arrProvisionValues['.$countRow.'][orderDocumentsAddressCity]" value="' . $ds["orderDocumentsAddressCity"] . '" />';
									$contentExportPDF .= '</td>';
									$contentExportPDF .= '<td style="background-color:#FEFFAF;">';
									$contentExportPDF .= wordwrap(specialSubstr($ds["orderDocumentDetailProductName"], 36), 20, "<br>", false);
									$contentExportPDF .= '<input type="hidden" name="arrProvisionValues['.$countRow.'][orderDocumentDetailProductName]" value="' . specialSubstr($ds["orderDocumentDetailProductName"]) . '" />';
									$contentExportPDF .= '<input type="hidden" name="arrProvisionValues['.$countRow.'][orderDocumentDetailProductNumber]" value="' . $ds["orderDocumentDetailProductNumber"] . '" />';
									$contentExportPDF .= '</td>';
									$contentExportPDF .= '<td style="text-align:right;" class="cellQuantity">';
									$contentExportPDF .= '<b>' . $ds["orderDocumentDetailProductQuantity"] . '</b>';
									$contentExportPDF .= '<input type="hidden" name="arrProvisionValues['.$countRow.'][orderDocumentDetailProductQuantity]" value="' . $ds["orderDocumentDetailProductQuantity"] . '" />';
									$contentExportPDF .= '</td>';
									$contentExportPDF .= '<td style="text-align:right; white-space:nowrap;" class="cellVkprice">';
									$contentExportPDF .= '<!-- BOF VK_PRICE_'.$countRow.' -->';
										$thisPriceStyle = '';
										if($ds["orderDocumentDetailProductSinglePrice"] < 0) {
											if($ds["orderDocumentsType"] != 'GU') {
												$ds["orderDocumentDetailProductSinglePrice"] = 0;
											}
											else {
												$thisPriceStyle = ' font-weight:bold; color:#FF0000; ';
											}
										}
										$contentExportPDF .= '<input type="text" class="inputField_40" style="text-align:right;' . $thisPriceStyle . '" name="arrProvisionValues['.$countRow.'][salesmanProvisionProductVkprice]" value="' . number_format($ds["orderDocumentDetailProductSinglePrice"], 2, ',' ,'') . '"/> EUR';
									$contentExportPDF .= '<!-- EOF VK_PRICE_'.$countRow.' -->';
									$contentExportPDF .= '</td>';
									$contentExportPDF .= '<td style="text-align:right; white-space:nowrap;background-color:#FEFFAF;" class="cellProvision">';

									if($ds["salesmenProvisionsDetailsProductProvisionPrice"] == '' || $ds["salesmenProvisionsDetailsProductProvisionPrice"] == null) {
										$ds["salesmenProvisionsDetailsProductProvisionPrice"] == 0;
									}
									if($arrSalesmanDatas["salesmenProvision"] > 0){
										// $thisProvision = $ds["orderDocumentDetailProductQuantity"] * ($ds["orderDocumentDetailProductSinglePrice"] * $arrSalesmanDatas["salesmenProvision"]/100);
										$thisProvision = $ds["orderDocumentDetailProductQuantity"] * $ds["orderDocumentDetailProductSinglePrice"];

										if(substr($ds["orderDocumentDetailProductKategorieID"], 0, 3) == '001') {
											if($ds["orderDocumentDetailProductSinglePrice"] > KZH_EK_PRICE){
												#$thisProvision = 0;
											}
											$thisProvision = 0;
										}

									}
									else {
										$thisProvision = 0;
										if(substr($ds["orderDocumentDetailProductKategorieID"], 0, 3) == '001') {
											$thisProvision = valueSign($ds["orderDocumentDetailProductSinglePrice"]) * $ds["orderDocumentDetailProductQuantity"] * (abs($ds["orderDocumentDetailProductSinglePrice"]) - KZH_EK_PRICE);
										}
									}
									if($ds["salesmenProvisionsDetailsProductProvisionPrice"] != '') {
										$thisProvision = $ds["salesmenProvisionsDetailsProductProvisionPrice"];
									}

									# if($thisProvision < 0) { $thisProvision = 0; }

									$displayTotalSum += $thisProvision;
									# echo $arrSalesmanDatas["salesmenProvision"] . '|' . $displayTotalSum . ' / ' . $thisProvision . ' / ' . $ds["salesmenProvisionsDetailsProductProvisionPrice"] . '<br />';
									$contentExportPDF .= '<!-- BOF PROVISION_PRICE_'.$countRow.' -->';
									$contentExportPDF .= '<input type="text" class="inputField_50" style="text-align:right;' . $thisPriceStyle . '" name="arrProvisionValues['.$countRow.'][salesmanProvisionValue]" value="' . number_format($thisProvision, 2, ',', '') . '"/>';
									$contentExportPDF .= '<!-- EOF PROVISION_PRICE_'.$countRow.' -->';
									$contentExportPDF .= ' EUR';
									$contentExportPDF .= '</td>';
									$contentExportPDF .= '</tr>';

									$contentExportPDF .= '<tr>';
									$contentExportPDF .= '<td></td>';
									$contentExportPDF .= '<!-- BOF DOCUMENT_NUMBER_ORDER -->';
									$contentExportPDF .= '<td></td>';
									$contentExportPDF .= '<!-- EOF DOCUMENT_NUMBER_ORDER -->';
									$contentExportPDF .= '<td colspan="8">';
									/*
									if(substr($ds["orderDocumentDetailProductKategorieID"], 0, 3) != '001') {
										if(!($ds["orderDocumentsShippingCosts"] > 0)) {
											//  . $ds["orderDocumentDetailProductKategorieID"] . '|' . $ds["orderDocumentsShippingCosts"]. ' | ' . $ds["orderDocumentsShippingCostsPackages"]. ' | ' . $ds["orderDocumentsShippingCostsPerPackage"]
											$contentExportPDF .= '<span class="notice" style="color:#FF0000;"><b>KEINE Versandkosten berechnet!!!</b></span>';
										}
									}
									*/
									$contentExportPDF .= '</td>';
									$contentExportPDF .= '</tr>';

									$countRow++;
								}

								$sql_additional = "
										SELECT
													`" . TABLE_SALESMEN_PROVISIONS_DETAILS . "`.`salesmenProvisionsDetailsID`,
													`" . TABLE_SALESMEN_PROVISIONS_DETAILS . "`.`salesmenProvisionsDetailsProvisionsID`,
													`" . TABLE_SALESMEN_PROVISIONS_DETAILS . "`.`salesmenProvisionsDetailsDocumentsDetailID`,
													`" . TABLE_SALESMEN_PROVISIONS_DETAILS . "`.`salesmenProvisionsDetailsDocumentNumber`,
													`" . TABLE_SALESMEN_PROVISIONS_DETAILS . "`.`salesmenProvisionsDetailsProductOrderDate`,
													`" . TABLE_SALESMEN_PROVISIONS_DETAILS . "`.`salesmenProvisionsDetailsProductAmount`,
													`" . TABLE_SALESMEN_PROVISIONS_DETAILS . "`.`salesmenProvisionsDetailsProductNumber`,
													`" . TABLE_SALESMEN_PROVISIONS_DETAILS . "`.`salesmenProvisionsDetailsProductName`,
													`" . TABLE_SALESMEN_PROVISIONS_DETAILS . "`.`salesmenProvisionsDetailsProductRecipientCustomerNumber`,
													`" . TABLE_SALESMEN_PROVISIONS_DETAILS . "`.`salesmenProvisionsDetailsProductRecipientName`,
													`" . TABLE_SALESMEN_PROVISIONS_DETAILS . "`.`salesmenProvisionsDetailsProductRecipientZipcode`,
													`" . TABLE_SALESMEN_PROVISIONS_DETAILS . "`.`salesmenProvisionsDetailsProductRecipientCity`,
													`" . TABLE_SALESMEN_PROVISIONS_DETAILS . "`.`salesmenProvisionsDetailsProductVkPreis`,
													`" . TABLE_SALESMEN_PROVISIONS_DETAILS . "`.`salesmenProvisionsDetailsProductProvisionPrice`,
													`" . TABLE_SALESMEN_PROVISIONS_DETAILS . "`.`salesmenProvisionsDetailsDisabled`,
													`" . TABLE_SALESMEN_PROVISIONS_DETAILS . "`.`salesmenProvisionsDetailsType`

												FROM `" . TABLE_SALESMEN_PROVISIONS_DETAILS . "`

												WHERE 1
													AND `" . TABLE_SALESMEN_PROVISIONS_DETAILS . "`.`salesmenProvisionsDetailsDisabled` != '1'
													AND `" . TABLE_SALESMEN_PROVISIONS_DETAILS . "`.`salesmenProvisionsDetailsType` = 'additional'
													AND `" . TABLE_SALESMEN_PROVISIONS_DETAILS . "`.`salesmenProvisionsDetailsProductOrderDate` BETWEEN '{###DATE_EXPORT_START###}' AND '{###DATE_EXPORT_END###}'
													AND `" . TABLE_SALESMEN_PROVISIONS_DETAILS . "`.`salesmenProvisionsDetailsProvisionsID` = '" . $thisProvisionID . "'
									";
								$sql_additional = preg_replace("/{###DATE_EXPORT_START###}/", $dateStart, $sql_additional);
								$sql_additional = preg_replace("/{###DATE_EXPORT_END###}/", $dateEnd, $sql_additional);

								$rs_additional = $dbConnection->db_query($sql_additional);
								$countTotalRows_additional = $dbConnection->db_getMysqlNumRows($rs_additional);
								if($countTotalRows_additional > 0){
									while($ds = mysqli_fetch_assoc($rs_additional)){

									if($thisProvisionID == 0){ $thisProvisionID = $ds["salesmenProvisionsDetailsProvisionsID"]; }
									if($countRow%2 == 0){ $rowClass = 'row0'; }
									else { $rowClass = 'row1'; }
									//  '.$rowClass.'
									$contentExportPDF .= '<tr class="displayCardDatasItemRows">';
									$contentExportPDF .= '<td style="text-align:right;">';
									$contentExportPDF .= '<b>' . ($countRow + 1) . '.</b>';
									$contentExportPDF .= '<input type="hidden" name="arrProvisionValues['.$countRow.'][orderDocumentDetailID]" value="' . $ds["orderDocumentDetailID"] . '" />';
									$contentExportPDF .= '<input type="hidden" name="arrProvisionValues['.$countRow.'][salesmenProvisionsDetailsDisabled]" value="0" />';
									$contentExportPDF .= '<input type="hidden" name="arrProvisionValues['.$countRow.'][salesmenProvisionsDetailsType]" value="additional" />';
									$contentExportPDF .= '</td>';
									$contentExportPDF .= '<!-- BOF DOCUMENT_NUMBER_ORDER -->';
									$contentExportPDF .= '<td style="background-color:#FEFFAF; white-space:nowrap;">';
									$contentExportPDF .= '' . $ds["salesmenProvisionsDetailsDocumentNumber"] . '';
									$contentExportPDF .= '<a href="' . $thisParsedUrl["path"] . '?downloadFile=' . basename($ds["salesmenProvisionsDetailsDocumentNumber"] . '_KNR-' . $ds["salesmenProvisionsDetailsProductRecipientCustomerNumber"] . '.pdf') . '&thisDocumentType=' . substr($ds["salesmenProvisionsDetailsDocumentNumber"], 0, 2) . '">' . '<img src="layout/icons/iconPDF.gif" width="16" height="16" title="Dokument &quot;' . $ds["orderDocumentsNumber"] . '&quot; herunterladen" alt="Download" /></a>';
									$contentExportPDF .= '<input type="hidden" name="arrProvisionValues['.$countRow.'][orderDocumentsNumber]" value="' . $ds["salesmenProvisionsDetailsDocumentNumber"] . '" />';
									#$contentExportPDF .= '</td>';
									#$contentExportPDF .= '<td>';
									#$contentExportPDF .= $arrPaymentStatusTypeDatas[$ds["orderDocumentsStatus"]]["paymentStatusTypesName"];
									#$contentExportPDF .= '</td>';
									$contentExportPDF .= '<!-- EOF DOCUMENT_NUMBER_ORDER -->';
									$contentExportPDF .= '<td>';
									$thisDate = $ds["salesmenProvisionsDetailsProductOrderDate"];
									$contentExportPDF .= formatDate($thisDate, 'display');
									$contentExportPDF .= '<input type="hidden" name="arrProvisionValues['.$countRow.'][orderDocumentsOrderDate]" value="' . $thisDate . '" />';
									$contentExportPDF .= '</td>';
									$contentExportPDF .= '<td style="background-color:#FEFFAF;">';
									#$contentExportPDF .= '<a href="' . PAGE_EDIT_CUSTOMER . '?editCustomerNumber=' . $ds["salesmenProvisionsDetailsProductRecipientCustomerNumber"]. '"><b title="Zu den Kundendaten">' . $ds["salesmenProvisionsDetailsProductRecipientCustomerNumber"]. '</b></a>';
									$contentExportPDF .= '' . $ds["salesmenProvisionsDetailsProductRecipientCustomerNumber"]. '';
									$contentExportPDF .= '<input type="hidden" name="arrProvisionValues['.$countRow.'][orderDocumentsCustomerNumber]" value="' . $ds["salesmenProvisionsDetailsProductRecipientCustomerNumber"] . '" />';
									$contentExportPDF .= '</td>';
									$contentExportPDF .= '<td>';
									$contentExportPDF .= wordwrap($ds["salesmenProvisionsDetailsProductRecipientName"], 20, "<br>", false);
									$contentExportPDF .= '<input type="hidden" name="arrProvisionValues['.$countRow.'][orderDocumentsAddressCompany]" value="' . $ds["salesmenProvisionsDetailsProductRecipientName"] . '" />';
									$contentExportPDF .= '</td>';
									$contentExportPDF .= '<td>';
									$contentExportPDF .= $ds["salesmenProvisionsDetailsProductRecipientZipcode"]. ' ' . $ds["salesmenProvisionsDetailsProductRecipientCity"];
									$contentExportPDF .= '<input type="hidden" name="arrProvisionValues['.$countRow.'][orderDocumentsAddressZipcode]" value="' . $ds["salesmenProvisionsDetailsProductRecipientZipcode"] . '" />';
									$contentExportPDF .= '<input type="hidden" name="arrProvisionValues['.$countRow.'][orderDocumentsAddressCity]" value="' . $ds["salesmenProvisionsDetailsProductRecipientCity"] . '" />';
									$contentExportPDF .= '</td>';
									$contentExportPDF .= '<td style="background-color:#FEFFAF;">';
									$contentExportPDF .= wordwrap(specialSubstr($ds["salesmenProvisionsDetailsProductName"], 36), 20, "<br>", false);
									$contentExportPDF .= '<input type="hidden" name="arrProvisionValues['.$countRow.'][orderDocumentDetailProductName]" value="' . specialSubstr($ds["salesmenProvisionsDetailsProductName"]) . '" />';
									$contentExportPDF .= '<input type="hidden" name="arrProvisionValues['.$countRow.'][orderDocumentDetailProductNumber]" value="' . $ds["salesmenProvisionsDetailsProductNumber"] . '" />';
									$contentExportPDF .= '</td>';
									$contentExportPDF .= '<td style="text-align:right;" class="cellQuantity">';
									$contentExportPDF .= '<b>' . $ds["orderDocumentDetailProductQuantity"] . '</b>';
									$contentExportPDF .= '<input type="hidden" name="arrProvisionValues['.$countRow.'][orderDocumentDetailProductQuantity]" value="' . $ds["orderDocumentDetailProductQuantity"] . '" />';
									$contentExportPDF .= '</td>';
									$contentExportPDF .= '<td style="text-align:right; white-space:nowrap;" class="cellVkprice">';
									$contentExportPDF .= '<!-- BOF VK_PRICE_'.$countRow.' -->';
										$thisPriceStyle = '';
										if($ds["salesmenProvisionsDetailsProductProvisionPrice"] < 0) {
											$thisPriceStyle = ' font-weight:bold; color:#FF0000; ';
										}
										$contentExportPDF .= '<input type="text" class="inputField_40" style="text-align:right;' . $thisPriceStyle . '" name="arrProvisionValues['.$countRow.'][salesmanProvisionProductVkprice]" value="' . number_format($ds["orderDocumentDetailProductSinglePrice"], 2, ',' ,'') . '"/> EUR';
									$contentExportPDF .= '<!-- EOF VK_PRICE_'.$countRow.' -->';
									$contentExportPDF .= '</td>';
									$contentExportPDF .= '<td style="text-align:right; white-space:nowrap;background-color:#FEFFAF;" class="cellProvision">';

									if($ds["salesmenProvisionsDetailsProductProvisionPrice"] == '' || $ds["salesmenProvisionsDetailsProductProvisionPrice"] == null) {
										$ds["salesmenProvisionsDetailsProductProvisionPrice"] == 0;
									}
									if($arrSalesmanDatas["salesmenProvision"] > 0){
										// $thisProvision = $ds["orderDocumentDetailProductQuantity"] * ($ds["orderDocumentDetailProductSinglePrice"] * $arrSalesmanDatas["salesmenProvision"]/100);
										$thisProvision = $ds["orderDocumentDetailProductQuantity"] * $ds["orderDocumentDetailProductSinglePrice"];
									}
									else {
										$thisProvision = 0;
										if(substr($ds["orderDocumentDetailProductKategorieID"], 0, 3) == '001') {
											$thisProvision = valueSign($ds["orderDocumentDetailProductSinglePrice"]) * $ds["orderDocumentDetailProductQuantity"] * (abs($ds["orderDocumentDetailProductSinglePrice"]) - KZH_EK_PRICE);
										}
									}
									if($ds["salesmenProvisionsDetailsProductProvisionPrice"] != '') {
										$thisProvision = $ds["salesmenProvisionsDetailsProductProvisionPrice"];
									}

									# if($thisProvision < 0) { $thisProvision = 0; }

									$displayTotalSum += $thisProvision;
									# echo $arrSalesmanDatas["salesmenProvision"] . '|' . $displayTotalSum . ' / ' . $thisProvision . ' / ' . $ds["salesmenProvisionsDetailsProductProvisionPrice"] . '<br />';
									$contentExportPDF .= '<!-- BOF PROVISION_PRICE_'.$countRow.' -->';
									$contentExportPDF .= '<input type="text" class="inputField_50" style="text-align:right;' . $thisPriceStyle . '" name="arrProvisionValues['.$countRow.'][salesmanProvisionValue]" value="' . number_format($thisProvision, 2, ',', '') . '"/>';
									$contentExportPDF .= '<!-- EOF PROVISION_PRICE_'.$countRow.' -->';
									$contentExportPDF .= ' EUR';
									$contentExportPDF .= '</td>';
									$contentExportPDF .= '</tr>';

									$contentExportPDF .= '<tr>';
									$contentExportPDF .= '<td></td>';
									$contentExportPDF .= '<!-- BOF DOCUMENT_NUMBER_ORDER -->';
									$contentExportPDF .= '<td></td>';
									$contentExportPDF .= '<!-- EOF DOCUMENT_NUMBER_ORDER -->';
									$contentExportPDF .= '<td colspan="8">';
									/*
									if(substr($ds["orderDocumentDetailProductKategorieID"], 0, 3) != '001') {
										if(!($ds["orderDocumentsShippingCosts"] > 0)) {
											//  . $ds["orderDocumentDetailProductKategorieID"] . '|' . $ds["orderDocumentsShippingCosts"]. ' | ' . $ds["orderDocumentsShippingCostsPackages"]. ' | ' . $ds["orderDocumentsShippingCostsPerPackage"]
											$contentExportPDF .= '<span class="notice" style="color:#FF0000;"><b>KEINE Versandkosten berechnet!!!</b></span>';
										}
									}
									*/
									$contentExportPDF .= '</td>';
									$contentExportPDF .= '</tr>';

									$countRow++;
								}
								}


								$contentExportPDF .= '<tr>';
								$contentExportPDF .= '<td>&nbsp;</td>';
								$contentExportPDF .= '<!-- BOF DOCUMENT_NUMBER_ORDER -->';
								$contentExportPDF .= '<td>&nbsp;</td>';
								$contentExportPDF .= '<!-- EOF DOCUMENT_NUMBER_ORDER -->';
								$contentExportPDF .= '<td colspan="8">&nbsp;</td>';
								$contentExportPDF .= '</tr>';

								$contentExportPDF .= '<tr>';
								$contentExportPDF .= '<td>&nbsp;</td>';
								$contentExportPDF .= '<!-- BOF DOCUMENT_NUMBER_ORDER -->';
								$contentExportPDF .= '<td>&nbsp;</td>';
								$contentExportPDF .= '<!-- EOF DOCUMENT_NUMBER_ORDER -->';
								$contentExportPDF .= '<td colspan="5">&nbsp;</td>';
								$contentExportPDF .= '<td colspan="2"><b>Zwischensumme:</b></td>';
								$contentExportPDF .= '<td style="white-space:nowrap;text-align:right;"><!-- BOF SUM_VALUE --><b><span id="displayTotalSum">'.number_format($displayTotalSum, 2, ',', '') .'</span></b><!-- EOF SUM_VALUE --> EUR</td>';
								$contentExportPDF .= '</tr>';

								$valueProvision = 0;
								if($arrSalesmanDatas["salesmenProvision"] > 0){
									$contentExportPDF .= '<tr>';
									$contentExportPDF .= '<td>&nbsp;</td>';
									$contentExportPDF .= '<!-- BOF DOCUMENT_NUMBER_ORDER -->';
									$contentExportPDF .= '<td>&nbsp;</td>';
									$contentExportPDF .= '<!-- EOF DOCUMENT_NUMBER_ORDER -->';
									$contentExportPDF .= '<td colspan="5">&nbsp;</td>';
									$contentExportPDF .= '<td colspan="2"><b>Gutschrift (' . $arrSalesmanDatas["salesmenProvision"]. ' %):</b></td>';
									$valueProvision = $displayTotalSum * ($arrSalesmanDatas["salesmenProvision"]/100);
									$contentExportPDF .= '<td style="white-space:nowrap;text-align:right;"><!-- BOF PROVISION_VALUE --><b><span id="displayProvision">'.number_format($valueProvision, 2, ',', '') .'</span></b><!-- EOF PROVISION_VALUE --> EUR</td>';
									$contentExportPDF .= '</tr>';
								}

								$contentExportPDF .= '<tr>';
								$contentExportPDF .= '<td>&nbsp;</td>';
								$contentExportPDF .= '<!-- BOF DOCUMENT_NUMBER_ORDER -->';
								$contentExportPDF .= '<td>&nbsp;</td>';
								$contentExportPDF .= '<!-- EOF DOCUMENT_NUMBER_ORDER -->';
								$contentExportPDF .= '<td colspan="5">&nbsp;</td>';
								$contentExportPDF .= '<td colspan="2"><b>MwSt. (' . MWST. ' %):</b></td>';
								$valueMwst = ($displayTotalSum + $valueProvision) * MWST/100;
								$contentExportPDF .= '<td style="white-space:nowrap;text-align:right;"><!-- BOF MWST_VALUE --><b><span id="displayMWST">'.number_format($valueMwst, 2, ',', '') .'</span></b><!-- EOF MWST_VALUE --> EUR</td>';
								$contentExportPDF .= '</tr>';

								$contentExportPDF .= '<tr>';
								$contentExportPDF .= '<td>&nbsp;</td>';
								$contentExportPDF .= '<!-- BOF DOCUMENT_NUMBER_ORDER -->';
								$contentExportPDF .= '<td>&nbsp;</td>';
								$contentExportPDF .= '<!-- EOF DOCUMENT_NUMBER_ORDER -->';
								$contentExportPDF .= '<td colspan="5">&nbsp;</td>';
								$contentExportPDF .= '<td colspan="2" id="sumline">&nbsp;</td>';
								$contentExportPDF .= '<td id="sumline"> </td>';
								$contentExportPDF .= '</tr>';

								$contentExportPDF .= '<tr>';
								$contentExportPDF .= '<td>&nbsp;</td>';
								$contentExportPDF .= '<!-- BOF DOCUMENT_NUMBER_ORDER -->';
								$contentExportPDF .= '<td>&nbsp;</td>';
								$contentExportPDF .= '<!-- EOF DOCUMENT_NUMBER_ORDER -->';
								$contentExportPDF .= '<td colspan="5">&nbsp;</td>';
								$contentExportPDF .= '<td colspan="2"><b>Gesamt:</b></td>';
								$contentExportPDF .= '<td style="white-space:nowrap;text-align:right;"><!-- BOF TOTAL_VALUE --><b><span id="displayTotal">'.number_format($displayTotalSum + $valueProvision + $valueMwst, 2, ',', '') .'</span></b><!-- EOF TOTAL_VALUE --> EUR</td>';
								$contentExportPDF .= '</tr>';

								$contentExportPDF .= '</tbody>';

								$contentExportPDF .= '<tfoot>';
								$contentExportPDF .= '<tr>';
								$contentExportPDF .= '<td>&nbsp;</td>';
								$contentExportPDF .= '<!-- BOF DOCUMENT_NUMBER_ORDER -->';
								$contentExportPDF .= '<td>&nbsp;</td>';
								$contentExportPDF .= '<!-- EOF DOCUMENT_NUMBER_ORDER -->';
								$contentExportPDF .= '<td colspan="5">&nbsp;</td>';
								$contentExportPDF .= '<td colspan="2" id="sumline">&nbsp;</td>';
								$contentExportPDF .= '<td id="sumline"> </td>';
								$contentExportPDF .= '</tr>';
								$contentExportPDF .= '</tfoot>';

								$contentExportPDF .= '</table>';

								echo $contentExportPDF;

								$pdfContentPage = $contentExportPDF;
								$pdfContentPage = removeUnnecessaryChars($pdfContentPage);

								// BOF WRITE TEMP HTML FILE
								// $thisCreatedBaseName = 'Provision_' . date('Y-m-d') . '_' . convertChars($arrSalesmenDatas[$_POST["exportOrdersVertreterID"]]["customersFirmenname"]) . '_' . $arrSalesmenDatas[$_POST["exportOrdersVertreterID"]]["customersKundennummer"];
								$thisCreatedBaseName = 'Interne-Gutschrift_' . date('Y-m-d') . '_' . convertChars($arrSalesmenDatas[$_POST["exportOrdersVertreterID"]]["customersFirmenname"]) . '_' . $arrSalesmenDatas[$_POST["exportOrdersVertreterID"]]["customersKundennummer"];
								$thisCreatedPdfName = $thisCreatedBaseName . '.pdf';
								$thisCreatedCsvName = $thisCreatedBaseName . '.csv';
								$tempHtmlFileName = "_temp_".$thisCreatedPdfName.'.html';
								if(file_exists(DIRECTORY_CREATED_DOCUMENTS_SALESMEN . $tempHtmlFileName)) {
									unlink(DIRECTORY_CREATED_DOCUMENTS_SALESMEN . $tempHtmlFileName);
								}
								clearstatcache();
								$fp = fopen(DIRECTORY_CREATED_DOCUMENTS_SALESMEN . $tempHtmlFileName, 'w');
								fwrite($fp, stripslashes($pdfContentPage));
								fclose($fp);
								if(file_exists(DIRECTORY_CREATED_DOCUMENTS_SALESMEN . $tempHtmlFileName)) {
									#$successMessage .= 'Die tempor&auml;re Datei &quot;' . basename(DIRECTORY_CREATED_DOCUMENTS_SALESMEN . $tempHtmlFileName) . '&quot; wurde erstellt.' . '<br />';
								}
								else  {
									#$errorMessage .= 'Die tempor&auml;re Datei &quot;' . basename(DIRECTORY_CREATED_DOCUMENTS_SALESMEN . $tempHtmlFileName) . '&quot; konnte nicht erstellt werden.' . '<br />';
								}
								// EOF WRITE TEMP HTML FILE

								echo '<input type="hidden" name="thisCreatedPdfName" value="' . $thisCreatedPdfName . '" />';
								echo '<input type="hidden" name="thisCreatedCsvName" value="' . $thisCreatedCsvName . '" />';
								echo '<input type="hidden" name="tempHtmlFileName" value="' . $tempHtmlFileName . '" />';
								echo '<input type="hidden" name="exportOrdersVertreterID" value="' . $_POST["exportOrdersVertreterID"] . '" />';

								if($arrGetUserRights["editProvisionDatas"]) {
									echo '<div class="actionButtonsArea">';
									echo '<input type="submit" class="inputButton3" name="submitFormProvision" value="Daten NUR speichern" /> ';
									echo '<input type="submit" class="inputButton3" name="createDocumentProvision" value="Speichern und PDF-Datei erzeugen" />';
									echo '</div>';
								}
								echo '</form>';

								echo '<br />';
							}
							else {
								$errorMessage .= ' F&uuml;r diesen Vertreter liegen keine offenen Provisionen vor.';
							}
						}
					?>
					<?php displayMessages(); ?>
				</div>

			</div>
		</div>
		<div class="clear"></div>
	</div>
</div>
<script language="javascript" type="text/javascript">
	$(document).ready(function() {
		$(function() {
			$.datepicker.setDefaults($.datepicker.regional[""]);
			$('#exportOrdersDateFrom').datepicker($.datepicker.regional["de"]);
			$('#exportOrdersDateTo').datepicker($.datepicker.regional["de"]);
		});
		colorRowMouseOver('#displayCardDatas tbody tr');
		$(document).scroll(function(){
			fixTableHeader('#displayCardDatas');
		});

		function sumProvisions() {
			// alert($(this).val());
			var totalSum = 0;
			var tempSum = 0;
			var valueSign = 1;
			var mwst = <?php echo MWST; ?>;
			var mwstValue = 0;
			var total = 0;
			$('#displayCardDatas .cellProvision input').each(function(index, value){
				// alert(index + ': ' + $(value).val());
				tempSum = $(value).val();
				tempSum = tempSum.replace(',', '.');
				tempSum = parseFloat(tempSum);
				tempSum = tempSum * 1;
				valueSign = 1;
				//alert(tempSum);
				if(tempSum < 0){
					////valueSign = -1;
					//tempSum = 0;
					//$(value).val(0);
					//$(this).css('font-weight', 'bold');
					//$(this).css('color', '#FF0000');
				}
				else {
					//$(this).css('font-weight', 'normal');
					//$(this).css('color', '#000000');
				}
				//alert(tempSum);
				tempSum = valueSign * tempSum;
				//alert(tempSum)
				totalSum += tempSum;
				//alert(valueSign + " * " + tempSum + " = " + totalSum);
			});
			//alert(totalSum);
			provisionValue = 0;
			<?php
				if($arrSalesmanDatas["salesmenProvision"] > 0){
			?>
			provisionValue = totalSum * <?php echo $arrSalesmanDatas["salesmenProvision"]; ?> / 100;
			<?php
				}
			?>
			mwstValue = (totalSum + provisionValue) * mwst/100;
			total = (totalSum + provisionValue) + mwstValue;

			totalSum = parseFloat(totalSum);
			totalSum = totalSum.toFixed(2);
			totalSum = totalSum.replace('.', ',');

			provisionValue = parseFloat(provisionValue);
			provisionValue = provisionValue.toFixed(2);
			provisionValue = provisionValue.replace('.', ',');

			mwstValue = parseFloat(mwstValue);
			mwstValue = mwstValue.toFixed(2);
			mwstValue = mwstValue.replace('.', ',');

			total = parseFloat(total);
			total = total.toFixed(2);
			total = total.replace('.', ',');

			$('#displayTotalSum').text(totalSum);
			$('#displayProvision').text(provisionValue);
			$('#displayMWST').text(mwstValue);
			$('#displayTotal').text(total);
		}

		$('#displayCardDatas .cellVkprice input').keyup(function(){
			var valueSign = 1;
			var thisVkPrice = $(this).val();
			thisVkPrice = thisVkPrice.replace(',', '.');
			thisVkPrice = thisVkPrice * 1;
			if(thisVkPrice < 0) { valueSign = -1;}
			var thisProvision = 0;
			var thisQuantity = $(this).parent().parent().find('.cellQuantity input').val();
			thisProvision = valueSign * thisQuantity * (Math.abs(thisVkPrice) - <?php echo KZH_EK_PRICE; ?>);
			thisProvision = thisProvision.toFixed(2)
			$(this).parent().parent().find('.cellProvision input').val(thisProvision);
			sumProvisions();
		});
		$('#displayCardDatas .cellProvision input').keyup(function(){
			sumProvisions();
		});

	});
</script>
<?php require_once('inc/footerHTML.inc.php'); ?>