<?php
	require_once('inc/requires.inc.php');
	require_once('inc/functions.inc.php');
	require_once(PATH_MAILBOX_CLASS);

	$jswindowMessage = "";
	$warningMessage = '';
	$errorMessage = '';
	$successMessage = '';
	$infoMessage = '';

	if($_GET["errorCode"] != ''){
		$errorMessage .= ' [FEHLER ' . $_GET["errorCode"] . '] ';
	}

	if($_GET["noRights"]){
		$errorMessage .= 'Sie haben keinen Zugriff auf die von Ihnen angeforderte Seite. ' . '<br />';
		#echo $errorMessage;
		#exit;
	}
	if($_GET["noExists"]){
		$errorMessage .= 'Die von Ihnen angeforderte Seite existiert nicht. ' . '<br />';
		#echo $errorMessage;
		#exit;
	}
	if($_GET["noConnex"]){
		$errorMessage .= 'Es kann keine Verbindung zur Datenbank hergestellt werden. ' . '<br />';
		#echo $errorMessage;
		#exit;
	}

	$dbConnection = new DB_Connection();
	$db_open = $dbConnection->db_connect();

	


?>
<?php
	require_once('inc/headerHTML.inc.php');
	$thisTitle = "Startseite";
	$headerHTML = preg_replace("/{###TITLE###}/", strip_tags($thisTitle), $headerHTML);
	echo $headerHTML;

	require_once(FILE_MENUE_TOP);
?>

<div id="mainArea">
	<div id="mainContent">
		<?php require_once(FILE_MENUE_SIDEBAR); ?>
		<div id="contentArea">
			<div id="contentAreaElements">
				<h1><?php if(SHOW_PAGE_TITLE_ICONS) { echo '<img src="' . PATH_ICONS_MENUE_TITLES . 'home.png" alt="" />'; } ?> <?php echo $thisTitle; ?></h1>
				<div class="contentDisplay">
					<div>
					<?php if(SHOW_DATA_BOXES) { ?>
					<div class="dataInfoArea">
						<?php
							$thisSrc = 'notifications.png';
							$modeSound = 'play';
							$thisStyle = '';
							$thisTitle = '';
							if(EMBED_SOUND_FILES && STOP_SOUND_FILES) {
								if($_COOKIE["modeSound"] == 'play'){
									$thisTitle = 'Alarm abschalten';
									$thisSrc = 'notifications.png';
									$modeSound = 'stop';
								}
								else {
									$thisTitle = 'Alarm einschalten';
									$thisSrc = 'noNotifications.png';
									$modeSound = 'play';
								}
								$thisStyle = 'cursor:pointer;';
							}
						?>
						<div class="dataInfoAreaHeader">
							<img id='buttonSoundNotifications' class='buttonSoundNotifications' src="<?php echo PATH_ICONS_MENUE_SIDEBAR; ?><?php echo $thisSrc; ?>" alt="" title="<?php echo $thisTitle; ?>" style="<?php echo $thisStyle; ?>" width="16" height="16" />
							Benachrichtigungen
						</div>
						<div class="dataInfoAreaContent">
							<?php
								$playAlarm = false;
								  
								// BOF INFOBOX WEB CONNECTION AND EXTERNAL DB CONNECTION
								if(!$isWebConnection || !$isExternalDBConnection){
									echo '<div class="dataBoxItem">';
									echo '<div class="dataBoxItemHeader">';
									if(SHOW_INFO_BOXES_ICONS) {
										echo '<img src="' . PATH_ICONS_MENUE_TITLES . 'warning.png" alt="" width="16" height="16" />';
									}
									echo 'ACHTUNG:</div>';
									echo '<div class="dataBoxItemContent">';
									if(!$isWebConnection){
										echo '<p style="color:#FF0000;"><b>Keine Internet-Verbindung</b></p>';
									}
									if(!$isExternalDBConnection){
										echo '<p style="color:#FF0000;"><b>Keine Verbindung zur Online-Datenbank</b></p>';
									}
									echo '</div>';
									echo '</div>';
								}
								// EOF INFOBOX WEB CONNECTION AND EXTERNAL DB CONNECTION

								// BOF INFOBOX NEW MAILS
						
								if($isWebConnection){
									$arrMailDatas = unserialize($userDatas["userMailDatas"]);
									if(SHOW_DATA_BOX_MAILS && ($arrMailDatas["serverInboxName"] != '' && $arrMailDatas["serverInboxLogin"] != '' && $arrMailDatas["serverInboxPassword"] != '')) {
										$arrNewMails = getNewMails();

										if(!empty($arrNewMails)) {
											$playAlarm = true;
											// if($arrNewMails["INFOS"]->Unread > 0) { $playAlarm = true; }
											$arrThisUserMail = unserialize($userDatas["userMailDatas"]);
											echo '<div class="dataBoxItem">';
												echo '<div class="dataBoxItemHeader">';
												if(SHOW_INFO_BOXES_ICONS) {
													echo '<img src="' . PATH_ICONS_MENUE_TITLES . 'mail.png" alt="" width="16" height="16" />';
												}
												echo 'Maileingang:</div>';
												echo '<div class="dataBoxItemContent">';
													echo '<div style="font-style:italic;">' . $arrThisUserMail["mailAddress"] . '</div>';
													echo '<table cellpadding="0" cellspacing="0" width="100%">';
													echo '<tr title="' . $arrNewMails["INFOS"]->Unread . ' ungelesene Mail(s) im Posteingang"><td> &bull; <a href="javascript:void();" class="buttonShowLinkDetails">Ungelesen</a>:</td><td style="text-align:right;">' . $arrNewMails["INFOS"]->Unread . '</td></tr>';
													echo '<tr title="' . $arrNewMails["INFOS"]->Nmsgs . ' Mail(s) im Posteingang"><td> &bull; <a href="javascript:void();" class="buttonShowLinkDetails">Insgesamt</a>:</td><td style="text-align:right;">' . $arrNewMails["INFOS"]->Nmsgs . '</td></tr>';
													// echo '<tr title="' . $arrNewMails["INFOS"]->Deleted . ' gel&ouml;schte Mails"><td> &bull; Gel&ouml;scht:</td><td style="text-align:right;">' . $arrNewMails["INFOS"]->Deleted . '</td></tr>';
													echo '</table>';
												echo '</div>';
											echo '</div>';
											if(!empty($arrNewMails["HEADERS"])) {
											
												$sortArray = array();
												foreach($arrNewMails["HEADERS"] as $key => $array) {
													$sortArray[$key] = strtotime($array["date"]);
													$arrNewMails["HEADERS"][$key]["strtotime"] = strtotime($array["date"]);
												}

												array_multisort($sortArray, SORT_DESC, SORT_NUMERIC, $arrNewMails["HEADERS"]);
												$contentText .= '<table cellpadding="0" cellspacing="0" width="660" class="border">';
												$contentText .= '<tr>';
												$contentText .= '<th style="width:200px;">Von</th>';
												$contentText .= '<th>Betreff</th>';
												$contentText .= '<th>Datum</th>';
												$contentText .= '</tr>';
												for($i = 0 ; $i < MAX_DISPLAY_NEW_MAILS; $i++) {
													if($arrNewMails["HEADERS"][$i]["from"] != ''){
														$contentText .= '<tr>';
														$contentText .= '<td>'. $arrNewMails["HEADERS"][$i]["from"] . '</td>';
														$contentText .= '<td>'. $arrNewMails["HEADERS"][$i]["subject"] . '</td>';
														$contentText .= '<td>'. formatDate(date('Y-m-d', $arrNewMails["HEADERS"][$i]["strtotime"]), 'display') . '</td>';
														$contentText .= '</tr>';
													}
												}
												$contentText .= '</table>';
												$contentFooter = '';

												$content .= '<div class="noticeBoxHeader">Details zu den letzten ' . MAX_DISPLAY_NEW_MAILS . ' Mails</div>';
												$content .= '<div class="noticeBoxContents">' . $contentText . '</div>';
												$content .= '<div class="noticeBoxFooter">' . $contentFooter . '</div>';
												$content .= '<div class="noticeBoxClose">' . '<img src="layout/iconClose.png" class="iconClose" width="14" height="14" alt="" title="Liste schlie&szlig;en">' . '</div>';
												$content = '<div class="loadNoticeContent">' . $content . '</div>';
												$content = '<div id="loadMailDetailsArea">' . $content . '</div>';
												echo $content;
											}

										}
									}
								}

								// EOF INFOBOX NEW MAILS

								// BOF INTERNAL INFOS
								echo '<div class="dataBoxItem">';
								echo '<div class="dataBoxItemHeader">';
								if(SHOW_INFO_BOXES_ICONS) {
									echo '<img src="' . PATH_ICONS_MENUE_TITLES . 'shop.png" alt="" width="16" height="16" />';
								}
								echo 'Interne Benachrichtigungen</div>';
								echo '<div class="dataBoxItemContent">';
								echo '<a href="' . PAGE_DISPLAY_MESSAGES . '">Weitere Infos &amp; Benachrichtigungen</a>';
								echo '</div>';
								echo '</div>';
								// EOF INTERNAL INFOS
							 
								// BOF CUSTOMER CALENDAR DATES
								 
								if(SHOW_DATA_BOX_CUSTOMER_CALENDAR && $arrGetUserRights["displayCustomerCalendarDates"]) {
								 
									$arrCustomerCalendarDates = getCustomerCalendarDates(date('Y-m-d'));
									 
									if(!empty($arrCustomerCalendarDates)) {
										$playAlarm = true;
										echo '<div class="dataBoxItem">';
											echo '<div class="dataBoxItemHeader">';
											if(SHOW_INFO_BOXES_ICONS) {
												echo '<img src="' . PATH_ICONS_MENUE_TITLES . 'calendar.png" alt="" width="16" height="16" />';
											}
											echo 'Kunden-Erinnerungen';
											echo '</div>';
											echo '<div class="dataBoxItemContent">';
											echo '<table cellpadding="0" cellspacing="0" width="100%">';
											foreach($arrCustomerCalendarDates as $thisCalendarDateKey => $thisCalendarDateValue){
												echo '<tr title="Kalenderbenachrichtigung f&uuml;r Kunde &quot;' . $thisCalendarDateValue["customersCalendarCustomersNumber"] . '&quot;: ' . ($thisCalendarDateValue["customersCalendarDescription"]) . '">';
												echo '<td> &bull; ';
												echo '<a href="' . PAGE_EDIT_CUSTOMER. '?searchBoxCustomer=' . $thisCalendarDateValue["customersCalendarCustomersNumber"] . '&amp;tab=tabs-10#tabs-10">';
												echo formatDate($thisCalendarDateValue["customersCalendarDate"], 'display');
												echo ': ';
												echo 'KNR &quot;' . ($thisCalendarDateValue["customersCalendarCustomersNumber"]) . '&quot;';
												echo '</a>';
												echo '</td>';
												echo '</tr>';
											}
											echo '</table>';
											echo '</div>';
										echo '</div>';
									}
								}
								// EOF CUSTOMER CALENDAR DATES
								// BOF INFOBOX ONLINE ORDERS
								if($isWebConnection){
									if(SHOW_DATA_BOX_ONLINE_ORDERS && $arrGetUserRights["displayOnlineOrders"]) {
										$thisOrderDateToday = date("Y-m-d");
										$thisOrderDateYesterday = date("Y-m-d", mktime(0, 0, 0, date('m'), (date('d') - 1), date('Y')));
										
										$arrTodayOnlineOrders = getLatestOnlineOrders($thisOrderDateYesterday, $thisOrderDateToday);
// echo 'xwcx';
										if(!empty($arrTodayOnlineOrders)) {
											$playAlarm = true;
											echo '<div class="dataBoxItem">';
												echo '<div class="dataBoxItemHeader">';
												if(SHOW_INFO_BOXES_ICONS) {
													echo '<img src="' . PATH_ICONS_MENUE_TITLES . 'shop.png" alt="" width="16" height="16" />';
												}
												echo 'Online-Bestellungen</div>';
												echo '<div class="dataBoxItemContent">';
													echo '<table cellpadding="0" cellspacing="0" width="100%">';
													echo '<tr title="' . $arrTodayOnlineOrders[$thisOrderDateToday] . ' Bestellungen am ' . formatDate($thisOrderDateToday, 'display') . '">';
													echo '<td> &bull; <a href="' . PAGE_DISPLAY_ONLINE_ORDERS . '">Heute</a>:</td>';
													echo '<td style="text-align:right;">';
													if($arrGetUserRights["displayOnlineOrders"]) {
														echo '<a href="' . PAGE_DISPLAY_ONLINE_ORDERS . '">';
													}
													echo $arrTodayOnlineOrders[$thisOrderDateToday];
													if($arrGetUserRights["displayOnlineOrders"]) {
														echo '</a>';
													}
													echo '</td>';
													echo '</tr>';

													echo '<tr title="' . $arrTodayOnlineOrders[$thisOrderDateYesterday] . ' Bestellungen am ' . formatDate($thisOrderDateYesterday, 'display') . '">';
													echo '<td> &bull; <a href="' . PAGE_DISPLAY_ONLINE_ORDERS . '">Gestern</a>:</td>';
													echo '<td style="text-align:right;">';
													if($arrGetUserRights["displayOnlineOrders"]) {
														echo '<a href="' . PAGE_DISPLAY_ONLINE_ORDERS . '">';
													}
													echo $arrTodayOnlineOrders[$thisOrderDateYesterday];
													if($arrGetUserRights["displayOnlineOrders"]) {
														echo '</a>';
													}
													echo '</td>';
													echo '</tr>';
													echo '</table>';

												echo '</div>';
											echo '</div>';
										}
									}
								}
								// EOF INFOBOX ONLINE ORDERS
								

								// BOF INFOBOX NEW ONLINE REGISTRATIONS
								if($isWebConnection){
									if(SHOW_DATA_BOX_NEW_ONLINE_REGISTRATIONS && $arrGetUserRights["displayOnlineOrders"]) {
										$arrOnlineShopRegistrations = getOnlineShopRegistrations();
										if(!empty($arrOnlineShopRegistrations)) {
											$playAlarm = true;
											echo '<div class="dataBoxItem">';
												echo '<div class="dataBoxItemHeader">';
												if(SHOW_INFO_BOXES_ICONS) {
													echo '<img src="' . PATH_ICONS_MENUE_TITLES . 'shop.png" alt="" width="16" height="16" />';
												}
												echo 'Neuanmeldungen im Shop:</div>';
												echo '<div class="dataBoxItemContent">';
												echo '<table cellpadding="0" cellspacing="0" width="100%">';
												foreach($arrOnlineShopRegistrations as $thisOnlineShopRegistrationKey => $thisOnlineShopRegistrationValue){
													$thisTitle = 'Zum Online-Shop &quot;' . preg_replace("/http:\/\//", "", constant("PATH_ONLINE_SHOP_" . strtoupper(MANDATOR))) . '&quot; ';
													echo '<tr title="' . $thisTitle . '"><td> &bull; ';
													if(1) {
														echo '<a href="' . constant("PATH_ONLINE_SHOP_" . strtoupper(MANDATOR)) . 'admin/customers.php?status=2" target="_blank" >';
													}
													echo 'Nicht freigeschaltet';
													if(1) {
														echo '</a>';
													}
													echo ':</td>';
													echo '<td style="text-align:right;">' . $arrOnlineShopRegistrations[$thisOnlineShopRegistrationKey] . '</td>';
													echo '</tr>';
												}
												echo '</table>';
												echo '</div>';
											echo '</div>';
										}
									}
								}
								// EOF INFOBOX NEW ONLINE REGISTRATIONS
								// BOF INFOBOX BOOKMAKINGS
								if(SHOW_DATA_BOX_ACCOUNTING && $arrGetUserRights["createDocuments"]) {
									// BOF GET OPEN INVOICES
									$arrOpenInvoiceDatas = getOpenInvoices();
									// EOF GET OPEN INVOICES
									echo '<div class="dataBoxItem">';
										$xxx = 'xxx';
										if($xxx > 0 || $xxx > 0 || $xxx > 0) {$playAlarm = true;}
										echo '<div class="dataBoxItemHeader">';
											if(SHOW_INFO_BOXES_ICONS) {
												echo '<img src="' . PATH_ICONS_MENUE_TITLES . 'accounting.png" alt="" width="16" height="16" />';
											}
											echo 'Buchhaltung:</div>';
										echo '<div class="dataBoxItemContent">';
										echo '<table cellpadding="0" cellspacing="0" width="100%">';

										/*
										echo '<tr title="' . $xxx . ' ungeschriebene Auftragsbest&auml;tigungen"><td> &bull; ';
										if($arrGetUserRights["displayConfirmations"] || $arrGetUserRights["editConfirmations"]) {
											echo '<a href="' . PAGE_DISPLAY_XXX . '">';
										}
										echo 'Ungeschriebene ABs';
										if($arrGetUserRights["displayConfirmations"] || $arrGetUserRights["editConfirmations"]) {
											echo '</a>';
										}
										echo ':</td>';
										echo '<td style="text-align:right;">' . $xxx . '</td>';
										echo '</tr>';
										*/

										/*
										echo '<tr title="' . $xxx . ' ungeschriebene Rechnungen"><td> &bull; ';
										if($arrGetUserRights["displayInvoices"] || $arrGetUserRights["editInvoices"]) {
											echo '<a href="' . PAGE_DISPLAY_XXX . '">';
										}
										echo 'Ungeschriebene REs';
										if($arrGetUserRights["displayInvoices"] || $arrGetUserRights["editInvoices"]) {
											echo '</a>';
										}
										echo ':</td>';
										echo '<td style="text-align:right;">' . $xxx . '</td>';
										echo '</tr>';
										*/

										echo '<tr title="Offene Rechnungen anzeigen"><td> &bull; ';
										if($arrGetUserRights["displayInvoices"] || $arrGetUserRights["editInvoices"]) {
											echo '<a href="' . PAGE_UNPAID_INVOICES . '">';
										}
										echo 'Offene Rechnungen';
										if($arrGetUserRights["displayInvoices"] || $arrGetUserRights["editInvoices"]) {
											echo '</a>';
										}
										echo ':</td>';
										echo '<td style="text-align:right;">' . $arrOpenInvoiceDatas[1]["statusCount"] . '</td>';
										echo '</tr>';

										echo '<tr title="Teilbezahlte Rechnungen anzeigen"><td> &bull; ';
										if($arrGetUserRights["displayInvoices"] || $arrGetUserRights["editInvoices"]) {
											echo '<a href="' . PAGE_PAIDPARTLY_INVOICES . '">';
										}
										echo 'Teilbezahlte Rechnungen';
										if($arrGetUserRights["displayInvoices"] || $arrGetUserRights["editInvoices"]) {
											echo '</a>';
										}
										echo ':</td>';
										echo '<td style="text-align:right;">' . $arrOpenInvoiceDatas[4]["statusCount"] . '</td>';
										echo '</tr>';

										echo '</table>';

										echo '</div>';
									echo '</div>';
								}
								// EOF INFOBOX BOOKMAKINGS
							 
								// BOF INFOBOX PERSONNEL VACATIONS
								if(SHOW_DATA_BOX_VACATIONS && $arrGetUserRights["displayCalendar"]) {
									$arrTodayVacations = getTodayPersonnelVacations(date('Y-m-d'));
									if(!empty($arrTodayVacations)) {
										$playAlarm = true;
										echo '<div class="dataBoxItem">';
											echo '<div class="dataBoxItemHeader">';
											if(SHOW_INFO_BOXES_ICONS) {
												echo '<img src="' . PATH_ICONS_MENUE_TITLES . 'suitcase.png" alt="" width="16" height="16" />';
											}
											echo 'Urlaub heute:</div>';
											echo '<div class="dataBoxItemContent">';
											foreach($arrTodayVacations as $thisTodayVacationKey => $thisTodayVacationValue){
												if($arrGetUserRights["displayCalendar"] || $arrGetUserRights["editCalendar"]) {
													echo '<a href="' . PAGE_DISPLAY_CALENDAR . '">';
												}
												if($thisTodayVacationKey > 0) {
													$thisTitle = 'Urlaub bis zum ' . formatDate($thisTodayVacationValue["personnelCalendarEnd"], 'display') . ' einschlie&szlig;lich';
												}
												else {
													$thisTitle = 'zum Kalender';
												}
												echo '<span title="' . $thisTitle . '"> &bull; ' . $thisTodayVacationValue["personnelFirstName"] . ' ' . $thisTodayVacationValue["personnelLastName"] . '</span><br />';
												if($arrGetUserRights["displayCalendar"] || $arrGetUserRights["editCalendar"]) {
													echo '</a>';
												}
											}
											echo '</div>';
										echo '</div>';
									}
								}
								// EOF INFOBOX PERSONNEL VACATIONS

								// BOF INFOBOX PERSONNEL DISEASES
								if(SHOW_DATA_BOX_DISEASES && $arrGetUserRights["displayCalendar"]) {
									$arrTodayDiseases = getTodayPersonnelDiseases(date('Y-m-d'));
									if(!empty($arrTodayDiseases)) {
										$playAlarm = true;
										echo '<div class="dataBoxItem">';
											echo '<div class="dataBoxItemHeader">';
											if(SHOW_INFO_BOXES_ICONS) {
												echo '<img src="' . PATH_ICONS_MENUE_TITLES . 'firstAid.png" alt="" width="16" height="16" />';
											}
											echo 'Krank heute:</div>';
											echo '<div class="dataBoxItemContent">';
											foreach($arrTodayDiseases as $thisTodayDiseaseKey => $thisTodayDiseaseValue){
												if($arrGetUserRights["displayCalendar"] || $arrGetUserRights["editCalendar"]) {
													echo '<a href="' . PAGE_DISPLAY_CALENDAR . '">';
												}
												if($thisTodayDiseaseKey > 0) {
													$thisTitle = 'Krank bis zum ' . formatDate($thisTodayDiseaseValue["personnelCalendarEnd"], 'display') . ' einschlie&szlig;lich';
												}
												else {
													$thisTitle = 'zum Kalender';
												}
												echo '<span title="' . $thisTitle . '"> &bull; ' . $thisTodayDiseaseValue["personnelFirstName"] . ' ' . $thisTodayDiseaseValue["personnelLastName"] . '</span><br />';
												if($arrGetUserRights["displayCalendar"] || $arrGetUserRights["editCalendar"]) {
													echo '</a>';
												}
											}
											echo '</div>';
										echo '</div>';
									}
								}
								// EOF INFOBOX PERSONNEL DISEASES

								// BOF INFOBOX PERSONNEL BIRTHDAYS
								if(SHOW_DATA_BOX_BIRTHDAYS) {
									$arrTodayBirthdays = getTodayPersonnelBirthdays(date('m-d'));

									if(!empty($arrTodayBirthdays)) {
										$playAlarm = true;
										echo '<div class="dataBoxItem">';
											echo '<div class="dataBoxItemHeader">';
											if(SHOW_INFO_BOXES_ICONS) {
												echo '<img src="' . PATH_ICONS_MENUE_TITLES . 'birthday.png" alt="" width="16" height="16" />';
											}
											echo 'Geburtstag heute:</div>';
											echo '<div class="dataBoxItemContent">';
											foreach($arrTodayBirthdays as $thisTodayBirthdayKey => $thisTodayBirthdayValue){
												if($arrGetUserRights["displayCalendar"] || $arrGetUserRights["editCalendar"]) {
													echo '<a href="' . PAGE_DISPLAY_CALENDAR . '">';
												}
												if($thisTodayBirthdayKey > 0) {
													$thisTitle = '*' . formatDate($thisTodayBirthdayValue["personnelBirthday"], 'display') . '';
												}
												else {
													$thisTitle = 'zum Kalender';
												}
												echo '<span title="' . $thisTitle . '"> &bull; ' . $thisTodayBirthdayValue["personnelFirstName"] . ' ' . $thisTodayBirthdayValue["personnelLastName"] . '</span><br />';
												if($arrGetUserRights["displayCalendar"] || $arrGetUserRights["editCalendar"]) {
													echo '</a>';
												}
											}
											echo '</div>';
										echo '</div>';
									}

									$arrCurrentBirthdays = getCurrentPersonnelBirthdays(date('m-d'));
									#dd('arrCurrentBirthdays');
									if($_COOKIE["isAdmin"] == 1){
								 
										if(!empty($arrCurrentBirthdays)) {
											unset($arrCurrentBirthdays[date('m-d')]);
										}

										if(!empty($arrCurrentBirthdays)) {
											$playAlarm = true;
											echo '<div class="dataBoxItem">';
												echo '<div class="dataBoxItemHeader">';
												if(SHOW_INFO_BOXES_ICONS) {
													echo '<img src="' . PATH_ICONS_MENUE_TITLES . 'birthday.png" alt="" width="16" height="16" />';
												}
												echo 'Geburtstage demn&auml;chst:</div>';
												echo '<div class="dataBoxItemContent">';
												unset($arrCurrentBirthdays[date('m-d')]);

												foreach($arrCurrentBirthdays as $thisCurrentBirthdaysKey => $thisCurrentBirthdaysValue){
													foreach($thisCurrentBirthdaysValue as $thisCurrentBirthdayKey => $thisCurrentBirthdayValue){
														if($arrGetUserRights["displayCalendar"] || $arrGetUserRights["editCalendar"]) {
															echo '<a href="' . PAGE_DISPLAY_CALENDAR . '">';
														}
														$thisTitle = '*' . formatDate($thisCurrentBirthdayValue["personnelBirthday"], 'display') . '';
														echo '<span title="' . $thisTitle . '"> &bull; ' . $thisCurrentBirthdayValue["personnelFirstName"] . ' ' . $thisCurrentBirthdayValue["personnelLastName"] . '</span><br />';
														if($arrGetUserRights["displayCalendar"] || $arrGetUserRights["editCalendar"]) {
															echo '</a>';
														}
													}
												}
												echo '</div>';
											echo '</div>';
										}
									}
								}
								// EOF INFOBOX PERSONNEL BIRTHDAYS
							?>
							<?php
								if($_COOKIE["modeSound"] == 'play' && $playAlarm && $_GET["loginSuccess"]) {
									$codeSound = embedSoundFile(DIRECTORY_SOUND_FILES . 'klingel_laut.mp3', 'notifications');
									echo $codeSound;
								}
							?>

						</div>
					</div>
					<?php } ?>

					<?php
					
						if($arrGetUserRights["editCustomers"] || $arrGetUserRights["displayCustomers"]) {
							$arrQuicklinks[] = array("LINK" => PAGE_DISPLAY_MESSAGES, "TEXT" => "Infos &amp; Benachrichtigungen", "ICON" => "messages.png");
						}

						if($arrGetUserRights["editProducts"] || $arrGetUserRights["displayProducts"]) {
							$arrQuicklinks[] = array("TEXT" => "Artikel", "LINK" => PAGE_DISPLAY_PRODUCTS, "ICON" => "products.png");
						}
						if($arrGetUserRights["editOrders"] || $arrGetUserRights["displayOrders"]) {
							$arrQuicklinks[] = array("TEXT" => "Produktions-&Uuml;bersicht", "LINK" => PAGE_DISPLAY_ORDERS_OVERVIEW, "ICON" => "ordersoverview2.png");
						}
						if($arrGetUserRights["displayTracking"]) {
							$arrQuicklinks[] = array("TEXT" => "Paket-Historie", "LINK" => PAGE_DELIVERY_HISTORY, "ICON" => "tracking.png");
							$arrQuicklinks[] = array("TEXT" => "Container/LKW-Listen", "LINK" => PAGE_CONTAINER_LISTS, "ICON" => "shipping.png");
						}
						if($arrGetUserRights["editOrders"] || $arrGetUserRights["displayOrders"]) {
							$arrQuicklinks[] = array("TEXT" => "Produktion anlegen", "LINK" => PAGE_EDIT_PROCESS . "?editID=NEW", "ICON" => "addProcess.png");
						}
						if($arrGetUserRights["exportOrders"]) {
							$arrQuicklinks[] = array("TEXT" => "Export Vertreter-Bestellungen", "LINK" => PAGE_EXPORT_ORDERS, "ICON" => "export.png");
						}
						if($arrGetUserRights["editCustomers"] || $arrGetUserRights["displayCustomers"]) {
							$arrQuicklinks[] = array("TEXT" => "Kunden", "LINK" => PAGE_EDIT_CUSTOMER . "", "ICON" => "customers.png");
						}
						if($arrGetUserRights["editCustomers"] || $arrGetUserRights["displayCustomers"]) {
							$arrQuicklinks[] = array("TEXT" => "Neukunden anlegen", "LINK" => PAGE_EDIT_CUSTOMER . "?editID=NEW", "ICON" => "addCustomer.png");
						}
						if($arrGetUserRights["editOffers"] || $arrGetUserRights["displayOffers"]) {
							$arrQuicklinks[] = array("TEXT" => "Angebote", "LINK" => PAGE_DISPLAY_OFFER, "ICON" => "create.png");
						}
						if($arrGetUserRights["editConfirmations"] || $arrGetUserRights["displayConfirmations"]) {
							$arrQuicklinks[] = array("TEXT" => "Auftr&auml;ge", "LINK" => PAGE_DISPLAY_CONFIRMATION, "ICON" => "create.png");
						}
						if($arrGetUserRights["editDeliveries"] || $arrGetUserRights["displayDeliveries"]) {
							$arrQuicklinks[] = array("TEXT" => "Lieferscheine", "LINK" => PAGE_DISPLAY_DELIVERY, "ICON" => "create.png");
						}
						if($arrGetUserRights["editInvoices"] || $arrGetUserRights["displayInvoices"]) {
							$arrQuicklinks[] = array("TEXT" => "Rechnungen", "LINK" => PAGE_DISPLAY_INVOICE, "ICON" => "create.png");
						}
						if($arrGetUserRights["editReminders"] || $arrGetUserRights["displayReminders"]) {
							$arrQuicklinks[] = array("TEXT" => "Mahnungen", "LINK" => PAGE_DISPLAY_REMINDER, "ICON" => "create.png");
						}
						if($arrGetUserRights["editLetters"] || $arrGetUserRights["displayLetters"]) {
							$arrQuicklinks[] = array("TEXT" => "Briefe", "LINK" => PAGE_CREATE_LETTER, "ICON" => "create.png");
						}

						if($arrGetUserRights["importOnlineOrders"] || $arrGetUserRights["displayOnlineOrders"]) {
							$arrQuicklinks[] = array("TEXT" => "Online-Bestellungen", "LINK" => PAGE_DISPLAY_ONLINE_ORDERS, "ICON" => "shop.png");
						}

						if($arrGetUserRights["displayCalendar"]) {
							$arrQuicklinks[] = array("TEXT" => "Personal-Kalender", "LINK" => PAGE_DISPLAY_CALENDAR, "ICON" => "calendar.png");
						}

						if($arrGetUserRights["displayOrderReceipts"]) {
							$arrQuicklinks[] = array("TEXT" => "Statistik ansehen", "LINK" => PAGE_DISPLAY_ORDERS_RECEIPTS, "ICON" => "statistics.png");
						}
						if($arrGetUserRights["editSuppliers"] || $arrGetUserRights["displaySuppliers"]) {
							$arrQuicklinks[] = array("TEXT" => "Lieferanten", "LINK" => PAGE_EDIT_DISTRIBUTORS, "ICON" => "suppliers.png");
						}
						if($arrGetUserRights["editSalesmen"] || $arrGetUserRights["displaySalesmen"]) {
							$arrQuicklinks[] = array("TEXT" => "Vertreterkarte", "LINK" => PAGE_EDIT_SALESMEN . '?openMap=true', "ICON" => "googleMaps.png");
						}
						if($arrGetUserRights["createEAN"]) {
							$arrQuicklinks[] = array("TEXT" => "EAN-/QR-Generator", "LINK" => PAGE_CODE_GENERATOR, "ICON" => "barcode.png", "TARGET" => "_blank");
						}
						if($arrGetUserRights["displaySendedMails"]) {
							$arrQuicklinks[] = array("TEXT" => "Versendete Mails", "LINK" => PAGE_DISPLAY_MAILS, "ICON" => "sendedMails.png");
						}
						if($arrGetUserRights["createEAN"]) {
							$arrQuicklinks[] = array("TEXT" => "Amazon-Bestellungen", "LINK" => PAGE_SHOP_AMAZON, "ICON" => "amazon.png", "TARGET" => "_blank");
						}

						if(empty($arrQuicklinks)) {
							$infoMessage .= 'Wenn Ihnen hier kein Menü angezeigt wird, wenden Sie sich bitte an den Administrator der Auftragslisten. <br />Anscheinend sind Ihnen noch keine Zugriffsrechte erteilt worden.' . '<br />';
						}

						displayMessages();

						if(!empty($arrQuicklinks)) {
							foreach($arrQuicklinks as $thisKey => $thisValue) {
								echo '<div class="quickLinks">';
								// echo '<h2>' . $thisGroupValue . '</h2>';
								//if(!empty($arrExternalLinks)) {
									//foreach($arrExternalLinks[$thisGroupKey] as $thisLinkKey => $thisLinkValue) {
										echo '<div class="quickLinkItem">';
										if($thisValue["ICON"] != "") {
											$thisImageSrc = $thisValue["ICON"];
										}
										else {
											$thisImageSrc = "spacer.gif";
										}
										if($thisValue["TARGET"] == '' || !isset($thisValue["TARGET"])) {
											$thisValue["TARGET"] = '_top';
										}
										echo '<p title="' . $thisValue["TITLE"] . '"><img src="' . PATH_ICONS_MENUE_QUICKLINKS . $thisValue["ICON"] . '" alt="" /><a href="' . $thisValue["LINK"] . '" title="' . $thisValue["TITLE"] . '" target="' . $thisValue["TARGET"] . '">' . ($thisValue["TEXT"]) . '</a></p>';
										echo '</div>';
									//}
								//}

								echo '</div>';
							}
							echo '<div class="clear"></div>';
						}
					?>
					</div>
				</div>
			</div>
		</div>
		<div class="clear"></div>
	</div>
</div>
<script language="javascript" type="text/javascript">
	$(document).ready(function() {
		$('.buttonShowLinkDetails').click(function () {
			$('#loadMailDetailsArea').toggle();
		});
		$('.iconClose').click(function(){
			$('#loadMailDetailsArea').fadeOut(animateTime);
		});

		setFormDutyFields(<?php echo $jsonFormDutyFields; ?>);

		$(function() {
			$.datepicker.setDefaults($.datepicker.regional[""]);
			$('#editOrdersBestellDatum').datepicker($.datepicker.regional["de"]);
			$('#editOrdersFreigabeDatum').datepicker($.datepicker.regional["de"]);
			$('#editOrdersBelichtungsDatum').datepicker($.datepicker.regional["de"]);
			$('#editOrdersAuslieferungsDatum').datepicker($.datepicker.regional["de"]);
			$('#editOrdersLieferDatum').datepicker($.datepicker.regional["de"]);
		});

		<?php
			if(EMBED_SOUND_FILES && STOP_SOUND_FILES) {
		?>
		$('.buttonSoundNotifications').click(function(){
			toggleSound($(this), '<?php echo $_COOKIE["modeSound"]; ?>');
		});
		<?php
			}
		?>
	});
</script>

<?php require_once('inc/footerHTML.inc.php'); ?>