<?php
	require_once('inc/requires.inc.php');

	if(!$arrGetUserRights["displayStocks"] && !$arrGetUserRights["editStocks"]) {
		header('location: ' . PAGE_EXIT_LOCATION);
		exit;
	}

	$jswindowMessage = "";
	$warningMessage = '';
	$errorMessage = '';
	$successMessage = '';
	$infoMessage = '';

	$dbConnection = new DB_Connection();
	$db_open = $dbConnection->db_connect();

	// BOF READ VERTRETER
		$arrAgentDatas = getAgents();
		$arrAgentDatas2 = getAgents2();
	// EOF READ VERTRETER

	// BOF READ PAYMENT TYPES
		$arrPaymentTypeDatas = getPaymentTypes();
	// EOF READ PAYMENT TYPES

	// BOF READ CUSTOMER GROUP DATAS
		$arrCustomerGroupDatas = getCustomerGroups();
	// EOF READ CUSTOMER GROUP DATAS

	// BOF READ CUSTOMER TYPES
		$arrCustomerTypeDatas = getCustomerTypes();
	// EOF READ CUSTOMER TYPES

	// BOF STORE PRODUCTS OPTION
		if($_POST["storeDatas"] != ""){
			$thisStoreID = $_POST["editID"];				

			if($thisStoreID == 'NEW'){
				$thisStoreID = '%';
			}

			if($_POST["editProductOptionsParentID"] == ''){
				$_POST["editProductOptionsParentID"] = '0';
			}
			$sql_storeOption = "
					REPLACE INTO `" . TABLE_STOCK_PRODUCT_OPTIONS . "` (
							
							`stockProductOptionsName`,
							`stockProductOptionsParentID`,
							`stockProductOptionsSort`,
							`stockProductOptionsActive`,
							`stockProductOptionsSelectable`,
							`stockProductOptionsFaktor`
					)
					VALUES (						
							'" . $_POST["editProductOptionsName"] . "',
							'" . $_POST["editProductOptionsParentID"] . "',
							'" . $_POST["editProductOptionsSort"] . "',
							'" . $_POST["editProductOptionsStatus"] . "',
							'" . $_POST["editProductOptionsSelectable"] . "',
							'" . $_POST["editProductOptionsFaktor"] . "'
					)
				";
			$rs_storeOption = $dbConnection->db_query($sql_storeOption);

			if($rs_storeOption){
				$successMessage .= 'Die Option wurde gespeichert.' . '<br />';
			}
			else {
				$errorMessage .= 'Die Option konnte nicht gespeichert werden.' . '<br />';
			}
			unset($_REQUEST["editID"]);
		}
	// EOF STORE PRODUCTS OPTION



	// BOF CANCEL PRODUCTS OPTION
		if($_POST["cancelDatas"] != ""){
			unset($_REQUEST["editID"]);
		}
	// EOF CANCEL PRODUCTS OPTION

	// BOF DELETE PRODUCTS OPTION
		if($_POST["deleteDatas"] != ""){
			if($_REQUEST["editProductOptionsType"] == "option"){
				$sql_deleteOption = "
						DELETE
							`productOptions`,
							`productAttributes`
							
						FROM `" . TABLE_STOCK_PRODUCT_OPTIONS . "` AS `productOptions`
						
						LEFT JOIN `" . TABLE_STOCK_PRODUCT_OPTIONS . "` AS `productAttributes`
						ON(`productOptions`.`stockProductOptionsID` = `productAttributes`.`stockProductOptionsParentID`)
						
						WHERE 1
							AND `productOptions`.`stockProductOptionsID` = '" . $_POST["editID"] . "'
					";
					
				$rs_deleteOption = $dbConnection->db_query($sql_deleteOption);
				if($rs_deleteOption){
					$successMessage .= 'Die Option und die zugehörigen Attribute wurden entfernt.' . '<br />';
				}
				else {
					$errorMessage .= 'Die Option und die zugehörigen Attribute konnten nicht entfernt werden.' . '<br />';
				}
			}
			else if($_REQUEST["editProductOptionsType"] == "attribute"){
				$sql_deleteAttribute = "
						DELETE							
							
						FROM `" . TABLE_STOCK_PRODUCT_OPTIONS . "`
						
						WHERE 1
							AND `" . TABLE_STOCK_PRODUCT_OPTIONS . "`.`stockProductOptionsID` = '" . $_POST["editID"] . "'
					";
				$rs_deleteAttribute = $dbConnection->db_query($sql_deleteAttribute);
				if($rs_deleteAttribute){
					$successMessage .= 'Das Attribute wurde entfernt.' . '<br />';
				}
				else {
					$errorMessage .= 'Das Attribute konnte nicht entfernt werden.' . '<br />';
				}
			}		
			
			unset($_REQUEST["editID"]);
		}
	// EOF DELETE PRODUCTS OPTION

	// BOF GET ALL PRODUCTS OPTION
		$sql_getOption = "
				SELECT
						`stockProductOptionsID`,
						`stockProductOptionsName`,
						`stockProductOptionsParentID`,
						`stockProductOptionsSort`,
						`stockProductOptionsActive`,
						`stockProductOptionsSelectable`,
						`stockProductOptionsFaktor`,
						
						IF(`stockProductOptionsParentID` = '0', 'option', 'attribute') AS `stockProductOptionsType`
					FROM `" . TABLE_STOCK_PRODUCT_OPTIONS . "`
					WHERE 1		
						

					ORDER BY
						`stockProductOptionsParentID`,
						`stockProductOptionsSort`,						
						`stockProductOptionsName`
						
			";
		$rs_getOption = $dbConnection->db_query($sql_getOption);

		$arrProductsOptionData = array();
		$arrProductsAttributeData = array();

		while($ds_getOption = mysqli_fetch_assoc($rs_getOption)){
			foreach(array_keys($ds_getOption) as $field){
				if($ds_getOption["stockProductOptionsType"] == 'option'){
					$arrProductsOptionData[$ds_getOption["stockProductOptionsID"]][$field] = $ds_getOption[$field];
				}
				else if($ds_getOption["stockProductOptionsType"] == 'attribute'){
					$arrProductsAttributeData[$ds_getOption["stockProductOptionsParentID"]][$ds_getOption["stockProductOptionsID"]][$field] = $ds_getOption[$field];
				}
			}
		}
		#dd('arrProductsOptionData');
		#dd('arrProductsAttributeData');
	// EOF GET ALL PRODUCTS OPTION
	

	// BOF GET SELECTED PRODUCTS OPTION
		if($_REQUEST["editID"] != '' && $_REQUEST["editID"] != 'NEW'){
			$sql_getCat = "
					SELECT
							`stockProductOptionsID`,
							`stockProductOptionsName`,
							`stockProductOptionsParentID`,
							`stockProductOptionsSort`,
							`stockProductOptionsActive`,
							`stockProductOptionsSelectable`,
							`stockProductOptionsFaktor`,
							IF(`stockProductOptionsParentID` = '0', 'option', 'attribute') AS `stockProductOptionsType`
						
						FROM `" . TABLE_STOCK_PRODUCT_OPTIONS . "`

						WHERE 1
							 AND `stockProductOptionsID` = '" . $_REQUEST["editID"] . "'
						
						LIMIT 1
						
				";
			$rs_getCat = $dbConnection->db_query($sql_getCat);

			$arrSelectedProductsCategory = array();
			while($ds_getCat = mysqli_fetch_assoc($rs_getCat)){
				foreach(array_keys($ds_getCat) as $field){
					$arrSelectedProductsOption[$field] = $ds_getCat[$field];
				}
			}
			
			$optionIsDeletable = false;
			$arrSelectedProductsAttributeData = array();
			if($arrSelectedProductsOption["stockProductOptionsType"] == 'attribute'){
				$optionIsDeletable = true;
			}
			else if($arrSelectedProductsOption["stockProductOptionsType"] == 'option'){
				if(!empty($arrProductsAttributeData[$arrSelectedProductsOption["stockProductOptionsID"]])){
					$arrSelectedProductsAttributeData = $arrProductsAttributeData[$arrSelectedProductsOption["stockProductOptionsID"]];
				}
				else {
					$optionIsDeletable = true;
				}
			}
			$optionIsDeletable = true;
		}
	// EOF GET SELECTED PRODUCTS OPTION
?>
<?php
	require_once('inc/headerHTML.inc.php');
	$thisTitle = "Artikel-Optionen";

	$headerHTML = preg_replace("/{###TITLE###}/", strip_tags($thisTitle), $headerHTML);
	echo $headerHTML;

	require_once(FILE_MENUE_TOP);

?>

<div id="mainArea">
	<div id="mainContent">
		<?php require_once(FILE_MENUE_SIDEBAR); ?>
		<div id="contentArea">
			<a name="top"></a>
			<div id="contentAreaElements">
				<h1><?php if(SHOW_PAGE_TITLE_ICONS) { echo '<img src="' . PATH_ICONS_MENUE_TITLES . 'shipping.png" alt="" />'; } ?> <?php echo $thisTitle; ?></h1>

				<?php displayMessages(); ?>
				<?php if($_REQUEST["editID"] == "" && $arrGetUserRights["editStocks"] == '1') { ?>
					<div class="menueActionsArea">
						<a href="<?php echo PAGE_PRODUCTS_STOCK_OPTIONS; ?>?editID=NEW&amp;optionsType=option" class="linkButton">Neue Option anlegen</a>
						<a href="<?php echo PAGE_PRODUCTS_STOCK_OPTIONS; ?>?editID=NEW&amp;optionsType=attribute" class="linkButton">Neues Attribut anlegen</a>
						<div class="clear"></div>
					</div>
				<?php } ?>

				<div class="adminInfo">Datensatz-ID: <?php echo $_REQUEST["editID"]; ?></div>

				<div class="contentDisplay">
					<div class="adminEditArea">
					<?php
						if($_REQUEST["editID"] == ''){
							$countRowLevel1 = 0;
							$countRowLevel2 = 0;

							if(!empty($arrProductsOptionData)){
								echo '<table cellpadding="0" cellspacing="0" border="0" width="100%" class="displayOrders">
									<tr>
										<th style="width:45px;text-align:right;">#</th>
										<th>Option</th>
										<th>Attribut</th>
										<th>Faktor f. VE</th>
										<th>Sortierung</th>
										<th>Status</th>										
										<th>Info</th>
									</tr>
								';

								foreach($arrProductsOptionData as $thisProductsOptionKey => $thisProductsOptionValue){
									
									if($countRowLevel1%2 == 0){ $rowClass = 'row0'; }
									else { $rowClass = 'row1'; }
									
									if($thisProductsSubCategoryValue["stockProductOptionsActive"] != '1'){
										$rowClass = 'row6';
									}

									// BOF OPTIONS
										echo '<tr style="font-weight:bold;" class="row2">';

										echo '<td style="text-align:right;">';
										echo '<b>' . ($countRowLevel1 + 1). '.</b>';
										echo '</td>';

										echo '<td>';
										echo '&#9632; ' . $thisProductsOptionValue["stockProductOptionsName"];
										echo '</td>';
										
										echo '<td>';
										echo '</td>';
										
										echo '<td>';
										echo '</td>';
										
										echo '<td style="text-align:right;">';
										echo $thisProductsOptionValue["stockProductOptionsSort"];
										echo '</td>';

										echo '<td>';
										// echo $thisProductsOptionValue["stockProductCategoriesActive"];
										if($thisProductsOptionValue["stockProductOptionsActive"] == '1'){
											$thisStatusImage = 'iconOk.png';
											$thisStatusTitle = 'aktiv';
										}
										else {
											$thisStatusImage = 'iconNotOk.png';
											$thisStatusTitle = 'nicht aktiv';
										}

										echo '<img src="layout/icons/' . $thisStatusImage . '" width="16" height="16" alt="' . $thisStatusTitle . '" title="' . $thisStatusTitle . '" />';
										echo '</td>';

										echo '<td>';									
										
										if($thisProductsOptionValue["stockProductOptionsSelectable"] == '1'){
											echo '<span class="toolItem"><img src="layout/icons/selectable.png" width="16" height="16" alt="Option ist in Produkten ausw&auml;hlbar" /></span>';
										}
										else{
											echo '<span class="toolItem"><img src="layout/icons/notSelectable.png" width="16" height="16" alt="Option ist nicht in Produkten ausw&auml;hlbar" /></span>';
										}
										
										if($arrGetUserRights["editStocks"]) {											
											echo '<span class="toolItem"><a href="' . PAGE_PRODUCTS_STOCK_OPTIONS . '?editID=' . $thisProductsOptionValue["stockProductOptionsID"] . '&amp;optionsType=' . $thisProductsOptionValue["stockProductOptionsType"] . '"><img src="layout/icons/iconEdit.gif" width="16" height="16" title="Option bearbeiten" alt="Bearbeiten" /></a></span>';
										}
										else {
											echo '<span class="toolItem"><img src="layout/spacer.gif" width="16" height="16" alt="" /></span>';
										}
										echo '</td>';

										echo '</tr>';
										
									// EOF OPTIONS
									
									// BOF ATTRIBUTES
										if(!empty($arrProductsAttributeData[$thisProductsOptionKey])){
											$countRowLevel2 = 0;
											foreach($arrProductsAttributeData[$thisProductsOptionKey] as $thisAttributeKey => $thisAttributeValue){
												
												if($countRowLevel2%2 == 0){ $rowClass = 'row0'; }
												else { $rowClass = 'row1'; }
												
												if($thisAttributeValue["stockProductOptionsActive"] != '1'){
													$rowClass = 'row6';
												}

												echo '<tr class="'.$rowClass.'">';												
												
												echo '<td style="text-align:right;">';
												echo '<b>' . ($countRowLevel2 + 1). '.</b>';
												echo '</td>';
												
												echo '<td>';
												echo '</td>';

												echo '<td>';
												echo '<img src="layout/icons/subCat.png" width="19" height="11" alt="" /> ' . $thisAttributeValue["stockProductOptionsName"];
												echo '</td>';
												
												echo '<td>';
												if($thisAttributeValue["stockProductOptionsFaktor"] != 0){
													echo '' . number_format($thisAttributeValue["stockProductOptionsFaktor"], 0, ',', '.');
												}
												echo '</td>';
												
												echo '<td>';
												echo $thisAttributeValue["stockProductOptionsSort"];
												echo '</td>';

												echo '<td>';
												// echo $thisAttributeValue["stockProductOptionsActive"];
												if($thisAttributeValue["stockProductOptionsActive"] == '1'){
													$thisStatusImage = 'iconOk.png';
													$thisStatusTitle = 'aktiv';
												}
												else {
													$thisStatusImage = 'iconNotOk.png';
													$thisStatusTitle = 'nicht aktiv';
												}

												echo '<img src="layout/icons/' . $thisStatusImage . '" width="16" height="16" alt="' . $thisStatusTitle . '" title="' . $thisStatusTitle . '" />';
												echo '</td>';

												echo '<td>';
												echo '<span class="toolItem"><img src="layout/spacer.gif" width="16" height="16" alt="" /></span>';
												if($arrGetUserRights["editStocks"]) {
													echo '<span class="toolItem"><a href="' . PAGE_PRODUCTS_STOCK_OPTIONS . '?editID=' . $thisAttributeValue["stockProductOptionsID"] . '&amp;optionsType=' . $thisAttributeValue["stockProductOptionsType"] . '"><img src="layout/icons/iconEdit.gif" width="16" height="16" title="Attribut bearbeiten" alt="Bearbeiten" /></a></span>';
												}
												else {
													echo '<span class="toolItem"><img src="layout/spacer.gif" width="16" height="16" alt="" /></span>';
												}
												echo '</td>';

												echo '</tr>';

												$countRowLevel2++;											
											}
										}
									// EOF ATTRIBUTES
									$countRowLevel1++;
								}

								echo '</table>';
							}
						}
						else {
							$thisOptionType = $_REQUEST["optionsType"];							
							if($arrSelectedProductsOption["stockProductOptionsType"] != ''){
								$thisOptionType = $arrSelectedProductsOption["stockProductOptionsType"];
							}
									
							?>

							<form name="formEditProductsCategory" id="formEditProductsCategory" method="post" action="<?php echo $_SERVER["PHP_SELF"]; ?>#tabs-1" enctype="multipart/form-data" >
								<input type="hidden" name="editID" value="<?php echo $_REQUEST["editID"]; ?>" />								
								<input type="hidden" name="editProductOptionsType" value="<?php echo $thisOptionType; ?>" />								
								
								<fieldset class="colored1">
									<legend>Artikel-<?php echo ucfirst($thisOptionType); ?>-Daten</legend>
									<table border="0" width="100%" cellspacing="0" cellpadding="0">										
										<tr>
											<td style="width:200px;"><b>Attribut-Name:</b></td>
											<td><input type="text" name="editProductOptionsName" class="inputField_510" value="<?php echo $arrSelectedProductsOption["stockProductOptionsName"]; ?>" /></td>
										</tr>
										<tr>
											<td style="width:200px;"><b>VE - Faktor:</b></td>
											<td>
												<input type="text" name="editProductOptionsFaktor" class="inputField_510" value="<?php echo number_format($arrSelectedProductsOption["stockProductOptionsFaktor"], 0, ',', '.'); ?>" />
												<p class="infoArea" style="width:506px">Wichtig bei Verpackungseinheiten: Name: VE 100 Stück &gt;&gt; Faktor 100</p>
											</td>
										</tr>										
										<tr>
											<td style="width:200px;"><b>Sortierung:</b></td>
											<td><input type="text" name="editProductOptionsSort" class="inputField_70" value="<?php echo $arrSelectedProductsOption["stockProductOptionsSort"]; ?>" /></td>
										</tr>										
										<tr>
											<td style="width:200px;"><b>Status:</b></td>
											<td>
												<?php
													$checked = '';
													if($arrSelectedProductsOption["stockProductOptionsActive"] == '1'){
														$checked = ' checked="checked" ';

													}
												?>
												<input type="checkbox" name="editProductOptionsStatus" value="1" <?php echo $checked; ?> /> Aktiv?
											</td>
										</tr>
<?php								
#dd('arrSelectedProductsOption');
#dd('_REQUEST');
?>
										<?php if($arrSelectedProductsOption["stockProductOptionsType"] == 'attribute' || $_REQUEST["optionsType"] == 'attribute'){ ?>
										<tr>	
											<td style="width:200px;"><b>Zugeh&ouml;rige Option:</b></td>
											<td>
												<select name="editProductOptionsParentID" class="inputSelect_510">
												<?php
													if(!empty($arrProductsOptionData)) {
														foreach($arrProductsOptionData as $arrProductsOptionDataKey => $arrProductsOptionDataValue){
															$selected = '';
															if($arrProductsOptionDataKey == $arrSelectedProductsOption["stockProductOptionsParentID"]){
																$selected = ' selected="selected" ';
															}															
															echo '<option value="' . $arrProductsOptionDataValue["stockProductOptionsID"] . '" ' . $selected . ' >' . $arrProductsOptionDataValue["stockProductOptionsName"] . '</option>';															
														}
													}
												?>
												</select>
											</td>
										</tr>
										<?php } ?>
										
										<?php if($arrSelectedProductsOption["stockProductOptionsType"] == 'option' || $_REQUEST["optionsType"] == 'option'){ ?>
										<tr>	
											<td style="width:200px;"><b>Option ausw&auml;hlbar:</b></td>
											<td>
												<?php
													$checked = '';
													if($arrSelectedProductsOption["stockProductOptionsSelectable"] == '1'){
														$checked = ' checked="checked" ';
													}
													echo '<input type="checkbox" name="editProductOptionsSelectable" value="1" ' . $checked . ' />';
												?>
											</td>
										</tr>
										<?php } ?>									
										
									</table>
								</fieldset>


								<div class="actionButtonsArea">
									<?php if($arrGetUserRights["editStocks"] == '1') { ?>
									<input type="submit" class="inputButton1 inputButtonGreen" name="storeDatas" value="Speichern" />
									&nbsp;									
									<?php if($_REQUEST["editID"] != 'NEW'){ ?>
									<?php
										if($arrSelectedProductsOption["stockProductOptionsType"] == 'option'){
											$thisWarningText = "Wollen Sie diese Option und die zugehörigen Attribute wirklich endgültig entfernen???\\nOder soll die Option nur deaktiviert werden?";
										}
										else if($arrSelectedProductsOption["stockProductOptionsType"] == 'attribute'){
											$thisWarningText = "Wollen Sie dieses Attribut wirklich endgültig entfernen???\\nOder soll das Attribut nur deaktiviert werden?";
										}										
									?>
									
									<input type="submit" class="inputButton1 inputButtonRed" name="deleteDatas" value="Endg&uuml;ltig l&ouml;schen" onclick="return showWarning('<?php echo $thisWarningText; ?>');" <?php if($optionIsDeletable == false){ echo ' disabled="disabled" '; } ?> />
									&nbsp;
									<?php } ?>
									<input type="submit" class="inputButton1 inputButtonOrange" name="cancelDatas" value="Abbrechen" onclick="return showWarning('Wollen Sie diesen Vorgang wirklich abbrechen??? ');" />
									<?php } ?>
								</div>
							</form>
							<?php
						}
					?>
				</div>
				</div>
				</div>
			<div class="clear"></div>
		</div>
	</div>
</div>

<script language="javascript" type="text/javascript">

</script>
<?php require_once('inc/footerHTML.inc.php'); ?>