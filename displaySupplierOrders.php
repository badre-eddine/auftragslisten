<?php
	require_once('inc/requires.inc.php');

	if(!$arrGetUserRights["displayTracking"]) {
		#header('location: ' . PAGE_EXIT_LOCATION);
		#exit;
	}

	$jswindowMessage = "";
	$warningMessage = '';
	$errorMessage = '';
	$successMessage = '';
	$infoMessage = '';

	$dbConnection = new DB_Connection();
	$db_open = $dbConnection->db_connect();

	$defaultSortField = "supplierOrdersOrderDate";
	$defaultSortDirection = "ASC";

	if($_REQUEST["sortField"] != "") {
		$sqlSortField = $_REQUEST["sortField"];
	}
	else {
		$sqlSortField = $defaultSortField;
	}

	if($_REQUEST["sortDirection"] != "") {
		$sqlSortDirection = $_REQUEST["sortDirection"];
	}
	else {
		$sqlSortDirection = $defaultSortDirection;
	}
	if($sqlSortDirection == "DESC") { $sqlSortDirection = "ASC"; }
	else { $sqlSortDirection = "DESC"; }

?>
<?php
	require_once('inc/headerHTML.inc.php');
	$thisTitle = "Lieferanten-Bestellungen";

	if($_POST["searchOrderNumber"] != "") {
		$thisTitle .= " - Bestellnummer ".$_POST["searchOrderNumber"];
	}
	if($_POST["searchCustomerName"] != "") {
		$thisTitle .= " - Kunde ".$_POST["searchCustomerName"];
	}
	if($_POST["searchWord"] != "") {
		$thisTitle .= " - Suchbegriff ".$_POST["searchWord"];
	}
	if($_REQUEST["displayOrderStatus"] != "") {
		$thisTitle .= " - Status ".$arrOrderStatusTypeDatas[$_REQUEST["displayOrderStatus"]]["orderStatusTypesName"];
	}
	$headerHTML = preg_replace("/{###TITLE###}/", strip_tags($thisTitle), $headerHTML);
	echo $headerHTML;

	require_once(FILE_MENUE_TOP);

	$sqlWhere = "";

	// BOF CREATE SQL FOR DISPLAY ORDERS
	if($_REQUEST["searchSupplier"] != "") {
		$sqlWhere = " AND `" . TABLE_SUPPLIERS . "`.`suppliersFirmenname` LIKE '%" . $_REQUEST["searchSupplier"] . "%' ";
	}
	else if($_REQUEST["searchCustomerNumber"] != "") {
		$sqlWhere = " AND (`ordersKundennummer` = '" . $_REQUEST["searchCustomerNumber"] . "') ";
		// $sqlWhere = " AND MATCH(`ordersKundenName`) AGAINST ('" . $_REQUEST["searchCustomerName"] . "') ";
	}

	else if($_REQUEST["searchWord"] != "") {
		$sqlWhere = " AND (
						`" . TABLE_SUPPLIERS . "`.`suppliersFirmenname` LIKE '%" . $_REQUEST["searchWord"] . "%'
						OR
						`ordersKundennummer` = '" . $_REQUEST["searchWord"] . "'
						OR
						`suppliersKundennummer` = '" . $_REQUEST["searchWord"] . "'
						OR
						`ordersKundenName` LIKE '%" . $_REQUEST["searchWord"] . "%'
					) ";
					// MATCH(`ordersVertreter`) AGAINST ('" . $_REQUEST["searchWord"] . "')
					// MATCH(`ordersKundenName`) AGAINST ('" . $_REQUEST["searchWord"] . "')
	}
	else {
		$sqlWhere = "";
	}

	$sqlSortField = "ORDER BY `" . $sqlSortField . "` ";

	$sql = "SELECT
				SQL_CALC_FOUND_ROWS

				`" . TABLE_SUPPLIER_ORDERS . "`.`supplierOrdersID`,
				`" . TABLE_SUPPLIER_ORDERS . "`.`supplierOrdersSupplierID`,
				`" . TABLE_SUPPLIER_ORDERS . "`.`supplierOrdersOrderProcessID`,
				`" . TABLE_SUPPLIER_ORDERS . "`.`supplierOrdersAmount`,
				`" . TABLE_SUPPLIER_ORDERS . "`.`supplierOrdersProductNumber`,
				`" . TABLE_SUPPLIER_ORDERS . "`.`supplierOrdersProductName`,
				`" . TABLE_SUPPLIER_ORDERS . "`.`supplierOrdersProductID`,
				`" . TABLE_SUPPLIER_ORDERS . "`.`supplierOrdersProductKategorieID`,
				`" . TABLE_SUPPLIER_ORDERS . "`.`supplierOrdersProductSinglePrice`,
				`" . TABLE_SUPPLIER_ORDERS . "`.`supplierOrdersOrderDate`,
				`" . TABLE_SUPPLIER_ORDERS . "`.`supplierOrdersDeliveryDate`,
				`" . TABLE_SUPPLIER_ORDERS . "`.`supplierOrdersDeliveryDirectToCustomer`,

				`" . TABLE_SUPPLIERS . "`.`suppliersID`,
				`" . TABLE_SUPPLIERS . "`.`suppliersKundennummer`,
				`" . TABLE_SUPPLIERS . "`.`suppliersFirmenname`,
				`" . TABLE_SUPPLIERS . "`.`suppliersFirmennameZusatz`,
				`" . TABLE_SUPPLIERS . "`.`suppliersOurCustomerNumber`,

				`" . TABLE_ORDERS . "`.`ordersAdditionalCostsID`,
				`" . TABLE_ORDERS . "`.`ordersKundennummer`,
				`" . TABLE_ORDERS . "`.`ordersKundenName`,
				`" . TABLE_ORDERS . "`.`ordersCustomerID`,

				`" . TABLE_PRINT_TYPES . "`.`printTypesName`

				FROM `" . TABLE_SUPPLIER_ORDERS . "`

				LEFT JOIN `" . TABLE_SUPPLIERS . "`
				ON(`" . TABLE_SUPPLIER_ORDERS . "`.`supplierOrdersSupplierID` = `" . TABLE_SUPPLIERS . "`.`suppliersID`)

				LEFT JOIN `" . TABLE_ORDERS . "`
				ON(`" . TABLE_SUPPLIER_ORDERS . "`.`supplierOrdersOrderProcessID` = `" . TABLE_ORDERS . "`.`ordersID`)

				LEFT JOIN `" . TABLE_PRINT_TYPES . "`
				ON(`" . TABLE_ORDERS . "`.`ordersAdditionalCostsID` = `" . TABLE_PRINT_TYPES . "`.`printTypesID`)


				WHERE 1
					AND `" . TABLE_SUPPLIER_ORDERS . "`.`supplierOrdersAmount` > 0
					" . $sqlWhere . " 
				
				ORDER BY 
					`" . TABLE_SUPPLIER_ORDERS . "`.`supplierOrdersOrderDate` DESC,
					`" . TABLE_SUPPLIER_ORDERS . "`.`supplierOrdersSupplierID`,
					`" . TABLE_ORDERS . "`.`ordersKundennummer`
			";

	// EOF CREATE SQL FOR DISPLAY ORDERS
?>
<div id="mainArea">
	<div id="mainContent">
		<?php require_once(FILE_MENUE_SIDEBAR); ?>
		<div id="contentArea">
			<a name="top"></a>
			<div id="contentAreaElements">
				<h1><?php if(SHOW_PAGE_TITLE_ICONS) { echo '<img src="' . PATH_ICONS_MENUE_TITLES . 'suppliers.png" alt="" />'; } ?> <?php echo $thisTitle; ?></h1>

				<div id="searchFilterArea">
					<form name="formFilterSearch" method="post" action="<?php echo $_SERVER["PHP_SELF"]; ?>">
					<table border="0" cellpadding="0" cellspacing="0" class="searchFilterContent">
						<tr>
							<td>
								<label for="searchOrderNumber">Lieferant:</label>
								<input type="text" name="searchSupplier" id="searchSupplier" class="inputField_70" value="<?php echo $_REQUEST["searchOrderNumber"]; ?>" />
							</td>
							<td>
								<label for="searchCustomerName">Kundennummer:</label>
								<input type="text" name="searchCustomerNumber" id="searchCustomerNumber" class="inputField_160" value="<?php echo $_REQUEST["searchCustomerName"]; ?>" />
							</td>

							<td>
								<label for="searchWord">Suchbegriff:</label>
								<input type="text" name="searchWord" id="searchWord" class="inputField_70" value="<?php echo $_REQUEST["searchWord"]; ?>" />
							</td>
							<td>
								<input type="hidden" name="editID" id="editID" value="" />
								<input type="submit" name="submitSearch" class="inputButton0" value="Suchen" />
							</td>
						</tr>
					</table>
					<input type="hidden" name="sortDirection" id="inputSortDirection" value="<?php echo $_REQUEST["sortDirection"]; ?>" />
					<input type="hidden" name="sortRow" id="inputSortRow" value="<?php echo $_REQUEST["sortRow"]; ?>" />
					<input type="hidden" name="sortField" id="inputSortField" value="<?php echo $_REQUEST["sortField"]; ?>" />
					<input type="hidden" name="displayType" id="inputDisplayType" value="<?php echo $_REQUEST["displayType"]; ?>" />
					<input type="hidden" name="submitSearchForm" id="inputSubmitSearchForm" value="1" />
					<input type="hidden" name="displayOrderStatus" value="<?php echo $_REQUEST["displayOrderStatus"]; ?>" />
					</form>
				</div>

				<?php displayMessages(); ?>

				<?php
					if($_REQUEST["page"] == "" || $_REQUEST["page"] == 0) {
						$_REQUEST["page"] = 1;
					}

					if(MAX_ORDERS_PER_PAGE > 0) {
						$sql .= " LIMIT " . (($_REQUEST["page"] - 1) * MAX_ORDERS_PER_PAGE) . ", " . MAX_ORDERS_PER_PAGE." ";
					}

					$rs = $dbConnection->db_query($sql);

					$sql_totalRows = "SELECT FOUND_ROWS() AS `countRows`";
					$rs_totalRows = $dbConnection->db_query($sql_totalRows);
					list($countRows) = mysqli_fetch_array($rs_totalRows);

					$pagesCount = ceil($countRows / MAX_ORDERS_PER_PAGE);

					if($countRows > 0) {
				?>
				<?php
					if($pagesCount > 1) {
						include(FILE_MENUE_PAGES);
					}
				?>
				<div class="contentDisplay">
					<table border="0" cellpadding="0" cellspacing="0" width="100%" class="displayOrders">
						<thead>
							<tr>
								<th style="width:45px;text-align:right;">#</th>
								<th>Bestelldatum</th>
								<th class="sortColumn">Lieferant</th>
								<th>Lieferanten-Nr.</th>
								<th>Lieferdatum</th>
								<th>K-Nr. Empf.</th>
								<th>Firma Empf.</th>
								<th>Zustellart</th>
								<th>Artikel-Nr.</th>
								<th class="sortColumn">Artikelname</th>
								<th>Menge</th>
								<th>Einzelpreis</th>
								<th>Gesamtpreis</th>
								<th>Druckart</th>
								<th>Prod.</th>
							</tr>
						</thead>

						<tbody>
					<?php
						$count = 0;
						while($ds = mysqli_fetch_assoc($rs)) {

							if($count%2 == 0){ $rowClass = 'row0'; }
							else { $rowClass = 'row1'; }

							echo '<tr class="'.$rowClass.'">';
								echo '<td style="text-align:right;"><b>' . ($count + 1) .'.</b></td>';
								echo '<td style="white-space:nowrap;">' . substr(formatDate($ds["supplierOrdersOrderDate"], 'display'), 0, 10) .'</td>';
								echo '<td style="white-space:nowrap;">';
								echo ''. $ds["suppliersFirmenname"] . '';
								echo '</td>';
								echo '<td>';
								echo '<a href="' . PAGE_EDIT_DISTRIBUTORS . '?editID=' . $ds["suppliersID"] . '">'. $ds["suppliersKundennummer"] . '</a>';
								echo '</td>';
								echo '<td>';
								echo ''. formatDate($ds["supplierOrdersDeliveryDate"], 'display') . '';
								echo '</td>';
								echo '<td style="white-space:nowrap;">';
								echo '<a href="' . PAGE_EDIT_CUSTOMER . '?searchCustomerNumber=' . $ds["ordersKundennummer"] . '">'. $ds["ordersKundennummer"] . '</a>';
								echo '</td>';
								echo '<td style="white-space:nowrap;">';
								echo ''. $ds["ordersKundenName"] . '';
								echo '</td>';
								echo '<td style="white-space:nowrap;">';
								$thisRecipientType = 'intern';
								$thisRecipientText = 'an Burhan';
								if($ds["supplierOrdersDeliveryDirectToCustomer"] == '1'){
									$thisRecipientType = 'extern';
									$thisRecipientText = 'zum Kunden';
								}
								echo '' . '<img src="layout/icons/'. $thisRecipientType  . '.gif" width="16" height="16" alt="" title="Lieferung an '. strtoupper($thisRecipientType)  . '"/> ' . $thisRecipientText;
								echo '</td>';
								echo '<td style="white-space:nowrap;">' . $ds["supplierOrdersProductNumber"] .'</td>';
								echo '<td style="white-space:nowrap;">' . $ds["supplierOrdersProductName"] .'</td>';
								echo '<td style="text-align:right;">' . number_format($ds["supplierOrdersAmount"], 0, ',', '') .'</td>';
								echo '<td style="text-align:right;">' . number_format($ds["supplierOrdersProductSinglePrice"], 0, ',', '') .'</td>';
								echo '<td style="text-align:right;">' . number_format($ds["supplierOrdersProductTotalPrice"], 0, ',', '') .'</td>';
								echo '<td>';
								echo ''. $ds["printTypesName"] . '';
								echo '</td>';
								echo '<td style="white-space:nowrap;">';
								echo '<a href="' . PAGE_EDIT_PROCESS . '?editID=' . $ds["supplierOrdersOrderProcessID"] . '"><img src="layout/icons/iconEdit.gif" width="16" height="16" alt="Produktion anzeigen" title="zugeh&ouml;rige Produktion anzeigen" /></a>';
								echo '</td>';
							echo '<tr>';
							$count++;
						}
					?>
						</tbody>
					</table>
				</div>
				<?php
					if($pagesCount > 1) {
						include(FILE_MENUE_PAGES);
					}
				?>
				<div style="text-align:right;" class="menueToTop">
					<a href="#top">nach oben</a>
				</div>
				<?php
					}
					else {
						echo '<p class="warningArea">Es wurden keine Daten gefunden!</p>';
					}
				?>
				<div class="clear"></div>
			</div>
			<div class="clear"></div>
		</div>
	</div>
</div>
<script language="javascript" type="text/javascript">
	$(document).ready(function() {
		setFocus('formFilterSearch', 'searchWord');
		colorRowMouseOver('.displayOrders tbody tr');

		$('#searchOrderNumber').keyup(function () {
			// loadSuggestions('searchOrderNumber', [{'triggerElement': '#searchOrderNumber', 'fieldNumber': '#searchOrderNumber', 'fieldName': '#searchCustomerName', 'fieldCity': '', 'fieldZipCode': ''}], 1);
		});
		$('#searchCustomerName').keyup(function () {
			// loadSuggestions('searchCustomerName', [{'triggerElement': '#searchCustomerName', 'fieldNumber': '#searchOrderNumber', 'fieldName': '#searchCustomerName', 'fieldCity': '', 'fieldZipCode': ''}], 1);
		});
		$('#searchSalesmanName').keyup(function () {
			// loadSuggestions('searchSalesmanName', [{'triggerElement': '#searchSalesmanName', 'fieldName': '#searchSalesmanName'}], 1);
		});
		$('#searchPLZ').keyup(function () {
			// loadSuggestions('searchPLZ', [{'triggerElement': '#searchPLZ', 'fieldZipCode': '#searchPLZ'}], 1);
		});

		$('.buttonNotice').click(function () {
			loadNotice($(this));
		});

	});
</script>

<?php require_once('inc/footerHTML.inc.php'); ?>