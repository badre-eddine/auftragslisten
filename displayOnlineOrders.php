<?php
	require_once('inc/requires.inc.php');

	if(!$arrGetUserRights["displayOnlineOrders"] && !$arrGetUserRights["importOnlineOrders"]) {
		header('location: ' . PAGE_EXIT_LOCATION);
		exit;
	}

	/*
	$arrPaymentTypes = array (
		"banktransfer" => ("banktransfer"),
		"cash" => ("cash"),
		"cc" => ("cc"),
		"cod" => ("cod"),
		"invoice" => ("invoice"),
		"moneyorder" => ("moneyorder"),
	);
	*/

	$jswindowMessage = "";
	$warningMessage = '';
	$errorMessage = '';
	$successMessage = '';
	$infoMessage = '';

	$searchDateStart = date("Y-m-d", mktime(0, 0, 0, date("m"), (date("d") - 30), date("Y")));
	if(trim($_REQUEST["searchDateStart"]) != '') {
		$searchDateStart = formatDate($_REQUEST["searchDateStart"], "store");
	}
	$searchDateEnd = date("Y-m-d");
	if(trim($_REQUEST["searchDateEnd"]) != '') {
		$searchDateEnd = formatDate($_REQUEST["searchDateEnd"], "store");
	}

	$dbConnection = new DB_Connection();
	$db_open = $dbConnection->db_connect();

	$dbConnection_ExternShop = new DB_Connection(DB_HOST_EXTERN_SHOP, '', DB_NAME_EXTERN_SHOP, DB_USER_EXTERN_SHOP, DB_PASSWORD_EXTERN_SHOP);
	$dbOpen_ExternShop = $dbConnection_ExternShop->db_connect();

	// BOF READ PAYMENT TYPES
		$arrPaymentTypeDatas = getPaymentTypes();
	// EOF READ PAYMENT TYPES
?>

<?php
	require_once('inc/headerHTML.inc.php');
	$thisTitle = "Online-Bestellungen";
	$headerHTML = preg_replace("/{###TITLE###}/", strip_tags($thisTitle), $headerHTML);
	echo $headerHTML;

	require_once(FILE_MENUE_TOP);
?>
<div id="mainArea">
	<div id="mainContent">
		<?php require_once(FILE_MENUE_SIDEBAR); ?>
		<div id="contentArea">
			<div id="contentAreaElements">
				<h1><?php if(SHOW_PAGE_TITLE_ICONS) { echo '<img src="' . PATH_ICONS_MENUE_TITLES . 'shop.png" alt="" />'; } ?> <?php echo $thisTitle; ?></h1>
				<div class="contentDisplay">
					<p>Anzeige der im Online-Shop &quot;<?php echo COMPANY_INTERNET; ?>&quot; eingegangenen Bestellungen.</p>
					<div id="searchFilterArea">
						<form name="formDisplayOrders" action="<?php echo $_SERVER["PHP_SELF"]; ?>" method="post">
							<table class="noborder" cellspacing="0" cellpadding="0">
								<tr>
									<td>
										<label for="searchWord">Suchbegriff:</label>
										<input type="text" name="searchWord" id="searchWord" class="inputField_70" value="" />
									</td>
									<td>
										<label for="searchDateStart">von:</label>
										<input type="text" name="searchDateStart" id="searchDateStart" maxlength="2" class="inputField_70" readonly="readonly" value="<?php echo formatDate($searchDateStart, "display"); ?>" />
									</td>
									<td>
										<label for="searchDateEnd">bis:</label>
										<input type="text" name="searchDateEnd" id="searchDateEnd" maxlength="2" class="inputField_70" readonly="readonly" value="<?php echo formatDate($searchDateEnd, "display"); ?>" />
									</td>
									<td>
										<label for="searchNoCustomerNumber">ohne KNR:</label>
										<input type="checkbox" name="searchNoCustomerNumber" id="searchNoCustomerNumber" value="1" />
									</td>
									<td>
										<input type="submit" name="submitDisplayOrders" class="inputButton1" value="Daten anfordern" />
									</td>
								</tr>
							</table>
						</form>
					</div>

					<?php displayMessages(); ?>

					<?php
						// SET [GLOBAL | SESSION] group_concat_max_len = val;
						$sql = "SET SESSION `group_concat_max_len` = 8096;";
						$rs = $dbConnection_ExternShop->db_query($sql);

						$where = "";
						if($_REQUEST["searchWord"] != ""){
							$where .= "
									AND (
										`o`.`customers_lastname` = '" . $_REQUEST["searchWord"] . "'
										OR
										`o`.`customers_postcode` = '" . $_REQUEST["searchWord"] . "'
										OR
										`o`.`customers_cid` = '" . $_REQUEST["searchWord"] . "'
										OR
										`o`.`orders_id` = '" . $_REQUEST["searchWord"] . "'
									)
								";
						}
						else if($_REQUEST["searchNoCustomerNumber"] != ""){
							$where .= "
									AND (
										`o`.`customers_cid` IS NULL
										OR
										`o`.`customers_cid` = ''
									)
								";
						}

						$sql = "
							SELECT

									SQL_CALC_FOUND_ROWS

									`orders_id`,

									`totalDatas`,

									`value`,

									`billing_address_format_id`,
									`billing_city`,
									`billing_company`,
									`billing_country_iso_code_2`,
									`billing_country`,
									`billing_firstname`,
									`billing_lastname`,
									`billing_name`,
									`billing_postcode`,
									`billing_state`,
									`billing_street_address`,
									`billing_suburb`,

									`comments`,
									`currency`,

									`customers_address_format_id`,
									`customers_cid`,
									`customers_city`,
									`customers_company`,
									`customers_country`,
									`customers_email_address`,
									`customers_fax`,
									`customers_firstname`,
									`customers_id`,
									`customers_ip`,
									`customers_lastname`,
									`customers_name`,
									`customers_postcode`,
									`customers_state`,
									`customers_status_name`,
									`customers_status`,
									`customers_street_address`,
									`customers_suburb`,
									`customers_telephone`,

									`date_purchased`,

									`delivery_address_format_id`,
									`delivery_city`,
									`delivery_company`,
									`delivery_country_iso_code_2`,
									`delivery_country`,
									`delivery_firstname`,
									`delivery_lastname`,
									`delivery_name`,
									`delivery_postcode`,
									`delivery_state`,
									`delivery_street_address`,
									`delivery_suburb`,

									`orders_status`,
									`payment_class`,
									`payment_method`,
									IF(`payment_method` = 'invoice', '1',
										IF(`payment_method` = 'cod', '3',
											IF(`payment_method` = 'moneyorder', '2',
												IF(`payment_method` = 'paypal_ipn', '7',
													`payment_method`
												)
											)
										)
									) AS `payment_methodConverted`,
									`shipping_class`,
									`shipping_method`,

									GROUP_CONCAT(
											'[F1]Gruppe:[/F1] ', `products_group`,
											':::',
											'[F1]Art.Nr:[/F1] ', `products_model`,
											':::',
											'[F1]Art.Name:[/F1] ', `products_name`,
											':::',
											'[F1]Art.Preis:[/F1] ', CONCAT( CAST(`products_price` AS DECIMAL(14, 2)), ' EURO'),
											':::',
											'[F1]Ges.Preis:[/F1] ', CONCAT( CAST(`final_price` AS DECIMAL(14, 2)), ' EURO'),
											':::',
											'[F1]MwSt.:[/F1] ', CONCAT( CAST(`products_tax` AS DECIMAL(14, 2)), ' PERCENT'),
											':::',
											'[F1]Menge:[/F1] ', `products_quantity`,
											':::',
											'[F1]Art.Opt.:[/F1] :::', `products_options`,
											':::',
											'[F1]Aufschlag:[/F1] ', CONCAT(`price_prefix`, `options_values_price`, ' EURO'),
											':::'
											SEPARATOR '######'
									) AS `product_datas`,

									`customers_files`

								FROM (
									SELECT
										`o`.`orders_id`,
										`op`.`products_model`,

										GROUP_CONCAT(
											DISTINCT
											`ot`.`class`,
											':',
											`ot`.`title`,
											':',
											IF(`ot`.`text` IS NULL, '', `ot`.`text`),
											':',
											IF(`ot`.`value` IS NULL, 0, `ot`.`value`)
											ORDER BY `sort_order` ASC
											SEPARATOR '#'
										) AS `totalDatas`,

										`ot`.`value`,

										`o`.`billing_address_format_id`,
										`o`.`billing_city`,
										`o`.`billing_company`,
										`o`.`billing_country_iso_code_2`,
										`o`.`billing_country`,
										`o`.`billing_firstname`,
										`o`.`billing_lastname`,
										`o`.`billing_name`,
										`o`.`billing_postcode`,
										`o`.`billing_state`,
										`o`.`billing_street_address`,
										`o`.`billing_suburb`,

										`o`.`comments`,
										`o`.`currency`,

										`o`.`customers_address_format_id`,
										`o`.`customers_cid`,
										`o`.`customers_city`,
										`o`.`customers_company`,
										`o`.`customers_country`,
										`o`.`customers_email_address`,
										`o`.`customers_fax`,
										`o`.`customers_firstname`,
										`o`.`customers_id`,
										`o`.`customers_ip`,
										`o`.`customers_lastname`,
										`o`.`customers_name`,
										`o`.`customers_postcode`,
										`o`.`customers_state`,
										`o`.`customers_status_name`,
										`o`.`customers_status`,
										`o`.`customers_street_address`,
										`o`.`customers_suburb`,
										`o`.`customers_telephone`,

										`o`.`date_purchased`,

										`o`.`delivery_address_format_id`,
										`o`.`delivery_city`,
										`o`.`delivery_company`,
										`o`.`delivery_country_iso_code_2`,
										`o`.`delivery_country`,
										`o`.`delivery_firstname`,
										`o`.`delivery_lastname`,
										`o`.`delivery_name`,
										`o`.`delivery_postcode`,
										`o`.`delivery_state`,
										`o`.`delivery_street_address`,
										`o`.`delivery_suburb`,

										`o`.`orders_status`,
										`o`.`payment_class`,
										`o`.`payment_method`,
										`o`.`shipping_class`,
										`o`.`shipping_method`,

										`op`.`products_id`,
										`op`.`products_name`,
										`op`.`products_price`,
										`op`.`products_discount_made`,
										`op`.`products_shipping_time`,
										`op`.`final_price`,
										`op`.`products_tax`,
										`op`.`products_quantity`,

										GROUP_CONCAT(
											DISTINCT
											'[F2]',
											`pa`.`products_options`,
											'[/F2]: ',
											`pa`.`products_options_values`
											SEPARATOR ':::'
										) AS `products_options`,
										CAST(`pa`.`options_values_price` AS DECIMAL(14, 2)) AS `options_values_price`,
										`pa`.`price_prefix`,

										`p`.`products_group`,
										`p`.`products_type`,


										GROUP_CONCAT(
											 DISTINCT `file_name` SEPARATOR ';'
										) AS `customers_files`


									FROM `" . TABLE_SHOP_ORDERS_TOTAL . "` AS `ot`

									JOIN `" . TABLE_SHOP_ORDERS . "` AS `o`
									ON(`o`.`orders_id` = `ot`.`orders_id`)

									LEFT JOIN `" . TABLE_SHOP_ORDERS_PRODUCTS . "` AS `op`
									ON(`op`.`orders_id` = `ot`.`orders_id`)

									LEFT JOIN `" . TABLE_SHOP_PRODUCTS . "` AS `p`
									ON(`op`.`products_id` = `p`.`products_id`)

									LEFT JOIN `" . TABLE_SHOP_ORDERS_PRODUCTS_ATTRIBUTES . "` AS `pa`
									ON(`op`.`orders_id` = `pa`.`orders_id` AND `op`.`orders_products_id` = `pa`.`orders_products_id`)

									LEFT JOIN `" . TABLE_SHOP_ORDERS_FILES . "` AS `of`
									ON(`o`.`orders_id` = `of`.`orders_id`)

									WHERE 1
										/*AND `ot`.`class` = 'ot_subtotal'*/

										AND DATE_FORMAT(`o`.`date_purchased`, '%Y-%m-%d') >= '" . $searchDateStart . "'
										AND DATE_FORMAT(`o`.`date_purchased`, '%Y-%m-%d') <= '" . $searchDateEnd . "'
										" . $where . "
									GROUP BY CONCAT(`o`.`orders_id`, `op`.`orders_products_id`)

									ORDER BY `o`.`date_purchased` DESC, `o`.`orders_id`, `op`.`products_model`, `pa`.`products_options`
								) AS `groupTable`

								GROUP BY `orders_id`

								ORDER BY `date_purchased` DESC
							";

						if($_REQUEST["page"] == "" || $_REQUEST["page"] == 0) {
							$_REQUEST["page"] = 1;
						}

						if(MAX_DELIVERIES_PER_PAGE > 0) {
							$sql .= " LIMIT " . (($_REQUEST["page"] - 1) * MAX_DELIVERIES_PER_PAGE) . ", " . MAX_DELIVERIES_PER_PAGE." ";
						}

						$markerYear = '';
						$markerMonth = '';
						$countRow = 0;

						$rs = $dbConnection_ExternShop->db_query($sql);

						$sql_totalRows = "SELECT FOUND_ROWS() AS `countRows`";
						$rs_totalRows = $dbConnection_ExternShop->db_query($sql_totalRows);
						list($countRows) = mysqli_fetch_array($rs_totalRows);

						$pagesCount = ceil($countRows / MAX_DELIVERIES_PER_PAGE);
					?>

					<?php if($countRows > 0) { ?>
						<?php
							if($pagesCount > 1 && $_GET["loadAll"] != "true") {
								include(FILE_MENUE_PAGES);
							}
						?>
						<table cellspacing="0" cellpadding="0" width="100%" class="displayOrders">
							<thead>
								<tr>
									<th style="width:45px;text-align:right;">#</th>
									<th style="width:200px;">Datum</th>
									<th>Uhrzeit</th>
									<th>KW</th>
									<th>Bestell-Nr.</th>
									<th>Name des Bestellers</th>
									<th>Firma</th>
									<th>Auftragslisten-Kundennummer</th>
									<th colspan="2">Artikel-Betrag</th>
									<th>Zahlart</th>
									<th>Info</th>
								</tr>
							</thead>

							<tbody>

							<?php
								while ($ds = mysqli_fetch_assoc($rs)) {
									#dd('ds');
									$thisYear = substr($ds["date_purchased"], 0, 4);
									// $thisMonth = substr($ds["date_purchased"], 5, 2);
									// $thisMonth = date("M", strtotime($ds["date_purchased"]));
									// $thisMonth = strftime("%m", strtotime($ds["date_purchased"]));
									$thisMonth = getTimeNames(strftime("%m", strtotime($ds["date_purchased"])), 'month', 'long');
									if($markerYear != $thisYear) {
										echo "<tr>";
										echo '<td colspan="12" class="tableRowTitle1">';
										echo $thisYear;
										echo '</td>';
										echo "</tr>";
										$markerYear = $thisYear;
									}

									if($markerMonth != $thisMonth) {
										echo "<tr>";
										echo '<td colspan="12" class="tableRowTitle2">' . htmlentities(utf8_decode($thisMonth)) . ' ' . $thisYear . '</td>';
										echo "</tr>";
										$markerMonth = $thisMonth;
									}
									if($countRow%2 == 0){ $rowClass = 'row1'; }
									else { $rowClass = 'row0'; }

									echo '<tr class="' . $rowClass . '">';

									echo '<td style="text-align:right;">';
									echo '<b>'.($countRow + 1).'.</b> ';
									echo '</td>';

									echo '<td align="left" style="white-space:nowrap;">';
									// $thisDate = strftime("%d. %B %Y (%A)", strtotime($ds["date_purchased"]));
									$thisDate = strftime("%d. ", strtotime($ds["date_purchased"]));

									#$thisDate .=  utf8_decode(getTimeNames(strftime("%m", strtotime($ds["date_purchased"])), 'month', 'long')) . ' ';
									$thisDate .=  utf8_decode(getTimeNames(strftime("%m", strtotime($ds["date_purchased"])), 'month', 'short')) . ' ';
									$thisDate .= strftime("%Y ", strtotime($ds["date_purchased"])) . ' ';
									$thisDate .= '(' . getTimeNames(strftime("%w", strtotime($ds["date_purchased"])), 'day', 'short') . ')';
									$orderDate = date("Y-m-d", strtotime($ds["date_purchased"]));
									echo htmlentities($thisDate);
									echo '</td>';

									echo '<td align="left">';
									$thisTime = strftime("%H:%M:%S", strtotime($ds["date_purchased"]));
									echo $thisTime;
									echo '</td>';

									echo '<td align="left">';
									$thisWeek = strftime("%W", strtotime($ds["date_purchased"]));
									$thisWeek++;
									if($thisWeek < 10){
										$thisWeek = "0" . $thisWeek;
									}
									echo $thisWeek;
									echo '</td>';

									echo '<td>';
									echo '<b>' . $ds["orders_id"] . '</b>';
									echo '</td>';

									echo '<td style="white-space:nowrap;">';
									echo $ds["customers_name"];
									echo '</td>';

									echo '<td style="white-space:nowrap;">';
									echo $ds["customers_company"];
									echo '</td>';

									$arrTemp = explode('#', $ds["totalDatas"]);
									if(!empty($arrTemp)){
										/*
										ot_subtotal
										ot_shipping
										ot_cod_fee
										ot_sperrgut
										ot_subtotal_no_tax
										ot_tax
										ot_total
										*/
										$strThisTotalDatas = '';

										$thisSubTotalValue = 0;
										$thisTotalValue = 0;

										foreach($arrTemp as $thisTemp){
											$arrThisTotalDatas = explode(':', $thisTemp);

											$strThisTotalDatas .= '<tr><td><b>' . strip_tags($arrThisTotalDatas[1]) . '</b>:</td><td style="text-align:right">' . number_format(strip_tags($arrThisTotalDatas[4]), 2, ",", ".") . ' EUR</td></tr>';
											if($arrThisTotalDatas[0] == 'ot_total'){
												$thisTotalValue = $arrThisTotalDatas[4];
											}
											else if($arrThisTotalDatas[0] == 'ot_subtotal'){
												$thisSubTotalValue = $arrThisTotalDatas[4];
											}
										}
									}

									echo '<td>';
									if($ds["customers_cid"] != "") {
										echo '<b>';
										echo '<a href="' . PAGE_EDIT_CUSTOMER . '?editCustomerNumber=' . $ds["customers_cid"] . '" title="Kunden im Auftragslisten-Kundendbestand ansehen">';
										echo $ds["customers_cid"];
										echo '</a>';
										echo '</b>';
									}
									else {
										echo '<a href="' . PAGE_EDIT_CUSTOMER . '?searchBoxCustomer=' . $ds["customers_lastname"] . '" title="Kundenname im Auftragslisten-Kundendbestand suchen">';
										echo 'Kundennamen suchen';
										echo '</a>';
									}
									echo '</td>';

									echo '<td style="text-align:right;white-space:nowrap;background-color:#FEFFAF;">';
									#echo number_format($ds["value"], 2, ',', '.') . ' &euro;';
									#echo ' ### ';
									echo number_format($thisSubTotalValue, 2, ',', '.') . ' &euro;';
									echo '</td>';

									echo '<td style="text-align:right;white-space:nowrap;background-color:#FEFFAF;">';
									#echo number_format(($ds["value"] * (1 + MWST/100)), 2, ',', '.') . ' &euro;';
									#echo ' ### ';
									echo number_format($thisTotalValue, 2, ',', '.') . ' &euro;';
									echo '</td>';

									echo '<td style="text-align:center;white-space:nowrap;">';
									#echo $ds["payment_methodConverted"];
									echo $arrPaymentTypeDatas[$ds["payment_methodConverted"]]["paymentTypesName"];
									// echo ' (' . $ds["payment_method"].')';
									echo '</td>';

									echo '<td style="text-align:right;white-space:nowrap;">';
									if(trim($ds["customers_files"] != "")){
										echo '<span class="toolItem">';
										echo '<img src="layout/icons/iconImage.png" class="buttonShopOrderFiles" width="16" height="16" alt="Datei" title="Es liegen Bilder zur Bestellung vor" />';
										echo '<div class="orderDetailsContent" style="display:none; visibility:hidden;">';
										echo '<h2 style="border-bottom:1px solid #CCCCCC;">Kundendaten</h2>';

										echo '<table border="0" cellpadding="0" cellspacing="0" class="noborder">';
										echo '<tr><td><b>Bestellnummer:</b></td><td>' . $ds["orders_id"] . '</td></tr>';
										echo '<tr><td><b>Firma:</b></td><td>' . $ds["customers_company"] . '</td></tr>';
										echo '<tr><td><b>Kundennummer:</b></td><td><a href="' . PAGE_EDIT_CUSTOMER . '?searchBoxCustomer=' . $ds["customers_cid"] . '">' . $ds["customers_cid"] . '</a></td></tr>';
										echo '</table>';

										echo '<hr />';

										echo '<h2 style="border-bottom:1px solid #CCCCCC;">Dateien</h2>';
										$arrCustomers_files = explode(';', $ds["customers_files"]);										
										
										if(!empty($arrCustomers_files)){
											echo '<ul>';
											foreach($arrCustomers_files as $thisCustomers_files){
												$thisCustomers_files = ($thisCustomers_files);
												echo '<li>';
												echo '<a href="' . constant("PATH_ONLINE_SHOP_" . strtoupper(MANDATOR)) . "uploads/" . $thisCustomers_files . '" target="_blank">' . $thisCustomers_files . '</a>';
												echo '<br />';
												echo '<img src="' . constant("PATH_ONLINE_SHOP_" . strtoupper(MANDATOR)) . "uploads/" . $thisCustomers_files . '" width="36" height="36" alt="" title="" />';
												echo '</li>';
											}
											echo '</ul>';
										}
										echo '</div>';
										echo '</span>';
									}
									else {
										echo '<img src="layout/spacer.gif" width="16" height="16" alt="" title="Es liegen keine Bilder zur Bestellung vor" />';
									}

									if($arrGetUserRights["adminArea"]){
										echo '<span class="toolItem">';
										if($arrGetUserRights["importOnlineAcquisition"]){
											$arrOnlineCustomerDatas = array (
													"customersID" => $ds["customers_cid"],
													"customersKundennummer" => "",
													"customersFirmenname" => $ds["customers_company"],
													"customersFirmennameZusatz" => "",
													"customersFirmenInhaberVorname" => $ds["customers_firstname"],
													"customersFirmenInhaberNachname" => $ds["customers_lastname"],
													"customersFirmenInhaberAnrede" => 1,
													"customersFirmenInhaber2Vorname" => "",
													"customersFirmenInhaber2Nachname" => "",
													"customersFirmenInhaber2Anrede" => "",
													"customersUstID" => "",
													"customersAnsprechpartner1Vorname" => $ds["customers_firstname"],
													"customersAnsprechpartner1Nachname" => $ds["customers_lastname"],
													"customersAnsprechpartner1Anrede" => 1,
													"customersAnsprechpartner2Vorname" => "",
													"customersAnsprechpartner2Nachname" => "",
													"customersAnsprechpartner2Anrede" => "",
													"customersTelefon1" => $ds["customers_telephone"],
													"customersTelefon2" => "",
													"customersMobil1" => "",
													"customersMobil2" => "",
													"customersFax1" => $ds["customers_fax"],
													"customersFax2" => "",
													"customersMail1" => $ds["customers_email_address"],
													"customersMail2" => "",
													"customersHomepage" => "",
													"customersCompanyStrasse" => $ds["customers_street_address"],
													"customersCompanyHausnummer" => "",
													"customersCompanyCountry" => 81,
													"customersCompanyPLZ" => $ds["customers_postcode"],
													"customersCompanyOrt" => $ds["customers_city"],
													"customersLieferadresseFirmenname" => $ds["delivery_company"],
													"customersLieferadresseFirmennameZusatz" => "",
													"customersLieferadresseStrasse" => $ds["delivery_street_address"],
													"customersLieferadresseHausnummer" => "",
													"customersLieferadressePLZ" => $ds["delivery_postcode"],
													"customersLieferadresseOrt" => $ds["delivery_city"],
													"customersLieferadresseLand" => 81,
													"customersRechnungsadresseFirmenname" => $ds["billing_company"],
													"customersRechnungsadresseFirmennameZusatz" => "",
													"customersRechnungsadresseStrasse" => $ds["billing_street_address"],
													"customersRechnungsadresseHausnummer" => "",
													"customersRechnungsadressePLZ" => $ds["billing_postcode"],
													"customersRechnungsadresseOrt" => $ds["billing_city"],
													"customersRechnungsadresseLand" => 81,
													"customersKontoinhaber" => "",
													"customersBankName" => "",
													"customersBankKontonummer" => "",
													"customersBankLeitzahl" => "",
													"customersBankIBAN" => "",
													"customersBankBIC" => "",
													"customersBezahlart" => 0,
													"customersZahlungskondition" => 0,
													"customersTyp" => 5,
													"customersVertreterID" => 0,
													"customersVertreterName" => "",
													"customersNotiz"  => "",
													"customersUserID" => "",
													"customersTimeCreated" => $ds["date_purchased"],
													"customersEntryDate" => substr($ds["date_purchased"], 0, 10),
													"customersLastVisit" => "0000-00-00",
													"customersNextVisit" => "0000-00-00",
													"customersAcquisitionHistory" => ""
											);
											#echo '<hr />';
											#echo '<h2>Kunden importieren</h2>';

											#$arrAcquisitionUserDatas = getAquisitionUsers('usersID');
											$arrOnlineCustomerDatas["customersVertreterID"] = 0;
											unset($arrOnlineCustomerDatas["customersID"]);
											unset($arrOnlineCustomerDatas["customersUserID"]);

											$importURL = '';
											#$importURL .= PAGE_EDIT_CUSTOMER;
											$importURL .= PAGE_IMPORT_CUSTOMER;
											$importURL .= '?editID=NEW';
											$importURL .= '&amp;source=ONLINE-SHOP';
											$importURL .= '&amp;importCustomerDatas=' . urlencode(serialize($arrOnlineCustomerDatas));
											#echo $importURL . '<br />';
											echo '<a href="' . (($importURL)) . '" ><img src="layout/icons/iconCompareAcquisitionCustomers.png" width="16" height="16" title="Kundendaten importieren (Datensatz-ID '.$ds["customers_id"] .')" alt="Bearbeiten" /></a>';
										}
										echo '</span>';
									}

									echo '<span class="toolItem">';
										echo '<img src="layout/icons/iconInfo.png" class="buttonShopOrderInfo" width="16" height="16" alt="Details" title="Detailinformationen zu dieser Bestellung ansehen" />';
										echo '<div class="orderDetailsContent" style="display:none; visibility:hidden;">';

										echo '<h2 style="border-bottom:1px solid #CCCCCC;">Kundendaten</h2>';

										echo '<table border="0" cellpadding="0" cellspacing="0" class="noborder">';
										echo '<tr><td><b>Bestellnummer:</b></td><td>' . $ds["orders_id"] . '</td></tr>';
										echo '<tr><td><b>Firma:</b></td><td>' . $ds["customers_company"] . '</td></tr>';
										echo '<tr><td><b>Kundennummer:</b></td><td><a href="' . PAGE_EDIT_CUSTOMER . '?searchBoxCustomer=' . $ds["customers_cid"] . '">' . $ds["customers_cid"] . '</a></td></tr>';
										echo '<tr><td><b>Bestelldatum:</b></td><td>' . $thisDate . ', ' . $thisTime . ' Uhr</td></tr>';
										echo '<tr><td><b>Zahlart:</b></td><td>' . $arrPaymentTypeDatas[$ds["payment_methodConverted"]]["paymentTypesName"] . '</td></tr>';
										echo '<tr><td><b>Versand:</b></td><td>' . $ds["shipping_method"] . '</td></tr>';
										#echo '<tr><td><b>Versand2:</b></td><td>' . $ds["shipping_class"] . '</td></tr>';

										echo '</table>';

										echo '<hr />';

										echo '<h2 style="border-bottom:1px solid #CCCCCC;">Kontaktadresse</h2>';
										echo '<table border="0" width="860" cellpadding="0" cellspacing="0" class="border">';
										echo '<tr>';
										// echo '<th>Firma</th>';
										echo '<th>Name</th>';
										echo '<th>Straße</th>';
										echo '<th>PLZ</th>';
										echo '<th>Stadt</th>';
										// echo '<th>Bundesland</th>';
										echo '<th>Land</th>';
										echo '<th>Mail</th>';
										echo '<th>Fax</th>';
										echo '<th>Telefon</th>';
										echo '<th>Status</th>';
										echo '</tr>';

										echo '<tr>';
										// echo '<td>' . $ds["customers_company"] . '</td>';
										echo '<td>';
										if($ds["customers_name"] != "") { echo $ds["customers_name"]; }
										else { echo $ds["customers_firstname"] . ' ' . $ds["customers_lastname"]; }
										echo '</td>';
										echo '<td>' . $ds["customers_street_address"] . '</td>';
										echo '<td>' . $ds["customers_postcode"] . '</td>';
										echo '<td>' . $ds["customers_city"];
										if($ds["customers_suburb"] != '') { echo ' (' .$ds["customers_suburb"]. ')'; }
										echo '</td>';
										// echo '<td>';
										// echo $ds["customers_state"];
										// echo '</td>';
										echo '<td>' . $ds["customers_country"] . '</td>';
										echo '<td>';
										if($ds["customers_email_address"] != '') {
											echo '<a href="mailto:'.$ds["customers_email_address"].'">' . $ds["customers_email_address"] . '</a>';
										}
										echo '</td>';
										echo '<td>' . formatPhoneNumber($ds["customers_fax"]) . '</td>';
										echo '<td>' . formatPhoneNumber($ds["customers_telephone"]) . '</td>';
										echo '<td>' . $ds["customers_status_name"] . '(' . $ds["customers_status"] . ')' . '</td>';
										echo '</tr>';
										echo '</table>';
										echo '<hr />';

										echo '<h2 style="border-bottom:1px solid #CCCCCC;">Artikel</h2>';
										#echo '<table border="0" width="860" cellpadding="0" cellspacing="0" class="border">';

										preg_match_all("/\[F1\](.*)\[\/F1\]/ismU", $ds["product_datas"], $found);
										$arrFieldHeaders = $found[1];
										$arrFieldHeaders = array_unique($arrFieldHeaders);

										$arrTempProductDatas = explode('######', $ds["product_datas"]);

										$arrProductDatasNew = array();
										$arrProductDatasTemp = array();
										if(!empty($arrTempProductDatas)){

											foreach($arrTempProductDatas as $thisTempProductDatasKey => $thisTempProductDatasValue){
												$arrTemp = explode(":::", $thisTempProductDatasValue);
												foreach($arrTemp as $thisKey => $thisValue){
													$arrProductDatasTemp[$thisTempProductDatasKey]["d" . $thisKey] = $thisValue;
												}
												$arrProductDatasTemp[$thisTempProductDatasKey]['pd'] = $thisTempProductDatasValue;
											}

											foreach($arrProductDatasTemp as $thisTempProductDatasKey => $thisTempProductDatasValue){

												foreach($thisTempProductDatasValue as $thisTempProductKey => $thisTempProductDatas){
													$fieldKey = '';
													$fieldValue = '';
													unset($arrFound);

													if($thisTempProductKey != 'pd'){
														$getFieldKey = preg_match("/\[F[0-9]\](.*)\[\/F[0-9]\]/", $thisTempProductDatas, $arrFound);
														$fieldKey = preg_replace("/:/", "", $arrFound[1]);
														$getFieldValue = preg_match("/\[\/F[0-9]\](.*)/", $thisTempProductDatas, $arrFound);
														$fieldValue = preg_replace("/:/", "", $arrFound[1]);
													#$fieldValue = preg_replace("/PERCENT/", " %", $fieldValue);
														$fieldValue = preg_replace("/EURO/", " &euro;", $fieldValue);
													}
													else if($thisTempProductKey == 'pd'){
														$fieldKey = 'Produktdaten';
														$fieldValue = $thisTempProductDatas;

														$fieldValue = preg_replace("/\[F1\].*\[\/F1\].*:::/ismU", "", $fieldValue);

														$fieldValue = preg_replace('/([0-9]{1,}\.[0-9]{1,} PERCENT|EURO)/', '<span class="nowrap">$1</span>', $fieldValue);
														$fieldValue = preg_replace("/PERCENT/", " %", $fieldValue);
														$fieldValue = preg_replace("/EURO/", " &euro;", $fieldValue);

														$fieldValue = utf8_decode($fieldValue);

														$fieldValue = strip_tags($fieldValue);
														$fieldValue = htmlentities($fieldValue);

														$fieldValue = preg_replace("/[\t\n\r]{1,}/", " ", $fieldValue);

														$fieldValue = preg_replace("/\[F2\]/ism", '<b>', $fieldValue);
														$fieldValue = preg_replace("/\[\/F2\]/ism", '</b>', $fieldValue);

														$fieldValue = preg_replace("/:::/", "</li><li>", $fieldValue);
														$fieldValue = preg_replace("/<li>[ ]{1,}<\/li>/ismU", "", $fieldValue);

														$fieldValue = '<ul style="padding-left:12px;margin-bottom:0;"><li>' . $fieldValue . '</li></ul>';
														$fieldValue = preg_replace("/<li><\/li>/ismU", "", $fieldValue);
														
														$fieldValue = preg_replace('/<ul style="padding-left:12px;margin-bottom:0;"></ul>/ismU', '', $fieldValue);
														
													}
													if($fieldKey != ''){
														$arrProductDatasNew[$thisTempProductDatasKey . "_ARTIKEL"][trim($fieldKey)] = trim($fieldValue);
													}
														#dd('arrProductDatasNew');
												}

												if($arrProductDatasNew[$thisTempProductDatasKey . "_ARTIKEL"]["Farb-Anzahl"] != "") {
													$thisCountColors = preg_replace("/\-[a-zA-Z]{1,}/", "", strip_tags($arrProductDatasNew[$thisTempProductDatasKey . "_ARTIKEL"]["Farb-Anzahl"]));
													$arrProductDatasNew[$thisTempProductDatasKey . "_ARTIKEL"]["Farb-Anzahl_2"] = $thisCountColors;
												}
												else if($arrProductDatasNew[$thisTempProductDatasKey . "_ARTIKEL"]["Druckart"] != ""){
													$thisCountColors = 1;
													$arrProductDatasNew[$thisTempProductDatasKey . "_ARTIKEL"]["Farb-Anzahl"] = $thisCountColors;
													$arrProductDatasNew[$thisTempProductDatasKey . "_ARTIKEL"]["Farb-Anzahl_2"] = $thisCountColors;
												}
												if($arrProductDatasNew[$thisTempProductDatasKey . "_ARTIKEL"]["Druckart"] != "") {
													$thisPrintType = preg_replace("/zzgl\..*/ism", "", strip_tags($arrProductDatasNew[$thisTempProductDatasKey . "_ARTIKEL"]["Druckart"]));
													$matchResult = preg_match_all("/([0-9]{1,}[\.,][0-9]{1,}) /ismU", $arrProductDatasNew[$thisTempProductDatasKey . "_ARTIKEL"]["Druckart"], $arrFound);
													if($matchResult){
														$thisPrintCosts = trim(preg_replace("/,/", ".", $arrFound[1][0]));
													}
													$arrProductDatasNew[$thisTempProductDatasKey . "_ARTIKEL"]["Druckkosten"] = $thisPrintCosts;
												}

												if(preg_match("/Erstbestellung/ismU", $arrProductDatasNew[$thisTempProductDatasKey . "_ARTIKEL"]["Bestellart"])){
													if($arrProductDatasNew[$thisTempProductDatasKey . "_ARTIKEL"]["Druckart"] != "") {
														$arrProductDatasNew[$thisTempProductDatasKey . "_SATZKOSTEN"] = array();
														$arrProductDatasNew[$thisTempProductDatasKey . "_SATZKOSTEN"]["Gruppe"] = $arrProductDatasNew[$thisTempProductDatasKey . "_ARTIKEL"]["Gruppe"];
														$arrProductDatasNew[$thisTempProductDatasKey . "_SATZKOSTEN"]["Art.Nr"] = "SATZK";

														$arrProductDatasNew[$thisTempProductDatasKey . "_SATZKOSTEN"]["Art.Name"] = "Satzkosten";
														$arrProductDatasNew[$thisTempProductDatasKey . "_SATZKOSTEN"]["Art.Preis"] = $thisPrintCosts;

														// BOF KZH-PRINTCOSTS NOT DEPENDING ON COLORS
															if($arrProductDatasNew[$thisTempProductDatasKey . "_ARTIKEL"]["Gruppe"] == '001'){
																#$arrProductDatasNew[$thisTempProductDatasKey . "_SATZKOSTEN"]["Ges.Preis"] = 1 * $thisPrintCosts;
																$arrProductDatasNew[$thisTempProductDatasKey . "_SATZKOSTEN"]["Ges.Preis"] = $thisCountColors * $thisPrintCosts;

																if($orderDate >= '2017-03-20'){
																	if($arrProductDatasNew[$thisTempProductDatasKey . "_ARTIKEL"]["Menge"] >= 200){
																		$arrProductDatasNew[$thisTempProductDatasKey . "_SATZKOSTEN"]["Ges.Preis"] = 0;
																		$arrProductDatasNew[$thisTempProductDatasKey . "_SATZKOSTEN"]["Art.Preis"] = 0;
																	}
																}
															}
															else {
																$arrProductDatasNew[$thisTempProductDatasKey . "_SATZKOSTEN"]["Ges.Preis"] = $thisCountColors * $thisPrintCosts;
															}
														// EOF KZH-PRINTCOSTS NOT DEPENDING ON COLORS

														$arrProductDatasNew[$thisTempProductDatasKey . "_SATZKOSTEN"]["MwSt."] = MWST;
														$arrProductDatasNew[$thisTempProductDatasKey . "_SATZKOSTEN"]["Menge"] = $thisCountColors;
														$arrProductDatasNew[$thisTempProductDatasKey . "_SATZKOSTEN"]["Art.Opt."] = $arrProductDatasNew[$thisTempProductDatasKey . "_ARTIKEL"]["Druckart"];
														$arrProductDatasNew[$thisTempProductDatasKey . "_SATZKOSTEN"]["Bestellart"] = "";
														$arrProductDatasNew[$thisTempProductDatasKey . "_SATZKOSTEN"]["Artikel-Farbe"] = "";
														$arrProductDatasNew[$thisTempProductDatasKey . "_SATZKOSTEN"]["Druckart"] = $arrProductDatasNew[$thisTempProductDatasKey . "_ARTIKEL"]["Druckart"];
														$arrProductDatasNew[$thisTempProductDatasKey . "_SATZKOSTEN"]["Farb-Anzahl"] = $arrProductDatasNew[$thisTempProductDatasKey . "_ARTIKEL"]["Farb-Anzahl"];
														$arrProductDatasNew[$thisTempProductDatasKey . "_SATZKOSTEN"]["1. Druckfarbe"] = "";
														$arrProductDatasNew[$thisTempProductDatasKey . "_SATZKOSTEN"]["Aufschlag"] = "";
														$arrProductDatasNew[$thisTempProductDatasKey . "_SATZKOSTEN"]["Farb-Anzahl_2"] = $thisCountColors;
														$arrProductDatasNew[$thisTempProductDatasKey . "_SATZKOSTEN"]["Druckkosten"] = $thisPrintCosts;
														$arrProductDatasNew[$thisTempProductDatasKey . "_SATZKOSTEN"]["Produktdaten"] = $arrProductDatasNew[$thisTempProductDatasKey . "_ARTIKEL"]["Druckart"];
													}
												}
											}
										}

										if(!empty($arrProductDatasNew)){

											echo '<table border="0" width="860" cellpadding="0" cellspacing="0" class="border">';
											echo '<tr>';

											echo '<th style="width:15px;">#</th>';

											echo '<th>';
											echo implode('</th><th>', $arrFieldHeaders);
											echo '</th>';
											echo '</tr>';
											$count = 0;

											foreach($arrProductDatasNew as $thisProductDatasNewKey => $thisProductDatasNewValue){
												if($count%2 == 0){ $rowClass = 'row0'; }
												else { $rowClass = 'row1'; }
												echo '<tr class="'.$rowClass.'">';

												echo '<td style="text-align:right;">';
												echo '<b>' . ($count + 1). '.</b>';
												echo '</td>';

												echo '<td style="white-space:nowrap;">';
												echo $thisProductDatasNewValue["Gruppe"];
												echo '</td>';

												echo '<td style="font-weight:bold;white-space:nowrap;">';
												echo $thisProductDatasNewValue["Art.Nr"];
												echo '</td>';

												echo '<td style="font-weight:bold;">';
												echo $thisProductDatasNewValue["Art.Name"];
												echo '</td>';

												echo '<td style="text-align:right;white-space:nowrap;font-weight:bold;">';
												echo number_format($thisProductDatasNewValue["Art.Preis"], 2, ",", ".") . " &euro;";
												echo '</td>';

												echo '<td style="text-align:right;white-space:nowrap;font-weight:bold;background-color:#FEFFAF">';
												echo number_format($thisProductDatasNewValue["Ges.Preis"], 2, ",", ".") . " &euro;";
												echo '</td>';

												echo '<td style="white-space:nowrap;">';
												echo number_format($thisProductDatasNewValue["MwSt."], 2, ",", ".") . " %";
												echo '</td>';

												echo '<td style="text-align:right;white-space:nowrap;font-weight:bold;background-color:#FEFFAF">';
												echo $thisProductDatasNewValue["Menge"];
												echo '</td>';

												echo '<td>';
												echo $thisProductDatasNewValue["Produktdaten"];
												echo '</td>';

												echo '<td style="text-align:right;white-space:nowrap;font-weight:bold;background-color:#FEFFAF">';
												echo $thisProductDatasNewValue["Aufschlag"];
												echo '</td>';

												echo '</tr>';

												if(preg_match("/^[0-9]{1,}_SATZKOSTEN$/", $thisProductDatasNewKey)){
												echo '<tr>';
												echo '<td colspan="10">';
												echo '<hr />';
												echo '</td>';
												echo '</tr>';
												}

												$count++;
											}
											echo '</table>';
										}

										echo '<hr />';

										echo '<table border="0" cellpadding="0" cellspacing="0" class="noborder">';
										echo $strThisTotalDatas;
										echo '</table>';

										echo '<hr />';

										echo '<h2 style="border-bottom:1px solid #CCCCCC;">Hochgeladene Bilder zur Bestellung</h2>';
										if($ds["customers_files"] != ""){											
											$arrCustomers_files = explode(';', $ds["customers_files"]);
											if(!empty($arrCustomers_files)){
												echo '<ul>';
												foreach($arrCustomers_files as $thisCustomers_files){
													$thisCustomers_files = ($thisCustomers_files);
													echo '<li><a href="' . constant("PATH_ONLINE_SHOP_" . strtoupper(MANDATOR)) . "uploads/" . $thisCustomers_files . '" target="_blank">' . $thisCustomers_files . '</a></li>';
												}
												echo '</ul>';
											}
										}
										else {
											echo '<p>Es wurden keine Bilder zur Bestellung hochgeladen</p>';
										}

										echo '<hr />';

										echo '<h2 style="border-bottom:1px solid #CCCCCC;">Rechnungsadresse</h2>';
										echo '<table border="0" width="860" cellpadding="0" cellspacing="0" class="border">';
										echo '<tr>';
										echo '<th>Firma</th>';
										echo '<th>Name</th>';
										echo '<th>Straße</th>';
										echo '<th>PLZ</th>';
										echo '<th>Stadt</th>';
										// echo '<th>Bundesland</th>';
										echo '<th style="width:100px;">Land</th>';
										echo '</tr>';

										echo '<tr>';
										echo '<td>' . $ds["billing_company"] . '</td>';
										echo '<td>';
										if($ds["billing_name"] != "") { echo $ds["billing_name"]; }
										else { echo $ds["billing_firstname"] . ' ' . $ds["billing_lastname"]; }
										echo '</td>';
										echo '<td>' . $ds["billing_street_address"] . '</td>';
										echo '<td>' . $ds["billing_postcode"] . '</td>';
										echo '<td>' . $ds["billing_city"];
										if($ds["billing_suburb"] != '') { echo ' (' .$ds["billing_suburb"]. ')'; }
										echo '</td>';
										// echo '<td>';
										// echo $ds["billing_state"];
										// echo '</td>';
										echo '<td>' . $ds["billing_country"] . '</td>';
										echo '</tr>';
										echo '</table>';
										echo '<hr />';

										echo '<h2 style="border-bottom:1px solid #CCCCCC;">Lieferadresse</h2>';
										echo '<table border="0" width="860" cellpadding="0" cellspacing="0" class="border">';
										echo '<tr>';
										echo '<th>Firma</th>';
										echo '<th>Name</th>';
										echo '<th>Straße</th>';
										echo '<th>PLZ</th>';
										echo '<th>Stadt</th>';
										// echo '<th>Bundesland</th>';
										echo '<th style="width:100px;">Land</th>';
										echo '</tr>';
										echo '<tr>';
										echo '<td>' . $ds["delivery_company"] . '</td>';
										echo '<td>';
										if($ds["delivery_name"] != "") { echo $ds["delivery_name"]; }
										else { echo $ds["delivery_firstname"] . ' ' . $ds["delivery_lastname"]; }
										echo '</td>';
										echo '<td>' . $ds["delivery_street_address"] . '</td>';
										echo '<td>' . $ds["delivery_postcode"] . '</td>';
										echo '<td>' . $ds["delivery_city"];
										if($ds["delivery_suburb"] != '') { echo ' (' .$ds["delivery_suburb"]. ')'; }
										echo '</td>';
										// echo '<td>';
										// echo $ds["delivery_state"];
										// echo '</td>';
										echo '<td>' . $ds["delivery_country"] . '</td>';
										echo '</tr>';
										echo '</table>';
										echo '<hr />';


										echo '<h2 style="border-bottom:1px solid #CCCCCC;">Weitere Daten</h2>';
										echo '<table border="0" width="860" cellpadding="0" cellspacing="0" class="border">';
										echo '<tr>';

										echo '<th>Bemerkung</th>';
										echo '<th>W&auml;hrung</th>';
										// echo '<th>'.('CustomersCid').'</th>';
										echo '<th>Shop-KundenID</th>';
										echo '<th style="width:100px;">IP-Adresse</th>';
										echo '</tr>';

										echo '<tr>';
										echo '<td>' . nl2br($ds["comments"]) . '</td>';
										echo '<td style="width:76px;">' . $ds["currency"] . '</td>';
										//echo '<td>' . $ds["customers_cid"] . '</td>';
										echo '<td style="width:76px;"><a href="' . preg_replace('/{###SHOP_CUSTOMER_ID###}/', $ds["customers_id"], PAGE_DISPLAY_SHOP_CUSTUMER) . '" target="_blank" title="Kundendaten im Online-Shop anzeigen">' . $ds["customers_id"] . '</a></td>';
										echo '<td style="width:76px;"><a href="'. PAGE_LOCALIZE_IP . $ds["customers_ip"] . '" target="_blank" title="IP-Details anzeigen anzeigen">' . $ds["customers_ip"] . '</a>' . '</td>';
										echo '</tr>';

										echo '</table>';
										echo '<hr />';
										echo '</div>';
									echo '</span>';
									echo '</td>';

									echo '</tr>';

									$countRow++;
								}
						?>
						</tbody>
					</table>
					<?php
						if($pagesCount > 1 && $_GET["loadAll"] != "true") {
							include(FILE_MENUE_PAGES);
						}
					?>
					<?php } else { ?>
					<p class="infoArea">Es wurden keine Daten gefunden.</p>
					<?php } ?>
				</div>
			</div>
		</div>
		<div class="clear"></div>
	</div>
</div>

<script language="javascript" type="text/javascript">
	$(document).ready(function() {
		$('.buttonShopOrderInfo').css('cursor', 'pointer');
		$('.buttonShopOrderInfo').click(function () {
			loadOrderDetails($(this), '<?php echo BASEPATH; ?>', 'Details der Bestellung');
		});

		$('.buttonShopOrderFiles').css('cursor', 'pointer');
		$('.buttonShopOrderFiles').click(function () {
			loadOrderDetails($(this), '<?php echo BASEPATH; ?>', 'Dateien zur Bestellung');
		});

		$(function() {
			$.datepicker.setDefaults($.datepicker.regional[""]);
			$('#searchDateStart').datepicker($.datepicker.regional["de"]);
			$('#searchDateEnd').datepicker($.datepicker.regional["de"]);
		});

		$('#searchNoCustomerNumber').live('click', function (){
			var checkedNoCustomerNumber = $('#searchNoCustomerNumber').attr('checked');
			if(checkedNoCustomerNumber == 'checked'){
				$('#searchDateStart').val('');
				$('#searchDateEnd').val('');
			}
			else {
				$('#searchDateStart').val('<?php echo formatDate($searchDateStart, "display"); ?>');
				$('#searchDateEnd').val('<?php echo formatDate($searchDateEnd, "display"); ?>');
			}

		});

		$('#searchWord').live('click', function (){
			$('#searchDateStart').val('');
			$('#searchDateEnd').val('');
		});

		colorRowMouseOver('.displayOrders tbody tr');
		// toggleAreas();
	});
</script>

<?php
	$dbConnection_ExternShop->db_close();
?>

<?php require_once('inc/footerHTML.inc.php'); ?>