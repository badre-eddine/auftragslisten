<?php
	require_once('inc/requires.inc.php');

	if(!$arrGetUserRights["adminArea"]) {
		header('location: ' . PAGE_EXIT_LOCATION);
		exit;
	}

	require_once(DIRECTORY_PDF_TEMPLATES . '/template_TEXT_DATAS.inc.php');
	require_once(DIRECTORY_MAIL_TEMPLATES . 'template_TEXT_DATAS.inc.php');

	$_REQUEST["documentType"] = 'BR';

	$basicCreatedPdfName = "{###MANDATOR###}_briefvorlage_blanko_{###BANK_ACCOUNT###}_seite_{###PAGE_NUMBER###}.pdf";

	// BOF DOWNLOAD FILE
	if($_REQUEST["downloadFile"] != "") {
		require_once('classes/downloadFile.class.php');
		$thisDownload = new downloadFile(DIRECTORY_DOWNLOAD_FILES, $_REQUEST["downloadFile"]);
		$errorMessage .= $thisDownload->startDownload();
	}
	// EOF DOWNLOAD FILE

	$jswindowMessage = "";
	$warningMessage = '';
	$errorMessage = '';
	$successMessage = '';
	$infoMessage = '';

	$dbConnection = new DB_Connection();
	$db_open = $dbConnection->db_connect();

	if($_REQUEST["cancelDatas"]) {
		$_REQUEST["editID"] = "";
	}

	// BOF READ BANK TYPES
		$arrBankAccountTypeDatas = getBankAccountTypes();
	// EOF READ BANK TYPES

	// BOF READ MANDATORIES
		$arrMandatoryDatas = getMandatories();
	// EOF READ MANDATORIES

	// BOF READ DOCUMENT DATAS
		$loadedAdressDatas = array(
			"ADRESS_COMPANY" => '<textarea name="pdfRecipient" id="pdfRecipient" rows="6" cols="30">Empfänger-Adresse</textarea>',
			"ADRESS_MANAGER" => "",
			"ADRESS_CONTACT" => "",
			"ADRESS_STREET" => "",
			"ADRESS_CITY" => "",
			"ADRESS_COUNTRY" => "",
			"ADRESS_MAIL" => "",
			"ADRESS_FAX" => "",
			"CUSTOMER_NUMBER" => "",
			"CUSTOMER_PHONE" => "",
			"CUSTOMER_MAIL" => "",
			"CUSTOMERS_ORDER_NUMBER" => "",

			"SHIPPING_COSTS" => "",
			"SHIPPING_TYPE" => "",
			"SHIPPING_TYPE_NUMBER" => "",
			"PACKAGING_COSTS" => "",
			"CARD_CASH_ON_DELIVERY_VALUE" => "",
			"SHIPPING_DATE" => "",
			"BINDING_DATE" => "",
			"INVOICE_DATE" => "",

			"COMPANY_HEADER_IMAGE" => DIRECTORY_PDF_IMAGES . "briefkopf.jpg",
			"COMPANY_NAME_LONG" => COMPANY_NAME_LONG,
			"COMPANY_PHONE" => COMPANY_PHONE,
			"COMPANY_FAX" => COMPANY_FAX,
			"COMPANY_INTERNET" => COMPANY_INTERNET,
			"COMPANY_MANAGER" => COMPANY_MANAGER,
			"COMPANY_MAIL" => COMPANY_MAIL,
			"COMPANY_STREET_NAME" => COMPANY_STREET_NAME,
			"COMPANY_STREET_NUMBER" => COMPANY_STREET_NUMBER,
			"COMPANY_ZIPCODE" => COMPANY_ZIPCODE,
			"COMPANY_CITY" => COMPANY_CITY,
			"COMPANY_TAX_NUMBER" => COMPANY_TAX_NUMBER,
			"COMPANY_UID_NUMBER" => COMPANY_UID_NUMBER,
			"COMPANY_VENUE" => COMPANY_VENUE,

			"BANK_ACCOUNT_TYPE" => $arrBankAccountTypeDatas[$_POST["selectBankAccount"]]["bankAccountTypesShortName"],
			"BANK_ACCOUNT_CODE" => $arrBankAccountTypeDatas[$_POST["selectBankAccount"]]["bankAccountTypesBankCode"],
			"BANK_ACCOUNT_NAME" => $arrBankAccountTypeDatas[$_POST["selectBankAccount"]]["bankAccountTypesName"],
			"BANK_ACCOUNT_NUMBER" => $arrBankAccountTypeDatas[$_POST["selectBankAccount"]]["bankAccountTypesAccountNumber"],
			"BANK_ACCOUNT_IBAN" => $arrBankAccountTypeDatas[$_POST["selectBankAccount"]]["bankAccountTypesIBAN"],
			"BANK_ACCOUNT_BIC" => $arrBankAccountTypeDatas[$_POST["selectBankAccount"]]["bankAccountTypesBIC"],
			"INTRO_TEXT" => "",
			"OUTRO_TEXT" => "",
			"EDITOR_TEXT" => "",
			"REGARDS_TEXT" => "",
			"SALUTATION_TEXT" => "",
			"PAYMENT_CONDITIONS" => "",
			"PAYMENT_CONDITION" => "",
			"PAYMENT_TYPE" => "",
		);

		$loadedDocumentDatas = array(
			"PAYMENT_TYPE" => "",
			"SALUTATION_TEXT" => "",
			"REGARDS_TEXT" => "",
			"INVOICE_SKONTO" => "",

			"DOCUMENT_DATE" => '<div style="width:80mm;">&nbsp;</div>',
			"CREATION_DATE" => '<div style="width:80mm;">&nbsp;</div>',

			"DOCUMENT_NUMBER_AN" => "",
			"DOCUMENT_NUMBER_AB" => "",
			"DOCUMENT_NUMBER_LS" => "",
			"DOCUMENT_NUMBER_RE" => "",
			"DOCUMENT_NUMBER_MA" => "",
			"DOCUMENT_NUMBER_M1" => "",
			"DOCUMENT_NUMBER_M2" => "",
			"DOCUMENT_NUMBER_M3" => "",
			"DOCUMENT_NUMBER_IK" => "",
			"DOCUMENT_NUMBER_GU" => "",
			"DOCUMENT_NUMBER_BR" => "",

			"CUSTOMER_VAT_ID" => "",
			"CUSTOMERS_TAX_ACCOUNT" => "",

			"ADDRESS_INVOICE" => "",
			"ADDRESS_DELIVERY" => "",

			"BANK_DATAS" => "",
			"WARNING_INVOICE_IS_PAID" => "",

			"CUSTOMER_NUMBER" => "",
			"DOCUMENT_NUMBER" => "",

			"EDITOR_TEXT" => "",
			"SUBJECT" => "",

			"FOOTER_NOTICE" => "",
		);


	if($_POST["storeDatas"] != "" ) {
		if($_POST["pdfTempName"] != '' && file_exists($_POST["pdfTempName"])) {
			$thisPaymentDeadline = '';

			// BOF createPDF
				require_once(DIRECTORY_HTML2PDF . 'html2pdf.class.php');
				$thisCreatedPdfName = $basicCreatedPdfName;
				$thisCreatedPdfName = preg_replace("/{###MANDATOR###}/", strtolower(MANDATOR), $thisCreatedPdfName);
				$thisCreatedPdfName = preg_replace("/{###BANK_ACCOUNT###}/", strtolower($_POST["selectBankAccount"]), $thisCreatedPdfName);
				$thisCreatedPdfName = preg_replace("/{###PAGE_NUMBER###}/", $_POST["selectBlankoPageNumber"], $thisCreatedPdfName);

				try {
					// $html2pdf = new HTML2PDF('P', 'A4', 'de', true, 'UTF-8', array(0, 0, 0, 0));
					$html2pdf = new HTML2PDF('P', 'A4', 'de', true, 'UTF-8', array(22, 5, 5, 5));
					// $html2pdf = new HTML2PDF('P', 'A4', 'de');
					// $html2pdf = new HTML2PDF('P', 'A4', 'de', true, 'UTF-8');
					// $html2pdf->setModeDebug();
					// $html2pdf->setDefaultFont('Arial');
					// $html2pdf->createIndex('Inhaltsverzeichnis', 12, 8, false, true, 2);

					// BOF READ TEMP HTML FILE
					if(file_exists($_POST["pdfTempName"])) {
						$pdfContentPage = "";
						$fp = fopen($_POST["pdfTempName"], 'r');
						$pdfContentPage = fread($fp, filesize($_POST["pdfTempName"]));
						fclose($fp);
						$pdfContentPage = stripslashes($pdfContentPage);

						#echo $_POST["pdfTempName"] . '<br>';
						#echo htmlentities($pdfContentPage);
						#exit;

						if(!DEBUG) {
							unlink($_POST["pdfTempName"]);
						}
					}
					clearstatcache();
					// EOF READ TEMP HTML FILE

					ob_start();
					echo $pdfContentPage;
					$contentPDF = ob_get_clean();

					$html2pdf->writeHTML($contentPDF, isset($_GET['vuehtml']));
					$html2pdf->Output($thisCreatedPdfName, 'F', DIRECTORY_DOWNLOAD_FILES);

					if(file_exists($thisCreatedPdfName)) {
						unlink($thisCreatedPdfName);
					}
					clearstatcache();
				}
				catch(HTML2PDF_exception $e) {
					echo $e;
					exit;
				}
				$_POST["selectBankAccount"] = "";

				if(file_exists(DIRECTORY_DOWNLOAD_FILES . $thisCreatedPdfName)) {
					$successMessage .= 'Die PDF-Datei &quot;' . $thisCreatedPdfName . '&quot wurde generiert. '.'<br />';
					$showPdfDownloadLink = true;
				}
				else {
					$errorMessage .= 'Die PDF-Datei &quot;' . $thisCreatedPdfName . '&quot konnte nicht generiert werden. '.'<br />';
				}
				clearstatcache();

			// EOF createPDF

		}
		else {
			$errorMessage .= 'Die PDF-Basis-Datei &quot;' . $_POST["pdfTempName"] . '&quot konnte nicht temporär gespeichert werden. '.'<br />';
		}
		clearstatcache();
	}

?>
<?php
	require_once('inc/headerHTML.inc.php');

	$thisTitle .= 'Briefpapier erstellen';
	$headerHTML = preg_replace("/{###TITLE###}/", ($thisTitle), $headerHTML);
	echo $headerHTML;

	require_once(FILE_MENUE_TOP);
?>
<div id="mainArea">
	<div id="mainContent">
		<?php require_once(FILE_MENUE_SIDEBAR); ?>

		<div id="contentArea">

			<div id="contentAreaElements">
				<h1><?php if(SHOW_PAGE_TITLE_ICONS) { echo '<img src="' . PATH_ICONS_MENUE_TITLES . 'stationary.png" alt="" />'; } ?> <?php echo ($thisTitle); ?></h1>

				<form name="formSearchPersonnel" method="post" action="<?php echo $_SERVER["PHP_SELF"]; ?>">
				<div id="searchFilterArea">
					<table border="0" width="" cellspacing="0" cellpadding="0" class="searchFilterContent">
						<tr>
							<td><b>Vorlagen-Seite:</b></td>
							<td>
								<select name="selectBlankoPageNumber" id="selectBlankoPageNumber" class="inputSelect_170">
									<option value="1" <?php if($_POST["selectBlankoPageNumber"] == 1){ echo ' selected="selected" ';} ?> >Seite 1</option>
									<option value="2" <?php if($_POST["selectBlankoPageNumber"] == 2){ echo ' selected="selected" ';} ?> >Seite 2</option>
								</select>
							</td>
							<td><b>Bank:</b></td>
							<td>
								<select name="selectBankAccount" class="inputSelect_170" onchange="submit();">
									<option value=""></option>
							<?php

								if(!empty($arrBankAccountTypeDatas)){
									foreach($arrBankAccountTypeDatas as $thisKey => $thisValue){
										$selected = '';
										if($_POST["selectBankAccount"] == ""){
											if($thisValue["bankAccountTypesDefault"] == '1'){
												$selected = ' selected="selected" ';
											}
										}
										else if($_POST["selectBankAccount"] == $thisKey){
											$selected = ' selected="selected" ';
										}
										echo '<option value="' . $thisKey . '" ' . $selected . ' >' . $thisValue["bankAccountTypesName"] . ' [' . $thisValue["bankAccountTypesShortName"] . ']</option>';
									}
								}
							?>
								</select>
							</td>
							<td>
								<input type="hidden" name="xxxeditID" id="xxxeditID" value="" />
								<input type="submit" name="submitSearch" class="inputButton0" value="Erstellen" />
							</td>
						</tr>
					</table>
				</div>
				</form>

				<?php
					if($_POST["selectBlankoPageNumber"] == '' && $_POST["storeDatas"] == "") {
						if(!empty($arrMandatoryDatas)){
							echo '<ul class="downloadDocuments">';
								foreach($arrMandatoryDatas as $thisMandatory){
									for($i = 1 ; $i < 3 ; $i++){
										$thisFile = $basicCreatedPdfName;
										$thisFile = preg_replace("/{###MANDATOR###}/", $thisMandatory["mandatoriesShortName"], $thisFile);
										$thisFile = preg_replace("/{###PAGE_NUMBER###}/", $i, $thisFile);
										if(file_exists(DIRECTORY_DOWNLOAD_FILES . $thisFile)){
											$thisFileType = getFileType($thisFile);
											echo '<li style="list-style-image: url(layout/icons/icon' . strtoupper($thisFileType) . '.gif);" >';
											echo '<p class="documentLink">';
											echo '<a href="' . $_SERVER["PHP_SELF"]. '?downloadFile=' . basename($thisFile) . '">' . basename($thisFile) . '</a>';
											echo '</p>';
											echo '<div class="documentToolItems">';
											echo '<span class="toolItem">';
											echo '<img src="layout/icons/mailAttachement.png" class="buttonSendDocument" rel="' . urlencode(basename($thisFile)) . '#DF" width="16" height="16" title="Dokument &quot;' . $arrDocumentDatas[$thisKey]["orderDocumentsNumber"] . '&quot; direkt per Mail versenden" alt="Dokument versenden" />';
											echo '</span>';
											echo '</div>';
											echo '<div class="clear"></div>';
											echo '</li>';
										}
									}
								}
							echo '</ul>';
						}
					}
				?>

				<div class="xxcontentDisplay">
					<?php
						if($_POST["storeDatas"] != "") {
							$openPdfFile = true;
							$printPdfFile = true;
							if($printPdfFile || $openPdfFile) {
								echo '<script language="javascript" type="text/javascript">';
								echo 'openPDF("' . DIRECTORY_DOWNLOAD_FILES . $thisCreatedPdfName . '", ' , $printPdfFile . ', "printPDF_' . $_REQUEST["documentType"] . '");';
								echo '</script>';

							}
							if($showPdfDownloadLink) {
								// echo '<p class="dataTableContent"><b>DOWNLOAD-LINK: </b><a href="'.DIRECTORY_DOWNLOAD_FILES . $thisCreatedPdfName.'" target="_blank">'.$thisCreatedPdfName.'</a></p>';
								echo '<p class="dataTableContent"><b>DOWNLOAD-LINK: </b> <img src="layout/icons/icon' . getFileType(basename($thisCreatedPdfName)) . '.gif" height="16" width="16" alt="" /> <a href="' . $_SERVER["REQUEST_URI"] . '?documentType=' . $_REQUEST["documentType"] . '&downloadFile=' . $thisCreatedPdfName.'">'.$thisCreatedPdfName.'</a></p>';
							}
							echo '<hr />';
							$_REQUEST["editID"] = "";
						}
					?>
				</div>




			<?php if($_POST["selectBlankoPageNumber"] > 0 && $_POST["storeDatas"] == "") { ?>
			<div class="xxadminEditArea">
				<?php
					// BOF DOCUMENT PREVIEW
				?>
				<form name="formCreateDocument" method="post" action="<?php echo $_SERVER["REQUEST_URI"]; ?>" enctype="multipart/form-data">
				<h2>Dokument-Vorschau</h2>
				<?php
					// BOF CREATE PDF CONTENT AND PREVIEW DATAS
					$loadPdfContent = "";
					$loadContentTemplate = implode('', file(DIRECTORY_PDF_TEMPLATES . '/template_BASIC.html'));
					$loadCardTemplate = implode('', file(DIRECTORY_PDF_TEMPLATES . '/template_CARD_DATAS.html'));

					#$loadContentTemplate = preg_replace("/{###DOCUMENT_DATAS###}/", "", $loadContentTemplate);
					$loadContentTemplate = preg_replace("/{###DOCUMENT_DATAS###}/", $arrDocumentDatas['BR'], $loadContentTemplate);
					$loadPdfContent = $loadContentTemplate;

					// echo $loadCardDatas;
					if($_POST["selectBlankoPageNumber"] == 1 ){
						$thisRows = 34;
					}
					else if($_POST["selectBlankoPageNumber"] == 2 ){
						$thisRows = 50;
					}
					$loadPdfContent = preg_replace("/{###CARD_DATAS###}/", '<textarea name="pdfText" id="pdfText" rows="' . $thisRows . '" cols="78">Text</textarea>', $loadPdfContent);

					// STYLES
					$loadTemplateStyles = implode('', file(DIRECTORY_PDF_TEMPLATES . 'template_STYLES.html'));

					$loadPdfContent = preg_replace("/{###STYLES###}/", $loadTemplateStyles, $loadPdfContent);

					$arrDocumentDatas['BR'] = '
							<!-- BOF_CREATION_DATE --><tr><td align="left"><b>Datum:&nbsp;</b></td><td align="left">{###CREATION_DATE###}</td></tr><!-- EOF_CREATION_DATE -->
						';

					$loadedDocumentDatas["CUSTOMER_NUMBER"] = "";
					$loadedDocumentDatas["DOCUMENT_NUMBER"] = "";
					$loadedDocumentDatas["CREATION_DATE"] = '<input type="text" name="pdfDate" id="pdfDate" value="' . date("d.m.Y"). '" >';
					$loadedDocumentDatas["EDITOR_TEXT"] = "";
					$loadedDocumentDatas["CUSTOMERS_TAX_ACCOUNT"] = "";
					$loadedDocumentDatas["CUSTOMERS_SALESMAN"] = "";
					$loadedDocumentDatas["SALESMANS_CUSTOMER"] = "";
					$loadedDocumentDatas["BANK_ACCOUNT_WARNING"] = "";
					$loadedDocumentDatas["SUBJECT"] = '<input type="text" name="pdfSubject" id="pdfSubject" value="Betreff" style="width:180mm">';
					#$loadedDocumentDatas["SUBJECT"] = "";
					$companyHeaderImageInfo = getimagesize($loadedAdressDatas["COMPANY_HEADER_IMAGE"]);
					$companyHeaderImageInfoProportion = $companyHeaderImageInfo[1] / $companyHeaderImageInfo[0] * 80;

					if($_POST["selectBlankoPageNumber"] == 2){
						if(MANDATOR == 'bctr'){
							$loadPdfContent = preg_replace('/<p class="pdfHeaderAdressArea" align="right">.*<\/p>/ismU', '', $loadPdfContent);
							$loadPdfContent = preg_replace('/<div id="pdfHeaderArea">/', '<div id="pdfHeaderArea"><p class="pdfHeaderAdressArea" align="right"><img src="{###COMPANY_HEADER_IMAGE###}" style="width:80mm;height:' . $companyHeaderImageInfoProportion . 'mm;" /></p>', $loadPdfContent);
							$loadPdfContent = preg_replace('/<table cellpadding="0" cellspacing="0" align="right" class="pdfHeaderAdressArea">.*<\/table>/ismU', '', $loadPdfContent);
							$loadPdfContent = preg_replace('/<table cellpadding="0" cellspacing="0" id="pdfHeaderDatasArea">.*<div id="pdfContentArea">/ismU', '<div id="pdfContentArea">', $loadPdfContent);
						}
						else if(MANDATOR == 'b3'){
							$loadPdfContent = preg_replace('/<div id="pdfHeaderArea">.*<\/div>.*<br>/ismU', '<div id="pdfHeaderArea"><p class="pdfHeaderAdressArea" align="right"><img src="{###COMPANY_HEADER_IMAGE###}" style="width:65mm;" /></p></div>', $loadPdfContent);
						}
						$loadedDocumentDatas["SUBJECT"] = "";
						$loadPdfContent = preg_replace('/<h1>.*<\/h1>/ismU', '', $loadPdfContent);
					}

					if(!empty($loadedAdressDatas)) {
						foreach($loadedAdressDatas as $thisKey => $thisValue) {
							if($thisValue != "") {
								#$loadPdfContent = preg_replace("/{###".$thisKey."###}/", $thisValue . "<br />", $loadPdfContent);
								$loadPdfContent = preg_replace("/{###".$thisKey."###}/", $thisValue . "", $loadPdfContent);
							}
							else {
								$loadPdfContent = preg_replace("/{###".$thisKey."###}/", "", $loadPdfContent);
							}
						}
					}

					if(!empty($loadedDocumentDatas)) {
						foreach($loadedDocumentDatas as $thisKey => $thisValue) {
							$loadPdfContent = preg_replace("/{###".$thisKey."###}/", $thisValue, $loadPdfContent);
							if(trim($thisValue) == '') {
								$loadPdfContent = preg_replace("/<!-- BOF_".$thisKey." -->(.*)<!-- EOF_".$thisKey." -->/", '', $loadPdfContent);
							}
						}
					}

					$loadPdfContent = removeUnnecessaryChars($loadPdfContent);
					$addCssStyles = "
							input, textarea, select {border: solid 0.1mm white;background: #FFF;}
							#pdfText {font-family:Arial; font-size:10pt;}
							#pdfDate {font-family:Arial; font-size:8pt;text-align:right;}
							#pdfSubject {font-family:Arial; font-weight:bold;font-size:12pt;width:180mm;}
							#pdfRecipient {font-family:Arial; font-size:9pt;}
						";
					$addCssStyles = removeUnnecessaryChars($addCssStyles);
					$loadPdfContent = preg_replace("/(--><\/style>)/", $addCssStyles."$1", $loadPdfContent);

					$pdfDisplayContent = $loadPdfContent;

					if($_POST["editDocType"] != "") {
						$pdfDisplayContent = preg_replace("/{###DOCUMENT_NUMBER###}/", $_POST["originDocumentNumber"], $pdfDisplayContent);
					}
					else {
						$pdfDisplayContent = preg_replace("/{###DOCUMENT_NUMBER###}/", constant("BASIC_NUMBER_" . $_REQUEST["documentType"]), $pdfDisplayContent);
					}
					$pdfDisplayContent = preg_replace("/{###DOCUMENT_NUMBER###}/", constant("BASIC_NUMBER_" . $_REQUEST["documentType"]), $pdfDisplayContent);
					$pdfCreateContent = $loadPdfContent;

					// BOF REMOVE PDF TAGS IN PDF DISPLAY CONTENT
						$pdfDisplayContent = preg_replace('/<page(.*)>/ismU', '', $pdfDisplayContent);
						$pdfDisplayContent = preg_replace('/<\/page(.*)>/ismU', '', $pdfDisplayContent);
					// EOF REMOVE PDF TAGS IN PDF DISPLAY CONTENT

					// BOF REMOVE INPUT FIELDS IN PDF CREATION CONTENT
						$pdfCreateContent = preg_replace('/<input (.*) \/>/ismU', '', $pdfCreateContent);
						// <input type="hidden" name="arrItemDatas[4369][basketItemId]" value="4369" />
					// EOF REMOVE INPUT FIELDS IN PDF CREATION CONTENT

					// EOF CREATE PDF CONTENT AND PREVIEW DATAS

					// BOF DISPLAY PDF PREVIEW CONTENT
					echo '<div id="displayCreateDocument" style="padding:10px; background-color:#FFF;">';
					# echo htmlentities($pdfDisplayContent);
					echo $pdfDisplayContent;
					echo '</div>';

					// EOF DISPLAY PDF PREVIEW CONTENT

					echo '<hr />';

					// BOF DISPLAY PDF CREATION CONTENT
					#echo '<div id="displayCreateDocument" style="padding:10px; background-color:#FFF;">';
					#echo $pdfCreateContent;
					#echo htmlentities($pdfCreateContent);
					#echo '</div>';
					//EOF DISPLAY PDF CREATION CONTENT

					// BOF WRITE PDF CREATION CONTENT TO TMP FILE
						$pdfTempName = tempnam('tmp', 'tempname');
						$pdfTempFile = fopen($pdfTempName, 'w');
						fwrite($pdfTempFile, $pdfCreateContent);
						fclose($pdfTempFile);

						if($pdfTempName != '' && file_exists($pdfTempName)) {
							$successMessage .= ' Die temporäre Datei ' . $pdfTempName . ' wurde generiert. ' . '<br />';
						}
						else {
							$errorMessage .= ' Die temporäre Datei ' . $pdfTempName . ' konnte nicht generiert werden. ' . '<br />';
						}
						clearstatcache();
					// EOF WRITE PDF CREATION CONTENT TO TMP FILE
					if($pdfTempName != '' && file_exists($pdfTempName)) {
						echo '<input type="hidden" name="pdfTempName" value="' . $pdfTempName . '" />';
					}
				?>

				<br />
				<script type="text/javascript" language="javascript">
					window.alert('Bitte kontrollieren Sie, ob die Vorschau korrekt ist.\nBitte erst speichern, wenn die Daten in Ordnung sind.');
				</script>

				<div class="actionButtonsArea">
					<input type="hidden" name="selectBlankoPageNumber" value="<?php echo $_POST["selectBlankoPageNumber"]; ?>" />
					<input type="hidden" name="selectBankAccount" value="<?php echo $_POST["selectBankAccount"]; ?>" />
					<input type="submit" class="inputButton1 inputButtonGreen" name="storeDatas" value="PDF erzeugen" />
				</div>

				</form>
				<?php
					// EOF DOCUMENT PREVIEW
				?>
			</div>
			<?php } ?>
			<?php
				echo '<hr />';
				displayMessages();
			?>

		</div>
		<div class="clear"></div>


	</div>
</div>

<script language="javascript" type="text/javascript">
	$(document).ready(function() {
		colorRowMouseOver('.displayOrders tbody tr');

		$('.buttonSendDocument').click(function() {
			var mailDocumentDatas = $(this).attr('rel');
			sendAttachedDocument($(this), mailDocumentDatas, '<?php echo $thisDocumentValue[$thisKey]["orderDocumentsCustomerNumber"]; ?>', '<?php echo $_REQUEST["editID"]; ?>', '', '<?php echo $_SERVER["PHP_SELF"]; ?>', '');
		});
	});
</script>

<?php require_once('inc/footerHTML.inc.php'); ?>