<?php
	
	if(!empty($arrGetUserRights)) {
		$arrMenueSidebar = array();
		$i = -1;

		$i++;
		$arrMenueSidebar[$i] = array("TEXT" => "Startseite", "LINK" => PAGE_QUICK_LINKS, "ICON" => "home.png");

		$i++;
		$arrMenueSidebar[$i] = array("TEXT" => "Stammdaten", "LINK" => array(), "ICON" => "company.png");
		if($arrGetUserRights["editCustomers"] || $arrGetUserRights["displayCustomers"]) {
			$arrMenueSidebar[$i]["LINK"][] = array("TEXT" => "Kunden (Debitoren)", "LINK" => PAGE_EDIT_CUSTOMER, "ICON" => "customers.png");
		}
		if($arrGetUserRights["displayOnlineAcquisition"] || $arrGetUserRights["displayOnlineAcquisition"]) {
			$arrMenueSidebar[$i]["LINK"][] = array("TEXT" => "Au&szlig;endienst-Neukunden", "LINK" => PAGE_DISPLAY_CUSTOMERS_ONLINE, "ICON" => "acquisition.png");
		}
		if($arrGetUserRights["editDistributors"] || $arrGetUserRights["displaySuppliers"]) {
			$arrMenueSidebar[$i]["LINK"][] = array("TEXT" => "Lieferanten (Kreditoren)", "LINK" => PAGE_EDIT_DISTRIBUTORS, "ICON" => "suppliers.png");
		}

		$showSeparator = true;
		if($showSeparator) { $arrMenueSidebar[$i]["LINK"][] = array("TEXT" => "{###SEPARATOR###}"); }
		$showSeparator = false;

		if($arrGetUserRights["editCustomers"] || $arrGetUserRights["displayCustomers"]) {
			$arrMenueSidebar[$i]["LINK"][] = array("TEXT" => "Vertreter", "LINK" => PAGE_EDIT_SALESMEN, "ICON" => "salesman.png");
		}
		$arrMenueSidebar[$i]["LINK"][] = array("TEXT" => "PLZ-Karte", "LINK" => PAGE_SALESMEN_FLASH_MAP, "TITLE" => "PLZ-Karte", "ICON" => "iconMapPLZ.png");

		$showSeparator = true;
		if($showSeparator) { $arrMenueSidebar[$i]["LINK"][] = array("TEXT" => "{###SEPARATOR###}"); }
		$showSeparator = false;

		if($arrGetUserRights["editCompanyDatas"] || $arrGetUserRights["displayCompanyDatas"]) {
			$arrMenueSidebar[$i]["LINK"][] = array("TEXT" => "Unsere Firmendaten", "LINK" => PAGE_COMPANY_DATAS, "ICON" => "home.png");
		}
		if($arrGetUserRights["editPersonnel"] || $arrGetUserRights["displayPersonnel"]) {
			$arrMenueSidebar[$i]["LINK"][] = array("TEXT" => "Mitarbeiter", "LINK" => PAGE_DISPLAY_PERSONNEL, "ICON" => "personnel.png");
		}
		if($arrGetUserRights["editCalendar"] || $arrGetUserRights["displayCalendar"]) {
			$arrMenueSidebar[$i]["LINK"][] = array("TEXT" => "Kalender", "LINK" => PAGE_DISPLAY_CALENDAR, "ICON" => "calendar.png");
		}

		//if($arrGetUserRights["displayInternalMessages"]) {
		if(1) {
			$arrMenueSidebar[$i]["LINK"][] = array("TEXT" => "Infos &amp; Benachrichtigungen", "LINK" => PAGE_DISPLAY_MESSAGES, "ICON" => "messages.png");
		}

		if($arrGetUserRights["editBankDatas"] || $arrGetUserRights["displayBankDatas"]) {
			$arrMenueSidebar[$i]["LINK"][] = array("TEXT" => "Bankdaten", "LINK" => PAGE_DISPLAY_BANK_DATAS, "ICON" => "bank.png");
		}
		if($arrGetUserRights["editShippingDatas"] || $arrGetUserRights["displayShippingDatas"]) {
			$arrMenueSidebar[$i]["LINK"][] = array("TEXT" => "Versand", "LINK" => PAGE_SHIPPING_DATAS, "ICON" => "shipping.png");
		}

		$showSeparator = true;
		if($showSeparator) { $arrMenueSidebar[$i]["LINK"][] = array("TEXT" => "{###SEPARATOR###}"); }
		$showSeparator = false;

		if($arrGetUserRights["editProducts"] || $arrGetUserRights["displayProducts"]) {
			#$arrMenueSidebar[$i]["LINK"][] = array("TEXT" => "Artikel", "LINK" => PAGE_DISPLAY_PRODUCTS, "ICON" => "products.png");
		}

		if($arrGetUserRights["displayStocks"] || $arrGetUserRights["editStocks"]) {
			#$arrMenueSidebar[$i]["LINK"][] = array("TEXT" => "EAN-Nummern verwalten", "LINK" => PAGE_EDIT_EAN_NUMBERS, "ICON" => "barcode.png");
		}

		if($arrGetUserRights["editAddresses"] || $arrGetUserRights["displayAddresses"]) {
			#$arrMenueSidebar[$i]["LINK"][] = array("TEXT" => "Adressen", "LINK" => PAGE_DISPLAY_ADDRESSES, "ICON" => "addresses.png");
			$arrMenueSidebar[$i]["LINK"][] = array("TEXT" => "KFZ-Zulassungsstellen", "LINK" => PAGE_DISPLAY_ADDRESSES, "ICON" => "addresses.png");
		}

		$i++;
		$arrMenueSidebar[$i] = array("TEXT" => "E-Mails", "LINK" => array(), "ICON" => "mail.png");
		if($arrGetUserRights["displayWebmail"]) {
			$arrMenueSidebar[$i]["LINK"][] = array("TEXT" => "Webmail", "LINK" => PAGE_MAIL_ACCOUNT_HOSTER_4, "ICON" => "mail_alfa.png", "TARGET" => "_blank");
			$arrMenueSidebar[$i]["LINK"][] = array("TEXT" => "Alfahosting", "LINK" => PAGE_MAIL_ACCOUNT_HOSTER_1, "ICON" => "mail_alfa.png", "TARGET" => "_blank");
		}
		if($arrGetUserRights["displaySendedMails"]) {
			$arrMenueSidebar[$i]["LINK"][] = array("TEXT" => "Gesendete Auftragslisten-Mails", "LINK" => PAGE_DISPLAY_MAILS, "ICON" => "sendedMails.png");
		}

		$i++;
		$arrMenueSidebar[$i] = array("TEXT" => "Produktionsvorgänge", "LINK" => array(), "ICON" => "create.png");
		if($arrGetUserRights["editOrders"] || $arrGetUserRights["displayOrders"]) {
			$arrMenueSidebar[$i]["LINK"][] = array("TEXT" => "Neue Produktion anlegen", "LINK" => PAGE_EDIT_PROCESS . "?editID=NEW", "ICON" => "addProcess.png");
		}
		if($arrGetUserRights["displayOrdersOverview"] || $arrGetUserRights["editOrders"] || $arrGetUserRights["displayOrders"]) {
			$arrMenueSidebar[$i]["LINK"][] = array("TEXT" => "Produktions-Übersicht", "LINK" => PAGE_DISPLAY_ORDERS_OVERVIEW, "ICON" => "ordersoverview.png");
		}
		if($arrGetUserRights["displayOrdersOverview"] || $arrGetUserRights["editOrders"] || $arrGetUserRights["displayOrders"]) {
			$arrMenueSidebar[$i]["LINK"][] = array("TEXT" => "TR-Produktionen", "LINK" => PAGE_DISPLAY_EXTERNAL_PRODUCTIONS, "ICON" => "iconTR.png");
		}

		if($arrGetUserRights["displayTracking"]) {
			$arrMenueSidebar[$i]["LINK"][] = array("TEXT" => "TR-Container/LKW-Listen", "LINK" => PAGE_CONTAINER_LISTS, "ICON" => "shipping.png");
		}

		if($arrGetUserRights["displayOrdersOverview"] || $arrGetUserRights["editOrders"] || $arrGetUserRights["displayOrders"]) {
			$arrMenueSidebar[$i]["LINK"][] = array("TEXT" => "ABS-Leistendaten", "LINK" => PAGE_DISPLAY_ABS_PLATE_DATA, "ICON" => "abs-plate.png");
		}

		if($arrGetUserRights["displayPrintProductionFiles"] || $arrGetUserRights["editPrintProductionFiles"]) {
			$arrMenueSidebar[$i]["LINK"][] = array("TEXT" => "Belichtungs-Auftr&auml;ge", "LINK" => PAGE_CREATE_PRINT_PRODUCTION_FILES, "ICON" => "datasheet.png");
		}

		$showSeparator = true;
		if($showSeparator) { $arrMenueSidebar[$i]["LINK"][] = array("TEXT" => "{###SEPARATOR###}"); }
		$showSeparator = false;

		if($arrGetUserRights["importOnlineOrders"] || $arrGetUserRights["displayOnlineOrders"]) {
			$arrMenueSidebar[$i]["LINK"][] = array("TEXT" => "Online-Bestellungen", "LINK" => PAGE_DISPLAY_ONLINE_ORDERS, "ICON" => "shop.png");
		}

		if($arrGetUserRights["importOnlineOrders"] || $arrGetUserRights["displayOnlineOrders"]) {
			$arrMenueSidebar[$i]["LINK"][] = array("TEXT" => "Lieferanten-Bestellungen", "LINK" => PAGE_DISPLAY_SUPPLIER_ORDERS, "ICON" => "suppliers.png");
		}

		if($arrGetUserRights["importOnlineOrders"] || $arrGetUserRights["displayOnlineOrders"]) {
			$arrMenueSidebar[$i]["LINK"][] = array("TEXT" => "Amazon-Bestellungen", "LINK" => PAGE_DISPLAY_AMAZON_ORDERS, "ICON" => "amazon.png");
		}

		$showSeparator = true;
		if($showSeparator) { $arrMenueSidebar[$i]["LINK"][] = array("TEXT" => "{###SEPARATOR###}"); }
		$showSeparator = false;

		if($arrGetUserRights["displayTracking"]) {
			$arrMenueSidebar[$i]["LINK"][] = array("TEXT" => "Sendungsverfolgung", "LINK" => PAGE_SEARCH_TRACKING, "ICON" => "tracking.png");
		}
		if($arrGetUserRights["displayTracking"]) {
			$arrMenueSidebar[$i]["LINK"][] = array("TEXT" => "Paket-Historie", "LINK" => PAGE_DELIVERY_HISTORY, "ICON" => "tracking.png");
		}


		$i++;
		$showSeparator = false;
		$arrMenueSidebar[$i] = array("TEXT" => "Lager-&amp; Produkt-Verwaltung", "LINK" => array(), "ICON" => "stock.png");
		if($arrGetUserRights["displayStocks"]) {
			$arrMenueSidebar[$i]["LINK"][] = array("TEXT" => "Bestand-Listen", "LINK" => PAGE_PRODUCTS_STOCK_OVERVIEW, "ICON" => "editStock.png");
			#$arrMenueSidebar[$i]["LINK"][] = array("TEXT" => "Bestand-Verbrauch", "LINK" => PAGE_PRODUCTS_STOCK_CONSUMPTION, "ICON" => "stock.png");
		}				
		
		if($arrGetUserRights["editStocks"]) {			
			// $arrMenueSidebar[$i]["LINK"][] = array("TEXT" => "Bestands-Eingabe", "LINK" => PAGE_PRODUCTS_STOCK_EDIT, "ICON" => "stock.png");
			
			$arrMenueSidebar[$i]["LINK"][] = array("TEXT" => "Lager ansehen", "LINK" => 'showStock.php', "ICON" => "shipping.png");
			$arrMenueSidebar[$i]["LINK"][] = array("TEXT" => "Lager Details", "LINK" => 'showAllStock.php', "ICON" => "shipping.png");
			$arrMenueSidebar[$i]["LINK"][] = array("TEXT" => "Wawi-Artikel", "LINK" => PAGE_PRODUCTS_STOCK_PRODUCTS, "ICON" => "products.png");
			$arrMenueSidebar[$i]["LINK"][] = array("TEXT" => "Artikel-Kategorien", "LINK" => PAGE_PRODUCTS_STOCK_CATEGORIES, "ICON" => "categories.png");
			$arrMenueSidebar[$i]["LINK"][] = array("TEXT" => "Artikel-Optionen &amp; Attribute", "LINK" => PAGE_PRODUCTS_STOCK_OPTIONS, "ICON" => "categories.png");
			$arrMenueSidebar[$i]["LINK"][] = array("TEXT" => "EAN-Nummern verwalten", "LINK" => PAGE_EDIT_EAN_NUMBERS, "ICON" => "barcode.png");
			
			$arrMenueSidebar[$i]["LINK"][] = array("TEXT" => "Online-Artikel ", "LINK" => PAGE_DISPLAY_PRODUCTS, "ICON" => "products.png");
			// $arrMenueSidebar[$i]["LINK"][] = array("TEXT" => "Verpackungseinheiten", "LINK" => PAGE_PRODUCTS_STOCK_PACKAGING_UNIT, "ICON" => "shipping.png");
			$showSeparator = true;
			if($showSeparator) { $arrMenueSidebar[$i]["LINK"][] = array("TEXT" => "{###SEPARATOR###}"); }
			$showSeparator = false;
			$arrMenueSidebar[$i]["LINK"][] = array("TEXT" => "Drucker-Auftr&auml;ge ", "LINK" => PAGE_DISPLAY_PRINTERS_ORDERRS, "ICON" => "datasheet.png");
		}
		

		$i++;
		$showSeparator = false;
		$arrMenueSidebar[$i] = array("TEXT" => "Buchhaltung", "LINK" => array(), "ICON" => "accounting.png");

		if($arrGetUserRights["editConfirmations"] || $arrGetUserRights["Confirmations"]) {
			$arrMenueSidebar[$i]["LINK"][] = array("TEXT" => "Aufträge ohne Rechnung", "LINK" => PAGE_DISPLAY_CONFIRMATIONS_WITHOUT_INVOICE, "ICON" => "noInvoices.png");
			$arrMenueSidebar[$i]["LINK"][] = array("TEXT" => "{###SPACER###}");
			$showSeparator = true;
		}

		if($showSeparator) { $arrMenueSidebar[$i]["LINK"][] = array("TEXT" => "{###SEPARATOR###}"); }

		$showSeparator = false;

		if($arrGetUserRights["editOffers"] || $arrGetUserRights["displayOffers"]) {
			#$arrMenueSidebar[$i]["LINK"][] = array("TEXT" => "Alle ansehen", "LINK" => PAGE_DISPLAY_DOCUMENTS, "ICON" => "documents4.png");
			#$arrMenueSidebar[$i]["LINK"][] = array("TEXT" => "{###SPACER###}");
			#$showSeparator = true;
		}
		if($arrGetUserRights["editOffers"] || $arrGetUserRights["displayOffers"]) {
			$arrMenueSidebar[$i]["LINK"][] = array("TEXT" => "Angebote ansehen", "LINK" => PAGE_DISPLAY_OFFER, "ICON" => "documents4.png");
			#$arrMenueSidebar[$i]["LINK"][] = array("TEXT" => "Angebot schreiben", "LINK" => PAGE_CREATE_OFFER, "ICON" => "create.png");
			$arrMenueSidebar[$i]["LINK"][] = array("TEXT" => "{###SPACER###}");
			$showSeparator = true;
		}
		if($arrGetUserRights["editConfirmations"] || $arrGetUserRights["Confirmations"]) {
			$arrMenueSidebar[$i]["LINK"][] = array("TEXT" => "Aufträge ansehen", "LINK" => PAGE_DISPLAY_CONFIRMATION, "ICON" => "documents4.png");
			#$arrMenueSidebar[$i]["LINK"][] = array("TEXT" => "Auftrag schreiben", "LINK" => PAGE_CREATE_CONFIRMATION, "ICON" => "create.png");
			$arrMenueSidebar[$i]["LINK"][] = array("TEXT" => "{###SPACER###}");
			$showSeparator = true;
		}
		if($arrGetUserRights["editDeliveries"] || $arrGetUserRights["displayDeliveries"]) {
			$arrMenueSidebar[$i]["LINK"][] = array("TEXT" => "Lieferscheine ansehen", "LINK" => PAGE_DISPLAY_DELIVERY, "ICON" => "documents4.png");
			#$arrMenueSidebar[$i]["LINK"][] = array("TEXT" => "Lieferschein schreiben", "LINK" => PAGE_CREATE_DELIVERY, "ICON" => "create.png");
			$arrMenueSidebar[$i]["LINK"][] = array("TEXT" => "{###SPACER###}");
			$showSeparator = true;
		}
		if($arrGetUserRights["editInvoices"] || $arrGetUserRights["displayInvoices"]) {
			$arrMenueSidebar[$i]["LINK"][] = array("TEXT" => "Rechnungen ansehen", "LINK" => PAGE_DISPLAY_INVOICE, "ICON" => "documents4.png");
			#$arrMenueSidebar[$i]["LINK"][] = array("TEXT" => "Rechnung schreiben", "LINK" => PAGE_CREATE_INVOICE, "ICON" => "create.png");
			$arrMenueSidebar[$i]["LINK"][] = array("TEXT" => "{###SPACER###}");
			$showSeparator = true;
		}
		if($arrGetUserRights["editCredits"] || $arrGetUserRights["displayCredits"]) {
			$arrMenueSidebar[$i]["LINK"][] = array("TEXT" => "Gutschriften ansehen", "LINK" => PAGE_DISPLAY_CREDIT, "ICON" => "documents4.png");
			// $arrMenueSidebar[$i]["LINK"][] = array("TEXT" => "Gutschrift schreiben", "LINK" => PAGE_CREATE_CREDIT, "ICON" => "create.png");
			$arrMenueSidebar[$i]["LINK"][] = array("TEXT" => "{###SPACER###}");
		}
		if($arrGetUserRights["editConfirmations"] || $arrGetUserRights["Confirmations"]) {
			$arrMenueSidebar[$i]["LINK"][] = array("TEXT" => "Reklamationen ansehen", "LINK" => PAGE_DISPLAY_REKLAMATION, "ICON" => "documents4.png");
			#$arrMenueSidebar[$i]["LINK"][] = array("TEXT" => "Auftrag schreiben", "LINK" => PAGE_CREATE_CONFIRMATION, "ICON" => "create.png");
			$arrMenueSidebar[$i]["LINK"][] = array("TEXT" => "{###SPACER###}");
			$showSeparator = true;
		}

		if($showSeparator) { if($showSeparator) { $arrMenueSidebar[$i]["LINK"][] = array("TEXT" => "{###SEPARATOR###}"); } }

		$showSeparator = false;
		if($arrGetUserRights["editReminders"] || $arrGetUserRights["displayReminders"]) {
			$arrMenueSidebar[$i]["LINK"][] = array("TEXT" => "Zahlungserinnerungen ansehen", "LINK" => PAGE_DISPLAY_REMINDER, "ICON" => "documents4.png");
			// $arrMenueSidebar[$i]["LINK"][] = array("TEXT" => "Zahlungserinnerung schreiben", "LINK" => PAGE_CREATE_REMINDER, "ICON" => "create.png");
			$arrMenueSidebar[$i]["LINK"][] = array("TEXT" => "{###SPACER###}");
			$showSeparator = true;
		}
		if($arrGetUserRights["editReminders"] || $arrGetUserRights["displayReminders"]) {
			$arrMenueSidebar[$i]["LINK"][] = array("TEXT" => "1. Mahnungen ansehen", "LINK" => PAGE_DISPLAY_FIRST_DEMAND, "ICON" => "documents4.png");
			// $arrMenueSidebar[$i]["LINK"][] = array("TEXT" => "Mahnung schreiben", "LINK" => PAGE_DISPLAY_FIRST_DEMAND, "ICON" => "create.png");
			$arrMenueSidebar[$i]["LINK"][] = array("TEXT" => "{###SPACER###}");
			$showSeparator = true;
		}
		if($arrGetUserRights["editReminders"] || $arrGetUserRights["displayReminders"]) {
			$arrMenueSidebar[$i]["LINK"][] = array("TEXT" => "2. Mahnungen ansehen", "LINK" => PAGE_DISPLAY_SECOND_DEMAND, "ICON" => "documents4.png");
			// $arrMenueSidebar[$i]["LINK"][] = array("TEXT" => "Mahnung schreiben", "LINK" => PAGE_DISPLAY_SECOND_DEMAND, "ICON" => "create.png");
			$arrMenueSidebar[$i]["LINK"][] = array("TEXT" => "{###SPACER###}");
			$showSeparator = true;
		}

		if($arrGetUserRights["editInvoices"] || $arrGetUserRights["displayInvoices"]) {
			#$arrMenueSidebar[$i]["LINK"][] = array("TEXT" => "Abschreibungen ansehen", "LINK" => PAGE_DISPLAY_DEPRECIATION, "ICON" => "documents4.png");
			#$arrMenueSidebar[$i]["LINK"][] = array("TEXT" => "{###SPACER###}");
			#$showSeparator = true;
		}

		if($arrGetUserRights["editInvoices"] || $arrGetUserRights["displayInvoices"]) {
			#$arrMenueSidebar[$i]["LINK"][] = array("TEXT" => "Mahnbescheide ansehen", "LINK" => PAGE_DISPLAY_SUMMONS, "ICON" => "documents4.png");
			#$arrMenueSidebar[$i]["LINK"][] = array("TEXT" => "{###SPACER###}");
			#$showSeparator = true;
		}

		if($showSeparator) { $arrMenueSidebar[$i]["LINK"][] = array("TEXT" => "{###SEPARATOR###}"); }

		if($arrGetUserRights["editLetters"] || $arrGetUserRights["displayLetters"]) {
			$arrMenueSidebar[$i]["LINK"][] = array("TEXT" => "Briefe ansehen", "LINK" => PAGE_DISPLAY_LETTER, "ICON" => "documents4.png");
			#$arrMenueSidebar[$i]["LINK"][] = array("TEXT" => "Brief schreiben", "LINK" => PAGE_CREATE_LETTER, "ICON" => "create.png");
			$arrMenueSidebar[$i]["LINK"][] = array("TEXT" => "{###SPACER###}");
			$showSeparator = true;
		}

		if($arrGetUserRights["exportOrders"]) {
			// $arrMenueSidebar[$i]["LINK"][] = array("TEXT" => "Export Vertreter-Bestellungen", "LINK" => PAGE_EXPORT_ORDERS, "ICON" => "");
		}
		/*
		if($arrGetUserRights["editCustomers"] || $arrGetUserRights["displayCustomers"]) {
			$arrMenueSidebar[$i]["LINK"][] = array("TEXT" => "Bankdaten", "LINK" => PAGE_ADRESSES, "ICON" => "");
		}
		if($arrGetUserRights["editCustomers"] || $arrGetUserRights["displayCustomers"]) {
			$arrMenueSidebar[$i]["LINK"][] = array("TEXT" => "Abrufen", "LINK" => PAGE_ADRESSES, "ICON" => "");
		}
		if($arrGetUserRights["editCustomers"] || $arrGetUserRights["displayCustomers"]) {
			$arrMenueSidebar[$i]["LINK"][] = array("TEXT" => "KTO-Auszug", "LINK" => PAGE_ADRESSES, "ICON" => "");
		}
		*/

		$showSeparator = false;
		if($arrGetUserRights["editFinancialDatas"] || $arrGetUserRights["displayFinancialDatas"]) {
		$i++;
		$arrMenueSidebar[$i] = array("TEXT" => "Finanzen", "LINK" => array(), "ICON" => "bank.png");
		if($arrGetUserRights["editFinancialDatas"] || $arrGetUserRights["displayFinancialDatas"]) {
			$arrMenueSidebar[$i]["LINK"][] = array("TEXT" => "Alle Auftragsbest&auml;tigungen", "LINK" => PAGE_ALL_CONFIRMATIONS, "ICON" => "bank.png");
			$arrMenueSidebar[$i]["LINK"][] = array("TEXT" => "{###SPACER###}");
			$showSeparator = true;
		}
		if($arrGetUserRights["editFinancialDatas"] || $arrGetUserRights["displayFinancialDatas"]) {
			$arrMenueSidebar[$i]["LINK"][] = array("TEXT" => "Offene Auftragsbest&auml;tigungen", "LINK" => PAGE_UNPAID_CONFIRMATIONS, "ICON" => "bank.png");
			$arrMenueSidebar[$i]["LINK"][] = array("TEXT" => "{###SPACER###}");
			$showSeparator = true;
		}
		if($arrGetUserRights["editFinancialDatas"] || $arrGetUserRights["displayFinancialDatas"]) {
			$arrMenueSidebar[$i]["LINK"][] = array("TEXT" => "Bezahlte Auftragsbest&auml;tigungen", "LINK" => PAGE_PAID_CONFIRMATIONS, "ICON" => "bank.png");
			$arrMenueSidebar[$i]["LINK"][] = array("TEXT" => "{###SPACER###}");
			$showSeparator = true;
		}

		if($showSeparator) { $arrMenueSidebar[$i]["LINK"][] = array("TEXT" => "{###SEPARATOR###}"); }
		}

		$showSeparator = false;
		if($arrGetUserRights["editFinancialDatas"] || $arrGetUserRights["displayFinancialDatas"]) {
			$arrMenueSidebar[$i]["LINK"][] = array("TEXT" => "Gutgeschriebene Rechnungen", "LINK" => PAGE_CREDITED_INVOICES, "ICON" => "bank.png");
			$arrMenueSidebar[$i]["LINK"][] = array("TEXT" => "{###SPACER###}");
			$showSeparator = true;
		}

		if($arrGetUserRights["editFinancialDatas"] || $arrGetUserRights["displayFinancialDatas"]) {
			$arrMenueSidebar[$i]["LINK"][] = array("TEXT" => "Alle Gutschriften", "LINK" => PAGE_ALL_CREDITS, "ICON" => "bank.png");
			$arrMenueSidebar[$i]["LINK"][] = array("TEXT" => "{###SPACER###}");
			$showSeparator = true;
		}
		if($showSeparator) { $arrMenueSidebar[$i]["LINK"][] = array("TEXT" => "{###SEPARATOR###}"); }

		$showSeparator = false;
		if($arrGetUserRights["editFinancialDatas"] || $arrGetUserRights["displayFinancialDatas"]) {
			$arrMenueSidebar[$i]["LINK"][] = array("TEXT" => "Alle Rechnungen", "LINK" => PAGE_ALL_INVOICES, "ICON" => "bank.png");
			$arrMenueSidebar[$i]["LINK"][] = array("TEXT" => "{###SPACER###}");
			$showSeparator = true;
		}

		if($arrGetUserRights["editFinancialDatas"] || $arrGetUserRights["displayFinancialDatas"]) {
			$arrMenueSidebar[$i]["LINK"][] = array("TEXT" => "Offene Rechnungen", "LINK" => PAGE_UNPAID_INVOICES, "ICON" => "bank.png");
			$arrMenueSidebar[$i]["LINK"][] = array("TEXT" => "{###SPACER###}");
			$showSeparator = true;
		}

		if($arrGetUserRights["editFinancialDatas"] || $arrGetUserRights["displayFinancialDatas"]) {
			$arrMenueSidebar[$i]["LINK"][] = array("TEXT" => "Teilbezahlte Rechnungen", "LINK" => PAGE_PAIDPARTLY_INVOICES, "ICON" => "bank.png");
			$arrMenueSidebar[$i]["LINK"][] = array("TEXT" => "{###SPACER###}");
			$showSeparator = true;
		}

		if($arrGetUserRights["editFinancialDatas"] || $arrGetUserRights["displayFinancialDatas"]) {
			$arrMenueSidebar[$i]["LINK"][] = array("TEXT" => "Bezahlte Rechnungen", "LINK" => PAGE_PAID_INVOICES, "ICON" => "bank.png");
			$arrMenueSidebar[$i]["LINK"][] = array("TEXT" => "{###SPACER###}");
			$showSeparator = true;
		}

		if($showSeparator) { $arrMenueSidebar[$i]["LINK"][] = array("TEXT" => "{###SEPARATOR###}"); }

		$showSeparator = false;
		if($arrGetUserRights["editFinancialDatas"] || $arrGetUserRights["displayFinancialDatas"]) {
			$arrMenueSidebar[$i]["LINK"][] = array("TEXT" => "&Uuml;berf&auml;llige Rechnungen", "LINK" => PAGE_REMIND_INVOICES, "ICON" => "bank.png");
			$arrMenueSidebar[$i]["LINK"][] = array("TEXT" => "{###SPACER###}");
			$showSeparator = true;
		}
		if($arrGetUserRights["editFinancialDatas"] || $arrGetUserRights["displayFinancialDatas"]) {
			$arrMenueSidebar[$i]["LINK"][] = array("TEXT" => "Zahlungserinnerungen", "LINK" => PAGE_REMINDED_INVOICES, "ICON" => "bank.png");
			$arrMenueSidebar[$i]["LINK"][] = array("TEXT" => "{###SPACER###}");
			$showSeparator = true;
		}
		if($arrGetUserRights["editFinancialDatas"] || $arrGetUserRights["displayFinancialDatas"]) {
			$arrMenueSidebar[$i]["LINK"][] = array("TEXT" => "1. Mahnungen", "LINK" => PAGE_FIRST_DEMANDED_INVOICES, "ICON" => "bank.png");
			$arrMenueSidebar[$i]["LINK"][] = array("TEXT" => "{###SPACER###}");
			$showSeparator = true;
		}
		if($arrGetUserRights["editFinancialDatas"] || $arrGetUserRights["displayFinancialDatas"]) {
			$arrMenueSidebar[$i]["LINK"][] = array("TEXT" => "2. Mahnungen", "LINK" => PAGE_SECOND_DEMANDED_INVOICES, "ICON" => "bank.png");
			$arrMenueSidebar[$i]["LINK"][] = array("TEXT" => "{###SPACER###}");
			$showSeparator = true;
		}

		if($arrGetUserRights["editFinancialDatas"] || $arrGetUserRights["displayFinancialDatas"]) {
			$arrMenueSidebar[$i]["LINK"][] = array("TEXT" => "Abschreibungen", "LINK" => PAGE_DEPRECIATED_INVOICES, "ICON" => "bank.png");
			$arrMenueSidebar[$i]["LINK"][] = array("TEXT" => "{###SPACER###}");
			$showSeparator = true;
		}

		if($arrGetUserRights["editFinancialDatas"] || $arrGetUserRights["displayFinancialDatas"]) {
			$arrMenueSidebar[$i]["LINK"][] = array("TEXT" => "Mahnbescheide", "LINK" => PAGE_SUMMONSED_INVOICES, "ICON" => "bank.png");
			$arrMenueSidebar[$i]["LINK"][] = array("TEXT" => "{###SPACER###}");
			$showSeparator = true;
		}

		if($arrGetUserRights["editFinancialDatas"] || $arrGetUserRights["displayFinancialDatas"]) {
			$arrMenueSidebar[$i]["LINK"][] = array("TEXT" => "Inkasso/Anwalt", "LINK" => PAGE_CASHING_INVOICES, "ICON" => "bank.png");
			$arrMenueSidebar[$i]["LINK"][] = array("TEXT" => "{###SPACER###}");
			$showSeparator = true;
		}

		if($arrGetUserRights["editFinancialDatas"]) {
			$arrMenueSidebar[$i]["LINK"][] = array("TEXT" => "Zahlungsbewegungen", "LINK" => PAGE_DISPLAY_FINANCIAL_STATISTICS, "ICON" => "payment.png");
			$arrMenueSidebar[$i]["LINK"][] = array("TEXT" => "{###SPACER###}");
			$showSeparator = true;
		}

		/*
		if($arrGetUserRights["editCustomers"] || $arrGetUserRights["displayCustomers"]) {
			$arrMenueSidebar[$i]["LINK"][] = array("TEXT" => "Abrufen", "LINK" => PAGE_ADRESSES, "ICON" => "");
		}
		if($arrGetUserRights["editCustomers"] || $arrGetUserRights["displayCustomers"]) {
			$arrMenueSidebar[$i]["LINK"][] = array("TEXT" => "Konto-Auszug", "LINK" => PAGE_ADRESSES, "ICON" => "");
		}
		*/
		
		$i++;
		$showSeparator = false;
		$arrMenueSidebar[$i] = array("TEXT" => "Telefon-Marketing", "LINK" => array(), "ICON" => "phoneMarketing.png");
		if($arrGetUserRights["displayDistribution"] || $arrGetUserRights["displayOrdersOverview"] || $arrGetUserRights["editOrders"] || $arrGetUserRights["displayOrders"]) {
			$arrMenueSidebar[$i]["LINK"][] = array("TEXT" => "Bestell-Intervalle", "LINK" => PAGE_DISPLAY_CUSTOMERS_ORDER_INTERVALS, "ICON" => "calendar.png");
		}
		if($arrGetUserRights["displayDistribution"]) {
			$arrMenueSidebar[$i]["LINK"][] = array("TEXT" => "Telefon-Termine", "LINK" => PAGE_PHONE_MARKETING_DATES, "ICON" => "iconTime.png");
			$arrMenueSidebar[$i]["LINK"][] = array("TEXT" => "Telefon-Statistik", "LINK" => PAGE_PHONE_MARKETING_STATISTICS, "ICON" => "statistics.png");
		}

		$i++;
		$showSeparator = false;
		$arrMenueSidebar[$i] = array("TEXT" => "Provisionen / Vertrieb", "LINK" => array(), "ICON" => "acquisition.png");

		# if($showSeparator) { $arrMenueSidebar[$i]["LINK"][] = array("TEXT" => "{###SEPARATOR###}"); }
		if($arrGetUserRights["editProvisionDatas"] || $arrGetUserRights["displayProvisionDatas"]) {
			$arrMenueSidebar[$i]["LINK"][] = array("TEXT" => "Vertreter-Provision", "LINK" => PAGE_SALESMEN_PROVISION, "ICON" => "salesman.png");
			$arrMenueSidebar[$i]["LINK"][] = array("TEXT" => "{###SPACER###}");
			$showSeparator = true;
		}
		if($arrGetUserRights["editProvisionDatas"] || $arrGetUserRights["displayProvisionDatas"]) {
			$arrMenueSidebar[$i]["LINK"][] = array("TEXT" => "Verb&auml;nde-Provision", "LINK" => PAGE_GROUPS_PROVISION, "ICON" => "groups.png");
			$arrMenueSidebar[$i]["LINK"][] = array("TEXT" => "{###SPACER###}");
			$showSeparator = true;
		}
		if($arrGetUserRights["displayDistribution"]) {
			$arrMenueSidebar[$i]["LINK"][] = array("TEXT" => "Au&szlig;endienst-Tagesberichte", "LINK" => PAGE_DISPLAY_SALESMEN_REPORTS, "ICON" => "report.png");
		}
		if($arrGetUserRights["displayDistribution"]) {
			$arrMenueSidebar[$i]["LINK"][] = array("TEXT" => "Vertreter-Verk&auml;ufe", "LINK" => PAGE_DISPLAY_SALESMEN_SALES, "ICON" => "salesman.png");
		}
		if($arrGetUserRights["displayDistribution"]) {
			$arrMenueSidebar[$i]["LINK"][] = array("TEXT" => "Vertreter-Direktverkauf", "LINK" => PAGE_DISPLAY_SALESMEN_DIRECT_SALES, "ICON" => "salesman.png");
		}
		if($arrGetUserRights["displaySales"]) {
			$arrMenueSidebar[$i]["LINK"][] = array("TEXT" => "Vertreter-Umsatz", "LINK" => PAGE_DISPLAY_SALEMEN_STATISTICS, "ICON" => "statistics.png");
		}

		if($arrGetUserRights["displayDistribution"]) {
			$arrMenueSidebar[$i]["LINK"][] = array("TEXT" => "Vertriebs-Modul", "LINK" => PAGE_DISPLAY_DISTRIBUTION, "ICON" => "distribution.png", "TARGET" => "_blank");
			$arrMenueSidebar[$i]["LINK"][] = array("TEXT" => "{###SPACER###}");
			$showSeparator = true;
		}

		###############
		$i++;
		$arrMenueSidebar[$i] = array("TEXT" => "Dokumente / Dateien", "LINK" => array(), "ICON" => "documents5.png");
		if($arrGetUserRights["displayDocuments"]) {
			$arrMenueSidebar[$i]["LINK"][] = array("TEXT" => "Kundendokumente", "LINK" => PAGE_DOWNLOAD_DOCUMENTS, "ICON" => "documents3.png");
		}
		if($arrGetUserRights["displayDocuments"]) {
			$arrMenueSidebar[$i]["LINK"][] = array("TEXT" => "Containerlisten", "LINK" => PAGE_DOWNLOAD_CONTAINER_LISTS, "ICON" => "ordersoverview.png");
		}
		if($arrGetUserRights["displayDocuments"]) {
			$arrMenueSidebar[$i]["LINK"][] = array("TEXT" => "Kataloge / Preislisten", "LINK" => PAGE_DOWNLOAD_CATALOGUES, "ICON" => "catalogue.png");
		}
		if($arrGetUserRights["displayDocuments"]) {
			// $arrMenueSidebar[$i]["LINK"][] = array("TEXT" => "Kundendateien", "LINK" => PAGE_UPLOADED_FILES, "ICON" => "documents3.png");
		}
		if($arrGetUserRights["displayDocuments"]) {
			$arrMenueSidebar[$i]["LINK"][] = array("TEXT" => "Formulare / Briefpapier", "LINK" => PAGE_DOWNLOAD_FILES, "ICON" => "documents5.png");
		}
		if($arrGetUserRights["displayDocuments"]) {
			$arrMenueSidebar[$i]["LINK"][] = array("TEXT" => "Handb&uuml;cher", "LINK" => PAGE_DOWNLOAD_MANUALS, "ICON" => "book.png");
		}
		if($arrGetUserRights["displayDocuments"]) {
			$arrMenueSidebar[$i]["LINK"][] = array("TEXT" => "Montage-Anleitungen", "LINK" => PAGE_DOWNLOAD_SHEETS, "ICON" => "montage.png");
		}

		$i++;
		$arrMenueSidebar[$i] = array("TEXT" => "Statistiken", "LINK" => array(), "ICON" => "statistics.png");
		if($arrGetUserRights["editStocks"] || $arrGetUserRights["displayStocks"]) {
			#$arrMenueSidebar[$i]["LINK"][] = array("TEXT" => "Bestand", "LINK" => PAGE_DISPLAY_STOCK, "ICON" => "statistics.png");
			if($arrGetUserRights["adminArea"]) {
				#$arrMenueSidebar[$i]["LINK"][] = array("TEXT" => "Lagerbestand", "LINK" => PAGE_DISPLAY_STOCK_INVENTORY, "ICON" => "statistics.png");
			}
		}
		if($arrGetUserRights["displayOnlineOrdersStatistics"]) {
			$arrMenueSidebar[$i]["LINK"][] = array("TEXT" => "Produkt-Statistik", "LINK" => PAGE_DISPLAY_PRODUCT_STATISTICS, "ICON" => "statistics.png");
		}

		if($arrGetUserRights["displayOrderReceipts"]) {
			$arrMenueSidebar[$i]["LINK"][] = array("TEXT" => "Bestell-Eingang", "LINK" => PAGE_DISPLAY_ORDERS_RECEIPTS, "ICON" => "statistics.png");
		}
		if($arrGetUserRights["displayOnlineOrdersStatistics"]) {
			$arrMenueSidebar[$i]["LINK"][] = array("TEXT" => "Online-Bestellungen", "LINK" => PAGE_DISPLAY_ONLINE_ORDERS_STATISTICS, "ICON" => "statistics.png");
		}
		if($arrGetUserRights["displayOnlineOrdersStatistics"]) {
			$arrMenueSidebar[$i]["LINK"][] = array("TEXT" => "Amazon-Bestellungen", "LINK" => PAGE_DISPLAY_SALES_AMAZON, "ICON" => "statistics.png");
		}
		if($arrGetUserRights["displayOrderReceipts"]) {
			$arrMenueSidebar[$i]["LINK"][] = array("TEXT" => "Layout-Arbeiten / Korrekturen", "LINK" => PAGE_DISPLAY_ORDERS_GRAPHICS, "ICON" => "statistics.png");
		}
		if($arrGetUserRights["displayOrderReceipts"]) {
			$arrMenueSidebar[$i]["LINK"][] = array("TEXT" => "Neukunden im Auftragslisten", "LINK" => PAGE_DISPLAY_NEW_CUSTOMERS_Auftragslisten, "ICON" => "statistics.png");
		}

		if($arrGetUserRights["displayOrderReceipts"]) {
			$arrMenueSidebar[$i]["LINK"][] = array("TEXT" => "Neukunden im Shop", "LINK" => PAGE_DISPLAY_NEW_CUSTOMERS_SHOP, "ICON" => "statistics.png");
		}

		if($arrGetUserRights["displayDistribution"]) {
			$arrMenueSidebar[$i]["LINK"][] = array("TEXT" => "Kennzeichenhalter pro PLZ", "LINK" => PAGE_DISPLAY_KZH_PRO_PLZ, "ICON" => "statistics.png");
			$arrMenueSidebar[$i]["LINK"][] = array("TEXT" => "{###SPACER###}");
			$showSeparator = true;
		}

		if($arrGetUserRights["displaySales"]) {
			$arrMenueSidebar[$i]["LINK"][] = array("TEXT" => "Umsatz", "LINK" => PAGE_DISPLAY_SALES, "ICON" => "statistics.png");
		}
		if($arrGetUserRights["displayDistribution"]) {
			$arrMenueSidebar[$i]["LINK"][] = array("TEXT" => "Wiederverk&auml;ufer-Umsatz", "LINK" => PAGE_DISPLAY_RESELLERS_SALES, "ICON" => "salesman.png");
			$arrMenueSidebar[$i]["LINK"][] = array("TEXT" => "Kunden-Umsatz", "LINK" => PAGE_DISPLAY_CUSTOMER_PURCHASES, "ICON" => "customers.png");
		}
		if($arrGetUserRights["displayDistribution"]) {
			$arrMenueSidebar[$i]["LINK"][] = array("TEXT" => "Kunden-Betreuung", "LINK" => PAGE_DISPLAY_SALEMEN_CUSTOMER_CARES, "ICON" => "acquisition.png");
			$arrMenueSidebar[$i]["LINK"][] = array("TEXT" => "{###SPACER###}");
			$showSeparator = true;
		}
		if($arrGetUserRights["displayDistribution"]) {
			$arrMenueSidebar[$i]["LINK"][] = array("TEXT" => "PLZ-Umsatz", "LINK" => PAGE_DISPLAY_ZIPCODE_SALES, "ICON" => "plz.png");
		}

		if($arrGetUserRights["externalLinks"]) {
			$i++;
			$arrMenueSidebar[$i] = array("TEXT" => "Externe Links", "LINK" => PAGE_EXTERNAL_LINKS, "ICON" => "externalLinks.png");
		}

		$i++;
		$arrMenueSidebar[$i] = array("TEXT" => "DATEV / Faktoring", "LINK" => array(), "ICON" => "datev.png");

		if($arrGetUserRights["editFinancialDatas"] || $arrGetUserRights["displayFinancialDatas"]) {
			#$arrMenueSidebar[$i]["LINK"][] = array("TEXT" => "Steuerberater-Export DEBITOREN", "LINK" => PAGE_EXPORT_TAXES_DATA, "ICON" => "exportTaxAccountantData.png");
			$arrMenueSidebar[$i]["LINK"][] = array("TEXT" => "DATEV-Export DEBITOREN", "LINK" => PAGE_EXPORT_TAXES_DATA, "ICON" => "exportTaxAccountantData.png");
			#$arrMenueSidebar[$i]["LINK"][] = array("TEXT" => "DATEV-Export KREDITOREN", "LINK" => PAGE_EXPORT_TAXES_DATA, "ICON" => "exportTaxAccountantData.png");
		}

		if($arrGetUserRights["editFinancialDatas"]) {
			$arrMenueSidebar[$i]["LINK"][] = array("TEXT" => "Faktoring-Export", "LINK" => PAGE_EXPORT_FACTORING, "ICON" => "factoringExport.png");
			if($arrGetUserRights["adminArea"] || 1) {
				$arrMenueSidebar[$i]["LINK"][] = array("TEXT" => "{###SPACER###}");
				$arrMenueSidebar[$i]["LINK"][] = array("TEXT" => "Faktoring Fehl. Debitoren", "LINK" => PAGE_EXPORT_FACTORING_MISSING_CUSTOMERS, "ICON" => "factoringExport.png");
			}
			$arrMenueSidebar[$i]["LINK"][] = array("TEXT" => "{###SPACER###}");
			$arrMenueSidebar[$i]["LINK"][] = array("TEXT" => "Faktoring-Import", "LINK" => PAGE_IMPORT_FACTORING, "ICON" => "factoringImport.png");
		}

		$i++;
		$arrMenueSidebar[$i] = array("TEXT" => "Werkzeuge", "LINK" => array(), "ICON" => "tools.png");

		if($showSeparator) { $arrMenueSidebar[$i]["LINK"][] = array("TEXT" => "{###SEPARATOR###}"); }

		if($arrGetUserRights["displaySendedMails"]) {
			$arrMenueSidebar[$i]["LINK"][] = array("TEXT" => "{###SPACER###}");
		}

		if($arrGetUserRights["editInternalMessages"]) {
			$arrMenueSidebar[$i]["LINK"][] = array("TEXT" => "Benachrichtigungen in- &amp; extern", "LINK" => PAGE_EDIT_INTERNAL_MESSAGES, "ICON" => "messages.png");
		}

		if($arrGetUserRights["exportOrders"]) {
			$arrMenueSidebar[$i]["LINK"][] = array("TEXT" => "Export Vertreter-Bestellungen", "LINK" => PAGE_EXPORT_ORDERS, "ICON" => "export.png");
		}
		if($arrGetUserRights["editFinancialDatas"] || $arrGetUserRights["displayFinancialDatas"]) {
			$arrMenueSidebar[$i]["LINK"][] = array("TEXT" => "Rechnungen-Export", "LINK" => PAGE_EXPORT_INVOICES, "ICON" => "finance.png");
		}


		if($arrGetUserRights["exportCustomers"]) {
			$arrMenueSidebar[$i]["LINK"][] = array("TEXT" => "PLZ-Kunden-Export", "LINK" => PAGE_EXPORT_CUSTOMERS_PER_ZIPCODES_DATAS, "ICON" => "export.png");
			$arrMenueSidebar[$i]["LINK"][] = array("TEXT" => "Mail-Adressen-Export", "LINK" => PAGE_EXPORT_CUSTOMER_MAILS, "ICON" => "export.png");
		}
		if($arrGetUserRights["exportCustomers"]) {
			$arrMenueSidebar[$i]["LINK"][] = array("TEXT" => "Shop-Nichtbesteller-Export", "LINK" => PAGE_EXPORT_SHOP_CUSTOMERS_NO_ORDER_PER_ZIPCODES_DATAS, "ICON" => "export.png");
		}
		if($arrGetUserRights["exportCustomers"]) {
			$arrMenueSidebar[$i]["LINK"][] = array("TEXT" => "Fahrberichte erstellen", "LINK" => PAGE_GENERATE_SALESMAN_REPORTS, "ICON" => "export.png");
		}

		if($arrGetUserRights["createEAN"]) {
			$arrMenueSidebar[$i]["LINK"][] = array("TEXT" => "EAN-/QR-Generator", "LINK" => PAGE_CODE_GENERATOR, "ICON" => "barcode.png", "TARGET" => "_blank");
		}
		if(1) {
			$arrMenueSidebar[$i]["LINK"][] = array("TEXT" => "IBAN-/BIC-Generator", "LINK" => PAGE_IBAN_GENERATOR, "ICON" => "calculator2.png", "TARGET" => "_blank");
		}
		if(1) {
			$arrMenueSidebar[$i]["LINK"][] = array("TEXT" => "Taschenrechner", "LINK" => PAGE_CALCULATOR, "ICON" => "calculator.png", "TARGET" => "_blank");
		}
		if($arrGetUserRights["adminArea"]) {
			$arrMenueSidebar[$i]["LINK"][] = array("TEXT" => "Briefpapier", "LINK" => PAGE_CREATE_STATIONARY, "ICON" => "stationary.png", "TARGET" => "");
		}


		$i++;
		$arrMenueSidebar[$i] = array("TEXT" => "Daten-Verwaltung", "LINK" => array(), "ICON" => "database.png");

		if($arrGetUserRights["updateOnlineCustomers"] || $arrGetUserRights["editCustomers"]) {
			$arrMenueSidebar[$i]["LINK"][] = array("TEXT" => "Shop-Kundenabgleich", "LINK" => PAGE_UPDATE_ONLINE_CUSTOMER_NUMBERS, "ICON" => "update.png");
		}
		if($arrGetUserRights["updateAcquisitionCustomers"]) {
			#$arrMenueSidebar[$i]["LINK"][] = array("TEXT" => "OKE-Kundenabgleich", "LINK" => PAGE_UPDATE_ACQUISITION_CUSTOMER_NUMBERS, "ICON" => "update.png");
		}
		if($arrGetUserRights["importDatas"]) {
			$arrMenueSidebar[$i]["LINK"][] = array("TEXT" => "Shop-Artikel-Import", "LINK" => PAGE_IMPORT_SHOP_TABLES, "ICON" => "update.png");
		}
		if($arrGetUserRights["reviseDocumentDatas"] || $arrGetUserRights["insertDeliSprintDatas"] || $arrGetUserRights["changeDocumentCustomerNumber"]) {
			$arrMenueSidebar[$i]["LINK"][] = array("TEXT" => "Daten-Korrekturen &amp; Tools", "LINK" => PAGE_REVISE_DOCUMENT_DATAS, "ICON" => "revise.png");
		}
		#else if($arrGetUserRights["insertDeliSprintDatas"]) {
			#$arrMenueSidebar[$i]["LINK"][] = array("TEXT" => "DeliSprint-Daten importieren", "LINK" => PAGE_REVISE_DOCUMENT_DATAS, "ICON" => "dpd.png");
		#}
		if($arrGetUserRights["reviseDocumentDatas"]) {
			// $arrMenueSidebar[$i]["LINK"][] = array("TEXT" => "Doppelte Kunden finden", "LINK" => PAGE_FIND_DUPLICATE_CUSTOMERS, "ICON" => "duplicates.png");
		}
		if($arrGetUserRights["importDatas"]) {
			// $arrMenueSidebar[$i]["LINK"][] = array("TEXT" => "Daten-Import", "LINK" => PAGE_IMPORT_DATAS, "ICON" => "import.png");
		}
		if($arrGetUserRights["exportDatas"]) {
			$arrMenueSidebar[$i]["LINK"][] = array("TEXT" => "DB-Tabellen-Export", "LINK" => PAGE_EXPORT_DATAS, "ICON" => "export.png");
		}
		if($arrGetUserRights["displayOnlineUsers"]) {
			$arrMenueSidebar[$i]["LINK"][] = array("TEXT" => "Auftragslisten-Online-Log", "LINK" => PAGE_ONLINE_USERS, "ICON" => "online.png");
		}
		if($arrGetUserRights["createTotalBackUp"]) {
			// $arrMenueSidebar[$i]["LINK"][] = array("TEXT" => "Daten-BackUp", "LINK" => PAGE_CREATE_BACKUP, "ICON" => "export.png");
			$arrMenueSidebar[$i]["LINK"][] = array("TEXT" => "Cloud-Sicherung", "LINK" => PAGE_CLOUD_BACKUP, "ICON" => "cloud.png");
		}
		if($arrGetUserRights["importDatas"]) {
			// $arrMenueSidebar[$i]["LINK"][] = array("TEXT" => "Shop-Server-Logs", "LINK" => PAGE_SERVER_LOGS, "ICON" => "import.png");
		}

		$i++;
		$arrMenueSidebar[$i] = array("TEXT" => "Einstellungen", "LINK" => array(), "ICON" => "config.png");

		if($arrGetUserRights["editCompanyDatas"] || $arrGetUserRights["displayCompanyDatas"]) {
			// $arrMenueSidebar[$i]["LINK"][] = array("TEXT" => "Firmendaten", "LINK" => PAGE_COMPANY_DATAS, "ICON" => "company.png");
		}
		if($arrGetUserRights["editBankDatas"] || $arrGetUserRights["displayBankDatas"]) {
			// $arrMenueSidebar[$i]["LINK"][] = array("TEXT" => "Bankdaten", "LINK" => PAGE_BANK_DATAS, "ICON" => "bank.png");
		}

		if($arrGetUserRights["editUser"]) {
			$arrMenueSidebar[$i]["LINK"][] = array("TEXT" => "Auftragslisten-Nutzer", "LINK" => PAGE_EDIT_USER, "ICON" => "personnel.png");
		}

		if($arrGetUserRights["editBasicDatas"] || $arrGetUserRights["displayBasicDatas"]) {
			$arrMenueSidebar[$i]["LINK"][] = array("TEXT" => "Basis-Konfiguration", "LINK" => PAGE_CONFIG_BASIC, "ICON" => "config.png");
		}
		if($arrGetUserRights["editFtpDatas"] || $arrGetUserRights["displayFtpDatas"]) {
			$arrMenueSidebar[$i]["LINK"][] = array("TEXT" => "FTP-Konfiguration", "LINK" => PAGE_CONFIG_FTP, "ICON" => "ftp.png");
		}
		if($arrGetUserRights["editMailDatas"] || $arrGetUserRights["displayMailDatas"]) {
			$arrMenueSidebar[$i]["LINK"][] = array("TEXT" => "Mail-Konfiguration", "LINK" => PAGE_CONFIG_MAIL, "ICON" => "mail.png");
		}

		if($arrGetUserRights["editDbDatas"] || $arrGetUserRights["displayDbDatas"]) {
			$arrMenueSidebar[$i]["LINK"][] = array("TEXT" => "DB-Konfiguration", "LINK" => PAGE_CONFIG_DB, "ICON" => "database.png");
		}
		if($arrGetUserRights["createParams"]) {
			$arrMenueSidebar[$i]["LINK"][] = array("TEXT" => "Parameter-Konfiguration", "LINK" => PAGE_CONFIG_PARAMS, "ICON" => "params.png");
		}

		$i++;
		$arrMenueSidebar[$i] = array("TEXT" => "Suchmaschinen-Optimierung", "LINK" => array(), "ICON" => "seo.png");

		if($arrGetUserRights["seoTools"]) {
			$arrMenueSidebar[$i]["LINK"][] = array("TEXT" => "Shop-Server-Statistik", "LINK" => PAGE_SERVER_STATISTIC, "ICON" => "seo.png", "TARGET" => "_blank");
			$arrMenueSidebar[$i]["LINK"][] = array("TEXT" => "Google-Index", "LINK" => PAGE_GOOGLE_INDEX, "ICON" => "google.png", "TARGET" => "_blank");
			$arrMenueSidebar[$i]["LINK"][] = array("TEXT" => "Google-Analytics", "LINK" => PAGE_GOOGLE_ANALYTICS, "ICON" => "google.png", "TARGET" => "_blank");
			$arrMenueSidebar[$i]["LINK"][] = array("TEXT" => "Google-Webmaster-Tools", "LINK" => PAGE_GOOGLE_WEBMASTERTOOLS, "ICON" => "google.png", "TARGET" => "_blank");
			$arrMenueSidebar[$i]["LINK"][] = array("TEXT" => "Bing-Index", "LINK" => PAGE_BING_INDEX, "ICON" => "bing.png", "TARGET" => "_blank");
			$arrMenueSidebar[$i]["LINK"][] = array("TEXT" => "Bing-Webmaster-Tools", "LINK" => PAGE_BING_WEBMASTERTOOLS, "ICON" => "bing.png", "TARGET" => "_blank");
			$arrMenueSidebar[$i]["LINK"][] = array("TEXT" => "Yahoo-Index", "LINK" => PAGE_YAHOO_INDEX, "ICON" => "yahoo.png", "TARGET" => "_blank");
			$arrMenueSidebar[$i]["LINK"][] = array("TEXT" => "Seiten-Analyse", "LINK" => PAGE_SEITWERT_WEBPAGE_ANALYTICS, "ICON" => "analysis.png", "TARGET" => "_blank");
			$arrMenueSidebar[$i]["LINK"][] = array("TEXT" => "Duplicate Content", "LINK" => PAGE_COPYSCAPE_WEBPAGE_FIND_DUPLICATE_CONTENT, "ICON" => "duplicateContent.png", "TARGET" => "_blank");
			$arrMenueSidebar[$i]["LINK"][] = array("TEXT" => "Duplicate Content", "LINK" => PAGE_SITESENTRAL_WEBPAGE_FIND_DUPLICATE_CONTENT, "ICON" => "duplicateContent.png", "TARGET" => "_blank");
			$arrMenueSidebar[$i]["LINK"][] = array("TEXT" => "Counter &amp; PageRanks", "LINK" => DISPLAY_SHOP_VISITORS, "ICON" => "seo.png");
		}

		$i++;
		$arrMenueSidebar[$i] = array("TEXT" => "System", "LINK" => array(), "ICON" => "system.png");
		if($arrGetUserRights["phpInfo"]) {
			$arrMenueSidebar[$i]["LINK"][] = array("TEXT" => "PHP-Info", "LINK" => PAGE_PHP_INFO, "ICON" => "phpinfo.png");
		}

		if($arrGetUserRights["serverInfo"]) {
			$arrMenueSidebar[$i]["LINK"][] = array("TEXT" => "Server-Info", "LINK" => PAGE_SERVER_INFO , "ICON" => "server.png");
		}
		if($arrGetUserRights["serverInfo"]) {
			$arrMenueSidebar[$i]["LINK"][] = array("TEXT" => "Apache-Manual", "LINK" => PAGE_APACHE_MANUAL, "ICON" => "apache.png", "TARGET" => "_blank");
		}
		if($arrGetUserRights["dbInfo"]) {
			$arrMenueSidebar[$i]["LINK"][] = array("TEXT" => "DB-Info", "LINK" => PAGE_DB_INFO, "ICON" => "database.png");
		}
		if($arrGetUserRights["editLogFiles"]) {
			$arrMenueSidebar[$i]["LINK"][] = array("TEXT" => "DB-Logdatei", "LINK" => PAGE_DB_LOG_FILES, "ICON" => "log.png");
		}
		if($arrGetUserRights["openPhpMyAdmin"]) {
			$arrMenueSidebar[$i]["LINK"][] = array("TEXT" => "DB-Mysql-Dumper", "LINK" => PATH_mysqli_DUMPER, "ICON" => "mysqlDumper.png", "TARGET" => "_blank");
		}
		if($arrGetUserRights["serverInfo"]) {
			$arrMenueSidebar[$i]["LINK"][] = array("TEXT" => "Shop-Provider", "LINK" => PATH_ONLINE_PROVIDER, "ICON" => "domain.png", "TARGET" => "_blank");
		}
		if($arrGetUserRights["openPhpMyAdmin"]) {
			$arrMenueSidebar[$i]["LINK"][] = array("TEXT" => "PhpMyAdmin (local)", "LINK" => PAGE_DISPLAY_PHPMYADMIN_LOCAL, "ICON" => "phpmyadmin.png", "TARGET" => "_blank");
			$arrMenueSidebar[$i]["LINK"][] = array("TEXT" => "PhpMyAdmin (extern)", "LINK" => PAGE_DISPLAY_PHPMYADMIN_EXTERN, "ICON" => "phpmyadmin.png", "TARGET" => "_blank");
		}

		if($arrGetUserRights["serverInfo"]) {
			$arrMenueSidebar[$i]["LINK"][] = array("TEXT" => "Verzeichnisbaum", "LINK" => PAGE_DISPLAY_TREESIZE, "ICON" => "sitemap.png");
			$arrMenueSidebar[$i]["LINK"][] = array("TEXT" => "File-Explorer", "LINK" => PAGE_DISPLAY_FILE_EXPLORER, "ICON" => "sitemap.png");
			$arrMenueSidebar[$i]["LINK"][] = array("TEXT" => "File-Manager", "LINK" => PAGE_DISPLAY_FILE_MANAGER, "ICON" => "sitemap.png");
		}

		if($arrGetUserRights["openPhpTerm"]) {
			$arrMenueSidebar[$i]["LINK"][] = array("TEXT" => "PhpTerm", "LINK" => PAGE_DISPLAY_PHPTERM, "ICON" => "phpterm.png");
		}
		if($arrGetUserRights["cleanAuftragslisten"]) {
			$arrMenueSidebar[$i]["LINK"][] = array("TEXT" => "Auftragslisten bereinigen", "LINK" => PAGE_CLEAN_SYSTEM, "ICON" => "clean.png");
		}

		if($arrGetUserRights["displayHelp"] || true) {
			$i++;
			$arrMenueSidebar[$i] = array("TEXT" => "Hilfe", "LINK" => PAGE_HELP, "ICON" => "help.png");
		}
	}

	if(!empty($arrMenueSidebar)) {
		?>
		<div id="menueSidebar">
			<div id="menueSidebarContent">
				<ul class="menueSidebarItems">
					<?php
						foreach($arrMenueSidebar as $thisMenueKey_Level1 => $thisMenueValue_Level1) {
							if(!is_array($thisMenueValue_Level1["LINK"])) {
								if($thisMenueValue_Level1["ICON"] != "") {
									$thisImageSrc = $thisMenueValue_Level1["ICON"];
								}
								else {
									$thisImageSrc = "spacer.gif";
								}
								if($thisMenueValue_Level1["TARGET"] == '' || !isset($thisMenueValue_Level1["TARGET"])) {
									$thisMenueValue_Level1["TARGET"] = '_top';
								}
								echo '<li>';
								if(SHOW_MENUE_SIDE_ICONS) {
									echo '<img src="' . PATH_ICONS_MENUE_SIDEBAR . $thisMenueValue_Level1["ICON"] . '" alt="" />';
								}
								echo '<a href="' . $thisMenueValue_Level1["LINK"] . '" ';
								if(preg_match("/" . urlencode($thisMenueValue_Level1["LINK"]) . "/", urlencode($_SERVER["REQUEST_URI"]))) {
									echo ' class="menueSidebarActive_Level1" ';
								}
								echo ' target="' . $thisMenueValue_Level1["TARGET"] . '" >' . $thisMenueValue_Level1["TEXT"] . '</a></li>';
							}
							else {
								if(!empty($thisMenueValue_Level1["LINK"])) {
									echo '<li id="menueSidebar_' . $thisMenueKey_Level1 . '" class="menueSidebarButton">';
									if(SHOW_MENUE_SIDE_ICONS) {
										echo '<img src="' . PATH_ICONS_MENUE_SIDEBAR . $thisMenueValue_Level1["ICON"] . '" alt="" />';
									}
									echo ''. $thisMenueValue_Level1["TEXT"] . '';
									echo '<ul>';
									foreach($thisMenueValue_Level1["LINK"] as $thisMenueKey_Level2 => $thisMenueValue_Level2) {
										if($thisMenueValue_Level2["TEXT"] != '{###SPACER###}' && $thisMenueValue_Level2["TEXT"] != '{###SEPARATOR###}') {
											if($thisMenueValue_Level2["ICON"] != "") {
												$thisImageSrc = $thisMenueValue_Level2["ICON"];
											}
											else {
												$thisImageSrc = "spacer.gif";
											}
											if($thisMenueValue_Level2["TARGET"] == '' || !isset($thisMenueValue_Level2["TARGET"])) {
												$thisMenueValue_Level2["TARGET"] = '_top';
											}
											echo '<li class="menueSidebar_Level2">';
											if(SHOW_MENUE_SIDE_ICONS) {
												echo '<img src="' . PATH_ICONS_MENUE_SIDEBAR . $thisMenueValue_Level2["ICON"] . '" alt="" width="16" height="16" />';
											}
											echo '<a href="' . $thisMenueValue_Level2["LINK"] . '"';
											if(preg_match("/" . urlencode($thisMenueValue_Level2["LINK"]) . "/", urlencode($_SERVER["REQUEST_URI"]))) {
												echo ' class="menueSidebarActive_Level2" ';
												$elementToggle = '#menueSidebar_' . $thisMenueKey_Level1;
											}
											echo ' target="' . $thisMenueValue_Level2["TARGET"] . '" >' . $thisMenueValue_Level2["TEXT"] . '</a></li>';
										}
										else if ( $thisMenueValue_Level2["TEXT"] == '{###SEPARATOR###}' ){
											echo '<li class="menueSidebarSeparator">' . '&nbsp;' . '</li>';
										}
										else {
											echo '<li class="menueSidebarSpacer">' . '&nbsp;' . '</li>';
										}
									}
									echo '</ul>';
									echo '</li>';
								}
							}
						}
						if($userDatas["usersLogin"] == 'thorsten'){
							#echo '<li><a href="displayOrderDocumentsNeuTest.php">TEST-Buchhaltung</a></li>';
						}
					?>
				</ul>
				<div id="searchBoxesArea">
					<?php
						if(empty($arrGetUserRights)){ $arrGetUserRights = readUserRights(); }
						echo '<p class="title">';
						if(SHOW_MENUE_SIDE_ICONS) {
							echo '<img src="' . PATH_ICONS_MENUE_SIDEBAR . 'search.png" alt="" />';
						}
						echo 'Suchen</p>';
					?>

					<?php if($arrGetUserRights["displayCustomers"] || $arrGetUserRights["editCustomers"]) { ?>
					<form name="formSearchSidebarCustomers" method="post" action="<?php echo PATH_SEARCH; ?>" >
						<div class="formElement">
							<div class="label"><label for="searchBoxCustomer">Kunde:</label></div>
							<div class="field"><input type="text" id="searchBoxCustomer" name="searchBoxCustomer" value="" class="inputField" /> <input type="submit" name="submitSearchSidebarCustomer" onclick="return checkFormDutyFields(this.form.name, [{'fieldName': 'searchBoxCustomer', 'fieldLabel': 'Kunde'}]);" class="inputButton" value="Los" title="Kunde suchen" /></div>
							<div class="clear"></div>
						</div>
					</form>
					<?php } ?>

					<?php if($arrGetUserRights["displayOrders"] || $arrGetUserRights["editOrders"]) { ?>
					<form name="formSearchSidebarProduction" method="post" action="<?php echo PATH_SEARCH; ?>" >
						<div class="formElement">
							<div class="label"><label for="searchBoxProduction">Produktion:</label></div>
							<div class="field"><input type="text" id="searchBoxProduction" name="searchBoxProduction" value="" class="inputField" /> <input type="submit" name="submitSearchSidebarProduction" onclick="return checkFormDutyFields(this.form.name, [{'fieldName': 'searchBoxProduction', 'fieldLabel': 'Produktion'}]);" class="inputButton" value="Los" title="Produktion suchen" /></div>
							<div class="clear"></div>
						</div>
					</form>
					<?php } ?>

					<?php if($arrGetUserRights["displayDocuments"]) { ?>
					<form name="formSearchSidebarFile" method="post" action="<?php echo PATH_SEARCH; ?>" >
						<div class="formElement">
							<div class="label"><label for="searchBoxFile">Datei:</label></div>
							<div class="field"><input type="text" id="searchBoxFile" name="searchBoxFile" value="" class="inputField" /> <input type="submit" name="submitSearchSidebarFile" onclick="return checkFormDutyFields(this.form.name, [{'fieldName': 'searchBoxFile', 'fieldLabel': 'Datei'}]);" class="inputButton" value="Los" title="Datei suchen" /></div>
							<div class="clear"></div>
						</div>
					</form>
					<?php } ?>

					<?php if($arrGetUserRights["displaySalesmen"] || $arrGetUserRights["editSalesmen"]) { ?>
					<form name="formSearchSidebarSalesmen" method="post" action="<?php echo PATH_SEARCH; ?>" >
						<div class="formElement">
							<div class="label"><label for="searchBoxSalesman">Vertreter:</label></div>
							<div class="field"><input type="text" id="searchBoxSalesman" name="searchBoxSalesman" value="" class="inputField" /> <input type="submit" name="submitSearchSidebarSalesman" onclick="return checkFormDutyFields(this.form.name, [{'fieldName': 'searchBoxSalesman', 'fieldLabel': 'Vertreter'}]);" class="inputButton" value="Los" title="Vertreter suchen" /></div>
							<div class="clear"></div>
						</div>
					</form>
					<?php } ?>

					<?php if($arrGetUserRights["displayCustomers"] || $arrGetUserRights["editCustomers"]) { ?>
					<form name="formSearchSidebarDeliveryTracking" method="post" action="<?php echo PATH_SEARCH; ?>" >
						<div class="formElement">
							<div class="label"><label for="searchBoxDeliveryTracking">Versand:</label></div>
							<div class="field"><input type="text" id="searchBoxDeliveryTracking" name="searchBoxDeliveryTracking" value="" class="inputField" /> <input type="submit" name="submitSearchSidebarCustomer" onclick="return checkFormDutyFields(this.form.name, [{'fieldName': 'searchBoxDeliveryTracking', 'fieldLabel': 'Tracking-Nr'}]);" class="inputButton" value="Los" title="Sendung suchen" /></div>
							<div class="clear"></div>
						</div>
					</form>
					<?php } ?>

					<?php if($arrGetUserRights["displayPersonnel"] || $arrGetUserRights["editPersonnel"]) { ?>
					<form name="formSearchSidebarPersonnel" method="post" action="<?php echo PATH_SEARCH; ?>" >
						<div class="formElement">
							<div class="label"><label for="searchBoxPersonnel">Mitarbeiter:</label></div>
							<div class="field"><input type="text" id="searchBoxPersonnel" name="searchBoxPersonnel" value="" class="inputField" /> <input type="submit" name="submitSearchSidebarPersonnel" onclick="return checkFormDutyFields(this.form.name, [{'fieldName': 'searchBoxPersonnel', 'fieldLabel': 'Mitarbeiter'}]);" class="inputButton" value="Los" title="Mitarbeiter suchen" /></div>
							<div class="clear"></div>
						</div>
					</form>
					<?php } ?>

					<?php if($arrGetUserRights["displayProducts"] || $arrGetUserRights["editProducts"]) { ?>
					<form name="formSearchSidebarProducts" method="post" action="<?php echo PATH_SEARCH; ?>" >
						<div class="formElement">
							<div class="label"><label for="searchBoxProduct">Artikel:</label></div>
							<div class="field"><input type="text" id="searchBoxProduct" name="searchBoxProduct" value="" class="inputField" /> <input type="submit" name="submitSearchSidebarProduct" onclick="return checkFormDutyFields(this.form.name, [{'fieldName': 'searchBoxProduct', 'fieldLabel': 'Artikel'}]);" class="inputButton" value="Los" title="Artikel suchen" /></div>
							<div class="clear"></div>
						</div>
					</form>
					<?php } ?>

					<?php if($arrGetUserRights["displaySuppliers"] || $arrGetUserRights["editSuppliers"]) { ?>
					<form name="formSearchSidebarSuppliers" method="post" action="<?php echo PATH_SEARCH; ?>" >
						<div class="formElement">
							<div class="label"><label for="searchBoxSupplier">Lieferant:</label></div>
							<div class="field"><input type="text" id="searchBoxSupplier" name="searchBoxSupplier" value="" class="inputField" /> <input type="submit" name="submitSearchSidebarSupplier" onclick="return checkFormDutyFields(this.form.name, [{'fieldName': 'searchBoxSupplier', 'fieldLabel': 'Artikel'}]);" class="inputButton" value="Los" title="Lieferanten/Dienstleister suchen" /></div>
							<div class="clear"></div>
						</div>
					</form>
					<?php } ?>

				</div>
			</div>
		</div>
		<script language="javascript" type="text/javascript">
			$(document).ready(function() {
				var animateTime = 0;
				// $('.menueSidebarButton ul').toggle();
				$('.menueSidebarActive_Level2').parent().parent('ul').toggle();
				$('.menueSidebarButton').click(function(){
					$('.menueSidebarButton ul').hide(animateTime);
					$(this).find('ul').toggle(animateTime);
				});
			});
		</script>
<?php
	}
	else if(!isset($_SESSION["usersID"])){
?>
	<div id="menueSidebar">
		<div id="menueSidebarContent">
			<ul class="menueSidebarItems">
				<li>
					<?php
						if(SHOW_MENUE_SIDE_ICONS) { echo '<img src="' . PATH_ICONS_MENUE_SIDEBAR . 'shop.png" alt="" />'; }
						echo '<a href="' . PATH_ONLINE_SHOP_B3 . '" target="_blank" >B3 Online-Shop</a>';
					?>
				</li>
				<li>
					<?php
						if(SHOW_MENUE_SIDE_ICONS) { echo '<img src="' . PATH_ICONS_MENUE_SIDEBAR . 'shop.png" alt="" />'; }
						echo '<a href="' . PATH_ONLINE_SHOP_BCTR . '" target="_blank" >B-CTR Online-Shop</a>';
					?>
				</li>
				<li>
					<?php
						if(SHOW_MENUE_SIDE_ICONS) { echo '<img src="' . PATH_ICONS_MENUE_SIDEBAR . 'amazon.png" alt="" />'; }
						echo '<a href="' . PAGE_SHOP_AMAZON . '" target="_blank" >Amazon-Shop</a>';
					?>
				</li>
				<li>
					<?php
						if(SHOW_MENUE_SIDE_ICONS) { echo '<img src="' . PATH_ICONS_MENUE_SIDEBAR . 'mail.png" alt="" />'; }
						echo '<a href="' . PAGE_MAIL_ACCOUNT_HOSTER_4 . '" target="_blank" >E-Mails</a>';
					?>
				</li>
				<li>&nbsp;</li>
			</ul>
		</div>
	</div>
<?php
	}
?>

