<?php
	if(!empty($arrGetUserRights) && (isset($_SESSION["usersID"]) && $_SESSION["usersID"]) != "" || (isset($_COOKIE["usersID"]) && $_COOKIE["usersID"] != ""))
	{
		$arrMenueTop[] = array("LINK" => PAGE_QUICK_LINKS, "TITLE" => "Zur Startseite", "TEXT" => "Startseite", "TARGET" => "", "ICON" => "home.png");

		if($arrGetUserRights["displayOrdersOverview"] || $arrGetUserRights["editOrders"] || $arrGetUserRights["displayOrders"]) {
			$arrMenueTop[] = array("LINK" => PAGE_DISPLAY_ORDERS_OVERVIEW, "TITLE" => "&Uuml;bersicht aller Produktions-Prozesse ansehen", "TEXT" => "Produktionen", "TARGET" => "", "ICON" => "ordersoverview.png");
		}

		/*
		if($arrGetUserRights["exportOrders"]) {
			$arrMenueTop[] = array("LINK" => PAGE_EXPORT_ORDERS, "TITLE" => "Bestellungen f&uuml;r Vertreter exportieren", "TEXT" => "Exportieren", "TARGET" => "", "ICON" => "");
		}
		*/

		if($arrGetUserRights["editOrders"] || $arrGetUserRights["displayOrders"]) {
			$arrMenueTop[] = array("LINK" => PAGE_EDIT_PROCESS . "?editID=NEW", "TITLE" => "Neue Produktion anlegen", "TEXT" => "Neue Produktion", "TARGET" => "", "ICON" => "addProcess.png");
		}

		if($arrGetUserRights["editCustomers"] || $arrGetUserRights["displayCustomers"]) {
			// $arrMenueTop[] = array("LINK" => PAGE_EDIT_CUSTOMER, "TITLE" => "Kunden eintragen oder editieren", "TEXT" => "Kunden", "TARGET" => "", "ICON" => "customers.png");
		}

		if($arrGetUserRights["editCustomers"] || $arrGetUserRights["displayCustomers"]) {
			$arrMenueTop[] = array("LINK" => PAGE_EDIT_CUSTOMER . "?editID=NEW", "TITLE" => "Neukunden anlegen", "TEXT" => "Neuer Kunde", "TARGET" => "", "ICON" => "addCustomer.png");
		}

		#if($arrGetUserRights["displayTracking"]) {
			#$arrMenueTop[] = array("LINK" => PAGE_SEARCH_TRACKING, "TITLE" => "Lieferung verfolgen", "TEXT" => "Sendungsverfolgung", "ICON" => "tracking.png");
		#}

		if($arrGetUserRights["displayDocuments"]) {
			$arrMenueTop[] = array("LINK" => PAGE_DOWNLOAD_CATALOGUES, "TITLE" => "Preislisten anzeigen", "TEXT" => "Kataloge / Preislisten", "ICON" => "catalogue.png");
		}

		if($arrGetUserRights["displayTracking"]) {
			$arrMenueTop[] = array("LINK" => PAGE_DELIVERY_HISTORY, "TITLE" => "Lieferung verfolgen", "TEXT" => "Paket-Historie", "ICON" => "tracking.png");
		}

		/*
		if($arrGetUserRights["displayOnlineAcquisition"]) {
			$arrMenueTop[] = array("LINK" => PAGE_DISPLAY_CUSTOMERS_ONLINE; "  PAGE_DISPLAY_CUSTOMERS_ONLINE."/",))   ", "TITLE" => "", "TEXT" => "Online-Kundenerfassung", "TARGET" => "", "ICON" => "");
		}
		*/

		if($arrGetUserRights["editProducts"] || $arrGetUserRights["displayProducts"]) {
			// $arrMenueTop[] = array("LINK" => PAGE_DISPLAY_PRODUCTS, "TITLE" => "Artikel ansehen", "TEXT" => "Artikel", "TARGET" => "", "ICON" => "products.png");
		}

		/*
		if($arrGetUserRights["editOffers"] || $arrGetUserRights["displayOffers"]) {
			$arrMenueTop[] = array("LINK" => PAGE_DISPLAY_OFFER, "TITLE" => "Angebote ansehen oder umwandeln", "TEXT" => "Angebote", "TARGET" => "", "ICON" => "create.png");
		}

		if($arrGetUserRights["editConfirmations"] || $arrGetUserRights["displayConfirmations"]) {
			$arrMenueTop[] = array("LINK" => PAGE_DISPLAY_CONFIRMATION, "TITLE" => "Auftragsbest&auml;tigungen ansehen oder umwandeln", "TEXT" => "Auftragsbest&auml;tigungen", "TARGET" => "", "ICON" => "create.png");
		}

		if($arrGetUserRights["editInvoices"] || $arrGetUserRights["displayInvoices"]) {
			$arrMenueTop[] = array("LINK" => PAGE_DISPLAY_INVOICE, "TITLE" => "Rechnungen ansehen oder umwandeln", "TEXT" => "Rechnungen", "TARGET" => "", "ICON" => "create.png");
		}
		*/

		if($arrGetUserRights["editLetters"] || $arrGetUserRights["displayLetters"]) {
			// $arrMenueTop[] = array("LINK" => PAGE_DISPLAY_LETTER, "TITLE" => "Brief an Kunden erstellen", "TEXT" => "Brief", "TARGET" => "", "ICON" => "create.png");
		}

		if($arrGetUserRights["displayOrderReceipts"]) {
			$arrMenueTop[] = array("LINK" => PAGE_DISPLAY_ORDERS_RECEIPTS, "TITLE" => "Bestelleing&auml;nge ansehen", "TEXT" => "Statistik", "TARGET" => "", "ICON" => "statistics.png");
		}

		$arrMenueTop[] = array("LINK" => PAGE_SALESMEN_FLASH_MAP, "TITLE" => "PLZ-Karte", "TEXT" => "PLZ-Karte", "TARGET" => "", "ICON" => "iconMapPLZ.png");
		if($arrGetUserRights["editCalendar"] || $arrGetUserRights["displayCalendar"]) {
			$arrMenueTop[] = array("LINK" => PAGE_DISPLAY_CALENDAR, "TITLE" => "Personal-Kalender", "TEXT" => "Kalender", "TARGET" => "", "ICON" => "calendar.png");
		}

		if($arrGetUserRights["editUser"]) {
			$arrMenueTop[] = array("LINK" => PAGE_EDIT_USER, "TITLE" => "Auftragslisten-Benutzerverwaltung", "TEXT" => "Benutzer", "TARGET" => "", "ICON" => "personnel.png");
		}

		if($arrGetUserRights["createTotalBackUp"]) {
			// $arrMenueTop[] = array("LINK" => PAGE_CREATE_BACKUP, "TITLE" => "Backups verwalten", "TEXT" => "Backup", "TARGET" => "", "ICON" => "export.png");
		}
		if($arrGetUserRights["adminArea"]) {
			$arrMenueTop[] = array("LINK" => 'test.php', "TITLE" => "TEST-SKRIPT", "TEXT" => "TEST", "TARGET" => "_blank", "ICON" => "export.png");
		}
	}


	if($_SESSION["usersID"] != '') {
		$arrMenueTop[] = array("LINK" => constant("PATH_ONLINE_SHOP_" . strtoupper(MANDATOR)), "TITLE" => "Zum " . COMPANY_NAME_SHORT . " Online-Shop", "TEXT" => strtoupper(MANDATOR) . " SHOP", "TARGET" => "_blank", "ICON" => "shop.png");
		$arrMenueTop[] = array("LINK" => PAGE_INDEX . "?mode=logoutUser", "TITLE" => "Aus der Verwaltung ausloggen", "TEXT" => "Logout", "TARGET" => "", "ICON" => "logout.png", "STYLE" => "color:#FF0000; font-weight:bold;");

	}
?>

<div id="menueTopArea">
	<div id="menueTopContent">
		<?php
			if(!empty($arrMenueTop)) {
				echo '<ul class="menueTopItems">';
				foreach($arrMenueTop as $thisKey => $thisValue) {
					if($thisValue["ICON"] != "") {
						$thisImageSrc = $thisValue["ICON"];
					}
					else {
						$thisImageSrc = "spacer.gif";
					}
					if($thisValue["TARGET"] == '') {
						$thisValue["TARGET"] == '_top';
						//<img src="layout/menueIcons/' . $thisValue["ICON"] . '" alt="" />
					}
					echo '<li>';
					echo '<a href="' . $thisValue["LINK"] . '" ';

					if(preg_match("/" . urlencode($thisValue["LINK"]) . "/",  urlencode($_SERVER["REQUEST_URI"]))) {
						echo ' class="menueTopActive" ';
					}
					if($thisValue["TARGET"] == '' || !isset($thisValue["TARGET"])) {
						$thisValue["TARGET"] = '_top';
					}
					echo 'title="' . $thisValue["TITLE"] . '" target="' . $thisValue["TARGET"] . '" style="' . $thisValue["STYLE"] . '" >';
					if(SHOW_MENUE_TOP_ICONS) {
						echo  '<img src="' . PATH_ICONS_MENUE_SIDEBAR . $thisValue["ICON"] . '" alt="" width="11" height="11" /> ';
					}
					echo ($thisValue["TEXT"]) . '</a></li>';
				}
				echo '</ul>';
			}
			else{
				echo '<li>&nbsp;</li>';
			}
		?>
		<div class="clear"></div>
	</div>
</div>