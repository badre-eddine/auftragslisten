<?php
	$menueSliderWidth = '100%';

	$content = '';
	$contentForm = '';

	$menueSliderStyle = '';
	if(1){
		#$menueSliderStyle = 'style="height:30px;"';
		$contentForm .= '<div id="searchSlidebarBoxesArea">';


		/*
		$contentForm .= '<span class="title">';
			if(SHOW_MENUE_SIDE_ICONS) {
				$contentForm .= '<img src="' . PATH_ICONS_MENUE_SIDEBAR . 'search.png" alt="" />';
			}
		$contentForm .= 'Suchen</span>';
		*/

		if($arrGetUserRights["displayCustomers"] || $arrGetUserRights["editCustomers"]) {
			$contentForm .= '
				<form name="formSearchSlidebarCustomers" method="post" action="' . PATH_SEARCH . '" >
					<div class="formElement">
						<div class="label"><label for="searchBoxCustomer">Kunde:</label></div>
						<div class="field"><input type="text" id="searchBoxCustomer" name="searchBoxCustomer" value="" class="inputField" /> <input type="submit" name="submitSearchSidebarCustomer"  class="inputButton" value="Los" title="Kunde suchen" /></div>
					</div>
				</form>
			';
		}

		if($arrGetUserRights["displayOrders"] || $arrGetUserRights["editOrders"]) {
			$contentForm .= '
				<form name="formSearchSlidebarProduction" method="post" action="' . PATH_SEARCH . '" >
					<div class="formElement">
						<div class="label"><label for="searchBoxProduction">Produktion:</label></div>
						<div class="field"><input type="text" id="searchBoxProduction" name="searchBoxProduction" value="" class="inputField" /> <input type="submit" name="submitSearchSidebarProduction"  class="inputButton" value="Los" title="Produktion suchen" /></div>
					</div>
				</form>
			';
		}

		if($arrGetUserRights["displayDocuments"]) {
			$contentForm .= '
				<form name="formSearchSlidebarFile" method="post" action="' . PATH_SEARCH . '" >
					<div class="formElement">
						<div class="label"><label for="searchBoxFile">Datei:</label></div>
						<div class="field"><input type="text" id="searchBoxFile" name="searchBoxFile" value="" class="inputField" /> <input type="submit" name="submitSearchSidebarFile"  class="inputButton" value="Los" title="Datei suchen" /></div>
					</div>
				</form>
			';
		}

		/*
		if($arrGetUserRights["displaySalesmen"] || $arrGetUserRights["editSalesmen"]) {
			$contentForm .= '
				<form name="formSearchSlidebarSalesmen" method="post" action="' . PATH_SEARCH . '" >
					<div class="formElement">
						<div class="label"><label for="searchBoxSalesman">Vertreter:</label></div>
						<div class="field"><input type="text" id="searchBoxSalesman" name="searchBoxSalesman" value="" class="inputField" /> <input type="submit" name="submitSearchSidebarSalesman"  class="inputButton" value="Los" title="Vertreter suchen" /></div>
					</div>
				</form>
			';
		}
		*/

		if($arrGetUserRights["displayCustomers"] || $arrGetUserRights["editCustomers"]) {
			$contentForm .= '
				<form name="formSearchSlidebarDeliveryTracking" method="post" action="' . PATH_SEARCH . '" >
					<div class="formElement">
						<div class="label"><label for="searchBoxDeliveryTracking">Versand:</label></div>
						<div class="field"><input type="text" id="searchBoxDeliveryTracking" name="searchBoxDeliveryTracking" value="" class="inputField" /> <input type="submit" name="submitSearchSidebarCustomer"  class="inputButton" value="Los" title="Sendung suchen" /></div>
					</div>
				</form>
			';
		}

		/*
		if($arrGetUserRights["displayPersonnel"] || $arrGetUserRights["editPersonnel"]) {
			$contentForm .= '
				<form name="formSearchSlidebarPersonnel" method="post" action="' . PATH_SEARCH . '" >
					<div class="formElement">
						<div class="label"><label for="searchBoxPersonnel">Mitarbeiter:</label></div>
						<div class="field"><input type="text" id="searchBoxPersonnel" name="searchBoxPersonnel" value="" class="inputField" /> <input type="submit" name="submitSearchSidebarPersonnel"  class="inputButton" value="Los" title="Mitarbeiter suchen" /></div>
					</div>
				</form>
			';
		}
		*/

		/*
		if($arrGetUserRights["displayProducts"] || $arrGetUserRights["editProducts"]) {
			$contentForm .= '
				<form name="formSearchSlidebarProducts" method="post" action="' . PATH_SEARCH . '" >
					<div class="formElement">
						<div class="label"><label for="searchBoxProduct">Artikel:</label></div>
						<div class="field"><input type="text" id="searchBoxProduct" name="searchBoxProduct" value="" class="inputField" /> <input type="submit" name="submitSearchSidebarProduct"  class="inputButton" value="Los" title="Artikel suchen" /></div>
					</div>
				</form>
			';
		}
		*/

		/*
		if($arrGetUserRights["displaySuppliers"] || $arrGetUserRights["editSuppliers"]) {
			$contentForm .= '
				<form name="formSearchSlidebarSuppliers" method="post" action="' . PATH_SEARCH . '" >
					<div class="formElement">
						<div class="label"><label for="searchBoxSupplier">Lieferant:</label></div>
						<div class="field"><input type="text" id="searchBoxSupplier" name="searchBoxSupplier" value="" class="inputField" /> <input type="submit" name="submitSearchSidebarSupplier"  class="inputButton" value="Los" title="Lieferanten/Dienstleister suchen" /></div>
					</div>
				</form>
			';
		}
		*/

		$contentForm .= '</div>';
		$contentForm = preg_replace("/[\t\n\r]{1,}/", "", $contentForm);
		#dd('contentForm');
		$contentForm = preg_replace("/\'/", "'", $contentForm);
		$contentForm = preg_replace("/\//", "\/", $contentForm);
		#dd('contentForm');
	}

	$content .= '
		<div id="menueSlide" ' . $menueSliderStyle . '>
			<div id="menueSlideContent">
				<span id="buttonMenueSlider" style="float:left;"><img src="layout/icons/iconOpen.gif" width="10" height="10" alt="" title="" /><\/span>

				' . $contentForm . '

				<a href="' . PAGE_QUICK_LINKS . '" title="&uarr; Zur Startseite"><span><img src="layout/menueIcons/menueSidebar/home.png" width="9" height="11" alt="&larr; zur Startseite" /><\/span>zur Startseite<\/a>
				<a href="#topPage" title="&uarr; Zum Seitenanfang"><span><img src="layout/icons/iconGoUp.png" width="9" height="11" alt="&uarr; nach oben" /><\/span>zum Seitenanfang<\/a>
				<a href="#bottomPage" title="&darr; Zum Seitenende"><span><img src="layout/icons/iconGoDown.png" width="9" height="11" alt="&darr; nach oben" /><\/span>zum Seitenende<\/a>
				<a href="javascript:window.print()" title="Seite drucken"><span><img src="layout/icons/printer.png" width="11" height="11" alt="Seite drucken" /><\/span>Seite drucken<\/a>
			<\/div>
		<\/div>
	';

	$content = preg_replace("/[\t\n\r]{1,}/", "", $content);
	#echo ($content);

	#echo $contentForm;

?>
<script language="javascript" type="text/javascript">
	<!--
	/* <![CDATA[ */
	$(document).ready(function() {
		$('#mainArea').css('position', 'relative');
		$('body').append('<?php echo ($content); ?>');
		$('body').prepend('<a name="topPage"><\/a>');
		$('body').append('<a name="bottomPage"><\/a>');

		var documentAreaWidth = $('#documentArea').css('width');

		var windowWidth = $(window).width();
		var documentWidth = $(document).width();

		var menueSliderMode = '0';
		var menueSliderButtonImage = '';
		var menueSliderButtonTitle = 'Slide-Menü öffnen';
		var menueSliderWidth = (parseInt(documentAreaWidth) - 5);
		var menueSliderPositionRight = (50 - windowWidth);
		var menueSliderPositionRightNew;

		var menueSliderPositionHeight = $('#menueSlide').height();

		var menueSliderPositionHeightNew;
		var menueSliderPositionNew = 0;

		var documentMarginBottom = 0;
		var documentMarginBottomNew = 0;

		$('#menueSlide').css('width', menueSliderWidth + 'px');
		$('#menueSlide').css('right', menueSliderPositionRight + 'px');
		$('#buttonMenueSlider img').attr('title', menueSliderButtonTitle);

		function toggleMenueSlider(){
			if(menueSliderMode == '0'){
				menueSliderButtonImage = 'layout/icons/iconClose.gif';
				menueSliderPositionRightNew	= parseInt((windowWidth - parseInt(documentAreaWidth)) / 2);
				menueSliderPositionHeightNew = $('#menueSlide').outerHeight() + 10;
				menueSliderButtonTitle = 'Slide-Menü schließen';
				documentMarginBottomNew = menueSliderPositionHeightNew + 50;
			}
			else {
				menueSliderButtonImage = 'layout/icons/iconOpen.gif';
				menueSliderPositionRightNew	= menueSliderPositionRight;
				menueSliderPositionHeightNew = menueSliderPositionHeight;
				menueSliderButtonTitle = 'Slide-Menü öffnen';
				documentMarginBottomNew = documentMarginBottom;
			}

			$('body').css('margin-bottom', documentMarginBottomNew + 'px');
			$('#menueSlide').animate({
				right: menueSliderPositionRightNew + 'px',
				height: menueSliderPositionHeightNew + 'px'
			});
			$('#buttonMenueSlider img').attr('src', menueSliderButtonImage);
			$('#buttonMenueSlider img').attr('title', menueSliderButtonTitle);
			$('#buttonMenueSlider img').attr('alt', menueSliderButtonTitle);
		}

		$('#buttonMenueSlider').live('click', function(){
			if(menueSliderMode == '0'){
				menueSliderMode = '1';
			}
			else {
				menueSliderMode = '0';
			}
			toggleMenueSlider();
			// BOF SET COOKIE
				cookieName = 'displayMenueSlider';
				var  cookieExpires = new Date();
				cookieExpires = new Date(cookieExpires.getTime() + 1000 * 60 * 60 * 24 * 365);
				document.cookie = cookieName + '=' + menueSliderMode + '; expires=' + cookieExpires + '; path=/; ';
			// EOF SET COOKIE
		});

		<?php if(isset($_COOKIE["displayMenueSlider"])){ ?>
			menueSliderMode = '<?php echo $_COOKIE["displayMenueSlider"]; ?>';
			toggleMenueSlider();
		<?php } ?>
	});
	/* ]]> */
	-->
</script>