<?php
	// $url = $_SERVER["PHP_SELF"];
	$url = $_SERVER["REDIRECT_URL"];

	#$pageSeperator = "&";
	$pageSeperator = "&amp;";

	if(!empty($_REQUEST)) {
		$arrUrlAddParams = array();
		foreach($_REQUEST as $thisKey => $thisValue){
			if($thisKey != "PHPSESSID" && $thisKey != "page" && $thisKey != "submitSearch" && $thisKey != "changeStatus" && $thisValue != "") {
				$thisVarType = gettype($_REQUEST[$thisKey]);								
				if(is_array($_REQUEST[$thisKey]) && $thisVarType == 'array'){
					foreach($_REQUEST[$thisKey] as $thisArrayKey => $thisArrayValue){
						$arrUrlAddParams[] = $thisKey . "[" . $thisArrayKey . "]" . "=" . $thisArrayValue;
					}
				}
				else {
					$arrUrlAddParams[] = $thisKey . "=" . $thisValue;				
				}
			}
		}
		$strUrlAddParams = implode($pageSeperator, $arrUrlAddParams);
	}
	if($strUrlAddParams != "") {
		$url .= "?" . $strUrlAddParams;
	}
	else {
		$pageSeperator = "?";
	}

	// --- current page
	if (!empty($_REQUEST["page"])) {
		$current_page = $_REQUEST["page"];
		if ($current_page < 1) { $current_page = 1; }
		if ($current_page > $pagesCount) { $current_page = $pagesCount; }
	}
	else {
		$current_page = 1;
	}

	// --- create menuePages

	/*
	for ($s=1; $s <= $pagesCount; $s++)	{
		if($s < ($current_page + MAX_PAGES_IN_MENUE / 2) || $s > ($pagesCount - MAX_PAGES_IN_MENUE / 2)){
			if (!empty($side_bar)) $side_bar .= '';
			if ($s == $current_page) $side_bar .= ' <span class="currentPage">' . $s . '</span> ';
			else $side_bar .= ' <span><a href="' . $url . $pageSeperator . 'page=' . $s . '" title="Seite ' . $s . ' anzeigen" >' . $s . '</a></span> ';
		}
		else if ($s == ($current_page + MAX_PAGES_IN_MENUE / 2)){
			$side_bar .= ' <span class="menuePagesFiller">...</span> ';
		}
	}
	*/

	$startMenueFirstPages = $current_page - 1 * (MAX_PAGES_IN_MENUE / 4) + 1;
	$endMenueFirstPages = $current_page + 1 * (MAX_PAGES_IN_MENUE / 4) - 1;

	if($startMenueFirstPages < 1) {
		$startMenueFirstPages = 1;
		$endMenueFirstPages = 2 * (MAX_PAGES_IN_MENUE / 4);
	}
	else if($endMenueFirstPages >= $pagesCount) {
		$startMenueFirstPages = $pagesCount - 2 * (MAX_PAGES_IN_MENUE / 4) + 1;
		$endMenueFirstPages = $pagesCount;
	}
	if($endMenueFirstPages > $pagesCount) {
		$endMenueFirstPages = $pagesCount;
	}
	if($startMenueFirstPages < 1) {
		$startMenueFirstPages = 1;
	}

	for ($s=$startMenueFirstPages; $s <= $endMenueFirstPages; $s++)	{
		if ($s == $current_page) $side_bar .= ' <span class="currentPage">' . $s . '</span> ';
		else $side_bar .= ' <span><a href="' . $url . $pageSeperator . 'page=' . $s . '" title="Seite ' . $s . ' anzeigen" >' . $s . '</a></span> ';
	}
	if($current_page < $pagesCount && $pagesCount > MAX_PAGES_IN_MENUE) {
		$side_bar .= ' <span class="menuePagesFiller"><b>. . .</b></span> ';
	}

	$side_current = 'Seite ' . $current_page . ' von <i>' . $pagesCount . '</i> Seiten';

	if ($current_page > 1) $prev = '<span> <a href="' . $url . $pageSeperator . 'page=' . ($current_page - 1) . '" title="Seite ' . ($current_page - 1) . ' anzeigen"> &lt; zur&uuml;ck </a></span>';
	if ($current_page < $pagesCount) $next = '<span> <a href="' . $url . $pageSeperator . 'page=' . ($current_page + 1) . '" title="' . strip_tags($displayPageTitle) . 'Seite ' . ($current_page + 1) . ' anzeigen" > weiter &gt; </a></span>';

	$start = '<span> <a href="' . $url . $pageSeperator . 'page=1" title="Zur ersten Seite" > &lt;&lt; Anfang </a></span>';
	$end = '<span> <a href="' . $url . $pageSeperator . 'page=' . $pagesCount . '" title="Zur letzten Seite" > Ende &gt;&gt; </a></span>';
	
	$side_switch .= '';
	for($p = 1 ; $p < ($pagesCount + 1) ; $p++){
		$side_switch .= ' <span><a href="' . $url . $pageSeperator . 'page=' . $p . '" title="Seite ' . $p . ' anzeigen" >' . $p . '</a></span><br />';
	}
?>

<div class="menuePagesArea">
	<table width="100%" border="0" cellpadding="0" cellspacing="0">
		<tr>
			<td class="menuePagesStart"><?php echo $start; ?></td>
			<td class="menuePagesPrev"><?php echo $prev; ?></td>
			<td class="menuePagesSelect"><?php echo $side_bar; ?></td>
			<?php if($pagesCount > 1) { ?>
			<td class="menuePagesBox">gehe zu Seite: <div><?php echo $side_switch; ?></div></td>
			<?php } ?>
			<td class="menuePagesNext"><?php echo $next; ?></td>
			<td class="menuePagesEnd"><?php echo $end; ?></td>
			<td class="menuePagesCurrent"><?php echo $side_current; ?></td>
		</tr>
	</table>
</div>

<?php
   $side_bar = "";
?>


