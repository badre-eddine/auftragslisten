<?php
	require_once('inc/requires.inc.php');

	if(!$arrGetUserRights["editStocks"] && !$arrGetUserRights["displayStocks"]) {
		header('location: ' . PAGE_EXIT_LOCATION);
		exit;
	}

	$jswindowMessage = "";
	$warningMessage = '';
	$errorMessage = '';
	$successMessage = '';
	$infoMessage = '';

	$dbConnection = new DB_Connection();
	$db_open = $dbConnection->db_connect();

	// BOF READ ORDER CATEGORIES
		$arrOrderCategoriesTypeDatas = getOrderCategories();
	// EOF READ ORDER CATEGORIES

	// BOF STORE STOCK
	if($_POST["storeNewStock"] != "") {
		$sql = "INSERT INTO `" . TABLE_STOCK_ITEMS . "` (
									`stockItemsID`,
									`stockItemsDate`,
									`stockItemsQuantity`,
									`stockItemsCategoryID`
								)
								VALUES (
									'%',
									'".formatDate($_POST["newStockDate"], 'store')."',
									'".$_POST["newStockQuantity"]."',
									'".$_POST["newStockCategory"]."'
								)
		";
		$rs = $dbConnection->db_query($sql);
	}
	// EOF STORE STOCK


	// BOF GET CONSUMPTION
	$sql = "
			SELECT
					DATE_FORMAT(`ordersBestellDatum`, '%Y') AS `ordersBestellJahr`,
					DATE_FORMAT(`ordersBestellDatum`, '%u') AS `ordersBestellWoche`,
					`ordersArtikelKategorieID` AS `ordersCategoryID`,

					`ordersBestellDatum`,
					SUM(`ordersArtikelMenge`) AS `ordersProductMenge`,
					CONCAT(DATE_FORMAT(`ordersBestellDatum`, '%Y'), '-', DATE_FORMAT(`ordersBestellDatum`, '%u')) AS 'ordersWeek'

				FROM `" . TABLE_ORDERS . "`
				WHERE `ordersBestellDatum` != '0000-00-00'
					AND `ordersUseNoStock` != '1'
						AND (`ordersStatus` = '4' OR `ordersStatus` = '5')

				GROUP BY CONCAT(`ordersBestellJahr`, '-', `ordersBestellWoche`, ';', `ordersArtikelKategorieID`)

			UNION

			SELECT
					DATE_FORMAT(`ordersBestellDatum`, '%Y') AS `ordersBestellJahr`,
					DATE_FORMAT(`ordersBestellDatum`, '%u') AS `ordersBestellWoche`,
					`ordersAdditionalArtikelKategorieID` AS `ordersCategoryID`,

					`ordersBestellDatum`,
					SUM(`ordersAdditionalArtikelMenge`) AS `ordersProductMenge`,
					CONCAT(DATE_FORMAT(`ordersBestellDatum`, '%Y'), '-', DATE_FORMAT(`ordersBestellDatum`, '%u')) AS 'ordersWeek'

				FROM `" . TABLE_ORDERS . "`
				WHERE `ordersBestellDatum` != '0000-00-00'
					AND `ordersUseNoStock` != '1'
						AND (`ordersStatus` = '4' OR `ordersStatus` = '5')

				GROUP BY CONCAT(`ordersBestellJahr`, '-', `ordersBestellWoche`, ';', `ordersAdditionalArtikelKategorieID`)

				ORDER BY `ordersBestellJahr` DESC, `ordersBestellWoche` DESC
	";


	$rs = $dbConnection->db_query($sql);
	while($ds = mysqli_fetch_assoc($rs)) {
		foreach(array_keys($ds) as $field) {
			$arrConsumptionDatas[$ds["ordersCategoryID"]][$ds["ordersWeek"]][$field] = $ds[$field];
		}
		$arrConsumptionQuantity[$ds["ordersCategoryID"]] += $ds["ordersProductMenge"];
	}

	// EOF GET CONSUMPTION

	// BOF GET STOCK
	$sql = "SELECT
				`stockItemsID`,
				`stockItemsDate`,
				`stockItemsQuantity`,
				`stockItemsCategoryID`

				FROM `" . TABLE_STOCK_ITEMS . "`
				ORDER BY `stockItemsCategoryID`, `stockItemsDate` DESC
	";
	$rs = $dbConnection->db_query($sql);
	while($ds = mysqli_fetch_assoc($rs)) {
		foreach(array_keys($ds) as $field) {
			$arrStockDatas[$ds["stockItemsCategoryID"]][$ds["stockItemsID"]][$field] = $ds[$field];
		}
		$arrStockQuantity[$ds["stockItemsCategoryID"]] += $ds["stockItemsQuantity"];
	}
	// BOF GET STOCK
?>
<?php
	require_once('inc/headerHTML.inc.php');
	$thisTitle = "Statistik";
	$headerHTML = preg_replace("/{###TITLE###}/", strip_tags($thisTitle), $headerHTML);
	echo $headerHTML;

	require_once(FILE_MENUE_TOP);
?>
<div id="mainArea">
	<div id="mainContent">
		<?php require_once(FILE_MENUE_SIDEBAR); ?>
		<div id="contentArea">
			<div id="contentAreaElements">
				<h1><?php if(SHOW_PAGE_TITLE_ICONS) { echo '<img src="' . PATH_ICONS_MENUE_TITLES . 'statistics.png" alt="" />'; } ?> <?php echo $thisTitle; ?></h1>
				<!--
				<div id="displayStatusCountArea">
					<span class="displayStatusCount"><b><a href="displayOrders.php" title="Alle Vorg&auml;nge anzeigen">Insgesamt</a>:</b> <?php echo $countStatusTypesTotal; ?></span>
					<?php
						if(!empty($arrCountStatusTypes)) {
							foreach($arrCountStatusTypes as $thisKey => $thisValue) {
								echo ' &bull; <span class="displayStatusCount"><b><a href="?displayOrderStatus='.$thisValue["orderStatusTypesID"].'" title="'.$thisValue["orderStatusTypesName"].'e Vorg&auml;nge anzeigen">'.$thisValue["orderStatusTypesName"].'</a>:</b> '.$thisValue["ordersStatusCount"].'</span>';
							}
						}
					?>
				</div>
				-->
				<!--
				<div id="searchFilterArea">
					<form name="formFilterSearch" method="post" action="<?php echo $_SERVER["PHP_SELF"]; ?>">
					<table border="0" cellpadding="0" cellspacing="0" width="100%" class="searchFilterContent">
						<tr>
							<td>
								<label for="searchCustomerNumber">Kundennummer:</label>
								<input type="text" name="searchCustomerNumber" id="searchCustomerNumber" class="inputField_70" value="" />
							</td>
							<td>
								<label for="searchCustomerName">Kundenname:</label>
								<input type="text" name="searchCustomerName" id="searchCustomerName" class="inputField_160" value="" />
							</td>
							<td>
								<label for="searchPLZ">PLZ:</label>
								<input type="text" name="searchPLZ" id="searchPLZ" class="inputField_40" />
							</td>
							<td>
								<label for="searchWord">Suchbegriff:</label>
								<input type="text" name="searchWord" id="searchWord" class="inputField_70" />
							</td>
							<td>
								<input type="hidden" name="editID" id="editID" value="" />
								<input type="submit" name="submitSearch" class="inputButton0" value="Suchen" />
							</td>

							<td>
								<a href="editOrder.php?editID=NEW" class="linkButton">Neuer Vorgang</a>
							</td>
						</tr>
					</table>
					</form>
				</div>
				-->

				<div class="contentDisplay">
					<?php displayMessages(); ?>
					<?php
						if($arrGetUserRights["editStocks"] == '1')
						{
					?>
					<div class="boxArea1">
						<h2>Neue Lieferungen eintragen</h2>
						<div>
							<div class="boxAreaContent">
								<form name="formEditStock" action="<?php echo $_SERVER["PHP_SELF"]; ?>" method="post">
									<table border="0" cellpadding="0" cellspacing="0">
										<tr>
											<td><label for="newStockCategory">Artikel:</label></td>
											<td>
												<select name="newStockCategory" id="newStockCategory" class="inputSelect_120">
													<option value="0"></option>
													<?php
														if(!empty($arrOrderCategoriesTypeDatas)) {
															foreach($arrOrderCategoriesTypeDatas as $thisKey => $thisValue){
																$thisDistance = "";
																if(strlen($thisKey) > 3) { $thisDistance = " &bull; "; }
																echo '<option class="'.$thisValue["orderCategoriesClass"].'" value="'.$thisKey.'">'.$thisDistance.$thisValue["orderCategoriesName"].'</option>';
															}
														}
													?>
												</select>
											</td>
											<td style="border-right:1px dotted #333333;"></td>
											<td><label for="newStockDate">Anlieferungs-Datum:</label></td>
											<td><input type="text" name="newStockDate" id="newStockDate" class="inputField_100" readonly value="" /></td>
											<td style="border-right:1px dotted #333333;"></td>
											<td><label for="newStockQuantity">St&uuml;ckzahl:</label></td>
											<td><input type="text" name="newStockQuantity" id="newStockQuantity" class="inputField_100" value="" /></td>
											<td style="border-right:1px dotted #333333;"></td>
											<td style="text-align:right;"><input type="submit" name="storeNewStock" class="inputButton2" value="Speichern" /></td>
										</tr>
									</table>
								</form>
							</div>
						</div>
					</div>
					<?php
						}
						if(!empty($arrStockDatas)) {
							foreach($arrStockDatas as $thisKey => $thisValue) {
								echo '<div class="boxArea1">';
								echo '<h2>Bestands&uuml;bersicht '.$arrOrderCategoriesTypeDatas[$thisKey]["orderCategoriesName"].'</h2>';

								echo '<p><b>Bestand insgesamt:</b> ' . ($arrStockQuantity[$thisKey] - $arrConsumptionQuantity[$thisKey]) .'</p>' ;
								echo '		<div class="boxArea2">
											<div class="boxAreaContent">
												<h3>Lieferungen '.$arrOrderCategoriesTypeDatas[$thisKey]["orderCategoriesName"].'</h3>
												<div>
								';
								echo '<p><b>Lieferungen insgesamt:</b> ' . $arrStockQuantity[$thisKey].'</p>' ;
								echo '
										<table border="0" class="displayOrders" cellpadding="0" cellspacing="0" width="100%">
											<tr>
												<th style="width:45px;text-align:right;">#</th>
												<th>Eingangs-Datum</th>
												<th>St&uuml;ckzahl</th>
											</tr>
										';
										$countRow = 1;
										foreach($arrStockDatas[$thisKey] as $thisDataKey => $thisDataValue) {
											if($countRow%2 == 0){ $rowClass = 'row0'; }
											else { $rowClass = 'row1'; }
											echo '<tr class="' . $rowClass . '">';
											echo '<td style="text-align:right;"><b>' . $countRow . '.</b></td>';
											echo '<td>' . formatDate($arrStockDatas[$thisKey][$thisDataKey]["stockItemsDate"], 'display') . '</td>';
											echo '<td>' . $arrStockDatas[$thisKey][$thisDataKey]["stockItemsQuantity"] . '</td>';
											echo '</tr>';
											$countRow++;
										}

								echo '</table>';

								echo '			</div>
											</div>
										</div>
										<div class="boxArea2">
											<div class="boxAreaContent">
												<h3>Versand '.$arrOrderCategoriesTypeDatas[$thisKey]["orderCategoriesName"].'</h3>
												<div>
								';
								echo '<p><b>Verbrauch insgesamt:</b> ' . $arrConsumptionQuantity[$thisKey].'</p>' ;

								echo '
										<table border="0" class="displayOrders" cellpadding="0" cellspacing="0" width="100%">
											<tr>
												<th style="width:45px;text-align:right;">#</th>
												<th>Liefer-KW</th>
												<th>St&uuml;ckzahl</th>
											</tr>
								';

								if(!empty($arrConsumptionDatas[$ds["ordersCategoryID"]]))
								{
									foreach($arrConsumptionDatas[$ds["ordersCategoryID"]] as $thisDataKey => $thisDataValue) {
										if($countRow%2 == 0){ $rowClass = 'row0'; }
											else { $rowClass = 'row1'; }
											echo '<tr class="' . $rowClass . '">';
											echo '<td style="text-align:right;"><b>' . $countRow . '.</b></td>';
											echo '<td>'.$thisDataKey . formatDate($arrConsumptionDatas[$thisKey][$thisDataKey]["stockItemsDate"], 'display') . '</td>';
											echo '<td>' . $arrConsumptionDatas[$thisKey][$thisDataKey]["ordersProductMenge"] . '</td>';
											echo '</tr>';
											$countRow++;
									}
								}

								echo '</table>';

								echo '				</div>
												</div>
											</div>
											<div class="clear"></div>
									</div>
								';
							}
						}
					?>
				</div>
			</div>
		</div>
	</div>
</div>
<script language="javascript" type="text/javascript">
	$(document).ready(function() {
		$(function() {
			$.datepicker.setDefaults($.datepicker.regional[""]);
			$('#newStockDate').datepicker($.datepicker.regional["de"]);
			$('h2').next().toggle();
			$('h2').click(function(){
				$(this).next().toggle();
			});
		});
	});
</script>

<?php require_once('inc/footerHTML.inc.php'); ?>