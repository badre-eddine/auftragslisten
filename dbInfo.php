<?php
	require_once('inc/requires.inc.php');

	if(!$arrGetUserRights["dbInfo"]) {
		header('location: ' . PAGE_EXIT_LOCATION);
		exit;
	}

	$jswindowMessage = '';
	$warningMessage = '';
	$errorMessage = '';
	$successMessage = '';
	$infoMessage = '';

	$dbConnection = new DB_Connection();
	$db_open = $dbConnection->db_connect();

	// BOF KILL SQL-PROZESS
		if((int)$_GET["killProzessID"] > 0){
			#$sql_killProcess = "KILL " . $_GET["killProzessID"] . ";";
			$sql_killProcess = "KILL QUERY " . $_GET["killProzessID"] . ";";
			$rs_killProcess = $dbConnection->db_query($sql_killProcess);
			if($rs_killProcess){
				$successMessage .= ' Der SQL-Prozess ID: ' . $_GET["killProzessID"] . ' wurde beendet.' . '<br />';
			}
			else{
				$errorMessage .= ' Der SQL-Prozess ID: ' . $_GET["killProzessID"] . ' konnte nicht beendet werden.' . '<br />';
			}
		}
	// EOF KILL SQL-PROZESS
?>
<?php
	require_once('inc/headerHTML.inc.php');
	$thisTitle = "Datenbank-INFO";
	$headerHTML = preg_replace("/{###TITLE###}/", strip_tags($thisTitle), $headerHTML);
	echo $headerHTML;

	require_once(FILE_MENUE_TOP);
?>
<div id="mainArea">
	<div id="mainContent">
		<?php require_once(FILE_MENUE_SIDEBAR); ?>
		<div id="contentArea">
			<div id="contentAreaElements">
				<h1><?php if(SHOW_PAGE_TITLE_ICONS) { echo '<img src="' . PATH_ICONS_MENUE_TITLES . 'database.png" alt="" />'; } ?> <?php echo $thisTitle; ?></h1>

				<?php displayMessages(); ?>
				<?php
					printf("<b>MySQL Server Version:</b> %s\n", mysqli_get_server_info());
				?>

				<hr />
				
				<div class="detailsArea">
					<h2>Momentaner MySQL-Serverstatus</h2>
					<div class="displayDetails">
					<?php
						$db_status = explode('  ', mysqli_stat($db_open));
						$arrDbStatus = array();
						if(!empty($db_status)) {
							foreach($db_status as $thisValue) {
								$arrTemp = explode(':', $thisValue);
								$arrDbStatus[$arrTemp[0]] = $arrTemp[1];
							}
						}
						if(!empty($arrDbStatus)) {
							echo '<table border="0" cellpadding="0" cellspacing="0" width="100%" class="displayOrders">';
								echo '<tr>';
								foreach($arrDbStatus as $thisKey => $thisValue) {
									echo '<th>' .  $thisKey . '</th>';
								}
								echo '</tr>';
								echo '<tr>';
								foreach($arrDbStatus as $thisKey => $thisValue) {
									echo '<td>' .  $thisValue . '</td>';
								}
								echo '</tr>';
							echo '</table>';
						}
					?>
					</div>
				</div>
				
				<hr />

				<div class="detailsArea">
				<h2>MySQL-Status</h2>
				<div class="displayDetails">
					<?php
						$sql = 'SHOW STATUS';
						$rs = $dbConnection->db_query($sql);

						$count = 0;
						#echo '<div style="height:90px;overflow:auto;">';
						echo '<table border="0" cellpadding="0" cellspacing="0" class="displayOrders">';
						while ($ds = mysqli_fetch_assoc($rs)) {
							if($count%2 == 0){ $rowClass = 'row0'; }
							else { $rowClass = 'row1'; }
							echo '<tr class="' . $rowClass . '">';
							echo '<th style="text-align:left;">' . $ds['Variable_name'] . '</th><td style="text-align:right;">' . $ds['Value'] . "</td>";
							echo '</tr>';
							$count++;
						}
						echo '</table>';
						#echo '</div>';
					?>
					</div>
				</div>
				
				<hr />
				
				<div class="detailsArea">
				<h2>MySQL-Prozesse</h2>
				<div class="displayDetails">
					<?php
						$rs = mysqli_list_processes($db_open);

						$countProcesses = mysqli_num_rows($rs);
						echo '<p>' . $countProcesses . ' Prozesse</p>';
						$count = 0;
						echo '<table border="0" cellpadding="0" cellspacing="0" class="displayOrders">';
						echo '<tr>';
						echo '<th>#</th>';
						echo '<th>Id</th>';
						echo '<th>User</th>';
						echo '<th>Host</th>';
						echo '<th>db</th>';
						echo '<th>Command</th>';
						echo '<th>Time</th>';
						echo '<th>SQL</th>';
						echo '<th>Aktion</th>';
						echo '</tr>';
							
						while ($ds = mysqli_fetch_assoc($rs)){
							if($count%2 == 0){ $rowClass = 'row0'; }
							else { $rowClass = 'row1'; }
							echo '<tr class="' . $rowClass . '">';
							echo '<td style="text-align:right;"><b>' . ($count + 1) . '</b>.</td>';
							echo '<td>' . $ds["Id"] . '</td>';
							echo '<td>' . $ds["User"] . '</td>';
							echo '<td>' . $ds["Host"] . '</td>';
							echo '<td>' . $ds["db"] . '</td>';
							echo '<td>' . $ds["Command"] . '</td>';
							echo '<td style="text-align:right;white-space:nowrap;">' . $ds["Time"] . ' sec [' . number_format(($ds["Time"] / 60), 2) . ' min]</td>';
							echo '<td>' . $ds["Info"] . '</td>';
							echo '<td><a href="?killProzessID=' . $ds["Id"] . '" title="SQL-Prozess ' . $ds["Id"] . ' killen" onclick="return showWarning(\'Soll der SQL-Prozess ' . $ds["Id"] . ' wirklich beendet werden? \');"><img src="layout/icons/iconDelete.png" alt="x" width="14" height="14" /></a></td>';
							echo '</tr>';
							$count++;
						}
						echo '</table>';
						mysqli_free_result($rs);
					?>
					</div>
				</div>
				
				<hr />

				<div class="detailsArea">
				<h2>Dokument-Tabellen-Feldanzahl</h2>
				<div class="displayDetails">
					<?php
						// BOF GET MANDATORIES
							$arrMandatories = getMandatories();
						// EOF GET MANDATORIES

						// BOF GET DOCUMENTTYPES
							$arrDocumentTypes = getDocumentTypes();
						// EOF GET DOCUMENTTYPES

						// BOF CREATE TABLE NAMES
							if(!empty($arrMandatories) && !empty($arrDocumentTypes)){
								$arrTables = array();
								$arrTablesWhereSql = array();
								foreach($arrMandatories as $thisMandatory){
									foreach($arrDocumentTypes as $thisDocumentType){
										$tableDocument			= "TABLE_ORDER_" . $thisDocumentType["createdDocumentsTypesShortName"];
										$tableDocumentDetails	= "TABLE_ORDER_" . $thisDocumentType["createdDocumentsTypesShortName"] . "_DETAILS";
										if(defined($tableDocument)){
											$thisTableDocument = constant($tableDocument);
											$thisTableDocument = preg_replace("/" . MANDATOR . "/", $thisMandatory["mandatoriesShortName"], $thisTableDocument);
											$arrTablesWhereSql[] = " SELECT '" . $thisMandatory["mandatoriesShortName"] . "' AS `mandator`, '" . $tableDocument . "' AS `basic_name`, `table_name`, count(*) AS `columns` FROM `information_schema`.`columns` WHERE `table_name` = '" . $thisTableDocument . "'" ;

										}
										if(defined($tableDocument)){
											$thisTableDocumentDetails = constant($tableDocumentDetails);
											$thisTableDocumentDetails = preg_replace("/" . MANDATOR . "/", $thisMandatory["mandatoriesShortName"], $thisTableDocumentDetails);
											$arrTablesWhereSql[] = " SELECT '" . $thisMandatory["mandatoriesShortName"] . "' AS `mandator`, '" . $tableDocumentDetails . "' AS `basic_name`, `table_name`, count(*) AS `columns` FROM `information_schema`.`columns` WHERE `table_name` = '" . $thisTableDocumentDetails . "'" ;
										}
									}
								}
								if(!empty($arrTablesWhereSql)){
									$sql = implode("\n UNION \n", $arrTablesWhereSql) . " GROUP BY `table_name` ORDER BY `basic_name`, `table_name` ";
									$rs = $dbConnection->db_query($sql);
									$arrTableFieldData = array();
									while($ds = mysqli_fetch_assoc($rs)){
										foreach(array_keys($ds) as $field){
											$arrTableFieldData[$ds["basic_name"]][$ds["mandator"]][$field] = $ds[$field];
										}
									}

									if(!empty($arrTableFieldData)){
										echo '<table border="0" cellpadding="0" cellspacing="0" class="displayOrders">';

										echo '<tr>';
										for($i = 0; $i < count($arrMandatories); $i++){
											echo '<th>Tabelle</th>';
											echo '<th>Felder</th>';
										}
										echo '<th>Check</th>';
										echo '</tr>';
										$count = 0;
										foreach($arrTableFieldData as $thisTableFieldDataKey => $thisTableFieldDataValue){
											$arrCheckFieldCount = array();
											if($count%2 == 0){ $rowClass = 'row0'; }
											else { $rowClass = 'row1'; }
											echo '<tr class="' . $rowClass . '">';
											foreach($thisTableFieldDataValue as $thisTableFieldDataKey2 => $thisTableFieldDataValue2){
												echo '<td>';
												echo $thisTableFieldDataValue2["table_name"];
												echo '</td>';

												echo '<td>';
												echo $thisTableFieldDataValue2["columns"];
												$arrCheckFieldCount[] = $thisTableFieldDataValue2["columns"];
												echo '</td>';
											}

											echo '<td>';
											if(!empty($arrCheckFieldCount)){
												$arrCheckFieldCount = array_unique($arrCheckFieldCount);
												if(count($arrCheckFieldCount) == 1){
													echo '<span class="row3">' . 'OK' . '</span>';
												}
												else {
													echo '<span class="row2">' . 'FEHLER' . '</span>';
												}
											}
											echo '</td>';

											echo '</tr>';

											$count++;
										}

										echo '</table>';
									}
								}
							}
						// EOF CREATE TABLE NAMES
					?>
					</div>
				</div>
				
				<hr />

				<div class="detailsArea">
					<h2>Tabellen-Status</h2>
					<div class="displayDetails">
					<?php
						$sql = 'SHOW TABLE STATUS';
						$rs = $dbConnection->db_query($sql);
						$countTables = mysqli_num_rows($rs);
						echo '<p>' . $countTables . ' Tabellen</p>';
						$count = 0;
						#echo '<div style="height:90px;overflow:auto;">';
						echo '<table border="0" cellpadding="0" cellspacing="0" class="displayOrders">';
							echo '<tr style="font-size:11px;">';
							echo '<th>#</th>';
							echo '<th>Name</th>';
							echo '<th>Comment</th>';
							echo '<th>Engine</th>';
							echo '<th>Version</th>';
							echo '<th>Row_format</th>';
							echo '<th>Rows</th>';
							echo '<th>Avg_row_length</th>';
							echo '<th>Data_length</th>';
							echo '<th>Max_data_length</th>';
							echo '<th>Index_length</th>';
							echo '<th>Data_free</th>';
							echo '<th>Auto_increment</th>';
							echo '<th>Create_time</th>';
							echo '<th>Update_time</th>';
							echo '<th>Check_time</th>';
							echo '<th>Collation</th>';
							echo '<th>Checksum</th>';
							echo '<th>Create_options</th>';
							echo '</tr>';
						while ($ds = mysqli_fetch_assoc($rs)) {

							if($count%2 == 0){ $rowClass = 'row0'; }
							else { $rowClass = 'row1'; }
							$thisCellClass = '';
							if($ds["Comment"] == "VIEW"){
								$rowClass = 'row9';
								$thisCellClass = 'row9';
							}

							echo '<tr class="' . $rowClass . '">';

							echo '<td style="text-align:right;"><b>' . ($count + 1) . '</b>.</td>';

							echo '<td>' . $ds["Name"] . ' </td>';

							echo '<td class="' . $thisCellClass . '">' . $ds["Comment"] . ' </td>';

							$thisCellClass = '';
							if($ds["Engine"] == "MyISAM"){
								$thisCellClass = 'row3';
							}
							else if($ds["Engine"] != ""){
								$thisCellClass = 'row2';
							}

							echo '<td class="' . $thisCellClass . '">' . $ds["Engine"] . ' </td>';
							echo '<td>' . $ds["Version"] . ' </td>';
							echo '<td>' . $ds["Row_format"] . ' </td>';
							echo '<td style="text-align:right;">' . $ds["Rows"] . ' </td>';
							echo '<td style="text-align:right;">' . $ds["Avg_row_length"] . ' </td>';
							echo '<td style="text-align:right;">' . $ds["Data_length"] . ' </td>';
							echo '<td style="text-align:right;">' . $ds["Max_data_length"] . ' </td>';
							echo '<td style="text-align:right;">' . $ds["Index_length"] . ' </td>';
							echo '<td style="text-align:right;">' . $ds["Data_free"] . ' </td>';
							echo '<td style="text-align:right;">' . $ds["Auto_increment"] . ' </td>';
							echo '<td>' . $ds["Create_time"] . ' </td>';
							echo '<td>' . $ds["Update_time"] . ' </td>';
							echo '<td>' . $ds["Check_time"] . ' </td>';
							echo '<td>' . $ds["Collation"] . ' </td>';
							echo '<td>' . $ds["Checksum"] . ' </td>';
							echo '<td>' . $ds["Create_options"] . ' </td>';
							echo '</tr>';
							$count++;
						}
						echo '</table>';
						#echo '</div>';
					?>
					</div>
				</div>
				
				<hr />

				<div class="detailsArea">
					<h2>MySql-Variablen</h2>
					<div class="displayDetails">
					<?php
						$sql = 'SHOW VARIABLES';
						$rs = $dbConnection->db_query($sql);

						#echo '<div style="height:90px;overflow:auto;">';
						echo '<table border="0" cellpadding="0" cellspacing="0" width="100%" class="displayOrders">';

						$count = 0;
						while ($ds = mysqli_fetch_assoc($rs)) {
							if($count%2 == 0){ $rowClass = 'row0'; }
							else { $rowClass = 'row1'; }
							echo '<tr class="' . $rowClass . '">';
							echo '<th style="text-align:left;">' . $ds["Variable_name"] . '</th>';
							echo '<td>' .  htmlentities($ds["Value"]) . '</td>';
							echo '</tr>';
						}
						echo '</table>';
						#echo '</div>';
					?>				
					</div>
				</div>
				
				<hr />				
				
			</div>
		</div>
		<div class="clear"></div>
	</div>
	<div class="clear"></div>
</div>

<script language="javascript" type="text/javascript">
	$(document).ready(function() {
		colorRowMouseOver('.displayOrders tbody tr');
	});

	// BOF ADD TOOGLE BUTTON
		var buttonToggle = '<img src="layout/icons/iconToggle3.png" class="buttonToggleDetails" width="14" height="14" alt="" title="Details ein-/ausblenden" style="float:right;" />';
		$('.detailsArea h2').append(buttonToggle);
		
		var thisDefaultHeight = '90px';
		$('.displayDetails').css('height', thisDefaultHeight);
		$('.displayDetails').css('display', 'block');
		
		$('.buttonToggleDetails').live('click', function(){
			/*
			$(this).parent().next('.displayDetails').toggle();
			*/
			
			var thisDefaultHeight = '90px';
			var thisNewHeight = thisDefaultHeight;
			var thisHeightCurrent = $(this).parent().next('.displayDetails').css('height');
			
			if(thisHeightCurrent == thisDefaultHeight){
				thisNewHeight = '100%';
			}
			$(this).parent().next('.displayDetails').css('max-height', thisNewHeight);
			$(this).parent().next('.displayDetails').css('height', thisNewHeight);
		});
	// EOF ADD TOOGLE BUTTON
</script>
<?php require_once('inc/footerHTML.inc.php'); ?>