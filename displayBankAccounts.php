<?php
	require_once('inc/requires.inc.php');

	if(!$arrGetUserRights["displayBankDatas"] && !$arrGetUserRights["editBankDatas"]) {
		header('location: ' . PAGE_EXIT_LOCATION);
		exit;
	}

	// BOF DEFINE DUTY FIELDS
		$arrFormDutyFields = array(
			"editBankAccountTypesShortName" => "Bankname-Kurzform",
			"editBankAccountTypesName" => "Bankname",
			"editBankAccountTypesAccountNumber" => "Kontonummer",
			"editBankAccountTypesBankCode" => "Bankleitzahl"
		);
		// CREATE JSON DUTY FIELD
		$jsonFormDutyFields = createJson($arrFormDutyFields);
	// BOF DEFINE DUTY FIELDS

	$jswindowMessage = "";
	$warningMessage = '';
	$errorMessage = '';
	$successMessage = '';
	$infoMessage = '';

	$dbConnection = new DB_Connection();
	$db_open = $dbConnection->db_connect();

	if(trim($_REQUEST["searchBoxPersonnel"]) != '') {
		$sql = "SELECT
				`personnelID`

				FROM `" . TABLE_PERSONNEL . "`

				WHERE `personnelFirstName` = '" . trim($_REQUEST["searchBoxPersonnel"]) . "'
					OR `personnelLastName` = '" . trim($_REQUEST["searchBoxPersonnel"]) . "'

				LIMIT 1
		";
		$rs = $dbConnection->db_query($sql);

		list($thisPersonnelID) = mysqli_fetch_array($rs);

		if($thisPersonnelID != "") { $_REQUEST["editID"] = $thisPersonnelID; }
		else {
			$warningMessage .= ' Es wurde kein Mitarbeitert gefunden. ';
		}
	}

	if($_REQUEST["editID"] == "" && $_REQUEST["addDatas"] != "")
	{
		$_REQUEST["editID"] = "NEW";
		//header("location: displayOrders.php?ordersType=Bestellung");
		//exit;
	}

	// BOF STORE DATAS
	if($_POST["storeDatas"] != "") {

		$doAction = checkFormDutyFields($arrFormDutyFields);

		if($doAction) {
			if($_POST["editBankAccountTypesID"] == "") {
				// $_POST["editBankAccountTypesID"] = "%";
				$sql = "SELECT (MAX(`personnelID`) + 1) AS `newPersonnelID` FROM`" . TABLE_PERSONNEL . "`";
				$rs = $dbConnection->db_query($sql);
				list($thisBankAccountTypesID) = mysqli_fetch_array($rs);
				if($thisBankAccountTypesID == null || $thisBankAccountTypesID == 0){
					$thisBankAccountTypesID = 1;
				}
			}
			else {
				$thisBankAccountTypesID = $_POST["bankAccountTypesID"];
			}

			// BOF UNSET DEFAULT
				if($_POST["editBankAccountTypesDefault"] == ''){
					$_POST["editBankAccountTypesDefault"] = 0;
				}
				if($_POST["editBankAccountTypesDefault"] == '1'){
					$sql = "UPDATE `" . TABLE_BANK_ACCOUNT_TYPES . "` SET `bankAccountTypesDefault` = '0'";
					$rs = $dbConnection->db_query($sql);
				}
			// BOF UNSET DEFAULT

			$sql = "REPLACE INTO `" . TABLE_BANK_ACCOUNT_TYPES . "` (
									
									`bankAccountTypesShortName`,
									`bankAccountTypesName`,
									`bankAccountTypesAccountNumber`,
									`bankAccountTypesBankCode`,
									`bankAccountTypesIBAN`,
									`bankAccountTypesBIC`,
									`bankAccountTypesShowWarning`,
									`bankAccountTypesActive`,
									`bankAccountTypesDefault`,
									`bankAccountTypesAccountOwner`,
									`bankAccountTypesNotice`
								)
								VALUES (
									
									'".strtoupper(preg_replace("/[^a-zA-Z0-9]/", "", $_POST["editBankAccountTypesShortName"]))."',
									'".($_POST["editBankAccountTypesName"])."',
									'".($_POST["editBankAccountTypesAccountNumber"])."',
									'".($_POST["editBankAccountTypesBankCode"])."',
									'".($_POST["editBankAccountTypesIBAN"])."',
									'".($_POST["editBankAccountTypesBIC"])."',
									'".($_POST["editBankAccountTypesShowWarning"])."',
									'".($_POST["editBankAccountTypesActive"])."',
									'".($_POST["editBankAccountTypesDefault"])."',
									'".($_POST["editBankAccountTypesAccountOwner"])."',
									'".addslashes($_POST["editBankAccountTypesNotice"])."'
								)
			";

			$rs = $dbConnection->db_query($sql);

			if($rs) {
				$successMessage .= ' Der Datensatz wurde gespeichert.' .'<br />';
				$_REQUEST["editID"] = mysqli_insert_id();
			}
			else {
				$errorMessage .= ' Der Datensatz konnte nicht gespeichert werden.' .'<br />';
			}
		}
		else {
			$errorMessage .= ' Der Datensatz konnte nicht gespeichert werden. Bitte füllen Sie alle Pflichtfelder aus.' .'<br />';
		}
	}
	// EOF STORE DATAS

	// BOF DELETE DATAS
	if($_POST["deleteDatas"] != "") {
		$sql = "DELETE FROM `" . TABLE_BANK_ACCOUNT_TYPES . "` WHERE `bankAccountTypesID` = '".$_POST["editBankAccountTypesID"]."'";
		$rs = $dbConnection->db_query($sql);

		if($rs) {
			$successMessage .= ' Der Datensatz wurde endgültig gelöscht. ' .'<br />';
		}
		else {
			$errorMessage .= ' Der Datensatz konnte nicht gelöscht werden. ' .'<br />';
		}
	}
	// EOF DELETE DATAS

	// BOF READ ALL DATAS
	if($_REQUEST["editID"] == "") {
		$sql = "SELECT
				`bankAccountTypesID`,
				`bankAccountTypesShortName`,
				`bankAccountTypesName`,
				`bankAccountTypesAccountNumber`,
				`bankAccountTypesBankCode`,
				`bankAccountTypesIBAN`,
				`bankAccountTypesBIC`,
				`bankAccountTypesShowWarning`,
				`bankAccountTypesActive`,
				`bankAccountTypesDefault`,
				`bankAccountTypesAccountOwner`,
				`bankAccountTypesNotice`

				FROM `" . TABLE_BANK_ACCOUNT_TYPES . "`

				WHERE 1

				ORDER BY `bankAccountTypesName`
		";
		$rs = $dbConnection->db_query($sql);

		while($ds = mysqli_fetch_assoc($rs)) {
			foreach(array_keys($ds) as $field) {
				$arrBankAccountDatas[$ds["bankAccountTypesID"]][$field] = $ds[$field];
			}
		}
	}
	// EOF READ ALL DATAS

	// BOF READ SELECTED DATAS
	if($_REQUEST["editID"] != "") {
		$sql = "SELECT
					`bankAccountTypesID`,
					`bankAccountTypesShortName`,
					`bankAccountTypesName`,
					`bankAccountTypesAccountNumber`,
					`bankAccountTypesBankCode`,
					`bankAccountTypesIBAN`,
					`bankAccountTypesBIC`,
					`bankAccountTypesShowWarning`,
					`bankAccountTypesActive`,
					`bankAccountTypesDefault`,
					`bankAccountTypesAccountOwner`,
					`bankAccountTypesNotice`

					FROM `" . TABLE_BANK_ACCOUNT_TYPES . "`

					WHERE `bankAccountTypesID` = '" . $_REQUEST["editID"] . "'
			";

			$rs = $dbConnection->db_query($sql);

			$selectedBankAccountDatas = array();
			list(
				$selectedBankAccountDatas["bankAccountTypesID"],
				$selectedBankAccountDatas["bankAccountTypesShortName"],
				$selectedBankAccountDatas["bankAccountTypesName"],
				$selectedBankAccountDatas["bankAccountTypesAccountNumber"],
				$selectedBankAccountDatas["bankAccountTypesBankCode"],
				$selectedBankAccountDatas["bankAccountTypesIBAN"],
				$selectedBankAccountDatas["bankAccountTypesBIC"],
				$selectedBankAccountDatas["bankAccountTypesShowWarning"],
				$selectedBankAccountDatas["bankAccountTypesActive"],
				$selectedBankAccountDatas["bankAccountTypesDefault"],
				$selectedBankAccountDatas["bankAccountTypesAccountOwner"],
				$selectedBankAccountDatas["bankAccountTypesNotice"]

			) = mysqli_fetch_array($rs);
		}
	// EOF READ SELECTED DATAS
?>
<?php
	require_once('inc/headerHTML.inc.php');
	if($_REQUEST["editID"] == "NEW") {
		$thisTitle = "Bankkonto anlegen";
	}
	else {
		$thisTitle = "Bankkonten bearbeiten";
		if($selectedBankAccountDatas["bankAccountTypesName"] != ''){
			$thisTitle .= ': <span class="headerSelectedEntry">' . $selectedBankAccountDatas["bankAccountTypesName"] . '</span>';
		}
	}
	$headerHTML = preg_replace("/{###TITLE###}/", strip_tags($thisTitle), $headerHTML);
	echo $headerHTML;

	require_once(FILE_MENUE_TOP);
?>
<div id="mainArea">
	<div id="mainContent">
		<?php require_once(FILE_MENUE_SIDEBAR); ?>
		<div id="contentArea">
			<div id="contentAreaElements">
				<h1><?php if(SHOW_PAGE_TITLE_ICONS) { echo '<img src="' . PATH_ICONS_MENUE_TITLES . 'personnel.png" alt="" />'; } ?> <?php echo $thisTitle; ?></h1>

				<div class="contentDisplay">

					<?php displayMessages(); ?>

					<form name="editBankAccountDatas" method="post" action="<?php echo $_SERVER["PHP_SELF"]; ?>">

					<?php if($_REQUEST["editID"] == "") { ?>
					<div id="searchFilterArea">
						<table border="0" width="100%" cellspacing="0" cellpadding="0" class="searchFilterContent">
							<tr>
								<td><b>Bankkonten:</b></td>
								<td>
									<select name="editID" id="editID" class="inputSelect_510" onchange="submit();">
										<option value=""> --- Bitte w&auml;hlen --- </option>
										<?php
											if(!empty($arrBankAccountDatas)) {
												foreach($arrBankAccountDatas as $thisKey => $thisValue) {
													if(!$arrBankAccountDatas[$thisKey]["bankAccountTypesActive"]) {
														$style = "color:#FF0000;font-style:italic;text-decoration:line-through;";
													}
													else {
														$style = "";
													}
													$thisStandard = '';
													if($arrBankAccountDatas[$thisKey]["bankAccountTypesDefault"] == '1') {
														$thisStandard = ' [Standard]';
														$style = "background-color:#00EE00;";
													}
													echo '<option style="'. $style .'" value="'.$thisKey.'">' . $arrBankAccountDatas[$thisKey]["bankAccountTypesNotice"] . ' ' . $arrBankAccountDatas[$thisKey]["bankAccountTypesName"].' (Konto-Nr: '.$arrBankAccountDatas[$thisKey]["bankAccountTypesAccountNumber"].' )' . $thisStandard . '</option>';
												}
											}
										?>
									</select>
								</td>
								<?php
									if($arrGetUserRights["editPersonnel"]) {
								?>
								<td><input name="addDatas" id="addDatas" type="submit" value="Neues Bankkonto anlegen" class="inputButton1" /></td>
								<?php
									}
								?>
							</tr>
						</table>
					</div>
					<?php } ?>

					<?php
						if($_REQUEST["editID"] != "")
						{
					?>
					<div class="adminInfo">Datensatz-ID: <?php echo $selectedBankAccountDatas["bankAccountTypesID"]; ?></div>
					<div class="adminEditArea">
					<p class="warningArea">Pflichtfelder <span class="dutyField">(*)</span> m&uuml;ssen ausgef&uuml;llt werden!</p>
					<input type="hidden" name="editBankAccountTypesID" value="<?php echo $selectedBankAccountDatas["bankAccountTypesID"]; ?>" />
					<fieldset>
						<legend>Konto-Daten</legend>

						<table border="0" width="100%" cellspacing="0" cellpadding="0">
							<tr>
								<td style="width:200px;"><b>Bankkonto aktivieren:</b></td>
								<td>
									<input type="checkbox" name="editBankAccountTypesActive" id="editBankAccountTypesActive" class="inputCheckbox1" value="1" <?php if($selectedBankAccountDatas["bankAccountTypesActive"] == 1) { echo ' checked="checked" '; } ?> />
								</td>
							</tr>
							<tr>
								<td style="width:200px;"><b>Bankkonto als Standard:</b></td>
								<td>
									<input type="checkbox" name="editBankAccountTypesDefault" id="editBankAccountTypesDefault" class="inputCheckbox1" value="1" <?php if($selectedBankAccountDatas["bankAccountTypesDefault"] == 1) { echo ' checked="checked" '; } ?> />
								</td>
							</tr>
							<tr>
								<td><b>Bankname:</b></td>
								<td><input type="text" name="editBankAccountTypesName" id="editBankAccountTypesName" class="inputField_510" value="<?php echo ($selectedBankAccountDatas["bankAccountTypesName"]); ?>" /></td>
							</tr>
							<tr>
								<td><b>Bankname (Kurzform):</b></td>
								<td><input type="text" name="editBankAccountTypesShortName" id="editBankAccountTypesShortName" class="inputField_510" value="<?php echo ($selectedBankAccountDatas["bankAccountTypesShortName"]); ?>" /></td>
							</tr>
							<tr>
								<td><b>Kontonummer:</b></td>
								<td><input type="text" name="editBankAccountTypesAccountNumber" id="editBankAccountTypesAccountNumber" class="inputField_510" value="<?php echo ($selectedBankAccountDatas["bankAccountTypesAccountNumber"]); ?>" /></td>
							</tr>
							<tr>
								<td><b>Kontoinhaber:</b></td>
								<td><input type="text" name="editBankAccountTypesAccountOwner" id="editBankAccountTypesAccountOwner" class="inputField_510" value="<?php echo ($selectedBankAccountDatas["bankAccountTypesAccountOwner"]); ?>" /></td>
							</tr>

							<tr>
								<td><b>Bankleitzahl:</b></td>
								<td><input type="text" name="editBankAccountTypesBankCode" id="editBankAccountTypesBankCode" class="inputField_510" value="<?php echo ($selectedBankAccountDatas["bankAccountTypesBankCode"]); ?>" /></td>
							</tr>
							<tr>
								<td><b>IBAN:</b></td>
								<td><input type="text" name="editBankAccountTypesIBAN" id="editBankAccountTypesIBAN" class="inputField_510" value="<?php echo ($selectedBankAccountDatas["bankAccountTypesIBAN"]); ?>" /></td>
							</tr>
							<tr>
								<td><b>BIC:</b></td>
								<td><input type="text" name="editBankAccountTypesBIC" id="editBankAccountTypesBIC" class="inputField_510" value="<?php echo ($selectedBankAccountDatas["bankAccountTypesBIC"]); ?>" /></td>
							</tr>
							<tr>
								<td><b>Hinweis anzeigen:</b></td>
								<td>
									<input type="checkbox" name="editBankAccountTypesShowWarning" id="editBankAccountTypesShowWarning" class="inputCheckbox1" value="1" <?php if($selectedBankAccountDatas["bankAccountTypesShowWarning"] == 1) { echo ' checked="checked" '; } ?> />
								</td>
							</tr>
							<tr>
								<td><b>Notiz:</b></td>
								<td><input type="text" name="editBankAccountTypesNotice" id="editBankAccountTypesNotice" class="inputField_510" value="<?php echo ($selectedBankAccountDatas["bankAccountTypesNotice"]); ?>" /></td>
							</tr>
						</table>
					</fieldset>

					<?php
						if($arrGetUserRights["editBankDatas"]) {
					?>
					<div class="actionButtonsArea">
						<input type="submit" class="inputButton1" name="storeDatas" value="Speichern" onclick="return checkFormDutyFields(this.form.name, <?php echo $jsonFormDutyFields; ?>);" />
						&nbsp;
						<!--
						<input type="submit" class="inputButton1" name="deleteDatas" value="Entfernen" onclick="return showWarning(' Wollen Sie diesen Eintrag endgültig entfernen??? ');" />
						&nbsp;
						-->
						<input type="submit" class="inputButton1" name="resetDatas" value="Abbrechen" onclick="return showWarning(' Wollen Sie diesen Vorgang wirklich abbrechen??? '); "/>
					</div>
					<?php
						}
					?>
					</div>

					<?php
						}
					?>
					</form>
				</div>
			</div>
		</div>
		<div class="clear"></div>
	</div>
</div>

<script language="javascript" type="text/javascript">
	$(document).ready(function() {
		$(function() {

		});
		setFormDutyFields(<?php echo $jsonFormDutyFields; ?>);
		colorRowMouseOver('.displayOrders tbody tr');
	});
</script>

<?php require_once('inc/footerHTML.inc.php'); ?>