<?php
	require_once('inc/requires.inc.php');

	DEFINE('DATE_STOCK_START', '2013-02-10');

	if(!$arrGetUserRights["editStocks"] && !$arrGetUserRights["displayStocks"]) {
		header('location: ' . PAGE_EXIT_LOCATION);
		exit;
	}

	$jswindowMessage = "";
	$warningMessage = '';
	$errorMessage = '';
	$successMessage = '';
	$infoMessage = '';

	$dbConnection = new DB_Connection();
	$db_open = $dbConnection->db_connect();


	// BOF GET MANDATORIES
		$arrMandatories = getMandatories();
	// EOF GET MANDATORIES

	// BOF GET PRODUCTS VPE
		$arrProductsVPE = getProductsVPE();
	// EOF GET PRODUCTS VPE

	// BOF GET PRINT TYPES
		$arrPrintTypes = getPrintTypes();
	// EOF GET PRINT TYPES
	
	// BOF READ BESTELL STATUS TYPEN
		$arrOrderStatusTypeDatas = getOrderStatusTypes();
		$arrOrderStatusTypeDatasExtern = getOrderStatusTypesExtern();
	// EOF READ BESTELL STATUS TYPEN

	// BOF GET PRODUCTS CATEGORIES
		#$arrProductsCategories = getOrderCategories();
		#$arrProductsCategories = getOrderProducts();
		#dd('arrProductsCategories');
	// EOF GET PRODUCTS CATEGORIES

	// BOF GET ALL PRODUCT CATEGORIES
		$sql_getCat = "
				SELECT
						`" . TABLE_STOCK_PRODUCT_CATEGORIES . "`.`stockProductCategoriesID`,
						`" . TABLE_STOCK_PRODUCT_CATEGORIES . "`.`stockProductCategoriesLevelID`,
						`" . TABLE_STOCK_PRODUCT_CATEGORIES . "`.`stockProductCategoriesParentID`,
						`" . TABLE_STOCK_PRODUCT_CATEGORIES . "`.`stockProductCategoriesRelationID`,
						`" . TABLE_STOCK_PRODUCT_CATEGORIES . "`.`stockProductCategoriesName`,
						`" . TABLE_STOCK_PRODUCT_CATEGORIES . "`.`stockProductCategoriesText`,
						`" . TABLE_STOCK_PRODUCT_CATEGORIES . "`.`stockProductCategoriesShortName`,
						`" . TABLE_STOCK_PRODUCT_CATEGORIES . "`.`stockProductCategoriesProductNumber`,
						`" . TABLE_STOCK_PRODUCT_CATEGORIES . "`.`stockProductCategoriesProductNumber_bctr`,
						`" . TABLE_STOCK_PRODUCT_CATEGORIES . "`.`stockProductCategoriesProductNumber_b3`,
						`" . TABLE_STOCK_PRODUCT_CATEGORIES . "`.`stockProductCategoriesSort`,
						`" . TABLE_STOCK_PRODUCT_CATEGORIES . "`.`stockProductCategoriesActive`,
						`" . TABLE_STOCK_PRODUCT_CATEGORIES . "`.`stockProductCategoriesContainsStockProducts`

					FROM `" . TABLE_STOCK_PRODUCT_CATEGORIES . "`
					WHERE 1
						AND `" . TABLE_STOCK_PRODUCT_CATEGORIES . "`.`stockProductCategoriesParentID` = '000'
						AND `" . TABLE_STOCK_PRODUCT_CATEGORIES . "`.`stockProductCategoriesContainsStockProducts` = '1'

					ORDER BY
						`" . TABLE_STOCK_PRODUCT_CATEGORIES . "`.`stockProductCategoriesSort`,
						`" . TABLE_STOCK_PRODUCT_CATEGORIES . "`.`stockProductCategoriesName`

						/*,
						`stockProductCategoriesParentID`,
						`stockProductCategoriesLevelID`
						*/

			";
		$rs_getCat = $dbConnection->db_query($sql_getCat);

		$arrProductsCategoryData = array();

		while($ds_getCat = mysqli_fetch_assoc($rs_getCat)){
			foreach(array_keys($ds_getCat) as $field){
				$arrProductsCategoryData[$ds_getCat["stockProductCategoriesLevelID"]][$field] = $ds_getCat[$field];
			}
		}
	// EOF GET ALL PRODUCT CATEGORIES


	// BOF GET ALL PRODUCTS
		$sqlWhere = "";

		if($_REQUEST["searchProductCategory"] != ""){
			$sqlWhere = " AND `productCategories`.`stockProductCategoriesParentID` = '" . $_REQUEST["searchProductCategory"] . "' ";
		}
		else if($_REQUEST["searchProductNumber"] != ""){
			$sqlWhere = "
					AND (
						`productCategories`.`stockProductCategoriesProductNumber` = '" . $_REQUEST["searchProductNumber"] . "'
						OR `productCategories`.`stockProductCategoriesProductNumber_bctr` = '" . $_REQUEST["searchProductNumber"] . "'
						OR `productCategories`.`stockProductCategoriesProductNumber_b3` = '" . $_REQUEST["searchProductNumber"] . "'
					)
				";
		}
		else if($_REQUEST["searchProductName"] != ""){
			$sqlWhere = "
					AND (
						`productCategories`.`stockProductCategoriesName` LIKE '%" . $_REQUEST["searchProductName"] . "%'
						OR `productCategories`.`stockProductCategoriesText` LIKE '%" . $_REQUEST["searchProductName"] . "%'
					)
				";
		}
		else if($_REQUEST["searchWord"] != ""){
			$sqlWhere = "
					AND (
						`productCategories`.`stockProductCategoriesName` LIKE '%" . $_REQUEST["searchWord"] . "%'
						OR `productCategories`.`stockProductCategoriesText` LIKE '%" . $_REQUEST["searchWord"] . "%'
						OR `productCategories`.`stockProductCategoriesProductNumber` LIKE '%" . $_REQUEST["searchWord"] . "%'
						OR `productCategories`.`stockProductCategoriesProductNumber_bctr` LIKE '%" . $_REQUEST["searchWord"] . "%'
						OR `productCategories`.`stockProductCategoriesProductNumber_b3` LIKE '%" . $_REQUEST["searchWord"] . "%'
					)
				";
		}

		if($_REQUEST["searchDeactivatedProducts"] != "1"){
			$sqlWhere .= " AND `productCategories`.`stockProductCategoriesActive` = '1' ";
		}
		if($_REQUEST["searchForeignProducts"] != "1"){
			$sqlWhere .= " AND `productCategories`.`stockProductCategoriesProductIsForeign` != '1' ";
		}

		$sql_getCat = "
				SELECT
						*,

						GROUP_CONCAT(DISTINCT `" . TABLE_ORDERS . "`.`ordersID`) AS `ordersIDs`,
						
						GROUP_CONCAT(
							CONCAT(
								`" . TABLE_ORDERS . "`.`ordersID`, ':',
								`" . TABLE_ORDERS . "`.`ordersKundennummer`, ':',
								`" . TABLE_ORDERS . "`.`ordersKundenName`, ':',
								`" . TABLE_ORDERS . "`.`ordersArtikelMenge`, ':',
								`" . TABLE_ORDERS . "`.`ordersBestellDatum`, ':',
								`" . TABLE_ORDERS . "`.`ordersStatus`, ':',
								`" . TABLE_ORDERS . "`.`ordersArtikelPrintColorsCount`, ':',
								`" . TABLE_ORDERS . "`.`ordersArtikelWithBorder`, ':',
								`" . TABLE_ORDERS . "`.`ordersArtikelWithClearPaint`, ':',
								`" . TABLE_ORDERS . "`.`ordersAdditionalCostsID`, ':',
								IF(`" . TABLE_PRODUCTIONS_TRANSFER . "`.`ordersProductionsTransferOrdersID` IS NULL, 'DE', 'TR')								
							)							
							ORDER BY
								`" . TABLE_PRODUCTIONS_TRANSFER . "`.`ordersProductionsTransferOrdersID` ASC,
								`" . TABLE_ORDERS . "`.`ordersKundennummer` ASC
							SEPARATOR '###'
						) AS `ordersDetails`,
						
						SUM(`" . TABLE_ORDERS . "`.`ordersArtikelMenge`) AS `productQuantity_DE`,
						SUM(`" . TABLE_PRODUCTIONS_TRANSFER . "`.`ordersProductionsTransferOrdersProductQuantity`) AS `productQuantity_TR`,					

						IF(`" . TABLE_PRODUCTIONS_TRANSFER . "`.`ordersProductionsTransferOrdersID` IS NULL, 'DE', 'TR') AS `productionLocation`

					FROM (
						SELECT
								`productCategories`.`stockProductCategoriesID` AS `stockProductsID`,
								`productCategories`.`stockProductCategoriesLevelID` AS `stockProductsLevelID`,
								`productCategories`.`stockProductCategoriesParentID` AS `stockProductsParentID`,
								`productCategories`.`stockProductCategoriesRelationID` AS `stockProductsRelationID`,
								`productCategories`.`stockProductCategoriesName` AS `stockProductsName`,
								`productCategories`.`stockProductCategoriesText` AS `stockProductsText`,
								`productCategories`.`stockProductCategoriesShortName` AS `stockProductsShortName`,
								`productCategories`.`stockProductCategoriesProductNumber` AS `stockProductsProductNumber`,
								`productCategories`.`stockProductCategoriesProductNumber_bctr` AS `stockProductsProductNumber_bctr`,
								`productCategories`.`stockProductCategoriesProductNumber_b3` AS `stockProductsProductNumber_b3`,
								`productCategories`.`stockProductCategoriesSort` AS `stockProductsSort`,
								`productCategories`.`stockProductCategoriesActive` AS `stockProductsActive`,
								`productCategories`.`stockProductCategoriesContainsStockProducts` AS `stockProductsContainsStockProducts`,

								`parentCategories`.`stockProductCategoriesID` AS `stockParentCategoriesID`,
								`parentCategories`.`stockProductCategoriesLevelID` AS `stockParentCategoriesLevelID`,
								`parentCategories`.`stockProductCategoriesParentID` AS `stockParentCategoriesParentID`,
								`parentCategories`.`stockProductCategoriesRelationID` AS `stockParentCategoriesRelationID`,
								`parentCategories`.`stockProductCategoriesName` AS `stockParentCategoriesName`,
								`parentCategories`.`stockProductCategoriesText` AS `stockParentCategoriesText`,
								`parentCategories`.`stockProductCategoriesShortName` AS `stockParentCategoriesShortName`,
								`parentCategories`.`stockProductCategoriesProductNumber` AS `stockParentCategoriesProductNumber`,
								`parentCategories`.`stockProductCategoriesProductNumber_bctr` AS `stockParentCategoriesProductNumber_bctr`,
								`parentCategories`.`stockProductCategoriesProductNumber_b3` AS `stockParentCategoriesProductNumber_b3`,
								`parentCategories`.`stockProductCategoriesSort` AS `stockParentCategoriesSort`,
								`parentCategories`.`stockProductCategoriesActive` AS `stockParentCategoriesActive`,
								`parentCategories`.`stockProductCategoriesContainsStockProducts` AS `stockParentContainsStockProducts`,

								GROUP_CONCAT(
									DISTINCT
									`productOptions`.`stockProductOptionsName`,
									'|',
									`productAttributes`.`stockProductOptionsName`,
									'|',
									`productAttributes`.`stockProductOptionsFaktor`
									ORDER BY `productOptions`.`stockProductOptionsSort` ASC, `productAttributes`.`stockProductOptionsName` ASC
									SEPARATOR '###'
								) AS `stockProductData`,

								`" . TABLE_GTIN_CODES . "`.`codesGtinID`,
								`" . TABLE_GTIN_CODES . "`.`codesGtinType`,
								`" . TABLE_GTIN_CODES . "`.`codesGtinNumber`,
								`" . TABLE_GTIN_CODES . "`.`codesGtinProductName`,
								`" . TABLE_GTIN_CODES . "`.`codesGtinProductNumber`


							FROM `" . TABLE_STOCK_PRODUCT_CATEGORIES . "` AS `productCategories`

							LEFT JOIN `" . TABLE_STOCK_PRODUCT_CATEGORIES . "` AS `parentCategories`
							ON(`productCategories`.`stockProductCategoriesParentID` = `parentCategories`.`stockProductCategoriesLevelID`)

							LEFT JOIN `" . TABLE_STOCK_PRODUCT_DETAIL_DATA . "`
							ON(`productCategories`.`stockProductCategoriesID` = `" . TABLE_STOCK_PRODUCT_DETAIL_DATA . "`.`stockProductDetailDataProductID`)

							LEFT JOIN `" . TABLE_STOCK_PRODUCT_OPTIONS . "` AS `productOptions`
							ON(`" . TABLE_STOCK_PRODUCT_DETAIL_DATA . "`.`stockProductDetailDataOptionID` = `productOptions`.`stockProductOptionsID`)

							LEFT JOIN `" . TABLE_STOCK_PRODUCT_OPTIONS . "` AS `productAttributes`
							ON(`" . TABLE_STOCK_PRODUCT_DETAIL_DATA . "`.`stockProductDetailDataAttributeID` = `productAttributes`.`stockProductOptionsID`)

							LEFT JOIN `" . TABLE_GTIN_CODES . "`
							ON(
								`productCategories`.`stockProductCategoriesProductNumber` = `" . TABLE_GTIN_CODES . "`.`codesGtinProductNumber`
								OR `productCategories`.`stockProductCategoriesProductNumber_bctr` = `" . TABLE_GTIN_CODES . "`.`codesGtinProductNumber`
								OR `productCategories`.`stockProductCategoriesProductNumber_b3` = `" . TABLE_GTIN_CODES . "`.`codesGtinProductNumber`
							)

							WHERE 1
								AND `productCategories`.`stockProductCategoriesParentID` != '000'
								AND `parentCategories`.`stockProductCategoriesContainsStockProducts` = '1'

								" . $sqlWhere . "

							GROUP BY
								`productCategories`.`stockProductCategoriesID`

						) AS `tempTable`

						LEFT JOIN `" . TABLE_ORDERS . "`
						ON(
							(
								`tempTable`.`stockProductsProductNumber` = `" . TABLE_ORDERS . "`.`ordersArtikelnummer`
								OR `tempTable`.`stockProductsProductNumber_bctr` = `" . TABLE_ORDERS . "`.`ordersArtikelnummer`
								OR `tempTable`.`stockProductsProductNumber_b3` = `" . TABLE_ORDERS . "`.`ordersArtikelnummer`
							)
							AND (
								`" . TABLE_ORDERS . "`.`ordersStatus` = '3'
								OR `" . TABLE_ORDERS . "`.`ordersStatus` = '7'
								OR `" . TABLE_ORDERS . "`.`ordersStatus` = '2'
							)
						)

						LEFT JOIN `" . TABLE_PRODUCTIONS_TRANSFER . "`
						ON(`" . TABLE_ORDERS . "`.`ordersID` = `" . TABLE_PRODUCTIONS_TRANSFER . "`.`ordersProductionsTransferOrdersID`)

						WHERE 1
							/*
							AND (
									`" . TABLE_ORDERS . "`.`ordersStatus` = '3'
									OR `" . TABLE_ORDERS . "`.`ordersStatus` = '7'
									OR `" . TABLE_ORDERS . "`.`ordersStatus` = '2'
								)
							*/

						GROUP BY
							`tempTable`.`stockProductsID`

						HAVING
							`tempTable`.`stockProductsID` IS NOT NULL

						ORDER BY
							`tempTable`.`stockParentCategoriesSort`,

							`tempTable`.`stockProductsSort` ASC,
							`tempTable`.`stockProductsName` ASC,
							`tempTable`.`stockProductsParentID` ASC
							/*,
							`tempTable`.`stockProductsLevelID`
							*/

			";
		#dd('sql_getCat');
		
		$sql_setLen = "SET GLOBAL group_concat_max_len=999999999999999";
		$rs_setLen = $dbConnection->db_query($sql_setLen);
		
		$rs_getCat = $dbConnection->db_query($sql_getCat);

		$arrProductsData = array();

		while($ds_getCat = mysqli_fetch_assoc($rs_getCat)){
			foreach(array_keys($ds_getCat) as $field){
				$arrProductsData[$ds_getCat["stockProductsParentID"]][$ds_getCat["stockProductsID"]][$field] = $ds_getCat[$field];
			}
			/*
			if($ds_getCat["productionLocation"] == 'DE'){
				$arrProductsData[$ds_getCat["stockProductsParentID"]][$ds_getCat["stockProductsID"]]["productSum_DE"] = $ds_getCat["productQuantity_DE"];
			}
			else if($ds_getCat["productionLocation"] == 'TR'){
				$arrProductsData[$ds_getCat["stockProductsParentID"]][$ds_getCat["stockProductsID"]]["productSum_TR"] = $ds_getCat["productQuantity_TR"];
			}
			*/
			
			$arrProductsData[$ds_getCat["stockProductsParentID"]][$ds_getCat["stockProductsID"]]["productSum_DE"] = ($ds_getCat["productQuantity_DE"] - $ds_getCat["productQuantity_TR"]);			
			$arrProductsData[$ds_getCat["stockProductsParentID"]][$ds_getCat["stockProductsID"]]["productSum_TR"] = $ds_getCat["productQuantity_TR"];			
		}
	// EOF GET ALL PRODUCTS

	// BOF GET PRODUCTS STOCK AND USAGE
		$sql_getProductsUsage = "
				SELECT

					`productCategories`.`stockProductCategoriesProductNumber`,
					`productCategories`.`stockProductCategoriesProductNumber_bctr`,
					`productCategories`.`stockProductCategoriesProductNumber_b3`,
					`productCategories`.`stockProductCategoriesName`,
					`productCategories`.`stockProductCategoriesID`,

					SUM(IFNULL(`stockProductReceipts`.`productReceipts`, 0)) AS `productReceipts`,
					SUM(IFNULL(`ordersProcess`.`productProcessTotal`, 0)) AS `productProcessTotal`,

					SUM(IFNULL(`ordersProductionsTransfer`.`productUsageTotal_TR`, 0)) AS `productUsageTotal_TR`,
					SUM(IFNULL(`ordersProductionsTransfer`.`productUsageTotal_TR2`, 0)) AS `productUsageTotal_TR2`,

					(SUM(IFNULL(`stockProductReceipts`.`productReceipts`, 0)) - SUM(IFNULL(`ordersProcess`.`productProcessTotal`, 0)) + SUM(IFNULL(`ordersProductionsTransfer`.`productUsageTotal_TR`, 0))) AS `productsCurrentStock`,

					(IFNULL(`stockProductReceipts`.`productReceipts`, 0) - IFNULL(`ordersProcess`.`productProcessTotal`, 0) + IFNULL(`ordersProductionsTransfer`.`productUsageTotal_TR`, 0)) AS `productsCurrentStock2`

				FROM `" . TABLE_STOCK_PRODUCT_CATEGORIES . "` AS `productCategories`

				LEFT JOIN `" . TABLE_GTIN_CODES . "`
				ON(`productCategories`.`stockProductCategoriesProductNumber_bctr` = `" . TABLE_GTIN_CODES . "`.`codesGtinProductNumber`)

				LEFT JOIN (
					SELECT
						SUM(IFNULL(`productStockQuantity`, 0)) AS `productReceipts`,
						`productStockItemNumber`,
						`productStockProductID`
						FROM `" . TABLE_STOCK_PRODUCT_RECEIPTS . "`
						GROUP BY `productStockProductID`
				) AS `stockProductReceipts`
				ON(`productCategories`.`stockProductCategoriesID` = `stockProductReceipts`.`productStockProductID`)

				LEFT JOIN (
					SELECT
						SUM(IFNULL(`ordersArtikelMenge`, 0)) AS `productProcessTotal`,
						`ordersArtikelNummer`,
						`ordersArtikelID`
					FROM `" . TABLE_ORDERS . "`
					WHERE 1
						AND (
							`ordersStatus` = '3'
							OR `ordersStatus` = '4'
							OR `ordersStatus` = '7'
						)
					GROUP BY `ordersArtikelID`
				) AS `ordersProcess`
				ON(`productCategories`.`stockProductCategoriesID` = `ordersProcess`.`ordersArtikelID`)

				LEFT JOIN (
					SELECT
						SUM(IFNULL(`ordersProductionsTransferOrdersProductQuantity`, 0)) AS `productUsageTotal_TR`,
						SUM(IFNULL(`ordersProductionsTransferOrdersProductOriginalQuantity`, 0)) AS `productUsageTotal_TR2`,
						`ordersProductionsTransferOrdersProductNumber`,
						`ordersProductionsTransferOrdersProductID`
					FROM `" . TABLE_PRODUCTIONS_TRANSFER . "`
					WHERE 1
						AND `ordersProductionsTransferProductionStatus` != '6'
					GROUP BY `ordersProductionsTransferOrdersProductID`
				) AS `ordersProductionsTransfer`
				ON(`productCategories`.`stockProductCategoriesID` = `ordersProductionsTransfer`.`ordersProductionsTransferOrdersProductID`)


				WHERE 1
					AND `productCategories`.`stockProductCategoriesParentID` != '000'
					" . $sqlWhere . "
				GROUP BY `productCategories`.`stockProductCategoriesID`	
				
			";
#dd('sql_getProductsUsage');
		$rs_getProductsUsage = $dbConnection->db_query($sql_getProductsUsage);
		$arrProductsUsageData = array();
		while($ds_getProductsUsage = mysqli_fetch_assoc($rs_getProductsUsage)){
			foreach(array_keys($ds_getProductsUsage) as $field){
				$arrProductsUsageData[$ds_getProductsUsage["stockProductCategoriesID"]][$field] = $ds_getProductsUsage[$field];
			}
		}

	// EOF GET PRODUCTS STOCK AND USAGE
?>
<?php
	require_once('inc/headerHTML.inc.php');
	$thisTitle = "Waren-Bestandslisten";
	$headerHTML = preg_replace("/{###TITLE###}/", strip_tags($thisTitle), $headerHTML);
	echo $headerHTML;

	require_once(FILE_MENUE_TOP);
?>

	<div id="menueSidebarToggleArea">
		<img src="layout/icons/iconToggle.png" class="buttonToggleSidebarMenue" width="10" height="10" alt="" title="Seitennavigation ein-/ausblenden" />
		<div id="menueSidebarToggleContent">
			<?php require_once(FILE_MENUE_SIDEBAR); ?>
			<div class="clear"></div>
		</div>
	</div>
	<div id="contentArea2">
		<div id="contentAreaElements">
			<h1><?php if(SHOW_PAGE_TITLE_ICONS) { echo '<img src="' . PATH_ICONS_MENUE_TITLES . 'editStock.png" alt="" />'; } ?> <?php echo $thisTitle; ?></h1>

			<div class="contentDisplay">

				<?php if($_COOKIE["isAdmin"] == '1'){ ?>
					<div style="text-align:right">
						<a href="displayProductStockLists_OLD.php">Alte Liste zur Kontrolle</a>
					</div>
				<?php }?>

				<div id="searchFilterArea">
				<form name="formFilterSearch" method="post" action="<?php echo $_SERVER["PHP_SELF"]; ?>">
				<table border="0" cellpadding="0" cellspacing="0" class="searchFilterContent">
					<tr>
						<td>
							<label for="searchProductNumber">Artikelnummer:</label>
							<input type="text" name="searchProductNumber" id="searchProductNumber" class="inputField_70" />
						</td>
						<td>
							<label for="searchProductName">Artikelname:</label>
							<input type="text" name="searchProductName" id="searchProductName" class="inputField_120" />
						</td>
						<td>
							<label for="searchProductCategory">Artikelkategorie:</label>
							<select name="searchProductCategory" id="searchProductCategory" class="inputField_120" >
								<option value="">Alle Kategorien</option>
								<?php
									if(!empty($arrProductsCategoryData)){
										foreach($arrProductsCategoryData as $thisKey => $thisValue){
											echo '<option value="' . $thisValue["stockProductCategoriesLevelID"] . '">' . htmlentities($thisValue["stockProductCategoriesName"]) . '</option>';
										}
									}
								?>
							</select>
						</td>
						<td>
							<label for="searchWord">Suchbegriff:</label>
							<input type="text" name="searchWord" id="searchWord" class="inputField_70" />
						</td>
						<td>
							<label for="searchDeactivatedProducts">Deaktivierte anzeigen?:</label>
							<input type="checkbox" name="searchDeactivatedProducts" id="searchDeactivatedProducts" value="1" />
						</td>
						<td>
							<label for="searchForeignProducts">Fremdartikel anzeigen?:</label>
							<input type="checkbox" name="searchForeignProducts" id="searchForeignProducts" value="1" />
						</td>
						<td>
							<input type="submit" name="submitformFilterSearch" id="submitformFilterSearch" class="inputButton0" value="Suchen" />
						</td>
					</tr>
				</table>
				</form>
			</div>

				<?php
					if(!empty($arrProductsData)){

						echo '<table border="0" cellpadding="0" cellspacing="0" width="100%" class="displayOrders" style="width:100% important;">';
						echo '<thead>';
						echo '<tr>';

						echo '<th>#</th>';

						echo '<th style="width:200px;">Art-Nr.</th>';
						echo '<th>Art-Name</th>';
						echo '<th class="spacer"></th>';
						#echo '<th style="width:100px;">EAN-Nr.</th>';
						echo '<th style="width:100px;">VE</th>';
						echo '<th class="spacer"></th>';
						echo '<th style="width:100px;" colspan="2">Ben&ouml;tigt <img src="layout/flags/flag_DE.gif" width="16" height="12" alt="DE" title="" /></th>';
						echo '<th style="width:100px;">Drucke <img src="layout/flags/flag_DE.gif" width="16" height="12" alt="DE" title="" /></th>';
						echo '<th class="spacer"></th>';
						echo '<th style="width:100px;" colspan="2">Ben&ouml;tigt <img src="layout/flags/flag_TR.gif" width="16" height="12" alt="TR" title="" /></th>';
						echo '<th style="width:100px;">Drucke <img src="layout/flags/flag_TR.gif" width="16" height="12" alt="TR" title="" /></th>';
						echo '<th class="spacer"></th>';
						echo '<th style="width:100px;" colspan="2">Ben&ouml;tigt GESAMT</th>';
						echo '<th>Gesamt-Drucke</th>';
						echo '<th class="spacer"></th>';
						echo '<th style="width:100px;" colspan="2">Lagerbestand <img src="layout/flags/flag_DE.gif" width="16" height="12" alt="DE" title="" /></th>';
											
						echo '<th class="spacer"></th>';
						#echo '<th>Aktiv?</th>';
						
						echo '<th style="width:50px;">Aktion</th>';

						echo '</tr>';

						echo '</thead>';

						echo '<tbody>';

						$countRow = 0;

						$countRowLevel1 = 0;
						$countRowLevel2 = 0;

						foreach($arrProductsData as $thisCategoryKey => $thisCategoryValue){

							// BOF CATEGORIES							
								echo '<tr style="font-weight:bold;" class="row2">';

								echo '<td style="text-align:right;">';
								echo '<b>' . ($countRowLevel1 + 1). '.</b>';
								echo '</td>';

								echo '<td colspan="22">';
								echo '&#9632; ' . htmlentities($arrProductsCategoryData[$thisCategoryKey]["stockProductCategoriesName"]);
								echo '</td>';

								echo '</tr>';									
							// EOF CATEGORIES

							// BOF PRODUCTS
								foreach($thisCategoryValue as $thisProductsKey => $thisProductsValue){
									$countPrintsTotal = 0;
									$arrLocationCountPrintsTotal = array();
									
									#dd('thisProductsValue');
									if($countRowLevel1%2 == 0){ $rowClass = 'row0'; }
									else { $rowClass = 'row1'; }

									if($countRowLevel2%2 == 0){ $rowClass = 'row0'; }
									else { $rowClass = 'row1'; }

									if($thisProductsValue["stockProductsActive"] != '1'){
										$rowClass = 'row6';
									}
									else if(($thisProductsValue["productSum_DE"] + $thisProductsValue["productSum_TR"]) == 0){
										#$rowClass = 'row10';
									}

									if($thisProductsValue["stockProductData"] != ''){									
										$arrStockProductData = explode("###", $thisProductsValue["stockProductData"]);
										$arrProductData = array();
										if(!empty($arrStockProductData)){
											foreach($arrStockProductData as $thisStockProductDataKey => $thisStockProductDataValue){
												$arrTemp = explode("|", $thisStockProductDataValue);
												$arrProductData[$arrTemp[0]][] = array($arrTemp[1], $arrTemp[2]);												
											}
										}										
										$thisVeName = $arrProductData["Verpackungseinheiten"][0][0];
										$thisVeFaktor = $arrProductData["Verpackungseinheiten"][0][1];										
									}
									
									if($thisVeFaktor == '' || $thisVeFaktor == 0){
										$thisVeFaktor = 1;
										$thisVeName = 'St&uuml;ck';
									}
									
									if(round($arrProductsUsageData[$thisProductsValue["stockProductsID"]]["productsCurrentStock"] / $thisVeFaktor) < 10){
										$rowClass = 'row10';
									}
									else if(round($arrProductsUsageData[$thisProductsValue["stockProductsID"]]["productsCurrentStock"] / $thisVeFaktor) < 20){
										$rowClass = 'row2';
									}
									else{
										$rowClass = 'row3';
									}

									echo '<tr class="'.$rowClass.'">';

									echo '<td style="text-align:right;" title="' . $thisProductsValue["stockProductsID"] . '">';									
									echo '<b>' . ($countRowLevel1 + 1). '_' . ($countRowLevel2 + 1). '.</b>';
									echo '</td>';

									echo '<td style="white-space:nowrap;">';
									echo '<img src="layout/icons/subCat.png" width="19" height="11" alt="" />';
									echo $thisProductsValue["stockProductsProductNumber"];
									echo '</td>';

									echo '<td style="white-space:nowrap;">';
									echo $thisProductsValue["stockProductsName"];

									if($thisProductsValue["ordersIDs"] != ''){										
										
										echo ' <img src="layout/icons/iconInfo.png" class="buttonShowProductionDetails" width="12" height="12" alt="Info" title="Details anzeigen" />';
										echo '<div class="productionDetailsArea" style="display:none;position:absolute;top:auto;left:auto;background-color:#FFF;padding:10px;border:1px solid #333;box-shadow: 6px 6px 8px #333;">';

										echo '<table cellpadding="0" cellspacing="0" width="100%">';

										echo '<tr>';
										echo '<th style="width:20px;">#</th>';
										echo '<th>ID</th>';
										echo '<th>KNR</th>';
										echo '<th>Kunde</th>';
										echo '<th>Menge</th>';
										echo '<th>Datum</th>';
										echo '<th>Status</th>';
										echo '<th>Anz. Farben</th>';
										echo '<th>Umrandung</th>';
										echo '<th>Klarlack</th>';
										echo '<th>Druckart</th>';
										echo '<th>Gesamt-Drucke</th>';
										echo '<th>Ort</th>';
										echo '</tr>';

										$countStockDetails = 0;

										$arrThisOrderDetails = explode("###", $thisProductsValue["ordersDetails"]);
										foreach($arrThisOrderDetails as $thisOrderDetailsData){
											$arrOrderDetails = explode(":", $thisOrderDetailsData);											

											if($countStockDetails%2 == 0){ $rowClass = 'row0'; }
											else { $rowClass = 'row1'; }
										
											echo '<tr class="' . $rowClass . '">';
											echo '<td style="text-align:right"><b>' . ($countStockDetails + 1) . '.</b></td>';
											echo '<td><b><a href="' . PAGE_DISPLAY_ORDERS. '?searchBoxProduction=' . $arrOrderDetails[0] . '">' . $arrOrderDetails[0] . '</a></b></td>';
											echo '<td><b><a href="' . PAGE_EDIT_CUSTOMER. '?searchBoxCustomer=' . $arrOrderDetails[1] . '">' . $arrOrderDetails[1] . '</a></b></td>';
											echo '<td>' . htmlentities($arrOrderDetails[2]) . '</td>';
											echo '<td style="text-align:right">' . $arrOrderDetails[3] . '</td>';
											echo '<td>' . formatDate($arrOrderDetails[4], 'display') . '</td>';
											echo '<td>' . htmlentities($arrOrderStatusTypeDatas[$arrOrderDetails[5]]["orderStatusTypesName"]) . '</td>';
											
											echo '<td style="text-align:right">' . $arrOrderDetails[6] . '</td>';
											echo '<td>' . $arrOrderDetails[7] . '</td>';
											echo '<td>' . $arrOrderDetails[8] . '</td>';
											echo '<td>' . htmlentities($arrPrintTypes[$arrOrderDetails[9]]["printTypesName"]) . '</td>';											
											
											$countPrintsThis = ($arrOrderDetails[3] * $arrOrderDetails[6]);
											$countPrintsTotal += $countPrintsThis;											
											echo '<td style="text-align:right">' . $countPrintsThis . '</td>';
											
											$arrLocationCountPrintsTotal[$arrOrderDetails[10]] += $countPrintsThis;	
											
											echo '<td><img src="layout/flags/flag_' . $arrOrderDetails[10] . '.gif" width="20" height="14" alt="' . $arrOrderDetails[10] . '" title="" /></td>';
											echo '</tr>';

											$countStockDetails++;
										}

										echo '</table>';
										echo '</div>';
									}
									echo '</td>';

									echo '<td class="spacer"></td>';

									#echo '<td style="white-space:nowrap;">';
									#echo $thisProductsValue["codesGtinNumber"];
									#echo '</td>';

									echo '<td style="white-space:nowrap;">';
									echo $thisVeName . ' [Faktor ' .  number_format($thisVeFaktor, 0, ',', '.') . ']';
									#$arrProductsVPE[$thisProductsDataValue["productDataProductVpeID"]]["productVpeName"];
									echo '</td>';

									echo '<td class="spacer"></td>';

									echo '<td style="text-align:right;background-color:#ffffcf;font-weight:bold;">';
									echo number_format($thisProductsValue["productSum_DE"], 0, ',', '.');
									echo '</td>';
									echo '<td style="text-align:right;white-space:nowrap;">';
									echo '' . round($thisProductsValue["productSum_DE"] / $thisVeFaktor) . ' VE';
									echo '</td>';
									
									echo '<td style="text-align:right;background-color:#dffeca;font-weight:bold;">';
									echo number_format($arrLocationCountPrintsTotal["DE"], 0, ',', '.');
									echo '</td>';
									echo '<td class="spacer"></td>';

									echo '<td style="text-align:right;background-color:#ffffcf;font-weight:bold;">';
									echo number_format($thisProductsValue["productSum_TR"], 0, ',', '.');
									echo '</td>';
									echo '<td style="text-align:right;white-space:nowrap;">';
									echo '' . round($thisProductsValue["productSum_TR"] / $thisVeFaktor) . ' VE';
									echo '</td>';
									echo '<td style="text-align:right;background-color:#dffeca;font-weight:bold;">';
									echo number_format($arrLocationCountPrintsTotal["TR"], 0, ',', '.');
									echo '</td>';

									echo '<td class="spacer"></td>';

									echo '<td style="text-align:right;background-color:#ffffcf;font-weight:bold;">';
									echo number_format(($thisProductsValue["productSum_DE"] + $thisProductsValue["productSum_TR"]), 0, ',', '.');
									echo '</td>';
									echo '<td style="text-align:right;white-space:nowrap;">';
									echo '' . round(($thisProductsValue["productSum_DE"] + $thisProductsValue["productSum_TR"]) / $thisVeFaktor) . ' VE';
									echo '</td>';								
									
									echo '<td style="text-align:right;background-color:#dffeca;font-weight:bold;">';
									echo number_format($countPrintsTotal, 0, ',', '.');
									echo '</td>';

									echo '<td class="spacer"></td>';

									echo '<td style="text-align:right;background-color:#ffffcf;font-weight:bold;">';
									#echo $thisProductsValue["orderCategoriesUseInStock"];
									echo number_format($arrProductsUsageData[$thisProductsValue["stockProductsID"]]["productsCurrentStock"], 0, ',', '.');
									echo '</td>';
									
									echo '<td style="text-align:right;white-space:nowrap;">';									
									echo '' .  number_format(round($arrProductsUsageData[$thisProductsValue["stockProductsID"]]["productsCurrentStock"] / $thisVeFaktor), 0, ',', '.') . ' VE';
									if(round($arrProductsUsageData[$thisProductsValue["stockProductsID"]]["productsCurrentStock"] / $thisVeFaktor) < 10){
										echo ' <img src="layout/icons/iconAttention.png" class="iconAttention" height="14" width="14" alt="" />';
									}
									echo '</td>';

									echo '<td class="spacer"></td>';									
									
									echo '<td>';
									if($arrGetUserRights["editStocks"]) {
										echo '<span class="toolItem"><a href="' . PAGE_PRODUCTS_STOCK_EDIT . '?editID='.$thisProductsValue["stockProductsID"] . '"><img src="layout/icons/iconEdit.gif" width="16" height="16" title="Warenbestand bearbeiten" alt="Bearbeiten" /></a></span>';
									}
									else {
										echo '<span class="toolItem"><img src="layout/spacer.gif" width="16" height="16" alt="" /></span>';
									}
									echo '</td>';

									echo '</tr>';

									$countRowLevel2++;									
								}
								$countRowLevel1++;
							// EOF PRODUCTS
						}

						echo '</tbody>';

						echo '</table>';
					}
					else {
						echo '<p class="infoArea">Es wurden keine Daten gefunden!</p>';
					}
				?>

			</div>
		</div>
	</div>

<script language="javascript" type="text/javascript">
	$(document).ready(function() {

		/*
		$('#searchWord').keyup(function () {
			loadSuggestions('searchProduct', [{'triggerElement': '#searchWord', 'fieldText': '#searchWord'}], 1);
		});
		$('#editOrdersArtikelBezeichnung').keyup(function () {
			loadSuggestions('searchProductName', [{'triggerElement': '#editOrdersArtikelBezeichnung', 'fieldNumber': '#editOrdersArtikelNummer', 'fieldName': '#editOrdersArtikelBezeichnung', 'fieldID': '#editOrdersArtikelID'}], 0);
		});
		$('#editOrdersArtikelNummer').keyup(function () {
			loadSuggestions('searchProductNumber', [{'triggerElement': '#editOrdersArtikelNummer', 'fieldNumber': '#editOrdersArtikelNummer', 'fieldName': '#editOrdersArtikelBezeichnung', 'fieldID': '#editOrdersArtikelID'}], 1);
		});
		*/
		colorRowMouseOver('.displayProducts tbody tr');
		$('.buttonNotice').click(function () {
			loadNotice($(this));
		});
		$('.buttonLoadProductDetails').click(function(){
			loadProductDetails($(this).attr('alt'));
		});
		$(".displayProducts .displayProductImage img").css('display', 'none').show().lazyload({
			container: $(".displayProductImage"),
			effect : "fadeIn"
		});

		$(function() {
			$.datepicker.setDefaults($.datepicker.regional[""]);
			$('#newStockDate').datepicker($.datepicker.regional["de"]);
			$('#newStockDate').datepicker("option", "maxDate", "0" );
			colorRowMouseOver('.displayOrders tbody tr');
			$('.buttonShopOrderInfo').css('cursor', 'pointer');
			$('.buttonShopOrderInfo').click(function () {
				loadOrderDetails($(this), '<?php echo BASEPATH; ?>', 'Details des Verbrauchs');
			});
		});

		$('.buttonToggleSidebarMenue').click(function() {
			$('#menueSidebarToggleContent').toggle();
		});
		$('.buttonShowProductionDetails').css('cursor', 'pointer');
		$('.buttonShowProductionDetails').click(function() {
			$('.productionDetailsArea').not($(this).next('.productionDetailsArea')).hide();
			$(this).next('.productionDetailsArea').toggle();
		});
		
		blink('.iconAttention', 1000, 0.1, 400);
	});
</script>

<?php require_once('inc/footerHTML.inc.php'); ?>