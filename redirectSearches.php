<?php
	require_once('config/configFiles.inc.php');

	if(trim($_POST["searchBoxCustomer"]) != "") {
		header('location:' . PAGE_EDIT_CUSTOMER . '?searchBoxCustomer=' . trim($_POST["searchBoxCustomer"]));
		exit;
	}

	else if(trim($_POST["searchBoxFile"]) != "") {
		header('location:' . PAGE_DOWNLOAD_DOCUMENTS . '?searchBoxFile=' . trim($_POST["searchBoxFile"]));
		exit;
	}

	else if(trim($_POST["searchBoxPersonnel"]) != "") {
		header('location:' . PAGE_DISPLAY_PERSONNEL . '?searchBoxPersonnel=' . trim($_POST["searchBoxPersonnel"]));
		exit;
	}

	else if(trim($_POST["searchBoxSalesman"]) != "") {
		header('location:' . PAGE_EDIT_SALESMEN . '?searchBoxSalesman=' . trim($_POST["searchBoxSalesman"]));
		exit;
	}

	else if(trim($_POST["searchBoxProduct"]) != "") {
		header('location:' . PAGE_DISPLAY_PRODUCTS . '?searchBoxProduct=' . trim($_POST["searchBoxProduct"]));
		exit;
	}

	else if(trim($_POST["searchBoxProduction"]) != "") {
		header('location:' . PAGE_DISPLAY_ORDERS . '?searchBoxProduction=' . trim($_POST["searchBoxProduction"]));
		exit;
	}

	else if(trim($_POST["searchBoxSupplier"]) != "") {
		header('location:' . PAGE_EDIT_DISTRIBUTORS . '?searchBoxSupplier=' . trim($_POST["searchBoxSupplier"]));
		exit;
	}

	else if(trim($_POST["searchBoxDeliveryTracking"]) != "") {
		#header('location:' . PAGE_SEARCH_TRACKING . '?searchBoxDeliveryTracking=' . trim($_POST["searchBoxDeliveryTracking"]));
		header('location:' . PAGE_DELIVERY_HISTORY . '?searchBoxDeliveryTracking=' . trim($_POST["searchBoxDeliveryTracking"]));
		exit;
	}

	else {
		header('location:' . PAGE_INDEX);
		exit;
	}
?>