<?php
	require_once('inc/requires.inc.php');

	if(!$arrGetUserRights["openPhpTerm"]) {
		header('location: ' . PAGE_EXIT_LOCATION);
		exit;
	}

	$jswindowMessage = '';
	$warningMessage = '';
	$errorMessage = '';
	$successMessage = '';
	$infoMessage = '';

	$dbConnection = new DB_Connection();
	$db_open = $dbConnection->db_connect();
?>
<?php
	require_once('inc/headerHTML.inc.php');
	$thisTitle = "System-Info";
	$headerHTML = preg_replace("/{###TITLE###}/", strip_tags($thisTitle), $headerHTML);
	echo $headerHTML;

	require_once(FILE_MENUE_TOP);

?>
<div id="mainArea">
	<div id="mainContent">
		<?php require_once(FILE_MENUE_SIDEBAR);	?>
		<div id="contentArea">
			<div id="contentAreaElements">
				<h1><?php if(SHOW_PAGE_TITLE_ICONS) { echo '<img src="' . PATH_ICONS_MENUE_TITLES . 'server.png" alt="" />'; } ?> <?php echo $thisTitle; ?></h1>

				<div class="detailsArea">
					<h3>PhpSysInfo</h3>
					<div class="displayDetails2">
					<iframe id="framePhpSysInfo" class="iFrameModule" width="100%" height="100%" src="<?php echo PATH_SERVER_INFO; ?>"><a href="<?php echo PATH_SERVER_INFO; ?>">System-Info</a></iframe>
					</div>
				</div>

				<hr />

				<div class="detailsArea">
					<h3>Linfo</h3>
					<div class="displayDetails2">
					<iframe id="frameLinfo" class="iFrameModule" width="100%" height="100%" src="<?php echo PATH_SERVER_INFO2; ?>"><a href="<?php echo PATH_SERVER_INFO2; ?>">System-Info</a></iframe>
					</div>
				</div>
			</div>
		</div>
		<div class="clear"></div>
	</div>
	<div class="clear"></div>
</div>

<script language="javascript" type="text/javascript">
	$(document).ready(function() {
		$(function() {
			$( "#datepicker" ).datepicker();
		});
		colorRowMouseOver('.displayOrders tbody tr');


		// BOF ADD TOOGLE BUTTON
			var buttonToggle = '<img src="layout/icons/iconToggle3.png" class="buttonToggleDetails" width="14" height="14" alt="" title="Details ein-/ausblenden" style="float:right;" />';
			$('.detailsArea h3').append(buttonToggle);

			$('.buttonToggleDetails').live('click', function(){
				$(this).parent().next('.displayDetails2').toggle();
				// var thisFrameID = $(this).parent().next('.displayDetails').find('iframe').attr('id');
				var thisFrameID = $(this).parent().parent().find('iframe').attr('id');
				thisFrameID = '' + thisFrameID;
				thisFrameHeight = getFrameHeight(thisFrameID);
				if(thisFrameHeight != '' && thisFrameHeight != undefined){
					$('#' + thisFrameID).css('height', (thisFrameHeight + 0) + 'px');
					$(this).parent().next('.displayDetails2').css('height', (thisFrameHeight + 60) + 'px');
					$(this).parent().next('.displayDetails2').css('max-height', (thisFrameHeight + 60) + 'px');
				}
			});
		// EOF ADD TOOGLE BUTTON

		function getFrameHeight(element) {
			try {
				iframe = document.getElementById(element);
				var newHeight;
				var innerDoc = (iframe.contentDocument) ? iframe.contentDocument : iframe.contentWindow.document;
				if (innerDoc.body.offsetHeight) {
					newHeight = (innerDoc.body.offsetHeight + 32); //Extra height FireFox
				} else if (iframe.Document && iframe.Document.body.scrollHeight) {
					newHeight = (iframe.Document.body.scrollHeight);
				}
				return newHeight;
			}
			catch(err) {
				// alert(err.message);
			}
		}

		function changeHeight(iframe) {
			try {
				iframe = document.getElementById(iframe);
				var innerDoc = (iframe.contentDocument) ? iframe.contentDocument : iframe.contentWindow.document;
				if (innerDoc.body.offsetHeight) {
					/*iframe.height = */alert(innerDoc.body.offsetHeight + 32); //Extra height FireFox
				} else if (iframe.Document && iframe.Document.body.scrollHeight) {
					/*iframe.height = */alert(iframe.Document.body.scrollHeight);
				}
			}
			catch(err) {
				alert(err.message);
			}
		}
	});
</script>

<?php require_once('inc/footerHTML.inc.php'); ?>