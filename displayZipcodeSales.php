<?php
	require_once('inc/requires.inc.php');

	if(!$arrGetUserRights["displayDistribution"]) {
		header('location: ' . PAGE_EXIT_LOCATION);
		exit;
	}

	$jswindowMessage = '';
	$warningMessage = '';
	$errorMessage = '';
	$successMessage = '';
	$infoMessage = '';

	$dbConnection = new DB_Connection();
	$db_open = $dbConnection->db_connect();

	$searchCountry = 81;
	$searchTable = TABLE_ORDER_CONFIRMATIONS; // TABLE_ORDER_INVOICES | TABLE_ORDER_CONFIRMATIONS

	// BOF READ SALESMEN WITH ZIP CODES
		$arrSalesmenWithActiveAreasData = getSalesmenWithActiveAreas();
		if(!empty($arrSalesmenWithActiveAreasData)){
			$arrTemp = array();
			foreach($arrSalesmenWithActiveAreasData as $thisSalesmenWithActiveAreasData){
				$arrTemp[$thisSalesmenWithActiveAreasData["customersID"]] = $thisSalesmenWithActiveAreasData;
			}
			$arrSalesmenWithActiveAreasData = $arrTemp;
		}
	// EOF READ SALESMEN WITH ZIP CODES

	// BOF GET SALESMEN
		#$arrSalesmenZipcodeDatas = getRelationSalesmenZipcode();
		$arrSalesmenZipcodeDatas = getRelationSalesmenZipcode2();

		if(!empty($arrTestAreas)){
			foreach($arrTestAreas as $thisTestAreaKey => $thisTestAreaValue){
				$arrTemp = explode(";", $thisTestAreaValue["area"]);
				if(!empty($arrTemp)){
					foreach($arrTemp as $thisArea){
						$arrSalesmenZipcodeDatas[$thisArea][ "testID_" . $thisTestAreaKey] = array(
							"kundenname" => "Testgebiet " . $thisTestAreaKey . ' - ' . $thisTestAreaValue["name"],
							"kundennummer" => "-",
							"kundenID" => "testID_" . $thisTestAreaKey,
							"kundenPLZ" => $thisArea,
							"kundenPLZaktiv" => 0
						);
					}
				}
			}
		}
	// EOF GET SALESMEN

	// BOF GET ALL DOUBLE AND TRIPLE ZIP CODE AREAS
		$arrAllZipcodes = getAllZipcodes();
		$arrAllDoubleZipCodes = $arrAllZipcodes['double'];
		$arrAllTripleZipCodes = $arrAllZipcodes['triple'];
	// EOF GET ALL DOUBLE AND TRIPLE ZIP CODE AREAS

	function getThisZipcodeSalesmenDatas($thisZipCodeDatasKey, $countEnd){
		global $arrSalesmenZipcodeDatas;

		$arrThisSalesmenDatas = array();

		foreach($arrSalesmenZipcodeDatas[$thisZipCodeDatasKey] as $thisSalesmenZipcodeDatas){
			$arrThisSalesmenDatas[$thisSalesmenZipcodeDatas["kundenID"]] = array("kundenname" => $thisSalesmenZipcodeDatas["kundenname"], "kundennummer" => $thisSalesmenZipcodeDatas["kundennummer"], "kundenID" => $thisSalesmenZipcodeDatas["kundenID"], "kundenPLZ" => $thisSalesmenZipcodeDatas["kundenPLZ"]);
		}

		if(in_array($countEnd, array(10, 100))){
			for($i = 0 ; $i < $countEnd ; $i++){
				if(!empty($arrSalesmenZipcodeDatas[$thisZipCodeDatasKey . $i])){
					foreach($arrSalesmenZipcodeDatas[$thisZipCodeDatasKey . $i] as $thisSalesmenZipcodeDatas){
						#$arrThisSalesmenDatas[$thisSalesmenZipcodeDatas["kundenID"]] = array("kundenname" => $thisSalesmenZipcodeDatas["kundenname"], "kundennummer" => $thisSalesmenZipcodeDatas["kundennummer"], "kundenID" => $thisSalesmenZipcodeDatas["kundenID"], "kundenPLZ" => $thisSalesmenZipcodeDatas["kundenPLZ"]);
						$arrThisSalesmenDatas[] = array("kundenname" => $thisSalesmenZipcodeDatas["kundenname"], "kundennummer" => $thisSalesmenZipcodeDatas["kundennummer"], "kundenID" => $thisSalesmenZipcodeDatas["kundenID"], "kundenPLZ" => $thisSalesmenZipcodeDatas["kundenPLZ"]);
					}
				}
			}
		}
		else if($countEnd == 1){
			if(!empty($arrSalesmenZipcodeDatas[substr($thisZipCodeDatasKey, 0, -1)])){
				foreach($arrSalesmenZipcodeDatas[substr($thisZipCodeDatasKey, 0, -1)] as $thisSalesmenZipcodeDatas){
					$arrThisSalesmenDatas[$thisSalesmenZipcodeDatas["kundenID"]] = array("kundenname" => $thisSalesmenZipcodeDatas["kundenname"], "kundennummer" => $thisSalesmenZipcodeDatas["kundennummer"], "kundenID" => $thisSalesmenZipcodeDatas["kundenID"], "kundenPLZ" => $thisSalesmenZipcodeDatas["kundenPLZ"]);
				}
			}
			/*
			foreach($arrSalesmenZipcodeDatas as $thisSalesmenZipcodeDatasKey => $thisSalesmenZipcodeDatasValue){
				if($_COOKIE["isAdmin"] == '1'){
					echo 'thisSalesmenZipcodeDatasKey<pre>';
					print($thisSalesmenZipcodeDatasKey);
					echo '</pre>';
				}
			}
			*/
		}

		return $arrThisSalesmenDatas;
	}

	if($_POST["submitSearch"] != ""){
		$arrSql = array();
		$arrSql[] = "
				SELECT
						SUM(`orderDocumentsTotalPrice`) AS `totalSum`,
						SUBSTRING(`customersCompanyPLZ`, 1, " . $_POST["searchZipcodeType"] . ") AS `zipcodePart`
						FROM `" . $searchTable . "`

						LEFT JOIN `" . TABLE_CUSTOMERS . "`
						ON(`" . $searchTable . "`.`orderDocumentsCustomerNumber` = `" . TABLE_CUSTOMERS . "`.`customersKundennummer`)

						WHERE 1
							AND `orderDocumentsDocumentDate` >= '" . formatDate($_POST["searchDateStart"], "store") . "'
							AND `orderDocumentsDocumentDate` <= '" . formatDate($_POST["searchDateEnd"], "store") . "'
							AND `customersCompanyCountry` = '" . $searchCountry . "'

						GROUP BY `zipcodePart`

			";

		if(!empty($arrSql)){
			$sql = "
					SELECT
						`tempTable`.`zipcodePart`,
						`tempTable`.`totalSum`

						FROM (
						" . implode(" UNION ", $arrSql). "
						) AS `tempTable`

						ORDER BY `tempTable`.`zipcodePart`
				";

			$rs = $dbConnection->db_query($sql);
			echo mysqli_error();
			while($ds = mysqli_fetch_assoc($rs)){
				foreach(array_keys($ds) as $field){
					$arrZipCodeDatas[$ds["zipcodePart"]][$field] = $ds[$field];
				}
			}
		}
	}
?>
<?php
	require_once('inc/headerHTML.inc.php');
	$thisTitle = "PLZ-Umsatz";

	$headerHTML = preg_replace("/{###TITLE###}/", strip_tags($thisTitle), $headerHTML);
	echo $headerHTML;

	require_once(FILE_MENUE_TOP);
?>

<?php require_once(FILE_MENUE_SIDEBAR); ?>
<div id="contentArea">
	<a name="top"></a>
	<div id="contentAreaElements">
		<h1><?php if(SHOW_PAGE_TITLE_ICONS) { echo '<img src="' . PATH_ICONS_MENUE_TITLES . 'plz.png" alt="" />'; } ?> <?php echo $thisTitle; ?></h1>
		<p class="infoArea">
			<?php
				$thisInfo = '';
				$thisInfo .= 'Ausgewertet werden nur ';
				if(preg_match("/^" . MANDATOR . "_/", $searchTable)){
					$thisInfo .= strtoupper(MANDATOR) . '-';
				}
				if(preg_match("/_orderConfirmations$/", $searchTable)){
					$thisInfo .= 'Auftragsbest&auml;tigungen';
				}
				else if(preg_match("/_orderInvoices$/", $searchTable)){
					$thisInfo .= 'Rechnungen';
				}
				$thisInfo .= '!';
				echo $thisInfo;
			?>
		</p>
		<div id="searchFilterArea">
			<form name="formSearchDatas" method="post" action="<?php echo $_SERVER["PHP_SELF"]; ?>">
			<table border="0" cellpadding="0" cellspacing="0" class="searchFilterContent">
				<tr>
					<td>
						<label for="searchDateStart">von:</label>
						<input type="text" name="searchDateStart" id="searchDateStart" maxlength="2" class="inputField_70" readonly="readonly" value="<?php if($_POST["searchDateStart"] != '') { echo $_POST["searchDateStart"]; } else { echo date ('d.m.Y'); } ?>" />
					</td>

					<td>
						<label for="searchDateEnd">bis:</label>
						<input type="text" name="searchDateEnd" id="searchDateEnd" maxlength="2" class="inputField_70" readonly="readonly" value="<?php if($_POST["searchDateEnd"] != '') { echo $_POST["searchDateEnd"]; } else { echo date ('d.m.Y'); } ?>" />
					</td>

					<td>
						<label for="searchZipcodeType">PLZ:</label>
						<select name="searchZipcodeType" id="searchZipcodeType" class="inputSelect_200">
							<?php
								for($i = 1 ; $i < 4 ; $i++){
									$selected = '';
									if($_POST["searchZipcodeType"] == $i) {
										$selected = ' selected = "selected" ';
									}
									echo '<option value="' . $i . '" ' . $selected . ' >' . $i . '-stellige PLZ-Gebiete</option>';
								}
							?>
						</select>
					</td>

					<td>
						<input type="hidden" name="editID" id="editID" value="" />
						<input type="submit" name="submitSearch" class="inputButton0" value="Anzeigen" />
					</td>
				</tr>
			</table>
			<input type="hidden" name="submitSearchForm" id="submitSearchForm" value="1" />
			</form>
		</div>
		<div class="contentDisplay">
			<?php displayMessages(); ?>

			<?php if(!empty($arrZipCodeDatas)) { ?>
			<table width="100%" cellpadding="0" cellspacing="0" class="displayOrders">
				<thead>
					<tr>
						<th style="width:70px;">PLZ</th>
						<th>Gebiet</th>
						<th>Verteter</th>
						<th style="width:70px;">Umsatz</th>
					</tr>
				</thead>
				<tbody>
				<?php
					$countRow = 0;
					$totalSum = 0;
					foreach($arrZipCodeDatas as $thisZipCodeDatasKey => $thisZipCodeDatasValue){

						$rowClass = 'row0';
						if($countRow%2 == 0) {
							$rowClass = 'row1';
						}

						$rowStyle = '';
						if($thisInterval < SALEMESMEN_AREA_CONTRACT_START_DATE){
							$rowStyle = 'opacity:1;';
						}

						echo '<tr class="' . $rowClass . '">';

						echo '<td>';
						echo '<b>' . $thisZipCodeDatasKey . '</b>';
						echo '</td>';

						echo '<td>';
						if(strlen($thisZipCodeDatasKey) > 1){
							$thisAreaDescription = htmlentities($arrAllDoubleZipCodes[substr($thisZipCodeDatasKey, 0, 2)]["zipcodesDoubleDescription"]);
						}
						else {
							$thisAreaDescription = '';
							for($i = 0 ; $i < 10 ; $i++){
								if($arrAllDoubleZipCodes[$thisZipCodeDatasKey . $i]["zipcodesDoubleDescription"] != ''){
									$thisAreaDescription .= '&bull; ' . $thisZipCodeDatasKey . $i . ': ' . htmlentities($arrAllDoubleZipCodes[$thisZipCodeDatasKey . $i]["zipcodesDoubleDescription"]) . '<br />';
								}
							}
						}
						echo $thisAreaDescription;
						echo '</td>';

						echo '<td>';

						$thisAreaSalesmen = '';
						$arrThisSalesmenDatas = array();

						if(strlen($thisZipCodeDatasKey) == 1){
							$arrThisSalesmenDatas = getThisZipcodeSalesmenDatas($thisZipCodeDatasKey, 100);
						}
						else if(strlen($thisZipCodeDatasKey) == 2){
							$arrThisSalesmenDatas = getThisZipcodeSalesmenDatas($thisZipCodeDatasKey, 10);
						}
						else if(strlen($thisZipCodeDatasKey) == 3){
							$arrThisSalesmenDatas = getThisZipcodeSalesmenDatas($thisZipCodeDatasKey, 1);
						}
						$thisZipcodeMarker = '';
						if(!empty($arrThisSalesmenDatas)){
							foreach($arrThisSalesmenDatas as $thisSalesmenDatas){
								$thisZipCodeBorder = '';
								if($thisZipcodeMarker != $thisSalesmenDatas["kundenPLZ"] && count($arrThisSalesmenDatas) > 1){
									$thisZipcodeMarker = $thisSalesmenDatas["kundenPLZ"];
									$thisZipCodeBorder = 'border-top:1px solid #AAA;';
								}
								$thisAreaSalesmen .= '<div style="margin:0;padding:0;white-space:nowrap;' . $thisZipCodeBorder . '">';
								#$thisAreaSalesmen .= '&bull; ' . $arrSalesmenWithActiveAreasData[$thisSalesmenDatas["kundenID"]]["salesmenAreas"] . ': ' . htmlentities($thisSalesmenDatas["kundenname"]) . ' (' . $thisSalesmenDatas["kundennummer"] . ')' . '<br />';
								$thisAreaSalesmen .= '&bull; ' . $thisSalesmenDatas["kundenPLZ"] . ': ' . htmlentities($thisSalesmenDatas["kundenname"]) . ' (' . $thisSalesmenDatas["kundennummer"] . ')' . '';
								$thisAreaSalesmen .= '</div>';
							}
						}
						echo $thisAreaSalesmen;
						echo '</td>';

						echo '<td style="text-align:right;background-color:#FEFFAF;font-weight:bold;">';
						echo number_format($thisZipCodeDatasValue["totalSum"], 2, ',', '.');
						$totalSum += $thisZipCodeDatasValue["totalSum"];
						echo '</td>';

						echo '</tr>';
						$countRow++;
					}
				?>
				<tr>
					<td colspan="4"><hr /></td>
				<tr>
					<td><b>Gesamt</b></td>
					<td colspan="2"></td>
					<td style="text-align:right;background-color:#FEFFAF;font-weight:bold;"><?php echo number_format($totalSum, 2, ',', '.'); ?></td>
				</tr>

				</tbody>
			</table>
			<?php } ?>

		</div>
	</div>
</div>

<script language="javascript" type="text/javascript">
	$(document).ready(function() {
		$(function() {
			colorRowMouseOver('.displayOrders tbody tr');
			$.datepicker.setDefaults($.datepicker.regional[""]);
			$('#searchDateStart').datepicker($.datepicker.regional["de"]);
			$('#searchDateEnd').datepicker($.datepicker.regional["de"]);
			$('#searchDateStart').datepicker("option", "maxDate", "0");
			$('#searchDateEnd').datepicker("option", "maxDate", "0");
		});
	});
</script>
<?php require_once('inc/footerHTML.inc.php'); ?>