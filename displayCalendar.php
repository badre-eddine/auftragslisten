<?php
	require_once('inc/requires.inc.php');

	if(!$arrGetUserRights["displayCalendar"] && !$arrGetUserRights["editCalendar"]) {
		header('location: ' . PAGE_EXIT_LOCATION);
		exit;
	}

	if($_REQUEST["selectedMonth"] == '') { $_REQUEST["selectedMonth"] = date('m'); }
	if($_REQUEST["selectedYear"] == '') { $_REQUEST["selectedYear"] = date('Y'); }

	$jswindowMessage = "";
	$warningMessage = '';
	$errorMessage = '';
	$successMessage = '';
	$infoMessage = '';

	$dbConnection = new DB_Connection();
	$db_open = $dbConnection->db_connect();

	/*
			SELECT
				@rownum := @rownum + 1 AS `rownum`,
				`u`.`holidaysHolydayName`
			FROM (
				SELECT `holidaysHolydayName`
				FROM `common_holidays`
				WHERE `holidaysType` = 'gesetzlich'
				GROUP BY `holidaysHolydayName`
			) AS`u`,
			(SELECT @rownum := 0) AS `r`


			#############################

			UPDATE `common_holidays`

			LEFT JOIN (
							SELECT
								@rownum := @rownum + 1 AS `rownum`,
								`u`.`holidaysHolydayName`
							FROM (
								SELECT `holidaysHolydayName`
								FROM `common_holidays`
								WHERE `holidaysType` = 'gesetzlich'
								GROUP BY `holidaysHolydayName`
							) AS`u`,
							(SELECT @rownum := 0) AS `r`
			) AS `temp`
			ON (`common_holidays`.`holidaysHolydayName` = `temp`.`holidaysHolydayName` )

			SET `holidaysHolydayNameType` = `temp`.`rownum`
	*/

	// BOF GET VACATIONS AND DISEASES
		$sql = "SELECT
					`personnelCalendarID`,
					`personnelCalendarPersonnelID`,
					UNIX_TIMESTAMP(`personnelCalendarStart`) AS `personnelCalendarStartTimestamp`,
					UNIX_TIMESTAMP(`personnelCalendarEnd`) AS `personnelCalendarEndTimestamp`,
					`personnelCalendarStart`,
					`personnelCalendarEnd`,
					`personnelCalendarWorkDays`,
					`personnelCalendarTotalDays`,
					`personnelCalendarReason`,

					`personnelFirstName`,
					`personnelLastName`

					FROM `" . TABLE_PERSONNEL_CALENDAR . "`
					LEFT JOIN `" . TABLE_PERSONNEL . "`
					ON (`" . TABLE_PERSONNEL_CALENDAR . "`.`personnelCalendarPersonnelID` = `" . TABLE_PERSONNEL . "`.`personnelID`)

					WHERE
						`personnelCalendarStart` LIKE '" . $_REQUEST["selectedYear"] . '-' . $_REQUEST["selectedMonth"] . "%'
						AND `personnelCalendarReason` != 'ueberstunden'

				UNION

				SELECT
					`personnelCalendarID`,
					`personnelCalendarPersonnelID`,
					UNIX_TIMESTAMP(`personnelCalendarStart`) AS `personnelCalendarStartTimestamp`,
					UNIX_TIMESTAMP(`personnelCalendarEnd`) AS `personnelCalendarEndTimestamp`,
					`personnelCalendarStart`,
					`personnelCalendarEnd`,
					`personnelCalendarWorkDays`,
					`personnelCalendarTotalDays`,
					`personnelCalendarReason`,

					`personnelFirstName`,
					`personnelLastName`

					FROM `" . TABLE_PERSONNEL_CALENDAR . "`
					LEFT JOIN `" . TABLE_PERSONNEL . "`
					ON (`" . TABLE_PERSONNEL_CALENDAR . "`.`personnelCalendarPersonnelID` = `" . TABLE_PERSONNEL . "`.`personnelID`)

					WHERE
						`personnelCalendarEnd` LIKE '" . $_REQUEST["selectedYear"] . '-' . $_REQUEST["selectedMonth"] . "%'
						AND `personnelCalendarReason` != 'ueberstunden'

				ORDER BY `personnelCalendarReason` DESC, `personnelCalendarStartTimestamp` ASC
		";

		$rs = $dbConnection->db_query($sql);
		$selectedPersonnelCalendarDatas = array();
		while($ds = mysqli_fetch_assoc($rs)) {
			foreach(array_keys($ds) as $field) {
				$selectedPersonnelCalendarDatas[$ds["personnelCalendarID"]][$field] = $ds[$field];
			}
		}
	// EOF GET VACATIONS AND DISEASES

	// BOF GET OFFICIAL HOLIDAYS
		$sql = "SELECT
					`holidaysDate`,
					`holidaysHolydayName`,
					`holidaysType`,
					`holidaysActive`
				FROM `" . TABLE_OFFICIAL_HOLIDAYS . "`
				WHERE `holidaysType` = 'gesetzlich'
				AND `holidaysDate` LIKE '" . $_REQUEST["selectedYear"] . '-' . $_REQUEST["selectedMonth"] . "%'
		";
		$rs = $dbConnection->db_query($sql);
		$selectedHolidaysDatas = array();
		while($ds = mysqli_fetch_assoc($rs)) {
			foreach(array_keys($ds) as $field) {
				$selectedHolidaysDatas[$ds["holidaysDate"]][$field] = $ds[$field];
			}
		}
	// EOF GET OFFICIAL HOLIDAYS

	// BOF GET BIRTHDAYS
		$sql = "SELECT
					`personnelID`,
					`personnelFirstName`,
					`personnelLastName`,
					`personnelBirthday`,
					`personnelActive`,

					CONCAT(DATE_FORMAT(`personnelBirthday`, '%m'), '-', DATE_FORMAT(`personnelBirthday`, '%d')) AS `personnelBirthdayDate`

				FROM `" . TABLE_PERSONNEL . "`
				WHERE `personnelActive` = '1'
				AND DATE_FORMAT(`personnelBirthday`, '%m') = '" . $_REQUEST["selectedMonth"] . "'
		";
		$rs = $dbConnection->db_query($sql);
		$selectedBirthdayDatas = array();
		while($ds = mysqli_fetch_assoc($rs)) {
			foreach(array_keys($ds) as $field) {
				$selectedBirthdayDatas[$ds["personnelBirthdayDate"]][$ds["personnelID"]][$field] = $ds[$field];
			}
		}
	// EOF GET BIRTHDAYS
?>
<?php
	require_once('inc/headerHTML.inc.php');
	$thisTitle = "Kalender";
	$headerHTML = preg_replace("/{###TITLE###}/", strip_tags($thisTitle), $headerHTML);
	echo $headerHTML;

	require_once(FILE_MENUE_TOP);
?>

<div id="mainArea">
	<div id="mainContent">
		<?php require_once(FILE_MENUE_SIDEBAR); ?>
		<div id="contentArea">
			<div id="contentAreaElements">
				<h1><?php if(SHOW_PAGE_TITLE_ICONS) { echo '<img src="' . PATH_ICONS_MENUE_TITLES . 'calendar.png" alt="" />'; } ?> <?php echo $thisTitle; ?></h1>

				<?php displayMessages(); ?>

				<div id="searchFilterArea">
					<form name="formAdminBackup" action="<?php echo $_SERVER["PHP_SELF"]; ?>" method="post">
						<table border="0" cellspacing="0" cellpadding="0">
							<tr>
								<td>
									<label for="selectedMonth">Monat:</label>
									<select name="selectedMonth" id="selectedMonth" class="inputSelect_120" >
										<?php
											for($i = 1 ; $i < 13 ; $i++) {
												$selectMonth = $i;
												if($selectMonth < 10){ $selectMonth = '0' . $selectMonth; }
												$selected = '';
												if($_REQUEST["selectedMonth"] != '') {
													if($selectMonth == $_REQUEST["selectedMonth"]) { $selected = ' selected="selected" '; }
												}
												else {
													if($selectMonth == date('m')) { $selected = ' selected="selected" '; }
												}
												echo '<option value="' . $selectMonth . '" ' . $selected .' >' . getTimeNames($selectMonth, 'month', 'long') . '</option>';
											}
										?>
									</select>
								</td>
								<td>
									<label for="selectedYear">Jahr:</label>
									<select name="selectedYear" id="selectedYear" class="inputSelect_70" >
										<?php
											for($i = 1971 ; $i < 2090 ; $i++) {
												$selectYear = $i;
												$selected = '';
												if($_REQUEST["selectedYear"] != '') {
													if($selectYear == $_REQUEST["selectedYear"]) { $selected = ' selected="selected" '; }
												}
												else {
													if($selectYear == date('Y')) { $selected = ' selected="selected" '; }
												}
												echo '<option value="' . $selectYear . '" ' . $selected .' >' . $selectYear . '</option>';
											}
										?>
									</select>
								</td>
								<td width="130">
									<input type="submit" name="submitForm" class="inputButton2" value="Anzeigen" />
								</td>
								<td>
									<?php
										$monthBack = ($_REQUEST["selectedMonth"] - 1);
										$yearBack = $_REQUEST["selectedYear"];
										if($monthBack < 1) {
											$monthBack = 12;
											$yearBack = $_REQUEST["selectedYear"] - 1;
										}
										if($monthBack < 10) {
											$monthBack = '0' . $monthBack;
										}

									?>
									<a href="<?php echo $_SERVER["PHP_SELF"]; ?>?selectedYear=<?php echo $yearBack; ?>&amp;selectedMonth=<?php echo $monthBack; ?>" class="linkButton"><span>&lt; Vorheriger Monat</span></a>
								</td>
								<td>
									<?php
										$monthNext = ($_REQUEST["selectedMonth"] + 1);
										$yearNext = $_REQUEST["selectedYear"];
										if($monthNext > 12) {
											$monthNext = 1;
											$yearNext = $_REQUEST["selectedYear"] + 1;
										}
										if($monthNext < 10) {
											$monthNext = '0' . $monthNext;
										}
									?>
									<a href="<?php echo $_SERVER["PHP_SELF"]; ?>?selectedYear=<?php echo $yearNext; ?>&amp;selectedMonth=<?php echo $monthNext; ?>" class="linkButton"><span>N&auml;chster Monat &gt;</span></a>
								</td>
								<td width="20">&nbsp;</td>
								<td>
									<?php
										if($arrGetUserRights["editCalendar"]) {
									?>
									<a href="<?php echo PAGE_DISPLAY_PERSONNEL; ?>" class="linkButton"><span>Neuer Eintrag</span></a>
									<?php
										}
									?>
								</td>
							</tr>
						</table>
					</form>
				</div>

				<div class="contentDisplay">
				<?php

					$selectedDay = 1;
					if($_REQUEST["selectedMonth"] != '') {
						$selectedMonth = $_REQUEST["selectedMonth"];
					}
					else {
						$selectedMonth = date("m");
					}
					if($_REQUEST["selectedYear"] != '') {
						$selectedYear = $_REQUEST["selectedYear"];
					}
					else {
						$selectedYear = date("m");
					}

				?>
					<div class="calendarArea">
						<?php
							$selectedMonthMkTime = mktime(0, 0, 0, $selectedMonth, $selectedDay, $selectedYear);
							$selectedMonthName = date('F', $selectedMonthMkTime);
							$selectedMonthCountDays = date('t', $selectedMonthMkTime);
							$firstDayOfMonth = date('w', $selectedMonthMkTime);

							if($firstDayOfMonth == 0) { $startDay = (-5); }
							else { $startDay = (-1) * ($firstDayOfMonth - 2); }

							$fillDays = 7 - (($selectedMonthCountDays + 1) - $startDay)%7;
							if($fillDays == 7) { $fillDays = 0; }

							echo '<h2>' . getTimeNames($selectedMonth, 'month', 'long') . ' ' . $selectedYear . '</h2>';
						?>
						<div class="calendarAreaContent">
							<?php
								$count = 1;
								for($i = $startDay; $i < (($selectedMonthCountDays + 1) + $fillDays) ; $i++){
									$thisDay = $i;
									$thisDayMkTime = mktime(0, 0, 0, $selectedMonth, $thisDay, $selectedYear);
									$thisDay = date('d', $thisDayMkTime);
									$thisDayMonth = date('m-d', $thisDayMkTime);
									$thisDayName = date('D', $thisDayMkTime);
									$thisDayOfWeek = date('w', $thisDayMkTime);
									$thisWeekOfYear = date('W', $thisDayMkTime);
									$thisDayDate = date('Y-m-d', $thisDayMkTime);

									$thisBG = '';
									$thisBorder = '';
									$thisColorItem = '';
									$thisColorDay = '';
									$thisMarkerHoliday = '';
									$thisMarkerBirthday = '';
									$thisOpacity = 1;

									if($thisDayOfWeek == 0) {
										$thisBG = ' background-color:#FFDFE4; ';
										$thisBorder = ' border-color:#EE0000; ';
										$thisColorItem = '';
										$thisColorDay = ' color:#FF0000; ';
									}

									if($selectedHolidaysDatas[$thisDayDate]["holidaysHolydayName"] != "") {
										$thisBG = ' background-color:#FFDAAF; ';
										$thisBorder = ' border-color:#FF8A00; ';
										$thisColorItem = '';
										$thisColorDay = ' color: #FF5A00; cursor:pointer; ';
									}

									if($thisDayDate == date('Y-m-d')) {
										$thisBG = ' background-color:#DBFFCF; ';
										$thisBorder = ' border-color:#28CF00; ';
										$thisColorItem = '';
										// $thisColorDay = '';
									}

									if($i < 1 || $i > $selectedMonthCountDays) {
										$thisBG = ' background-color:#EEEEEE; ';
										$thisBorder = ' border-color:#DDDDDD; ';
										$thisColorItem = ' color:CCCCCC; ';
										$thisColorDay = '';
										$thisOpacity = ' opacity:0.7; ';
									}

									if($count%7 == 1) {
										echo '<div class="calendarDisplayWeek">';
										echo 'KW ' . $thisWeekOfYear . '';
										echo '</div>';
									}
									if($selectedHolidaysDatas[$thisDayDate]["holidaysHolydayName"] != "") {
										$thisMarkerHoliday = '<img src="layout/icons/holiday.png" alt="Feiertag" />';
									}

									if(SHOW_CALENDAR_BIRTHDAYS){
										if(!empty($selectedBirthdayDatas[$thisDayMonth])) {
											foreach($selectedBirthdayDatas[$thisDayMonth] as $thisKey => $thisValue) {
													$thisMarkerBirthday .= '<img style="cursor:pointer" src="layout/icons/birthday.png" alt="Geb." title="Geburtstag: ' . $thisValue["personnelFirstName"] . ' ' . $thisValue["personnelLastName"] . ' *' . formatDate($thisValue["personnelBirthday"], 'display') . '" width="10" height="10" /> ' . substringChars($thisValue["personnelFirstName"], 0, 2) . '. ' . $thisValue["personnelLastName"] . '<br />';
											}
										}
									}
									$thisTitle = '';
									if($selectedHolidaysDatas[$thisDayDate]["holidaysHolydayName"] != '') {
										$thisTitle = 'Feiertag: ' . $selectedHolidaysDatas[$thisDayDate]["holidaysHolydayName"];
									}

									echo '<div class="calendarItemArea" style="' . $thisBG. $thisBorder . $thisColorItem . $thisOpacity . '" >';
										echo '<div class="calendarItemAreaContent">';
											echo '<div class="calendarDisplayDay" title="' . $thisTitle . '" style="' . $thisColorDay . '"><b>' . $thisDay . '.</b> | ' . getTimeNames($thisDayOfWeek, 'day', 'long') . ' ' . $thisMarkerHoliday . '</div>';
											echo '<div class="calendarDisplayContent">';
											echo $thisMarkerBirthday;
											if(!empty($selectedPersonnelCalendarDatas)) {
												foreach($selectedPersonnelCalendarDatas as $thisPersonnelCalendarDatas){
													if($thisDayMkTime >= $thisPersonnelCalendarDatas["personnelCalendarStartTimestamp"] && $thisDayMkTime <= $thisPersonnelCalendarDatas["personnelCalendarEndTimestamp"]) {
														if($arrGetUserRights["editCalendar"]) {
															echo '<a href="' . PAGE_DISPLAY_PERSONNEL . '?editID=' . $thisPersonnelCalendarDatas["personnelCalendarPersonnelID"] .' ';
														}
														else {
															echo '<span ';
														}

														$thisIcon = '';
														if($thisPersonnelCalendarDatas["personnelCalendarReason"] == 'urlaub') {
															$thisIcon = '<img src="layout/icons/vacations2.png" alt="urlaub" width="10" height="10" />';
														}
														else if($thisPersonnelCalendarDatas["personnelCalendarReason"] == 'krank') {
															$thisIcon = '<img src="layout/icons/firstAid.png" alt="krank" width="10" height="10" />';
														}

														echo '" style="cursor:pointer" title="' . strtoupper($thisPersonnelCalendarDatas["personnelCalendarReason"]) . ' ' . $thisPersonnelCalendarDatas["personnelFirstName"] . ' ' . $thisPersonnelCalendarDatas["personnelLastName"] . ': ' . formatDate($thisPersonnelCalendarDatas["personnelCalendarStart"], 'display') . ' - ' .  formatDate($thisPersonnelCalendarDatas["personnelCalendarEnd"], 'display') . '">' . $thisIcon . ' ' . substringChars($thisPersonnelCalendarDatas["personnelFirstName"], 0, 2) . '. ' . $thisPersonnelCalendarDatas["personnelLastName"] . '';
														if($arrGetUserRights["editCalendar"]) {
															echo '</a><br />';
														}
														else {
															echo '</span><br />';
														}
													}
												}
											}
											echo '</div>';
										echo '</div>';
									echo '</div>';

									if($count%7 == 0) {
										echo '<div class="clear"></div>';
									}
									$count++;
								}
							?>
							<div class="clear"></div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<?php require_once('inc/footerHTML.inc.php'); ?>