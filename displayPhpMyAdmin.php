<?php
	require_once('inc/requires.inc.php');

	if(!$arrGetUserRights["openPhpMyAdmin"]) {
		header('location: ' . PAGE_EXIT_LOCATION);
		exit;
	}

	$jswindowMessage = '';
	$warningMessage = '';
	$errorMessage = '';
	$successMessage = '';
	$infoMessage = '';

	$dbConnection = new DB_Connection();
	$db_open = $dbConnection->db_connect();
?>
<?php
	require_once('inc/headerHTML.inc.php');
	$thisTitle = "PHP-MyAdmin";
	$headerHTML = preg_replace("/{###TITLE###}/", strip_tags($thisTitle), $headerHTML);
	echo $headerHTML;

	require_once(FILE_MENUE_TOP);
?>
<div id="mainArea">
	<div id="mainContent">
		<?php require_once(FILE_MENUE_SIDEBAR); ?>
		<div id="contentArea">
			<div id="contentAreaElements">
				<h1><?php if(SHOW_PAGE_TITLE_ICONS) { echo '<img src="' . PATH_ICONS_MENUE_TITLES . 'phpmyadmin.png" alt="" />'; } ?> <?php echo $thisTitle; ?></h1>
				<iframe class="iFrameModule" width="100%" height="100%" src="<?php echo PATH_PHPMYADMIN; ?>"><a href="<?php echo PATH_PHPMYADMIN; ?>">PHP-MyAdmin</a><iframe>
				<?php displayMessages(); ?>
			</div>
		</div>
		<div class="clear"></div>
	</div>
	<div class="clear"></div>
</div>

<script language="javascript" type="text/javascript">
	$(document).ready(function() {
		$(function() {
			$( "#datepicker" ).datepicker();
		});
		colorRowMouseOver('.displayOrders tbody tr');
	});
</script>

<?php require_once('inc/footerHTML.inc.php'); ?>