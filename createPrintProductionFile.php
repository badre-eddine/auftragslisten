<?php
	require_once('inc/requires.inc.php');
	set_time_limit(0);

	if(!$arrGetUserRights["displayPrintProductionFiles"] && !$arrGetUserRights["editPrintProductionFiles"]) {
		header('location: ' . PAGE_EXIT_LOCATION);
		exit;
	}

	/*
	displayPrintProductionFiles', 'TITLE' => 'Produktionaauftr&auml;ge ansehen');
	$arrSetUserRights[$i][] = array('NAME' => 'editPrintProductionFiles
	*/

	// BOF DEFINE LANGUAGEFILES
		$arrLanguageFileDatas = array(
			"DE" => array("ID" => "DE",		"TEXT" => "deutsch",		"FILE" => "de.xml"),
			"TR" => array("ID" => "TR",		"TEXT" => "t&uuml;rkisch",	"FILE" => "tr.xml"),
		);
	// EOF DEFINE LANGUAGEFILES

	DEFINE('MAX_PRODUCTIONS_PER_FILM', 15);

	$jswindowMessage = "";
	$warningMessage = '';
	$errorMessage = '';
	$successMessage = '';
	$infoMessage = '';

	$dbConnection = new DB_Connection();
	$db_open = $dbConnection->db_connect();

	$userDatas = getUserDatas();

	// BOF SEND MAIL WITH ATTACHED DOCUMENT
	if($_POST["sendAttachedDocument"] == '1') {
		$thisCreatedPdfName = $_POST["mailDocumentFilename"];
		# $_POST["selectSubject"] = $arrDocumentTypeDatas[$_REQUEST["documentType"]]["createdDocumentsTypesName"] . ' Nr.: ' . '{###DOCUMENT_NUMBER###}';
		# $_POST["selectSubject"] = 'Datenexport: ' . $thisCreatedPdfName;
		$generatedDocumentNumber = $_POST["mailDocumentFilename"];

		require_once(DIRECTORY_HTML_MIME_MAIL . 'htmlMimeMail.php');
		require_once(DIRECTORY_MAIL_TEMPLATES . 'template_BASIC.inc.php');
		require_once(DIRECTORY_MAIL_TEMPLATES . 'template_TEXT_DATAS.inc.php');
		#require_once("inc/mail.inc.php");

		// BOF SEND MAIL
		require_once("classes/createMail.class.php");

		// BOF CREATE SUBJECT
		$thisSubject = $_POST["selectSubject"];
		if($_POST["sendAttachedMailSubject"] != '') {
			$thisSubject = $_POST["sendAttachedMailSubject"] . ', ' . $thisSubject;
		}
		// EOF CREATE SUBJECT

		// BOF GET ATTACHED FILE

		$arrPathInfo = pathinfo($_POST["mailDocumentFilename"]);

		$pathDocumentFolder = $arrPathInfo["dirname"] . "/";
		$thisCreatedPdfName = $arrPathInfo["basename"];

		$arrAttachmentFiles = array (
			rawurldecode($pathDocumentFolder . $thisCreatedPdfName) => str_replace(" ", "_", rawurldecode(myUnHtmlEntities($thisCreatedPdfName)))
		);

		// EOF GET ATTACHED FILE

		$thisSubject .= " - " . $arrPathInfo["filename"];
		// $arrMailContentDatas[$_POST["selectMailtextTemplates"]],
		#$thisDocumentType = $_POST["documentType"];
		$thisDocumentType = "BB";
		$generatedDocumentNumber = "";
		#$arrThisMailContentDatas = $arrMailContentDatas[$_POST["selectMailtextTemplates"]];
		$arrThisMailContentDatas = $arrMailContentDatas["DEFAULT"];

		$createMail = new createMail(
							$thisSubject,													// TEXT:	MAIL_SUBJECT
							$thisDocumentType,												// STRING:	DOCUMENT TYPE
							$generatedDocumentNumber,										// STRING:	DOCUMENT NUMBER
							$arrAttachmentFiles,											// ARRAY:	ATTACHMENT FILES
							$_POST["selectCustomersRecipientMail"],	 						// STRING:	RECIPIENTS
							$arrThisMailContentDatas,										// MAIL_TEXT_TEMPLATE
							$mailTemplate["html"],											// MAIL_BODY_TEMPLATE
							'',																// STRING:	ADDITIONAL TEXT
							$_POST["sendAttachedMailText"],									// STRING:	SEND ATTACHED TEXT
							true,															// BOOLEAN:	SEND ATTACHED DOCUMENT
							DIRECTORY_MAIL_IMAGES,											// DIRECTORY_MAIL_IMAGES
							$_POST["selectMailtextSender"]									// STRING: SENDER
						);

		$createMailResult = $createMail->returnResult();
		$sendMailToClient = $createMailResult;
		// EOF SEND MAIL

		#if(!$sendMailToUs) { $errorMessage .= ' Die Mail-Kopie konnte nicht an ' . implode(', ', $arrOrderMail) . ' versendet werden! '.'<br />'; }
		#else { $successMessage .= ' Die Mail-Kopie wurde an ' . implode(', ', $arrOrderMail) . ' versendet! '.'<br />'; }

		if(!$sendMailToClient) { $errorMessage .= ' Die Mail konnte nicht an ' . $_POST["selectCustomersRecipientName"] . ' (' . $_POST["selectCustomersRecipientMail"] . ') versendet werden! '.'<br />'; }
		else {
			$successMessage .= ' Die Mail wurde an ' . $_POST["selectCustomersRecipientName"] . ' (' . $_POST["selectCustomersRecipientMail"] . ') versendet! '.'<br />';
		}
		unset($_POST["exportOrdersVertreterID"]);
	}
	// EOF SEND MAIL WITH ATTACHED DOCUMENT

	// BOF READ COUNTRIES
	$arrCountryTypeDatas = getCountryTypes();
	// EOF READ COUNTRIES

	// BOF READ ORDER CATEGORIES
	$arrOrderCategoriesTypeDatas = getOrderCategories();
	// EOF READ ORDER CATEGORIES

	// BOF OUR COMPANY DATAS
	$arrAllMandatoryCompanyDatas = getAllMandatoryCompanyDatas();
	// EOF OUR COMPANY DATAS

	function getProductionFilesFileSheetNumber($thisFile){
		preg_match("/_([0-9]{2})\.pdf/", $thisFile, $arrFound);
		$thisSheetNumber = $arrFound[1];
		return $thisSheetNumber;
	}
	function printProductionFilesFileDate($thisFile) {
		preg_match("/([0-9]{4}-[0-9]{2}-[0-9]{2})/", $thisFile, $arrFound);
		$thisDate = $arrFound[1];
		return $thisDate;
	}
	function renamePrintProductionFile($thisFile){
		$thisNewFileName = basename($thisFile);

		$pattern = "_([0-9]{2})\.([0-9]{2})\.([0-9]{4})";
		$replace = "_$3-$2-$1";
		$thisNewFileName = preg_replace("/" . $pattern . "/", $replace, $thisNewFileName);

		$pattern = "_([0-9]{1})\.([0-9]{2})\.([0-9]{4})";
		$replace = "_$3-$2-0$1";
		$thisNewFileName = preg_replace("/" . $pattern . "/", $replace, $thisNewFileName);

		$pattern = "_([0-9]{2})\.([0-9]{2})\.([0-9]{2})";
		$replace = "_" . substr(date("Y"), 0, 2) . "$3-$2-$1";
		$thisNewFileName = preg_replace("/" . $pattern . "/", $replace, $thisNewFileName);

		$pattern = "(Belichtungsbogen|Belichtung)";
		$replace = "belichtung";
		$thisNewFileName = preg_replace("/" . $pattern . "/ism", $replace, $thisNewFileName);


		$pattern = "_([0-9])\.([a-z]{3})$";
		$replace = "_0$1.$2";
		$thisNewFileName = preg_replace("/" . $pattern . "/", $replace, $thisNewFileName);

		$pattern = "-([0-9]{2})(\.[a-z]{3})$";
		$replace = "-$1_01$2";
		$thisNewFileName = preg_replace("/" . $pattern . "/", $replace, $thisNewFileName);

		$thisNewFileName = strtolower($thisNewFileName);

		return $thisNewFileName;
	}

	function formatAdressData($arrDatas){
		$arrAdressFormatted = "";
		if($arrDatas["knr"] != "") { $arrAdressFormatted .= $arrDatas["knr"] . '<br />'; }
		if($arrDatas["name"] != "") { $arrAdressFormatted .= $arrDatas["name"] . '<br />'; }
		if($arrDatas["nameAdd"] != "") { $arrAdressFormatted .= $arrDatas["nameAdd"] . '<br />'; }
		if($arrDatas["street"] != "") { $arrAdressFormatted .= $arrDatas["street"] . ' '; }
		if($arrDatas["streetNumber"] != "") { $arrAdressFormatted .= $arrDatas["streetNumber"] . '<br />'; }
		if($arrDatas["zipcode"] != "") { $arrAdressFormatted .= $arrDatas["zipcode"] . ' '; }
		if($arrDatas["city"] != "") { $arrAdressFormatted .= $arrDatas["city"] . '<br />'; }
		if($arrDatas["country"] != "") { $arrAdressFormatted .= $arrDatas["country"] . ''; }
		return $arrAdressFormatted;
	}

	function getSenderAndRecipientAdress(){
		global $arrCountryTypeDatas, $arrThisRecipientAdress, $arrThisSenderAdress, $thisPrintProductionDataValue, $arrAllMandatoryCompanyDatas;

		// BOF RECIPIENT ADRESS
		$arrThisRecipientAdress = array();
		$arrThisRecipientAdress["USE"] = array();

		$arrThisRecipientAdress["CUSTOMER"] = array();
		$arrThisRecipientAdress["CUSTOMER"]["id"] = $thisPrintProductionDataValue["customersID"];
		$arrThisRecipientAdress["CUSTOMER"]["knr"] = $thisPrintProductionDataValue["customersKundennummer"];
		$arrThisRecipientAdress["CUSTOMER"]["name"] = $thisPrintProductionDataValue["customersLieferadresseFirmenname"];
		$arrThisRecipientAdress["CUSTOMER"]["nameAdd"] = $thisPrintProductionDataValue["customersLieferadresseFirmennameZusatz"];
		$arrThisRecipientAdress["CUSTOMER"]["street"] = $thisPrintProductionDataValue["customersLieferadresseStrasse"];
		$arrThisRecipientAdress["CUSTOMER"]["streetNumber"] = $thisPrintProductionDataValue["customersLieferadresseHausnummer"];
		$arrThisRecipientAdress["CUSTOMER"]["zipcode"] = $thisPrintProductionDataValue["customersLieferadressePLZ"];
		$arrThisRecipientAdress["CUSTOMER"]["city"] = $thisPrintProductionDataValue["customersLieferadresseOrt"];
		$arrThisRecipientAdress["CUSTOMER"]["country"] = $arrCountryTypeDatas[$thisPrintProductionDataValue["customersLieferadresseLand"]]["countries_name"];
		$arrThisRecipientAdress["CUSTOMER"]["countryID"] = $thisPrintProductionDataValue["customersLieferadresseLand"];

		$arrThisRecipientAdress["SALESMAN"] = array();
		$arrThisRecipientAdress["SALESMAN"]["id"] = $thisPrintProductionDataValue["salesmansID"];
		$arrThisRecipientAdress["SALESMAN"]["knr"] = $thisPrintProductionDataValue["salesmansKundennummer"];
		$arrThisRecipientAdress["SALESMAN"]["name"] = $thisPrintProductionDataValue["salesmansLieferadresseFirmenname"];
		$arrThisRecipientAdress["SALESMAN"]["nameAdd"] = $thisPrintProductionDataValue["salesmansLieferadresseFirmennameZusatz"];
		$arrThisRecipientAdress["SALESMAN"]["street"] = $thisPrintProductionDataValue["salesmansLieferadresseStrasse"];
		$arrThisRecipientAdress["SALESMAN"]["streetNumber"] = $thisPrintProductionDataValue["salesmansLieferadresseHausnummer"];
		$arrThisRecipientAdress["SALESMAN"]["zipcode"] = $thisPrintProductionDataValue["salesmansLieferadressePLZ"];
		$arrThisRecipientAdress["SALESMAN"]["city"] = $thisPrintProductionDataValue["salesmansLieferadresseOrt"];
		$arrThisRecipientAdress["SALESMAN"]["country"] = $arrCountryTypeDatas[$thisPrintProductionDataValue["salesmansLieferadresseLand"]]["countries_name"];
		$arrThisRecipientAdress["SALESMAN"]["countryID"] = $thisPrintProductionDataValue["salesmansLieferadresseLand"];

		if($thisPrintProductionDataValue["customersUseSalesmanDeliveryAdress"] == "1"){
			$arrThisRecipientAdress["USE"] = $arrThisRecipientAdress["SALESMAN"];
		}
		else {
			$arrThisRecipientAdress["USE"] = $arrThisRecipientAdress["CUSTOMER"];
		}
		// EOF RECIPIENT ADRESS

		// BOF SENDER ADRESS
		$arrThisSenderAdress = array();
		$arrThisSenderAdress["USE"] = array();

		$arrThisSenderAdress["CUSTOMER"] = array();
		$arrThisSenderAdress["CUSTOMER"]["knr"] = $thisPrintProductionDataValue["customersKundennummer"];
		$arrThisSenderAdress["CUSTOMER"]["name"] = $thisPrintProductionDataValue["customersFirmenname"];
		$arrThisSenderAdress["CUSTOMER"]["nameAdd"] = $thisPrintProductionDataValue["customersFirmennameZusatz"];
		$arrThisSenderAdress["CUSTOMER"]["street"] = $thisPrintProductionDataValue["customersCompanyStrasse"];
		$arrThisSenderAdress["CUSTOMER"]["streetNumber"] = $thisPrintProductionDataValue["customersCompanyHausnummer"];
		$arrThisSenderAdress["CUSTOMER"]["zipcode"] = $thisPrintProductionDataValue["customersCompanyPLZ"];
		$arrThisSenderAdress["CUSTOMER"]["city"] = $thisPrintProductionDataValue["customersCompanyCompanyOrt"];
		$arrThisSenderAdress["CUSTOMER"]["country"] = $arrCountryTypeDatas[$thisPrintProductionDataValue["customersCompanyCountry"]]["countries_name"];

		$arrThisSenderAdress["SALESMAN"] = array();
		$arrThisSenderAdress["SALESMAN"]["knr"] = $thisPrintProductionDataValue["salesmansKundennummer"];
		$arrThisSenderAdress["SALESMAN"]["name"] = $thisPrintProductionDataValue["salesmansFirmenname"];
		$arrThisSenderAdress["SALESMAN"]["nameAdd"] = $thisPrintProductionDataValue["salesmansFirmennameZusatz"];
		$arrThisSenderAdress["SALESMAN"]["street"] = $thisPrintProductionDataValue["salesmansCompanyStrasse"];
		$arrThisSenderAdress["SALESMAN"]["streetNumber"] = $thisPrintProductionDataValue["salesmansCompanyHausnummer"];
		$arrThisSenderAdress["SALESMAN"]["zipcode"] = $thisPrintProductionDataValue["salesmansCompanyPLZ"];
		$arrThisSenderAdress["SALESMAN"]["city"] = $thisPrintProductionDataValue["salesmansCompanyOrt"];
		$arrThisSenderAdress["SALESMAN"]["country"] = $arrCountryTypeDatas[$thisPrintProductionDataValue["salesmansCompanyCountry"]]["countries_name"];

		$arrThisSenderAdress["AGENT"] = array();
		$arrThisSenderAdress["AGENT"]["knr"] = $thisPrintProductionDataValue["agentsKundennummer"];
		$arrThisSenderAdress["AGENT"]["name"] = $thisPrintProductionDataValue["agentsFirmenname"];
		$arrThisSenderAdress["AGENT"]["nameAdd"] = $thisPrintProductionDataValue["agentsFirmennameZusatz"];
		$arrThisSenderAdress["AGENT"]["street"] = $thisPrintProductionDataValue["agentsCompanyStrasse"];
		$arrThisSenderAdress["AGENT"]["streetNumber"] = $thisPrintProductionDataValue["agentsCompanyHausnummer"];
		$arrThisSenderAdress["AGENT"]["zipcode"] = $thisPrintProductionDataValue["agentsCompanyPLZ"];
		$arrThisSenderAdress["AGENT"]["city"] = $thisPrintProductionDataValue["agentsCompanyOrt"];
		$arrThisSenderAdress["AGENT"]["country"] = $arrCountryTypeDatas[$thisPrintProductionDataValue["agentsCompanyCountry"]]["countries_name"];

		$arrThisSenderAdress["OUR_COMPANY"] = array();
		$arrThisSenderAdress["OUR_COMPANY"]["knr"] = '';
		$arrThisSenderAdress["OUR_COMPANY"]["name"] = $arrAllMandatoryCompanyDatas[strtolower($thisPrintProductionDataValue["ordersMandant"])]["COMPANY_NAME_LONG"];
		$arrThisSenderAdress["OUR_COMPANY"]["nameAdd"] = '';
		$arrThisSenderAdress["OUR_COMPANY"]["street"] = $arrAllMandatoryCompanyDatas[strtolower($thisPrintProductionDataValue["ordersMandant"])]["COMPANY_STREET_NAME"];
		$arrThisSenderAdress["OUR_COMPANY"]["streetNumber"] = $arrAllMandatoryCompanyDatas[strtolower($thisPrintProductionDataValue["ordersMandant"])]["COMPANY_STREET_NUMBER"];
		$arrThisSenderAdress["OUR_COMPANY"]["zipcode"] = $arrAllMandatoryCompanyDatas[strtolower($thisPrintProductionDataValue["ordersMandant"])]["COMPANY_ZIPCODE"];
		$arrThisSenderAdress["OUR_COMPANY"]["city"] = $arrAllMandatoryCompanyDatas[strtolower($thisPrintProductionDataValue["ordersMandant"])]["COMPANY_CITY"];
		$arrThisSenderAdress["OUR_COMPANY"]["country"] = $arrAllMandatoryCompanyDatas[strtolower($thisPrintProductionDataValue["ordersMandant"])]["COMPANY_COUNTRY"];

		if($thisPrintProductionDataValue["ordersMandant"] == 'B3'){
			$arrThisSenderAdress["USE"] = $arrThisSenderAdress["OUR_COMPANY"];
			if($thisPrintProductionDataValue["customersUseSalesmanInvoiceAdress"] == "1"){
				$arrThisSenderAdress["USE"] = $arrThisSenderAdress["AGENT"];
			}
		}
		else if($thisPrintProductionDataValue["ordersMandant"] == 'BCTR'){
			$arrThisSenderAdress["USE"] = $arrThisSenderAdress["OUR_COMPANY"];
			if($thisPrintProductionDataValue["customersUseSalesmanInvoiceAdress"] == "1"){
				$arrThisSenderAdress["USE"] = $arrThisSenderAdress["AGENT"];
			}
		}
		// EOF SENDER ADRESS
	}

	function getPrintProductionDatas(){
		global $dbConnection, $arrPrintProductionFiles, $warningMessage, $errorMessage, $successMessage, $infoMessage;

		// BOF READ STORED PRODUCTION FILE ORDER DATAS
		$sql = "
				SELECT
						`printProductionOrdersID`,
						`printProductionOrdersOrderID`,
						`printProductionOrdersPrintProductionFilesID`,
						`printProductionOrdersCompanyID`,
						`printProductionOrdersCompanyNumber`,
						`printProductionOrdersCompanyName`,
						`printProductionOrdersCompanyNameAdd`,

						`printProductionOrdersSenderID`,
						`printProductionOrdersSenderNumber`,
						`printProductionOrdersSenderName`,
						`printProductionOrdersSenderNameAdd`,
						`printProductionOrdersSenderStreet`,
						`printProductionOrdersSenderStreetNumber`,
						`printProductionOrdersSenderZipcode`,
						`printProductionOrdersSenderCity`,
						`printProductionOrdersSenderCountryID`,
						`printProductionOrdersSenderCountryName`,

						`printProductionOrdersRecipientID`,
						`printProductionOrdersRecipientNumber`,
						`printProductionOrdersRecipientName`,
						`printProductionOrdersRecipientNameAdd`,
						`printProductionOrdersRecipientStreet`,
						`printProductionOrdersRecipientStreetNumber`,
						`printProductionOrdersRecipientZipcode`,
						`printProductionOrdersRecipientCity`,
						`printProductionOrdersRecipientCountryID`,
						`printProductionOrdersRecipientCountryName`,

						`printProductionOrdersProductDatas`,
						`printProductionOrdersLayoutFilePath`,
						`printProductionOrdersComment`,
						`printProductionOrdersMandatory`,
						`printProductionOrdersPaymentTypeCashOnDelivery`

					FROM `" . TABLE_PRINT_PRODUCTION_ORDERS . "`
						WHERE 1
							AND `printProductionOrdersPrintProductionFilesID` = '" . $_POST["searchPrintProductionFileID"]. "'
					ORDER BY `printProductionOrdersCompanyName`

			";
		$rs = $dbConnection->db_query($sql);

		$countRows = $dbConnection->db_getMysqlNumRows($rs);

		while($ds = mysqli_fetch_assoc($rs)){
			foreach(array_keys($ds) as $field){
				$arrLoadPrintProductionData[$ds["printProductionOrdersOrderID"]][$field] = $ds[$field];
			}
		}

		return $arrLoadPrintProductionData;
	}

	function getOrderProcessDatas($arrOrderIDs = array()) {
		global $dbConnection, $arrPrintProductionFiles, $warningMessage, $errorMessage, $successMessage, $infoMessage;

		$where = "";
		if(!empty($arrOrderIDs)){
			$arrWhere = array();
			foreach($arrOrderIDs as $thisOrderKey => $thisOrderValue){
				if($thisOrderValue == "1"){
					$arrWhere[] = " `" . TABLE_ORDERS . "`.`ordersID` = '" . $thisOrderKey . "' ";
				}
			}
			if(!empty($arrWhere)){
				$where = " AND (" . implode(" OR ", $arrWhere) . ")";
			}
		}

		$sql = "
			SELECT
				`" . TABLE_ORDERS . "`.`ordersID`,
				`" . TABLE_ORDERS . "`.`ordersKundennummer`,
				`" . TABLE_ORDERS . "`.`ordersCustomerID`,
				`" . TABLE_ORDERS . "`.`ordersStatus`,
				`" . TABLE_ORDERS . "`.`ordersSourceType`,
				`" . TABLE_ORDERS . "`.`ordersSourceName`,
				`" . TABLE_ORDERS . "`.`ordersBestellDatum`,
				`" . TABLE_ORDERS . "`.`ordersStornoDatum`,
				`" . TABLE_ORDERS . "`.`ordersFreigabeDatum`,
				`" . TABLE_ORDERS . "`.`ordersFreigabeArt`,
				`" . TABLE_ORDERS . "`.`ordersBelichtungsDatum`,
				`" . TABLE_ORDERS . "`.`ordersProduktionsDatum`,
				`" . TABLE_ORDERS . "`.`ordersAuslieferungsDatum`,
				`" . TABLE_ORDERS . "`.`ordersLieferwoche`,
				`" . TABLE_ORDERS . "`.`ordersLieferDatum`,
				`" . TABLE_ORDERS . "`.`ordersKundenName`,
				`" . TABLE_ORDERS . "`.`ordersKommission`,
				`" . TABLE_ORDERS . "`.`ordersInfo`,
				`" . TABLE_ORDERS . "`.`ordersPLZ`,
				`" . TABLE_ORDERS . "`.`ordersOrt`,
				`" . TABLE_ORDERS . "`.`ordersLand`,
				`" . TABLE_ORDERS . "`.`ordersArtikelID`,
				`" . TABLE_ORDERS . "`.`ordersArtikelNummer`,
				`" . TABLE_ORDERS . "`.`ordersArtikelBezeichnung`,
				`" . TABLE_ORDERS . "`.`ordersArtikelKategorieID`,
				`" . TABLE_ORDERS . "`.`ordersArtikelMenge`,
				`" . TABLE_ORDERS . "`.`ordersArtikelPrintColorsCount`,
				`" . TABLE_ORDERS . "`.`ordersArtikelWithBorder`,
				`" . TABLE_ORDERS . "`.`ordersAufdruck`,
				`" . TABLE_ORDERS . "`.`ordersAdditionalArtikelID`,
				`" . TABLE_ORDERS . "`.`ordersAdditionalArtikelNummer`,
				`" . TABLE_ORDERS . "`.`ordersAdditionalArtikelBezeichnung`,
				`" . TABLE_ORDERS . "`.`ordersAdditionalArtikelKategorieID`,
				`" . TABLE_ORDERS . "`.`ordersAdditionalArtikelMenge`,
				`" . TABLE_ORDERS . "`.`ordersSinglePreis`,
				`" . TABLE_ORDERS . "`.`ordersTotalPreis`,
				`" . TABLE_ORDERS . "`.`ordersDruckFarbe`,
				`" . TABLE_ORDERS . "`.`ordersAdditionalCosts`,
				`" . TABLE_ORDERS . "`.`ordersAdditionalCostsID`,
				`" . TABLE_ORDERS . "`.`ordersVertreter`,
				`" . TABLE_ORDERS . "`.`ordersVertreterID`,
				`" . TABLE_ORDERS . "`.`ordersMailAdress`,
				`" . TABLE_ORDERS . "`.`ordersContactOthers`,
				`" . TABLE_ORDERS . "`.`ordersMandant`,
				`" . TABLE_ORDERS . "`.`ordersOrderType`,
				`" . TABLE_ORDERS . "`.`ordersPerExpress`,
				`" . TABLE_ORDERS . "`.`ordersBankAccountType`,
				`" . TABLE_ORDERS . "`.`ordersPaymentType`,
				`" . TABLE_ORDERS . "`.`ordersPaymentStatusType`,
				`" . TABLE_ORDERS . "`.`ordersNotizen`,
				`" . TABLE_ORDERS . "`.`ordersDruckerName`,
				`" . TABLE_ORDERS . "`.`ordersUserID`,
				`" . TABLE_ORDERS . "`.`ordersTimeCreated`,
				`" . TABLE_ORDERS . "`.`ordersArchive`,
				`" . TABLE_ORDERS . "`.`ordersUseNoStock`,
				`" . TABLE_ORDERS . "`.`ordersDirectSale`,
				`" . TABLE_ORDERS . "`.`ordersReklamationsgrund`,

				`customersData`.`customersID`,
				`customersData`.`customersKundennummer`,

				`customersData`.`customersFirmenname`,
				`customersData`.`customersFirmennameZusatz`,
				`customersData`.`customersFirmenInhaberVorname`,
				`customersData`.`customersFirmenInhaberNachname`,
				`customersData`.`customersFirmenInhaberAnrede`,
				`customersData`.`customersFirmenInhaber2Vorname`,
				`customersData`.`customersFirmenInhaber2Nachname`,
				`customersData`.`customersFirmenInhaber2Anrede`,

				`customersData`.`customersAnsprechpartner1Vorname`,
				`customersData`.`customersAnsprechpartner1Nachname`,
				`customersData`.`customersAnsprechpartner1Anrede`,
				`customersData`.`customersAnsprechpartner1Typ`,

				`customersData`.`customersAnsprechpartner2Vorname`,
				`customersData`.`customersAnsprechpartner2Nachname`,
				`customersData`.`customersAnsprechpartner2Anrede`,
				`customersData`.`customersAnsprechpartner2Typ`,

				`customersData`.`customersCompanyStrasse`,
				`customersData`.`customersCompanyHausnummer`,
				`customersData`.`customersCompanyCountry`,
				`customersData`.`customersCompanyPLZ`,
				`customersData`.`customersCompanyOrt`,

				`customersData`.`customersLieferadresseFirmenname`,
				`customersData`.`customersLieferadresseFirmennameZusatz`,
				`customersData`.`customersLieferadresseStrasse`,
				`customersData`.`customersLieferadresseHausnummer`,
				`customersData`.`customersLieferadressePLZ`,
				`customersData`.`customersLieferadresseOrt`,
				`customersData`.`customersLieferadresseLand`,

				`customersData`.`customersRechnungsadresseFirmenname`,
				`customersData`.`customersRechnungsadresseFirmennameZusatz`,
				`customersData`.`customersRechnungsadresseStrasse`,
				`customersData`.`customersRechnungsadresseHausnummer`,
				`customersData`.`customersRechnungsadressePLZ`,
				`customersData`.`customersRechnungsadresseOrt`,
				`customersData`.`customersRechnungsadresseLand`,

				`customersData`.`customersVertreterID`,
				`customersData`.`customersVertreterName`,
				`customersData`.`customersUseSalesmanDeliveryAdress`,
				`customersData`.`customersUseSalesmanInvoiceAdress`,

				`customersData`.`customersTyp`,
				`customersData`.`customersGruppe`,
				`customersData`.`customersNotiz`,
				`customersData`.`customersActive`,

				/* ------------------------------------------------------------------------------- */

				`agentsData`.`customersID` AS `agentsID`,
				`agentsData`.`customersKundennummer` AS `agentsKundennummer`,

				`agentsData`.`customersFirmenname` AS `agentsFirmenname`,
				`agentsData`.`customersFirmennameZusatz` AS `agentsFirmennameZusatz`,
				`agentsData`.`customersFirmenInhaberVorname` AS `agentsFirmenInhaberVorname`,
				`agentsData`.`customersFirmenInhaberNachname` AS `agentsFirmenInhaberNachname`,
				`agentsData`.`customersFirmenInhaberAnrede` AS `agentsFirmenInhaberAnrede`,
				`agentsData`.`customersFirmenInhaber2Vorname` AS `agentsFirmenInhaber2Vorname`,
				`agentsData`.`customersFirmenInhaber2Nachname` AS `agentsFirmenInhaber2Nachname`,
				`agentsData`.`customersFirmenInhaber2Anrede` AS `agentsFirmenInhaber2Anrede`,

				`agentsData`.`customersAnsprechpartner1Vorname` AS `agentsAnsprechpartner1Vorname`,
				`agentsData`.`customersAnsprechpartner1Nachname` AS `agentsAnsprechpartner1Nachname`,
				`agentsData`.`customersAnsprechpartner1Anrede` AS `agentsAnsprechpartner1Anrede`,
				`agentsData`.`customersAnsprechpartner1Typ` AS `agentsAnsprechpartner1Typ`,

				`agentsData`.`customersAnsprechpartner2Vorname` AS `agentsAnsprechpartner2Vorname`,
				`agentsData`.`customersAnsprechpartner2Nachname` AS `agentsAnsprechpartner2Nachname`,
				`agentsData`.`customersAnsprechpartner2Anrede` AS `agentsAnsprechpartner2Anrede`,
				`agentsData`.`customersAnsprechpartner2Typ` AS `agentsAnsprechpartner2Typ`,

				`agentsData`.`customersCompanyStrasse` AS `agentsCompanyStrasse`,
				`agentsData`.`customersCompanyHausnummer` AS `agentsCompanyHausnummer`,
				`agentsData`.`customersCompanyCountry` AS `agentsCompanyCountry`,
				`agentsData`.`customersCompanyPLZ` AS `agentsCompanyPLZ`,
				`agentsData`.`customersCompanyOrt` AS `agentsCompanyOrt`,

				`agentsData`.`customersLieferadresseFirmenname` AS `agentsLieferadresseFirmenname`,
				`agentsData`.`customersLieferadresseFirmennameZusatz` AS `agentsLieferadresseFirmennameZusatz`,
				`agentsData`.`customersLieferadresseStrasse` AS `agentsLieferadresseStrasse`,
				`agentsData`.`customersLieferadresseHausnummer` AS `agentsLieferadresseHausnummer`,
				`agentsData`.`customersLieferadressePLZ` AS `agentsLieferadressePLZ`,
				`agentsData`.`customersLieferadresseOrt` AS `agentsLieferadresseOrt`,
				`agentsData`.`customersLieferadresseLand` AS `agentsLieferadresseLand`,

				`agentsData`.`customersRechnungsadresseFirmenname` AS `agentsRechnungsadresseFirmenname`,
				`agentsData`.`customersRechnungsadresseFirmennameZusatz` AS `agentsRechnungsadresseFirmennameZusatz`,
				`agentsData`.`customersRechnungsadresseStrasse` AS `agentsRechnungsadresseStrasse`,
				`agentsData`.`customersRechnungsadresseHausnummer` AS `agentsRechnungsadresseHausnummer`,
				`agentsData`.`customersRechnungsadressePLZ` AS `agentsRechnungsadressePLZ`,
				`agentsData`.`customersRechnungsadresseOrt` AS `agentsRechnungsadresseOrt`,
				`agentsData`.`customersRechnungsadresseLand` AS `agentsRechnungsadresseLand`,

				`agentsData`.`customersVertreterID` AS `agentsVertreterID`,
				`agentsData`.`customersVertreterName` AS `agentsVertreterName`,
				`agentsData`.`customersUseSalesmanDeliveryAdress` AS `agentsUseSalesmanDeliveryAdress`,
				`agentsData`.`customersUseSalesmanInvoiceAdress` AS `agentsUseSalesmanInvoiceAdress`,

				`agentsData`.`customersTyp` AS `agentsTyp`,
				`agentsData`.`customersGruppe` AS `agentsGruppe`,
				`agentsData`.`customersNotiz` AS `agentsNotiz`,
				`agentsData`.`customersActive` AS `agentsActive`,

				/* ------------------------------------------------------------------------------- */

				`salesmansData`.`customersID` AS `salesmansID`,
				`salesmansData`.`customersKundennummer` AS `salesmansKundennummer`,

				`salesmansData`.`customersFirmenname` AS `salesmansFirmenname`,
				`salesmansData`.`customersFirmennameZusatz` AS `salesmansFirmennameZusatz`,
				`salesmansData`.`customersFirmenInhaberVorname` AS `salesmansFirmenInhaberVorname`,
				`salesmansData`.`customersFirmenInhaberNachname` AS `salesmansFirmenInhaberNachname`,
				`salesmansData`.`customersFirmenInhaberAnrede` AS `salesmansFirmenInhaberAnrede`,
				`salesmansData`.`customersFirmenInhaber2Vorname` AS `salesmansFirmenInhaber2Vorname`,
				`salesmansData`.`customersFirmenInhaber2Nachname` AS `salesmansFirmenInhaber2Nachname`,
				`salesmansData`.`customersFirmenInhaber2Anrede` AS `salesmansFirmenInhaber2Anrede`,

				`salesmansData`.`customersAnsprechpartner1Vorname` AS `salesmansAnsprechpartner1Vorname`,
				`salesmansData`.`customersAnsprechpartner1Nachname` AS `salesmansAnsprechpartner1Nachname`,
				`salesmansData`.`customersAnsprechpartner1Anrede` AS `salesmansAnsprechpartner1Anrede`,
				`salesmansData`.`customersAnsprechpartner1Typ` AS `salesmansAnsprechpartner1Typ`,

				`salesmansData`.`customersAnsprechpartner2Vorname` AS `salesmansAnsprechpartner2Vorname`,
				`salesmansData`.`customersAnsprechpartner2Nachname` AS `salesmansAnsprechpartner2Nachname`,
				`salesmansData`.`customersAnsprechpartner2Anrede` AS `salesmansAnsprechpartner2Anrede`,
				`salesmansData`.`customersAnsprechpartner2Typ` AS `salesmansAnsprechpartner2Typ`,

				`salesmansData`.`customersCompanyStrasse` AS `salesmansCompanyStrasse`,
				`salesmansData`.`customersCompanyHausnummer` AS `salesmansCompanyHausnummer`,
				`salesmansData`.`customersCompanyCountry` AS `salesmansCompanyCountry`,
				`salesmansData`.`customersCompanyPLZ` AS `salesmansCompanyPLZ`,
				`salesmansData`.`customersCompanyOrt` AS `salesmansCompanyOrt`,

				`salesmansData`.`customersLieferadresseFirmenname` AS `salesmansLieferadresseFirmenname`,
				`salesmansData`.`customersLieferadresseFirmennameZusatz` AS `salesmansLieferadresseFirmennameZusatz`,
				`salesmansData`.`customersLieferadresseStrasse` AS `salesmansLieferadresseStrasse`,
				`salesmansData`.`customersLieferadresseHausnummer` AS `salesmansLieferadresseHausnummer`,
				`salesmansData`.`customersLieferadressePLZ` AS `salesmansLieferadressePLZ`,
				`salesmansData`.`customersLieferadresseOrt` AS `salesmansLieferadresseOrt`,
				`salesmansData`.`customersLieferadresseLand` AS `salesmansLieferadresseLand`,

				`salesmansData`.`customersRechnungsadresseFirmenname` AS `salesmansRechnungsadresseFirmenname`,
				`salesmansData`.`customersRechnungsadresseFirmennameZusatz` AS `salesmansRechnungsadresseFirmennameZusatz`,
				`salesmansData`.`customersRechnungsadresseStrasse` AS `salesmansRechnungsadresseStrasse`,
				`salesmansData`.`customersRechnungsadresseHausnummer` AS `salesmansRechnungsadresseHausnummer`,
				`salesmansData`.`customersRechnungsadressePLZ` AS `salesmansRechnungsadressePLZ`,
				`salesmansData`.`customersRechnungsadresseOrt` AS `salesmansRechnungsadresseOrt`,
				`salesmansData`.`customersRechnungsadresseLand` AS `salesmansRechnungsadresseLand`,

				`salesmansData`.`customersVertreterID` AS `salesmansVertreterID`,
				`salesmansData`.`customersVertreterName` AS `salesmansVertreterName`,
				`salesmansData`.`customersUseSalesmanDeliveryAdress` AS `salesmansUseSalesmanDeliveryAdress`,
				`salesmansData`.`customersUseSalesmanInvoiceAdress` AS `salesmansUseSalesmanInvoiceAdress`,

				`salesmansData`.`customersTyp` AS `salesmansTyp`,
				`salesmansData`.`customersGruppe` AS `salesmansGruppe`,
				`salesmansData`.`customersNotiz` AS `salesmansNotiz`,
				`salesmansData`.`customersActive` AS `salesmansActive`

				/* ------------------------------------------------------------------------------- */

			FROM `" . TABLE_ORDERS . "`

			LEFT JOIN `" . TABLE_CUSTOMERS . "` AS `customersData`
			ON(`" . TABLE_ORDERS . "`.`ordersKundennummer` = `customersData`.`customersKundennummer` )

			LEFT JOIN `" . TABLE_CUSTOMERS . "` AS `agentsData`
			ON(`customersData`.`customersVertreterID` = `agentsData`.`customersID` AND `customersData`.`customersVertreterID` != '')

			LEFT JOIN `" . TABLE_CUSTOMERS . "` AS `salesmansData`
			ON(`" . TABLE_ORDERS . "`.`ordersVertreterID` = `salesmansData`.`customersID` AND `" . TABLE_ORDERS . "`.`ordersVertreterID` != '')

			WHERE 1
				AND (
					`ordersBelichtungsDatum` = '". $arrPrintProductionFiles[$_REQUEST["editID"]]["printProductionFilesFileDate"] . "'
					OR
					`ordersProduktionsDatum` = '". $arrPrintProductionFiles[$_REQUEST["editID"]]["printProductionFilesFileDate"] . "'
				)
				" . $where . "

			ORDER BY `ordersKundennummer` ASC
		";
		// ORDER BY `ordersKundennummer` ASC

		$rs = $dbConnection->db_query($sql);

		$countRows = $dbConnection->db_getMysqlNumRows($rs);

		if($countRows > 0){
			while($ds = mysqli_fetch_assoc($rs)){
				foreach(array_keys($ds) as $field){
					$arrOrderProcessData[$ds["ordersID"]][$field] = $ds[$field];
				}
			}
		}
		else{
			$warningMessage .= ' Es wurden keine Produktionen gefunden!' . '<br />';
		}
		 return $arrOrderProcessData;
	}

	if($_POST["cancelDatas"] != ""){
		unset($_POST["searchPrintProductionFileID"]);
		unset($_REQUEST["searchPrintProductionFileID"]);

		$_POST["searchPrintProductionFileID"] = '';
		$_REQUEST["searchPrintProductionFileID"] = '';
	}

	// BOF DOWNLOAD FILE
	if($_GET["downloadFile"] != "" && $_GET["documentType"] != "") {
		$fileDirectory = DIRECTORY_PRINT_PRODUCTION_FILES . '';
		$_GET["downloadFile"] = utf8_decode(urldecode($_GET["downloadFile"]));

		require_once('classes/downloadFile.class.php');
		$thisDownload = new downloadFile($fileDirectory, $_GET["downloadFile"]);
		$errorMessage .= $thisDownload->startDownload();
	}
	// EOF DOWNLOAD FILE


	// BOF UPLOAD PRODUCTION PRINT FILE
	if($_POST["submitUploadProductionFile"] != ""){
		$fileIsSelected = false;
		if(empty($_FILES["uploadPrintProductionFile"]["name"])){
			$errorMessage .= ' Es wurde keine Datei ausgew&auml;hlt!' . '<br />';
			$fileIsSelected = false;
		}
		else {
			foreach($_FILES["uploadPrintProductionFile"]["name"] as $thisFileKey => $thisFileValue){
				$thisFileName = convertChars($_FILES["uploadPrintProductionFile"]["name"][$thisFileKey]);
				$thisFileType = strtolower(substr($thisFileName, -3));

				if($thisFileType != "pdf" && $thisFileType != ""){
					#$errorMessage .= ' Es wurde keine PDF-Datei ausgew&auml;hlt!' . '<br />';
					if(!$fileIsSelected){ $fileIsSelected = false; }
				}
				else {
					$thisFileName = renamePrintProductionFile($thisFileName);
					$thisFileDate = printProductionFilesFileDate($thisFileName);

					if($thisFileName != ""){
						$fileIsSelected = true;
						if(file_exists(DIRECTORY_PRINT_PRODUCTION_FILES . $thisFileName)){
							unlink(DIRECTORY_PRINT_PRODUCTION_FILES . $thisFileName);
						}

						if(!file_exists(DIRECTORY_PRINT_PRODUCTION_FILES . $thisFileName)){
							$copyResult = copy($_FILES["uploadPrintProductionFile"]["tmp_name"][$thisFileKey], DIRECTORY_PRINT_PRODUCTION_FILES . $thisFileName);
							unlink($_FILES["uploadPrintProductionFile"]["tmp_name"][$thisFileKey]);
							if(!$copyResult){
								$errorMessage .= ' Die PDF-Datei &quot;' . $thisFileName . '&quot;konnte nicht im Verzeichnis gespeichert werden!' . '<br />';
							}
							else {
								$successMessage .= ' Die PDF-Datei &quot;' . $thisFileName . '&quot;wurde im Verzeichnis gespeichert!' . '<br />';
								// BOF STORE INTO DB
									if($_POST["selectLanguageID"] == ''){
										$_POST["selectLanguageID"] = 'DE';
									}
									$sql = "
										/*INSERT IGNORE INTO `" . TABLE_PRINT_PRODUCTION_FILES . "` (*/
										REPLACE INTO `" . TABLE_PRINT_PRODUCTION_FILES . "` (
																	`printProductionFilesFileDate`,
																	`printProductionFilesFileName`,
																	`printProductionFilesFilePath`,
																	`printProductionFilesDataFilePath`,
																	`printProductionFilesLanguage`
															)
															VALUES (
																'" . $thisFileDate . "',
																'" . $thisFileName . "',
																'" . DIRECTORY_PRINT_PRODUCTION_FILES . $thisFileName . "',
																'" . $zipFileName . "',
																'" . $_POST["selectLanguageID"] . "'
															)

										";

										$rs = $dbConnection->db_query($sql);
										if($rs){
											$successMessage .= ' Die PDF-Datei &quot;' . $thisFileName . '&quot; wurde in der Datenbank gespeichert!' . '<br />';
										}
										else {
											$errorMessage .= ' Die PDF-Datei &quot;' . $thisFileName . '&quot; konnte nicht in der Datenbank gespeichert werden!' . '<br />';
										}
								// EOF STORE INTO DB
							}
						}
						else {
							$errorMessage .= ' Die PDF-Datei &quot;' . $thisFileName . '&quot; konnte nicht gespeichert werden, da sie schon existiert!' . '<br />';
						}
					}
					else {
						if(!$fileIsSelected){ $fileIsSelected = false; }
					}
				}
			}
			if(!$fileIsSelected){ $errorMessage .= ' Es wurde keine PDF-Datei ausgew&auml;hlt!' . '<br />'; }
		}
	}
	// EOF UPLOAD PRODUCTION PRINT FILE

	// BOF READ printProductionFiles
		$arrPrintProductionFiles = array();
		#$arrPrintProductionFiles = read_dir(DIRECTORY_PRINT_PRODUCTION_FILES);
		#rsort($arrPrintProductionFiles);
		$where = "";
		if($_REQUEST["searchPrintProductionFileID"] != ""){
			$where = " AND `printProductionFilesID` = '" . $_REQUEST["searchPrintProductionFileID"] . "'";
		}

		$sql = "
				SELECT
					`" . TABLE_PRINT_PRODUCTION_FILES . "`.`printProductionFilesID`,
					`" . TABLE_PRINT_PRODUCTION_FILES . "`.`printProductionFilesFileDate`,
					`" . TABLE_PRINT_PRODUCTION_FILES . "`.`printProductionFilesFileName`,
					`" . TABLE_PRINT_PRODUCTION_FILES . "`.`printProductionFilesFilePath`,
					`" . TABLE_PRINT_PRODUCTION_FILES . "`.`printProductionFilesDataFilePath`,
					`" . TABLE_PRINT_PRODUCTION_FILES . "`.`printProductionFilesLanguage`,

					COUNT(`" . TABLE_PRINT_PRODUCTION_ORDERS . "`.`printProductionOrdersID`) AS `countSelectedOrders`

				FROM `" . TABLE_PRINT_PRODUCTION_FILES . "`

				LEFT JOIN `" . TABLE_PRINT_PRODUCTION_ORDERS . "`
				ON(`" . TABLE_PRINT_PRODUCTION_FILES . "`.`printProductionFilesID` = `" . TABLE_PRINT_PRODUCTION_ORDERS . "`.`printProductionOrdersPrintProductionFilesID`)
				WHERE 1
					" . $where . "

					GROUP BY `" . TABLE_PRINT_PRODUCTION_FILES . "`.`printProductionFilesID`
				ORDER BY `printProductionFilesFileName` DESC

			";

		$rs = $dbConnection->db_query($sql);

		$countRows = $dbConnection->db_getMysqlNumRows($rs);

		if($countRows > 0){
			while($ds = mysqli_fetch_assoc($rs)){
				foreach(array_keys($ds) as $field){
					$arrPrintProductionFiles[$ds["printProductionFilesID"]][$field] = $ds[$field];
				}
			}
		}
		else{
			$warningMessage .= ' Es wurden keine Belichtungs-Dateien gefunden!' . '<br />';
		}
		if($_REQUEST["editID"] != ""){
			$thisPrintProductionFilePath = DIRECTORY_PRINT_PRODUCTION_FILES . $arrPrintProductionFiles[$_REQUEST["editID"]]["printProductionFilesFileName"];
			$arrThisPrintProductionFileInfo = pathinfo($thisPrintProductionFilePath);
			$thisPrintProductionDirPath = DIRECTORY_PRINT_PRODUCTION_FILES . $arrThisPrintProductionFileInfo["filename"] . '/';
			$zipFileName = $thisPrintProductionDirPath . basename($arrThisPrintProductionFileInfo["filename"]).'.zip';
		}
	// EOF READ printProductionFiles

	// BOF READ ALL PRODUCTION FILE DATAS FROM CURRENT DAY
		$sql = "
				SELECT
					`" . TABLE_PRINT_PRODUCTION_FILES . "`.`printProductionFilesID`,
					`" . TABLE_PRINT_PRODUCTION_FILES . "`.`printProductionFilesFileDate`,
					`" . TABLE_PRINT_PRODUCTION_FILES . "`.`printProductionFilesFileName`,
					`" . TABLE_PRINT_PRODUCTION_FILES . "`.`printProductionFilesFilePath`,
					`" . TABLE_PRINT_PRODUCTION_FILES . "`.`printProductionFilesDataFilePath`,
					`" . TABLE_PRINT_PRODUCTION_FILES . "`.`printProductionFilesLanguage`,

					`" . TABLE_PRINT_PRODUCTION_ORDERS . "`.`printProductionOrdersID`,
					`" . TABLE_PRINT_PRODUCTION_ORDERS . "`.`printProductionOrdersOrderID`,
					`" . TABLE_PRINT_PRODUCTION_ORDERS . "`.`printProductionOrdersPrintProductionFilesID`,
					`" . TABLE_PRINT_PRODUCTION_ORDERS . "`.`printProductionOrdersCompanyName`


				FROM `" . TABLE_PRINT_PRODUCTION_FILES . "`

				LEFT JOIN `" . TABLE_PRINT_PRODUCTION_ORDERS . "`
				ON(`" . TABLE_PRINT_PRODUCTION_FILES . "`.`printProductionFilesID` = `" . TABLE_PRINT_PRODUCTION_ORDERS . "`.`printProductionOrdersPrintProductionFilesID`)
				WHERE 1
					AND `" . TABLE_PRINT_PRODUCTION_FILES . "`.`printProductionFilesFileDate` = '" . $arrPrintProductionFiles[$_REQUEST["searchPrintProductionFileID"]]["printProductionFilesFileDate"] . "'


				ORDER BY `printProductionFilesFileName` DESC

			";

		$rs = $dbConnection->db_query($sql);

		$countRows = $dbConnection->db_getMysqlNumRows($rs);

		$arrPrintProductionFilesTodaysOrders = array();
		$arrPrintProductionFilesTodaysOrders2 = array();

		if($countRows > 0){
			while($ds = mysqli_fetch_assoc($rs)){
				foreach(array_keys($ds) as $field){
					$arrPrintProductionFilesTodaysOrders[$ds["printProductionFilesID"]][$ds["printProductionOrdersOrderID"]][$field] = $ds[$field];
				}
				$arrPrintProductionFilesTodaysOrderIDs[$ds["printProductionOrdersOrderID"]] = $ds["printProductionFilesID"];
			}
		}
#dd('arrPrintProductionFilesTodaysOrderIDs');

	// EOF READ ALL PRODUCTION FILE DATAS FROM CURRENT DAY

	// BOF STORE FILES AND DATAS
		if($_POST["storeDatas"] != ""){
			// BOF DELETE STORED DATAS
				// BOF DELETE STORED FILES
					$arrLoadPrintProductionData = getPrintProductionDatas();
					if(!empty($arrLoadPrintProductionData)){
						foreach($arrLoadPrintProductionData as $thisLoadPrintProductionDataKey => $thisLoadPrintProductionDataValue){
							$thisFileToDelete = $thisLoadPrintProductionDataValue["printProductionOrdersLayoutFilePath"];
							if(file_exists($thisFileToDelete)){
								unlink($thisFileToDelete);
							}
						}
					}
				// EOF DELETE STORED FILES

				// BOF DELETE STORED DATAS
					$sql = "
						DELETE FROM `" . TABLE_PRINT_PRODUCTION_ORDERS . "`
							WHERE 1
								AND `printProductionOrdersPrintProductionFilesID` = '" . $_POST["searchPrintProductionFileID"]. "'

						";
					$rs = $dbConnection->db_query($sql);
				// EOF DELETE STORED DATAS
			// EOF DELETE STORED DATAS

			if($_POST["editID"] != "" && !empty($_POST["arrSelectOrderID"])) {
				// BOF CREATE DIR FOR PRODUCTION FILES
					if(!is_dir($thisPrintProductionDirPath)){
						$warningMessage .= ' Der Ordner &quot;' . basename($thisPrintProductionDirPath) . '&quot; existiert nicht!' . '<br />';
						$rsMkDir = mkdir($thisPrintProductionDirPath);
						if($rsMkDir){
							$successMessage .= ' Der Ordner &quot;' . basename($thisPrintProductionDirPath) . '&quot; wurde angelegt!' . '<br />';
						}
						else {
							$errorMessage .= ' Der Ordner &quot;' . basename($thisPrintProductionDirPath) . '&quot; konnte nicht angelegt werden!' . '<br />';
						}
					}
					else {
						$successMessage .= ' Der Ordner &quot;' . basename($thisPrintProductionDirPath) . '&quot; ist vorhanden!' . '<br />';
					}
				// BOF CREATE DIR FOR PRODUCTION FILES

				$arrFilesToZip = array();

				// BOF COPY PRINT PRODUCTION FILE
					if(is_dir($thisPrintProductionDirPath)){
						$filePathSource = $thisPrintProductionFilePath;
						$filePathTarget = $thisPrintProductionDirPath . basename($thisPrintProductionFilePath);
						if(file_exists($filePathTarget)){
							$rsUnlink = unlink($filePathTarget);
						}
						$rsCopy = copy($filePathSource, $filePathTarget);
						if(!$rsCopy){
							$errorMessage .= ' Der Belichtungsbogen &quot;' . basename($thisPrintProductionFilePath) . '&quot; konnte nicht im Zielordner &quot;' . ($thisPrintProductionDirPath) . '&quot; abgelegt werden!' . '<br />';
						}
						else {
							$successMessage .= ' Der Belichtungsbogen &quot;' . basename($thisPrintProductionFilePath) . '&quot; wurde im Zielordner &quot;' . ($thisPrintProductionDirPath) . '&quot; abgelegt!' . '<br />';
							$arrFilesToZip[] = $filePathTarget;
						}
					}
				// EOF COPY PRINT PRODUCTION FILE

				// BOF READ DATAS FROM ORDERS PROCESS
				$arrOrderProcessData = getOrderProcessDatas($_POST["arrSelectOrderID"]);
				// BOF READ DATAS FROM ORDERS PROCESS


				foreach($_POST["arrSelectOrderID"] as $thisSelectOrderIdKey => $thisSelectOrderIdValue){
					$thisPrintProductionDataValue = $arrOrderProcessData[$thisSelectOrderIdKey];
					if($thisSelectOrderIdValue == "1"){
						// BOF COPY FILE
							$filePathSource = "";
							$filePathTarget = "";
							if($_FILES["arrSelectOrderID"]["tmp_name"][$thisSelectOrderIdKey] != ""){
								$filePathSource = $_FILES["arrSelectOrderID"]["tmp_name"][$thisSelectOrderIdKey];
								$filePathTarget = DIRECTORY_UPLOAD_FILES . convertChars($_FILES["arrSelectOrderID"]["name"][$thisSelectOrderIdKey]);
								$filePathTarget = preg_replace("/ copy/", "", $filePathTarget);

								// BOF STORE FILE IN CUSTOMERS-LAYOUT-DIR
								$thisCustomerNumber = $thisPrintProductionDataValue["ordersKundennummer"];
								$thisFileTime = filemtime(stripslashes($filePathSource));
								$thisDocumentNumber = "KA-" . date('ymdHis');

								$thisDeleteFile = utf8_decode(urldecode(basename($filePathTarget)));
								if(file_exists(DIRECTORY_UPLOAD_FILES . ($thisDeleteFile))){
									$resultUnlink = unlink(DIRECTORY_UPLOAD_FILES . $thisDeleteFile);
									$thisDeleteFile = (urldecode(basename($filePathTarget)));
									$sql = "DELETE
												FROM `" . TABLE_CREATED_DOCUMENTS ."`
												WHERE 1
													AND `createdDocumentsFilename` = '" . addslashes(DIRECTORY_UPLOAD_FILES . $thisDeleteFile) . "'
													AND `createdDocumentsType` = 'KA'
									";
									$rs = $dbConnection->db_query($sql);
								}

								$sql_insert = "
									INSERT INTO `" . TABLE_CREATED_DOCUMENTS ."` (
												`createdDocumentsID`,
												`createdDocumentsType`,
												`createdDocumentsNumber`,
												`createdDocumentsCustomerNumber`,
												`createdDocumentsTitle`,
												`createdDocumentsFilename`,
												`createdDocumentsContent`,
												`createdDocumentsUserID`,
												`createdDocumentsTimeCreated`
											)
											VALUES (
												'%',
												'KA',
												'" . $thisDocumentNumber . "',
												'" . $thisCustomerNumber."',
												'" . addslashes(basename($filePathTarget))."',
												'" . addslashes($filePathTarget) . "',
												'-',
												'" . $_SESSION["usersID"]."',
												'" . date("Y-m-d H:i:s", filemtime($filePathSource))."'
											)
									";
								$rs_insert = $dbConnection->db_query($sql_insert);

								$resultCopy = copy($filePathSource, DIRECTORY_UPLOAD_FILES . utf8_decode(basename($filePathTarget)));
								if(file_exists($filePathSource)){
									$rsUnlink = unlink($filePathSource);
								}
								$filePathSource = DIRECTORY_UPLOAD_FILES . utf8_decode(basename($filePathTarget));
								// EOF STORE FILE IN CUSTOMERS-LAYOUT-DIR
							}
							else if($_POST["arrSelectOrderExistingLayoutFile"][$thisSelectOrderIdKey]){
								// BOF USE EXISTING FILE IF NO FILE WAS UPLOADED
								$filePathSource = DIRECTORY_UPLOAD_FILES . $_POST["arrSelectOrderExistingLayoutFile"][$thisSelectOrderIdKey];
								#$filePathTarget = $thisPrintProductionDirPath . convertChars($_POST["arrSelectOrderExistingLayoutFile"][$thisSelectOrderIdKey]);
								if(!file_exists($filePathSource)){
									#$filePathSource = "";
								}
								if(!file_exists($filePathTarget)){
									#$filePathTarget = "";
								}
								// EOF USE EXISTING FILE IF NO FILE WAS UPLOADED
							}

							$filePathTarget = $thisPrintProductionDirPath . basename($filePathSource);
							if(mb_detect_encoding($filePathSource) == "UTF-8") {
								$filePathSource = utf8_decode($filePathSource);
							}

							if($filePathSource != "" && $filePathTarget != ""){
								if(file_exists($filePathTarget)){
									$rsUnlink = unlink($filePathTarget);
								}
								$rsCopy = copy($filePathSource, $thisPrintProductionDirPath . basename($filePathTarget));
								if(!$rsCopy){
									$errorMessage .= ' Der Korrekturabzug &quot;' . basename($filePathTarget) . '&quot; konnte nicht im Zielordner &quot;' . ($thisPrintProductionDirPath) . '&quot; abgelegt werden!' . '<br />';
								}
								else {
									$successMessage .= ' Der Korrekturabzug &quot;' . basename($filePathTarget) . '&quot; wurde im Zielordner &quot;' . ($thisPrintProductionDirPath) . '&quot; abgelegt!' . '<br />';
								}
							}
						// EOF COPY FILE

						// $thisPrintProductionDataValue = $arrOrderProcessData[$thisSelectOrderIdKey]; # MOVED

						// BOF GET ADRESSES
						getSenderAndRecipientAdress();
						// BOF GET ADRESSES

						// BOF STORE DATAS
							$thisLayoutFile = '';
							#if($_FILES["arrSelectOrderID"]["name"][$thisSelectOrderIdKey] != ""){
								$thisLayoutFile = $filePathTarget;
							#}
							$thisLayoutFile = $filePathTarget;

							// BOF SERIALIZE PRODUCT DATAS
							$arrProductDatas = array();
							$arrProductDatas["ordersArtikelNummer"] = $thisPrintProductionDataValue["ordersArtikelNummer"];
							$arrProductDatas["ordersArtikelBezeichnung"] = addslashes($thisPrintProductionDataValue["ordersArtikelBezeichnung"]);
							$arrProductDatas["ordersArtikelMenge"] = $thisPrintProductionDataValue["ordersArtikelMenge"];
							$arrProductDatas["ordersDruckFarbe"] = $thisPrintProductionDataValue["ordersDruckFarbe"];
							$arrProductDatas["ordersArtikelPrintColorsCount"] = $thisPrintProductionDataValue["ordersArtikelPrintColorsCount"];
							$arrProductDatas["ordersAufdruck"] = $thisPrintProductionDataValue["ordersAufdruck"];
							$arrProductDatas["ordersKommission"] = $thisPrintProductionDataValue["ordersKommission"];

							if($thisPrintProductionDataValue["ordersAdditionalArtikelMenge"] > 0){
								$arrProductDatas["orderCategoriesPartNumber"] = $arrOrderCategoriesTypeDatas[$thisPrintProductionDataValue["ordersAdditionalArtikelKategorieID"]]["orderCategoriesPartNumber"];

								$arrProductDatas["ordersAdditionalArtikelMenge"] = $thisPrintProductionDataValue["ordersAdditionalArtikelMenge"] ;

								$arrProductDatas["orderCategoriesName"] = $arrOrderCategoriesTypeDatas[$thisPrintProductionDataValue["ordersAdditionalArtikelKategorieID"]]["orderCategoriesName"];
							}

							$strProductDatas = serialize($arrProductDatas);
							// EOF SERIALIZE PRODUCT DATAS

							// BOF CREATE PDF CONTENT

								// BOF GET LANGUAGE FILE
									$thisSelectLanguageID = 'DE';
									if($_POST["selectLanguageID"] != ""){
										$thisSelectLanguageID = $_POST["selectLanguageID"];
									}
									require_once(preg_replace("/{###LANGUAGE_SHORT_NAME###}/", strtolower($thisSelectLanguageID), LANGUAGE_FILE_NAME));
								// EOF GET LANGUAGE FILE
								$thisCreatedPdfName = "_temp_" . $thisPrintProductionDataValue["ordersKundennummer"] . "_data.pdf";

								$pdfContentPage = "";

								// BOF STYLES
									$pdfContentPage .= '
										<style type="text/css">
											<!--
												* { font-family: arial;}
												h1 { font-weight:normal; font-size: 16pt; padding: 0 0 2mm 0; margin:0;border-bottom: 0.1mm solid #000000;}
												h2 { font-weight:normal; font-size: 16pt; padding: 0 0 2mm 0; margin:0}
												table {font-weight: normal; font-size:14pt;margin:0 0 10mm 20mm;}
												td { vertical-align: top; border-bottom:0.1mm solid #333333; padding: 1mm 2mm 1mm 2mm;}
												p { font-size:15pt; color: #CC0000;  padding:0; margin: 2mm 0 2mm 0;}
												hr { height: 0.1mm; border: 0pt solid #333333; color:#333333; background-color:#333333; padding:0; margin: 2mm 0 2mm 0;}
											-->
										</style>
									';

								// EOF STYLES

								// BOF PAGE 1: DATAS
									$pdfContentPage .= '<page pageset="old" backtop="10mm" backbottom="55mm" backleft="0mm" backright="10mm">';
									$pdfContentPage .= '<h1>';
									$pdfContentPage .= '{###PRODUCTION_DATA###}' . '<br><br>';
									#$pdfContentPage .= ' • ';
									$pdfContentPage .= '{###CUSTUMER_NUMBER###}: ' . $thisPrintProductionDataValue["ordersKundennummer"];
									$pdfContentPage .= ' • ';
									$pdfContentPage .= '{###COMPANY_NAME###}: ' . $thisPrintProductionDataValue["ordersKundenName"];
									$pdfContentPage .= '</h1>';
									$pdfContentPage .= '<p>{###EXPOSITION_FILM###} / {###FILE###}: &quot;' . $arrPrintProductionFiles[$_REQUEST["editID"]]["printProductionFilesFileName"] . '&quot; vom ' . formatDate($arrPrintProductionFiles[$_REQUEST["editID"]]["printProductionFilesFileDate"], "display") . ' - Bogen ' . getProductionFilesFileSheetNumber($arrPrintProductionFiles[$_REQUEST["editID"]]["printProductionFilesFileName"]) . '</p>';

									if($arrProductDatas["ordersKommission"] != "" || $arrProductDatas["ordersAufdruck"] != ""){
										$pdfContentPage .= '<p>';
									}

									if($arrProductDatas["ordersKommission"] != ""){
										$pdfContentPage .= '{###COMMISSION###}: ' . $arrProductDatas["ordersKommission"];
									}

									if($arrProductDatas["ordersKommission"] != "" && $arrProductDatas["ordersAufdruck"] != ""){
										$pdfContentPage .= ' • ';
									}

									if($arrProductDatas["ordersAufdruck"] != ""){
										$pdfContentPage .= '{###PRINT_TEXT###}:: ' . $arrProductDatas["ordersAufdruck"];
									}
									if($arrProductDatas["ordersKommission"] != "" || $arrProductDatas["ordersAufdruck"] != ""){
										$pdfContentPage .= '</p>';
									}

									$pdfContentPage .= '<table border="0" cellpadding="0" cellspacing="0">';

									$pdfContentPage .= '<tr>';
									$pdfContentPage .= '<td>{###PRODUCT_NUMMER###}:</td>';
									$pdfContentPage .= '<td>';
									$pdfContentPage .= $arrProductDatas["ordersArtikelNummer"];
									$pdfContentPage .= '</td>';
									$pdfContentPage .= '</tr>';

									$pdfContentPage .= '<tr>';
									$pdfContentPage .= '<td>{###PRODUCT_NAME###}:</td>';
									$pdfContentPage .= '<td>';
									$pdfContentPage .= $arrProductDatas["ordersArtikelBezeichnung"];
									$pdfContentPage .= '</td>';
									$pdfContentPage .= '</tr>';

									$pdfContentPage .= '<tr>';
									$pdfContentPage .= '<td>{###PRODUCT_QUANTITY###}:</td>';
									$pdfContentPage .= '<td>';
									$pdfContentPage .= $arrProductDatas["ordersArtikelMenge"];
									$pdfContentPage .= '</td>';
									$pdfContentPage .= '</tr>';

									$pdfContentPage .= '<tr>';
									$pdfContentPage .= '<td>{###PRODUCT_COLOR_NAMES###}:</td>';
									$pdfContentPage .= '<td>';
									$pdfContentPage .= $arrProductDatas["ordersDruckFarbe"];
									$pdfContentPage .= '</td>';
									$pdfContentPage .= '</tr>';

									$pdfContentPage .= '<tr>';
									$pdfContentPage .= '<td>{###PRODUCT_COLOR_COUNT###}:</td>';
									$pdfContentPage .= '<td>';
									$pdfContentPage .= $arrProductDatas["ordersArtikelPrintColorsCount"];
									$pdfContentPage .= '</td>';
									$pdfContentPage .= '</tr>';

									if($thisPrintProductionDataValue["ordersAdditionalArtikelMenge"] > 0){
										$pdfContentPage .= '<tr>';
										$pdfContentPage .= '<td>{###PRODUCT_ADDITIONAL_NAME###}:</td>';
										$pdfContentPage .= '<td>';
										$pdfContentPage .= $arrProductDatas["orderCategoriesName"];
										$pdfContentPage .= '</td>';
										$pdfContentPage .= '</tr>';

										$pdfContentPage .= '<tr>';
										$pdfContentPage .= '<td>{###PRODUCT_ADDITIONAL_QUANTITY###}:</td>';
										$pdfContentPage .= '<td>';
										$pdfContentPage .= $arrProductDatas["ordersAdditionalArtikelMenge"];
										$pdfContentPage .= '</td>';
										$pdfContentPage .= '</tr>';
									}

									/*
									$arrProductDatas["ordersArtikelNummer"] = $thisPrintProductionDataValue["ordersArtikelNummer"];
									$arrProductDatas["ordersArtikelBezeichnung"] = addslashes($thisPrintProductionDataValue["ordersArtikelBezeichnung"]);
									$arrProductDatas["ordersArtikelMenge"] = $thisPrintProductionDataValue["ordersArtikelMenge"];
									$arrProductDatas["ordersDruckFarbe"] = $thisPrintProductionDataValue["ordersDruckFarbe"];
									$arrProductDatas["ordersArtikelPrintColorsCount"] = $thisPrintProductionDataValue["ordersArtikelPrintColorsCount"];

									if($thisPrintProductionDataValue["ordersAdditionalArtikelMenge"] > 0){
										$arrProductDatas["orderCategoriesPartNumber"] = $arrOrderCategoriesTypeDatas[$thisPrintProductionDataValue["ordersAdditionalArtikelKategorieID"]]["orderCategoriesPartNumber"];

										$arrProductDatas["ordersAdditionalArtikelMenge"] = $thisPrintProductionDataValue["ordersAdditionalArtikelMenge"] ;

										$arrProductDatas["orderCategoriesName"] = $arrOrderCategoriesTypeDatas[$thisPrintProductionDataValue["ordersAdditionalArtikelKategorieID"]]["orderCategoriesName"];
									}
									*/

									$pdfContentPage .= '</table>';

									$pdfContentPage .= '<hr>';

									$pdfContentPage .= '<h2>';
									$pdfContentPage .= '{###SENDER_ADRESS###}:';
									$pdfContentPage .= '</h2>';

									$pdfContentPage .= '<table border="0" cellpadding="0" cellspacing="0">';
									$pdfContentPage .= '<tr>';
									$pdfContentPage .= '<td>{###CUSTUMER_NUMBER###}:</td>';
									$pdfContentPage .= '<td>' . $arrThisSenderAdress["USE"]["knr"] . '</td>';
									$pdfContentPage .= '</tr>';
									$pdfContentPage .= '<tr>';
									$pdfContentPage .= '<td>{###COMPANY_NAME###}:</td>';
									$pdfContentPage .= '<td>' . $arrThisSenderAdress["USE"]["name"] . '</td>';
									$pdfContentPage .= '</tr>';
									$pdfContentPage .= '<tr>';
									$pdfContentPage .= '<td>{###STREET###}:</td>';
									$pdfContentPage .= '<td>' . $arrThisSenderAdress["USE"]["street"] . ' ' .  $arrThisSenderAdress["USE"]["streetNumber"] . '</td>';
									$pdfContentPage .= '</tr>';
									$pdfContentPage .= '<tr>';
									$pdfContentPage .= '<td>{###ZIPCODE###} / {###CITY###}:</td>';
									$pdfContentPage .= '<td>' . $arrThisSenderAdress["USE"]["zipcode"] . ' ' .  $arrThisSenderAdress["USE"]["city"] . '</td>';
									$pdfContentPage .= '</tr>';
									$pdfContentPage .= '<tr>';
									$pdfContentPage .= '<td>{###COUNTRY###}:</td>';
									$pdfContentPage .= '<td>' . $arrThisSenderAdress["USE"]["country"] . '</td>';
									$pdfContentPage .= '</tr>';
									$pdfContentPage .= '</table>';

									$pdfContentPage .= '<hr>';

									$pdfContentPage .= '<h2>';
									$pdfContentPage .= '{###RECIPIENT_ADRESS###}:';
									$pdfContentPage .= '</h2>';

									$pdfContentPage .= '<table border="0" cellpadding="0" cellspacing="0">';
									$pdfContentPage .= '<tr>';
									$pdfContentPage .= '<td>{###CUSTUMER_NUMBER###}:</td>';
									$pdfContentPage .= '<td>' . $arrThisRecipientAdress["USE"]["knr"] . '</td>';
									$pdfContentPage .= '</tr>';
									$pdfContentPage .= '<tr>';
									$pdfContentPage .= '<td>{###COMPANY_NAME###}:</td>';
									$pdfContentPage .= '<td>' . $arrThisRecipientAdress["USE"]["name"] . '</td>';
									$pdfContentPage .= '</tr>';
									$pdfContentPage .= '<tr>';
									$pdfContentPage .= '<td>{###STREET###}:</td>';
									$pdfContentPage .= '<td>' . $arrThisRecipientAdress["USE"]["street"] . ' ' .  $arrThisRecipientAdress["USE"]["streetNumber"] . '</td>';
									$pdfContentPage .= '</tr>';
									$pdfContentPage .= '<tr>';
									$pdfContentPage .= '<td>{###ZIPCODE###} / {###CITY###}:</td>';
									$pdfContentPage .= '<td>' . $arrThisRecipientAdress["USE"]["zipcode"] . ' ' .  $arrThisRecipientAdress["USE"]["city"] . '</td>';
									$pdfContentPage .= '</tr>';
									$pdfContentPage .= '<tr>';
									$pdfContentPage .= '<td>{###COUNTRY###}:</td>';
									$pdfContentPage .= '<td>' . $arrThisRecipientAdress["USE"]["country"] . '</td>';
									$pdfContentPage .= '</tr>';
									$pdfContentPage .= '</table>';

									if($_POST["arrSelectOrderPaymentTypeCashOnDelivery"][$thisSelectOrderIdKey] == "1"){
										$pdfContentPage .= '<p style="color:#FF0000;border:1mm solid #FF0000;padding: 2mm;"><b>NO DPD !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!</b></p>';
									}

									$pdfContentPage .= '';
									#$pdfContentPage .= '<hr>';
									$pdfContentPage .= '</page>';
								// EOF PAGE 1: DATAS

								// BOF PAGE 2: LAYOUT
									// UPLOADED FILE FILE BE MERGED WITH PAGE 1
								// EOF PAGE 2: LAYOUT

								// BOF INCLUDE LANGUAGE
								if(!empty($arrLanguageData)){
									foreach($arrLanguageData as $arrLanguageDataKey => $arrLanguageDataValue){
										$pattern = '{###' . $arrLanguageDataKey . '###}';

										if(mb_detect_encoding($arrLanguageDataValue) == "UTF-8"){
											#$arrLanguageDataValue = (utf8_decode($arrLanguageDataValue));
										}
										#$arrLanguageDataValue = htmlentities($arrLanguageDataValue);
										// BOF
											#$thisLocalType = strtolower($_POST["selectLanguageID"]) . '_' . strtoupper($_POST["selectLanguageID"]);
											setlocale(LC_CTYPE, $thisLocalType); // cp1254
											setlocale(LC_ALL, $thisLocalType); // cp1254


											#iconv_set_encoding("internal_encoding", "UTF-8");
											#iconv_set_encoding("output_encoding", "cp1254");
											#$arrLanguageDataValue = iconv('UTF-8', 'ISO8859-9', $arrLanguageDataValue);
											#$arrLanguageDataValue = iconv('UTF-8', 'ASCII//TRANSLIT', $arrLanguageDataValue);
											#$arrLanguageDataValue = iconv('UTF-8', 'cp1254', $arrLanguageDataValue);
											#$arrLanguageDataValue = iconv('UTF-8', 'ISO8859-9', $arrLanguageDataValue);
										// EOF

										$replace = ($arrLanguageDataValue);
										$pdfContentPage = preg_replace('/' . $pattern . '/', ($replace), $pdfContentPage);
									}
								}
								// EOF INCLUDE LANGUAGE

								$pdfContentPage = removeUnnecessaryChars($pdfContentPage);

								// BOF CREATE PDF
									require_once(DIRECTORY_HTML2PDF . 'html2pdf.class.php');
									// utf8
									try {
										$html2pdf = new HTML2PDF('L', 'A4', strtolower($_POST["selectLanguageID"]), true, 'utf8', array(22, 5, 5, 5));

										if(file_exists($thisPrintProductionDirPath . $thisCreatedPdfName)) {
											unlink($thisPrintProductionDirPath . $thisCreatedPdfName);
										}
										clearstatcache();

										// BOF TEST PAGE SPLIT
											if($userDatas["usersLogin"] != 'xxxthorsten'){
												$pdfContentPage = preg_replace('/#pdfContentArea/', '', $pdfContentPage);
												$pdfContentPage = preg_replace('/<div id="pdfContentArea">/', '', $pdfContentPage);
												$pdfContentPage = preg_replace('/<\/div><page_header>/ismU', '<page_header>', $pdfContentPage);
											}
										// BOF TEST PAGE SPLIT

										ob_start();
										echo $pdfContentPage;
										$contentPDF = ob_get_clean();

										$html2pdf->writeHTML($contentPDF, isset($_GET['vuehtml']));
										$html2pdf->Output($thisCreatedPdfName, 'F', $thisPrintProductionDirPath);

										if(file_exists($thisCreatedPdfName)) {
											unlink($thisCreatedPdfName);
										}
										clearstatcache();
									}
									catch(HTML2PDF_exception $e) {
										echo $e;
										exit;
									}

									if(file_exists($thisPrintProductionDirPath . $thisCreatedPdfName)) {
										#$successMessage .= 'Die PDF-Datei &quot;' . $thisCreatedPdfName . '&quot wurde generiert. '.'<br />';
										$showPdfDownloadLink = true;

										// BOF MERGE FILES
											$arrClassMethods = get_class_methods($html2pdf);
											unset($html2pdf);

											if(class_exists('FPDF') != true) {
												#require_once('fpdf/fpdf.php');
											}
											if(class_exists('FPDI') != true) {
												#require_once('fpdi/fpdi.php');
											}

											require_once(DIRECTORY_PDF_MERGER . "PDFMerger.php");

											$pdf = new PDFMerger;
											$fileMergedPdfName = $thisPrintProductionDirPath . $thisPrintProductionDataValue["ordersKundennummer"] . '_' . $thisSelectOrderIdKey . '_' . 'data_layout.pdf';

											if(file_exists($thisPrintProductionDirPath . $thisCreatedPdfName)){
												$pdf->addPDF($thisPrintProductionDirPath . $thisCreatedPdfName, 'all');
											}
											if(file_exists($filePathTarget)){
												$pdf->addPDF($filePathTarget, 'all');
											}

											if(file_exists($thisPrintProductionDirPath . $thisCreatedPdfName) && file_exists($filePathTarget)){

												if(file_exists($fileMergedPdfName)){
													unlink($fileMergedPdfName);
												}

												$pdf->merge('file', $fileMergedPdfName);

												unlink($thisPrintProductionDirPath . $thisCreatedPdfName);
												unlink($filePathTarget);

												$arrFilesToZip[] = $fileMergedPdfName;
											}
										// EOF MERGE FILES

									}
									else {
										$errorMessage .= 'Die PDF-Datei &quot;' . $thisCreatedPdfName . '&quot konnte nicht generiert werden. '.'<br />';
									}
									clearstatcache();
								// BOF CREATE PDF

							// EOF CREATE PDF CONTENT
							$thisPaymentTypeCashOnDelivery = $_POST["arrSelectOrderPaymentTypeCashOnDelivery"][$thisSelectOrderIdKey];
							if($thisPaymentTypeCashOnDelivery != "1"){
								$thisPaymentTypeCashOnDelivery = 0;
							}
							$sql = "
								INSERT IGNORE INTO `" . TABLE_PRINT_PRODUCTION_ORDERS . "` (
												`printProductionOrdersID`,
												`printProductionOrdersOrderID`,
												`printProductionOrdersPrintProductionFilesID`,

												`printProductionOrdersCompanyID`,
												`printProductionOrdersCompanyNumber`,
												`printProductionOrdersCompanyName`,
												`printProductionOrdersCompanyNameAdd`,

												`printProductionOrdersSenderID`,
												`printProductionOrdersSenderNumber`,
												`printProductionOrdersSenderName`,
												`printProductionOrdersSenderNameAdd`,
												`printProductionOrdersSenderStreet`,
												`printProductionOrdersSenderStreetNumber`,
												`printProductionOrdersSenderZipcode`,
												`printProductionOrdersSenderCity`,
												`printProductionOrdersSenderCountryID`,
												`printProductionOrdersSenderCountryName`,

												`printProductionOrdersRecipientID`,
												`printProductionOrdersRecipientNumber`,
												`printProductionOrdersRecipientName`,
												`printProductionOrdersRecipientNameAdd`,
												`printProductionOrdersRecipientStreet`,
												`printProductionOrdersRecipientStreetNumber`,
												`printProductionOrdersRecipientZipcode`,
												`printProductionOrdersRecipientCity`,
												`printProductionOrdersRecipientCountryID`,
												`printProductionOrdersRecipientCountryName`,

												`printProductionOrdersProductDatas`,
												`printProductionOrdersLayoutFilePath`,
												`printProductionOrdersComment`,
												`printProductionOrdersMandatory`,
												`printProductionOrdersPaymentTypeCashOnDelivery`
											)
											VALUES (
												'%',
												'" . $thisSelectOrderIdKey . "',
												'" . $_POST["editID"] . "',

												'" . $thisPrintProductionDataValue["ordersCustomerID"] . "',
												'" . $thisPrintProductionDataValue["ordersKundennummer"] . "',
												'" . addslashes($thisPrintProductionDataValue["ordersKundenName"]) . "',
												'',

												'" . $arrThisSenderAdress["USE"]["id"] . "',
												'" . $arrThisSenderAdress["USE"]["knr"] . "',
												'" . addslashes($arrThisSenderAdress["USE"]["name"]) . "',
												'" . addslashes($arrThisSenderAdress["USE"]["nameAdd"]) . "',
												'" . addslashes($arrThisSenderAdress["USE"]["street"]) . "',
												'" . addslashes($arrThisSenderAdress["USE"]["streetNumber"]) . "',
												'" . $arrThisSenderAdress["USE"]["zipcode"] . "',
												'" . addslashes($arrThisSenderAdress["USE"]["city"]) . "',
												'" . $arrThisSenderAdress["USE"]["countryID"] . "',
												'" . addslashes($arrThisSenderAdress["USE"]["country"]) . "',

												'" . $arrThisRecipientAdress["USE"]["id"] . "',
												'" . $arrThisRecipientAdress["USE"]["knr"] . "',
												'" . addslashes($arrThisRecipientAdress["USE"]["name"]) . "',
												'" . addslashes($arrThisRecipientAdress["USE"]["nameAdd"]) . "',
												'" . addslashes($arrThisRecipientAdress["USE"]["street"]) . "',
												'" . addslashes($arrThisRecipientAdress["USE"]["streetNumber"]) . "',
												'" . $arrThisRecipientAdress["USE"]["zipcode"] . "',
												'" . addslashes($arrThisRecipientAdress["USE"]["city"]) . "',
												'" . $arrThisRecipientAdress["USE"]["countryID"] . "',
												'"  .$arrThisRecipientAdress["USE"]["country"] . "',

												'" . addslashes($strProductDatas) . "',
												'" . $thisLayoutFile . "',
												'',
												'" . $thisPrintProductionDataValue["ordersMandant"] . "',
												'" . $thisPaymentTypeCashOnDelivery . "'
											)
								";
							$rs = $dbConnection->db_query($sql);
							if(!$rs){
								$errorMessage .= ' Die Daten konnten nicht gespeichert werden !' . '<br />';
								echo mysqli_error();
							}
							else {
								$successMessage .= ' Die Daten wurden gespeichert! ' . '<br />';
							}
						// EOF STORE DATAS
					}
				}

				// BOF SET PRODUCTION PRINTING PLANT
					$arrSql_Where = array();
					foreach($_POST["arrSelectOrderID"] as $thisSelectOrderIdKey => $thisSelectOrderIdValue){
						$arrSql_Where[] = " `ordersID` = '" . $thisSelectOrderIdKey . "' ";
					}
					$sql = "UPDATE
								`" . TABLE_ORDERS . "`
								SET `ordersProductionPrintingPlant` = '" . $_POST["selectLanguageID"] . "'
								WHERE 1 AND (
									" . implode(" OR ", $arrSql_Where). "
								)
						";
					#dd('sql');
					$rs = $dbConnection->db_query($sql);
					if(!$rs){
						$errorMessage .= ' Der Produktionsstandort konnte nicht gespeichert werden !' . '<br />';
						echo mysqli_error();
					}
					else {
						$successMessage .= ' Der Produktionsstandort wurde gespeichert! ' . '<br />';
					}
				// EOF SET PRODUCTION PRINTING PLANT

				// BOF CREATE ZIP FILE
					if(file_exists($thisPrintProductionDirPath . basename($arrThisPrintProductionFileInfo["filename"]).'.zip')){
						unlink($thisPrintProductionDirPath . basename($arrThisPrintProductionFileInfo["filename"]).'.zip');
					}

					$createCommand = ZIP_PATH.' a ' . $zipFileName . ' ' . BASEPATH . $thisPrintProductionDirPath . '';
					$arrExecResults[] = $createCommand;
					$arrExecResults[] = exec($createCommand, $error);
					if(!empty($error)) {
						$arrExecResults[] = array('1. ZIP: error', $error);
					}
				// EOF CREATE ZIP FILE

				// BOF STORE ZIPNAME
					$sql = "UPDATE
									`" . TABLE_PRINT_PRODUCTION_FILES . "`
									SET
										`printProductionFilesDataFilePath` = '" . $zipFileName . "',
										`printProductionFilesLanguage` = '" . $_POST["selectLanguageID"] . "'
									WHERE 1
										AND `printProductionFilesID` = " . $_POST["editID"] . "
						";
					$rs = $dbConnection->db_query($sql);
				// EOF STORE ZIPNAME

				// BOF DELETE ZIP SOURCE FILES
				if(!empty($arrFilesToZip)){
					foreach($arrFilesToZip as $thisFileToZip){
						if(file_exists($thisFileToZip)){
							unlink($thisFileToZip);
						}
					}
				}
				// EOF DELETE ZIP SOURCE FILES
			}
			else {
				if($_POST["editID"] == ""){
					$errorMessage .= ' Es ist keine Belichtungs-Datei zugeordnet!' . '<br />';
				}
				if(empty($_POST["arrSelectOrderID"])){
					$errorMessage .= ' Es sind keine Produktionen zugeordnet!' . '<br />';
				}
			}
		}
	// EOF STORE FILES AND DATAS

	// BOF READ ORDERS
		if($_REQUEST["searchPrintProductionFileID"] != ""){
			$_REQUEST["editID"] = $_REQUEST["searchPrintProductionFileID"];
			// BOF READ STORED PRODUCTION FILE ORDER DATAS
				$arrLoadPrintProductionData = array();
				$arrLoadPrintProductionData = getPrintProductionDatas();
			// EOF READ STORED PRODUCTION FILE ORDER DATAS

			// BOF READ DATAS FROM ORDERS PROCESS
				$arrOrderProcessData = getOrderProcessDatas();
			// EOF READ DATAS FROM ORDERS PROCESS

			// BOF
				$arrPrintProductionData = $arrOrderProcessData;
			// EOF

			$arrTemp = array();

			foreach($arrPrintProductionData as $thisPrintProductionDataKey => $thisPrintProductionDataValue){
				if(!empty($thisPrintProductionDataValue) && !empty($arrLoadPrintProductionData[$thisPrintProductionDataKey])){
					$arrTemp[1][$thisPrintProductionDataKey] = array_merge($thisPrintProductionDataValue, $arrLoadPrintProductionData[$thisPrintProductionDataKey]);
				}
				else {
					if(!empty($thisPrintProductionDataValue)){
						$arrTemp[0][$thisPrintProductionDataKey] = $thisPrintProductionDataValue;
					}
					else if(!empty($arrLoadPrintProductionData[$thisPrintProductionDataKey])){
						#$arrTemp[$thisPrintProductionDataKey] =
					}
				}
			}
			$tempCount = count($arrTemp[1]);
			$arrPrintProductionData = $arrTemp;
		}
	// EOF READ ORDERS PROCESS

?>
<?php

	require_once('inc/headerHTML.inc.php');
	$thisTitle = 'Belichtungs-Auftr&auml;ge generieren';
	$headerHTML = preg_replace("/{###TITLE###}/", strip_tags($thisTitle), $headerHTML);
	echo $headerHTML;

	require_once(FILE_MENUE_TOP);
?>


	<div id="menueSidebarToggleArea">
		<img src="layout/icons/iconToggle.png" class="buttonToggleSidebarMenue" width="10" height="10" alt="" title="Seitennavigation ein-/ausblenden" />
		<div id="menueSidebarToggleContent">
		<?php require_once(FILE_MENUE_SIDEBAR); ?>
		<div class="clear"></div>
		</div>
	</div>
	<div id="contentArea2">
		<div id="contentAreaElements">
			<h1><?php if(SHOW_PAGE_TITLE_ICONS) { echo '<img src="' . PATH_ICONS_MENUE_TITLES . 'datasheet.png" alt="" />'; } ?> <?php echo $thisTitle; ?></h1>

			<?php
				if($_REQUEST["editID"] != ""){
					echo '
						<p>
							<b>Ausgew&auml;hlte Belichtungs-Datei: </b>
							<span class="headerSelectedEntry">
								<a href="' . $_SERVER["PHP_SELF"] . '?editID=' . $_REQUEST["editID"] . '&documentType=BB&downloadFile=' . urlencode(basename($arrPrintProductionFiles[$_REQUEST["editID"]]["printProductionFilesFileName"])) . '">
									<img src="layout/icons/iconPDF.gif" width="16" height="16" title="Dokument herunterladen" alt="Download" />
										&quot;' . $arrPrintProductionFiles[$_REQUEST["editID"]]["printProductionFilesFileName"] . '&quot; vom ' . formatDate($arrPrintProductionFiles[$_REQUEST["editID"]]["printProductionFilesFileDate"], "display") . ' &bull; Bogen ' . getProductionFilesFileSheetNumber($arrPrintProductionFiles[$_REQUEST["editID"]]["printProductionFilesFileName"]) . '
								</a>
							</span>
						</p>
					';
				}
			?>

			<div class="contentDisplay">
				<?php displayMessages(); ?>

				<?php
					if(!empty($arrPrintProductionFiles) && $arrPrintProductionFiles[$_POST["searchPrintProductionFileID"]]["printProductionFilesDataFilePath"] != ""){
						$zipFileName = $arrPrintProductionFiles[$_POST["searchPrintProductionFileID"]]["printProductionFilesDataFilePath"];
					}
					if(file_exists($zipFileName)){
						$arrPathInfo = pathinfo($zipFileName);
						echo '<p>
							<b>DOWNLOAD-LINK:</b> ' . '<img src="layout/icons/icon' . getFileType(basename($zipFileName)) . '.gif" width="16" height="16" alt="" title="Datei herunterladen"/> ' . '<a href="' . $zipFileName . '">' . basename($zipFileName) . '</a>
							</p>
							<p class="dataTableContent">
							<b>Datei versenden: </b><img src="layout/icons/mailAttachement.png" class="buttonSendDocument" rel="' . $arrPathInfo["dirname"] . '/' . urlencode($arrPathInfo["basename"]) . '#BB" width="16" height="16" title="Datei direkt per Mail versenden" alt="Dokument versenden" />
							</p>
						';

					}
				?>

				<?php
					if($_REQUEST["searchPrintProductionFileID"] == ""){
				?>
				<div id="searchFilterArea">
					<form name="formFilterSearch" method="post" action="<?php echo $_SERVER["PHP_SELF"]; ?>">
						<table border="0" cellpadding="0" cellspacing="0" class="searchFilterContent">
							<tr>
								<td style="width:240px;">
									<label for="searchPrintProductionFileID">PDF-Belichtungsdatei w&auml;hlen:</label>
								</td>
								<td>
									<select name="searchPrintProductionFileID" id="searchPrintProductionFileID" class="inputField_300">
										<option value=""></option>
										<?php
											if(!empty($arrPrintProductionFiles)){
												$thisMarker = '';
												foreach($arrPrintProductionFiles as $thisFileKey => $thisFileValue){
													if($thisFileValue["printProductionFilesFileDate"] != $thisMarker){
														$thisMarker = $thisFileValue["printProductionFilesFileDate"];
														echo '<option value="" class="tableRowTitle1">Belichtung vom ' . formatDate($thisFileValue["printProductionFilesFileDate"], "display") . '</option>';
													}
													$selected = '';
													if($_REQUEST["searchPrintProductionFileID"] == $thisFileKey){
														$selected = ' selected="selected" ';
													}
													$thisStyle = 'color:#990000;font-style:italic;';
													if($thisFileValue["countSelectedOrders"] > 0){
														if($thisFileValue["printProductionFilesLanguage"] == 'TR'){
															$thisStyle = 'background-color:#F6FC88;font-weight:normal;';
														}
														else {
															$thisStyle = 'background-color:#EEEEEE;font-weight:normal;';
														}
													}
													$thisStyle .= 'font-size:11px;';
													$thisDisplayFileName = basename($thisFileValue["printProductionFilesFileName"]);
													$thisDisplayFileName = preg_replace("/belichtung_/", "", $thisDisplayFileName);
													$thisDisplayFileName = preg_replace("/\.pdf/", "", $thisDisplayFileName);
													$arrTemp = explode("_", $thisDisplayFileName);
													$thisDate = $arrTemp[0];
													$thisSheetNumber = $arrTemp[1];
													$thisDisplayFileName = formatDate($thisDate, "display") . ' ' . 'Bogen-Nr. ' . $thisSheetNumber;
													echo '<option class="option_' . $thisFileValue["printProductionFilesLanguage"] . '" value="' . $thisFileKey . '" ' . $selected . ' style="' . $thisStyle . '" >' . $thisDisplayFileName . ' - ' . $thisFileValue["countSelectedOrders"] . ' Produktionen zugeordnet [' . $thisFileValue["printProductionFilesLanguage"] . ']</option>';
												}
											}
										?>
									</select>
								</td>
								<td>
									<input type="submit" name="submitSearchProductionFile" class="inputButton0" value="Suchen" />
								</td>
							</tr>
						</table>
					</form>
				</div>
				<?php
					if($arrGetUserRights["editPrintProductionFiles"]) {
				?>
				<hr />
				<div class="adminEditArea">
					<form name="formUploadPrintProductionFile" method="post" action="<?php echo $_SERVER["PHP_SELF"]; ?>" enctype="multipart/form-data">
						<table border="0" cellpadding="0" cellspacing="0" class="searchFilterContent">
						<?php
							for($i = 0; $i < (MAX_FILE_UPLOADS + 40); $i++){
						?>
							<tr>
								<td style="width:240px;">
									<label for="uploadPrintProductionFile_<?php echo $i; ?>"><?php echo ($i + 1); ?>. PDF-Belichtungsdatei hochladen:</label>
								</td>
								<td>
									<input type="file" name="uploadPrintProductionFile[<?php echo $i; ?>]" id="uploadPrintProductionFile_<?php echo $i; ?>" class="inputField_300" />
								</td>
								<td></td>
							</tr>
						<?php
							}
						?>
							<tr>
								<td></td>
								<td></td>
								<td>
									<input type="submit" name="submitUploadProductionFile" class="inputButton0" value="Hochladen" />
								</td>
							</tr>
						</table>
					</form>
				</div>
				<?php
					}
				?>
				<?php
					}
					else {
						// BOF GET ORDERS
							if(!empty($arrPrintProductionData)){
								echo '<form name="formSelectOrders" method="post" action="' . $_SERVER["PHP_SELF"] . '" enctype="multipart/form-data" >';
								// BOF SELECT LANGUAGE
								echo '<div id="searchFilterArea">';
								echo '<table border="0" cellpadding="0" cellspacing="0" class="searchFilterContent">';
								echo '<tr>';
								echo '<td>';
								echo '<label for="selectLanguageID">Sprache w&auml;hlen:</label>';
								echo '</td>';
								echo '<td>';
								echo '<select name="selectLanguageID" class="inputField_300">';
								if(!empty($arrLanguageFileDatas)){
									foreach($arrLanguageFileDatas as $thisLanguageFileKey => $thisLanguageFileData){
										$selected = '';
										if($thisLanguageFileKey == $_REQUEST["selectLanguageID"]){
											$selected = ' selected="selected" ';
										}
										else if($_REQUEST["selectLanguageID"] == "" && $thisLanguageFileKey == "TR"){
											$selected = ' selected="selected" ';
										}
										echo '<option value="' . $thisLanguageFileKey . '"' . $selected . ' >' . $thisLanguageFileData["TEXT"] . '</option>';
									}
								}
								echo '</select>';
								echo '</td>';

								echo '<td> ';
								echo '</td>';

								echo '<td>';
								echo '<a href="' . PAGE_CREATE_PRINT_PRODUCTION_FILES . '" class="linkButton"><span>zur&uuml;ck</span></a>';
								echo '</td>';
								echo '</tr>';
								echo '</table>';
								echo '</div>';
								// EOF SELECT LANGUAGE

								echo '<input type="hidden" name="editID" value="' . $_REQUEST["editID"] . '" />';
								?>
								<div class="colorsLegend">
									<div class="legendItem">
										<b>F&auml;rbung:</b>
									</div>

									<div class="legendItem">
										<b>Zeilen:</b>
									</div>

									<div class="legendItem">
										<span class="legendColorField" id="colorField3"></span>
										<span class="legendDescription">Diesem Bogen zugeordnet</span>
										<div class="clear"></div>
									</div>
									<div class="legendSeperator">&nbsp;</div>

									<!--
									<div class="legendItem">
										<span class="legendColorField" id="colorField7"></span>
										<span class="legendDescription">Freigegeben</span>
										<div class="clear"></div>
									</div>
									-->
									<!--
									<div class="legendItem">
										<span class="legendColorField" id="colorField8"></span>
										<span class="legendDescription">Belichtet</span>
										<div class="clear"></div>
									</div>
									-->

									<div class="legendItem">
										<span class="legendColorField" id="colorField1"></span>
										<span class="legendDescription">Einem anderen Bogen vom <?php echo formatDate($arrPrintProductionFiles[$_REQUEST["editID"]]["printProductionFilesFileDate"], 'display'); ?> zugeordnet</span>
										<div class="clear"></div>
									</div>

									<div class="legendItem">
										<span class="legendColorField" id="colorField8"></span>
										<span class="legendDescription">Keinem Bogen vom <?php echo formatDate($arrPrintProductionFiles[$_REQUEST["editID"]]["printProductionFilesFileDate"], 'display'); ?> zugeordnet</span>
										<div class="clear"></div>
									</div>


									<div class="legendSeperator">&nbsp;</div>

									<div class="clear"></div>
								</div>
								<?php

								echo '<fieldset>';
								echo '<legend>Dem Belichtungsbogen zugeh&ouml;rige belichtete Produktionen ausw&auml;hlen - <span style="color:#333;">Zugeordnete Produktionen: <span class="displayCountSelectedProductions" style="color:#FF0000;font-size:20px;">0</span></span></legend>';
								echo '<div><b>Zugeordnete Produktionen: <span class="displayCountSelectedProductions">0</span></b></div>';
								echo '<p class="warningArea">Es ist UNBEDINGT darauf zu achten, dass die hier angezeigten Daten KORREKT sind!!!!</p>';

								$count = 0;

								for($thisSelectedKey = 1 ; $thisSelectedKey > -1 ; $thisSelectedKey--){
									$thisRowStyle = '';
									if($thisSelectedKey == 0){
										$thisRowStyle = ' style="color:#FF0000;" ';
										$thisProductionsHeader = ' Diesem Bogen nicht zugeordnete Produktionen ';
									}
									else if($thisSelectedKey == 1){
										$thisRowStyle = '';
										$thisProductionsHeader = ' Diesem Bogen zugeordnete Produktionen ';
									}

									echo '<tr>';
									echo '<h2>' . $thisProductionsHeader . ':</h2>';

									echo '<table border="0" width="100%" cellspacing="0" cellpadding="0" class="displayOrders">';
									echo '<tr>';
									echo '<th style="width:20px;">#</th>';
									echo '<th style="width:20px;"><img src="layout/icons/iconUse.png" width="16" height="16" alt="" /></th>';
									echo '<th>MDT</th>';
									echo '<th>KNR</th>';
									echo '<th style="width:90px;">Name</th>';
									echo '<th>Lieferadresse</th>';
									echo '<th>Absenderadresse</th>';
									echo '<th>Zahlart</th>';
									echo '<th style="width:70px;">Eingang</th>';
									echo '<th style="width:70px;">Belichtung</th>';
									echo '<th style="width:70px;">Produktion</th>';
									echo '<th>Menge x Artikelbezeichnung</th>';
									echo '<th>Korrektur-Datei (PDF)</th>';
									echo '</tr>';

									foreach($arrPrintProductionData[$thisSelectedKey] as $thisPrintProductionDataKey => $thisPrintProductionDataValue){

										if($count%2 == 0){ $rowClass = 'row0'; }
										else { $rowClass = 'row1'; }

										if(in_array($thisPrintProductionDataKey, array_keys($arrPrintProductionFilesTodaysOrderIDs))){
											$rowClass = 'row2';
										}
										else {
											// $rowClass = 'row9';
										}

										echo '<tr ' . $thisRowStyle . ' class="'.$rowClass.'">';
										echo '<td style="text-align:right;">';
										echo '<b>' . ($count + 1) . '.</b>';
										echo '</td>';

										echo '<td >';
										$checked = '';
										if(!empty($arrLoadPrintProductionData[$thisPrintProductionDataKey])){
											$checked = ' checked="checked" ';
										}
										echo '<input type="checkbox" name="arrSelectOrderID[' . $thisPrintProductionDataKey . ']" id="selectOrderID_' . $thisPrintProductionDataKey . '" class="checkboxSelectProduction" value="1" title="Produktion &uuml;bernehmen" ' . $checked . '/>';

										if($arrGetUserRights["editOrders"]) {
											echo '<span class="toolItem">
												<a href="' . PAGE_EDIT_PROCESS . '?editID='.$thisPrintProductionDataKey. $jsonReturnParams . '">
													<img src="layout/icons/iconEdit.gif" width="16" height="16" title="Produktion bearbeiten (Datensatz '.$thisPrintProductionDataKey.')" alt="Bearbeiten" />
												</a></span>
											';
										}

										echo '</td>';

										$thisCellStyle = 'color:#000099;';
										if($thisPrintProductionDataValue["ordersMandant"] == 'BCTR') {
											$thisCellStyle = 'color:#990000;';
										}

										echo '<td style="' . $thisCellStyle . ';font-size:10px;">';
										echo '<b>' . $thisPrintProductionDataValue["ordersMandant"] . '</b>';
										echo '</td>';

										echo '<td><b>';
										echo '<a href="' . PAGE_EDIT_CUSTOMER . '?searchCustomerNumber=' . $thisPrintProductionDataValue["ordersKundennummer"] . '">';
										echo $thisPrintProductionDataValue["ordersKundennummer"];
										echo '</a>';
										echo '</b></td>';

										echo '<td style="font-size:10px;"><b>';
										echo $thisPrintProductionDataValue["ordersKundenName"];
										echo '</b></td>';

										// BOF GET ADRESSES
										getSenderAndRecipientAdress();
										// BOF GET ADRESSES

										// BOF RECIPIENT ADRESS
										echo '<td style="white-space:nowrap;font-size:10px;">';
										echo formatAdressData($arrThisRecipientAdress["USE"]);
										echo '</td>';
										// EOF RECIPIENT ADRESS

										// BOF SENDER ADRESS
										echo '<td style="white-space:nowrap;font-size:10px;">';
										echo formatAdressData($arrThisSenderAdress["USE"]);
										echo '</td>';
										// EOF SENDER ADRESS

										// BOF PAYMENT TYPE
										$checked = '';
										$thisPaymentStyle = '';

										if($arrLoadPrintProductionData[$thisPrintProductionDataKey]["printProductionOrdersPaymentTypeCashOnDelivery"] == "1"){
											$checked = ' checked="checked" ';
											$thisPaymentStyle = ' style="background-color:#97EDFC;" ';
										}
										echo '<td ' . $thisPaymentStyle . '>';
										echo '<input type="checkbox" name="arrSelectOrderPaymentTypeCashOnDelivery[' . $thisPrintProductionDataKey . ']" id="selectOrderID_' . $thisPrintProductionDataKey . '" class="checkboxThisCashOnDelivery" value="1" title="Versand per Nachnahme" ' . $checked . '/> NN?';
										echo '</td>';
										// EOF PAYMENT TYPE

										echo '<td style="font-size:10px;">';
										echo formatDate($thisPrintProductionDataValue["ordersBestellDatum"], "display");
										echo '</td>';

										echo '<td style="font-size:10px;">';
										echo formatDate($thisPrintProductionDataValue["ordersBelichtungsDatum"], "display");
										echo '</td>';

										echo '<td style="font-size:10px;">';
										echo formatDate($thisPrintProductionDataValue["ordersProduktionsDatum"], "display");
										echo '</td>';

										echo '<td style="font-size:10px;">';

										echo '';
										echo $thisPrintProductionDataValue["ordersArtikelMenge"] ;
										echo ' x ';
										echo $thisPrintProductionDataValue["ordersArtikelBezeichnung"];
										echo ' [';
										echo $thisPrintProductionDataValue["ordersArtikelNummer"];
										echo ']';
										if($thisPrintProductionDataValue["ordersAufdruck"] != ""){
											echo '<div class="remarksArea"><b>Aufdruck</b>: ' . $thisPrintProductionDataValue["ordersAufdruck"] . '</div>';
										}
										if($thisPrintProductionDataValue["ordersKommission"] != ""){
											echo '<div class="remarksArea"><b>Kommission</b>: ' . $thisPrintProductionDataValue["ordersKommission"] . '</div>';
										}
										if($thisPrintProductionDataValue["ordersAdditionalArtikelMenge"] > 0){
											echo '<hr />';
											echo $arrOrderCategoriesTypeDatas[$thisPrintProductionDataValue["ordersAdditionalArtikelKategorieID"]]["orderCategoriesPartNumber"];
											#echo $thisPrintProductionDataValue["ordersAdditionalArtikelNummer"] ;
											echo ' ';
											echo $thisPrintProductionDataValue["ordersAdditionalArtikelMenge"];
											echo ' x ';
											echo $arrOrderCategoriesTypeDatas[$thisPrintProductionDataValue["ordersAdditionalArtikelKategorieID"]]["orderCategoriesName"];
											#echo $thisPrintProductionDataValue["ordersAdditionalArtikelBezeichnung"];
										}
										echo '</td>';

										echo '<td>';
										echo '<table border="0" width="100%" cellspacing="0" cellpadding="0">';
										#if($arrGetUserRights["editPrintProductionFiles"]) {
										if(1) {
											$arrThisLayoutFiles = getLayoutFiles(array($thisPrintProductionDataValue["ordersKundennummer"], $arrThisRecipientAdress["USE"]["knr"]));
											echo '<tr>';
												echo '<td style="white-space:nowrap; width:82px;font-size:10px;"><b>Kunden-Dateien:</b></td>';
												echo '<td style="white-space:nowrap;font-size:10px;" class="selectLayoutFileArea">';
												if(!empty($arrThisLayoutFiles)){
													echo '<div style="border: 1px solid #FF0000;padding:2px;">';
													foreach($arrThisLayoutFiles as $thisLayoutFileKey => $thisLayoutFileValue){
														echo '<div style="border-bottom:1px dotted #333;padding-left:4px;cursor:pointer;" title="' .basename($thisLayoutFileValue["createdDocumentsFilename"]) . '">';
														$thisChecked = '';
														if(basename($arrLoadPrintProductionData[$thisPrintProductionDataKey]["printProductionOrdersLayoutFilePath"]) == basename($thisLayoutFileValue["createdDocumentsFilename"])){
															$thisChecked = ' checked = "checked" ';
														}
														echo '<input type="radio" name="arrSelectOrderExistingLayoutFile['.$thisPrintProductionDataKey.']" value="' . basename($thisLayoutFileValue["createdDocumentsFilename"]) . '" ' . $thisChecked . '/> ';
														if(file_exists(DIRECTORY_UPLOAD_FILES . basename($thisLayoutFileValue["createdDocumentsFilename"]))){
															echo ' <a href="' . DIRECTORY_UPLOAD_FILES . basename($thisLayoutFileValue["createdDocumentsFilename"]) . '" target="_blank">';
														}
														echo ' <img src="layout/icons/iconPDF.gif" width="14" height="14" title="Datei ansehen" alt="" /> ';
														if(file_exists(DIRECTORY_UPLOAD_FILES . basename($thisLayoutFileValue["createdDocumentsFilename"]))){
															echo '</a>';
														}
														echo substr(basename($thisLayoutFileValue["createdDocumentsFilename"]), 0, 80);
														echo '</div>';
													}
													echo '<div style="padding-left:4px;cursor:pointer;" title="' .basename($thisLayoutFileValue["createdDocumentsFilename"]) . '">';
													$thisChecked = '';
													if($arrLoadPrintProductionData[$thisPrintProductionDataKey]["printProductionOrdersLayoutFilePath"] == ""){
														$thisChecked = ' checked = "checked" ';
													}
													echo '<span class="unselectLayoutFileAreaWrapper">';
													echo '<input type="radio" class="unselectLayoutFileArea" name="arrSelectOrderExistingLayoutFile['.$thisPrintProductionDataKey.']" value="" ' . $thisChecked . '/> <b>keine der bereits hochgeladenen Korrekturabz&uuml;ge</b>';
													echo '</span>';
													echo '</div>';
													echo '</div>';
												}
												else {
													echo '<p class="blinkArea">Bitte eine PDF-Datei bei diesem Kunden hochladen.</p>';
												}
												echo'</td>';
												echo '</tr>';
												if($arrGetUserRights["editPrintProductionFiles"]) {
													echo '<tr>';
													echo '<td style="white-space:nowrap;font-size:10px;"><b>Datei hochladen:</b> </td>';
													echo '<td style="white-space:nowrap;" class="uploadLayoutFileArea">' . '<input type="file" name="arrSelectOrderID['.$thisPrintProductionDataKey.']" id="selectOrderID_'.$thisPrintProductionDataKey.'" class="inputField_260" />' . '</td>';
													echo '</tr>';
												}
										}
										echo '</table>';
										echo '</td>';
										echo '</tr>';

										$count++;
									}
									echo '<tr>';
									echo '<td colspan="13"><hr /></td>';
									echo '</tr>';
									echo '</table><br /><br />';
								}

								echo '</fieldset>';
								?>
								<div class="actionButtonsArea">
									<?php if($arrGetUserRights["editPrintProductionFiles"]) { ?>
									<input type="hidden" name="searchPrintProductionFileID" value="<?php echo $_REQUEST["searchPrintProductionFileID"]; ?>" />
									<input type="submit" class="inputButton1 inputButtonGreen" name="storeDatas" value="Speichern" onclick="return showWarning('Wollen Sie diesen Vorgang wirklich speichern??? ');" />
									<input type="submit" class="inputButton1 inputButtonOrange" name="cancelDatas" value="Abbrechen" onclick="return showWarning('Wollen Sie diesen Vorgang wirklich abbrechen??? ');" />
									<?php } ?>
								</div>
								<?php
								echo '</form>';
							}
						// EOF GET ORDERS
					}
				?>

				<?php displayMessages(); ?>
				<?php
					if(!empty($arrExecResults)){
						dd('arrExecResults');
					}
				?>
			</div>
		</div>
	</div>
	<div class="clear"></div>



<script language="javascript" type="text/javascript">
	$(document).ready(function() {

		// BOF BLINKING SCRIPT
		function blink(element, times, speed) {
			if (times > 0 || times < 0) {
				if ($(element).hasClass("blink"))
					$(element).removeClass("blink");
				else
					$(element).addClass("blink");
			}

			clearTimeout(function () {
				blink(element, times, speed);
			});

			if (times > 0 || times < 0) {
				setTimeout(function () {
					blink(element, times, speed);
				}, speed);
				times -= .5;
			}
		}
		blink('.blinkArea', 1000, 1000);
		blink($('.unselectLayoutFileArea:checked').parent(), 1000, 1000);
		$("input[name*='arrSelectOrderExistingLayoutFile']").parent().css('color', '#0000FF !important');
		$("input[name*='arrSelectOrderExistingLayoutFile']").live('click', function(){
			var thisSelectOrderExistingLayoutFileValue = $(this).val();
			if(thisSelectOrderExistingLayoutFileValue != ''){
				// alert($(this).val());
				$(this).parent().parent().find('.unselectLayoutFileAreaWrapper').hide();
				// $('.unselectLayoutFileArea:checked').unbind('blink');
			}
			else {
				// blink($('.unselectLayoutFileArea:checked').parent(), 1000, 1000);
				$(this).parent().parent().find('.unselectLayoutFileAreaWrapper').show();
			}
		});
		// EOF BLINKING SCRIPT

		<?php if(!empty($arrPrintProductionData)){ ?>
		alert('Es ist UNBEDINGT darauf zu achten, dass die hier angezeigten Daten KORREKT sind!!!!');
		<?php } ?>
		$(function() {
			$( "#datepicker" ).datepicker();
			$.datepicker.setDefaults($.datepicker.regional[""]);
			$('#editPersonnelVacationsStart').datepicker($.datepicker.regional["de"]);
			$('#editPersonnelVacationsEnd').datepicker($.datepicker.regional["de"]);
			$('#editPersonnelJobStart').datepicker($.datepicker.regional["de"]);
			$('#editPersonnelJobEnd').datepicker($.datepicker.regional["de"]);
			//$('#editPersonnelBirthday').datepicker($.datepicker.regional["de"]);
			$('#editPersonnelDiseaseStart').datepicker($.datepicker.regional["de"]);
			$('#editPersonnelDiseaseEnd').datepicker($.datepicker.regional["de"]);
			$('#editPersonnelOvertimeDate').datepicker($.datepicker.regional["de"]);
		});
		$(function() {
			$('#tabs').tabs();
		});

		function checkSelectedProductions(){
			var thisBgColor = '#D0F9C7';
			countChecked = $('.checkboxSelectProduction:checked').length;
			$('.displayCountSelectedProductions').text(countChecked);
			$('.checkboxSelectProduction:checked').parent().parent().find('td').css('background-color', thisBgColor);
		}

		function checkCashOnDelivery(){
			var thisBgColor = '#97EDFC';
			$('.checkboxThisCashOnDelivery:checked').parent().css('background-color', thisBgColor);
		}
		checkSelectedProductions();
		checkCashOnDelivery();

		$('.checkboxSelectProduction').click(function(){
			var thisChecked = $(this).attr('checked');
			var thisBgColor = '';
			var countChecked = 0;
			if(thisChecked == 'checked'){
				thisBgColor = '#D0F9C7';
			}
			else {
				thisBgColor = '';
			}
			$(this).parent().parent().find('td').css('background-color', thisBgColor);
			countChecked = $('.checkboxSelectProduction:checked').length;
			$('.displayCountSelectedProductions').text(countChecked);
			if(countChecked > <?php echo MAX_PRODUCTIONS_PER_FILM; ?>){
				window.alert('ACHTUNG, Sie haben mehr als die maximale Anzahl Belichtungen (<?php echo MAX_PRODUCTIONS_PER_FILM; ?>) pro Bogen ausgewählt.');
			}
		});

		$('.checkboxThisCashOnDelivery').click(function(){
			var thisChecked = $(this).attr('checked');
			var thisBgColor = '';
			if(thisChecked == 'checked'){
				thisBgColor = '#97EDFC';
			}
			else {
				thisBgColor = '';
			}
			$(this).parent().css('background-color', thisBgColor);
		});


		$('.buttonSendDocument').click(function() {
			var mailDocumentFilename = $(this).attr('rel');
			sendAttachedDocument($(this), mailDocumentFilename, '<?php echo $salesmanKundennummer; ?>', '<?php echo $_REQUEST["editID"]; ?>', 'BB', '<?php echo $_SERVER["REQUEST_URI"]; ?>', 'production@burhan-ctr.de');
		});
		// setFormDutyFields(<?php echo $jsonFormDutyFields; ?>);
		colorRowMouseOver('.displayOrders tbody tr');

		$('.uploadLayoutFileArea input').click(function(){
			$(this).parent().parent().parent().find('.unselectLayoutFileArea').attr('checked', 'checked');
		});
		$('.buttonToggleSidebarMenue').click(function() {
			$('#menueSidebarToggleContent').toggle();
		});
	});
</script>

<?php require_once('inc/footerHTML.inc.php'); ?>