<?php
	require_once('inc/requires.inc.php');

	if(!$arrGetUserRights["editFinancialDatas"]) {
		header('location: ' . PAGE_EXIT_LOCATION);
		exit;
	}

	DEFINE('KREDITOREN_ID_BCTR', '2311'); // BCTR
	DEFINE('KREDITOREN_ID_B3', '2349'); // B3

	DEFINE('KREDITOREN_ID', constant("KREDITOREN_ID_" . strtoupper(MANDATOR))); // B3

	DEFINE('BASEPATH_Auftragslisten', '');
	DEFINE('PATH_EXPORTED_FILES', BASEPATH_Auftragslisten . 'exportedFactoringFiles/');

	require_once(BASEPATH_Auftragslisten . 'config/configParams_' . strtolower(MANDATOR) . '.inc.php');
	require_once(BASEPATH_Auftragslisten . 'config/configBasic.inc.php');
	require_once(BASEPATH_Auftragslisten . 'config/configTables.inc.php');
	require_once(BASEPATH_Auftragslisten . 'inc/functions.inc.php');
	require_once(BASEPATH_Auftragslisten . 'classes/DB_Connection.class.php');

	function convertExportString($string){
		$string = utf8_decode($string);
		return $string;
	}

	$jswindowMessage = '';
	$warningMessage = '';
	$errorMessage = '';
	$successMessage = '';
	$infoMessage = '';

	$dbConnection = new DB_Connection();
	$db_open = $dbConnection->db_connect();

	if($_GET["downloadFile"] != "") {
		require_once('classes/downloadFile.class.php');
		$downloadPath = '';
		if($_GET["downloadType"] == "EXPORT"){
			$downloadPath = PATH_EXPORTED_FILES;
			$thisDownloadFile = $_GET["downloadFile"];
		}
		else if(in_array($_GET["downloadType"], array("GU", "RE"))){
			$downloadPath = DIRECTORY_CREATED_DOCUMENTS_CUSTOMERS;
			$thisDownloadFile = searchDownloadFile($downloadPath . $_GET["downloadFile"], $_GET["downloadType"]);
		}
		$thisDownload = new downloadFile($downloadPath, $thisDownloadFile);
		$errorMessage .= $thisDownload->startDownload();
	}

	// BOF GET CUSTOMER DATA
		if($_POST["submitExportForm"] == 1) {
			if(!empty($_POST["exportCustomerNumbers"])) {
				$arrExportCustomerNumbers = array();
				$strExportCustomerNumbers = $_POST["exportCustomerNumbers"];
				$strExportCustomerNumbers = preg_replace("/,/", "\n", $strExportCustomerNumbers);
				$strExportCustomerNumbers = preg_replace("/;/", "\n", $strExportCustomerNumbers);
				$strExportCustomerNumbers = preg_replace("/ /", "\n", $strExportCustomerNumbers);

				$arrTempExportCustomerNumbers = explode("\n", $strExportCustomerNumbers);
				if(!empty($arrTempExportCustomerNumbers)){
					foreach($arrTempExportCustomerNumbers as $thisTempExportCustomerNumber){
						$thisTempExportCustomerNumber = trim($thisTempExportCustomerNumber);
						if($thisTempExportCustomerNumber != ""){
							$arrExportCustomerNumbers[] = $thisTempExportCustomerNumber;
						}
					}
				}

				if(!empty($arrExportCustomerNumbers)){
					// BOF EXPORT MISSING CUSTOMERS
						$where = " AND (`customersKundennummer` = '" . implode(" ' OR `customersKundennummer` = '", $arrExportCustomerNumbers) . "') ";
						$sql_exportMissingCustomers = "
								SELECT
								
									`customersKundennummer` AS `DEBITORENNUMMER`,
									`customersFirmenname` AS `NAME`,
									`customersFirmennameZusatz` AS `NAME_ZUSATZ`,
									CONCAT(`customersFirmenInhaberVorname`, ' ', `customersFirmenInhaberNachname`) AS `INHABER_1`,
									CONCAT(`customersFirmenInhaber2Vorname`, ' ', `customersFirmenInhaber2Nachname`) AS `INHABER_2`,
									CONCAT(`customersCompanyStrasse`, ' ', `customersCompanyHausnummer`) AS `ANSCHRIFT`,
									`customersCompanyCountry` AS `LAND`,
									`customersCompanyPLZ` AS `PLZ`,
									`customersCompanyOrt` AS `ORT`,
									`customersTelefon1` AS `TELEFON_1`,
									`customersTelefon2` AS `TELEFON_2`,
									`customersMobil1` AS `MOBIL_1`,
									`customersMobil2` AS `MOBIL_2`,
									`customersFax1` AS `FAX_1`,
									`customersFax2` AS `FAX_2`,
									`customersMail1` AS `MAIL_1`,
									`customersMail2` AS `MAIL_2`,
									`customersHomepage` AS `HOMEPAGE`

								FROM `" . TABLE_CUSTOMERS . "`
								WHERE 1
									" . $where . "
									ORDER BY `DEBITORENNUMMER` ASC
							";


						$rs_exportMissingCustomers = $dbConnection->db_query($sql_exportMissingCustomers);

						$countTotal = $dbConnection->db_getMysqlNumRows($rs_exportMissingCustomers);

						$exportCSV_content = '';

						$csvFieldSeparator = ";";
						$csvLineSeparator = "\r\n";

						if($countTotal > 0){

							$exportStoreFilename = "fehlendeDebitoren_" . date("Y-m-d") . ".csv";
							$exportDownloadFilePath = tempnam("/tmp", "exportMissingCustomers_");

							$fp_export = fopen($exportDownloadFilePath, "a+");
							$countRow = 0;
							while($ds_exportMissingCustomers = mysqli_fetch_assoc($rs_exportMissingCustomers)){
								if($countRow == 0){
									$exportCSV_content = '"' . implode('"' . $csvFieldSeparator . '"' , array_keys($ds_exportMissingCustomers)) . '"' . $csvLineSeparator;
									$exportCSV_content = convertExportString($exportCSV_content);
									fwrite($fp_export, $exportCSV_content);
								}
								$exportCSV_content = '"' . implode('"' . $csvFieldSeparator . '"' , array_values($ds_exportMissingCustomers)) . '"' . $csvLineSeparator;
								$exportCSV_content = convertExportString($exportCSV_content);
								fwrite($fp_export, $exportCSV_content);
								$countRow++;
							}
							if($fp_export){
								fclose($fp_export);
							}

							// BOF DOWNLOAD EXPORT FILE	AND DELETE TMP FILE
								if(file_exists($exportDownloadFilePath)){
									header('Content-Description: File Transfer');
									header('Content-Type: application/octet-stream');
									header('Content-Disposition: attachment; filename=' . basename($exportStoreFilename));
									header('Content-Transfer-Encoding: binary');
									header('Expires: 0');
									header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
									header('Pragma: public');
									header('Content-Length: ' . filesize($exportDownloadFilePath));
									header('Connection: Close');
									ob_clean();
									flush();
									readfile($exportDownloadFilePath);

									if(file_exists($exportDownloadFilePath)){
										unlink($exportDownloadFilePath);
									}
									exit;
								}
							// EOF DOWNLOAD EXPORT FILE	AND DELETE TMP FILE

						}
						else {
							$warningMessage .= 'Es wurden keine Daten gefunden.' . '<br />';
						}
					// EOF EXPORT MISSING CUSTOMERS
				}
				else {
					$warningMessage .= 'Es wurden keine Kundennummern eingegeben.' . '<br />';
				}
			}
			else {
				$warningMessage .= 'Es wurden keine Daten eingegeben.' . '<br />';
			}
		}
	// EOF GET CUSTOMER DATA
?>
<?php
	require_once('inc/headerHTML.inc.php');
	$thisTitle = "Faktoring: fehlende Kunden exportieren";

	$headerHTML = preg_replace("/{###TITLE###}/", strip_tags($thisTitle), $headerHTML);
	echo $headerHTML;

	require_once(FILE_MENUE_TOP);
?>

<div id="mainArea">
	<div id="mainContent">
		<?php require_once(FILE_MENUE_SIDEBAR); ?>
		<div id="contentArea">
			<a name="top"></a>
			<div id="contentAreaElements">
				<h1><?php if(SHOW_PAGE_TITLE_ICONS) { echo '<img src="' . PATH_ICONS_MENUE_TITLES . 'export.png" alt="" />'; } ?> <?php echo $thisTitle; ?></h1>
				<p><b>Kreditoren-ID: <?php echo KREDITOREN_ID; ?></b></p>
				<div style="text-align:right"><a href="<?php echo URL_FACTORING_PORTAL; ?>" target="_blank">Zum Factoring Portal</a></div>
				<div id="searchFilterArea" style="background-color:#EEE;background-image:none;">
					<form name="formExportDatas" method="post" action="<?php echo $_SERVER["PHP_SELF"]; ?>">
					<table border="0" cellpadding="0" cellspacing="0" class="searchFilterContent">
						<tr>
							<td>
								<label for="exportCustomerNumbers">Kunden-Nummern:</label>
								<p class="infoArea">Pro Zeile eine Kundennummer eingeben!!</p>
								<textarea name="exportCustomerNumbers" id="exportCustomerNumbers" class="inputTextarea_200x400"></textarea>
							</td>
							<td>
								<input type="hidden" name="editID" id="editID" value="" />
								<input type="submit" name="submitExport" class="inputButton0" value="Kunden-Daten exportieren" />
							</td>
						</tr>
					</table>
					<input type="hidden" name="submitExportForm" id="submitExportForm" value="1" />
					</form>
				</div>

				<div class="contentDisplay">
					<?php displayMessages(); ?>
					<?php
						if($_POST["submitExportForm"] == 1) {

						}
					?>
				</div>
			</div>
		</div>
		<div class="clear"></div>
	</div>
</div>
<script language="javascript" type="text/javascript">
	$(document).ready(function() {
		$(function() {

		});

		$('.buttonNotice').click(function () {
			loadNotice($(this));
		});
	});
</script>
<?php require_once('inc/footerHTML.inc.php'); ?>