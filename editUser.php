<?php
	require_once('inc/requires.inc.php');

	if(!$arrGetUserRights["displayUser"] && !$arrGetUserRights["editUser"]) {
		header('location: ' . PAGE_EXIT_LOCATION);
		exit;
	}

	$jswindowMessage = "";
	$warningMessage = '';
	$errorMessage = '';
	$successMessage = '';
	$infoMessage = '';

	$dbConnection = new DB_Connection();
	$db_open = $dbConnection->db_connect();

	// BOF READ COUNTRIES
		$arrPersonnelTypeDatas = getPersonnelTypes();
	// EOF READ COUNTRIES

	// BOF DEFINE RIGHTS
	$arrSetUserRightsHeader = array();
	$arrSetUserRights = array ();
	$i = -1;

	$i++;
	$arrSetUserRightsHeader[$i]	= 'Stammdaten';
	$arrSetUserRights[$i][] = array('NAME' => 'displayProducts', 'TITLE' => 'Produkte anzeigen');
	$arrSetUserRights[$i][] = array('NAME' => 'editProducts', 'TITLE' => 'Produkte anlegen und bearbeiten');
	$arrSetUserRights[$i][] = array('NAME' => 'displayCustomers', 'TITLE' => 'Kunden anzeigen');
	$arrSetUserRights[$i][] = array('NAME' => 'editCustomers', 'TITLE' => 'Kunden anlegen und bearbeiten');
	$arrSetUserRights[$i][] = array('NAME' => 'updateOnlineCustomers', 'TITLE' => 'Kundennummern der Shop-Kunden abgleichen');
	$arrSetUserRights[$i][] = array('NAME' => 'updateAcquisitionCustomers', 'TITLE' => 'Kundennummern der Vertreter-Kundenerfassung abgleichen');
	$arrSetUserRights[$i][] = array('NAME' => 'displaySalesmen', 'TITLE' => 'Vertreter anzeigen');
	$arrSetUserRights[$i][] = array('NAME' => 'editSalesmen', 'TITLE' => 'Vertreter anlegen und bearbeiten');
	$arrSetUserRights[$i][] = array('NAME' => 'displayPersonnel', 'TITLE' => 'Mitarbeiter anzeigen');
	$arrSetUserRights[$i][] = array('NAME' => 'editPersonnel', 'TITLE' => 'Mitarbeiter anlegen und bearbeiten');
	$arrSetUserRights[$i][] = array('NAME' => 'displayCompanyDatas', 'TITLE' => 'Firmendaten anzeigen');
	$arrSetUserRights[$i][] = array('NAME' => 'editCompanyDatas', 'TITLE' => 'Firmendaten bearbeiten');
	$arrSetUserRights[$i][] = array('NAME' => 'displayCalendar', 'TITLE' => 'Kalender anzeigen');
	$arrSetUserRights[$i][] = array('NAME' => 'editCalendar', 'TITLE' => 'Kalender bearbeiten');
	$arrSetUserRights[$i][] = array('NAME' => 'displayDocuments', 'TITLE' => 'Dokumente ansehen');
	$arrSetUserRights[$i][] = array('NAME' => 'displayShippingDatas', 'TITLE' => 'Versanddaten anzeigen');
	$arrSetUserRights[$i][] = array('NAME' => 'editShippingDatas', 'TITLE' => 'Versanddaten bearbeiten');
	$arrSetUserRights[$i][] = array('NAME' => 'displaySuppliers', 'TITLE' => 'Lieferanten anzeigen');
	$arrSetUserRights[$i][] = array('NAME' => 'editSuppliers', 'TITLE' => 'Lieferanten bearbeiten');
	$arrSetUserRights[$i][] = array('NAME' => 'displayAddresses', 'TITLE' => 'Adressen bearbeiten');
	$arrSetUserRights[$i][] = array('NAME' => 'editAddresses', 'TITLE' => 'Adressen anzeigen');
	$arrSetUserRights[$i][] = array('NAME' => 'displayCustomerCalendarDates', 'TITLE' => 'Kunden-Termine anzeigen');

	$i++;
	$arrSetUserRightsHeader[$i]	= 'E-Mails';
	$arrSetUserRights[$i][] = array('NAME' => 'sendMails', 'TITLE' => 'Mails versenden');
	$arrSetUserRights[$i][] = array('NAME' => 'displaySendedMails', 'TITLE' => 'Auftragslisten-Mailversand ansehen');
	$arrSetUserRights[$i][] = array('NAME' => 'displayWebmail', 'TITLE' => 'Webmail ansehen');

	$i++;
	$arrSetUserRightsHeader[$i]	= 'Vorgänge / Produktionen';
	$arrSetUserRights[$i][] = array('NAME' => 'displayOrdersOverview', 'TITLE' => 'Bestellungs-Übersicht');
	$arrSetUserRights[$i][] = array('NAME' => 'displayOnlineOrders', 'TITLE' => 'Online-Bestellungen ansehen');
	$arrSetUserRights[$i][] = array('NAME' => 'importOnlineOrders', 'TITLE' => 'Online-Bestellungen importieren');
	$arrSetUserRights[$i][] = array('NAME' => 'displayOnlineAcquisition', 'TITLE' => 'Vertreter-Kundenerfassung ansehen');
	$arrSetUserRights[$i][] = array('NAME' => 'importOnlineAcquisition', 'TITLE' => 'Vertreter-Kundenerfassung importieren');
	$arrSetUserRights[$i][] = array('NAME' => 'displayOrders', 'TITLE' => 'Bestellungen anzeigen');
	$arrSetUserRights[$i][] = array('NAME' => 'editOrders', 'TITLE' => 'Bestellungen anlegen und bearbeiten');

	$i++;
	$arrSetUserRightsHeader[$i]	= 'Buchhaltung';
	$arrSetUserRights[$i][] = array('NAME' => 'createDocuments', 'TITLE' => 'Dokumente erstellen');
	$arrSetUserRights[$i][] = array('NAME' => 'displayOffers', 'TITLE' => 'Angebote anzeigen');
	$arrSetUserRights[$i][] = array('NAME' => 'editOffers', 'TITLE' => 'Angebote anlegen und bearbeiten');
	$arrSetUserRights[$i][] = array('NAME' => 'displayConfirmations', 'TITLE' => 'Auftragsbestätigungen anzeigen');
	$arrSetUserRights[$i][] = array('NAME' => 'editConfirmations', 'TITLE' => 'Auftragsbestätigungen anlegen und bearbeiten');
	$arrSetUserRights[$i][] = array('NAME' => 'displayInvoices', 'TITLE' => 'Rechnungen anzeigen');
	$arrSetUserRights[$i][] = array('NAME' => 'editInvoices', 'TITLE' => 'Rechnungen anlegen und bearbeiten');
	$arrSetUserRights[$i][] = array('NAME' => 'displayDeliveries', 'TITLE' => 'Lieferscheine anzeigen');
	$arrSetUserRights[$i][] = array('NAME' => 'editDeliveries', 'TITLE' => 'Lieferscheine anlegen und bearbeiten');
	$arrSetUserRights[$i][] = array('NAME' => 'displayReminders', 'TITLE' => 'Mahnungen anzeigen');
	$arrSetUserRights[$i][] = array('NAME' => 'editReminders', 'TITLE' => 'Mahnungen anlegen und bearbeiten');
	$arrSetUserRights[$i][] = array('NAME' => 'displayCredits', 'TITLE' => 'Gutschriften anzeigen');
	$arrSetUserRights[$i][] = array('NAME' => 'editCredits', 'TITLE' => 'Gutschriften anlegen und bearbeiten');
	$arrSetUserRights[$i][] = array('NAME' => 'displayLetters', 'TITLE' => 'Briefe anzeigen');
	$arrSetUserRights[$i][] = array('NAME' => 'editLetters', 'TITLE' => 'Briefe anlegen und bearbeiten');
	$arrSetUserRights[$i][] = array('NAME' => 'copyInvoices', 'TITLE' => 'Rechnungen kopieren');

	$i++;
	$arrSetUserRightsHeader[$i]	= 'Finanzen';
	$arrSetUserRights[$i][] = array('NAME' => 'displayBankDatas', 'TITLE' => 'Bankdaten anzeigen');
	$arrSetUserRights[$i][] = array('NAME' => 'editBankDatas', 'TITLE' => 'Bankdaten bearbeiten');
	$arrSetUserRights[$i][] = array('NAME' => 'displayFinancialDatas', 'TITLE' => 'Finanzen anzeigen');
	$arrSetUserRights[$i][] = array('NAME' => 'editFinancialDatas', 'TITLE' => 'Finanzen bearbeiten');
	$arrSetUserRights[$i][] = array('NAME' => 'setInvoicePaymentStatus', 'TITLE' => 'Zahlungsstatus der Rechnungen setzen');
	$arrSetUserRights[$i][] = array('NAME' => 'diplayAllResultsOnOnePage', 'TITLE' => 'Alle Resultate auf einer Seite anzeigen');
	$arrSetUserRights[$i][] = array('NAME' => 'displayRemindersCosts', 'TITLE' => 'Mahnkosten anzeigen');
	$arrSetUserRights[$i][] = array('NAME' => 'displayFactoringData', 'TITLE' => 'Faktoring-Daten anzeigen');
	$arrSetUserRights[$i][] = array('NAME' => 'displayDatevData', 'TITLE' => 'DATEV-Daten anzeigen');

	$i++;
	$arrSetUserRightsHeader[$i]	= 'Provisionen / Vertrieb';
	$arrSetUserRights[$i][] = array('NAME' => 'displayProvisionDatas', 'TITLE' => 'Provisionen anzeigen');
	$arrSetUserRights[$i][] = array('NAME' => 'editProvisionDatas', 'TITLE' => 'Provisionen bearbeiten');
	$arrSetUserRights[$i][] = array('NAME' => 'displayDistribution', 'TITLE' => 'Vertriebsdaten ansehen');

	$i++;
	$arrSetUserRightsHeader[$i]	= 'Dokumente / Dateien';
	$arrSetUserRights[$i][] = array('NAME' => 'uploadDocuments', 'TITLE' => 'Korrekturabzüge hochladen');
	$arrSetUserRights[$i][] = array('NAME' => 'uploadFiles', 'TITLE' => 'Handbücher und Formulare hochladen');

	$i++;
	$arrSetUserRightsHeader[$i]	= 'Statistiken';
	$arrSetUserRights[$i][] = array('NAME' => 'displayOrderReceipts', 'TITLE' => 'Bestell-Eing&auml;nge anzeigen');
	$arrSetUserRights[$i][] = array('NAME' => 'displaySales', 'TITLE' => 'Umsatz anzeigen');
	$arrSetUserRights[$i][] = array('NAME' => 'displayStocks', 'TITLE' => 'Bestände anzeigen');
	$arrSetUserRights[$i][] = array('NAME' => 'editStocks', 'TITLE' => 'Bestände anlegen und bearbeiten');
	$arrSetUserRights[$i][] = array('NAME' => 'displayOnlineOrdersStatistics', 'TITLE' => 'Statistik Online-Bestellungen');


	$i++;
	$arrSetUserRightsHeader[$i]	= 'Werkzeuge';
	$arrSetUserRights[$i][] = array('NAME' => 'externalLinks', 'TITLE' => 'Externe Links anzeigen');
	$arrSetUserRights[$i][] = array('NAME' => 'exportOrders', 'TITLE' => 'Bestellungen für Vertreter exportieren');
	$arrSetUserRights[$i][] = array('NAME' => 'createEAN', 'TITLE' => 'QR-/GLN Codes generieren');
	$arrSetUserRights[$i][] = array('NAME' => 'displayOnlineUsers', 'TITLE' => 'Online-User anzeigen');
	$arrSetUserRights[$i][] = array('NAME' => 'displayTracking', 'TITLE' => 'Sendungs-Tracking anzeigen');
	$arrSetUserRights[$i][] = array('NAME' => 'displaySendedMails', 'TITLE' => 'Gesendete Mails ansehen');
	$arrSetUserRights[$i][] = array('NAME' => 'switchMandator', 'TITLE' => 'Mandator-Umschalter anzeigen');
	$arrSetUserRights[$i][] = array('NAME' => 'exportCustomers', 'TITLE' => 'Kunden exportieren');
	$arrSetUserRights[$i][] = array('NAME' => 'displayHelp', 'TITLE' => 'Hilfe anzeigen');
	$arrSetUserRights[$i][] = array('NAME' => 'displayInternalMessages', 'TITLE' => 'Interne Nachrichten anzeigen');
	$arrSetUserRights[$i][] = array('NAME' => 'editInternalMessages', 'TITLE' => 'Interne Nachrichten bearbeiten');
	$arrSetUserRights[$i][] = array('NAME' => 'displayPrintProductionFiles', 'TITLE' => 'Belichtungsauftr&auml;ge ansehen');
	$arrSetUserRights[$i][] = array('NAME' => 'editPrintProductionFiles', 'TITLE' => 'Belichtungsauftr&auml;ge erstellen');
	$arrSetUserRights[$i][] = array('NAME' => 'transferPrintProduction', 'TITLE' => 'Produktionen transferieren');
	$arrSetUserRights[$i][] = array('NAME' => 'insertDeliSprintDatas', 'TITLE' => 'DPD-DeliSprint importieren');
	$arrSetUserRights[$i][] = array('NAME' => 'changeDocumentCustomerNumber', 'TITLE' => 'Dokument-Kundennummer ändern');

	$i++;
	$arrSetUserRightsHeader[$i]	= 'Daten-Verwaltung';
	$arrSetUserRights[$i][] = array('NAME' => 'exportDatas', 'TITLE' => 'Daten exportieren');
	$arrSetUserRights[$i][] = array('NAME' => 'importDatas', 'TITLE' => 'Daten importieren');
	$arrSetUserRights[$i][] = array('NAME' => 'reviseDocumentDatas', 'TITLE' => 'Daten korrigieren');

	$i++;
	$arrSetUserRightsHeader[$i]	= 'Admin-Bereich ';
	$arrSetUserRights[$i][] = array('NAME' => 'adminArea', 'TITLE' => 'Zugang Adminbereich');
	$arrSetUserRights[$i][] = array('NAME' => 'phpInfo', 'TITLE' => 'PHP-Info');
	$arrSetUserRights[$i][] = array('NAME' => 'serverInfo', 'TITLE' => 'Server-Info');
	$arrSetUserRights[$i][] = array('NAME' => 'dbInfo', 'TITLE' => 'DB-Info');
	$arrSetUserRights[$i][] = array('NAME' => 'displayUser', 'TITLE' => 'Benutzer anzeigen');
	$arrSetUserRights[$i][] = array('NAME' => 'editUser', 'TITLE' => 'Benutzer anlegen und bearbeiten');
	$arrSetUserRights[$i][] = array('NAME' => 'createTotalBackUp', 'TITLE' => 'BackUps verwalten');
	$arrSetUserRights[$i][] = array('NAME' => 'displayFtpDatas', 'TITLE' => 'FTP-Zugang bearbeiten');
	$arrSetUserRights[$i][] = array('NAME' => 'editFtpDatas', 'TITLE' => 'FTP-Zugang bearbeiten');
	$arrSetUserRights[$i][] = array('NAME' => 'displayDbDatas', 'TITLE' => 'Datenbank-Zugang anzeigen');
	$arrSetUserRights[$i][] = array('NAME' => 'editDbDatas', 'TITLE' => 'Datenbank-Zugang bearbeiten');
	$arrSetUserRights[$i][] = array('NAME' => 'displayMailDatas', 'TITLE' => 'Mailkonto anzeigen');
	$arrSetUserRights[$i][] = array('NAME' => 'editMailDatas', 'TITLE' => 'Mailkonto bearbeiten');
	$arrSetUserRights[$i][] = array('NAME' => 'createParams', 'TITLE' => 'Parameter erstellen');
	$arrSetUserRights[$i][] = array('NAME' => 'displayBasicDatas', 'TITLE' => 'Grundkonfiguration anzeigen');
	$arrSetUserRights[$i][] = array('NAME' => 'editBasicDatas', 'TITLE' => 'Grundkonfiguration bearbeiten');
	$arrSetUserRights[$i][] = array('NAME' => 'editLogFiles', 'TITLE' => 'Logdateien bearbeiten');
	$arrSetUserRights[$i][] = array('NAME' => 'openPhpMyAdmin', 'TITLE' => 'PhpMyAdmin öffnen');
	$arrSetUserRights[$i][] = array('NAME' => 'openPhpTerm', 'TITLE' => 'PhpTerm öffnen');
	$arrSetUserRights[$i][] = array('NAME' => 'cleanAuftragslisten', 'TITLE' => 'Auftragslisten bereinigen');
	$arrSetUserRights[$i][] = array('NAME' => 'displayMenueIcons', 'TITLE' => 'Menü-Icons anzeigen');


	$i++;
	$arrSetUserRightsHeader[$i]	= 'SEO-Tools';
	$arrSetUserRights[$i][] = array('NAME' => 'seoTools', 'TITLE' => 'SEO-Tools anzeigen');




	// BOF DEFINE RIGHTS

	// BOF DEFINE DUTY FIELDS
		$arrFormDutyFields = array(
			"editUserFirstName" => "Vorname",
			"editUserLastName" => "Nachname",
			"editUserLogin" => "Login"
		);
		// CREATE JSON DUTY FIELD
		$jsonFormDutyFields = createJson($arrFormDutyFields);
	// BOF DEFINE DUTY FIELDS

	if($_REQUEST["editID"] == "" && $_REQUEST["addDatas"] != "")
	{
		$_REQUEST["editID"] = "NEW";
		//header("location: displayOrders.php?ordersType=Bestellung");
		//exit;
	}

	// BOF STORE DATAS
	if($_POST["storeDatas"] != "") {

		$doAction = checkFormDutyFields($arrFormDutyFields);

		if($doAction) {
			if($_POST["editUserID"] == "") {
				// $_POST["editUserID"] = "%";
				$sql = "SELECT (MAX(`usersID`) + 1) AS `newUsersID` FROM`" . TABLE_USERS . "`";
				$rs = $dbConnection->db_query($sql);
				list($thisUsersID) = mysqli_fetch_array($rs);
				if($thisUsersID == null || $thisUsersID == 0){
					$thisUsersID = 1;
				}
			}
			else {
				$thisUsersID = $_POST["editUserID"];
			}
			$sql = "REPLACE INTO `" . TABLE_USERS . "` (
									`usersID`,
									`usersLogin`,
									`usersPassword`,
									`usersFirstName`,
									`usersLastName`,
									`userMailDatas`,
									`usersRights`,
									`usersActive`,
									`usersUserToPersonnel`
								)
								VALUES (
									'".$thisUsersID."',
									'".$_POST["editUserLogin"]."',";
			if($_POST["editUserNewPassword"] != "") {
				$sql .= "			'".md5($_POST["editUserNewPassword"])."',";
			}
			else {
				$sql .= "			'".$_POST["editUserPassword"]."',";
			}

			$sql .= "				'".($_POST["editUserFirstName"])."',
									'".($_POST["editUserLastName"])."',
									'".addslashes(serialize($_POST["editUserMailDatas"]))."',
									'".addslashes(serialize($_POST["editUserRights"]))."',
									'".$_POST["editUserActive"]."',
									'".$_POST["editUserToPersonnel"]."'
								)
			";

			$rs = $dbConnection->db_query($sql);

			if($rs) {
				$successMessage .= ' Der Datensatz wurde gespeichert.' .'<br />';
			}
			else {
				$errorMessage .= ' Der Datensatz konnte nicht gespeichert werden.' .'<br />';
			}
		}
		else {
			$errorMessage .= ' Der Datensatz konnte nicht gespeichert werden. Bitte füllen Sie alle Pflichtfelder aus.' .'<br />';
		}
	}
	// EOF STORE DATAS

	// BOF DELETE DATAS
		if($_POST["deleteDatas"] != "") {
			$sql = "DELETE FROM `" . TABLE_USERS . "` WHERE `usersID` = '".$_POST["editUserID"]."'";
			$rs = $dbConnection->db_query($sql);

			if($rs) {
				$successMessage .= ' Der Datensatz wurde endgültig gelöscht. ' .'<br />';
			}
			else {
				$errorMessage .= ' Der Datensatz konnte nicht gelöscht werden. ' .'<br />';
			}
		}


	// EOF DELETE DATAS

	// BOF READ ALL DATAS
	if($_REQUEST["editID"] == "") {
		$sql = "SELECT
				`usersActive`,
				`usersLastName`,
				`usersFirstName`,
				`usersID`,
				`usersLogin`

				FROM `" . TABLE_USERS . "`

				WHERE 1

				ORDER BY `usersActive` DESC, `usersLastName`, `usersFirstName`
		";
		$rs = $dbConnection->db_query($sql);

		while($ds = mysqli_fetch_assoc($rs)) {
			foreach(array_keys($ds) as $field) {
				$arrUserDatas[$ds["usersID"]][$field] = $ds[$field];
			}
		}
	}
	// EOF READ ALL DATAS

	// BOF READ SELECTED DATAS
	if($_REQUEST["editID"] != "") {
		$sql = "SELECT
					`usersID`,
					`usersLogin`,
					`usersPassword`,
					`usersFirstName`,
					`usersLastName`,
					`userMailDatas`,
					`usersRights`,
					`usersActive`,
					`usersUserToPersonnel`

					FROM `" . TABLE_USERS . "`

					WHERE `usersID` = '".$_REQUEST["editID"]."'
			";

			$rs = $dbConnection->db_query($sql);

			$selectedUserDatas = array();
			list(
				$selectedUserDatas["usersID"],
				$selectedUserDatas["usersLogin"],
				$selectedUserDatas["usersPassword"],
				$selectedUserDatas["usersFirstName"],
				$selectedUserDatas["usersLastName"],
				$selectedUserDatas["userMailDatas"],
				$selectedUserDatas["usersRights"],
				$selectedUserDatas["usersActive"],
				$selectedUserDatas["usersUserToPersonnel"]

			) = mysqli_fetch_array($rs);
		}
	// EOF READ SELECTED DATAS

	// BOF READ PERSONNEL DATAS
	if($_REQUEST["editID"] != "") {
		$arrPersonnelDatas = getPersonnelDatas();
	}
	// EOF READ PERSONNEL DATAS
?>
<?php
	require_once('inc/headerHTML.inc.php');
	if($_REQUEST["editID"] == "NEW") {
		$thisTitle = "Auftragslisten-Benutzer anlegen";
	}
	else {
		$thisTitle = "Auftragslisten-Benutzer bearbeiten";
	}
	$headerHTML = preg_replace("/{###TITLE###}/", strip_tags($thisTitle), $headerHTML);
	echo $headerHTML;

	require_once(FILE_MENUE_TOP);
?>
<div id="mainArea">
	<div id="mainContent">
		<?php require_once(FILE_MENUE_SIDEBAR); ?>
		<div id="contentArea">
			<div id="contentAreaElements">
				<h1><?php if(SHOW_PAGE_TITLE_ICONS) { echo '<img src="' . PATH_ICONS_MENUE_TITLES . 'personnel.png" alt="" />'; } ?> <?php echo $thisTitle; ?></h1>
				<div class="adminInfo">Datensatz-ID: <?php echo $selectedUserDatas["usersID"]; ?></div>
				<div class="contentDisplay">

					<?php displayMessages(); ?>

					<form name="editUserDatas" method="post" action="<?php echo $_SERVER["PHP_SELF"]; ?>">

					<?php
						if($_REQUEST["editID"] == "")
						{
					?>
					<div id="searchFilterArea">
						<table border="0" cellspacing="0" cellpadding="0">
							<tr>
								<td><b>Auftragslisten-Benutzer:</b></td>
								<td>
									<select name="editID" id="editID" class="inputSelect_510" onchange="this.form.submit();">
										<option value=""> --- Bitte w&auml;hlen --- </option>
										<?php
											if(!empty($arrUserDatas)) {
												foreach($arrUserDatas as $thisKey => $thisValue) {
													if(!$arrUserDatas[$thisKey]["usersActive"]) {
														$style = "color:#FF0000;font-style:italic;text-decoration:line-through;";
													}
													else {
														$style = "";
													}
													echo '<option style="'. $style .'" value="'.$thisKey.'">'.($arrUserDatas[$thisKey]["usersLastName"].', '.$arrUserDatas[$thisKey]["usersFirstName"].' ('.$thisValue["usersLogin"].')').' #ID ' . $thisKey . '</option>';
												}
											}
										?>
									</select>
								</td>
								<?php
									if($arrGetUserRights["editUser"]) {
								?>
								<td><input name="addDatas" id="addDatas" type="submit" value="Neuen Auftragslisten-Benutzer anlegen" class="inputButton1" /></td>
								<?php
									}
								?>
							</tr>
						</table>
					</div>
					<?php
						}
					?>

					<?php if($_REQUEST["editID"] != ""){ ?>
					<div class="adminEditArea">
					<p class="warningArea">Pflichtfelder m&uuml;ssen ausgef&uuml;llt werden!</p>
					<fieldset>
						<legend>Login-Daten</legend>
						<input type="hidden" name="editUserID" value="<?php echo $selectedUserDatas["usersID"]; ?>" />
						<table border="0" width="100%" cellspacing="0" cellpadding="0">
							<tr>
								<td><b>Vorname:</b></td>
								<td><input type="text" name="editUserFirstName" id="editUserFirstName" class="inputField_510" value="<?php echo ($selectedUserDatas["usersFirstName"]); ?>" /></td>
							</tr>
							<tr>
								<td><b>Nachname:</b></td>
								<td><input type="text" name="editUserLastName" id="editUserLastName" class="inputField_510" value="<?php echo ($selectedUserDatas["usersLastName"]); ?>" /></td>
							</tr>
							<tr>
								<td><b>Login:</b></td>
								<td><input type="text" name="editUserLogin" id="editUserLogin" class="inputField_510" value="<?php echo $selectedUserDatas["usersLogin"]; ?>" /></td>
							</tr>
							<tr>
								<td><b>Passwort:</b></td>
								<td>
									<input type="text" name="editUserNewPassword" id="editUserNewPassword" class="inputField_510" value="" />
									<input type="hidden" name="editUserPassword" id="editUserPassword" class="inputField_510" value="<?php echo $selectedUserDatas["usersPassword"]; ?>" />
								</td>
							</tr>
							<tr>
								<td><b>Auftragslisten-Benutzer aktivieren:</b></td>
								<td>
									<input type="checkbox" name="editUserActive" id="editUserActive" class="inputCheckbox1" value="1" <?php if($selectedUserDatas["usersActive"] == 1) { echo ' checked '; } ?> />
								</td>
							</tr>
							<tr>
								<td><b>Mitarbeiter zuordnen:</b></td>
								<td>
									<select name="editUserToPersonnel" id="editUserToPersonnel" class="inputSelect_510">
										<option value=""> --- Bitte w&auml;hlen --- </option>
										<?php
											if(!empty($arrPersonnelDatas)) {
												$thisMarker = '';
												foreach($arrPersonnelDatas as $thisKey => $thisValue) {
													if($thisMarker != $arrPersonnelDatas[$thisKey]["personnelGroup"]){
														$thisMarker = $arrPersonnelDatas[$thisKey]["personnelGroup"];
														echo '<option class="tableRowTitle1" value="">' . $arrPersonnelTypeDatas[$thisValue["personnelGroup"]]["personnelTypesName"] . '</option>';
													}
													if(!$arrPersonnelDatas[$thisKey]["personnelActive"]) {
														$style = "color:#FF0000;font-style:italic;text-decoration:line-through;";
													}
													else {
														$style = "";
													}
													$selected = '';
													if($thisKey == $selectedUserDatas["usersUserToPersonnel"]){
														$selected = ' selected="selected" ';
													}
													echo '<option style="'. $style .'" value="'.$thisKey.'" ' . $selected . ' >' . $arrPersonnelDatas[$thisKey]["personnelLastName"].', '.$arrPersonnelDatas[$thisKey]["personnelFirstName"].' ('.$arrPersonnelTypeDatas[$thisValue["personnelGroup"]]["personnelTypesName"].') - ' . $arrPersonnelDatas[$thisKey]["personnelActiveText"] . '</option>';
												}
											}
										?>
									</select>
								</td>
							</tr>
						</table>
					</fieldset>

					<fieldset>
						<legend>Mailkonto-Daten</legend>
						<?php
							$arrMandatoryDatas = getMandatories();

							$arrUserMailDatas = unserialize($selectedUserDatas["userMailDatas"]);

							foreach($arrMandatoryDatas as $thisMandatoryData){
								echo '<h2>' . $thisMandatoryData["mandatoriesName"] . '</h2>';
						?>
						<table border="0" width="100%" cellspacing="0" cellpadding="0">
							<tr>
								<td width="160"><b>Mail-Adresse:</b></td>
								<td><input type="text" name="editUserMailDatas[<?php echo $thisMandatoryData["mandatoriesShortName"]; ?>][mailAddress]" class="inputField_510" value="<?php echo $arrUserMailDatas[$thisMandatoryData["mandatoriesShortName"]]["mailAddress"]; ?>" /></td>
							</tr>
							<tr>
								<td><b>Posteingang Server:</b></td>
								<td><input type="text" name="editUserMailDatas[<?php echo $thisMandatoryData["mandatoriesShortName"]; ?>][serverInboxName]" class="inputField_510" value="<?php echo $arrUserMailDatas[$thisMandatoryData["mandatoriesShortName"]]["serverInboxName"]; ?>" /></td>
							</tr>
							<tr>
								<td><b>Posteingang Typ:</b></td>
								<td><input type="text" name="editUserMailDatas[<?php echo $thisMandatoryData["mandatoriesShortName"]; ?>][serverInboxType]" class="inputField_510" value="<?php echo $arrUserMailDatas[$thisMandatoryData["mandatoriesShortName"]]["serverInboxType"]; ?>" /></td>
							</tr>
							<tr>
								<td><b>Posteingang Port:</b></td>
								<td><input type="text" name="editUserMailDatas[<?php echo $thisMandatoryData["mandatoriesShortName"]; ?>][serverInboxPort]" class="inputField_510" value="<?php echo $arrUserMailDatas[$thisMandatoryData["mandatoriesShortName"]]["serverInboxPort"]; ?>" /></td>
							</tr>
							<tr>
								<td><b>Posteingang Login:</b></td>
								<td><input type="text" name="editUserMailDatas[<?php echo $thisMandatoryData["mandatoriesShortName"]; ?>][serverInboxLogin]" class="inputField_510" value="<?php echo $arrUserMailDatas[$thisMandatoryData["mandatoriesShortName"]]["serverInboxLogin"]; ?>" /></td>
							</tr>
							<tr>
								<td><b>Posteingang Passwort:</b></td>
								<td><input type="text" name="editUserMailDatas[<?php echo $thisMandatoryData["mandatoriesShortName"]; ?>][serverInboxPassword]" class="inputField_510" value="<?php echo $arrUserMailDatas[$thisMandatoryData["mandatoriesShortName"]]["serverInboxPassword"]; ?>" /></td>
							</tr>
							<tr>
								<td><b>Postausgang Server:</b></td>
								<td><input type="text" name="editUserMailDatas[<?php echo $thisMandatoryData["mandatoriesShortName"]; ?>][serverOutboxName]" class="inputField_510" value="<?php echo $arrUserMailDatas[$thisMandatoryData["mandatoriesShortName"]]["serverOutboxName"]; ?>" /></td>
							</tr>
							<tr>
								<td><b>Postausgang Typ:</b></td>
								<td><input type="text" name="editUserMailDatas[<?php echo $thisMandatoryData["mandatoriesShortName"]; ?>][serverOutboxType]" class="inputField_510" value="<?php echo $arrUserMailDatas[$thisMandatoryData["mandatoriesShortName"]]["serverOutboxType"]; ?>" /></td>
							</tr>
							<tr>
								<td><b>Postausgang Port:</b></td>
								<td><input type="text" name="editUserMailDatas[<?php echo $thisMandatoryData["mandatoriesShortName"]; ?>][serverOutboxPort]" class="inputField_510" value="<?php echo $arrUserMailDatas[$thisMandatoryData["mandatoriesShortName"]]["serverOutboxPort"]; ?>" /></td>
							</tr>
							<tr>
								<td><b>Postausgang Login:</b></td>
								<td><input type="text" name="editUserMailDatas[<?php echo $thisMandatoryData["mandatoriesShortName"]; ?>][serverOutboxLogin]" class="inputField_510" value="<?php echo $arrUserMailDatas[$thisMandatoryData["mandatoriesShortName"]]["serverOutboxLogin"]; ?>" /></td>
							</tr>
							<tr>
								<td><b>Postausgang Passwort:</b></td>
								<td><input type="text" name="editUserMailDatas[<?php echo $thisMandatoryData["mandatoriesShortName"]; ?>][serverOutboxPassword]" class="inputField_510" value="<?php echo $arrUserMailDatas[$thisMandatoryData["mandatoriesShortName"]]["serverOutboxPassword"]; ?>" /></td>
							</tr>
						</table>
						<hr />
						<?php
							}
						?>
					</fieldset>
					<fieldset>
						<legend>Benutzer-Rechte</legend>
							<div class="selectCheckboxes">
								<span class="selectAll">Alle Rechte in allen Bereichen aktivieren</span> <span class="unselectAll">Alle Rechte in allen Bereichen deaktivieren</span>
							</div>
							<hr />
							<?php
								$arrUsersRights = unserialize($selectedUserDatas["usersRights"]);
								if(!empty($arrSetUserRightsHeader)) {
									foreach($arrSetUserRightsHeader as $thisHeaderKey => $thisHeaderValue) {
										echo '<div class="userRightsArea">';
										echo '<h2>' . $thisHeaderValue . '</h2>';
										echo '<div class="selectCheckboxes">
												<span class="selectAll">Alle Rechte in diesem Bereich aktivieren</span> <span class="unselectAll">Alle Rechte in diesem Bereich deaktivieren</span>
											</div>
										';
										if(!empty($arrSetUserRights)) {
											echo '<div class="inputArea2">';
											foreach($arrSetUserRights[$thisHeaderKey] as $thisRightKey => $thisRightValue) {
												echo '<div class="userRightsItem">
														<input type="checkbox" class="editUserRights" name="editUserRights[' . $thisRightValue["NAME"] . ']" value="1" ';
														if($arrUsersRights[$thisRightValue["NAME"]] == "1") { echo ' checked = "checked" '; }
														echo '/> ' . $thisRightValue["TITLE"] . '
													</div>
												';
											}
											echo '<div class="clear"></div>';
											echo '</div>';
										}
										echo '</div>';
									}
								}
							?>
					</fieldset>
					<?php if($arrGetUserRights["editUser"]) { ?>
					<div class="actionButtonsArea">
						<input type="submit" class="inputButton1" name="storeDatas" value="Speichern" onclick="return checkFormDutyFields(this.form.name, <?php echo $jsonFormDutyFields; ?>);"  onclick="return showWarning(' Wollen Sie diesen Vorgang wirklich speichern??? '); "/>
						&nbsp;
						<input type="submit" class="inputButton1" name="deleteDatas" value="Entfernen" onclick="return showWarning(' Wollen Sie diesen Eintrag endgültig entfernen??? ');" />
						&nbsp;
						<input type="submit" class="inputButton1" name="resetDatas" value="Abbrechen" onclick="return showWarning(' Wollen Sie diesen Vorgang wirklich abbrechen??? '); "/>
					</div>
					<?php } ?>
					</div>
					<?php } ?>
					</form>
				</div>
			</div>
		</div>
		<div class="clear"></div>
	</div>
</div>

<script language="javascript" type="text/javascript">
	$(document).ready(function() {
		$(function() {
			$( "#datepicker" ).datepicker();
		});
		setFormDutyFields(<?php echo $jsonFormDutyFields; ?>);
		$('.selectAll').click(function() {
			$(this).parent().parent().find('.editUserRights').attr('checked', true);
		});
		$('.unselectAll').click(function() {
			$(this).parent().parent().find('.editUserRights').attr('checked', false);
		});
	});
</script>

<?php require_once('inc/footerHTML.inc.php'); ?>