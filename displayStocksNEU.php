<?php
	require_once('inc/requires.inc.php');

	DEFINE('DATE_STOCK_START', '2013-02-10');

	if(!$arrGetUserRights["editStocks"] && !$arrGetUserRights["displayStocks"]) {
		header('location: ' . PAGE_EXIT_LOCATION);
		exit;
	}

	$jswindowMessage = "";
	$warningMessage = '';
	$errorMessage = '';
	$successMessage = '';
	$infoMessage = '';

	$dbConnection = new DB_Connection();
	$db_open = $dbConnection->db_connect();

	// BOF READ ORDER CATEGORIES
		$arrOrderCategoriesTypeDatas = getOrderCategories('all', '');
		#dd('arrOrderCategoriesTypeDatas');
		$arrOrderCategoriesTypeDatasResort = array();
		$arrCountOrderCategoriesProducts = array();
		if(!empty($arrOrderCategoriesTypeDatas)){
			foreach($arrOrderCategoriesTypeDatas as $thisOrderCategoriesTypeDatasKey => $thisOrderCategoriesTypeDatasValue){
				$thisCategoryID = substr($thisOrderCategoriesTypeDatasValue["orderCategoriesRelationID"], 0, 3);

				if($thisOrderCategoriesTypeDatasValue["orderCategoriesUseInStock"] == '1'){
					if($thisOrderCategoriesTypeDatasValue["orderCategoriesRelationID"] != ''){
						$arrOrderCategoriesTypeDatasResort[$thisCategoryID][$thisOrderCategoriesTypeDatasValue["orderCategoriesRelationID"]][$thisOrderCategoriesTypeDatasValue["orderCategoriesID"]] = $thisOrderCategoriesTypeDatasValue;
						if(!isset($arrCountOrderCategoriesProducts[$thisCategoryID])) {$arrCountOrderCategoriesProducts[$thisCategoryID] = 0; }
						else { $arrCountOrderCategoriesProducts[$thisCategoryID]++;	}
					}
					else if($thisOrderCategoriesTypeDatasValue["orderCategoriesLevelID"] != ''){
						$arrOrderCategoriesTypeDatasResort[$thisOrderCategoriesTypeDatasValue["orderCategoriesLevelID"]][$thisOrderCategoriesTypeDatasValue["orderCategoriesLevelID"]][$thisOrderCategoriesTypeDatasValue["orderCategoriesID"]] = $thisOrderCategoriesTypeDatasValue;
					}
				}
			}
			$arrOrderCategoriesTypeDatas = $arrOrderCategoriesTypeDatasResort;
			#dd('arrOrderCategoriesTypeDatas');
		}
	// EOF READ ORDER CATEGORIES

	// BOF DELETE
		if($_GET["deleteNewStock"] == "1" && $_GET["deleteStockItemID"] != "") {
			$sql = "DELETE FROM `" . TABLE_STOCK_ITEMS . "`
						WHERE `stockItemsID` = '" . $_GET["deleteStockItemID"] . "'
				";
			$rs = $dbConnection->db_query($sql);
			if($rs) {
				$successMessage .= ' Der Eintrag wurde entfernt.' .'<br />';
			}
			else {
				$errorMessage .= ' Der Eintrag konnte nicht entfernt werden.' . '<br />';
			}
		}
	// EOF DELETE

	// BOF STORE STOCK
	if($_POST["storeNewStock"] != "") {
		$arrNewStockCategory = explode('#', $_POST["newStockCategory"]);
		$sql = "INSERT INTO `" . TABLE_STOCK_ITEMS . "` (
									`stockItemsID`,
									`stockItemsDate`,
									`stockItemsQuantity`,
									`stockItemsCategoryID`,
									`stockItemsPartNumber`,
									`stockItemsStoreDatetime`
								)
								VALUES (
									'%',
									'".formatDate($_POST["newStockDate"], 'store')."',
									'".$_POST["newStockQuantity"]."',
									'".$arrNewStockCategory[0]."',
									'".$arrNewStockCategory[1]."',
									NOW()
								)
		";
		$rs = $dbConnection->db_query($sql);
		if($rs) {
			$successMessage .= ' Der Eintrag wurde gespeichert.' .'<br />';
		}
		else {
			$errorMessage .= ' Der Eintrag konnte nicht gespeichert werden.' . '<br />';
		}
	}
	// EOF STORE STOCK


	// BOF GET CONSUMPTION
	$sql = "
			SELECT
					/*
					`orderCategoriesName`,
					`orderCategoriesPartNumber`,
					`orderCategoriesRelationID`,
					`orderCategoriesUseInStock`,
					*/
					`tempTable`.`ordersBestellDatum`,
					`tempTable`.`ordersBestellWoche`,
					`tempTable`.`ordersBestellJahr`,
					`tempTable`.`ordersProductMenge`,

					`tempTable`.`ordersWeek`,
					`tempTable`.`ordersStatus`,
					`tempTable`.`ordersArtikelNummer`,
					`tempTable`.`ordersID`

				FROM (
					SELECT
							DATE_FORMAT(`ordersBestellDatum`, '%Y') AS `ordersBestellJahr`,
							DATE_FORMAT(`ordersBestellDatum`, '%u') AS `ordersBestellWoche`,
							IF(`ordersArtikelNummer` IS NULL || `ordersArtikelNummer` = '', 'xxx', `ordersArtikelNummer`) AS `ordersArtikelNummer`,

							`ordersBestellDatum`,
							SUM(`ordersArtikelMenge`) AS `ordersProductMenge`,
							CONCAT(DATE_FORMAT(`ordersBestellDatum`, '%Y'), '-', DATE_FORMAT(`ordersBestellDatum`, '%u')) AS `ordersWeek`,
							`ordersStatus`,
							`ordersID`

						FROM `" . TABLE_ORDERS . "`
						WHERE 1
							AND `ordersBestellDatum` != '0000-00-00'
							AND (`ordersStatus` = '2' AND `ordersBestellDatum` > '" . DATE_STOCK_START . "')

						GROUP BY CONCAT(`ordersBestellJahr`, '-', `ordersBestellWoche`, ';', `ordersArtikelNummer`)

					UNION

					SELECT
							DATE_FORMAT(`ordersBestellDatum`, '%Y') AS `ordersBestellJahr`,
							DATE_FORMAT(`ordersBestellDatum`, '%u') AS `ordersBestellWoche`,
							IF(`ordersAdditionalArtikelNummer` IS NULL || `ordersAdditionalArtikelNummer` = '', 'xxx', `ordersAdditionalArtikelNummer`) AS `ordersArtikelNummer`,

							`ordersBestellDatum`,
							SUM(`ordersAdditionalArtikelMenge`) AS `ordersProductMenge`,
							CONCAT(DATE_FORMAT(`ordersBestellDatum`, '%Y'), '-', DATE_FORMAT(`ordersBestellDatum`, '%u')) AS `ordersWeek`,
							`ordersStatus`,
							`ordersID`

						FROM `" . TABLE_ORDERS . "`
						WHERE 1
							AND `ordersBestellDatum` != '0000-00-00'
							AND (`ordersStatus` = '2' AND `ordersBestellDatum` > '" . DATE_STOCK_START . "')

						GROUP BY CONCAT(`ordersBestellJahr`, '-', `ordersBestellWoche`, ';', `ordersAdditionalArtikelNummer`)

					/* ################################## */

					UNION

					/* ################################## */

						SELECT
							DATE_FORMAT(`ordersFreigabeDatum`, '%Y') AS `ordersBestellJahr`,
							DATE_FORMAT(`ordersFreigabeDatum`, '%u') AS `ordersBestellWoche`,
							IF(`ordersArtikelNummer` IS NULL || `ordersArtikelNummer` = '', 'xxx', `ordersArtikelNummer`) AS `ordersArtikelNummer`,

							`ordersFreigabeDatum`,
							SUM(`ordersArtikelMenge`) AS `ordersProductMenge`,
							CONCAT(DATE_FORMAT(`ordersFreigabeDatum`, '%Y'), '-', DATE_FORMAT(`ordersFreigabeDatum`, '%u')) AS `ordersWeek`,
							`ordersStatus`,
							`ordersID`

						FROM `" . TABLE_ORDERS . "`
						WHERE 1
							AND `ordersFreigabeDatum` != '0000-00-00'
							AND (`ordersStatus` = '2' AND `ordersFreigabeDatum` > '" . DATE_STOCK_START . "')
						GROUP BY CONCAT(`ordersBestellJahr`, '-', `ordersBestellWoche`, ';', `ordersArtikelNummer`)

					UNION

					SELECT
							DATE_FORMAT(`ordersFreigabeDatum`, '%Y') AS `ordersBestellJahr`,
							DATE_FORMAT(`ordersFreigabeDatum`, '%u') AS `ordersBestellWoche`,
							IF(`ordersAdditionalArtikelNummer` IS NULL || `ordersAdditionalArtikelNummer` = '', 'xxx', `ordersAdditionalArtikelNummer`) AS `ordersArtikelNummer`,

							`ordersFreigabeDatum`,
							SUM(`ordersAdditionalArtikelMenge`) AS `ordersProductMenge`,
							CONCAT(DATE_FORMAT(`ordersFreigabeDatum`, '%Y'), '-', DATE_FORMAT(`ordersFreigabeDatum`, '%u')) AS `ordersWeek`,
							`ordersStatus`,
							`ordersID`

						FROM `" . TABLE_ORDERS . "`
						WHERE 1
							AND `ordersFreigabeDatum` != '0000-00-00'
							AND (`ordersStatus` = '2' AND `ordersFreigabeDatum` > '" . DATE_STOCK_START . "')

						GROUP BY CONCAT(`ordersBestellJahr`, '-', `ordersBestellWoche`, ';', `ordersAdditionalArtikelNummer`)

					/* ################################## */

					UNION

					/* ################################## */

						SELECT
							DATE_FORMAT(`ordersBelichtungsDatum`, '%Y') AS `ordersBestellJahr`,
							DATE_FORMAT(`ordersBelichtungsDatum`, '%u') AS `ordersBestellWoche`,
							IF(`ordersArtikelNummer` IS NULL || `ordersArtikelNummer` = '', 'xxx', `ordersArtikelNummer`) AS `ordersArtikelNummer`,

							`ordersBelichtungsDatum`,
							SUM(`ordersArtikelMenge`) AS `ordersProductMenge`,
							CONCAT(DATE_FORMAT(`ordersBelichtungsDatum`, '%Y'), '-', DATE_FORMAT(`ordersBelichtungsDatum`, '%u')) AS `ordersWeek`,
							`ordersStatus`,
							`ordersID`

						FROM `" . TABLE_ORDERS . "`
						WHERE 1
							AND `ordersBelichtungsDatum` != '0000-00-00'
							AND (
								(`ordersStatus` = '3' AND `ordersBelichtungsDatum` > '" . DATE_STOCK_START . "')
								OR
								(`ordersStatus` = '7' AND `ordersProduktionsDatum` > '" . DATE_STOCK_START . "')
							)

						GROUP BY CONCAT(`ordersBestellJahr`, '-', `ordersBestellWoche`, ';', `ordersArtikelNummer`)

					UNION

						SELECT
							DATE_FORMAT(`ordersBelichtungsDatum`, '%Y') AS `ordersBestellJahr`,
							DATE_FORMAT(`ordersBelichtungsDatum`, '%u') AS `ordersBestellWoche`,
							IF(`ordersAdditionalArtikelNummer` IS NULL || `ordersAdditionalArtikelNummer` = '', 'xxx', `ordersAdditionalArtikelNummer`) AS `ordersArtikelNummer`,

							`ordersBelichtungsDatum`,
							SUM(`ordersAdditionalArtikelMenge`) AS `ordersProductMenge`,
							CONCAT(DATE_FORMAT(`ordersBelichtungsDatum`, '%Y'), '-', DATE_FORMAT(`ordersBelichtungsDatum`, '%u')) AS `ordersWeek`,
							`ordersStatus`,
							`ordersID`

						FROM `" . TABLE_ORDERS . "`
						WHERE 1
							AND `ordersBelichtungsDatum` != '0000-00-00'
							AND (
								(`ordersStatus` = '3' AND `ordersBelichtungsDatum` > '" . DATE_STOCK_START . "')
								OR
								(`ordersStatus` = '7' AND `ordersProduktionsDatum` > '" . DATE_STOCK_START . "')
							)

						GROUP BY CONCAT(`ordersBestellJahr`, '-', `ordersBestellWoche`, ';', `ordersAdditionalArtikelNummer`)

					/* ################################## */

					UNION

					/* ################################## */

						SELECT
							DATE_FORMAT(`ordersAuslieferungsDatum`, '%Y') AS `ordersBestellJahr`,
							DATE_FORMAT(`ordersAuslieferungsDatum`, '%u') AS `ordersBestellWoche`,
							IF(`ordersArtikelNummer` IS NULL || `ordersArtikelNummer` = '', 'xxx', `ordersArtikelNummer`) AS `ordersArtikelNummer`,

							`ordersAuslieferungsDatum`,
							SUM(`ordersArtikelMenge`) AS `ordersProductMenge`,
							CONCAT(DATE_FORMAT(`ordersAuslieferungsDatum`, '%Y'), '-', DATE_FORMAT(`ordersAuslieferungsDatum`, '%u')) AS `ordersWeek`,
							`ordersStatus`,
							`ordersID`

						FROM `" . TABLE_ORDERS . "`
						WHERE 1
							AND `ordersAuslieferungsDatum` != '0000-00-00'
							AND (`ordersStatus` = '4' AND `ordersAuslieferungsDatum` > '" . DATE_STOCK_START . "')

						GROUP BY CONCAT(`ordersBestellJahr`, '-', `ordersBestellWoche`, ';', `ordersArtikelNummer`)

					UNION

					SELECT
							DATE_FORMAT(`ordersAuslieferungsDatum`, '%Y') AS `ordersBestellJahr`,
							DATE_FORMAT(`ordersAuslieferungsDatum`, '%u') AS `ordersBestellWoche`,
							IF(`ordersAdditionalArtikelNummer` IS NULL || `ordersAdditionalArtikelNummer` = '', 'xxx', `ordersAdditionalArtikelNummer`) AS `ordersArtikelNummer`,

							`ordersAuslieferungsDatum`,
							SUM(`ordersAdditionalArtikelMenge`) AS `ordersProductMenge`,
							CONCAT(DATE_FORMAT(`ordersAuslieferungsDatum`, '%Y'), '-', DATE_FORMAT(`ordersAuslieferungsDatum`, '%u')) AS `ordersWeek`,
							`ordersStatus`,
							`ordersID`

						FROM `" . TABLE_ORDERS . "`
						WHERE 1
							AND `ordersAuslieferungsDatum` != '0000-00-00'
							AND (`ordersStatus` = '4' AND `ordersAuslieferungsDatum` > '" . DATE_STOCK_START . "')

						GROUP BY CONCAT(`ordersBestellJahr`, '-', `ordersBestellWoche`, ';', `ordersAdditionalArtikelNummer`)

					ORDER BY `ordersBestellJahr` DESC, `ordersBestellWoche` DESC

			) AS `tempTable`

		LEFT JOIN `" . TABLE_PRODUCTIONS_TRANSFER . "`
		ON(`tempTable`.`ordersID` = `" . TABLE_PRODUCTIONS_TRANSFER . "`.`ordersProductionsTransferOrdersID`)

		LEFT JOIN `" . TABLE_ORDER_CATEGORIES . "` AS `bctrProductCategories`
		ON (`tempTable`.`ordersArtikelNummer` = `bctrProductCategories`.`orderCategoriesPartNumber_bctr`)

		LEFT JOIN `" . TABLE_ORDER_CATEGORIES . "` AS `b3ProductCategories`
		ON (`tempTable`.`ordersArtikelNummer` = `b3ProductCategories`.`orderCategoriesPartNumber_b3`)

		WHERE 1
			AND `common_ordersProductionsTransfer`.`ordersProductionsTransferOrdersID` IS NULL
			AND (
				`bctrProductCategories`.`orderCategoriesUseInStock` = '1'
				OR `b3ProductCategories`.`orderCategoriesUseInStock` = '1'
			)
	";

	$rs = $dbConnection->db_query($sql);

	while($ds = mysqli_fetch_assoc($rs)) {

		if($ds["orderCategoriesPartNumber_bctr"] != ''){
			$ds["ordersArtikelNummer"] = $ds["orderCategoriesPartNumber_bctr"];
		}

		if($ds["ordersStatus"] == 4) {
			foreach(array_keys($ds) as $field) {
				$arrConsumptionDatas[$ds["ordersArtikelNummer"]][$ds["ordersWeek"]][$field] = $ds[$field];
			}
		}

		if($ds["ordersStatus"] == 4) {
			$arrConsumptionQuantity[$ds["ordersArtikelNummer"]] += $ds["ordersProductMenge"];
		}
		else if($ds["ordersStatus"] == 2) {
			$arrReleasedQuantity[$ds["ordersArtikelNummer"]] += $ds["ordersProductMenge"];
			$arrNeededQuantity[$ds["ordersArtikelNummer"]] += $ds["ordersProductMenge"];
		}
		else if($ds["ordersStatus"] == 3) {
			$arrExposedQuantity[$ds["ordersArtikelNummer"]] += $ds["ordersProductMenge"];
			$arrNeededQuantity[$ds["ordersArtikelNummer"]] += $ds["ordersProductMenge"];
		}
	}
	// EOF GET CONSUMPTION
#dd('arrConsumptionDatas');
	// BOF GET STOCK
	$sql = "SELECT
				`" . TABLE_STOCK_ITEMS . "`.`stockItemsID`,
				`" . TABLE_STOCK_ITEMS . "`.`stockItemsDate`,
				`" . TABLE_STOCK_ITEMS . "`.`stockItemsQuantity`,
				`" . TABLE_STOCK_ITEMS . "`.`stockItemsCategoryID`,
				`" . TABLE_STOCK_ITEMS . "`.`stockItemsPartNumber`,
				`" . TABLE_STOCK_ITEMS . "`.`stockItemsStoreDatetime`,

				`products`.`orderCategoriesID` AS `productCategoriesID`,
				`products`.`orderCategoriesParentID` AS `productCategoriesParentID`,
				`products`.`orderCategoriesLevelID` AS `productCategoriesLevelID`,
				`products`.`orderCategoriesRelationID` AS `productCategoriesRelationID`,

				IF(`products`.`orderCategoriesRelationID` != '',
					SUBSTRING(`products`.`orderCategoriesRelationID`, 1, 3),
					SUBSTRING(`" . TABLE_STOCK_ITEMS . "`.`stockItemsCategoryID`, 1, 3)
				) AS `productCategoriesRelationParentID`,

				`products`.`orderCategoriesShortName` AS `productCategoriesShortName`,
				`products`.`orderCategoriesName` AS `productCategoriesName`,
				`products`.`orderCategoriesPartNumber` AS `productCategoriesPartNumber`,
				`products`.`orderCategoriesName_bctr` AS `productCategoriesName_bctr`,
				`products`.`orderCategoriesPartNumber_bctr` AS `productCategoriesPartNumber_bctr`,
				`products`.`orderCategoriesName_b3` AS `productCategoriesName_b3`,
				`products`.`orderCategoriesPartNumber_b3` AS `productCategoriesPartNumber_b3`,
				`products`.`orderCategoriesSort` AS `productCategoriesSort`,
				`products`.`orderCategoriesActive` AS `productCategoriesActive`,
				`products`.`orderCategoriesUseInStock` AS `productCategoriesUseInStock`,

				`parentProducts`.`orderCategoriesID` AS `parentProductCategoriesID`,
				`parentProducts`.`orderCategoriesParentID` AS `parentProductCategoriesParentID`,
				`parentProducts`.`orderCategoriesLevelID` AS `parentProductCategoriesLevelID`,
				`parentProducts`.`orderCategoriesRelationID` AS `parentProductCategoriesRelationID`,

				IF(`parentProducts`.`orderCategoriesRelationID` != '',
					SUBSTRING(`parentProducts`.`orderCategoriesRelationID`, 1, 3),
					SUBSTRING(`" . TABLE_STOCK_ITEMS . "`.`stockItemsCategoryID`, 1, 3)
				) AS `parentProductCategoriesRelationParentID`,

				`parentProducts`.`orderCategoriesShortName` AS `parentProductCategoriesShortName`,
				`parentProducts`.`orderCategoriesName` AS `parentProductCategoriesName`,
				`parentProducts`.`orderCategoriesPartNumber` AS `parentProductCategoriesPartNumber`,
				`parentProducts`.`orderCategoriesName_bctr` AS `parentProductCategoriesName_bctr`,
				`parentProducts`.`orderCategoriesPartNumber_bctr` AS `parentProductCategoriesPartNumber_bctr`,
				`parentProducts`.`orderCategoriesName_b3` AS `parentProductCategoriesName_b3`,
				`parentProducts`.`orderCategoriesPartNumber_b3` AS `parentProductCategoriesPartNumber_b3`,
				`parentProducts`.`orderCategoriesSort` AS `parentProductCategoriesSort`,
				`parentProducts`.`orderCategoriesActive` AS `parentProductCategoriesActive`,
				`parentProducts`.`orderCategoriesUseInStock` AS `parentProductCategoriesUseInStock`

				FROM `" . TABLE_STOCK_ITEMS . "`

				LEFT JOIN `" . TABLE_ORDER_CATEGORIES . "` AS `products`
				ON(`" . TABLE_STOCK_ITEMS . "`.`stockItemsCategoryID` = `products`.`orderCategoriesRelationID`)

				LEFT JOIN `" . TABLE_ORDER_CATEGORIES . "` AS `parentProducts`
				ON(`products`.`orderCategoriesRelationID` = `parentProducts`.`orderCategoriesID`)

				WHERE 1
					AND `products`.`orderCategoriesUseInStock` = '1'
					AND `parentProducts`.`orderCategoriesUseInStock` = '1'


				GROUP BY CONCAT(
							`" . TABLE_STOCK_ITEMS . "`.`stockItemsPartNumber`,
							`" . TABLE_STOCK_ITEMS . "`.`stockItemsStoreDatetime`)

				ORDER BY
					`" . TABLE_STOCK_ITEMS . "`.`stockItemsCategoryID`,
					`" . TABLE_STOCK_ITEMS . "`.`stockItemsDate` DESC
		";

	$rs = $dbConnection->db_query($sql);
	while($ds = mysqli_fetch_assoc($rs)) {
		#dd('ds');
		$thisPartNumber = $ds["parentProductCategoriesPartNumber"];
		if($thisPartNumber == ''){
			$thisPartNumber = $ds["stockItemsPartNumber"];
		}
		foreach(array_keys($ds) as $field) {
			#$arrStockDatas[$ds["orderCategoriesRelationParentID"]][$ds["stockItemsCategoryID"]][$ds["stockItemsID"]][$field] = $ds[$field];
			$arrStockDatas[$thisPartNumber][$ds["stockItemsID"]][$field] = $ds[$field];
		}

		$arrStockQuantity[$thisPartNumber] += $ds["stockItemsQuantity"];
		#$arrStockQuantity[$ds["stockItemsCategoryID"]] += $ds["stockItemsQuantity"];
	}
	// EOF GET STOCK

	// BOF GET SUPPLIER ORDERS
	$sql = "
			SELECT
					`" . TABLE_SUPPLIER_ORDERS . "`.`supplierOrdersID`,
					`" . TABLE_SUPPLIER_ORDERS . "`.`supplierOrdersSupplierID`,
					`" . TABLE_SUPPLIER_ORDERS . "`.`supplierOrdersOrderProcessID`,
					SUM(`" . TABLE_SUPPLIER_ORDERS . "`.`supplierOrdersAmount`) AS `supplierOrdersAmount`,
					`" . TABLE_SUPPLIER_ORDERS . "`.`supplierOrdersProductNumber`,
					`" . TABLE_SUPPLIER_ORDERS . "`.`supplierOrdersProductName`,
					`" . TABLE_SUPPLIER_ORDERS . "`.`supplierOrdersProductID`,
					`" . TABLE_SUPPLIER_ORDERS . "`.`supplierOrdersProductKategorieID`,
					`" . TABLE_SUPPLIER_ORDERS . "`.`supplierOrdersProductSinglePrice`,
					`" . TABLE_SUPPLIER_ORDERS . "`.`supplierOrdersOrderDate`,
					`" . TABLE_SUPPLIER_ORDERS . "`.`supplierOrdersDeliveryDate`,
					`" . TABLE_SUPPLIER_ORDERS . "`.`supplierOrdersDeliveryDirectToCustomer`,

					SUBSTRING(`" . TABLE_SUPPLIER_ORDERS . "`.`supplierOrdersProductKategorieID`, 1, 3) AS `orderCategoriesRelationParentID`

				FROM `" . TABLE_SUPPLIER_ORDERS . "`

				WHERE 1
					AND `" . TABLE_SUPPLIER_ORDERS . "`.`supplierOrdersDeliveryDirectToCustomer` != '1'


				GROUP BY `" . TABLE_SUPPLIER_ORDERS . "`.`supplierOrdersProductNumber`
		";
	$rs = $dbConnection->db_query($sql);
	while($ds = mysqli_fetch_assoc($rs)) {
		foreach(array_keys($ds) as $field) {
			$arrSupplierOrderDatas[$ds["supplierOrdersProductNumber"]][$field] = $ds[$field];
		}
		$arrSupplierOrderQuantity[$ds["supplierOrdersProductNumber"]] += $ds["supplierOrdersAmount"];
	}
	// EOF GET SUPPLIER ORDERS

	// BOF AMAZON ORDERS
	// EOF AMAZON ORDERS

	dd('arrOrderCategoriesTypeDatas');
?>
<?php
	require_once('inc/headerHTML.inc.php');
	$thisTitle = "Bestand - Wareneing&auml;nge und Warenausg&auml;nge";
	$headerHTML = preg_replace("/{###TITLE###}/", strip_tags($thisTitle), $headerHTML);
	echo $headerHTML;

	require_once(FILE_MENUE_TOP);
?>
<div id="mainArea">
	<div id="mainContent">
		<?php require_once(FILE_MENUE_SIDEBAR); ?>
<div id="contentArea">
	<div id="contentAreaElements">
		<h1><?php if(SHOW_PAGE_TITLE_ICONS) { echo '<img src="' . PATH_ICONS_MENUE_TITLES . 'statistics.png" alt="" />'; } ?> <?php echo $thisTitle; ?></h1>

		<div class="contentDisplay">
			<?php displayMessages(); ?>

			<?php if($arrGetUserRights["editStocks"] == '1') { ?>

			<div class="boxArea1">
				<h2>Neue Lieferungen eintragen</h2>
				<div>
					<div class="boxAreaContent">
						<form name="formEditStock" action="<?php echo $_SERVER["PHP_SELF"]; ?>" method="post">
							<table border="0" cellpadding="0" cellspacing="0" width="">
								<tr>
									<td><label for="newStockCategory">Artikel:</label></td>
									<td>
										<select name="newStockCategory" id="newStockCategory" class="inputSelect_200">
											<option value="0"></option>
											<?php
												if(!empty($arrOrderCategoriesTypeDatas)) {
													$thisMarker1 = '';
													$thisMarker2 = '';
													foreach($arrOrderCategoriesTypeDatas as $thisOrderCategoriesTypeDatasKey => $thisOrderCategoriesTypeDatasValue){
														foreach($thisOrderCategoriesTypeDatasValue as $thisOrderCategoriesTypeDatasKey2 => $thisOrderCategoriesTypeDatasValue2){
															foreach($thisOrderCategoriesTypeDatasValue2 as $thisKey => $thisValue){
																$thisClass = '';
																$thisStyle = '';
																if($thisValue["parentProductCategoriesRelationID"]){
																	//$thisValue
																}
																if($thisValue["orderCategoriesPartNumber"] == ''){
																	//$thisValue
																}

																if($thisMarker1 != $thisOrderCategoriesTypeDatasKey){
																	$thisClass = 'tableRowTitle1';
																	$thisMarker1 = $thisOrderCategoriesTypeDatasKey;
																}

																if($thisMarker2 != $thisOrderCategoriesTypeDatasKey2){
																	$thisStyle = 'border-top:2px solid #666;';
																	$thisMarker2 = $thisOrderCategoriesTypeDatasKey2;
																}

																$thisDistance = "";
																$thisProductNumber = "";
																if(strlen($thisKey) > 3){
																	$thisProductNumber = ' [Art-Nr: ' .$thisValue["orderCategoriesPartNumber"] . ']';
																}

																if(strlen($thisKey) > 3) { $thisDistance = " &bull; "; }
																echo '<option class="' . $thisClass . '" style="' . $thisStyle . '" value="'.$thisKey.'">' . $thisDistance . $thisValue["orderCategoriesName_bctr"] . $thisProductNumber . '</option>';
															}
														}
													}
												}
											?>
										</select>
									</td>
									<td style="border-right:1px dotted #333333;"></td>
									<td><label for="newStockDate">Anlieferungs-Datum:</label></td>
									<td><input type="text" name="newStockDate" id="newStockDate" class="inputField_70" readonly value="" /></td>
									<td style="border-right:1px dotted #333333;"></td>
									<td><label for="newStockQuantity">St&uuml;ckzahl:</label></td>
									<td><input type="text" name="newStockQuantity" id="newStockQuantity" class="inputField_100" value="" /></td>
									<td style="border-right:1px dotted #333333;"></td>
									<td style="text-align:right;"><input type="submit" name="storeNewStock" class="inputButton2" value="Speichern" /></td>
								</tr>
							</table>
						</form>
					</div>
				</div>
			</div>
			<hr />
			<?php
				}
				if(!empty($arrOrderCategoriesTypeDatas)) {

					echo '<h2>Bestands&uuml;bersichten</h2>';
					echo '<a href="displayStocks.php">ZUR ALTEN VERSION</a>';

					echo '<table border="0" class="displayOrders" cellpadding="0" cellspacing="0" width="100%">';
					echo '<tr>';
					echo '<th style="width:45px;text-align:right;">#</th>';
					echo '<th>Artikel</th>';
					echo '<th>Art.-Nr</th>';
					if($arrGetUserRights["editStocks"] == '1'){
						echo '<th>KAT-ID</th>';
					}

					echo '<th>Lieferungen insg.</th>';
					echo '<th>Verbrauch insg.<br />(Versendet)</th>';
					echo '<th>Ben&ouml;tigt insg.<br />(Freigaben + Belichtet)</th>';
					echo '<th>AMAZON-Verk.</th>';
					echo '<th>Bestellt</th>';
					echo '<th>Aktueller Bestand insg.</th>';
					echo '<th style="width:45px;">Details</th>';
					echo '<tr>';

					$count = 0;

					$thisCategorieMarker = '';
					$thisParentProductMarker = '';

					foreach($arrOrderCategoriesTypeDatas as $thisCategoryKey => $thisCategoryValue) {
						foreach($thisCategoryValue as $thisGroupKey => $thisGroupValue) {
							foreach($thisGroupValue as $thisProductKey => $thisProductValue) {
								if($thisCategorieMarker != $thisCategoryKey && $arrCountOrderCategoriesProducts[$thisCategoryKey] > 0){
									echo '<tr style="">';

									echo '<td class="tableRowTitle1" colspan="11">';
									echo $arrOrderCategoriesTypeDatas[$thisCategoryKey][$thisGroupKey][$thisProductKey]["orderCategoriesName"];
									echo '</td>';

									echo '</tr>';
									$thisCategorieMarker = $thisCategoryKey;
								}


								if($thisParentProductMarker != $thisGroupKey && $arrOrderCategoriesTypeDatas[$thisCategoryKey][$thisGroupKey][$thisProductKey]["parentProductCategoriesName"] != ''&& $arrOrderCategoriesTypeDatas[$thisCategoryKey][$thisGroupKey][$thisProductKey]["productSource"] == 'Auftragslisten'){
									echo '<tr style="">';

									echo '<td class="tableRowTitle2" colspan="11">';

									echo $arrOrderCategoriesTypeDatas[$thisCategoryKey][$thisGroupKey][$thisProductKey]["parentProductCategoriesName"];
									echo '</td>';

									echo '</tr>';
									$thisParentProductMarker = $thisGroupKey;
								}

								if(strlen($thisProductKey) > 3){
									$thisPartNumber = $arrOrderCategoriesTypeDatas[$thisCategoryKey][$thisGroupKey][$thisProductKey]["orderCategoriesPartNumber"];

									if($count%2 == 0){ $rowClass = 'row0'; }
									else { $rowClass = 'row1'; }

									echo '<tr class="'.$rowClass.'">';

									echo '<td style="text-align:right;"><b>'.($count + 1).'.</b></td>';

									echo '<td style="white-space:nowrap;">';
									# echo $arrOrderCategoriesTypeDatas[$thisCategoryKey][$thisGroupKey][$thisProductKey]["orderCategoriesName"];
									echo $arrOrderCategoriesTypeDatas[$thisCategoryKey][$thisGroupKey][$thisProductKey]["orderCategoriesName"];
									echo '</td>';

									echo '<td style="white-space:nowrap;">';
									echo $thisPartNumber;
									echo '</td>';

									if($arrGetUserRights["editStocks"] == '1'){
										echo '<td>';
										echo $arrOrderCategoriesTypeDatas[$thisCategoryKey][$thisGroupKey][$thisProductKey]["orderCategoriesID"];
										echo '</td>';
									}

									echo '<td style="text-align:right;">';
									$thisStockQuantity = $arrStockQuantity[$thisPartNumber];
									if($thisStockQuantity == ''){
										$thisStockQuantity = 0;
									}
									echo number_format($thisStockQuantity, 0, ',', '.');
									echo '</td>';

									echo '<td style="text-align:right;">';
									$thisConsumptionQuantity = $arrConsumptionQuantity[$thisPartNumber];
									if($thisConsumptionQuantity == ''){
										$thisConsumptionQuantity = 0;
									}
									echo number_format($thisConsumptionQuantity, 0, ',', '.');
									echo '</td>';

									echo '<td style="text-align:right;">';
									$thisNeededQuantity = $arrNeededQuantity[$thisPartNumber];
									if($thisNeededQuantity == ''){
										$thisNeededQuantity = 0;
									}
									echo number_format($thisNeededQuantity, 0, ',', '.');
									echo '</td>';

									echo '<td style="text-align:right;">';
									echo 'AMZ';
									echo '</td>';

									echo '<td style="text-align:right;">';
									$thisSupplierOrderQuantity = $arrSupplierOrderQuantity[$thisPartNumber];
									if($thisSupplierOrderQuantity == ''){
										$thisSupplierOrderQuantity = 0;
									}
									echo number_format($thisSupplierOrderQuantity, 0, ',', '.');
									echo '</td>';

									echo '<td style="text-align:right; background-color:#FEFFAF;">';

									#####################################

									$thisDifferenceConsumptionToStockQuantity = $arrStockQuantity[$thisPartNumber] - $arrConsumptionQuantity[$thisPartNumber];
									echo number_format($thisDifferenceConsumptionToStockQuantity, 0, ',', '.');
									if(($thisDifferenceConsumptionToStockQuantity) < $arrNeededQuantity[$thisPartNumber]) {
										echo ' <img src="layout/icons/iconAttention.png" width="16" height="16" alt="Achtung" title="Achtung, Bestand gering"> ';
									}
									else {
										echo ' <img src="layout/icons/iconOk.png" width="16" height="16" alt="ok" title="Bestand in Ordnung"> ';
									}
									echo '</td>';

									echo '<td style="text-align:center;">';
										echo '<img src="layout/icons/iconInfo.png" class="buttonShopOrderInfo" alt="" title="Detailinformationen">';
										echo '<div class="orderDetailsContent" style="display:none; visibility:hidden;">';

										echo '<p>';
										echo '<b>Bestand insgesamt:</b> ' . number_format($thisDifferenceConsumptionToStockQuantity, 0, ',', '.');
										if(($thisDifferenceConsumptionToStockQuantity) < $arrNeededQuantity[$thisPartNumber]) {
											echo ' <img src="layout/icons/iconAttention.png" width="16" height="16" alt="Achtung" title="Achtung, Bestand gering"> ';
										}
										else {
											echo ' <img src="layout/icons/iconOk.png" width="16" height="16" alt="ok" title="Bestand in Ordnung"> ';
										}
										echo '</p>';
										echo '		<div class="boxArea2">
													<div class="boxAreaContent">
														<h3>Lieferungen ' . $arrOrderCategoriesTypeDatas[$thisPartNumber]["orderCategoriesName"] . '</h3>
														<div>
											';
										echo '<p><b>Lieferungen insgesamt:</b> ' . number_format($arrStockQuantity[$thisPartNumber], 0, ',', '.') . '</p>' ;
										echo '<table border="0" class="border" cellpadding="0" cellspacing="0" width="100%"">
												<tr>
													<th style="width:45px;text-align:right;">#</th>
													<th>Eingangs-Datum</th>
													<th>St&uuml;ckzahl</th>
													<th>Aktion</th>
												</tr>
											';
											$countRow = 1;
											$thisMarker = '';
											foreach($arrStockDatas[$thisPartNumber] as $thisDataKey => $thisDataValue) {
												$thisRowTitle = date("Y", strtotime($arrStockDatas[$thisPartNumber][$thisDataKey]["stockItemsDate"]));
												if($thisMarker != $thisRowTitle){
													echo '<tr class="tableRowTitle1">';
													echo '<td colspan="4">';
													echo $thisRowTitle;
													echo '</td>';
													echo '</tr>';
													$thisMarker = $thisRowTitle;
												}

												if($countRow%2 == 0){ $rowClass = 'row0'; }
												else { $rowClass = 'row1'; }
												echo '<tr class="' . $rowClass . '">';
												echo '<td style="text-align:right;"><span title="'.$thisDataKey.'"><b>' . $countRow . '. </b></span></td>';
												echo '<td style="text-align:center;">' . formatDate($arrStockDatas[$thisPartNumber][$thisDataKey]["stockItemsDate"], 'display') . '</td>';
												echo '<td style="text-align:right; background-color:#FEFFAF;">' . number_format($arrStockDatas[$thisPartNumber][$thisDataKey]["stockItemsQuantity"], 0, ',', '.') . '</td>';
												echo '<td style="text-align:center;"><a href="' . $_SERVER["PHP_SELF"] . '?deleteNewStock=1&deleteStockItemID=' . $thisDataKey . '" onclick="return showWarning(\'Soll dieser Eintrag wirklich entfernt werden?\');"><img src="layout/icons/iconDelete.png" width="16" height="16" alt="Entfernen" title="Diesen Eintrag entfernen" /></a></td>';
												echo '</tr>';
												$countRow++;
											}
										echo '</table>';

										echo '			</div>
													</div>
												</div>
											';
										echo '<div class="boxArea2">
												<div class="boxAreaContent">
													<h3>Versand '.$arrOrderCategoriesTypeDatas[$thisPartNumber]["orderCategoriesName"].'</h3>
													<div>
											';
										echo '<p><b>Verbrauch insgesamt:</b> ' . number_format($arrConsumptionQuantity[$thisPartNumber], 0, ',', '.') . '</p>' ;

										echo '<table border="0" class="border" cellpadding="0" cellspacing="0" width="100%">
												<tr>
													<th style="width:45px;text-align:right;">#</th>
													<th>Liefer-KW</th>
													<th>St&uuml;ckzahl</th>
												</tr>
											';

										if(!empty($arrConsumptionDatas[$thisPartNumber])) {
											$countRow = 1;
											$thisMarker = '';
											foreach($arrConsumptionDatas[$thisPartNumber] as $thisDataKey => $thisDataValue) {
												$thisRowTitle = substr($thisDataKey, 0, 4);
												if($thisMarker != $thisRowTitle){
													echo '<tr class="tableRowTitle1">';
													echo '<td colspan="3">';
													echo $thisRowTitle;
													echo '</td>';
													echo '</tr>';
													$thisMarker = $thisRowTitle;
												}

												if($countRow%2 == 0){ $rowClass = 'row0'; }
												else { $rowClass = 'row1'; }
												echo '<tr class="' . $rowClass . '">';
												echo '<td style="text-align:right;"><b>' . $countRow . '.</b> </td>';
												echo '<td style="text-align:center;">' . $thisDataKey . formatDate($arrConsumptionDatas[$thisPartNumber][$thisDataKey]["stockItemsDate"], 'display') . '</td>';
												echo '<td style="text-align:right; background-color:#FEFFAF;">' . number_format($arrConsumptionDatas[$thisPartNumber][$thisDataKey]["ordersProductMenge"], 0, ',', '.') . '</td>';
												echo '</tr>';
												$countRow++;
											}
										}

										echo '</table>';

										echo '			</div>
													</div>
												</div>
												<div class="clear"></div>
											</div>

											';
									echo '</td>';
									echo '</tr>';
									$count++;
								}
							}
						}
					}
					echo '</table>';
				}
			?>
		</div>

	</div>
</div>
</div>
</div>
<script language="javascript" type="text/javascript">
	$(document).ready(function() {
		$(function() {
			$.datepicker.setDefaults($.datepicker.regional[""]);
			$('#newStockDate').datepicker($.datepicker.regional["de"]);
			colorRowMouseOver('.displayOrders tbody tr');
			$('.buttonShopOrderInfo').css('cursor', 'pointer');
			$('.buttonShopOrderInfo').click(function () {
				loadOrderDetails($(this), '<?php echo BASEPATH; ?>', 'Details des Verbrauchs');
			});
		});
	});
</script>

<?php require_once('inc/footerHTML.inc.php'); ?>