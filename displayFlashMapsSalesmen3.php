<?php
	require_once('inc/requires.inc.php');

	if(!$arrGetUserRights["editSalesmen"] && !$arrGetUserRights["displaySalesmen"]) {
		#header('location: ' . PAGE_EXIT_LOCATION);
		#exit;
	}

	$jswindowMessage = '';
	$warningMessage = '';
	$errorMessage = '';
	$successMessage = '';
	$infoMessage = '';

	$dbConnection = new DB_Connection();
	$db_open = $dbConnection->db_connect();

	// BOF SET MAP TYPE
		$thisMapType = "FLASH"; // FLASH || GOOGLE
		if($_REQUEST["displayMapType"] != ""){
			$thisMapType = $_REQUEST["displayMapType"];
		}
		DEFINE("PLZ_MAP_TYPE", $thisMapType); // FLASH || GOOGLE
	// EOF SET MAP TYPE

	if(PLZ_MAP_TYPE == "FLASH"){
		$fileXML = PATH_XML_FILE_SALESMEN;
	}

	// BOF GET DOUBLE ZIP CODES
		$arrDoubleZipcodes = getDoubleZipcodes();
		$arrUnusedDoubleZipcodes = getDoubleZipcodes('0');
	// BOF GET DOUBLE ZIP CODES

	// BOF DEFINE AREA BG-COLORS
		$arrColorsPLZ = array(
				'#FFF200',
				'#FDB913',

				'#23AA4A',
				'#80C342',
				'#905501',
				'#F58220',
				'#E31D3C',
				'#F58F98',
				'#9A258F',
				'#0094C9',
				'#00C0E8',

				'#008060',

				#'#FFCCFF',
				#'#FFCC99',
				#'#CC6600',
				#'#99CCCC',
				#'#66FF33',
				#'#FFFF33',
				#'#9acd32',
				#'#ee82ee',
				#'#d2b48c',
				#'#66cdaa',
				#'#98fb98',
				#'#00bfff',
				#'#f0b488',
			);
	// EOF DEFINE AREA BG-COLORS
?>
<?php
	require_once('inc/headerHTML.inc.php');
	$thisTitle = 'Vertreter PLZ-Karte';
	$thisIcon = 'iconMapPLZ.png';
	$headerHTML = preg_replace("/{###TITLE###}/", strip_tags($thisTitle), $headerHTML);

	if(PLZ_MAP_TYPE == "GOOGLE"){
		$headerHTML = preg_replace('/<body/ism', '<body id="bodyFrame" onload="initialize();"', $headerHTML);
	}
	echo $headerHTML;

	require_once(FILE_MENUE_TOP);
?>

<div id="mainArea">
	<div id="mainContent">
		<?php require_once(FILE_MENUE_SIDEBAR); ?>
		<div id="contentArea">
			<div id="contentAreaElements">
				<h1><?php if(SHOW_PAGE_TITLE_ICONS) { echo '<img src="' . PATH_ICONS_MENUE_TITLES . $thisIcon . '" alt="" />'; } ?> <?php echo $thisTitle; ?></h1>

				<div id="searchFilterArea">
					<form name="formDisplayMapType" method="post" action="<?php echo $_SERVER["PHP_SELF"]; ?>">
						<table border="0" cellpadding="0" cellspacing="0" class="searchFilterContent">
							<tr>
								<td>
									<label for="displayMapType">Kartentyp:</label>
								</td>
								<td>
									<select name="displayMapType" id="displayMapType" class="inputField_200">
										<option value="FLASH" <?php if(PLZ_MAP_TYPE == "FLASH"){ echo ' selected="selected" '; } ?> >Flash-Karte</option>
										<option value="GOOGLE" <?php if(PLZ_MAP_TYPE == "GOOGLE"){ echo ' selected="selected" '; } ?> >Google-Karte</option>
									</select>
								</td>
								<td>
									<input type="submit" name="submitDisplayMapType" class="inputButton1" value="Karte anzeigen" />
								</td>
							</tr>
						</table>
					</form>
				</div>
				<?php
					if(!empty($arrUnusedDoubleZipcodes)){
						$arrTemp = array();
						foreach($arrUnusedDoubleZipcodes as $thisUnusedDoubleZipcodeKey => $thisUnusedDoubleZipcodeValue){
							$arrTemp[] = $thisUnusedDoubleZipcodeKey;
						}
						$unusedZipcodes = implode(', ', $arrTemp);
						?>
						<p class="infoArea">Folgende PLZ sind nicht in Gebrauch: <?php echo $unusedZipcodes; ?></p>
						<?php
					}
				?>

				<div class="contentDisplay">
					<?php if(PLZ_MAP_TYPE == "FLASH"){ ?>

					<?php
						// BOF GET SALESMEN WITH ZIPCODES
							$todayYear = date("Y");
							$todayMonth = date("m");
							$sql_getAreas = "SELECT
										`bctr_salesmen`.`salesmenID`,
										`bctr_salesmen`.`salesmenKundennummer`,
										`bctr_salesmen`.`salesmenFirmenname`,
										`bctr_salesmen`.`salesmenAreas`,

										`common_customers`.`customersID`

									FROM `bctr_salesmen`

									LEFT JOIN `common_customers`
									ON(`bctr_salesmen`.`salesmenKundennummer` = `common_customers`.`customersKundennummer`)

									WHERE 1
										AND `bctr_salesmen`.`salesmenAreas` != ''
										AND `bctr_salesmen`.`salesmenActive` = '1'
								";

							$rs_getAreas = $dbConnection->db_query($sql_getAreas);
							$arrSalesmenDatas = array();
							while($ds_getAreas = mysqli_fetch_assoc($rs_getAreas)){
								$arrAreas = explode(';', $ds_getAreas["salesmenAreas"]);
								if(!empty($arrAreas)){
									foreach($arrAreas as $thisArea) {
										if(preg_match("/-/", $thisArea)){
											$arrTemp = explode("-", $thisArea);
											#dd('arrTemp');
											for($i = $arrTemp[0] ; $i < ($arrTemp[1] + 1) ; $i++){
												$thisZipcode = $i;
												if($thisZipcode < 10){ $thisZipcode = '0' . $thisZipcode; }
												foreach(array_keys($ds_getAreas) as $field){
													$arrSalesmenDatas[$ds_getAreas["salesmenID"]][$thisZipcode][$field] = $ds_getAreas[$field];
												}
											}
										}
										else {
											foreach(array_keys($ds_getAreas) as $field){
												$arrSalesmenDatas[$ds_getAreas["salesmenID"]][$thisArea][$field] = $ds_getAreas[$field];
											}
										}
									}
								}
							}
						// EOF GET SALESMEN WITH ZIPCODES

						// BOF GET COUNT ITEMS PER PLZ DEPENDING ON SALESMAN
							$arrCountPlzItemsMonth = array();
							$strCountPlzItemsMonth = "";

							$arrCountPlzItemsYear = array();
							$strCountPlzItemsYear = "";

							// if($arrGetUserRights["displayOrderReceipts"]) {
							if(!1) {
								if(!empty($arrSalesmenDatas)){
									$sql_getOrders = "";
									$arrSql_getOrders = array();
									foreach($arrSalesmenDatas as $thisSalesmenDatasKey => $thisSalesmenDatasValue){
										foreach($thisSalesmenDatasValue as $thisSalesmanZipcodeKey => $thisSalesmanZipcodeValue){
											$arrSql_getOrders[] = "
												SELECT
													SUBSTRING(`" . TABLE_ORDER_CONFIRMATIONS. "`.`orderDocumentsAddressZipcode`, 1, 2) AS `areaCode`,
													`" . TABLE_ORDER_CONFIRMATIONS. "`.`orderDocumentsDocumentDate`,
													`" . TABLE_ORDER_CONFIRMATIONS. "`.`orderDocumentsSalesman`,
													SUM(`" . TABLE_ORDER_CONFIRMATIONS_DETAILS. "`.`orderDocumentDetailProductQuantity`) AS `countItems`,

													{###DATE_TYPE###}

													FROM `" . TABLE_ORDER_CONFIRMATIONS. "`
													LEFT JOIN `" . TABLE_ORDER_CONFIRMATIONS_DETAILS. "`
													ON(`" . TABLE_ORDER_CONFIRMATIONS. "`.`orderDocumentsID` = `" . TABLE_ORDER_CONFIRMATIONS_DETAILS. "`.`orderDocumentDetailDocumentID`)

													WHERE 1
														{###DATE_SELECTION###}
														AND `" . TABLE_ORDER_CONFIRMATIONS. "`.`orderDocumentsSalesman` = '" . $thisSalesmanZipcodeValue["customersID"] . "'
														AND SUBSTRING(`" . TABLE_ORDER_CONFIRMATIONS. "`.`orderDocumentsAddressZipcode`, 1, 2) = '" . $thisSalesmanZipcodeKey . "'
														AND SUBSTRING(`" . TABLE_ORDER_CONFIRMATIONS_DETAILS. "`.`orderDocumentDetailProductKategorieID`, 1, 3) = '001'
													GROUP BY CONCAT(`areaCode`, '#', `datetype`)
											";
										}
									}
									if(!empty($arrSql_getOrders)){
										$sql_getOrdersMonth = implode(' UNION ', $arrSql_getOrders);
										$sql_getOrdersMonth = preg_replace("/{###DATE_TYPE###}/", " 'year' AS `datetype` ", $sql_getOrdersMonth);
										$sql_getOrdersMonth = preg_replace("/{###DATE_SELECTION###}/", " AND SUBSTRING(`" . TABLE_ORDER_CONFIRMATIONS. "`.`orderDocumentsDocumentDate`, 1, 4) = '" . $todayYear . "' ", $sql_getOrdersMonth);

										$sql_getOrdersYear = implode(' UNION ', $arrSql_getOrders);
										$sql_getOrdersYear = preg_replace("/{###DATE_TYPE###}/", "'month' AS `datetype` ", $sql_getOrdersYear);
										$sql_getOrdersYear = preg_replace("/{###DATE_SELECTION###}/", " AND SUBSTRING(`" . TABLE_ORDER_CONFIRMATIONS. "`.`orderDocumentsDocumentDate`, 1, 7) = '" . $todayYear . "-" . $todayMonth . "' ", $sql_getOrdersYear);

										$sql_getOrders = $sql_getOrdersMonth . ' UNION ' . $sql_getOrdersYear;

										$rs_getOrders = $dbConnection->db_query($sql_getOrders);

										while($ds_getOrders = mysqli_fetch_assoc($rs_getOrders)){
											if($ds_getOrders["datetype"] == 'month'){
												$arrCountPlzItemsMonth[] = $ds_getOrders["areaCode"]. '#' . $ds_getOrders["countItems"];
											}
											else if($ds_getOrders["datetype"] == 'year'){
												$arrCountPlzItemsYear[] = $ds_getOrders["areaCode"]. '#' . $ds_getOrders["countItems"];
											}
										}
										$strCountPlzItemsYear = implode(';', $arrCountPlzItemsYear);
										$strCountPlzItemsMonth = implode(';', $arrCountPlzItemsMonth);
									}
									// AND SUBSTRING(`" . TABLE_ORDER_CONFIRMATIONS. "`.`orderDocumentsDocumentDate`, 1, 4) = '" . $todayYear . "'
								}
							}
							#dd('strCountPlzItemsYear');
							#dd('strCountPlzItemsMonth');
						// BOF GET COUNT ITEMS PER PLZ DEPENDING ON SALESMAN
					?>
					<div id="flashContent">
						<object classid="clsid:d27cdb6e-ae6d-11cf-96b8-444553540000" width="800" height="700" id="plz_karte" align="left">
							<?php

								$FlashVars = '';
								$FlashVars .= 'pathSalesmenXML=' . (PATH_XML_FILE_SALESMEN);
								$FlashVars .= '&amp;pathZipcodeXML=' . (FILE_XML_ZIPCODES_DOUBLE);
								$FlashVars .= '&amp;todayYear=' . ($todayYear);
								$FlashVars .= '&amp;todayMonth=' . ($todayMonth);

								if($strCountPlzItemsYear != ''){
									$FlashVars .= '&strCountPlzItemsYear=' . ($strCountPlzItemsYear);
								}
								if($strCountPlzItemsMonth != ''){
									$FlashVars .= '&strCountPlzItemsMonth=' . ($strCountPlzItemsMonth);
								}
							?>
							<param name="FlashVars" value="<?php echo $FlashVars; ?>" />
							<param name="movie" value="<?php echo FILE_SALESMEN_FLASH_MAP; ?>" />
							<param name="quality" value="best" />
							<param name="bgcolor" value="#ffffff" />
							<param name="play" value="true" />
							<param name="loop" value="true" />
							<param name="wmode" value="transparent" />
							<param name="scale" value="showall" />
							<param name="menu" value="false" />
							<param name="devicefont" value="false" />
							<param name="salign" value="" />
							<param name="allowScriptAccess" value="sameDomain" />
							<!--[if !IE]>-->
							<object type="application/x-shockwave-flash" data="<?php echo FILE_SALESMEN_FLASH_MAP; ?>" width="800" height="700" >
								<param name="FlashVars" value="<?php echo $FlashVars; ?>" />
								<param name="movie" value="<?php echo FILE_SALESMEN_FLASH_MAP; ?>" />
								<param name="quality" value="best" />
								<param name="bgcolor" value="#ffffff" />
								<param name="play" value="true" />
								<param name="loop" value="true" />
								<param name="wmode" value="transparent" />
								<param name="scale" value="showall" />
								<param name="menu" value="false" />
								<param name="devicefont" value="false" />
								<param name="salign" value="" />
								<param name="allowScriptAccess" value="sameDomain" />
							<!--<![endif]-->
								<a href="http://www.adobe.com/go/getflash">
									<img src="http://www.adobe.com/images/shared/download_buttons/get_flash_player.gif" alt="Get Adobe Flash player" />
								</a>
							<!--[if !IE]>-->
							</object>
							<!--<![endif]-->
						</object>
					</div>
					<?php } ?>

					<?php if(PLZ_MAP_TYPE == "GOOGLE"){ ?>
						<?php

							// BOF GET SALESMAN TO ZIPCODE
								$arrRelationSalesmenZipcodeDatas = getRelationSalesmenZipcode();

								if($_COOKIE["isAdmin"] == '1'){
									$arrRelationSalesmenZipcodeDatas = getRelationSalesmenZipcode2();
								}

								// BOF NEW SORT OF ZIPCODES
									$arrRelationSalesmenZipcodeDatasSorted = array();
									// BOF ADD BURHAN TO NON ADM-AREAS
										$xxxarrRelationSalesmenZipcodeDatasSorted["00000"]["00"] = array(
															"kundenname" => "BURHAN CTR e. K.",
															"kundennummer" => "48464",
															"kundenID" => 13301,
															"kundenPLZ" => "00",
															"kundenPLZaktiv" => 0
														);
									// EOF ADD BURHAN TO NON ADM-AREAS

									if(!empty($arrRelationSalesmenZipcodeDatas)){
										foreach($arrRelationSalesmenZipcodeDatas as $thisKey => $thisValue){
											$thisKeyNew = $thisKey . str_repeat("0", (5 - strlen($thisKey)));
											$arrRelationSalesmenZipcodeDatasSorted[$thisKeyNew][$thisKey] = $arrRelationSalesmenZipcodeDatas[$thisKey];
										}
										ksort($arrRelationSalesmenZipcodeDatasSorted);
									}
								// EOF NEW SORT OF ZIPCODES
							// EOF GET SALESMAN TO ZIPCODE

							// BOF SET ZIP CODE ARRAY DATA
								$arrZipCodes = array();
								$arrZipCodesPerLength = array();
								$arrZipCodesToSalesman = array();
								$arrSalesmanToZipCodes = array();
								function setZipCodes(){
									global $arrSalesmanToZipCodes, $arrZipCodes, $arrZipCodesPerLength, $arrZipCodesToSalesman, $arrRelationSalesmenZipcodeDatas;
									if(!empty($arrRelationSalesmenZipcodeDatas)){
										foreach($arrRelationSalesmenZipcodeDatas as $thisZipcode => $thisZipcodeData){
											foreach($thisZipcodeData as $thisSalesmanID => $thisValue){
												$arrZipCodes[] = $thisZipcode;
												$arrZipCodesPerLength[strlen($thisZipcode)][] = $thisZipcode;

												if($arrZipCodesToSalesman[$thisZipcode] != ""){
													$arrZipCodesToSalesman[$thisZipcode] .= "#" . $thisValue["kundenID"];
												}
												else {
													$arrZipCodesToSalesman[$thisZipcode] = $thisValue["kundenID"];
												}

												$arrSalesmanToZipCodes[$thisValue["kundenID"]] = $thisValue;
												// BOF ADD DOUBLE DIGIT ZIP CODE
													if(strlen($thisZipcode) > 2){
														$thisZipcodeDouble = substr($thisZipcode, 0, 2);
														$arrZipCodesPerLength[strlen($thisZipcodeDouble)][] = $thisZipcodeDouble;

														if($arrZipCodesToSalesman[$thisZipcodeDouble] != ""){
															$arrZipCodesToSalesman[$thisZipcodeDouble] .= "#" . $thisValue["kundenID"];
														}
														else {
															$arrZipCodesToSalesman[$thisZipcodeDouble] = $thisValue["kundenID"];
														}
													}
												// EOF ADD DOUBLE DIGIT ZIP CODE
											}
										}
										foreach($arrZipCodesPerLength as $thisKey => $thisValue){
											$arrZipCodesPerLength[$thisKey] = array_unique($arrZipCodesPerLength[$thisKey]);
											sort($arrZipCodesPerLength[$thisKey]);
										}

										/*
										foreach($arrZipCodesToSalesman as $thisKey => $thisValue){
											if(preg_match("/#/", $thisValue)){
												$arrTemp = explode("#", $thisValue);

												sort($arrTemp);
												$arrTemp = array_unique($arrTemp);
												$arrZipCodesToSalesman[$thisKey] = implode("#", $arrTemp);
												$tempCount = 0;
												foreach($arrTemp as $thisTempValue){
													$arrRelationSalesmenZipcodeDatas[$thisKey][$tempCount] = $arrSalesmanToZipCodes[$thisTempValue];
													$tempCount++;
												}
											}
										}
										*/
									}
								}
								setZipCodes();

							// EOF SET ZIP CODE ARRAY DATA

							// BOF SET SALESMAN DATA
								function getSalesmenData(){
									global $arrRelationSalesmenZipcodeDatas;



									$content = '';
									$content .= 'var arrSalesmanDatas = new Array();' . "\n";
									if(!empty($arrRelationSalesmenZipcodeDatas)){
										foreach($arrRelationSalesmenZipcodeDatas as $thisZipcode => $thisZipcodeData){

											// $content .= 'arrSalesmanDatas["' . $thisZipcode . '"] = new Array(); '. "\n";
											if(strlen($thisZipcode) == 2){
												foreach($thisZipcodeData as $thisSalesmanID => $thisSalesmanData){
													foreach($thisSalesmanData as $thisSalesmanDataKey => $thisSalesmanDataValue){
														$content .= 'arrSalesmanDatas["' . $thisZipcode . '"] = new Array();' . "\n";
														$content .= 'arrSalesmanDatas["' . $thisZipcode . '"]["' . $thisSalesmanID . '"] = new Array();' . "\n";
														$content .= 'arrSalesmanDatas["' . $thisZipcode . '"]["' . $thisSalesmanID . '"]["' . $thisSalesmanDataKey . '"] = "' . $thisSalesmanDataValue . '";' . "\n";
													}
												}
											}
										}
									}
									return $content;
								}
								$getSalesmenData = getSalesmenData();
							// EOF SET SALESMAN DATA

							// BOF GET GOOGLE MAPS COUNTRY AREAS
								function getCountryBordersData(){
									global $dbConnection;
									$arrGoogleMapCountryAreas = array();
									$sql = "SELECT
												`lat` AS `latitude`,
												`lon` AS `longitude`,
												`country`,
												`subcountry`,
												IF(`subcountry` != '' , CONCAT(`country`, '_', `subcountry`), `country`) AS `area`

												FROM `" . TABLE_GEODB_AREAS . "`

												WHERE 1
													AND `subcountry` = ''
													AND `country` = 'DE'
										";
									$rs = $dbConnection->db_query($sql);

									while($ds = mysqli_fetch_assoc($rs)) {
										$arrGoogleMapCountryAreas[$ds["area"]][] = 'new google.maps.LatLng(' . $ds["latitude"] . ', ' . $ds["longitude"] . ')';
									}

									// BOF COUNTRY BORDERS
										if(!empty($arrGoogleMapCountryAreas)){
											$content = '';

											$countItem = 0;
											foreach($arrGoogleMapCountryAreas as $thisKey => $thisValue) {
												$content .= 'arrMyCountryAreaPoints[' . $countItem . '] = [' . implode(", ", $thisValue) . '];';
												$countItem++;
											}
											$content .= '
													function showCountryBorders(map){
														if(arrMyCountryAreaPoints.length > 0){
															var arrMyCountryAreaPolygon = new Array();
															for(i = 0 ; i < arrMyCountryAreaPoints.length ; i++){
																arrMyCountryAreaPolygon[i] = new google.maps.Polygon({
																		paths: arrMyCountryAreaPoints[i],
																		strokeColor: "#00FF3A",
																		strokeOpacity: 0.8,
																		strokeWeight: 6,
																		fillColor: "transparent",
																		fillOpacity: 0.0
																		});
																arrMyCountryAreaPolygon[i].setMap(map);
															}
														}
													}
												';
										}
									// EOF COUNTRY BORDERS
									return $content;
								}
								$getCountryBordersData = getCountryBordersData();
							// EOF GET GOOGLE MAPS COUNTRY AREAS

							// BOF GET GOOGLE MAPS ZIPCODE AREAS
								function getGoogleZipcodeAreas(){
									global $dbConnection, $arrDoubleZipcodes, $arrColorsPLZ, $arrZipCodes, $arrZipCodesPerLength, $arrZipCodesToSalesman;

									// BOF USE NOT ONLY DOUBLE DIGIT ZIPCODES
										/*
										$arrSQL = array();

										if(!empty($arrZipCodesPerLength)){
											foreach($arrZipCodesPerLength as $thisKey => $thisValue){
												$thisKeyLength = $thisKey;
												if($thisKeyLength < 3){
													if($thisKeyLength == 2){
														$arrSQL[] = "
																SELECT
																	`zipcodeDoubleDigit`,
																	`coordinates`,
																	`zipcodeDoubleDigit` AS `salesmanAreaKey`

																	FROM `" . TABLE_GEODB_DOUBLEDIGIT_AREAS_NEW . "`

																	WHERE 1
																		AND (
																			`zipcodeDoubleDigit` = '" . implode("' OR `zipcodeDoubleDigit` = '", $thisValue) . "'
																		)
															";
													}
													else if($thisKeyLength == 1){
														$arrSQL[] = "
																SELECT
																	`zipcodeDoubleDigit`,
																	`coordinates`,
																	SUBSTRING(`zipcodeDoubleDigit`, 1, 1) AS `salesmanAreaKey`

																	FROM `" . TABLE_GEODB_DOUBLEDIGIT_AREAS_NEW . "`

																	WHERE 1
																		AND (
																			`zipcodeDoubleDigit` LIKE '" . implode("%' OR `zipcodeDoubleDigit` LIKE '", $thisValue) . "%'
																		)
															";
													}
												}
												else if($thisKeyLength < 6){
													if($thisKeyLength == 5){
														$arrSQL[] = "
																SELECT
																	`zipcode` AS `zipcodeDoubleDigit`,
																	`coordinates`,
																	`zipcode` AS `salesmanAreaKey`
																	FROM `" . TABLE_GEODB_ZIPCODE_AREAS . "`
																	WHERE 1
																		AND (
																			`zipcode` = '" . implode("' OR `zipcode` = '", $thisValue) . "'
																		)
															";
													}
													else if($thisKeyLength == 4){
														$arrSQL[] = "
																SELECT
																	`zipcode` AS `zipcodeDoubleDigit`,
																	`coordinates`,
																	SUBSTRING(`zipcode`, 1, 4) AS `salesmanAreaKey`
																	FROM `" . TABLE_GEODB_ZIPCODE_AREAS . "`
																	WHERE 1
																		AND (
																			`zipcode` LIKE '" . implode("%' OR `zipcode` LIKE '", $thisValue) . "%'
																		)
															";
													}
													else if($thisKeyLength == 3){
														$arrSQL[] = "
																SELECT
																	`zipcode` AS `zipcodeDoubleDigit`,
																	`coordinates`,
																	SUBSTRING(`zipcode`, 1, 3) AS `salesmanAreaKey`
																	FROM `" . TABLE_GEODB_ZIPCODE_AREAS . "`
																	WHERE 1
																		AND (
																			`zipcode` LIKE '" . implode("%' OR `zipcode` LIKE '", $thisValue) . "%'
																		)
															";
													}
												}
											}
											$arrZipcodesNotInUse = array();
											if(!empty($arrDoubleZipcodes)){
												foreach($arrDoubleZipcodes as $thisKey => $thisValue){
													if(!in_array($thisKey, array_keys($arrZipCodesToSalesman))){
														$arrZipcodesNotInUse[] = $thisKey;
													}
												}
											}
											if(!empty($arrZipcodesNotInUse)){
												$arrSQL[] = "
													SELECT
														`zipcodeDoubleDigit`,
														`coordinates`,
														'00' AS `salesmanAreaKey`

														FROM `" . TABLE_GEODB_DOUBLEDIGIT_AREAS_NEW . "`

														WHERE 1
															AND (
																`zipcodeDoubleDigit` = '" . implode("' OR `zipcodeDoubleDigit` = '", $arrZipcodesNotInUse) . "'
															)
												";
											}
										}
										*/
									// EOF USE NOT ONLY DOUBLE DIGIT ZIPCODES

									// BOF USE ONLY DOUBLE DIGIT ZIPCODES
										$arrSQL = array();
										if(!empty($arrZipCodesPerLength[2])){
											$arrSearchZipCodeAreas = $arrZipCodesPerLength[2];
											$arrSQL[] = "
													SELECT
														`zipcodeDoubleDigit`,
														`coordinates`,
														`zipcodeDoubleDigit` AS `salesmanAreaKey`

														FROM `" . TABLE_GEODB_DOUBLEDIGIT_AREAS_NEW . "`

														WHERE 1
															AND (
																`zipcodeDoubleDigit` = '" . implode("' OR `zipcodeDoubleDigit` = '", $arrSearchZipCodeAreas) . "'
															)
												";

											$arrZipcodesNotInUse = array();
											if(!empty($arrDoubleZipcodes)){
												foreach($arrDoubleZipcodes as $thisKey => $thisValue){
													if(!in_array($thisKey, array_keys($arrZipCodesToSalesman))){
														$arrZipcodesNotInUse[] = $thisKey;
													}
												}
											}
											if(!empty($arrZipcodesNotInUse)){
												$arrSQL[] = "
													SELECT
														`zipcodeDoubleDigit`,
														`coordinates`,
														'00' AS `salesmanAreaKey`

														FROM `" . TABLE_GEODB_DOUBLEDIGIT_AREAS_NEW . "`

														WHERE 1
															AND (
																`zipcodeDoubleDigit` = '" . implode("' OR `zipcodeDoubleDigit` = '", $arrZipcodesNotInUse) . "'
															)
												";
											}
										}
									// EOF USE ONLY DOUBLE DIGIT ZIPCODES

									if(!empty($arrSQL)){
										$sql_searchZipcodeAreas = implode(" UNION ", $arrSQL);

										$rs_searchZipcodeAreas = $dbConnection->db_query($sql_searchZipcodeAreas);
										$arrGoogleMapDoubleDigitZipCodeAreas = array();

										while($ds_searchZipcodeAreas = mysqli_fetch_assoc($rs_searchZipcodeAreas)) {
											if($ds_searchZipcodeAreas["boundaryType"] != 'innerBoundaryIs'){

											}

											$thisCoordinates = $ds_searchZipcodeAreas["coordinates"];
											$thisSalesmanID = $arrZipCodesToSalesman[$ds_searchZipcodeAreas["salesmanAreaKey"]];


											if($thisSalesmanID == ''){
												$thisSalesmanID = "PLZ_NOT_IN_USE";
											}

											if(strlen($ds_searchZipcodeAreas["zipcodeDoubleDigit"]) < 3){
												$searchPattern = "/(\[)([0-9\.\-]*),([0-9\.\-]*)(\])/ismU";
												$replace = "new google.maps.LatLng($2, $3)";
											}
											else if(strlen($ds_searchZipcodeAreas["zipcodeDoubleDigit"]) > 2){
												$searchPattern = "/(\[)([0-9\.\-]*),([0-9\.\-]*)(\])/ismU";
												$replace = "new google.maps.LatLng($3, $2)";
											}
											$thisCoordinates = preg_replace($searchPattern, $replace, $thisCoordinates);
											#$arrGoogleMapDoubleDigitZipCodeAreas[$thisSalesmanID][$ds_searchZipcodeAreas["zipcodeDoubleDigit"]] = $thisCoordinates;
											$arrGoogleMapDoubleDigitZipCodeAreas[$thisSalesmanID][$ds_searchZipcodeAreas["zipcodeDoubleDigit"]][] = $thisCoordinates;
										}
										$arrGoogleMapZipCodeAreas = $arrGoogleMapDoubleDigitZipCodeAreas;
									}

									$content = '';

									if(!empty($arrGoogleMapZipCodeAreas)){
										/*
										foreach($arrGoogleMapZipCodeAreas as $thisSalesmanID => $thisSalesmanGoogleMapZipCodeAreas) {
											$content .= 'arrMyZipcodeAreaPoints["' . $thisSalesmanID . '"] = new Array();';
											$countItem = 0;
											foreach($thisSalesmanGoogleMapZipCodeAreas as $thisKey => $thisValue) {
												#$content .= 'arrMyZipcodeAreaPoints[' . $thisSalesmanID . '][' . $countItem . '] = [' . $thisValue . '];';
												#$content .= 'arrMyZipcodeAreaPoints[' . $countItem . '] = [' . $thisValue . '];';

												$content .= 'arrMyZipcodeAreaPoints["' . $thisSalesmanID . '"]["' . $thisKey . '"] = [' . $thisValue . '];';
												$countItem++;
											}
										}
										*/
										foreach($arrGoogleMapZipCodeAreas as $thisSalesmanID => $thisSalesmanGoogleMapZipCodeAreas) {
											$content .= 'arrMyZipcodeAreaPoints["' . $thisSalesmanID . '"] = new Array();';
											foreach($thisSalesmanGoogleMapZipCodeAreas as $thisZipcode => $thisGoogleMapZipCodeAreas){
												$content .= 'arrMyZipcodeAreaPoints["' . $thisSalesmanID . '"]["' . $thisZipcode . '"] = new Array();';
												$countItem = 0;
												foreach($thisGoogleMapZipCodeAreas as $thisGoogleMapZipCodeAreaKey => $thisGoogleMapZipCodeAreaData){
													$content .= 'arrMyZipcodeAreaPoints["' . $thisSalesmanID . '"]["' . $thisZipcode . '"]["' . $countItem . '"] = [' . $thisGoogleMapZipCodeAreaData . '];';
													$countItem++;
												}
											}
										}

										$content .= '
												function getColors(index){
													var getColor = "";
													var arrColors = new Array("' . implode('", "', $arrColorsPLZ) . '");
													getColor = arrColors[index];

													return getColor;
												}

												function hexToRgb(hex) {
													// Expand shorthand form (e.g. "03F") to full form (e.g. "0033FF")
													var shorthandRegex = /^#?([a-f\d])([a-f\d])([a-f\d])$/i;
													hex = hex.replace(shorthandRegex, function(m, r, g, b) {
														return r + r + g + g + b + b;
													});

													var result = /^#?([a-f\d]{2})([a-f\d]{2})([a-f\d]{2})$/i.exec(hex);
													return result ? {
														r: parseInt(result[1], 16),
														g: parseInt(result[2], 16),
														b: parseInt(result[3], 16)
													} : null;
												}

												function greyoutWorld(map) {
													var world_geometry = new google.maps.FusionTablesLayer({
														query: {
															select: "geometry",
															from: "1N2LBk4JHwWpOY4d9fobIn27lfnZ5MDy-NoqqRpk",
															where: "ISO_2DIGIT NOT IN (\'DE\',\'DK\',\'SE\')"
														},
														//add some grey color to cover the rest of the world
														styles: [{
															polygonOptions: {
																fillColor: "#000000",
																fillOpacity: 0.1,
																strokeColor: "transparent",
																strokeWeight: 0.1,
																strokeOpacity: 0.1

															},
															polylineOptions: {
																strokeColor: "transparent",
																strokeWeight: 0.1,
																strokeOpacity: 0.1
															}
														}],
														map: map,
														suppressInfoWindows: true
													});
												}

												function showZipCodeAreas(map){
													var countSalesmen = 0;
													var countPolygon = 0;
													var zipcodeAreaFillOpacity = 1;

													if(arrMyZipcodeAreaPoints.length > 0){
														var arrMyZipcodeAreaPolygon = new Array();
														for(i in arrMyZipcodeAreaPoints){
															var thisColor = getColors(countSalesmen);
															if(i == "PLZ_NOT_IN_USE") {
																thisColor = "#FFFFFF";
															}

															thisColorObjRGB = hexToRgb(thisColor);
															thisColorRGB = "rgb(" + thisColorObjRGB.r + ", " + thisColorObjRGB.g + ", " + thisColorObjRGB.b + ")";
															thisBorderColorRGB = "rgb(" + thisColorObjRGB.r + ", " + thisColorObjRGB.g + ", " + thisColorObjRGB.b + ")";
															thisColorRGBA = "rgb(" + thisColorObjRGB.r + ", " + thisColorObjRGB.g + ", " + thisColorObjRGB.b + ", " + zipcodeAreaFillOpacity + ")";

															/*
															if(arrSalesmanDatas[i]){
																$("." + arrSalesmanDatas[i]["kundenID"]).css("background-color", (thisColorRGBA));
															}
															*/

															if(i == "PLZ_NOT_IN_USE") {
																thisColor = "#FFFFFF";
																// thisColor = "linear-gradient(to left, #FFFFFF, #000000)";
																// thisColor = "linear-gradient(to top, white 0%, black 50%)";
																// thisColor = "background:linear-gradient(45deg, white, black)";
															}
															arrMyZipcodeAreaPolygon[i] = new Array();
															// for(k = 0 ; k < arrMyZipcodeAreaPoints[i].length ; k++){
															for(k in arrMyZipcodeAreaPoints[i]){
																$(".PLZ_" + k + " .sideInfoItem:first").css("background-color", (thisColorRGBA));
																$(".PLZ_" + k).css("border-left", "10px solid " + thisBorderColorRGB);
																for(r in arrMyZipcodeAreaPoints[i][k]){

																	infWindowKey = i+"_"+k+"_"+r;

																	// BOF GET CENTER OF POLYGON
																		var bounds = new google.maps.LatLngBounds();
																		for (c = 0; c < arrMyZipcodeAreaPoints[i][k][r].length; c++) {
																			bounds.extend(arrMyZipcodeAreaPoints[i][k][r][c]);
																		}
																		var thisPolygonCenter = bounds.getCenter();
																	// EOF GET CENTER OF POLYGON

																	// DEFINE INFO WINDOW
																		var html		= new Array();
																		var imageExists	= false;
																		html[infWindowKey] = "";

																		html[infWindowKey] += "<div class=\"myGmapInfo\">";
																		// html[infWindowKey] += "  <p class=\"headline\">" + "PLZ-Gebiet: " + arrSalesmanDatas[i]["kundenPLZ"] + "</p>";
																		html[infWindowKey] += "  <p class=\"headline\">" + "PLZ-Gebiet: " + k + "</p>";

																		if(arrSalesmanDatas[i]){
																			html[infWindowKey] += "<table border=\"0\" width=\"\" cellpadding=\"0\" cellspacing=\"0\">";
																			html[infWindowKey] += "  <tr><td><b>ADM/Vertreter:</b></td><td>" + arrSalesmanDatas[i]["kundenname"] + "</td></tr>";
																			// html[infWindowKey] += "  <tr><td><b>PLZ-Gebiet:</b></td><td>" + arrSalesmanDatas[i]["kundenPLZ"] + "</td></tr>";
																			if(arrDoubleZipcodeDescriptions[k]){
																				html[infWindowKey] += "  <tr><td><b>PLZ-Gebiet:</b></td><td>" + k + "<br>" + arrDoubleZipcodeDescriptions[k] + "</td></tr>";
																			}

																			html[infWindowKey] += "  <tr><td><b>KNR:</b></td><td>" + arrSalesmanDatas[i]["kundennummer"] + "</td></tr>";
																			// html[infWindowKey] += "  <tr><td><b>ID:</b></td><td>" + arrSalesmanDatas[i]["kundenID"] + "</td></tr>";

																			html[infWindowKey] += "</table>";
																			html[infWindowKey] += "<a href=\"' . PAGE_EDIT_CUSTOMER . '?searchBoxCustomer=" + arrSalesmanDatas[i]["kundennummer"] + "\" class=\"linkButton\">Verteter-Details anzeigen</a>";
																			infoWindowArea = new google.maps.InfoWindow({
																				content: "lade....."
																			});
																		}
																		else if(i.match(/#/)){

																			/*
																			var arrTemp = i.split("#");

																			html[infWindowKey] += "<p><b>Aufgeteiltes Gebiet</b><p>";
																			html[infWindowKey] += "<table border=\"0\" width=\"\" cellpadding=\"0\" cellspacing=\"0\">";
																			if(arrDoubleZipcodeDescriptions[i]){
																				// html[infWindowKey] += "  <tr><td><b>PLZ-Gebiet:</b></td><td>" + i + "<br>" + arrDoubleZipcodeDescriptions[i] + "</td></tr>";
																			}

																			html[infWindowKey] += "  <tr><td><b>ADM/Vertreter:</b></td>";
																			html[infWindowKey] += "  <td>";
																			for(arrTempKey = 0 ; arrTempKey < arrTemp.length ; arrTempKey++){
																				html[infWindowKey] += arrSalesmanDatas[arrTemp[arrTempKey]]["kundenname"];
																				html[infWindowKey] += "<br />";
																				html[infWindowKey] += " <a href=\"' . PAGE_EDIT_CUSTOMER . '?searchBoxCustomer=" + arrSalesmanDatas[arrTemp[arrTempKey]]["kundennummer"] + "\" class=\"linkButton\">Verteter-Details anzeigen</a>";
																				html[infWindowKey] += "<br />";
																			}
																			html[infWindowKey] += "  </td></tr>"
																			html[infWindowKey] += "</table>";
																			*/
																		}
																		else {
																			html[infWindowKey] += "<table border=\"0\" width=\"\" cellpadding=\"0\" cellspacing=\"0\">";
																			html[infWindowKey] += "  <tr><td><b>ADM/Vertreter:</b></td><td>NICHT BESETZT</td></tr>";
																			html[infWindowKey] += "  <tr><td><b>PLZ-Gebiet:</b></td><td>" + k + "<br>" + arrDoubleZipcodeDescriptions[k] + "</td></tr>";
																			html[infWindowKey] += "</table>";
																		}
																		html[infWindowKey] += "<div class=\"clear\"</div>";
																		html[infWindowKey] += "</div>";
																	// END DEFINE INFO WINDOW

																	// BOF SET MARKER IN CENTER OF POLYGON
																		/*
																		var thisMarkerImage;
																		thisMarkerImage = "googleMapsIcon_" + count + ".png";
																		thisMarkerShadowImage = "googleMapsIconShadow.png";
																		thisMarkerSize = new Array(28, 20);
																		thisMarkerShadowSize = new Array(28, 20);
																		thisZindex = 0;

																		var image = new google.maps.MarkerImage(
																					"layout/icons/" + thisMarkerImage,
																					new google.maps.Size(thisMarkerSize[0], thisMarkerSize[1]),
																					new google.maps.Point(0,0),
																					new google.maps.Point(0, thisMarkerSize[1])
																		);

																		var shadow = new google.maps.MarkerImage(
																					"layout/icons/" + thisMarkerShadowImage,
																					new google.maps.Size(thisMarkerShadowSize[0], thisMarkerShadowSize[1]),
																					new google.maps.Point(0, 0),
																					new google.maps.Point(0, thisMarkerShadowSize[1])
																		);

																		var shape = {
																			coord: [1, 1, 1, 28, 20, 28, 20 , 1],
																			type: "poly"
																		};

																		var myLatLng = thisPolygonCenter;

																		markerPolygonCenter[infWindowKey] = new google.maps.Marker({
																			position: myLatLng,
																			map: map,
																			shadow: shadow,
																			icon: image,
																			shape: shape,
																			title: "PLZ-Gebiet: " + arrSalesmanDatas[i]["kundenPLZ"] + " | ADM: " + arrSalesmanDatas[i]["kundenname"],
																			zIndex: thisZindex,
																			html: html[infWindowKey],
																			polCenter: thisPolygonCenter
																		});

																		markerPolygonCenterShadow[infWindowKey] = new google.maps.Marker({
																			position: myLatLng,
																			map: map,
																			icon: shadow,
																			shape: shape,
																			zIndex: thisZindex
																		});
																		*/
																	// EOF SET MARKER IN CENTER OF POLYGON

																	// BOF SET POLYGON AREA
																		var strokeColor = "transparent";
																		var strokeWeight = 0.1;
																		if(k.length < 3){
																			strokeColor = "#333333";
																			strokeWeight = 1;
																		}
																		arrMyZipcodeAreaPolygon[infWindowKey] = new google.maps.Polygon({
																				paths: arrMyZipcodeAreaPoints[i][k][r],
																				strokeColor: strokeColor,
																				strokeOpacity: zipcodeAreaFillOpacity,
																				strokeWeight: strokeWeight,
																				fillColor: thisColor,
																				fillOpacity: zipcodeAreaFillOpacity,
																				zIndex:countPolygon,
																				html: html[infWindowKey],
																				polCenter: thisPolygonCenter
																			});
																		arrMyZipcodeAreaPolygon[infWindowKey].setMap(map);
																	// EOF SET POLYGON AREA

																	// BOF DEFINE MAP LABEL
																		if(k.length < 3){
																			var mapLabel = new MapLabel({
																				text: k,
																				position: thisPolygonCenter,
																				map: map,
																				fontSize: 12,
																				fontFamily: "Arial",
																				fontWeight: "bold",
																				align: "left",
																				fontColor: "#000000",
																				zIndex: (countPolygon + 1),
																				strokeColor: "#CCCCCC",
																				strokeWeight: 2
																			});
																			mapLabel.set("position", thisPolygonCenter);
																		}
																	// EOF DEFINE MAP LABEL

																	// SHOW INFO WINDOW
																		google.maps.event.addListener(arrMyZipcodeAreaPolygon[infWindowKey], "click", function() {
																			try{
																				if(infoWindowArea){
																					infoWindowArea.close();
																					infoWindowArea.setContent(this.html);
																					infoWindowArea.setPosition(this.polCenter);
																					infoWindowArea.open(map);
																				}
																			}
																			catch(e){
																				alert("error: " + e + " | i: " + i + " | k: " + k);
																			}
																		});

																		// createMarkerLinkInGmapSidebar(i, myPointDatas[i][k]["title"], marker[infWindowKey], imageExists);
																	// END SHOW INFO WINDOW

																}
															}
															if(i != "PLZ_NOT_IN_USE") {
																countSalesmen++;
															}
														}
														countPolygon++;
													}
												}
											';
									}
									return $content;
								}
								$getGoogleZipcodeAreasData = getGoogleZipcodeAreas();

							// EOF GET GOOGLE MAPS ZIPCODE AREAS

							// BOF GET ZIPCODE AEREA DESCRIPTION
								function getGoogleZipcodeAreaDescriptions(){
									global $arrDoubleZipcodes;
									$content = '';
									$content .= ' var arrDoubleZipcodeDescriptions = new Array();';

									if(!empty($arrDoubleZipcodes)){
										foreach($arrDoubleZipcodes as $thisKey => $thisValue){
											$content .= 'arrDoubleZipcodeDescriptions["' . $thisKey . '"] = "' . $arrDoubleZipcodes[$thisKey]["zipcodesDoubleDescription"] . '";';
										}
									}
									return $content;
								}
								$getGoogleZipcodeAreaDescriptions = getGoogleZipcodeAreaDescriptions();
							// EOF GET ZIPCODE AEREA DESCRIPTION
						?>
						<div id="myGoogleMapsArea" style="width:810px;">
							<div id="myGoogleMapCanvas">
								<span class="loadingArea"><img class="loader" src="layout/ajax-loader.gif" /> Bitte haben Sie ein wenig Geduld. Die Daten werden geladen...</span>
							</div>
							<div id="myGoogleMapInfoArea" style="width:200px;float:right;">
								<?php
									if(!empty($arrRelationSalesmenZipcodeDatas)){
										echo '<div class="xxxsideInfo" style="position:relative;">';
											echo '<div class="sideInfoHeader">PLZ | Au&szlig;endienst-Mitarbeiter</div>';
											echo '<div class="sideInfoContent">';
											$count = 0;

											$marker = '';

											foreach($arrRelationSalesmenZipcodeDatasSorted as $arrRelationSalesmenZipcodeDatas){
												foreach($arrRelationSalesmenZipcodeDatas as $thisZipcode => $thisZipcodeData){
													echo '<div class="PLZ_' . substr($thisZipcode, 0, 2) . '" style="margin:0;padding:0">';
													foreach($thisZipcodeData as $thisKey => $thisValue){
														if($count%2 == 0){ $rowClass = 'row0'; }
														else { $rowClass = 'row1'; }

														$thisStyle = '';
														if($marker != $thisZipcode){
															#$thisStyle = 'border-top:2px solid #F00';
															$marker = $thisZipcode;
															echo '<div class="sideInfoItem" style="font-weight:bold;letter-spacing:1px;background-color:#F5FF9F">';
																echo '<div class="sideInfoTitle">';
																echo 'PLZ-Gebiet ' . $thisZipcode;
																echo '</div>';
																echo '<div class="clear"></div>';
															echo '</div>';
														}

														echo '<div style="' . $thisStyle . '" class="sideInfoItem ' . $rowClass . ' KID_' . $thisValue["kundenID"] . '" title="' . $thisValue["kundenID"] . '" >';
															echo '<div class="sideInfoTitle" style="float:left;width:40px;">&bull; ' . $thisZipcode . '</div>';
															echo '<div class="sideInfoText" style="white-space:nowrap;font-weight:bold;" title="' . $arrDoubleZipcodes[substr($thisZipcode, 0, 2)]["zipcodesDoubleDescription"] . '">' . $thisValue["kundenname"] . '</div>';
															echo '<div class="clear"></div>';
														echo '</div>';
														$count++;
													}
													echo '</div>';
												}
											}
											echo '</div>';
										echo '</div>';
									}
								?>
							</div>
							<div class="clear"></div>
						</div>
						<script language="javascript" type="text/javascript">
							<!--
							/* <![CDATA[ */
							$(document).ready(function() {
								$('#myGoogleMapsArea').css({
									'width': '100%',
									'white-space': 'nowrap'
								});
								$('#myGoogleMapInfoArea').css({
									'width': '18%',
									'min-width': '200px',
									'border': '1px solid #333'
								});

								$('#myGoogleMapCanvas').css({
									'float': 'left',
									'min-width': '600px',
									'width': '80%',
									'height': ($(window).height() - 140) + 'px'
								});
								$('.sideInfoContent').css({
									'height': ($(window).height() - 150) + 'px'
								});
							});
							/* ]]> */
							// -->
						</script>

						<!-- GOOGLE MAPS START -->
						<script language="javascript" type="text/javascript" src="<?php echo PATH_GOOGLE_MAPS_API; ?>"></script>
						<script language="javascript" type="text/javascript" src="<?php echo FILE_GOOGLE_MAP_LABEL_JS; ?>"></script>
						<script language="javascript" type="text/javascript">
							<!--
							/* <![CDATA[ */
							var elementId = "myGoogleMapCanvas";
							function initialize() {
								if (!document.getElementById(elementId)) {
									alert("Fehler: das Element mit der id " + elementId + " konnte nicht auf dieser Webseite gefunden werden!");
									return false;
								}
								else {
									/* BOF DEFINE DEFAULT VALUES */
										var myPointDatas	= new Array();


										var default_arrayKey	= 0;
										//var default_lat			= "52.103138";
										//var default_lon			= "7.622817";

										var default_lat			= "51.182568";
										var default_lon			= "10.695819";
										var default_zoom		= 6;

										var sidebarHtml		= "";              			/* HTML-Code für die gesamte Sidebar */
										var sidebarMarkers	= [];              			/* Array für die Marker */
										var sidebarCount	= 0;               			/* Zähler für die Marker (= Zeile in der Sidebar) */
										var marker				= new Array();
										var markerPolygonCenter	= new Array();
										var markerPolygonCenterShadow	= new Array();
										var activeMarker	= "";
										var activeMarkerPolygonCenter	= "";
										var infoWindowPoint			= new Array();
										infoWindowPolygon		= new Array();
										var infoWindowArea			= new Array();
										activeMarkerPolygon = "";

										var arrMyCountryAreaPoints = new Array();
										var arrMyZipcodeAreaPoints = new Array();
									/* EOF DEFINE DEFAULT VALUES */

									/* BOF DEFINE POINT DATAS */
										myPointDatas[0] = new Array();
										myPointDatas[0][0] = new Array();
										myPointDatas[0][0]["latitude"] = default_lat;
										myPointDatas[0][0]["longitude"] = default_lon;
										myPointDatas[0][0]["title"] = "BURHAN CTR";
										myPointDatas[0][0]["header"] = "BURHAN CTR";
										myPointDatas[0][0]["text"] = "";
										myPointDatas[0][0]["city"] = "";
										myPointDatas[0][0]["region_1"] = "";
										myPointDatas[0][0]["region_2"] = "";
										myPointDatas[0][0]["region_3"] = "";
										myPointDatas[0][0]["imagePath"] = "";
									/* EOF DEFINE POINT DATAS */

									var latlng = new google.maps.LatLng(myPointDatas[default_arrayKey][default_arrayKey]["latitude"], myPointDatas[default_arrayKey][default_arrayKey]["longitude"]);

									var myOptions = {
										zoom: default_zoom,
										center: latlng,
										panControl: true,
										zoomControl: true,
										navigationControl: true,
										mapTypeControl: true,
										scaleControl: true,
										overviewMapControl: true,
										streetViewControl: true,
										mapTypeId: google.maps.MapTypeId.TERRAIN
										/* ROADMAP zeigt die normalen Standard-2D-Kacheln von Google Maps an. */
										/* SATELLITE zeigt Fotokacheln an. */
										/* HYBRID zeigt eine Mischung aus Fotokacheln und eine Kachelebene für markante Merkmale an (Straßen, Ortsnamen). */
										/* TERRAIN zeigt Reliefkacheln für Erhebungen und GewÃ¤sser (Berge, Flüsse usw.) an. */
									};
									var map = new google.maps.Map(document.getElementById(elementId), myOptions);

									<?php echo $getSalesmenData; ?>

									<?php echo $getCountryBordersData; ?>
									if(arrMyCountryAreaPoints.length > 0){
										showCountryBorders(map);
									}

									<?php echo $getGoogleZipcodeAreaDescriptions; ?>
									<?php echo $getGoogleZipcodeAreasData; ?>
									if(arrMyZipcodeAreaPoints.length > 0){
										showZipCodeAreas(map);
									}
									// greyoutWorld(map);
									// BOF SET POINTS
										function setGmapMarkers(map, i) {
											var thisMarkerImage;
											thisMarkerImage = 'bctr_googleMarkerFlag.png';
											thisMarkerShadowImage = 'bctr_googleMarkerFlagShadow.png';
											thisMarkerSize = new Array(81, 84);
											thisMarkerShadowSize = new Array(81, 84);
											// thisZindex = (myPointDatas.length);
											thisZindex = 0;

											var image = new google.maps.MarkerImage(
														'layout/icons/' + thisMarkerImage,
														new google.maps.Size(thisMarkerSize[0], thisMarkerSize[1]),
														new google.maps.Point(0,0),
														new google.maps.Point(0, thisMarkerSize[1])
											);

											var shadow = new google.maps.MarkerImage(
															'layout/icons/' + thisMarkerShadowImage,
															new google.maps.Size(thisMarkerShadowSize[0], thisMarkerShadowSize[1]),
															new google.maps.Point(0, 0),
															new google.maps.Point(0, thisMarkerShadowSize[1])
											);

											var shape = {
												// coord: [1, (thisMarkerSize[1] + 1 - 20), 1, thisMarkerSize[1], thisMarkerSize[0], thisMarkerSize[1], thisMarkerSize[0] , (thisMarkerSize[1] + 1 - 20)],
												coord: [1, 1, 1, 28, 20, 28, 20 , 1],
												type: 'poly'
											};

											var myLatLng = new google.maps.LatLng(myPointDatas[i]["latitude"], myPointDatas[i]["longitude"]);
											marker[i] = new google.maps.Marker({
												position: myLatLng,
												map: map,
												shadow: shadow,
												icon: image,
												shape: shape,
												title: myPointDatas[i]["title"],
												zIndex: thisZindex
											});

											shadow[i] = new google.maps.Marker({
												position: myLatLng,
												map: map,
												icon: shadow,
												shape: shape,
												title: myPointDatas[i]["title"],
												zIndex: thisZindex
											});

											// DEFINE INFO WINDOW
												/*
												var html		= new Array();
												var imageExists	= false;
												html[i] = "";
												html[i] += "<div class='myGmapInfo'>";
												// html[i] += "  <p class='headline'>" + myPointDatas[i]["title"] + "</p>";
												html[i] += "  <p class='headline'>" + myPointDatas[i]["header"] + "</p>";

												if(myPointDatas[i]["imagePath"] != "") {
												  imageExists = true;
												  html[i] += '  <img src="' + myPointDatas[i]["imagePath"] + '" + myPointDatas[i]["imageDimension"] + alt="' + myPointDatas[i]["title"] + '"/>';
												}

												html[i] += '<table border="0" width="" cellpadding="0" cellspacing="0">';

												if(myPointDatas[i]["text"] != "") {
												  // html[i] += "  <p class='text'>" + myPointDatas[i]["text"].replace(/, /g, ",<br />") + "</p>";
												  html[i] += "  <tr><td><b>Vertreter:</b></td><td>" + myPointDatas[i]["text"] + "</td></tr>";
												}

												if(myPointDatas[i]["city"] != "") {
												  // html[i] += "  <tr><td><b>PLZ:</b></td><td>" + myPointDatas[i]["city"] + "</td></tr>";
												}

												if(myPointDatas[i]["region_1"] != "") {
												  html[i] += "  <tr><td><b>Bundesland:</b></td><td>" + myPointDatas[i]["region_1"] + "</td></tr>";
												}

												if(myPointDatas[i]["region_2"] != "") {
												  html[i] += "  <tr><td><b>Region:</b></td><td>" + myPointDatas[i]["region_2"] + "</td></tr>";
												}

												if(myPointDatas[i]["region_3"] != "") {
												  html[i] += "  <tr><td><b>Kreis:</b></td><td>" + myPointDatas[i]["region_3"] + "</td></tr>";
												}

												html[i] += "  <tr><td><b>Longitude:</b></td><td>" + myPointDatas[i]["longitude"] + "</td></tr>";
												html[i] += "  <tr><td><b>Latitude: </b></td><td>" + myPointDatas[i]["latitude"] + "</td></tr>";

												html[i] += '</table>';

												html[i] += "<div class='clear'></div>";
												html[i] += "</div>";

												infowindow[i] = new google.maps.InfoWindow({
													content: html[i]
												});
												*/
											// END DEFINE INFO WINDOW

											// SHOW INFO WINDOW
												/*
												google.maps.event.addListener(marker[i], 'click', function() {
													if(activeMarker > -1) {
														infowindow[activeMarker].close();
													}
													infowindow[i].open(map,marker[i]);
													activeMarker = i;
												});
												createMarkerLinkInGmapSidebar(i, myPointDatas[i]["title"], marker[i], imageExists);
												*/
											// END SHOW INFO WINDOW
										}
										for (i = 0 ; i < myPointDatas.length ; i++) {
											if(myPointDatas[i]){
												setGmapMarkers(map, i);
											}
										}
									// EOF SET POINTS
								}
							}
							/* ]]> */
							// -->
						</script>
						<!-- GOOGLE MAPS END -->
					<?php } ?>
				</div>
			</div>
		</div>
	</div>
</div>

<script language="javascript" type="text/javascript">
	<!--
	/* <![CDATA[ */
	$(document).ready(function() {
		<?php if($_REQUEST["editID"] != '' && $_REQUEST["editID"] != 'NEW'){ ?>
		<?php if(PLZ_MAP_TYPE == "GOOGLE"){ ?>
		$('body').attr('onload', 'initialize();');
		// google.maps.event.addDomListener(window, 'load', initialize);
		<?php } ?>
		<?php } ?>

		$('.buttonLoadMap').click(function() {
			loadGoogleMapSalesmen('4');
		});
		<?php if($_REQUEST["openMap"] == true){ ?>
			loadGoogleMapSalesmen('4');
		<?php } ?>

		<?php if($_REQUEST["editID"] != "NEW"){ ?>
		$(function() {
			$('#tabs').tabs();
		});
		<?php } ?>
		colorRowMouseOver('.displayOrders tbody tr');
		$('.buttonNotice').click(function () {
			loadNotice($(this));
		});
		setFormDutyFields(<?php echo $jsonFormDutyFields; ?>);

	});
	/* ]]> */
	// -->
</script>

<?php require_once('inc/footerHTML.inc.php'); ?>