<?php

	require_once('inc/requires.inc.php');

	DEFINE('TABLE_CUSTOMERS_TEST', '_test_customers');
	$thisDebug = false; // true|false
	if($thisDebug){
		DEFINE('TABLE_CUSTOMERS_USE', TABLE_CUSTOMERS_TEST);
	}
	else if(!$thisDebug){
		DEFINE('TABLE_CUSTOMERS_USE', TABLE_CUSTOMERS);
	}

	if(!$arrGetUserRights["importDatas"]) {
		header('location: ' . PAGE_EXIT_LOCATION);
		exit;
	}

	$arrFieldSeperator = array(
		"tab"			=> "\t",
		"semikolon"		=> ";",
		"komma"			=> ",",
	);

	$fieldTerminator = $_POST["selectFieldTerminator"];
	$fieldSeperator = $arrFieldSeperator[$_POST["selectFieldSeperator"]];
	$lineSeperator = $_POST["selectLineSeperator"];


	$jswindowMessage = '';
	$warningMessage = '';
	$errorMessage = '';
	$successMessage = '';
	$infoMessage = '';

	$dbConnection = new DB_Connection();
	$db_open = $dbConnection->db_connect();

	function createNonRelatedFieldFrom($thisKey, $thisValue){
		global $arrRecordsArray;
		$thisFieldItem = '<li id="recordsArray_noValue_' . $thisKey . '" style="background-color:#FFAFAF;">';
		#$thisFieldItem .= $thisKey . ' => ' . $thisValue;
		$thisFieldItem .= 'kein Feld zugeordnet';
		$thisFieldItem .= '<div class="buttonArea">';
		$thisFieldItem .= '<img src="layout/icons/iconClose.png" class="buttonRemoveItem" width="14" height="14" alt="" title="Element ans Ende verschieben" />';
		$thisFieldItem .= '</div>';
		$thisFieldItem .= '</li>';
		$arrRecordsArray[] = 'recordsArray_noValue[]=' . $thisKey;
		return $thisFieldItem;
	}
	function createRelatedFieldFrom($thisKey, $thisValue){
		global $arrRecordsArray;
		$thisFieldItem = '<li id="recordsArray_' . $thisValue . '" style="background-color:#BBFFAF;">';
		$thisFieldItem .= $thisValue;
		$thisFieldItem .= '<div class="buttonArea">';
		$thisFieldItem .= '<img src="layout/icons/iconCopy3.png" class="buttonCopyItem" width="14" height="14" alt="" title="Element Kopieren" />';
		$thisFieldItem .= '<img src="layout/icons/iconClose.png" class="buttonRemoveItem" width="14" height="14" alt="" title="Element ans Ende verschieben" />';
		$thisFieldItem .= '</div>';
		$thisFieldItem .= '</li>';
		$arrRecordsArray[] = 'recordsArray[]=' . $thisValue;
		return $thisFieldItem;
	}

	// BOF READ COUNTRIES
		$arrCountryTypeDatas = getCountryTypes();
	// EOF READ COUNTRIES

	// BOF READ CUSTOMER GROUP DATAS
		$arrCustomerGroupDatas = getCustomerGroups();
	// EOF READ CUSTOMER GROUP DATAS

	// BOF GET IMPORT SETTINGS
		$where = "";
		if($_POST["selectImportSettings"]){
			$where = " AND `settingsImportFilesID` = " . $_POST["selectImportSettings"] . "" ;
		}
		$sql = "SELECT
					`settingsImportFilesID`,
					`settingsImportFilesCustomerGroup`,
					`settingsImportFilesCustomerCountry`,
					`settingsImportFilesFieldNamesTo`,
					`settingsImportFilesFieldNamesFrom`,
					`settingsImportFilesFieldNamesFromToPos`,
					`settingsImportFilesStoreDatetime`
					FROM `" . TABLE_SETTINGS_IMPORT_FILES . "`

					WHERE 1
						" . $where . "

					ORDER BY `settingsImportFilesStoreDatetime` DESC
			";
			$rs = $dbConnection->db_query($sql);

			while($ds = mysqli_fetch_assoc($rs)){
				foreach(array_keys($ds) as $field){
					$arrImportSettingDatas[$ds["settingsImportFilesID"]][$field] = $ds[$field];
				}
			}
		// EOF GET IMPORT SETTINGS

	if($_POST["submitFormImportDatas"] != "" && $_POST["selectImportTable"] != "") {
		// BOF EXISTING FILE
		if($_POST["selectExistingFile"] != "") {
			$thisImportFile = $_POST["selectExistingFile"];
		}
		// EOF EXISTING FILE

		// BOF UPLOADED FILE
			if(!empty($_FILES["selectUploadFile"]) && $_FILES["selectUploadFile"]["name"] != ""){
				$fileError = false;
				if($_FILES["selectUploadFile"]["size"] == 0) {
					$errorMessage .= 'Die Datei &quot;' . $_POST["selectExistingFile"] . '&quot; hat keinen Inhalt! ' . '<br />';
					$fileError = true;
				}
				if($_FILES["selectUploadFile"]["error"] != 0) {
					$errorMessage .= 'Der Upload der Datei &quot;' . $_POST["selectExistingFile"] . '&quot; war fehlerhaft! ' . '<br />';
					$fileError = true;
				}
				if($_FILES["selectUploadFile"]["tmp_name"] == "") {
					$errorMessage .= 'Die Datei &quot;' . $_POST["selectExistingFile"] . '&quot; wurde nicht ins TEMP-Verzeichnis kopiert! ' . '<br />';
					$fileError = true;
				}

				if(!$fileError) {
					if(file_exists(DIRECTORY_IMPORT_FILES . $_FILES["selectUploadFile"]["name"])) {
						// unlink(DIRECTORY_IMPORT_FILES . $_FILES["selectUploadFile"]["name"]);
					}
					clearstatcache();
					$copyResult = copy($_FILES["selectUploadFile"]["tmp_name"], DIRECTORY_IMPORT_FILES . $_FILES["selectUploadFile"]["name"]);
					if(!$copyResult) {
						$errorMessage .= 'Die Datei &quot;' . $_FILES["selectUploadFile"]["name"] . '&quot; wurde nicht ins IMPORT-Verzeichnis &quot;' . DIRECTORY_IMPORT_FILES . '&quot; kopiert! ' . '<br />';
						$thisImportFile = "";
					}
					else {
						$thisImportFile = DIRECTORY_IMPORT_FILES . $_FILES["selectUploadFile"]["name"];
						$successMessage .= 'Die Datei &quot;' . $_FILES["selectUploadFile"]["name"] . '&quot; wurde ins IMPORT-Verzeichnis &quot;' . DIRECTORY_IMPORT_FILES . '&quot; kopiert! ' . '<br />';
					}
				}
			}
		// EOF UPLOADED FILE

		// BOF IMPORT FILE
		if($thisImportFile != "") {
			if(file_exists($thisImportFile)) {
				// BOF CREATE TEMP TABLE
				$fp = fopen($thisImportFile, 'r');
				$line = fgets($fp);
				fclose($fp);
				#$infoMessage .= 'line: ' . '<br />';
				#$infoMessage .= '<pre>';
				#$infoMessage .= $line;
				#$infoMessage .= '</pre>';


				$arrGetFields = explode($fieldSeperator, $line);
				$arrTableFieldNames = array();

				if(!empty($arrGetFields)) {
				// TABLE_IMPORT_CUSTOMERS_GROUP_MEMPERS | $prefixTablesCommon . 'importCustomerGroupMembers{###GROUP_NAME###}
					$sql = "DROP TABLE IF EXISTS `_tempTable_IMPORT_" . ($_POST["selectImportTable"]) . "`";
					$rs = $dbConnection->db_query($sql);

					$createTempTableFields = array();
					$sql_createTempTable = "CREATE TABLE IF NOT EXISTS `_tempTable_IMPORT_" . ($_POST["selectImportTable"]) . "` ( ";

					$count = 1;
					foreach($arrGetFields as $thisField) {
						if($_POST["selectFieldNamesInFirstRow"] != '1'){
							$thisField = 'FIELD_' . $count;
						}
						$thisTableField = strtoupper(trim(preg_replace("/\"/", "", preg_replace("/ /", "_", $thisField))));

						if($_POST["selectFieldNamesInFirstRow"] == '1'){
							$thisTableField =  $count . '_' . $thisTableField;
							#$thisTableField =  $thisTableField . 'x' . $count;
						}

						$arrTableFieldNames[] = $thisTableField;
						// $sql_createTempTableFields[] = "`" . $thisTableField . "` varchar(255) COLLATE utf8_unicode_ci NOT NULL";
						$createTempTableFields[] = "`" . $thisTableField . "` text COLLATE utf8_unicode_ci NOT NULL";
						$count++;
					}
					$sql_createTempTable .= implode(", ", $createTempTableFields);

					$sql_createTempTable .= ") ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;";
					$rs_createTempTable = $dbConnection->db_query($sql_createTempTable);
					if($rs_createTempTable) {
						$successMessage .= 'Die Tabelle &quot;`_tempTable_' . constant($_POST["selectImportTable"]) . '`&quot; wurde erfolgreich erstellt!' . '<br />';
					}
					else {
						$errorMessage .= 'Die Tabelle &quot;`_tempTable_' . constant($_POST["selectImportTable"]) . '`&quot; konnte nicht erstellt werden! ' . '<br />';
					}

					// BOF IMPORT INTO TEMP TABLE
					if($rs_createTempTable) {
						$fp = fopen($thisImportFile, 'r');
						$countItem = 0;
						while($lineContent = fgets($fp)){
							$countItem++;
							if($countItem == 1 && $_POST["selectFieldNamesInFirstRow"] == '1'){
								$infoMessage .= 'Der Datensatz '.$countItem.' wurde nicht in die Tabelle &quot; `_tempTable_' . constant($_POST["selectImportTable"]) . '`&quot; importiert, da Sie angegeben haben, dass die erste Zeile die Spaltennamen enth&auml;lt! ' . '<br />';
							}
							else {
								$lineContent = preg_replace("/\\r\\n/", $lineSeperator, $lineContent);

								$lineContent = preg_replace("/" . $fieldSeperator . "\"/", $fieldSeperator, $lineContent);
								$lineContent = preg_replace("/\"" . $fieldSeperator . "/", $fieldSeperator, $lineContent);

								$lineContent = preg_replace("/" . $lineSeperator . "\"/", $lineSeperator, $lineContent);
								$lineContent = preg_replace("/\"" . $lineSeperator . "/", $lineSeperator, $lineContent);

								$arrGetFieldContent = explode($fieldSeperator, $lineContent);

								$insertTempTableFieldContent = array();
								$sql_insertTempTable = "INSERT INTO `_tempTable_IMPORT_" . ($_POST["selectImportTable"]) . "` (
											`" . implode("`, `", $arrTableFieldNames). "`
											)
											VALUES (
								";
								foreach($arrGetFieldContent as $thisFieldContent) {
									$thisFieldContent = preg_replace("/([0-9]{2})\.([0-9]{2})\.([0-9]{4})/", "$3-$2-$1", $thisFieldContent);
									$thisFieldContent = preg_replace("/([0-9]{1,}),([0-9]{1,})/", "$1.$2", $thisFieldContent);
									$insertTempTableFieldContent[] = "'" . addslashes(trim($thisFieldContent)) . "'";
								}
								$sql_insertTempTable .= implode(", ", $insertTempTableFieldContent);
								$sql_insertTempTable .= ");";

								$rs_insertTempTable = $dbConnection->db_query($sql_insertTempTable);
								if($rs_insertTempTable) {
									$successMessage .= 'Der Datensatz '.$countItem.' wurde in die Tabelle &quot; `_tempTable_IMPORT_' . ($_POST["selectImportTable"]) . '`&quot; erfolgreich importiert!' . '<br />';
								}
								else {
									$errorMessage .= 'Der Datensatz '.$countItem.' konnte nicht in die Tabelle &quot; `_tempTable_IMPORT_' . ($_POST["selectImportTable"]) . '`&quot; importiert werden! ' . '<br />';
								}
							}
						}
						fclose($fp);
					}
					// EOF IMPORT INTO TEMP TABLE
				}
				// EOF CREATE TEMP TABLE

// BOF IMPORT CUSTOMERS
##########################
					if($_POST["selectImportTable"] != '') {

						$sql = "DROP TABLE IF EXISTS `_tempTable_IMPORT_" . ($_POST["selectImportTable"]) . "_USE`";
						$rs = $dbConnection->db_query($sql);

						$sql = "CREATE TABLE `_tempTable_IMPORT_" . ($_POST["selectImportTable"]) . "_USE` LIKE `". constant($_POST["selectImportTable"]) . "`";
						$rs = $dbConnection->db_query($sql);

						$sql = "ALTER TABLE `_tempTable_IMPORT_" . ($_POST["selectImportTable"]) . "_USE` DROP INDEX `customersKundennummer`, ADD INDEX `customersKundennummer` (`customersKundennummer`) COMMENT '' ";
						$rs = $dbConnection->db_query($sql);

						$sql = "ALTER TABLE `_tempTable_IMPORT_" . ($_POST["selectImportTable"]) . "_USE` ADD `customerExists` ENUM( '0', '1' ) NOT NULL DEFAULT '0' AFTER `customersID` ,ADD INDEX ( `customerExists` )";
						$rs = $dbConnection->db_query($sql);
						$sql = "ALTER TABLE `_tempTable_IMPORT_" . ($_POST["selectImportTable"]) . "_USE` ADD `customerIsImported` ENUM( '0', '1' ) NOT NULL DEFAULT '0' AFTER `customersID` ,ADD INDEX ( `customerIsImported` )";
						$rs = $dbConnection->db_query($sql);

						$sql = "SHOW COLUMNS FROM `" . TABLE_CUSTOMERS_USE . "`";
						$rs = $dbConnection->db_query($sql);
						while($ds = mysqli_fetch_assoc($rs)){
							$arrFieldsImportTo[] = $ds["Field"];
						}

						$sql = "SHOW COLUMNS FROM `_tempTable_IMPORT_" . ($_POST["selectImportTable"]) . "`";
						$rs = $dbConnection->db_query($sql);
						while($ds = mysqli_fetch_assoc($rs)){
							$arrFieldsImportFrom[] = $ds["Field"];
						}

						$sql = "TRUNCATE TABLE `_tempTable_IMPORT_" . ($_POST["selectImportTable"]) . "_USE`;";
						$rs = $dbConnection->db_query($sql);
					}

##########################
// EOF IMPORT CUSTOMERS
			}
			else {
				$errorMessage .= 'Die Datei &quot;' . $thisImportFile . '&quot; existiert nicht! ' . '<br />';
			}
			clearstatcache();
		}
		else {
			$errorMessage .= 'Es kann keine Datei importiert werden! ' . '<br />';
		}
		// EOF IMPORT FILE
	}
?>
<?php
	require_once('inc/headerHTML.inc.php');
	$thisTitle = "Daten-Import";
	$headerHTML = preg_replace("/{###TITLE###}/", strip_tags($thisTitle), $headerHTML);
	echo $headerHTML;

	require_once(FILE_MENUE_TOP);
?>
<div id="mainArea">
	<div id="mainContent">
		<?php require_once(FILE_MENUE_SIDEBAR); ?>
		<div id="contentArea">
			<div id="contentAreaElements">
				<h1><?php if(SHOW_PAGE_TITLE_ICONS) { echo '<img src="' . PATH_ICONS_MENUE_TITLES . 'import.png" alt="" />'; } ?> <?php echo $thisTitle; ?></h1>
				<?php
					if($thisDebug){
						echo '<p class="warningArea">DEBUG</p>';
					}
				?>
				<p class="infoArea">Die Dateien m&uuml;ssen im csv-Format vorliegen und in der ersten Zeile die Spaltennamen enthalten, die Felder m&uuml;ssen mit einem TAB getrennt sein!</p>
				<div class="adminEditArea">

				<?php if($_POST["submitFieldsImport"] == '' && (empty($arrFieldsImportTo) && empty($arrFieldsImportFrom))){ ?>

				<form name="formImportDatas" method="post" action="<?php echo $_SERVER["REQUEST_URI"]; ?>" enctype="multipart/form-data">
					<fieldset>
					<table border="0" cellpadding="0" cellspacing="0" width="100%">
						<tr>
							<td style="width:200px;"><b>Daten-Art:</b></td>
							<td>
								<select class="inputSelect_510" name="selectImportTable">
									<option value=""> - Bitte w&auml;hlen - </option>
									<option value="TABLE_CUSTOMERS" <?php if($_POST["selectImportTable"] == "TABLE_CUSTOMERS") { echo ' selected="selected" '; } ?>>Kunden</option>
								</select>
							</td>
						</tr>
						<tr>
							<td style="width:200px;"><b>Kundengruppe:</b></td>
							<td>
								<select class="inputSelect_510" name="selectImportMemberCustomerGroup">
									<option value="NONE"> - keine - </option>
									<?php
										if(!empty($arrCustomerGroupDatas)){
											foreach($arrCustomerGroupDatas as $thisKey => $thisValue){
												$selected = '';
												if($thisValue["customerGroupsID"] == $_POST["selectImportMemberCustomerGroup"]){
													$selected = ' selected="selected" ';
												}
												echo '<option value="' . $thisValue["customerGroupsID"] . '" ' . $selected . ' >' . $thisValue["customerGroupsName"] . '</option>';
											}
										}
									?>
								</select>
							</td>
						</tr>
						<tr>
							<td style="width:200px;"><b>Kundenland:</b></td>
							<td>
								<select class="inputSelect_510" name="selectImportMemberCustomerCountry">
									<option value=""> - </option>
									<?php
										if(!empty($arrCountryTypeDatas)) {
											foreach($arrCountryTypeDatas as $thisKey => $thisValue) {
												$selected = '';
												if($_POST["selectImportMemberCustomerCountry"] != ""){
													if($thisKey == $_POST["selectImportMemberCustomerCountry"]) {
														$selected = ' selected="selected" ';
													}
												}
												else if($thisKey == 81) {
													$selected = ' selected="selected" ';
												}
												echo '<option value="' . $thisKey . '" '.$selected.' >' . ($arrCountryTypeDatas[$thisKey]["countries_name"]). ' [' . $arrCountryTypeDatas[$thisKey]["countries_iso_code_2"] . ']</option>';
											}
										}
									?>
								</select>
							</td>
						</tr>
						<tr>
							<td style="width:200px;"><b>Vorhandene Datei verwenden:</b></td>
							<td>
								<?php
									$arrGetExistingImportFiles = read_dir(DIRECTORY_IMPORT_FILES);
								?>
								<select class="inputSelect_510" name="selectExistingFile">
									<option value=""> - Bitte w&auml;hlen -</option>
									<?php
										if(!empty($arrGetExistingImportFiles)){
											foreach($arrGetExistingImportFiles as $thisExistingFile){
												$selected = '';
												if($thisExistingFile == $_POST["selectExistingFile"]){
													$selected = ' selected="selected" ';
												}
												echo '<option value="' . $thisExistingFile . '" '.$selected.' >' . utf8_encode($thisExistingFile) . '</option>';
											}
										}
									?>
								</select>
							</td>
						</tr>
						<tr>
							<td style="width:200px;"><b>Hochzuladene Datei verwenden:</b></td>
							<td><input type="file" class="inputFile_510" name="selectUploadFile" /></td>
						</tr>

						<?php
							if(!empty($arrImportSettingDatas)){
						?>
						<tr>
							<td style="width:200px;"><b>Gespeicherte Feldzuordnungen:</b></td>
							<td>
								<select class="inputSelect_510" name="selectImportSettings">
									<option value=""> - </option>
									<?php
										foreach($arrImportSettingDatas as $thisImportSettingKey => $thisImportSettingValue){
											echo '<option value="' . $thisImportSettingValue["settingsImportFilesID"] . '">' . $arrCustomerGroupDatas[$thisImportSettingValue["settingsImportFilesCustomerGroup"]]["customerGroupsName"] . ' | ' . $arrCountryTypeDatas[$thisImportSettingValue["settingsImportFilesCustomerCountry"]]["countries_name"]. ' [' . $arrCountryTypeDatas[$thisImportSettingValue["settingsImportFilesCustomerCountry"]]["countries_iso_code_2"] . '] - ' . $thisImportSettingValue["settingsImportFilesStoreDatetime"] . '</option>';
										}
									?>
								</select>
							</td>
						</tr>
						<?php
							}
						?>

						<tr>
							<td style="width:200px;"><b>Feld-Trenner:</b></td>
							<td>
								<select class="inputSelect_510" name="selectFieldSeperator">
									<option value="tab" selected="selected" >Tabulator</option>
									<option value="semikolon">Semikolon</option>
									<option value="komma">Komma</option>
								</select>
							</td>
						</tr>
						<tr>
							<td style="width:200px;"><b>Feld-Begrenzer:</b></td>
							<td>
								<select class="inputSelect_510" name="selectFieldTerminator">
									<option value="nothing" selected="selected" >Keine</option>
									<option value="doubleQuotes">Doppelte Anf&uuml;hrungszeichen</option>
									<option value="singleQuotes">Einfache Anf&uuml;hrungszeichen</option>
								</select>
							</td>
						</tr>
						<tr>
							<td style="width:200px;"><b>Zeilen-Trenner:</b></td>
							<td>
								<select class="inputSelect_510" name="selectLineSeperator">
									<option value="" selected="selected" >auto</option>
									<option value="returnNewLine">\r\n</option>
									<option value="newLine">\n</option>
								</select>
							</td>
						</tr>
						<tr>
							<td style="width:200px;"><b>Spalten-Namen:</b></td>
							<td>
								<input type="checkbox" name="selectFieldNamesInFirstRow" checked="checked" value="1" /> Spalten-Namen in 1. Zeile vorhanden?
							</td>
						</tr>
					</table>
					<div class="actionButtonsArea">
						<input type="submit" name="submitFormImportDatas" class="inputButton1" value="Daten einlesen" />
					</div>
					</fieldset>
				</form>
				<?php } ?>

				<?php displayMessages(); ?>


<?php
// BOF SORT IMPORT FIELDS
	if(!empty($_POST["arrFieldsImportTo"]) && !empty($_POST["arrFieldsImportFrom"])){
		$arrUseFieldsImportTo = $_POST["arrFieldsImportTo"];
		$arrUseFieldsImportFrom = explode("&", $_POST["arrFieldsImportFrom"]);

		$arrSqlImportFieldNamesTo = array();
		$arrSqlImportFieldNamesFrom = array();
		$arrSqlImportFieldNamesFromToPos = array();
		$count = 0;
		foreach($arrUseFieldsImportTo as $thisKey => $thisValue) {
			if(!preg_match("/_noValue/", $arrUseFieldsImportFrom[$thisKey]) && $arrUseFieldsImportFrom[$thisKey] != "") {
				// echo $arrUseFieldsImportTo[$thisKey] . ' | ' . $arrUseFieldsImportFrom[$thisKey] . '<br />';
				$arrSqlImportFieldNamesTo[$count] = $arrUseFieldsImportTo[$thisKey];
				#$arrSqlImportFieldNamesFrom[$count] = preg_replace("/recordsArray\[\]=/", "", $arrUseFieldsImportFrom[$thisKey]);
				$arrSqlImportFieldNamesFrom[$count] = preg_replace("/^_/", "", preg_replace("/=/", "_", preg_replace("/recordsArray[_]{0,}(.*)\[\]/", "$1", $arrUseFieldsImportFrom[$thisKey])));
				$arrSqlImportFieldNamesFromToPos[$count] = $thisKey;
				$count++;
			}
		}

		// BOF STORE IMPORT SETTINGS
		$sql = "
				INSERT INTO `" . TABLE_SETTINGS_IMPORT_FILES . "` (
							`settingsImportFilesID`,
							`settingsImportFilesCustomerGroup`,
							`settingsImportFilesCustomerCountry`,
							`settingsImportFilesFieldNamesTo`,
							`settingsImportFilesFieldNamesFrom`,
							`settingsImportFilesFieldNamesFromToPos`,
							`settingsImportFilesStoreDatetime`
						)
						VALUES (
							'%',
							'" . $_POST["selectImportMemberCustomerGroup"] . "',
							'" . $_POST["selectImportMemberCustomerCountry"] . "',
							'" . serialize($arrSqlImportFieldNamesTo) . "',
							'" . serialize($arrSqlImportFieldNamesFrom) . "',
							'" . serialize($arrSqlImportFieldNamesFromToPos) . "',
							NOW()
						)
			";
			$rs = $dbConnection->db_query($sql);
		// EOF STORE IMPORT SETTINGS

		$sql = "";
		if(!empty($arrSqlImportFieldNamesTo) && !empty($arrSqlImportFieldNamesFrom)) {

			$sql = "SHOW TABLE STATUS FROM `" . DB_NAME . "` LIKE '_tempTable_IMPORT_" . ($_POST["selectImportTable"]) . "_USE'";
			$rs = $dbConnection->db_query($sql);
			$countRows1 = $dbConnection->db_getMysqlNumRows($rs);

			$sql = "SHOW TABLE STATUS FROM `" . DB_NAME . "` LIKE '_tempTable_IMPORT_" . ($_POST["selectImportTable"]) . "'";
			$rs = $dbConnection->db_query($sql);
			$countRows2 = $dbConnection->db_getMysqlNumRows($rs);
			$countRows = $countRows1 * $countRows2;


			if($countRows > 0) {
				// BOF INSERT EXISTING DATA
				/*
				$sql = "INSERT IGNORE INTO `_tempTable_IMPORT_" . ($_POST["selectImportTable"]) . "_USE`
							SELECT * FROM `". constant($_POST["selectImportTable"]) . "`
				";
				#$rs = $dbConnection->db_query($sql);
				*/
				// EOF INSERT EXISTING DATA

				// BOF DELETE FIRST LINE INCLUDING FIELD NAMES
					/*
					$sql = "DELETE FROM `_tempTable_IMPORT_" . ($_POST["selectImportTable"]) . "`
								WHERE `" . $arrSqlImportFieldNamesFrom[0] . "` = '" . $arrSqlImportFieldNamesFrom[0] . "'
					";
					#$rs = $dbConnection->db_query($sql);
					*/
				// EOF DELETE FIRST LINE INCLUDING FIELD NAMES

				// BOF INSERT NEW DATA
				$sql = "
							INSERT IGNORE INTO `_tempTable_IMPORT_" . ($_POST["selectImportTable"]) . "_USE` (
								`" . implode("`,`", $arrSqlImportFieldNamesTo). "`
							)
							SELECT
								`" . implode("`,`", $arrSqlImportFieldNamesFrom). "`

							FROM `_tempTable_IMPORT_" . ($_POST["selectImportTable"]) . "`
				";

				$rs = $dbConnection->db_query($sql);

				if($_POST["selectImportMemberCustomerCountry"] != ''){
					$sql = "UPDATE
								`_tempTable_IMPORT_" . ($_POST["selectImportTable"]) . "_USE`
								SET `customersCompanyCountry` = '" . $_POST["selectImportMemberCustomerCountry"] . "'
						";
					$rs = $dbConnection->db_query($sql);
				}
				if($_POST["selectImportMemberCustomerGroup"] != ''){
					$sql = "UPDATE
								`_tempTable_IMPORT_" . ($_POST["selectImportTable"]) . "_USE`
								SET `customersGruppe` = '" . $_POST["selectImportMemberCustomerGroup"] . "'
						";
					dd('sql');
					$rs = $dbConnection->db_query($sql);
				}

				// EOF INSERT NEW DATA

				if($rs) {
					$successMessage .= 'Die Daten wurden in die Tabelle &quot;`_tempTable_IMPORT_' . ($_POST["selectImportTable"]) . '_USE`&quot; erfolgreich importiert!' . '<br />';
					$sql = "DROP TABLE IF EXISTS `_tempTable_IMPORT_" . ($_POST["selectImportTable"]) . "`";
					#$rs = $dbConnection->db_query($sql);

					// BOF COPY CUSTOMER TEST TABLE
					$sql = "DROP TABLE IF EXISTS `" . TABLE_CUSTOMERS_TEST . "`";
					$rs = $dbConnection->db_query($sql);
					$sql = "CREATE TABLE IF NOT EXISTS `" . TABLE_CUSTOMERS_TEST . "` LIKE `". constant($_POST["selectImportTable"]) . "`";
					$rs = $dbConnection->db_query($sql);

					$sql = "INSERT INTO `" . TABLE_CUSTOMERS_TEST . "` SELECT * FROM  `". constant($_POST["selectImportTable"]) . "`";
					$rs = $dbConnection->db_query($sql);
					// EOF COPY CUSTOMER TEST TABLE

					// BOF COPY CUSTOMER SICH TABLE
					$sql = "DROP TABLE IF EXISTS `_SICH" . date("YmdHis") . "_" . TABLE_CUSTOMERS . "`";
					$rs = $dbConnection->db_query($sql);
					$sql = "CREATE TABLE IF NOT EXISTS `_SICH" . date("YmdHis") . "_" . TABLE_CUSTOMERS . "` LIKE `". constant($_POST["selectImportTable"]) . "`";
					$rs = $dbConnection->db_query($sql);

					$sql = "INSERT INTO `_SICH" . date("YmdHis") . "_" . TABLE_CUSTOMERS . "` SELECT * FROM  `". constant($_POST["selectImportTable"]) . "`";
					$rs = $dbConnection->db_query($sql);
					// EOF COPY CUSTOMER SICH TABLE

					// BOF CHECK IF IMPORTED CUSTOMER EXISTS
					$xxx_sql = "
						SELECT
								`_tempTable_IMPORT_" . ($_POST["selectImportTable"]) . "_USE`.`customersID`,
								`" . TABLE_CUSTOMERS_USE . "`.`customersID`,

								`_tempTable_IMPORT_" . ($_POST["selectImportTable"]) . "_USE`.`customersKundennummer`,
								`" . TABLE_CUSTOMERS_USE . "`.`customersKundennummer`,

								`_tempTable_IMPORT_" . ($_POST["selectImportTable"]) . "_USE`.`customersFirmenname`,
								`" . TABLE_CUSTOMERS_USE . "`.`customersFirmenname`,

								`_tempTable_IMPORT_" . ($_POST["selectImportTable"]) . "_USE`.`customersCompanyStrasse`,
								`" . TABLE_CUSTOMERS_USE . "`.`customersCompanyStrasse`,

								`_tempTable_IMPORT_" . ($_POST["selectImportTable"]) . "_USE`.`customersCompanyHausnummer`,
								`" . TABLE_CUSTOMERS_USE . "`.`customersCompanyHausnummer`,

								`_tempTable_IMPORT_" . ($_POST["selectImportTable"]) . "_USE`.`customersCompanyPLZ`,
								`" . TABLE_CUSTOMERS_USE . "`.`customersCompanyPLZ`,

								`_tempTable_IMPORT_" . ($_POST["selectImportTable"]) . "_USE`.`customersCompanyOrt`,
								`" . TABLE_CUSTOMERS_USE . "`.`customersCompanyOrt`


								FROM `_tempTable_IMPORT_" . ($_POST["selectImportTable"]) . "_USE`

								INNER JOIN `" . TABLE_CUSTOMERS_USE . "`
								ON(
									CONCAT(REPLACE(`_tempTable_IMPORT_" . $_POST["selectImportTable"] . "_USE`.`customersCompanyStrasse`, ' ', ''), REPLACE(`_tempTable_IMPORT_" . ($_POST["selectImportTable"]) . "_USE`.`customersCompanyHausnummer`, ' ', '')) = CONCAT(REPLACE(`" . TABLE_CUSTOMERS_USE . "`.`customersCompanyStrasse`, ' ', ''), REPLACE(`" . TABLE_CUSTOMERS_USE . "`.`customersCompanyHausnummer`, ' ', ''))

									AND
									CONCAT(REPLACE(`_tempTable_IMPORT_" . $_POST["selectImportTable"] . "_USE`.`customersCompanyPLZ`, ' ', ''), REPLACE(`_tempTable_IMPORT_" . ($_POST["selectImportTable"]) . "_USE`.`customersCompanyOrt`, ' ', '')) = CONCAT(REPLACE(`" . TABLE_CUSTOMERS_USE . "`.`customersCompanyPLZ`, ' ', ''), REPLACE(`" . TABLE_CUSTOMERS_USE . "`.`customersCompanyOrt`, ' ', ''))

									/*
									AND
									`_tempTable_IMPORT_" . ($_POST["selectImportTable"]) . "_USE`.`customersFirmenname` = `" . TABLE_CUSTOMERS_USE . "`.`customersFirmenname`
									*/
								)

								WHERE 1
					";


					$sql = "UPDATE
								`_tempTable_IMPORT_" . ($_POST["selectImportTable"]) . "_USE`
								INNER JOIN `" . TABLE_CUSTOMERS_USE . "`
								ON(
						";

					if($_POST["selectImportMemberCustomerCountry"] == 81){
						$sql .= "
									CONCAT(REPLACE(REPLACE(`_tempTable_IMPORT_" . $_POST["selectImportTable"] . "_USE`.`customersCompanyStrasse`, ' ', ''), 'straße', 'str.'), REPLACE(`_tempTable_IMPORT_" . ($_POST["selectImportTable"]) . "_USE`.`customersCompanyHausnummer`, ' ', '')) = CONCAT(REPLACE(REPLACE(`" . TABLE_CUSTOMERS_USE . "`.`customersCompanyStrasse`, ' ', ''), 'straße', 'str.'), REPLACE(`" . TABLE_CUSTOMERS_USE . "`.`customersCompanyHausnummer`, ' ', ''))
							";
					}
					else {
						$sql .= "
									CONCAT(REPLACE(`_tempTable_IMPORT_" . $_POST["selectImportTable"] . "_USE`.`customersCompanyStrasse`, ' ', ''), REPLACE(`_tempTable_IMPORT_" . ($_POST["selectImportTable"]) . "_USE`.`customersCompanyHausnummer`, ' ', '')) = CONCAT(REPLACE(`" . TABLE_CUSTOMERS_USE . "`.`customersCompanyStrasse`, ' ', ''), REPLACE(`" . TABLE_CUSTOMERS_USE . "`.`customersCompanyHausnummer`, ' ', ''))
							";
					}

					$sql .= "
									AND
									CONCAT(REPLACE(`_tempTable_IMPORT_" . $_POST["selectImportTable"] . "_USE`.`customersCompanyPLZ`, ' ', ''), REPLACE(`_tempTable_IMPORT_" . ($_POST["selectImportTable"]) . "_USE`.`customersCompanyOrt`, ' ', '')) = CONCAT(REPLACE(`" . TABLE_CUSTOMERS_USE . "`.`customersCompanyPLZ`, ' ', ''), REPLACE(`" . TABLE_CUSTOMERS_USE . "`.`customersCompanyOrt`, ' ', ''))
/*
									AND
									`_tempTable_IMPORT_" . ($_POST["selectImportTable"]) . "_USE`.`customersFirmenname` = `" . TABLE_CUSTOMERS_USE . "`.`customersFirmenname`
*/
								)
								SET
									`_tempTable_IMPORT_" . ($_POST["selectImportTable"]) . "_USE`.`customerExists` = '1',
									`_tempTable_IMPORT_" . ($_POST["selectImportTable"]) . "_USE`.`customersKundennummer` = `" . TABLE_CUSTOMERS_USE . "`.`customersKundennummer`,
									`_tempTable_IMPORT_" . ($_POST["selectImportTable"]) . "_USE`.`customersGruppe` =
									CASE
										WHEN `" . TABLE_CUSTOMERS_USE . "`.`customersGruppe` IS NULL OR `" . TABLE_CUSTOMERS_USE . "`.`customersGruppe` = ''
											THEN CONCAT(`_tempTable_IMPORT_" . ($_POST["selectImportTable"]) . "_USE`.`customersGruppe`, '')
										ELSE
											CONCAT(`" . TABLE_CUSTOMERS_USE . "`.`customersGruppe`, ';', `_tempTable_IMPORT_" . ($_POST["selectImportTable"]) . "_USE`.`customersGruppe`)
									END

								WHERE 1
						";
						dd('sql');
					$rs = $dbConnection->db_query($sql);

					$sql = "SELECT COUNT(`customerExists`) AS `rsAffectedRows` FROM `_tempTable_IMPORT_" . ($_POST["selectImportTable"]) . "_USE` WHERE 1 AND `customerExists`= '1' ";
					$rs = $dbConnection->db_query($sql);
					list($rsAffectedRows) = mysqli_fetch_array($rs);
					if($rsAffectedRows > 0){
						$infoMessage .= $rsAffectedRows . ' existierende Kunden wurden gefunden' . '<br />';
					}
					// EOF CHECK IF IMPORTED CUSTOMER EXISTS

					// BOF DISABLE MEMBER GROUP IN CUSTOMERS AND SET AGAIN FOR IMPORTED EXISTING CUSTOMERS IF COUNTRY AND MEMBER GROUP IS SET
					if($_POST["selectImportMemberCustomerCountry"] != '' && $_POST["selectImportMemberCustomerGroup"] != ''){
						// BOF DISABLE MEMBER GROUP IN CUSTOMERS
						$sql = "UPDATE
									`" . TABLE_CUSTOMERS_USE . "`
									SET `" . TABLE_CUSTOMERS_USE . "`.`customersGruppe` = ''
									WHERE 1
										AND `" . TABLE_CUSTOMERS_USE . "`.`customersGruppe` = '" . $_POST["selectImportMemberCustomerGroup"] . "'
										AND `" . TABLE_CUSTOMERS_USE . "`.`customersCompanyCountry` = '" . $_POST["selectImportMemberCustomerCountry"] . "'
							";
						$rs = $dbConnection->db_query($sql);

						$sql = "UPDATE
								`" . TABLE_CUSTOMERS_USE . "`
								SET `" . TABLE_CUSTOMERS_USE . "`.`customersGruppe` = REPLACE(`" . TABLE_CUSTOMERS_USE . "`.`customersGruppe`, ';" . $_POST["selectImportMemberCustomerGroup"] . ";', ';')
								WHERE 1
									AND `" . TABLE_CUSTOMERS_USE . "`.`customersGruppe` LIKE '%;" . $_POST["selectImportMemberCustomerGroup"] . ";%'
									AND `" . TABLE_CUSTOMERS_USE . "`.`customersCompanyCountry` = '" . $_POST["selectImportMemberCustomerCountry"] . "'
							";
						$rs = $dbConnection->db_query($sql);

						$sql = "UPDATE
								`" . TABLE_CUSTOMERS_USE . "`
								SET `" . TABLE_CUSTOMERS_USE . "`.`customersGruppe` = SUBSTRING(`" . TABLE_CUSTOMERS_USE . "`.`customersGruppe`, (LENGTH('" . $_POST["selectImportMemberCustomerGroup"] . ";') + 1), LENGTH(`" . TABLE_CUSTOMERS_USE . "`.`customersGruppe`))
								WHERE 1
									AND `" . TABLE_CUSTOMERS_USE . "`.`customersGruppe` LIKE '" . $_POST["selectImportMemberCustomerGroup"] . ";%'
									AND `" . TABLE_CUSTOMERS_USE . "`.`customersCompanyCountry` = '" . $_POST["selectImportMemberCustomerCountry"] . "'
							";
						$rs = $dbConnection->db_query($sql);
						dd('sql');
						$sql = "UPDATE
								`" . TABLE_CUSTOMERS_USE . "`
								SET `" . TABLE_CUSTOMERS_USE . "`.`customersGruppe` = SUBSTRING(`" . TABLE_CUSTOMERS_USE . "`.`customersGruppe`, 1, (LENGTH(`" . TABLE_CUSTOMERS_USE . "`.`customersGruppe`) - LENGTH(';" . $_POST["selectImportMemberCustomerGroup"] . "')))
								WHERE 1
									AND `" . TABLE_CUSTOMERS_USE . "`.`customersGruppe` LIKE '%;" . $_POST["selectImportMemberCustomerGroup"] . "'
									AND `" . TABLE_CUSTOMERS_USE . "`.`customersCompanyCountry` = '" . $_POST["selectImportMemberCustomerCountry"] . "'
							";
						$rs = $dbConnection->db_query($sql);
						// EOF DISABLE MEMBER GROUP IN CUSTOMERS

						// BOF SET AGAIN MEMBER GROUP IN CUSTOMERS
						$sql = "UPDATE
									`" . TABLE_CUSTOMERS_USE . "`
									LEFT JOIN `_tempTable_IMPORT_" . ($_POST["selectImportTable"]) . "_USE`
									ON(`" . TABLE_CUSTOMERS_USE . "`.`customersKundennummer` = `_tempTable_IMPORT_" . ($_POST["selectImportTable"]) . "_USE`.`customersKundennummer`)

									SET
										`" . TABLE_CUSTOMERS_USE . "`.`customersGruppe` =
										CASE
											WHEN (`" . TABLE_CUSTOMERS_USE . "`.`customersGruppe` IS NULL OR `" . TABLE_CUSTOMERS_USE . "`.`customersGruppe` = '')
												THEN '" . $_POST["selectImportMemberCustomerGroup"] . "'
											ELSE
												CONCAT(`" . TABLE_CUSTOMERS_USE . "`.`customersGruppe`, ';', '" . $_POST["selectImportMemberCustomerGroup"] . "')
										END
									WHERE 1
										AND `_tempTable_IMPORT_" . ($_POST["selectImportTable"]) . "_USE`.`customersKundennummer` != ''
							";
						$rs = $dbConnection->db_query($sql);
						// EOF SET AGAIN MEMBER GROUP IN CUSTOMERS
					}
					// EOF DISABLE MEMBER GROUP IN CUSTOMERS AND SET AGAIN FOR IMPORTED EXISTING CUSTOMERS IF COUNTRY AND MEMBER GROUP IS SET


					// BOF IMPORT NOT EXISTING IMPORTED CUSTOMERS INTO CUSTOMERS
					if($_POST["selectImportMemberCustomerCountry"] != ''){
						$sql = "SELECT
									`customersID`,
									`customersKundennummer`,
									`customersFirmenname`,
									`customersFirmennameZusatz`,
									`customersFirmenInhaberVorname`,
									`customersFirmenInhaberNachname`,
									`customersFirmenInhaberAnrede`,
									`customersFirmenInhaber2Vorname`,
									`customersFirmenInhaber2Nachname`,
									`customersFirmenInhaber2Anrede`,
									`customersAnsprechpartner1Vorname`,
									`customersAnsprechpartner1Nachname`,
									`customersAnsprechpartner1Anrede`,
									`customersAnsprechpartner1Typ`,
									`customersAnsprechpartner2Vorname`,
									`customersAnsprechpartner2Nachname`,
									`customersAnsprechpartner2Anrede`,
									`customersAnsprechpartner2Typ`,
									`customersTelefon1`,
									`customersTelefon2`,
									`customersMobil1`,
									`customersMobil2`,
									`customersFax1`,
									`customersFax2`,
									`customersMail1`,
									`customersMail2`,
									`customersHomepage`,
									`customersCompanyStrasse`,
									`customersCompanyHausnummer`,
									`customersCompanyCountry`,
									`customersCompanyPLZ`,
									`customersCompanyOrt`,
									`customersLieferadresseFirmenname`,
									`customersLieferadresseFirmennameZusatz`,
									`customersLieferadresseStrasse`,
									`customersLieferadresseHausnummer`,
									`customersLieferadressePLZ`,
									`customersLieferadresseOrt`,
									`customersLieferadresseLand`,
									`customersRechnungsadresseFirmenname`,
									`customersRechnungsadresseFirmennameZusatz`,
									`customersRechnungsadresseStrasse`,
									`customersRechnungsadresseHausnummer`,
									`customersRechnungsadressePLZ`,
									`customersRechnungsadresseOrt`,
									`customersRechnungsadresseLand`,
									`customersBankName`,
									`customersKontoinhaber`,
									`customersBankKontonummer`,
									`customersBankLeitzahl`,
									`customersBankIBAN`,
									`customersBankBIC`,
									`customersBezahlart`,
									`customersZahlungskondition`,
									`customersRabatt`,
									`customersRabattType`,
									`customersUseProductMwst`,
									`customersUseProductDiscount`,
									`customersUstID`,
									`customersVertreterID`,
									`customersVertreterName`,
									`customersUseSalesmanDeliveryAdress`,
									`customersUseSalesmanInvoiceAdress`,
									`customersTyp`,
									`customersGruppe`,
									`customersNotiz`,
									`customersUserID`,
									`customersTimeCreated`,
									`customersActive`,
									`customersDatasUpdated`,
									`customersTaxAccountID`,
									`customersInvoicesNotPurchased`,
									`customersBadPaymentBehavior`,
									`customersEntryDate`

								FROM `_tempTable_IMPORT_" . ($_POST["selectImportTable"]) . "_USE`
								WHERE 1
									AND `customerExists` = '0'
									AND `customerIsImported` = '0'
							";
						$rs = $dbConnection->db_query($sql);

						while($ds = mysqli_fetch_assoc($rs)){
							#dd('ds');
							foreach($ds as $thisDsKey => $thisDsValue){
								$ds[$thisDsKey] = trim($thisDsValue);
							}
							#dd('ds');
							$thisCustomerType = 1;
							$thisCustomerGroup = $_POST["selectImportMemberCustomerGroup"];
							$thisGeneratedCustomerNumber = generateCustomerNumber('customers', $thisCustomerType, $ds["customersCompanyCountry"], $ds["customersCompanyPLZ"].$ds["customersCompanyOrt"]);
							$thisCustomersTaxAccountID = getNewCustomerTaxAccountNumber($ds["customersCompanyCountry"], $arrCountryTypeDatas[$ds["customersCompanyCountry"]]["countries_iso_code_2"], 1);
							dd('thisGeneratedCustomerNumber');
							dd('thisCustomersTaxAccountID');
							if($thisGeneratedCustomerNumber != '' && $thisCustomersTaxAccountID != ''){
								$thisSql_insert = "
									INSERT INTO `" . TABLE_CUSTOMERS_USE . "`(
										`customersID`,
										`customersKundennummer`,
										`customersFirmenname`,
										`customersFirmennameZusatz`,
										`customersFirmenInhaberVorname`,
										`customersFirmenInhaberNachname`,
										`customersFirmenInhaberAnrede`,
										`customersFirmenInhaber2Vorname`,
										`customersFirmenInhaber2Nachname`,
										`customersFirmenInhaber2Anrede`,
										`customersAnsprechpartner1Vorname`,
										`customersAnsprechpartner1Nachname`,
										`customersAnsprechpartner1Anrede`,
										`customersAnsprechpartner1Typ`,
										`customersAnsprechpartner2Vorname`,
										`customersAnsprechpartner2Nachname`,
										`customersAnsprechpartner2Anrede`,
										`customersAnsprechpartner2Typ`,
										`customersTelefon1`,
										`customersTelefon2`,
										`customersMobil1`,
										`customersMobil2`,
										`customersFax1`,
										`customersFax2`,
										`customersMail1`,
										`customersMail2`,
										`customersHomepage`,
										`customersCompanyStrasse`,
										`customersCompanyHausnummer`,
										`customersCompanyCountry`,
										`customersCompanyPLZ`,
										`customersCompanyOrt`,
										`customersLieferadresseFirmenname`,
										`customersLieferadresseFirmennameZusatz`,
										`customersLieferadresseStrasse`,
										`customersLieferadresseHausnummer`,
										`customersLieferadressePLZ`,
										`customersLieferadresseOrt`,
										`customersLieferadresseLand`,
										`customersRechnungsadresseFirmenname`,
										`customersRechnungsadresseFirmennameZusatz`,
										`customersRechnungsadresseStrasse`,
										`customersRechnungsadresseHausnummer`,
										`customersRechnungsadressePLZ`,
										`customersRechnungsadresseOrt`,
										`customersRechnungsadresseLand`,
										`customersBankName`,
										`customersKontoinhaber`,
										`customersBankKontonummer`,
										`customersBankLeitzahl`,
										`customersBankIBAN`,
										`customersBankBIC`,
										`customersBezahlart`,
										`customersZahlungskondition`,
										`customersRabatt`,
										`customersRabattType`,
										`customersUseProductMwst`,
										`customersUseProductDiscount`,
										`customersUstID`,
										`customersVertreterID`,
										`customersVertreterName`,
										`customersUseSalesmanDeliveryAdress`,
										`customersUseSalesmanInvoiceAdress`,
										`customersTyp`,
										`customersGruppe`,
										`customersNotiz`,
										`customersUserID`,
										`customersTimeCreated`,
										`customersActive`,
										`customersDatasUpdated`,
										`customersTaxAccountID`,
										`customersInvoicesNotPurchased`,
										`customersBadPaymentBehavior`,
										`customersEntryDate`
									)
									VALUES(
										'%',
										'" . $thisGeneratedCustomerNumber . "',
										'" . addslashes($ds["customersFirmenname"]) . "',
										'" . addslashes($ds["customersFirmennameZusatz"]) . "',
										'" . addslashes($ds["customersFirmenInhaberVorname"]) . "',
										'" . addslashes($ds["customersFirmenInhaberNachname"]) . "',
										'" . $ds["customersFirmenInhaberAnrede"] . "',
										'" . addslashes($ds["customersFirmenInhaber2Vorname"]) . "',
										'" . addslashes($ds["customersFirmenInhaber2Nachname"]) . "',
										'" . $ds["customersFirmenInhaber2Anrede"] . "',
										'" . addslashes($ds["customersAnsprechpartner1Vorname"]) . "',
										'" . addslashes($ds["customersAnsprechpartner1Nachname"]) . "',
										'" . $ds["customersAnsprechpartner1Anrede"] . "',
										'" . $ds["customersAnsprechpartner1Typ"] . "',
										'" . addslashes($ds["customersAnsprechpartner2Vorname"]) . "',
										'" . addslashes($ds["customersAnsprechpartner2Nachname"]) . "',
										'" . $ds["customersAnsprechpartner2Anrede"] . "',
										'" . $ds["customersAnsprechpartner2Typ"] . "',
										'" . $ds["customersTelefon1"] . "',
										'" . $ds["customersTelefon2"] . "',
										'" . $ds["customersMobil1"] . "',
										'" . $ds["customersMobil2"] . "',
										'" . $ds["customersFax1"] . "',
										'" . $ds["customersFax2"] . "',
										'" . $ds["customersMail1"] . "',
										'" . $ds["customersMail2"] . "',
										'" . preg_replace("/http:\/\//", "", $ds["customersHomepage"]) . "',
										'" . addslashes(formatStreet($ds["customersCompanyStrasse"])) . "',
										'" . $ds["customersCompanyHausnummer"] . "',
										'" . $ds["customersCompanyCountry"] . "',
										'" . $ds["customersCompanyPLZ"] . "',
										'" . addslashes($ds["customersCompanyOrt"]) . "',
										'" . $ds["customersLieferadresseFirmenname"] . "',
										'" . $ds["customersLieferadresseFirmennameZusatz"] . "',
										'" . addslashes(formatStreet($ds["customersLieferadresseStrasse"])) . "',
										'" . $ds["customersLieferadresseHausnummer"] . "',
										'" . $ds["customersLieferadressePLZ"] . "',
										'" . addslashes($ds["customersLieferadresseOrt"]) . "',
										'" . $ds["customersLieferadresseLand"] . "',
										'" . $ds["customersRechnungsadresseFirmenname"] . "',
										'" . $ds["customersRechnungsadresseFirmennameZusatz"] . "',
										'" . addslashes(formatStreet($ds["customersRechnungsadresseStrasse"])) . "',
										'" . $ds["customersRechnungsadresseHausnummer"] . "',
										'" . $ds["customersRechnungsadressePLZ"] . "',
										'" . addslashes($ds["customersRechnungsadresseOrt"]) . "',
										'" . $ds["customersRechnungsadresseLand"] . "',
										'" . addslashes($ds["customersBankName"]) . "',
										'" . addslashes($ds["customersKontoinhaber"]) . "',
										'" . cleanChars($ds["customersBankKontonummer"]) . "',
										'" . cleanChars($ds["customersBankLeitzahl"]) . "',
										'" . cleanChars($ds["customersBankIBAN"]) . "',
										'" . cleanChars($ds["customersBankBIC"]) . "',
										'" . $ds["customersBezahlart"] . "',
										'" . $ds["customersZahlungskondition"] . "',
										'" . $ds["customersRabatt"] . "',
										'" . $ds["customersRabattType"] . "',
										'" . $ds["customersUseProductMwst"] . "',
										'" . $ds["customersUseProductDiscount"] . "',
										'" . $ds["customersUstID"] . "',
										'" . $ds["customersVertreterID"] . "',
										'" . $ds["customersVertreterName"] . "',
										'" . $ds["customersUseSalesmanDeliveryAdress"] . "',
										'" . $ds["customersUseSalesmanInvoiceAdress"] . "',
										'" . $thisCustomerType . "',
										'" . $thisCustomerGroup . "',
										'',
										1,
										NOW(),
										'1',
										NOW(),
										'" . $thisCustomersTaxAccountID . "',
										'0',
										'0',
										NOW()
									)
								";
								$thisRs_insert = $dbConnection->db_query($thisSql_insert);
								echo mysqli_error();
								if($thisRs_insert){
									$errorMessage .= 'Der Datensatz ID ' . $ds["customersID"] . ' wurde in die Tabelle &quot;`' . TABLE_CUSTOMERS_USE . '`&quot; importiert! ' . '<br />';
								}
								else {
									$errorMessage .= 'Der Datensatz ' . $ds["customersID"] . ' konnte nicht in die Tabelle &quot;`' . TABLE_CUSTOMERS_USE . '`&quot; importiert werden! ' . '<br />';
								}
							}
							else {
								if($thisGeneratedCustomerNumber == ''){
									$errorMessage .= 'F&uuml; Datensatz ID ' . $ds["customersID"] . ' konnte keine Kundennummer generiert werden ! ' . '<br />';
								}
								if($thisCustomersTaxAccountID == ''){
									$errorMessage .= 'F&uuml; Datensatz ID ' . $ds["customersID"] . ' konnte keine FiBu-Kontonummer generiert werden ! ' . '<br />';
								}
							}
						}
						$sql = "DROP TABLE IF EXISTS `_tempTable_IMPORT_" . ($_POST["selectImportTable"]) . "_USE`";
						#$rs = $dbConnection->db_query($sql);

						$sql = "DROP TABLE IF EXISTS `_tempTable_IMPORT_" . ($_POST["selectImportTable"]) . "`";
						#$rs = $dbConnection->db_query($sql);
					}
					// EOF IMPORT NOT EXISTING IMPORTED CUSTOMERS INTO CUSTOMERS
				}
				else {
					$errorMessage .= 'Die Daten konnten nicht in die Tabelle &quot;`_tempTable_IMPORT_' . ($_POST["selectImportTable"]) . '_USE`&quot; importiert werden! ' . '<br />';
				}

			}
			else {
				if($countRows1 < 1) {
					$errorMessage .= 'Die Tabelle &quot;`_tempTable_IMPORT_' . ($_POST["selectImportTable"]) . '_USE`&quot; existiert nicht mehr! ' . '<br />';
				}
				if($countRows2 < 1) {
					$errorMessage .= 'Die Tabelle &quot;`_tempTable_IMPORT_' . ($_POST["selectImportTable"]) . '`&quot; existiert nicht mehr! ' . '<br />';
				}
			}

dd('sql');
dd('_POST');
dd('_FILES');
dd('arrGetFields');
dd('arrUseFieldsImportTo');
dd('arrUseFieldsImportFrom');
dd('arrSqlImportFieldNamesTo');
dd('arrSqlImportFieldNamesFrom');
dd('arrSqlImportFieldNamesFromToPos');
dd('countRows1');
dd('countRows2');
dd('countRows');

echo serialize($arrSqlImportFieldNamesTo) . '<br />';
echo serialize($arrSqlImportFieldNamesFrom) . '<br />';
echo serialize($arrSqlImportFieldNamesFromToPos) . '<br />';
displayMessages();

		}
		else {
			if(empty($arrSqlImportFieldNamesTo)){
				$errorMessage .= 'yEs sind keine Felder der Ziel-Tabelle vorhanden!' . '<br />';
			}
			if(empty($arrSqlImportFieldNamesFrom)) {
				$errorMessage .= 'yEs sind keine Felder der Quellen-Tabelle vorhanden!' . '<br />';
			}
		}
	}
	else {
		if(isset($_POST["arrFieldsImportTo"]) && empty($_POST["arrFieldsImportTo"])){
			$errorMessage .= 'xEs sind keine Felder der Ziel-Tabelle vorhanden!' . '<br />';
		}
		if(isset($_POST["arrFieldsImportFrom"]) && empty($_POST["arrFieldsImportFrom"])) {
			$errorMessage .= 'xEs sind keine Felder der Quellen-Tabelle vorhanden!' . '<br />';
		}
	}
	if(!empty($arrFieldsImportTo) && !empty($arrFieldsImportFrom)){
		if($_POST["selectImportSettings"] != ''){
			$arrThisImportSettings = $arrImportSettingDatas[$_POST["selectImportSettings"]];
			$arrThisImportSettings["settingsImportFilesFieldNamesTo"] = unserialize($arrThisImportSettings["settingsImportFilesFieldNamesTo"]);
			$arrThisImportSettings["settingsImportFilesFieldNamesFrom"] = unserialize($arrThisImportSettings["settingsImportFilesFieldNamesFrom"]);
			$arrThisImportSettings["settingsImportFilesFieldNamesFromToPos"] = unserialize($arrThisImportSettings["settingsImportFilesFieldNamesFromToPos"]);
			$arrThisImportSettings["settingsImportFilesFieldNamesFromToPosFlip"] = array_flip($arrThisImportSettings["settingsImportFilesFieldNamesFromToPos"]);
		}

		echo '<form name="formImportFields" method="post" action="" enctype="multipart/form-data">';
		echo '<input type="hidden" name="selectImportTable" value="' . $_POST["selectImportTable"] . '" />';
		echo '<table border="0" width="100%" class="border">';

		echo '<tr>';
		echo '<th>Auftragslisten-Felder</th>';
		echo '<th>Import-Felder</th>';
		echo '</tr>';

		echo '<tr>';
		echo '<td class="fieldsImportTo">';
		echo '<ul>';
		foreach($arrFieldsImportTo as $thisKey => $thisValue) {
			echo '<li>';
			echo $thisValue;
			echo '<input type="hidden" name="arrFieldsImportTo[' . $thisKey . ']" value="' . $thisValue . '"/>';
			echo '</li>';
		}
		echo '</ul>';
		echo '</td>';
		echo '<td class="fieldsImportFrom">';
		echo '<ul>';
		$arrAddNonRelatedFieldFrom = array();
		$arrAddRelatedFieldFromKeys = array();
		$strRecordsArray = '';
		$arrRecordsArray = array();

		foreach($arrFieldsImportTo as $thisKey => $thisValue) {
			if(isset($arrThisImportSettings["settingsImportFilesFieldNamesFromToPosFlip"][$thisKey])){
				echo createRelatedFieldFrom($thisKey, $arrThisImportSettings["settingsImportFilesFieldNamesFrom"][$arrThisImportSettings["settingsImportFilesFieldNamesFromToPosFlip"][$thisKey]]);

				$arrAddNonRelatedFieldFrom[] = createNonRelatedFieldFrom($thisKey, $thisValue);
				$arrAddRelatedFieldFromKeys[] = $thisKey;
			}
			else {
				echo createNonRelatedFieldFrom($thisKey, $thisValue);
			}
		}

		foreach($arrFieldsImportFrom as $thisKey => $thisValue) {
			#dd('thisValue');
			#dd('arrThisImportSettings["settingsImportFilesFieldNamesFrom"]');
			if(!in_array(strtoupper($thisValue), $arrThisImportSettings["settingsImportFilesFieldNamesFrom"])){
				echo createRelatedFieldFrom($thisKey, $thisValue);
			}
			else {

			}
		}
		if(!empty($arrAddNonRelatedFieldFrom)){
			foreach($arrAddNonRelatedFieldFrom as $thisNonRelatedFieldFrom) {
				echo $thisNonRelatedFieldFrom;
			}
		}
		echo '</ul>';
		echo '</td>';
		echo '</tr>';
		echo '</table>';

		if(!empty($arrRecordsArray)){
			$strRecordsArray = implode('&', $arrRecordsArray);
		}
		#echo $strRecordsArray;
		foreach($_POST as $thisPostKey => $thisPostValue){
			echo '<input type="hidden" name="' . $thisPostKey . '" value="' . $thisPostValue . '" />';
		}
		#echo '<input type="hidden" name="arrFieldsImportFrom" id="arrFieldsImportFrom" value="" />';
		echo '<input type="hidden" name="arrFieldsImportFrom" id="arrFieldsImportFrom" value="' . $strRecordsArray . '" />';
		echo '<input type="submit" class="inputButton1" name="submitFieldsImport" value="Import starten" />';
		echo '</form>';
?>
		<script type="text/javascript" language="javascript">
			$(document).ready(function(){
				$(function() {
					function sortListItems(elementContainerList, elementField) {
						$(elementContainerList + " ul").sortable({
							opacity: 0.6,
							update: function() {
								$(elementField).val($(this).sortable("serialize"));
								// var order = $(this).sortable("serialize") + '&action=updateRecordsListings';
								//$.post("updateDB.php", order, function(theResponse){
									//$("#contentRight").html(theResponse);
								//});
								//alert($(elementField).val());
							}
						});
					}
					var animateTime = 200;
					sortListItems('.fieldsImportFrom', '#arrFieldsImportFrom');
					$('.buttonCopyItem').live('click', function () {
						var thisID = $(this).parent().parent().attr('id');
						// $(this).parent().parent().clone().appendTo($(this).parent().parent().parent());
						$(this).parent().parent().clone().insertAfter($(this).parent().parent());
						sortListItems('.fieldsImportFrom', '#arrFieldsImportFrom');
						$('#arrFieldsImportFrom').val($('.fieldsImportFrom ul').sortable("serialize"));
					});
					$('.buttonRemoveItem').live('click', function () {
						// $(this).parent().remove();
						var thisID = $(this).parent().parent().attr('id');
						$(this).parent().parent().parent().append($(this).parent().parent());
						sortListItems('.fieldsImportFrom', '#arrFieldsImportFrom');
						$('#arrFieldsImportFrom').val($('.fieldsImportFrom ul').sortable("serialize"));
					});
				});
			});
		</script>
	</div>
<?php
	}
// EOF SORT IMPORT FIELDS
?>

				<?php displayMessages(); ?>
			</div>
		</div>
		<div class="clear"></div>
	</div>
	<div class="clear"></div>
</div>

<script language="javascript" type="text/javascript">
	$(document).ready(function() {
		$(function() {
			$( "#datepicker" ).datepicker();
		});
		colorRowMouseOver('.displayOrders tbody tr');
	});
</script>

<?php require_once('inc/footerHTML.inc.php'); ?>