<?php

require_once('vendor/autoload.php');

use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\SMTP;
use PHPMailer\PHPMailer\Exceptioxn;

class createMail {	

	var $strMailContentType = '';
	var $strDocumentType = '';
	var $strDocumentNumber = '';
	#var $strNowDate = date("d.m.Y, H:i:s");
	
	var $strMailContent_HTML = '';
	var $strMailContent_TEXT = '';	
	
	var $strMailAdditionalText = '';
	var $strMailSendAttachedText = '';
	var $strMailSubject = '';
	
	var $arrMailResults = array();
	var $strMailImagesDirectory = '';
	var $strMailAttachmentDirectory = '';
	var $arrMailAttachmentFiles = array();		
	var $arrMailTextTemplate = array();
	var $arrMailBodyTemplate = array();
	var $arrRelatedDocuments = array();
	var $arrMailRecipients = '';
	var $strMailSender = '';
	var $boolSendAttachedDocument = false;
	var $boolMailSendResult = false;
	var $strDeliveryDatas = '';
	var $strAttachmentPathPrefix = '';
	var $strRecipientBcc = '';
	var $mail = "";

	public function __construct(
		$strMailSubject='',
		$strDocumentType='', 
		$strDocumentNumber='', 
		$arrMailAttachmentFiles=array(), 
		$arrMailRecipients='', 
		$arrMailTextTemplate=array(), 
		$arrMailBodyTemplate=array(), 
		$strMailAdditionalText='',
		$strMailSendAttachedText='',
		$boolSendAttachedDocument=false,
		$strMailImagesDirectory='',
		$strMailSender='',
		$strMailBcc=''
	) {
		//$this->htmlMimeMail();
		$this->mail = new PHPMailer(true);
		$this->setDocumentNumber($strDocumentNumber);
		$this->setRelatedDocuments(array($strDocumentNumber));
		$this->setMailSubject($strMailSubject);
		$this->setAttachmentFiles($arrMailAttachmentFiles);
		$this->setDocumentType($strDocumentType);			
		$this->setMailRecipients($arrMailRecipients);			
		$this->setMailTextTemplate($arrMailTextTemplate);
		$this->setMailBodyTemplate($arrMailBodyTemplate);
		$this->setMailAdditionaltext($strMailAdditionalText);
		$this->setMailSendAttachedText($strMailSendAttachedText);
		$this->setSendAttachedDocument($boolSendAttachedDocument);			
		$this->setMailImagesDirectory($strMailImagesDirectory);		
		$this->setMailSender($strMailSender);
		$this->setMailRecipientBcc($strMailBcc);
		$this->createMailContent();			
		
		// BOF RETURN RESULT			
		return $this->boolMailSendResult;
		// EOF RETURN RESULT
	}
	

		
	private function setDocumentNumber($strDocumentNumber){
		$this->strDocumentNumber = $strDocumentNumber;
	}

	private function setRelatedDocuments($arrRelatedDocuments=array()){
		$this->arrRelatedDocuments = getRelatedDocuments($arrRelatedDocuments);			
	}

	public function setMailSubject($strMailSubject){
		$this->strMailSubject = $strMailSubject;	
		$this->strMailSubject = preg_replace('/{###DOCUMENT_NUMBER###}/', $this->strDocumentNumber, $this->strMailSubject);
		$this->strMailSubject = preg_replace('/{###DOCUMENT_AB_NUMBER###}/', $this->arrRelatedDocuments["AB"] . ' ('.$this->strDocumentNumber . ')', $this->strMailSubject);			
	}

	private function setAttachmentFiles($arrMailAttachmentFiles){
		$this->arrMailAttachmentFiles = $arrMailAttachmentFiles;
	}
	
	private function setDocumentType($strDocumentType){
		$this->strDocumentType = $this->strDocumentType;
	}

	public function setMailRecipients($arrMailRecipients){
		if(preg_match("/;/", $arrMailRecipients)){
			$arrTemp = explode(";", $arrMailRecipients);
			if(!empty($this->arrMailRecipients)){
				$this->arrMailRecipients = array_merge($this->arrMailRecipients, $arrTemp);
			}
			else {
				$this->arrMailRecipients = $arrTemp;
			}	
		}
		else if(preg_match("/,/", $arrMailRecipients)){
			$arrTemp = explode(",", $arrMailRecipients);
			$this->arrMailRecipients = array_merge($this->arrMailRecipients, $arrTemp);
		}
		else {
			$this->arrMailRecipients = $arrMailRecipients;
		}					
		$this->arrMailRecipients = array_unique($this->arrMailRecipients);

		if(is_array($this->arrMailRecipients))
		{
			foreach($this->arrMailRecipients as $mail)
			{
				$this->mail->addAddress($mail); 
			}
		}else{
			$this->arrMailRecipients = $arrMailRecipients;
			$this->mail->addAddress($arrMailRecipients); 
		}
 
	}
		
 
	 

	private function setMailTextTemplate($arrMailTextTemplate){
		$this->arrMailTextTemplate = $arrMailTextTemplate;
	}
	private function setMailBodyTemplate($arrMailBodyTemplate){
		$this->arrMailBodyTemplate = $arrMailBodyTemplate;
	}
	
	private function setMailAdditionaltext($strMailAdditionalText){
		$this->strMailAdditionalText = $strMailAdditionalText;
	}

	public function setMailSendAttachedText($strMailSendAttachedText){			
		$this->strMailSendAttachedText = $strMailSendAttachedText;	
	}

	public function setSendAttachedDocument($boolSendAttachedDocument){			
		$this->boolSendAttachedDocument = $boolSendAttachedDocument;	
	}

	public function setMailImagesDirectory($strMailImagesDirectory){			
		$this->strMailImagesDirectory = $strMailImagesDirectory;			
	}

	public function setMailSender($strMailSender){
		$this->strMailSender = $strMailSender;
	}

	public function setMailRecipientBcc($strRecipientBcc){
		$this->strRecipientBcc = $strRecipientBcc;
		if(!empty($this->strRecipientBcc))
		{
			$this->mail->addBCC($this->strRecipientBcc);	
		}
			
	}

	private function setMailDatas(){			
		$this->mail->CharSet = 'UTF-8';

		$this->mail->Encoding = 'base64';
		 
		
		if($this->strMailSender != ''){
	
			$this->mail->setFrom(MAIL_ADDRESS_FROM);
			//$this->mail->setFrom(MAIL_ADDRESS_FROM_NAME);
			$this->mail->ReplyTo(MAIL_ADDRESS_REPLY_TO);
		}
		
		if($this->strRecipientBcc != ''){
			$this->mail->addBCC($this->strRecipientBcc);
		} 
		
		//$this->setHeader('Errors-To', MAIL_ERRORS_TO);
		//$this->setHeader('Return-Path', MAIL_ERRORS_TO); 

		$this->mail->Subject = $this->strMailSubject;			
		//$this->setHeader('Date', date('D, d M y H:i:s O'));
	}
	

	public function createMailContent(){			
			// BOF CREATE HTML MAIL TO CUSTOMER
				#if(!empty($this->arrMailBodyTemplate) || $this->arrMailBodyTemplate != ''){
				if(1){
					$this->strMailContent_HTML = $this->arrMailBodyTemplate;
					
					$this->strMailContent_HTML = preg_replace('/{###MAIL_HEADER_IMAGE###}/',$this->arrMailTextTemplate["MAIL_HEADER_IMAGE"] , $this->strMailContent_HTML);
					 
					#$this->strMailContent_HTML = preg_replace('/{###MAIL_TITLE###}/',$this->arrMailTextTemplate["MAIL_TITLE"], $this->strMailContent_HTML);
					$this->strMailContent_HTML = preg_replace('/{###MAIL_TITLE###}/',$this->arrMailTextTemplate["MAIL_TITLE"], $this->strMailContent_HTML);
					$this->strMailContent_HTML = preg_replace('/{###MAIL_DATE_VALUE###}/', date("d.m.Y, H:i:s"), $this->strMailContent_HTML);

					#$this->strMailContent_HTML = preg_replace('/{###MAIL_SALUTATION###}/', $this->arrMailTextTemplate["MAIL_SALUTATION"], $this->strMailContent_HTML);

					if(($this->boolSendAttachedDocument == '1' || $this->boolSendAttachedDocument == true) && $this->strMailSendAttachedText != '') {
						$this->strMailContent_HTML = preg_replace('/{###MAIL_CONTENT###}/', nl2br($this->strMailSendAttachedText), $this->strMailContent_HTML);
						$this->strMailContent_HTML = preg_replace('/{###MAIL_SALUTATION###}/', '', $this->strMailContent_HTML);
					}
					else {
						if($this->strMailAdditionalText != "") {					
							$this->strMailContent_HTML = preg_replace('/{###MAIL_CONTENT###}/', ($this->strMailAdditionalText), $this->strMailContent_HTML);
							$this->strMailContent_HTML = preg_replace('/{###MAIL_SALUTATION###}/', '', $this->strMailContent_HTML);
							$this->strMailContent_HTML = preg_replace('/{###MAIL_REGARDS###}/', '', $this->strMailContent_HTML);
						}
						$this->strMailContent_HTML = preg_replace('/{###MAIL_CONTENT###}/', $this->arrMailTextTemplate["MAIL_CONTENT"], $this->strMailContent_HTML);
						$this->strMailContent_HTML = preg_replace('/{###MAIL_SALUTATION###}/', $this->arrMailTextTemplate["MAIL_SALUTATION"], $this->strMailContent_HTML);
					}
					$this->strMailContent_HTML = preg_replace('/{###MAIL_REGARDS###}/', $this->arrMailTextTemplate["MAIL_REGARDS"], $this->strMailContent_HTML);
					$this->strMailContent_HTML = preg_replace('/{###DOCUMENT_NUMBER###}/', $this->strDocumentNumber, $this->strMailContent_HTML);			
					$this->strMailContent_HTML = preg_replace('/{###DOCUMENT_TYPE_NAME###}/', '', $this->strMailContent_HTML);			
					$this->strMailContent_HTML = preg_replace('/{###DOCUMENT_AB_NUMBER###}/', 'Auftrag ' . $this->arrRelatedDocuments["AB"] . ' / '.$this->strDocumentNumber . '' , $this->strMailContent_HTML);			
					$this->strMailContent_HTML = preg_replace('/{###DELIVERY_TRACKING_NUMBER###}/', $this->strDeliveryDatas, $this->strMailContent_HTML);			
					$this->strMailContent_HTML = preg_replace('/{###MAIL_SIGNATURE###}/', $this->arrMailTextTemplate["MAIL_SIGNATURE"], $this->strMailContent_HTML);
					$this->strMailContent_HTML = preg_replace('/{###MAIL_COPYRIGHT###}/', $this->arrMailTextTemplate["MAIL_COPYRIGHT"], $this->strMailContent_HTML);
				}
				if($this->strMailContent_HTML == '' && $this->strMailAdditionalText != ''){
					$this->strMailContent_HTML = $this->strMailAdditionalText;
				}
				$this->strMailContent_HTML = removeUnnecessaryChars($this->strMailContent_HTML);
			// EOF CREATE HTML MAIL TO CUSTOMER				
			
			// BOF CREATE TEXT MAIL
				$this->strMailContent_TEXT = $this->strMailContent_HTML;
				$this->strMailContent_TEXT = COMPANY_NAME_LONG . "<hr />" . $this->strMailContent_TEXT;
				$this->strMailContent_TEXT = "" . convertHtmlToText($this->strMailContent_TEXT);	
				// BOF BUG EBNER NO ATTACHMENTS?
				$this->strMailContent_TEXT = '';
				// EOF BUG EBNER NO ATTACHMENTS?
			// EOF CREATE TEXT MAIL
			
			// BOF SET MAIL DATAS
				$this->setMailDatas();
			// EOF SET MAIL DATAS
			
			// BOF ADD CONTENT 
				$this->mail->isHTML(true);    
				$this->mail->Subject = $this->strMailSubject;
				$this->mail->Body = $this->strMailContent_HTML;
				$this->mail->AltBody = $this->strMailContent_TEXT; 

				 
			// EOF ADD CONTENT	

		// BOF ADD ATTACHMENT						
			if(!empty($this->arrMailAttachmentFiles)) {			
				foreach($this->arrMailAttachmentFiles as $thisFile => $thisFileName) {
					if(file_exists(utf8_decode($thisFile))) {
						// BOF GET ATTACHMENT MIME TYPE
						$thisFileMimeType = '';
						$thisFileMimeType = $this->getMimeTypes('', ($thisFile));
						// BOF GET ATTACHMENT MIME TYPE						
						//$this->addAttachment(utf8_decode($thisFile)), utf8_decode($thisFileName), $thisFileMimeType);	
						$this->mail->addAttachment(utf8_decode($thisFile), utf8_decode($thisFileName));										
					}					
				}
			}
			if(file_exists(PATH_VCARD)) {
				$this->mail->addAttachment(PATH_VCARD, 'Kontakt_vCard.vcf');
			}
			else if(file_exists($this->strAttachmentPathPrefix . PATH_VCARD)) {
				$this->mail->addAttachment($this->strAttachmentPathPrefix . PATH_VCARD, 'Kontakt_vCard.vcf');				
			}
			if(file_exists(PATH_AGB_PDF)) {
				$this->mail->addAttachment(PATH_AGB_PDF, 'AGB.pdf');
			}
			else if(file_exists($this->strAttachmentPathPrefix . PATH_AGB_PDF)) {
				$this->mail->addAttachment($this->strAttachmentPathPrefix . PATH_AGB_PDF, 'AGB.pdf');
			}			
			clearstatcache();			
			// EOF ADD ATTACHMENT
			
			// BOF ADD RECIPIENTS AND SEND
			if(!empty($this->arrMailRecipients)){
				if(MAIL_SEND_TYPE == 'smtp') {
					//$this->setSMTPParams(MAIL_ACCOUNT_SMTP, MAIL_ACCOUNT_PORT, MAIL_ACCOUNT_HELO, MAIL_ACCOUNT_AUTH, MAIL_ACCOUNT_USER, MAIL_ACCOUNT_PASSWORD);
					$this->mail->SMTPDebug = 0;
					$this->mail->isSMTP(); 

					$this->mail->SMTPOptions = array(
						'ssl' => array(
							'verify_peer' => false,
							'verify_peer_name' => false,
							'allow_self_signed' => true
						)
					);
					$this->mail->Host 	   = 	MAIL_ACCOUNT_SMTP;
					$this->mail->Port      = 	MAIL_ACCOUNT_PORT;
					$this->mail->SMTPAuth  = 	MAIL_ACCOUNT_AUTH;
					$this->mail->Username  = 	MAIL_ACCOUNT_USER;
					$this->mail->Password  =	MAIL_ACCOUNT_PASSWORD; 
					$this->mail->SMTPSecure = 	PHPMailer::ENCRYPTION_STARTTLS; 

					
					
					$this->mail->setFrom(MAIL_ADDRESS_FROM,MAIL_ADDRESS_FROM_NAME);
					//MAIL_ADDRESS_FROM_NAME 
					$this->boolMailSendResult = $this->mail->send();
					
					if(!$this->boolMailSendResult) {					
						$this->boolMailSendResult = 1;	
					}
				}
				else {
					$this->boolMailSendResult = 0;
				}
			}
			else {
				#unset($this->boolMailSendResult);
				$this->boolMailSendResult = null;
			}			
			// EOF ADD RECIPIENTS AND SEND
			
			// BOF LOG SENDED MAIL
		//$this->logSendedMails();
			// EOF LOG SENDED MAIL
	}

	private function getMimeTypes($fileContentType, $fileName){
		// BOF WORKAROUND FOR PDF NOT SHOWN AS mimetype: text/plain
		$fileName = basename($fileName);
		#if ($fileContentType = 'text/plain') {
		if(1){
			$arrMimeTypes = array(
				'ai' => 'application/postscript',
				'avi' => 'video/avi',
				'bmp' => 'image/bmp',
				'css' => 'text/css',
				'doc' => 'application/msword',
				'docm' => 'application/vnd.ms-word.document.macroEnabled.12',
				'docx' => 'application/vnd.openxmlformats-officedocument.wordprocessingml.document',
				'dot' => 'application/msword',
				'dotm' => 'application/vnd.ms-word.template.macroEnabled.12',
				'dotx' => 'application/vnd.openxmlformats-officedocument.wordprocessingml.template',
				'gz' => 'application/x-gzip',
				'gzip' => 'application/x-gzip',
				'help' => 'application/x-helpfile',
				'htm' => 'text/html',
				'html' => 'text/html',
				'ico' => 'image/x-icon',
				'ics' => 'text/x-vcalendar',
				'jpe' => 'image/jpeg',
				'jpeg' => 'image/jpeg',
				'jpg' => 'image/jpeg',
				'js' => 'text/javascript',
				'log' => 'text/plain',
				'mov' => 'video/quicktime',
				'mp3' => 'audio/mpeg3',
				'odc' => 'application/vnd.oasis.opendocument.chart',
				'odf' => 'application/vnd.oasis.opendocument.formula',
				'odg' => 'application/vnd.oasis.opendocument.graphics',
				'odi' => 'application/vnd.oasis.opendocument.image',
				'odp' => 'application/vnd.oasis.opendocument.presentation',
				'ods' => 'application/vnd.oasis.opendocument.spreadsheet',
				'odt' => 'application/vnd.oasis.opendocument.text',
				'otc' => 'application/vnd.oasis.opendocument.chart-template',
				'otf' => 'application/vnd.oasis.opendocument.formula-template',
				'otg' => 'application/vnd.oasis.opendocument.graphics-template',
				'oth' => 'application/vnd.oasis.opendocument.text-web',
				'oti' => 'application/vnd.oasis.opendocument.image-template',
				'otm' => 'application/vnd.oasis.opendocument.text-master',
				'otp' => 'application/vnd.oasis.opendocument.presentation-template',
				'ots' => 'application/vnd.oasis.opendocument.spreadsheet-template',
				'ott' => 'application/vnd.oasis.opendocument.text-template',
				'pdf' => 'application/pdf',
				'pot' => 'application/vnd.ms-powerpoint',
				'pps' => 'application/vnd.ms-powerpoint',
				'ppsm' => 'application/vnd.ms-powerpoint.slideshow.macroEnabled.12',
				'ppsx' => 'application/vnd.openxmlformats-officedocument.presentationml.slideshow',
				'ppt' => 'application/vnd.ms-powerpoint',
				'pptm' => 'application/vnd.ms-powerpoint.presentation.macroEnabled.12',
				'pptx' => 'application/vnd.openxmlformats-officedocument.presentationml.presentation',
				'rar' => 'application/x-rar-compressed',
				'swf' => 'application/x-shockwave-flash',
				'tar' => 'application/x-tar',
				'text' => 'text/plain',
				'txt' => 'text/plain',
				'tif' => 'image/tiff',
				'vcf' => 'text/x-vcard',
				'vcs' => 'text/x-vcalendar',
				'wav' => 'audio/wav',
				'xla' => 'application/vnd.ms-excel',
				'xlc' => 'application/vnd.ms-excel',
				'xlm' => 'application/vnd.ms-excel',
				'xls' => 'application/vnd.ms-excel',
				'xlsb' => 'application/vnd.ms-excel.sheet.binary.macroEnabled.12',
				'xlsm' => 'application/vnd.ms-excel.sheet.macroEnabled.12',
				'xlsx' => 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet',
				'xlt' => 'application/vnd.ms-excel',
				'xlw' => 'application/vnd.ms-excel',
				'xml' => 'text/xml',
				'xps' => 'application/vnd.ms-xpsdocument',
				'zip' => 'application/zip',			
			);
		
			if($fileName) {
				// get extention
				$fileNameParts = explode(".", $fileName);
				$fileExtension = $fileNameParts[1];	

				//$fileNameParts = split(".", $fileName);					
				//$fileExtension = end($fileNameParts);
				if ($fileExtension && isset($arrMimeTypes[$fileExtension])){
					$fileContentType = $arrMimeTypes[$fileExtension];
				}
			}
		}		
		// EOF WORKAROUND FOR PDF NOT SHOWN AS mimetype: text/plain	
		return $fileContentType;
	}
	
 
}
?>