<?php

	class DB_Connection {

		var $db_host;
		var $db_port;
		var $db_name;
		var $db_user;
		var $db_password;
		var $db_open;
		var $db_select;
		var $db_close;
		var $db_charset = 'utf8';
		var $db_errors = array();
		var $db_debug = true;
		var $db_query_result;

		public function __construct($db_host = DB_HOST, $db_port = '', $db_name = DB_NAME, $db_user = DB_USER, $db_password = DB_PASSWORD) {		
		// echo 	$db_host."-".$db_port."-".$db_name."-".$db_user."-".$db_password;
			// exit();
			$curPageName = substr($_SERVER["SCRIPT_NAME"],strrpos($_SERVER["SCRIPT_NAME"],"/")+1);  

			$this->db_host = "localhost";//$db_host;
			$this->db_name = "burhanctrwawineu";//$db_name;
			$this->db_user = "root";//$db_user;
			$this->db_password = "";//$db_password
			if($db_port=='') $db_port = 3307;//$db_port;
		//	if($curPageName == 'quickLinks.php'){$db_port = 3306;}
			$this->db_port = $db_port;
			$this->db_query_result;
		
		 
		}

		public function db_connect() {
			if(!$this->db_open) {
				// $this->db_open = mysqli_connect($this->db_host, $this->db_user, $this->db_password) or die('no db-connex: ' . mysqli_error($this->db_open));
				//echo $this->db_host ."+". $this->db_user."+". $this->db_password;
				$this->db_open = mysqli_connect($this->db_host, $this->db_user, $this->db_password, $this->db_name,$this->db_port) or die("can't connect to host--".$this->db_host.'--'. $this->db_user.'--'. $this->db_password.'--'. $this->db_name. mysqli_connect_error());
				if(!$this->db_open) {
					$this->db_errors[] = 'no db-connex: ' . mysqli_error($this->db_open);
					$this->db_errorLog('no db-connex: ' . mysqli_error($this->db_open));
				// echo $this->db_host.'+'.$this->db_user.'+'. $this->db_password;
				// exit();
				}
				$this->db_setCharset($this->db_charset);
				if(mysqli_error($this->db_open)) {
					$this->db_errors[] = mysqli_error($this->db_open);
					$this->db_errorLog(mysqli_error($this->db_open));
				}
				if($this->db_open) {
					// $this->db_select = mysqli_select_db($this->db_name, $this->db_open);
					if(mysqli_error($this->db_open)) {
						$this->db_errors[] = 'no db-select: ' . mysqli_error($this->db_open);
						$this->db_errorLog('no db-select: ' . mysqli_error($this->db_open));
						$this->db_outputNoDbConnex();
					}
					return $this->db_open;
				}
				else {
					return false;
				}
			}
		}
		public function db_close() {
			if($this->db_open) {
				$this->db_close = mysqli_close($this->db_open);
			}
			return $this->db_close;
		}
		public function db_query($sql) {			
			$this->db_sqlLog($sql);
			if(!$this->db_open) {
				$this->db_connect();
			}
			if($this->db_open && $sql != '') {
				$this->db_query_result = mysqli_query($this->db_open, $sql );
				if(mysqli_error($this->db_open)) {
					$this->db_errors[] = "SQL: " . $sql . '<br />ERROR: ' . mysqli_error($this->db_open) . '<br /><br />';
					#$this->db_errorLog($sql . '<br />ERROR: ' . mysqli_error($this->db_open));
					$this->db_errorLog('ERROR: ' . mysqli_error($this->db_open));
				}
				return $this->db_query_result;
			}
			else {
				return false;
			}
		}

		public function db_setCharset($charset) {
			if($charset != '') {
				$this->db_charset = $charset;			
			}

			mysqli_set_charset($this->db_charset, $this->db_open); // diese Zeile für PHP ab Version 5.2.3 
			mysqli_query($this->db_open, "SET NAMES '".$this->db_charset."'"); // diese Zeile für PHP bis Version 5.2.3
			mysqli_query($this->db_open, "SET CHARACTER SET ".$this->db_charset.""); // diese Zeile für PHP bis Version 5.2.3
			if(mysqli_error($this->db_open)) {
				$this->db_errors[] = mysqli_error($this->db_open);
				$this->db_errorLog(mysqli_error($this->db_open));
			}
		}

		public function db_displayErrors() {
			
			if(!empty($this->db_errors)) {
				return implode('<br />', $this->db_errors);
			}
		}

		public function db_getInsertID() {
			return mysqli_insert_id($this->db_open);
		}
		
		public function db_getMysqlNumRows($result){			
			$thisResult = mysqli_num_rows($result);
			if(mysqli_error($this->db_open)) {
				$this->db_errors[] = mysqli_error($this->db_open);
				$this->db_errorLog(mysqli_error($this->db_open));
			}
			return $thisResult;			
		}
		
		public function db_getMysqlAffectedRows($result){			
			$thisResult = mysqli_affected_rows($result);
			if(mysqli_error($this->db_open)) {
				$this->db_errors[] = mysqli_error($this->db_open);
				$this->db_errorLog(mysqli_error($this->db_open));
			}
			return $thisResult;			
		}

		public function db_errorLog($error) {
			if(LOG_SQL_QUERY_ERRORS){
				$content = '';
				$content .= date('Y-m-d H:i:s') . "\t";
				$content .= $_SERVER["HTTP_HOST"] . "\t";
				$content .= $_SERVER["PHP_SELF"] . "\t";
				$content .= MANDATOR . "\t";
				$content .= $_SESSION["usersID"] . "\t";
				$error = $this->cleanQuery($error);
				$content .= $error . "\n";
				$fp = fopen(PATH_LOGFILES_DB_QUERY_ERRORS, 'a+');
				fwrite($fp, $content);
				fclose($fp);
			}
		}
		
		public function db_sqlLog($sql) {		
			#if(LOG_SQL_QUERY){
			if(LOG_SQL_QUERY){
				$content = '';				
				$content .= date('Y-m-d H:i:s') . "\t";
				$content .= $_SERVER["HTTP_HOST"] . "\t";
				$content .= $_SERVER["PHP_SELF"] . "\t";
				$content .= MANDATOR . "\t";
				$content .= $_SESSION["usersID"] . "\t";
				$sql = $this->cleanQuery($sql);
				$content .= $sql . "\n";
				if(substr(trim($sql), 0, 6) != 'SELECT'){
					$fp = fopen(PATH_LOGFILES_DB_QUERY, 'a+');
					fwrite($fp, $content);
					fclose($fp);
				}				
			}
		}
		
		public function cleanQuery($query) {
			$query = preg_replace("/\\t/", " ", $query);
			$query = preg_replace("/\\r/", " ", $query);
			$query = preg_replace("/\\n/", " ", $query);
			while(preg_match("/  /", $query)){
				$query = preg_replace("/  /", " ", $query);
			}
			return $query;
		}
		
		public function db_outputNoDbConnex(){			
			$message = '<div style="color:#FF0000;font-size:10px;">';
			$message .= 'Es kann keine Verbindung zur Datenbank hergestellt werden.';
			$message .= '</div>';
			#die($message);
			print($message);			
		}
	}
?>
