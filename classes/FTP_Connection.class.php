<?php
	class FTP_Connection {

		var $fs_host;
		var $fs_port;
		var $fs_name;
		var $fs_user;
		var $fs_password;
		var $fs_open;
		var $fs_login;
		var $fs_close;
		var $fs_mode = FTP_BINARY; //  FTP_ASCII or FTP_BINARY
		var $fs_errors = array();
		var $fs_debug = true;
		var $fs_query_result;

		function __construct($fs_host = FTP_SERVER, $fs_port = FTP_PORT, $fs_user = FTP_LOGIN, $fs_password = FTP_PASSWORD) {			
			$this->fs_host = $fs_host;
			$this->fs_name = $fs_name;
			$this->fs_user = $fs_user;
			$this->fs_password = $fs_password;
			$this->fs_port = $fs_port;
			$this->fs_query_result;
		}

		function fs_connect() {
			if(!$this->fs_open) {				
				$this->fs_open = ftp_connect($this->fs_host);
				$this->fs_login = ftp_login($this->fs_open, $this->fs_user, $this->fs_password);
				$this->fs_setMode($this->fs_mode);
				if(!$this->fs_open) {
					$this->fs_errors[] = 'no ftp-connex: ' . ($this->fs_open);
					$this->fs_errorLog('no ftp-connex: ' . ($this->fs_open));
					die('Es kann keine Verbindung zum FTP-Server hergestellt werden.');
				}
				if(!$this->fs_login) {
					$this->fs_errors[] = 'no ftp-login: ' . ($this->fs_login);
					$this->fs_errorLog('no ftp-login: ' . ($this->fs_login));
					die('Es kann kein Login zum FTP-Server hergestellt werden.');
				}
				return $this->fs_open;				
			}
			else {
				return false;
			}
		}
		function fs_close() {
			if($this->fs_open) {
				$this->fs_close = ftp_close($this->fs_open);
			}
			return $this->fs_close;
		}
		function fs_query($cmd) {
			if(!$this->fs_open) {
				$this->fs_connect();
			}
			if($this->fs_open && $cmd != '') {
				$this->fs_query_result = ftp_exec($this->fs_open, $cmd);
				if(!$this->fs_query_result) {
					$this->fs_errors[] = "FTP: " . $cmd . '<br />ERROR: ' . ($this->fs_query_result) . '<br /><br />';
					$this->fs_errorLog(($this->fs_query_result));
				}
				return $this->fs_query_result;
			}
			else {
				return false;
			}
		}
		
		function fs_setMode($mode) {
			if($mode != '') {
				$this->fs_mode = $mode;			
			}
		}
		
		function fs_put($fileFrom, $fileTo) {
			$fp = fopen($fileFrom, 'r');
			$this->fs_query_result = ftp_fput($this->fs_open, $fp, $fileTo, $this->fs_mode);
			if(!$this->fs_query_result) {
				$this->fs_errors[] = "FTP: " . $fileFrom . ' TO ' . $fileTo . '<br />ERROR: ' . ($this->fs_query_result) . '<br /><br />';
				$this->fs_errorLog(($this->fs_query_result));
			}
		}
		
		function fs_get($fileFrom, $fileTo) {
			$fp = fopen($fileTo, 'w');
			$this->fs_query_result = ftp_get($this->fs_open, $fp, $fileFrom, $this->fs_mode);
			if(!$this->fs_query_result) {
				$this->fs_errors[] = "FTP: " . $fileTo . ' TO ' . $fileFrom . '<br />ERROR: ' . ($this->fs_query_result) . '<br /><br />';
				$this->fs_errorLog(($this->fs_query_result));
			}
		}
		
		function fs_fileExists($filePath) {
			$arrThisFileList = ftp_nlist($this->fs_open, $filePath);
			if(!empty($arrThisFileList)){
				if(in_array($filePath, $arrThisFileList)){
					return true;
				}
				else {
					return false;
				}
			}
			else {
				return false;
			}
		}
		
		function fs_getListDir($filePath) {
			$arrThisFileList = ftp_nlist($this->fs_open, $filePath);
			return $arrThisFileList;
		}

		function fs_displayErrors() {
			if(!empty($this->fs_errors)) {
				return implode('<br />', $this->fs_errors);
			}
		}

		function fs_errorLog($error) {
			if(LOG_FTP_QUERY_ERRORS){
				$content = '';
				$content .= date('Y-m-d H:i:s') . "\t";
				$content .= $_SERVER["HTTP_HOST"] . "\t";
				$content .= $_SERVER["PHP_SELF"] . "\t";
				$content .= MANDATOR . "\t";
				$content .= $_SESSION["usersID"] . "\t";
				$error = $this->cleanQuery($error);
				$content .= $error . "\n";
				$fp = fopen(PATH_LOGFILES_FTP_QUERY_ERRORS, 'a+');
				fwrite($fp, $content);
				fclose($fp);
			}
		}
		
		function fs_sqlLog($cmd) {		
			if(LOG_FTP_QUERY){
				$content = '';				
				$content .= date('Y-m-d H:i:s') . "\t";
				$content .= $_SERVER["HTTP_HOST"] . "\t";
				$content .= $_SERVER["PHP_SELF"] . "\t";
				$content .= MANDATOR . "\t";
				$content .= $_SESSION["usersID"] . "\t";
				$cmd = $this->cleanQuery($cmd);
				$content .= $cmd . "\n";
				$fp = fopen(PATH_LOGFILES_FTP_QUERY, 'a+');
				fwrite($fp, $content);
				fclose($fp);
			}
		}
		
		function cleanQuery($query) {
			$query = preg_replace("/\\t/", " ", $query);
			$query = preg_replace("/\\r/", " ", $query);
			$query = preg_replace("/\\n/", " ", $query);
			while(preg_match("/  /", $query)){
				$query = preg_replace("/  /", " ", $query);
			}
			return $query;
		}
	}
?>