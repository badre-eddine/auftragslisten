<?php
	class googleMaps extends DB_Connection {
		var $arrColorsPLZ = array(
			'#FFF200',
			'#FDB913',
			'#F58220',
			'#E31D3C',
			'#F58F98',
			'#9A258F',
			'#0094C9',
			'#00C0E8',
			'#008060',
			'#23AA4A',
			'#80C342',
			'#905501',

			#'#FFCCFF',
			#'#FFCC99',
			#'#CC6600',
			#'#99CCCC',
			#'#66FF33',
			#'#FFFF33',
			#'#9acd32',
			#'#ee82ee',
			#'#d2b48c',
			#'#66cdaa',
			#'#98fb98',
			#'#00bfff',
			#'#f0b488',
		);
		var $strGoogleMapsCanvas		= '';
		var $strGoogleMapsCanvasID		= '';
		var $strGoogleMapsCanvasWidth	= '100%';
		var $strGoogleMapsCanvasHeight	= '($(window).height() - 30) + "px"';
		var $arrGoogleMapZipCodeAreas	= array();
		var $arrGoogleMapCountryAreas 	= array();
		var $arrGoogleMapPoints 		= array();
		var $strGoogleMapPoints 		= '';
		var $strGoogleMapsJsDatas 		= '';
		
		var $strDefaultPointLat			= "52.103138";
		var $strDefaultPointLon			= "7.622817";
		var $strDefaultZoom				= 6;

		var	$boolShowCountryAreas		= true;	// SHOW_COUNTRY_AREAS
		var	$boolShowZipCodeAreas		= true; // SHOW_ZIPCODE_AREAS
		
		var $arrZipCodeAreaDetailDatas	= array();
		var $arrZipCodes	= array();
		var $arrZipCodesPerLength	= array();
		var $arrZipCodesToSalesman	= array();
		
		var $strJsParams = '';
	
		function __construct($strGoogleMapsCanvasID){			
			$this->setGoogleMapsCanvasID($strGoogleMapsCanvasID);			
		}
		
		function setGoogleMapsCanvasID($strGoogleMapsCanvasID){
			$this->strGoogleMapsCanvasID = $strGoogleMapsCanvasID;
		}
		
		function setDefaultPointLatLon($strDefaultPointLat, $strDefaultPointLon){
			$this->strDefaultPointLat = $strDefaultPointLat;
			$this->strDefaultPointLon = $strDefaultPointLon;
		}
		
		function setZipCodeAreaDetailDatas($arrZipCodeAreaDetailDatas){
			$this->arrZipCodeAreaDetailDatas = $arrZipCodeAreaDetailDatas;
			$this->setZipCodes();
			#$this->getGoogleZipcodeAreas();
		}
		
		function setZipCodes(){	
			if(!empty($this->arrZipCodeAreaDetailDatas)){				
				foreach($this->arrZipCodeAreaDetailDatas as $thisKey => $thisValue){
					$this->arrZipCodes[] = $thisKey;
					$this->arrZipCodesPerLength[strlen($thisKey)][] = $thisKey;	
					$this->arrZipCodesToSalesman[$thisKey] = $thisValue["kundenID"];
				}				
			}			
		}
		
		function setShowCountryAreas($boolShowCountryAreas){
			$this->boolShowCountryAreas = $boolShowCountryAreas;
		}
		function setShowZipCodeAreas($boolShowZipCodeAreas){
			$this->boolShowZipCodeAreas = $boolShowZipCodeAreas;
		}		
		
		function setDefaultZoom($strDefaultZoom){
			$this->strDefaultZoom = $strDefaultZoom;
		}
		
		function setJsParams($arrJsParams){
			$this->strJsParams = '';
			if(is_array($arrJsParams)){
				if(!empty($arrJsParams)){
					foreach($arrJsParams as $thisKey => $thisValue){
						$this->strJsParams .= 'var ' . $thisKey . ' = "' . $thisValue . '"';
					}
				}
			}
		}
		
		function getJsParams(){
			return $this->strJsParams;
		}
		
		// BOF GET ZIPCODE AREAS
		function getGoogleZipcodeAreas(){
			$dbConnection = new DB_Connection();
			$arrSQL = array();
			
			if(!empty($this->arrZipCodesPerLength)){
				#echo '<pre>';
				#print_r($this->arrZipCodesPerLength);
				#echo '</pre>';
				foreach($this->arrZipCodesPerLength as $thisKey => $thisValue){					
					
					if($thisKey < 3){
						if($thisKey == 2){
							$arrSQL[] = "
									SELECT
										`zipcodeDoubleDigit`,
										`coordinates`

										FROM `" . TABLE_GEODB_DOUBLEDIGIT_AREAS . "`

										WHERE 1
											AND (
												`zipcodeDoubleDigit` = '" . implode("' OR `zipcodeDoubleDigit` = '", $thisValue) . "' 
											)
								";
						}
						else if($thisKey == 1){
							$arrSQL[] = "
									SELECT
										`zipcodeDoubleDigit`,
										`coordinates`

										FROM `" . TABLE_GEODB_DOUBLEDIGIT_AREAS . "`

										WHERE 1
											AND (
												`zipcodeDoubleDigit` LIKE '" . implode("%' OR `zipcodeDoubleDigit` LIKE '", $thisValue) . "%' 
											)
								";
						}
					}
					else if($thisKey < 6){
						if($thisKey == 5){
							$arrSQL[] = "
									SELECT
										`zipcode` AS `zipcodeDoubleDigit`,
										`coordinates`
										FROM `" . TABLE_GEODB_ZIPCODE_AREAS . "`
										WHERE 1
											AND (
												`zipcode` = '" . implode("' OR `zipcode` = '", $thisValue) . "' 
											)
								";
						}
						else {
							$arrSQL[] = "
									SELECT
										`zipcode` AS `zipcodeDoubleDigit`,
										`coordinates`
										FROM `" . TABLE_GEODB_ZIPCODE_AREAS . "`
										WHERE 1
											AND (
												`zipcode` LIKE '" . implode("%' OR `zipcode` LIKE '", $thisValue) . "%' 
											)
								";
						}
					}
				}
			}
			
			if(!empty($arrSQL)){
				$sql = implode(" UNION ", $arrSQL);

				$rs = $dbConnection->db_query($sql);
				$arrGoogleMapDoubleDigitZipCodeAreas = array();
				while($ds = mysqli_fetch_assoc($rs)) {
					$thisCoordinates = $ds["coordinates"];				

					if(strlen($ds["zipcodeDoubleDigit"]) < 3){					
						$searchPattern = "/(\[)([0-9\.\-]*),([0-9\.\-]*)(\])/ismU";
						$replace = "new google.maps.LatLng($2, $3)";
					}
					else if(strlen($ds["zipcodeDoubleDigit"]) > 2){	
						$searchPattern = "/(\[)([0-9\.\-]*),([0-9\.\-]*)(\])/ismU";						
						$replace = "new google.maps.LatLng($3, $2)";
					}
					$thisCoordinates = preg_replace($searchPattern, $replace, $thisCoordinates);
					$arrGoogleMapDoubleDigitZipCodeAreas[$ds["zipcodeDoubleDigit"]] = $thisCoordinates;
				}
				$this->arrGoogleMapZipCodeAreas = $arrGoogleMapDoubleDigitZipCodeAreas;				
			}
			
			$content = '';
			if(!empty($this->arrGoogleMapZipCodeAreas)){
				
				$content .= '<script type="text/javascript" language="javascript">';
				$content .= 'var arrMyZipcodeAreaPoints = new Array();';						
			
				#foreach($arrSalesmanGoogleMapZipCodeAreas as $thisSalesmanID => $thisSalesmanGoogleMapZipCodeAreas) {					
					#$content .= 'arrMyZipcodeAreaPoints[' . $thisSalesmanID . '] = new Array();';				
					$countItem = 0;
					#foreach($thisSalesmanGoogleMapZipCodeAreas as $thisKey => $thisValue) {
					foreach($this->arrGoogleMapZipCodeAreas as $thisKey => $thisValue) {			
						#$content .= 'arrMyZipcodeAreaPoints[' . $thisSalesmanID . '][' . $countItem . '] = [' . $thisValue . ']';			
						$content .= 'arrMyZipcodeAreaPoints[' . $countItem . '] = [' . $thisValue . '];';			
						$countItem++;
					}
				#}	
					
				$content .= '
						function showZipCodeAreas(map){							
							var count = 0;
							if(arrMyZipcodeAreaPoints.length > 0){
								var arrMyZipcodeAreaPolygon = new Array();
								for(i in arrMyZipcodeAreaPoints){												
									// arrMyZipcodeAreaPolygon[i] = new Array();									
									infWindowKey = i;
									thisColor = "#FF0000";
									arrMyZipcodeAreaPolygon[infWindowKey] = new google.maps.Polygon({
											paths: arrMyZipcodeAreaPoints[i],
											strokeColor: thisColor,
											strokeOpacity: 1,
											strokeWeight: 0.5,
											fillColor: thisColor,
											fillOpacity: 0.5,
											zIndex:1
											});
									arrMyZipcodeAreaPolygon[infWindowKey].setMap(map);									
									
									
									// var thisPolyGonMarker = new google.maps.LatLng(' . $this->strDefaultPointLat . ',' . $this->strDefaultPointLon . ');
									thisPolyGonMarker = new google.maps.LatLng(arrMyZipcodeAreaPoints[i][0]);
									
									// DEFINE INFO WINDOW
									var html = new Array();	
									html[infWindowKey] = "HALLO: " + infWindowKey;
									
									infoWindowPolygon[infWindowKey] = new Array();
									//infoWindowPolygon[infWindowKey] = new google.maps.InfoWindow({
										//content: html[infWindowKey]
									//});
									
									arrMyZipcodeAreaPolygon[infWindowKey].set("windowContent", i);									
										
									google.maps.event.addListener(arrMyZipcodeAreaPolygon[infWindowKey], "click", function() {										
										infoWindowPolygon[infWindowKey] = new google.maps.InfoWindow();
										infoWindowPolygon[infWindowKey].setContent("Information : " + arrMyZipcodeAreaPolygon[infWindowKey].get("windowContent"));
										infoWindowPolygon[infWindowKey].setPosition(thisPolyGonMarker);
										if(activeMarkerPolygon != "") {
											infoWindowPolygon[activeMarkerPolygon].close();
										}
										try{
											infoWindowPolygon[infWindowKey].open(map);										
										}
										catch(e){
											alert("error: " + e + " | i: " + i);
										}
										activeMarkerPolygon = infWindowKey;
									});
								}
							}
						}
					';
				$content .= '</script>';	
			}			
			return $content;
		}
		// EOF GET ZIPCODE AREAS
		
		// BOF GET GOOGLE MAPS COUNTRY AREAS
		function getGoogleMapsCountryAreas(){
			$dbConnection = new DB_Connection();
			$this->arrGoogleMapCountryAreas = array();
			if($this->boolShowCountryAreas){
				$sql = "SELECT
							`lat` AS `latitude`,
							`lon` AS `longitude`,
							`country`,
							`subcountry`,
							IF(`subcountry` != '' , CONCAT(`country`, '_', `subcountry`), `country`) AS `area`

							FROM `" . TABLE_GEODB_AREAS . "`

							WHERE 1
								AND `subcountry` = ''
								AND `country` = 'DE'
					";				
				$rs = $dbConnection->db_query($sql);

				while($ds = mysqli_fetch_assoc($rs)) {
					$this->arrGoogleMapCountryAreas[$ds["area"]][] = 'new google.maps.LatLng(' . $ds["latitude"] . ', ' . $ds["longitude"] . ')';
				}
				
				// BOF COUNTRY BORDERS
				if(!empty($this->arrGoogleMapCountryAreas)){
					$content = '';
					$content .= '<script type="text/javascript" language="javascript">';					
					$content .= 'var arrMyCountryAreaPoints = new Array();';			
			
					$countItem = 0;
					foreach($this->arrGoogleMapCountryAreas as $thisKey => $thisValue) {			
						$content .= 'arrMyCountryAreaPoints[' . $countItem . '] = [' . implode(", ", $thisValue) . '];';
						$countItem++;
					}
					$content .= '
							function showCountryBorders(map){
								if(arrMyCountryAreaPoints.length > 0){
									var arrMyCountryAreaPolygon = new Array();
									for(i = 0 ; i < arrMyCountryAreaPoints.length ; i++){
										arrMyCountryAreaPolygon[i] = new google.maps.Polygon({
												paths: arrMyCountryAreaPoints[i],
												strokeColor: "#00FF3A",
												strokeOpacity: 0.8,
												strokeWeight: 2,
												fillColor: "transparent",
												fillOpacity: 0.0
												});
										arrMyCountryAreaPolygon[i].setMap(map);
									}
								}
							}
						';
					$content .= '</script>';					
					
					return $content;
				}
				// EOF COUNTRY BORDERS								
			}			
		}
		// EOF GET GOOGLE MAPS COUNTRY AREAS
		
		function setGoogleMapsJsDatas(){			
			$this->strGoogleMapsJsDatas = '';
			$this->strGoogleMapsJsDatas .= '				
				<script type="text/javascript" language="javascript">
					function initialize() {
						if (!document.getElementById("' . $this->strGoogleMapsCanvasID . '")) {
							alert("Fehler: das Element mit der id "+ "' . $this->strGoogleMapsCanvasID . '"+ " konnte nicht auf dieser Webseite gefunden werden!");
							return false;
						}
						else {
							/* BOF DEFINE DEFAULT VALUES */
								var myPointDatas	= new Array();

								var elementId = "' . $this->strGoogleMapsCanvasID . '";
								var default_arrayKey	= 0;
								var default_lat			= "' . $this->strDefaultPointLat . '";
								var default_lon			= "' . $this->strDefaultPointLon . '";
								var default_zoom		= ' . $this->strDefaultZoom . ';

								var sidebarHtml		= "";              			/* HTML-Code für die gesamte Sidebar */
								var sidebarMarkers	= [];              			/* Array für die Marker */
								var sidebarCount	= 0;               			/* Zähler für die Marker (= Zeile in der Sidebar) */
								var marker				= new Array();
								var markerPolygonCenter	= new Array();
								var markerPolygonCenterShadow	= new Array();
								var activeMarker	= "";
								var activeMarkerPolygonCenter	= "";
								var infoWindowPoint			= new Array();
								infoWindowPolygon		= new Array();
								var infoWindowArea			= new Array();
								activeMarkerPolygon = "";
							/* EOF DEFINE DEFAULT VALUES */
						
							/* BOF DEFINE POINT DATAS */
								myPointDatas[0] = new Array();
								myPointDatas[0][0] = new Array();
								myPointDatas[0][0]["latitude"] = default_lat;
								myPointDatas[0][0]["longitude"] = default_lon;
								myPointDatas[0][0]["title"] = "BURHAN CTR";
								myPointDatas[0][0]["header"] = "BURHAN CTR";
								myPointDatas[0][0]["text"] = "";
								myPointDatas[0][0]["city"] = "";
								myPointDatas[0][0]["region_1"] = "";
								myPointDatas[0][0]["region_2"] = "";
								myPointDatas[0][0]["region_3"] = "";
								myPointDatas[0][0]["imagePath"] = "";
							/* EOF DEFINE POINT DATAS */
						
							var latlng = new google.maps.LatLng(myPointDatas[default_arrayKey][default_arrayKey]["latitude"], myPointDatas[default_arrayKey][default_arrayKey]["longitude"]);

							var myOptions = {
								zoom: default_zoom,
								center: latlng,
								panControl: true,
								zoomControl: true,
								navigationControl: true,
								mapTypeControl: true,
								scaleControl: true,
								overviewMapControl: true,
								streetViewControl: true,
								mapTypeId: google.maps.MapTypeId.TERRAIN
								/* ROADMAP zeigt die normalen Standard-2D-Kacheln von Google Maps an. */
								/* SATELLITE zeigt Fotokacheln an. */
								/* HYBRID zeigt eine Mischung aus Fotokacheln und eine Kachelebene für markante Merkmale an (Straßen, Ortsnamen). */
								/* TERRAIN zeigt Reliefkacheln für Erhebungen und GewÃ¤sser (Berge, Flüsse usw.) an. */
							};
							var map = new google.maps.Map(document.getElementById("' . $this->strGoogleMapsCanvasID . '"), myOptions);
				';				
				
			if($this->boolShowCountryAreas) {
				$this->strGoogleMapsJsDatas .= 'showCountryBorders(map);';
			}
			if($this->boolShowZipCodeAreas) {
				$this->strGoogleMapsJsDatas .= 'showZipCodeAreas(map);';
			}
			$this->strGoogleMapsJsDatas .= '
						}
					}
				';
			
			$this->strGoogleMapsJsDatas .= '</script>';
		}
		
		function setGoogleMapsCanvas(){		
			$this->strGoogleMapsCanvas = '';
			$this->strGoogleMapsCanvas .= '
				<script type="text/javascript" src="' . PATH_GOOGLE_MAPS_API . '"></script>
				<div id="contentAreaElements">
					<div id="myGoogleMapsArea">
						<div id="' . $this->strGoogleMapsCanvasID . '">
							<span class="loadingArea"><img class="loader" src="layout/ajax-loader.gif" /> Bitte haben Sie ein wenig Geduld. Die Daten werden geladen...</span>
						</div>
					</div>
				</div>
				<script language="javascript" type="text/javascript">
					$("#' . $this->strGoogleMapsCanvasID . '").css({
						"width": "100%",
						"height": ($(window).height() - 30) + "px"
					});
				</script>
			';			
		}
		
		function getGoogleMapsCanvas(){
			$this->setGoogleMapsCanvas();
			return $this->strGoogleMapsCanvas;
		}
		
		function getGoogleMapsJsDatas(){
			$this->setGoogleMapsJsDatas();
			return $this->strGoogleMapsJsDatas;
		}
		
		function getGoogleMaps(){
			$content = '';
			$content .= $this->getGoogleMapsCanvas();
			$content .= $this->getGoogleMapsJsDatas();
			$content .= $this->getGoogleMapsCountryAreas();
			$content .= $this->getGoogleZipcodeAreas();
			return $content;		
		}
		
		function writeGoogleMapPoints(){			
			if(!empty($this->arrGoogleMapPoints)){				
				foreach($this->arrGoogleMapPoints as $thisKey => $thisValue){
					$this->strGoogleMapPoints = $thisValue;
					#echo ($this->strGoogleMapPoints);
				}
			}		
		}
	}
?>