<?php
	#class createImage extends Imagick {
	class createImage {
		var $imgDpi		= 72;
		var $imgWidth	= 600;
		var $imgHeight	= 200;
		var $imgStartX	= 0;
		var $imgStartY	= 120;
		
		var $sourceFilePath;
		var $targetFilePath;
		var $sourcePageNum	= 0;
		var $sourceFileType;
		
		public function __construct($sourceFilePath, $targetFilePath, $imgDpi=72, $imgWidth=600, $imgHeight=200, $imgStartX=0, $imgStartY=0, $sourcePageNum=0) {			
			$this->imgDpi		= $imgDpi;
			$this->imgWidth		= $imgWidth;
			$this->imgHeight	= $imgHeight;
			$this->imgStartX	= $imgStartX;
			$this->imgStartY	= $imgStartY;
			
			$this->sourceFilePath	= $sourceFilePath;
			$this->targetFilePath	= $targetFilePath;
			$this->sourcePageNum	= $sourcePageNum;
			$this->sourceFileFileType	= strtolower(pathinfo($sourceFilePath,  PATHINFO_EXTENSION));
			$this->targetFileFileType	= strtolower(pathinfo($targetFilePath,  PATHINFO_EXTENSION));
			// $this->targetFileFileType	= 'png';
			
			$this->generateImage();
		}
		
		public function createImage($sourceFilePath, $targetFilePath, $imgDpi, $imgWidth, $imgHeight, $imgStartX, $imgStartY, $sourcePageNum=0) {
			self::__construct($sourceFilePath, $targetFilePath, $imgDpi, $imgWidth, $imgHeight, $imgStartX, $imgStartY, $sourcePageNum=0);			
		}	
		
		public function getTargetPath(){
			return $this->targetFilePath;
		}
		
		private function generateImage(){
			
			$imageCreate = new Imagick();
			$imageCreate->setResolution($this->imgDpi, $this->imgDpi);			// set the resolution of the resulting jpg
						
			$imageCreate->setBackgroundColor('#FFFFFF');
			
			if($this->sourceFileFileType == 'pdf'){				
				$imageCreate->readImage($this->sourceFilePath . '[' . $this->sourcePageNum . ']');		// [0] for the first page
				
				// BOF GET PDF SIZE
					$command = escapeshellcmd('identify -format "%wx%h" ' . $this->sourceFilePath);
					$geometry = exec($command);					
					list($thisPdfWidth, $thisPdfHeight) = explode("x", $geometry);
					if($this->imgStartY >= $thisPdfHeight){
						$this->imgStartY = 0;
					}
				// EOF GET PDF SIZE
			}
			else {				
				$imageCreate->readImage($this->sourceFilePath);
			}
			#$thisColorSpace = $imageCreate->getImageColorspace();
			#echo 'cs1: ' . $imageCreate->getImageColorspace() . '<br>';	
			if($thisColorSpace == 12){		
				$imageCreate->setImageColorspace(imagick::COLORSPACE_CMYK); 
			}
			#echo 'cs2: ' . $imageCreate->getImageColorspace() . '<br>';			
			$imageCreate->setImageFormat($this->targetFileFileType); // 'jpg'
			
			
			$imageCreate->ThumbnailImage($this->imgWidth, $this->imgHeight, true);
			$imageCreate->cropImage($this->imgWidth, $this->imgHeight, $this->imgStartX, $this->imgStartY);
			// $imageCreate->getNumberImages();
			// header('Content-Type: image/jpeg');
			if(file_exists($this->targetFilePath)){
				unlink($this->targetFilePath);
			}
			$fp_storeImage = fopen($this->targetFilePath, 'w');
			fwrite($fp_storeImage, $imageCreate);
			fclose($fp_storeImage);
			
			clearstatcache();
			flush();
		}
	}
?>	