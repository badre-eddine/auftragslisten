<?php
	class createMail extends htmlMimeMail {	
		var $strMailContentType = '';
		var $strDocumentType = '';
		var $strDocumentNumber = '';
		#var $strNowDate = date("d.m.Y, H:i:s");
		
		var $strMailContent_HTML = '';
		var $strMailContent_TEXT = '';	
		
		var $strMailAdditionalText = '';
		var $strMailSendAttachedText = '';
		var $strMailSubject = '';
		
		var $arrMailResults = array();
		var $strMailImagesDirectory = '';
		var $strMailAttachmentDirectory = '';
		var $arrMailAttachmentFiles = array();		
		var $arrMailTextTemplate = array();
		var $arrMailBodyTemplate = array();
		var $arrRelatedDocuments = array();
		var $arrMailRecipients = '';
		var $strMailSender = '';
		var $boolSendAttachedDocument = false;
		var $boolMailSendResult = false;
		var $strDeliveryDatas = '';
		
		public function createMail(
			$strMailSubject='',
			$strDocumentType='', 
			$strDocumentNumber='', 
			$arrMailAttachmentFiles=array(), 
			$arrMailRecipients='', 
			$arrMailTextTemplate=array(), 
			$arrMailBodyTemplate=array(), 
			$strMailAdditionalText='',
			$strMailSendAttachedText='',
			$boolSendAttachedDocument=false,
			$strMailImagesDirectory='',
			$strMailSender=''
		) {
			$this->htmlMimeMail();
		
			$this->setDocumentNumber($strDocumentNumber);
			$this->setRelatedDocuments(array($strDocumentNumber));
			$this->setMailSubject($strMailSubject);
			$this->setAttachmentFiles($arrMailAttachmentFiles);
			$this->setDocumentType($strDocumentType);			
			$this->setMailRecipients($arrMailRecipients);			
			$this->setMailTextTemplate($arrMailTextTemplate);
			$this->setMailBodyTemplate($arrMailBodyTemplate);
			$this->setMailAdditionaltext($strMailAdditionalText);
			$this->setMailSendAttachedText($strMailSendAttachedText);
			$this->setSendAttachedDocument($boolSendAttachedDocument);			
			$this->setMailImagesDirectory($strMailImagesDirectory);		
			$this->setMailSender($strMailSender);
			$this->createMailContent();
			
			
			// BOF RETURN RESULT			
			return $this->boolMailSendResult;
			// EOF RETURN RESULT
		}
		
		public function setMailSender($strMailSender){
			$this->strMailSender = $strMailSender;
		}
		public function setMailRecipients($arrMailRecipients){
			if(preg_match("/;/", $arrMailRecipients)){
				$arrTemp = explode(";", $arrMailRecipients);
				$this->arrMailRecipients = array_merge($this->arrMailRecipients, $arrTemp);
			}
			else if(preg_match("/,/", $arrMailRecipients)){
				$arrTemp = explode(",", $arrMailRecipients);
				$this->arrMailRecipients = array_merge($this->arrMailRecipients, $arrTemp);
			}
			else {
				$this->arrMailRecipients[] = $arrMailRecipients;
			}					
			$this->arrMailRecipients = array_unique($this->arrMailRecipients);
		}
		
		public function setMailSubject($strMailSubject){
			$this->strMailSubject = $strMailSubject;	
			$this->strMailSubject = preg_replace('/{###DOCUMENT_NUMBER###}/', $this->strDocumentNumber, $this->strMailSubject);
			$this->strMailSubject = preg_replace('/{###DOCUMENT_AB_NUMBER###}/', $this->arrRelatedDocuments["AB"] . ' ('.$this->strDocumentNumber . ')', $this->strMailSubject);			
		}
		public function setSendAttachedDocument($boolSendAttachedDocument){			
			$this->boolSendAttachedDocument = $boolSendAttachedDocument;	
		}
		public function setMailSendAttachedText($strMailSendAttachedText){			
			$this->strMailSendAttachedText = $strMailSendAttachedText;	
		}
		
		public function setMailImagesDirectory($strMailImagesDirectory){			
			$this->strMailImagesDirectory = $strMailImagesDirectory;			
		}
		
		public function setDeliveryDatas($arrDeliveryDatas=array()){
			$arrDeliverDatas["TYPE"] = '';
			$arrDeliverDatas["TRACKING_NUMBER"] = '';
			$arrDeliverDatas["TRACKING_LINK"] = '';
			$this->strDeliveryDatas = '<p>' . $arrDeliverDatas["TYPE"] . '-Tracking-Nummer: ' . $arrDeliverDatas["TRACKING_LINK"] . '</p>';
		}
		
		public function createMailContent(){			
			// BOF CREATE HTML MAIL TO CUSTOMER			
			$this->strMailContent_HTML = $this->arrMailBodyTemplate;
			
			$this->strMailContent_HTML = preg_replace('/{###MAIL_HEADER_IMAGE###}/',$this->arrMailTextTemplate["MAIL_HEADER_IMAGE"] , $this->strMailContent_HTML);

			#$this->strMailContent_HTML = preg_replace('/{###MAIL_TITLE###}/',$this->arrMailTextTemplate["MAIL_TITLE"], $this->strMailContent_HTML);
			$this->strMailContent_HTML = preg_replace('/{###MAIL_TITLE###}/',$this->arrMailTextTemplate["MAIL_TITLE"], $this->strMailContent_HTML);
			$this->strMailContent_HTML = preg_replace('/{###MAIL_DATE_VALUE###}/', date("d.m.Y, H:i:s"), $this->strMailContent_HTML);

			#$this->strMailContent_HTML = preg_replace('/{###MAIL_SALUTATION###}/', $this->arrMailTextTemplate["MAIL_SALUTATION"], $this->strMailContent_HTML);

			if(($this->boolSendAttachedDocument == '1' || $this->boolSendAttachedDocument == true) && $this->strMailSendAttachedText != '') {
				$this->strMailContent_HTML = preg_replace('/{###MAIL_CONTENT###}/', nl2br($this->strMailSendAttachedText), $this->strMailContent_HTML);
				$this->strMailContent_HTML = preg_replace('/{###MAIL_SALUTATION###}/', '', $this->strMailContent_HTML);
			}
			else {
				if($this->strMailAdditionalText != "") {					
					$this->strMailContent_HTML = preg_replace('/{###MAIL_CONTENT###}/', ($this->strMailAdditionalText), $this->strMailContent_HTML);
					$this->strMailContent_HTML = preg_replace('/{###MAIL_SALUTATION###}/', '', $this->strMailContent_HTML);
					$this->strMailContent_HTML = preg_replace('/{###MAIL_REGARDS###}/', '', $this->strMailContent_HTML);
				}
				$this->strMailContent_HTML = preg_replace('/{###MAIL_CONTENT###}/', $this->arrMailTextTemplate["MAIL_CONTENT"], $this->strMailContent_HTML);
				$this->strMailContent_HTML = preg_replace('/{###MAIL_SALUTATION###}/', $this->arrMailTextTemplate["MAIL_SALUTATION"], $this->strMailContent_HTML);
			}
			$this->strMailContent_HTML = preg_replace('/{###MAIL_REGARDS###}/', $this->arrMailTextTemplate["MAIL_REGARDS"], $this->strMailContent_HTML);
			$this->strMailContent_HTML = preg_replace('/{###DOCUMENT_NUMBER###}/', $this->strDocumentNumber, $this->strMailContent_HTML);			
			$this->strMailContent_HTML = preg_replace('/{###DOCUMENT_AB_NUMBER###}/', 'Auftrag ' . $this->arrRelatedDocuments["AB"] . ' / '.$this->strDocumentNumber . '' , $this->strMailContent_HTML);			
			$this->strMailContent_HTML = preg_replace('/{###DELIVERY_TRACKING_NUMBER###}/', $this->strDeliveryDatas, $this->strMailContent_HTML);			
			$this->strMailContent_HTML = preg_replace('/{###MAIL_SIGNATURE###}/', $this->arrMailTextTemplate["MAIL_SIGNATURE"], $this->strMailContent_HTML);
			$this->strMailContent_HTML = preg_replace('/{###MAIL_COPYRIGHT###}/', $this->arrMailTextTemplate["MAIL_COPYRIGHT"], $this->strMailContent_HTML);
			
			$this->strMailContent_HTML = removeUnnecessaryChars($this->strMailContent_HTML);
			// EOF CREATE HTML MAIL TO CUSTOMER				
			
			// BOF CREATE TEXT MAIL
			$this->strMailContent_TEXT = $this->strMailContent_HTML;
			$this->strMailContent_TEXT = COMPANY_NAME_LONG . "<hr />" . $this->strMailContent_TEXT;
			$this->strMailContent_TEXT = "" . convertHtmlToText($this->strMailContent_TEXT);		
			// EOF CREATE TEXT MAIL
			
			// BOF SET MAIL DATAS
			$this->setMailDatas();
			// EOF SET MAIL DATAS
			
			// BOF ADD CONTENT
			#$this->setHtml($this->strMailContent_HTML, $this->strMailContent_TEXT, $this->strMailImagesDirectory);
			$this->setHtml($this->strMailContent_HTML, $this->strMailContent_TEXT, DIRECTORY_MAIL_IMAGES);
			// EOF ADD CONTENT			
			
			// BOF ADD ATTACHMENT						
			if(!empty($this->arrMailAttachmentFiles)) {			
				foreach($this->arrMailAttachmentFiles as $thisFile => $thisFileName) {
					if(file_exists(utf8_decode($thisFile))) {
						$this->addAttachment($this->getFile(utf8_decode($thisFile)), utf8_decode($thisFileName), '');						
					}					
				}
			}
			if(file_exists(PATH_VCARD)) {
				$this->addAttachment($this->getFile(PATH_VCARD), 'Kontakt_vCard.vcf', 'application/text');
			}
			if(file_exists(PATH_AGB_PDF)) {
				$this->addAttachment($this->getFile(PATH_AGB_PDF), 'AGB.pdf', 'application/pdf');
			}
			clearstatcache();			
			// EOF ADD ATTACHMENT
			
			// BOF ADD RECIPIENTS AND SEND
			if(!empty($this->arrMailRecipients)){
				if(MAIL_SEND_TYPE == 'mail') {
					$this->boolMailSendResult = $this->send($this->arrMailRecipients, 'mail');
				}
				else if(MAIL_SEND_TYPE == 'smtp') {
					$this->setSMTPParams(MAIL_ACCOUNT_SMTP, MAIL_ACCOUNT_PORT, MAIL_ACCOUNT_HELO, MAIL_ACCOUNT_AUTH, MAIL_ACCOUNT_USER, MAIL_ACCOUNT_PASSWORD);
					// function setSMTPParams($host = null, $port = null, $helo = null, $auth = null, $user = null, $pass = null)
					$this->boolMailSendResult = $this->send($this->arrMailRecipients, 'smtp');
					
					if(!$this->boolMailSendResult) {					
											
					}
				}
				else {
					$this->boolMailSendResult = false;
				}
			}
			else {
				#unset($this->boolMailSendResult);
				$this->boolMailSendResult = null;
			}			
			// EOF ADD RECIPIENTS AND SEND
			
			// BOF LOG SENDED MAIL
			$this->logSendedMails();
			// EOF LOG SENDED MAIL
		}
		
		private function setMailDatas(){			
			$this->setTextCharset('utf-8');
			$this->setHtmlCharset('utf-8');
			$this->setTextEncoding('7bit');
			$this->setHtmlEncoding('quoted-printable');
			$this->setHeadCharset('utf-8');
			
			if($this->strMailSender != ''){
				$this->setHeader('From', '"' . COMPANY_NAME_SHORT . '" <' . $this->strMailSender . '>');
				$this->setHeader('Reply-To', $this->strMailSender);
			}
			else {
				$this->setMailSender(MAIL_ADDRESS_FROM);
				$this->setHeader('From', '"' . MAIL_ADDRESS_FROM_NAME . '" <' . MAIL_ADDRESS_FROM . '>');
				$this->setHeader('Reply-To', MAIL_ADDRESS_REPLY_TO);
			}
			$this->setHeader('Errors-To', MAIL_ERRORS_TO);
			$this->setHeader('Return-Path', MAIL_ERRORS_TO);
			#$this->setHeader('Subject', preg_replace('/{###DOCUMENT_NUMBER###}/', $this->strDocumentNumber, $this->strMailSubject).'');
			$this->setHeader('Subject', $this->strMailSubject);			
			$this->setHeader('Date', date('D, d M y H:i:s O'));
		}
		
		private function cleanMailContent(){
			$this->strMailContent_HTML = removeUnnecessaryChars($this->strMailContent_HTML);
		}
		
		private function setMailTextTemplate($arrMailTextTemplate){
			$this->arrMailTextTemplate = $arrMailTextTemplate;
		}
		private function setMailBodyTemplate($arrMailBodyTemplate){
			$this->arrMailBodyTemplate = $arrMailBodyTemplate;
		}
		
		private function setMailAdditionaltext($strMailAdditionalText){
			$this->strMailAdditionalText = $strMailAdditionalText;
		}
		
		private function setDocumentType($strDocumentType){
			$this->strDocumentType = $this->strDocumentType;
		}
		
		private function setRelatedDocuments($arrRelatedDocuments=array()){
			$this->arrRelatedDocuments = getRelatedDocuments($arrRelatedDocuments);			
		}
		
		private function setDocumentNumber($strDocumentNumber){
			$this->strDocumentNumber = $strDocumentNumber;
		}
		
		private function setAttachmentFiles($arrMailAttachmentFiles){
			$this->arrMailAttachmentFiles = $arrMailAttachmentFiles;
		}
		
		private function setAttachmentPath(){
		
		}
		
		private function logSendedMails(){
			logSendedMails(implode(';',$this->arrMailRecipients), $this->strMailSubject, $this->boolMailSendResult, $this->strDocumentType, $this->strDocumentNumber, $this->strMailSender, $this->strMailContent_HTML);
		}
		
		public function returnErrors(){		
			return $this->errors;
		}
		
		public function returnResult(){			
			$thisResult = null;			
			if(isset($this->boolMailSendResult)){
				$thisResult = $this->boolMailSendResult;
				if($thisResult == false){
					$thisResult = '0';
				}
				else if($thisResult == true){
					$thisResult = '1';
				}				
			}
			return $thisResult;
		}				
	}
?>