<?php
	require_once('inc/requires.inc.php');

	DEFINE ('KZH_EK_PRICE', 0.59);
	DEFINE ('KZH_EK_PRICE2', 0.65);

	if(!$arrGetUserRights["editProvisionDatas"] && !$arrGetUserRights["displayProvisionDatas"]) {
		header('location: ' . PAGE_EXIT_LOCATION);
		exit;
	}

	if($_GET["downloadFile"] != "") {
		$thisDownloadFolder = DIRECTORY_CREATED_DOCUMENTS_SALESMEN;
		if($_GET["thisDocumentType"] == 'AB' || $_GET["thisDocumentType"] == 'RE'){
			$thisDownloadFolder = DIRECTORY_CREATED_DOCUMENTS_CUSTOMERS;
		}
		require_once('classes/downloadFile.class.php');
		$thisDownload = new downloadFile($thisDownloadFolder, $_GET["downloadFile"]);
		$errorMessage .= $thisDownload->startDownload();
	}

	$jswindowMessage = '';
	$warningMessage = '';
	$errorMessage = '';
	$successMessage = '';
	$infoMessage = '';

	$dbConnection = new DB_Connection();
	$db_open = $dbConnection->db_connect();

	// BOF READ VERTRETER
		$arrSalesmenDatas = getSalesmen();
	// EOF READ VERTRETER

	// BOF READ BANK TYPES
		$arrBankAccountTypeDatas = getBankAccountTypes();
	// EOF READ BANK TYPES

	// BOF READ BESTELL ARTEN
		$arrOrderTypeDatas = getOrderTypes();
	// EOF READ BESTELL ARTEN

	// BOF READ BESTELL STATUS TYPEN
		$arrOrderStatusTypeDatas = getOrderStatusTypes();
	// EOF READ BESTELL STATUS TYPEN

	// BOF READ DOCUMENT TYPE DATAS
		$arrPaymentStatusTypeDatas = getPaymentStatusTypes();
	// EOF READ DOCUMENT TYPE DATAS

	// DEFINE('TABLE_ORDER_THIS', TABLE_ORDER_INVOICES);
	// DEFINE('TABLE_ORDER_THIS_DETAILS', TABLE_ORDER_INVOICES_DETAILS);

	// DEFINE('TABLE_ORDER_THIS', TABLE_ORDER_CONFIRMATIONS);
	// DEFINE('TABLE_ORDER_THIS_DETAILS', TABLE_ORDER_CONFIRMATIONS_DETAILS);

	// BOF SALESMAN DATAS
		$sql = "SELECT

					`" . TABLE_CUSTOMERS. "`.`customersID`,
					`" . TABLE_CUSTOMERS. "`.`customersKundennummer`,
					`" . TABLE_CUSTOMERS. "`.`customersFirmenname`,
					`" . TABLE_CUSTOMERS. "`.`customersFirmennameZusatz`,
					`" . TABLE_CUSTOMERS. "`.`customersFirmenInhaberVorname`,
					`" . TABLE_CUSTOMERS. "`.`customersFirmenInhaberNachname`,
					`" . TABLE_CUSTOMERS. "`.`customersFirmenInhaberAnrede`,
					`" . TABLE_CUSTOMERS. "`.`customersFirmenInhaber2Vorname`,
					`" . TABLE_CUSTOMERS. "`.`customersFirmenInhaber2Nachname`,
					`" . TABLE_CUSTOMERS. "`.`customersFirmenInhaber2Anrede`,
					`" . TABLE_CUSTOMERS. "`.`customersAnsprechpartner1Vorname`,
					`" . TABLE_CUSTOMERS. "`.`customersAnsprechpartner1Nachname`,
					`" . TABLE_CUSTOMERS. "`.`customersAnsprechpartner1Anrede`,
					`" . TABLE_CUSTOMERS. "`.`customersAnsprechpartner2Vorname`,
					`" . TABLE_CUSTOMERS. "`.`customersAnsprechpartner2Nachname`,
					`" . TABLE_CUSTOMERS. "`.`customersAnsprechpartner2Anrede`,
					`" . TABLE_CUSTOMERS. "`.`customersTelefon1`,
					`" . TABLE_CUSTOMERS. "`.`customersTelefon2`,
					`" . TABLE_CUSTOMERS. "`.`customersMobil1`,
					`" . TABLE_CUSTOMERS. "`.`customersMobil2`,
					`" . TABLE_CUSTOMERS. "`.`customersFax1`,
					`" . TABLE_CUSTOMERS. "`.`customersFax2`,
					`" . TABLE_CUSTOMERS. "`.`customersMail1`,
					`" . TABLE_CUSTOMERS. "`.`customersMail2`,
					`" . TABLE_CUSTOMERS. "`.`customersHomepage`,
					`" . TABLE_CUSTOMERS. "`.`customersCompanyStrasse`,
					`" . TABLE_CUSTOMERS. "`.`customersCompanyHausnummer`,
					`" . TABLE_CUSTOMERS. "`.`customersCompanyCountry`,
					`" . TABLE_CUSTOMERS. "`.`customersCompanyPLZ`,
					`" . TABLE_CUSTOMERS. "`.`customersCompanyOrt`,
					`" . TABLE_CUSTOMERS. "`.`customersLieferadresseFirmenname`,
					`" . TABLE_CUSTOMERS. "`.`customersLieferadresseStrasse`,
					`" . TABLE_CUSTOMERS. "`.`customersLieferadresseHausnummer`,
					`" . TABLE_CUSTOMERS. "`.`customersLieferadressePLZ`,
					`" . TABLE_CUSTOMERS. "`.`customersLieferadresseOrt`,
					`" . TABLE_CUSTOMERS. "`.`customersLieferadresseLand`,
					`" . TABLE_CUSTOMERS. "`.`customersRechnungsadresseFirmenname`,
					`" . TABLE_CUSTOMERS. "`.`customersRechnungsadresseStrasse`,
					`" . TABLE_CUSTOMERS. "`.`customersRechnungsadresseHausnummer`,
					`" . TABLE_CUSTOMERS. "`.`customersRechnungsadressePLZ`,
					`" . TABLE_CUSTOMERS. "`.`customersRechnungsadresseOrt`,
					`" . TABLE_CUSTOMERS. "`.`customersRechnungsadresseLand`,
					`" . TABLE_CUSTOMERS. "`.`customersBankName`,
					`" . TABLE_CUSTOMERS. "`.`customersKontoinhaber`,
					`" . TABLE_CUSTOMERS. "`.`customersBankKontonummer`,
					`" . TABLE_CUSTOMERS. "`.`customersBankLeitzahl`,
					`" . TABLE_CUSTOMERS. "`.`customersBankIBAN`,
					`" . TABLE_CUSTOMERS. "`.`customersBankBIC`,
					`" . TABLE_CUSTOMERS. "`.`customersBezahlart`,
					`" . TABLE_CUSTOMERS. "`.`customersZahlungskondition`,
					`" . TABLE_CUSTOMERS. "`.`customersRabatt`,
					`" . TABLE_CUSTOMERS. "`.`customersUseProductMwst`,
					`" . TABLE_CUSTOMERS. "`.`customersUseProductDiscount`,
					`" . TABLE_CUSTOMERS. "`.`customersUstID`,
					`" . TABLE_CUSTOMERS. "`.`customersVertreterID`,
					`" . TABLE_CUSTOMERS. "`.`customersVertreterName`,
					`" . TABLE_CUSTOMERS. "`.`customersUseSalesmanDeliveryAdress`,
					`" . TABLE_CUSTOMERS. "`.`customersUseSalesmanInvoiceAdress`,
					`" . TABLE_CUSTOMERS. "`.`customersTyp`,
					`" . TABLE_CUSTOMERS. "`.`customersGruppe`,
					`" . TABLE_CUSTOMERS. "`.`customersNotiz`,
					`" . TABLE_CUSTOMERS. "`.`customersUserID`,
					`" . TABLE_CUSTOMERS. "`.`customersTimeCreated`,
					`" . TABLE_CUSTOMERS. "`.`customersActive`,
					`" . TABLE_CUSTOMERS. "`.`customersDatasUpdated`,
					`" . TABLE_CUSTOMERS. "`.`customersTaxAccountID`,

					`" . TABLE_SALESMEN . "`.`salesmenID`,
					`" . TABLE_SALESMEN . "`.`salesmenKundennummer`,
					`" . TABLE_SALESMEN . "`.`salesmenFirmenname`,
					`" . TABLE_SALESMEN . "`.`salesmenSubSalesmanID`,
					`" . TABLE_SALESMEN . "`.`salesmenAreas`,
					`" . TABLE_SALESMEN . "`.`salesmenGetProvision`,
					`" . TABLE_SALESMEN . "`.`salesmenProvision`,
					`" . TABLE_SALESMEN . "`.`salesmenTypesID`,
					`" . TABLE_SALESMEN . "`.`salesmenTypesName`,
					`" . TABLE_SALESMEN . "`.`salesmenTimeCreated`,
					`" . TABLE_SALESMEN . "`.`salesmenActive`,
					`" . TABLE_SALESMEN . "`.`salesmenUseMwst`

				FROM `" . TABLE_CUSTOMERS. "`

				LEFT JOIN `" . TABLE_SALESMEN . "`
				ON(`" . TABLE_CUSTOMERS. "`.`customersKundennummer` = `" . TABLE_SALESMEN . "`.`salesmenKundennummer`)

				WHERE `" . TABLE_CUSTOMERS. "`.`customersID` = '" . $_POST["exportOrdersVertreterID"] . "'

				LIMIT 1
		";

		$rs = $dbConnection->db_query($sql);

		while($ds = mysqli_fetch_assoc($rs)) {
			foreach(array_keys($ds) as $field){
				$arrSalesmanDatas[$field] = $ds[$field];
			}
		}
		// BOF
			$useSalesmenProvisionPerRow = 0;
			if($arrSalesmanDatas["salesmenProvision"] > 0){
				$useSalesmenProvisionPerRow = $arrSalesmanDatas["salesmenProvision"];
				$arrSalesmanDatas["salesmenProvision"] = 0;
			}
		// EOF
#dd('arrSalesmanDatas');
#dd('useSalesmenProvisionPerRow');
	// EOF SALESMAN DATAS


	function storeDocumentDatas(){
		global $_POST, $_REQUEST, $arrSalesmanDatas, $dbConnection, $successMessage, $errorMessage, $displayTotalSum, $valueMwst, $valueTotal, $loadedDocumentDatas;

		#$thisCreatedDocumentNumber = "PR-".date('ym') . '_' . $arrSalesmanDatas["customersKundennummer"];
		$thisCreatedDocumentNumber = "PR-".date('ym', strtotime(formatDate($_REQUEST["exportOrdersDateTo"], "store"))) . '_' . $arrSalesmanDatas["customersKundennummer"];

		$arrTemp = array();
		$deleteWhere = '';
		foreach($_POST["arrProvisionValues"] as $thisKey => $thisValue) {
			$arrTemp[] = $thisValue["orderDocumentsNumber"];
		}
		if(!empty($arrTemp)){
			$arrTemp = array_unique($arrTemp);
			$arrDocumentContentDocumentNumbers = implode(";",$arrTemp);
			foreach($arrTemp as $thisKey => $thisValue) {
				if($thisValue != ''){
					// $deleteWhere .= " OR `salesmenProvisionsContentDocumentNumber` LIKE '%" . $thisValue . "%'";
				}
			}
		}

		$sql = "DELETE
			`" . TABLE_SALESMEN_PROVISIONS . "`,
			`" . TABLE_SALESMEN_PROVISIONS_DETAILS . "`

			FROM `" . TABLE_SALESMEN_PROVISIONS . "`

			LEFT JOIN `" . TABLE_SALESMEN_PROVISIONS_DETAILS . "`
			ON(`" . TABLE_SALESMEN_PROVISIONS . "`.`salesmenProvisionsID` = `" . TABLE_SALESMEN_PROVISIONS_DETAILS . "`.`salesmenProvisionsDetailsProvisionsID`)

			WHERE 1
				AND
				`" . TABLE_SALESMEN_PROVISIONS . "`.`salesmenProvisionsDocumentNumber` = '" . $thisCreatedDocumentNumber . "'
				" . $deleteWhere . "
		";

		$rs = $dbConnection->db_query($sql);

		$sql = "INSERT INTO `" . TABLE_SALESMEN_PROVISIONS . "` (

					`salesmenProvisionsID`,
					`salesmenProvisionsDocumentNumber`,
					`salesmenProvisionsSalesmanID`,
					`salesmenProvisionsDocumentDate`,
					`salesmenProvisionsSum`,
					`salesmenProvisionsMwst`,
					`salesmenProvisionsMwstValue`,
					`salesmenProvisionsTotal`,
					`salesmenProvisionsStatus`,
					`salesmenProvisionsDocumentPath`,
					`salesmenProvisionsContentDocumentNumber`
				)
				VALUES (
					'%',
					'" . $thisCreatedDocumentNumber . "',
					'" . $arrSalesmanDatas["customersID"] . "',
					'" . formatDate($loadedDocumentDatas["CREATION_DATE"], "store") . "',
					'" . $displayTotalSum . "',
					'" . MWST . "',
					'" . $valueMwst . "',
					'" . $valueTotal . "',
					'1',
					'" . DIRECTORY_CREATED_DOCUMENTS_SALESMEN . $_REQUEST["thisCreatedPdfName"] . "',
					'" . $arrDocumentContentDocumentNumbers . "'
				)
			";
		$rs = $dbConnection->db_query($sql);

		if($rs) {
			$successMessage .= ' Die Dokumentdaten wurden gespeichert.' .'<br />';
			$thisInsertID = mysqli_insert_id();
		}
		else {
			$errorMessage .= ' Die Dokumentdaten konnten nicht gespeichert werden.' .'<br />';
		}

		$sql = "INSERT IGNORE INTO `" . TABLE_SALESMEN_PROVISIONS_DETAILS . "` (
					`salesmenProvisionsDetailsID`,
					`salesmenProvisionsDetailsProvisionsID`,
					`salesmenProvisionsDetailsDocumentsDetailID`,
					`salesmenProvisionsDetailsDocumentNumber`,
					`salesmenProvisionsDetailsProductOrderDate`,
					`salesmenProvisionsDetailsProductAmount`,
					`salesmenProvisionsDetailsProductNumber`,
					`salesmenProvisionsDetailsProductName`,
					`salesmenProvisionsDetailsProductRecipientCustomerNumber`,
					`salesmenProvisionsDetailsProductRecipientName`,
					`salesmenProvisionsDetailsProductRecipientZipcode`,
					`salesmenProvisionsDetailsProductRecipientCity`,
					`salesmenProvisionsDetailsProductVkPreis`,
					`salesmenProvisionsDetailsProductProvisionPrice`,
					`salesmenProvisionsDetailsDisabled`,
					`salesmenProvisionsDetailsType`
				)
				VALUES
			";



		foreach($_POST["arrProvisionValues"] as $thisKey => $thisValue) {

			$arrSQL[] = " (
						'%',
						'" . $thisInsertID . "',
						'" . $thisValue["orderDocumentDetailID"] . "',
						'" . $thisValue["orderDocumentsNumber"] . "',
						'" . formatDate($thisValue["orderDocumentsOrderDate"], 'store') . "',
						'" . $thisValue["orderDocumentDetailProductQuantity"] . "',
						'" . $thisValue["orderDocumentDetailProductNumber"] . "',
						'" . addslashes($thisValue["orderDocumentDetailProductName"]) . "',
						'" . $thisValue["orderDocumentsCustomerNumber"] . "',
						'" . addslashes($thisValue["orderDocumentsAddressCompany"]) . "',
						'" . $thisValue["orderDocumentsAddressZipcode"] . "',
						'" . $thisValue["orderDocumentsAddressCity"] . "',
						'" . str_replace(',', '.', $thisValue["salesmanProvisionProductVkprice"]) . "',
						'" . str_replace(',', '.', $thisValue["salesmanProvisionValue"]) . "',

						'" . $thisValue["salesmenProvisionsDetailsDisabled"] . "',
						'" . $thisValue["salesmenProvisionsDetailsType"] . "'
					)
				";
		}

		if(!empty($arrSQL)){
			$sql = $sql . implode(", ", $arrSQL);
			$rs = $dbConnection->db_query($sql);

			if($rs) {
				$successMessage .= ' Die Provisionsdaten wurden gespeichert.' .'<br />';
			}
			else {
				$errorMessage .= ' Die Provisionsdaten konnten nicht gespeichert werden.' .'<br />';
			}
		}

		#$_POST["exportOrdersVertreterID"] = '';
		// BOF DELETE TEMP HTML FILE
		if(file_exists(DIRECTORY_CREATED_DOCUMENTS_SALESMEN . $_POST["tempHtmlFileName"])) {
			copy(DIRECTORY_CREATED_DOCUMENTS_SALESMEN . $_POST["tempHtmlFileName"], '_sich_' . date('YmdHis') . '_' . DIRECTORY_CREATED_DOCUMENTS_SALESMEN . $_POST["tempHtmlFileName"]);
			unlink(DIRECTORY_CREATED_DOCUMENTS_SALESMEN . $_POST["tempHtmlFileName"]);
		}
		clearstatcache();
		// EOF DELETE TEMP HTML FILE
	}

	// BOF STORE DATAS
	if($_POST["submitFormProvision"] != '' && $_POST["exportOrdersVertreterID"] > 0 && !empty($_POST["arrProvisionValues"])) {
		storeDocumentDatas();
	}
	// EOF STORE DATAS

	function createPdfDataRow($countRow, $dataType, $arrRowDatas){
		global $displayTotalSum, $arrSalesmanDatas, $useSalesmenProvisionPerRow, $valueSum;

		$pdfDataRow = '';

		if($dataType == 'empty'){
			$pdfDataRow .= '<!-- BOF EMPTY_ROW -->';
			$pdfDataRow .= '<tr class="emptyRow">';
			$pdfDataRow .= '<td></td>';
			$pdfDataRow .= '<!-- BOF DOCUMENT_NUMBER_ORDER -->';
			$pdfDataRow .= '<td></td>';
			$pdfDataRow .= '<!-- EOF DOCUMENT_NUMBER_ORDER -->';
			$pdfDataRow .= '<td colspan="8"></td>';
			$pdfDataRow .= '<!-- BOF DELETE_AREA -->';
			$pdfDataRow .= '<td></td>';
			$pdfDataRow .= '<!-- EOF DELETE_AREA -->';
			$pdfDataRow .= '</tr>';
			$pdfDataRow .= '<!-- EOF EMPTY_ROW -->';
		}

		else {
			if($countRow%2 == 0){ $rowClass = 'row0'; }
			else { $rowClass = 'row1'; }

			$thisStyle = '';
			if($arrRowDatas["DetailsDisabled"] == '1'){
				$thisStyle = ' style="opacity: 0.5; text-decoration: line-through;" ';
				$pdfDataRow .= '<!-- BOF DEACTIVATED_ROW -->';
			}

			$pdfDataRow .= '<tr class="displayCardDatasItemRows" ' . $thisStyle . '>';
			$pdfDataRow .= '<td style="text-align:right;">';
			$pdfDataRow .= '<b>';
			$pdfDataRow .= '<!-- BOF DOCUMENT_POS_INDEX -->';
			$pdfDataRow .= ($countRow + 1);
			$pdfDataRow .= '<!-- EOF DOCUMENT_POS_INDEX -->';
			$pdfDataRow .= '.</b>';
			$pdfDataRow .= '<input type="hidden" name="arrProvisionValues['.$countRow.'][orderDocumentDetailID]" value="' . $arrRowDatas["DetailID"] . '" />';
			$pdfDataRow .= '<input type="hidden" class="salesmenProvisionsDetailsType" name="arrProvisionValues['.$countRow.'][salesmenProvisionsDetailsType]" value="' . $dataType . '" />';
			$pdfDataRow .= '<input type="hidden" class="productKategorieID" name="arrProvisionValues['.$countRow.'][productKategorieID]" value="' . $arrRowDatas["ProductKategorieID"] . '" />';
			$pdfDataRow .= '<input type="hidden" class="salesmenProvision" name="arrProvisionValues['.$countRow.'][salesmenProvision]" value="' . $arrSalesmanDatas["salesmenProvision"] . '" />';
			$pdfDataRow .= '</td>';
			$pdfDataRow .= '<!-- BOF DOCUMENT_NUMBER_ORDER -->';
			$pdfDataRow .= '<td style="background-color:#FEFFAF; white-space:nowrap;">';
			$pdfDataRow .= '' . $arrRowDatas["DocumentsNumber"] . '';
			if(preg_match("/[A-Z]{2}-[0-9]{10}/", $arrRowDatas["DocumentsNumber"])){
				$pdfDataRow .= '<a href="' . $thisParsedUrl["path"] . '?downloadFile=' . basename($arrRowDatas["DocumentPath"]) . '&thisDocumentType=' . $arrRowDatas["DocumentsType"] . '">' . '<img src="layout/icons/icon' . getFileType(basename($arrRowDatas["DocumentPath"])) . '.gif" width="16" height="16" title="Dokument &quot;' . $arrRowDatas["DocumentsNumber"] . '&quot; herunterladen" alt="Download" /></a>';
			}
			$pdfDataRow .= '<input type="hidden" name="arrProvisionValues['.$countRow.'][orderDocumentsNumber]" value="' . $arrRowDatas["DocumentsNumber"] . '" />';
			$pdfDataRow .= '</td>';
			$pdfDataRow .= '<!-- EOF DOCUMENT_NUMBER_ORDER -->';
			$pdfDataRow .= '<td>';
			if($arrRowDatas["OrderDate"] > 0) {
				$thisDate = $arrRowDatas["OrderDate"];
			}
			else {
				$thisDate = $arrRowDatas["ProcessingDate"];
				#$thisDate = $arrRowDatas["DocumentDate"];
			}
			#$thisDate = $arrRowDatas["ProcessingDate"];
			$thisDate = $arrRowDatas["DocumentDate"];
			$pdfDataRow .= formatDate($thisDate, 'display');
			$pdfDataRow .= '<input type="hidden" name="arrProvisionValues['.$countRow.'][orderDocumentsOrderDate]" value="' . formatDate($thisDate, 'display') . '" />';
			$pdfDataRow .= '</td>';
			$pdfDataRow .= '<td style="background-color:#FEFFAF;">';
			#$pdfDataRow .= '<a href="' . PAGE_EDIT_CUSTOMER . '?editCustomerNumber=' . $arrRowDatas["CustomerNumber"]. '"><b title="Zu den Kundendaten">' . $arrRowDatas["CustomerNumber"]. '</b></a>';
			$pdfDataRow .= '' . $arrRowDatas["CustomerNumber"]. '';
			$pdfDataRow .= '<input type="hidden" name="arrProvisionValues['.$countRow.'][orderDocumentsCustomerNumber]" value="' . $arrRowDatas["CustomerNumber"] . '" />';
			$pdfDataRow .= '</td>';
			$pdfDataRow .= '<td>';
			$arrRowDatas["AddressCompany"] = $arrRowDatas["CustomerFirmenname"];
			$pdfDataRow .= wordwrap($arrRowDatas["AddressCompany"], 20, "<br>", false);
			$pdfDataRow .= '<input type="hidden" name="arrProvisionValues['.$countRow.'][orderDocumentsAddressCompany]" value="' . $arrRowDatas["AddressCompany"] . '" />';
			$pdfDataRow .= '</td>';
			$pdfDataRow .= '<td>';
			$arrRowDatas["AddressZipcode"] = $arrRowDatas["CustomerPLZ"];
			$arrRowDatas["AddressCity"] = $arrRowDatas["CustomerOrt"];
			$pdfDataRow .= $arrRowDatas["AddressZipcode"]. ' ' . $arrRowDatas["AddressCity"];
			$pdfDataRow .= '<input type="hidden" name="arrProvisionValues['.$countRow.'][orderDocumentsAddressZipcode]" value="' . $arrRowDatas["AddressZipcode"] . '" />';
			$pdfDataRow .= '<input type="hidden" name="arrProvisionValues['.$countRow.'][orderDocumentsAddressCity]" value="' . $arrRowDatas["AddressCity"] . '" />';
			$pdfDataRow .= '</td>';
			$pdfDataRow .= '<td style="background-color:#FEFFAF;">';
			if($dataType == 'shipping') { $pdfDataRow .= '<span class="notice" style="color:#FF0000;">'; }
			$pdfDataRow .= wordwrap(specialSubstr($arrRowDatas["ProductName"], 36), 20, "<br>", false);
			if($dataType == 'shipping') { $pdfDataRow .= '</span>'; }
			$pdfDataRow .= '<input type="hidden" name="arrProvisionValues['.$countRow.'][orderDocumentDetailProductName]" value="' . specialSubstr($arrRowDatas["ProductName"]) . '" />';
			$pdfDataRow .= '<input type="hidden" name="arrProvisionValues['.$countRow.'][orderDocumentDetailProductNumber]" value="' . $arrRowDatas["ProductNumber"] . '" />';
			$pdfDataRow .= '</td>';
			$pdfDataRow .= '<td style="text-align:right;" class="cellQuantity">';
			$pdfDataRow .= '<!-- BOF QUANTITY_VALUE_'.$countRow.' -->';
			if($dataType == 'shipping') {
				$pdfDataRow .= '<input type="text" name="arrProvisionValues['.$countRow.'][orderDocumentDetailProductQuantity]" class="inputField_20 setProductQuantity" style="text-align:right;" value="' . $arrRowDatas["ProductQuantity"] . '" />';
			}
			else {
				$pdfDataRow .= '<b>' . $arrRowDatas["ProductQuantity"] . '</b>';
				$pdfDataRow .= '<input type="hidden" name="arrProvisionValues['.$countRow.'][orderDocumentDetailProductQuantity]" value="' . $arrRowDatas["ProductQuantity"] . '" />';
			}
			$pdfDataRow .= '<!-- EOF QUANTITY_VALUE_'.$countRow.' -->';
			$pdfDataRow .= '</td>';
			$pdfDataRow .= '<td style="text-align:right; white-space:nowrap;" class="cellVkprice">';
			$pdfDataRow .= '<!-- BOF VK_PRICE_'.$countRow.' -->';
			$thisPriceStyle = '';
			if($dataType != 'additional'){
				if($arrRowDatas["ProductSinglePrice"] < 0) {
					if($arrRowDatas["DocumentsType"] != 'GU' && ($dataType == 'standard')) {
						$arrRowDatas["ProductSinglePrice"] = 0;
					}
					else {
						$thisPriceStyle = ' font-weight:bold; color:#FF0000; ';
					}
				}
			}
			else {
				if($arrRowDatas["ProductSinglePrice"] < 0) {
					$thisPriceStyle = ' font-weight:bold; color:#FF0000; ';
				}
			}
			$pdfDataRow .= '<input type="text" class="inputField_40" style="text-align:right;' . $thisPriceStyle . '" name="arrProvisionValues['.$countRow.'][salesmanProvisionProductVkprice]" value="' . number_format($arrRowDatas["ProductSinglePrice"], 2, ',' ,'') . '"/> EUR';
			$pdfDataRow .= '<!-- EOF VK_PRICE_'.$countRow.' -->';
			$pdfDataRow .= '</td>';
			$pdfDataRow .= '<td style="text-align:right; white-space:nowrap;background-color:#FEFFAF;" class="cellProvision">';
#echo 'pp1:'.$arrRowDatas["ProductProvisionPrice"].'<hr />';
			if($arrRowDatas["ProductProvisionPrice"] == '' || $arrRowDatas["ProductProvisionPrice"] == null) {
				$arrRowDatas["ProductProvisionPrice"] == 0;
			}
#echo 'pp2:'.$arrRowDatas["ProductProvisionPrice"].'<hr />';
			if($arrSalesmanDatas["salesmenProvision"] > 0){
				// $thisProvision = $arrRowDatas["ProductQuantity"] * ($arrRowDatas["ProductSinglePrice"] * $arrSalesmanDatas["salesmenProvision"]/100);
				$thisProvision = $arrRowDatas["ProductQuantity"] * $arrRowDatas["ProductSinglePrice"];

				if(substr($arrRowDatas["ProductKategorieID"], 0, 3) == '001') {
					#echo 'x:'.$arrRowDatas["ProductKategorieID"] . ' ';

					$thisEkPrice = KZH_EK_PRICE;
					if($arrRowDatas["ProductKategorieID"] == '001009' || $arrRowDatas["ProductKategorieID"] == '001010'){
						$thisEkPrice = KZH_EK_PRICE2;
					}
					if($arrRowDatas["ProductSinglePrice"] > $thisEkPrice){
						#$thisProvision = 0;
					}
					#$thisProvision = 0;
					#echo '<span style="font-size:20px;color:#FF0000;">TEST</span><hr />';
				}
			}
			else {
				#echo '<span style="font-size:20px;color:#FF0000;">TEST</span><hr />';
				#$thisProvision = 0;
				#echo 'xxx' . $useSalesmenProvisionPerRow . '<br>';
				if(substr($arrRowDatas["ProductKategorieID"], 0, 3) == '001') {
					if($useSalesmenProvisionPerRow > 0){
						$thisProvision = 0;
					}
					else {
						$thisEkPrice = KZH_EK_PRICE;
						if($arrRowDatas["ProductKategorieID"] == '001009' || $arrRowDatas["ProductKategorieID"] == '001010'){
							$thisEkPrice = KZH_EK_PRICE2;
						}
						$thisProvision = valueSign($arrRowDatas["ProductSinglePrice"]) * $arrRowDatas["ProductQuantity"] * (abs($arrRowDatas["ProductSinglePrice"]) - $thisEkPrice);
						#$thisProvision = 0;
						#echo 'thisEkPrice: ' . $thisEkPrice . '<br>';
					}
				}
				else {
					if($useSalesmenProvisionPerRow > 0){
						$thisProvision = (valueSign($arrRowDatas["ProductSinglePrice"]) * $arrRowDatas["ProductQuantity"] * $arrRowDatas["ProductSinglePrice"]) * ($useSalesmenProvisionPerRow / 100);
					}
					else {
						$thisProvision = 0;
					}
				}
			}

			//
			if(is_numeric($arrRowDatas["ProductProvisionPrice"])){
				$thisProvision = $arrRowDatas["ProductProvisionPrice"];
			}

			#echo 'pp3:'.$arrRowDatas["ProductProvisionPrice"].'<hr />';
			#echo 'x: ' . $arrRowDatas["ProductProvisionPrice"] . '|is_int: ' . is_int($arrRowDatas["ProductProvisionPrice"]) . '|is_float: ' . is_float($arrRowDatas["ProductProvisionPrice"]);

			# if($thisProvision < 0) { $thisProvision = 0; }

			#echo $thisProvision.'<br />';

			$thisDisplay = '';
			if($arrRowDatas["DetailsDisabled"] != '1'){
				$displayTotalSum += $thisProvision;
			}
			if($arrRowDatas["DetailsDisabled"] == '1'){
				$thisDisplay = ' disabled="disabled" ';
			}
			# echo $arrSalesmanDatas["salesmenProvision"] . '|' . $displayTotalSum . ' / ' . $thisProvision . ' / ' . $arrRowDatas["ProductProvisionPrice"] . '<br />';
			$pdfDataRow .= '<!-- BOF PROVISION_PRICE_'.$countRow.' -->';
			$pdfDataRow .= '<input type="text" class="inputField_50" style="text-align:right;' . $thisPriceStyle . '" name="arrProvisionValues['.$countRow.'][salesmanProvisionValue]" value="' . number_format($thisProvision, 2, ',', '') . '" ' . $thisDisplay . '/>';
			$pdfDataRow .= '<!-- EOF PROVISION_PRICE_'.$countRow.' -->';
			$pdfDataRow .= ' EUR';
			$pdfDataRow .= '</td>';

			$pdfDataRow .= '<!-- BOF DELETE_AREA -->';
			$thisChecked = '';
			$thisTitle = 'Eintrag deaktivieren';
			if($arrRowDatas["DetailsDisabled"] == '1'){
				$thisChecked = ' checked="checked" ';
				$thisTitle = 'Eintrag aktivieren';
			}
			$pdfDataRow .= '<td title="' . $thisTitle . '">';
			$pdfDataRow .= '<input type="checkbox" class="deactivateProvision" name="arrProvisionValues['.$countRow.'][salesmenProvisionsDetailsDisabled]" ' . $thisChecked . ' value="1" />';
			$pdfDataRow .= '</td>';
			$pdfDataRow .= '<!-- EOF DELETE_AREA -->';
			$pdfDataRow .= '</tr>';

			if($arrRowDatas["DetailsDisabled"] == '1'){
				$pdfDataRow .= '<!-- EOF DEACTIVATED_ROW -->';
			}
		}
		return $pdfDataRow;
	}

	if(($_POST["submitFormProvision"] != '' || $_POST["createDocumentProvision"] != '')){
		##dd('_POST');
	}

?>
<?php
	require_once('inc/headerHTML.inc.php');
	$thisTitle = "Berechnung der Vertreter-Provision (Interne Gutschriften)";
	if($_POST["exportOrdersVertreterID"] != "") {
		$thisTitle .= ' - <span class="headerSelectedEntry">Vertreter '.$arrSalesmenDatas[$_POST["exportOrdersVertreterID"]]["customersFirmenname"] . '</span>';
	}
	$headerHTML = preg_replace("/{###TITLE###}/", strip_tags($thisTitle), $headerHTML);
	echo $headerHTML;

	require_once(FILE_MENUE_TOP);
?>

<div id="mainArea">
	<div id="mainContent">
		<?php require_once(FILE_MENUE_SIDEBAR); ?>
		<div id="contentArea">
			<a name="top"></a>
			<div id="contentAreaElements">
				<h1><?php if(SHOW_PAGE_TITLE_ICONS) { echo '<img src="' . PATH_ICONS_MENUE_TITLES . 'export.png" alt="" />'; } ?> <?php echo $thisTitle; ?></h1>

				<div id="searchFilterArea">
					<form name="formSearchDatas" method="post" action="<?php echo $_SERVER["PHP_SELF"]; ?>">
					<table border="0" cellpadding="0" cellspacing="0" class="searchFilterContent">
						<tr>
							<td>
								<label for="exportOrdersVertreterID">Vertreter:</label>
								<select id="exportOrdersVertreterID" name="exportOrdersVertreterID" class="inputSelect_300">
									<option value=""> - </option>
									<?php
										if(!empty($arrSalesmenDatas)) {
											$thisMarker = '';
											foreach($arrSalesmenDatas as $thisKey => $thisValue) {
												if($arrSalesmenDatas[$thisKey]["salesmenGetProvision"] == '1'){
													$selected = '';
													if($thisKey == $_POST["exportOrdersVertreterID"]) {
														$selected = ' selected ';
													}
													echo '<option value="' . $thisKey . '" '.$selected.' >' . ($arrSalesmenDatas[$thisKey]["customersFirmenname"]). ' | K-NR: '.$arrSalesmenDatas[$thisKey]["customersKundennummer"].'</option>';
												}
											}
										}
									?>
								</select>
							</td>
							<td>
								<label for="exportOrdersDateFrom">von:</label>
								<?php
									if($_POST["exportOrdersDateFrom"] != '') {
										$fromDate = $_POST["exportOrdersDateFrom"];
									}
									else {
										$fromDate = formatDate(date("Y-m-d", mktime(1, 1, 1, (date("m") - 1), (1), date("Y"))), "display");
									}
								?>
								<input type="text" name="exportOrdersDateFrom" id="exportOrdersDateFrom" maxlength="2" class="inputField_70" readonly value="<?php echo $fromDate; ?>" />
							</td>
							<td>
								<label for="exportOrdersDateTo">bis:</label>
								<?php
									if($_POST["exportOrdersDateTo"] != '') {
										$toDate = $_POST["exportOrdersDateTo"];
									}
									else {
										$toDate = formatDate(date("Y-m-d", mktime(1, 1, 1, date("m"), (0), date("Y"))), "display");
									}
								?>
								<input type="text" name="exportOrdersDateTo" id="exportOrdersDateTo" maxlength="2" class="inputField_70" readonly value="<?php echo $toDate; ?>" />
							</td>
							<!--
							<td>
								<input type="checkbox" name="displayUnExportedLastMonth" value="1" <?php if($_POST["displayUnExportedLastMonth"] == '1'){ echo ' checked="checked" '; } ?> /> <span class="infotext">Unberechnete Provisionen des Vormonats anzeigen?</span>
							</td>
							-->
							<td>
								<input type="hidden" name="editID" id="editID" value="" />
								<input type="submit" name="submitExport" class="inputButton0" value="Vertreter-Auftr&auml;ge anzeigen" />
							</td>
						</tr>
					</table>
					<input type="hidden" name="submitExportForm" id="submitExportForm" value="1" />
					</form>
				</div>

				<div class="contentDisplay">
					<?php
						if($_POST["exportOrdersVertreterID"] != "") {
							#$thisCreatedBaseName = 'Interne-Gutschrift_' . date('Y-m-d') . '_' . convertChars($arrSalesmenDatas[$_POST["exportOrdersVertreterID"]]["customersFirmenname"]) . '_' . $arrSalesmenDatas[$_POST["exportOrdersVertreterID"]]["customersKundennummer"];
							#$thisCreatedBaseName = 'Interne-Gutschrift_' . date('Y-m-d') . '_' . convertChars($arrSalesmenDatas[$_POST["exportOrdersVertreterID"]]["customersFirmenname"]) . '_' . $arrSalesmenDatas[$_POST["exportOrdersVertreterID"]]["customersKundennummer"];

							$thisCreatedBaseName = 'Interne-Gutschrift_' . $arrSalesmenDatas[$_POST["exportOrdersVertreterID"]]["customersKundennummer"] . '_' . convertChars($arrSalesmenDatas[$_POST["exportOrdersVertreterID"]]["customersFirmenname"]);
							$thisCreatedPdfName = $thisCreatedBaseName . '_{###DATE_END###}_{###DATE_START###}.pdf';

							if($_REQUEST["exportOrdersDateTo"] != "" && $_REQUEST["exportOrdersDateFrom"] != ""){
								$thisCreatedPdfName = preg_replace("/{###DATE_END###}/", formatDate($_REQUEST["exportOrdersDateTo"], 'store'), $thisCreatedPdfName);
								$thisCreatedPdfName = preg_replace("/{###DATE_START###}/", formatDate($_REQUEST["exportOrdersDateFrom"], 'store'), $thisCreatedPdfName);
							}


							#$thisCreatedDocumentNumber = "PR-".date('ym') . '_' . $arrSalesmanDatas["customersKundennummer"];
							$thisCreatedDocumentNumber = "PR-".date('ym', strtotime(formatDate($_REQUEST["exportOrdersDateTo"], "store"))) . '_' . $arrSalesmanDatas["customersKundennummer"];
							echo '<h2>' . $thisCreatedBaseName . '</h2>';
							if($arrSalesmanDatas["salesmenUseMwst"] == '1'){
								echo '<p class="infoArea">MwSt. wird angezeigt.</p>';
							}
							else {
								echo '<p class="infoArea">MwSt. wird nicht angezeigt.</p>';
							}
							echo '<table cellpadding="0" cellspacing="0">';
							echo '<tr>';
							echo '<td><b>Vertreter-Name: </b></td>';
							echo '<td>';
							echo $arrSalesmanDatas["salesmenFirmenname"] . ' [' . $arrSalesmanDatas["customersKundennummer"] . ']';
							echo '</td>';
							echo '</tr>';

							if($arrSalesmanDatas["salesmenProvision"] > 0){
								echo '<tr>';
								echo '<td><b>Vertreter-Provision: </b></td>';
								echo '<td><b style="color:#CC0000;">' . number_format($arrSalesmanDatas["salesmenProvision"], 2, ',', '') . ' %</b> vom Netto-Preis</td>';
								echo '</tr>';
							}
							else if($useSalesmenProvisionPerRow > 0){
								echo '<tr>';
								echo '<td><b>Vertreter-Provision: </b></td>';
								echo '<td><b style="color:#CC0000;">' . number_format($useSalesmenProvisionPerRow, 2, ',', '') . ' %</b> vom Netto-Preis</td>';
								echo '</tr>';
							}

							echo '<tr>';
							echo '<td><b>Vertreter-Gebiete: </b></td>';
							echo '<td>';
							echo preg_replace("/;/", " / ", $arrSalesmanDatas["salesmenAreas"]);
							if(SALESMEN_TERRITORY_PROTECTION && $arrSalesmanDatas["salesmenAreaProtection"] == '1') {
								echo '<br /><p class="infotext">Es werden nur Auftr&auml;ge innerhalb des Vertretergebietes angezeigt.</p>';
							}
							echo '</td>';
							echo '</tr>';
							echo '</table>';

							echo '<div>';
							echo '<span title="Zu den Kundendaten von ' . $arrSalesmanDatas["salesmenFirmenname"] . '"><a href="' . PAGE_EDIT_CUSTOMER. '?searchCustomerNumber=' . $arrSalesmanDatas["customersKundennummer"] . '" class="linkButton" onclick="return showWarning(\'Wollen Sie diese Seite wirklich verlassen?\nNicht gespeicherte Daten gehen verloren.\');">Zu den Kundendaten</a></span>';
							echo '<span title="Zu den Vertreterdaten von ' . $arrSalesmanDatas["salesmenFirmenname"] . '"><a href="' . PAGE_EDIT_SALESMEN. '?editID=' . $arrSalesmanDatas["salesmenID"] . '" class="linkButton" class="linkButton" onclick="return showWarning(\'Wollen Sie diese Seite wirklich verlassen?\nNicht gespeicherte Daten gehen verloren.\');">Zu den Vertreterdaten</a></span>';
							echo '<div class="clear"></div>';
							echo '</div>';
							echo '<hr />';

							// BOF CREATE SQL FOR PROVISIONS FROM INVOICES DEPENDING ON SALESMAN AREAS

							$arrAreas = explode(';', $arrSalesmanDatas["salesmenAreas"]);
							$where = "";

							if(SALESMEN_TERRITORY_PROTECTION && $arrSalesmanDatas["salesmenAreaProtection"] == '1') {
								if(!empty($arrAreas)) {
									$where .= " AND (";
									$arrWhere = array();

									foreach($arrAreas as $thisZipcode) {
										if(preg_match("/-/", $thisZipcode)) {
											$arrZipcodes = explode("-", $thisZipcode);
											$arrWhere[] = " (`" . "{###THIS_TABLE###}" . "`.`orderDocumentsAddressZipcode` BETWEEN " . $arrZipcodes[0] . str_repeat("0", (5 - strlen($arrZipcodes[0]))) . " AND " . $arrZipcodes[1] . str_repeat("9", (5 - strlen($arrZipcodes[1]))) . ") ";
										}
										else {
											$arrWhere[] = " `" . "{###THIS_TABLE###}" . "`.`orderDocumentsAddressZipcode` LIKE '" . $thisZipcode . "%' ";
										}
									}
									$where .= implode (" OR ", $arrWhere);
									$where .= ") ";
								}
							}

							$sql_basic = "
									SELECT
										`tempTable`.`salesmenProvisionsDetailsID`,
										`tempTable`.`salesmenProvisionsDetailsProvisionsID`,
										`tempTable`.`salesmenProvisionsDetailsDocumentsDetailID`,
										`tempTable`.`salesmenProvisionsDetailsDocumentNumber`,
										`tempTable`.`salesmenProvisionsDetailsProductOrderDate`,
										`tempTable`.`salesmenProvisionsDetailsProductAmount`,
										`tempTable`.`salesmenProvisionsDetailsProductNumber`,
										`tempTable`.`salesmenProvisionsDetailsProductName`,
										`tempTable`.`salesmenProvisionsDetailsProductRecipientCustomerNumber`,
										`tempTable`.`salesmenProvisionsDetailsProductRecipientName`,
										`tempTable`.`salesmenProvisionsDetailsProductRecipientZipcode`,
										`tempTable`.`salesmenProvisionsDetailsProductRecipientCity`,
										`tempTable`.`salesmenProvisionsDetailsProductVkPreis`,
										`tempTable`.`salesmenProvisionsDetailsProductProvisionPrice`,
										`tempTable`.`salesmenProvisionsDetailsDisabled`,
										`tempTable`.`salesmenProvisionsDetailsType`,

										`tempTable`.`orderDocumentsID`,
										`tempTable`.`orderDocumentsNumber`,
										`tempTable`.`orderDocumentsType`,
										`tempTable`.`orderDocumentsOrdersIDs`,
										`tempTable`.`orderDocumentsCustomerNumber`,
										`tempTable`.`orderDocumentsProcessingDate`,
										`tempTable`.`orderDocumentsDocumentDate`,
										`tempTable`.`orderDocumentsOrderDate`,
										`tempTable`.`orderDocumentsInvoiceDate`,
										`tempTable`.`orderDocumentsCreditDate`,
										`tempTable`.`orderDocumentsDeliveryDate`,
										`tempTable`.`orderDocumentsSalesman`,
										`tempTable`.`orderDocumentsKommission`,
										`tempTable`.`orderDocumentsAddressCompany`,
										`tempTable`.`orderDocumentsAddressName`,
										`tempTable`.`orderDocumentsAddressStreet`,
										`tempTable`.`orderDocumentsAddressStreetNumber`,
										`tempTable`.`orderDocumentsAddressZipcode`,
										`tempTable`.`orderDocumentsAddressCity`,
										`tempTable`.`orderDocumentsAddressCountry`,
										`tempTable`.`orderDocumentsAddressMail`,
										`tempTable`.`orderDocumentsSumPrice`,
										`tempTable`.`orderDocumentsDiscount`,
										`tempTable`.`orderDocumentsDiscountType`,
										`tempTable`.`orderDocumentsDiscountPercent`,
										`tempTable`.`orderDocumentsMwst`,
										`tempTable`.`orderDocumentsMwstPrice`,
										`tempTable`.`orderDocumentsShippingCosts`,
										`tempTable`.`orderDocumentsShippingCostsPackages`,
										`tempTable`.`orderDocumentsShippingCostsPerPackage`,
										`tempTable`.`orderDocumentsPackagingCosts`,
										`tempTable`.`orderDocumentsCashOnDelivery`,
										`tempTable`.`orderDocumentsTotalPrice`,
										`tempTable`.`orderDocumentsDocumentPath`,
										`tempTable`.`orderDocumentsTimestamp`,
										`tempTable`.`orderDocumentsStatus`,
										`tempTable`.`orderDocumentsSendMail`,

										`tempTable`.`orderDocumentDetailOrderID`,
										`tempTable`.`orderDocumentDetailDocumentID`,
										`tempTable`.`orderDocumentDetailID`,
										`tempTable`.`orderDocumentDetailOrderType`,
										`tempTable`.`orderDocumentDetailProductNumber`,
										`tempTable`.`orderDocumentDetailProductName`,
										`tempTable`.`orderDocumentDetailProductQuantity`,
										`tempTable`.`orderDocumentDetailProductSinglePrice`,
										`tempTable`.`orderDocumentDetailPosType`,
										`tempTable`.`orderDocumentDetailProductKategorieID`,

										`tempTable`.`documentsToDocumentsCreatedDocumentNumber`,

										`tempTable`.`concatGroup`,

										`" . TABLE_CUSTOMERS . "`.`customersID`,
										`" . TABLE_CUSTOMERS . "`.`customersKundennummer`,
										`" . TABLE_CUSTOMERS . "`.`customersFirmenname`,
										`" . TABLE_CUSTOMERS . "`.`customersCompanyPLZ`,
										`" . TABLE_CUSTOMERS . "`.`customersCompanyOrt`

										FROM (

												SELECT
													`" . TABLE_SALESMEN_PROVISIONS_DETAILS . "`.`salesmenProvisionsDetailsID`,
													`" . TABLE_SALESMEN_PROVISIONS_DETAILS . "`.`salesmenProvisionsDetailsProvisionsID`,
													`" . TABLE_SALESMEN_PROVISIONS_DETAILS . "`.`salesmenProvisionsDetailsDocumentsDetailID`,
													`" . TABLE_SALESMEN_PROVISIONS_DETAILS . "`.`salesmenProvisionsDetailsDocumentNumber`,
													`" . TABLE_SALESMEN_PROVISIONS_DETAILS . "`.`salesmenProvisionsDetailsProductOrderDate`,
													`" . TABLE_SALESMEN_PROVISIONS_DETAILS . "`.`salesmenProvisionsDetailsProductAmount`,
													`" . TABLE_SALESMEN_PROVISIONS_DETAILS . "`.`salesmenProvisionsDetailsProductNumber`,
													`" . TABLE_SALESMEN_PROVISIONS_DETAILS . "`.`salesmenProvisionsDetailsProductName`,
													`" . TABLE_SALESMEN_PROVISIONS_DETAILS . "`.`salesmenProvisionsDetailsProductRecipientCustomerNumber`,
													`" . TABLE_SALESMEN_PROVISIONS_DETAILS . "`.`salesmenProvisionsDetailsProductRecipientName`,
													`" . TABLE_SALESMEN_PROVISIONS_DETAILS . "`.`salesmenProvisionsDetailsProductRecipientZipcode`,
													`" . TABLE_SALESMEN_PROVISIONS_DETAILS . "`.`salesmenProvisionsDetailsProductRecipientCity`,
													`" . TABLE_SALESMEN_PROVISIONS_DETAILS . "`.`salesmenProvisionsDetailsProductVkPreis`,
													`" . TABLE_SALESMEN_PROVISIONS_DETAILS . "`.`salesmenProvisionsDetailsProductProvisionPrice`,
													`" . TABLE_SALESMEN_PROVISIONS_DETAILS . "`.`salesmenProvisionsDetailsDisabled`,
													`" . TABLE_SALESMEN_PROVISIONS_DETAILS . "`.`salesmenProvisionsDetailsType`,

													`" . TABLE_ORDER_CONFIRMATIONS . "`.`orderDocumentsID`,
													`" . TABLE_ORDER_CONFIRMATIONS . "`.`orderDocumentsNumber`,
													`" . TABLE_ORDER_CONFIRMATIONS . "`.`orderDocumentsType`,
													`" . TABLE_ORDER_CONFIRMATIONS . "`.`orderDocumentsOrdersIDs`,
													`" . TABLE_ORDER_CONFIRMATIONS . "`.`orderDocumentsCustomerNumber`,
													`" . TABLE_ORDER_CONFIRMATIONS . "`.`orderDocumentsProcessingDate`,
													`" . TABLE_ORDER_CONFIRMATIONS . "`.`orderDocumentsDocumentDate`,
													`" . TABLE_ORDER_CONFIRMATIONS . "`.`orderDocumentsOrderDate`,
													`" . TABLE_ORDER_CONFIRMATIONS . "`.`orderDocumentsInvoiceDate`,
													`" . TABLE_ORDER_CONFIRMATIONS . "`.`orderDocumentsCreditDate`,
													`" . TABLE_ORDER_CONFIRMATIONS . "`.`orderDocumentsDeliveryDate`,
													`" . TABLE_ORDER_CONFIRMATIONS . "`.`orderDocumentsSalesman`,
													`" . TABLE_ORDER_CONFIRMATIONS . "`.`orderDocumentsKommission`,
													`" . TABLE_ORDER_CONFIRMATIONS . "`.`orderDocumentsAddressCompany`,
													`" . TABLE_ORDER_CONFIRMATIONS . "`.`orderDocumentsAddressName`,
													`" . TABLE_ORDER_CONFIRMATIONS . "`.`orderDocumentsAddressStreet`,
													`" . TABLE_ORDER_CONFIRMATIONS . "`.`orderDocumentsAddressStreetNumber`,
													`" . TABLE_ORDER_CONFIRMATIONS . "`.`orderDocumentsAddressZipcode`,
													`" . TABLE_ORDER_CONFIRMATIONS . "`.`orderDocumentsAddressCity`,
													`" . TABLE_ORDER_CONFIRMATIONS . "`.`orderDocumentsAddressCountry`,
													`" . TABLE_ORDER_CONFIRMATIONS . "`.`orderDocumentsAddressMail`,
													`" . TABLE_ORDER_CONFIRMATIONS . "`.`orderDocumentsSumPrice`,
													`" . TABLE_ORDER_CONFIRMATIONS . "`.`orderDocumentsDiscount`,
													`" . TABLE_ORDER_CONFIRMATIONS . "`.`orderDocumentsDiscountType`,
													`" . TABLE_ORDER_CONFIRMATIONS . "`.`orderDocumentsDiscountPercent`,
													`" . TABLE_ORDER_CONFIRMATIONS . "`.`orderDocumentsMwst`,
													`" . TABLE_ORDER_CONFIRMATIONS . "`.`orderDocumentsMwstPrice`,
													`" . TABLE_ORDER_CONFIRMATIONS . "`.`orderDocumentsShippingCosts`,
													`" . TABLE_ORDER_CONFIRMATIONS . "`.`orderDocumentsShippingCostsPackages`,
													`" . TABLE_ORDER_CONFIRMATIONS . "`.`orderDocumentsShippingCostsPerPackage`,
													`" . TABLE_ORDER_CONFIRMATIONS . "`.`orderDocumentsPackagingCosts`,
													`" . TABLE_ORDER_CONFIRMATIONS . "`.`orderDocumentsCashOnDelivery`,
													`" . TABLE_ORDER_CONFIRMATIONS . "`.`orderDocumentsTotalPrice`,
													`" . TABLE_ORDER_CONFIRMATIONS . "`.`orderDocumentsDocumentPath`,
													`" . TABLE_ORDER_CONFIRMATIONS . "`.`orderDocumentsTimestamp`,
													`" . TABLE_ORDER_CONFIRMATIONS . "`.`orderDocumentsStatus`,
													`" . TABLE_ORDER_CONFIRMATIONS . "`.`orderDocumentsSendMail`,

													`" . TABLE_ORDER_CONFIRMATIONS_DETAILS . "`.`orderDocumentDetailID`,
													`" . TABLE_ORDER_CONFIRMATIONS_DETAILS . "`.`orderDocumentDetailDocumentID`,
													`" . TABLE_ORDER_CONFIRMATIONS_DETAILS . "`.`orderDocumentDetailOrderID`,
													`" . TABLE_ORDER_CONFIRMATIONS_DETAILS . "`.`orderDocumentDetailOrderType`,
													`" . TABLE_ORDER_CONFIRMATIONS_DETAILS . "`.`orderDocumentDetailProductNumber`,
													`" . TABLE_ORDER_CONFIRMATIONS_DETAILS . "`.`orderDocumentDetailProductName`,
													`" . TABLE_ORDER_CONFIRMATIONS_DETAILS . "`.`orderDocumentDetailProductKategorieID`,
													`" . TABLE_ORDER_CONFIRMATIONS_DETAILS . "`.`orderDocumentDetailProductQuantity`,
													`" . TABLE_ORDER_CONFIRMATIONS_DETAILS . "`.`orderDocumentDetailProductSinglePrice`,
													`" . TABLE_ORDER_CONFIRMATIONS_DETAILS . "`.`orderDocumentDetailPosType`,
													`" . TABLE_ORDER_CONFIRMATIONS_DETAILS . "`.`orderDocumentDetailGroupingID`,

													'' AS `documentsToDocumentsCreatedDocumentNumber`,

													CONCAT(`" . TABLE_ORDER_CONFIRMATIONS . "`.`orderDocumentsNumber`, '-', `" . TABLE_ORDER_CONFIRMATIONS_DETAILS . "`.`orderDocumentDetailOrderID`, '-', IF(`" . TABLE_SALESMEN_PROVISIONS_DETAILS . "`.`salesmenProvisionsDetailsID` IS NOT NULL, `" . TABLE_SALESMEN_PROVISIONS_DETAILS . "`.`salesmenProvisionsDetailsID`, '')) AS `concatGroup`

													FROM `" . TABLE_ORDER_CONFIRMATIONS . "`

													LEFT JOIN `" . TABLE_ORDER_CONFIRMATIONS_DETAILS . "`
													ON(`" . TABLE_ORDER_CONFIRMATIONS . "`.`orderDocumentsID` = `" . TABLE_ORDER_CONFIRMATIONS_DETAILS . "`.`orderDocumentDetailDocumentID` )

													LEFT JOIN `" . TABLE_SALESMEN_PROVISIONS_DETAILS . "`
													ON(
														`" . TABLE_ORDER_CONFIRMATIONS . "`.`orderDocumentsNumber` = `" . TABLE_SALESMEN_PROVISIONS_DETAILS . "`.`salesmenProvisionsDetailsDocumentNumber`
														/*
														AND
														`" . TABLE_ORDER_CONFIRMATIONS_DETAILS . "`.`orderDocumentDetailProductNumber` = `" . TABLE_SALESMEN_PROVISIONS_DETAILS . "`.`salesmenProvisionsDetailsProductNumber`
														*/
														AND
														`" . TABLE_ORDER_CONFIRMATIONS_DETAILS . "`.`orderDocumentDetailID` = `" . TABLE_SALESMEN_PROVISIONS_DETAILS . "`.`salesmenProvisionsDetailsDocumentsDetailID`
													)

													WHERE 1
														AND (
															`" . TABLE_SALESMEN_PROVISIONS_DETAILS . "`.`salesmenProvisionsDetailsType` != 'additional'
															OR
															`" . TABLE_SALESMEN_PROVISIONS_DETAILS . "`.`salesmenProvisionsDetailsType` IS NULL
														)
														/*
														AND (
															`" . TABLE_SALESMEN_PROVISIONS_DETAILS . "`.`salesmenProvisionsDetailsDisabled` != '1'
															OR
															`" . TABLE_SALESMEN_PROVISIONS_DETAILS . "`.`salesmenProvisionsDetailsDisabled` IS NULL
														)
														*/
														AND (
															`" . TABLE_ORDER_CONFIRMATIONS . "`.`orderDocumentsSalesman` = '" . $_POST["exportOrdersVertreterID"] . "'
											";

											if($arrSalesmanDatas["salesmenSubSalesmanID"] > 0){

												$sql_basic .= "
															OR
															`" . TABLE_ORDER_CONFIRMATIONS . "`.`orderDocumentsSalesman` = '" .  $arrSalesmanDatas["salesmenSubSalesmanID"] . "'
												";
											}

											$sql_basic .= "
														)
														AND `" . TABLE_ORDER_CONFIRMATIONS_DETAILS . "`.`orderDocumentDetailPosType` = 'product'

														/*
														AND (
															`" . TABLE_SALESMEN_PROVISIONS_DETAILS . "`.`salesmenProvisionsDetailsDocumentNumber` = ''
															OR
															`" . TABLE_SALESMEN_PROVISIONS_DETAILS . "`.`salesmenProvisionsDetailsDocumentNumber` IS NULL
														)
														*/

														" . str_replace("{###THIS_TABLE###}", TABLE_ORDER_CONFIRMATIONS, $where) . "

													HAVING (
														/*
														`orderDocumentsProcessingDate` BETWEEN '{###DATE_EXPORT_START###}' AND '{###DATE_EXPORT_END###}'
														*/
														`orderDocumentsDocumentDate` BETWEEN '{###DATE_EXPORT_START###}' AND '{###DATE_EXPORT_END###}'
													)

												UNION

												SELECT
													`" . TABLE_SALESMEN_PROVISIONS_DETAILS . "`.`salesmenProvisionsDetailsID`,
													`" . TABLE_SALESMEN_PROVISIONS_DETAILS . "`.`salesmenProvisionsDetailsProvisionsID`,
													`" . TABLE_SALESMEN_PROVISIONS_DETAILS . "`.`salesmenProvisionsDetailsDocumentsDetailID`,
													`" . TABLE_SALESMEN_PROVISIONS_DETAILS . "`.`salesmenProvisionsDetailsDocumentNumber`,
													`" . TABLE_SALESMEN_PROVISIONS_DETAILS . "`.`salesmenProvisionsDetailsProductOrderDate`,
													`" . TABLE_SALESMEN_PROVISIONS_DETAILS . "`.`salesmenProvisionsDetailsProductAmount`,
													`" . TABLE_SALESMEN_PROVISIONS_DETAILS . "`.`salesmenProvisionsDetailsProductNumber`,
													`" . TABLE_SALESMEN_PROVISIONS_DETAILS . "`.`salesmenProvisionsDetailsProductName`,
													`" . TABLE_SALESMEN_PROVISIONS_DETAILS . "`.`salesmenProvisionsDetailsProductRecipientCustomerNumber`,
													`" . TABLE_SALESMEN_PROVISIONS_DETAILS . "`.`salesmenProvisionsDetailsProductRecipientName`,
													`" . TABLE_SALESMEN_PROVISIONS_DETAILS . "`.`salesmenProvisionsDetailsProductRecipientZipcode`,
													`" . TABLE_SALESMEN_PROVISIONS_DETAILS . "`.`salesmenProvisionsDetailsProductRecipientCity`,
													`" . TABLE_SALESMEN_PROVISIONS_DETAILS . "`.`salesmenProvisionsDetailsProductVkPreis`,
													`" . TABLE_SALESMEN_PROVISIONS_DETAILS . "`.`salesmenProvisionsDetailsProductProvisionPrice`,
													`" . TABLE_SALESMEN_PROVISIONS_DETAILS . "`.`salesmenProvisionsDetailsDisabled`,
													`" . TABLE_SALESMEN_PROVISIONS_DETAILS . "`.`salesmenProvisionsDetailsType`,

													`" . TABLE_ORDER_INVOICES . "`.`orderDocumentsID`,
													`" . TABLE_ORDER_INVOICES . "`.`orderDocumentsNumber`,
													`" . TABLE_ORDER_INVOICES . "`.`orderDocumentsType`,
													`" . TABLE_ORDER_INVOICES . "`.`orderDocumentsOrdersIDs`,
													`" . TABLE_ORDER_INVOICES . "`.`orderDocumentsCustomerNumber`,
													`" . TABLE_ORDER_INVOICES . "`.`orderDocumentsProcessingDate`,
													`" . TABLE_ORDER_INVOICES . "`.`orderDocumentsDocumentDate`,
													`" . TABLE_ORDER_INVOICES . "`.`orderDocumentsOrderDate`,
													`" . TABLE_ORDER_INVOICES . "`.`orderDocumentsInvoiceDate`,
													`" . TABLE_ORDER_INVOICES . "`.`orderDocumentsCreditDate`,
													`" . TABLE_ORDER_INVOICES . "`.`orderDocumentsDeliveryDate`,
													`" . TABLE_ORDER_INVOICES . "`.`orderDocumentsSalesman`,
													`" . TABLE_ORDER_INVOICES . "`.`orderDocumentsKommission`,
													`" . TABLE_ORDER_INVOICES . "`.`orderDocumentsAddressCompany`,
													`" . TABLE_ORDER_INVOICES . "`.`orderDocumentsAddressName`,
													`" . TABLE_ORDER_INVOICES . "`.`orderDocumentsAddressStreet`,
													`" . TABLE_ORDER_INVOICES . "`.`orderDocumentsAddressStreetNumber`,
													`" . TABLE_ORDER_INVOICES . "`.`orderDocumentsAddressZipcode`,
													`" . TABLE_ORDER_INVOICES . "`.`orderDocumentsAddressCity`,
													`" . TABLE_ORDER_INVOICES . "`.`orderDocumentsAddressCountry`,
													`" . TABLE_ORDER_INVOICES . "`.`orderDocumentsAddressMail`,
													`" . TABLE_ORDER_INVOICES . "`.`orderDocumentsSumPrice`,
													`" . TABLE_ORDER_INVOICES . "`.`orderDocumentsDiscount`,
													`" . TABLE_ORDER_INVOICES . "`.`orderDocumentsDiscountType`,
													`" . TABLE_ORDER_INVOICES . "`.`orderDocumentsDiscountPercent`,
													`" . TABLE_ORDER_INVOICES . "`.`orderDocumentsMwst`,
													`" . TABLE_ORDER_INVOICES . "`.`orderDocumentsMwstPrice`,
													`" . TABLE_ORDER_INVOICES . "`.`orderDocumentsShippingCosts`,
													`" . TABLE_ORDER_INVOICES . "`.`orderDocumentsShippingCostsPackages`,
													`" . TABLE_ORDER_INVOICES . "`.`orderDocumentsShippingCostsPerPackage`,
													`" . TABLE_ORDER_INVOICES . "`.`orderDocumentsPackagingCosts`,
													`" . TABLE_ORDER_INVOICES . "`.`orderDocumentsCashOnDelivery`,
													`" . TABLE_ORDER_INVOICES . "`.`orderDocumentsTotalPrice`,
													`" . TABLE_ORDER_INVOICES . "`.`orderDocumentsDocumentPath`,
													`" . TABLE_ORDER_INVOICES . "`.`orderDocumentsTimestamp`,
													`" . TABLE_ORDER_INVOICES . "`.`orderDocumentsStatus`,
													`" . TABLE_ORDER_INVOICES . "`.`orderDocumentsSendMail`,

													`" . TABLE_ORDER_INVOICES_DETAILS . "`.`orderDocumentDetailID`,
													`" . TABLE_ORDER_INVOICES_DETAILS . "`.`orderDocumentDetailDocumentID`,
													`" . TABLE_ORDER_INVOICES_DETAILS . "`.`orderDocumentDetailOrderID`,
													`" . TABLE_ORDER_INVOICES_DETAILS . "`.`orderDocumentDetailOrderType`,
													`" . TABLE_ORDER_INVOICES_DETAILS . "`.`orderDocumentDetailProductNumber`,
													`" . TABLE_ORDER_INVOICES_DETAILS . "`.`orderDocumentDetailProductName`,
													`" . TABLE_ORDER_INVOICES_DETAILS . "`.`orderDocumentDetailProductKategorieID`,
													`" . TABLE_ORDER_INVOICES_DETAILS . "`.`orderDocumentDetailProductQuantity`,
													`" . TABLE_ORDER_INVOICES_DETAILS . "`.`orderDocumentDetailProductSinglePrice`,
													`" . TABLE_ORDER_INVOICES_DETAILS . "`.`orderDocumentDetailPosType`,
													`" . TABLE_ORDER_INVOICES_DETAILS . "`.`orderDocumentDetailGroupingID`,

													`" . TABLE_DOCUMENTS_TO_DOCUMENTS . "`.`documentsToDocumentsCreatedDocumentNumber`,

													CONCAT(`" . TABLE_ORDER_INVOICES . "`.`orderDocumentsNumber`, '-', `" . TABLE_ORDER_INVOICES_DETAILS . "`.`orderDocumentDetailOrderID`, '-', IF(`" . TABLE_SALESMEN_PROVISIONS_DETAILS . "`.`salesmenProvisionsDetailsID` IS NOT NULL, `" . TABLE_SALESMEN_PROVISIONS_DETAILS . "`.`salesmenProvisionsDetailsID`, '')) AS `concatGroup`

												FROM `" . TABLE_ORDER_INVOICES . "`

												LEFT JOIN `" . TABLE_DOCUMENTS_TO_DOCUMENTS . "`
												ON(`" . TABLE_ORDER_INVOICES . "`.`orderDocumentsNumber` = `" . TABLE_DOCUMENTS_TO_DOCUMENTS . "`.`documentsToDocumentsCreatedDocumentNumber`)

												LEFT JOIN `" . TABLE_ORDER_INVOICES_DETAILS . "`
												ON(`" . TABLE_ORDER_INVOICES . "`.`orderDocumentsID` = `" . TABLE_ORDER_INVOICES_DETAILS . "`.`orderDocumentDetailDocumentID` )

												LEFT JOIN `" . TABLE_SALESMEN_PROVISIONS_DETAILS . "`
												ON(
													`" . TABLE_ORDER_INVOICES . "`.`orderDocumentsNumber` = `" . TABLE_SALESMEN_PROVISIONS_DETAILS . "`.`salesmenProvisionsDetailsDocumentNumber`
													/*
													AND
													`" . TABLE_ORDER_INVOICES_DETAILS . "`.`orderDocumentDetailProductNumber` = `" . TABLE_SALESMEN_PROVISIONS_DETAILS . "`.`salesmenProvisionsDetailsProductNumber`
													*/
													AND
													`" . TABLE_ORDER_INVOICES_DETAILS . "`.`orderDocumentDetailID` = `" . TABLE_SALESMEN_PROVISIONS_DETAILS . "`.`salesmenProvisionsDetailsDocumentsDetailID`
												)

												WHERE 1
													AND (
															`" . TABLE_SALESMEN_PROVISIONS_DETAILS . "`.`salesmenProvisionsDetailsType` != 'additional'
															OR
															`" . TABLE_SALESMEN_PROVISIONS_DETAILS . "`.`salesmenProvisionsDetailsType` IS NULL
													)
													/*
													AND (
														`" . TABLE_SALESMEN_PROVISIONS_DETAILS . "`.`salesmenProvisionsDetailsDisabled` != '1'
														OR
														`" . TABLE_SALESMEN_PROVISIONS_DETAILS . "`.`salesmenProvisionsDetailsDisabled` IS NULL
													)
													*/
													AND (
														`" . TABLE_ORDER_INVOICES . "`.`orderDocumentsSalesman` = '" . $_POST["exportOrdersVertreterID"] . "'
											";
									if($arrSalesmanDatas["salesmenSubSalesmanID"] > 0){
										$sql_basic .= "
													OR
													`" . TABLE_ORDER_INVOICES . "`.`orderDocumentsSalesman` = '" .  $arrSalesmanDatas["salesmenSubSalesmanID"] . "'
											";
									}
									$sql_basic .= "
													)
													AND `" . TABLE_ORDER_INVOICES_DETAILS . "`.`orderDocumentDetailPosType` = 'product'

													/*
													AND (
														`" . TABLE_SALESMEN_PROVISIONS_DETAILS . "`.`salesmenProvisionsDetailsDocumentNumber` = ''
														OR `" . TABLE_SALESMEN_PROVISIONS_DETAILS . "`.`salesmenProvisionsDetailsDocumentNumber` IS NULL
													)
													*/

													" . str_replace("{###THIS_TABLE###}", TABLE_ORDER_INVOICES, $where) . "
												GROUP BY CONCAT(`orderDocumentsNumber`, '-', `" . TABLE_ORDER_INVOICES_DETAILS . "`.`orderDocumentDetailOrderID`, '-', `" . TABLE_SALESMEN_PROVISIONS_DETAILS . "`.`salesmenProvisionsDetailsID`)
												HAVING (
														/*
														`orderDocumentsProcessingDate` BETWEEN '{###DATE_EXPORT_START###}' AND '{###DATE_EXPORT_END###}'
														*/
														`orderDocumentsDocumentDate` BETWEEN '{###DATE_EXPORT_START###}' AND '{###DATE_EXPORT_END###}'
														 AND `documentsToDocumentsCreatedDocumentNumber` IS NULL
												)
												/* ################################################################ */

											UNION

												SELECT
													`" . TABLE_SALESMEN_PROVISIONS_DETAILS . "`.`salesmenProvisionsDetailsID`,
													`" . TABLE_SALESMEN_PROVISIONS_DETAILS . "`.`salesmenProvisionsDetailsProvisionsID`,
													`" . TABLE_SALESMEN_PROVISIONS_DETAILS . "`.`salesmenProvisionsDetailsDocumentsDetailID`,
													`" . TABLE_SALESMEN_PROVISIONS_DETAILS . "`.`salesmenProvisionsDetailsDocumentNumber`,
													`" . TABLE_SALESMEN_PROVISIONS_DETAILS . "`.`salesmenProvisionsDetailsProductOrderDate`,
													`" . TABLE_SALESMEN_PROVISIONS_DETAILS . "`.`salesmenProvisionsDetailsProductAmount`,
													`" . TABLE_SALESMEN_PROVISIONS_DETAILS . "`.`salesmenProvisionsDetailsProductNumber`,
													`" . TABLE_SALESMEN_PROVISIONS_DETAILS . "`.`salesmenProvisionsDetailsProductName`,
													`" . TABLE_SALESMEN_PROVISIONS_DETAILS . "`.`salesmenProvisionsDetailsProductRecipientCustomerNumber`,
													`" . TABLE_SALESMEN_PROVISIONS_DETAILS . "`.`salesmenProvisionsDetailsProductRecipientName`,
													`" . TABLE_SALESMEN_PROVISIONS_DETAILS . "`.`salesmenProvisionsDetailsProductRecipientZipcode`,
													`" . TABLE_SALESMEN_PROVISIONS_DETAILS . "`.`salesmenProvisionsDetailsProductRecipientCity`,
													`" . TABLE_SALESMEN_PROVISIONS_DETAILS . "`.`salesmenProvisionsDetailsProductVkPreis`,
													`" . TABLE_SALESMEN_PROVISIONS_DETAILS . "`.`salesmenProvisionsDetailsProductProvisionPrice`,
													`" . TABLE_SALESMEN_PROVISIONS_DETAILS . "`.`salesmenProvisionsDetailsDisabled`,
													`" . TABLE_SALESMEN_PROVISIONS_DETAILS . "`.`salesmenProvisionsDetailsType`,

													`" . TABLE_ORDER_CREDITS . "`.`orderDocumentsID`,
													`" . TABLE_ORDER_CREDITS . "`.`orderDocumentsNumber`,
													`" . TABLE_ORDER_CREDITS . "`.`orderDocumentsType`,
													`" . TABLE_ORDER_CREDITS . "`.`orderDocumentsOrdersIDs`,
													`" . TABLE_ORDER_CREDITS . "`.`orderDocumentsCustomerNumber`,
													`" . TABLE_ORDER_CREDITS . "`.`orderDocumentsProcessingDate`,
													`" . TABLE_ORDER_CREDITS . "`.`orderDocumentsDocumentDate`,
													`" . TABLE_ORDER_CREDITS . "`.`orderDocumentsOrderDate`,
													`" . TABLE_ORDER_CREDITS . "`.`orderDocumentsInvoiceDate`,
													`" . TABLE_ORDER_CREDITS . "`.`orderDocumentsCreditDate`,
													`" . TABLE_ORDER_CREDITS . "`.`orderDocumentsDeliveryDate`,
													`" . TABLE_ORDER_CREDITS . "`.`orderDocumentsSalesman`,
													`" . TABLE_ORDER_CREDITS . "`.`orderDocumentsKommission`,
													`" . TABLE_ORDER_CREDITS . "`.`orderDocumentsAddressCompany`,
													`" . TABLE_ORDER_CREDITS . "`.`orderDocumentsAddressName`,
													`" . TABLE_ORDER_CREDITS . "`.`orderDocumentsAddressStreet`,
													`" . TABLE_ORDER_CREDITS . "`.`orderDocumentsAddressStreetNumber`,
													`" . TABLE_ORDER_CREDITS . "`.`orderDocumentsAddressZipcode`,
													`" . TABLE_ORDER_CREDITS . "`.`orderDocumentsAddressCity`,
													`" . TABLE_ORDER_CREDITS . "`.`orderDocumentsAddressCountry`,
													`" . TABLE_ORDER_CREDITS . "`.`orderDocumentsAddressMail`,
													`" . TABLE_ORDER_CREDITS . "`.`orderDocumentsSumPrice`,
													`" . TABLE_ORDER_CREDITS . "`.`orderDocumentsDiscount`,
													`" . TABLE_ORDER_CREDITS . "`.`orderDocumentsDiscountType`,
													`" . TABLE_ORDER_CREDITS . "`.`orderDocumentsDiscountPercent`,
													`" . TABLE_ORDER_CREDITS . "`.`orderDocumentsMwst`,
													`" . TABLE_ORDER_CREDITS . "`.`orderDocumentsMwstPrice`,
													`" . TABLE_ORDER_CREDITS . "`.`orderDocumentsShippingCosts`,
													`" . TABLE_ORDER_CREDITS . "`.`orderDocumentsShippingCostsPackages`,
													`" . TABLE_ORDER_CREDITS . "`.`orderDocumentsShippingCostsPerPackage`,
													`" . TABLE_ORDER_CREDITS . "`.`orderDocumentsPackagingCosts`,
													`" . TABLE_ORDER_CREDITS . "`.`orderDocumentsCashOnDelivery`,
													`" . TABLE_ORDER_CREDITS . "`.`orderDocumentsTotalPrice`,
													`" . TABLE_ORDER_CREDITS . "`.`orderDocumentsDocumentPath`,
													`" . TABLE_ORDER_CREDITS . "`.`orderDocumentsTimestamp`,
													`" . TABLE_ORDER_CREDITS . "`.`orderDocumentsStatus`,
													`" . TABLE_ORDER_CREDITS . "`.`orderDocumentsSendMail`,

													`" . TABLE_ORDER_CREDITS_DETAILS . "`.`orderDocumentDetailID`,
													`" . TABLE_ORDER_CREDITS_DETAILS . "`.`orderDocumentDetailDocumentID`,
													`" . TABLE_ORDER_CREDITS_DETAILS . "`.`orderDocumentDetailOrderID`,
													`" . TABLE_ORDER_CREDITS_DETAILS . "`.`orderDocumentDetailOrderType`,
													`" . TABLE_ORDER_CREDITS_DETAILS . "`.`orderDocumentDetailProductNumber`,
													`" . TABLE_ORDER_CREDITS_DETAILS . "`.`orderDocumentDetailProductName`,
													`" . TABLE_ORDER_CREDITS_DETAILS . "`.`orderDocumentDetailProductKategorieID`,
													`" . TABLE_ORDER_CREDITS_DETAILS . "`.`orderDocumentDetailProductQuantity`,
													(`" . TABLE_ORDER_CREDITS_DETAILS . "`.`orderDocumentDetailProductSinglePrice`) * (-1),
													`" . TABLE_ORDER_CREDITS_DETAILS . "`.`orderDocumentDetailPosType`,
													`" . TABLE_ORDER_CREDITS_DETAILS . "`.`orderDocumentDetailGroupingID`,

													'' AS `documentsToDocumentsCreatedDocumentNumber`,

													CONCAT(`" . TABLE_ORDER_CREDITS . "`.`orderDocumentsNumber`, '-', `" . TABLE_ORDER_CREDITS_DETAILS . "`.`orderDocumentDetailOrderID`, '-', IF(`" . TABLE_SALESMEN_PROVISIONS_DETAILS . "`.`salesmenProvisionsDetailsID` IS NOT NULL, `" . TABLE_SALESMEN_PROVISIONS_DETAILS . "`.`salesmenProvisionsDetailsID`, '')) AS `concatGroup`

													FROM `" . TABLE_ORDER_CREDITS . "`

													LEFT JOIN `" . TABLE_ORDER_CREDITS_DETAILS . "`
													ON(`" . TABLE_ORDER_CREDITS . "`.`orderDocumentsID` = `" . TABLE_ORDER_CREDITS_DETAILS . "`.`orderDocumentDetailDocumentID` )

													LEFT JOIN `" . TABLE_SALESMEN_PROVISIONS_DETAILS . "`
													ON(
														`" . TABLE_ORDER_CREDITS . "`.`orderDocumentsNumber` = `" . TABLE_SALESMEN_PROVISIONS_DETAILS . "`.`salesmenProvisionsDetailsDocumentNumber`
														/*
														AND
														`" . TABLE_ORDER_CREDITS_DETAILS . "`.`orderDocumentDetailProductNumber` = `" . TABLE_SALESMEN_PROVISIONS_DETAILS . "`.`salesmenProvisionsDetailsProductNumber`
														*/
														AND
														`" . TABLE_ORDER_CREDITS_DETAILS . "`.`orderDocumentDetailID` = `" . TABLE_SALESMEN_PROVISIONS_DETAILS . "`.`salesmenProvisionsDetailsDocumentsDetailID`
													)

													WHERE 1
														AND (
															`" . TABLE_SALESMEN_PROVISIONS_DETAILS . "`.`salesmenProvisionsDetailsType` != 'additional'
															OR
															`" . TABLE_SALESMEN_PROVISIONS_DETAILS . "`.`salesmenProvisionsDetailsType` IS NULL
														)
														/*
														AND (
															`" . TABLE_SALESMEN_PROVISIONS_DETAILS . "`.`salesmenProvisionsDetailsDisabled` != '1'
															OR
															`" . TABLE_SALESMEN_PROVISIONS_DETAILS . "`.`salesmenProvisionsDetailsDisabled` IS NULL
														)
														*/
														AND (
															`" . TABLE_ORDER_CREDITS . "`.`orderDocumentsSalesman` = '" . $_POST["exportOrdersVertreterID"] . "'
											";

											if($arrSalesmanDatas["salesmenSubSalesmanID"] > 0){

												$sql_basic .= "
															OR
															`" . TABLE_ORDER_CREDITS . "`.`orderDocumentsSalesman` = '" .  $arrSalesmanDatas["salesmenSubSalesmanID"] . "'
												";
											}

											$sql_basic .= "
														)
														AND `" . TABLE_ORDER_CREDITS_DETAILS . "`.`orderDocumentDetailPosType` = 'product'

														/*
														AND (
															`" . TABLE_SALESMEN_PROVISIONS_DETAILS . "`.`salesmenProvisionsDetailsDocumentNumber` = ''
															OR
															`" . TABLE_SALESMEN_PROVISIONS_DETAILS . "`.`salesmenProvisionsDetailsDocumentNumber` IS NULL
														)
														*/

														" . str_replace("{###THIS_TABLE###}", TABLE_ORDER_CREDITS, $where) . "

													HAVING (
														/*
														`orderDocumentsProcessingDate` BETWEEN '{###DATE_EXPORT_START###}' AND '{###DATE_EXPORT_END###}'
														*/
														`orderDocumentsDocumentDate` BETWEEN '{###DATE_EXPORT_START###}' AND '{###DATE_EXPORT_END###}'
													)
												/* ################################################################ */

										) AS `tempTable`

										LEFT JOIN `" . TABLE_CUSTOMERS . "`
										ON(`tempTable`.`orderDocumentsCustomerNumber` = `" . TABLE_CUSTOMERS . "`.`customersKundennummer`)

									GROUP BY `tempTable`.`concatGroup`
							";
						$sql_order = "
									ORDER BY FIELD(`orderDocumentsType`, 'GU', 'AB', 'RE'), `orderDocumentsDocumentDate` ASC, `orderDocumentsNumber` ASC

							";

						// EOF CREATE SQL FOR PROVISIONS FROM INVOICES DEPENDING ON SALESMAN AREAS

					/*
					WHERE `" . TABLE_ORDER_CONFIRMATIONS . "`.`orderDocumentsProcessingDate`
											BETWEEN '" . formatDate($_POST["exportDateStart"], "store") . "' AND '" . formatDate($_POST["exportDateEnd"], "store")."'

					*/
							$sql = $sql_basic;
							$dateStart = formatDate($_POST["exportOrdersDateFrom"], "store");
							$dateEnd = formatDate($_POST["exportOrdersDateTo"], "store");
							$sql = preg_replace("/{###DATE_EXPORT_START###}/", $dateStart, $sql);
							$sql = preg_replace("/{###DATE_EXPORT_END###}/", $dateEnd, $sql);
							$sqlAdd = '';


							if($_POST["displayUnExportedLastMonth"] == '1') {
								$sqlAdd = $sql_basic;
								$dateStartLastMonth = date("Y-m-d", mktime(0, 0, 0, (date("m", strtotime($dateStart)) - 1), 1, date("Y", strtotime($dateStart))));
								$dateEndLastMonth = date("Y-m-d", mktime(0, 0, 0, (date("m", strtotime($dateStart)) - 0), 0, date("Y", strtotime($dateStart))));
								$sqlAdd = preg_replace("/{###DATE_EXPORT_START###}/", $dateStartLastMonth, $sqlAdd);
								$sqlAdd = preg_replace("/{###DATE_EXPORT_END###}/", $dateEndLastMonth, $sqlAdd);
								$sql .= ' UNION ' . $sqlAdd . ' HAVING `tempTable`.`salesmenProvisionsDetailsID` IS NULL ';
							}

							$sql .= $sql_order;

							$rs = $dbConnection->db_query($sql);

							$countTotalRows = $dbConnection->db_getMysqlNumRows($rs);

							while($ds = mysqli_fetch_assoc($rs)){
								if($thisProvisionID == 0){ $thisProvisionID = $ds["salesmenProvisionsDetailsProvisionsID"]; }

								foreach(array_keys($ds) as $field){
									if($ds["salesmenProvisionsDetailsType"] == '' || $ds["salesmenProvisionsDetailsType"] == NULL){
										$ds["salesmenProvisionsDetailsType"] = 'standard';
									}
									$arrProvisionDatas[$ds["orderDocumentDetailID"]][$ds["salesmenProvisionsDetailsType"]][$field] = $ds[$field];

									if(substr($ds["orderDocumentDetailProductKategorieID"], 0, 3) != '001' && $ds["orderDocumentsType"] != 'GU') {
										if(!($ds["orderDocumentsShippingCosts"] > 0)) {
											$arrProvisionDatas[$ds["orderDocumentDetailID"]]['shippingAdd'][$field] = $ds[$field];
										}
									}
								}
							}

							$sql_additional = "
										SELECT
													`" . TABLE_SALESMEN_PROVISIONS_DETAILS . "`.`salesmenProvisionsDetailsID`,
													`" . TABLE_SALESMEN_PROVISIONS_DETAILS . "`.`salesmenProvisionsDetailsProvisionsID`,
													`" . TABLE_SALESMEN_PROVISIONS_DETAILS . "`.`salesmenProvisionsDetailsDocumentsDetailID`,
													`" . TABLE_SALESMEN_PROVISIONS_DETAILS . "`.`salesmenProvisionsDetailsDocumentNumber`,
													`" . TABLE_SALESMEN_PROVISIONS_DETAILS . "`.`salesmenProvisionsDetailsProductOrderDate`,
													`" . TABLE_SALESMEN_PROVISIONS_DETAILS . "`.`salesmenProvisionsDetailsProductAmount`,
													`" . TABLE_SALESMEN_PROVISIONS_DETAILS . "`.`salesmenProvisionsDetailsProductNumber`,
													`" . TABLE_SALESMEN_PROVISIONS_DETAILS . "`.`salesmenProvisionsDetailsProductName`,
													`" . TABLE_SALESMEN_PROVISIONS_DETAILS . "`.`salesmenProvisionsDetailsProductRecipientCustomerNumber`,
													`" . TABLE_SALESMEN_PROVISIONS_DETAILS . "`.`salesmenProvisionsDetailsProductRecipientName`,
													`" . TABLE_SALESMEN_PROVISIONS_DETAILS . "`.`salesmenProvisionsDetailsProductRecipientZipcode`,
													`" . TABLE_SALESMEN_PROVISIONS_DETAILS . "`.`salesmenProvisionsDetailsProductRecipientCity`,
													`" . TABLE_SALESMEN_PROVISIONS_DETAILS . "`.`salesmenProvisionsDetailsProductVkPreis`,
													`" . TABLE_SALESMEN_PROVISIONS_DETAILS . "`.`salesmenProvisionsDetailsProductProvisionPrice`,
													`" . TABLE_SALESMEN_PROVISIONS_DETAILS . "`.`salesmenProvisionsDetailsDisabled`,
													`" . TABLE_SALESMEN_PROVISIONS_DETAILS . "`.`salesmenProvisionsDetailsType`

												FROM `" . TABLE_SALESMEN_PROVISIONS_DETAILS . "`

												WHERE 1
													/*
													AND `" . TABLE_SALESMEN_PROVISIONS_DETAILS . "`.`salesmenProvisionsDetailsDisabled` != '1'
													*/
													AND `" . TABLE_SALESMEN_PROVISIONS_DETAILS . "`.`salesmenProvisionsDetailsType` = 'additional'
													AND `" . TABLE_SALESMEN_PROVISIONS_DETAILS . "`.`salesmenProvisionsDetailsProductOrderDate` BETWEEN '{###DATE_EXPORT_START###}' AND '{###DATE_EXPORT_END###}'
													AND `" . TABLE_SALESMEN_PROVISIONS_DETAILS . "`.`salesmenProvisionsDetailsProvisionsID` = '" . $thisProvisionID . "'
									";
							$sql_additional = preg_replace("/{###DATE_EXPORT_START###}/", $dateStart, $sql_additional);
							$sql_additional = preg_replace("/{###DATE_EXPORT_END###}/", $dateEnd, $sql_additional);

							$rs_additional = $dbConnection->db_query($sql_additional);
							$countTotalRows_additional = $dbConnection->db_getMysqlNumRows($rs_additional);

							while($ds = mysqli_fetch_assoc($rs_additional)){
								foreach(array_keys($ds) as $field){
									$arrProvisionDatasAdditional[$ds["salesmenProvisionsDetailsID"]][$ds["salesmenProvisionsDetailsType"]][$field] = $ds[$field];
								}
							}

							$contentExportPDF = '';

							if(!empty($arrProvisionDatas)) {
								$contentExportPDF_HEAD = '';
								$contentExportPDF_HEAD .= '<thead>';
								$contentExportPDF_HEAD .= '<tr>';
								$contentExportPDF_HEAD .= '<th>Pos</th>';
								$contentExportPDF_HEAD .= '<!-- BOF DOCUMENT_NUMBER_ORDER -->';
								$contentExportPDF_HEAD .= '<th>Auftragsnummer</th>';
								#$contentExportPDF_HEAD .= '<th>Produktionsstatus</th>';
								$contentExportPDF_HEAD .= '<!-- EOF DOCUMENT_NUMBER_ORDER -->';
								$contentExportPDF_HEAD .= '<th>Datum</th>';
								$contentExportPDF_HEAD .= '<th>K-Nr.</th>';
								$contentExportPDF_HEAD .= '<th>Firma</th>';
								$contentExportPDF_HEAD .= '<th>Ort</th>';
								$contentExportPDF_HEAD .= '<th>Artikel</th>';
								$contentExportPDF_HEAD .= '<th>Menge</th>';
								$contentExportPDF_HEAD .= '<th>VK-Preis</th>';
								if($arrSalesmanDatas["salesmenProvision"] > 0){
									$contentExportPDF_HEAD .= '<th>Gesamt</th>';
								}
								else {
									$contentExportPDF_HEAD .= '<th>Gutschrift</th>';
								}
								$contentExportPDF_HEAD .= '<!-- BOF DELETE_AREA -->';
								$contentExportPDF_HEAD .= '<th><img src="layout/icons/iconDelete.png" width="16 height="16" alt="" title="Entfernen"></th>';
								$contentExportPDF_HEAD .= '<!-- EOF DELETE_AREA -->';
								$contentExportPDF_HEAD .= '</tr>';
								$contentExportPDF_HEAD .= '</thead>';

								$contentExportPDF_TABLE = '<table border="0" cellpadding="0" cellspacing="0" width="0" class="displayOrders" id="displayCardDatas" >';
								#$contentExportPDF_TABLE = '<table border="0" cellpadding="0" cellspacing="0" width="0">';

								echo '<form name="formProvision" action="' . $_SERVER["PHP_SELF"]. '" method="post" enctype="multipart/form-data">';
								echo '<input type="hidden" name="exportOrdersVertreterID" value="' . $_REQUEST["exportOrdersVertreterID"] . '" />';
								$contentExportPDF .= $contentExportPDF_TABLE;

								$contentExportPDF .= $contentExportPDF_HEAD;

								$contentExportPDF .= '<tbody>';

								$countRow = 0;
								$displayTotalSum = 0;
								$thisProvisionID = 0;
								$thisParsedUrl = parse_url($_SERVER["REQUEST_URI"]);
#dd('arrProvisionDatas');
								foreach($arrProvisionDatas as $thisProvisionDatasKey => $thisProvisionDatasValue){
									$arrRowDatasStandard = array(
										"DetailID" => $arrProvisionDatas[$thisProvisionDatasKey]["standard"]["orderDocumentDetailID"],
										"ProductKategorieID" => $arrProvisionDatas[$thisProvisionDatasKey]["standard"]["orderDocumentDetailProductKategorieID"],
										"ProductName" => $arrProvisionDatas[$thisProvisionDatasKey]["standard"]["orderDocumentDetailProductName"],
										"ProductNumber" => $arrProvisionDatas[$thisProvisionDatasKey]["standard"]["orderDocumentDetailProductNumber"],
										"ProductQuantity" => $arrProvisionDatas[$thisProvisionDatasKey]["standard"]["orderDocumentDetailProductQuantity"],
										"ProductSinglePrice" => $arrProvisionDatas[$thisProvisionDatasKey]["standard"]["orderDocumentDetailProductSinglePrice"],
										"AddressCity" => $arrProvisionDatas[$thisProvisionDatasKey]["standard"]["orderDocumentsAddressCity"],
										"AddressCompany" => $arrProvisionDatas[$thisProvisionDatasKey]["standard"]["orderDocumentsAddressCompany"],
										"AddressZipcode" => $arrProvisionDatas[$thisProvisionDatasKey]["standard"]["orderDocumentsAddressZipcode"],
										"CustomerNumber" => $arrProvisionDatas[$thisProvisionDatasKey]["standard"]["orderDocumentsCustomerNumber"],
										"DocumentPath" => $arrProvisionDatas[$thisProvisionDatasKey]["standard"]["orderDocumentsDocumentPath"],
										"DocumentsNumber" => $arrProvisionDatas[$thisProvisionDatasKey]["standard"]["orderDocumentsNumber"],
										"OrderDate" => $arrProvisionDatas[$thisProvisionDatasKey]["standard"]["orderDocumentsOrderDate"],
										"ProcessingDate" => $arrProvisionDatas[$thisProvisionDatasKey]["standard"]["orderDocumentsProcessingDate"],
										"DocumentDate" => $arrProvisionDatas[$thisProvisionDatasKey]["standard"]["orderDocumentsDocumentDate"],
										"DocumentsType" => $arrProvisionDatas[$thisProvisionDatasKey]["standard"]["orderDocumentsType"],
										"ProductProvisionPrice" => $arrProvisionDatas[$thisProvisionDatasKey]["standard"]["salesmenProvisionsDetailsProductProvisionPrice"],
										"DetailsType" => $arrProvisionDatas[$thisProvisionDatasKey]["standard"]["salesmenProvisionsDetailsType"],
										"DetailsDisabled" => $arrProvisionDatas[$thisProvisionDatasKey]["standard"]["salesmenProvisionsDetailsDisabled"],

										"CustomerID" => $arrProvisionDatas[$thisProvisionDatasKey]["standard"]["customersID"],
										"CustomerKundennummer" => $arrProvisionDatas[$thisProvisionDatasKey]["standard"]["customersKundennummer"],
										"CustomerFirmenname" => $arrProvisionDatas[$thisProvisionDatasKey]["standard"]["customersFirmenname"],
										"CustomerPLZ" => $arrProvisionDatas[$thisProvisionDatasKey]["standard"]["customersCompanyPLZ"],
										"CustomerOrt" => $arrProvisionDatas[$thisProvisionDatasKey]["standard"]["customersCompanyOrt"]
									);
									$contentExportPDF .= createPdfDataRow($countRow, "standard", $arrRowDatasStandard);
									#dd('arrRowDatasStandard');

									if($arrProvisionDatas[$thisProvisionDatasKey]["shipping"]["orderDocumentDetailID"] > 0){
										$arrRowDatasShipping = array(
											"DetailID" => $arrProvisionDatas[$thisProvisionDatasKey]["shipping"]["salesmenProvisionsDetailsDocumentsDetailID"],
											"ProductKategorieID" => 'VERSAND',
											"ProductName" => 'Nicht berechnete Versandkosten',
											"ProductNumber" => $arrProvisionDatas[$thisProvisionDatasKey]["shipping"]["salesmenProvisionsDetailsProductNumber"],
											"ProductQuantity" => $arrProvisionDatas[$thisProvisionDatasKey]["shipping"]["salesmenProvisionsDetailsProductAmount"],
											"ProductSinglePrice" => $arrProvisionDatas[$thisProvisionDatasKey]["shipping"]["salesmenProvisionsDetailsProductVkPreis"],
											"AddressCity" => $arrProvisionDatas[$thisProvisionDatasKey]["shipping"]["salesmenProvisionsDetailsProductRecipientCity"],
											"AddressCompany" => $arrProvisionDatas[$thisProvisionDatasKey]["shipping"]["salesmenProvisionsDetailsProductRecipientName"],
											"AddressZipcode" => $arrProvisionDatas[$thisProvisionDatasKey]["shipping"]["salesmenProvisionsDetailsProductRecipientZipcode"],
											"CustomerNumber" => $arrProvisionDatas[$thisProvisionDatasKey]["shipping"]["salesmenProvisionsDetailsProductRecipientCustomerNumber"],
											"DocumentPath" => $arrProvisionDatas[$thisProvisionDatasKey]["shipping"]["orderDocumentsDocumentPath"],
											"DocumentsNumber" => $arrProvisionDatas[$thisProvisionDatasKey]["shipping"]["salesmenProvisionsDetailsDocumentNumber"],
											"OrderDate" => $arrProvisionDatas[$thisProvisionDatasKey]["shipping"]["salesmenProvisionsDetailsProductOrderDate"],
											"ProcessingDate" => $arrProvisionDatas[$thisProvisionDatasKey]["shipping"]["salesmenProvisionsDetailsProductOrderDate"],
											"DocumentsType" => substr($arrProvisionDatas[$thisProvisionDatasKey]["shipping"]["salesmenProvisionsDetailsDocumentNumber"], 0, 2),
											"ProductProvisionPrice" => $arrProvisionDatas[$thisProvisionDatasKey]["shipping"]["salesmenProvisionsDetailsProductProvisionPrice"],
											"DetailsType" => $arrProvisionDatas[$thisProvisionDatasKey]["shipping"]["salesmenProvisionsDetailsType"],
											"DetailsDisabled" => $arrProvisionDatas[$thisProvisionDatasKey]["shipping"]["salesmenProvisionsDetailsDisabled"],

											"CustomerID" => $arrProvisionDatas[$thisProvisionDatasKey]["shipping"]["customersID"],
											"CustomerKundennummer" => $arrProvisionDatas[$thisProvisionDatasKey]["shipping"]["customersKundennummer"],
											"CustomerFirmenname" => $arrProvisionDatas[$thisProvisionDatasKey]["shipping"]["customersFirmenname"],
											"CustomerPLZ" => $arrProvisionDatas[$thisProvisionDatasKey]["shipping"]["customersCompanyPLZ"],
											"CustomerOrt" => $arrProvisionDatas[$thisProvisionDatasKey]["shipping"]["customersCompanyOrt"]
										);
										$countRow++;
										$contentExportPDF .= createPdfDataRow($countRow, "shipping", $arrRowDatasShipping);
									}
									else if($arrProvisionDatas[$thisProvisionDatasKey]["shippingAdd"]["orderDocumentDetailID"] > 0){
										$arrRowDatasShipping = array(
											"DetailID" => $arrProvisionDatas[$thisProvisionDatasKey]["shippingAdd"]["orderDocumentDetailID"],
											"ProductKategorieID" => 'VERSAND',
											"ProductName" => 'Nicht berechnete Versandkosten',
											"ProductNumber" => $arrProvisionDatas[$thisProvisionDatasKey]["shippingAdd"]["orderDocumentDetailProductNumber"],
											"ProductQuantity" => 1,
											"ProductSinglePrice" => (SHIPPING_COSTS * (-1)),
											"AddressCity" => $arrProvisionDatas[$thisProvisionDatasKey]["shippingAdd"]["orderDocumentsAddressCity"],
											"AddressCompany" => $arrProvisionDatas[$thisProvisionDatasKey]["shippingAdd"]["orderDocumentsAddressCompany"],
											"AddressZipcode" => $arrProvisionDatas[$thisProvisionDatasKey]["shippingAdd"]["orderDocumentsAddressZipcode"],
											"CustomerNumber" => $arrProvisionDatas[$thisProvisionDatasKey]["shippingAdd"]["orderDocumentsCustomerNumber"],
											"DocumentPath" => $arrProvisionDatas[$thisProvisionDatasKey]["shippingAdd"]["orderDocumentsDocumentPath"],
											"DocumentsNumber" => $arrProvisionDatas[$thisProvisionDatasKey]["shippingAdd"]["orderDocumentsNumber"],
											"OrderDate" => $arrProvisionDatas[$thisProvisionDatasKey]["shippingAdd"]["orderDocumentsOrderDate"],
											"ProcessingDate" => $arrProvisionDatas[$thisProvisionDatasKey]["shippingAdd"]["orderDocumentsProcessingDate"],
											"DocumentDate" => $arrProvisionDatas[$thisProvisionDatasKey]["shippingAdd"]["orderDocumentsDocumentDate"],
											"DocumentsType" => $arrProvisionDatas[$thisProvisionDatasKey]["shippingAdd"]["orderDocumentsType"],
											"ProductProvisionPrice" => (SHIPPING_COSTS * (-1)),
											"DetailsType" => $arrProvisionDatas[$thisProvisionDatasKey]["shippingAdd"]["salesmenProvisionsDetailsType"],
											"DetailsDisabled" => $arrProvisionDatas[$thisProvisionDatasKey]["shippingAdd"]["salesmenProvisionsDetailsDisabled"],

											"CustomerID" => $arrProvisionDatas[$thisProvisionDatasKey]["shippingAdd"]["customersID"],
											"CustomerKundennummer" => $arrProvisionDatas[$thisProvisionDatasKey]["shippingAdd"]["customersKundennummer"],
											"CustomerFirmenname" => $arrProvisionDatas[$thisProvisionDatasKey]["shippingAdd"]["customersFirmenname"],
											"CustomerPLZ" => $arrProvisionDatas[$thisProvisionDatasKey]["shippingAdd"]["customersCompanyPLZ"],
											"CustomerOrt" => $arrProvisionDatas[$thisProvisionDatasKey]["shippingAdd"]["customersCompanyOrt"]
										);
										$countRow++;
										$contentExportPDF .= createPdfDataRow($countRow, "shipping", $arrRowDatasShipping);
									}

									$contentExportPDF .= createPdfDataRow($countRow, "empty", '');
									$countRow++;
								}

								if(!empty($arrProvisionDatasAdditional)){
									foreach($arrProvisionDatasAdditional as $thisProvisionDatasAdditionalKey => $thisProvisionDatasAdditionalValue){
										$arrRowDatasAdditional = array(
											"DetailID" => $arrProvisionDatasAdditional[$thisProvisionDatasAdditionalKey]["additional"]["salesmenProvisionsDetailsID"],
											"ProductKategorieID" => '',
											"ProductName" => $arrProvisionDatasAdditional[$thisProvisionDatasAdditionalKey]["additional"]["salesmenProvisionsDetailsProductName"],
											"ProductNumber" => $arrProvisionDatasAdditional[$thisProvisionDatasAdditionalKey]["additional"]["salesmenProvisionsDetailsProductNumber"],
											"ProductQuantity" => ($arrProvisionDatasAdditional[$thisProvisionDatasAdditionalKey]["additional"]["salesmenProvisionsDetailsProductQuantity"] > 0)?($arrProvisionDatasAdditional[$thisProvisionDatasAdditionalKey]["additional"]["salesmenProvisionsDetailsProductQuantity"]):(1),
											"ProductSinglePrice" => $arrProvisionDatasAdditional[$thisProvisionDatasAdditionalKey]["additional"]["salesmenProvisionsDetailsProductVkPreis"],
											"AddressCity" => $arrProvisionDatasAdditional[$thisProvisionDatasAdditionalKey]["additional"]["salesmenProvisionsDetailsProductRecipientCity"],
											"AddressCompany" => $arrProvisionDatasAdditional[$thisProvisionDatasAdditionalKey]["additional"]["salesmenProvisionsDetailsProductRecipientName"],
											"AddressZipcode" => $arrProvisionDatasAdditional[$thisProvisionDatasAdditionalKey]["additional"]["salesmenProvisionsDetailsProductRecipientZipcode"],
											"CustomerNumber" => $arrProvisionDatasAdditional[$thisProvisionDatasAdditionalKey]["additional"]["salesmenProvisionsDetailsProductRecipientCustomerNumber"],
											"DocumentPath" => $arrProvisionDatasAdditional[$thisProvisionDatasAdditionalKey]["additional"]["salesmenProvisionsDetailsDocumentNumber"] . '_KNR-' . $arrProvisionDatasAdditional[$thisProvisionDatasAdditionalKey]["additional"]["salesmenProvisionsDetailsProductRecipientCustomerNumber"] . '.pdf',
											"DocumentsNumber" => $arrProvisionDatasAdditional[$thisProvisionDatasAdditionalKey]["additional"]["salesmenProvisionsDetailsDocumentNumber"],
											"OrderDate" => $arrProvisionDatasAdditional[$thisProvisionDatasAdditionalKey]["additional"]["salesmenProvisionsDetailsProductOrderDate"],
											"ProcessingDate" => $arrProvisionDatasAdditional[$thisProvisionDatasAdditionalKey]["additional"]["salesmenProvisionsDetailsProductOrderDate"],
											"DocumentDate" => $arrProvisionDatasAdditional[$thisProvisionDatasAdditionalKey]["additional"]["salesmenProvisionsDetailsProductOrderDate"],
											"DocumentsType" => substr($arrProvisionDatasAdditional[$thisProvisionDatasAdditionalKey]["additional"]["salesmenProvisionsDetailsDocumentNumber"], 0, 2),
											"ProductProvisionPrice" => $arrProvisionDatasAdditional[$thisProvisionDatasAdditionalKey]["additional"]["salesmenProvisionsDetailsProductProvisionPrice"],
											"DetailsType" => $arrProvisionDatasAdditional[$thisProvisionDatasAdditionalKey]["additional"]["salesmenProvisionsDetailsType"],
											"DetailsDisabled" => $arrProvisionDatasAdditional[$thisProvisionDatasAdditionalKey]["additional"]["salesmenProvisionsDetailsDisabled"],

											"CustomerID" => $arrProvisionDatasAdditional[$thisProvisionDatasAdditionalKey]["additional"]["customersID"],
											"CustomerKundennummer" => $arrProvisionDatasAdditional[$thisProvisionDatasAdditionalKey]["additional"]["customersKundennummer"],
											"CustomerFirmenname" => $arrProvisionDatasAdditional[$thisProvisionDatasAdditionalKey]["additional"]["customersFirmenname"],
											"CustomerPLZ" => $arrProvisionDatasAdditional[$thisProvisionDatasAdditionalKey]["additional"]["customersCompanyPLZ"],
											"CustomerOrt" => $arrProvisionDatasAdditional[$thisProvisionDatasAdditionalKey]["additional"]["customersCompanyOrt"]
										);
										$contentExportPDF .= createPdfDataRow($countRow, "additional", $arrRowDatasAdditional);
										$contentExportPDF .= createPdfDataRow($countRow, "empty", '');

										$countRow++;
									}
								}

								$contentExportPDF .= '<tr>';
								$contentExportPDF .= '<td>&nbsp;</td>';
								$contentExportPDF .= '<!-- BOF DOCUMENT_NUMBER_ORDER -->';
								$contentExportPDF .= '<td>&nbsp;</td>';
								$contentExportPDF .= '<!-- EOF DOCUMENT_NUMBER_ORDER -->';
								$contentExportPDF .= '<td colspan="8">&nbsp;</td>';
								$contentExportPDF .= '<!-- BOF DELETE_AREA -->';
								$contentExportPDF .= '<td> </td>';
								$contentExportPDF .= '<!-- EOF DELETE_AREA -->';
								$contentExportPDF .= '</tr>';

								// BOF INTERMEDIATE TOTAL
								if($arrSalesmanDatas["salesmenUseMwst"] == '1'){
									$contentExportPDF .= '<tr>';
									$contentExportPDF .= '<td>&nbsp;</td>';
									$contentExportPDF .= '<!-- BOF DOCUMENT_NUMBER_ORDER -->';
									$contentExportPDF .= '<td>&nbsp;</td>';
									$contentExportPDF .= '<!-- EOF DOCUMENT_NUMBER_ORDER -->';
									$contentExportPDF .= '<td colspan="5">&nbsp;</td>';
									$contentExportPDF .= '<td colspan="2"><b>Zwischensumme:</b></td>';
									$contentExportPDF .= '<td style="white-space:nowrap;text-align:right;"><!-- BOF SUM_VALUE --><b><span id="displayTotalSum">'.number_format($displayTotalSum, 2, ',', '') .'</span></b><!-- EOF SUM_VALUE --> EUR</td>';
									$contentExportPDF .= '<!-- BOF DELETE_AREA -->';
									$contentExportPDF .= '<td> </td>';
									$contentExportPDF .= '<!-- EOF DELETE_AREA -->';
									$contentExportPDF .= '</tr>';
								}
								// EOF INTERMEDIATE TOTAL

								$valueProvision = 0;
								if($arrSalesmanDatas["salesmenProvision"] > 0){
									$contentExportPDF .= '<tr>';
									$contentExportPDF .= '<td>&nbsp;</td>';
									$contentExportPDF .= '<!-- BOF DOCUMENT_NUMBER_ORDER -->';
									$contentExportPDF .= '<td>&nbsp;</td>';
									$contentExportPDF .= '<!-- EOF DOCUMENT_NUMBER_ORDER -->';
									$contentExportPDF .= '<td colspan="5">&nbsp;</td>';
									$contentExportPDF .= '<td colspan="2"><b>Gutschrift (' . $arrSalesmanDatas["salesmenProvision"]. ' %):</b></td>';
									$valueProvision = $displayTotalSum * ($arrSalesmanDatas["salesmenProvision"]/100);
									$contentExportPDF .= '<td style="white-space:nowrap;text-align:right;"><!-- BOF PROVISION_VALUE --><b><span id="displayProvision">'.number_format($valueProvision, 2, ',', '') .'</span></b><!-- EOF PROVISION_VALUE --> EUR</td>';
									$contentExportPDF .= '<!-- BOF DELETE_AREA -->';
									$contentExportPDF .= '<td> </td>';
									$contentExportPDF .= '<!-- EOF DELETE_AREA -->';
									$contentExportPDF .= '</tr>';
								}

								// BOF MWST
								$valueMwst = 0;
								if($arrSalesmanDatas["salesmenUseMwst"] == '1'){
									$contentExportPDF .= '<tr>';
									$contentExportPDF .= '<td>&nbsp;</td>';
									$contentExportPDF .= '<!-- BOF DOCUMENT_NUMBER_ORDER -->';
									$contentExportPDF .= '<td>&nbsp;</td>';
									$contentExportPDF .= '<!-- EOF DOCUMENT_NUMBER_ORDER -->';
									$contentExportPDF .= '<td colspan="5">&nbsp;</td>';
									$contentExportPDF .= '<td colspan="2"><b>MwSt. (' . MWST. ' %):</b></td>';
									$valueMwst = ($displayTotalSum + $valueProvision) * MWST/100;

									$contentExportPDF .= '<td style="white-space:nowrap;text-align:right;"><!-- BOF MWST_VALUE --><b><span id="displayMWST">'.number_format($valueMwst, 2, ',', '') .'</span></b><!-- EOF MWST_VALUE --> EUR</td>';
									$contentExportPDF .= '<!-- BOF DELETE_AREA -->';
									$contentExportPDF .= '<td> </td>';
									$contentExportPDF .= '<!-- EOF DELETE_AREA -->';
									$contentExportPDF .= '</tr>';
								}
								// EOF MWST
								$valueTotal = $displayTotalSum + $valueProvision + $valueMwst;

								$contentExportPDF .= '<tr>';
								$contentExportPDF .= '<td>&nbsp;</td>';
								$contentExportPDF .= '<!-- BOF DOCUMENT_NUMBER_ORDER -->';
								$contentExportPDF .= '<td>&nbsp;</td>';
								$contentExportPDF .= '<!-- EOF DOCUMENT_NUMBER_ORDER -->';
								$contentExportPDF .= '<td colspan="5">&nbsp;</td>';
								$contentExportPDF .= '<td colspan="2" id="sumline">&nbsp;</td>';
								$contentExportPDF .= '<td id="sumline"> </td>';
								$contentExportPDF .= '<!-- BOF DELETE_AREA -->';
								$contentExportPDF .= '<td> </td>';
								$contentExportPDF .= '<!-- EOF DELETE_AREA -->';
								$contentExportPDF .= '</tr>';

								$contentExportPDF .= '<tr>';
								$contentExportPDF .= '<td>&nbsp;</td>';
								$contentExportPDF .= '<!-- BOF DOCUMENT_NUMBER_ORDER -->';
								$contentExportPDF .= '<td>&nbsp;</td>';
								$contentExportPDF .= '<!-- EOF DOCUMENT_NUMBER_ORDER -->';
								$contentExportPDF .= '<td colspan="5">&nbsp;</td>';
								$contentExportPDF .= '<td colspan="2"><b>Gesamt:</b></td>';
								$contentExportPDF .= '<td style="white-space:nowrap;text-align:right;"><!-- BOF TOTAL_VALUE --><b><span id="displayTotal">'.number_format($valueTotal, 2, ',', '') .'</span></b><!-- EOF TOTAL_VALUE --> EUR</td>';
								$contentExportPDF .= '<!-- BOF DELETE_AREA -->';
								$contentExportPDF .= '<td> </td>';
								$contentExportPDF .= '<!-- EOF DELETE_AREA -->';
								$contentExportPDF .= '</tr>';

								$contentExportPDF .= '</tbody>';

								$contentExportPDF .= '<tfoot>';
								$contentExportPDF .= '<tr>';
								$contentExportPDF .= '<td>&nbsp;</td>';
								$contentExportPDF .= '<!-- BOF DOCUMENT_NUMBER_ORDER -->';
								$contentExportPDF .= '<td>&nbsp;</td>';
								$contentExportPDF .= '<!-- EOF DOCUMENT_NUMBER_ORDER -->';
								$contentExportPDF .= '<td colspan="5">&nbsp;</td>';
								$contentExportPDF .= '<td colspan="2" id="sumline">&nbsp;</td>';
								$contentExportPDF .= '<td id="sumline"> </td>';
								$contentExportPDF .= '<!-- BOF DELETE_AREA -->';
								$contentExportPDF .= '<td> </td>';
								$contentExportPDF .= '<!-- EOF DELETE_AREA -->';
								$contentExportPDF .= '</tr>';
								$contentExportPDF .= '</tfoot>';

								$contentExportPDF .= '</table>';

								if($_POST["createDocumentProvision"] == '') {

									echo $contentExportPDF;

									echo '<input type="hidden" name="thisCreatedPdfName" value="' . $thisCreatedPdfName . '" />';
									echo '<input type="hidden" name="exportOrdersVertreterID" value="' . $_POST["exportOrdersVertreterID"] . '" />';
									echo '<input type="hidden" name="exportOrdersDateFrom" value="' . $_POST["exportOrdersDateFrom"] . '" />';
									echo '<input type="hidden" name="exportOrdersDateTo" value="' . $_POST["exportOrdersDateTo"] . '" />';

									if($arrGetUserRights["editProvisionDatas"]) {
										echo '<div class="actionButtonsArea">';

										if($arrGetUserRights["adminArea"]){
											echo '<input type="button" class="inputButton3" name="buttonAddFormProvisionRow" id="buttonAddFormProvisionRow" value="Zeile hinzuf&uuml;gen" /> ';#
										}
										echo '<hr />';
										echo '<input type="submit" class="inputButton3 inputButtonOrange" name="submitFormProvision" value="Daten NUR speichern" /> ';
										echo '<input type="submit" class="inputButton3 inputButtonGreen" name="createDocumentProvision" value="Speichern und PDF-Datei erzeugen" />';
										echo '</div>';
									}
								}
								else {
									// BOF CREATE PDF
										$pdfContentPage = $contentExportPDF;
										$contentPDF = removeUnnecessaryChars($pdfContentPage);
										#$contentPDF = preg_replace('/<input(.*) value="(.*)"(.*)\/>/ismU', '$2', $contentPDF);
										$contentPDF = preg_replace('/<input(.*)\/>/ismU', '', $contentPDF);

										$contentPDF = preg_replace('/<input(.*)\/>/ismU', '', $contentPDF);

										foreach($_POST["arrProvisionValues"] as $thisKey => $thisValue) {
											$valueSum += str_replace(",", ".", $thisValue["salesmanProvisionValue"]);

											$contentPDF = preg_replace('/<!-- BOF VK_PRICE_'.$thisKey.' -->(.*)<!-- EOF VK_PRICE_'.$thisKey.' -->/ismU', $thisValue["salesmanProvisionProductVkprice"], $contentPDF);
											$contentPDF = preg_replace('/<!-- BOF PROVISION_PRICE_'.$thisKey.' -->(.*)<!-- EOF PROVISION_PRICE_'.$thisKey.' -->/ismU', $thisValue["salesmanProvisionValue"], $contentPDF);
											$contentPDF = preg_replace('/<!-- BOF QUANTITY_VALUE_'.$thisKey.' -->(.*)<!-- EOF QUANTITY_VALUE_'.$thisKey.' -->/ismU', $thisValue["orderDocumentDetailProductQuantity"], $contentPDF);
										}

										$contentPDF = preg_replace('/<!-- BOF DOCUMENT_NUMBER_ORDER -->(.*)<!-- EOF DOCUMENT_NUMBER_ORDER -->/ismU', '', $contentPDF);
										$contentPDF = preg_replace('/<!-- BOF DELETE_AREA -->(.*)<!-- EOF DELETE_AREA -->/ismU', '', $contentPDF);
										$contentPDF = preg_replace('/<!-- BOF DEACTIVATED_ROW -->(.*)<!-- EOF DEACTIVATED_ROW -->/ismU', '', $contentPDF);
										$contentPDF = preg_replace('/<!-- BOF EMPTY_ROW -->(.*)<!-- EOF EMPTY_ROW -->/ismU', '', $contentPDF);

										$contentPDF = preg_replace('/ style="background-color:#FEFFAF;"/', '', $contentPDF);
										$contentPDF = preg_replace('/background-color:#FEFFAF;/', '', $contentPDF);
										$contentPDF = preg_replace('/<tbody>/ismU', '', $contentPDF);
										$contentPDF = preg_replace('/<\/tbody>/ismU', '', $contentPDF);
										$contentPDF = preg_replace('/<thead>/ismU', '', $contentPDF);
										$contentPDF = preg_replace('/<\/thead>/ismU', '', $contentPDF);
										$contentPDF = preg_replace('/ row[0-9]/ismU', '', $contentPDF);

										# BOF REPAIR SUM VALUES IN PDF
										$valueMwst = 0;
										$valueProvision = 0;

										if($arrSalesmanDatas["salesmenUseMwst"] == '1'){
											$valueMwst = $valueSum * MWST/100;
											$pattern = '<!-- BOF SUM_VALUE --><b><span id="displayTotalSum">(.*)<\/span><\/b><!-- EOF SUM_VALUE -->';
											$replace = '<!-- BOF SUM_VALUE --><b><span id="displayTotalSum">' . number_format($valueSum, 2, ',', '') . '</span></b><!-- EOF SUM_VALUE -->';
											$contentPDF = preg_replace('/' . $pattern . '/', $replace, $contentPDF);

											$pattern = '<!-- BOF MWST_VALUE --><b><span id="displayMWST">(.*)<\/span><\/b><!-- EOF MWST_VALUE -->';
											$replace = '<!-- BOF MWST_VALUE --><b><span id="displayMWST">' . number_format($valueMwst, 2, ',', '') . '</span></b><!-- EOF MWST_VALUE -->';
											$contentPDF = preg_replace('/' . $pattern . '/', $replace, $contentPDF);
										}
										if($arrSalesmanDatas["salesmenProvision"] > 0){
											$valueProvision = $valueSum * ($arrSalesmanDatas["salesmenProvision"]/100);
											$pattern = '<!-- BOF PROVISION_VALUE --><b><span id="displayProvision">(.*)<\/span><\/b><!-- EOF PROVISION_VALUE -->';
											$replace = '<!-- BOF PROVISION_VALUE --><b><span id="displayProvision">' . number_format($valueProvision, 2, ',', '') . '</span></b><!-- EOF PROVISION_VALUE -->';
											$contentPDF = preg_replace('/' . $pattern . '/', $replace, $contentPDF);
										}

										$valueTotal = $valueSum + $valueProvision + $valueMwst;
										$pattern = '<!-- BOF TOTAL_VALUE --><b><span id="displayTotal">(.*)<\/span><\/b><!-- EOF TOTAL_VALUE -->';
										$replace = '<!-- BOF TOTAL_VALUE --><b><span id="displayTotal">' . number_format($valueTotal, 2, ',', '') . '</span></b><!-- EOF TOTAL_VALUE -->';
										$contentPDF = preg_replace('/' . $pattern . '/', $replace, $contentPDF);
										# EOF REPAIR SUM VALUES IN PDF

										// BOF SET NEW POS INDEX
										$contentPDF = preg_replace_callback(
											"/<!-- BOF DOCUMENT_POS_INDEX -->.*<!-- EOF DOCUMENT_POS_INDEX -->/ismU",
											function($matches) use (&$matchIndex){
												$matchIndex++;
												$posIndex = $matchIndex;
												return $posIndex++;
											},
											$contentPDF
										);
										// EOF SET NEW POS INDEX

										$loadPdfContent = "";
										$loadContentTemplate = implode('', file(DIRECTORY_PDF_TEMPLATES . '/template_BASIC.html'));
										require_once(DIRECTORY_PDF_TEMPLATES . 'template_TEXT_DATAS.inc.php');

										$loadPdfContent = $loadContentTemplate;
										$loadPdfContent = preg_replace("/{###CARD_DATAS###}/", $contentPDF, $loadPdfContent);

										// STYLES
										$loadTemplateStyles = implode('', file(DIRECTORY_PDF_TEMPLATES . 'template_STYLES.html'));
										$loadPdfContent = preg_replace("/{###STYLES###}/", $loadTemplateStyles, $loadPdfContent);

										$loadPdfContent = preg_replace("/{###DOCUMENT_DATAS###}/", $arrDocumentDatas["PR"], $loadPdfContent);

										$loadPdfContent = preg_replace('/<a href="(.*)">(.*)<\/a>/ismU', '$2', $loadPdfContent);
										// DOKUMENT-TYP PROVISION anlegen

										$loadedAdressDatas = array(
											"ADRESS_COMPANY" => $arrSalesmanDatas["customersFirmenname"],
											"ADRESS_MANAGER" => "",
											"ADRESS_CONTACT" => $arrSalesmanDatas["customersFirmenInhaberVorname"] . " " . $arrSalesmanDatas["customersFirmenInhaberNachname"],
											"ADRESS_STREET" => $arrSalesmanDatas["customersCompanyStrasse"].' '.$arrSalesmanDatas["customersCompanyHausnummer"],
											"ADRESS_CITY" => $arrSalesmanDatas["customersCompanyPLZ"].' '.$arrSalesmanDatas["customersCompanyOrt"],
											"ADRESS_COUNTRY" => $arrCountryTypeDatas[$arrSalesmanDatas["customersCompanyCountry"]]["countries_name"],
											"ADRESS_MAIL" => $arrSalesmanDatas["customersMail1"],
										);

										$loadedDocumentDatas = array(
											"DOCUMENT_NUMBER_RE" => "",
											"CUSTOMERS_SALESMAN" => "",
											"CUSTOMER_NUMBER" => $arrSalesmanDatas["customersKundennummer"],
											"CUSTOMERS_TAX_ACCOUNT" => $arrSalesmanDatas["customersTaxAccountID"],
											"CUSTOMER_PHONE" => "",
											"CUSTOMER_MAIL" => "",
											"CUSTOMERS_ORDER_NUMBER" => "",

											"SHIPPING_COSTS" => "",
											"SHIPPING_TYPE" => "",
											"SHIPPING_TYPE_NUMBER" => "",
											"PACKAGING_COSTS" => "",
											"CARD_CASH_ON_DELIVERY_VALUE" => "",
											"SHIPPING_DATE" => "",
											"BINDING_DATE" => "",

											"COMPANY_HEADER_IMAGE" => DIRECTORY_PDF_IMAGES . "briefkopf.jpg",
											"COMPANY_NAME_LONG" => COMPANY_NAME_LONG,
											"COMPANY_PHONE" => COMPANY_PHONE,
											"COMPANY_FAX" => COMPANY_FAX,
											"COMPANY_INTERNET" => COMPANY_INTERNET,
											"COMPANY_MANAGER" => COMPANY_MANAGER,
											"COMPANY_MAIL" => COMPANY_MAIL,
											"COMPANY_STREET_NAME" => COMPANY_STREET_NAME,
											"COMPANY_STREET_NUMBER" => COMPANY_STREET_NUMBER,
											"COMPANY_ZIPCODE" => COMPANY_ZIPCODE,
											"COMPANY_CITY" => COMPANY_CITY,
											"COMPANY_TAX_NUMBER" => COMPANY_TAX_NUMBER,
											"COMPANY_UID_NUMBER" => COMPANY_UID_NUMBER,
											"COMPANY_VENUE" => COMPANY_VENUE,

											"BANK_ACCOUNT_TYPE" => $arrBankAccountTypeDatas['KSK']["bankAccountTypesShortName"],
											"BANK_ACCOUNT_CODE" => $arrBankAccountTypeDatas['KSK']["bankAccountTypesBankCode"],
											"BANK_ACCOUNT_NAME" => $arrBankAccountTypeDatas['KSK']["bankAccountTypesName"],
											"BANK_ACCOUNT_NUMBER" => $arrBankAccountTypeDatas['KSK']["bankAccountTypesAccountNumber"],
											"BANK_ACCOUNT_IBAN" => $arrBankAccountTypeDatas['KSK']["bankAccountTypesIBAN"],
											"BANK_ACCOUNT_BIC" => $arrBankAccountTypeDatas['KSK']["bankAccountTypesBIC"],

											"INTRO_TEXT" => $arrContentDatas["PR"]["INTRO_TEXT"],
											"OUTRO_TEXT" => $arrContentDatas["PR"]["OUTRO_TEXT"],
											"EDITOR_TEXT" => $arrContentDatas["PR"]["EDITOR_TEXT"],
											"REGARDS_TEXT" => $arrContentDatas["PR"]["REGARDS_TEXT"],
											"SALUTATION_TEXT" => $arrContentDatas["PR"]["SALUTATION_TEXT"],
											"PAYMENT_CONDITIONS" => $arrContentDatas["PR"]["PAYMENT_CONDITIONS"],
											"PAYMENT_CONDITION" => $arrPaymentConditionDatas["PR"]["paymentConditionsName"],
											"PAYMENT_TYPE" => "",
											"FOOTER_NOTICE" => $arrContentDatas["PR"]["FOOTER_NOTICE"],
										);

										$loadedDocumentDatas["ADDRESS_INVOICE"] = "";
										$loadedDocumentDatas["ADDRESS_DELIVERY"] = "";
										$loadedDocumentDatas["INVOICE_SKONTO"] = "";

										$loadedDocumentDatas["WARNING_INVOICE_IS_PAID"] = "";

										$loadedDocumentDatas["CREATION_DATE"] = formatDate(date('Y-m-d'), 'display');

										$loadedDocumentDatas["DOCUMENT_NUMBER"] = $thisCreatedDocumentNumber;

										#$thisSubjectMonth = (date('m') - 1);
										#if($thisSubjectMonth == 0){ $thisSubjectMonth = 12; }

										$thisSubjectMonth = date('m', strtotime(formatDate($_REQUEST["exportOrdersDateTo"], "store")));

										#$loadSubjectText = 'Interne Provisionsgutschrift - Nr.: ' . '{###DOCUMENT_NUMBER###}';
										#$loadSubjectText = 'Interne Provisionsgutschrift: ' . getTimeNames($thisSubjectMonth, 'month', $mode='') . ' ' . date('Y');
										$loadSubjectText = 'Interne Provisionsgutschrift: ' . getTimeNames($thisSubjectMonth, 'month', $mode='') . ' ' . date('Y', strtotime(formatDate($_REQUEST["exportOrdersDateTo"], "store")) );

										$loadSubject = ''.$loadSubjectText. '';

										$loadPdfContent = preg_replace("/{###SUBJECT###}/", $loadSubject, $loadPdfContent);

										if(!empty($loadedAdressDatas)) {
											foreach($loadedAdressDatas as $thisKey => $thisValue) {
												if($thisValue != "") {
													$loadPdfContent = preg_replace("/{###".$thisKey."###}/", $thisValue . "<br />", $loadPdfContent);
												}
												else {
													$loadPdfContent = preg_replace("/{###".$thisKey."###}/", "", $loadPdfContent);
												}
											}
										}

										if(!empty($loadedDocumentDatas)) {
											// if(constant('BANK_ACCOUNT_' . $_POST["selectBankAccount"]. '_SHOW_WARNING')){
											$loadPdfContent = preg_replace("/{###BANK_ACCOUNT_WARNING###}/", "", $loadPdfContent);
											foreach($loadedDocumentDatas as $thisKey => $thisValue) {
												$loadPdfContent = preg_replace("/{###".$thisKey."###}/", $thisValue, $loadPdfContent);
												if(trim($thisValue) == '') {
													$loadPdfContent = preg_replace("/<!-- BOF_".$thisKey." -->(.*)<!-- EOF_".$thisKey." -->/", '', $loadPdfContent);
												}
											}
										}

										$loadPdfContent = preg_replace('/class="displayOrders"/', '', $loadPdfContent);
										$loadPdfContent = removeUnnecessaryChars($loadPdfContent);

										#$loadPdfContent = preg_replace('/<\/page>/', '</page><page pageset="old">NEUE SEITE</page>', $loadPdfContent);

										$loadPdfContent = preg_replace('/pageset="old"/', '', $loadPdfContent);

										$pdfContentPage = $loadPdfContent;
										#dd('pdfContentPage');
										require_once(DIRECTORY_HTML2PDF . 'html2pdf.class.php');

										try {
											$html2pdf = new HTML2PDF('P', 'A4', 'de', true, 'UTF-8', array(20, 10, 10, 10));

											if(file_exists(DIRECTORY_CREATED_DOCUMENTS_SALESMEN . $_REQUEST["thisCreatedPdfName"])) {
												// chmod(DIRECTORY_CREATED_DOCUMENTS_SALESMEN . $_REQUEST["thisCreatedPdfName"] , "0777");
												copy(DIRECTORY_CREATED_DOCUMENTS_SALESMEN . $_REQUEST["thisCreatedPdfName"], '_sich_' . date('YmdHis') . '_' . DIRECTORY_CREATED_DOCUMENTS_SALESMEN . $_REQUEST["thisCreatedPdfName"]);
												unlink(DIRECTORY_CREATED_DOCUMENTS_SALESMEN . $_REQUEST["thisCreatedPdfName"]);
											}
											clearstatcache();

											// BOF TEST PAGE SPLIT
												if($userDatas["usersLogin"] != 'xxxthorsten'){
													$pdfContentPage = preg_replace('/#pdfContentArea/', '', $pdfContentPage);
													$pdfContentPage = preg_replace('/<div id="pdfContentArea">/', '', $pdfContentPage);
													$pdfContentPage = preg_replace('/<\/div><page_header>/ismU', '<page_header>', $pdfContentPage);
												}
											// BOF TEST PAGE SPLIT

											ob_start();
											echo $pdfContentPage;
											$contentPDF = ob_get_clean();
											#$_GET['vuehtml'] = 1;
											$html2pdf->writeHTML($contentPDF, isset($_GET['vuehtml']));
											$html2pdf->Output($_REQUEST["thisCreatedPdfName"], 'F', DIRECTORY_CREATED_DOCUMENTS_SALESMEN);

											if(file_exists(DIRECTORY_CREATED_DOCUMENTS_SALESMEN . $_REQUEST["thisCreatedPdfName"])) {
												if(file_exists($_REQUEST["thisCreatedPdfName"])) {
													# copy($_REQUEST["thisCreatedPdfName"], '_sich_' . date('YmdHis') . '_' . $_REQUEST["thisCreatedPdfName"]);
													# unlink($_REQUEST["thisCreatedPdfName"]);
												}
											}
											clearstatcache();
										}
										catch(HTML2PDF_exception $e) {
											echo $e;
											exit;
										}
										if(file_exists(DIRECTORY_CREATED_DOCUMENTS_SALESMEN . $_REQUEST["thisCreatedPdfName"])) {
											$sql = "DELETE
														FROM `" . TABLE_CREATED_DOCUMENTS . "`
														WHERE 1
															AND `createdDocumentsTitle` = '" . $thisCreatedDocumentNumber . "'
												";
											$rs = $dbConnection->db_query($sql);

											$sql = "REPLACE INTO `" . TABLE_CREATED_DOCUMENTS . "` (
															`createdDocumentsID`,
															`createdDocumentsType`,
															`createdDocumentsNumber`,
															`createdDocumentsCustomerNumber`,
															`createdDocumentsTitle`,
															`createdDocumentsFilename`,
															`createdDocumentsContent`,
															`createdDocumentsUserID`,
															`createdDocumentsTimeCreated`,
															`createdDocumentsOrderIDs`
														)
														VALUES (
															'%',
															'PR',
															'" . $thisCreatedDocumentNumber . "',
															'" . $arrSalesmanDatas["customersKundennummer"] . "',
															'" . $thisCreatedDocumentNumber . "',
															'" . DIRECTORY_CREATED_DOCUMENTS_SALESMEN . $_REQUEST["thisCreatedPdfName"] . "',
															'',
															'" . $_SESSION["usersID"] . "',
															NOW(),
															'" . $arrDocumentContentDocumentNumbers . "'
														)
												";

											$rs = $dbConnection->db_query($sql);

											if($rs) {
												#$successMessage .= ' Die Provisionsdaten wurden gespeichert.' .'<br />';
											}
											else {
												#$errorMessage .= ' Die Provisionsdaten konnten nicht gespeichert werden.' .'<br />';
											}

											$successMessage .= 'Die PDF-Datei &quot;' . $_REQUEST["thisCreatedPdfName"] . '&quot wurde generiert. ' . '<br />';
											$showPdfDownloadLink = true;
										}
										else {
											$errorMessage .= 'Die PDF-Datei &quot;' . $_REQUEST["thisCreatedPdfName"] . '&quot konnte nicht generiert werden. ' . '<br />';
											$showPdfDownloadLink = false;
										}

										if($showPdfDownloadLink) {
											// echo '<p class="dataTableContent"><b>DOWNLOAD-LINK: </b><a href="'.DIRECTORY_CREATED_DOCUMENTS_SALESMEN . $thisCreatedPdfName.'" target="_blank">'.$thisCreatedPdfName.'</a></p>';
											echo '<p class="dataTableContent"><b>DOWNLOAD-LINK: </b><img src="layout/icons/iconPDF.gif" height="16" width="16" alt="" /> <a href="?downloadFile=' . $thisCreatedPdfName.'">'.utf8_decode($thisCreatedPdfName).'</a></p>';
											#echo '<p class="dataTableContent"><b>DOWNLOAD-LINK: </b><img src="layout/icons/iconEXCEL.gif" height="16" width="16" alt="" /> <a href="?downloadFile=' . $_POST["thisCreatedCsvName"].'">'.utf8_decode($_POST["thisCreatedCsvName"]).'</a></p>';
										}
										echo '<hr />';
										// $_POST["editID"] = "";

										clearstatcache();

									// EOF CREATE PDF
								}

								echo '</form>';

								echo '<br />';

								// BOF STORE DATAS
								if($_POST["createDocumentProvision"] != '' && $_POST["exportOrdersVertreterID"] > 0 && !empty($_POST["arrProvisionValues"])) {
									storeDocumentDatas();
								}
								/*
								if(($_POST["submitFormProvision"] != '' || $_POST["createDocumentProvision"] != '') && $_POST["exportOrdersVertreterID"] > 0 && !empty($_POST["arrProvisionValues"])) {

									$thisCreatedDocumentNumber = "PR-".date('ym') . '_' . $arrSalesmanDatas["customersKundennummer"];

									$arrTemp = array();
									$deleteWhere = '';
									foreach($_POST["arrProvisionValues"] as $thisKey => $thisValue) {
										$arrTemp[] = $thisValue["orderDocumentsNumber"];
									}
									if(!empty($arrTemp)){
										$arrTemp = array_unique($arrTemp);
										$arrDocumentContentDocumentNumbers = implode(";",$arrTemp);
										foreach($arrTemp as $thisKey => $thisValue) {
											$deleteWhere .= " OR `salesmenProvisionsContentDocumentNumber` LIKE '%" . $thisValue . "%'";
										}
									}

									$sql = "DELETE
										`" . TABLE_SALESMEN_PROVISIONS . "`,
										`" . TABLE_SALESMEN_PROVISIONS_DETAILS . "`

										FROM `" . TABLE_SALESMEN_PROVISIONS . "`

										LEFT JOIN `" . TABLE_SALESMEN_PROVISIONS_DETAILS . "`
										ON(`" . TABLE_SALESMEN_PROVISIONS . "`.`salesmenProvisionsID` = `" . TABLE_SALESMEN_PROVISIONS_DETAILS . "`.`salesmenProvisionsDetailsProvisionsID`)

										WHERE
											`" . TABLE_SALESMEN_PROVISIONS . "`.`salesmenProvisionsDocumentNumber` = '" . $thisCreatedDocumentNumber . "'
											" . $deleteWhere . "
									";

									$rs = $dbConnection->db_query($sql);

									$sql = "INSERT INTO `" . TABLE_SALESMEN_PROVISIONS . "` (

												`salesmenProvisionsID`,
												`salesmenProvisionsDocumentNumber`,
												`salesmenProvisionsSalesmanID`,
												`salesmenProvisionsDocumentDate`,
												`salesmenProvisionsSum`,
												`salesmenProvisionsMwst`,
												`salesmenProvisionsMwstValue`,
												`salesmenProvisionsTotal`,
												`salesmenProvisionsStatus`,
												`salesmenProvisionsDocumentPath`,
												`salesmenProvisionsContentDocumentNumber`
											)
											VALUES (
												'%',
												'" . $thisCreatedDocumentNumber . "',
												'" . $arrSalesmanDatas["customersID"] . "',
												'" . $loadedDocumentDatas["CREATION_DATE"] . "',
												'" . $displayTotalSum . "',
												'" . MWST . "',
												'" . $valueMwst . "',
												'" . $valueTotal . "',
												'1',
												'" . DIRECTORY_CREATED_DOCUMENTS_SALESMEN . $_REQUEST["thisCreatedPdfName"] . "',
												'" . $arrDocumentContentDocumentNumbers . "'
											)
										";
									$rs = $dbConnection->db_query($sql);

									if($rs) {
										$successMessage .= ' Die Dokumentdaten wurden gespeichert.' .'<br />';
										$thisInsertID = mysqli_insert_id();
									}
									else {
										$errorMessage .= ' Die Dokumentdaten konnten nicht gespeichert werden.' .'<br />';
									}

									$sql = "INSERT IGNORE INTO `" . TABLE_SALESMEN_PROVISIONS_DETAILS . "` (
												`salesmenProvisionsDetailsID`,
												`salesmenProvisionsDetailsProvisionsID`,
												`salesmenProvisionsDetailsDocumentsDetailID`,
												`salesmenProvisionsDetailsDocumentNumber`,
												`salesmenProvisionsDetailsProductOrderDate`,
												`salesmenProvisionsDetailsProductAmount`,
												`salesmenProvisionsDetailsProductNumber`,
												`salesmenProvisionsDetailsProductName`,
												`salesmenProvisionsDetailsProductRecipientCustomerNumber`,
												`salesmenProvisionsDetailsProductRecipientName`,
												`salesmenProvisionsDetailsProductRecipientZipcode`,
												`salesmenProvisionsDetailsProductRecipientCity`,
												`salesmenProvisionsDetailsProductVkPreis`,
												`salesmenProvisionsDetailsProductProvisionPrice`,
												`salesmenProvisionsDetailsDisabled`,
												`salesmenProvisionsDetailsType`
											)
											VALUES
										";



									foreach($_POST["arrProvisionValues"] as $thisKey => $thisValue) {

										$arrSQL[] = " (
													'%',
													'" . $thisInsertID . "',
													'" . $thisValue["orderDocumentDetailID"] . "',
													'" . $thisValue["orderDocumentsNumber"] . "',
													'" . formatDate($thisValue["orderDocumentsOrderDate"], 'store') . "',
													'" . $thisValue["orderDocumentDetailProductQuantity"] . "',
													'" . $thisValue["orderDocumentDetailProductNumber"] . "',
													'" . addslashes($thisValue["orderDocumentDetailProductName"]) . "',
													'" . $thisValue["orderDocumentsCustomerNumber"] . "',
													'" . addslashes($thisValue["orderDocumentsAddressCompany"]) . "',
													'" . $thisValue["orderDocumentsAddressZipcode"] . "',
													'" . $thisValue["orderDocumentsAddressCity"] . "',
													'" . str_replace(',', '.', $thisValue["salesmanProvisionProductVkprice"]) . "',
													'" . str_replace(',', '.', $thisValue["salesmanProvisionValue"]) . "',

													'" . $thisValue["salesmenProvisionsDetailsDisabled"] . "',
													'" . $thisValue["salesmenProvisionsDetailsType"] . "'
												)
											";
									}

									if(!empty($arrSQL)){
										$sql = $sql . implode(", ", $arrSQL);
										$rs = $dbConnection->db_query($sql);

										if($rs) {
											$successMessage .= ' Die Provisionsdaten wurden gespeichert.' .'<br />';
										}
										else {
											$errorMessage .= ' Die Provisionsdaten konnten nicht gespeichert werden.' .'<br />';
										}
									}

									#$_POST["exportOrdersVertreterID"] = '';
									// BOF DELETE TEMP HTML FILE
									if(file_exists(DIRECTORY_CREATED_DOCUMENTS_SALESMEN . $_POST["tempHtmlFileName"])) {
										unlink(DIRECTORY_CREATED_DOCUMENTS_SALESMEN . $_POST["tempHtmlFileName"]);
									}
									clearstatcache();
									// EOF DELETE TEMP HTML FILE
								}
								*/
								// BOF STORE DATAS
							}
							else {
								$errorMessage .= ' F&uuml;r diesen Vertreter liegen keine offenen Provisionen vor.';
							}
						}
					?>
					<?php
						if($warningMessage != "") { echo '<p class="warningArea">'.$warningMessage.'</p>'; }
						if($errorMessage != "") { echo '<p class="errorArea">'.$errorMessage.'</p>'; }
						if($successMessage != "") { echo '<p class="successArea">'.$successMessage.'</p>'; }
						if($infoMessage != "") { echo '<p class="infoArea">'.$infoMessage.'</p>'; }
					?>
				</div>

			</div>
		</div>
		<div class="clear"></div>
	</div>
</div>
<script language="javascript" type="text/javascript">
	$(document).ready(function() {
		$(function() {
			$.datepicker.setDefaults($.datepicker.regional[""]);
			$('#exportOrdersDateFrom').datepicker($.datepicker.regional["de"]);
			$('#exportOrdersDateFrom').datepicker("option", "maxDate", "0" );
			$('#exportOrdersDateTo').datepicker($.datepicker.regional["de"]);
			$('#exportOrdersDateTo').datepicker("option", "maxDate", "0" );
		});
		colorRowMouseOver('#displayCardDatas tbody tr');

		function addFormProvisionRow(path){
			var countRow = $('.displayCardDatasItemRows').length;
			var DetailID = 0;
			var documentPath = '';
			var documentNumber = '';
			var documentType = '';
			var documentFileType = 'PDF';
			var pdfDataRow = '';

			pdfDataRow = '';
			pdfDataRow += '<tr class="displayCardDatasItemRows">';
			pdfDataRow += '<td style="text-align:right;">';
			pdfDataRow += '<b>' + (countRow + 1) + '.</b>';
			pdfDataRow += '<input type="hidden" name="arrProvisionValues['+countRow+'][orderDocumentDetailID]" value="' + DetailID + '" />';
			// pdfDataRow += '<input type="text" class="inputField_200" name="arrProvisionValues['+countRow+'][salesmenProvisionsDetailsDisabled]" value="0" />';
			pdfDataRow += '<input type="hidden" class="salesmenProvisionsDetailsType" name="arrProvisionValues['+countRow+'][salesmenProvisionsDetailsType]" value="additional" />';
			pdfDataRow += '</td>';
			pdfDataRow += '<!-- BOF DOCUMENT_NUMBER_ORDER -->';
			pdfDataRow += '<td style="background-color:#FEFFAF; white-space:nowrap;">';
			pdfDataRow += '<input type="text" class="inputField_70" name="arrProvisionValues['+countRow+'][orderDocumentsNumber]" value="' + documentNumber + '" />';
			// pdfDataRow += '<a href="' + path + '?downloadFile=' + documentPath + '&thisDocumentType=' + documentType + '">' + '<img src="layout/icons/icon' + documentFileType + '.gif" width="16" height="16" title="Dokument &quot;' + documentNumber + '&quot; herunterladen" alt="Download" /></a>';
			pdfDataRow += '</td>';
			pdfDataRow += '<!-- EOF DOCUMENT_NUMBER_ORDER -->';
			pdfDataRow += '<td>';
			pdfDataRow += '<input type="text" class="inputField_70 setProvisionDate" name="arrProvisionValues['+countRow+'][orderDocumentsOrderDate]" value="" readonly="readonly" />';
			pdfDataRow += '</td>';
			pdfDataRow += '<td style="background-color:#FEFFAF;">';
			pdfDataRow += '<input type="text" class="inputField_50 setCustomerNumber" name="arrProvisionValues['+countRow+'][orderDocumentsCustomerNumber]" value="" />';
			pdfDataRow += '</td>';
			pdfDataRow += '<td>';
			pdfDataRow += '<input type="text" class="inputField_100 setCustomerCompany" name="arrProvisionValues['+countRow+'][orderDocumentsAddressCompany]" value="" />';
			pdfDataRow += '</td>';
			pdfDataRow += '<td style="white-space:nowrap;">';
			pdfDataRow += '<input type="text" class="inputField_40 setCustomerZipcode" name="arrProvisionValues['+countRow+'][orderDocumentsAddressZipcode]" value="" />';
			pdfDataRow += ' ';
			pdfDataRow += '<input type="text" class="inputField_70 setCustomerCity" name="arrProvisionValues['+countRow+'][orderDocumentsAddressCity]" value="" />';
			pdfDataRow += '</td>';
			pdfDataRow += '<td style="background-color:#FEFFAF;">';
			pdfDataRow += '<input type="text" class="inputField_120" name="arrProvisionValues['+countRow+'][orderDocumentDetailProductName]" value="" />';
			pdfDataRow += '</td>';
			pdfDataRow += '<td style="text-align:right;" class="cellQuantity">';
			pdfDataRow += '<input type="text" class="inputField_20 setProductQuantity" name="arrProvisionValues['+countRow+'][orderDocumentDetailProductQuantity]" value="1" />';
			pdfDataRow += '</td>';
			pdfDataRow += '<td style="text-align:right; white-space:nowrap;" class="cellVkprice">';
			pdfDataRow += '<!-- BOF VK_PRICE_'+countRow+' -->';
			pdfDataRow += '<input type="text" class="inputField_50" class="inputField_40" style="text-align:right;" name="arrProvisionValues['+countRow+'][salesmanProvisionProductVkprice]" value="0" /> EUR';
			pdfDataRow += '<!-- EOF VK_PRICE_'+countRow+' -->';
			pdfDataRow += '</td>';
			pdfDataRow += '<td style="text-align:right; white-space:nowrap;background-color:#FEFFAF;" class="cellProvision">';
			pdfDataRow += '<!-- BOF PROVISION_PRICE_'+countRow+' -->';
			pdfDataRow += '<input type="text" class="inputField_50" class="inputField_50" style="text-align:right;" name="arrProvisionValues['+countRow+'][salesmanProvisionValue]" value="0" />';
			pdfDataRow += '<!-- EOF PROVISION_PRICE_'+countRow+' -->';
			pdfDataRow += ' EUR';
			pdfDataRow += '</td>';
			pdfDataRow += '<!-- BOF DELETE_AREA -->';
			pdfDataRow += '<td title="Eintrag deaktivieren">';
			pdfDataRow += '<input type="checkbox" class="deactivateProvision" name="arrProvisionValues['+countRow+'][salesmenProvisionsDetailsDisabled]" value="1" />';
			pdfDataRow += '</td>';
			pdfDataRow += '<!-- EOF DELETE_AREA -->';
			pdfDataRow += '</tr>';

			pdfDataRow += '<tr class="emptyRow">';
			pdfDataRow += '<td></td>';
			pdfDataRow += '<!-- BOF DOCUMENT_NUMBER_ORDER -->';
			pdfDataRow += '<td></td>';
			pdfDataRow += '<!-- EOF DOCUMENT_NUMBER_ORDER -->';
			pdfDataRow += '<td colspan="8"></td>';
			pdfDataRow += '<!-- BOF DELETE_AREA -->';
			pdfDataRow += '<td></td>';
			pdfDataRow += '<!-- EOF DELETE_AREA -->';
			pdfDataRow += '</tr>';
			//$('#displayCardDatas').append(pdfDataRow);
			$('.emptyRow').last().after(pdfDataRow);
			$('.setProvisionDate').datepicker($.datepicker.regional["de"]);
			$('.setCustomerNumber').keyup(function () {
				loadSuggestions('searchCustomerNumberDatas', [{'triggerElement': $(this), 'fieldNumber': '.setCustomerNumber', 'fieldName': '.setCustomerCompany', 'fieldCity': '.setCustomerCity', 'fieldZipCode': '.setCustomerZipcode'}], 1);
			});
			$('.setCustomerZipcode').keyup(function () {
				loadSuggestions('searchPlzCity', [{'triggerElement': '.setCustomerZipcode', 'fieldCity': '.setCustomerCity', 'fieldZipCode': '.setCustomerZipcode'}], 1);
			});
		}

		function sumProvisions() {
			// alert($(this).val());
			var totalSum = 0;
			var tempSum = 0;
			var valueSign = 1;
			var mwst = <?php echo MWST; ?>;
			var mwstValue = 0;
			var total = 0;
			$('#displayCardDatas .cellProvision input').each(function(index, value){
				//alert(value + '|' + index + ': ' + $(value).val());

				if($(value).attr('disabled') == 'disabled'){

				}
				else {
					tempSum = $(value).val();
					tempSum = tempSum.replace(',', '.');
					tempSum = parseFloat(tempSum);
					tempSum = tempSum * 1;
					valueSign = 1;
					//alert(tempSum);
					if(tempSum < 0){
						////valueSign = -1;
						//tempSum = 0;
						//$(value).val(0);
						//$(this).css('font-weight', 'bold');
						//$(this).css('color', '#FF0000');
					}
					else {
						//$(this).css('font-weight', 'normal');
						//$(this).css('color', '#000000');
					}
					//alert(tempSum);
					tempSum = valueSign * tempSum;
					//alert(tempSum)
					totalSum += tempSum;
					//alert(valueSign + " * " + tempSum + " = " + totalSum);
				}
			});

			provisionValue = 0;
			<?php
				if($arrSalesmanDatas["salesmenProvision"] > 0){
			?>
			provisionValue = totalSum * <?php echo $arrSalesmanDatas["salesmenProvision"]; ?> / 100;
			<?php
				}
			?>


			<?php
				if($arrSalesmanDatas["salesmenUseMwst"] == '1'){
			?>
			mwstValue = (totalSum + provisionValue) * mwst/100;
			<?php
				}
				else {
			?>
			mwstValue = 0;
			<?php
				}
			?>

			total = (totalSum + provisionValue) + mwstValue;

			totalSum = parseFloat(totalSum);
			totalSum = totalSum.toFixed(2);
			totalSum = totalSum.replace('.', ',');

			provisionValue = parseFloat(provisionValue);
			provisionValue = provisionValue.toFixed(2);
			provisionValue = provisionValue.replace('.', ',');

			mwstValue = parseFloat(mwstValue);
			mwstValue = mwstValue.toFixed(2);
			mwstValue = mwstValue.replace('.', ',');

			total = parseFloat(total);
			total = total.toFixed(2);
			total = total.replace('.', ',');

			$('#displayTotalSum').text(totalSum);
			$('#displayProvision').text(provisionValue);
			$('#displayMWST').text(mwstValue);
			$('#displayTotal').text(total);
		}

		$('#displayCardDatas .cellVkprice input').live('keyup', function(){
			var thisProvisionType = $(this).parent().parent().find('.salesmenProvisionsDetailsType').val();
			var thisProductKategorieID = $(this).parent().parent().find('.productKategorieID').val();
			var thisSalesmenProvision = $(this).parent().parent().find('.salesmenProvision').val();
			// BOF GET EK-PRICE
			var thisEkPrice = 0;

			if(thisProductKategorieID.substring(0, 3) == '001') {
				thisEkPrice = <?php echo KZH_EK_PRICE; ?>;
				if(thisProductKategorieID == '001009' || thisProductKategorieID == '001010'){
					thisEkPrice = <?php echo KZH_EK_PRICE2; ?>;
				}
				if(thisSalesmenProvision > 0){
					thisEkPrice = 0;
				}
			}
			// EOF GET EK-PRICE

			var valueSign = 1;
			var thisVkPrice = $(this).val();
			if(thisVkPrice == ''){
				thisVkPrice = 0;
				$(this).val(thisVkPrice);
			}
			thisVkPrice = thisVkPrice.replace(',', '.');
			thisVkPrice = thisVkPrice * 1;
			if(thisVkPrice < 0) { valueSign = -1;}
			var thisProvision = 0;
			var thisQuantity = $(this).parent().parent().find('.cellQuantity input').val();

			if(thisProvisionType == 'standard'){
				thisProvision = valueSign * thisQuantity * (Math.abs(thisVkPrice) - thisEkPrice);
			}
			else {
				thisProvision = valueSign * thisQuantity * (Math.abs(thisVkPrice) - 0);
			}
			thisProvision = thisProvision.toFixed(2);
			$(this).parent().parent().find('.cellProvision input').val(thisProvision);
			sumProvisions();
		});
		$('#displayCardDatas .cellProvision input').live('keyup', function(){
			if($(this).val() == ''){
				thisProvision = 0;
				$(this).val(thisProvision);
			}
			sumProvisions();
		});

		$('#displayCardDatas .cellQuantity input').live('keyup', function(){
			var thisProvisionType = $(this).parent().parent().find('.salesmenProvisionsDetailsType').val();
			var valueSign = 1;
			var thisProvision = 0;
			var thisQuantity = $(this).parent().parent().find('.cellQuantity input').val();
			var thisVkPrice = $(this).parent().parent().find('.cellVkprice input').val();
			thisVkPrice = thisVkPrice.replace(',', '.');
			thisVkPrice = thisVkPrice * 1;
			if(thisVkPrice < 0) { valueSign = -1;}
			thisProvision = valueSign * thisQuantity * (Math.abs(thisVkPrice));
			thisProvision = thisProvision.toFixed(2);

			$(this).parent().parent().find('.cellProvision input').val(thisProvision);
			sumProvisions();
		});

		$('#buttonAddFormProvisionRow').click(function(){
			addFormProvisionRow('<?php echo $thisParsedUrl["path"]; ?>');
		});
		$('.deactivateProvision').live('click', function(){
			var thisOpacityValue = 1;
			var thisProvisionField = $(this).parent().parent().find('.cellProvision input');
			var thisProvisionValue = thisProvisionField.val();
			var thisTitle = "Eintrag deaktivieren";
			var thisTextDecoration = "";
			var thisBackgroundColor = "";
			thisProvisionField.removeAttr('disabled');
			if($(this).attr('checked')){
				thisOpacityValue = 0.5;
				thisProvisionField.attr('disabled', 'disabled');
				thisTitle = "Eintrag aktivieren";
				thisTextDecoration = "line-through";
				thisBackgroundColor = "#EFB3B3";
			}
			$(this).parent().parent().animate({
				 opacity: thisOpacityValue
			});
			$(this).attr('title', thisTitle);
			$(this).parent().parent('*').css('text-decoration', thisTextDecoration);
			$(this).parent().parent().find('input').css('text-decoration', thisTextDecoration);
			sumProvisions();
		});

		$(function() {
			$.datepicker.setDefaults($.datepicker.regional[""]);
		});
	});
</script>
<?php require_once('inc/footerHTML.inc.php'); ?>