<?php
	require_once('inc/requires.inc.php');

	if(!$arrGetUserRights["importDatas"]) {
		header('location: ' . PAGE_EXIT_LOCATION);
		exit;
	}

	// $documentRoot_onlineAcquisition	= $_SERVER["DOCUMENT_ROOT"];
	// /var/www/web23/html/burhan-ctr.de
	$documentRoot_onlineAcquisition	= "/var/www/web23/";
	$documentRoot_onlineAcquisition	= "";
	$dirLogFile_extern = "log/";
	$dirLogFilesOld_extern = "log/old/";
	$dirLogFilesOld_local = PATH_ONLINE_LOGFILES . "old/";
	$logFileName = 'access_log';
	$dirLogFile_local = PATH_ONLINE_LOGFILES;
	$pathLogFiles = $documentRoot_onlineAcquisition . $dirLogFile_extern . $logFileName;

	$jswindowMessage = '';
	$warningMessage = '';
	$errorMessage = '';
	$successMessage = '';
	$infoMessage = '';

	$dbConnection = new DB_Connection();
	$db_open = $dbConnection->db_connect();

	if($_POST["submitFormImportDatas"] != "") {
		// BOF IMPORT FILE
			// BOF FTP-CONNECT TO GET FILES
				$ftp_connect = ftp_connect(constant('FTP_SERVER'));
				$infoMessage .= 'ftp_connect: ' . $ftp_connect . '<br />';
				if($ftp_connect) {
					$ftp_login = ftp_login($ftp_connect, constant('FTP_LOGIN'), constant('FTP_PASSWORD'));

					// BOF GET FILE DATE
						$unix_timestampLogFile = ftp_mdtm($ftp_connect, $pathLogFiles);
						if($unix_timestampLogFile != "-1") { $unix_timestamp_server = $unix_timestampLogFile; }
						$infoMessage .= 'unix_timestamp_server: ' . date("Y-m-d H:i:s", $unix_timestamp_server) . '<br />';

						$timestamp_local = filemtime($dirLogFile_local . basename($pathLogFiles) . ".txt");
						$infoMessage .= 'timestamp_local: ' .$pathLogFiles . ' ' . date("Y-m-d H:i:s", $timestamp_local) . '<br />';
					// EOF GET FILE DATE

					// BOF COPY FILES INTO tmp IF NEW FILE
						if($timestamp_local < $unix_timestamp_server){
							# ftp_chmod($ftp_connect, '0777', $pathLogFiles);
							$ftp_get = ftp_get($ftp_connect, $dirLogFile_local . basename($pathLogFiles) . ".txt", $pathLogFiles, FTP_BINARY);
							if($ftp_get){
								$successMessage .= 'Die Datei &quot;' . $dirLogFile_local . basename($pathLogFiles) . ".txt" . '&quot; wurde aktualisiert.<br />';
							}
							else {
								$errorMessage .= 'Die Datei &quot;' . $dirLogFile_local . basename($pathLogFiles) . ".txt" . '&quot; konnte nicht aktualisiert werden.<br />';
							}
						}
						else {
							$warningMessage .= 'Es liegt keine aktuellere Datei &quot;' . $dirLogFile_local . basename($pathLogFiles) . ".txt" . '&quot; auf dem Server vor.<br />';
						}


						$arrLogFiles_Old = ftp_nlist($ftp_connect, $documentRoot_onlineAcquisition . $dirLogFilesOld_extern);
						if(!empty($arrLogFiles_Old)){
							foreach($arrLogFiles_Old as $thisFile){
								if(!preg_match("/\.$/", $thisFile)){
									if(!file_exists($dirLogFile_local . basename($thisFile))){
										$ftp_get = ftp_get($ftp_connect, $dirLogFile_local . basename($thisFile), $documentRoot_onlineAcquisition . $thisFile, FTP_BINARY);
										if($ftp_get){
											$successMessage .= 'Die Datei &quot;' . basename($thisFile) . '&quot; wurde heruntergeladen.<br />';
										}
										else {
											$errorMessage .= 'Die Datei &quot;' . basename($thisFile) . '&quot; konnte nicht heruntergeladen werden.<br />';
										}
									}
								}
							}
						}
					// EOF COPY FILES INTO tmp IF NEW FILE

					$ftp_close = ftp_quit($ftp_connect);
				}
			// EOF FTP-CONNECT TO GET FILES
		// EOF IMPORT FILE
	}
?>
<?php
	require_once('inc/headerHTML.inc.php');
	$thisTitle = "Shop-Server-Logs";
	$headerHTML = preg_replace("/{###TITLE###}/", strip_tags($thisTitle), $headerHTML);
	echo $headerHTML;

	require_once(FILE_MENUE_TOP);
?>
<div id="mainArea">
	<div id="mainContent">
		<?php require_once(FILE_MENUE_SIDEBAR); ?>
		<div id="contentArea">
			<div id="contentAreaElements">
				<h1><?php if(SHOW_PAGE_TITLE_ICONS) { echo '<img src="' . PATH_ICONS_MENUE_TITLES . 'import.png" alt="" />'; } ?> <?php echo $thisTitle; ?></h1>

				<div class="adminEditArea">
				<form name="formImportDatas" method="post" action="<?php echo $_SERVER["REQUEST_URI"]; ?>" enctype="multipart/form-data">
					<div class="actionButtonsArea">
						<input type="submit" name="submitFormImportDatas" class="inputButton1" value="Log-Dateien holen" />
					</div>
				</form>
				</div>
				<?php displayMessages(); ?>
			</div>
		</div>
		<div class="clear"></div>
	</div>
	<div class="clear"></div>
</div>

<script language="javascript" type="text/javascript">
	$(document).ready(function() {
		$(function() {
			$( "#datepicker" ).datepicker();
		});
		colorRowMouseOver('.displayOrders tbody tr');
	});
</script>

<?php require_once('inc/footerHTML.inc.php'); ?>