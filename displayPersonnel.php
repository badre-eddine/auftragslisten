<?php
	require_once('inc/requires.inc.php');

	if(!$arrGetUserRights["displayPersonnel"] && !$arrGetUserRights["editPersonnel"]) {
		header('location: ' . PAGE_EXIT_LOCATION);
		exit;
	}

	$jswindowMessage = "";
	$warningMessage = '';
	$errorMessage = '';
	$successMessage = '';
	$infoMessage = '';

	$dbConnection = new DB_Connection();
	$db_open = $dbConnection->db_connect();

	$userDatas = getUserDatas();

	if($userDatas["usersUserToPersonnel"] == $_REQUEST["editID"] && !$arrGetUserRights["editPersonnel"]) {
		$arrGetUserRights["editPersonnelSelf"] = true;
	}

	// BOF DEFINE DUTY FIELDS
		$arrFormDutyFields = array(
			"editPersonnelFirstName" => "Vorname",
			"editPersonnelLastName" => "Nachname"
		);
		// CREATE JSON DUTY FIELD
		$jsonFormDutyFields = createJson($arrFormDutyFields);
	// BOF DEFINE DUTY FIELDS

	if(trim($_REQUEST["searchBoxPersonnel"]) != '') {
		$sql = "SELECT
				`personnelID`

				FROM `" . TABLE_PERSONNEL . "`

				WHERE `personnelFirstName` = '" . trim($_REQUEST["searchBoxPersonnel"]) . "'
					OR `personnelLastName` = '" . trim($_REQUEST["searchBoxPersonnel"]) . "'

				LIMIT 1
		";
		$rs = $dbConnection->db_query($sql);

		list($thisPersonnelID) = mysqli_fetch_array($rs);

		if($thisPersonnelID != "") { $_REQUEST["editID"] = $thisPersonnelID; }
		else {
			$warningMessage .= ' Es wurde kein Mitarbeitert gefunden. ';
		}
	}

	if($_REQUEST["editID"] == "" && $_REQUEST["addDatas"] != "")
	{
		$_REQUEST["editID"] = "NEW";
		//header("location: displayOrders.php?ordersType=Bestellung");
		//exit;
	}

	// BOF READ COUNTRIES
		$arrCountryTypeDatas = getCountryTypes();
	// EOF READ COUNTRIES

	// BOF READ COUNTRIES
		$arrPersonnelTypeDatas = getPersonnelTypes();
	// EOF READ COUNTRIES

	// BOF STORE PERSONAL DATAS
	if($_POST["storePersonnelDatas"] != "") {
		$doAction = checkFormDutyFields($arrFormDutyFields);

		if($doAction) {
			if($_POST["editPersonnelID"] == "") {
				// $_POST["editPersonnelID"] = "%";
				$sql = "SELECT (MAX(`personnelID`) + 1) AS `newPersonnelID` FROM`" . TABLE_PERSONNEL . "`";
				$rs = $dbConnection->db_query($sql);
				list($thisPersonnelID) = mysqli_fetch_array($rs);
				if($thisPersonnelID == null || $thisPersonnelID == 0){
					$thisPersonnelID = 1;
				}
			}
			else {
				$thisPersonnelID = $_POST["editPersonnelID"];
			}

			$sql = "REPLACE INTO `" . TABLE_PERSONNEL . "` (
									`personnelID`,
									`personnelFirstName`,
									`personnelLastName`,
									`personnelPhone`,
									`personnelMobile`,
									`personnelFax`,
									`personnelMail`,
									`personnelPhoneBusiness`,
									`personnelMobileBusiness`,
									`personnelFaxBusiness`,
									`personnelMailBusiness`,
									`personnelGroup`,
									`personnelBirthday`,
									`personnelTaxNumber`,
									`personnelTaxClass`,
									`personnelFinanceOffice`,
									`personnelTotalPossibleVacationDaysPerYear`,
									`personnelTotalWorkHoursWeek`,
									`personnelJobStart`,
									`personnelJobEnd`,
									`personnelStreet`,
									`personnelStreetNumber`,
									`personnelZipcode`,
									`personnelCity`,
									`personnelCountry`,
									`personnelKontoinhaber`,
									`personnelBankName`,
									`personnelBankKontonummer`,
									`personnelBankLeitzahl`,
									`personnelBankIBAN`,
									`personnelBankBIC`,
									`personnelHealthInsurance`,
									`personnelSocialSecurityNumber`,
									`personnelCompanyCarNumberPlate`,
									`personnelCompanyCarNotice`,
									`personnelActive`
								)
								VALUES (
									'".$thisPersonnelID."',
									'".($_POST["editPersonnelFirstName"])."',
									'".($_POST["editPersonnelLastName"])."',
									'".cleanPhoneNumbers($_POST["editPersonnelPhone"])."',
									'".cleanPhoneNumbers($_POST["editPersonnelMobile"])."',
									'".cleanPhoneNumbers($_POST["editPersonnelFax"])."',
									'".$_POST["editPersonnelMail"]."',
									'".cleanPhoneNumbers($_POST["editPersonnelPhoneBusiness"])."',
									'".cleanPhoneNumbers($_POST["editPersonnelMobileBusiness"])."',
									'".cleanPhoneNumbers($_POST["editPersonnelFaxBusiness"])."',
									'".$_POST["editPersonnelMailBusiness"]."',
									'".$_POST["editPersonnelGroup"]."',
									'".formatDate($_POST["editPersonnelBirthday"], 'store')."',
									'".preg_replace("/ /", "", $_POST["editPersonnelTaxNumber"])."',
									'".$_POST["editPersonnelTaxClass"]."',
									'".$_POST["editPersonnelFinanceOffice"]."',
									'".$_POST["editPersonnelTotalPossibleVacationDaysPerYear"]."',
									'".$_POST["editPersonnelTotalWorkHoursWeek"]."',
									'".formatDate($_POST["editPersonnelJobStart"], 'store')."',
									'".formatDate($_POST["editPersonnelJobEnd"], 'store')."',
									'".($_POST["editPersonnelStreet"])."',
									'".($_POST["editPersonnelStreetNumber"])."',
									'".($_POST["editPersonnelZipcode"])."',
									'".($_POST["editPersonnelCity"])."',
									'".($_POST["editPersonnelCountry"])."',
									'".($_POST["editPersonnelKontoinhaber"])."',
									'".($_POST["editPersonnelBankName"])."',
									'".($_POST["editPersonnelBankKontonummer"])."',
									'".($_POST["editPersonnelBankLeitzahl"])."',
									'".($_POST["editPersonnelBankIBAN"])."',
									'".($_POST["editPersonnelBankBIC"])."',
									'".($_POST["editPersonnelHealthInsurance"])."',
									'".($_POST["editPersonnelSocialSecurityNumber"])."',
									'".($_POST["editPersonnelCompanyCarNumberPlate"])."',
									'".addslashes($_POST["editPersonnelCompanyCarNotice"])."',
									'".$_POST["editPersonnelActive"]."'
								)
			";

			$rs = $dbConnection->db_query($sql);

			if($rs) {
				$successMessage .= ' Der Datensatz wurde gespeichert.' .'<br />';
				$_REQUEST["editID"] = mysqli_insert_id();
			}
			else {
				$errorMessage .= ' Der Datensatz konnte nicht gespeichert werden.' .'<br />';
			}
		}
		else {
			$errorMessage .= ' Der Datensatz konnte nicht gespeichert werden. Bitte füllen Sie alle Pflichtfelder aus.' .'<br />';
		}
	}
	// EOF STORE PERSONAL DATAS

	// BOF INSERT VACATIONS
	if($_POST["storeVacationDatas"] != "") {
		if($_POST["editPersonnelVacationsEnd"] != "" || $_POST["editPersonnelVacationsEnd"] != "") {

			#if($_POST["editPersonnelVacationsWorkDays"] > 0 && $_POST["editPersonnelVacationsEnd"] != "" && $_POST["editPersonnelVacationsEnd"] != "") {
			if($_POST["editPersonnelVacationsEnd"] != "" && $_POST["editPersonnelVacationsEnd"] != "") {
			

				$personnelCalendarTotalDays = getCountDays(formatDate($_POST["editPersonnelVacationsStart"], 'store'), formatDate($_POST["editPersonnelVacationsEnd"], 'store'));
				$sql = "REPLACE INTO `" . TABLE_PERSONNEL_CALENDAR . "` (
								
								`personnelCalendarPersonnelID`,
								`personnelCalendarStart`,
								`personnelCalendarEnd`,
								`personnelCalendarWorkDays`,
								`personnelCalendarTotalDays`,
								`personnelCalendarReason`,
								`personnelCalendarStoreDate`
							)
							VALUES (
								
								'" . $_POST["editPersonnelID"] . "',
								'" . formatDate($_POST["editPersonnelVacationsStart"], 'store') . "',
								'" . formatDate($_POST["editPersonnelVacationsEnd"], 'store') . "',
								'" . $_POST["editPersonnelVacationsWorkDays"] . "',
								'" . $personnelCalendarTotalDays . "',
								'urlaub',
								NOW()
							);
				";
				$rs = $dbConnection->db_query($sql);

				if($rs) {
					$successMessage .= ' Der Datensatz wurde gespeichert.' .'<br />';
				}
				else {
					$errorMessage .= ' Der Datensatz konnte nicht gespeichert werden.' .'<br />';
				}
			}
			else {
				$errorMessage .= ' Die Urlaubsdaten sind nicht vollständig.' .'<br />';
			}
		}
		$_REQUEST["editID"] = $_POST["editPersonnelID"];
	}
	// EOF INSERT VACATIONS

	// BOF INSERT DISEASES
	if($_POST["storeDiseaseDatas"] != "") {
		if($_POST["editPersonnelDiseaseEnd"] != "" || $_POST["editPersonnelDiseaseEnd"] != "") {

			if($_POST["editPersonnelDiseaseWorkDays"] > 0 && $_POST["editPersonnelDiseaseEnd"] != "" && $_POST["editPersonnelDiseaseEnd"] != "") {

				$personnelCalendarTotalDays = getCountDays(formatDate($_POST["editPersonnelDiseaseStart"], 'store'), formatDate($_POST["editPersonnelDiseaseEnd"], 'store'));
				$sql = "REPLACE INTO `" . TABLE_PERSONNEL_CALENDAR . "` (
								
								`personnelCalendarPersonnelID`,
								`personnelCalendarStart`,
								`personnelCalendarEnd`,
								`personnelCalendarWorkDays`,
								`personnelCalendarTotalDays`,
								`personnelCalendarReason`,
								`personnelCalendarStoreDate`
							)
							VALUES (
								
								'" . $_POST["editPersonnelID"] . "',
								'" . formatDate($_POST["editPersonnelDiseaseStart"], 'store') . "',
								'" . formatDate($_POST["editPersonnelDiseaseEnd"], 'store') . "',
								'" . $_POST["editPersonnelDiseaseWorkDays"] . "',
								'" . $personnelCalendarTotalDays . "',
								'krank',
								NOW()
							);
				";
				$rs = $dbConnection->db_query($sql);

				if($rs) {
					$successMessage .= ' Der Datensatz wurde gespeichert.' .'<br />';
				}
				else {
					$errorMessage .= ' Der Datensatz konnte nicht gespeichert werden.' .'<br />';
				}
			}
			else {
				$errorMessage .= ' Die Urlaubsdaten sind nicht vollständig.' .'<br />';
			}
		}
	}
	// EOF INSERT DISEASES

	// BOF INSERT OVERTIMES
	if($_POST["storeOvertimeDatas"] != "") {
		if($_POST["editPersonnelOvertimeDate"] != "") {
			if($_POST["editPersonnelOvertimeHours"] > 0 && $_POST["editPersonnelOvertimeDate"] != "") {
				$sql = "REPLACE INTO `" . TABLE_PERSONNEL_CALENDAR . "` (
								`personnelCalendarID`,
								`personnelCalendarPersonnelID`,
								`personnelCalendarStart`,
								`personnelCalendarEnd`,
								`personnelCalendarWorkDays`,
								`personnelCalendarTotalDays`,
								`personnelCalendarReason`,
								`personnelCalendarStoreDate`
							)
							VALUES (
								'%',
								'" . $_POST["editPersonnelID"] . "',
								'" . formatDate($_POST["editPersonnelOvertimeDate"], 'store') . "',
								'" . formatDate($_POST["editPersonnelOvertimeDate"], 'store') . "',
								'" . $_POST["editPersonnelOvertimeHours"] . "',
								'" . $_POST["editPersonnelOvertimeHours"] . "',
								'ueberstunden',
								NOW()
							);
				";
				$rs = $dbConnection->db_query($sql);

				if($rs) {
					$successMessage .= ' Der Datensatz wurde gespeichert.' .'<br />';
				}
				else {
					$errorMessage .= ' Der Datensatz konnte nicht gespeichert werden.' .'<br />';
				}
			}
			else {
				$errorMessage .= ' Die &Uuml;berstundendaten sind nicht vollständig.' .'<br />';
			}
		}
	}
	// EOF INSERT OVERTIMES


	// BOF DELETE DATAS
	if($_GET["deleteVacationID"] != "") {
		$sql = "DELETE FROM `" . TABLE_PERSONNEL_CALENDAR . "` WHERE `personnelCalendarID` = '".$_GET["deleteVacationID"]."'";
		$rs = $dbConnection->db_query($sql);

		if($rs) {
			$successMessage .= ' Der Datensatz wurde endgültig gelöscht. ' .'<br />';
		}
		else {
			$errorMessage .= ' Der Datensatz konnte nicht gelöscht werden. ' .'<br />';
		}
		$_REQUEST["editID"] = $_REQUEST["editPersonnelID"];
	}

	if($_POST["deleteDatas"] != "") {
		$sql = "DELETE FROM `" . TABLE_PERSONNEL . "` WHERE `personnelID` = '".$_POST["editPersonnelID"]."'";
		// $rs = $dbConnection->db_query($sql);

		if($rs) {
			$successMessage .= ' Der Datensatz wurde endgültig gelöscht. ' .'<br />';
		}
		else {
			$errorMessage .= ' Der Datensatz konnte nicht gelöscht werden. ' .'<br />';
		}
	}
	// EOF DELETE DATAS

	// BOF READ ALL DATAS
	if($_REQUEST["editID"] == "") {
		$arrPersonnelDatas = getPersonnelDatas();
	}
	// EOF READ ALL DATAS

	// BOF READ SELECTED DATAS
	if($_REQUEST["editID"] != "") {
		$sql = "SELECT
					`personnelID`,
					`personnelFirstName`,
					`personnelLastName`,
					`personnelPhone`,
					`personnelMobile`,
					`personnelFax`,
					`personnelMail`,
					`personnelPhoneBusiness`,
					`personnelMobileBusiness`,
					`personnelFaxBusiness`,
					`personnelMailBusiness`,
					`personnelGroup`,
					`personnelBirthday`,
					`personnelTaxNumber`,
					`personnelTaxClass`,
					`personnelFinanceOffice`,
					`personnelTotalPossibleVacationDaysPerYear`,
					`personnelTotalWorkHoursWeek`,
					`personnelJobStart`,
					`personnelJobEnd`,
					`personnelStreet`,
					`personnelStreetNumber`,
					`personnelZipcode`,
					`personnelCity`,
					`personnelCountry`,
					`personnelKontoinhaber`,
					`personnelBankName`,
					`personnelBankKontonummer`,
					`personnelBankLeitzahl`,
					`personnelBankIBAN`,
					`personnelBankBIC`,
					`personnelHealthInsurance`,
					`personnelSocialSecurityNumber`,
					`personnelCompanyCarNumberPlate`,
					`personnelCompanyCarNotice`,
					`personnelActive`

					FROM `" . TABLE_PERSONNEL . "`

					WHERE `personnelID` = '" . $_REQUEST["editID"] . "'
			";

			$rs = $dbConnection->db_query($sql);

			$selectedPersonnelDatas = array();
			list(
				$selectedPersonnelDatas["personnelID"],
				$selectedPersonnelDatas["personnelFirstName"],
				$selectedPersonnelDatas["personnelLastName"],
				$selectedPersonnelDatas["personnelPhone"],
				$selectedPersonnelDatas["personnelMobile"],
				$selectedPersonnelDatas["personnelFax"],
				$selectedPersonnelDatas["personnelMail"],
				$selectedPersonnelDatas["personnelPhoneBusiness"],
				$selectedPersonnelDatas["personnelMobileBusiness"],
				$selectedPersonnelDatas["personnelFaxBusiness"],
				$selectedPersonnelDatas["personnelMailBusiness"],
				$selectedPersonnelDatas["personnelGroup"],
				$selectedPersonnelDatas["personnelBirthday"],
				$selectedPersonnelDatas["personnelTaxNumber"],
				$selectedPersonnelDatas["personnelTaxClass"],
				$selectedPersonnelDatas["personnelFinanceOffice"],
				$selectedPersonnelDatas["personnelTotalPossibleVacationDaysPerYear"],
				$selectedPersonnelDatas["personnelTotalWorkHoursWeek"],
				$selectedPersonnelDatas["personnelJobStart"],
				$selectedPersonnelDatas["personnelJobEnd"],
				$selectedPersonnelDatas["personnelStreet"],
				$selectedPersonnelDatas["personnelStreetNumber"],
				$selectedPersonnelDatas["personnelZipcode"],
				$selectedPersonnelDatas["personnelCity"],
				$selectedPersonnelDatas["personnelCountry"],
				$selectedPersonnelDatas["personnelKontoinhaber"],
				$selectedPersonnelDatas["personnelBankName"],
				$selectedPersonnelDatas["personnelBankKontonummer"],
				$selectedPersonnelDatas["personnelBankLeitzahl"],
				$selectedPersonnelDatas["personnelBankIBAN"],
				$selectedPersonnelDatas["personnelBankBIC"],
				$selectedPersonnelDatas["personnelHealthInsurance"],
				$selectedPersonnelDatas["personnelSocialSecurityNumber"],
				$selectedPersonnelDatas["personnelCompanyCarNumberPlate"],
				$selectedPersonnelDatas["personnelCompanyCarNotice"],
				$selectedPersonnelDatas["personnelActive"]

			) = mysqli_fetch_array($rs);

			// BOF GET VACATIONS
				$sql = "SELECT
							`personnelCalendarID`,
							`personnelCalendarPersonnelID`,
							`personnelCalendarStart`,
							`personnelCalendarEnd`,
							DATE_FORMAT(`personnelCalendarStart`, '%w') AS `personnelCalendarStartWeekDayNumber`,
							DATE_FORMAT(`personnelCalendarEnd`, '%w') AS `personnelCalendarEndWeekDayNumber`,
							DATE_FORMAT(`personnelCalendarStart`, '%M') AS `personnelCalendarStartMonth`,
							DATE_FORMAT(`personnelCalendarStart`, '%m') AS `personnelCalendarStartMonthNumber`,
							DATE_FORMAT(`personnelCalendarStart`, '%Y') AS `personnelCalendarStartYear`,
							DATE_FORMAT(`personnelCalendarEnd`, '%M') AS `personnelCalendarEndMonth`,
							DATE_FORMAT(`personnelCalendarEnd`, '%Y') AS `personnelCalendarEndYear`,
							`personnelCalendarWorkDays`,
							`personnelCalendarTotalDays`,
							`personnelCalendarReason`

							FROM `" . TABLE_PERSONNEL_CALENDAR . "`

							WHERE 1
								AND `personnelCalendarPersonnelID` = '" . $_REQUEST["editID"] . "'

							ORDER BY
								/*
								`personnelCalendarStartYear` DESC,
								`personnelCalendarStartMonthNumber` DESC
								*/
								`personnelCalendarStart` DESC
					";
				$rs = $dbConnection->db_query($sql);
				$selectedPersonnelCalendarDatas = array();
				$arrTotalCalendarDatas = array();
				$count = 0;
				while($ds = mysqli_fetch_assoc($rs)) {
					foreach(array_keys($ds) as $field) {
						#$selectedPersonnelCalendarDatas[$ds["personnelCalendarReason"]][$ds["personnelCalendarStartYear"]][$ds["personnelCalendarStartMonth"]][$ds["personnelCalendarID"]][$field] = $ds[$field];
						$selectedPersonnelCalendarDatas[$ds["personnelCalendarReason"]][$ds["personnelCalendarStartYear"]][$ds["personnelCalendarStartMonth"]][$count][$field] = $ds[$field];
						
						// BOF JAHRESÜBERGANG SPLITTEN
						// TAGE ANZAHL ERMITTELN
						// FOLGE MONAT ERMITTELN
						
						/*
						ds:

						Array
						(
							[personnelCalendarID] => 238
							[personnelCalendarPersonnelID] => 61
					xxx		[personnelCalendarStart] => 2016-12-27
					xxx		[personnelCalendarEnd] => 2017-01-06
					xxx		[personnelCalendarStartWeekDayNumber] => 2
					xxx		[personnelCalendarEndWeekDayNumber] => 5
					xxx		[personnelCalendarStartMonth] => December
					xxx		[personnelCalendarStartMonthNumber] => 12
					xxx		[personnelCalendarStartYear] => 2016
					xxx		[personnelCalendarEndMonth] => January
					xxx		[personnelCalendarEndYear] => 2017
					xxx		[personnelCalendarWorkDays] => 8.00
					xxx		[personnelCalendarTotalDays] => 11.00
							[personnelCalendarReason] => urlaub
						)

						*/
						if($ds["personnelCalendarStartYear"] < $ds["personnelCalendarEndYear"]){
							#$selectedPersonnelCalendarDatas[$ds["personnelCalendarReason"]][$ds["personnelCalendarStartYear"]][$ds["personnelCalendarStartMonth"]][$count]["personnelCalendarEnd"] = substr($ds["personnelCalendarStart"], 0, 4) . '-12-31';
							
							#$selectedPersonnelCalendarDatas[$ds["personnelCalendarReason"]][$ds["personnelCalendarStartYear"]][$ds["personnelCalendarStartMonth"]][$count]["personnelCalendarEnd"] = $ds["personnelCalendarEnd"], 0, 4) . '-12-31';
						}
						// EOF JAHRESÜBERGANG SPLITTEN
						
					}
					if(empty($arrTotalCalendarDatas[$ds["personnelCalendarStartYear"]])) {
						$arrTotalCalendarDatas[$ds["personnelCalendarStartYear"]] = 0;
					}
					$arrTotalCalendarDatas[$ds["personnelCalendarReason"]][$ds["personnelCalendarStartYear"]] += $ds["personnelCalendarWorkDays"];
					$count++;
				}
			// EOF GET VACATIONS
			#dd('selectedPersonnelCalendarDatas');
			#dd('arrTotalCalendarDatas');
		}
	// EOF READ SELECTED DATAS
?>
<?php
	require_once('inc/headerHTML.inc.php');
	if($_REQUEST["editID"] == "NEW") {
		$thisTitle = "Mitarbeiter anlegen";
	}
	else {
		$thisTitle = "Mitarbeiter bearbeiten";
		if(!empty($selectedPersonnelDatas)){
			$thisTitle .= ': <span  class="headerSelectedEntry">' . $selectedPersonnelDatas["personnelFirstName"] . ' ' . $selectedPersonnelDatas["personnelLastName"] . '</span>';
		}
	}
	$headerHTML = preg_replace("/{###TITLE###}/", strip_tags($thisTitle), $headerHTML);
	echo $headerHTML;

	require_once(FILE_MENUE_TOP);
?>
<div id="mainArea">
	<div id="mainContent">
		<?php require_once(FILE_MENUE_SIDEBAR); ?>
		<div id="contentArea">
			<div id="contentAreaElements">
				<h1><?php if(SHOW_PAGE_TITLE_ICONS) { echo '<img src="' . PATH_ICONS_MENUE_TITLES . 'personnel.png" alt="" />'; } ?> <?php echo $thisTitle; ?></h1>

				<?php if($_REQUEST["editID"] != "") { ?>
					<div class="actionButtonsArea">
						<a href="<?php echo PAGE_DISPLAY_PERSONNEL; ?>" class="linkButton">Zur Mitarbeiter-Auswahl</a>
						<div class="clear"></div>
					</div>
				<?php } ?>

				<div class="contentDisplay">

					<?php displayMessages(); ?>

					<?php if($_REQUEST["editID"] == "") { ?>
					<form name="formSearchPersonnel" method="post" action="<?php echo $_SERVER["PHP_SELF"]; ?>">
					<input type="hidden" name="editPersonnelID" value="<?php echo $selectedPersonnelDatas["personnelID"]; ?>" />
					<div id="searchFilterArea">
						<table border="0" cellspacing="0" cellpadding="0" class="searchFilterContent">
							<tr>
								<td><b>Mitarbeiter:</b></td>
								<td>
									<select name="editID" id="editID" class="inputSelect_510" onchange="submit();">
										<option value=""> --- Bitte w&auml;hlen --- </option>
										<?php
											if(!empty($arrPersonnelDatas)) {
												$thisMarker = '';
												foreach($arrPersonnelDatas as $thisKey => $thisValue) {
													if($thisMarker != $arrPersonnelDatas[$thisKey]["personnelGroup"]){
														$thisMarker = $arrPersonnelDatas[$thisKey]["personnelGroup"];
														echo '<option class="tableRowTitle1" value="">' . $arrPersonnelTypeDatas[$thisValue["personnelGroup"]]["personnelTypesName"] . '</option>';
													}
													if(!$arrPersonnelDatas[$thisKey]["personnelActive"]) {
														$style = "color:#FF0000;font-style:italic;text-decoration:line-through;";
													}
													else {
														$style = "";
													}
													echo '<option style="'. $style .'" value="'.$thisKey.'">' . $arrPersonnelDatas[$thisKey]["personnelLastName"].', '.$arrPersonnelDatas[$thisKey]["personnelFirstName"].' ('.$arrPersonnelTypeDatas[$thisValue["personnelGroup"]]["personnelTypesName"].') - ' . $arrPersonnelDatas[$thisKey]["personnelActiveText"] . '</option>';
												}
											}
										?>
									</select>
								</td>
								<?php if($arrGetUserRights["editPersonnel"]) { ?>
								<td><input name="addDatas" id="addDatas" type="submit" value="Neuen Mitarbeiter anlegen" class="inputButton1" /></td>
								<?php } ?>
							</tr>
						</table>
					</div>
					</form>
					<?php } ?>

					<?php if($_REQUEST["editID"] != "") { ?>
					<div id="tabs">

						<div class="adminInfo">Datensatz-ID: <?php echo $selectedPersonnelDatas["personnelID"]; ?></div>
						<div class="adminEditArea">
							<p class="warningArea">Pflichtfelder <span class="dutyField">(*)</span> m&uuml;ssen ausgef&uuml;llt werden!</p>

							<ul>
								<li><a href="#tabs-1">Personalien</a></li>
								<?php if($userDatas["usersUserToPersonnel"] == $_REQUEST["editID"] || $arrGetUserRights["editPersonnel"]) { ?>
								<li><a href="#tabs-2">Urlaub</a></li>
								<li><a href="#tabs-3">Fehlzeiten</a></li>
								<?php if($userDatas["usersLogin"] == 'thorsten'){ ?>
								<li><a href="#tabs-4">&Uuml;berstunden</a></li>
								<?php } ?>
								<?php } ?>
							</ul>

							<div id="tabs-1">
								<h2 class="headerTab">Personalien</h2>
								<form name="formEditPersonnelDatas" method="post" action="<?php echo $_SERVER["PHP_SELF"]; ?>">
								<input type="hidden" name="editPersonnelID" value="<?php echo $selectedPersonnelDatas["personnelID"]; ?>" />
								<fieldset>
									<legend>Mitarbeiter-Daten</legend>

									<table border="0" width="100%" cellspacing="0" cellpadding="0">
										<tr>
											<td style="width:130px;"><b>Mitarbeiter aktivieren:</b></td>
											<td>
												<input type="checkbox" name="editPersonnelActive" id="editPersonnelActive" class="inputCheckbox1" value="1" <?php if($selectedPersonnelDatas["personnelActive"] == 1) { echo ' checked="checked" '; } ?> />
											</td>
										</tr>
										<tr>
											<td><b>Vorname:</b></td>
											<td><input type="text" name="editPersonnelFirstName" id="editPersonnelFirstName" class="inputField_510" value="<?php echo ($selectedPersonnelDatas["personnelFirstName"]); ?>" /></td>
										</tr>
										<tr>
											<td><b>Nachname:</b></td>
											<td><input type="text" name="editPersonnelLastName" id="editPersonnelLastName" class="inputField_510" value="<?php echo ($selectedPersonnelDatas["personnelLastName"]); ?>" /></td>
										</tr>
										<tr>
											<td><b>Geburtstag:</b></td>
											<td><input type="text" name="editPersonnelBirthday" id="editPersonnelBirthday" maxlength="10" class="inputField_100" value="<?php echo formatDate($selectedPersonnelDatas["personnelBirthday"], 'display'); ?>" /> <span class="infotext">dd.mm.yyyy</span></td>
										</tr>
										<tr>
											<td><b>Gruppe:</b></td>
											<td>
												<select name="editPersonnelGroup" id="editPersonnelGroup" class="inputSelect_510"  <?php if(!$arrGetUserRights["editPersonnel"]) { echo ' readonly="readonly" '; } ?> >
												<?php
													if(!empty($arrPersonnelTypeDatas)) {
														foreach($arrPersonnelTypeDatas as $thisKey => $thisValue){
															$selected = '';
															if($selectedPersonnelDatas["personnelGroup"] == $thisKey) {
																$selected = ' selected="selected" ';
															}
															echo '<option value="' . $thisKey . '" ' . $selected . ' >' . $thisValue["personnelTypesName"] . '</option>';
														}
													}
												?>
												</select>
											</td>
										</tr>
										<tr>
											<td><b>Urlaubstage pro Jahr:</b></td>
											<td>
												<input type="text" name="editPersonnelTotalPossibleVacationDaysPerYear" id="editPersonnelTotalPossibleVacationDaysPerYear" class="inputField_100" value="<?php echo ($selectedPersonnelDatas["personnelTotalPossibleVacationDaysPerYear"]); ?>" <?php if(!$arrGetUserRights["editPersonnel"]) { echo ' readonly="readonly" '; } ?> />
												<?php
													if($selectedPersonnelDatas["personnelTotalPossibleVacationDaysPerYear"] > 0){
														echo ' <span class="infotext">' . number_format(($selectedPersonnelDatas["personnelTotalPossibleVacationDaysPerYear"] / 12), 2, ',', '') . ' Tage pro Monat</span>';
													}
												?>
											</td>
										</tr><tr>
											<td><b>Arbeitszeit pro Woche:</b></td>
											<td><input type="text" name="editPersonnelTotalWorkHoursWeek" id="editPersonnelTotalWorkHoursWeek" class="inputField_100" value="<?php echo ($selectedPersonnelDatas["personnelTotalWorkHoursWeek"]); ?>" <?php if(!$arrGetUserRights["editPersonnel"]) { echo ' readonly="readonly" '; } ?> /></td>
										</tr>
										<tr>
											<td><b>Firmeneintritt:</b></td>
											<td><input type="text" name="editPersonnelJobStart" id="editPersonnelJobStart" maxlength="10" class="inputField_100" value="<?php echo formatDate($selectedPersonnelDatas["personnelJobStart"], 'display'); ?>" <?php if(!$arrGetUserRights["editPersonnel"]) { echo ' readonly="readonly" '; } ?> /> <span class="infotext">dd.mm.yyyy</span></td>
										</tr>
										<tr>
											<td><b>Firmenaustritt:</b></td>
											<td><input type="text" name="editPersonnelJobEnd" id="editPersonnelJobEnd" maxlength="10" class="inputField_100" value="<?php echo formatDate($selectedPersonnelDatas["personnelJobEnd"], 'display'); ?>" <?php if(!$arrGetUserRights["editPersonnel"]) { echo ' readonly="readonly" '; } ?> /> <span class="infotext">dd.mm.yyyy</span></td>
										</tr>
									</table>
								</fieldset>

								<fieldset>
									<legend>Kontakt-Daten (privat)</legend>
									<table border="0" width="100%" cellspacing="0" cellpadding="0">
										<tr>
											<td style="width:130px;"><b>Telefon:</b></td>
											<td><input type="text" name="editPersonnelPhone" id="editPersonnelPhone" class="inputField_510" value="<?php echo formatPhoneNumber($selectedPersonnelDatas["personnelPhone"]); ?>" /></td>
										</tr>
										<tr>
											<td><b>Mobil:</b></td>
											<td><input type="text" name="editPersonnelMobile" id="editPersonnelMobile" class="inputField_510" value="<?php echo formatPhoneNumber($selectedPersonnelDatas["personnelMobile"]); ?>" /></td>
										</tr>
										<tr>
											<td><b>Fax:</b></td>
											<td><input type="text" name="editPersonnelFax" id="editPersonnelFax" class="inputField_510" value="<?php echo formatPhoneNumber($selectedPersonnelDatas["personnelFax"]); ?>" /></td>
										</tr>
										<tr>
											<td><b>Mail:</b></td>
											<td>
												<input type="text" name="editPersonnelMail" id="editPersonnelMail" class="inputField_510" value="<?php echo ($selectedPersonnelDatas["personnelMail"]); ?>" />
												<?php
													if($selectedPersonnelDatas["personnelMail"] != ""){
												?>
												<a href="mailto:<?php echo $selectedPersonnelDatas["personnelMail"]; ?>"><img src="layout/icons/iconMail.gif" width="16" height="14" alt="Mail" title="Mail an den ADM schreiben" /></a>
												<?php
													}
												?>
											</td>
										</tr>
									</table>
								</fieldset>
								<fieldset>
									<legend>Kontakt-Daten (dienstlich)</legend>
									<table border="0" width="100%" cellspacing="0" cellpadding="0">
										<tr>
											<td style="width:130px;"><b>Telefon:</b></td>
											<td><input type="text" name="editPersonnelPhoneBusiness" id="editPersonnelPhoneBusiness" class="inputField_510" value="<?php echo formatPhoneNumber($selectedPersonnelDatas["personnelPhoneBusiness"]); ?>" /></td>
										</tr>
										<tr>
											<td><b>Mobil:</b></td>
											<td><input type="text" name="editPersonnelMobileBusiness" id="editPersonnelMobileBusiness" class="inputField_510" value="<?php echo formatPhoneNumber($selectedPersonnelDatas["personnelMobileBusiness"]); ?>" /></td>
										</tr>
										<tr>
											<td><b>Fax:</b></td>
											<td><input type="text" name="editPersonnelFaxBusiness" id="editPersonnelFaxBusiness" class="inputField_510" value="<?php echo formatPhoneNumber($selectedPersonnelDatas["personnelFaxBusiness"]); ?>" /></td>
										</tr>
										<tr>
											<td><b>Mail:</b></td>
											<td>
												<input type="text" name="editPersonnelMailBusiness" id="editPersonnelMailBusiness" class="inputField_510" value="<?php echo ($selectedPersonnelDatas["personnelMailBusiness"]); ?>" />
												<?php
													if($selectedPersonnelDatas["personnelMailBusiness"] != ""){
												?>
												<a href="mailto:<?php echo $selectedPersonnelDatas["personnelMailBusiness"]; ?>"><img src="layout/icons/iconMail.gif" width="16" height="14" alt="Mail" title="Mail an den ADM schreiben" /></a>
												<?php
													}
												?>
											</td>
										</tr>
									</table>
								</fieldset>
								<fieldset>
									<legend>Anschrift</legend>
									<table border="0" width="100%" cellspacing="0" cellpadding="0">
										<tr>
											<td style="width:130px;"><b>Stra&szlig;e / Hausnummer:</b></td>
											<td>
												<input type="text" name="editPersonnelStreet" id="editPersonnelStreet" class="inputField_427" value="<?php echo ($selectedPersonnelDatas["personnelStreet"]); ?>" /> /
												<input type="text" name="editPersonnelStreetNumber" id="editPersonnelStreetNumber" class="inputField_70" value="<?php echo ($selectedPersonnelDatas["personnelStreetNumber"]); ?>" />
											</td>
										</tr>
										<tr>
											<td><b>PLZ / Ort:</b></td>
											<td>
												<input type="text" name="editPersonnelZipcode" id="editPersonnelZipcode" class="inputField_70" value="<?php echo ($selectedPersonnelDatas["personnelZipcode"]); ?>" /> /
												<input type="text" name="editPersonnelCity" id="editPersonnelCity" class="inputField_427" value="<?php echo ($selectedPersonnelDatas["personnelCity"]); ?>" /></td>
										</tr>
										<tr>
											<td><b>Land:</b></td>
											<td>
												<select name="editPersonnelCountry" id="editPersonnelCountry" class="inputSelect_510">
													<option value=""> - </option>
													<?php
														if(!empty($arrCountryTypeDatas)) {
															foreach($arrCountryTypeDatas as $thisKey => $thisValue) {
																$selected = '';
																if($_REQUEST["editID"] == "NEW" && $thisKey == 81) {
																	$selected = ' selected="selected" ';
																}
																else if($thisKey == $selectedPersonnelDatas["personnelCountry"]) {
																	$selected = ' selected="selected" ';
																}
																echo '
																	<option value="' . $thisKey . '" '.$selected.' >' . ($arrCountryTypeDatas[$thisKey]["countries_name"]). '</option>
																';
															}
														}
													?>
												</select>
											</td>
										</tr>
									</table>
								</fieldset>

								<?php if($userDatas["usersUserToPersonnel"] == $_REQUEST["editID"] || $arrGetUserRights["editPersonnel"]) { ?>


								<fieldset>
									<legend>Steuer-Daten</legend>
									<table border="0" width="100%" cellspacing="0" cellpadding="0">
										<tr>
											<td style="width:130px;"><b>Steuernummer:</b></td>
											<td><input type="text" name="editPersonnelTaxNumber" id="editPersonnelTaxNumber" class="inputField_100" value="<?php echo $selectedPersonnelDatas["personnelTaxNumber"]; ?>" /></td>
										</tr>
										<tr>
											<td><b>Steuerklasse:</b></td>
											<td>
												<select name="editPersonnelTaxClass" id="editPersonnelTaxClass" class="inputField_100" >
													<option value=""> - Bitte w&auml;hlen - </option>
													<?php
														for($i = 1 ; $i < 7 ; $i++){
															$selected = '';
															if($selectedPersonnelDatas["personnelTaxClass"] == $i){
																$selected = ' selected="selected" ';
															}
															echo '<option value="' . $i . '" ' . $selected . ' >' . getRomanNumerals($i) . '</option>';
														}
													?>
												</select>
											</td>
										</tr>
										<tr>
											<td><b>Finanzamt:</b></td>
											<td><input type="text" name="editPersonnelFinanceOffice" id="editPersonnelFinanceOffice" class="inputField_510" value="<?php echo $selectedPersonnelDatas["personnelFinanceOffice"]; ?>" /></td>
										</tr>
									</table>
								</fieldset>

								<fieldset>
									<legend>Dienstfahrzeug</legend>
									<table border="0" width="100%" cellspacing="0" cellpadding="0">
										<tr>
											<td style="width:130px;"><b>Kennzeichen:</b></td>
											<td><input type="text" name="editPersonnelCompanyCarNumberPlate" id="editPersonnelCompanyCarNumberPlate" class="inputField_100" value="<?php echo ($selectedPersonnelDatas["personnelCompanyCarNumberPlate"]); ?>" /></td>
										</tr>
										<tr>
											<td style="width:130px;"><b>Notiz:</b></td>
											<td><input type="text" name="editPersonnelCompanyCarNotice" id="editPersonnelCompanyCarNotice" class="inputField_510" value="<?php echo ($selectedPersonnelDatas["personnelCompanyCarNotice"]); ?>" /></td>
										</tr>
									</table>
								</fieldset>

								<fieldset>
									<legend>Krankenversicherung</legend>
									<table border="0" width="100%" cellspacing="0" cellpadding="0">
										<tr>
											<td style="width:130px;"><b>Krankenkasse:</b></td>
											<td><input type="text" name="editPersonnelHealthInsurance" id="editPersonnelHealthInsurance" class="inputField_510" value="<?php echo ($selectedPersonnelDatas["personnelHealthInsurance"]); ?>" /></td>
										</tr>
										<tr>
											<td style="width:130px;"><b>SozialVers-Nr:</b></td>
											<td><input type="text" name="editPersonnelSocialSecurityNumber" id="editPersonnelSocialSecurityNumber" class="inputField_510" value="<?php echo ($selectedPersonnelDatas["personnelSocialSecurityNumber"]); ?>" /></td>
										</tr>
									</table>
								</fieldset>

								<fieldset>
									<legend>Bankverbindung</legend>
									<table border="0" width="100%" cellspacing="0" cellpadding="0">
										<tr>
											<td style="width:130px;"><b>Kontoinhaber:</b></td>
											<td><input type="text" name="editPersonnelKontoinhaber" id="editPersonnelKontoinhaber" class="inputField_510" value="<?php echo ($selectedPersonnelDatas["personnelKontoinhaber"]); ?>" /></td>
										</tr>
										<tr>
											<td><b>Bankname:</b></td>
											<td><input type="text" name="editPersonnelBankName" id="editPersonnelBankName" class="inputField_510" value="<?php echo ($selectedPersonnelDatas["personnelBankName"]); ?>" /></td>
										</tr>
										<tr>
											<td><b>Bankleitzahl:</b></td>
											<td><input type="text" name="editPersonnelBankLeitzahl" id="editPersonnelBankLeitzahl" class="inputField_510" value="<?php echo ($selectedPersonnelDatas["personnelBankLeitzahl"]); ?>" /></td>
										</tr>
										<tr>
											<td><b>Kontonummer:</b></td>
											<td><input type="text" name="editPersonnelBankKontonummer" id="editPersonnelBankKontonummer" class="inputField_510" value="<?php echo ($selectedPersonnelDatas["personnelBankKontonummer"]); ?>" /></td>
										</tr>
										<tr>
											<td><b>IBAN:</b></td>
											<td><input type="text" name="editPersonnelBankIBAN" id="editPersonnelBankIBAN" class="inputField_510" value="<?php echo ($selectedPersonnelDatas["personnelBankIBAN"]); ?>" /></td>
										</tr>
										<tr>
											<td><b>BIC:</b></td>
											<td><input type="text" name="editPersonnelBankBIC" id="editPersonnelBankBIC" class="inputField_510" value="<?php echo ($selectedPersonnelDatas["personnelBankBIC"]); ?>" /></td>
										</tr>
									</table>
								</fieldset>
								<?php } ?>

								<div class="actionButtonsArea">
									<?php if($userDatas["usersUserToPersonnel"] == $_REQUEST["editID"] || $arrGetUserRights["editPersonnel"]) { ?>
									<input type="submit" class="inputButton1" name="storePersonnelDatas" value="Personalien speichern" />
									&nbsp;
									<?php } ?>
									<input type="submit" class="inputButton1" name="resetPersonnelDatas" value="Abbrechen" onclick="return showWarning(' Wollen Sie diesen Vorgang wirklich abbrechen??? '); "/>
								</div>

								</form>
							</div>

							<?php if($userDatas["usersUserToPersonnel"] == $_REQUEST["editID"] || $arrGetUserRights["editPersonnel"]) { ?>
							<div id="tabs-2">
								<h2 class="headerTab">Urlaub</h2>
								<form name="formEditPersonnelVacationsWorkDays" method="post" action="<?php echo $_SERVER["PHP_SELF"]; ?>">
								<input type="hidden" name="editPersonnelID" value="<?php echo $selectedPersonnelDatas["personnelID"]; ?>" />
								<?php if($selectedPersonnelDatas["personnelActive"] == 1){ ?>
								<?php if($arrGetUserRights["editPersonnel"]) { ?>
								<fieldset>
									<legend>Neuen Urlaub eintragen</legend>
									<p class="infoArea">Urlaub bitte nicht Jahresübergreifend eintragen sondern pro Jahr aufteilen!<br />Nicht 24.12.2014 - 07.01.2015, sondern 24.12.2014 - 31.12.2014 und 01.01.2015 - 07.01.2015</p>
									<table border="0" width="100%" cellspacing="0" cellpadding="0">
										<tr>
											<td width="200"><b>Urlaub von:</b></td>
											<td>
												<input type="text" name="editPersonnelVacationsStart" id="editPersonnelVacationsStart" maxlength="2" class="inputField_100" readonly="readonly" value="" />
											</td>
										</tr>
										<tr>
											<td><b>Urlaub bis (einschlie&szlig;lich):</b></td>
											<td>
												<input type="text" name="editPersonnelVacationsEnd" id="editPersonnelVacationsEnd" maxlength="2" class="inputField_100" readonly="readonly" value="" />
											</td>
										</tr>
										<tr>
											<td><b>Anzahl Werktage:</b></td>
											<td>
												<select name="editPersonnelVacationsWorkDays" id="editPersonnelVacationsWorkDays" class="inputSelect_70">
												<?php
													for($i = 0 ; $i < 120 ; $i = ($i + 0.5)) {
														echo '<option value="' .  $i . '" >' .  number_format($i, 1, ',', '') . '</option>';
													}
												?>
												</select> <span class="infotext">(reine Arbeitstage, ohne Feiertage etc.)</span>
												<div id="displayVacationsDaysTotal"></div>
												<div id="daysVacationsTotalWithoutWeekend"></div>
												<div id="daysVacationsTotalWithoutWeekendWithoutHolidays"></div>
											</td>
										</tr>
										<tr>
											<td><b>Enthaltene Feiertage:</b></td>
											<td id="displayVacationsHolidays"></td>
										</tr>
									</table>
								</fieldset>
								<?php } ?>
								<?php } ?>
								<fieldset>
									<legend>Bisher genommener Urlaub</legend>
									<p><b>Urlaubstage pro Jahr:</b> <?php echo $selectedPersonnelDatas["personnelTotalPossibleVacationDaysPerYear"]; ?></p>

									<?php
										// BOF GET VACATION DAYS IN FIRST QUARTER
											$sql = "
													SELECT
														SUM(`personnelCalendarWorkDays`) AS `personnelCalendarWorkDays`,
														SUM(`personnelCalendarTotalDays`) AS `personnelCalendarTotalDays`,
														DATE_FORMAT(`" . TABLE_PERSONNEL_CALENDAR . "`.`personnelCalendarStart`, '%Y') AS `personnelCalendarYear`,

														CONCAT(
															DATE_FORMAT(`" . TABLE_PERSONNEL_CALENDAR . "`.`personnelCalendarStart`, '%Y'),
															'#',
															IF(DATE_FORMAT(`" . TABLE_PERSONNEL_CALENDAR . "`.`personnelCalendarStart`, '%m') = '01' OR DATE_FORMAT(`" . TABLE_PERSONNEL_CALENDAR . "`.`personnelCalendarStart`, '%m') = '02' OR DATE_FORMAT(`" . TABLE_PERSONNEL_CALENDAR . "`.`personnelCalendarStart`, '%m') = '03', '1. Quartal',
																IF(DATE_FORMAT(`" . TABLE_PERSONNEL_CALENDAR . "`.`personnelCalendarStart`, '%m') = '04' OR DATE_FORMAT(`" . TABLE_PERSONNEL_CALENDAR . "`.`personnelCalendarStart`, '%m') = '05' OR DATE_FORMAT(`" . TABLE_PERSONNEL_CALENDAR . "`.`personnelCalendarStart`, '%m') = '06', '2. Quartal',
																	IF(DATE_FORMAT(`" . TABLE_PERSONNEL_CALENDAR . "`.`personnelCalendarStart`, '%m') = '07' OR DATE_FORMAT(`" . TABLE_PERSONNEL_CALENDAR . "`.`personnelCalendarStart`, '%m') = '08' OR DATE_FORMAT(`" . TABLE_PERSONNEL_CALENDAR . "`.`personnelCalendarStart`, '%m') = '09', '3. Quartal',
																		IF(DATE_FORMAT(`" . TABLE_PERSONNEL_CALENDAR . "`.`personnelCalendarStart`, '%m') = '10' OR DATE_FORMAT(`" . TABLE_PERSONNEL_CALENDAR . "`.`personnelCalendarStart`, '%m') = '11' OR DATE_FORMAT(`" . TABLE_PERSONNEL_CALENDAR . "`.`personnelCalendarStart`, '%m') = '12', '4. Quartal',
																			''
																		)
																	)
																)
															)
														) AS `interval`,

														CEILING(DATE_FORMAT(`" . TABLE_PERSONNEL_CALENDAR . "`.`personnelCalendarStart`, '%m') / 3) AS `quarter`


														FROM `" . TABLE_PERSONNEL_CALENDAR . "`
														WHERE 1
															AND `personnelCalendarReason` = 'urlaub'
															AND `personnelCalendarPersonnelID` = '" . $selectedPersonnelDatas["personnelID"] . "'
															/*
															AND (
																DATE_FORMAT(`" . TABLE_PERSONNEL_CALENDAR . "`.`personnelCalendarStart`, '%m') = '01'
																OR
																DATE_FORMAT(`" . TABLE_PERSONNEL_CALENDAR . "`.`personnelCalendarStart`, '%m') = '02'
																OR
																DATE_FORMAT(`" . TABLE_PERSONNEL_CALENDAR . "`.`personnelCalendarStart`, '%m') = '03'
															)
															*/
														GROUP BY CONCAT(
															DATE_FORMAT(`" . TABLE_PERSONNEL_CALENDAR . "`.`personnelCalendarStart`, '%Y'),
															'#',
															`quarter`
															)
														HAVING `quarter` = 1
												";

											$rs = $dbConnection->db_query($sql);
											$arrVacationDaysInFirstQuarter = array();
											while($ds = mysqli_fetch_assoc($rs)){
												$arrVacationDaysInFirstQuarter[$ds["personnelCalendarYear"]] = $ds["personnelCalendarWorkDays"];
											}
											#dd('arrVacationDaysInFirstQuarter');
										// EOF GET VACATION DAYS IN FIRST QUARTER
									?>


									<table border="0" width="100%" cellspacing="0" cellpadding="0" class="border">
										<tr>
											<th style="width:45px;">Jahr</th>
											<th>m&ouml;gliche Urlaubstage insgesamt</th>
											<th class="spacer"></th>
											<th>genommene Urlaubstage insgesamt</th>
											<th>Urlaubstage 1. Quartal</th>
											<th>Rest-Urlaubstage vom Vorjahr</th>
											<th class="spacer"></th>
											<th>noch m&ouml;gliche Urlaubstage insgesamt</th>
										</tr>
										<?php
											$arrThisUserHolidayData = array();
											if(!empty($arrTotalCalendarDatas)){
												if(!isset($arrTotalCalendarDatas["urlaub"][date("Y")])){
													$arrTotalCalendarDatas["urlaub"][date("Y")] = 0;
												}
												$arrTemp = $arrTotalCalendarDatas['urlaub'] ;
												ksort($arrTemp);

												foreach($arrTemp as $thisKey => $thisValue){

													$thisVacationDaysPerYear = 0;
													if(date("Y", strtotime($selectedPersonnelDatas["personnelJobStart"])) == $thisKey){
														// GET MONTHS OF WORK IN JOB START YEAR
														$jobStartMonth = date("m", strtotime($selectedPersonnelDatas["personnelJobStart"]));
														$workingMonths = 12 - $jobStartMonth;

														// GET WORKING DAYS IN JOB START MONTH
														$jobStartDay = date("d", strtotime($selectedPersonnelDatas["personnelJobStart"]));
														$jobStartWeek = date("W", strtotime($selectedPersonnelDatas["personnelJobStart"]));
														$jobStartMonthDays = date("t", strtotime($selectedPersonnelDatas["personnelJobStart"]));

														if($jobStartDay < 8){
															$workingMonths = $workingMonths + 1;
														}
														else {
															$workingMonths = $workingMonths + 0.5;
														}
														$thisVacationDaysPerYear = $workingMonths * 2; // 2 = FreeDaysPerMonth
													}
													else {
														$thisVacationDaysPerYear = $selectedPersonnelDatas["personnelTotalPossibleVacationDaysPerYear"];
													}

													$thisVacationDaysPerYearRest = $thisVacationDaysPerYear - $thisValue;

												#dd('thisVacationDaysPerYearRest');
													$arrThisUserHolidayData[$thisKey] = array();
													$arrThisUserHolidayData[$thisKey]["thisVacationDaysPerYear"]		= $thisVacationDaysPerYear;

													if(($thisKey - date("Y", strtotime(SOFTWARE_START_DATE)) > -1 )){
														if($arrThisUserHolidayData[($thisKey - 1)]["thisVacationDaysPerYearRest"] == ""){
															$arrThisUserHolidayData[$thisKey]["lastVacationDaysPerYearRest"] = 0;
														}
														else {
															$arrThisUserHolidayData[$thisKey]["lastVacationDaysPerYearRest"]	= $arrThisUserHolidayData[($thisKey - 1)]["thisVacationDaysPerYearRest"];
														}
														if($thisVacationDaysPerYearRest == ''){
															$thisVacationDaysPerYearRest = 0;
														}
														$arrThisUserHolidayData[$thisKey]["thisVacationDaysPerYearRest"]	= $thisVacationDaysPerYearRest;
													}
													else {
														$arrThisUserHolidayData[$thisKey]["lastVacationDaysPerYearRest"]	= 0;
														#$arrThisUserHolidayData[($thisKey - 1)]["thisVacationDaysPerYearRest"]	= 0;
													}

													$arrThisUserHolidayData[$thisKey]["thisVacationDaysPerYearUsed"]	= $thisValue;
													if($arrThisUserHolidayData[$thisKey]["thisVacationDaysPerYearRest"] == ''){
														$arrThisUserHolidayData[$thisKey]["thisVacationDaysPerYearRest"] = 0;
													}

													// BOF CORRECTION LAST FREE DAY FROM 2012
														if($_REQUEST["editID"] == 12){
															$arrThisUserHolidayData[2013]["lastVacationDaysPerYearRest"] =  11;
															$arrThisUserHolidayData[2012]["thisVacationDaysPerYearRest"] =  11;
															$arrThisUserHolidayData[2012]["thisVacationDaysPerYearUsed"] =  13;
														}
													// EOF CORRECTION LAST FREE DAY FROM 2012

													$thisVacationDaysPerYearRestTotal += $arrThisUserHolidayData[$thisKey]["thisVacationDaysPerYearRest"];
												}
											}

											// BOF NEW
											#dd('arrThisUserHolidayData');
											krsort($arrThisUserHolidayData);
											if(!empty($arrThisUserHolidayData)){
																								
												$countRow = 0;
												foreach($arrThisUserHolidayData as $thisKey => $thisValue){
													if($countRow%2 == 0){ $rowClass = 'row0'; }
													else { $rowClass = 'row1'; }
													if($thisKey == date("Y")){
														$rowClass = 'row3';
													}
													echo '<tr class="' . $rowClass . '">';
													echo '<td style="text-align:right;">';
													echo $thisKey;
													echo '</td>';

													echo '<td style="text-align:right;">';
													echo $thisValue["thisVacationDaysPerYear"];
													echo '</td>';

													echo '<td class="spacer"></td>';

													echo '<td style="text-align:right;">';
													echo $thisValue["thisVacationDaysPerYearUsed"];
													echo '</td>';

													echo '<td style="text-align:right;">';
													if($arrVacationDaysInFirstQuarter[$thisKey] == ''){
														$arrVacationDaysInFirstQuarter[$thisKey] = 0;
													}
													echo $arrVacationDaysInFirstQuarter[$thisKey];
													echo '</td>';

													echo '<td style="text-align:right;">';
													echo $thisValue["lastVacationDaysPerYearRest"];
													echo '</td>';

													echo '<td class="spacer"></td>';

													$arrThisUserHolidayData[$thisKey]["addLastVacationDaysPerYearRest"] = 0;
													if($arrVacationDaysInFirstQuarter[$thisKey] > $thisValue["lastVacationDaysPerYearRest"]){
														$arrThisUserHolidayData[$thisKey]["addLastVacationDaysPerYearRest"] = $thisValue["lastVacationDaysPerYearRest"];
													}
													else if($arrVacationDaysInFirstQuarter[$thisKey] == $thisValue["lastVacationDaysPerYearRest"]){
														$arrThisUserHolidayData[$thisKey]["addLastVacationDaysPerYearRest"] = $arrVacationDaysInFirstQuarter[$thisKey];
													}
													else if($arrVacationDaysInFirstQuarter[$thisKey] < $thisValue["lastVacationDaysPerYearRest"]){
														$arrThisUserHolidayData[$thisKey]["addLastVacationDaysPerYearRest"] = $arrVacationDaysInFirstQuarter[$thisKey];
													}

													if($arrThisUserHolidayData[$thisKey]["addLastVacationDaysPerYearRest"] == ''){
														$arrThisUserHolidayData[$thisKey]["addLastVacationDaysPerYearRest"] = 0;
													}

													echo '<td style="text-align:right;">';

													echo $thisValue["thisVacationDaysPerYearRest"];
													echo ' + ' . $arrThisUserHolidayData[$thisKey]["addLastVacationDaysPerYearRest"];
													echo ' = ' . ($thisValue["thisVacationDaysPerYearRest"] + $arrThisUserHolidayData[$thisKey]["addLastVacationDaysPerYearRest"]);
													echo '</td>';

													echo '</tr>';

													$countRow++;
												}
											}
											// EOF NEW

											// BOF OLD
											/*
											if(!empty($arrTotalCalendarDatas)){
												if(!isset($arrTotalCalendarDatas["urlaub"][date("Y")])){
													$arrTotalCalendarDatas["urlaub"][date("Y")] = 0;
												}
												$thisVacationDaysPerYearRestTotal = 0;
												$countRow = 0;
												foreach($arrTotalCalendarDatas['urlaub'] as $thisKey => $thisValue){
													if($countRow%2 == 0){ $rowClass = 'row0'; }
													else { $rowClass = 'row1'; }
													echo '<tr class="' . $rowClass . '">';
													echo '<td style="text-align:right;">' . $thisKey . '</td>';
													echo '<td style="text-align:right;">' . $thisValue . '</td>';
													echo '<td style="text-align:right;">';
													$thisVacationDaysPerYear = 0;
													if(date("Y", strtotime($selectedPersonnelDatas["personnelJobStart"])) == $thisKey){
														// GET MONTHS OF WORK IN JOB START YEAR
														$jobStartMonth = date("m", strtotime($selectedPersonnelDatas["personnelJobStart"]));
														$workingMonths = 12 - $jobStartMonth;

														// GET WORKING DAYS IN JOB START MONTH
														$jobStartDay = date("d", strtotime($selectedPersonnelDatas["personnelJobStart"]));
														$jobStartWeek = date("W", strtotime($selectedPersonnelDatas["personnelJobStart"]));
														$jobStartMonthDays = date("t", strtotime($selectedPersonnelDatas["personnelJobStart"]));

														if($jobStartDay < 8){
															$workingMonths = $workingMonths + 1;
														}
														else {
															$workingMonths = $workingMonths + 0.5;
														}
														$thisVacationDaysPerYear = $workingMonths * 2; // 2 = FreeDaysPerMonth
													}
													else {
														$thisVacationDaysPerYear = $selectedPersonnelDatas["personnelTotalPossibleVacationDaysPerYear"];
													}
													echo $thisVacationDaysPerYear;
													echo '</td>';
													echo '<td style="text-align:right;background-color:#FEFFAF;">';
													$thisVacationDaysPerYearRest = $thisVacationDaysPerYear - $thisValue;
													$thisVacationDaysPerYearRestTotal += $thisVacationDaysPerYearRest;
													echo ($thisVacationDaysPerYearRest);
													echo '</td>';
													echo '</tr>';
													$countRow++;
												}
											}
											*/
											// BOF OLD
										?>
										<!--
										<tr>
											<td colspan="8"><hr /></td>
										</tr>
										<tr class="row3">
											<td colspan="4"><b>Summe der noch m&ouml;glichen Urlaubstage:</b></td>
											<td></td>
											<td></td>
											<td class="spacer"></td>
											<td style="text-align:right;background-color:#FEFFAF;"><b><?php echo $thisVacationDaysPerYearRestTotal; ?></b> (+ Rest)</td>
										</tr>
										-->
									</table>
									<br />
									<table border="0" width="100%" cellspacing="0" cellpadding="0" class="border">
										<tr>
											<th style="width:45px;text-align:right;">#</th>
											<th>Jahr</th>
											<th>Monat</th>
											<th>Urlaub von</th>
											<th>Urlaub bis (einschlie&szlig;lich)</th>
											<th>Anzahl Werktage</th>
											<th>Tage insgesamt</th>
											<th></th>
										</tr>
										<?php
											$thisMarker = '';

											if(!empty($selectedPersonnelCalendarDatas)){
												$countRow = 0;

												foreach($selectedPersonnelCalendarDatas['urlaub'] as $thisKeyYear => $thisValueYear) {
													$thisPersonnelCalendarWorkDays = 0;
													if($thisMarker != $thisKeyYear){
														foreach($thisValueYear as $thisKeyMonth => $thisValueMonth) {
															foreach($thisValueMonth as $thisKey => $thisValue) {
																$thisPersonnelCalendarWorkDays += $thisValue["personnelCalendarWorkDays"];
															}
														}
														echo '<tr class="tableRowTitle1">';
														echo '<td colspan="5">';
														echo $thisKeyYear;
														echo '</td>';

														echo '<td style="text-align:right;">';
														echo $thisPersonnelCalendarWorkDays;
														echo '</td>';


														echo '<td colspan="2">';
														echo '</td>';
														echo '</tr>';
														$thisMarker = $thisKeyYear;
													}
													foreach($thisValueYear as $thisKeyMonth => $thisValueMonth) {
														foreach($thisValueMonth as $thisKey => $thisValue) {
															if($countRow%2 == 0){ $rowClass = 'row0'; }
															else { $rowClass = 'row1'; }
															echo '<tr class="' . $rowClass . '">';
															echo '<td style="text-align:right;"><b>' . ($countRow+1) . '.</b></td>';
															echo '<td style="text-align:left;">' . $thisKeyYear . '</td>';
															echo '<td style="text-align:left;">' . getTimeNames($thisValue["personnelCalendarStartMonthNumber"], 'month', 'long') . '</td>';
															echo '<td style="text-align:left;">' . formatDate($thisValue["personnelCalendarStart"], 'display') . ' ('. getTimeNames($thisValue["personnelCalendarStartWeekDayNumber"], 'day', 'long') . ')</td>';
															echo '<td style="text-align:left;">' . formatDate($thisValue["personnelCalendarEnd"], 'display') .' ('. getTimeNames($thisValue["personnelCalendarEndWeekDayNumber"], 'day', 'long') . ')</td>';
															echo '<td style="text-align:right;">' . $thisValue["personnelCalendarWorkDays"] . '</td>';
															echo '<td style="text-align:right;">' . $thisValue["personnelCalendarTotalDays"] . '</td>';
															echo '<td style="text-align:center;">';
																if($_COOKIE["isAdmin"] == '1' || $thisValue["personnelCalendarStart"] > date("Y-m-d") )  {
																	if($arrGetUserRights["editPersonnel"]) {
																		echo '<a href="' . $_SERVER["PHP_SELF"] . '?deleteVacationID=' . $thisValue["personnelCalendarID"] . '&editPersonnelID=' . $_REQUEST["editID"] . '"><img src="layout/icons/iconDelete.png" width="16" height="16" alt="Entfernen" title="Eintrag l&ouml;schen" onclick="return showWarning(\' Wollen Sie diesen Eintrag endgültig entfernen??? \');" /></a>';
																	}
																	else {
																		echo '<img src="layout/icons/spacer.gif" width="16" height="16" alt="" title="" />';
																	}
																}
																else {
																	echo '<img src="layout/icons/ok.png" width="16" height="16" alt="" title="Urlaub ist aktiv"/>';
																}
															echo '</td>';
															echo '</tr>';
															$countRow++;
														}
													}
												}
											}
										?>
									</table>
								</fieldset>
								<div class="actionButtonsArea">
									<?php if($arrGetUserRights["editPersonnel"]) { ?>
									<input type="submit" class="inputButton1" name="storeVacationDatas" value="Urlaub speichern" />
									&nbsp;
									<?php } ?>
									<input type="submit" class="inputButton1" name="resetVacationDatas" value="Abbrechen" onclick="return showWarning(' Wollen Sie diesen Vorgang wirklich abbrechen??? '); "/>
								</div>
								</form>
							</div>
							<?php } ?>

							<?php if($userDatas["usersUserToPersonnel"] == $_REQUEST["editID"] || $arrGetUserRights["editPersonnel"]) { ?>
							<div id="tabs-3">
								<h2 class="headerTab">Krankheit / Fehlzeiten</h2>
								<form name="formEditPersonnelDiseaseWorkDays" method="post" action="<?php echo $_SERVER["PHP_SELF"]; ?>">
								<input type="hidden" name="editPersonnelID" value="<?php echo $selectedPersonnelDatas["personnelID"]; ?>" />
								<fieldset>
									<legend>Bisherige Fehlzeiten</legend>
										<?php
											if(!empty($selectedPersonnelCalendarDatas)){

												echo '
													<table border="0" width="100%" cellspacing="0" cellpadding="0" class="border">
														<tr>
															<th style="width:45px;text-align:right;">#</th>
															<th>Jahr</th>
															<th>Monat</th>
															<th>Krank von</th>
															<th>Krank bis (einschlie&szlig;lich)</th>
															<th>Anzahl Werktage</th>
															<th>Tage insgesamt</th>
															<th></th>
														</tr>
													';


												$countRow = 0;
												$arrMissingTimesTotal = array();
												foreach($selectedPersonnelCalendarDatas['krank'] as $thisKeyYear => $thisValueYear) {
													foreach($thisValueYear as $thisKeyMonth => $thisValueMonth) {
														foreach($thisValueMonth as $thisKey => $thisValue) {
															if($countRow%2 == 0){ $rowClass = 'row0'; }
															else { $rowClass = 'row1'; }
															echo '<tr class="' . $rowClass . '">';
															echo '<td style="text-align:right;"><b>' . ($countRow+1) . '.</b></td>';
															echo '<td style="text-align:left;">' . $thisKeyYear . '</td>';
															echo '<td style="text-align:left;">' . getTimeNames($thisValue["personnelCalendarStartMonthNumber"], 'month', 'long') . '</td>';
															echo '<td style="text-align:left;">' . formatDate($thisValue["personnelCalendarStart"], 'display') . ' ('. getTimeNames($thisValue["personnelCalendarStartWeekDayNumber"], 'day', 'long') . ')</td>';
															echo '<td style="text-align:left;">' . formatDate($thisValue["personnelCalendarEnd"], 'display') .' ('. getTimeNames($thisValue["personnelCalendarEndWeekDayNumber"], 'day', 'long') . ')</td>';
															echo '<td style="text-align:right;">' . $thisValue["personnelCalendarWorkDays"] . '</td>';
															echo '<td style="text-align:right;background-color:#FEFFAF;">' . $thisValue["personnelCalendarTotalDays"] . '</td>';
															echo '<td style="text-align:center;">';
																if(strtotime($thisValue["personnelCalendarStart"]) <= date("Y-m-d") )  {
																	if($arrGetUserRights["editPersonnel"]) {
																		echo '<a href="' . $_SERVER["PHP_SELF"] . '?deleteVacationID=' . $thisValue["personnelCalendarID"] . '&editPersonnelID=' . $_REQUEST["editID"] . '"><img src="layout/icons/iconDelete.png" width="16" height="16" alt="Entfernen" title="Eintrag l&ouml;schen" onclick="return showWarning(\' Wollen Sie diesen Eintrag endgültig entfernen??? \');" /></a>';
																	}
																	else {
																		echo '<img src="layout/icons/spacer.gif" width="16" height="16" alt="" title="" />';
																	}
																}
																else {
																	echo '<img src="layout/icons/ok.png" width="16" height="16" alt="" title="" />';
																}
															echo '</td>';
															echo '</tr>';
															$arrMissingTimesTotal[$thisKeyYear ] += $thisValue["personnelCalendarTotalDays"];
															$countRow++;
														}
													}
												}
												echo '</table>';

												if(!empty($arrMissingTimesTotal)) {
													echo '<table border="0" width="100%" cellspacing="0" cellpadding="0" class="border">';
													echo '<tr>';
														echo '<th style="width:45px;">Jahr</th>';
														echo '<th>Fehltage insgesamt</th>';
													echo '</tr>';
													$countRow = 0;
													foreach($arrMissingTimesTotal as $thisKey => $thisValue){
														if($countRow%2 == 0){ $rowClass = 'row0'; }
															else { $rowClass = 'row1'; }
															echo '<tr class="' . $rowClass . '">';
															echo '<td>' . $thisKey . '</td>';
															echo '<td style="background-color:#FEFFAF;">' . $thisValue . '</td>';
														echo '</tr>';
														$countRow++;
													}
													echo '</table>';
												}

											}
										?>
								</fieldset>

								<?php if($selectedPersonnelDatas["personnelActive"] == 1){ ?>
								<?php if($arrGetUserRights["editPersonnel"]) { ?>
								<fieldset>
									<legend>Fehlzeiten aufgrund Krankheit eintragen</legend>
									<table border="0" width="100%" cellspacing="0" cellpadding="0">
										<tr>
											<td width="200"><b>Krank von:</b></td>
											<td>
												<input type="text" name="editPersonnelDiseaseStart" id="editPersonnelDiseaseStart" maxlength="2" class="inputField_100" readonly="readonly" value="" />
											</td>
										</tr>
										<tr>
											<td><b>Krank bis (einschlie&szlig;lich):</b></td>
											<td>
												<input type="text" name="editPersonnelDiseaseEnd" id="editPersonnelDiseaseEnd" maxlength="2" class="inputField_100" readonly="readonly" value="" />
											</td>
										</tr>
										<tr>
											<td><b>Anzahl Werktage:</b></td>
											<td>
												<select name="editPersonnelDiseaseWorkDays" id="editPersonnelDiseaseWorkDays" class="inputSelect_70">
												<?php
													for($i = 0 ; $i < 120 ; $i = ($i + 0.5)) {
														echo '<option value="' .  $i . '" >' .  number_format($i, 1, ',', '') . '</option>';
													}
												?>
												</select> <span class="infotext">(reine Arbeitstage, ohne Feiertage etc.)</span>
												<div id="displayDiseaseDaysTotal"></div>
												<div id="daysDiseaseTotalWithoutWeekend"></div>
												<div id="daysDiseaseTotalWithoutWeekendWithoutHolidays"></div>
											</td>
										</tr>
										<tr>
											<td><b>Enthaltene Feiertage:</b></td>
											<td id="displayDiseaseHolidays"></td>
										</tr>
									</table>
								</fieldset>
								<?php } ?>
								<?php } ?>
								<div class="actionButtonsArea">
									<?php if($arrGetUserRights["editPersonnel"]) { ?>
									<input type="submit" class="inputButton1" name="storeDiseaseDatas" value="Krankmeldung speichern" />
									&nbsp;
									<?php } ?>
									<input type="submit" class="inputButton1" name="resetDiseaseDatas" value="Abbrechen" onclick="return showWarning(' Wollen Sie diesen Vorgang wirklich abbrechen??? '); "/>
								</div>
								</form>
							</div>
							<?php } ?>

							<?php
								#if($userDatas["usersUserToPersonnel"] == $_REQUEST["editID"] || $arrGetUserRights["editPersonnel"]) {
								if($userDatas["usersLogin"] == 'thorsten'){
							?>
							<div id="tabs-4">
								<h2 class="headerTab">&Uuml;berstunden</h2>
								<form name="formEditPersonnelOvertimeHours" method="post" action="<?php echo $_SERVER["PHP_SELF"]; ?>">
								<input type="hidden" name="editPersonnelID" value="<?php echo $selectedPersonnelDatas["personnelID"]; ?>" />
								<fieldset>
									<legend>Gesamt-&Uuml;berstunden</legend>
									<table border="0" width="100%" cellspacing="0" cellpadding="0" class="border">
										<tr>
											<th style="width:45px;">Jahr</th>
											<th>gemachte &Uuml;berstunden insgesamt</th>
											<th>in Arbeitstagen</th>
										</tr>
										<?php
											if(!empty($arrTotalCalendarDatas)){
												foreach($arrTotalCalendarDatas['ueberstunden'] as $thisKey => $thisValue){
													if($countRow%2 == 0){ $rowClass = 'row0'; }
													else { $rowClass = 'row1'; }
													echo '<tr class="' . $rowClass . '">';
													echo '<td style="text-align:right;">' . $thisKey . '</td>';
													echo '<td style="text-align:right;">' . number_format($thisValue, 2, ",", "") . ' Stunden </td>';
													echo '<td style="text-align:right;">' . number_format($thisValue / 8, 2, ",", "") . ' Tage</td>';
													echo '</tr>';
												}
											}
										?>

									</table>
									<br />
									<legend>Bisherige &Uuml;berstunden</legend>
									<table border="0" width="100%" cellspacing="0" cellpadding="0" class="border">
										<tr>
											<th style="width:45px;text-align:right;">#</th>
											<th>Jahr</th>
											<th>Monat</th>
											<th>von</th>
											<th>bis (einschlie&szlig;lich)</th>
											<th>Anzahl </th>
											<th> insgesamt</th>
											<th></th>
										</tr>
										<?php
											if(!empty($selectedPersonnelCalendarDatas)){

												$countRow = 0;
												foreach($selectedPersonnelCalendarDatas['ueberstunden'] as $thisKeyYear => $thisValueYear) {
													foreach($thisValueYear as $thisKeyMonth => $thisValueMonth) {
														foreach($thisValueMonth as $thisKey => $thisValue) {
															if($countRow%2 == 0){ $rowClass = 'row0'; }
															else { $rowClass = 'row1'; }
															echo '<tr class="' . $rowClass . '">';
															echo '<td style="text-align:right;"><b>' . ($countRow+1) . '.</b></td>';
															echo '<td style="text-align:left;">' . $thisKeyYear . '</td>';
															echo '<td style="text-align:left;">' . getTimeNames($thisValue["personnelCalendarStartMonthNumber"], 'month', 'long') . '</td>';
															echo '<td style="text-align:left;">' . formatDate($thisValue["personnelCalendarStart"], 'display') . ' ('. getTimeNames($thisValue["personnelCalendarStartWeekDayNumber"], 'day', 'long') . ')</td>';
															echo '<td style="text-align:left;">' . formatDate($thisValue["personnelCalendarEnd"], 'display') .' ('. getTimeNames($thisValue["personnelCalendarEndWeekDayNumber"], 'day', 'long') . ')</td>';
															echo '<td style="text-align:right;">' . $thisValue["personnelCalendarWorkDays"] . '</td>';
															echo '<td style="text-align:right;">' . $thisValue["personnelCalendarTotalDays"] . '</td>';
															echo '<td style="text-align:center;">';
																if(strtotime($thisValue["personnelCalendarStart"]) <= date("Y-m-d") )  {
																	if($arrGetUserRights["editPersonnel"]) {
																		echo '<a href="' . $_SERVER["PHP_SELF"] . '?deleteVacationID=' . $thisValue["personnelCalendarID"] . '&editPersonnelID=' . $_REQUEST["editID"] . '"><img src="layout/icons/iconDelete.png" width="16" height="16" alt="Entfernen" title="Eintrag l&ouml;schen" onclick="return showWarning(\' Wollen Sie diesen Eintrag endgültig entfernen??? \');" /></a>';
																	}
																	else {
																		echo '<img src="layout/icons/spacer.gif" width="16" height="16" />';
																	}
																}
																else {
																	echo '<img src="layout/icons/ok.png" width="16" height="16" alt="" title=""/>';
																}
															echo '</td>';
															echo '</tr>';
															$countRow++;
														}
													}
												}
											}
										?>
									</table>
								</fieldset>

								<?php if($selectedPersonnelDatas["personnelActive"] == 1) { ?>
								<?php if($arrGetUserRights["editPersonnel"]) { ?>
								<fieldset>
									<legend>&Uuml;berstunden eintragen</legend>
									<table border="0" width="100%" cellspacing="0" cellpadding="0">
										<tr>
											<td width="200"><b>&Uuml;berstunde am:</b></td>
											<td>
												<input type="text" name="editPersonnelOvertimeDate" id="editPersonnelOvertimeDate" maxlength="2" class="inputField_100" readonly="readonly" value="" />
											</td>
										</tr>
										<tr>
											<td><b>Anzahl Stunden:</b></td>
											<td>
												<select name="editPersonnelOvertimeHours" class="inputSelect_100">
												<?php
													for($i = 0 ; $i < 4.25 ; $i = ($i + 0.25)) {
														echo '<option value="' .  $i . '" >' .  number_format($i, 2, ',', '') . ' Stunden</option>';
													}
												?>
												</select> <span class="infotext">(Angabe in Viertelstunden, 0.25 &cong; eine Viertelstunde)</span>
											</td>
										</tr>
									</table>
								</fieldset>
								<?php } ?>
								<?php } ?>
								</form>
								<div class="actionButtonsArea">
									<?php if($arrGetUserRights["editPersonnel"]) { ?>
									<input type="submit" class="inputButton1" name="storeOvertimeDatas" value="&Uuml;berstunden speichern" />
									&nbsp;
									<?php } ?>
									<input type="submit" class="inputButton1" name="resetOvertimeDatas" value="Abbrechen" onclick="return showWarning(' Wollen Sie diesen Vorgang wirklich abbrechen??? '); "/>
								</div>
							</div>
							<?php } ?>
						</div>
					</div>
					<?php } ?>
				</div>
			</div>
		</div>
		<div class="clear"></div>
	</div>
</div>

<script language="javascript" type="text/javascript">
	<!--
	/* <![CDATA[ */
	$(document).ready(function() {
		<?php
			if($arrGetUserRights["editPersonnel"]) {
		?>
		$(function() {
			$( "#datepicker" ).datepicker();
			$.datepicker.setDefaults($.datepicker.regional[""]);
			$('#editPersonnelVacationsStart').datepicker($.datepicker.regional["de"]);
			$('#editPersonnelVacationsEnd').datepicker($.datepicker.regional["de"]);
			$('#editPersonnelJobStart').datepicker($.datepicker.regional["de"]);
			$('#editPersonnelJobEnd').datepicker($.datepicker.regional["de"]);
			//$('#editPersonnelBirthday').datepicker($.datepicker.regional["de"]);
			$('#editPersonnelDiseaseStart').datepicker($.datepicker.regional["de"]);
			$('#editPersonnelDiseaseEnd').datepicker($.datepicker.regional["de"]);
			$('#editPersonnelOvertimeDate').datepicker($.datepicker.regional["de"]);
		});

		function convertDate(date, mode){
			var newDate = '';
			if(date != ''){
				if(mode == 'display'){
					var arrTemp = date.split('-');
					newDate = arrTemp[2] + '.' + arrTemp[1] + '.' + arrTemp[0];
				}
				else if(mode == 'store'){
					var arrTemp = date.split('.');
					newDate = arrTemp[2] + '-' + arrTemp[1] + '-' + arrTemp[0];
				}
			}

			return newDate;
		}

		function getNumberOfWorkingDays(dateStart, dateEnd, mode) {
			if(dateStart != '' && dateEnd != ''){
				var i, iDays, iDays1, iDays2, iDaysH, iWDay1, iWDay2;
				dateStart = convertDate(dateStart, 'store');
				dateEnd = convertDate(dateEnd, 'store');

				var strHolidays = '';
				var arrHolidays = new Array();
				var arrHolidayDates = new Array();
				var displayHolidaysContent = '';

				var loadURL = 'inc/getHolidays.inc.php';
				loadURL += '?dateStart=' + dateStart + '&dateEnd=' + dateEnd;
				$.get(loadURL, function(result) {
					strHolidays = result;

					arrHolidays = strHolidays.split(';');
					if(arrHolidays.length > 0){
						for(i = 0 ; i < arrHolidays.length ; i++){
							var arrTemp = new Array();
							arrTemp = arrHolidays[i].split(':');
							if(arrTemp[2]){
								arrHolidayDates[i] = new Date(arrTemp[0]);
								displayHolidaysContent += '<tr>';
								displayHolidaysContent += '<td>' + convertDate(arrTemp[0], 'display') + '<\/td>';
								displayHolidaysContent += '<td>' + arrTemp[2] + ' (' + arrTemp[1] + ')' + '<\/td>';
								// displayHolidaysContent += '<td>' + arrTemp[3] + '<\/td>';
								// displayHolidaysContent += '<td>' + arrTemp[4] + '<\/td>';
								displayHolidaysContent += '<\/tr>';
							}
						}
						if(displayHolidaysContent != ''){
							displayHolidaysContent = '<table border="1" cellpadding="0" cellspacing="0">' + '<tr>' + '<th>Datum<\/th>' + '<th>Feiertag<\/th>' + '<\/tr>' + displayHolidaysContent + '<\/table>';
						}
						else {
							displayHolidaysContent = 'Es sind keine Feiertage im Zeitraum enthalten!';
						}
					}

					dateStart = new Date(dateStart);
					dateEnd = new Date(dateEnd);

					iDays1 = Math.floor (dateStart.getTime() / 86400000);       //Tage seit dem 1.1.1970 (/ (1000 * 3600 * 24))
					iDays2 = Math.floor (dateEnd.getTime() / 86400000);
					iWDay1 = dateStart.getDay();	//Wochentag (0=Sonntag)
					iWDay2 = dateEnd.getDay();		//Wochentag (0=Sonntag)

					if (iDays2 < iDays1) {
						//ggf. alles tauschen, kann bei entspr. Vorbereitung des Fkt.-Aufrufes entfallen
						i = iDays1; iDays1 = iDays2; iDays2 = i;
						i = iWDay1; iWDay1 = iWDay2; iWDay2 = i;
					}
					var daysTotal = (iDays2 - iDays1 + 1);
					iDays = iDays2 - iDays1 + 1
									- (iWDay1 == 0 ? 1 : 0)   //1 x Samstag extra berücksichtigen
									- (iWDay2 == 6 ? 1 : 0)   //1 x Sonntag extra berücksichtigen
									- 2 * Math.floor ((iDays2 - iDays1 + iWDay1 - iWDay2) / 7);   //Differenz zwischen Sonntag-Sonntag

					var daysTotalWithoutWeekend = (iDays);

					for (i = 0; i < arrHolidayDates.length; i++) {
						iDaysH = Math.floor (arrHolidayDates[i].getTime() / 86400000);
						if (iDays1 <= iDaysH && iDaysH <= iDays2) {
							switch (arrHolidayDates[i].getDay()) {
								case 0:
								case 6:
								break;
								default:
								iDays = iDays - 1;
							}
						}
					}

					var daysTotalWithoutWeekendWithoutHolidays = (iDays);

					if(mode == 'vacations'){
						$('#editPersonnelVacationsWorkDays').val(iDays);
						$('#displayVacationsHolidays').html(displayHolidaysContent);
						$('#displayVacationsDaysTotal').text('Tage insgesamt: ' + daysTotal);
						$('#daysVacationsTotalWithoutWeekend').text('abzgl. Wochende: ' + daysTotalWithoutWeekend);
						$('#daysVacationsTotalWithoutWeekendWithoutHolidays').text('abzgl. Wochende + Feiertage: ' + daysTotalWithoutWeekendWithoutHolidays);
					}
					else if(mode == 'disease'){
						$('#editPersonnelDiseaseWorkDays').val(iDays);
						$('#displayDiseaseHolidays').html(displayHolidaysContent);
						$('#displayDiseaseDaysTotal').text('Tage insgesamt: ' + daysTotal);
						$('#daysDiseaseTotalWithoutWeekend').text('abzgl. Wochende: ' + daysTotalWithoutWeekend);
						$('#daysDiseaseTotalWithoutWeekendWithoutHolidays').text('abzgl. Wochende + Feiertage: ' + daysTotalWithoutWeekendWithoutHolidays);
					}
					return iDays;
				});
			}
		}

		$('#editPersonnelVacationsEnd').change(function() {
			var maxDate = $('#editPersonnelVacationsEnd').val();			
			var selectedVacationsEnd = $(this).datepicker('getDate');
			var minYear = selectedVacationsEnd.getFullYear();
			var minDate = '01.01.' + minYear;			
			
			$('#editPersonnelVacationsStart').datepicker("option", "maxDate", maxDate);
			$('#editPersonnelVacationsStart').datepicker("option", "minDate", minDate);
			
			var numberOfWorkingDays = getNumberOfWorkingDays($('#editPersonnelVacationsStart').val(), $('#editPersonnelVacationsEnd').val(), 'vacations');
			if(numberOfWorkingDays) {
				$('#editPersonnelVacationsWorkDays').val(numberOfWorkingDays);
			}
		});


		$('#editPersonnelVacationsStart').change(function() {
			var minDate = $('#editPersonnelVacationsStart').val();
			var selectedVacationsStart = $(this).datepicker('getDate');
			var maxYear = selectedVacationsStart.getFullYear();
			var maxDate = '31.12.' + maxYear;	
			
			$('#editPersonnelVacationsEnd').datepicker("option", "minDate", minDate);
			$('#editPersonnelVacationsEnd').datepicker("option", "maxDate", maxDate);

			var numberOfWorkingDays = getNumberOfWorkingDays($('#editPersonnelVacationsStart').val(), $('#editPersonnelVacationsEnd').val(), 'vacations');
			if(numberOfWorkingDays) {
				$('#editPersonnelVacationsWorkDays').val(numberOfWorkingDays);
			}
		});

		$('#editPersonnelDiseaseEnd').change(function() {
			var maxDate = $('#editPersonnelDiseaseEnd').val();
			$('#editPersonnelDiseaseStart').datepicker("option", "maxDate", maxDate);

			var numberOfDiseaseDays = getNumberOfWorkingDays($('#editPersonnelDiseaseStart').val(), $('#editPersonnelDiseaseEnd').val(), 'disease');
			if(numberOfDiseaseDays) {
				$('#editPersonnelDiseaseWorkDays').val(numberOfDiseaseDays);
			}
		});


		$('#editPersonnelDiseaseStart').change(function() {
			var minDate = $('#editPersonnelDiseaseStart').val();
			$('#editPersonnelDiseaseEnd').datepicker("option", "minDate", minDate);

			var numberOfDiseaseDays = getNumberOfWorkingDays($('#editPersonnelDiseaseStart').val(), $('#editPersonnelDiseaseEnd').val(), 'disease');
			if(numberOfDiseaseDays) {
				$('#editPersonnelDiseaseWorkDays').val(numberOfDiseaseDays);
			}
		});

		// setFormDutyFields(<?php echo $jsonFormDutyFields; ?>);
		colorRowMouseOver('.displayOrders tbody tr');

		$('#editPersonnelZipcode').keyup(function () {
			loadSuggestions('searchPlzCity', [{'triggerElement': '#editPersonnelZipcode', 'fieldCity': '#editPersonnelCity', 'fieldZipCode': '#editPersonnelZipcode'}], 1);
		});
		$('#editPersonnelCity').keyup(function () {
			loadSuggestions('searchCityPlz', [{'triggerElement': '#editPersonnelCity', 'fieldCity': '#editPersonnelCity', 'fieldZipCode': '#editPersonnelZipcode'}], 1);
		});

		$('#editPersonnelBankName').keyup(function(){
			loadSuggestions('searchBankName', [{'triggerElement': '#editPersonnelBankName', 'fieldCode': '#editPersonnelBankLeitzahl', 'fieldName': '#editPersonnelBankName', 'fieldBIC': '#editPersonnelBankBIC', 'fieldIBAN': '#editPersonnelBankIBAN'}], 1);
		});
		$('#editPersonnelBankLeitzahl').keyup(function(){
			loadSuggestions('searchBankCode', [{'triggerElement': '#editPersonnelBankLeitzahl', 'fieldCode': '#editPersonnelBankLeitzahl', 'fieldName': '#editPersonnelBankName', 'fieldBIC': '#editPersonnelBankBIC', 'fieldIBAN': '#editPersonnelBankIBAN'}], 1);
		});
		$('#editPersonnelFinanceOffice').keyup(function(){
			loadSuggestions('searchFinanceOffice', [{'triggerElement': '#editPersonnelFinanceOffice', 'fieldName': '#editPersonnelFinanceOffice'}], 1);
		});

		<?php
			}
		?>
		$(function() {
			$('#tabs').tabs();
		});
	});
	/* ]]> */
	// -->
</script>

<?php require_once('inc/footerHTML.inc.php'); ?>