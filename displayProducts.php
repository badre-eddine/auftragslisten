<?php
	require_once('inc/requires.inc.php');

	if(!$arrGetUserRights["displayProducts"] && !$arrGetUserRights["editProducts"]) {
		header('location: ' . PAGE_EXIT_LOCATION);
		exit;
	}

	$jswindowMessage = "";
	$warningMessage = '';
	$errorMessage = '';
	$successMessage = '';
	$infoMessage = '';

	$dbConnection = new DB_Connection();
	$db_open = $dbConnection->db_connect();

	// BOF GET PRODUCT SUPPLIERS
	$arrProductSupplierDatas = getProductSuppliers();
	// EOF GET PRODUCT SUPPLIERS
?>
<?php
	require_once('inc/headerHTML.inc.php');
	$thisTitle = "Produkte anzeigen";
	$headerHTML = preg_replace("/{###TITLE###}/", strip_tags($thisTitle), $headerHTML);
	echo $headerHTML;

	require_once(FILE_MENUE_TOP);
?>

<div id="mainArea">
	<div id="mainContent">
		<?php require_once(FILE_MENUE_SIDEBAR); ?>
		<div id="contentArea">
			<div id="contentAreaElements">
				<h1><?php if(SHOW_PAGE_TITLE_ICONS) { echo '<img src="' . PATH_ICONS_MENUE_TITLES . 'products.png" alt="" />'; } ?> <?php echo $thisTitle; ?></h1>
				<div class="contentDisplay">
					<div id="searchFilterArea">
						<form name="formFilterSearch" method="post" action="<?php echo $_SERVER["PHP_SELF"]; ?>">
						<table border="0" cellpadding="0" cellspacing="0" class="searchFilterContent">
							<tr>
								<td>
									<label for="editOrdersArtikelNummer">Artikelnummer:</label>
									<input type="text" name="editOrdersArtikelNummer" id="editOrdersArtikelNummer" class="inputField_100" />
								</td>
								<td>
									<label for="editOrdersArtikelBezeichnung">Artikelname:</label>
									<input type="text" name="editOrdersArtikelBezeichnung" id="editOrdersArtikelBezeichnung" class="inputField_260" />
									<input type="hidden" name="editOrdersArtikelID" id="editOrdersArtikelID" value="" />
								</td>
								<td>
									<label for="searchWord">Suchbegriff:</label>
									<input type="text" name="searchWord" id="searchWord" class="inputField_260" />
								</td>
								<td>
									<input type="submit" name="submitformFilterSearch" id="submitformFilterSearch" class="inputButton0" value="Suchen" />
								</td>
							</tr>
						</table>
						</form>
					</div>
					<?php displayMessages(); ?>

					<?php
						// BOF READ ALL DATAS
						if(1) {
							if(LOAD_EXTERNAL_SHOP_DATAS){
								$dbConnection_ExternShop = new DB_Connection(DB_HOST_EXTERN_SHOP, '', DB_NAME_EXTERN_SHOP, DB_USER_EXTERN_SHOP, DB_PASSWORD_EXTERN_SHOP);
								$dbOpen_ExternShop = $dbConnection_ExternShop->db_connect();
							}
							else {
								$dbConnection_ExternShop = $dbConnection;
							}

							if($_REQUEST["searchBoxProduct"] != "" ) {
								$where = " AND (
											`" . TABLE_PRODUCTS . "`.`products_model` LIKE '%" . $_REQUEST["searchBoxProduct"] . "%'
											OR
											`" . TABLE_PRODUCTS_DESCRIPTION . "`.`products_name` LIKE '%" . $_REQUEST["searchBoxProduct"] . "%'
										)
								";
								// (`" . TABLE_PRODUCTS_DESCRIPTION . "`.`products_description` LIKE '%" . $_REQUEST["searchBoxProduct"] . "%'
							}
							else if($_REQUEST["editOrdersArtikelNummer"] != "" ) {
								$where = " AND `" . TABLE_PRODUCTS . "`.`products_model` LIKE '%" . $_REQUEST["editOrdersArtikelNummer"] . "%' ";
							}
							else if($_REQUEST["editOrdersArtikelBezeichnung"] != "" ) {
								$where = " AND `" . TABLE_PRODUCTS_DESCRIPTION . "`.`products_name` LIKE '%" . $_REQUEST["editOrdersArtikelBezeichnung"] . "%' ";
							}
							else if($_REQUEST["searchWord"] != "" ) {
								$where = " AND (
												`" . TABLE_PRODUCTS . "`.`products_model` LIKE '%" . $_REQUEST["searchWord"] . "%'
												OR
												`" . TABLE_PRODUCTS_DESCRIPTION . "`.`products_name` LIKE '%" . $_REQUEST["searchWord"] . "%'
												OR
												`" . TABLE_PRODUCTS_DESCRIPTION . "`.`products_description` LIKE '%" . $_REQUEST["searchWord"] . "%'
												OR
												`" . TABLE_CATEGORIES_DESCRIPTION . "`.`categories_heading_title` LIKE '%" . $_REQUEST["searchWord"] . "%'
												OR
												`" . TABLE_CATEGORIES_DESCRIPTION . "`.`categories_name` LIKE '%" . $_REQUEST["searchWord"] . "%'
											)
									";
							}

							$where .= " AND `" . TABLE_PRODUCTS . "`.`products_status` = '1' ";

							$sql = "SELECT
											/* SQL_CALC_FOUND_ROWS */

											`" . TABLE_PRODUCTS . "`.`products_id`,
											`" . TABLE_PRODUCTS . "`.`products_ean`,
											`" . TABLE_PRODUCTS . "`.`products_quantity`,
											`" . TABLE_PRODUCTS . "`.`products_ebay_quantity`,
											`" . TABLE_PRODUCTS . "`.`products_shippingtime`,
											`" . TABLE_PRODUCTS . "`.`products_model`,
											`" . TABLE_PRODUCTS . "`.`products_sort`,
											CONCAT(
												'http://www.burhan-ctr.de/images/product_images/quickview_images/',
												`" . TABLE_PRODUCTS . "`.`products_image`
											) AS `products_image`,
											`" . TABLE_PRODUCTS . "`.`products_price`,
											`" . TABLE_PRODUCTS . "`.`products_discount_allowed`,
											`" . TABLE_PRODUCTS . "`.`products_date_added`,
											`" . TABLE_PRODUCTS . "`.`products_last_modified`,
											`" . TABLE_PRODUCTS . "`.`products_date_available`,
											`" . TABLE_PRODUCTS . "`.`products_weight`,
											`" . TABLE_PRODUCTS . "`.`products_status`,
											`" . TABLE_PRODUCTS . "`.`free_shipping`,
											`" . TABLE_PRODUCTS . "`.`max_free_shipping_amount`,
											`" . TABLE_PRODUCTS . "`.`max_free_shipping_cart`,
											`" . TABLE_PRODUCTS . "`.`products_tax_class_id`,
											`" . TABLE_PRODUCTS . "`.`product_template`,
											`" . TABLE_PRODUCTS . "`.`options_template`,
											`" . TABLE_PRODUCTS . "`.`manufacturers_id`,
											`" . TABLE_PRODUCTS . "`.`products_ordered`,
											`" . TABLE_PRODUCTS . "`.`products_fsk18`,
											`" . TABLE_PRODUCTS . "`.`products_vpe`,
											`" . TABLE_PRODUCTS . "`.`products_vpe_status`,
											`" . TABLE_PRODUCTS . "`.`products_vpe_value`,
											`" . TABLE_PRODUCTS . "`.`products_startpage`,
											`" . TABLE_PRODUCTS . "`.`products_startpage_sort`,
											`" . TABLE_PRODUCTS . "`.`nc_ultra_shipping_costs`,
											`" . TABLE_PRODUCTS . "`.`products_sperrgut`,

											`" . TABLE_PRODUCTS . "`.`products_group`,
											`" . TABLE_PRODUCTS . "`.`products_printtype`,

											`" . TABLE_PRODUCTS_DESCRIPTION . "`.`products_name`,
											`" . TABLE_PRODUCTS_DESCRIPTION . "`.`products_description`,

											`" . TABLE_CATEGORIES_DESCRIPTION . "`.`categories_id`,
											`" . TABLE_CATEGORIES_DESCRIPTION . "`.`categories_name`,
											`" . TABLE_CATEGORIES_DESCRIPTION . "`.`categories_heading_title`


									FROM `" . TABLE_PRODUCTS . "`

									INNER JOIN `" . TABLE_PRODUCTS_DESCRIPTION . "`
									ON(`" . TABLE_PRODUCTS . "`.`products_id` = `" . TABLE_PRODUCTS_DESCRIPTION . "`.`products_id`)

									LEFT JOIN `" . TABLE_PRODUCTS_TO_CATEGORIES . "`
									ON(`" . TABLE_PRODUCTS . "`.`products_id` = `" . TABLE_PRODUCTS_TO_CATEGORIES . "`.`products_id`)

									LEFT JOIN `" . TABLE_CATEGORIES . "`
									ON(`" . TABLE_PRODUCTS_TO_CATEGORIES . "`.`categories_id` = `" . TABLE_CATEGORIES . "`.`categories_id`)

									LEFT JOIN `" . TABLE_CATEGORIES_DESCRIPTION . "`
									ON(`" . TABLE_CATEGORIES . "`.`categories_id` = `" . TABLE_CATEGORIES_DESCRIPTION . "`.`categories_id`)

									WHERE 1
										" . $where . "
										AND `" . TABLE_CATEGORIES_DESCRIPTION . "`.`language_id` = '2'

									ORDER BY
										`" . TABLE_CATEGORIES . "`.`sort_order`,
										`" . TABLE_CATEGORIES . "`.`categories_id`,

										`" . TABLE_PRODUCTS . "`.`products_group`,
										`" . TABLE_PRODUCTS . "`.`products_sort`,
										`" . TABLE_PRODUCTS . "`.`products_model`
							";
							
							// BOF GET COUNT ALL ROWS
								$sql_getAllRows = "
										SELECT
											COUNT(`" . TABLE_PRODUCTS . "`.`products_id`) AS `countAllRows`
											
										FROM `" . TABLE_PRODUCTS . "`
										
										INNER JOIN `" . TABLE_PRODUCTS_DESCRIPTION . "`
										ON(`" . TABLE_PRODUCTS . "`.`products_id` = `" . TABLE_PRODUCTS_DESCRIPTION . "`.`products_id`)

										LEFT JOIN `" . TABLE_PRODUCTS_TO_CATEGORIES . "`
										ON(`" . TABLE_PRODUCTS . "`.`products_id` = `" . TABLE_PRODUCTS_TO_CATEGORIES . "`.`products_id`)

										LEFT JOIN `" . TABLE_CATEGORIES . "`
										ON(`" . TABLE_PRODUCTS_TO_CATEGORIES . "`.`categories_id` = `" . TABLE_CATEGORIES . "`.`categories_id`)

										LEFT JOIN `" . TABLE_CATEGORIES_DESCRIPTION . "`
										ON(`" . TABLE_CATEGORIES . "`.`categories_id` = `" . TABLE_CATEGORIES_DESCRIPTION . "`.`categories_id`)

										WHERE 1
											" . $where . "
											AND `" . TABLE_CATEGORIES_DESCRIPTION . "`.`language_id` = '2'										
									";
								$rs_getAllRows = $dbConnection->db_query($sql_getAllRows);
								list(
									$countAllRows
								) = mysqli_fetch_array($rs_getAllRows);
							// EOF GET COUNT ALL ROWS

							if($_REQUEST["page"] == "" || $_REQUEST["page"] == 0) {
								$_REQUEST["page"] = 1;
							}

							if(MAX_PRODUCTS_PER_PAGE > 0) {
								$sql .= " LIMIT " . (($_REQUEST["page"] - 1) * MAX_PRODUCTS_PER_PAGE) . ", " . MAX_PRODUCTS_PER_PAGE." ";
							}							

							$rs = $dbConnection_ExternShop->db_query($sql);
#dd('sql');
							// OLD BOF GET ALL ROWS
								#$sql_totalRows = "SELECT FOUND_ROWS() AS `countRows`";
								#$rs_totalRows = $dbConnection_ExternShop->db_query($sql_totalRows);
								#list($countRows) = mysqli_fetch_array($rs_totalRows);
							// OLD EOF GET ALL ROWS

							$countRows = $countAllRows;
							
							$pagesCount = ceil($countRows / MAX_PRODUCTS_PER_PAGE);

							if($pagesCount > 1 && $_REQUEST["displayOrderStatus"] == "") {
								include(FILE_MENUE_PAGES);
							}
							if($countRows > 0) {

								echo '<table border="0" cellpadding="0" cellspacing="0" width="100%" class="displayProducts">
										<thead>
											<tr>
												<th style="width:45px;text-align:right;">#</th>
												<th>Art.-Nr.</th>
												<th style="width:10px;">Bild</th>
												<th>Name</th>
												<th>Beschreibung</th>
												<th>Lieferant</th>
												<th>Details</th>
											</tr>
										</thead>

										<tbody>
								';

								$count = 0;

								###############################
								function displayPartOfText($string, $length=20, $break='<br />\n', $cut=false){
									$newString = $string;
									$newString = preg_replace("/</", " <", $string);
									$newString = strip_tags($newString);
									$newString = wordwrap($newString, $length, $break, $cut);
									$arrNewString = explode($break, $newString);
									$newString = $arrNewString[0];
									$newString .= ' ...';;
									return $newString;
								}
								###############################

								$marker = '';
								while($ds = mysqli_fetch_array($rs)) {
									if($marker != $ds["categories_id"]){
										echo '<tr class="tableRowTitle1">';
										echo '<td colspan="7">';
										echo ($ds["categories_name"]);
										echo '</td>';
										echo '</tr>';
										$marker = $ds["categories_id"];
									}

									if($count%2 == 0){ $rowClass = 'row0'; }
									else { $rowClass = 'row1'; }

									echo '<tr class="'.$rowClass.'">';
										echo '<td style="text-align:right;font-weight:bold;cursor:pointer;" title="Datensatz-ID: '.($ds["products_id"]).'">'.($count + 1).'.</td>';
										echo '<td style="white-space:nowrap">'.$ds["products_model"].'</td>';
										echo '<td style="text-align:center;">
												<div class="xdisplayProductImage">
													<img src="'.preg_replace("/quickview_images/", "thumbnail_images", $ds["products_image"]).'" style="width:200px;max-height:60px" alt="" title="" />
												</div>
											</td>';
										echo '<td>'.$ds["products_name"].'</td>';

										echo '<td>';
										#$thisProductsDescriptionShort = substr(strip_tags($ds["products_description"]), 0, 300).' ...';
										$thisProductsDescriptionShort = displayPartOfText($ds["products_description"], 300, '<br />', false);
										echo $thisProductsDescriptionShort;
										echo '</td>';

										echo '<td>';
										$arrGetSupplier = explode("/", $ds["products_model"]);
										$thisSupplierName = $arrProductSupplierDatas[$arrGetSupplier[(count($arrGetSupplier) - 1)]]["suppliersFirmenname"];
										$thisSupplierID = $arrProductSupplierDatas[$arrGetSupplier[(count($arrGetSupplier) - 1)]]["suppliersID"];
										echo $thisSupplier;
										echo '<a href="' . PAGE_EDIT_DISTRIBUTORS. '?editID=' . $thisSupplierID . '">' . $thisSupplierName . '</a>';
										echo '</td>';

										echo '<td style="white-space:nowrap;text-align:center;width:45px;">';
											// echo '<span class="toolItem"><img src="layout/icons/iconRtf.gif" class="buttonNotice" width="16" height="16" alt="Bemerkung: '.strip_tags(preg_replace("/<br \/\>/", "\\n", $ds["products_description"])).'" /></span>';
											echo '<span class="toolItem"><img src="layout/icons/iconView.png" class="buttonLoadProductDetails" width="16" height="16" title="Artikel ansehen (Datensatz '.$ds["products_id"].')" alt="'.$ds["products_id"].'" /></span>';
										echo '</td>';
									echo '</tr>';

									$count++;
								}

								echo '	</tbody>
									</table>
								';
								if($pagesCount > 1 && $_REQUEST["displayOrderStatus"] == "") {
									include(FILE_MENUE_PAGES);
								}
							}
							if(LOAD_EXTERNAL_SHOP_DATAS){
								if($dbConnection_ExternShop) {
									$dbConnection_ExternShop->db_close($dbOpen_ExternShop);
								}
							}
						}
						// EOF READ ALL DATAS
					?>
				</div>
			</div>
		</div>
		<div class="clear"></div>
	</div>
</div>

<script language="javascript" type="text/javascript">
	$(document).ready(function() {
		$('#searchWord').keyup(function () {
			loadSuggestions('searchProduct', [{'triggerElement': '#searchWord', 'fieldText': '#searchWord'}], 1);
		});
		$('#editOrdersArtikelBezeichnung').keyup(function () {
			loadSuggestions('searchProductName', [{'triggerElement': '#editOrdersArtikelBezeichnung', 'fieldNumber': '#editOrdersArtikelNummer', 'fieldName': '#editOrdersArtikelBezeichnung', 'fieldID': '#editOrdersArtikelID'}], 1);
		});
		$('#editOrdersArtikelNummer').keyup(function () {
			loadSuggestions('searchProductNumber', [{'triggerElement': '#editOrdersArtikelNummer', 'fieldNumber': '#editOrdersArtikelNummer', 'fieldName': '#editOrdersArtikelBezeichnung', 'fieldID': '#editOrdersArtikelID'}], 1);
		});
		colorRowMouseOver('.displayProducts tbody tr');
		$('.buttonNotice').click(function () {
			loadNotice($(this));
		});
		$('.buttonLoadProductDetails').click(function(){
			loadProductDetails($(this).attr('alt'));
		});
		$(".displayProducts .displayProductImage img").css('display', 'none').show().lazyload({
			container: $(".displayProductImage"),
			effect : "fadeIn"
		});
	});
</script>

<?php require_once('inc/footerHTML.inc.php'); ?>