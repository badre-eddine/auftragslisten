<?php
	require_once('inc/requires.inc.php');

	if(!$arrGetUserRights["displaySales"]) {
		header('location: ' . PAGE_EXIT_LOCATION);
		exit;
	}

	$jswindowMessage = '';
	$warningMessage = '';
	$errorMessage = '';
	$successMessage = '';
	$infoMessage = '';


	$dbConnection = new DB_Connection();
	$db_open = $dbConnection->db_connect();

	// BOF READ VERTRETER
		$arrSalesmenDatas = getSalesmen();
		$arrSalesmanToCustomerType = array();
		if(!empty($arrSalesmenDatas)){
			foreach($arrSalesmenDatas as $thisSalesmenDatasKey => $thisSalesmenDatasValue){
				$arrSalesmanToCustomerType[$thisSalesmenDatasValue["customersTyp"]][] = $thisSalesmenDatasKey;
			}
		}

	// EOF READ VERTRETER

	// BOF READ CUSTOMER TYPES
		$arrCustomerTypesDatas = getCustomerTypes();
	// EOF READ VERTRETER

	$defaultORDER = "orderDocumentsDocumentDate";
	$defaultSORT = "ASC";

	if($_POST["searchInterval"] == ""){
		$_POST["searchInterval"] = "WEEK";
	}

	// BOF READ VERTRETER
		$arrAgentDatas = getAgents();
		$arrAgentDatas2 = getAgents2();
	// EOF READ VERTRETER

	// BOF READ BESTELL ARTEN
		$arrOrderTypeDatas = getOrderTypes();
	// EOF READ BESTELL ARTEN
?>
<?php
	require_once('inc/headerHTML.inc.php');
	$thisTitle = "Vertreter-Statistik";
	if($_POST["searchSalesman"] != '') {
		$thisTitle .= ': <span class="headerSelectedEntry">' . $arrSalesmenDatas[$_POST["searchSalesman"]]["customersFirmenname"] . '</span>';
	}

	$headerHTML = preg_replace("/{###TITLE###}/", strip_tags($thisTitle), $headerHTML);
	echo $headerHTML;

	require_once(FILE_MENUE_TOP);

	$sqlSortField = "";
	$sqlSortDirection = "";
	$sqlWhere = "";

?>

<div id="mainArea">
	<div id="mainContent">
		<?php require_once(FILE_MENUE_SIDEBAR); ?>
		<div id="contentArea">
			<a name="top"></a>
			<div id="contentAreaElements">
				<h1><?php if(SHOW_PAGE_TITLE_ICONS) { echo '<img src="' . PATH_ICONS_MENUE_TITLES . 'statistics.png" alt="" />'; } ?> <?php echo $thisTitle; ?></h1>

				<div id="searchFilterArea">
					<form name="formSearchDatas" method="post" action="<?php echo $_SERVER["PHP_SELF"]; ?>">
					<table border="0" cellpadding="0" cellspacing="0" class="searchFilterContent">
						<tr>
							<td>
								<label for="searchSalesman">Vertreter:</label>
								<select name="searchSalesman" id="searchSalesman" class="inputField_300">
									<option value=""> - </option>
									<?php
										/*
										if(!empty($arrSalesmenDatas)) {
											$thisMarker = '';
											foreach($arrSalesmenDatas as $thisKey => $thisValue) {
												if($arrSalesmenDatas[$thisKey]["customersTyp"] != 2){
													if($thisMarker != $arrSalesmenDatas[$thisKey]["customersTyp"]){
														echo '<option value="" class="row3" style="font-weight:bold;">' . $arrCustomerTypesDatas[$arrSalesmenDatas[$thisKey]["customersTyp"]]["customerTypesName"] . '</option>';
														$thisMarker = $arrSalesmenDatas[$thisKey]["customersTyp"];
													}
													$selected = '';
													if($thisKey == $_POST["searchSalesman"]) {
														$selected = ' selected ';
													}
													echo '<option value="' . $thisKey . '" '.$selected.' >' . ($arrSalesmenDatas[$thisKey]["customersFirmenname"]). ' | K-NR: '.$arrSalesmenDatas[$thisKey]["customersKundennummer"].'</option>';
												}
											}
										}
										*/

										if(!empty($arrSalesmenDatas)) {
											$thisMarker = '';
											foreach($arrSalesmanToCustomerType as $thisCustomerType => $thisArrCustomerIDs) {
												foreach($thisArrCustomerIDs as $thisCustomerID) {
													if($thisCustomerType != 2){
														if($thisMarker != $thisCustomerType){
															echo '<option value="" class="row3" style="font-weight:bold;">' . $arrCustomerTypesDatas[$thisCustomerType]["customerTypesName"] . '</option>';
															$thisMarker = $thisCustomerType;
														}
														$selected = '';
														if($thisCustomerID == $_POST["searchSalesman"]) {
															$selected = ' selected ';
														}
														echo '<option value="' . $thisCustomerID . '" '.$selected.' >' . ($arrSalesmenDatas[$thisCustomerID]["customersFirmenname"]). ' | K-NR: '.$arrSalesmenDatas[$thisCustomerID]["customersKundennummer"].'</option>';
													}
												}
											}
										}
									?>
								</select>
							</td>
							<td>
								<label for="searchInterval">Zeitraum:</label>
								<select name="searchInterval" id="searchInterval" class="inputField_130">
									<option value=""></option>
									<option value="WEEK" <?php if($_POST["searchInterval"] == 'WEEK'){ echo ' selected="selected" '; } ?> >pro Woche</option>
									<option value="MONTH" <?php if($_POST["searchInterval"] == 'MONTH'){ echo ' selected="selected" '; } ?> >pro Monat</option>
									<option value="QUARTER" <?php if($_POST["searchInterval"] == 'QUARTER'){ echo ' selected="selected" '; } ?> >pro Vierteljahr</option>
									<option value="YEAR" <?php if($_POST["searchInterval"] == 'YEAR'){ echo ' selected="selected" '; } ?> >pro Jahr</option>
								 </select>
							</td>
							<td>
								<input type="hidden" name="editID" id="editID" value="" />
								<input type="submit" name="submitSearch" class="inputButton0" value="Anzeigen" />
							</td>
						</tr>
					</table>
					<input type="hidden" name="submitSearchForm" id="submitSearchForm" value="1" />
					</form>
				</div>

				<div class="contentDisplay">
					<?php

						//if($_POST["submitExportForm"] == 1) {
						if(1) {
							// BOF GET DATAS
							$whereInvoices = "";
							$whereConfirmations = "";
							$whereAcquisitions = "";
							$whereProducts = "";

							$dateFieldInvoices = "";
							$dateFieldConfirmations = "";
							$dateFieldAcquisitions = "";
							$dateFieldProducts = "";

							if($_POST["searchSalesman"] != ''){
								$whereInvoices .= " AND `" . TABLE_ORDER_INVOICES . "`.`orderDocumentsSalesman` = " .  $_POST["searchSalesman"] . " ";
								$whereConfirmations .= " AND `" . TABLE_ORDER_CONFIRMATIONS . "`.`orderDocumentsSalesman` = " .  $_POST["searchSalesman"] . " ";
								$whereAcquisitions .= " AND `" . TABLE_ACQUISATION_USERS . "`.`usersAuftragslistenCustomerID` = " .  $_POST["searchSalesman"] . " ";
								$whereProducts .= " AND `" . TABLE_ORDERS . "`.`ordersVertreterID` = " .  $_POST["searchSalesman"] . "
													AND SUBSTRING(`" . TABLE_ORDERS . "`.`ordersArtikelKategorieID`, 1, 3) = '001'
									";
							}

							if($_POST["searchInterval"] == "WEEK"){
								$whereInvoices .= "";
								$whereConfirmations .= "";
								$whereAcquisitions .= "";
								$whereProducts .= "";

								$dateFieldInvoices = "
									CONCAT(
										DATE_FORMAT(`" . TABLE_ORDER_INVOICES . "`.`orderDocumentsDocumentDate`, '%Y'),
										'#',
										DATE_FORMAT(`" . TABLE_ORDER_INVOICES . "`.`orderDocumentsDocumentDate`, '%u')
									) AS `interval`
								";
								$dateFieldConfirmations = "
									CONCAT(
										DATE_FORMAT(`" . TABLE_ORDER_CONFIRMATIONS . "`.`orderDocumentsDocumentDate`, '%Y'),
										'#',
										DATE_FORMAT(`" . TABLE_ORDER_CONFIRMATIONS . "`.`orderDocumentsDocumentDate`, '%u')
									) AS `interval`
								";
								$dateFieldAcquisitions = "
									CONCAT(
										DATE_FORMAT(`" . TABLE_ACQUISATION_CUSTOMERS_HISTORY . "`.`customersAcquisitionHistoryDatetime`, '%Y'),
										'#',
										DATE_FORMAT(`" . TABLE_ACQUISATION_CUSTOMERS_HISTORY . "`.`customersAcquisitionHistoryDatetime`, '%u')
									) AS `interval`
								";
								$dateFieldProducts = "
									CONCAT(
										DATE_FORMAT(`" . TABLE_ORDERS . "`.`ordersBestellDatum`, '%Y'),
										'#',
										DATE_FORMAT(`" . TABLE_ORDERS . "`.`ordersBestellDatum`, '%u')
									) AS `interval`
								";
							}
							else if($_POST["searchInterval"] == "MONTH"){
								// DATE_FORMAT(`" . TABLE_ORDER_INVOICES . "`.`orderDocumentsDocumentDate`, '%M') AS `interval`,
								$whereInvoices .= "";
								$whereConfirmations .= "";
								$whereAcquisitions .= "";
								$whereProducts .= "";

								$dateFieldInvoices = "
									CONCAT(
										DATE_FORMAT(`" . TABLE_ORDER_INVOICES . "`.`orderDocumentsDocumentDate`, '%Y'),
										'#',
										DATE_FORMAT(`" . TABLE_ORDER_INVOICES . "`.`orderDocumentsDocumentDate`, '%m')
									) AS `interval`
								";
								$dateFieldConfirmations = "
									CONCAT(
										DATE_FORMAT(`" . TABLE_ORDER_CONFIRMATIONS . "`.`orderDocumentsDocumentDate`, '%Y'),
										'#',
										DATE_FORMAT(`" . TABLE_ORDER_CONFIRMATIONS . "`.`orderDocumentsDocumentDate`, '%m')
									) AS `interval`
								";
								$dateFieldAcquisitions = "
									CONCAT(
										DATE_FORMAT(`" . TABLE_ACQUISATION_CUSTOMERS_HISTORY . "`.`customersAcquisitionHistoryDatetime`, '%Y'),
										'#',
										DATE_FORMAT(`" . TABLE_ACQUISATION_CUSTOMERS_HISTORY . "`.`customersAcquisitionHistoryDatetime`, '%m')
									) AS `interval`
								";
								$dateFieldProducts = "
									CONCAT(
										DATE_FORMAT(`" . TABLE_ORDERS . "`.`ordersBestellDatum`, '%Y'),
										'#',
										DATE_FORMAT(`" . TABLE_ORDERS . "`.`ordersBestellDatum`, '%m')
									) AS `interval`
								";
							}
							else if($_POST["searchInterval"] == "QUARTER"){
								$whereInvoices .= "";
								$whereConfirmations .= "";
								$whereAcquisitions .= "";
								$whereProducts .= "";

								$dateFieldInvoices = "
									CONCAT(
										DATE_FORMAT(`" . TABLE_ORDER_INVOICES . "`.`orderDocumentsDocumentDate`, '%Y'),
										'#',
										IF(DATE_FORMAT(`" . TABLE_ORDER_INVOICES . "`.`orderDocumentsDocumentDate`, '%m') = '01' OR DATE_FORMAT(`" . TABLE_ORDER_INVOICES . "`.`orderDocumentsDocumentDate`, '%m') = '02' OR DATE_FORMAT(`" . TABLE_ORDER_INVOICES . "`.`orderDocumentsDocumentDate`, '%m') = '03', '1. Quartal',
											IF(DATE_FORMAT(`" . TABLE_ORDER_INVOICES . "`.`orderDocumentsDocumentDate`, '%m') = '04' OR DATE_FORMAT(`" . TABLE_ORDER_INVOICES . "`.`orderDocumentsDocumentDate`, '%m') = '05' OR DATE_FORMAT(`" . TABLE_ORDER_INVOICES . "`.`orderDocumentsDocumentDate`, '%m') = '06', '2. Quartal',
												IF(DATE_FORMAT(`" . TABLE_ORDER_INVOICES . "`.`orderDocumentsDocumentDate`, '%m') = '07' OR DATE_FORMAT(`" . TABLE_ORDER_INVOICES . "`.`orderDocumentsDocumentDate`, '%m') = '08' OR DATE_FORMAT(`" . TABLE_ORDER_INVOICES . "`.`orderDocumentsDocumentDate`, '%m') = '09', '3. Quartal',
													IF(DATE_FORMAT(`" . TABLE_ORDER_INVOICES . "`.`orderDocumentsDocumentDate`, '%m') = '10' OR DATE_FORMAT(`" . TABLE_ORDER_INVOICES . "`.`orderDocumentsDocumentDate`, '%m') = '11' OR DATE_FORMAT(`" . TABLE_ORDER_INVOICES . "`.`orderDocumentsDocumentDate`, '%m') = '12', '4. Quartal',
														''
													)
												)
											)
										)
									) AS `interval`
								";
								$dateFieldConfirmations = "
									CONCAT(
										DATE_FORMAT(`" . TABLE_ORDER_CONFIRMATIONS . "`.`orderDocumentsDocumentDate`, '%Y'),
										'#',
										IF(DATE_FORMAT(`" . TABLE_ORDER_CONFIRMATIONS . "`.`orderDocumentsDocumentDate`, '%m') = '01' OR DATE_FORMAT(`" . TABLE_ORDER_CONFIRMATIONS . "`.`orderDocumentsDocumentDate`, '%m') = '02' OR DATE_FORMAT(`" . TABLE_ORDER_CONFIRMATIONS . "`.`orderDocumentsDocumentDate`, '%m') = '03', '1. Quartal',
											IF(DATE_FORMAT(`" . TABLE_ORDER_CONFIRMATIONS . "`.`orderDocumentsDocumentDate`, '%m') = '04' OR DATE_FORMAT(`" . TABLE_ORDER_CONFIRMATIONS . "`.`orderDocumentsDocumentDate`, '%m') = '05' OR DATE_FORMAT(`" . TABLE_ORDER_CONFIRMATIONS . "`.`orderDocumentsDocumentDate`, '%m') = '06', '2. Quartal',
												IF(DATE_FORMAT(`" . TABLE_ORDER_CONFIRMATIONS . "`.`orderDocumentsDocumentDate`, '%m') = '07' OR DATE_FORMAT(`" . TABLE_ORDER_CONFIRMATIONS . "`.`orderDocumentsDocumentDate`, '%m') = '08' OR DATE_FORMAT(`" . TABLE_ORDER_CONFIRMATIONS . "`.`orderDocumentsDocumentDate`, '%m') = '09', '3. Quartal',
													IF(DATE_FORMAT(`" . TABLE_ORDER_CONFIRMATIONS . "`.`orderDocumentsDocumentDate`, '%m') = '10' OR DATE_FORMAT(`" . TABLE_ORDER_CONFIRMATIONS . "`.`orderDocumentsDocumentDate`, '%m') = '11' OR DATE_FORMAT(`" . TABLE_ORDER_CONFIRMATIONS . "`.`orderDocumentsDocumentDate`, '%m') = '12', '4. Quartal',
														''
													)
												)
											)
										)
									) AS `interval`
								";
								$dateFieldAcquisitions = "
									CONCAT(
										DATE_FORMAT(`" . TABLE_ACQUISATION_CUSTOMERS_HISTORY . "`.`customersAcquisitionHistoryDatetime`, '%Y'),
										'#',
										IF(DATE_FORMAT(`" . TABLE_ACQUISATION_CUSTOMERS_HISTORY . "`.`customersAcquisitionHistoryDatetime`, '%m') = '01' OR DATE_FORMAT(`" . TABLE_ACQUISATION_CUSTOMERS_HISTORY . "`.`customersAcquisitionHistoryDatetime`, '%m') = '02' OR DATE_FORMAT(`" . TABLE_ACQUISATION_CUSTOMERS_HISTORY . "`.`customersAcquisitionHistoryDatetime`, '%m') = '03', '1. Quartal',
											IF(DATE_FORMAT(`" . TABLE_ACQUISATION_CUSTOMERS_HISTORY . "`.`customersAcquisitionHistoryDatetime`, '%m') = '04' OR DATE_FORMAT(`" . TABLE_ACQUISATION_CUSTOMERS_HISTORY . "`.`customersAcquisitionHistoryDatetime`, '%m') = '05' OR DATE_FORMAT(`" . TABLE_ACQUISATION_CUSTOMERS_HISTORY . "`.`customersAcquisitionHistoryDatetime`, '%m') = '06', '2. Quartal',
												IF(DATE_FORMAT(`" . TABLE_ACQUISATION_CUSTOMERS_HISTORY . "`.`customersAcquisitionHistoryDatetime`, '%m') = '07' OR DATE_FORMAT(`" . TABLE_ACQUISATION_CUSTOMERS_HISTORY . "`.`customersAcquisitionHistoryDatetime`, '%m') = '08' OR DATE_FORMAT(`" . TABLE_ACQUISATION_CUSTOMERS_HISTORY . "`.`customersAcquisitionHistoryDatetime`, '%m') = '09', '3. Quartal',
													IF(DATE_FORMAT(`" . TABLE_ACQUISATION_CUSTOMERS_HISTORY . "`.`customersAcquisitionHistoryDatetime`, '%m') = '10' OR DATE_FORMAT(`" . TABLE_ACQUISATION_CUSTOMERS_HISTORY . "`.`customersAcquisitionHistoryDatetime`, '%m') = '11' OR DATE_FORMAT(`" . TABLE_ACQUISATION_CUSTOMERS_HISTORY . "`.`customersAcquisitionHistoryDatetime`, '%m') = '12', '4. Quartal',
														''
													)
												)
											)
										)

									) AS `interval`
								";
								$dateFieldProducts = "
									CONCAT(
										DATE_FORMAT(`" . TABLE_ORDERS . "`.`ordersBestellDatum`, '%Y'),
										'#',
										IF(DATE_FORMAT(`" . TABLE_ORDERS . "`.`ordersBestellDatum`, '%m') = '01' OR DATE_FORMAT(`" . TABLE_ORDERS . "`.`ordersBestellDatum`, '%m') = '02' OR DATE_FORMAT(`" . TABLE_ORDERS . "`.`ordersBestellDatum`, '%m') = '03', '1. Quartal',
											IF(DATE_FORMAT(`" . TABLE_ORDERS . "`.`ordersBestellDatum`, '%m') = '04' OR DATE_FORMAT(`" . TABLE_ORDERS . "`.`ordersBestellDatum`, '%m') = '05' OR DATE_FORMAT(`" . TABLE_ORDERS . "`.`ordersBestellDatum`, '%m') = '06', '2. Quartal',
												IF(DATE_FORMAT(`" . TABLE_ORDERS . "`.`ordersBestellDatum`, '%m') = '07' OR DATE_FORMAT(`" . TABLE_ORDERS . "`.`ordersBestellDatum`, '%m') = '08' OR DATE_FORMAT(`" . TABLE_ORDERS . "`.`ordersBestellDatum`, '%m') = '09', '3. Quartal',
													IF(DATE_FORMAT(`" . TABLE_ORDERS . "`.`ordersBestellDatum`, '%m') = '10' OR DATE_FORMAT(`" . TABLE_ORDERS . "`.`ordersBestellDatum`, '%m') = '11' OR DATE_FORMAT(`" . TABLE_ORDERS . "`.`ordersBestellDatum`, '%m') = '12', '4. Quartal',
														''
													)
												)
											)
										)
									) AS `interval`
								";
							}
							else if($_POST["searchInterval"] == "YEAR"){
								$whereInvoices .= "";
								$whereConfirmations .= "";
								$whereAcquisitions .= "";
								$whereProducts .= "";

								$dateFieldInvoices = "
									CONCAT(
										DATE_FORMAT(`" . TABLE_ORDER_INVOICES . "`.`orderDocumentsDocumentDate`, '%Y'),
										'#',
										DATE_FORMAT(`" . TABLE_ORDER_INVOICES . "`.`orderDocumentsDocumentDate`, '%Y')
									) AS `interval`
								";
								$dateFieldConfirmations = "
									CONCAT(
										DATE_FORMAT(`" . TABLE_ORDER_CONFIRMATIONS . "`.`orderDocumentsDocumentDate`, '%Y'),
										'#',
										DATE_FORMAT(`" . TABLE_ORDER_CONFIRMATIONS . "`.`orderDocumentsDocumentDate`, '%Y')
									) AS `interval`
								";
								$dateFieldAcquisitions = "
									CONCAT(
										DATE_FORMAT(`" . TABLE_ACQUISATION_CUSTOMERS_HISTORY . "`.`customersAcquisitionHistoryDatetime`, '%Y'),
										'#',
										DATE_FORMAT(`" . TABLE_ACQUISATION_CUSTOMERS_HISTORY . "`.`customersAcquisitionHistoryDatetime`, '%Y')
									) AS `interval`
								";
								$dateFieldProducts = "
									CONCAT(
										DATE_FORMAT(`" . TABLE_ORDERS . "`.`ordersBestellDatum`, '%Y'),
										'#',
										DATE_FORMAT(`" . TABLE_ORDERS . "`.`ordersBestellDatum`, '%Y')
									) AS `interval`
								";
							}

							$arrIntervals = array();
							$arrDatas = array();

							$sqlInvoices = "
								SELECT
									" . $dateFieldInvoices . ",

									'invoices' AS `dataType`,
									COUNT(`" . TABLE_ORDER_INVOICES . "`.`orderDocumentsID`) AS `countItems`,
									SUM(`" . TABLE_ORDER_INVOICES . "`.`orderDocumentsTotalPrice`) AS `totalPrice`

									FROM `" . TABLE_ORDER_INVOICES . "`
									WHERE 1
										" . $whereInvoices . "

									GROUP BY `interval`

									ORDER BY `interval` DESC
								";

							$sqlConfirmations = "
								SELECT
									" . $dateFieldConfirmations . ",

									'confirmations' AS `dataType`,
									COUNT(`" . TABLE_ORDER_CONFIRMATIONS . "`.`orderDocumentsID`) AS `countItems`,
									SUM(`" . TABLE_ORDER_CONFIRMATIONS . "`.`orderDocumentsTotalPrice`) AS `totalPrice`

									FROM `" . TABLE_ORDER_CONFIRMATIONS . "`
									WHERE 1
										" . $whereConfirmations . "

									GROUP BY `interval`

									ORDER BY `interval` DESC
								";
							$sqlAcquisitions = "

								SELECT
									" . $dateFieldAcquisitions . ",

									/*
									`" . TABLE_ACQUISATION_CUSTOMERS . "`.`customersTimeCreated`,
									`" . TABLE_ACQUISATION_CUSTOMERS . "`.`customersEntryDate`,
									`" . TABLE_ACQUISATION_CUSTOMERS_HISTORY . "`.`customersAcquisitionHistoryDatetime`
									*/

									'acquisitions' AS `dataType`,
									COUNT(`" . TABLE_ACQUISATION_CUSTOMERS . "`.`customersID`) AS `countItems`,
									SUM(0)  AS `totalPrice`

									FROM `" . TABLE_ACQUISATION_USERS . "`

									LEFT JOIN `" . TABLE_ACQUISATION_CUSTOMERS . "`
									ON(`" . TABLE_ACQUISATION_USERS . "`.`usersID` = `" . TABLE_ACQUISATION_CUSTOMERS . "`.`customersVertreterID`)

									LEFT JOIN `" . TABLE_ACQUISATION_CUSTOMERS_HISTORY . "`
									ON(`" . TABLE_ACQUISATION_CUSTOMERS . "`.`customersID` = `" . TABLE_ACQUISATION_CUSTOMERS_HISTORY . "`.`customersAcquisitionHistoryCustomerID`)
									WHERE 1
										" . $whereAcquisitions . "

									GROUP BY `interval`

									ORDER BY `interval` DESC
								";

							$sqlProducts = "
								SELECT
									" . $dateFieldProducts . ",

									'products' AS `dataType`,
									`" . TABLE_ORDERS . "`.`ordersStatus`,
									`" . TABLE_ORDERS . "`.`ordersSourceType`,
									`" . TABLE_ORDERS . "`.`ordersArtikelID`,
									`" . TABLE_ORDERS . "`.`ordersArtikelNummer`,
									`" . TABLE_ORDERS . "`.`ordersArtikelKategorieID`,
									SUM(`" . TABLE_ORDERS . "`.`ordersArtikelMenge`) AS `countItems`,
									`" . TABLE_ORDERS . "`.`ordersAdditionalArtikelID`,
									`" . TABLE_ORDERS . "`.`ordersAdditionalArtikelNummer`,
									`" . TABLE_ORDERS . "`.`ordersAdditionalArtikelKategorieID`,
									`" . TABLE_ORDERS . "`.`ordersAdditionalArtikelMenge`,
									`" . TABLE_ORDERS . "`.`ordersVertreterID`

									FROM `" . TABLE_ORDERS . "`
									WHERE 1
										" . $whereProducts . "

									GROUP BY `interval`

									ORDER BY `interval` DESC
								";


							$rsInvoices = $dbConnection->db_query($sqlInvoices);

							$countTotalRowsInvoice = $dbConnection->db_getMysqlNumRows($rsInvoices);

							while($ds = mysqli_fetch_assoc($rsInvoices)) {
								$arrIntervals[] = $ds["interval"];
								$arrLabelsGraph[$ds["dataType"]][$ds["interval"]] = $ds["interval"];
								$arrDatasGraph[$ds["dataType"]]["countItems"][$ds["interval"]] = $ds["countItems"];
								$arrDatasGraph[$ds["dataType"]]["totalPrice"][$ds["interval"]] = $ds["totalPrice"];
								foreach(array_keys($ds) as $field){
									$arrDatas[$ds["interval"]][$ds["dataType"]][$field] = $ds[$field];
								}
							}

							$rsConfirmations = $dbConnection->db_query($sqlConfirmations);

							$countTotalRowsConfirmations = $dbConnection->db_getMysqlNumRows($rsConfirmations);

							while($ds = mysqli_fetch_assoc($rsConfirmations)) {
								$arrIntervals[] = $ds["interval"];
								$arrLabelsGraph[$ds["dataType"]][$ds["interval"]] = $ds["interval"];
								$arrDatasGraph[$ds["dataType"]]["countItems"][$ds["interval"]] = $ds["countItems"];
								$arrDatasGraph[$ds["dataType"]]["totalPrice"][$ds["interval"]] = $ds["totalPrice"];
								foreach(array_keys($ds) as $field){
									$arrDatas[$ds["interval"]][$ds["dataType"]][$field] = $ds[$field];
								}
							}

							if(DB_HOST_EXTERN_ACQUISITION != '' && DB_NAME_EXTERN_ACQUISITION != '' && DB_USER_EXTERN_ACQUISITION != '' && DB_PASSWORD_EXTERN_ACQUISITION) {
								$existsOnlineAquisition = true;
							}
							else {
								$existsOnlineAquisition = false;
								$errorMessage .= 'Es sind keine Zugansdaten f&uuml;r die Datenbankverbindung zur ' . strtoupper(MANDATOR) . '-Kundenerfassung eingetragen. ' . '<br />';
							}

							if($existsOnlineAquisition) {

								$dbConnection_ExternAcqisition = new DB_Connection(DB_HOST_EXTERN_ACQUISITION, '', DB_NAME_EXTERN_ACQUISITION, DB_USER_EXTERN_ACQUISITION, DB_PASSWORD_EXTERN_ACQUISITION);
								$db_openExternAcqisition = $dbConnection_ExternAcqisition->db_connect();

								$rsAcquisitions = $dbConnection_ExternAcqisition->db_query($sqlAcquisitions);

								$countTotalRowsAcquisitions = $dbConnection_ExternAcqisition->db_getMysqlNumRows($rsAcquisitions);

								while($ds = mysqli_fetch_assoc($rsAcquisitions)) {
									$arrIntervals[] = $ds["interval"];
									$arrLabelsGraph[$ds["dataType"]][$ds["interval"]] = $ds["interval"];
									$arrDatasGraph[$ds["dataType"]]["countItems"][$ds["interval"]] = $ds["countItems"];
									$arrDatasGraph[$ds["dataType"]]["totalPrice"][$ds["interval"]] = $ds["totalPrice"];
									foreach(array_keys($ds) as $field){
										$arrDatas[$ds["interval"]][$ds["dataType"]][$field] = $ds[$field];
									}
								}
							}

							$rsProducts = $dbConnection->db_query($sqlProducts);

							$countTotalRowsProducts = $dbConnection->db_getMysqlNumRows($rsProducts);

							while($ds = mysqli_fetch_assoc($rsProducts)) {
								$arrIntervals[] = $ds["interval"];
								$arrLabelsGraph[$ds["dataType"]][$ds["interval"]] = $ds["interval"];
								$arrDatasGraph[$ds["dataType"]]["countItems"][$ds["interval"]] = $ds["countItems"];
								$arrDatasGraph[$ds["dataType"]]["totalPrice"][$ds["interval"]] = $ds["totalPrice"];
								foreach(array_keys($ds) as $field){
									$arrDatas[$ds["interval"]][$ds["dataType"]][$field] = $ds[$field];
								}
							}

							if(!empty($arrIntervals)){
								$arrIntervals = array_unique($arrIntervals);
								arsort($arrIntervals);
							}

							if($countTotalRowsInvoice > 0 || $countTotalRowsConfirmations > 0 || $countTotalRowsAcquisitions > 0 || $countTotalRowsProducts > 0) {
							?>
								<!-- BOF GRAPH ELEMENTS -->
								<script type="text/javascript" language="javascript" src="<?php echo PATH_RGRAPH; ?>libraries/RGraph.common.core.js" ></script>
								<script type="text/javascript" language="javascript" src="<?php echo PATH_RGRAPH; ?>libraries/RGraph.bar.js" ></script>
								<script type="text/javascript" language="javascript" src="<?php echo PATH_RGRAPH; ?>libraries/RGraph.line.js" ></script>

								<div id="tabs">
									<div class="adminEditArea">
										<ul>
											<li><a href="#tabs-1">Rechnungsdiagramme</a></li>
											<li><a href="#tabs-2">Auftragsdiagramme</a></li>
											<li><a href="#tabs-3">Produktdiagramme</a></li>
											<li><a href="#tabs-4">Kundenerfassungsdiagramme</a></li>
										</ul>
										<div id="tabs-1">
											<h2>Anzahl der Rechnungen</h2>
											<canvas id="cvs_invoicesCountItems" width="900" height="250">[No canvas support]</canvas>
											<hr />
											<h2>Summe der Rechnungen</h2>
											<canvas id="cvs_invoicesTotalPrice" width="900" height="250">[No canvas support]</canvas>
										</div>

										<div id="tabs-2">
											<h2>Anzahl der Auftr&auml;ge</h2>
											<canvas id="cvs_confirmationsCountItems" width="900" height="250">[No canvas support]</canvas>
											<hr />
											<h2>Summe der Auftr&auml;ge</h2>
											<canvas id="cvs_confirmationsTotalPrice" width="900" height="250">[No canvas support]</canvas>
										</div>

										<div id="tabs-3">
											<h2>Produkt-St&uuml;ckzahlen (Kennzeichenhalter)</h2>
											<canvas id="cvs_productCountItems" width="900" height="250">[No canvas support]</canvas>
										</div>

										<div id="tabs-4">
											<h2>Anzahl der Kundenbesuche</h2>
											<canvas id="cvs_acquisitionsCountItems" width="900" height="250">[No canvas support]</canvas>
										</div>
									</div>
								</div>
								<!-- EOF GRAPH ELEMENTS -->

							<?php
								echo '<table width="100%" cellpadding="0" cellspacing="0" class="displayOrders">';

								echo '<tr>';
								echo '<th style="width:45px" rowspan="2">#</th>';
								echo '<th style="width:80px" rowspan="2">Zeitraum</th>';
								echo '<th colspan="2">Auftragsbest&auml;tigungen</th>';
								echo '<th colspan="2">Rechnungen</th>';
								echo '<th rowspan="2" style="width:150px">Eintr&auml;ge Kundenerfassung</th>';
								echo '<th colspan="2">St&uuml;ckzahlen</th>';
								echo '<th style="width:50px" rowspan="2">Info</th>';
								echo '</tr>';

								echo '<tr>';
								echo '<th>Anzahl</th>';
								echo '<th>Betrag</th>';
								echo '<th>Anzahl</th>';
								echo '<th>Betrag</th>';
								echo '<th>KZH</th>';
								echo '<th>Sonstige</th>';
								echo '</tr>';

								$countRow = 0;
								$thisMarker = "";

								foreach($arrIntervals as $thisIntervalKey => $thisIntervalValue) {
									if($arrDatas[$thisIntervalValue]['confirmations']['countItems'] > 0 || $arrDatas[$thisIntervalValue]['invoices']['countItems'] > 0 || $arrDatas[$thisIntervalValue]['acquisitions']['countItems'] > 0 || $arrDatas[$thisIntervalValue]['products']['countItems'] > 0){
										if($countRow == 0) {

										}

										$rowClass = 'row0';
										if($countRow%2 == 0) {
											$rowClass = 'row1';
										}
										$arrTemp = explode('#', $thisIntervalValue);
										if($thisMarker != $arrTemp[0]) {
											$thisMarker = $arrTemp[0];
											echo '<tr><td colspan="10" class="tableRowTitle1">' . $arrTemp[0] . '</td></tr>';
										}

										echo '<tr class="'.$rowClass.'">';

										echo '<td style="text-align:right;"><b>';
										echo ($countRow + 1);
										echo '.</b></td>';
										echo '<td>';

										if($_POST["searchInterval"] == "MONTH"){
											$thisMonth = getTimeNames($arrTemp[1], 'month', 'long');
											echo $thisMonth;
										}
										else {
											if($_POST["searchInterval"] == "WEEK"){ echo 'KW '; }
											echo $arrTemp[1];
										}
										echo '</td>';
										echo '<td style="text-align:right;background-color:#FEFFAF;">';
										echo '' . $arrDatas[$thisIntervalValue]['confirmations']['countItems'] . '';
										echo '</td>';
										echo '<td style="text-align:right;background-color:#FEFFAF;">';
										echo '' . number_format($arrDatas[$thisIntervalValue]['confirmations']['totalPrice'], 2, ',', '.') . ' &euro;';
										echo '</td>';

										echo '<td style="text-align:right;background-color:#FEFFAF;">';
										echo '' . $arrDatas[$thisIntervalValue]['invoices']['countItems'] . '';
										echo '</td>';
										echo '<td style="text-align:right;background-color:#FEFFAF;">';
										echo '' . number_format($arrDatas[$thisIntervalValue]['invoices']['totalPrice'], 2, ',', '.') . ' &euro;';
										echo '</td>';

										echo '<td style="text-align:right;">';
										echo '' . $arrDatas[$thisIntervalValue]['acquisitions']['countItems'] . '';
										echo '</td>';

										echo '<td style="text-align:right;">';
										echo '' . $arrDatas[$thisIntervalValue]['products']['countItems'] . '';
										echo '</td>';

										echo '<td style="text-align:right;">';
										echo 'xxx';
										echo '</td>';

										echo '<td>';
										echo '<span class="toolItem">';
										echo '<img src="layout/icons/iconInfo.png" class="buttonShopOrderInfo" alt="Details" title="Detailinformationen ansehen">';
										echo '<div class="orderDetailsContent" style="display:none; visibility:hidden;">';
										echo '</div>';
										echo '</span>';
										echo '</td>';
										echo '</tr>';
										$countRow++;
									}
								}
								echo '</table>';
							}
							else {
								$warningMessage .= 'F&uuml;r den gew&auml;hlten Zeitraum wurden keine Daten gefunden.';
							}
							// EOF GET DATAS
						}
					?>
					<?php displayMessages(); ?>
				</div>

			</div>
		</div>
		<div class="clear"></div>
	</div>
</div>
<script language="javascript" type="text/javascript">
	$(document).ready(function() {
		$(function() {
			colorRowMouseOver('.displayOrders tbody tr');
			$('.buttonShopOrderInfo').css('cursor', 'pointer');
			$('.buttonShopOrderInfo').click(function () {
				loadOrderDetails($(this), '<?php echo BASEPATH; ?>', 'Details der Bestellung');
			});

			// BOF GET DATAS FOR GRAPH CASH
			<?php
				if(!empty($arrDatasGraph["invoices"])){
					$arrDatasGraph["invoices"]["countItems"] = array_reverse($arrDatasGraph["invoices"]["countItems"], true);
					$arrDatasGraph["invoices"]["totalPrice"] = array_reverse($arrDatasGraph["invoices"]["totalPrice"], true);
					$arrLabelsGraph["invoices"] = array_reverse($arrLabelsGraph["invoices"] , true);
				}
				if(!empty($arrDatasGraph["confirmations"])){
					$arrDatasGraph["confirmations"]["countItems"] = array_reverse($arrDatasGraph["confirmations"]["countItems"], true);
					$arrDatasGraph["confirmations"]["totalPrice"] = array_reverse($arrDatasGraph["confirmations"]["totalPrice"], true);
					$arrLabelsGraph["confirmations"] = array_reverse($arrLabelsGraph["confirmations"] , true);
				}
				if(!empty($arrDatasGraph["acquisitions"])){
					$arrDatasGraph["acquisitions"]["countItems"] = array_reverse($arrDatasGraph["acquisitions"]["countItems"], true);
					$arrDatasGraph["acquisitions"]["totalPrice"]= array_reverse($arrDatasGraph["acquisitions"]["totalPrice"], true);
					$arrLabelsGraph["acquisitions"] = array_reverse($arrLabelsGraph["acquisitions"] , true);
				}
				if(!empty($arrDatasGraph["products"])){
					$arrDatasGraph["products"]["countItems"] = array_reverse($arrDatasGraph["products"]["countItems"], true);
					$arrDatasGraph["products"]["totalPrice"]= array_reverse($arrDatasGraph["products"]["totalPrice"], true);
					$arrLabelsGraph["products"] = array_reverse($arrLabelsGraph["products"] , true);
				}
			?>

			createGraph('cvs_invoicesCountItems', [<?php echo implode(",", array_values($arrDatasGraph["invoices"]["countItems"])); ?>], [<?php echo "'" . implode("','", $arrLabelsGraph["invoices"]) . "'"; ?>], 'Line', [['green','red']]);
			createGraph('cvs_invoicesTotalPrice', [<?php echo implode(",", array_values($arrDatasGraph["invoices"]["totalPrice"])); ?>], [<?php echo "'" . implode("','", $arrLabelsGraph["invoices"]) . "'"; ?>], 'Line', [['green','red']]);
			createGraph('cvs_confirmationsCountItems', [<?php echo implode(",", array_values($arrDatasGraph["confirmations"]["countItems"])); ?>], [<?php echo "'" . implode("','", $arrLabelsGraph["confirmations"]) . "'"; ?>], 'Line', [['green','red']]);
			createGraph('cvs_confirmationsTotalPrice', [<?php echo implode(",", array_values($arrDatasGraph["confirmations"]["totalPrice"])); ?>], [<?php echo "'" . implode("','", $arrLabelsGraph["confirmations"]) . "'"; ?>], 'Line', [['green','red']]);
			createGraph('cvs_acquisitionsCountItems', [<?php echo implode(",", array_values($arrDatasGraph["acquisitions"]["countItems"])); ?>], [<?php echo "'" . implode("','", $arrLabelsGraph["acquisitions"]) . "'"; ?>], 'Line', [['green','red']]);
			createGraph('cvs_productCountItems', [<?php echo implode(",", array_values($arrDatasGraph["products"]["countItems"])); ?>], [<?php echo "'" . implode("','", $arrLabelsGraph["acquisitions"]) . "'"; ?>], 'Line', [['green','red']]);

		// EOF GET DATAS FOR GRAPH CASH
		});
		$(function() {
			$('#tabs').tabs();
		});
	});
</script>
<?php require_once('inc/footerHTML.inc.php'); ?>