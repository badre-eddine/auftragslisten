<?php
	require_once('inc/requires.inc.php');

	if(!$arrGetUserRights["openPhpTerm"]) {
		header('location: ' . PAGE_EXIT_LOCATION);
		exit;
	}

	$jswindowMessage = '';
	$warningMessage = '';
	$errorMessage = '';
	$successMessage = '';
	$infoMessage = '';

	$dbConnection = new DB_Connection();
	$db_open = $dbConnection->db_connect();
?>
<?php
	require_once('inc/headerHTML.inc.php');
	$thisTitle = "PHP-TERM";
	$headerHTML = preg_replace("/{###TITLE###}/", strip_tags($thisTitle), $headerHTML);
	echo $headerHTML;

	require_once(FILE_MENUE_TOP);
?>
<div id="mainArea">
	<div id="mainContent">
		<?php require_once(FILE_MENUE_SIDEBAR); ?>
		<div id="contentArea">
			<div id="contentAreaElements">
				<h1><?php if(SHOW_PAGE_TITLE_ICONS) { echo '<img src="' . PATH_ICONS_MENUE_TITLES . 'phpterm.png" alt="" />'; } ?> <?php echo $thisTitle; ?></h1>

				<?php displayMessages(); ?>
				<div id="tabs">
					<ul>
						<li><a href="#tabs-1">Spezielle Befehle</a></li>
						<li><a href="#tabs-2">MS-DOS-Kommandos</a></li>
						<li><a href="#tabs-3">Befehle in Batch-Dateien</a></li>
					</ul>
					<div class="descriptionArea">
						<div id="tabs-1">
							<h2>Spezielle Befehle</h2>
							<table border="0" cellpadding="0" cellspacing="0" width="100%">
								<tr><td style="width:200px;"><b>mysqlcheck</b></td><td>mysqlcheck -u burhan -pburhan --optimize --all-databases)</td></tr>
								<tr><td style="width:200px;"><b>zip</b></td><td>C:\Programme\7-Zip\7z.exe a ###FILE_DESTINATION###}.7z {###DIR_SOURCE###})</td></tr>
								<tr><td style="width:200px;"><b>copy</b></td><td>copy Y:\Software\Auftragslisten_SICHERUNGEN\{###FILE_NAME###} E:\myFILES\Auftragslisten\{###FILE_NAME###})</td></tr>
								<tr><td style="width:200px;"><b>del</b></td><td>del Y:\Outlook\THORSTEN_Auftragslisten\{###FILE_NAME###})</td></tr>
							</table>
						</div>

						<div id="tabs-2">
							<h2>MS-DOS-Kommandos</h2>
							<p><i>Anm.:</i>Die folgende Zusammenstellung basiert im wesentlichen auf der DOS-Version 3.2. </p>
							<table border="0" cellpadding="0" cellspacing="0" width="100%">
								<tr><td style="width:200px;"><b>CD</b></td><td>CD<i>Pfad</i>oder CD .. (zur h&ouml;heren Ebene) oder CD \ (Wurzelverzeichnis)<br />change directory</td></tr>
								<tr><td><b>CHKDSK</b></td><td>check disk (Information &uuml;ber die Plattenbelegung)<br />CHKDSK [<i>d</i>:][<i>Pfad</i>][<i>Datei</i>] [<b>/F</b>Korrektur,<b>/V</b>Dateien anzeigen]</td></tr>
								<tr><td><b>CLS</b></td><td>clear screen (Bildschirm l&ouml;schen)</td></tr>
								<tr><td><b>COPY</b></td><td>COPY [<i>Pfad</i>]<i>name.ext</i>[<i>Pfad</i>][<i>name.ext</i>] kopieren.<br />Optionaler Parameter:<b>/V</b>verifizieren.<br />Bsp.:<tt>COPY c:\ordner1\datei1.txt a:\datei2</tt></td></tr>
								<tr><td><b>DATE</b></td><td>Datum<i>tt.mm.jj</i></td></tr>
								<tr><td><b>DEL</b></td><td>DEL [<i>Pfad</i>]<i>name.ext</i>Datei l&ouml;schen (delete)</td></tr>
								<tr><td><b>DIR</b></td><td>directory (Dateiverzeichnis) [<b>..</b>h&ouml;here Ebene,<b>/P</b>seitenweise,<b>/W</b>kurz]</td></tr>
								<tr><td><b>ECHO</b></td><td>ECHO [on off] (<i>Text</i>) Textausgabe auf Bildschirm</td></tr>
								<tr><td><b>ERASE</b></td><td>ERASE [<i>Pfad</i>]<i>name.ext</i>Datei l&ouml;schen (wie DEL)</td></tr>
								<tr><td><b>FIND</b></td><td>z.B.<tt>DIR |FIND "EXE"</tt>(alle "..."-Dateien);<br />FIND [/V][/C][/N] "<i>Zeichenfolge</i>" [<i>datei</i>]</td></tr>
								<tr><td><b>FORMAT</b></td><td>FORMAT A: Diskette formatieren (HD);<br />FORMAT A: /4 (5 1/4" auf 360 KB);<br />FORMAT A: /S (DOS-Systemdiskette erzeugen)</td></tr>
								<tr><td><b>MD</b></td><td>make directory (neues Verzeichnis anlegen).<br />Bsp.:<tt>MD ebene2</tt></td></tr>
								<tr><td><b>MORE</b></td><td>(Stop nach jeder Seite)<br />z.B.<tt>TYPE datei |MORE</tt></td></tr>
								<tr><td><b>PATH</b></td><td>Suchpfade, PATH<i>Pfad1;Pfad2;Pfad3</i></td></tr>
								<tr><td><b>PRINT</b></td><td>PRINT<i>datei</i><br />drucken einer Datei (verschiedene Optionen)</td></tr>
								<tr><td><b>PROMPT</b></td><td>(Systemanfrage) $p (Pfad), $g (Pfeil &gt;), $t (Zeit), $d (Datum), $h (Backspace), $_ (line feed)</td></tr>
								<tr><td><b>RD</b></td><td>remove directory (Verzeichnis l&ouml;schen; das Verzeichnis mu&szlig; leer sein!)</td></tr>
								<tr><td><b>REN</b></td><td>REN<i>name1 name2</i>rename (neuer Name)</td></tr>
								<tr><td><b>SET</b></td><td>Einstellungen anzeigen bzw. &auml;ndern (comspec, prompt, path)</td></tr>
								<tr><td><b>SORT</b></td><td>sortieren, z.B.<tt>DIR |SORT</tt>(/R r&uuml;ckw&auml;rts, /+<i>n</i>ab Spalte<i>n</i>)</td></tr>
								<tr><td><b>TIME</b></td><td>Zeit<i>hh:mm:ss</i></td></tr>
								<tr><td><b>TYPE</b></td><td>listet eine ASCII-Textdatei (seitenweise mit |MORE)</td></tr>
								<tr><td><b>VER</b></td><td>zeigt die MS-DOS-Versionsnummer</td></tr>
								<tr><td><b>VERIFY</b></td><td>[ON OFF] verifizieren (beim Kopieren)</td></tr>
								<tr><td><b>VOL</b></td><td>Laufwerksname wird angezeigt</td></tr>
							</table>

							<table border="0" cellpadding="0" cellspacing="0" width="100%">
								<tr><td style="width:200px;">Ein-/Ausgabespezifikationen:</td><td><b>con</b>(Terminal),<b>prn</b>od.<b>lpt1</b>(Drucker),<b>com1</b>,<b>com2</b>,... (RS232)</td></tr>
								<tr><td>Umlenkung der Ein- oder Ausgabe:</td><td>&gt;prn Ausgabe auf den Drucker<br />&gt;<i>datei</i>Ausgabe auf eine Datei<br />&gt;&gt;<i>datei</i>Ausgabe an eine Datei anh&auml;ngen<br />&lt;<i>datei</i>Eingabe von einer Datei<br />&gt;nul Ausgabe unterdr&uuml;cken</td></tr>
								<tr><td>autoexec.bat (auf oberster Ebene!)</td><td>wird beim Programmstart ausgef&uuml;hrt</td></tr>
							</table>
							<hr />
							<h3>weitere Kommandos:</h3>
							<table border="0" cellpadding="0" cellspacing="0" width="100%">
								<tr><td style="width:200px;"><b>APPEND</b></td><td>Suchpfad f&uuml;r Hilfsdateien (au&szlig;er BAT, COM, EXE)</td></tr>
								<tr><td><b>ASSIGN</b></td><td>Laufwerk zuweisen;<tt>ASSIGN A=C</tt>(Umleitung auf C:)</td></tr>
								<tr><td><b>ATTRIB</b></td><td>Lese/Schreibschutz<br />ATTRIB [+R -R] [+A -A] [<i>Pfad</i>]<i>name</i>[<i>.ext</i>];<br />+R nur lesen (Schreib-/L&ouml;schschutz),<br />+A &Auml;nderungsattribut setzen.</td></tr>
								<tr><td><b>BACKUP</b></td><td>BACKUP d:[<i>Pfad</i>][<i>Datei</i>[<i>.erw</i>]]<i>d</i>: [/S][/M][/A][/D:<i>tt.mm.jj</i>]<br />Sicherungskopie im Backup-Format erstellen</td></tr>
								<tr><td><b>BREAK</b></td><td>[ON OFF] Reaktion auf Ctrl-Break</td></tr>
								<tr><td><b>COMMAND</b></td><td>COMMAND [<i>d</i>:][<i>Pfad</i>] [/P][/C<i>zeichenfolge</i>][/E:<i>xxxxx</i>]<br />Aufruf eines neuen Befehlsinterpretierers</td></tr>
								<tr><td><b>COMP</b></td><td>COMP<i>datei1 datei2</i>vergleicht zwei Dateien</td></tr>
								<tr><td><b>CTTY</b></td><td><tt>CTTY com1</tt><br />Umlenkung der Standardein- und -ausgabe (von CON)</td></tr>
								<tr><td><b>DISKCOMP</b></td><td>vergleicht zwei Disketten</td></tr>
								<tr><td><b>DISKCOPY</b></td><td>DISKCOPY<i>d</i>:<i>d</i>:<br />kopiert vollst&auml;ndige Disketten</td></tr>
								<tr><td><b>EXE2BIN</b></td><td>EXE2BIN<i>dat1 dat2</i><br />(Umwandlung von .exe-Dateien in .com oder .bin)</td></tr>
								<tr><td><b>GRAFTABL</b></td><td>(l&auml;dt Tab. mit zus&auml;tzl. Zeichendaten f. Grafikmod.)</td></tr>
								<tr><td><b>GRAPHICS</b></td><td>zum Ausdrucken von Grafik</td></tr>
								<tr><td><b>JOIN</b></td><td>(logische Verkn&uuml;pfung zweier Verzeichnisse)</td></tr>
								<tr><td><b>KEYB GR</b></td><td>deutsche Tastaturbelegung (bis DOS 3.2: KEYBGR)</td></tr>
								<tr><td><b>LABEL</b></td><td><tt>label a:</tt><i>name</i><br />(Erstellen oder &Auml;ndern von Disketten-Namen)</td></tr>
								<tr><td><b>MODE</b></td><td>(Betriebsmodus f&uuml;r Drucker und asynchrone Daten&uuml;bertragung, s.u.)</td></tr>
								<tr><td><b>RECOVER</b></td><td>Wiederherstellung von Dateien auf Diskette mit defektem Sektor</td></tr>
								<tr><td><b>REPLACE</b></td><td>REPLACE [<i>Pfad</i>]<i>name</i>[<i>Pfad</i>] [<i>Optionen</i>] ersetzt Dateien</td></tr>
								<tr><td><b>RESTORE</b></td><td>RESTORE<i>d</i>: [<i>d</i>:][<i>Pfad</i>][<i>Datei</i>] [/S][/P]<br />Zur&uuml;ckspeichern von BACKUPs</td></tr>
								<tr><td><b>SELECT</b></td><td>SELECT [[A:]<i>d</i>:[<i>Pfad</i>]]<i>xxxyy</i><br />installiert DOS auf neuer (!) Platte</td></tr>
								<tr><td><b>SHARE</b></td><td>Unterst&uuml;tzung f&uuml;r gemeinsamen Dateizugriff</td></tr>
								<tr><td><b>SUBST</b></td><td>SUBST<i>d</i>:<i>d</i>:Pfad<br />Verwendung eines Laufwerkbuchstabens f&uuml;r einen Pfad</td></tr>
								<tr><td><b>SYS</b></td><td>SYS<i>d</i>: Kopieren der System-Dateien auf neue Diskette</td></tr>
								<tr><td><b>TREE</b></td><td>TREE [<i>d</i>:] [/F] Auflistung der Unterverzeichnisse (mit Dateien)</td></tr>
								<tr><td><b>XCOPY</b></td><td>kopiert Gruppen von Dateien (<tt>XCOPY C:\vrz A: [/M /S]</tt>)</td></tr>
								<tr><td><b>EDLIN</b></td><td>Zeileneditor (praktisch ersetzt durch<b>EDIT</b>)</td></tr>
							</table>
						</div>

						<div id="xtabs-3">
							<h2>Befehle in Batch-Dateien</h2>
							<p>In Batch-Dateien (*.BAT): ECHO, FOR, GOTO, IF, PAUSE, REM, SHIFT.</p>
							<table border="0" cellpadding="0" cellspacing="0" width="100%">
								<tr><td style="width:200px;">Parameter %1 ... %9</td><td>(Kommandozeilen-Parameter; %<i>name</i>% bezeichnet die in<i>name</i>gespeicherte Zeichenkette).</td></tr>
								<tr><td><b>ECHO</b></td><td>ECHO [ON OFF] (<i>text</i>) Textausgabe auf Bildschirm; &lt; | &gt; werden interpretiert (ggf. "&lt;" benutzen)</td></tr>
								<tr><td><b>FOR</b></td><td>FOR %%<i>variable</i>IN (<i>satz</i>) DO<i>befehl</i></td></tr>
								<tr><td><b>GOTO</b></td><td>GOTO<i>marke</i>(Sprungbefehl)<br />:<i>marke</i>(die Sprungadresse [mit Doppelpunkt] mu&szlig; als separate Zeile stehen)</td></tr>
								<tr><td><b>IF</b></td><td>IF<i>bedingung befehl</i><br />(bedingte Anweisung, z.B.:<br /><tt>IF [NOT] EXIST [Pfad]name.ext ...</tt>[Datei existent?],<br /><tt>IF ERRORLEVEL 1 ...</tt>[Programmabbruch],<br /><tt>IF %1==name ...</tt>[&Uuml;bergabeparameter = name?],<br /><tt>IF x%variable%==xParameter ...</tt>)</td></tr>
								<tr><td><b>PAUSE</b></td><td>wartet auf das Dr&uuml;cken einer Taste</td></tr>
								<tr><td><b>SET</b></td><td>SET<i>variable</i>=<i>parameter</i>(setzt Parameter; Zuweisung)</td></tr>
								<tr><td><b>SHIFT</b></td><td>(dient zur Verwendung von mehr als 10 Parametern)</td></tr>
							</table>
							<p>Beliebige DOS-Befehle und Programme k&ouml;nnen aus Batch-Dateien aufgerufen werden.</p>
							<table border="0" cellpadding="0" cellspacing="0" width="100%">
								<tr><td style="width:200px;">"Joker":</td><td>Abk&uuml;rzung (bei DIR, DEL, COPY)<br /><b>?</b>steht f&uuml;r ein Zeichen,<br /><b>*</b>steht f&uuml;r mehrere Zeichen.<br />(Vorsicht: DEL *.* l&ouml;scht das gesamte Verzeichnis!)</td></tr>
								<tr><td><b>MODE</b></td><td><tt>MODE LPT1:80,6[,P]</tt>Drucker-Einstellung (80 Zeich./Zl., 6 cpi)<br /><tt>MODE LPT1=COM1</tt>Druckausgabe &uuml;ber COM1 lenken<br /><tt>MODE COMn[:]baud[,[parit&auml;t][,[datenbits][,[stopbits][,P]]]]</tt>P:Wdh.<br /><tt>MODE [n][,[m][,T]] 40,80,BW40,BW80,CO40,CO80,MONO</tt>(Bildschirm)</td></tr>
							</table>

							<hr />
							<h3>spezielle Tastenbefehle (DOS-Ebene)</h3>
							<table border="0" cellpadding="0" cellspacing="0" width="100%">
								<tr><td style="width:200px;">Pause</td><td>stoppt die Bildschirmausgabe vor&uuml;bergehend; weiter mit (fast) beliebigem Tastendruck</td></tr>
								<tr><td>Strg-Untbr (Ctrl-Break)</td><td>Abbruch eines Befehls</td></tr>
								<tr><td>Druck (Shift-PrintScr)</td><td>druckt Bildschirm-"Hardcopy" [Text, bzw. GRAPHICS (s.o.), nicht &uuml;ber Netz]</td></tr>
								<tr><td>ESC</td><td>Eingabe l&ouml;schen</td></tr>
								<tr><td>Alt-Strg-Entf (Alt-Ctrl-Delete)</td><td>Rechner neu "booten"</td></tr>
								<tr><td>Alt-Strg-F1</td><td>(bei KEYB GR) umschalten auf US-Tastatur</td></tr>
								<tr><td>Alt-Strg-F2</td><td>(bei KEYB GR) umschalten auf deutsche Tastatur</td></tr>
								<tr><td>F1</td><td>wiederholt letztes Kommando zeichenweise</td></tr>
								<tr><td>F3</td><td>wiederholt letztes Kommando</td></tr>
								<tr><td>F5</td><td>(@ Eingabe l&ouml;schen)</td></tr>
								<tr><td>F6</td><td>Strg-Z (^Z) Dateiende</td></tr>
								<tr><td>F7</td><td>Strg-@ (ASCII-Zeichen Nr. 0)</td></tr>
								<tr><td>Strg-C</td><td>Abbruch</td></tr>
								<tr><td>Strg-P</td><td>Druckerausgabe ein-/ausschalten</td></tr>
								<tr><td>Strg-S</td><td>stoppt Bildschirmausgabe</td></tr>
							</table>
						</div>
					</div>
				</div>
				<div class="clear"></div>
				<hr>
				<div>
					<iframe class="iFrameModule" width="100%" height="100%" src="<?php echo PATH_PHPTERM; ?>"><a href="<?php echo PATH_PHPTERM; ?>">PHP-Term</a></iframe>
				</div>
			</div>
		</div>
		<div class="clear"></div>
	</div>
	<div class="clear"></div>
</div>

<script language="javascript" type="text/javascript">
	$(document).ready(function() {
		$(function() {
			$( "#datepicker" ).datepicker();
		});
		colorRowMouseOver('.displayOrders tbody tr');
		$(function() {
			$('#tabs').tabs();
		});
	});
</script>

<?php require_once('inc/footerHTML.inc.php'); ?>