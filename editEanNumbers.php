<?php
	require_once('inc/requires.inc.php');

	if(!$arrGetUserRights["editCustomers"] && !$arrGetUserRights["displayCustomers"]) {
		header('location: ' . PAGE_EXIT_LOCATION);
		exit;
	}

	$jswindowMessage = "";
	$warningMessage = '';
	$errorMessage = '';
	$successMessage = '';
	$infoMessage = '';

	$dbConnection = new DB_Connection();
	$db_open = $dbConnection->db_connect();

	function showEanDatas($arrEanData, $tabID){
		global $arrGetUserRights;
		$content = '';
		if(!empty($arrEanData)){
			if($arrGetUserRights["adminArea"]){
				$content .= '<form name="formEditEanNumbers_' . $tabID . '" method="post" action="' . $_SERVER["PHP_SELF"]. '#' . $tabID . '">';
			}

			$countRow = 0;
			$content .= '<table border="0" width="100%" cellpadding="0" cellspacing="0" class="displayOrders">';
			$content .= '<tr>';
			$content .= '<th style="width:15px;">#</th>';
			$content .= '<th style="width:120px;">EAN-Nummer</th>';
			$content .= '<th>Artikel-Nr.</th>';
			$content .= '<th>Artikel-Name</th>';
			$content .= '<th style="width:60px;">Info</th>';
			$content .= '</tr>';

			$thisEanCount = 0;

			foreach($arrEanData as $thisEanDatasKey => $thisEanDatasValue){

				$thisEanBasicNumber = substr($thisEanDatasValue["codesGtinNumber"], 0, -1);
				if($thisEanCount != $thisEanBasicNumber){
					if($thisEanCount != $thisEanBasicNumber - 1 && $countRow > 0){
						$content .= '<tr><td colspan="5"><hr /></td></tr>';
					}
					$thisEanCount = $thisEanBasicNumber;
				}
				if($countRow%2 == 0){ $rowClass = 'row0'; }
				else { $rowClass = 'row1'; }

				if($thisEanDatasValue["codesGtinProductNumber"] != ""){
					$rowClass = 'row3';
				}

				$content .= '<tr class="'.$rowClass.'">';

				$content .= '<td style="text-align:right;">';
				$content .= '<b>' . ($countRow + 1) . '.</b>';
				$content .= '</td>';

				// codesGtinID
				// codesGtinType
				$content .= '<td>';
				$content .= ($thisEanDatasValue["codesGtinNumber"]);
				if($arrGetUserRights["adminArea"]){
					$content .= '<span class="editEanArea" style="display:none;">';
					$content .= '<input type="hidden" name="editCodesGtinNumber[' . $thisEanDatasValue["codesGtinNumber"] . ']" value="' . htmlentities($thisEanDatasValue["codesGtinNumber"]) . '" />';
					$content .= '<input type="hidden" name="editCodesGtinID[' . $thisEanDatasValue["codesGtinNumber"] . ']" value="' . htmlentities($thisEanDatasValue["codesGtinID"]) . '" />';
					$content .= '</span>';
				}
				$content .= '</td>';

				$content .= '<td style="white-space:nowrap;">';
				$content .= ($thisEanDatasValue["codesGtinProductNumber"]) . '';
				if($arrGetUserRights["adminArea"]){
					$content .= '<br />';
					$content .= '<span class="editEanArea" style="display:none;">';
					$content .= '<input type="text" name="editCodesGtinProductNumber[' . $thisEanDatasValue["codesGtinNumber"] . ']" class="inputField_260" value="' . htmlentities(utf8_decode($thisEanDatasValue["codesGtinProductNumber"])) . '" />';
					$content .= '</span>';
				}
				$content .= '</td>';

				$content .= '<td>';
				$content .= ($thisEanDatasValue["codesGtinProductName"]) . '';
				if($arrGetUserRights["adminArea"]){
					$content .= '<br />';
					$content .= '<span class="editEanArea" style="display:none;">';
					$content .= '<input type="text" name="editCodesGtinProductName[' . $thisEanDatasValue["codesGtinNumber"] . ']" class="inputField_510" value="' . htmlentities(utf8_decode($thisEanDatasValue["codesGtinProductName"])) . '" />';
					$content .= '</span>';
				}
				$content .= '</td>';

				$content .= '<td>';

				if($arrGetUserRights["adminArea"]){
					$content .= '<span class="toolItem">';
					$content .= '<img src="layout/icons/iconEdit.gif" class="buttonEditEanItem" width="14" height="14" alt="EAN bearbeiten" title="EAN bearbeiten" />';
					$content .= '</span>';

					$content .= '<span class="editEanArea" style="display:none;">';

					$content .= '<span class="toolItem">';
					$content .= '<img src="layout/icons/iconCancel.gif" class="buttonCancelEditEanItem" width="14" height="14" alt="EAN abbrechen" title="EAN abbrechen" />';
					$content .= '</span>';

					$content .= '<span class="toolItem">';
					#$content .= '<img src="layout/icons/iconSave.png" class="buttonStoreEanItem" width="14" height="14" alt="EAN speichern" title="EAN speichern" />';
					$content .= '<input type="image" src="layout/icons/iconSave.png" name="storeEanItem[' . $thisEanDatasValue["codesGtinNumber"] . ']" class="buttonStoreEanItem" style="width:14px;height14px;" alt="EAN speichern" title="EAN speichern" />';
					$content .= '</span>';

					$content .= '</span>';
				}

				$content .= '</td>';

				$content .= '</tr>';
				$countRow++;
			}
			$content .= '</table>';

			if($arrGetUserRights["adminArea"]){
				$content .= '</form>';
			}
		}
		echo $content;
	}

	// BOF READ USER DATAS
		$userDatas = getUserDatas();
	// EOF READ USER DATAS

	// BOF STORE EAN CODE
		if(!empty($_POST["storeEanItem"])){
			foreach($_POST["storeEanItem"] as $thisEanItemKey => $thisEanItemValue){
				if($_POST["editCodesGtinNumber"][$thisEanItemKey] != ""){
					$_xxx_sql = "
							REPLACE INTO `" . TABLE_GTIN_CODES . "` (
												`codesGtinID`,
												`codesGtinType`,
												`codesGtinNumber`,
												`codesGtinProductName`,
												`codesGtinProductNumber`
											)
											VALUES (
												'" . $_POST["editCodesGtinID"][$thisEanItemKey] . "',
												'EAN',
												'" . $_POST["editCodesGtinNumber"][$thisEanItemKey] . "',
												'" . $_POST["editCodesGtinProductName"][$thisEanItemKey] . "',
												'" . $_POST["editCodesGtinProductNumber"][$thisEanItemKey] . "'
											)
						";
					$sql = "
							UPDATE `" . TABLE_GTIN_CODES . "`
								SET
									`codesGtinType` = 'EAN',
									`codesGtinProductName` = '" . $_POST["editCodesGtinProductName"][$thisEanItemKey] . "',
									`codesGtinProductNumber` = '" . $_POST["editCodesGtinProductNumber"][$thisEanItemKey] . "'
								WHERE 1
									AND `codesGtinNumber` = '" . $_POST["editCodesGtinNumber"][$thisEanItemKey] . "'
						";
				#dd('sql');
					$rs = $dbConnection->db_query($sql);

					if($rs){
						$successMessage .= 'Die EAN-Daten wurden gespeichert.' . '<br />';
					}
					else {
						$errorMessage .= 'Die EAN-Daten konnten nicht gespeichert werden.' . '<br />';
					}
				}
			}
		}
	// EOF STORE EAN CODE

	// BOF GET ALL EAN CODES
		$where = "";
		if($_REQUEST["searchProduct"] != ""){
			$where = " AND (
					`codesGtinProductName` LIKE '%" . $_REQUEST["searchProduct"] . "%'
					OR
					`codesGtinProductNumber` LIKE '%" . $_REQUEST["searchProduct"] . "%'
				)
			";
		}
		else if($_REQUEST["searchEanNumber"] != ""){
			$where = " AND `codesGtinNumber` LIKE '%" . $_REQUEST["searchEanNumber"] . "%' ";
		}
		else if($_REQUEST["searchWord"] != ""){
			$where = " AND (
					`codesGtinProductName` LIKE '%" . $_REQUEST["searchWord"] . "%'
					OR
					`codesGtinProductNumber` LIKE '%" . $_REQUEST["searchWord"] . "%'
					OR
					`codesGtinNumber` LIKE '%" . $_REQUEST["searchWord"] . "%'
				)
			";
		}

		$sql = "SELECT
						`codesGtinID`,
						`codesGtinType`,
						`codesGtinNumber`,
						`codesGtinProductName`,
						`codesGtinProductNumber`
					FROM `" . TABLE_GTIN_CODES . "`

					WHERE 1
						" . $where . "
					ORDER BY `codesGtinNumber`
			";

		$rs = $dbConnection->db_query($sql);
		$arrAllEanDatas = array();
		$arrUsedEanDatas = array();
		while($ds = mysqli_fetch_assoc($rs)){
			$arrEanDatas = array();
			foreach(array_keys($ds) As $field){
				$arrAllEanDatas[$ds["codesGtinID"]][$field] = $ds[$field];
			}
			if($ds["codesGtinProductNumber"]){
				$arrUsedEanDatas[$ds["codesGtinID"]] = $arrAllEanDatas[$ds["codesGtinID"]];
			}
		}
	// EOF GET ALL EAN CODES
?>
<?php
	require_once('inc/headerHTML.inc.php');
	$thisTitle = "EAN-Nummern (GLN) verwalten";
	$headerHTML = preg_replace("/{###TITLE###}/", strip_tags($thisTitle), $headerHTML);
	echo $headerHTML;

	require_once(FILE_MENUE_TOP);
?>
<div id="mainArea">
	<div id="mainContent">
		<?php require_once(FILE_MENUE_SIDEBAR); ?>
		<div id="contentArea">
			<div id="contentAreaElements">
				<h1><?php if(SHOW_PAGE_TITLE_ICONS) { echo '<img src="' . PATH_ICONS_MENUE_TITLES . 'barcode.png" alt="" />'; } ?> <?php echo $thisTitle; ?></h1>

				<div class="contentDisplay">
					<?php displayMessages(); ?>
					<table cellpadding="0" cellspacing="0">
						<tr><td><b>Globale Lokationsnummer (GLN)</b>:</td><td>42 60298 79 000 7</td></tr>
						<tr><td><b>Ziffern 1 - 9</b>:</td><td>Unver&auml;nderliche Basisnummer</td></tr>
						<tr><td><b>Ziffern 10 - 12</b>:</td><td>Individueller Nummernteil</td></tr>
						<tr><td><b>Ziffer 13</b>:</td><td>Pr&uuml;fziffer</td></tr>
					</table>

					<div id="searchFilterArea">
						<form name="formFilterSearch" method="get" action="<?php echo $_SERVER["PHP_SELF"]; ?>">
							<table border="0" cellpadding="0" cellspacing="0" class="searchFilterContent">
								<tr>
									<td>
										<label for="searchProduct">Artikel:</label>
										<input type="text" name="searchProduct" id="searchProduct" class="inputField_160" value="" />
									</td>
									<td>
										<label for="searchEanNumber">EAN/GLN-Nr:</label>
										<input type="text" name="searchEanNumber" id="searchEanNumber" class="inputField_160" value="" />
									</td>

									<td>
										<label for="searchWord">Suchbegriff:</label>
										<input type="text" name="searchWord" id="searchWord" class="inputField_100" />
									</td>
									<td>
										<input type="hidden" name="xxxeditID" id="xxxeditID" value="" />
										<input type="submit" name="submitSearch" class="inputButton0" value="Suchen" />
									</td>
								</tr>
							</table>
						</form>
					</div>

					<div id="tabs">
						<div class="adminEditArea">
							<ul>
								<li><a href="#tabs-1">Alle EAN-Nummern</a></li>
								<li><a href="#tabs-2">Verwendete EAN-Nummern</a></li>
							</ul>
						</div>

						<div id="tabs-1">
							<?php
								echo '<p>' . count($arrAllEanDatas). ' Eintr&auml;ge</p>';
								if(!empty($arrAllEanDatas)){
									showEanDatas($arrAllEanDatas, 'tabs-1');
								}
								else {
									$infoMessage .= 'Es sind keine Nummern vorhanden.' . '<br />' ;
								}
								displayMessages();
							?>
						</div>

						<div id="tabs-2">
							<?php
								echo '<p>' . count($arrUsedEanDatas). ' Eintr&auml;ge</p>';
								if(!empty($arrUsedEanDatas)){
									showEanDatas($arrUsedEanDatas, 'tabs-2');
								}
								else {
									$infoMessage .= 'Es sind keine Nummern in Verwendung.' . '<br />' ;
								}
								displayMessages();
							?>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<script language="javascript" type="text/javascript">
	$(document).ready(function() {

		setFocus('formFilterSearch', 'searchWord');
		colorRowMouseOver('.displayOrders tbody tr');
		$(function() {
			$.datepicker.setDefaults($.datepicker.regional[""]);
			$('#displayReportsDateFrom').datepicker($.datepicker.regional["de"]);
			$('#displayReportsDateTo').datepicker($.datepicker.regional["de"]);
		});
		$(function() {
			$('#tabs').tabs();
		});

		$('.buttonToggleSidebarMenue').click(function() {
			$('#menueSidebarToggleContent').toggle();
		});

		$('.buttonCancelEditEanItem').live('click', function(){
			$(this).parent().parent().parent().parent().find('.editEanArea').css('display', 'none');
			$(this).parent().parent().parent().parent().find('.buttonEditEanItem').css('display', 'inline');
			$(this).parent().parent().parent().parent().find('td').css('border', '');
		});

		$('.buttonEditEanItem').live('click', function(){
			$(this).parent().parent().parent().parent().find('.editEanArea').css('display', 'none');
			$(this).parent().parent().parent().parent().find('.editEanArea input').attr('disabled', 'true');
			$(this).parent().parent().parent().parent().find('.buttonEditEanItem').css('display', 'inline');
			$(this).parent().parent().parent().parent().find('td').css('border', '');
			$(this).parent().parent().parent().find('.editEanArea').css('display', 'inline');
			$(this).parent().parent().parent().find('.editEanArea input').removeAttr('disabled');
			$(this).parent().parent().parent().find('.buttonEditEanItem').css('display', 'none');
			$(this).parent().parent().parent().find('td').css('border', '1px solid #FF0000');
		});
	});
</script>

<?php require_once('inc/footerHTML.inc.php'); ?>