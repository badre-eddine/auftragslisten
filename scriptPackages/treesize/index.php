<?php
	$Seite = 'TreeSize for PHP-Webspace';
	
	if(!defined("SET_DATE_DEFAULT_TIMEZONE")) { DEFINE('SET_DATE_DEFAULT_TIMEZONE', 'Europe/Berlin'); }
	if(!defined("LC_ALL")) { DEFINE('LC_ALL', '"de_DE@euro", "de_DE", "de", "ge"'); }
	
	date_default_timezone_set(SET_DATE_DEFAULT_TIMEZONE);
	setlocale(LC_ALL, constant("LC_ALL"));
	setlocale(LC_TIME, constant("LC_ALL"));
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
        "http://www.w3.org/TR/html4/loose.dtd">
<HTML lang="de">
<head>
<title><?php echo $Seite; ?></title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">

<style type="text/css">
html {font-size:100.001%;}
body {font-size:80%; font-family: Arial, Helvetica, Tahoma, Verdana, Sans-Serif; background-color:white;}
h1 {font-size:130%;}
h2 {font-size:110%;}
small {font-size:80%;}
table {font-size:100%; border-collapse: collapse;}
td,th {text-align:left; white-space: nowrap; padding: 0 1em; border: 0px solid #ddd;}
td.zahl {text-align:right; width:8em;}
a, a:link, a:visited {COLOR: rgb(0,0,138);}
a:hover {COLOR: rgb(0,0,238); text-decoration: none;}
.silver {color:silver;}
.silver a, .silver a:link, .silver a:visited {COLOR: rgb(90,90,138);}
.silver a:hover {COLOR: rgb(0,0,238); text-decoration: none;}
</style>

</head>
<body>
<?php
if($_GET['directory'] > '') {$directory = $_SERVER['DOCUMENT_ROOT'].'/'.$_GET['directory'];} 
else {$directory = $_SERVER['DOCUMENT_ROOT'];}
echo $directory;

function dirsize($path)
{
	$s = 0;
	$result[$path] = 0;
	$handle = @opendir($path);
	if ($handle)
	{
		while (false !== ($file = readdir($handle)))
		{
			if ($file != "." && $file != "..")
			{
				$name = $path . "/" . $file;
				if (is_dir($name))
				{
					$ar = dirsize($name);
					while (list($key, $value) = each ($ar)) {$s++; $result[$key] = $value;}
				}
				else {$result[$path] += @filesize($name);}
			}
		}
		closedir($handle);
	}
	return $result;
}
$data = dirsize($directory);
$data1 = $data;

while (list($key, $value) = each ($data))
{
	if (!preg_match("'\/'",str_replace($directory.'/','',$key))) 
	{
		$verzeichnis[str_replace($directory.'/','',$key)] = $value;
		$dir1 = str_replace($directory.'/','',$key);
		while (list($key1, $value1) = each ($data1))
		{
			$key2 = str_replace($directory.'/','',$key1);
			$subdir = explode('/',$key2);
			if (preg_match("'\/'",str_replace($directory.'/','',$key1)) AND $dir1 == $subdir[0])
			{
				$verzeichnis[$subdir[0]] += $value1;
			}
		}
		reset($data1);
	}
}
@reset($data);
@arsort($verzeichnis);
@reset($verzeichnis);
$handle1 = @opendir($directory);
if ($handle1)
{
	while (false !== ($file = readdir($handle1)))
	{
		if ($file != "." && $file != "..")
		{
			$name = $directory . "/" . $file;
			if (!is_dir($name)) {$Dateien[$file] = @filesize($name);}
		}
	}
	closedir($handle1);
}
@arsort($Dateien);
@reset($Dateien);
?>
<div class="silver">
<h1 id="oben"><?php echo $Seite; ?><small> &copy; <a href="http://rtol.de">RTOL.de</a></small></h1>
<h2><?php echo str_replace('//','/',$directory); ?></h2>
</div>
<table>
<?php
if ($directory != $_SERVER['DOCUMENT_ROOT'])
{
	$root = explode('/',$_SERVER['DOCUMENT_ROOT']);
	$directory_neu1 = explode('/',str_replace($_SERVER['DOCUMENT_ROOT'].'/','',$directory));
	$backdir = count($directory_neu1);
	for($i=0;$i < $backdir - 1;$i++) {$directory_neu .= '/'.$directory_neu1[$i];}
	if($backdir > 0) 
	{
?>
	<tr>
		<td class="zahl">zur�ck nach:</td>
		<td><a href="<?php echo $_SERVER['PHP_SELF']; ?>?directory=<?php echo $directory_neu; ?>"><?php echo $root[count($root)-1]; ?><?php if($directory_neu > '') {echo $directory_neu;} ?></a></td>
	</tr>
<?php }} ?>
<?php while (list($key, $value) = @each ($verzeichnis)) {$Summe += $value; ?>
	<tr>
		<td class="zahl"><?php echo number_format($value / 1024, 3, ",", "."); ?> kB</td>
		<td><a href="<?php echo $_SERVER['PHP_SELF']; ?>?directory=<?php echo $_GET['directory']; ?>/<?php echo str_replace($_SERVER['DOCUMENT_ROOT'].'/','',$key); ?>">
		<?php echo str_replace($directory,'',$key); ?></a></td>
	</tr>
<?php } ?>
</table>
<table>
<?php while (list($key, $value) = @each ($Dateien)) {$Summe1 += $value; ?>
	<tr>
		<td class="zahl"><?php echo number_format($value / 1024, 3, ",", "."); ?> kB</td>
		<td><?php echo $key; ?></td>
	</tr>
<?php } ?>
</table>
<p>Summe Dateien: <?php echo number_format($Summe1 / 1024, 3, ",", "."); ?> kB<br>
Summe Verzeichnis: <b><?php echo number_format(($Summe + $Summe1) / 1024 / 1024, 3, ",", "."); ?> MB</b></p>
</body>
</html>
