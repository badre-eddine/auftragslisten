/**
 * move all ads dom element from src div to target div now use absolute position
 * to iframe banners
 */
function relocateAds(tgt,src) {
    try{
        //get elements
        var $s = $('#' + src);
        var $t = $('#' + tgt);
        //vars
        var last_pos = 0;
        var last_height = 0;

        //set initial pos and show
        $s.css({position:'absolute'}).show();                   

        /**
         * set interval to check window/components resize
         * on pos change or height change reset
         */
        setInterval(function(){
            var pos = $t.offset();
            if(pos !== last_pos ){
                last_pos = pos;
                $s.css(pos);
            }
            var height = $s.height();
            if(height !== last_height){
                last_height = height;
                $t.css({height:last_height});
            }
        },1000);                

    }catch(e){}

}