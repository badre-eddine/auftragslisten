if(!com) var com = {};
if(!com.jmadden) com.jmadden = {};
if(!com.jmadden.collapse) com.jmadden.collapse = {};

com.jmadden.collapse = {
	collCon: new Array(),
	setMouseListeners: function (){
		window.addEventListener("mouseup",com.jmadden.collapse.getTarget,false);
	},
	getTarget: function (e){
		if(window.content.getSelection()==""){//if no text is selected 
			//window.content.getSelection().anchorNode.nodeValue == "";window.onmouseup.preventDefault();
			if(window.content.document.title.indexOf("Source Chart for") != -1){
				var vrsc_isKeyPressed = (e.shiftKey) ? true : (e.ctrlKey) ? true : (e.altKey) ? true : false;
				var is_vrsClick = (e.type.toLowerCase() == "mouseup" && e.button != 2) ? true : false;
				
				var containerV = e.target;
				
				var thisVSCBorder=(containerV.style.cursor=='pointer' ? true : false);
				if(is_vrsClick && thisVSCBorder){
					if(containerV.childNodes[0].nodeType != 1){

						
						com.jmadden.collapse.collCon.push(containerV.innerHTML); //put html in array as new index
						containerV.innerHTML = "<input type=\"hidden\" value=\""+(com.jmadden.collapse.collCon.length-1)+"\">"; //COLLAPSE!!
						
						var VSC_newTop=this.getPos(containerV);
						
						if((VSC_newTop.top<window.content.document.body.scrollTop) || (VSC_newTop.top>(window.content.document.body.scrollTop+window.clientHeight))){
							window.content.document.body.scrollTop=VSC_newTop.top-e.clientY;
						}
					} 
					else {// end if NOT a form element, COLLAPSE OR form element is present, so EXPAND
						var hiddenValue = parseInt(containerV.childNodes[0].value); //get hidden field's value
						containerV.innerHTML = com.jmadden.collapse.collCon[hiddenValue]; //EXPAND!!
						com.jmadden.collapse.collCon[hiddenValue] = ""; //delete the array element's value
						window.content.getSelection().removeAllRanges();
					}
				} //end if thisVSCBorder	
			}
		} //end if text is selected
	}, //end function
	getPos: function (VSC_obj) {
		var VSC_output = new Object();
		var VSC_mytop=0, VSC_myleft=0;
		while(VSC_obj) {
			VSC_mytop+= VSC_obj.offsetTop;
			VSC_myleft+= VSC_obj.offsetLeft;
			VSC_obj= VSC_obj.offsetParent;
		}
		VSC_output.left = VSC_myleft;
		VSC_output.top = VSC_mytop;
		return VSC_output;
	}
};

window.addEventListener("load",com.jmadden.collapse.setMouseListeners,false);