<?php
	set_time_limit(0);
	require_once('config/configFiles.inc.php');
	require_once('config/config.inc.php');
	require_once('inc/functions.inc.php');

	if($_POST["generateNumber"] != "") {
		$arrGeneratedNumbers = generateNumber($_POST);
	}
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
			"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
	<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="de" lang="de">
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
		<title>EAN - Generierung</title>
		<meta name="keywords" content="" />
		<meta name="description" content="" />
		<meta name="robots" content="NOINDEX,NOFOLLOW" />
		<meta name="author" content="Thorsten Hoff" />
		<meta http-equiv="imagetoolbar" content="no" />
		<meta http-equiv="content-type" content="text/html; utf-8" />
		<meta http-equiv="Content-Script-Type" content="text/javascript" />
		<meta http-equiv="Content-Style-Type" content="text/css" />
		<meta name="revisit-after" content="1 days" />
		<meta http-equiv="content-language" content="de" />
		<meta http-equiv="cache-control" content="no-cache" />
		<link rel="shortcut icon" href="favicon.png" />
	
		<link rel="stylesheet" type="text/css" href="<?php echo PATH_CSS_LAYOUT; ?>" media="screen" charset="utf-8" />
		<script language="javascript" type="text/javascript" src="<?php echo PATH_JQUERY; ?>"></script>
		<script language="javascript" type="text/javascript" src="<?php echo PATH_JS_FUNCTIONS; ?>"></script>
	</head>

<body>
	<div id="pageArea">
		<div id="documentArea">
			<div id="headerMenueArea">
				<div id="headerMenueContent">
					<a href="<?php echo DOCUMENT_ROOT; ?>" class="menueActive">GLN-Prüfziffernrechner</a>
					<a href="<?php echo PATH_BARCODE_GENERATOR; ?>">Barcode-Generator</a>
					<a href="<?php echo PATH_BARCODE_GENERATOR_2; ?>">Barcode-Generator 2</a>
					<a href="<?php echo PATH_QRCODE_GENERATOR; ?>">QR-Code-Generator</a>
					<div class="clear"></div>
				</div>
			</div>
			<div id="contentArea">
				<div id="contentAreaElements">
					<h1>GLN- / EAN-Nummern</h1>
				
					<?php
						if(!empty($arrGeneratedNumbers)) {
							echo '<h2>Generierte EAN-Nummern</h2>';
							echo '<div class="scrollContent">';
								echo '<pre style="font-family: monospace !important;">';
								$countItems = 1;
								echo "\n" . str_repeat('-', 92) . "\n\t";
								foreach($arrGeneratedNumbers as $thisGeneratedNumber) {							
									// echo $thisGeneratedNumber . '<br />';
									echo substr($thisGeneratedNumber, 0, 2) . '<span class="numberSpacer"></span>' . substr($thisGeneratedNumber, 2, 5) . '<span class="numberSpacer"></span>' . substr($thisGeneratedNumber, 7, 5) . '<span class="numberSpacer"></span>' . substr($thisGeneratedNumber, 12, 1);
																	
									if($countItems%3 == 0) {
										echo "\n" . str_repeat('-', 92) . "\n\t";
									}
									else {
										echo "\t|\t";
									}
									$countItems++;
								}
								echo "\n" . str_repeat('-', 92) . "\n";
								echo '</pre>';
							echo '</div>';
						
							if($_POST["createBarCode"] == 1) {
								echo '<h2>Generierte Barcodes</h2>';
								echo '<div class="scrollContent">';
									$filenameBarcodeImage = PATH_STORE_GENEREATED_BARCODE_IMAGES.'{###GENERATED_NUMBER###}_{###BARCODE_TYPE###}_{###BARCODE_STYLE###}_{###BARCODE_WIDTH###}x{###BARCODE_HEIGHT###}.png';
									foreach($arrGeneratedNumbers as $thisGeneratedNumber) {
										$thisFilenameGeneratedBarcodeImage = preg_replace("/{###BARCODE_TYPE###}/", BARCODE_TYPE, preg_replace("/{###BARCODE_STYLE###}/", BARCODE_STYLE, preg_replace("/{###BARCODE_WIDTH###}/", $barCodeWidth, preg_replace("/{###BARCODE_HEIGHT###}/", $barCodeHeight, preg_replace("/{###GENERATED_NUMBER###}/", $thisGeneratedNumber, $filenameBarcodeImage)))));
										if(file_exists($thisFilenameGeneratedBarcodeImage)) {
											// unlink($thisFilenameGeneratedBarcodeImage);
											$thisGeneratedBarcodeImage = $thisFilenameGeneratedBarcodeImage;
										}
										else {										
											$thisGeneratedBarcodeImage = 'http://localhost'.DOCUMENT_ROOT.PATH_BARCODE_GENERATOR.'image.php?code='.$thisGeneratedNumber.'&style='.BARCODE_STYLE.'&type='.$barCodeType.'&width='.$barCodeWidth.'&height='.$barCodeHeight.'&xres='.$barCodeXres.'&font='.$barCodeFont;
											$resultCopy = copy($thisGeneratedBarcodeImage, $thisFilenameGeneratedBarcodeImage);
										}
										echo '<img src="' . $thisGeneratedBarcodeImage . '" width="'.$barCodeWidth.'" height="'.$barCodeHeight.'" alt="' . $thisGeneratedNumber . '" title="' . $thisGeneratedNumber . '" />';
										clearstatcache();
										flush();
									}
								echo '</div>';
							}
						
							if($_POST["createQrCode"] == 1) {							
								echo '<h2>Generierte QR-Codes</h2>';
								echo '<div class="scrollContent">';
								
									$filenameGeneratedQrImage = PATH_STORE_GENEREATED_QRCODE_IMAGES.'{###GENERATED_NUMBER###}_{###QRCODE_ECC###}_{###QRCODE_SIZE###}.png';
									foreach($arrGeneratedNumbers as $thisGeneratedNumber) {
										$thisFilenameGeneratedQrImage = preg_replace("/{###QRCODE_ECC###}/", $errorCorrectionLevel, preg_replace("/{###QRCODE_SIZE###}/", $matrixPointSize, preg_replace("/{###GENERATED_NUMBER###}/", $thisGeneratedNumber, $filenameGeneratedQrImage)));									
										if(file_exists($thisFilenameGeneratedQrImage)) {
											// unlink($thisFilenameGeneratedQrImage);
										}
										else {
											require_once(PATH_QRCODE_GENERATOR . 'qrlib.php');							
											$matrixPointSize = min(max((int)$matrixPointSize, 1), 10);									
											QRcode::png($thisGeneratedNumber, $thisFilenameGeneratedQrImage, $errorCorrectionLevel, $matrixPointSize, 2); 
											// $resultCopy = copy($thisGeneratedBarcodeImage, $thisFilenameGeneratedQrImage);
										}									
										echo '<img src="' . $thisFilenameGeneratedQrImage . '" width="'.($matrixPointSize * QRCODE_SIZE_STEP).'" height="'.($matrixPointSize * QRCODE_SIZE_STEP).'" alt="' . $thisGeneratedNumber . '" title="' . $thisGeneratedNumber . '" />';
										clearstatcache();
										flush();
									}
								echo '</div>';
							}
						}				
					?>
	   
					<h2>GTIN-13- bzw. GLN-Prüfziffernrechner</h2>
					<form name="formGenerateEAN" method="post" action="<?php echo $_SERVER["PHP_SELF"]; ?>">
						<p>
							<b>Unsere Unveränderliche Basisnummer: </b><?php echo substr(EAN_BASIC_CODE, 0, 2) . '<span class="numberSpacer"></span>' . substr(EAN_BASIC_CODE, 2, 5) . '<span class="numberSpacer"></span>' . substr(EAN_BASIC_CODE, 7, 5) . '<span class="numberSpacer"></span>' . substr(EAN_BASIC_CODE, 12, 1)?>
						</p>
						<table border="0" cellpadding="0" cellspacing="0" width="100%">
							<tr>
								<td><label for="countryNumber">Ländernummer</label></td>
								<td><label for="companyNumber">Betriebsnummer</label></td>
								<td><label for="productNumber">Artikelnummer</label></td>
								<!--
								<td><label for="checkNumber">Prüfziffer</label></td>
								-->
								<td><label for="createQrCode">QR-Code(s)</label></td>
								<td><label for="createBarCode">Barcode(s)</label></td>
								<td></td>
								<td></td>
							</tr>
							<tr>
								<td>
									<select name="countryNumber" id="countryNumber" class="inputSelect">
										<?php
											if(!empty($arrCountryNumbers)){
												foreach($arrCountryNumbers as $thisKey => $thisValue) {
													$selected = '';
													if($_POST["countryNumber"] != "") {
														if($_POST["countryNumber"] == $thisValue["number"]) {
															$selected = ' selected="true" ';
														}
													}
													else {
														if(substr(EAN_BASIC_CODE, 0, 2) == $thisValue["number"]) {
															$selected = ' selected="true" ';
														}
													}
													echo '<option value="' . $thisValue["number"] . '" ' . $selected . ' >' . $thisValue["number"] . ' ' . htmlentities($thisValue["country"]) . '</option>';
												}
											}
										?>
									</select>							
								</td>
								<td><input name="companyNumber" id="companyNumber" class="inputField" value="<?php echo substr(EAN_BASIC_CODE, 2, 5); ?>" readonly /></td>
							
								<?php
									if($_POST["productNumber"] != '') {
										$thisProductNumber = $_POST["productNumber"];
									}
									else {
										$thisProductNumber = substr(EAN_BASIC_CODE, 7, strlen(EAN_BASIC_CODE));
									}								
								?>
								<td><input name="productNumber" id="productNumber" class="inputField" maxlength="5" value="<?php echo $thisProductNumber; ?>" /></td>
								<!--
								<td><input name="checkNumber" id="checkNumber" class="inputField" maxlength="1" value="<?php echo $checkNumber; ?>" readonly /></td>
								-->
								<td>
									<input type="checkbox" name="createQrCode" class="inputCheck" id="createQrCode" value="1" />
									<img src="layout/iconParameter.gif" width="16" height="16" class="buttonAdditionalFormElements" alt="QR-Code-Parameter" title="QR-Code-Parameter setzen" />
									<div id="formQrCodeArea" class="additionalFormElements">
										<table border="0" cellpadding="0" cellspacing="0">
											<tr>
												<td><b>ECC:</b></td>
												<td>
													<select name="qrCodeLevel">
														<option value="L" <?php if($errorCorrectionLevel=='L'){ echo ' selected'; } ?> >L - smallest</option>
														<option value="M" <?php if($errorCorrectionLevel=='M'){ echo ' selected'; } ?> >M</option>
														<option value="Q" <?php if($errorCorrectionLevel=='Q'){ echo ' selected'; } ?> >Q</option>
														<option value="H" <?php if($errorCorrectionLevel=='H'){ echo ' selected'; } ?> >H - best</option>
													</select>
												</td>
											</tr>
											<tr>
												<td><b>Size:</b></td>
												<td>
													<select name="qrCodeSize">'
														<?php
															for($i=1;$i<=10;$i++) {
																echo '<option value="'.$i.'"'.(($matrixPointSize==$i)?' selected':'').'>'.$i.'</option>';
															}
														?>
													</select>
												</td>
											</tr>
										</table>
									</div>
								</td>
								<td>
									<input type="checkbox" name="createBarCode" class="inputCheck" id="createBarCode" value="1" />
									<img src="layout/iconParameter.gif" width="16" height="16" class="buttonAdditionalFormElements" alt="Barcode-Parameter" title="Barcode-Parameter setzen" />
									<div id="formBarCodeArea" class="additionalFormElements">
										<table border="0" cellpadding="0" cellspacing="0">
											<tr>
												<td><b>Type:</b></td>
												<td>
													<select name="barCodeType" size="1" class="inputSelect">
														<option value="I25" <?php echo ($barCodeType=="I25" ? "selected" : " ")?>>Interleaved 2 of 5</option>
														<option value="C39" <?php echo ($barCodeType=="C39" ? "selected" : " ")?>>Code 39</option>
														<option value="C128A" <?php echo ($barCodeType=="C128A" ? "selected" : " ")?>>Code 128-A</option>
														<option value="C128B" <?php echo ($barCodeType=="C128B" ? "selected" : " ")?>>Code 128-B</option>
														<option value="C128C" <?php echo ($barCodeType=="C128C" ? "selected" : " ")?>>Code 128-C</option>
													</select>
												</td>
											</tr>
											<tr>
												<td><b>Output:</b></td>
												<td>
													<select name="barCodeOutput" class="inputSelect" size="1">
														<option value="png" <?php echo ($barCodeOutput=="png" ? "selected" : " ")?>>Portable Network Graphics (PNG)</option>
														<option value="jpeg" <?php echo ($barCodeOutput=="jpeg" ? "selected" : " ")?>>Joint Photographic Experts Group(JPEG)</option>
													</select>
												</td>
											</tr>
											<tr>
												<td><b>Styles:</b></td>
												<td>
													<input type="Checkbox" class="inputField" name="barCodeBorder" <?php echo ($barCodeBorder=="on" ? "CHECKED" : " ")?> />Draw border									
													<input type="Checkbox" class="inputField" name="barCodeDrawtext" <?php echo ($barCodeDrawtext=="on" ? "CHECKED" : " ")?> />Draw value text									
													<input type="Checkbox" class="inputField" name="barCodeStretchtext" <?php echo ($barCodeStretchtext=="on" ? "CHECKED" : " ")?> />Stretch text										
													<input type="Checkbox" class="inputField" name="barCodeNegative" <?php echo ($barCodeNegative=="on" ? "CHECKED" : " ")?> />Negative (White on black)
												</td>
											</tr>
											<tr>
												<td><b>ImageSize:</b></td>
												<td>
													Width: <input type="text" size="6" class="inputField" maxlength="3" name="barCodeWidth" value="<?php echo $barCodeWidth?>">									
													Height: <input type="text" size="6" class="inputField" maxlength="3" name="barCodeHeight" value="<?php echo $barCodeHeight?>">
												</td>
											</tr>
											<tr>
												<td><b>Xres:</b></td>
												<td>
													<input type="Radio" class="inputField" name="barCodeXres" value="1" <?php echo ($barCodeXres=="1" ? "CHECKED" : " ")?> />1
													<input type="Radio" class="inputField" name="barCodeXres" value="2" <?php echo ($barCodeXres=="2" ? "CHECKED" : " ")?> />2
													<input type="Radio" class="inputField" name="barCodeXres" value="3" <?php echo ($barCodeXres=="3" ? "CHECKED" : " ")?> />3
												</td>											
											</tr>
											<tr>
												<td><b>FontSize:</b></td>
												<td>
													<input type="Radio" class="inputField" name="barCodeFont" value="1" <?php echo ($barCodeFont=="1" ? "CHECKED" : " ")?> />1
													<input type="Radio" class="inputField" name="barCodeFont" value="2" <?php echo ($barCodeFont=="2" ? "CHECKED" : " ")?> />2
													<input type="Radio" class="inputField" name="barCodeFont" value="3" <?php echo ($barCodeFont=="3" ? "CHECKED" : " ")?> />3
													<input type="Radio" class="inputField" name="barCodeFont" value="4" <?php echo ($barCodeFont=="4" ? "CHECKED" : " ")?> />4
													<input type="Radio" class="inputField" name="barCodeFont" value="5" <?php echo ($barCodeFont=="5" ? "CHECKED" : " ")?> />5
												</td>
											</tr>
										</table>
									</div>
								</td>
								<td><input type="submit" name="generateNumber" id="generateNumber" class="inputButton" value="Pr&uuml;fziffer(n) erzeugen" readonly /></td>
								<td><img src="images/iconInfo.png" id="buttonInfo" class="buttonImage" width="16" height="16" alt="Info" title="Infos anzeigen" /></td>
							</tr>
						</table>					
                    </form>
				
					<div id="infoArea">
					<h2>Aufbau der GTIN-13- bzw. GLN-Nummer</h2>
					<p>Man unterscheidet zwischen der GTIN (Global Trade Item Number - früher EAN-A Code), wodurch Produkte und Dienstleistungen eindeutig identifiziert werden, und der GLN  (Global Location Number - früher EAN-L Code), womit Partner und Lokationen (z. B. Unternehmen, Unternehmensteile, Lagerorte etc.) identifiziert werden. <br />
					 EAN = Europäische Artikel Nummer</p>					 
					<h3>Aufteilung des Zifferncodes</h3>
					<p>Dieser weltweit am meisten eingesetzte 13-stelliger numerische Strichcode besteht aus folgenden Segmenten:</p>
					<div class="area2cols">
						<div class="colLeft">
							<ul>
								<li>2-stellige Ländernummer</li>
								<li>5-stellige Betriebsnummer des Artikel-Herstellers</li>
								<li>5-stellige Artikelnummer des Herstellers für den jeweiligen Artikel</li>
								<li>1-stellige Prüfziffer für die vorstehenden Codesegmente.</li>
							</ul>             
							<ul>
								<li>Die Strichkodes werden vergeben unter <a href="http://www.gs1-germany.de/" target="_blank">GS1 Germany</a>.</li>
								<li>Hersteller können Sie ermitteln unter <a href="http://www.gepir.de/v31_client/" target="_blank">GEPIR Germany</a>.</li>
							</ul>             
						</div>
						<div class="colRight">
							<img src="images/ean_segmente.gif" width="345" height="116" alt="EAN-Segmente" />
						</div>
						<div class="clear"></div>
					</div>
					<h3>Aufteilung des Strichcodes</h3>
					<p>Die Darstellung der EAN als Strichcodierung erfolgt in zwei Blöcken: <br />
					Der linke Block enthält die Stellen 2 bis 7, der rechte Block die Stellen 8 bis 13 der EAN. <br />
					Die erste Stelle wird durch ein besonderes Verfahren im linken Block versteckt.<br />
					Je zwei Schmale, nach unten etwas längere Striche dienen links und rechts als Randzeichen sowie als Trennzeichen in der Mitte.<br />
					Zum Codieren jeder Ziffer werden drei verschiedene Codetabellen verwendet.</p>
					<img src="images/code.gif" width="786" height="335" alt="Code-Tabelle" /><br />
					<p>
					Für die rechte Hälfte der EAN wird ausschließlich der Code C verwendet.<br />
					Bei der linken Hälfte geht es nicht ganz so einfach, denn dort wird die erste EAN-Ziffer wie folgt eingearbeitet:<br />
					In Abhängigkeit von der ersten Ziffer der EAN wird in linken Block (Ziffer 2 bis 7) nach einem bestimmten Muster zwischen den beiden Codes A und B gewechselt. Die Streifencodes von A und B sind voneinander verschieden - deshalb lässt sich umgekehrt aus den Streifen auch das verwendete Codemuster und damit nach die erste Ziffer der EAN ermitteln.<br />
					Diese umständliche Codierung der ersten Ziffer war erforderlich, damit EAN-Strichcode-Leser auch die in der USA verwendeten UPC-Strichcodes verarbeiten können: Diese erste Ziffer ist nämlich dem UPC-System hinzugefügt worden. Die nur 12stelligen UPC-Nummern verwenden in der linken Hälfte ausschließlich den Code A und erhalten daher im EAN-System automatisch als (zusätzliche) erste Ziffer die Null.</p>
					<p>In der Übersicht erkennt man folgendes:</p>
					<ul>
						<li>Jede Ziffer wird durch sieben Dualziffern so kodiert, dass zwei dunkle und zwei helle Streifen unterschiedliche Breite entstehen.</li>
						<li>Der Code A ergibt sich aus Code C, indem man die jeweilige Stelle negiert.</li>
						<li>Der Code B entsteht aus Code C, indem man die Reihenfolge von Schwarz und Weiß genau umkehrt.</li>
						<li>Dabei beginnen Code A und Code B mit einem hellen Streifen und enden mit einem dunklen Streifen, bei Code C ist es genau umgekehrt. </li>
					</ul>
					<p>Die Codes sind bewußt so gewählt, daß der Computer auch erkennen kann, ob die Streifen mit dem Lesegerät von rechts nach links oder umgekehrt gelesen werden - unterschiedliche Leserichtungen können also nicht zu Verwechslungen führen!</p>
					<p>Ein EAN-Zeichen besteht aus unterschiedlich dicken Balken und Zwischenrämen. Ein Balken oder Zwischenraum wird aus unterschiedlich vielen Modulen zusammengesetzt. Ein Modul ist wiederum das Rastermaß für die Breite von Balken und Zwischenräumen.<br />
					Ein EAN-Zeichen wind nun aus sieben Modulen gebildet (Bild 1). Die Balken werden im folgenden als '1', die Zwischenräume als '0' dargestellt. Von rechts nach links besteht der Code, die im Bild 2 dargestellt, aus: </p>
					<div class="area2cols">
						<div class="colLeft">
							<ul>
								<li>einem Randzeichen, bestehend aus drei Modulen (codiert: 101),</li>
								<li>sechs Nutzzeichen, bestehend aus je sieben Modulen (Stelle 1-6) aus dem Zeichensatz C,</li>
								<li>einem Trennzeichen, bestehend aus je sieben Modulen (codiert 01010),</li>
								<li>sechs Nutzzeichen, bestehend aus je sieben Modulen (Stelle 7-12) aus dem Zeichensatz A und B,</li>
								<li>und einem Randzeichen, bestehend aus drei Modulen (codiert 101).</li>
							</ul>
						</div>
						<div class="colRight">
							<img src="images/ean_strichcode.gif" width="244" height="163" />
						</div>
						<div class="clear"></div>
					</div>
					<h3>Die Prüfziffer</h3>
					<p>Mit Hilfe der letzten Ziffer, der Prüfziffer, können falsche Artikelnummern festgestellt werden. Dabei wird wie folgt geprüft: Von links nach rechts werden die einzelnen Ziffern abwechselnd mit den Faktoren 1 und 3 multipliziert und die Produkte addiert. Der EAN wird nur dann akzeptiert, wenn die so gebildete Prüfsumme ohne Rest durch 10 teilbar ist. Soll also die Prüfziffer bestimmt werden, so bildet man zunächst die Prüfsumme für die ersten 12 Stellen. Die richtige Prüfziffer ergibt sich dann durch Ergänzung zum nächsten Vielfachen von 10. </p>
					<p><img src="images/ziffer.gif" width="600" height="122" /></p>
					<h3>Die Lesegeräte</h3>
					<p>In gewissem Umfang ist die Größe und Druckfarbe des Strichcodes freigestellt. Das setzt besondere Ansprüche an die Lesegeräte. Diese Lesegeräte (Scanner genannt) arbeiten deshalb mit einem präzisem Laserstrahl, der über den EAN-Code geführt wird. Dabei können sie sogar bewegte Codes aus bis zu 30 cm Entfernung mit großer Geschwindigkeit erkennen. Das Verfahren sei nur kurz angedeutet: Der Laserscanner schickt einen durch einen Polygonenspiegel abgelenkten Laserstrahl 50 mal in der Sekunde über das Lesefeld. Die unterschiedlichen Reflexionen des Laserstrahls bei Strichen und Lücken werden über ein Spiegel- und Linsensystem auf einen Fotoempfänger geleitet und in einen Impulszug umgewandelt.</p>
					</div>
				</div>
			</div>
		</div>
	</div>

	<script language="javascript" type="text/javascript">
		$(document).ready(function() {
			$('#formQrCodeArea').css('display', 'none');
			$('#formBarCodeArea').css('display', 'none');
			$('.buttonAdditionalFormElements').click(function(){
				toggleArea($(this).next());
			});
			$('#buttonInfo').click(function(){
				toggleArea('#infoArea');
			});
		});
	</script>
</body>

</html>