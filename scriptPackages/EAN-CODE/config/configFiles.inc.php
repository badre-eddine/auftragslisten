<?php
	// define('DOCUMENT_ROOT', $_SERVER["DOCUMENT_ROOT"] . '/mein_htdocs/SCRIPTE/EAN-CODE/');
	define('DOCUMENT_ROOT', '/Auftragslisten/scriptPackages/EAN-CODE/');
	// define('DOCUMENT_ROOT', '/EAN-CODE/');

	define('PATH_JQUERY', DOCUMENT_ROOT . 'scriptPackages/jquery/jquery-1.8.2.min.js');
	define('PATH_BARCODE_GENERATOR', 'scriptPackages/barcode-0.0.8a/');
	define('PATH_BARCODE_GENERATOR_2', 'scriptPackages/barcodegen.1d-php5.v5.1.0/');
	define('PATH_QRCODE_GENERATOR', 'scriptPackages/phpqrcode-2010100721_1.1.4/');
	define('PATH_JS_FUNCTIONS', DOCUMENT_ROOT . 'js/functions.js');
	define('PATH_CSS_LAYOUT', DOCUMENT_ROOT . 'css/layout.css');
	define('PATH_STORE_GENEREATED_BARCODE_IMAGES', '_generatedBarcodeImages/');
	define('PATH_STORE_GENEREATED_QRCODE_IMAGES', '_generatedQrcodeImages/');
?>