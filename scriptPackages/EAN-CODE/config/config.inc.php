<?php
	set_time_limit(0);

	// DEFINE COUNTRY CODES
	$arrCountryNumbers = array(
		array('number' => '00', 'country' => 'USA, Kanada'),
		array('number' => '01', 'country' => 'USA, Kanada'),
		array('number' => '02', 'country' => 'USA, Kanada'),
		array('number' => '03', 'country' => 'USA, Kanada'),
		array('number' => '04', 'country' => 'USA, Kanada'),
		array('number' => '05', 'country' => 'USA, Kanada'),
		array('number' => '06', 'country' => 'USA, Kanada'),
		array('number' => '07', 'country' => 'USA, Kanada'),
		array('number' => '08', 'country' => 'USA, Kanada'),
		array('number' => '09', 'country' => 'USA, Kanada'),
		array('number' => '10', 'country' => 'Reserve'),
		array('number' => '11', 'country' => 'Reserve'),
		array('number' => '12', 'country' => 'Reserve'),
		array('number' => '13', 'country' => 'Reserve'),
		array('number' => '14', 'country' => 'Reserve'),
		array('number' => '15', 'country' => 'Reserve'),
		array('number' => '16', 'country' => 'Reserve'),
		array('number' => '17', 'country' => 'Reserve'),
		array('number' => '18', 'country' => 'Reserve'),
		array('number' => '19', 'country' => 'Reserve'),
		array('number' => '20', 'country' => 'Interne Nr.'),
		array('number' => '21', 'country' => 'Interne Nr.'),
		array('number' => '22', 'country' => 'Interne Nr.'),
		array('number' => '23', 'country' => 'Interne Nr.'),
		array('number' => '24', 'country' => 'Interne Nr.'),
		array('number' => '25', 'country' => 'Interne Nr.'),
		array('number' => '26', 'country' => 'Interne Nr.'),
		array('number' => '27', 'country' => 'Interne Nr.'),
		array('number' => '28', 'country' => 'Interne Nr.'),
		array('number' => '29', 'country' => 'Interne Nr.'),
		array('number' => '30', 'country' => 'Frankreich'),
		array('number' => '31', 'country' => 'Frankreich'),
		array('number' => '32', 'country' => 'Frankreich'),
		array('number' => '33', 'country' => 'Frankreich'),
		array('number' => '34', 'country' => 'Frankreich'),
		array('number' => '35', 'country' => 'Frankreich'),
		array('number' => '36', 'country' => 'Frankreich'),
		array('number' => '37', 'country' => 'Frankreich'),
		array('number' => '40', 'country' => 'Deutschland'),
		array('number' => '41', 'country' => 'Deutschland'),
		array('number' => '42', 'country' => 'Deutschland'),
		array('number' => '43', 'country' => 'Deutschland'),
		array('number' => '44', 'country' => 'Deutschland'),
		array('number' => '49', 'country' => 'Japan'),
		array('number' => '50', 'country' => 'Großbritannien'),
		array('number' => '54', 'country' => 'Belgien'),
		array('number' => '57', 'country' => 'Dänemark'),
		array('number' => '64', 'country' => 'Finnland'),
		array('number' => '70', 'country' => 'Norwegen'),
		array('number' => '73', 'country' => 'Schweden'),
		array('number' => '76', 'country' => 'Schweiz'),
		array('number' => '80', 'country' => 'Italien'),
		array('number' => '81', 'country' => 'Italien'),
		array('number' => '84', 'country' => 'Spanien'),
		array('number' => '87', 'country' => 'Niederlande'),
		array('number' => '90', 'country' => 'Österreich'),
		array('number' => '91', 'country' => 'Österreich'),
		array('number' => '93', 'country' => 'Australien'),
		array('number' => '978', 'country' => 'Bücher'),
	);

	// EAN BASIC 1 - 7
	DEFINE('EAN_BASIC_CODE', '426029879');
	DEFINE('EAN_CODE_LENGTH', 13);

	// BOF DEFINE BARCODE PARAMS
	DEFINE('BARCODE_OUTPUT', 'png'); // png | jpeg
	DEFINE('BARCODE_BORDER', 'on'); // on | off
	DEFINE('BARCODE_DRAWTEXT', 'on'); // on | off
	DEFINE('BARCODE_STRETCHTEXT', 'on'); // on | off
	DEFINE('BARCODE_NEGATIVE', 'off'); // on | off
	require_once(PATH_BARCODE_GENERATOR.'barcode.php');
	if($_POST["barCodeOutput"] != "") {
		$barCodeOutput = $_POST["barCodeOutput"];
	}
	else {
		$barCodeOutput = BARCODE_OUTPUT;
	}

	if($_POST["barCodeBorder"] != "") {
		$barCodeBorder = $_POST["barCodeBorder"];
	}
	else {
		$barCodeBorder = BARCODE_BORDER;
	}

	if($_POST["barCodeDrawtext"] != "") {
		$barCodeDrawtext = $_POST["barCodeDrawtext"];
	}
	else {
		$barCodeDrawtext = BARCODE_DRAWTEXT;
	}

	if($_POST["barCodeStretchtext"] != "") {
		$barCodeStretchtext = $_POST["barCodeStretchtext"];
	}
	else {
		$barCodeStretchtext = BARCODE_STRETCHTEXT;
	}

	if($_POST["barCodeNegative"] != "") {
		$barCodeNegative = $_POST["barCodeNegative"];
	}
	else {
		$barCodeNegative = BARCODE_NEGATIVE;
	}
	$styleBarcode  = BCS_ALIGN_CENTER;
	$styleBarcode |= ($barCodeOutput  == "png" ) ? BCS_IMAGE_PNG  : 0;
	$styleBarcode |= ($barCodeOutput  == "jpeg") ? BCS_IMAGE_JPEG : 0;
	$styleBarcode |= ($barCodeBorder  == "on"  ) ? BCS_BORDER 	  : 0;
	$styleBarcode |= ($barCodeDrawtext == "on"  ) ? BCS_DRAW_TEXT  : 0;
	$styleBarcode |= ($barCodeStretchtext == "on" ) ? BCS_STRETCH_TEXT  : 0;
	$styleBarcode |= ($barCodeNegative == "on"  ) ? BCS_REVERSE_COLOR  : 0;
	DEFINE('BARCODE_STYLE', $styleBarcode);
	DEFINE('BARCODE_TYPE', 'C128A'); // "I25" | "C39" | "C128A" | "C128B" | "C128C"
	DEFINE('BARCODE_WIDTH', 200);
	DEFINE('BARCODE_HEIGHT', 80);
	DEFINE('BARCODE_XRES', 1); // 1 - 3
	DEFINE('BARCODE_FONT', 2);	// 1 - 5
	// EOF DEFINE BARCODE PARAMS

	// BOF DEFINE QR-CODE PARAMS
	DEFINE('QRCODE_ECC', 'H'); // L | M | Q | H
	DEFINE('QRCODE_SIZE', 4);  // 1 - 10
	DEFINE('QRCODE_SIZE_STEP', 25);
	if($_POST["qrCodeLevel"] != "") {
		$errorCorrectionLevel = $_POST["qrCodeLevel"];
	}
	else {
		$errorCorrectionLevel = QRCODE_ECC;
	}

	if($_POST["qrCodeSize"] != "") {
		$matrixPointSize = $_POST["qrCodeSize"];
	}
	else {
		$matrixPointSize = QRCODE_SIZE;
	}
	// EOF DEFINE QR-CODE PARAMS

	##############



	if($_POST["barCodeType"] != "") {
		$barCodeType = $_POST["barCodeType"];
	}
	else {
		$barCodeType = BARCODE_TYPE;
	}

	if($_POST["barCodeWidth"] != "") {
		$barCodeWidth = $_POST["barCodeWidth"];
	}
	else {
		$barCodeWidth = BARCODE_WIDTH;
	}

	if($_POST["barCodeHeight"] != "") {
		$barCodeHeight = $_POST["barCodeHeight"];
	}
	else {
		$barCodeHeight = BARCODE_HEIGHT;
	}

	if($_POST["barCodeXres"] != "") {
		$barCodeXres = $_POST["barCodeXres"];
	}
	else {
		$barCodeXres = BARCODE_XRES;
	}

	if($_POST["barCodeFont"] != "") {
		$barCodeFont = $_POST["barCodeFont"];
	}
	else {
		$barCodeFont = BARCODE_FONT;
	}
?>