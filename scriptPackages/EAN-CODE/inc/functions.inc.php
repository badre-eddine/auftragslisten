<?php
	function generateNumber($arrDatas) {

		$arrGeneratedNumbers = array();
		$arrNumbersToGenerate = array();
		$basicCode = $arrDatas["countryNumber"];
		$basicCode .= $arrDatas["companyNumber"];
		$basicCode .= $arrDatas["productNumber"];
	
		$mod = 10;
	
		$arrMultipliers = array(1, 3);
	
		$basicCodeLength = strlen($basicCode);
		$seriallyNumbers = pow(10, ((EAN_CODE_LENGTH - 1) - $basicCodeLength)) ;
		if($basicCodeLength == (EAN_CODE_LENGTH - 1)) {
			$arrNumbersToGenerate[] = $basicCode;
		}
		else {
			for($k = 0 ; $k < $seriallyNumbers ; $k++) {
				$arrNumbersToGenerate[] = $basicCode . str_repeat('0', (strlen($seriallyNumbers) - strlen($k) - 1)) . $k;
			}
		}
	
		foreach($arrNumbersToGenerate as $thisNumberToGenerate) {		
			$thisSum = 0;	
			for($i = 0 ; $i < (EAN_CODE_LENGTH - 1) ; $i++) {
				$thisChar = substr($thisNumberToGenerate, $i, 1);
			
				if($i%2 == 0) {
					$thisResult = $thisChar * $arrMultipliers[0];
				}
				else {
					$thisResult = $thisChar * $arrMultipliers[1];
				}
				$thisSum += $thisResult;
				//echo "thisChar: " .  $thisChar . '<br />';
				//echo "thisResult: " .  $thisResult . '<br />';
				//echo "thisSum: " .  $thisSum . '<br />';
			}
			$thisCheckNumber = $mod - ($thisSum%$mod);
			if($thisCheckNumber >= $mod) { $thisCheckNumber = $thisCheckNumber - $mod; }
			// echo "thisCheckNumber: " .  $thisCheckNumber . '<br />';
			$EAN = $thisNumberToGenerate.$thisCheckNumber;
			$arrGeneratedNumbers[] = $EAN;
		}
	
		return $arrGeneratedNumbers;
	}

	// -----------------------------------------
?>