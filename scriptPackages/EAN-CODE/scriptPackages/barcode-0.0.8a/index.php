<?php
	set_time_limit(0);
	require_once('../../config/configFiles.inc.php');
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
			"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
	<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="de" lang="de">
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
		<title>EAN - Generierung</title>
		<meta name="keywords" content="" />
		<meta name="description" content="" />
		<meta name="robots" content="NOINDEX,NOFOLLOW" />
		<meta name="author" content="Thorsten Hoff" />
		<meta http-equiv="imagetoolbar" content="no" />
		<meta http-equiv="content-type" content="text/html; utf-8" />
		<meta http-equiv="Content-Script-Type" content="text/javascript" />
		<meta http-equiv="Content-Style-Type" content="text/css" />
		<meta name="revisit-after" content="1 days" />
		<meta http-equiv="content-language" content="de" />
		<meta http-equiv="cache-control" content="no-cache" />
		<link rel="shortcut icon" href="<?php echo DOCUMENT_ROOT; ?>favicon.png" />
	
		<link rel="stylesheet" type="text/css" href="<?php echo PATH_CSS_LAYOUT; ?>" media="screen" charset="utf-8" />
	</head>

<body>
	<div id="pageArea">
		<div id="documentArea">
			<div id="headerMenueArea">
				<div id="headerMenueContent">
					<a href="<?php echo DOCUMENT_ROOT; ?>">GLN-Prüfziffernrechner</a>
					<a href="<?php echo DOCUMENT_ROOT . PATH_BARCODE_GENERATOR; ?>" class="menueActive">Barcode-Generator</a>
					<a href="<?php echo DOCUMENT_ROOT . PATH_BARCODE_GENERATOR_2; ?>">Barcode-Generator 2</a>
					<a href="<?php echo DOCUMENT_ROOT . PATH_QRCODE_GENERATOR; ?>">QR-Code-Generator</a>
					<div class="clear"></div>
				</div>
			</div>
		<div id="contentArea">
			<div id="contentAreaElements">
			<h1>Barcode-Generator</h1>
<?php
 require_once("config.php");

 define (__TRACE_ENABLED__, false);
 define (__DEBUG_ENABLED__, false);
								   
 require("barcode.php");		   
 require("i25object.php");
 require("c39object.php");
 require("c128aobject.php");
 require("c128bobject.php");
 require("c128cobject.php");
 						  
/* Default value */
if (!isset($output))  $output   = "png"; 
if (!isset($barcode)) $barcode  = "0123456789";
if (!isset($type))    $type     = "I25";
if (!isset($width))   $width    = "460";
if (!isset($height))  $height   = "120";
if (!isset($xres))    $xres     = "2";
if (!isset($font))    $font     = "5";
/*********************************/ 
								
if (isset($barcode) && strlen($barcode)>0) {    
  $style  = BCS_ALIGN_CENTER;

  $style |= ($output  == "png" ) ? BCS_IMAGE_PNG  : 0;
  $style |= ($output  == "jpeg") ? BCS_IMAGE_JPEG : 0;
  $style |= ($border  == "on"  ) ? BCS_BORDER 	  : 0;
  $style |= ($drawtext== "on"  ) ? BCS_DRAW_TEXT  : 0;
  $style |= ($stretchtext== "on" ) ? BCS_STRETCH_TEXT  : 0;
  $style |= ($negative== "on"  ) ? BCS_REVERSE_COLOR  : 0;
  
  switch ($type)
  {
    case "I25":
			  $obj = new I25Object(250, 120, $style, $barcode);
			  break;
    case "C39":
			  $obj = new C39Object(250, 120, $style, $barcode);
			  break;
    case "C128A":
			  $obj = new C128AObject(250, 120, $style, $barcode);
			  break;
    case "C128B":
			  $obj = new C128BObject(250, 120, $style, $barcode);
			  break;
    case "C128C":
              $obj = new C128CObject(250, 120, $style, $barcode);
			  break;
	default:
			$obj = false;
  }
  if ($obj) {
     if ($obj->DrawObject($xres)) {
         echo "<table align='center'><tr><td><img src='./image.php?code=".$barcode."&style=".$style."&type=".$type."&width=".$width."&height=".$height."&xres=".$xres."&font=".$font."'></td></tr></table>";
     } else echo "<table align='center'><tr><td><font color='#FF0000'>".($obj->GetError())."</font></td></tr></table>";
  }
}
?>
<br>
<form method="post" action="<?php echo $_SERVER["PHP_SELF"]; ?>">
<table align="center" border="1" cellpadding="4" cellspacing="0">
 <tr>
  <td bgcolor="#EFEFEF"><b>Typ</b></td>
  <td><select name="type" style="WIDTH: 260px" size="1" class="inputSelect">
  		<option value="I25" <?php echo ($type=="I25" ? "selected" : " ")?>>Interleaved 2 of 5
  		<option value="C39" <?php echo ($type=="C39" ? "selected" : " ")?>>Code 39
  		<option value="C128A" <?php echo ($type=="C128A" ? "selected" : " ")?>>Code 128-A
		<option value="C128B" <?php echo ($type=="C128B" ? "selected" : " ")?>>Code 128-B
        <option value="C128C" <?php echo ($type=="C128C" ? "selected" : " ")?>>Code 128-C</select></td>
 </tr>
 <tr>
  <td bgcolor="#EFEFEF"><b>Ausgabe-Dateiformat</b></td>
  <td><select name="output" class="inputSelect" style="WIDTH: 260px" size="1">
   		<option value="png" <?php echo ($output=="png" ? "selected" : " ")?>>Portable Network Graphics (PNG)
   		<option value="jpeg" <?php echo ($output=="jpeg" ? "selected" : " ")?>>Joint Photographic Experts Group(JPEG)</select></td>
 </tr>
 <tr>
  <td rowspan="4" bgcolor="#EFEFEF"><b>Stil</b></td>
  <td rowspan="1"><input type="Checkbox" class="inputField" name="border" <?php echo ($border=="on" ? "CHECKED" : " ")?>>mit Umrandung</td>
 </tr>
 <tr>
  <td><input type="Checkbox" name="drawtext" class="inputField" <?php echo ($drawtext=="on" ? "CHECKED" : " ")?>>Text einf&uuml;gen</td>
 </tr>
 <tr>
  <td><input type="Checkbox" name="stretchtext" class="inputField" <?php echo ($stretchtext=="on" ? "CHECKED" : " ")?>>Text strecken</td>
 </tr>
 <tr>
  <td><input type="Checkbox" name="negative" class="inputField" <?php echo ($negative=="on" ? "CHECKED" : " ")?>>Negativ (Weiss auf schwarz)</td>
 </tr>
 <tr>
  <td rowspan="2" bgcolor="#EFEFEF"><b>Ma&szlig;e</b></td>
  <td rowspan="1">Breite: <input type="text" size="6" maxlength="3" name="width" class="inputField" value="<?php echo $width?>"></td>
 </tr>
 <tr>
  <td>H&ouml;he: <input type="text" size="6" maxlength="3" name="height" class="inputField" value="<?php echo $height?>"></td>
 </tr>
 <tr>
  <td bgcolor="#EFEFEF"><b>Xres</b></td>
  <td>
      <input type="Radio" name="xres" class="inputRadio" value="1" <?php echo ($xres=="1" ? "CHECKED" : " ")?>>1&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
      <input type="Radio" name="xres" class="inputRadio" value="2" <?php echo ($xres=="2" ? "CHECKED" : " ")?>>2&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
      <input type="Radio" name="xres" class="inputRadio" value="3" <?php echo ($xres=="3" ? "CHECKED" : " ")?>>3&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
  </td>
 </tr>
 <tr>
  <td bgcolor="#EFEFEF"><b>Schriftgr&ouml;&szlig;e</b></td>
  <td>
      <input type="Radio" name="font" class="inputField" value="1" <?php echo ($font=="1" ? "CHECKED" : " ")?>>1&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
      <input type="Radio" name="font" class="inputField" value="2" <?php echo ($font=="2" ? "CHECKED" : " ")?>>2&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
      <input type="Radio" name="font" class="inputField" value="3" <?php echo ($font=="3" ? "CHECKED" : " ")?>>3&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
      <input type="Radio" name="font" class="inputField" value="4" <?php echo ($font=="4" ? "CHECKED" : " ")?>>4&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
      <input type="Radio" name="font" class="inputField" value="5" <?php echo ($font=="5" ? "CHECKED" : " ")?>>5&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
  </td>
 </tr>
 <tr>
  <td bgcolor="#EFEFEF"><b>Daten</b></td>
  <td><input type="Text" size="24" name="barcode" class="inputField" style="WIDTH: 260px" value="<?php echo $barcode?>"></td>
 </tr>
 <tr>
 </tr>
 <tr>
  <td colspan="2" align="center"><input type="Submit" name="Submit" class="inputButton" value="Generierung starten"></td>
 </tr>
</table>
</form>
			</div>
		</div>
	</div>
</div>
</body>
</html>
