function toggleArea(element) {
	var animationTime = 'fast';
	var modeDisplay = $(element).css('display')	
	// $(element).toggle(animationTime);
	if(modeDisplay == 'block') {
		// $(element).fadeOut(animationTime);
		$(element).slideUp(animationTime);
		$('#buttonInfo').attr('title', 'Infos anzeigen');
	}
	else {
		// $(element).fadeIn(animationTime);
		$(element).slideDown(animationTime);
		$('#buttonInfo').attr('title', 'Infos ausblenden');
	}
}