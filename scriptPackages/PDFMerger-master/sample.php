<?php
include 'PDFMerger.php';

$pdf = new PDFMerger;

$pdf->addPDF('scriptPackages/PDFMerger-master/samplepdfs/one.pdf', '1, 3, 4')
	->addPDF('scriptPackages/PDFMerger-master/samplepdfs/two.pdf', '1-2')
	->addPDF('scriptPackages/PDFMerger-master/samplepdfs/three.pdf', 'all')
	->merge('file', 'scriptPackages/PDFMerger-master/samplepdfs/TEST2.pdf');
	
	//REPLACE 'file' WITH 'browser', 'download', 'string', or 'file' for output options
	//You do not need to give a file path for browser, string, or download - just the name.
