<?php
	set_time_limit(0);

	require_once($_SERVER["DOCUMENT_ROOT"]."/includes/configure.php");
	require_once($_SERVER["DOCUMENT_ROOT"]."/includes/database_tables.php");

	$pdfHtmlTemplate = 'pdfHtmlTemplate.html';
	$imagePath = '../../images/product_images/original_images/';
	$logoPath = "../../templates/xtc5/img/";

	if(file_exists($pdfHtmlTemplate)) {
		unlink($pdfHtmlTemplate);
	}


	$db_open = mysqli_connect(DB_SERVER, DB_SERVER_USERNAME, DB_SERVER_PASSWORD) or die('Keine Verbindung zur Datenbank!');
	$db_select = mysqli_select_db(DB_DATABASE);

	$sql = "SELECT 

				`categories`.`categories_id`,
				`categories_description`.`categories_name`
			
				FROM `categories`
			
				LEFT JOIN `categories_description`
				ON (`categories`.`categories_id` = `categories_description`.`categories_id`)
			
				WHERE `categories`.`categories_status` = 1
			
				ORDER BY `categories_description`.`categories_name`
	";

	$rs = mysqli_query($sql);

	while($ds = mysqli_fetch_array($rs)) {
		$arrCategories[$ds["categories_id"]] = $ds["categories_name"];	
	}

	$sql = "SELECT 
					`customers_status`.`customers_status_id`,
					`customers_status`.`customers_status_name`
				FROM `customers_status`
			
				ORDER BY `customers_status`.`customers_status_name`
	";

	$rs = mysqli_query($sql);

	while($ds = mysqli_fetch_array($rs)) {
		$arrCustomers[$ds["customers_status_id"]] = $ds["customers_status_name"];	
	}

	if($_POST["submitForm"] != "") {
	
		$sql_category = "";	
		if($_POST["selectCategories"] != "" && $_POST["selectCategories"] != "all") {
			$strCategories = implode("`products_to_categories`.`categories_id`", $_POST["selectCategories"]);
			foreach($_POST["selectCategories"] as $thisCategoryID) {
				$sql_category .= " `products_to_categories`.`categories_id` = '".$thisCategoryID."' OR ";
			}
			$sql_category = " AND ( ".substr($sql_category, 0, -4). " )";
		}
		$sql_customer = "";
		if($_POST["selectCustomer"] != "" && $_POST["selectCustomer"] != "none") {
			$thisCustomerGroupTitle = "Preise f&uuml;r ".$arrCustomers[$_POST["selectCustomer"]];
		}
		else {
			$thisCustomerGroupTitle = "ohne Preise";
		}
	
		$sql = "
				SELECT 
					`products_to_categories`.`products_id` AS `products_id`,
					`products_to_categories`.`categories_id` AS `categories_id`,
				
					`categories_description`.`categories_name`,
				
					`products`.`products_image`,
				
					`products_description`.`products_name`,
					`products_description`.`products_description`,
				
				
					`products_options`.`products_options_name`,
				
				
					`products_attributes`.`options_id`,
					`products_attributes`.`options_values_id`,
					`products_attributes`.`options_values_price`,
					`products_attributes`.`price_prefix`,
					`products_attributes`.`attributes_model`,
					GROUP_CONCAT(`products_options_values`.`products_options_values_name` SEPARATOR '#')
				
				
				FROM `products_to_categories`
			
				INNER JOIN `categories`
				ON(`products_to_categories`.`categories_id` = `categories`.`categories_id`)
			
				INNER JOIN `categories_description`
				ON(`products_to_categories`.`categories_id` = `categories_description`.`categories_id`)
			
				INNER JOIN `products`
				ON(`products_to_categories`.`products_id` = `products`.`products_id`)
			
				INNER JOIN `products_description`
				ON(`products`.`products_id` = `products_description`.`products_id`)
			
				INNER JOIN `products_attributes`
				ON(`products`.`products_id` = `products_attributes`.`products_id`)
			
				INNER JOIN `products_options`
				ON (`products_attributes`.`options_id` = `products_options`.`products_options_id`)
			
				INNER JOIN `products_options_values`
				ON(`products_attributes`.`options_values_id` = `products_options_values`.`products_options_values_id`)
			
				WHERE 
					`categories`.`categories_status` = 1
					".$sql_category."
			
				GROUP BY 
					`products`.`products_id`,
					`products_attributes`.`options_id`
			
				ORDER BY 
					`products_to_categories`.`categories_id`,
					`products`.`products_id`,
					`products_attributes`.`options_id`,
					`products_options_values`.`products_options_values_name`
				
			
	
		";	
	
		$rs = mysqli_query($sql);
	
		// BOF WRITE TITLE PAGE
			// <img src="'.$logoPath.'header_pdf.jpg">
			$pdfTitlePage = '<style type="text/css">
				<!--
					table.page_header {width: 100%; border-bottom: 1pt solid #333333; background-color: #FFFFFF; padding: 2mm; margin:0 0 10pt 0; }
					table.page_footer {width: 100%; border-top: 1pt solid #333333; background-color: #FFFFFF; padding: 2mm; font-size: 6pt;}
					.h1 {width: 100%; color: #FFFFFF; text-align:right; background-color:#B70718; padding: 0 1pt 1pt 1pt; margin:10pt 0 4pt 0; font-size: 12pt; font-weight: bold;letter-spacing: 1px;}
					.h2 {width: 100%; color: #000000; text-align:left; background-color:#CCCCCC; padding: 1pt 1pt 1pt 1pt; margin:0 0 4pt 0; font-size: 10pt; font-weight: bold;letter-spacing: 1px;}
					.h3 {color: #000077}						
			

					.productContent {padding: 0; margin: 0;width: 100%;}
					ul{margin:0; padding:0;}
					li{margin:0; padding:0;}
				-->
				</style>
				<page backtop="14mm" backbottom="14mm" backleft="10mm" backright="10mm" style="font-size: 12pt">
					<page_header>
						<table class="page_header">
							<tr>
								<td style="width: 50%; text-align: left">								
									<img src="'.$logoPath.'logo_medium.png" style="width:50mm;">
								</td>						
								<td style="width: 50%; text-align: right">								
									<b>BURHAN-CTR</b> - '.$thisCustomerGroupTitle.' - <b>Stand:</b> '.date("d.m.Y").'
								</td>
							</tr>
						</table>
					</page_header>
					<page_footer>
						<table class="page_footer">
							<tr>
								<td style="width: 100%; text-align: center; font-size:7pt;">
									BURHAN CTR e. K. &bull; Grüner Weg 83 &bull; D-48268 Greven &bull; Tel.: +49 2571 / 80 00 22 &bull; Fax: +49 2571 / 80 00 33 &bull; E-Mail: <a href="mailto:info@burhan-ctr.de">info@burhan-ctr.de</a>
								</td>
							</tr>
							<tr>
								<td style="width: 100%; text-align: right">
									page [[page_cu]]/[[page_nb]]
								</td>
							</tr>
						</table>
					</page_footer>
					<bookmark title="Inhaltsverzeichnis" level="0" ></bookmark>
				</page>';
			$pdfTitlePage = preg_replace("/\\\t/", "", $pdfTitlePage);
			$pdfTitlePage = preg_replace("/\\\r/", "", $pdfTitlePage);
			$pdfTitlePage = preg_replace("/\\\n/", "", $pdfTitlePage);
		
			$fp = fopen($pdfHtmlTemplate, 'a+');
			fwrite($fp, $pdfTitlePage);
			fclose($fp);
		// EOF WRITE TITLE PAGE

	
	
		$thisMarkerCategoryID = 0;
		$thisMarkerProductID = 0;
		while($ds = mysqli_fetch_assoc($rs)) {		
		
			if($ds["products_id"] != $thisMarkerProductID) {
				$thisProductName = $ds["products_name"];
			
				$pdfContentPage = '
					<page pageset="old" backtop="14mm" backbottom="14mm" backleft="10mm" backright="10mm">
				';
				if($ds["categories_id"] != $thisMarkerCategoryID) {
					$thisCategoryName = $ds["categories_name"];
					$thisMarkerCategoryID = $ds["categories_id"];
					$pdfContentPage .= '<bookmark title="'.$thisCategoryName.'" level="0" ></bookmark>';				
				}
			
				$pdfContentPage .= '<div class="h1">'.$thisCategoryName.'</div>
						<div class="h2">'.$thisProductName.'</div>
						<table class="productContent">
							<tr style="vertical-align:top;">
								<td style="width:50%; text-align:left;">
									<img src="'.$imagePath.$ds["products_image"].'" style="width:50mm;">
								</td>						
								<td style="width:50%; text-align:left;">
									'.$ds["products_description"].'
								</td>
							</tr>											
						</table>';			
			}
			// $pdfContentPage .= '<pre>'.print_r($ds).'</pre>';
			
			if($ds["products_id"] != $thisMarkerProductID) {
				$pdfContentPage .= '
					</page>';		
		
				$thisMarkerProductID = $ds["products_id"];
			
				$pdfContentPage = preg_replace("/\\\t/", "", $pdfContentPage);
				$pdfContentPage = preg_replace("/\\\r/", "", $pdfContentPage);
				$pdfContentPage = preg_replace("/\\\n/", "", $pdfContentPage);
				// BOF WRITE HTML TEMPLATE FOR PDF.EXPORT
					$fp = fopen($pdfHtmlTemplate, 'a+');
					fwrite($fp, $pdfContentPage);
					fclose($fp);
				// EOF WRITE HTML TEMPLATE FOR PDF.EXPORT
				// echo "thisTemplate: ".htmlentities($pdfContentPage);
			}
		
		
		}

		if(file_exists($pdfHtmlTemplate)) {
		// include(dirname(__FILE__).'/res/exemple08.php');
			// $fp = fopen ($pdfHtmlTemplate, 'r');		
			// $contentPDF = fread ($fp, filesize($pdfHtmlTemplate) );
			// fclose ($fp);
			ob_start();
			include($pdfHtmlTemplate);	
			$contentPDF = ob_get_clean();

			require_once('html2pdf.class.php');
			try
			{
				$html2pdf = new HTML2PDF('P', 'A4', 'de', true, 'UTF-8', 0);
				$html2pdf->writeHTML($contentPDF, isset($_GET['vuehtml']));
				$html2pdf->createIndex('Inhaltsverzeichnis', 25, 12, false, true, 1);
				$html2pdf->Output('bookmark.pdf');
				exit;
			}
			catch(HTML2PDF_exception $e) {
				echo $e;
				exit;
			}
		}

	
	}

	$db_close = mysqli_close($db_open);

	if($_POST["submitForm"] == "")
	{
?>
<form name="formCreatePDF" method="post">
<table border="1" cellpadding="2" cellspacing="0" width="">
	<tr>
		<td>Kategorien:</td>
		<td>
			<select multiple="multiple" name="selectCategories[]" size="2">
				<option value="all">ALLE KATEGORIEN</option>
				<?php
					if(!empty($arrCategories)) {
						foreach($arrCategories as $thisCategoriesKey => $thisCategoriesValue) {
							echo '<option value="'.$thisCategoriesKey.'">'.$thisCategoriesValue.'</option>';
						}
					}
				?>
			</select>
		</td>	
	</tr>
	<tr>
		<td>Preise anzeigen für:</td>
		<td>
			<select name="selectCustomer" size="2">
				<option value="none">KEINE PREISE ANZEIGEN</option>
				<?php
					if(!empty($arrCustomers)) {
						foreach($arrCustomers as $thisCustomersKey => $thisCustomersValue) {
							echo '<option value="'.$thisCustomersKey.'">'.$thisCustomersValue.'</option>';
						}
					}
				?>
			</select>
		</td>	
	</tr>
</table>
<input type="submit" name="submitForm" value="PDF erstellen" />
</form>
<?php
	}

?>