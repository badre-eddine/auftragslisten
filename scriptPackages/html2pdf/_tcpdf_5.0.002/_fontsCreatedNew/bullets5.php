<?php
// TCPDF FONT FILE DESCRIPTION
$type='TrueTypeUnicode';
$name='bullets5';
$up=0;
$ut=0;
$dw=1015;
$diff='';
$originalsize=91480;
$enc='cp1250';
$file='bullets5.z';
$ctg='bullets5.ctg.z';
$desc=array('Flags'=>32,'FontBBox'=>'[-40 0 967 1015]','ItalicAngle'=>0,'Ascent'=>1015,'Descent'=>0,'Leading'=>0,'CapHeight'=>1015,'XHeight'=>1015,'StemV'=>34,'StemH'=>15,'AvgWidth'=>20,'MaxWidth'=>1016,'MissingWidth'=>1015);
$cw=array(0=>1015);
// --- EOF ---
