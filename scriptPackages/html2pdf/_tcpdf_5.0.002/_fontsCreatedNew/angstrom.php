<?php
// TCPDF FONT FILE DESCRIPTION
$type='TrueTypeUnicode';
$name='Angstrom';
$up=-133;
$ut=20;
$dw=500;
$diff='';
$originalsize=28984;
$enc='cp1250';
$file='angstrom.z';
$ctg='angstrom.ctg.z';
$desc=array('Flags'=>32,'FontBBox'=>'[-11 -19 1052 800]','ItalicAngle'=>0,'Ascent'=>800,'Descent'=>-200,'Leading'=>0,'CapHeight'=>721,'XHeight'=>800,'StemV'=>70,'StemH'=>30,'AvgWidth'=>0,'MaxWidth'=>1086,'MissingWidth'=>500);
$cw=array(0=>500,32=>500,65=>677,66=>475,67=>512,68=>576,69=>535,70=>509,71=>553,72=>564,73=>233,74=>518,75=>463,76=>504,77=>665,78=>567,79=>619,80=>458,81=>619,82=>455,83=>544,84=>639,85=>567,86=>657,87=>1086,88=>599,89=>440,90=>527,65535=>0);
// --- EOF ---
