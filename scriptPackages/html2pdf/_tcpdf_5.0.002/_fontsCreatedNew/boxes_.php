<?php
// TCPDF FONT FILE DESCRIPTION
$type='TrueTypeUnicode';
$name='boxes_';
$up=0;
$ut=0;
$dw=1000;
$diff='';
$originalsize=17288;
$enc='cp1250';
$file='boxes_.z';
$ctg='boxes_.ctg.z';
$desc=array('Flags'=>32,'FontBBox'=>'[43 43 955 955]','ItalicAngle'=>0,'Ascent'=>955,'Descent'=>43,'Leading'=>89,'CapHeight'=>909,'XHeight'=>909,'StemV'=>34,'StemH'=>15,'AvgWidth'=>69,'MaxWidth'=>1002,'MissingWidth'=>1000);
$cw=array(0=>1000);
// --- EOF ---
