<?php
// TCPDF FONT FILE DESCRIPTION
$type='TrueTypeUnicode';
$name='business';
$up=0;
$ut=0;
$dw=1000;
$diff='';
$originalsize=33500;
$enc='cp1250';
$file='business.z';
$ctg='business.ctg.z';
$desc=array('Flags'=>32,'FontBBox'=>'[39 33 955 955]','ItalicAngle'=>0,'Ascent'=>955,'Descent'=>33,'Leading'=>79,'CapHeight'=>909,'XHeight'=>909,'StemV'=>34,'StemH'=>15,'AvgWidth'=>250,'MaxWidth'=>1002,'MissingWidth'=>1000);
$cw=array(0=>1000);
// --- EOF ---
