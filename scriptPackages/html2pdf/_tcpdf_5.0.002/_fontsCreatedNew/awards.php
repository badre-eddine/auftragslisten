<?php
// TCPDF FONT FILE DESCRIPTION
$type='TrueTypeUnicode';
$name='awards';
$up=0;
$ut=0;
$dw=1000;
$diff='';
$originalsize=14308;
$enc='cp1250';
$file='awards.z';
$ctg='awards.ctg.z';
$desc=array('Flags'=>32,'FontBBox'=>'[44 39 970 955]','ItalicAngle'=>0,'Ascent'=>955,'Descent'=>39,'Leading'=>84,'CapHeight'=>786,'XHeight'=>786,'StemV'=>34,'StemH'=>15,'AvgWidth'=>307,'MaxWidth'=>1018,'MissingWidth'=>1000);
$cw=array(0=>1000);
// --- EOF ---
