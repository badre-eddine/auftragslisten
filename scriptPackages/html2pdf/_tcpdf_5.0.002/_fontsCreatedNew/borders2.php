<?php
// TCPDF FONT FILE DESCRIPTION
$type='TrueTypeUnicode';
$name='borders2';
$up=0;
$ut=0;
$dw=1000;
$diff='';
$originalsize=33708;
$enc='cp1250';
$file='borders2.z';
$ctg='borders2.ctg.z';
$desc=array('Flags'=>32,'FontBBox'=>'[43 43 955 955]','ItalicAngle'=>0,'Ascent'=>955,'Descent'=>43,'Leading'=>89,'CapHeight'=>900,'XHeight'=>900,'StemV'=>34,'StemH'=>15,'AvgWidth'=>115,'MaxWidth'=>1002,'MissingWidth'=>1000);
$cw=array(0=>1000);
// --- EOF ---
