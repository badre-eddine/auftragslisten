<?php
// TCPDF FONT FILE DESCRIPTION
$type='TrueTypeUnicode';
$name='AimsinyourLifenormal';
$up=-65;
$ut=10;
$dw=576;
$diff='';
$originalsize=67421;
$enc='cp1250';
$file='aims.z';
$ctg='aims.ctg.z';
$desc=array('Flags'=>32,'FontBBox'=>'[-6 0 1546 995]','ItalicAngle'=>0,'Ascent'=>992,'Descent'=>0,'Leading'=>0,'CapHeight'=>657,'XHeight'=>657,'StemV'=>70,'StemH'=>30,'AvgWidth'=>121,'MaxWidth'=>0,'MissingWidth'=>576);
$cw=array(0=>576);
// --- EOF ---
