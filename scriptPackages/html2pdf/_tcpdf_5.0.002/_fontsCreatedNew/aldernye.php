<?php
// TCPDF FONT FILE DESCRIPTION
$type='TrueTypeUnicode';
$name='AlderneyEngraved';
$up=-117;
$ut=18;
$dw=706;
$diff='';
$originalsize=40204;
$enc='cp1250';
$file='aldernye.z';
$ctg='aldernye.ctg.z';
$desc=array('Flags'=>32,'FontBBox'=>'[-53 -294 914 894]','ItalicAngle'=>0,'Ascent'=>894,'Descent'=>-294,'Leading'=>0,'CapHeight'=>706,'XHeight'=>706,'StemV'=>88,'StemH'=>38,'AvgWidth'=>408,'MaxWidth'=>834,'MissingWidth'=>706);
$cw=array(0=>706);
// --- EOF ---
