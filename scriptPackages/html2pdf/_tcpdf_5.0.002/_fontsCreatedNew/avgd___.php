<?php
// TCPDF FONT FILE DESCRIPTION
$type='TrueTypeUnicode';
$name='AvantGarde-Demi';
$up=-92;
$ut=90;
$dw=800;
$diff='';
$originalsize=30148;
$enc='cp1250';
$file='avgd___.z';
$ctg='avgd___.ctg.z';
$desc=array('Flags'=>32,'FontBBox'=>'[-121 -251 1248 1025]','ItalicAngle'=>0,'Ascent'=>800,'Descent'=>-200,'Leading'=>0,'CapHeight'=>800,'XHeight'=>800,'StemV'=>2,'StemH'=>1,'AvgWidth'=>0,'MaxWidth'=>1280,'MissingWidth'=>800);
$cw=array(0=>800);
// --- EOF ---
