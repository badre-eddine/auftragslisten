<?php
// TCPDF FONT FILE DESCRIPTION
$type='TrueTypeUnicode';
$name='Akka';
$up=-49;
$ut=24;
$dw=750;
$diff='';
$originalsize=67296;
$enc='cp1250';
$file='akka.z';
$ctg='akka.ctg.z';
$desc=array('Flags'=>32,'FontBBox'=>'[0 -223 869 1063]','ItalicAngle'=>0,'Ascent'=>785,'Descent'=>-28,'Leading'=>0,'CapHeight'=>625,'XHeight'=>625,'StemV'=>34,'StemH'=>15,'AvgWidth'=>651,'MaxWidth'=>894,'MissingWidth'=>750);
$cw=array(0=>750);
// --- EOF ---
