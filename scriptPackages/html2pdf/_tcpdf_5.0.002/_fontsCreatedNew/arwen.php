<?php
// TCPDF FONT FILE DESCRIPTION
$type='TrueTypeUnicode';
$name='ArwenRegular';
$up=-120;
$ut=60;
$dw=857;
$diff='';
$originalsize=8220;
$enc='cp1250';
$file='arwen.z';
$ctg='arwen.ctg.z';
$desc=array('Flags'=>32,'FontBBox'=>'[0 -126 804 857]','ItalicAngle'=>0,'Ascent'=>771,'Descent'=>-125,'Leading'=>0,'CapHeight'=>750,'XHeight'=>857,'StemV'=>1,'StemH'=>0,'AvgWidth'=>0,'MaxWidth'=>857,'MissingWidth'=>857);
$cw=array(0=>857,32=>281,33=>113,44=>122,46=>122,58=>122,59=>122,65=>614,66=>512,67=>708,68=>591,69=>569,70=>466,71=>752,72=>495,73=>113,74=>345,75=>527,76=>456,77=>688,78=>488,79=>788,80=>473,81=>788,82=>584,83=>463,84=>518,85=>518,86=>537,87=>815,88=>527,89=>574,90=>539,160=>250,8226=>113,65535=>857);
// --- EOF ---
