<?php
// TCPDF FONT FILE DESCRIPTION
$type='TrueTypeUnicode';
$name='BabysBreathEaster';
$up=-100;
$ut=50;
$dw=500;
$diff='';
$originalsize=110520;
$enc='cp1250';
$file='babysbreatheaster.z';
$ctg='babysbreatheaster.ctg.z';
$desc=array('Flags'=>32,'FontBBox'=>'[0 -21 984 978]','ItalicAngle'=>0,'Ascent'=>750,'Descent'=>-170,'Leading'=>30,'CapHeight'=>976,'XHeight'=>856,'StemV'=>70,'StemH'=>30,'AvgWidth'=>647,'MaxWidth'=>996,'MissingWidth'=>500);
$cw=array(0=>500,32=>500,65=>586,66=>606,67=>769,68=>996,69=>557,70=>884,71=>607,72=>616,73=>619,74=>609,75=>572,65535=>500);
// --- EOF ---
