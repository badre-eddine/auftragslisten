<?php
// TCPDF FONT FILE DESCRIPTION
$type='TrueTypeUnicode';
$name='bullets2';
$up=0;
$ut=0;
$dw=1000;
$diff='';
$originalsize=36248;
$enc='cp1250';
$file='bullets2.z';
$ctg='bullets2.ctg.z';
$desc=array('Flags'=>32,'FontBBox'=>'[45 45 955 955]','ItalicAngle'=>0,'Ascent'=>955,'Descent'=>45,'Leading'=>91,'CapHeight'=>909,'XHeight'=>909,'StemV'=>34,'StemH'=>15,'AvgWidth'=>30,'MaxWidth'=>1002,'MissingWidth'=>1000);
$cw=array(0=>1000);
// --- EOF ---
