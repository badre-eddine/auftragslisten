<?php
// TCPDF FONT FILE DESCRIPTION
$type='TrueTypeUnicode';
$name='BrookmanSwashBold';
$up=-49;
$ut=24;
$dw=750;
$diff='';
$originalsize=23568;
$enc='cp1250';
$file='brook.z';
$ctg='brook.ctg.z';
$desc=array('Flags'=>32,'FontBBox'=>'[-235 -444 1639 977]','ItalicAngle'=>0,'Ascent'=>802,'Descent'=>-454,'Leading'=>0,'CapHeight'=>625,'XHeight'=>625,'StemV'=>34,'StemH'=>15,'AvgWidth'=>784,'MaxWidth'=>1568,'MissingWidth'=>750);
$cw=array(0=>750);
// --- EOF ---
