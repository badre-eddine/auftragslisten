<?php
// TCPDF FONT FILE DESCRIPTION
$type='TrueTypeUnicode';
$name='Starburst';
$up=-111;
$ut=17;
$dw=917;
$diff='';
$originalsize=40008;
$enc='cp1250';
$file='bursla_.z';
$ctg='bursla_.ctg.z';
$desc=array('Flags'=>32,'FontBBox'=>'[-11 -131 1176 917]','ItalicAngle'=>0,'Ascent'=>917,'Descent'=>-223,'Leading'=>0,'CapHeight'=>833,'XHeight'=>917,'StemV'=>70,'StemH'=>30,'AvgWidth'=>0,'MaxWidth'=>1211,'MissingWidth'=>917);
$cw=array(0=>917,32=>331,33=>386,34=>654,36=>495,38=>760,39=>380,44=>425,45=>479,46=>321,48=>867,49=>470,50=>738,51=>863,52=>841,53=>830,54=>836,55=>787,56=>793,57=>839,58=>331,59=>376,63=>666,65=>996,66=>890,67=>776,68=>912,69=>705,70=>682,71=>856,72=>890,73=>389,74=>769,75=>857,76=>749,77=>1211,78=>843,79=>911,80=>858,81=>855,82=>879,83=>849,84=>806,85=>863,86=>841,87=>1153,88=>822,89=>820,90=>835,160=>331,162=>553,223=>973,255=>300,65535=>917);
// --- EOF ---
