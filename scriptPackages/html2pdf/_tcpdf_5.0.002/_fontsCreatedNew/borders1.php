<?php
// TCPDF FONT FILE DESCRIPTION
$type='TrueTypeUnicode';
$name='borders1';
$up=0;
$ut=0;
$dw=1000;
$diff='';
$originalsize=24948;
$enc='cp1250';
$file='borders1.z';
$ctg='borders1.ctg.z';
$desc=array('Flags'=>32,'FontBBox'=>'[43 43 955 955]','ItalicAngle'=>0,'Ascent'=>955,'Descent'=>43,'Leading'=>89,'CapHeight'=>307,'XHeight'=>307,'StemV'=>34,'StemH'=>15,'AvgWidth'=>205,'MaxWidth'=>1002,'MissingWidth'=>1000);
$cw=array(0=>1000);
// --- EOF ---
