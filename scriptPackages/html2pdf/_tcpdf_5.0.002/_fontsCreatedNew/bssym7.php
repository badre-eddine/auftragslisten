<?php
// TCPDF FONT FILE DESCRIPTION
$type='TrueTypeUnicode';
$name='BookshelfSymbolSeven';
$up=-94;
$ut=47;
$dw=1000;
$diff='';
$originalsize=55540;
$enc='cp1250';
$file='bssym7.z';
$ctg='bssym7.ctg.z';
$desc=array('Flags'=>32,'FontBBox'=>'[0 -137 984 859]','ItalicAngle'=>0,'Ascent'=>859,'Descent'=>-141,'Leading'=>0,'CapHeight'=>215,'XHeight'=>215,'StemV'=>274,'StemH'=>117,'AvgWidth'=>656,'MaxWidth'=>1000,'MissingWidth'=>1000);
$cw=array(0=>1000);
// --- EOF ---
