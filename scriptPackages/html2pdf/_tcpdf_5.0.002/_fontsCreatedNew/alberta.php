<?php
// TCPDF FONT FILE DESCRIPTION
$type='TrueTypeUnicode';
$name='Alberta';
$up=-49;
$ut=24;
$dw=750;
$diff='';
$originalsize=19300;
$enc='cp1250';
$file='alberta.z';
$ctg='alberta.ctg.z';
$desc=array('Flags'=>32,'FontBBox'=>'[0 -170 1019 807]','ItalicAngle'=>0,'Ascent'=>784,'Descent'=>-15,'Leading'=>0,'CapHeight'=>625,'XHeight'=>625,'StemV'=>34,'StemH'=>15,'AvgWidth'=>661,'MaxWidth'=>1045,'MissingWidth'=>750);
$cw=array(0=>750);
// --- EOF ---
