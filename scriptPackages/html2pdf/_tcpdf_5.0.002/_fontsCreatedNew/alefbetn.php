<?php
// TCPDF FONT FILE DESCRIPTION
$type='TrueTypeUnicode';
$name='AlefbetNormal';
$up=-24;
$ut=12;
$dw=750;
$diff='';
$originalsize=20332;
$enc='cp1250';
$file='alefbetn.z';
$ctg='alefbetn.ctg.z';
$desc=array('Flags'=>32,'FontBBox'=>'[-96 -352 775 803]','ItalicAngle'=>0,'Ascent'=>803,'Descent'=>-357,'Leading'=>0,'CapHeight'=>625,'XHeight'=>625,'StemV'=>17,'StemH'=>7,'AvgWidth'=>0,'MaxWidth'=>967,'MissingWidth'=>750);
$cw=array(0=>750);
// --- EOF ---
