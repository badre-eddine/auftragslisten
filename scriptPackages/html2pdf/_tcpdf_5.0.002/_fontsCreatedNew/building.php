<?php
// TCPDF FONT FILE DESCRIPTION
$type='TrueTypeUnicode';
$name='building';
$up=0;
$ut=0;
$dw=1000;
$diff='';
$originalsize=44024;
$enc='cp1250';
$file='building.z';
$ctg='building.ctg.z';
$desc=array('Flags'=>32,'FontBBox'=>'[44 44 955 955]','ItalicAngle'=>0,'Ascent'=>955,'Descent'=>44,'Leading'=>89,'CapHeight'=>331,'XHeight'=>331,'StemV'=>34,'StemH'=>15,'AvgWidth'=>177,'MaxWidth'=>1002,'MissingWidth'=>1000);
$cw=array(0=>1000);
// --- EOF ---
