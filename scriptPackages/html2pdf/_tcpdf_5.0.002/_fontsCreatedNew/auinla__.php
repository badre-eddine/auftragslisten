<?php
// TCPDF FONT FILE DESCRIPTION
$type='TrueTypeUnicode';
$name='CarrickCaps';
$up=-89;
$ut=13;
$dw=1004;
$diff='';
$originalsize=40760;
$enc='cp1250';
$file='auinla__.z';
$ctg='auinla__.ctg.z';
$desc=array('Flags'=>32,'FontBBox'=>'[0 -2 942 1004]','ItalicAngle'=>0,'Ascent'=>1004,'Descent'=>-200,'Leading'=>0,'CapHeight'=>996,'XHeight'=>1004,'StemV'=>70,'StemH'=>30,'AvgWidth'=>0,'MaxWidth'=>1004,'MissingWidth'=>1004);
$cw=array(0=>1004,32=>300,65=>907,66=>897,67=>907,68=>892,69=>916,70=>904,71=>911,72=>914,73=>936,74=>936,75=>883,76=>875,77=>979,78=>880,79=>914,80=>869,81=>901,82=>893,83=>912,84=>895,85=>892,86=>914,87=>959,88=>917,89=>909,90=>903,160=>300,255=>300,65535=>1004);
// --- EOF ---
