<?php
// TCPDF FONT FILE DESCRIPTION
$type='TrueTypeUnicode';
$name='BuccaneerRegular';
$up=-133;
$ut=20;
$dw=800;
$diff='';
$originalsize=18576;
$enc='cp1250';
$file='buccanee.z';
$ctg='buccanee.ctg.z';
$desc=array('Flags'=>32,'FontBBox'=>'[-184 -348 1043 830]','ItalicAngle'=>0,'Ascent'=>830,'Descent'=>-348,'Leading'=>0,'CapHeight'=>811,'XHeight'=>490,'StemV'=>1,'StemH'=>0,'AvgWidth'=>234,'MaxWidth'=>1106,'MissingWidth'=>800);
$cw=array(0=>800,32=>500,34=>366,38=>1000,39=>222,44=>282,46=>282,58=>282,59=>282,65=>786,66=>658,67=>749,68=>784,69=>658,70=>567,71=>756,72=>714,73=>306,74=>686,75=>816,76=>700,77=>966,78=>868,79=>665,80=>616,81=>728,82=>840,83=>700,84=>792,85=>721,86=>756,87=>1106,88=>1056,89=>805,90=>791,96=>282,97=>602,98=>623,99=>486,100=>616,101=>560,102=>623,103=>658,104=>690,105=>300,106=>420,107=>728,108=>364,109=>1092,110=>714,111=>616,112=>651,113=>660,114=>420,115=>504,116=>448,117=>714,118=>654,119=>973,120=>648,121=>777,122=>516,160=>500,215=>0,253=>0,321=>0,65535=>800);
// --- EOF ---
