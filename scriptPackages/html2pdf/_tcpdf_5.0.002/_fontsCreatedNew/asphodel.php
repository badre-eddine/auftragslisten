<?php
// TCPDF FONT FILE DESCRIPTION
$type='TrueTypeUnicode';
$name='Asphodel';
$up=-133;
$ut=20;
$dw=500;
$diff='';
$originalsize=22356;
$enc='cp1250';
$file='asphodel.z';
$ctg='asphodel.ctg.z';
$desc=array('Flags'=>32,'FontBBox'=>'[-228 -135 8076 800]','ItalicAngle'=>0,'Ascent'=>800,'Descent'=>-200,'Leading'=>0,'CapHeight'=>759,'XHeight'=>323,'StemV'=>70,'StemH'=>30,'AvgWidth'=>292,'MaxWidth'=>1000,'MissingWidth'=>500);
$cw=array(0=>500,32=>471,34=>256,39=>158,44=>212,46=>268,48=>316,49=>164,50=>311,51=>307,52=>305,53=>287,54=>301,55=>288,56=>315,57=>321,58=>167,59=>212,65=>476,66=>502,67=>437,68=>517,69=>505,70=>486,71=>591,72=>672,73=>278,74=>227,75=>584,76=>308,77=>585,78=>638,79=>531,80=>489,81=>537,82=>575,83=>544,84=>427,85=>526,86=>452,87=>588,88=>565,89=>415,90=>485,96=>158,97=>279,98=>273,99=>283,100=>278,101=>282,102=>177,103=>295,104=>277,105=>133,106=>128,107=>284,108=>167,109=>465,110=>271,111=>240,112=>256,113=>280,114=>254,115=>284,116=>207,117=>279,118=>285,119=>416,120=>282,121=>273,122=>309,160=>610,215=>1000,65535=>0);
// --- EOF ---
