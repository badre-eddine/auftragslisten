<?php
// TCPDF FONT FILE DESCRIPTION
$type='TrueTypeUnicode';
$name='BusinessIndustrialDingbats';
$up=-24;
$ut=12;
$dw=750;
$diff='';
$originalsize=40144;
$enc='cp1250';
$file='busindng.z';
$ctg='busindng.ctg.z';
$desc=array('Flags'=>32,'FontBBox'=>'[0 0 2000 2000]','ItalicAngle'=>0,'Ascent'=>2000,'Descent'=>-5,'Leading'=>0,'CapHeight'=>625,'XHeight'=>625,'StemV'=>17,'StemH'=>7,'AvgWidth'=>0,'MaxWidth'=>750,'MissingWidth'=>750);
$cw=array(0=>750);
// --- EOF ---
