<?php
// TCPDF FONT FILE DESCRIPTION
$type='TrueTypeUnicode';
$name='CableDingbats';
$up=-32;
$ut=5;
$dw=750;
$diff='';
$originalsize=18368;
$enc='cp1250';
$file='cablding.z';
$ctg='cablding.ctg.z';
$desc=array('Flags'=>32,'FontBBox'=>'[0 -205 4385 916]','ItalicAngle'=>0,'Ascent'=>802,'Descent'=>-210,'Leading'=>0,'CapHeight'=>625,'XHeight'=>625,'StemV'=>17,'StemH'=>7,'AvgWidth'=>560,'MaxWidth'=>4404,'MissingWidth'=>750);
$cw=array(0=>750);
// --- EOF ---
