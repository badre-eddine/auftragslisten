<?php
// TCPDF FONT FILE DESCRIPTION
$type='TrueTypeUnicode';
$name='Angerthas-Moria';
$up=-91;
$ut=41;
$dw=819;
$diff='';
$originalsize=44104;
$enc='cp1250';
$file='angerthasmoria.z';
$ctg='angerthasmoria.ctg.z';
$desc=array('Flags'=>32,'FontBBox'=>'[0 -93 1084 819]','ItalicAngle'=>0,'Ascent'=>819,'Descent'=>-181,'Leading'=>34,'CapHeight'=>819,'XHeight'=>819,'StemV'=>29,'StemH'=>12,'AvgWidth'=>420,'MaxWidth'=>1124,'MissingWidth'=>819);
$cw=array(0=>819);
// --- EOF ---
