<?php
// TCPDF FONT FILE DESCRIPTION
$type='TrueTypeUnicode';
$name='Boneyard';
$up=-133;
$ut=20;
$dw=500;
$diff='';
$originalsize=32204;
$enc='cp1250';
$file='bo.z';
$ctg='bo.ctg.z';
$desc=array('Flags'=>32,'FontBBox'=>'[-4 -40 839 982]','ItalicAngle'=>0,'Ascent'=>982,'Descent'=>-200,'Leading'=>0,'CapHeight'=>976,'XHeight'=>800,'StemV'=>70,'StemH'=>30,'AvgWidth'=>0,'MaxWidth'=>1000,'MissingWidth'=>500);
$cw=array(0=>500,32=>500,65=>691,66=>440,67=>1000,68=>582,69=>616,70=>1000,71=>1000,72=>633,73=>1000,74=>1000,75=>1000,76=>579,77=>1000,78=>636,79=>821,80=>1000,81=>1000,82=>638,87=>855,89=>749,101=>1000,65535=>0);
// --- EOF ---
