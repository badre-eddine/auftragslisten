<?php
// TCPDF FONT FILE DESCRIPTION
$type='TrueTypeUnicode';
$name='Bizarro';
$up=-133;
$ut=20;
$dw=1003;
$diff='';
$originalsize=42208;
$enc='cp1250';
$file='bizz____.z';
$ctg='bizz____.ctg.z';
$desc=array('Flags'=>32,'FontBBox'=>'[0 0 941 1003]','ItalicAngle'=>0,'Ascent'=>1003,'Descent'=>-200,'Leading'=>0,'CapHeight'=>1000,'XHeight'=>1003,'StemV'=>70,'StemH'=>30,'AvgWidth'=>0,'MaxWidth'=>1003,'MissingWidth'=>1003);
$cw=array(0=>1003,32=>250,65=>737,66=>745,67=>721,68=>798,69=>790,70=>762,71=>824,72=>785,73=>325,74=>700,75=>677,76=>807,77=>792,78=>814,79=>880,80=>763,81=>799,82=>715,83=>697,84=>716,85=>810,86=>810,87=>883,88=>743,89=>780,90=>699,160=>250,255=>300,65535=>1003);
// --- EOF ---
