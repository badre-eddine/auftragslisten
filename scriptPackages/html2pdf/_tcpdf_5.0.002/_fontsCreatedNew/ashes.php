<?php
// TCPDF FONT FILE DESCRIPTION
$type='TrueTypeUnicode';
$name='CatScratchReverseItalic';
$up=-80;
$ut=50;
$dw=500;
$diff='';
$originalsize=100780;
$enc='cp1250';
$file='ashes.z';
$ctg='ashes.ctg.z';
$desc=array('Flags'=>96,'FontBBox'=>'[-135 -265 992 983]','ItalicAngle'=>0,'Ascent'=>817,'Descent'=>-182,'Leading'=>150,'CapHeight'=>817,'XHeight'=>817,'StemV'=>43,'StemH'=>18,'AvgWidth'=>540,'MaxWidth'=>1000,'MissingWidth'=>500);
$cw=array(0=>500);
// --- EOF ---
