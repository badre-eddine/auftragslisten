<?php
// TCPDF FONT FILE DESCRIPTION
$type='TrueTypeUnicode';
$name='Block-Out';
$up=-50;
$ut=50;
$dw=500;
$diff='';
$originalsize=24300;
$enc='cp1250';
$file='blockout.z';
$ctg='blockout.ctg.z';
$desc=array('Flags'=>32,'FontBBox'=>'[0 -200 1350 892]','ItalicAngle'=>-50,'Ascent'=>892,'Descent'=>-200,'Leading'=>0,'CapHeight'=>800,'XHeight'=>800,'StemV'=>70,'StemH'=>30,'AvgWidth'=>662,'MaxWidth'=>1400,'MissingWidth'=>500);
$cw=array(0=>500);
// --- EOF ---
