<?php
// TCPDF FONT FILE DESCRIPTION
$type='TrueTypeUnicode';
$name='Arsis';
$up=-27;
$ut=11;
$dw=750;
$diff='';
$originalsize=35564;
$enc='cp1250';
$file='ar_____.z';
$ctg='ar_____.ctg.z';
$desc=array('Flags'=>32,'FontBBox'=>'[-94 -198 639 869]','ItalicAngle'=>0,'Ascent'=>687,'Descent'=>-185,'Leading'=>0,'CapHeight'=>625,'XHeight'=>625,'StemV'=>17,'StemH'=>7,'AvgWidth'=>244,'MaxWidth'=>750,'MissingWidth'=>750);
$cw=array(0=>750);
// --- EOF ---
