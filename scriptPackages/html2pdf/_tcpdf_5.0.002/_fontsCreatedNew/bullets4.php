<?php
// TCPDF FONT FILE DESCRIPTION
$type='TrueTypeUnicode';
$name='bullets4';
$up=0;
$ut=0;
$dw=965;
$diff='';
$originalsize=89796;
$enc='cp1250';
$file='bullets4.z';
$ctg='bullets4.ctg.z';
$desc=array('Flags'=>32,'FontBBox'=>'[0 0 965 965]','ItalicAngle'=>0,'Ascent'=>965,'Descent'=>0,'Leading'=>35,'CapHeight'=>965,'XHeight'=>965,'StemV'=>34,'StemH'=>15,'AvgWidth'=>10,'MaxWidth'=>1013,'MissingWidth'=>965);
$cw=array(0=>965);
// --- EOF ---
