<?php
// TCPDF FONT FILE DESCRIPTION
$type='TrueTypeUnicode';
$name='arrows2';
$up=0;
$ut=0;
$dw=1000;
$diff='';
$originalsize=29268;
$enc='cp1250';
$file='arrows2.z';
$ctg='arrows2.ctg.z';
$desc=array('Flags'=>32,'FontBBox'=>'[44 37 955 955]','ItalicAngle'=>0,'Ascent'=>955,'Descent'=>37,'Leading'=>83,'CapHeight'=>909,'XHeight'=>909,'StemV'=>34,'StemH'=>15,'AvgWidth'=>85,'MaxWidth'=>1002,'MissingWidth'=>1000);
$cw=array(0=>1000);
// --- EOF ---
