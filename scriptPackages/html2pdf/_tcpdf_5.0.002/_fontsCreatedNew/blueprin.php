<?php
// TCPDF FONT FILE DESCRIPTION
$type='TrueTypeUnicode';
$name='BluePrint';
$up=-133;
$ut=20;
$dw=500;
$diff='';
$originalsize=23324;
$enc='cp1250';
$file='blueprin.z';
$ctg='blueprin.ctg.z';
$desc=array('Flags'=>32,'FontBBox'=>'[-83 -223 1013 917]','ItalicAngle'=>0,'Ascent'=>800,'Descent'=>-205,'Leading'=>0,'CapHeight'=>666,'XHeight'=>666,'StemV'=>1,'StemH'=>0,'AvgWidth'=>0,'MaxWidth'=>1000,'MissingWidth'=>500);
$cw=array(0=>500);
// --- EOF ---
