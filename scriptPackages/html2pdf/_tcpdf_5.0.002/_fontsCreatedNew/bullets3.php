<?php
// TCPDF FONT FILE DESCRIPTION
$type='TrueTypeUnicode';
$name='bullets3';
$up=0;
$ut=0;
$dw=1000;
$diff='';
$originalsize=22404;
$enc='cp1250';
$file='bullets3.z';
$ctg='bullets3.ctg.z';
$desc=array('Flags'=>32,'FontBBox'=>'[45 45 955 833]','ItalicAngle'=>0,'Ascent'=>833,'Descent'=>45,'Leading'=>212,'CapHeight'=>786,'XHeight'=>786,'StemV'=>34,'StemH'=>15,'AvgWidth'=>52,'MaxWidth'=>1002,'MissingWidth'=>1000);
$cw=array(0=>1000);
// --- EOF ---
