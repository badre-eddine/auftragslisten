<?php
// TCPDF FONT FILE DESCRIPTION
$type='TrueTypeUnicode';
$name='BenjaminCapsCaps001001';
$up=-26;
$ut=4;
$dw=874;
$diff='';
$originalsize=17892;
$enc='cp1250';
$file='becc__.z';
$ctg='becc__.ctg.z';
$desc=array('Flags'=>32,'FontBBox'=>'[-322 -639 2704 874]','ItalicAngle'=>0,'Ascent'=>874,'Descent'=>-639,'Leading'=>0,'CapHeight'=>800,'XHeight'=>874,'StemV'=>1,'StemH'=>0,'AvgWidth'=>0,'MaxWidth'=>1071,'MissingWidth'=>874);
$cw=array(0=>874,32=>490,48=>500,65=>850,66=>792,67=>876,68=>816,69=>851,70=>802,71=>885,72=>862,73=>112,74=>348,75=>856,76=>766,77=>1071,78=>801,79=>890,80=>798,81=>868,82=>784,83=>823,84=>911,85=>826,86=>786,87=>1029,88=>881,89=>803,90=>825,160=>61,65535=>874);
// --- EOF ---
