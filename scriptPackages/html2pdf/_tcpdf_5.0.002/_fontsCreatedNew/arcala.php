<?php
// TCPDF FONT FILE DESCRIPTION
$type='TrueTypeUnicode';
$name='TejaratchiCaps';
$up=-102;
$ut=15;
$dw=1050;
$diff='';
$originalsize=24156;
$enc='cp1250';
$file='arcala.z';
$ctg='arcala.ctg.z';
$desc=array('Flags'=>32,'FontBBox'=>'[0 -112 985 1050]','ItalicAngle'=>0,'Ascent'=>1050,'Descent'=>-250,'Leading'=>0,'CapHeight'=>770,'XHeight'=>1050,'StemV'=>70,'StemH'=>30,'AvgWidth'=>0,'MaxWidth'=>1050,'MissingWidth'=>1050);
$cw=array(0=>1050,32=>245,65=>588,66=>552,67=>501,68=>542,69=>434,70=>438,71=>526,72=>538,73=>236,74=>463,75=>535,76=>446,77=>725,78=>555,79=>542,80=>503,81=>555,82=>554,83=>524,84=>546,85=>501,86=>548,87=>785,88=>535,89=>524,90=>495,160=>500,255=>300,65535=>1050);
// --- EOF ---
