<?php
// TCPDF FONT FILE DESCRIPTION
$type='TrueTypeUnicode';
$name='Bilbo-handRegular';
$up=-100;
$ut=50;
$dw=500;
$diff='';
$originalsize=60956;
$enc='cp1250';
$file='bilbo.z';
$ctg='bilbo.ctg.z';
$desc=array('Flags'=>32,'FontBBox'=>'[-343 -548 906 830]','ItalicAngle'=>0,'Ascent'=>830,'Descent'=>-548,'Leading'=>9,'CapHeight'=>395,'XHeight'=>575,'StemV'=>70,'StemH'=>30,'AvgWidth'=>306,'MaxWidth'=>582,'MissingWidth'=>500);
$cw=array(0=>500,32=>277,33=>67,34=>147,35=>558,36=>301,37=>319,38=>371,39=>80,40=>139,41=>109,42=>273,43=>279,44=>56,45=>187,46=>97,47=>263,48=>368,49=>130,50=>430,51=>430,52=>435,53=>454,54=>278,55=>354,56=>349,57=>306,58=>89,59=>80,60=>277,61=>258,62=>286,63=>205,64=>582,65=>281,69=>370,79=>319,85=>373,91=>168,92=>303,93=>158,97=>297,98=>293,99=>385,100=>313,101=>366,102=>249,103=>311,104=>293,105=>156,106=>212,107=>335,108=>154,109=>462,110=>253,111=>329,112=>333,113=>329,114=>388,115=>323,116=>345,117=>361,118=>348,119=>398,120=>318,121=>295,122=>510,123=>230,124=>98,125=>178,57344=>0,57345=>500,57346=>500,65535=>500);
// --- EOF ---
