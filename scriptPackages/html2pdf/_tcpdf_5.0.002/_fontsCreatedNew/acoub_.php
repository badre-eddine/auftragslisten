<?php
// TCPDF FONT FILE DESCRIPTION
$type='TrueTypeUnicode';
$name='AcousticBass';
$up=-133;
$ut=20;
$dw=500;
$diff='';
$originalsize=30292;
$enc='cp1250';
$file='acoub_.z';
$ctg='acoub_.ctg.z';
$desc=array('Flags'=>32,'FontBBox'=>'[-23 -457 991 880]','ItalicAngle'=>0,'Ascent'=>875,'Descent'=>-453,'Leading'=>0,'CapHeight'=>924,'XHeight'=>598,'StemV'=>70,'StemH'=>30,'AvgWidth'=>646,'MaxWidth'=>1000,'MissingWidth'=>500);
$cw=array(0=>500,32=>402,33=>290,34=>321,35=>557,36=>533,37=>527,38=>564,39=>275,40=>320,41=>285,42=>566,43=>474,44=>383,45=>448,46=>1000,47=>588,48=>657,49=>602,50=>644,51=>519,52=>592,53=>706,54=>599,55=>634,56=>673,57=>620,58=>294,59=>300,61=>499,63=>467,64=>560,65=>794,66=>668,67=>652,68=>732,69=>725,70=>719,71=>682,72=>787,73=>551,74=>684,75=>762,76=>693,77=>947,78=>740,79=>660,80=>713,81=>682,82=>779,83=>693,84=>661,85=>751,86=>808,87=>819,88=>805,89=>818,90=>691,97=>650,98=>712,99=>631,100=>653,101=>615,102=>642,103=>598,104=>820,105=>626,106=>450,107=>732,108=>675,109=>790,110=>804,111=>682,112=>691,113=>682,114=>742,115=>745,116=>650,117=>836,118=>739,119=>854,120=>696,121=>686,122=>600,188=>451,189=>427,8208=>448,65535=>0);
// --- EOF ---
