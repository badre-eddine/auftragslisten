<?php
// TCPDF FONT FILE DESCRIPTION
$type='TrueTypeUnicode';
$name='BoinkoMatic';
$up=-133;
$ut=20;
$dw=500;
$diff='';
$originalsize=13064;
$enc='cp1250';
$file='boinm_.z';
$ctg='boinm_.ctg.z';
$desc=array('Flags'=>32,'FontBBox'=>'[-65 -159 1242 800]','ItalicAngle'=>0,'Ascent'=>800,'Descent'=>-200,'Leading'=>0,'CapHeight'=>677,'XHeight'=>800,'StemV'=>70,'StemH'=>30,'AvgWidth'=>0,'MaxWidth'=>1226,'MissingWidth'=>500);
$cw=array(0=>500,32=>500,65=>793,66=>744,67=>571,68=>811,69=>669,70=>615,71=>716,72=>790,73=>407,74=>557,75=>805,76=>623,77=>1000,78=>837,79=>701,80=>675,81=>695,82=>780,83=>583,84=>672,85=>814,86=>790,87=>1226,88=>808,89=>799,90=>664,65535=>0);
// --- EOF ---
