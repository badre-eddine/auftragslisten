<?php
// TCPDF FONT FILE DESCRIPTION
$type='TrueTypeUnicode';
$name='BeaniePlain';
$up=0;
$ut=0;
$dw=500;
$diff='';
$originalsize=50940;
$enc='cp1250';
$file='beanie.z';
$ctg='beanie.ctg.z';
$desc=array('Flags'=>32,'FontBBox'=>'[0 -340 1730 1255]','ItalicAngle'=>0,'Ascent'=>1078,'Descent'=>-345,'Leading'=>0,'CapHeight'=>667,'XHeight'=>667,'StemV'=>1,'StemH'=>0,'AvgWidth'=>0,'MaxWidth'=>1770,'MissingWidth'=>500);
$cw=array(0=>500);
// --- EOF ---
