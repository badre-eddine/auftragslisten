<?php
// TCPDF FONT FILE DESCRIPTION
$type='TrueTypeUnicode';
$name='3DLETBRK';
$up=-100;
$ut=50;
$dw=736;
$diff='';
$originalsize=60300;
$enc='cp1250';
$file='3dlet.z';
$ctg='3dlet.ctg.z';
$desc=array('Flags'=>32,'FontBBox'=>'[0 -1 726 731]','ItalicAngle'=>0,'Ascent'=>731,'Descent'=>-1,'Leading'=>9,'CapHeight'=>668,'XHeight'=>697,'StemV'=>70,'StemH'=>30,'AvgWidth'=>642,'MaxWidth'=>736,'MissingWidth'=>736);
$cw=array(0=>736,32=>500,33=>667,34=>736,35=>667,36=>667,37=>667,38=>667,40=>667,41=>667,42=>667,48=>671,49=>671,50=>671,51=>671,52=>671,53=>671,54=>671,55=>671,56=>671,57=>671,64=>667,65=>667,66=>667,67=>667,68=>667,69=>667,70=>667,71=>667,72=>667,73=>667,74=>667,75=>667,76=>667,77=>667,78=>667,79=>667,80=>667,81=>667,82=>667,83=>667,84=>667,85=>667,86=>667,87=>667,88=>667,89=>667,90=>667,94=>667,97=>671,98=>671,99=>671,100=>671,101=>671,102=>671,103=>671,104=>671,105=>671,106=>671,107=>671,108=>671,109=>671,110=>671,111=>671,112=>671,113=>671,114=>671,115=>671,116=>671,117=>671,118=>671,119=>671,120=>671,121=>671,122=>671,198=>667,230=>671,57344=>0,57345=>500,65535=>736);
// --- EOF ---
