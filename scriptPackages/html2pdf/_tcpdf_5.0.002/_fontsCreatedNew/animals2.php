<?php
// TCPDF FONT FILE DESCRIPTION
$type='TrueTypeUnicode';
$name='animals2';
$up=0;
$ut=0;
$dw=1000;
$diff='';
$originalsize=30280;
$enc='cp1250';
$file='animals2.z';
$ctg='animals2.ctg.z';
$desc=array('Flags'=>32,'FontBBox'=>'[-8 -10 603 603]','ItalicAngle'=>0,'Ascent'=>603,'Descent'=>-10,'Leading'=>388,'CapHeight'=>245,'XHeight'=>245,'StemV'=>34,'StemH'=>15,'AvgWidth'=>619,'MaxWidth'=>1000,'MissingWidth'=>1000);
$cw=array(0=>1000);
// --- EOF ---
