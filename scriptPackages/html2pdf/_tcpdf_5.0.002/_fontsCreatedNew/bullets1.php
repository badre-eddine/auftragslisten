<?php
// TCPDF FONT FILE DESCRIPTION
$type='TrueTypeUnicode';
$name='bullets1';
$up=0;
$ut=0;
$dw=1000;
$diff='';
$originalsize=38696;
$enc='cp1250';
$file='bullets1.z';
$ctg='bullets1.ctg.z';
$desc=array('Flags'=>32,'FontBBox'=>'[45 45 955 601]','ItalicAngle'=>0,'Ascent'=>601,'Descent'=>45,'Leading'=>444,'CapHeight'=>545,'XHeight'=>545,'StemV'=>34,'StemH'=>15,'AvgWidth'=>74,'MaxWidth'=>1002,'MissingWidth'=>1000);
$cw=array(0=>1000);
// --- EOF ---
