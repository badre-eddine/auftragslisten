<?php
// TCPDF FONT FILE DESCRIPTION
$type='TrueTypeUnicode';
$name='AbileneFLFfog_';
$up=-133;
$ut=20;
$dw=900;
$diff='';
$originalsize=59724;
$enc='cp1250';
$file='abilenf.z';
$ctg='abilenf.ctg.z';
$desc=array('Flags'=>32,'FontBBox'=>'[-50 -108 1050 901]','ItalicAngle'=>0,'Ascent'=>900,'Descent'=>-100,'Leading'=>0,'CapHeight'=>900,'XHeight'=>900,'StemV'=>2,'StemH'=>1,'AvgWidth'=>0,'MaxWidth'=>1200,'MissingWidth'=>900);
$cw=array(0=>900);
// --- EOF ---
