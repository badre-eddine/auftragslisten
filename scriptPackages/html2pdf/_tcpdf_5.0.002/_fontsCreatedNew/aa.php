<?php
// TCPDF FONT FILE DESCRIPTION
$type='TrueTypeUnicode';
$name='AarcoverRegular';
$up=-111;
$ut=17;
$dw=833;
$diff='';
$originalsize=27164;
$enc='cp1250';
$file='aa.z';
$ctg='aa.ctg.z';
$desc=array('Flags'=>32,'FontBBox'=>'[-18 -223 1312 917]','ItalicAngle'=>0,'Ascent'=>917,'Descent'=>-223,'Leading'=>0,'CapHeight'=>833,'XHeight'=>833,'StemV'=>1,'StemH'=>0,'AvgWidth'=>0,'MaxWidth'=>1317,'MissingWidth'=>833);
$cw=array(0=>833,32=>330,33=>367,34=>371,36=>404,39=>204,44=>192,45=>538,46=>150,47=>429,48=>628,49=>490,50=>805,51=>701,52=>708,53=>757,54=>751,55=>643,56=>782,57=>677,58=>267,59=>300,63=>529,65=>620,66=>632,67=>598,68=>607,69=>726,70=>810,71=>623,72=>741,73=>288,74=>665,75=>637,76=>582,77=>800,78=>725,79=>620,80=>615,81=>628,82=>661,83=>860,84=>725,85=>636,86=>653,87=>810,88=>725,89=>589,90=>782,160=>330,177=>549,181=>576,247=>549,376=>1317,931=>713,937=>768,960=>549,8216=>204,8217=>204,8220=>371,8221=>371,8706=>494,8710=>612,8719=>823,8730=>549,8734=>713,8747=>274,8776=>549,8800=>549,8804=>549,8805=>549,9674=>494,65535=>833);
// --- EOF ---
