<?php
// TCPDF FONT FILE DESCRIPTION
$type='TrueTypeUnicode';
$name='Calaveras';
$up=-133;
$ut=20;
$dw=500;
$diff='';
$originalsize=458224;
$enc='cp1250';
$file='calavera323.z';
$ctg='calavera323.ctg.z';
$desc=array('Flags'=>32,'FontBBox'=>'[-12 -199 1375 896]','ItalicAngle'=>0,'Ascent'=>896,'Descent'=>-200,'Leading'=>0,'CapHeight'=>1001,'XHeight'=>800,'StemV'=>70,'StemH'=>30,'AvgWidth'=>0,'MaxWidth'=>1369,'MissingWidth'=>500);
$cw=array(0=>500,32=>500,65=>1141,66=>1369,67=>562,68=>712,69=>649,70=>580,71=>729,72=>1000,73=>703,74=>397,75=>725,76=>370,77=>707,78=>784,79=>581,80=>945,81=>676,82=>394,83=>743,84=>665,85=>1214,86=>627,87=>867,88=>661,89=>623,65535=>500);
// --- EOF ---
