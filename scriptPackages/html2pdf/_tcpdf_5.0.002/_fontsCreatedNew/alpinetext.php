<?php
// TCPDF FONT FILE DESCRIPTION
$type='TrueTypeUnicode';
$name='AlpineText';
$up=-155;
$ut=101;
$dw=365;
$diff='';
$originalsize=4116;
$enc='cp1250';
$file='alpinetext.z';
$ctg='alpinetext.ctg.z';
$desc=array('Flags'=>32,'FontBBox'=>'[-103 -9 544 732]','ItalicAngle'=>0,'Ascent'=>732,'Descent'=>-9,'Leading'=>184,'CapHeight'=>667,'XHeight'=>667,'StemV'=>34,'StemH'=>15,'AvgWidth'=>468,'MaxWidth'=>546,'MissingWidth'=>365);
$cw=array(0=>365,32=>225,97=>546,98=>546,99=>546,100=>546,101=>546,102=>546,104=>546,105=>270,108=>265,110=>546,111=>488,65535=>365);
// --- EOF ---
