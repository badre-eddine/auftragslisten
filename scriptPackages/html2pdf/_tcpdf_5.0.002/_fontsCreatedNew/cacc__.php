<?php
// TCPDF FONT FILE DESCRIPTION
$type='TrueTypeUnicode';
$name='CarrickCapsCaps001001';
$up=-22;
$ut=3;
$dw=1003;
$diff='';
$originalsize=43012;
$enc='cp1250';
$file='cacc__.z';
$ctg='cacc__.ctg.z';
$desc=array('Flags'=>32,'FontBBox'=>'[0 -1 941 1003]','ItalicAngle'=>0,'Ascent'=>1003,'Descent'=>-1,'Leading'=>0,'CapHeight'=>996,'XHeight'=>1003,'StemV'=>1,'StemH'=>0,'AvgWidth'=>0,'MaxWidth'=>1003,'MissingWidth'=>1003);
$cw=array(0=>1003,32=>544,48=>500,65=>907,66=>897,67=>907,68=>892,69=>916,70=>904,71=>911,72=>914,73=>936,74=>936,75=>883,76=>875,77=>979,78=>880,79=>914,80=>869,81=>901,82=>893,83=>912,84=>895,85=>892,86=>914,87=>959,88=>917,89=>909,90=>903,160=>61,65535=>1003);
// --- EOF ---
