<?php
/*************************************************************************
* CREATE FONTS FOR TCPDF
*
* LICENSE: This source file is subject to version 3.0 of the PHP license
* that is available through the world-wide-web at the following URI:
* http://www.php.net/license/3_0.txt.  If you did not receive a copy of
* the PHP License and are unable to obtain it through the web, please
* send a note to license@php.net so we can mail you a copy immediately.
*
* @category   NUMBER PLATES EDITOR
* @package    PackageName
* @author     THORSTEN HOFF <info@thorsten-hoff.de>
* @copyright  2012 THORSTEN HOFF
* @license    http://www.php.net/license/3_0.txt  PHP License 3.0
* @version    1.0
*************************************************************************/

	set_time_limit(0);
	
	// TIMEZONE / PHP_INI
	if(!defined("SET_DATE_DEFAULT_TIMEZONE")) { DEFINE('SET_DATE_DEFAULT_TIMEZONE', 'Europe/Berlin'); }
	if(!defined("LC_ALL")) { DEFINE('LC_ALL', '"de_DE@euro", "de_DE", "de", "ge"'); }
	
	date_default_timezone_set(SET_DATE_DEFAULT_TIMEZONE);
	setlocale(LC_ALL, constant("LC_ALL"));
	setlocale(LC_TIME, constant("LC_ALL"));
	
	// CONFIG	
	define('PATH_DOCUMENT_ROOT', $_SERVER["DOCUMENT_ROOT"] . '/makeFont/');
	define('PATH_FONTS_TO_CONVERT_DIR', 'fontsToConvert/');
	#define('PATH_FONTS_TO_CONVERT_DIR', 'C:/Windows/Fonts/');
	#define('PATH_FONTS_TO_CONVERT_DIR', 'V:/BACK-UPs/Fonts/');
	define('PATH_FONTS_CONVERTED', 'fontsConverted/');
	define('PATH_INCLUDEDTTF_FONTS', 'includedTtfFonts/');
	#define(FONTS_DIR, PATH_DOCUMENT_ROOT . PATH_FONTS_TO_CONVERT_DIR);
	define(FONTS_DIR, PATH_FONTS_TO_CONVERT_DIR);
	define('PATH_TTF_INFO_CLASS', 'lib/ttfinfoplus/ttfInfo.class.php');
	define('HTML2PDF_TCPDF_VERSION_ADD_FONTS', '5_9_179');
	define('PATH_HTMLTOPDF', 'lib/html2pdf/');
	define('PATH_TCPDF_FONTS_DIR', PATH_HTMLTOPDF . '_tcpdf_' . HTML2PDF_USED_TCPDF_VERSION . '/fonts/');	
	define('PATH_TCPDF_ADD_FONTS', PATH_HTMLTOPDF . '_tcpdf_' . HTML2PDF_TCPDF_VERSION_ADD_FONTS . '_MOD_ADDFONT/' . 'tcpdf.php');
	define('PATH_CSS_FONTS', 'fontsConvertedCss/fonts.css');
	
	#define('CONVERSION_CHARSET', '');
	define('CONVERSION_CHARSET', 'cp1250');
	
	function read_dir($dir) {
		if(preg_match("/\/$/", $dir)) {
			$dir = substr($dir, 0, (strlen($dir) - 1));
		}
		$array = array();
		$d = dir($dir);
		while (false !== ($entry = $d->read())) {
			if($entry!='.' && $entry!='..')
			{
				$entry = $dir.'/'.$entry;

				$filePath = addslashes($entry);
				$fileName = basename(stripslashes($filePath));
				
				if(!preg_match("/^[\._]/", $fileName)){
					// echo "1:filePath: ".$filePath."<br>";
					// echo "fileName: ".$fileName."<br>";

					if(is_dir($entry))
					{
						$array[] = $entry;
						$array = array_merge($array, read_dir($entry));
					}
					else
					{
						$array[] = $entry;
					}
				}
				clearstatcache();
			}
		}
		$d->close();
		return $array;
	}
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<HTML lang="de" xml:lang="de" xmlns="http://www.w3.org/1999/xhtml">
<head>
	<title>Make Font</title>	
</head>

<body>
	<div id="pageWrapper">		
		<div class="contentWrapper">
			<div class="contentArea">
				<h1>Make Font</h1>
				
				<?php
					// BOF GET FONTS
						$arrGetFonts = read_dir(PATH_FONTS_TO_CONVERT_DIR);
					
						if(!empty($arrGetFonts)) {
							$arrUsedFonts = array();
							foreach($arrGetFonts as $thisFont) {
								#$arrUsedFonts[] = basename(strtolower(trim($thisFont)));
								$arrUsedFonts[] = basename((trim($thisFont)));
							}
							$arrUsedFonts = array_unique($arrUsedFonts);
						}
					// BOF GET FONTS
					
					//BOF READ FONT-DIRECTORY
						require_once(PATH_TTF_INFO_CLASS);						
						
echo 'arrUsedFonts<pre>';
print_r($arrUsedFonts);								
echo '</pre>';
echo 'FONTS_DIR: ' . htmlentities(FONTS_DIR) . "<hr>";

								
								
						$fileInfo = new ttfInfo();
						//$fileInfo->setFontFile(FONTS_DIR . '/' . $file);
						//$fileInfo->getFontInfo();
						
						$fileInfo->setFontsDir(FONTS_DIR);
						$fontDatas = $fileInfo->readFontsDir();
						
						$fontDataFields = array(
							0 => 'Copyright',
							1 => 'Font Family',
							2 => 'Font Subfamily',
							3 => 'Unique identifier',
							4 => 'Full name',
							5 => 'Version',
							6 => 'Postscript name'
						);

						$arrIncludeCssFonts = array();
						
echo 'fontDatas->array<pre>';
print_r($fontDatas->array);								
echo '</pre>';

						foreach($fontDatas->array AS $key => $var)
						{
							$file = basename($key);
							$path_parts = pathinfo($file);
echo 'path_parts<pre>';
print_r($path_parts);								
echo '</pre>';							
							$file_ext = strtolower($path_parts['extension']);
							$file_name = ($path_parts['filename']);
							echo 'file: ' . ($file) . "<hr>";
							if ($file_ext == 'ttf') {
								#if(in_array(strtolower($var[1]), $arrUsedFonts)) {
								if(in_array(($file), $arrUsedFonts) && $var[1] != "") {
									echo 'Schriftart <b>' . $var[1] . '</b> wird im Editor verwendet.' . '<br />';
									
									if(!empty($var)){
										foreach ($var AS $row => $data)
										{
											echo isset($fontDataFields[$row])?$fontDataFields[$row]:$row, ':', $data . '<br />';
										}
									}
									
									echo '<b>PFAD</b>: ' . $key . '<br />';						
									echo '<b>DATEI</b>: ' . $file . '<br />';
									echo '<b>EXT</b>: ' . $file_ext . '<br />';
									echo "<b>FONT-FAMILY</b>: " . $var[1] . '<br />';
									
									/*
										* Convert all font filenames to lowercase and rename using the following schema:
										* [basic-font-name-in-lowercase].ttf for regular font
										* [basic-font-name-in-lowercase]b.ttf for bold variation
										* [basic-font-name-in-lowercase]i.ttf for oblique variation
										* [basic-font-name-in-lowercase]bi.ttf for bold oblique variation
										
										TCPDF-FONT-FILE: 
											- fontfamily.ctg.z
											- fontfamily.php
											- fontfamily.z									  
									*/									
									
									#$tcpdfFileName = preg_replace('/ /', '', strtolower($var[1])); //  . '.' . $file_ext
									$tcpdfFileName = preg_replace('/ /', '', strtolower($file_name));
									
									echo '<b>TCPDF FONT Name</b>: ' . $tcpdfFileName . '<br />';
									
									// BOF CHECK TTF FONTS
									if(file_exists(PATH_INCLUDEDTTF_FONTS . $file)) {
										echo 'Die Schriftdatei <b>' . $file . '</b> ist inkludiert' . '<br />';
									}
									else {
										echo 'Die Schriftdatei <b>' . $file . '</b> ist nicht inkludiert' . '<br />';
										
										// $copyResult = copy(FONTS_DIR .  $file, PATH_INCLUDEDTTF_FONTS . $file);
										$copyResult = copy(FONTS_DIR .  $file, PATH_INCLUDEDTTF_FONTS . $tcpdfFileName . '.' . $file_ext);
										#$copyResult = copy(FONTS_DIR .  $file, PATH_INCLUDEDTTF_FONTS . $tcpdfFileName);
										if($copyResult) {
											echo 'Die Schriftdatei <b>' . $file . '</b> wurde inkludiert' . '<br />';
											
											// @font-face { font-family:Garamond; src:url(garamond.eot), url(garamond.pfr); }
											// @font-face { font-family:Garamond; src:url(garamond.ttf) format('truetype'); }
											// font-style: ' . $fontDataFields[2] . '; font-weight: bold; 
											$arrIncludeCssFonts[] = '@font-face { font-family:' . strtolower($var[1]) . '; src:url(' . '../' . PATH_INCLUDEDTTF_FONTS . $tcpdfFileName . '.' . $file_ext . ');';
										}
										else {
											echo 'Die Schriftdatei <b>' . $file . '</b> konnte nicht inkludiert werden' . '<br />';
										}
									}
									// EOF CHECK TTF FONTS
									
									// BOF CHECK TCPDF FONTS
									if(file_exists(PATH_TCPDF_FONTS_DIR . $tcpdfFileName . 'ctg.z') && file_exists(PATH_TCPDF_FONTS_DIR . $tcpdfFileName . 'php') && file_exists(PATH_TCPDF_FONTS_DIR . $tcpdfFileName . 'z')) {
										echo 'Die TCPFF-Schriftdateien existieren' . '<br />';
									}
									else {
										echo 'Die TCPFF-Schriftdateien existieren noch nicht und m&uuml;ssen generiert werden' . '<br />';
										
										require_once(PATH_TCPDF_ADD_FONTS);
										$fontToAdd = new TCPDF();
										// addTTFfont($fontfile, $fonttype='', $enc='', $flags=32, $outpath='', $platid=3, $encid=1)								
										// $fontToAdd->addTTFfont($key);
										// $result_fontName = $fontToAdd->addTTFfont($key, '', '', 32, PATH_FONTS_CONVERTED);
										#$result_fontName = $fontToAdd->addTTFfont(PATH_INCLUDEDTTF_FONTS . $tcpdfFileName . '.' . $file_ext, '', '', 32, PATH_FONTS_CONVERTED);
										$result_fontName = $fontToAdd->addTTFfont(PATH_INCLUDEDTTF_FONTS . $tcpdfFileName . '.' . $file_ext, '', CONVERSION_CHARSET, 32, PATH_FONTS_CONVERTED);
									}
									// EOF CHECK TCPDF FONTS
									echo '<hr /><br />';
								}
								else  {
									echo 'Schriftart <b>' . $var[1] . '</b> wird nicht im Editor verwendet.' . '<br />';
								}								
							}	
						}
						
						// $result_fontName = $this->addTTFfont(PATH_DOCUMENT_ROOT . PATH_FONTS_TO_CONVERT_DIR . $file);							
							
						//BOF READ FONT-DIRECTORY		
						
						// BOF WRITE FONTS-CSS
							if(!empty($arrIncludeCssFonts)) {
								
								$arrIncludeCssFonts = array_unique($arrIncludeCssFonts);
								
								$fileContent = '@charset "utf-8";' . "\n" . implode("\n", $arrIncludeCssFonts);
								
								$fp = fopen(PATH_CSS_FONTS, "w");
								$writeResult = fwrite($fp, $fileContent);
								fclose($fp);

								if($writeResult){
									echo 'Die Datei ' . PATH_CSS_FONTS . ' wurde generiert.' . '<br />';
								}
								else {
									echo 'Die Datei ' . PATH_CSS_FONTS . ' konnte nicht generiert werden .' . '<br />';
								}								
							}
						// EOF WRITE FONTS-CSS
					
				?>
				
			</div>
		</div>		
	</div>
	
</body>

</html>