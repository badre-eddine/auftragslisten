<?php
/*
	This is an example of how to use this class as an included class.
	it will return an array of all information available in the 'name' table as an array.
*/
include 'ttfInfo.class.php';

echo '
<html>
<head>
	<link rel="stylesheet" href="ttfinfo.class.css" type="text/css" media="all" />
<head>
<body>
';

echo '<h1>Array dump</h1><pre>';

	// call the ttfInfo class
	$ttfInfo = new ttfInfo;
	// Set the Font resource
	$ttfInfo->setFontFile('GuinevereCaps.ttf');
	// dump the info
	print_r($ttfInfo->getFontInfo());

$ntable = array(
	0 => 'Copyright',
	1 => 'Font Family',
	2 => 'Font Subfamily',
	3 => 'Unique identifier',
	4 => 'Full name',
	5 => 'Version',
	6 => 'Postscript name'
);

echo '<h1>Formatted info</h1>
<div class="container">
	<div class="float">',$ttfInfo->filename,'</div>
	';

	// call the ttfInfo class
	//$ttfInfo2 = new ttfInfo;
	// Set the Font resource
	//$ttfInfo2->setFontFile('Fingerprint-Oblique.ttf');
	// dump the info

	foreach ($ttfInfo->getFontInfo() AS $key => $data)
	{
		echo '<div class="floatl">',isset($ntable[$key])?$ntable[$key]:$key, ':</div><div class="floatr">', $data,'</div>
	';
	}

echo '
<div class="spacer">&nbsp;</div>
</div>
</body>
</html>';
?>