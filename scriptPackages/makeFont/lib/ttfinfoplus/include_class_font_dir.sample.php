<?php
/*
	This is an example of how to use this class as an included class.
	this file diminstrats how to read a directory of fonts and format it.
*/
include 'ttfInfo.class.php';

echo '
<html>
<head>
	<link rel="stylesheet" href="ttfinfo.class.css" type="text/css" media="all" />
<head>
<body>
';
$ntable = array(
	0 => 'Copyright',
	1 => 'Font Family',
	2 => 'Font Subfamily',
	3 => 'Unique identifier',
	4 => 'Full name',
	5 => 'Version',
	6 => 'Postscript name'
);

	// call the ttfInfo class
	$ttfInfo = new ttfInfo;
	// Set the Font resource
	$ttfInfo->setFontsDir('G:/NET.SERVER/home/NuFont/');
	$ttfInfo->readFontsDir();
	
	//print_r($ttfInfo->array);

echo '<div class="container">
';

	foreach($ttfInfo->array AS $key => $var)
	{
		echo '
	<div class="float">',$key,"</div>\n";

		foreach ($var AS $row => $data)
		{
			echo '	<div class="floatl">',isset($ntable[$row])?$ntable[$row]:$row, ':</div><div class="floatr">', $data,"</div>\n";
		}

		echo '
<div class="spacer">&nbsp;</div>
';
	}

echo '
</div>
</body>
</html>';
?>