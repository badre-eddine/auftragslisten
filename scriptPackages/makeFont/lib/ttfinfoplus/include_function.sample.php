<?php
/*
	This is an example of how to use this class as an included fuction, and leave the class alone.
	it will return an array of all information available in the 'name' table as an array.
*/
include 'ttfInfo.class.php';

echo '<h1>Array dump</h1><pre>';
$fontinfo = getFontInfo('Fingerprint-Oblique.ttf');
print_r($fontinfo);
echo '</pre>';
?>