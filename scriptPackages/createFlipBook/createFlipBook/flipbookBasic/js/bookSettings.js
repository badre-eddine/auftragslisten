/*
flippingBook.pages = [
	"flip/pages/001.jpg"	
];
*/


flippingBook.contents = [
	
	[ "Titelblatt", 1 ],
	[ "Kennzeichenhalter", 2 ],
	[ "Miniletter", 4 ],
	[ "Kombitaschen", 5 ],
	[ "Fussmatten", 6 ],
	[ "Werkstattbedarf", 8 ],
	[ "Reinigungsmittel", 12 ],
	[ "Lasergravur", 14 ],
	[ "Bestellschein", 15 ],
	[ "Kontakt", 16 ]
	
];

// define custom book settings here
// flippingBook.settings.pagesSet = 15;
flippingBook.settings.bookWidth = 1000;
flippingBook.settings.bookHeight = 708;
flippingBook.settings.zoomImageWidth = 1300;
flippingBook.settings.zoomImageHeight = 1840;
flippingBook.settings.useCustomCursors = false;
flippingBook.settings.dropShadowEnabled = false;
flippingBook.settings.flipSound = "flip/sounds/02.mp3";
flippingBook.settings.flipCornerStyle = "first page only";
flippingBook.settings.zoomHintEnabled = true;
flippingBook.settings.zoomPath = "flip/pages/large/";

flippingBook.settings.hardcover = false;
flippingBook.settings.downloadURL = "http://www.burhan-ctr.de/pdf-kataloge/german/Gesamtkatalog_Ohne-Preise.pdf";
flippingBook.settings.downloadSize = "Gr��e: 4.7 Mb";
flippingBook.settings.preloaderType = "Thin"; // "Progress Bar", "Round", "Thin", "Dots", "Gradient Wheel", "Gear Wheel", "Line", "Animated Book", "None"

// default settings can be found in the flippingbook.js file
flippingBook.create();
