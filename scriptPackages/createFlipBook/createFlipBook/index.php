<?php	
	$debug = false;
	error_reporting("E_STRICT");
	set_time_limit(0);
	require_once('inc/config.flipBook.inc.php');	
	require_once('inc/flipBook.class.php');	
	require_once('inc/resizeImages.class.php');	
	
	$jswindowMessage = "";
	$warningMessage = '';
	$errorMessage = '';
	$successMessage = '';
	$infoMessage = '';
		
	if($_POST["cancelConverting"] != "") {
		unset($_POST);
	}
	
	function convertArrayToHTML($arr, $baseCode) {		
		global $k, $arrConfigFlipbook;
		$i = 0;
		foreach($arr as $thisKey => $thisValue) {			
			if(is_array($thisValue)) {
				$k = $i;
				echo '<div class="imageDir" id="imageDir_' . $k . '"><input type="checkbox" class="imageDir_'.$k.'" name="selectUseDir['.$thisKey.']" value="1" /> ' . ' Ordner ' . $k . ': ' . $thisKey . '</div>';
				convertArrayToHTML($thisValue, $baseCode);
			}
			else {
				#echo '<div class="imageFile" >' .$k. '<input type="checkbox" id="imageFile_' . $k . '_' . $i . '"class="imageDir_'.$k.'" name="selectUseFile['.$thisValue.']" value="1" /> <img src="' . preg_replace("/".preg_replace("/\//", "\/", DOCUMENT_ROOT)."/", "../", $thisValue) . '" width="48" height="24" alt="' . basename($thisValue) . '"/>' . ' Bild ' . $i . ': ' . basename($thisValue) . '</div>';
				echo '<div class="imageFile" >' .$k. '<input type="checkbox" id="imageFile_' . $k . '_' . $i . '"class="imageDir_'.$k.'" name="selectUseFile['.$thisValue.']" value="1" /> <img src="' . $thisValue . '" width="48" height="24" alt="' . basename($thisValue) . '"/>' . ' Bild ' . $i . ': ' . basename($thisValue) . '</div>';
			}
			$i++;			
		}
	}
	
	function copy_directory( $source, $destination ) {
		if(is_dir($source)) {
			@mkdir( $destination );
			$directory = dir( $source );
			while(FALSE !== ($readdirectory = $directory->read())) {
				if($readdirectory == '.' || $readdirectory == '..') {
					continue;
				}
				$PathDir = $source . '/' . $readdirectory; 
				if(is_dir( $PathDir)) {
					copy_directory($PathDir, $destination . '/' . $readdirectory);
					continue;
				}
				copy($PathDir, $destination . '/' . $readdirectory);
			}	 
			$directory->close();
		}
		else {
			copy($source, $destination);
		}
	}
	
	function readDirectory($dir, $searchSubDirs=false) {
		$listDir = array();
		if($handler = opendir($dir)) {
			while (($sub = readdir($handler)) !== FALSE) {
				if ($sub != "." && $sub != ".." && $sub != "Thumb.db") {
					if(is_file($dir."/".$sub)) {
						// $listDir[] = $sub;
						$listDir[] = $dir."/".$sub;
					}
					elseif(is_dir($dir."/".$sub)){
						if($searchSubDirs) {
							$listDir[$sub] = readDirectory($dir."/".$sub, $searchSubDirs);
						}
					}
				}
			}   
			closedir($handler);
		}
		return $listDir; 
	}
	
	if($_POST["actionType"] == "1" && $_POST["submitUploadFile"] != "") {
		if($_FILES["selectUploadFile"]["name"] != "") {
			
			// if($_FILES["selectUploadFile"]["type"] == "application/pdf") {
			if(1) {
				// application/download
				
				if(copy($_FILES["selectUploadFile"]["tmp_name"], DIRECTORY_PDF_FILES . $_FILES["selectUploadFile"]["name"])) {
					$successMessage .= 'Die PDF-DATEI wurde kopiert nach &quot;' . DIRECTORY_PDF_FILES . '&quot; ' . '<br />';
					$_POST["actionType"] = "2";
				}
				else {
					$errorMessage .= 'Die PDF-DATEI konnte nicht nach &quot;' . DIRECTORY_PDF_FILES . '&quot; kopiert werden.' . '<br />';
				}
			}
			else {
				$errorMessage .= 'Die hochgeladene Datei ist keine PDF-DATEI' . '<br />';
			}
		}
		else {
			$errorMessage .= 'Es wurde keine PDF-DATEI hochgeladen.' . '<br />';
		}
		// $arrConfigFlipbook["DIRECTORY_PDF_FILES"]
	}	
	
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<title>ADMIN FLIP-Katalog</title>
	<link rel="stylesheet" href="css/layout.css" />
	<script type="text/javascript" language="javascript" src="js/functions.js" ></script>
	<script type="text/javascript" language="javascript" src="../scriptPackages/jquery-1.7.2.min.js" ></script>
</head>

<body>
	<div id="pageArea">
		<div id="documentArea">
			<div id="headerArea">	
				<div id="headerAreaContent">	
					<h1>Generierung eines Bl&auml;tter-Kataloges aus einem PDF oder Bildern</h1>
				</div>
			</div>
			<div class="contentArea">	
				<?php if($_POST["actionType"] == "") { ?>	
					<div id="actionTypeMenueArea">
						<form name="formActionType" method="post" action="<?php echo $_SERVER["PHP_SELF"]; ?>" >
						<div class="actionTypeItem"><input type="radio" id="actionType1" name="actionType" value="1" /> <label for="actionType1">PDF-Datei hochladen und konvertieren</label></div>
						<div class="actionTypeItem"><input type="radio" id="actionType2" name="actionType" value="2" /> <label for="actionType2">Existierende PDF-Datei konvertieren</label></div>
						<div class="actionTypeItem"><input type="radio" id="actionType3" name="actionType" value="3" /> <label for="actionType3">Bilder-Ordner verwenden</label></div>
						<div class="actionTypeItem"><input type="submit" name="submitActionType" value="Weiter" /></div>
						<div class="clear"></div>
						</form>
					</div>
				<?php } ?>
				<?php		
					// BOF ACTION TYPE 1 : UPLOAD PDF FOR CONVERTING
						if($_POST["actionType"] == "1") {			
							echo '
								<div id="actionTypeMenueArea">
									<form name="formUploadFile" method="post" action="' . $_SERVER["PHP_SELF"] . '" enctype="multipart/form-data" >
										PDF-Datei: <input type="file" name="selectUploadFile" />
										<input type="hidden" name="actionType" value="' . $_POST["actionType"] . '" />
										<input type="submit" name="submitUploadFile" value="Hochladen" />
									</form>
								</div>
							';			
						}
					// EOF ACTION TYPE 1 : UPLOAD PDF FOR CONVERTING		
					
					// BOF ACTION TYPE 2 OR 3 : CREATE SETTINGS	
						if($_POST["startConverting"] == "" && $_POST["actionType"] != "") {				
							echo '<form name="formCreateSettings" method="post" action="'.$_SERVER["PHP_SELF"].'" >';
							
							// BOF ACTION TYPE 3 : USE SELECTED IMAGES
								if($_POST["actionType"] == "3") {
									// $arrConfigFlipbook["DIRECTORY_GALLERY_IMAGES"]
									echo '<h2>Bildauswahl aus dem Ordner : ' . DIRECTORY_GALLERY_IMAGES . '</h2>';
									$arrDirImages = readDirectory(DIRECTORY_GALLERY_IMAGES, true);						
									echo '							
											<div class="imageItemsArea">
									';		
												if(!empty($arrDirImages)) {					
													convertArrayToHTML($arrDirImages, '');
												}			
									echo '		
											</div>
											<input type="hidden" name="actionType" value="' . $_POST["actionType"] . '" />						
									';
								}
							// EOF ACTION TYPE 3 : USE SELECTED IMAGES				
							
							// BOF SET PARAMS
								if(!empty($arrParamFlipbook)) {	
								
									$c = 1;
									
									echo '<div class="configItemsArea">';
										echo '<h2>Parameter</h2>';					
										echo '<div class="configItemsContent">';
										echo '<table border="0" cellspacing="0" cellpadding="0" width="100%">';			
										
										if($_POST["actionType"] == "2") {							
											echo '<tr>
													<td style="width:200px;"><strong>PDF-Dateien</strong></td>
													<td>
														<select name="SELECTED_PDF_FILE" class="inputSelect1" onchange="setPdfFilePath(\'' . $pathPDFs . '\');">
															<option value=""> --- Bitte w&auml;hlen --- </option>
												';									
											$arrPdfFiles = readDirectory(DIRECTORY_PDF_FILES, false);
											if(!empty($arrPdfFiles)) {
												foreach($arrPdfFiles as $thisPdfFile) {
													echo '<option value="'.basename($thisPdfFile).'"> '.basename($thisPdfFile).' </option>';
												}
											}								
											echo '
														</select>
													</td>
												</tr>';
										}
										
										if($_POST["actionType"] != "1") {
											foreach ($arrParamFlipbook as $thisKey => $thisValue) {					
												if($c % 2 == 0) { $class = "row1"; }
												else { $class = "row2"; }
												
												if(in_array($thisKey, $arrReadonlyParams)){ $thisReadonly = "readonly"; }
												else { $thisReadonly = ""; }
												
												echo '<tr class="'.$class.'">
														<td><strong>'.$thisKey.'</strong></td>
														<td><input class="inputField1" type="text" name="'.$thisKey.'" value="'.$thisValue.'" '.$thisReadonly.' /></td>
													</tr>';
												$c++;									
											}
										}
									
										echo '</table>';
										echo '</div>';
									echo '</div>';						
									echo '<div class="clear"></div>';
									
									echo '<input type="hidden" name="actionType" value="' . $_POST["actionType"] . '" />';
									echo '<div class="actionArea">';
										echo '<input type="submit" name="startConverting" value="FlipBook erstellen" />';	
										echo ' ';
										echo '<input type="submit" name="cancelConverting" value="Abbrechen" />';
									echo '</div>';
								}
							// EOF SET PARAMS
							echo '</form>';	
						}
					// EOF ACTION TYPE 2 OR 3 : CREATE SETTINGS	
					
					// BOF START CONVERTING PDF EXTRACTING IMAGES		
						if($_POST["startConverting"] != "" && $_POST["actionType"] == 2) {				
							flush();
							
							// BOF START CREATING PAGE IMAGES			
								$_POST["PDF_FILE"] = $_POST["SELECTED_PDF_FILE"];
														
								$bookPdf = new flipBook(); 	
								
								$bookPdf->setConvertParams($_POST);
								$bookPdf->createFlipBookFolder(PATH_CREATED_FLIPBOOKS, $_POST["PATH_NEW_CREATED_FLIPBOOK"]);					
								// $bookPdf->displayConvertParams();						
								$bookPdf->convertPdf2Jpg(0);
								// $bookPdf->displayConvertPdfCommands();
								// $bookPdf->checkImagePageName(78);
								// $bookPdf->displayFilenames();
								$bookPdf->displayConvertStatus();			
								$bookFileLinks = $bookPdf->createFlipbookLinks();
								$getFlipBookFolder = $bookPdf->getFlipBookFolder();
								
								echo '<hr />';								
								echo '<p><a href="' . $getFlipBookFolder . $bookFileLinks["filePathBook"] . '" target="_blank">' . basename($bookFileLinks["filePathBook"]). '</a>' . '<br /></p>';				
							// EOF START CREATING PAGE IMAGES
						}
					// EOF START CONVERTING PDF EXTRACTING IMAGES
					
					// BOF CONVERTING AND COPYING SELECTED IMAGES
						if($_POST["startConverting"] != "" && $_POST["actionType"] == 3) {				
							if(!empty($_POST["selectUseFile"])){
								
								$bookGalerie = new flipBook(); 			
								$bookGalerie->setConvertParams($_POST);
								$bookGalerie->createFlipBookFolder(PATH_CREATED_FLIPBOOKS, $_POST["PATH_NEW_CREATED_FLIPBOOK"]);
								// $bookGalerie->displayConvertParams();
								$bookGalerie->createGalerieImages();		
								// $bookGalerie->displayConvertStatus();		
								$bookFileLinks = $bookGalerie->createFlipbookLinks();				
								$getFlipBookFolder = $bookGalerie->getFlipBookFolder();
								
								echo '<hr />';								
								echo '<p><a href="' . $getFlipBookFolder . $bookFileLinks["filePathBook"] . '" target="_blank">' . basename($bookFileLinks["filePathBook"]). '</a>' . '<br /></p>';
							}
						}
					// EOF CONVERTING AND COPYING SELECTED IMAGES		
				?>	
				
				<?php
					if($warningMessage != "") {	echo '<p class="warningArea">'.$warningMessage.'</p>'; } 
					if($errorMessage != "") {	echo '<p class="errorArea">'.$errorMessage.'</p>'; } 
					if($successMessage != "") {	echo '<p class="successArea">'.$successMessage.'</p>'; } 
					if($infoMessage != "") {	echo '<p class="infoArea">'.$infoMessage.'</p>'; }
				?>
			</div>
		</div>
	</div>

<script language="javascript" type="text/javascript">
	$(document).ready(function() {
		$('.imageDir').click(function() {
			var getID = $(this).attr('id');
			var getStatus = $(this).children().attr('checked');
			var setStatus;
			if(getStatus) { setStatus = true; }
			else  { setStatus = false; }
			
			$('.imageFile .' + getID).attr('checked', setStatus);
		});
		
		$('.imageFile').click(function() {
			var getID = $(this).attr('id');
			var getStatus = $(this).attr('checked');
			alert(getID);
			var setStatus;
			if(getStatus) { setStatus = true; }
			else  { setStatus = false; }
			
			//$(getID).attr('checked', setStatus);
		});
	});
</script>

</body>
</html>