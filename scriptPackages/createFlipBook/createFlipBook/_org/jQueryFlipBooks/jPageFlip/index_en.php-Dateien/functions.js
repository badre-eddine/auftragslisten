function insertComment(obj,hierarchy) {
  var arrObjId = obj.split('_');
  var commentIsOpen;
  if ($('#commentReply_'+arrObjId[1]+'_'+arrObjId[2]+'_'+arrObjId[3]).length > 0)
    commentIsOpen = true;
  else
    commentIsOpen = false;  
  $('.commentReply').remove();
  if (commentIsOpen == false) {
    $('#comment').clone().attr({'id':'commentReply_'+arrObjId[1]+'_'+arrObjId[2]+'_'+arrObjId[3],'class':'commentReply'}).appendTo('#div_'+arrObjId[1]+'_'+arrObjId[2]+'_'+arrObjId[3]);
    $('#commentReply_'+arrObjId[1]+'_'+arrObjId[2]+'_'+arrObjId[3]).children('.fcomment').append('<input type="hidden" name="pid" value="'+arrObjId[1]+'">');
    $('#commentReply_'+arrObjId[1]+'_'+arrObjId[2]+'_'+arrObjId[3]).children('.fcomment').append('<input type="hidden" name="tid" value="'+arrObjId[3]+'">');
    $('#commentReply_'+arrObjId[1]+'_'+arrObjId[2]+'_'+arrObjId[3]).children('.fcomment').append('<input type="hidden" name="hierarchy" value="'+(parseInt(hierarchy) + 1)+'">');
  }
  
}

function openPopUp(_url,width,height) {
  popUp = window.open(_url, "PopUp", "width="+width+",height="+height+",left=100,top=100,scrollbars=yes");
  popUp.focus();

}