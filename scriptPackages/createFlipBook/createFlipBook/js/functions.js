function setPdfFilePath(pathPDFs) {
	var form = window.document.forms["formCreateSettings"];
	var getPdfFileField = form.elements["SELECTED_PDF_FILE"];
	var gettPdfFileFieldValue = getPdfFileField.options[getPdfFileField.selectedIndex].value;
	var setPdfFileField = form.elements["PDF_FILE"];
	var getFlipBookPageNamePrefixField = form.elements["FLIPBOOK_IMAGE_BASENAME_PREFIX"];
	var setFlipBookPageNameField = form.elements["FLIPBOOK_IMAGE_BASENAME"];
	
	// alert(form.elements["FLIPBOOK_IMAGE_BASENAME"].value);
	
	if(gettPdfFileFieldValue != '') {
		// gettPdfFileFieldValue = pathPDFs + gettPdfFileFieldValue;
		setPdfFileField.value = gettPdfFileFieldValue;
		setFlipBookPageNameField.value = getFlipBookPageNamePrefixField.value + '_' + form.elements["PDF_FILE"].value.substring(0, (form.elements["PDF_FILE"].value.length - 4)) + '.jpg';
		// alert(setFlipBookPageNameField.value);
	}
}