<?php
	// require_once('config.flipBook.inc.php');//configuration file

	class flipBook {
		public $convert_status = 0;
		public $zip_status = 0;
		public $copy_status = 0;
		
		function __construct() {
			$convert_status = 0;
			$zip_status = 0;
			
			if (is_dir("book") == false){
				echo "Please create folder 'book'<br>";
				exit;
			}
			if (is_dir("book/pages/") == false){
				echo "Please create folder 'book\pages'<br>";
				exit;
			}
			if (is_dir("zip") == false){
				echo "Please create folder 'zip'<br>";
				exit;
			}
		}

		function mkdir_r($dirName, $rights=0777){
			$dirs = explode('/', $dirName);
			$dir = '';
			foreach ($dirs as $part) {
				$dir .= $part.'/';
				if (!is_dir($dir) && strlen($dir)>0) {
					mkdir($dir, $rights);
				}
			}
		}
		
		//Function to convert  pdf files to jpg
		//Requires : IMAGEMAGICK
		function convertPdf2Jpg($pdf_file, $jpglocSmall, $jpglocLarge) {		
			// $cmd="convert ".$pdf_file." ".$jpgloc;
			// $cmd = "convert -quality 100 ".$pdf_file." ".$jpgloc;
			// $convert_status = exec($cmd);	
			
			/*			
			-contrast-stretch black-point{xwhite-point}{%}}
			-level black_point{,white_point}{%}{,gamma}
			*/
			
			$cmd_small = 'convert ';
			$cmd_large = 'convert ';
			
			if(FLIPBOOK_BACKGROUND_COLOR_NEW != '') {
				$cmd_small .= '-background '.FLIPBOOK_BACKGROUND_COLOR_NEW.' ';
				$cmd_large .= '-background '.FLIPBOOK_BACKGROUND_COLOR_NEW.' ';
			}
			if(FLIPBOOK_COLORSPACE_NEW != '') {
				$cmd_small .= '-colorspace '.FLIPBOOK_COLORSPACE_NEW.' ';
				$cmd_large .= '-colorspace '.FLIPBOOK_COLORSPACE_NEW.' ';
			}
			
			if(FLIPBOOK_CONTRAST_STRETCH_NEW != '') {
				$cmd_small .= '-contrast-stretch '.FLIPBOOK_CONTRAST_STRETCH_NEW.' ';
				$cmd_large .= '-contrast-stretch '.FLIPBOOK_CONTRAST_STRETCH_NEW.' ';
			}
			
			if(FLIPBOOK_LEVEL_NEW != '') {
				$cmd_small .= '-level '.FLIPBOOK_LEVEL_NEW.' ';
				$cmd_large .= '-level '.FLIPBOOK_LEVEL_NEW.' ';
			}			
			
			if(FLIPBOOK_DENSITY_SMALL_NEW > 0) {
				$cmd_small .= '-density '.FLIPBOOK_DENSITY_SMALL_NEW.' ';				
			}
			if(FLIPBOOK_DENSITY_LARGE_NEW > 0) {
				$cmd_large .= '-density '.FLIPBOOK_DENSITY_LARGE_NEW.' ';				
			}
			if(FLIPBOOK_QUALITY_SMALL_NEW > 0) {
				$cmd_small .= '-quality '.FLIPBOOK_QUALITY_SMALL_NEW.' ';
			}			
			if(FLIPBOOK_QUALITY_LARGE_NEW > 0) {
				$cmd_large .= '-quality '.FLIPBOOK_QUALITY_LARGE_NEW.' ';
			}
			if(FLIPBOOK_RESIZE_SMALL_NEW != '') {
				$cmd_small .= '-resize '.FLIPBOOK_RESIZE_SMALL_NEW.' ';
			}
			if(FLIPBOOK_RESIZE_LARGE_NEW != '') {
				$cmd_large .= '-resize '.FLIPBOOK_RESIZE_LARGE_NEW.' ';
			}
			
			$cmd_small .= DIRECTORY_PDF_FILES_NEW . PDF_FILE.' '.PATH_FLIPBOOK_IMAGES_SMALL_NEW;
			$convert_status = exec($cmd_small);	
			
			$cmd_large .= DIRECTORY_PDF_FILES_NEW . PDF_FILE_NEW.' '.PATH_FLIPBOOK_IMAGES_LARGE_NEW;
			$convert_status = exec($cmd_large);	
			
			echo "cmd_small: ".$cmd_small."<br>";
			echo "cmd_large: ".$cmd_large."<br>";
			
			echo "convert_status: ".$convert_status."<br>";
			echo "error: ".$error."<br>";
			echo "<pre>";
			print_r($error);
			echo "</pre>";
			
			return $convert_status;
		}
		
		//Function to read directory
		function readFolderDirectory($dir) {
			$listDir = array();
			if($handler = opendir($dir)) {
				while (($sub = readdir($handler)) !== FALSE) {
					if ($sub != "." && $sub != ".." && $sub != "Thumb.db") {
						if(is_file($dir."/".$sub)) {
							$listDir[] = $sub;
						}
						elseif(is_dir($dir."/".$sub)){
							// $listDir[$sub] = flipBook::ReadFolderDirectory($dir."/".$sub);
						}
					}
				}   
				closedir($handler);
			}
			return $listDir; 
		}
		
		//Creating and populating xml file for digibook
		//function name :CreateXMLFile
		//arguments : xmlfile  and image location
		function createXMLFile($xmlfile, $imgloc) {
			$stringDataBody = '';
			$dir = flipBook::readFolderDirectory($imgloc);//Open location  of  image sliced
			$fh = fopen($xmlfile, 'w') or die("can't open file");
			$stringDataTop = '<?xml version="1.0" encoding="iso-8859-1"?>'."\n";
			$stringDataTop .= '<datas>'."\n";
			for ($i = 0 ; $i < sizeof($dir) ; $i++) {
				$file = "page-".$i.".jpg";
				$stringDataBody .= "\t".'<data>'."\n";
				$stringDataBody .= "\t\t".'<image>'."pages/".$file.'</image>'."\n";
				$stringDataBody .= "\t\t".'<url>'."pages/".$file.'</url>'."\n";
				$stringDataBody .= "\t\t".'<text>Page: '.$i.'</text>'."\n";
				$stringDataBody .= "\t".'</data>'."\n";
				
				echo "Extracting...".$file."<br>";
			}
			$stringDataBottom = '</datas>';
			fwrite($fh, $stringDataTop.$stringDataBody.$stringDataBottom);//Write file name to xml file
			fclose($fh);
		}
		
		function createJSFile($jsfile, $imgloc) {
		
			$arrStringDataBody = array();
			$dir = flipBook::readFolderDirectory($imgloc);//Open location  of  image sliced
			if(file_exists($jsfile)) {
				unlink($jsfile);
			}
			$fh = fopen($jsfile, 'w') or die("can't open file");
			$stringDataTop = 'flippingBook.pages = ['."\n";
			
			for ($i = 0 ; $i < sizeof($dir) ; $i++) {
				$file = "page-".$i.".jpg";				
				$arrStringDataBody[] = '"flip/pages/'.$file.'",'."\n";					
				echo "Extracting...".$file."<br>";
			}
			$stringDataBody = implode("", $arrStringDataBody);
			$stringDataBottom = '];';
			fwrite($fh, $stringDataTop.$stringDataBody.$stringDataBottom);//Write file name to xml file
			fclose($fh);
		}
		
		function createZIP($ziploc) {
			$cmd = "zip -r  ".$ziploc."  book/";
			$zip_status = exec($cmd);
			echo "zip_status: ".$zip_status."<br>";
			return $zip_status;
		}	
		
		function copyImages($imgloc) {			
			$dir = flipBook::readFolderDirectory($imgloc);//Open location  of  image sliced
			
			for ($i = 0 ; $i < sizeof($dir) ; $i++) {		
				$file = "page-".$i.".jpg";
				$copy_status = copy('../flip/pages/'.$file, '../flip/pages/large/'.$file);				
				echo "Copying...".$file."<br>";
			}			
		}
		
		function createSmallImages($imgloc) {			
			$dir = flipBook::readFolderDirectory($imgloc);//Open location  of  image sliced
			
			for ($i = 0 ; $i < sizeof($dir) ; $i++) {		
				$file = "page-".$i.".jpg";				
			}			
		}
		
		function deleteExistingFiles($imgloc) {
			$dir = flipBook::readFolderDirectory($imgloc);
			for ($i = 0 ; $i < sizeof($dir) ; $i++) {
				$file = "page-".$i.".jpg";
				
				if(file_exists($imgloc.$file)) {
					unlink($imgloc.$file);
					echo "Deleting " . $imgloc.$file."<br>";
				}
				else {
					echo "COULD NOT DELETE " . $imgloc.$file."<br>";
				}				
			}
		}

	}//end class
 ?>

