<?php
	// require_once('config.flipBook.inc.php');//configuration file

	class flipBook {
		private $arrResult = array();		
		
		private $convertPdfCommand_small = '';
		private $convertPdfCommand_large = '';
		private $convertImageCommand_small = '';
		private $convertImageCommand_large = '';
		private $zipCommand = '';
		private $arrConvertParams = array();
		private	$arrPageImagesPaths = array();
		private $pageImagesDir = 'pages/';
		private $pageImagesPath = '';
		private $pageImagesDir_small = 'pages/small/';
		private $pageImagesDir_large = 'pages/large/';
		private $pageImageName = 'page.jpg';		
		private $filePathBook = '';		
		private $fileNameJS = 'js/bookSettings.js';
		private $fileNameXML = 'xml/bookPages.xml';
		private $fileNameZIP = 'zip/book.zip';
		private $pageImageHeightLarge = '';
		private $pageImageHeightSmall = '';
		private $pdfDownloadUrl = '';
		private $pdfDownloadFilesize = '';
		private $flipBookFolder = '';
		private $pageSound = 'sounds/02.mp3'; 
		
		
		
		function __construct() {	
			echo date("H:i:s") . ": Initializing book...<br /><br />";			
		}

		private function checkDirectory($dir) {
			if(!is_dir($dir)) {
				$this->arrResult["existDir"][] = 'Der Ordner ' . $dir . ' existiert nicht! ' . '<br />';
				$result = mkdir($dir);
				chmod($dir, 0777);
				if($result) {
					$this->arrResult["existDir"][] = 'Der Ordner ' . $dir . ' wurde angelegt!' . '<br />';
				}
				else {
					$this->arrResult["existDir"][] = 'Der Ordner ' . $dir . ' konnte nicht angelegt werden!' . '<br />';
				}
			}
			else {
				$this->arrResult["existDir"][] = 'Der Ordner ' . $dir . ' existiert bereits! ' . '<br />';
			}
		}
		
		private function setImageName($img) {
			$thisPageImageName = preg_replace("/\//", "_", $img);
			return $thisPageImageName;
		}
		
		private function setImageDirectory($path) {
			$arrTemp = explode('/', $path);
			return $arrTemp[(count($arrTemp) - 2)];
		}
		
		public function createGalerieImages() {		
			$this->setConvertImageCommand();			
			foreach($this->arrConvertParams["selectUseFile"] as $thisFileKey => $thisFileValue) {
				if($thisFileValue) {
					if($this->pageImagesDir == '') {
						$this->pageImagesDir = $this->setImageDirectory($thisFileKey);										
						$this->pageImagesPath = DIRECTORY_GALLERY_IMAGES.$this->pageImagesDir;						
						$this->checkDirectory($this->pageImagesPath);						
					}					
					
					$this->pageImageName = $this->setImageName($thisFileKey);
							
					echo '<hr />' . $thisFileKey . ' ---->>>' . $this->pageImageName . '<hr />';
										
					$this->checkDirectory($this->flipBookFolder . $this->pageImagesDir_small);					
					$this->checkDirectory($this->flipBookFolder . $this->pageImagesDir_large);					
					
					$this->deleteSingleFile($this->flipBookFolder . $this->pageImagesDir_small . $this->pageImageName);
					$this->deleteSingleFile($this->flipBookFolder . $this->pageImagesDir_large . $this->pageImageName);
					
					$thisResult = $this->copySingleImage($thisFileKey, $this->flipBookFolder . $this->pageImagesDir_small . $this->pageImageName);					
					if($thisResult) {
						$this->arrPageImagesPaths[] = $this->pageImageName;						
						$this->arrResult["convertSmall"][] = $this->execCommand($this->convertImageCommand_small . ' ' . $this->flipBookFolder . $this->pageImagesDir_small . $this->pageImageName, 0);
					}
					
					$thisResult = $this->copySingleImage($thisFileKey, $this->flipBookFolder . $this->pageImagesDir_large . $this->pageImageName);
					if($thisResult) {
						$this->arrResult["convertLarge"][] = $this->execCommand($this->convertImageCommand_large . ' ' . $this->flipBookFolder . $this->pageImagesDir_large . $this->pageImageName, 0);
					}
					
					if($this->pageImageHeightSmall == '') {					
						$this->pageImageHeightSmall = $this->getPageImageSize($this->flipBookFolder . $this->pageImagesDir_small . $this->pageImageName, 1);
					}
					if($this->pageImageHeightLarge == '') {					
						$this->pageImageHeightLarge = $this->getPageImageSize($this->flipBookFolder . $this->pageImagesDir_large . $this->pageImageName, 1);
					}
				}
			}
			
			if(!empty($this->arrPageImagesPaths)) {
				$this->createXmlFile();
				$this->createJsFile();
				$this->createBookFile('direct');
			}
			echo date("H:i:s") . ": Finish...<br><br><br>";
		}		
		
		//Function to convert  pdf files to jpg
		//Requires : IMAGEMAGICK
		public function convertPdf2Jpg($modeConvert=0) {	
			$this->pdfDownloadUrl = $this->copyOriginalFilePDF(DIRECTORY_PDF_FILES . $this->arrConvertParams["PDF_FILE"]);
			
			$this->pdfDownloadFilesize = $this->getFileSize(DIRECTORY_PDF_FILES . $this->arrConvertParams["PDF_FILE"]);
			$thisPattern = "/" . preg_replace("/\//", "\/", DOCUMENT_ROOT) . "/";
			$thisReplace = "";										
											
			#$this->pageImagesPath = $this->flipBookFolder . $this->pageImagesDir;						
			#$this->checkDirectory($this->pageImagesPath);	
			
			$this->checkDirectory($this->flipBookFolder . $this->pageImagesDir_small);			
			$this->checkDirectory($this->flipBookFolder . $this->pageImagesDir_large);			
			
			$this->setConvertPdfCommand();
		
			// modeConvert = 0 : CONVERT small AND large
			// modeConvert = 1 : CONVERT only small
			// modeConvert = 2 : CONVERT only large
			
			// -contrast-stretch black-point{xwhite-point}{%}}
			// -level black_point{,white_point}{%}{,gamma}
			if($modeConvert == 0 || $modeConvert == 2) {
				$message = 'start converting large files' . '<br /><br />';
				$dirImages = $this->flipBookFolder . $this->pageImagesDir_large;
				$pathImages = $this->flipBookFolder . $this->pageImagesDir_large;
				$convertPdfCommand = $this->convertPdfCommand_large;
				$actionConvert = "convertLarge";
				$actionRename = "renameLarge";
			}		
			else if($modeConvert == 1) {
				$message = 'start converting small files' . '<br /><br />';
				$dirImages = $this->flipBookFolder . $this->pageImagesDir_small;
				$pathImages = $this->flipBookFolder . $this->pageImagesDir_small;
				$convertPdfCommand = $this->convertPdfCommand_small;
				$actionConvert = "convertSmall";
				$actionRename = "renameSmall";
			}
			if($modeConvert > 2 && $modeConvert < 0) {
				$message = 'Cannot convert: no modeConvert ( ' . $modeConvert . ' ) selected in function convertPdf2Jpg' . '<br /><br />';
				echo $message;
			}
			else {
				flush();				
				$this->deleteExistingFiles($this->flipBookFolder . $this->pageImagesDir_small);
				$this->deleteExistingFiles($this->flipBookFolder . $this->pageImagesDir_large);
				$this->arrResult[$actionConvert][] = $this->execCommand($convertPdfCommand, 0);
				if(file_exists($pathImages . $this->pageImageName)) {
					$this->arrResult[$actionRename][] = rename($pathImages . $this->pageImageName, $pathImages . $this->getImagePageName(0));
				}
			}
			
			if($modeConvert == 0 || $modeConvert == 2) {			
				$this->copyAndResizeImages($this->flipBookFolder . $this->pageImagesDir_large, $this->flipBookFolder . $this->pageImagesDir_small);
			}
			
			$this->createXmlFile();					
			$this->createJsFile();				
			// $this->createZipFile();		
			$this->createBookFile('direct');	

			echo date("H:i:s") . ": Finish...<br><br><br>";
		}		
		
		public function setConvertParams($arrConvertParams) {			
			if(!empty($arrConvertParams)){
				foreach($arrConvertParams as $thisKey => $thisValue) {
					if(!is_array($thisValue)) { $thisValue = stripslashes($thisValue); }
					$arrConvertParams[$thisKey] = ($thisValue);					
				}
			}
			$this->arrConvertParams = $arrConvertParams;				
		}
		
		public function displayFilenames() {
			echo "this->pageImageName :".$this->pageImageName. '<br />';
			echo "this->pageImagesDir :".$this->pageImagesDir. '<br />';			
			echo "this->fileNameXML :".$this->fileNameXML. '<br />';								
			echo "this->fileNameJS :".$this->fileNameJS. '<br />';		
			echo "this->fileNameZIP :".$this->fileNameZIP. '<br />';
		}
		
		public function displayConvertParams() {
			echo "ConvertParams:<pre>";
			print_r($this->arrConvertParams);
			echo "</pre>";
		}
		
		public function getFileSize($path) {
			$thisFilesize = filesize($path);
			$thisUnit = "B";
			if($thisFilesize > 1000) {
				$thisUnit = "KB";
				$thisFilesize = $thisFilesize / 1000;
			}
			if($thisFilesize > 1000) {
				$thisUnit = "MB";
				$thisFilesize = $thisFilesize / 1000;
			}
			
			return number_format($thisFilesize, "2", "," , ".") . ' ' . $thisUnit;
		}
		
		public function displayConvertStatus() {
			echo "ConvertStatus:<pre>";
			print_r($this->arrResult);
			echo "</pre>";
		}
		
		private function setConvertImageCommand() {
			$this->convertImageCommand_small = IM_MOGRIFY . ' -resize ' . preg_replace("/x/", " x ", $this->arrConvertParams["FLIPBOOK_RESIZE_SMALL"]) . ' '. $this->flipBookFolder . $this->pageImagesDir_small;
			$this->convertImageCommand_large = IM_MOGRIFY . ' -resize ' . preg_replace("/x/", " x ", $this->arrConvertParams["FLIPBOOK_RESIZE_LARGE"]) . ' '. $this->flipBookFolder . $this->pageImagesDir_large;
		}
		
		private function setConvertPdfCommand() {			
			$this->convertPdfCommand_small = IM_CONVERT . ' ';
			$this->convertPdfCommand_large = IM_CONVERT . ' ';

			if($this->arrConvertParams["FLIPBOOK_BACKGROUND_COLOR"] != '') {
				$this->convertPdfCommand_small .= '-background ' . $this->arrConvertParams["FLIPBOOK_BACKGROUND_COLOR"] . ' ';
				$this->convertPdfCommand_large .= '-background ' .  $this->arrConvertParams["FLIPBOOK_BACKGROUND_COLOR"] . ' ';
			}
			if($this->arrConvertParams["FLIPBOOK_COLORSPACE"] != '') {
				$this->convertPdfCommand_small .= '-colorspace ' .  $this->arrConvertParams["FLIPBOOK_COLORSPACE"] . ' ';
				$this->convertPdfCommand_large .= '-colorspace ' .  $this->arrConvertParams["FLIPBOOK_COLORSPACE"] . ' ';
			}

			if($this->arrConvertParams["FLIPBOOK_CONTRAST_STRETCH"] != '') {
				$this->convertPdfCommand_small .= '-contrast-stretch ' .  $this->arrConvertParams["FLIPBOOK_CONTRAST_STRETCH"] . ' ';
				$this->convertPdfCommand_large .= '-contrast-stretch ' .  $this->arrConvertParams["FLIPBOOK_CONTRAST_STRETCH"] . ' ';
			}

			if($this->arrConvertParams["FLIPBOOK_LEVEL"] != '') {
				$this->convertPdfCommand_small .= '-level ' .  $this->arrConvertParams["FLIPBOOK_LEVEL"] . ' ';
				$this->convertPdfCommand_large .= '-level ' .  $this->arrConvertParams["FLIPBOOK_LEVEL"] . ' ';
			}			

			if($this->arrConvertParams["FLIPBOOK_DENSITY_SMALL"] > 0) {
				$this->convertPdfCommand_small .= '-density ' .  $this->arrConvertParams["FLIPBOOK_DENSITY_SMALL"] . ' ';				
			}
			if($this->arrConvertParams["FLIPBOOK_DENSITY_LARGE"] > 0) {
				$this->convertPdfCommand_large .= '-density ' .  $this->arrConvertParams["FLIPBOOK_DENSITY_LARGE"] . ' ';				
			}
			if($this->arrConvertParams["FLIPBOOK_QUALITY_SMALL"] > 0) {
				$this->convertPdfCommand_small .= '-quality ' .  $this->arrConvertParams["FLIPBOOK_QUALITY_SMALL"] . ' ';
			}			
			if($this->arrConvertParams["FLIPBOOK_QUALITY_LARGE"] > 0) {
				$this->convertPdfCommand_large .= '-quality ' .  $this->arrConvertParams["FLIPBOOK_QUALITY_LARGE"] . ' ';
			}
			if($this->arrConvertParams["FLIPBOOK_RESIZE_SMALL"] != '') {
				$this->convertPdfCommand_small .= '-resize ' .  preg_replace("/ x/", "x", $this->arrConvertParams["FLIPBOOK_RESIZE_SMALL"]) . ' ';
			}
			if($this->arrConvertParams["FLIPBOOK_RESIZE_LARGE"] != '') {
				$this->convertPdfCommand_large .= '-resize ' .  preg_replace("/ x/", "x", $this->arrConvertParams["FLIPBOOK_RESIZE_LARGE"]) . ' ';
			}

			$this->convertPdfCommand_small .= DIRECTORY_PDF_FILES . $this->arrConvertParams["PDF_FILE"].' ' . $this->flipBookFolder . $this->pageImagesDir_small . $this->pageImageName;
			$this->convertPdfCommand_large .= DIRECTORY_PDF_FILES . $this->arrConvertParams["PDF_FILE"].' ' . $this->flipBookFolder . $this->pageImagesDir_large . $this->pageImageName;
		}
		
		public function displayConvertPdfCommands() {
			echo "this->convertPdfCommand_small: ".$this->convertPdfCommand_small."<br /><br />";
			echo "this->convertPdfCommand_large: ".$this->convertPdfCommand_large."<br /><br />";
		}
		
		//Function to read directory
		private function readFolderDirectory($dir) {
			$listDir = array();
			if($handler = opendir($dir)) {
				while (($sub = readdir($handler)) !== FALSE) {
					if ($sub != "." && $sub != ".." && $sub != "Thumb.db") {
						if(is_file($dir."/".$sub)) {
							$listDir[] = $sub;
						}
						elseif(is_dir($dir."/".$sub)){
							// $listDir[$sub] = $this->ReadFolderDirectory($dir."/".$sub);
						}
					}
				}   
				closedir($handler);
			}
			return $listDir; 
		}
		
		private function createXmlFile() {
			$stringDataBody = '';
			
			if($this->arrConvertParams["actionType"] == 3) {
				$pageDatas = $this->arrPageImagesPaths;
				$pageDatasPath = "";
			}
			else {
				$pageDatas = $this->readFolderDirectory($this->flipBookFolder . $this->pageImagesDir_small);
				$thisPattern = "/" . preg_replace("/\//", "\/", DOCUMENT_ROOT) . "/";
				$thisReplace = "";
				$pageDatasPath = preg_replace($thisPattern, $thisReplace, $this->flipBookFolder . $this->pageImagesDir_small);				
			}
			$thisXmlFile = $this->flipBookFolder . $this->fileNameXML;
			#echo $this->flipBookFolder .'|||' .  $this->fileNameXML;
			if(file_exists($thisXmlFile)) {
				unlink($thisXmlFile);
			}
			$fp = fopen($thisXmlFile, 'w') or die("can't open file " . $thisXmlFile);
			$stringDataTop = '<?xml version="1.0" encoding="iso-8859-1"?>' . "\n";
			$stringDataTop .= '<datas>' . "\n";
			for ($i = 0 ; $i < sizeof($pageDatas) ; $i++) {
				$pageImageName = $this->getImagePageName($i);
				$stringDataBody .= "\t" . '<data>' . "\n";
				
				$stringDataBody .= "\t\t" . '<image>' . $pageDatasPath . $pageDatas[$i] . '</image>' . "\n";
				$stringDataBody .= "\t\t" . '<url>' . $pageDatasPath . $pageDatas[$i] . '</url>' . "\n";
				$stringDataBody .= "\t\t" . '<text>Seite: ' . $i . '</text>' . "\n";
				$stringDataBody .= "\t" . '</data>' . "\n";
				
				$this->arrResult["XML"][] =  "Extracting for XML" . $thisXmlFile.": " . $pageDatas[$i] . "<br />";
			}
			$stringDataBottom = '</datas>';
			fwrite($fp, $stringDataTop.$stringDataBody.$stringDataBottom);//Write file name to xml file
			fclose($fp);
		}
		
		private function createJsFile() {

			$arrStringPagesContent = array();
			
			if($this->arrConvertParams["actionType"] == 3) {
				$pageDatas = $this->arrPageImagesPaths;				
			}
			else {
				$pageDatas = $this->readFolderDirectory($this->flipBookFolder . $this->pageImagesDir_small);						
			}
			
			$thisJsFile = $this->flipBookFolder . $this->fileNameJS;			
			if(file_exists($thisJsFile)) {
				unlink($thisJsFile);
			}
			$fp = fopen($thisJsFile, 'w') or die("can't open file " . $thisJsFile);
			$stringDataPagesTop = 'flippingBook.pages = [' . "\n";
			
			for ($i = 0 ; $i < sizeof($pageDatas) ; $i++) {
				$pageImageName = $this->getImagePageName($i);				
				#$arrStringPagesContent[] = "\t" . '"' . preg_replace("/" . PATH_CREATED_FLIPBOOKS . $this->arrConvertParams["PATH_NEW_CREATED_FLIPBOOK"] . "/", "", $this->pageImagesDir_small) . $pageDatas[$i] . '",' . "\n";					
				$arrStringPagesContent[] = "\t" . '"' . $this->pageImagesDir_small . $pageDatas[$i] . '",' . "\n";					
				$this->arrResult["JS"][] =  "Extracting for JS" . $thisJsFile.": " . $pageDatas[$i] . "<br />";				
			}
			$stringDataPagesContent = implode("", $arrStringPagesContent);
			$stringDataPagesBottom = '];';
			
			$stringDataPages = $stringDataPagesTop.$stringDataPagesContent.$stringDataPagesBottom;
			
			$stringDataPagesChapters = '
				flippingBook.contents = [	
						[ "Titelblatt", 1 ],
						[ "Inhaltsverzeichnis", 2 ],						
						[ "Kontakt", 5 ],					
					];
			';
			
			$stringDataPagesSettings = '
					// define custom book settings here
					// flippingBook.settings.pagesSet = ' . count($pageData). ';
					flippingBook.settings.bookWidth = ' . ($this->arrConvertParams["FLIPBOOK_DENSITY_SMALL"] * 2) . ';
					flippingBook.settings.bookHeight = ' . $this->pageImageHeightSmall . ';
					flippingBook.settings.zoomImageWidth = ' . $this->arrConvertParams["FLIPBOOK_DENSITY_LARGE"] . '; 
					flippingBook.settings.zoomImageHeight = ' . $this->pageImageHeightLarge . ';
					flippingBook.settings.useCustomCursors = false;
					flippingBook.settings.dropShadowEnabled = false;
					flippingBook.settings.flipSound = "' . $this->pageSound . '";
					flippingBook.settings.flipCornerStyle = "first page only";
					flippingBook.settings.zoomHintEnabled = true;
					flippingBook.settings.zoomPath = "' . $this->pageImagesDir_large . '";

					flippingBook.settings.hardcover = false;
					flippingBook.settings.downloadURL = "' . $this->pdfDownloadUrl . '";
					flippingBook.settings.downloadSize = "Größe: ' . $this->pdfDownloadFilesize . '";
					flippingBook.settings.preloaderType = "Thin"; // "Progress Bar", "Round", "Thin", "Dots", "Gradient Wheel", "Gear Wheel", "Line", "Animated Book", "None"

					// default settings can be found in the flippingbook.js file
					flippingBook.create();
			';

			$fileContent = trim($stringDataPages) . "\n" . preg_replace("/\[ \"/", "\t[ \"", preg_replace("/\\t/", "", $stringDataPagesChapters)) . "\n" . preg_replace("/\\t/", "", $stringDataPagesSettings);
		
			fwrite($fp, $fileContent);//Write file name to xml file
			fclose($fp);
		}
		
		function createBookFile($filemode = 'direct') {		
			if($filemode == 'direct'){
				$fp = fopen(PATH_TEMPLATE_FLIPBOOK_DIRECT_FILE, 'r') or die("can't open file " . PATH_TEMPLATE_FLIPBOOK_DIRECT_FILE);
				$content = fread($fp , filesize(PATH_TEMPLATE_FLIPBOOK_DIRECT_FILE));
				fclose($fp);
			}
			else if($filemode == 'simple'){
				$fp = fopen(PATH_TEMPLATE_FLIPBOOK_SIMPLE_FILE, 'r') or die("can't open file " . PATH_TEMPLATE_FLIPBOOK_SIMPLE_FILE);
				$content = fread($fp , filesize(PATH_TEMPLATE_FLIPBOOK_SIMPLE_FILE));
				fclose($fp);
			}
			else if($filemode == 'lighbox'){
				$fp = fopen(PATH_TEMPLATE_FLIPBOOK_LIGHTBOX_FILE, 'r') or die("can't open file " . PATH_TEMPLATE_FLIPBOOK_LIGHTBOX_FILE);
				$content = fread($fp , filesize(PATH_TEMPLATE_FLIPBOOK_LIGHTBOX_FILE));
				fclose($fp);
			}		
			
			$content = preg_replace("/{###DIRECTORY_FLIPBOOK###}/", "", $content);
			$content = preg_replace("/{###BOOK_SETTINGS###}/", $this->fileNameJS, $content);
			
			$this->filePathBook = basename(preg_replace("/\.htm\.tmpl/", "", PATH_TEMPLATE_FLIPBOOK_DIRECT_FILE) . ".htm");
							
			$fp = fopen($this->flipBookFolder . $this->filePathBook, "w");
			fwrite($fp, $content);
			fclose($fp);
		}
		
		private function createZipFile() {
			// $this->zipCommand = "zip -r  " . $this->arrConvertParams["PATH_ZIP_FILE"] . " " . $this->arrConvertParams["PATH_XML_FILE"];
			$this->zipCommand = "zip " . $this->arrConvertParams["PATH_ZIP_FILE"] . " " . $this->arrConvertParams["PATH_XML_FILE"];
			$this->arrResult["zip"][] = $this->execCommand($this->zipCommand, 0);
			echo "this->zipCommand: " . $this->zipCommand . "<br /><br />";			
		}	
		
		private function getImagePageName($pageNumber) {
			$arrTemp = explode(".", $this->pageImageName);
			$fileType = $arrTemp[(count($arrTemp) - 1)];
			$thisPageImageName = preg_replace("/.".$fileType."/", "", $this->pageImageName)."-".$pageNumber.".".$fileType;			
			
			return $thisPageImageName;
		}
		
		public function checkImagePageName($pageNumber) {
			echo "checkImagePageName: " . $this->getImagePageName($pageNumber) . "<br /><br />";
		}
		
		private function copySingleImage($pathOrig, $pathCopy){
			$thisResult .= copy($pathOrig, $pathCopy);	
			chmod($pathCopy, 0777);
			$this->arrResult["copy"][] = $thisResult;
			$this->arrResult["copy"][] =  "Copying...".$pathOrig." --->> " . $pathCopy . "<br /><br />";
			return $thisResult;
		}
		
		private function copyAndResizeImages($pathOrig, $pathCopy) {	
			$this->setConvertImageCommand();
			$dir = $this->readFolderDirectory($pathOrig);
			
			for ($i = 0 ; $i < sizeof($dir) ; $i++) {		
				$pageImageName = $this->getImagePageName($i);
				$this->copySingleImage($pathOrig . $pageImageName, $pathCopy . $pageImageName);	
				#$this->convertImageCommand_small = $this->convertImageCommand_small . ' ' .$pathCopy . $pageImageName;	
				$thisConvertImageCommand_small = $this->convertImageCommand_small . ' ' .$pathCopy . $pageImageName;					
				$this->resizeImage($thisConvertImageCommand_small);				
				
				if($this->pageImageHeightSmall == '') {					
					$this->pageImageHeightSmall = $this->getPageImageSize($pathCopy . $pageImageName, 1);
				}
				if($this->pageImageHeightLarge == '') {					
					$this->pageImageHeightLarge = $this->getPageImageSize($pathOrig . $pageImageName, 1);
				}
			}			
		}
		
		private function getPageImageSize($path, $modeInfo=1) {
			$arrTemp = getimagesize($path);			
			return $arrTemp[$modeInfo];
		}
		
		private function resizeImage($command) {			
			$this->execCommand($command, 0);
		}
		
		private function deleteSingleFile($path) {
			if(file_exists($path)) {
				unlink($path);
				$this->arrResult["deleteImages"][] = 'Loesche ' . $path . '<br />';
			}			
		}
		
		private function deleteExistingFiles($path) {
			$dir = $this->readFolderDirectory($path);
			for ($i = 0 ; $i < sizeof($dir) ; $i++) {
				$pageImageName = $this->getImagePageName($i);
				$this->deleteSingleFile($path.$pageImageName);							
			}
		}
		
		private function execCommand($command, $modeExec = 0) {
			$return = "";
			$command = IM_PATH . $command;
			// $command = escapeshellcmd($command);
			$this->arrResult["command"][] = $command;
			if($modeExec == 1) {
				exec($command, $arrOutput, $result);				
				$this->arrResult["execCommand"][] = $command;
				$this->arrResult["execCommand"][] = $arrOutput;
				$this->arrResult["execCommand"][] = $result;
				$return = $result;
			}
			else {
				$result = shell_exec($command);
				$this->arrResult["execCommand"][] = $command;
				$this->arrResult["execCommand"][] = $error;
				$this->arrResult["execCommand"][] = $result;
				$return = $error;
			}
			return $return;
		}
		
		public function createFlipbookLinks() {
			$arrLinks = array();
			$arrLinks["pageImagesDir"] = $this->pageImagesDir;					
			$arrLinks["fileNameXML"] = $this->fileNameXML;					
			$arrLinks["fileNameJS"] = $this->fileNameJS;
			$arrLinks["fileNameZIP"] = $this->fileNameZIP;
			$arrLinks["filePathBook"] = $this->filePathBook;
			return $arrLinks;	
		}
		
		private function copy_directory($source, $destination) {
			if(is_dir($source)) {
				@mkdir( $destination );
				$directory = dir( $source );
				while(FALSE !== ($readdirectory = $directory->read())) {
					if($readdirectory == '.' || $readdirectory == '..') {
						continue;
					}
					$PathDir = $source . '/' . $readdirectory; 
					if(is_dir($PathDir)) {
						copy_directory($PathDir, $destination . '/' . $readdirectory);
						continue;
					}
					copy($PathDir, $destination . '/' . $readdirectory);
				}
		 
				$directory->close();
			}
			else {
				copy($source, $destination);
			}
		}
		
		public function createFlipBookFolder($dirCreation, $dirNewFlipbook) {
			// BOF CREATE BASIC FOLDERS AND COPY BASIC FILES					
			if(!is_dir($dirCreation)){ mkdir($dirCreation); }			
			$this->flipBookFolder = $dirCreation . $dirNewFlipbook;
			if(!is_dir($this->flipBookFolder)){ mkdir($this->flipBookFolder); }
			$this->copy_directory(DIRECTORY_BASIC_FLIPBOOK, $this->flipBookFolder);	
			// BOF CREATE BASIC FOLDERS AND COPY BASIC FILES
		}
		
		public function getFlipBookFolder() {
			return $this->flipBookFolder;
		}
		
		private function copyOriginalFilePDF($file) {
			$this->arrResult["copyOriginalPDF"][] = copy($file, $this->flipBookFolder . "pdf/" . basename($file));
			return "pdf/" . basename($file);
		}
		
		public function setSoundFile($pageSound) {
			$this->pageSound = $pageSound;
		}

	}//end class
 ?>

