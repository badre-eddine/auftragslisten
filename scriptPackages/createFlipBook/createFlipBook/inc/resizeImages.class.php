<?php
	class resizeImages extends flipBook
	{
		var $imageResizeWidth = "";
		var $imageResizeHeight = "";
		var $imageOriginalWidth = "";
		var $imageOriginalHeight = "";
		var $imageName = "";
		var $imageFolder = "";
		var $imagePath = "";	
		var $imageDimensions = "";
		var $imageFilesize = "";
		var $imageType = "";
		
		var $tempImageName = "";
		var $tempImageNamePrefix = "_tmp_";
		var $tempImageFolder = "";
		var $tempImagePath = "";			
		
		function setImageResizeDimensions ($imageResizeWidth, $imageResizeHeight)
		{
			$this->imageResizeWidth = $imageResizeWidth;
			$this->imageResizeHeight = $imageResizeHeight;
		}
				
		function setImagePath ($imageFolder, $imageName)
		{
			$this->imageFolder = $imageFolder;
			$this->imageName = $imageName;
			$this->imagePath = $this->imageFolder.$this->imageName;	
			
			$this->tempImageFolder = $imageFolder;
			$this->tempImageName = $this->tempImageNamePrefix.$this->imageName;
			$this->tempImagePath = $this->tempImageFolder.$this->tempImageName;					
		}
		
		function getImageType()
		{
			$arrThisFile = explode(".", $this->imageName);
			$this->imageType = strtolower($arrThisFile[(count($arrThisFile) - 1)]);
			return $this->imageType;
		}
		
		function getImageDimensions ()
		{
			return 'width="'.$this->imageResizeWidth.'" height="'.$this->imageResizeHeight.'"';
		}
		
		function getImageFilesize ()
		{
			$this->imageFilesize = filesize($this->imagePath);
			return $this->imageFilesize;
		}
		
		function copyUploadedImage($file)
		{
			copy($file, $this->imagePath);
			$arrImageInfo = getimagesize($this->imagePath);			
			$this->imageOriginalWidth = $arrImageInfo[0];
			$this->imageOriginalHeight = $arrImageInfo[1];		
		}
		
		function resizeImage()
		{						
			if(($this->imageOriginalWidth > $this->imageResizeWidth) || ($this->imageOriginalHeight > $this->imageResizeHeight))
			{
				$copy = copy($this->imagePath, $this->tempImagePath);				
				chmod($this->tempImagePath, 0777);
				$newsize = $this->imageResizeWidth . "x" . $this->imageResizeHeight;				
				$cmd = "/usr/bin/mogrify -resize $newsize $this->tempImagePath";
				//$cmd = "/usr/bin/mogrify -version";
				$result = shell_exec($cmd); //, $exec_output, $exec_retval
				//print_r($result);
				$copy = copy($this->tempImagePath, $this->imagePath);				
				unlink($this->tempImagePath);				
				//Header( "Content-type: image/jpg");
			}
		}
		
		function convertCmykToRgb()
		{
			// CONVERT CMYK INTO RGB
				// convert -colorspace RGB cmyk.tiff rgb.tif
				$copy = copy($this->imagePath, $this->tempImagePath);				
				chmod($this->tempImagePath, 0777);
				$cmd = "/usr/bin/mogrify convert -colorspace RGB $this->tempImagePath $this->tempImagePath";
				$result = shell_exec($cmd); //, $exec_output, $exec_retval				
				$copy = copy($this->tempImagePath, $this->imagePath);				
				unlink($this->tempImagePath);
		}
	}
?>