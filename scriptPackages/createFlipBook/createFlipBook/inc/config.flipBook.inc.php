<?php
	// BOF EDITABLE PARAMS DEFAULT	
		$arrParamFlipbook["FLIPBOOK_BACKGROUND_COLOR"]			= "rgb(255, 255, 255)";
		$arrParamFlipbook["FLIPBOOK_CONTRAST_STRETCH"]			= "";
		$arrParamFlipbook["FLIPBOOK_LEVEL"] 					= "2%";
		$arrParamFlipbook["FLIPBOOK_COLORSPACE"]				= "RGB";
		// $arrParamFlipbook["FLIPBOOK_DENSITY_SMALL"]			= 500;
		// $arrParamFlipbook["FLIPBOOK_DENSITY_LARGE"]			= 1300;
		$arrParamFlipbook["FLIPBOOK_DENSITY_SMALL"]				= 500;
		$arrParamFlipbook["FLIPBOOK_DENSITY_LARGE"]				= 1000;
		$arrParamFlipbook["FLIPBOOK_QUALITY_SMALL"]				= 70;
		$arrParamFlipbook["FLIPBOOK_QUALITY_LARGE"]				= 90;	
		$arrParamFlipbook["FLIPBOOK_RESIZE_SMALL"]				= $arrParamFlipbook["FLIPBOOK_DENSITY_SMALL"] . " x";
		$arrParamFlipbook["FLIPBOOK_RESIZE_LARGE"]				= $arrParamFlipbook["FLIPBOOK_DENSITY_LARGE"] . " x";	
		$arrParamFlipbook["PATH_NEW_CREATED_FLIPBOOK"]			= date("Y-m-d_H-i-s") . "/";
	// EOF EDITABLE PARAMS DEFAULT	
	

	// BOF DEFINE CONSTANTS
		DEFINE("BASEPATH", "");
		DEFINE("DOCUMENT_ROOT", "");
	
		// PDF FILES
			DEFINE("DIRECTORY_PDF_FILES", "pdf/");
			DEFINE("DIRECTORY_GALLERY_IMAGES", "gallery");
		
		// IMAGE MAGICK
			// DEFINE("IM_PATH", "/usr/bin/");
			DEFINE("IM_PATH", "");
			DEFINE("IM_CONVERT", "convert"); // convert
			DEFINE("IM_MOGRIFY", "mogrify"); // mogrify
		
		// BASIC FLIPBOOK FILES
			DEFINE("DIRECTORY_BASIC_FLIPBOOK", "flipbookBasic/");	
			DEFINE("DIRECTORY_TEMPLATE_FILES", "flipbookTemplates/");
			DEFINE("PATH_TEMPLATE_JS_PAGES_FILE", DIRECTORY_TEMPLATE_FILES . "bookPages.js.tmpl");
			DEFINE("PATH_TEMPLATE_JS_SETTINGS_FILE", DIRECTORY_TEMPLATE_FILES . "bookSettings.js.tmpl");
			DEFINE("PATH_TEMPLATE_XML_FILE", DIRECTORY_TEMPLATE_FILES . "flipBook.xml.tmpl");
			DEFINE("PATH_TEMPLATE_FLIPBOOK_DIRECT_FILE", DIRECTORY_TEMPLATE_FILES . "flipBookDirect.htm.tmpl");
			DEFINE("PATH_TEMPLATE_FLIPBOOK_SIMPLE_FILE", DIRECTORY_TEMPLATE_FILES . "flipBookSimple.htm.tmpl");
			DEFINE("PATH_TEMPLATE_FLIPBOOK_LIGHTBOX_FILE", DIRECTORY_TEMPLATE_FILES . "flipBookLightBox.htm.tmpl");	
		
		// FLIPBOOK CREATION DIR		
			DEFINE("PATH_CREATED_FLIPBOOKS" , "createdFlipBooks/");	
	
		#DEFINE("DOCUMENT_ROOT", $_SERVER["DOCUMENT_ROOT"] . "/SCRIPTE/FLIPBOOK/meinFlipBook/");
		#DEFINE("BASEPATH", $_SERVER["HTTP_HOST"] . "/SCRIPTE/FLIPBOOK/meinFlipBook/");
		// DEFINE("DOCUMENT_ROOT", $_SERVER["DOCUMENT_ROOT"] . "/meinFlipBook/");
		
	// EOF DEFINE CONSTANTS	
?>

