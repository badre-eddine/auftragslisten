<?php
	function modifyImage($sourceImageName, $newImageWidth, $newImageHeight, $newImageName, $newImageFolder, $newImageMode)
	{
		$ext = explode('.', $sourceImageName);  
		$ext = $ext[count($ext)-1]; 
		
		if (file_exists($newImageFolder.$sourceImageName) and (strtolower($ext) == "jpg"))
		{			
			if($newImageMode == "resizeImage")
			{
				//MAINPIC RESIZE ONLY WHEN MAINPIC-SIZES GREATER THAN NEW SIZES ($newImageWidth, $newImageHeight)!!!
					$mainPicInfo = getimagesize($newImageFolder.$newImageName);				
					
					if(($mainPicInfo[0] > $newImageWidth) && ($mainPicInfo[1] > $newImageHeight))
					{
						$copy1 = copy($newImageFolder.$sourceImageName, "/tmp/".$newImageName);
						chmod("/tmp/".$newImageName, 0777);
						$newsize = $newImageWidth . "x" . $newImageHeight;
						$cmd = "/usr/bin/mogrify -resize $newsize /tmp/$newImageName";
						//$cmd = "/usr/bin/mogrify -version";
						$result = shell_exec($cmd); //, $exec_output, $exec_retval
						//print_r($result);
						$copy2 = copy("/tmp/".$newImageName, $newImageFolder.$newImageName);
						chmod($newImageFolder.$newImageName, 0777); 
						if(file_exists("/tmp/".$newImageName))
						{
							unlink("/tmp/".$newImageName);
						}
						
						if($copy1 && $copy2) { $return = true; }
						else { $return = false; }
					}	
					else				
					{
						$return = false; 
					}
			}
			
			if($newImageMode == "createThumb")
			{					
				//THUMBNAIL				
					if(file_exists($newImageFolder.$newImageName))
					{
						unlink($newImageFolder.$newImageName);
					}
					$copy1 = copy($newImageFolder.$sourceImageName, "/tmp/".$newImageName);					
					chmod("/tmp/".$newImageName, 0777);
					$newSize = $newImageWidth . "x" . $newImageHeight;
					$newQuality = 60;
					$cmd = "/usr/bin/mogrify -resize $newSize /tmp/$newImageName";
					exec($cmd); //, $exec_output, $exec_retval
					$cmd = "/usr/bin/mogrify -quality $newQuality /tmp/$newImageName";
					exec($cmd); //, $exec_output, $exec_retval
					$copy2 = copy("/tmp/".$newImageName, $newImageFolder.$newImageName);	
										
					chmod($newImageFolder.$newImageName, 0777); 
					if(file_exists("/tmp/".$newImageName))
					{
						unlink("/tmp/".$newImageName);
					}
					if($copy1 && $copy2) { $return = true; }
					else { $return = false; }
			}			
		}
		else				
		{
			$return = false; 
		}
		
		return $return;
	}
?>