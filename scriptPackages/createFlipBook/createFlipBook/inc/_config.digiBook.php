<?php	
	// define('PDF_FILE', '../pdf/test3.pdf');
	define('PDF_FILE', '../pdf/prospekt_burhan_2.pdf');
	define('IMAGE_LOCATION', '../flip/pages/');
	define('XML_LOCATION', 'book/');
	define('ZIP_LOCATION', 'zip/book.zip');
	define('JS_LOCATION', '../flip/js/bookPages.js');
	// define('FLIPBOOK_BACKGROUND_COLOR', 'rgb(255, 255, 255)');
	define('FLIPBOOK_BACKGROUND_COLOR', '');
	define('FLIPBOOK_CONTRAST_STRETCH', '');
	define('FLIPBOOK_LEVEL', '2%');
	define('FLIPBOOK_COLORSPACE', 'RGB');
	// define('FLIPBOOK_DENSITY_SMALL', 500);
	// define('FLIPBOOK_DENSITY_LARGE', 1300);
	define('FLIPBOOK_DENSITY_SMALL', 500);
	define('FLIPBOOK_DENSITY_LARGE', 1000);
	define('FLIPBOOK_QUALITY_SMALL', 70);
	define('FLIPBOOK_QUALITY_LARGE', 92);	
	define('FLIPBOOK_RESIZE_SMALL', '400x');
	define('FLIPBOOK_RESIZE_LARGE', '1300x');
	define('FLIPBOOK_PATH_IMAGES_SMALL', ' ../flip/pages/page.jpg');
	define('FLIPBOOK_PATH_IMAGES_LARGE', ' ../flip/pages/large/page.jpg');
?>