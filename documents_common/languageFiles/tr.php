<?php
	$arrLanguageData = array(
		"FILE" => "dosya",
		"PRODUCTION_DATA" => "üretim bilgileri",
		"PRODUCT_ADDITIONAL_NAME" => "sırf çıta",
		"PRODUCT_ADDITIONAL_QUANTITY" => "sırf çıta miktarı",
		"PRODUCT_COLOR_COUNT" => "renk sayısı",
		"PRODUCT_COLOR_NAMES" => "renk ismi",
		"PRODUCT_NAME" => "ürün ismi",
		"PRODUCT_NUMMER" => "ürün numarası",
		"PRODUCT_QUANTITY" => "miktarı",
		"RECIPIENT_ADRESS" => "alıcının adresi",
		"SENDER_ADRESS" => "gönderenın adresi",
		"COMPANY_NAME" => "sirketin ismi",
		"STREET" => "Sokak", //yol, Cadde
		"STREET_NUMBER" => "ev numarası",
		"ZIPCODE" => "posta kodu",
		"CITY" => "sehir ",
		"COUNTRY" => "ülke",
		"CUSTUMER_NUMBER" => "müsteri numarası",
		"EXPOSITION_FILM" => "Pozlama-Filmi",
		"CORRECTED_FILM" => "düzelfilmis Filmler",
		"PRINT_TEXT" => "baskı",
		"COMMISSION" => "komission",
		"PRODUCT_WITH_CLEARPAINT" => "Vernik",
		"CONTAINER_PLACE_BACK" => "TIR arkası",
		"CONTAINER_PLACE_BACK_NAME" => "TIR arkası",
		"USE_NEUTRAL_PACKING" => "Reklamsiz",
		"USE_NO_NEUTRAL_PACKING" => "Reklamli (BURHAN)",
		"PACKING" => "ambalaj",
		"PRINTING_PLATE_NUMBER" => "klişe numarası",
		
		"ORDER_TYPE" => "Sipariş",
		"ORDER_TYPE_1" => "Yeni sipariş",
		"ORDER_TYPE_2" => "Tekrar ısmarlama",
		"ORDER_TYPE_3" => "Reklamasyon",
		"ORDER_TYPE_4" => "bilinmeyen"
	);
?>