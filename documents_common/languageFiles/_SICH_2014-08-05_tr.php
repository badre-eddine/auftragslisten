<?php
	$arrLanguageData = array(
		"FILE" => "dosya",
		"PRODUCTION_DATA" => "üretim bilgileri",
		"PRODUCT_ADDITIONAL_NAME" => "sırf çıta",
		"PRODUCT_ADDITIONAL_QUANTITY" => "sırf çıta miktarı",
		"PRODUCT_COLOR_COUNT" => "renk sayısı",
		"PRODUCT_COLOR_NAMES" => "renk ismi",
		"PRODUCT_NAME" => "ürün ismi",
		"PRODUCT_NUMMER" => "ürün numarası",
		"PRODUCT_QUANTITY" => "miktarı",
		"RECIPIENT_ADRESS" => "alıcının adresi",
		"SENDER_ADRESS" => "gönderenın adresi",
		"COMPANY_NAME" => "şirketin ismi",
		"STREET" => "Sokak", //yol, Cadde
		"STREET_NUMBER" => "ev numarası",
		"ZIPCODE" => "posta kodu",
		"CITY" => "şehir ",
		"COUNTRY" => "ülke",
		"CUSTUMER_NUMBER" => "müşteri numarası",
		"EXPOSITION_FILM" => "Pozlama-Filmi",
		"CORRECTED_FILM" => "düzelfilmiş Filmler",
		"PRINT_TEXT" => "baskı",
		"COMMISSION" => "komission",
	);
?>