<?php
	require_once('inc/requires.inc.php');

	if(!$arrGetUserRights["createTotalBackUp"]) {
		header('location: ' . PAGE_EXIT_LOCATION);
		exit;
	}

	$jswindowMessage = "";
	$warningMessage = '';
	$errorMessage = '';
	$successMessage = '';
	$infoMessage = '';

	$dbConnection = new DB_Connection();
	$db_open = $dbConnection->db_connect();

	// BOF CONFIG
		$arrBackUpFilenames = array(
						"backupDatabase" => BACKUP_DATABASE_FILE,
						"backupImages" => BACKUP_IMAGES_FILE,
						"backupFiles" => BACKUP_FILES_FILE,
						"backupComplete" => BACKUP_COMPLETE_FILE,
						"deleteBackups" => DELETE_BACKUPS_FILE
		);

		$arrBackupExcludes = array(
			DIRECTORY_BACKUP_FILES
		);

		$arrBackupIncludes = array(
			BASEPATH
		);
	// EOF CONFIG

	function myExec($command, $applicationOS = APPLICATION_OS) {
		$command = escapeshellcmd($command);
		if($applicationOS == 'WIN') {
			$result = shell_exec($command);
		}
		else {
			$result = exec($command, $error);
		}
		echo $command . '<br />';
		return $result;
	}

	function deleteBackups($folderPath, $backUpType, $arrFiles) {
		global $_POST;
		$arrExecResults = array();
		if(!empty($arrFiles)) {
			foreach($arrFiles as $thisFile) {
				if($backUpType == "showBackups"){
					$thisFile = $folderPath.$thisFile;
				}

				if(file_exists($thisFile)) {
					#echo $thisFile . '<hr />';
					#@chmod($thisFile, "0777");
					if(unlink($thisFile)) { $arrExecResults[] = $thisFile." entfernt."."<br />"; }
					 else { $arrExecResults[] = $thisFile." nicht entfernt."."<br />"; }
				}
				clearstatcache();
			}
		}
		return $arrExecResults;
	}

	function makeBackup($backUpType, $thisFileName) {
		global $dbConnection, $arrBackupIncludes, $arrBackupExcludes, $arrBackUpFilenames;

		$arrExecResults = array();
		$arrExecResults[] = $backUpType;

		if($backUpType == "backupDatabase" || $backUpType == "backupComplete") {

			// BOF TOTAL DB IN SINGLE FILES
			$sql = 'SHOW TABLE STATUS';
			$rs = $dbConnection->db_query($sql);

			$arrTableNames = array();
			while($ds = mysqli_fetch_assoc($rs)) {
				$arrTableNames[] = $ds["Name"];
			}

			if(!empty($arrTableNames)) {
				foreach($arrTableNames as $thisTableName) {
					$createCommand = ''.MYSQLDUMP_PATH.' --opt --complete-insert -h '.DB_HOST.' -u '.DB_USER.' -p'.DB_PASSWORD.' -r '.DIRECTORY_BACKUP_FILES.$thisFileName.'_'.$thisTableName.'.sql '.DB_NAME.' '.$thisTableName . ' ' .TRAP_ERROR;
					#$createCommand = ''.MYSQLDUMP_PATH.' --opt --complete-insert --lock-all-tables --extended-insert=FALSE -h '.DB_HOST.' -u '.DB_USER.' -p'.DB_PASSWORD.' -r '.DIRECTORY_BACKUP_FILES.$thisFileName.'_'.$thisTableName.'.sql '.DB_NAME.' '.$thisTableName . ' ' .TRAP_ERROR;

					// $createCommand = escapeshellcmd($createCommand);
					$createCommand = addslashes($createCommand);
					$arrExecResults[] = 'COMMAND: ' . $createCommand;
					$arrExecResults[] = 'EXEC: ' . exec($createCommand, $error);
					// $arrExecResults[] = shell_exec($createCommand);
					if(!empty($error)) {
						$Serror = array('0. BACKUP: mysqldump error', $error);
						$arrExecResults[] = $Serror;
					}
				}
			}
			/*
			echo "<pre>";
			print_r($arrExecResults);
			echo "</pre>";
			exit;
			*/
			// EOF TOTAL DB IN SINGLE FILES

			// BOF TOTAL DB IN ONE FILE
			/*
			##$createCommand = ''.MYSQLDUMP_PATH.' --opt -h '.DB_HOST.' -u '.DB_USER.' -p'.DB_PASSWORD.' -r '.DIRECTORY_BACKUP_FILES.$thisFileName.'.sql '.DB_NAME.' '.TRAP_ERROR;
			// $createCommand = ''.MYSQLDUMP_PATH.' --opt -h '.DB_HOST.' -u '.DB_USER.' -p'.DB_PASSWORD.' -r '.DIRECTORY_BACKUP_FILES.$thisFileName.'.txt '.DB_NAME.' '.TRAP_ERROR;
			// $createCommand = MYSQLDUMP_PATH . ' -h ' . DB_HOST .' -u '. DB_USER .' -p ' . DB_PASSWORD . ' --opt ' . DB_NAME . ' > ' . DIRECTORY_BACKUP_FILES . $thisFileName . '.txt.' .TRAP_ERROR;
			// $createCommand = "\"".MYSQLDUMP_PATH."\" > meinbackup.txt";
			// $createCommand = "\"".MYSQLDUMP_PATH."\" --opt --skip-extended-insert --complete-insert --host=" . DB_HOST ." --user=". DB_USER ." --password=" . DB_PASSWORD . " " . DB_NAME . " > ".DIRECTORY_BACKUP_FILES. $thisFileName;
			$createCommand = ''.MYSQLDUMP_PATH.' --opt -h '.DB_HOST.' -u '.DB_USER.' -p'.DB_PASSWORD.' -r '.DIRECTORY_BACKUP_FILES.$thisFileName.'.sql '.DB_NAME.' '.TRAP_ERROR;

			// $createCommand = escapeshellcmd($createCommand);
			$arrExecResults[] = $createCommand;
			$arrExecResults[] = exec($createCommand, $error);
			// $arrExecResults[] = shell_exec($createCommand);
			if(!empty($error)) {
				$Serror = array('0. BACKUP: mysqldump error', $error);
				$arrExecResults[] = $Serror;
			}
			*/
			// EOF TOTAL DB IN ONE FILE
		}

		// Make TAR archive
		if (APPLICATION_OS == 'WIN') {
			// chdir(preg_replace("/\//", "\\", BASEPATH));
			// chdir(BASEPATH);
			$arrExecResults[] = APPLICATION_OS;
			if($backUpType == "backupDatabase") {
				// BOF TOTAL DB IN ONE FILE
				/*
				// SQL file
					$createCommand = ZIP_PATH.' a -ttar '.DIRECTORY_BACKUP_FILES . $thisFileName.'.tar '.DIRECTORY_BACKUP_FILES.$thisFileName.'.sql ';
					$arrExecResults[] = $createCommand;
					$arrExecResults[] = exec($createCommand, $error);
					if(!empty($error)) {
						$arrExecResults[] = array('1. BACKUP: mysqldump error', $error);
					}
				*/
				// BOF TOTAL DB IN ONE FILE


				// BOF TOTAL DB IN SINGLE FILES
				if(!empty($arrTableNames)) {
					foreach($arrTableNames as $thisTableName) {
						$createCommand = ZIP_PATH.' a -ttar '.DIRECTORY_BACKUP_FILES . $thisFileName.'.tar '.DIRECTORY_BACKUP_FILES.$thisFileName.'_'.$thisTableName.'.sql ';
						$arrExecResults[] = $createCommand;
						$arrExecResults[] = exec($createCommand, $error);
						if(!empty($error)) {
							$arrExecResults[] = array('1. BACKUP: mysqldump error', $error);
						}
					}
				}
				// EOF TOTAL DB IN SINGLE FILES
			}

			if($backUpType == "backupImages" || $backUpType == "backupComplete") {
				// Images
					$createCommand = ZIP_PATH.' a -ttar -r '.DIRECTORY_BACKUP_FILES.$thisFileName.'.tar '.'images/* ';
					$arrExecResults[] = $createCommand;
					$arrExecResults[] = exec($createCommand, $error);
					if(!empty($error)) {
						$arrExecResults[] = array('2. BACKUP: backupImages error', $error);
					}
			}

			if($backUpType == "backupFiles" || $backUpType == "backupComplete") {

				$thisExclude = "";
				if(!empty($arrBackupExcludes)) {
					foreach($arrBackupExcludes as $thisValue) {
						$thisValue = preg_replace("/\/$/", "", $thisValue);
						$thisExclude .= ' --exclude=\'' . $thisValue . '\'';
					}
				}

				if(!empty($arrBackupIncludes)) {
					foreach($arrBackupIncludes as $thisInclude) {
						// $createCommand = ZIP_PATH.' a -ttar '.DIRECTORY_BACKUP_FILES.$thisFileName.'.tar '.$thisFolder.'/* -r';
						# $createCommand = "\"".ZIP_PATH."\" a ".$thisExclude." -ttar ".DIRECTORY_BACKUP_FILES.$thisFileName.".tar ".$thisInclude;
						$createCommand = ZIP_PATH.' a -ttar '.DIRECTORY_BACKUP_FILES.$thisFileName.'.tar '.$thisInclude;
						if(is_dir($thisInclude)) {
							$createCommand .= "/* -r";
						}
						clearstatcache();
						$arrExecResults[] = "thisInclude : " . $thisInclude;
						$arrExecResults[] = $createCommand;
						// $createCommand = escapeshellcmd($createCommand);
						$arrExecResults[] = exec($createCommand, $error);
						if(!empty($error)) {
							$arrExecResults[] = array('3. BACKUP: backupFiles error', $error);
						}
						#@chmod(DIRECTORY_BACKUP_FILES.$thisFileName.".tar", "0777");
					}
				}
			}

			// GZIP
				$createCommand = ZIP_PATH.' a -tgzip '.DIRECTORY_BACKUP_FILES.$thisFileName.'.tar.gz '.DIRECTORY_BACKUP_FILES.$thisFileName.'.tar';
				$arrExecResults[] = $createCommand;
				// $createCommand = escapeshellcmd($createCommand);
				$arrExecResults[] = exec($createCommand, $error);
				if(!empty($error)) {
					$arrExecResults[] = array('4. BACKUP: backupFiles error', $error);
				}
				if(file_exists(DIRECTORY_BACKUP_FILES.$thisFileName.'.tar')) {
					// @chmod(DIRECTORY_BACKUP_FILES.$thisFileName.".tar", "0777");
					unlink(DIRECTORY_BACKUP_FILES.$thisFileName.'.tar');
				}
				clearstatcache();
		}
		else {	// UNIX/LINUX

			$thisExclude = "";
			if(!empty($arrBackupExcludes)) {
				foreach($arrBackupExcludes as $thisValue) {
					$thisValue = preg_replace("/\/$/", "", $thisValue);
					$thisExclude .= ' --exclude=\'' . $thisValue . '\'';
				}
			}

			$createCommand = TAR_PATH.' -zcf '.DIRECTORY_BACKUP_FILES.$thisFileName.'.tar.gz ';
			// $createCommand = TAR_PATH.' --exclude=\'backupFiles\' -zcf '.DIRECTORY_BACKUP_FILES.$thisFileName.'.tar.gz ';

			if($backUpType == "backupDatabase" || $backUpType == "backupComplete") {
				// BOF TOTAL DB IN ONE FILE
				// SQL file
					$createCommand .= DIRECTORY_BACKUP_FILES.$thisFileName.'.sql ';
				// EOF TOTAL DB IN ONE FILE

				// BOF TOTAL DB IN SINGLE FILES
					if(!empty($arrTableNames)) {
						foreach($arrTableNames as $thisTableName) {
							$createCommand .= DIRECTORY_BACKUP_FILES.$thisFileName.'_'.$thisTableName.'.sql ';
						}
					}
				// EOF TOTAL DB IN SINGLE FILES
			}

			if($backUpType == "backupImages" || $backUpType == "backupComplete") {
				// Images
					$createCommand .= $basePath.'images/ ';

			}

			if($backUpType == "backupFiles" || $backUpType == "backupComplete") {
				if(!empty($arrBackupIncludes)){
					foreach($arrBackupIncludes as $thisInclude) {
						$createCommand .= $basePath.$thisInclude.'';
						if(is_dir($thisInclude)) {
							$createCommand .= '/';
						}
						clearstatcache();
						$createCommand .= ' ';
					}
				}
			}
			$createCommand = escapeshellcmd($createCommand);
			$arrExecResults[] = exec($createCommand.' '.TRAP_ERROR,$error);
		}
		if($backUpType == "backupDatabase" || $backUpType == "backupComplete") {
			if(file_exists(DIRECTORY_BACKUP_FILES.$thisFileName.'.sql')) {
				#@chmod(DIRECTORY_BACKUP_FILES.$thisFileName.".txt", "0777");
				unlink(DIRECTORY_BACKUP_FILES.$thisFileName.'.sql');
			}
			clearstatcache();
			if(!empty($arrTableNames)) {
				foreach($arrTableNames as $thisTableName) {
					if(file_exists(DIRECTORY_BACKUP_FILES.$thisFileName.'_'.$thisTableName.'.sql')) {
						#@chmod(DIRECTORY_BACKUP_FILES.$thisFileName.".txt", "0777");
						unlink(DIRECTORY_BACKUP_FILES.$thisFileName.'_'.$thisTableName.'.sql');
					}
					clearstatcache();
				}
			}
		}

		return $arrExecResults;
	}

	function restoreDatabase($folderPath, $thisFileNames, $backUpType) {
		global $dbConnection, $arrBackupIncludes, $arrBackupExcludes, $arrBackUpFilenames, $_POST;

		$arrExecResults = array();
		$arrExecResults[] = $backUpType;

		if(!empty($_POST["unzipBackupFileNames"])){
			foreach($_POST["unzipBackupFileNames"] as $thisKey => $thisValue){
				$arrExecResults[] = $thisKey .' => ' . $thisValue;
				$thisFileType = getFileType(basename($thisValue));
				$thisFileType = strtolower($thisFileType);
				$arrThisFilePathinfo =  pathinfo(basename($thisValue));
				$destinationFilename = preg_replace("/\." . $thisFileType. "/", "", $thisValue);
				$arrDestinationThisFilePathinfo =  pathinfo(basename($destinationFilename));
				$destinationFileType = getFileType(basename($destinationFilename));
				$destinationFileType = strtolower($destinationFileType);
				if(file_exists($destinationFilename)){
					$arrExecResults[] = 'UNLINK ' . unlink($destinationFilename);
				}
				$createCommand = UNZIP_PATH . " e " . $folderPath . $thisValue;
				#$createCommand = escapeshellcmd($createCommand);
				$arrExecResults[] = $arrThisFilePathinfo;
				$arrExecResults[] = $arrDestinationThisFilePathinfo;
				$arrExecResults[] = 'COMMAND: ' . $createCommand;
				$arrExecResults[] = 'EXEC: ' . exec($createCommand, $error, $output);
				if(!empty($error)) {
					$arrExecResults[] = array('1. RESTORE: unpack error', $error);
				}
				if(!empty($output)) {
					$arrExecResults[] = array('2. RESTORE: unpack output', $output);
				}
				if(file_exists($destinationFilename)){
					$arrExecResults[] = 'COPY ' . copy($destinationFilename, $folderPath.$destinationFilename);
					$arrExecResults[] = 'UNLINK ' . unlink($destinationFilename);
				}

				#copy($location.‘/’.$file,$newLocation.‘/’.$file);
                #unlink($location.‘/’.$file);
			}

			foreach (glob("*.sql") as $filename) {
				$arrExecResults[] = $filename . " - SIZE: " . filesize($filename);
				#$createCommand = "copy *.sql > " . $folderPath;
				#$createCommand = "mv " . $filename . " " . $folderPath . $filename;
				#$createCommand = "copy *.sql " . $folderPath;
				#$arrExecResults[] = 'COMMAND: ' . $createCommand;
				#$arrExecResults[] = 'EXEC: ' . exec($createCommand, $error, $output);
				#if(!empty($error)) {
					#$arrExecResults[] = array('1. COPY: copy error', $error);
				#}
				#if(!empty($output)) {
					#$arrExecResults[] = array('2. COPY: copy output', $output);
				#}
				if(file_exists($folderPath . $filename)){
					unlink($folderPath . $filename);
				}
				$arrExecResults[] = copy($filename, $folderPath . $filename);
				if(file_exists($folderPath . $filename)){
					unlink($filename);
				}
				clearstatcache();
			}
		}
		else if(!empty($_POST["restoreBackupFileNames"])){
			foreach($_POST["restoreBackupFileNames"] as $thisKey => $thisValue){
				$arrExecResults[] = $thisKey .' => ' . $thisValue;
				if(file_exists($folderPath.$thisValue)){
					$arrExecResults[] = $thisValue . ' existiert.';

					/*
					$fp = fopen($folderPath.$thisValue, 'r');
					$thisFileContent = fread($fp, filesize($folderPath.$thisValue));
					fclose($fp);

					#$thisFileContent = preg_replace("/--(.*)\\n/", "", $thisFileContent);
					#$thisFileContent = preg_replace("/\/\*(.*)\*\/;\\n/", "", $thisFileContent);
					#$thisFileContent = preg_replace("/;\\n/", ";####BREAK####", $thisFileContent);
					$arrSql = explode("####BREAK####", $thisFileContent);

					if(!empty($arrSql)){

						foreach($arrSql as $thisSql){
							$thisSql = trim($thisSql);
							if($thisSql != "") {
								$rs = $dbConnection->db_query($thisSql);
								if(mysqli_error($db_open)){
									$arrExecResults[] = $thisSql;
									$arrExecResults[] = mysqli_error($db_open);
								}
							}
							clearstatcache();
						}
					}
					*/
					#$createCommand = mysqli_PATH . ' -h '.DB_HOST.' -u '.DB_USER.' -p'.DB_PASSWORD.' ' .DB_NAME.' < '.$folderPath.$thisValue;
					$createCommand = 'mysql -u '.DB_USER.' -p'.DB_PASSWORD.' ' .DB_NAME.' < '.BASEPATH.$folderPath.$thisValue;
					$createCommand = mysqli_PATH . ' -u '.DB_USER.' -p'.DB_PASSWORD.' ' .DB_NAME.' < '.BASEPATH.$folderPath.$thisValue;
					#$createCommand = addslashes($createCommand);
					$arrExecResults[] = 'COMMAND: ' . $createCommand;
					$arrExecResults[] = 'EXEC: ' . exec($createCommand);
					if(!empty($error)) {
						$arrExecResults[] = array('1. RESTORE: insert error', $error);
					}
					if(!empty($output)) {
						$arrExecResults[] = array('2. RESTORE: insert output', $output);
				}
				}
				else {
					$arrExecResults[] = $thisValue . ' existiert nicht.';
				}
				clearstatcache();
			}
		}

		return $arrExecResults;
	}

	$thisFileName = $_SERVER['HTTP_HOST']."_".str_replace("{###DATE###}", date("Y-m-d_H-i-s"), $arrBackUpFilenames[$_POST["selectBackupAction"]]);
	$downloadFile = false;

	if($_POST["selectBackupAction"] == "backupDatabase") {
		$arrExecResults = makeBackup($_POST["selectBackupAction"], $thisFileName);
		$downloadFile = true;
	}
	else if($_POST["selectBackupAction"] == "backupImages") {
		$arrExecResults = makeBackup($_POST["selectBackupAction"], $thisFileName);
		$downloadFile = true;
	}
	else if($_POST["selectBackupAction"] == "backupFiles") {
		$arrExecResults = makeBackup($_POST["selectBackupAction"], $thisFileName);
		$downloadFile = true;
	}
	else if($_POST["selectBackupAction"] == "backupComplete") {
		$arrExecResults = makeBackup($_POST["selectBackupAction"], $thisFileName);
		$downloadFile = true;
	}
	else if($_POST["selectBackupAction"] == "deleteBackups") {
		$arrBackUpFilesToDelete = read_dir(DIRECTORY_BACKUP_FILES);
		$arrExecResults = deleteBackups(DIRECTORY_BACKUP_FILES, $_POST["selectBackupAction"], $arrBackUpFilesToDelete);
		$downloadFile = false;
	}
	else if($_POST["selectBackupAction"] == "showBackups") {
		if(!empty($_POST["deleteBackupFileNames"])){
			$arrBackUpFilesToDelete = $_POST["deleteBackupFileNames"];
			$arrExecResults = deleteBackups(DIRECTORY_BACKUP_FILES, $_POST["selectBackupAction"], $arrBackUpFilesToDelete);
			$downloadFile = false;
		}
	}
	else if($_POST["selectBackupAction"] == "restoreDatabase") {
		$arrExecResults = restoreDatabase(DIRECTORY_BACKUP_FILES, '', $_POST["selectBackupAction"]);
		$downloadFile = false;
	}

	$downloadFilePath = DIRECTORY_BACKUP_FILES.$thisFileName.".tar.gz";

	if($_POST["selectBackupAction"] == "backupDatabase" || $_POST["selectBackupAction"] == "backupImages" || $_POST["selectBackupAction"] == "backupFiles" || $_POST["selectBackupAction"] == "backupComplete") {
		require_once('classes/downloadFile.class.php');
		$thisDownload = new downloadFile(DIRECTORY_BACKUP_FILES, basename($downloadFilePath));
		$errorMessage .= $thisDownload->startDownload();
	}

 ?>

<?php
	require_once('inc/headerHTML.inc.php');
	$thisTitle = "DATEN-SICHERUNG";
	$headerHTML = preg_replace("/{###TITLE###}/", strip_tags($thisTitle), $headerHTML);
	echo $headerHTML;

	require_once(FILE_MENUE_TOP);
?>

<div id="mainArea">
	<div id="mainContent">
		<?php require_once(FILE_MENUE_SIDEBAR); ?>

<div id="contentArea">
	<div id="contentAreaElements">
		<h1><?php if(SHOW_PAGE_TITLE_ICONS) { echo '<img src="' . PATH_ICONS_MENUE_TITLES . 'export.png" alt="" />'; } ?> <?php echo $thisTitle; ?></h1>
		<div class="contentDisplay">
		<?php displayMessages(); ?>
		<h2>MS-DOS Kommadozeilen</h2>
		<p class="infoArea">

			&bull; C:\Programme\7-Zip\7z.exe a Z:\Outlook\THORSTEN_Auftragslisten\temp\###FILE_NAME###}.7z C:\Auftragslisten_SERVER\webroot\Auftragslisten\<br />
			&bull; copy X:\Auftragslisten_SICHERUNGEN\{###FILE_NAME###} E:\myFILES\Auftragslisten\{###FILE_NAME###}<br />
			&bull; copy Z:\THORSTEN_Auftragslisten\temp\{###FILE_NAME###}	X:\Auftragslisten_SICHERUNGEN\{###FILE_NAME###}<br />
			&bull; copy Z:\THORSTEN_Auftragslisten\temp\{###FILE_NAME###}	E:\myFILES\Auftragslisten\{###FILE_NAME###}<br />

		</p>

		<div id="searchFilterArea">
		<form name="formAdminBackup" action="<?php echo $_SERVER["PHP_SELF"]; ?>" method="post">
		<table border="0" width="100%" cellspacing="0" cellpadding="0">
			<tr>
				<?php
					if($_POST["selectBackupAction"] == "" || empty($_POST["selectBackupAction"])){
						#$_POST["selectBackupAction"] = "showBackups";
					}
				?>
				<td width="200"><b>Aktion ausw&auml;hlen:</b></td>
				<td width="300">
					<select name="selectBackupAction" class="inputSelect_510" <?php if($_POST["selectBackupAction"] != ""){ echo ' disabled="true" '; } ?>	>
						<option value="-"> --- Bitte w&auml;hlen --- </option>
						<option value="showBackups">Sicherungen anzeigen</option>
						<option value="backupDatabase" <?php if($_POST["selectBackupAction"] == "backupDatabase") { echo " selected "; } ?> >Datenbank-Sicherung erstellen</option>
						<option value="restoreDatabase" <?php if($_POST["selectBackupAction"] == "restoreDatabase") { echo " selected "; } ?> >Datenbank-Sicherung einspielen</option>
						<!--
						<option value="backupImages" <?php if($_POST["selectBackupAction"] == "backupImages") { echo " selected "; } ?> >Bilder-Sicherung erstellen</option>
						-->
						<option value="backupFiles" <?php if($_POST["selectBackupAction"] == "backupFiles") { echo " selected "; } ?> >Dateien-Sicherung erstellen</option>
						<option value="backupComplete" <?php if($_POST["selectBackupAction"] == "backupComplete") { echo " selected "; } ?> >Komplett-Sicherung erstellen</option>
						<option value="deleteBackups" <?php if($_POST["selectBackupAction"] == "deleteBackups") { echo " selected "; } ?> >Alle BackUps l&ouml;schen</option>
					</select>
				</td>
				<?php
					if($_POST["selectBackupAction"] == "") {
				?>
				<td width="100">
					<input type="submit" name="submitForm" class="inputButton2" value="Starten" />
				</td>

				<?php
					}
					else {
				?>
				<td width="100">
					<!--
					<a href="<?php echo $_SERVER["PHP_SELF"]; ?>" class="linkButton"><span>Zur&uuml;ck</span></a>
					-->
					<input type="submit" name="resetForm" class="inputButton2" value="Zur&uuml;ck" />
				</td>
				<?php
					}
				?>
			</tr>
		</table>
		</form>
		</div>

		<h2>Vorhandene Sicherungen:</h2>
		<div class="adminEditArea">
		<?php
			$readDir = DIRECTORY_BACKUP_FILES;
			if(is_dir($readDir)) {
				$arrBackUpFiles = read_dir($readDir);
			}
			clearstatcache();
			if(!empty($arrBackUpFiles)) {
				sort($arrBackUpFiles);

				if($_POST["selectBackupAction"] == "restoreDatabase") {
					$formName = "formRestoreBackupFiles";
					$classCheckbox = "restoreBackupFileNames";
				}
				else if($_POST["selectBackupAction"] == "showBackups") {
					$formName = "formShowBackupFiles";
					$classCheckbox = "deleteBackupFileNames";
				}
				echo '<form name="' . $formName . '" method="post" action="">';
				echo '<ul>';
				$count = 0;
				foreach($arrBackUpFiles as $thisFile) {
					$thisFilesize = filesize($thisFile);

					if($count%2 == 0){ $rowClass = 'row0'; }
					else { $rowClass = 'row1'; }

					echo '<li class="'.$rowClass.'" style="border-bottom:1px dotted #333;">';

					$thisFormattedFilesize = formatFilesizeAuto($thisFilesize);
					echo '<a href="'.$thisFile.'">'.basename($thisFile).'</a> ('. $thisFormattedFilesize .')';
					if($_POST["selectBackupAction"] == "restoreDatabase") {
						$thisFileType = getFileType(basename($thisFile));
						$thisFileType = strtolower($thisFileType);
						$arrPathinfo =  pathinfo(basename($thisFile));

						if($thisFileType != "sql"){
							if(in_array($thisFileType, array('gz', 'tar', 'zip', '7z'))){
								echo '<br /> <input type="checkbox" class="unzipBackupFileNames" name="unzipBackupFileNames[]" value="'.basename($thisFile).'" /> <span class="">Keine SQL-Datei. Datei erst entpacken?</span>';
							}
							else {
								echo '<br /> <span class="">Keine SQL-Datei.</span>';
							}
						}
						else {
							echo '<br /> <input type="checkbox" class="restoreBackupFileNames" name="restoreBackupFileNames[]" value="'.basename($thisFile).'" /> <span class="">Diese Datei einspielen?</span>';
						}
					}
					else if($_POST["selectBackupAction"] != ""){
						echo '<br /> <input type="checkbox" class="deleteBackupFileNames" name="deleteBackupFileNames[]" value="'.basename($thisFile).'" /> <span class="">Diese Datei entfernen?</span>';
					}
					echo '</li>';
					$count++;
				}
				echo '</ul>';

				if($_POST["selectBackupAction"] != ""){
					echo '<input type="hidden" name="selectBackupAction" value="' . $_POST["selectBackupAction"] . '" />';
					echo '<input type="button" name="buttonCheckAll" onclick="$(\'.'.$classCheckbox.'\').attr(\'checked\', \'checked\');$(\'.'.$classCheckbox.'\').parent().css(\'background-color\',\'#00FF00\')" value="Alle ausw&auml;hlen" />';
					echo '<input type="button" name="buttonUncheckAll" onclick="$(\'.'.$classCheckbox.'\').attr(\'checked\', false);$(\'.'.$classCheckbox.'\').parent().css(\'background-color\',\'\')" value="Alle abw&auml;hlen" />';
					echo '<input type="submit" name="submitFormRestoreBackupFiles" value="Weiter" />';
				}
				echo '</form>';
			}
			else {
				echo '<p class="infoArea">Keine Sicherungs-Dateien vorhanden.</p>';
			}
		?>

		<?php
			if(!empty($arrExecResults)) {
				echo '<pre class="warningArea">';
				print_r($arrExecResults);
				echo '</pre>';
			}
		?>

		</div>

		</div>
		<div class="clear"></div>
	</div>
</div>
</div>
</div>
<script language="javascript" type="text/javascript">
	<!--
	/* <![CDATA[ */
	var animateTime = 1;
	$(document).ready(function() {
		$('.restoreBackupFileNames').click(function(){
			var thisChecked = $(this).attr('checked');
			if(thisChecked == 'checked') {
				$(this).parent().css('background-color','#00FF00');
			}
			else {
				$(this).parent().css('background-color','');
			}
		});
		$('.deleteBackupFileNames').click(function(){
			var thisChecked = $(this).attr('checked');
			if(thisChecked == 'checked') {
				$(this).parent().css('background-color','#00FF00');
			}
			else {
				$(this).parent().css('background-color','');
			}
		});
		$('.unzipBackupFileNames').click(function(){
			var thisChecked = $(this).attr('checked');
			if(thisChecked == 'checked') {
				$(this).parent().css('background-color','#FFFF00');
			}
			else {
				$(this).parent().css('background-color','');
			}
		});

	});
	/* ]]> */
	// -->
</script>

<?php require_once('inc/footerHTML.inc.php'); ?>
