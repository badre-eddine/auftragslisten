<?php

// autoload_static.php @generated by Composer

namespace Composer\Autoload;

class ComposerStaticInitb90d9d54a9710aaf18ee3c4af6f7e37b
{
    public static $prefixLengthsPsr4 = array (
        'P' => 
        array (
            'PHPMailer\\PHPMailer\\' => 20,
        ),
    );

    public static $prefixDirsPsr4 = array (
        'PHPMailer\\PHPMailer\\' => 
        array (
            0 => __DIR__ . '/..' . '/phpmailer/phpmailer/src',
        ),
    );

    public static $classMap = array (
        'Composer\\InstalledVersions' => __DIR__ . '/..' . '/composer/InstalledVersions.php',
    );

    public static function getInitializer(ClassLoader $loader)
    {
        return \Closure::bind(function () use ($loader) {
            $loader->prefixLengthsPsr4 = ComposerStaticInitb90d9d54a9710aaf18ee3c4af6f7e37b::$prefixLengthsPsr4;
            $loader->prefixDirsPsr4 = ComposerStaticInitb90d9d54a9710aaf18ee3c4af6f7e37b::$prefixDirsPsr4;
            $loader->classMap = ComposerStaticInitb90d9d54a9710aaf18ee3c4af6f7e37b::$classMap;

        }, null, ClassLoader::class);
    }
}
