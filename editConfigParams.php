<?php
	require_once('inc/requires.inc.php');

	// 'BANK_DATAS','BASIC_CONFIG','COMPANY_DATAS','DB_CONFIG','DB_TABLES','FILES_CONFIG','FTP_CONFIG','MAIL_CONFIG','PHP_INI'

	if(!$arrGetUserRights["createParams"]) {
		header('location: ' . PAGE_EXIT_LOCATION);
		exit;
	}

	$jswindowMessage = "";
	$warningMessage = '';
	$errorMessage = '';
	$successMessage = '';
	$infoMessage = '';

	$dbConnection = new DB_Connection();
	$db_open = $dbConnection->db_connect();

	// BOF STORE DATAS
	if($_POST["storeDatas"] != "") {

		$doAction = checkFormDutyFields($arrFormDutyFields);

		if($doAction) {
			$sql = "";

			if(!empty($_POST["newConfigDatas"])) {
				foreach($_POST["newConfigDatas"] as $thisKey => $thisValue){
					if(trim($thisValue["category"]) != "" && trim($thisValue["group"]) != "" && trim($thisValue["param"]) != "" && trim($thisValue["name"]) != "") {
						$sql = " REPLACE INTO `" . TABLE_CONFIG_DATAS . "` (
										`configDatasID`,
										`configDatasCategorie`,
										`configDatasGroupID`,
										`configDatasParam`,
										`configDatasName`,
										`configDatasDescription`,
										`configDatasValue`,
										`configDatasSetValueFieldType`,
										`configDatasSetValueFieldValues`
									)
									VALUES (
										'%',
										'" . trim($thisValue["category"]) . "',
										'" . trim($thisValue["group"]) . "',
										'" . trim($thisValue["param"]) . "',
										'" . trim($thisValue["name"]) . "',
										'',
										'" . trim($thisValue["value"]) . "',
										'" . trim($thisValue["fieldType"]) . "',
										'" . trim($thisValue["fieldValue"]) . "'
								);";
						$rs = $dbConnection->db_query($sql);
						if($rs) {
							$successMessage .= ' Der Datensatz "' . $thisValue["param"] . '" wurde gespeichert.' .'<br />';

							$sql = "UPDATE `bctr_configdatas`
										SET `configDatasSetValueFieldType` = 'boolean',
											`configDatasSetValueFieldValues` = '" . json_encode(array('true','false')) . "'
										WHERE `configDatasValue` = 'true'
											OR `configDatasValue` = 'false'
							";


							$rs = $dbConnection->db_query($sql);
						}
						else {
							$errorMessage .= ' Der Datensatz "' . $thisValue["param"]. '" konnte nicht gespeichert werden.' .'<br />';
						}
					}
				}
			}
			writeConfigFile();
		}
		else {
			$errorMessage .= ' Der Datensatz konnte nicht gespeichert werden. Bitte füllen Sie alle Pflichtfelder aus.' .'<br />';
		}
	}
	// EOF STORE DATAS

	// BOF READ ALL DATAS
		$sql = "SELECT

					`configDatasID`,
					`configDatasCategorie`,
					`configDatasGroupID`,
					`configDatasParam`,
					`configDatasName`,
					`configDatasDescription`,
					`configDatasValue`,
					`configDatasSetValueFieldType`,
					`configDatasSetValueFieldValues`

				FROM `" . TABLE_CONFIG_DATAS . "`

				WHERE 1

				ORDER BY
					`configDatasCategorie`,
					`configDatasGroupID`,
					`configDatasParam`
		";
		$rs = $dbConnection->db_query($sql);

		while($ds = mysqli_fetch_assoc($rs)) {
			foreach(array_keys($ds) as $field) {
				$arrConfigDatas[$ds["configDatasCategorie"]][$ds["configDatasGroupID"]][$ds["configDatasID"]][$field] = $ds[$field];
			}
		}
	// EOF READ ALL DATAS
?>
<?php
	require_once('inc/headerHTML.inc.php');

	$thisTitle = "Parameter hinzuf&uuml;gen";

	$headerHTML = preg_replace("/{###TITLE###}/", strip_tags($thisTitle), $headerHTML);
	echo $headerHTML;

	require_once(FILE_MENUE_TOP);
?>
<div id="mainArea">
	<div id="mainContent">
		<?php require_once(FILE_MENUE_SIDEBAR); ?>
		<div id="contentArea">
			<div id="contentAreaElements">
				<h1><?php if(SHOW_PAGE_TITLE_ICONS) { echo '<img src="' . PATH_ICONS_MENUE_TITLES . 'params.png" alt="" />'; } ?> <?php echo $thisTitle; ?></h1>
				<div class="contentDisplay">
					<?php displayMessages(); ?>

					<div class="adminEditArea">
						<form name="editConfigDatas" method="post" action="<?php echo $_SERVER["PHP_SELF"]; ?>">

						<!--
						<p class="warningArea">Pflichtfelder m&uuml;ssen ausgef&uuml;llt werden!</p>
						-->

						<fieldset>
							<legend>Neue Parameter anlegen</legend>
							<table border="0" width="100%" cellspacing="0" cellpadding="0">
								<tr>
									<th>Kategorie</th>
									<th>Gruppe</th>
									<th>Parameter</th>
									<th>Name</th>
									<th>Wert</th>
									<th>Feld-Typ</th>
									<th>Feld-Werte</th>
								</tr>
								<?php
									for($i = 0 ; $i < 10 ; $i++) {
										echo '<tr>';
										echo '<td><input type="text" id="newConfigDatas_category_' . $i . '" name="newConfigDatas[' . $i . '][category]" class="inputField_100" value="" /></td>';
										echo '<td><input type="text" id="newConfigDatas_group_' . $i . '" name="newConfigDatas[' . $i . '][group]" class="inputField_100" value="" /></td>';
										echo '<td><input type="text" id="newConfigDatas_param_' . $i . '" name="newConfigDatas[' . $i . '][param]" class="inputField_100" value="" /></td>';
										echo '<td><input type="text" id="newConfigDatas_name_' . $i . '" name="newConfigDatas[' . $i . '][name]" class="inputField_100" value="" /></td>';
										echo '<td><input type="text" id="newConfigDatas_value_' . $i . '" name="newConfigDatas[' . $i . '][value]" class="inputField_160" value="" /></td>';
										echo '<td>';
										echo '<select id="newConfigDatas_fieldType_' . $i . '" name="newConfigDatas[' . $i . '][fieldType]" class="inputSelect_70">';
											echo '<option value="input" selected="selected">input</option>';
											echo '<option value="select">select</option>';
											echo '<option value="boolean">boolean</option>';
										echo '</select>';
										echo '</td>';
										echo '<td><input type="text" id="newConfigDatas_fieldValue_' . $i . '" name="newConfigDatas[' . $i . '][fieldValue]" class="inputField_160" value="" /></td>';
										echo '</tr>';
									}
								?>
							</table>
						</fieldset>

						<?php
							if($arrGetUserRights["createParams"]) {
						?>
						<div class="actionButtonsArea">
							<input type="submit" class="inputButton1" name="storeDatas" value="Speichern" onclick="return checkFormDutyFields(this.form.name, <?php echo $jsonFormDutyFields; ?>); return showWarning(' Wollen Sie diesen Vorgang wirklich speichern??? '); "/>
							&nbsp;
							<input type="submit" class="inputButton1" name="resetDatas" value="Abbrechen" onclick="return showWarning(' Wollen Sie diesen Vorgang wirklich abbrechen??? '); "/>
						</div>
						<?php
							}
						?>
						</form>

						<hr />

						<h2>xVorhandene Parameter</h2>
						<?php
							if(!empty($arrConfigDatas)) {
								echo '<table border="0" width="100%" cellspacing="0" cellpadding="0" class="displayOrders">';
								echo '	<tr>
											<th width="16%">Parameter</th>
											<th width="16%">Typ</th>
											<th width="16%">Wert</th>
											<th width="16%">Name</th>
											<th width="16%">Gruppe</th>
											<th width="16%">Kategorie</th>
											<th width="4%">ID</th>
										</tr>
								';
								$count = 0;
								foreach($arrConfigDatas as $thisConfigCategoryKey => $thisConfigCategoryValue){
									if(!empty($thisConfigCategoryValue)) {
										foreach($thisConfigCategoryValue as $thisConfigGroupKey => $thisConfigGroupValue){
											foreach($thisConfigGroupValue as $thisConfigDataKey => $thisConfigDataValue){
												if($count%2 == 0){ $rowClass = 'row0'; }
												else { $rowClass = 'row1'; }
												echo '<tr class="' . $rowClass . '">'	;
												echo '<td style="font-size:10px;"><b>' . $thisConfigDataValue["configDatasParam"] . '</b></td>';
												echo '<td style="font-size:10px;"><b>' . $thisConfigDataValue["configDatasSetValueFieldType"] . '</b></td>';
												echo '<td>' . htmlentities(utf8_decode($thisConfigDataValue["configDatasValue"])) . '</td>';
												echo '<td style="font-size:10px;">' . $thisConfigDataValue["configDatasName"] . '</td>';
												echo '<td style="font-size:10px;">' . $thisConfigGroupKey . '</td>';
												echo '<td style="font-size:10px;">' . $thisConfigCategoryKey . '</td>';
												echo '<td style="font-size:10px;text-align:right;">' . $thisConfigDataValue["configDatasID"] . '</td>';
												echo '</tr>';
												$count++;
											}
										}
									}
								}
								echo '</table>';
							}
						?>
					</div>
				</div>
			</div>
		</div>
		<div class="clear"></div>
	</div>
</div>

<script language="javascript" type="text/javascript">
	$(document).ready(function() {
		$(function() {
			$( "#datepicker" ).datepicker();
		});
		colorRowMouseOver('.displayOrders tbody tr');
		setFormDutyFields(<?php echo $jsonFormDutyFields; ?>);
	});
</script>

<?php require_once('inc/footerHTML.inc.php'); ?>