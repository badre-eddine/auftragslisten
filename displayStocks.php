<?php
	require_once('inc/requires.inc.php');

	DEFINE('DATE_STOCK_START', '2013-02-10');

	if(!$arrGetUserRights["editStocks"] && !$arrGetUserRights["displayStocks"]) {
		header('location: ' . PAGE_EXIT_LOCATION);
		exit;
	}

	$jswindowMessage = "";
	$warningMessage = '';
	$errorMessage = '';
	$successMessage = '';
	$infoMessage = '';

	$dbConnection = new DB_Connection();
	$db_open = $dbConnection->db_connect();

	// BOF READ ORDER CATEGORIES
		$arrOrderCategoriesTypeDatas = getOrderCategories('all');
	// EOF READ ORDER CATEGORIES

	// BOF DELETE
		if($_GET["deleteNewStock"] == "1" && $_GET["deleteStockItemID"] != "") {
			$sql = "DELETE FROM `" . TABLE_STOCK_ITEMS . "`
						WHERE `stockItemsID` = '" . $_GET["deleteStockItemID"] . "'
				";
			$rs = $dbConnection->db_query($sql);
			if($rs) {
				$successMessage .= ' Der Eintrag wurde entfernt.' .'<br />';
			}
			else {
				$errorMessage .= ' Der Eintrag konnte nicht entfernt werden.' . '<br />';
			}
		}
	// EOF DELETE

	// BOF STORE STOCK
	if($_POST["storeNewStock"] != "") {
		$sql = "INSERT INTO `" . TABLE_STOCK_ITEMS . "` (
									`stockItemsID`,
									`stockItemsDate`,
									`stockItemsQuantity`,
									`stockItemsCategoryID`,
									`stockItemsStoreDatetime`
								)
								VALUES (
									'%',
									'".formatDate($_POST["newStockDate"], 'store')."',
									'".$_POST["newStockQuantity"]."',
									'".$_POST["newStockCategory"]."',
									NOW()
								)
		";
		$rs = $dbConnection->db_query($sql);
		if($rs) {
				$successMessage .= ' Der Eintrag wurde gespeichert.' .'<br />';
			}
			else {
				$errorMessage .= ' Der Eintrag konnte nicht gespeichert werden.' . '<br />';
			}
	}
	// EOF STORE STOCK


	// BOF GET CONSUMPTION
	$sql = "
			SELECT
					`orderCategoriesName`,
					`orderCategoriesPartNumber`,
					`orderCategoriesRelationID`,
					`orderCategoriesUseInStock`,
					`tempTable`.`ordersBestellDatum`,
					`tempTable`.`ordersBestellWoche`,
					`tempTable`.`ordersBestellJahr`,
					`tempTable`.`ordersProductMenge`,
					`tempTable`.`ordersCategoryID`,
					`tempTable`.`ordersWeek`,
					`tempTable`.`ordersStatus`

				FROM (
					SELECT
							DATE_FORMAT(`ordersBestellDatum`, '%Y') AS `ordersBestellJahr`,
							DATE_FORMAT(`ordersBestellDatum`, '%u') AS `ordersBestellWoche`,
							IF(`ordersArtikelKategorieID` IS NULL || `ordersArtikelKategorieID` = '', '000', `ordersArtikelKategorieID`) AS `ordersCategoryID`,

							`ordersBestellDatum`,
							SUM(`ordersArtikelMenge`) AS `ordersProductMenge`,
							CONCAT(DATE_FORMAT(`ordersBestellDatum`, '%Y'), '-', DATE_FORMAT(`ordersBestellDatum`, '%u')) AS `ordersWeek`,
							`ordersStatus`

						FROM `" . TABLE_ORDERS . "`
						WHERE `ordersBestellDatum` != '0000-00-00'
							/* AND `ordersUseNoStock` != '1' */
							AND (`ordersStatus` = '2' AND `ordersBestellDatum` > '" . DATE_STOCK_START . "')

						GROUP BY CONCAT(`ordersBestellJahr`, '-', `ordersBestellWoche`, ';', `ordersArtikelKategorieID`)

					UNION

					SELECT
							DATE_FORMAT(`ordersBestellDatum`, '%Y') AS `ordersBestellJahr`,
							DATE_FORMAT(`ordersBestellDatum`, '%u') AS `ordersBestellWoche`,
							IF(`ordersAdditionalArtikelKategorieID` IS NULL || `ordersAdditionalArtikelKategorieID` = '', '000', `ordersAdditionalArtikelKategorieID`) AS `ordersCategoryID`,

							`ordersBestellDatum`,
							SUM(`ordersAdditionalArtikelMenge`) AS `ordersProductMenge`,
							CONCAT(DATE_FORMAT(`ordersBestellDatum`, '%Y'), '-', DATE_FORMAT(`ordersBestellDatum`, '%u')) AS `ordersWeek`,
							`ordersStatus`

						FROM `" . TABLE_ORDERS . "`
						WHERE `ordersBestellDatum` != '0000-00-00'
							/* AND `ordersUseNoStock` != '1' */
							AND (`ordersStatus` = '2' AND `ordersBestellDatum` > '" . DATE_STOCK_START . "')

						GROUP BY CONCAT(`ordersBestellJahr`, '-', `ordersBestellWoche`, ';', `ordersAdditionalArtikelKategorieID`)

					/* ################################## */

					UNION

					/* ################################## */

						SELECT
							DATE_FORMAT(`ordersFreigabeDatum`, '%Y') AS `ordersBestellJahr`,
							DATE_FORMAT(`ordersFreigabeDatum`, '%u') AS `ordersBestellWoche`,
							IF(`ordersArtikelKategorieID` IS NULL || `ordersArtikelKategorieID` = '', '000', `ordersArtikelKategorieID`) AS `ordersCategoryID`,

							`ordersFreigabeDatum`,
							SUM(`ordersArtikelMenge`) AS `ordersProductMenge`,
							CONCAT(DATE_FORMAT(`ordersFreigabeDatum`, '%Y'), '-', DATE_FORMAT(`ordersFreigabeDatum`, '%u')) AS `ordersWeek`,
							`ordersStatus`

						FROM `" . TABLE_ORDERS . "`
						WHERE `ordersFreigabeDatum` != '0000-00-00'
							/* AND `ordersUseNoStock` != '1' */
							AND (`ordersStatus` = '2' AND `ordersFreigabeDatum` > '" . DATE_STOCK_START . "')
						GROUP BY CONCAT(`ordersBestellJahr`, '-', `ordersBestellWoche`, ';', `ordersArtikelKategorieID`)

					UNION

					SELECT
							DATE_FORMAT(`ordersFreigabeDatum`, '%Y') AS `ordersBestellJahr`,
							DATE_FORMAT(`ordersFreigabeDatum`, '%u') AS `ordersBestellWoche`,
							IF(`ordersAdditionalArtikelKategorieID` IS NULL || `ordersAdditionalArtikelKategorieID` = '', '000', `ordersAdditionalArtikelKategorieID`) AS `ordersCategoryID`,

							`ordersFreigabeDatum`,
							SUM(`ordersAdditionalArtikelMenge`) AS `ordersProductMenge`,
							CONCAT(DATE_FORMAT(`ordersFreigabeDatum`, '%Y'), '-', DATE_FORMAT(`ordersFreigabeDatum`, '%u')) AS `ordersWeek`,
							`ordersStatus`

						FROM `" . TABLE_ORDERS . "`
						WHERE `ordersFreigabeDatum` != '0000-00-00'
							/* AND `ordersUseNoStock` != '1' */
							AND (`ordersStatus` = '2' AND `ordersFreigabeDatum` > '" . DATE_STOCK_START . "')

						GROUP BY CONCAT(`ordersBestellJahr`, '-', `ordersBestellWoche`, ';', `ordersAdditionalArtikelKategorieID`)

					/* ################################## */

					UNION

					/* ################################## */

						SELECT
							DATE_FORMAT(`ordersBelichtungsDatum`, '%Y') AS `ordersBestellJahr`,
							DATE_FORMAT(`ordersBelichtungsDatum`, '%u') AS `ordersBestellWoche`,
							IF(`ordersArtikelKategorieID` IS NULL || `ordersArtikelKategorieID` = '', '000', `ordersArtikelKategorieID`) AS `ordersCategoryID`,

							`ordersBelichtungsDatum`,
							SUM(`ordersArtikelMenge`) AS `ordersProductMenge`,
							CONCAT(DATE_FORMAT(`ordersBelichtungsDatum`, '%Y'), '-', DATE_FORMAT(`ordersBelichtungsDatum`, '%u')) AS `ordersWeek`,
							`ordersStatus`

						FROM `" . TABLE_ORDERS . "`
						WHERE `ordersBelichtungsDatum` != '0000-00-00'
							/* AND `ordersUseNoStock` != '1' */
							AND (
								(`ordersStatus` = '3' AND `ordersBelichtungsDatum` > '" . DATE_STOCK_START . "')
								OR
								(`ordersStatus` = '7' AND `ordersProduktionsDatum` > '" . DATE_STOCK_START . "')
							)

						GROUP BY CONCAT(`ordersBestellJahr`, '-', `ordersBestellWoche`, ';', `ordersArtikelKategorieID`)

					UNION

						SELECT
							DATE_FORMAT(`ordersBelichtungsDatum`, '%Y') AS `ordersBestellJahr`,
							DATE_FORMAT(`ordersBelichtungsDatum`, '%u') AS `ordersBestellWoche`,
							IF(`ordersAdditionalArtikelKategorieID` IS NULL || `ordersAdditionalArtikelKategorieID` = '', '000', `ordersAdditionalArtikelKategorieID`) AS `ordersCategoryID`,

							`ordersBelichtungsDatum`,
							SUM(`ordersAdditionalArtikelMenge`) AS `ordersProductMenge`,
							CONCAT(DATE_FORMAT(`ordersBelichtungsDatum`, '%Y'), '-', DATE_FORMAT(`ordersBelichtungsDatum`, '%u')) AS `ordersWeek`,
							`ordersStatus`

						FROM `" . TABLE_ORDERS . "`
						WHERE `ordersBelichtungsDatum` != '0000-00-00'
							/* AND `ordersUseNoStock` != '1' */
							AND (
								(`ordersStatus` = '3' AND `ordersBelichtungsDatum` > '" . DATE_STOCK_START . "')
								OR
								(`ordersStatus` = '7' AND `ordersProduktionsDatum` > '" . DATE_STOCK_START . "')
							)

						GROUP BY CONCAT(`ordersBestellJahr`, '-', `ordersBestellWoche`, ';', `ordersAdditionalArtikelKategorieID`)

					/* ################################## */

					UNION

					/* ################################## */

						SELECT
							DATE_FORMAT(`ordersAuslieferungsDatum`, '%Y') AS `ordersBestellJahr`,
							DATE_FORMAT(`ordersAuslieferungsDatum`, '%u') AS `ordersBestellWoche`,
							IF(`ordersArtikelKategorieID` IS NULL || `ordersArtikelKategorieID` = '', '000', `ordersArtikelKategorieID`) AS `ordersCategoryID`,

							`ordersAuslieferungsDatum`,
							SUM(`ordersArtikelMenge`) AS `ordersProductMenge`,
							CONCAT(DATE_FORMAT(`ordersAuslieferungsDatum`, '%Y'), '-', DATE_FORMAT(`ordersAuslieferungsDatum`, '%u')) AS `ordersWeek`,
							`ordersStatus`

						FROM `" . TABLE_ORDERS . "`
						WHERE `ordersAuslieferungsDatum` != '0000-00-00'
							/* AND `ordersUseNoStock` != '1' */
							AND (`ordersStatus` = '4' AND `ordersAuslieferungsDatum` > '" . DATE_STOCK_START . "')

						GROUP BY CONCAT(`ordersBestellJahr`, '-', `ordersBestellWoche`, ';', `ordersArtikelKategorieID`)

					UNION

					SELECT
							DATE_FORMAT(`ordersAuslieferungsDatum`, '%Y') AS `ordersBestellJahr`,
							DATE_FORMAT(`ordersAuslieferungsDatum`, '%u') AS `ordersBestellWoche`,
							IF(`ordersAdditionalArtikelKategorieID` IS NULL || `ordersAdditionalArtikelKategorieID` = '', '000', `ordersAdditionalArtikelKategorieID`) AS `ordersCategoryID`,

							`ordersAuslieferungsDatum`,
							SUM(`ordersAdditionalArtikelMenge`) AS `ordersProductMenge`,
							CONCAT(DATE_FORMAT(`ordersAuslieferungsDatum`, '%Y'), '-', DATE_FORMAT(`ordersAuslieferungsDatum`, '%u')) AS `ordersWeek`,
							`ordersStatus`

						FROM `" . TABLE_ORDERS . "`
						WHERE `ordersAuslieferungsDatum` != '0000-00-00'
							/* AND `ordersUseNoStock` != '1' */
							AND (`ordersStatus` = '4' AND `ordersAuslieferungsDatum` > '" . DATE_STOCK_START . "')

						GROUP BY CONCAT(`ordersBestellJahr`, '-', `ordersBestellWoche`, ';', `ordersAdditionalArtikelKategorieID`)

					ORDER BY `ordersBestellJahr` DESC, `ordersBestellWoche` DESC

			) AS `tempTable`
		LEFT JOIN `common_productcategories`
		ON (`tempTable`.`ordersCategoryID` = `common_productcategories`.`orderCategoriesID`)

	/* HAVING  ( */
		/* `ordersCategoryID` != '' */
		/* AND `ordersCategoryID` != '0' */
		/* AND `orderCategoriesUseInStock` = '1' */
	/* ) */
	";
#dd('sql');
	$rs = $dbConnection->db_query($sql);
	while($ds = mysqli_fetch_assoc($rs)) {

		if($ds["ordersStatus"] == 4) {
			foreach(array_keys($ds) as $field) {
				$arrConsumptionDatas[$ds["ordersCategoryID"]][$ds["ordersWeek"]][$field] = $ds[$field];
			}
		}

		if($ds["orderCategoriesRelationID"] == ''){
			$ds["orderCategoriesRelationID"] = $ds["ordersCategoryID"];
		}

		if($ds["ordersStatus"] == 4) {
			#$arrConsumptionQuantity[$ds["ordersCategoryID"]] += $ds["ordersProductMenge"];
			$arrConsumptionQuantity[$ds["orderCategoriesRelationID"]] += $ds["ordersProductMenge"];
		}
		else if($ds["ordersStatus"] == 2) {
			#$arrReleasedQuantity[$ds["ordersCategoryID"]] += $ds["ordersProductMenge"];
			#$arrNeededQuantity[$ds["ordersCategoryID"]] += $ds["ordersProductMenge"];
			$arrReleasedQuantity[$ds["orderCategoriesRelationID"]] += $ds["ordersProductMenge"];
			$arrNeededQuantity[$ds["orderCategoriesRelationID"]] += $ds["ordersProductMenge"];
		}
		else if($ds["ordersStatus"] == 3) {
			#$arrExposedQuantity[$ds["ordersCategoryID"]] += $ds["ordersProductMenge"];
			#$arrNeededQuantity[$ds["ordersCategoryID"]] += $ds["ordersProductMenge"];
			$arrExposedQuantity[$ds["orderCategoriesRelationID"]] += $ds["ordersProductMenge"];
			$arrNeededQuantity[$ds["orderCategoriesRelationID"]] += $ds["ordersProductMenge"];
		}
	}
#dd('arrConsumptionQuantity');
	// EOF GET CONSUMPTION

	// BOF GET STOCK
	$sql = "SELECT
				`stockItemsID`,
				`stockItemsDate`,
				`stockItemsQuantity`,
				`stockItemsCategoryID`,
				`stockItemsStoreDatetime`

				FROM `" . TABLE_STOCK_ITEMS . "`
				ORDER BY `stockItemsCategoryID`, `stockItemsDate` DESC
		";
	$rs = $dbConnection->db_query($sql);
	while($ds = mysqli_fetch_assoc($rs)) {
		foreach(array_keys($ds) as $field) {
			$arrStockDatas[$ds["stockItemsCategoryID"]][$ds["stockItemsID"]][$field] = $ds[$field];
		}
		$arrStockQuantity[$ds["stockItemsCategoryID"]] += $ds["stockItemsQuantity"];
	}
	// BOF GET STOCK


	// BOF GET SUPPLIER ORDERS
	$sql = "
			SELECT
					`" . TABLE_SUPPLIER_ORDERS . "`.`supplierOrdersID`,
					`" . TABLE_SUPPLIER_ORDERS . "`.`supplierOrdersSupplierID`,
					`" . TABLE_SUPPLIER_ORDERS . "`.`supplierOrdersOrderProcessID`,
					`" . TABLE_SUPPLIER_ORDERS . "`.`supplierOrdersAmount`,
					`" . TABLE_SUPPLIER_ORDERS . "`.`supplierOrdersProductNumber`,
					`" . TABLE_SUPPLIER_ORDERS . "`.`supplierOrdersProductName`,
					`" . TABLE_SUPPLIER_ORDERS . "`.`supplierOrdersProductID`,
					`" . TABLE_SUPPLIER_ORDERS . "`.`supplierOrdersProductKategorieID`,
					`" . TABLE_SUPPLIER_ORDERS . "`.`supplierOrdersProductSinglePrice`,
					`" . TABLE_SUPPLIER_ORDERS . "`.`supplierOrdersOrderDate`,
					`" . TABLE_SUPPLIER_ORDERS . "`.`supplierOrdersDeliveryDate`,
					`" . TABLE_SUPPLIER_ORDERS . "`.`supplierOrdersDeliveryDirectToCustomer`

				FROM `" . TABLE_SUPPLIER_ORDERS . "`

				WHERE 1
					AND `" . TABLE_SUPPLIER_ORDERS . "`.`supplierOrdersDeliveryDirectToCustomer` != '1'
		";
	$rs = $dbConnection->db_query($sql);
	while($ds = mysqli_fetch_assoc($rs)) {
		foreach(array_keys($ds) as $field) {
			$arrSupplierDatas[$ds["supplierOrdersID"]][$field] = $ds[$field];
		}
		#$arrStockQuantity[$ds["stockItemsCategoryID"]] += $ds["stockItemsQuantity"];
	}
	#dd('arrSupplierDatas');
	// EOF GET SUPPLIER ORDERS
?>
<?php
	require_once('inc/headerHTML.inc.php');
	$thisTitle = "Bestand - Wareneing&auml;nge und Warenausg&auml;nge";
	$headerHTML = preg_replace("/{###TITLE###}/", strip_tags($thisTitle), $headerHTML);
	echo $headerHTML;

	require_once(FILE_MENUE_TOP);
?>
<div id="mainArea">
	<div id="mainContent">
		<?php require_once(FILE_MENUE_SIDEBAR); ?>
<div id="contentArea">
	<div id="contentAreaElements">
		<h1><?php if(SHOW_PAGE_TITLE_ICONS) { echo '<img src="' . PATH_ICONS_MENUE_TITLES . 'statistics.png" alt="" />'; } ?> <?php echo $thisTitle; ?></h1>
		<!--
		<div id="displayStatusCountArea">
			<span class="displayStatusCount"><b><a href="displayOrders.php" title="Alle Vorg&auml;nge anzeigen">Insgesamt</a>:</b> <?php echo $countStatusTypesTotal; ?></span>
			<?php
				if(!empty($arrCountStatusTypes)) {
					foreach($arrCountStatusTypes as $thisKey => $thisValue) {
						echo ' &bull; <span class="displayStatusCount"><b><a href="?displayOrderStatus='.$thisValue["orderStatusTypesID"].'" title="'.$thisValue["orderStatusTypesName"].'e Vorg&auml;nge anzeigen">'.$thisValue["orderStatusTypesName"].'</a>:</b> '.$thisValue["ordersStatusCount"].'</span>';
					}
				}
			?>
		</div>
		-->
		<!--
		<div id="searchFilterArea">
			<form name="formFilterSearch" method="post" action="<?php echo $_SERVER["PHP_SELF"]; ?>">
			<table border="0" cellpadding="0" cellspacing="0" width="100%" class="searchFilterContent">
				<tr>
					<td>
						<label for="searchCustomerNumber">Kundennummer:</label>
						<input type="text" name="searchCustomerNumber" id="searchCustomerNumber" class="inputField_70" value="" />
					</td>
					<td>
						<label for="searchCustomerName">Kundenname:</label>
						<input type="text" name="searchCustomerName" id="searchCustomerName" class="inputField_160" value="" />
					</td>
					<td>
						<label for="searchPLZ">PLZ:</label>
						<input type="text" name="searchPLZ" id="searchPLZ" class="inputField_40" />
					</td>
					<td>
						<label for="searchWord">Suchbegriff:</label>
						<input type="text" name="searchWord" id="searchWord" class="inputField_70" />
					</td>
					<td>
						<input type="hidden" name="editID" id="editID" value="" />
						<input type="submit" name="submitSearch" class="inputButton0" value="Suchen" />
					</td>

					<td>
						<a href="editOrder.php?editID=NEW" class="linkButton">Neuer Vorgang</a>
					</td>
				</tr>
			</table>
			</form>
		</div>
		-->

		<div class="contentDisplay">
			<?php displayMessages(); ?>
			<?php if($arrGetUserRights["editStocks"] == '1') { ?>

			<div class="boxArea1">
				<h2>Neue Lieferungen eintragen</h2>
				<div>
					<div class="boxAreaContent">
						<form name="formEditStock" action="<?php echo $_SERVER["PHP_SELF"]; ?>" method="post">
							<table border="0" cellpadding="0" cellspacing="0">
								<tr>
									<td><label for="newStockCategory">Artikel:</label></td>
									<td>
										<select name="newStockCategory" id="newStockCategory" class="inputSelect_200">
											<option value="0"></option>
											<?php
												if(!empty($arrOrderCategoriesTypeDatas)) {
													foreach($arrOrderCategoriesTypeDatas as $thisKey => $thisValue){
														$thisDistance = "";
														$thisProductNumber = "";
														if(strlen($thisKey) > 3){
															$thisProductNumber = ' [Art-Nr: ' .$thisValue["orderCategoriesPartNumber"] . ']';
														}

														if(strlen($thisKey) > 3) { $thisDistance = " &bull; "; }
														echo '<option class="'.$thisValue["orderCategoriesClass"].'" value="'.$thisKey.'">' . $thisDistance . $thisValue["orderCategoriesName"] . $thisProductNumber . '</option>';
													}
												}
											?>
										</select>
									</td>
									<td style="border-right:1px dotted #333333;"></td>
									<td><label for="newStockDate">Anlieferungs-Datum:</label></td>
									<td><input type="text" name="newStockDate" id="newStockDate" class="inputField_70" readonly value="" /></td>
									<td style="border-right:1px dotted #333333;"></td>
									<td><label for="newStockQuantity">St&uuml;ckzahl:</label></td>
									<td><input type="text" name="newStockQuantity" id="newStockQuantity" class="inputField_100" value="" /></td>
									<td style="border-right:1px dotted #333333;"></td>
									<td style="text-align:right;"><input type="submit" name="storeNewStock" class="inputButton2" value="Speichern" /></td>
								</tr>
							</table>
						</form>
					</div>
				</div>
			</div>
			<hr />
			<?php
				}
				if(!empty($arrStockDatas)) {
					#dd('arrOrderCategoriesTypeDatas');
					#dd('arrStockDatas');
					echo '<h2>Bestands&uuml;bersichten</h2>';
					echo '<a href="' . PAGE_DISPLAY_STOCK_INVENTORY . '">ZUR NEUEN VERSION</a>';
					echo '<table border="0" class="displayOrders" cellpadding="0" cellspacing="0" width="100%">';
					echo '<tr>';
					echo '<th style="width:45px;text-align:right;">#</th>';
					echo '<th>Artikel</th>';
					echo '<th>Art.-Nr</th>';

					echo '<th>Lieferungen insg.</th>';
					echo '<th>Verbrauch insg.<br />(Versendet)</th>';
					echo '<th>Ben&ouml;tigt insg.<br />(Freigaben + Belichtet)</th>';
					echo '<th>Aktueller Bestand insg.</th>';
					echo '<th style="width:45px;">Details</th>';
					echo '<tr>';

					$count = 0;

					$thisMarker = '';
					foreach($arrStockDatas as $thisKey => $thisValue) {
						$thisParentKey = substr($thisKey, 0, 3);
						if($thisMarker != $thisParentKey){
							echo '<tr style="">';

							echo '<td class="tableRowTitle1" colspan="9">';
							echo $arrOrderCategoriesTypeDatas[$thisParentKey]["orderCategoriesName"];
							echo '</td>';

							echo '</tr>';
							$thisMarker = $thisParentKey;
						}

						if($count%2 == 0){ $rowClass = 'row0'; }
						else { $rowClass = 'row1'; }

						echo '<tr class="'.$rowClass.'">';

						echo '<td style="text-align:right;"><b>'.($count + 1).'.</b></td>';

						echo '<td style="white-space:nowrap;">';
						# echo $arrOrderCategoriesTypeDatas[$thisKey]["orderCategoriesName"];
						echo $arrOrderCategoriesTypeDatas[$thisKey]["orderCategoriesName"];
						echo '</td>';

						echo '<td>';
						echo $arrOrderCategoriesTypeDatas[$thisKey]["orderCategoriesPartNumber"];
						echo '</td>';

						echo '<td style="text-align:right;">'.$arrStockQuantity[$thisKey].'</td>';

						echo '<td style="text-align:right;">'.$arrConsumptionQuantity[$thisKey].'</td>';

						echo '<td style="text-align:right;">';
						echo $arrNeededQuantity[$thisKey];
						echo '</td>';

						echo '<td style="text-align:right; background-color:#FEFFAF;">';
						echo ($arrStockQuantity[$thisKey] - $arrConsumptionQuantity[$thisKey]);
						if(($arrStockQuantity[$thisKey] - $arrConsumptionQuantity[$thisKey]) < $arrNeededQuantity[$thisKey]) {
							echo ' <img src="layout/icons/iconAttention.png" width="16" height="16" alt="Achtung" title="Achtung, Bestand gering"> ';
						}
						else {
							echo ' <img src="layout/icons/iconOk.png" width="16" height="16" alt="ok" title="Bestand in Ordnung"> ';
						}
						echo '</td>';

						echo '<td style="text-align:center;">';
						echo '<img src="layout/icons/iconInfo.png" class="buttonShopOrderInfo" alt="" title="Detailinformationen">';
						echo '<div class="orderDetailsContent" style="display:none; visibility:hidden;">';

						echo '<p><b>Bestand insgesamt:</b> ' . ($arrStockQuantity[$thisKey] - $arrConsumptionQuantity[$thisKey]) .'</p>' ;
						echo '		<div class="boxArea2">
									<div class="boxAreaContent">
										<h3>Lieferungen '.$arrOrderCategoriesTypeDatas[$thisKey]["orderCategoriesName"].'</h3>
										<div>
						';
						echo '<p><b>Lieferungen insgesamt:</b> ' . $arrStockQuantity[$thisKey].'</p>' ;
						echo '
								<table border="0" class="border" cellpadding="0" cellspacing="0" width="100%"">
									<tr>
										<th style="width:45px;text-align:right;">#</th>
										<th>Eingangs-Datum</th>
										<th>St&uuml;ckzahl</th>
										<th>Aktion</th>
									</tr>
								';
								$countRow = 1;
								foreach($arrStockDatas[$thisKey] as $thisDataKey => $thisDataValue) {
									if($countRow%2 == 0){ $rowClass = 'row0'; }
									else { $rowClass = 'row1'; }
									echo '<tr class="' . $rowClass . '">';
									echo '<td style="text-align:right;"><span title="'.$thisDataKey.'"><b>' . $countRow . '. </b></span></td>';
									echo '<td style="text-align:center;">' . formatDate($arrStockDatas[$thisKey][$thisDataKey]["stockItemsDate"], 'display') . '</td>';
									echo '<td style="text-align:right; background-color:#FEFFAF;">' . $arrStockDatas[$thisKey][$thisDataKey]["stockItemsQuantity"] . '</td>';
									echo '<td style="text-align:center;"><a href="' . $_SERVER["PHP_SELF"] . '?deleteNewStock=1&deleteStockItemID=' . $thisDataKey . '" onclick="return showWarning(\'Soll dieser Eintrag wirklich entfernt werden?\');"><img src="layout/icons/iconDelete.png" width="16" height="16" alt="Entfernen" title="Diesen Eintrag entfernen" /></a></td>';
									echo '</tr>';
									$countRow++;
								}

						echo '</table>';

						echo '			</div>
									</div>
								</div>
								<div class="boxArea2">
									<div class="boxAreaContent">
										<h3>Versand '.$arrOrderCategoriesTypeDatas[$thisKey]["orderCategoriesName"].'</h3>
										<div>
						';
						echo '<p><b>Verbrauch insgesamt:</b> ' . $arrConsumptionQuantity[$thisKey].'</p>' ;

						echo '
								<table border="0" class="border" cellpadding="0" cellspacing="0" width="100%">
									<tr>
										<th style="width:45px;text-align:right;">#</th>
										<th>Liefer-KW</th>
										<th>St&uuml;ckzahl</th>
									</tr>
						';

						if(!empty($arrConsumptionDatas[$thisKey]))
						{
							$countRow = 1;
							foreach($arrConsumptionDatas[$thisKey] as $thisDataKey => $thisDataValue) {
								if($countRow%2 == 0){ $rowClass = 'row0'; }
									else { $rowClass = 'row1'; }
									echo '<tr class="' . $rowClass . '">';
									echo '<td style="text-align:right;"><b>' . $thisDataKey . '|' . $countRow . '.</b> </td>';
									echo '<td style="text-align:center;">'.$thisDataKey . formatDate($arrConsumptionDatas[$thisKey][$thisDataKey]["stockItemsDate"], 'display') . '</td>';
									echo '<td style="text-align:right; background-color:#FEFFAF;">' . $arrConsumptionDatas[$thisKey][$thisDataKey]["ordersProductMenge"] . '</td>';
									echo '</tr>';
									$countRow++;
							}
						}

						echo '</table>';

					echo '				</div>
									</div>
								</div>
								<div class="clear"></div>
							</div>

						';
						$count++;
					}
					echo '</table>';
				}
			?>
		</div>

	</div>
</div>
</div>
</div>
<script language="javascript" type="text/javascript">
	$(document).ready(function() {
		$(function() {
			$.datepicker.setDefaults($.datepicker.regional[""]);
			$('#newStockDate').datepicker($.datepicker.regional["de"]);
			$('#newStockDate').datepicker("option", "maxDate", "0" );
			colorRowMouseOver('.displayOrders tbody tr');
			$('.buttonShopOrderInfo').css('cursor', 'pointer');
			$('.buttonShopOrderInfo').click(function () {
				loadOrderDetails($(this), '<?php echo BASEPATH; ?>', 'Details des Verbrauchs');
			});
		});
	});
</script>

<?php require_once('inc/footerHTML.inc.php'); ?>