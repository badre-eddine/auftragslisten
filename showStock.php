<?php

	require_once('inc/requires.inc.php');

	if(!$arrGetUserRights["displayStocks"] && !$arrGetUserRights["editStocks"]) {
		header('location: ' . PAGE_EXIT_LOCATION);
		exit;
	}
	
	DEFINE('MAX_STOCK_PRICE_ADDITIONAL_ROWS', 2);

	$jswindowMessage = "";
	$warningMessage = '';
	$errorMessage = '';
	$successMessage = '';
	$infoMessage = '';
	
	
	if($_REQUEST["tab"] == ''){
		$_REQUEST["tab"] = "tabs-1";
	}

	$dbConnection = new DB_Connection();
	$db_open = $dbConnection->db_connect();

	// BOF READ VERTRETER
		$arrAgentDatas = getAgents();
		$arrAgentDatas2 = getAgents2();
	// EOF READ VERTRETER

	// BOF READ PAYMENT TYPES
		$arrPaymentTypeDatas = getPaymentTypes();
	// EOF READ PAYMENT TYPES

	// BOF READ CUSTOMER GROUP DATAS
		$arrCustomerGroupDatas = getCustomerGroups();
	// EOF READ CUSTOMER GROUP DATAS

	// BOF READ CUSTOMER TYPES
		$arrCustomerTypeDatas = getCustomerTypes();
	// EOF READ CUSTOMER TYPES	
	
	// BOF READ ORDER CATEGORIES		
		$arrAllProductDatas = getOrderProducts();		
	// EOF READ ORDER CATEGORIES
	
	// BOF READ PRINT COLORS
		$arrSupplierDatas = getSuppliers();
	// EOF READ PRINT COLORS
	
	// BOF GET CUSTOMER GROUPS
		$arrCustomerGroups = getCustomerGroups();		
		// BOF ONLY USE CUSTOMER GROUPS HAVING PRICELIST
		foreach($arrCustomerGroups as $thisCustomerGroupKey => $thisCustomerGroupValue){
			if($thisCustomerGroupValue["customerGroupsHasPricelist"] != '1'){
				unset($arrCustomerGroups[$thisCustomerGroupKey]);
			}
		}
	// EOF GET CUSTOMER GROUPS

	// BOF GET CUSTOMER TYPES
		$arrCustomerTypes = getCustomerTypes();
		// BOF ONLY USE CUSTOMER TYPES HAVING PRICELIST
		foreach($arrCustomerTypes as $thisCustomerTypeKey => $thisCustomerTypeValue){
			if($thisCustomerTypeValue["customerTypesHasPricelist"] != '1'){
				unset($arrCustomerTypes[$thisCustomerTypeKey]);
			}
		}
	// EOF GET CUSTOMER GROUPS
	

		
	

	
	// EOF GET SELECTED PRODUCT PRICES
	

	
	// EOF STORE PRODUCT

	// BOF CANCEL PRODUCTS_CAT
	
	// EOF CANCEL PRODUCTS_CAT

	// BOF DELETE PRODUCTS_CAT
	
	// EOF DELETE PRODUCTS_CAT
	
	// BOF GET ALL PRODUCTS OPTIONS AND ATTRIBUTES	
	
	// BOF GET ALL PRODUCTS OPTIONS AND ATTRIBUTES

	// BOF GET ALL PRODUCT CATEGORIES
		$sql_getCat = "
				SELECT
						`stockProductCategoriesID`,
						`stockProductCategoriesLevelID`,
						`stockProductCategoriesParentID`,
						`stockProductCategoriesRelationID`,						
						`stockProductCategoriesName`,
						`stockProductCategoriesName_TR`,
						`stockProductCategoriesText`,
						`stockProductCategoriesShortName`,
						`stockProductCategoriesProductNumber`,
						`stockProductCategoriesProductNumber_bctr`,
						`stockProductCategoriesProductNumber_b3`,
						`stockProductCategoriesSort`,
						`stockProductCategoriesActive`,
						`stockProductCategoriesProductIsForeign`,
						`stockProductCategoriesProductSupplier`,
						`stockProductCategoriesProductSupplierProductNumber`
						
					FROM `" . TABLE_STOCK_PRODUCT_CATEGORIES . "`
					WHERE 1
						AND `stockProductCategoriesParentID` = '000'

					ORDER BY
						`stockProductCategoriesSort`,
						`stockProductCategoriesName`
						
						/*,
						`stockProductCategoriesParentID`,
						`stockProductCategoriesLevelID`		
						*/						
						
			";
		$rs_getCat = $dbConnection->db_query($sql_getCat);

		$arrProductsCategoryData = array();

		while($ds_getCat = mysqli_fetch_assoc($rs_getCat)){
			foreach(array_keys($ds_getCat) as $field){
				$arrProductsCategoryData[$ds_getCat["stockProductCategoriesLevelID"]][$field] = $ds_getCat[$field];
			}
		}
	// EOF GET ALL PRODUCT CATEGORIES

	// BOF GET ALL PRODUCTS
	
		$sqlWhere = "";
		
		if($_REQUEST["searchProductCategory"] != ""){
			$sqlWhere = " AND `productCategories`.`stockProductCategoriesParentID` = '" . $_REQUEST["searchProductCategory"] . "' ";
		}
		else if($_REQUEST["searchProductNumber"] != ""){
			$sqlWhere = " 
					AND (
						`productCategories`.`stockProductCategoriesProductNumber` = '" . $_REQUEST["searchProductNumber"] . "' 
						OR `productCategories`.`stockProductCategoriesProductNumber_bctr` = '" . $_REQUEST["searchProductNumber"] . "' 
						OR `productCategories`.`stockProductCategoriesProductNumber_b3` = '" . $_REQUEST["searchProductNumber"] . "' 
					)
				";
		}
		else if($_REQUEST["searchProductName"] != ""){
			$sqlWhere = " 
					AND (
						`productCategories`.`stockProductCategoriesName` LIKE '%" . $_REQUEST["searchProductName"] . "%' 
						OR `productCategories`.`stockProductCategoriesText` LIKE '%" . $_REQUEST["searchProductName"] . "%' 
					)
				";
		}
		else if($_REQUEST["searchWord"] != ""){
			$sqlWhere = " 
					AND (
						`productCategories`.`stockProductCategoriesName` LIKE '%" . $_REQUEST["searchWord"] . "%' 
						OR `productCategories`.`stockProductCategoriesText` LIKE '%" . $_REQUEST["searchWord"] . "%' 
						OR `productCategories`.`stockProductCategoriesProductNumber` LIKE '%" . $_REQUEST["searchWord"] . "%' 
						OR `productCategories`.`stockProductCategoriesProductNumber_bctr` LIKE '%" . $_REQUEST["searchWord"] . "%' 
						OR `productCategories`.`stockProductCategoriesProductNumber_b3` LIKE '%" . $_REQUEST["searchWord"] . "%' 
					)
				";
		}
		
		if($_REQUEST["searchDeactivatedProducts"] != "1"){
			$sqlWhere .= " AND `productCategories`.`stockProductCategoriesActive` = '1' ";
		}
		
		
		
		$sql_getCat = "
				SELECT
						`productCategories`.`stockProductCategoriesID` AS `stockProductsID`,
						

						GROUP_CONCAT(		
							DISTINCT
							`productOptions`.`stockProductOptionsName`,
							'|',							
							`productAttributes`.`stockProductOptionsName`
							ORDER BY `productOptions`.`stockProductOptionsSort` ASC, `productAttributes`.`stockProductOptionsName` ASC
							SEPARATOR '###'
						) AS `stockProductData`						
						
						/*
						`" . TABLE_STOCK_PRODUCT_DETAIL_DATA . "`.`stockProductDetailDataProductID`,
						`" . TABLE_STOCK_PRODUCT_DETAIL_DATA . "`.`stockProductDetailDataOptionID`,
						`" . TABLE_STOCK_PRODUCT_DETAIL_DATA . "`.`stockProductDetailDataAttributeID`,	
						
						`productOptions`.`stockProductOptionsID`,
						`productOptions`.`stockProductOptionsName`,						
						`productOptions`.`stockProductOptionsParentID`,
						`productOptions`.`stockProductOptionsSort`,
						`productOptions`.`stockProductOptionsActive`,
						`productOptions`.`stockProductOptionsSelectable`,
						
						`productAttributes`.`stockProductOptionsID`,
						`productAttributes`.`stockProductOptionsName`,
						`productAttributes`.`stockProductOptionsParentID`,
						`productAttributes`.`stockProductOptionsSort`,
						`productAttributes`.`stockProductOptionsActive`,
						`productAttributes`.`stockProductOptionsSelectable`	
						
						
						
						FROM `" . TABLE_STOCK_PRODUCT_CATEGORIES . "` AS `parentCategories`
						*/						
						
					FROM `" . TABLE_STOCK_PRODUCT_CATEGORIES . "` AS `productCategories`
					
					LEFT JOIN `" . TABLE_STOCK_PRODUCT_CATEGORIES . "` AS `parentCategories`
					ON(`productCategories`.`stockProductCategoriesParentID` = `parentCategories`.`stockProductCategoriesLevelID`)				
	
					LEFT JOIN `" . TABLE_STOCK_PRODUCT_DETAIL_DATA . "`
					ON(`productCategories`.`stockProductCategoriesID` = `" . TABLE_STOCK_PRODUCT_DETAIL_DATA . "`.`stockProductDetailDataProductID`)
					
					LEFT JOIN `" . TABLE_STOCK_PRODUCT_OPTIONS . "` AS `productOptions`
					ON(`" . TABLE_STOCK_PRODUCT_DETAIL_DATA . "`.`stockProductDetailDataOptionID` = `productOptions`.`stockProductOptionsID`)
					
					LEFT JOIN `" . TABLE_STOCK_PRODUCT_OPTIONS . "` AS `productAttributes`
					ON(`" . TABLE_STOCK_PRODUCT_DETAIL_DATA . "`.`stockProductDetailDataAttributeID` = `productAttributes`.`stockProductOptionsID`)					
					
					WHERE 1
						AND `productCategories`.`stockProductCategoriesParentID` != '000'
						
						" . $sqlWhere . "

					GROUP BY
						`productCategories`.`stockProductCategoriesID`
						
					ORDER BY
						`parentCategories`.`stockProductCategoriesSort`,
						
						`productCategories`.`stockProductCategoriesSort` ASC,
						`productCategories`.`stockProductCategoriesName` ASC,
						`productCategories`.`stockProductCategoriesParentID` ASC
						/*,
						`productCategories`.`stockProductCategoriesLevelID`	
						*/					
			";
			
		
		$rs_getCat = $dbConnection->db_query($sql_getCat);

		$arrProductsData = array();

		while($ds_getCat = mysqli_fetch_assoc($rs_getCat)){
			foreach(array_keys($ds_getCat) as $field){
				$arrProductsData[$ds_getCat["stockProductsParentID"]][$ds_getCat["stockProductsID"]][$field] = $ds_getCat[$field];
			}
		}
	// EOF GET ALL PRODUCTS

	// BOF GET SELECTED PRODUCT
	
	// EOF GET SELECTED PRODUCT
	
	// BOF GET SELECTED PRODUCTS ATTRIBUTES
	
	// BOF lager ansehen kathy 
?>
<?php
	require_once('inc/headerHTML.inc.php');
	

	switch ($_GET["stockcountry"]) {
		case 'de':
			$thisTitle = "Lager ansehen DE";
			$queryWhere = "where stockCountry = 'de'";
			break;
		case 'tr':
			$thisTitle = "Lager ansehen TR";
			$queryWhere = "where stockCountry = 'tr'";
			break;
		
		default:
		$thisTitle = "Lager ansehen DE + TR";
		$queryWhere = "";
			break;
	}
 

	
	if($_REQUEST["editID"] != ""){
		if($_REQUEST["editID"] == "NEW"){
			$thisTitle .= ': <span class="headerSelectedEntry">Neues Produkt</span>';
		}
		else {
			$thisTitle .= ': <span class="headerSelectedEntry">' . htmlentities($arrSelectedProductsCategory["stockProductsName"]) . ' &bull; ' . htmlentities($arrSelectedProductsCategory["stockProductsProductNumber"]) . '</span>';
		}
	}

	$headerHTML = preg_replace("/{###TITLE###}/", strip_tags($thisTitle), $headerHTML);
	echo $headerHTML;

	require_once(FILE_MENUE_TOP);

?>

<?php require_once(FILE_MENUE_SIDEBAR); ?>
<div id="contentArea">
	<a name="top"></a>
	<div id="contentAreaElements">
		<h1><?php if(SHOW_PAGE_TITLE_ICONS) { echo '<img src="' . PATH_ICONS_MENUE_TITLES . 'shipping.png" alt="" />'; } ?> <?php echo $thisTitle; ?></h1>

		<?php displayMessages(); ?>
		<?php if($_REQUEST["editID"] == "" && $arrGetUserRights["editStocks"] == '1') { ?>
			<div class="menueActionsArea">
				<a href="addStock.php" class="linkButton">Neu Ware anlegen </a>
				<a href="showStock.php?stockcountry=all" class="linkButton">Lager DE + TR </a>
				<a href="showStock.php?stockcountry=de" class="linkButton">Lager DE </a>
				<a href="showStock.php?stockcountry=tr" class="linkButton">Lager TR </a>
				<div class="clear"></div>
			</div>
		<?php } ?>

	<!-- 	<div class="adminInfo">
			Datensatz-ID: <?php echo $_REQUEST["editID"]; ?>
			Level-ID: <?php echo $arrSelectedProductsCategory["stockProductsLevelID"]; ?>
		</div> -->		

		<div class="contentDisplay">
			<?php if($_REQUEST["editID"] == ''){ ?>
	<!-- 		<div id="searchFilterArea">
				<form name="formFilterSearch" method="post" action="<?php echo $_SERVER["PHP_SELF"]; ?>">
				<table border="0" cellpadding="0" cellspacing="0" class="searchFilterContent">
					<tr>
						<td>
							<label for="searchProductNumber">Artikelnummer:</label>
							<input type="text" name="searchProductNumber" id="searchProductNumber" class="inputField_70" />
						</td>
						<td>
							<label for="searchProductName">Artikelname:</label>
							<input type="text" name="searchProductName" id="searchProductName" class="inputField_120" />
						</td>
						<td>								
							<label for="searchProductCategory">Artikelkategorie:</label>
							<select name="searchProductCategory" id="searchProductCategory" class="inputField_120" >
								<option value=""></option>
								<?php
									if(!empty($arrProductsCategoryData)){
										foreach($arrProductsCategoryData as $thisKey => $thisValue){
											echo '<option value="' . $thisValue["stockProductCategoriesLevelID"] . '">' . htmlentities($thisValue["stockProductCategoriesName"]) . ' [' . $thisValue["stockProductCategoriesLevelID"] . ']</option>';
										}
									}
								?>
							</select>
						</td>
						<td>
							<label for="searchWord">Suchbegriff:</label>
							<input type="text" name="searchWord" id="searchWord" class="inputField_70" />
						</td>
						<td>
							<label for="searchDeactivatedProducts">Deaktivierte anzeigen?:</label>
							<input type="checkbox" name="searchDeactivatedProducts" id="searchDeactivatedProducts" value="1" />
						</td>
						<td>
							<input type="submit" name="submitformFilterSearch" id="submitformFilterSearch" class="inputButton0" value="Suchen" />
						</td>
					</tr>
				</table>
				</form>
			</div> -->
			<?php } ?>
		
			<div class="adminEditArea">
			<?php			
			
				if($_REQUEST["editID"] == ''){
					$countRowLevel1 = 0;
					$countRowLevel2 = 0;
# dd('arrProductsCategoryData');
					if(!empty($arrProductsData)){
						echo '<table cellpadding="0" cellspacing="0" border="0" width="100%" class="displayOrders">
							<colgroup>								
								<col />
								<col />
								<col />
								<col />								
								<col />
								<col />
								<col />
								<col />
							';
						if($_COOKIE["isAdmin"] == '1'){
							echo '
									<col />
									<col />
									<col />
									<col />
								';
						}
						echo '
								<col />
							</colgroup>
						
							<tr>								
								<th style="width:45px;text-align:right;">#</th>
								<th>Artikel</th>
								<th>stock entred</th>
								<th>Aufträge Aktuell</th>
								<th>Aufträge verschickt </th>
								<th>Lager aktuell</th>
								<th>Lager Verfugbar </th>  
							';
							
					/* 	if($_COOKIE["isAdmin"] == '1'){
							echo '		
								<th>ID</th>
								<th>LID</th>
								<th>PID</th>
								<th>RID</th>
							';
						} */
						echo '
							 
							</tr>
						';
						$stock_query = "SELECT id_article,qty,article, sum(qty) as toto_qty FROM `common_stock` ".$queryWhere." group  by article";
						$rs_getCat = $dbConnection->db_query($stock_query);

					
						//ordersStatus
						//ordersArtikelBezeichnung

						$my_i = 0 ;
						if($_GET["stockcountry"]=="tr"){
							$tableName = "common_ordersproductionstransfer";
							$menge = "ordersProductionsTransferOrdersProductQuantity";
							$productName ="ordersProductionsTransferOrdersProductName";
							$orderStatut = "ordersProductionsTransferProductionStatus"; 
						}
						else {						
							$tableName = "common_ordersprocess";
							$menge = "ordersArtikelMenge";
							$productName ="ordersArtikelBezeichnung";
							$orderStatut = "ordersStatus";
						}

						

						while($ds_getCat = mysqli_fetch_assoc($rs_getCat))
						{ 		
							$x = $y = $z = 0 ;
							/*
							ordersProductionsTransferOrdersProductQuantity als menge
							ordersProductionsTransferOrdersProductName als productname
							ordersProductionsTransferProductionStatus als orderstatus*/


							$cmd_pending_query = "SELECT sum(`$menge`) as total ,count(*) as toto_cmd, $productName , $orderStatut ,ordersBestellDatum
											FROM `$tableName` 
											where $productName = '".$ds_getCat['article']."' 
											and  $orderStatut = 1
											or   $orderStatut = 2
											or   $orderStatut = 3
									
											 
											and `ordersBestellDatum`   > ".STOCK_DATE."
											group by $orderStatut , $productName";
							 
						 
							$cmd_done_query = "SELECT sum(`$menge`) as total ,count(*) as toto_cmd, $productName , $orderStatut ,ordersBestellDatum
											FROM `$tableName` 
											where $productName = '".$ds_getCat['article']."'
											and  $orderStatut = 4 
											and `ordersBestellDatum`   > ".STOCK_DATE."
											group by $orderStatut , $productName";

											
								//die($cmd_pending_query);
							
							$rs_cmd_pending = $dbConnection->db_query($cmd_pending_query);
							
							$rs_cmd_done = $dbConnection->db_query($cmd_done_query);

							$my_i++;
							if($my_i%2 == 0){ $rowClass = 'row0'; }
							else { $rowClass = 'row1'; }


						/* 	while($ds_getCat = mysqli_fetch_assoc($rs_getCat))
							{  */ 
								$ds_cmd_pending = mysqli_fetch_row($rs_cmd_pending);
								$ds_cmd_done = mysqli_fetch_row($rs_cmd_done);

 
							
							// BOF CATEGORIES
								echo '<tr style="font-weight:bold;" class="'.$rowClass.'">';	

								echo '<td style="text-align:left;" title="ID: ' .  $ds_getCat['id_article']   . '">';
							 	echo $ds_getCat['--'];
								echo '</td>';

								echo '<td>';
								echo '&#9632; ' . $ds_getCat['article'] ; 
								echo '</td>';

								echo '<td>';
								echo  $ds_getCat['toto_qty'] ;
								echo '</td>';

								echo '<td>';
								if( !empty($ds_cmd_pending[0]) )	
									{	
								   
									   echo $ds_cmd_pending[0].'<small> ('.$ds_cmd_pending[1],')</small>'  ; 
									}else{
									   echo '0';
									}
								   echo '</td>';
								   

								   echo '<td>';
								if( !empty($ds_cmd_done[0]) )	
								{	
							   
								   echo $ds_cmd_done[0].'<small> ('.$ds_cmd_done[1],')</small>'  ; 
								}else{
								   echo '0';
								}
							  
								echo '</td>';


								echo '<td>';
									//echo  $ds_getCat['toto_qty'] - $ds_cmd_done[0] ;
									echo  $ds_getCat['toto_qty'] - $ds_cmd_pending[0] - $ds_cmd_done[0];
								echo '</td>';

						
								
								echo '<td>';
									//echo  $ds_getCat['toto_qty'] - $ds_cmd_done[0] ;
									echo  $ds_getCat['toto_qty'] - $ds_cmd_pending[0] - $ds_cmd_done[0];
								echo '</td>';

							  
						/* 		echo '<td>';
								echo '<span class="toolItem"><a href="showStock.php?editID=' . $thisProductsValue["stockProductsID"] . '"><img src="layout/icons/iconEdit.gif" width="16" height="16" title="Artikel bearbeiten" alt="Bearbeiten" /></a></span>';
								echo '</td>'; */
								
								echo '</tr>';

					 

						 
						}	 
						echo '</table>';
					}
		

				
					?>							
					
						
			<?php } ?>
		</div>
		</div>
		</div>
	<div class="clear"></div>
</div>

<script language="javascript" type="text/javascript">	
	<!--
	/* <![CDATA[ */
	$(document).ready(function() {
		$('.buttonToggleSidebarMenue').click(function() {
			$('#menueSidebarToggleContent').toggle();
		});

		$('.buttonSendDocument').click(function() {
			var mailDocumentDatas = $(this).attr('rel');
			var mailAdress = '';
			sendAttachedDocument($(this), mailDocumentDatas, '<?php echo $thisDocumentValue[$thisKey]["orderDocumentsCustomerNumber"]; ?>', '<?php echo $_REQUEST["editID"]; ?>', '', '<?php echo $_SERVER["PHP_SELF"]; ?>', mailAdress);
		});

		colorRowMouseOver('.displayOrders tbody tr');
		
		$('#editProductCategoriesRelationID').live('change', function() {
			if($(this).val() == ''){
				alert('Sie haben eine Kategorie gewählt. Es ist hier nur die Auswahl eines Produkts möglich!');
				$('#editProductCategoriesRelationID option:first').attr('selected', 'selected');
				
			}
		});
				
		$('.buttonShowProductDetails').css('cursor', 'pointer');
		$('.buttonShowProductDetails').click(function() {
			$('.productDetails').not($(this).next('.productDetails')).hide();
			$(this).next('.productDetails').toggle();
		});
		
	});
	/* ]]> */
	// -->
</script>
<?php require_once('inc/footerHTML.inc.php'); ?>