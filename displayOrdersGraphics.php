<?php
	require_once('inc/requires.inc.php');

	if(!$arrGetUserRights["displayOrderReceipts"]) {
		header('location: ' . PAGE_EXIT_LOCATION);
		exit;
	}

	$arrExecResults = array();

	/*
	echo PATH_SENDED_LAYOUT_FILES;
	if(file_exists(DIRECTORY_IMPORT_FILES . "listKorrekturabzuege.txt")){
		unlink(DIRECTORY_IMPORT_FILES . "listKorrekturabzuege.txt");
	}
	$command = "dir " . PATH_SENDED_LAYOUT_FILES . "*.pdf > " . DIRECTORY_IMPORT_FILES . "listKorrekturabzuege.txt";
	exec($command, $error, $output);

	$arrExecResults[] = 'command: ' . $command;

	if(!empty($error)) {
		$arrExecResults[] = array('1. GET FILE LIST: error', $error);
	}
	if(!empty($output)) {
		$arrExecResults[] = array('2. GET FILE LIST: output', $output);
	}

	if(!empty($arrExecResults)) {
		echo '<pre class="warningArea">';
		print_r($arrExecResults);
		echo '</pre>';
	}


	#echo phpinfo();

	if(is_dir(PATH_SENDED_LAYOUT_FILES)){
		echo 'ja' . '<br />';
	}
	else {
		echo 'nein' . '<br />';
	}

	echo $_SERVER["DOCUMENT_ROOT"]. '<br>';
	echo 'x' . copy('Z:/Vorlagen/Kunden_Mails/59391_01_stieglitzer_autohandel_gmbh_arnsberg_Merkur_negativ_echtansicht.pdf', 'test1.pdf') . '<br />';;
	echo 'y' . copy('http://192.168.2.200/Kunden_Mails/59391_01_stieglitzer_autohandel_gmbh_arnsberg_Merkur_negativ_echtansicht.pdf', 'test2.pdf') . '<br />';;
	*/

	$jswindowMessage = "";
	$warningMessage = '';
	$errorMessage = '';
	$successMessage = '';
	$infoMessage = '';

	$dbConnection = new DB_Connection();
	$db_open = $dbConnection->db_connect();

	$sql = "SET lc_time_names = 'de_DE'";
	$rs = $dbConnection->db_query($sql);

	$defaultDisplayType = 'WEEK';
	if($_REQUEST["searchInterval"] == "") {
		$displayType = $defaultDisplayType;
	}
	else {
		$displayType = $_REQUEST["searchInterval"];
	}

	if($displayType == 'WEEK') {
		#$dateField = " CONCAT(DATE_FORMAT(`" . TABLE_ORDERS . "`.`ordersBestellDatum`, '%Y'), '-', DATE_FORMAT(`" . TABLE_ORDERS . "`.`ordersBestellDatum`, '%v'), '_', 'KW') ";
		$dateField = " CONCAT(IF(DATE_FORMAT(`common_ordersProcess`.`ordersBestellDatum`, '%m') = '12' AND DATE_FORMAT(`common_ordersProcess`.`ordersBestellDatum`, '%v') = '01', (DATE_FORMAT(`common_ordersProcess`.`ordersBestellDatum`, '%Y') + 1),  DATE_FORMAT(`common_ordersProcess`.`ordersBestellDatum`, '%Y')), '-', DATE_FORMAT(`common_ordersProcess`.`ordersBestellDatum`, '%v'), '_', 'KW') ";
		#$dateField2 = " CONCAT(DATE_FORMAT(`" . TABLE_CREATED_LAYOUT_FILES . "`.`createdLayoutFilesDatetime`, '%Y'), '-', DATE_FORMAT(`" . TABLE_CREATED_LAYOUT_FILES . "`.`createdLayoutFilesDatetime`, '%v'), '_', 'KW') ";
		$dateField2 = " CONCAT(IF(DATE_FORMAT(`" . TABLE_CREATED_LAYOUT_FILES . "`.`createdLayoutFilesDatetime`, '%m') = '12' AND DATE_FORMAT(`" . TABLE_CREATED_LAYOUT_FILES . "`.`createdLayoutFilesDatetime`, '%v') = '01', (DATE_FORMAT(`" . TABLE_CREATED_LAYOUT_FILES . "`.`createdLayoutFilesDatetime`, '%Y') + 1),  DATE_FORMAT(`" . TABLE_CREATED_LAYOUT_FILES . "`.`createdLayoutFilesDatetime`, '%Y')), '-', DATE_FORMAT(`" . TABLE_CREATED_LAYOUT_FILES . "`.`createdLayoutFilesDatetime`, '%v'), '_', 'KW') ";
		$groupField = "";
	}
	else if($displayType == 'YEAR') {
		$dateField = " CONCAT(DATE_FORMAT(`" . TABLE_ORDERS . "`.`ordersBestellDatum`, '%Y')) ";
		$dateField2 = " CONCAT(DATE_FORMAT(`" . TABLE_CREATED_LAYOUT_FILES . "`.`createdLayoutFilesDatetime`, '%Y')) ";
		$groupField = "";
	}
	else if($displayType == 'QUARTER') {
		$dateField = " CONCAT(
					DATE_FORMAT(`" . TABLE_ORDERS . "`.`ordersBestellDatum`, '%Y'),
					'#',
					IF(DATE_FORMAT(`" . TABLE_ORDERS . "`.`ordersBestellDatum`, '%m') = '01' OR DATE_FORMAT(`" . TABLE_ORDERS . "`.`ordersBestellDatum`, '%m') = '02' OR DATE_FORMAT(`" . TABLE_ORDERS . "`.`ordersBestellDatum`, '%m') = '03', '1. Quartal',
						IF(DATE_FORMAT(`" . TABLE_ORDERS . "`.`ordersBestellDatum`, '%m') = '04' OR DATE_FORMAT(`" . TABLE_ORDERS . "`.`ordersBestellDatum`, '%m') = '05' OR DATE_FORMAT(`" . TABLE_ORDERS . "`.`ordersBestellDatum`, '%m') = '06', '2. Quartal',
							IF(DATE_FORMAT(`" . TABLE_ORDERS . "`.`ordersBestellDatum`, '%m') = '07' OR DATE_FORMAT(`" . TABLE_ORDERS . "`.`ordersBestellDatum`, '%m') = '08' OR DATE_FORMAT(`" . TABLE_ORDERS . "`.`ordersBestellDatum`, '%m') = '09', '3. Quartal',
								IF(DATE_FORMAT(`" . TABLE_ORDERS . "`.`ordersBestellDatum`, '%m') = '10' OR DATE_FORMAT(`" . TABLE_ORDERS . "`.`ordersBestellDatum`, '%m') = '11' OR DATE_FORMAT(`" . TABLE_ORDERS . "`.`ordersBestellDatum`, '%m') = '12', '4. Quartal',
									''
								)
							)
						)
					)
				)
			";
		$dateField2 = " CONCAT(
					DATE_FORMAT(`" . TABLE_CREATED_LAYOUT_FILES . "`.`createdLayoutFilesDatetime`, '%Y'),
					'#',
					IF(DATE_FORMAT(`" . TABLE_CREATED_LAYOUT_FILES . "`.`createdLayoutFilesDatetime`, '%m') = '01' OR DATE_FORMAT(`" . TABLE_CREATED_LAYOUT_FILES . "`.`createdLayoutFilesDatetime`, '%m') = '02' OR DATE_FORMAT(`" . TABLE_CREATED_LAYOUT_FILES . "`.`createdLayoutFilesDatetime`, '%m') = '03', '1. Quartal',
						IF(DATE_FORMAT(`" . TABLE_CREATED_LAYOUT_FILES . "`.`createdLayoutFilesDatetime`, '%m') = '04' OR DATE_FORMAT(`" . TABLE_CREATED_LAYOUT_FILES . "`.`createdLayoutFilesDatetime`, '%m') = '05' OR DATE_FORMAT(`" . TABLE_CREATED_LAYOUT_FILES . "`.`createdLayoutFilesDatetime`, '%m') = '06', '2. Quartal',
							IF(DATE_FORMAT(`" . TABLE_CREATED_LAYOUT_FILES . "`.`createdLayoutFilesDatetime`, '%m') = '07' OR DATE_FORMAT(`" . TABLE_CREATED_LAYOUT_FILES . "`.`createdLayoutFilesDatetime`, '%m') = '08' OR DATE_FORMAT(`" . TABLE_CREATED_LAYOUT_FILES . "`.`createdLayoutFilesDatetime`, '%m') = '09', '3. Quartal',
								IF(DATE_FORMAT(`" . TABLE_CREATED_LAYOUT_FILES . "`.`createdLayoutFilesDatetime`, '%m') = '10' OR DATE_FORMAT(`" . TABLE_CREATED_LAYOUT_FILES . "`.`createdLayoutFilesDatetime`, '%m') = '11' OR DATE_FORMAT(`" . TABLE_CREATED_LAYOUT_FILES . "`.`createdLayoutFilesDatetime`, '%m') = '12', '4. Quartal',
									''
								)
							)
						)
					)
				)
			";
			$groupField = "";
	}
	else if($displayType == 'MONTH') {
		// $dateField = " CONCAT(DATE_FORMAT(`" . TABLE_ORDERS . "`.`ordersBestellDatum`, '%Y'), '-', DATE_FORMAT(`" . TABLE_ORDERS . "`.`ordersBestellDatum`, '%M')) ";
		// $dateField2 = " CONCAT(DATE_FORMAT(`" . TABLE_CREATED_LAYOUT_FILES . "`.`createdLayoutFilesDatetime`, '%Y'), '-', DATE_FORMAT(`" . TABLE_CREATED_LAYOUT_FILES . "`.`createdLayoutFilesDatetime`, '%M')) ";

		$dateField = " CONCAT(DATE_FORMAT(`" . TABLE_ORDERS . "`.`ordersBestellDatum`, '%Y'), '-', DATE_FORMAT(`" . TABLE_ORDERS . "`.`ordersBestellDatum`, '%m')) ";
		$dateField2 = " CONCAT(DATE_FORMAT(`" . TABLE_CREATED_LAYOUT_FILES . "`.`createdLayoutFilesDatetime`, '%Y'), '-', DATE_FORMAT(`" . TABLE_CREATED_LAYOUT_FILES . "`.`createdLayoutFilesDatetime`, '%m')) ";
		$groupField = "";
	}
	else if($displayType == 'DAY') {
		$dateField = " `" . TABLE_ORDERS . "`.`ordersBestellDatum` ";
		$dateField2 = " `" . TABLE_CREATED_LAYOUT_FILES . "`.`createdLayoutFilesDatetime` ";
		$groupField = "";
	}

	// BOF GET RECEIPTS
		$arrReceiptQuantity = array();

		$sql = "
				SELECT
					" . $dateField . " AS `orderReceiptDate`,
					SUBSTRING(`ordersArtikelKategorieID`, 1, 3) AS `categoryType`,

					COUNT(`" . TABLE_ORDERS . "`.`ordersID`) AS `countOrders`,
					`" . TABLE_ORDERS . "`.`ordersStatus`,
					`" . TABLE_ORDERS . "`.`ordersOrderType`,
					GROUP_CONCAT(`ordersArtikelKategorieID`) AS `categorieIDs`

				FROM `" . TABLE_ORDERS . "`

				WHERE 1
					/* AND SUBSTRING(`ordersArtikelKategorieID`, 1, 3) = '001' */
					AND `" . TABLE_ORDERS . "`.`ordersOrderType` = 1

				GROUP BY CONCAT(" . $dateField . ", SUBSTRING(`ordersArtikelKategorieID`, 1, 3))

				ORDER BY `" . TABLE_ORDERS . "`.`ordersBestellDatum` DESC
		";

		$rs = $dbConnection->db_query($sql);
		while($ds = mysqli_fetch_assoc($rs)) {
			if($ds["categoryType"] == '001'){
				$arrReceiptQuantity[$ds["orderReceiptDate"]]["KZH"]["countOrders"] += $ds["countOrders"];
			}
			else {
				$arrReceiptQuantity[$ds["orderReceiptDate"]]["SON"]["countOrders"] += $ds["countOrders"];
			}
		}
	// EOF GET RECEIPTS

	// BOF SENDED FILES
		$sql = "
					SELECT
						" . $dateField2 . " AS `orderReceiptDate`,
						COUNT(`" . TABLE_CREATED_LAYOUT_FILES . "`.`createdLayoutFilesDatetime`) AS `countOrders`

					FROM `" . TABLE_CREATED_LAYOUT_FILES . "`

					WHERE 1

					GROUP BY " . $dateField2 . "

					ORDER BY `" . TABLE_CREATED_LAYOUT_FILES . "`.`createdLayoutFilesDatetime` DESC
			";

		$rs = $dbConnection->db_query($sql);

		while($ds = mysqli_fetch_assoc($rs)) {
			$arrReceiptQuantity[$ds["orderReceiptDate"]]["PDF"]["countOrders"] = $ds["countOrders"];
		}
	// BOF SENDED FILES

	if(!empty($arrReceiptQuantity)){
		krsort($arrReceiptQuantity);
	}
?>
<?php
	require_once('inc/headerHTML.inc.php');
	$thisTitle = "Korrektur-Arbeiten / Neubestellungen";
	if($displayType == 'YEAR') { $thisTitle .= ': Jahres-Ansicht'; }
	if($displayType == 'MONTH') { $thisTitle .= ': Monats-Ansicht'; }
	if($displayType == 'WEEK') { $thisTitle .= ': Wochen-Ansicht'; }
	if($displayType == 'DAY') { $thisTitle .= ': Tages-Ansicht'; }
	$headerHTML = preg_replace("/{###TITLE###}/", strip_tags($thisTitle), $headerHTML);
	echo $headerHTML;

	require_once(FILE_MENUE_TOP);

?>
<div id="mainArea">
	<div id="mainContent">
		<?php require_once(FILE_MENUE_SIDEBAR); ?>
<div id="contentArea">
	<div id="contentAreaElements">
		<h1><?php if(SHOW_PAGE_TITLE_ICONS) { echo '<img src="' . PATH_ICONS_MENUE_TITLES . 'statistics.png" alt="" />'; } ?> <?php echo $thisTitle; ?></h1>
		<p>Korrekturarbeiten, ausgehend von Neubestellungen.</p>
		<?php displayMessages(); ?>

		<div id="searchFilterArea">
			<form name="formSearchDatas" method="post" action="<?php echo $_SERVER["PHP_SELF"]; ?>">
				<table border="0" cellpadding="0" cellspacing="0" class="searchFilterContent">
					<tr>
						<td>
							<label for="searchInterval">Zeitraum:</label>
							<select name="searchInterval" id="searchInterval" class="inputField_130">
								<option value=""></option>
								<option value="DAY" <?php if($displayType == 'DAY'){ echo ' selected="selected" '; } ?> >pro Tag</option>
								<option value="WEEK" <?php if($displayType == 'WEEK'){ echo ' selected="selected" '; } ?> >pro Woche</option>
								<option value="MONTH" <?php if($displayType == 'MONTH'){ echo ' selected="selected" '; } ?> >pro Monat</option>
								<option value="QUARTER" <?php if($displayType == 'QUARTER'){ echo ' selected="selected" '; } ?> >pro Vierteljahr</option>
								<option value="YEAR" <?php if($displayType == 'YEAR'){ echo ' selected="selected" '; } ?> >pro Jahr</option>
							 </select>
						</td>
						<td>
							<input type="hidden" name="editID" id="editID" value="" />
							<input type="submit" name="submitSearch" class="inputButton0" value="Anzeigen" />
						</td>
					</tr>
				</table>
				<input type="hidden" name="submitSearchForm" id="submitSearchForm" value="1" />
			</form>
			<!--
			<a href="<?php echo $_SERVER["PHP_SELF"]; ?>?displayType=day" <?php if($displayType == "day"){ echo ' class="active" '; } ?> >Tages-Ansicht</a>
			<a href="<?php echo $_SERVER["PHP_SELF"]; ?>?displayType=week" <?php if($displayType == "week"){ echo ' class="active" '; } ?> >KW-Ansicht</a>
			<a href="<?php echo $_SERVER["PHP_SELF"]; ?>?displayType=month" <?php if($displayType == "month"){ echo ' class="active" '; } ?> >Monats-Ansicht</a>
			<a href="<?php echo $_SERVER["PHP_SELF"]; ?>?displayType=year" <?php if($displayType == "year"){ echo ' class="active" '; } ?> >Jahres-Ansicht</a>
			-->
		</div>

		<!-- BOF GRAPH ELEMENTS -->
		<script type="text/javascript" language="javascript" src="<?php echo PATH_RGRAPH; ?>libraries/RGraph.common.core.js" ></script>
		<script type="text/javascript" language="javascript" src="<?php echo PATH_RGRAPH; ?>libraries/RGraph.bar.js" ></script>
		<script type="text/javascript" language="javascript" src="<?php echo PATH_RGRAPH; ?>libraries/RGraph.line.js" ></script>

		<div id="tabs">
			<ul>
				<li><a href="#tabs-1">Neubestellungen KZH</a></li>
				<li><a href="#tabs-2">Neubestellungen Sonstige</a></li>
				<li><a href="#tabs-3">Gesendete Korrektur-Abz&uuml;ge</a></li>
			</ul>
			<div id="tabs-1">
				<div class="adminEditArea">
					<h2>Neubestellungen KZH</h2>
					<canvas id="cvs_dataKZH" width="900" height="250">[No canvas support]</canvas>
				</div>
			</div>
			<div id="tabs-2">
				<div class="adminEditArea">
					<h2>Neubestellungen Sonstige</h2>
					<canvas id="cvs_dataSON" width="900" height="250">[No canvas support]</canvas>
				</div>
			</div>
			<div id="tabs-3">
				<div class="adminEditArea">
					<h2>Gesendete Korrektur-Abz&uuml;ge</h2>
					<canvas id="cvs_dataPDF" width="900" height="250">[No canvas support]</canvas>
				</div>
			</div>
		</div>
		<!-- EOF GRAPH ELEMENTS -->

		<div class="contentDisplay">
			<?php
				if(!empty($arrReceiptQuantity)) {					
					$count = 0;
					$thisMarker = "";

					echo '<table border="0" cellpadding="0" cellspacing="0" width="100%" class="displayOrders">';
					echo '<tr>';
					echo '<th width="3%">#</th>';
					echo '<th width="17%">Zeitraum</th>';
					echo '<th width="40%">Neubestellungen KZH</th>';
					echo '<th width="20%">Neubestellungen Sonstige</th>';
					echo '<th width="20%">Neubestellungen Insgesamt</th>';
					echo '<th width="20%">Gesendete Korrektur-Abz&uuml;ge</th>';
					// echo '<th>Insgesamt</th>';
					echo '</tr>';
					foreach($arrReceiptQuantity as $thisKey => $thisValue) {
						if($displayType == 'WEEK') {
							$arrDateDisplay = explode("-", $thisKey);
							$thisDateDisplay = str_replace('_', '. ', $arrDateDisplay[1]);
							if($thisMarker != $arrDateDisplay[0]) {
								$thisMarker = $arrDateDisplay[0];
								echo '<tr><td colspan="6" class="tableRowTitle1">' . $thisMarker . '</td></tr>';
							}
						}
						else if($displayType == 'YEAR') {
							$thisDateDisplay = $thisKey;
						}
						else if($displayType == 'MONTH') {
							$arrDateDisplay = explode("-", $thisKey);
							// $thisDateDisplay = $arrDateDisplay[1] . ' ' . $arrDateDisplay[0];
							$thisDateDisplay = getTimeNames($arrDateDisplay[1], 'month', 'long') . ' ' . $arrDateDisplay[0];
							if($thisMarker != $arrDateDisplay[0]) {
								$thisMarker = $arrDateDisplay[0];
								echo '<tr><td colspan="6" class="tableRowTitle1">' . $thisMarker . '</td></tr>';
							}
						}
						else if($displayType == 'DAY') {
							$arrDateDisplay = explode("-", $thisKey);
							$thisDateDisplay = formatDate($thisKey, 'display');
							if($thisMarker != $arrDateDisplay[0]) {
								$thisMarker = $arrDateDisplay[0];
								echo '<tr><td colspan="6" class="tableRowTitle1">' . $thisMarker . '</td></tr>';
							}
						}

						if($count%2 == 0){ $rowClass = 'row0'; }
						else { $rowClass = 'row1'; }
						echo '<tr class="' . $rowClass . '">';
						echo '<td style="text-align:right;">' .  ($count+1) . '.</td>';
						echo '<td style="text-align:right;">';
						echo $thisDateDisplay;

						echo '</td>';
						echo '<td style="text-align:right;">' .  $thisValue["KZH"]["countOrders"] . '</td>';
						echo '<td style="text-align:right;">' .  $thisValue["SON"]["countOrders"] . '</td>';
						echo '<td style="text-align:right;">' .  ($thisValue["SON"]["countOrders"] + $thisValue["KZH"]["countOrders"]) . '</td>';
						echo '<td style="text-align:right;background-color:#FEFFAF;">' .  $thisValue["PDF"]["countOrders"] . '</td>';
						echo '</tr>';
						$count++;
						$arrGraphDatas["KZH"][$thisKey] = $thisValue["KZH"]["countOrders"];
						$arrGraphDatas["SON"][$thisKey] = $thisValue["SON"]["countOrders"];
						$arrGraphDatas["PDF"][$thisKey] = $thisValue["PDF"]["countOrders"];
					}
					echo '</table>';
				}
				#dd('arrGraphDatas');
			?>
		</div>
	</div>
</div>
</div>
</div>

<script language="javascript" type="text/javascript">
	$(document).ready(function() {
		colorRowMouseOver('.displayOrders tr');

		// BOF GET DATAS FOR GRAPH
			<?php
				if(!empty($arrGraphDatas)){
				$arrGraphDatas["KZH"] = array_reverse($arrGraphDatas["KZH"], true);
				$arrGraphDatas["SON"] = array_reverse($arrGraphDatas["SON"], true);
				$arrGraphDatas["PDF"] = array_reverse($arrGraphDatas["PDF"], true);
			?>
			var arrGraphDatas = new Array();
			arrGraphDatas["KZH"] = new Array();
			arrGraphDatas["KZH"]["data"]	= [<?php echo implode(",", array_values($arrGraphDatas["KZH"])); ?>];
			arrGraphDatas["KZH"]["labels"]	= ['<?php echo implode("','", array_keys($arrGraphDatas["KZH"])); ?>'];
			arrGraphDatas["KZH"]["labels"]	= '';

			arrGraphDatas["SON"] = new Array();
			arrGraphDatas["SON"]["data"]	= [<?php echo implode(",", array_values($arrGraphDatas["SON"])); ?>];
			arrGraphDatas["SON"]["labels"]	= ['<?php echo implode("','", array_keys($arrGraphDatas["SON"])); ?>'];
			arrGraphDatas["SON"]["labels"]	= '';

			arrGraphDatas["PDF"] = new Array();
			arrGraphDatas["PDF"]["data"]	= [<?php echo implode(",", array_values($arrGraphDatas["PDF"])); ?>];
			arrGraphDatas["PDF"]["labels"]	= ['<?php echo implode("','", array_keys($arrGraphDatas["PDF"])); ?>'];
			arrGraphDatas["PDF"]["labels"]	= '';

			// createGraph('cvs_orders', [<?php echo implode(",", array_values($arrGraphDatasOrders)); ?>], [<?php echo "'" . implode("','", array_keys($arrGraphDatasOrders)) . "'"; ?>], 'Line', [['green','red']]);
			createGraph('cvs_dataKZH', [<?php echo implode(",", array_values($arrGraphDatas["KZH"])); ?>], [<?php echo "'" . implode("','", $arrLabelsGraph) . "'"; ?>], 'Line', [['green','red']]);
			createGraph('cvs_dataSON', [<?php echo implode(",", array_values($arrGraphDatas["SON"])); ?>], [<?php echo "'" . implode("','", $arrLabelsGraph) . "'"; ?>], 'Line', [['green','red']]);
			createGraph('cvs_dataPDF', [<?php echo implode(",", array_values($arrGraphDatas["PDF"])); ?>], [<?php echo "'" . implode("','", $arrLabelsGraph) . "'"; ?>], 'Line', [['green','red']]);
			/*
			var lineALL = new RGraph.Line('cvs', arrGraphDatas["KZH"]["data"], arrGraphDatas["SON"]["data"], arrGraphDatas["PDF"]["data"]);
			lineALL.Set('chart.labels', arrGraphDatas["PDF"]["labels"]);
			lineALL.Set('labels.above', true);
			// lineALL.Set('colors', ['#FFD600']);
			lineALL.Set('shadow', true);
            lineALL.Set('shadow.offsetx', 2);
            lineALL.Set('shadow.offsety', 2);
            lineALL.Set('shadow.blur', 4);
			lineALL.Set('chart.linewidth', 2);
			lineALL.Set('chart.colors.alternate', false);
			lineALL.Set('chart.colors', ['#009900','#000099','#990000']);
			lineALL.Set('chart.background.grid', true);
			lineALL.Set('chart.gutter.left', 50);
			lineALL.Set('chart.hmargin', 5);
			lineALL.Set('chart.tickmarks', null);
            // lineALL.Set('chart.units.post', ' Stk');
			lineALL.Set('chart.units.post', '');
			lineALL.Set('chart.background.grid.hlines', false);
            lineALL.Set('chart.background.grid.autofit.numvlines', 11);
            lineALL.Set('chart.animation.unfold.initial', 0);
			lineALL.Draw();
			*/
			<?php
				}
			?>
		// EOF GET DATAS FOR GRAPH
		$(function() {
			$('#tabs').tabs();
		});
	});
</script>

<?php require_once('inc/footerHTML.inc.php'); ?>