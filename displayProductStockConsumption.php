<?php
	require_once('inc/requires.inc.php');

	DEFINE('DATE_STOCK_START', '2013-02-10');

	if(!$arrGetUserRights["editStocks"] && !$arrGetUserRights["displayStocks"]) {
		header('location: ' . PAGE_EXIT_LOCATION);
		exit;
	}

	$jswindowMessage = "";
	$warningMessage = '';
	$errorMessage = '';
	$successMessage = '';
	$infoMessage = '';

	$dbConnection = new DB_Connection();
	$db_open = $dbConnection->db_connect();


	// BOF GET MANDATORIES
		$arrMandatories = getMandatories();
	// EOF GET MANDATORIES

	// BOF GET PRODUCTS VPE
		$arrProductsVPE = getProductsVPE();
	// EOF GET PRODUCTS VPE

	// BOF GET PRODUCTS CATEGORIES
		#$arrProductsCategories = getOrderCategories();
	// EOF GET PRODUCTS CATEGORIES

	// BOF GET PRODUCT CATEGORIES
		$sql_getProductCategories = "
				SELECT
						`productDataID`,
						`productDataProductName`,
						`productDataProductShortName`,
						`productDataProductNumber`,
						`productDataProductEAN`,
						`productDataProductVpeID`,
						`productDataProductID`,
						`productDataProductParentID`,
						`productDataProductLevelID`,
						`productDataProductRelationID`,
						`productDataProductGroupID`,
						`productDataProductionPrice`,
			";
		if(!empty($arrMandatories)){
			foreach($arrMandatories as $thisMandatoryData){
				$sql_getProductCategories .= "
						`productDataProductName_" . strtolower($thisMandatoryData["mandatoriesShortName"]) . "`,
						`productDataPartNumber_" . strtolower($thisMandatoryData["mandatoriesShortName"]) . "`,
					";
			}
		}

		$sql_getProductCategories  .= "
						`productDataActive`

					FROM `" . TABLE_STOCK_PRODUCT_DATA . "`

					WHERE 1
						AND (
							`productDataProductRelationID` = ''
							OR `productDataProductID` = `productDataProductRelationID`
						)

					ORDER BY
						`productDataProductRelationID`
			";
		$rs_getProductCategories = $dbConnection->db_query($sql_getProductCategories);

		$arrProductCategories = array();
		while($ds_getProductCategories = mysqli_fetch_assoc($rs_getProductCategories)){
			foreach(array_keys($ds_getProductCategories) as $field){
				$arrProductCategories[$ds_getProductCategories["productDataProductID"]][$field] = $ds_getProductCategories[$field];
			}
		}
	// EOF GET PRODUCT CATEGORIES


	// BOF GET PRODUCTS DATA FROM `PRODUCTS_DATA`
		$where = "";

		if($_REQUEST["editID"] != ""){
			$where = " AND `productDataProductID` = '" . $_REQUEST["editID"] . "' ";
		}
		else if($_REQUEST["searchProductNummer"] != ""){
			$where = " AND (
							`productDataProductNumber` LIKE '%" . $_REQUEST["searchProductNummer"] . "%'
							OR `productDataPartNumber_bctr` LIKE '%" . $_REQUEST["searchProductNummer"] . "%'
							OR `productDataPartNumber_b3` LIKE '%" . $_REQUEST["searchProductNummer"] . "%'
						)
				";
		}
		else if($_REQUEST["searchProductName"] != ""){
			$where = " AND (
							`productDataProductShortName` LIKE '%" . $_REQUEST["searchProductName"] . "%'
							OR `productDataProductName` LIKE '%" . $_REQUEST["searchProductName"] . "%'
							OR `productDataProductName_bctr` LIKE '%" . $_REQUEST["searchProductName"] . "%'
							OR `productDataProductName_b3` LIKE '%" . $_REQUEST["searchProductName"] . "%'
						)
				";
		}
		else if($_REQUEST["searchProductCategory"] != ""){
			$where = " AND (
							`productDataProductParentID` = '" . $_REQUEST["searchProductCategory"] . "'
						)
				";
		}
		else if($_REQUEST["searchProductEAN"] != ""){
			$where = " AND (
							`productDataProductShortName` LIKE '%" . $_REQUEST["searchProductEAN"] . "%'
							OR `productDataProductName` LIKE '%" . $_REQUEST["searchProductEAN"] . "%'
							OR `productDataProductName_bctr` LIKE '%" . $_REQUEST["searchProductEAN"] . "%'
							OR `productDataProductName_b3` LIKE '%" . $_REQUEST["searchProductEAN"] . "%'
							OR `" . TABLE_GTIN_CODES . "`.`codesGtinNumber` LIKE '%" . $_REQUEST["searchProductEAN"] . "%'
						)
				";
		}
		else if($_REQUEST["searchWord"] != ""){
			$where = " AND (
							`productDataProductNumber` LIKE '%" . $_REQUEST["searchWord"] . "%'
							OR `productDataPartNumber_bctr` LIKE '%" . $_REQUEST["searchWord"] . "%'
							OR `productDataPartNumber_b3` LIKE '%" . $_REQUEST["searchWord"] . "%'
							OR `productDataProductShortName` LIKE '%" . $_REQUEST["searchWord"] . "%'
							OR `productDataProductName` LIKE '%" . $_REQUEST["searchWord"] . "%'
							OR `productDataProductName_bctr` LIKE '%" . $_REQUEST["searchWord"] . "%'
							OR `productDataProductName_b3` LIKE '%" . $_REQUEST["searchWord"] . "%'
						)
				";
		}

		if($_REQUEST["displayDeactivatedProducts"] != "1"){
			$where .= " AND `productDataActive` = '1' ";
		}
		else {
			$where .= "";
		}

		$sql_getProductsData = "
				SELECT

						`" . TABLE_STOCK_PRODUCT_DATA . "`.`productDataID`,
						`" . TABLE_STOCK_PRODUCT_DATA . "`.`productDataProductName`,
						`" . TABLE_STOCK_PRODUCT_DATA . "`.`productDataProductShortName`,
						`" . TABLE_STOCK_PRODUCT_DATA . "`.`productDataProductNumber`,
						`" . TABLE_STOCK_PRODUCT_DATA . "`.`productDataProductEAN`,
						`" . TABLE_STOCK_PRODUCT_DATA . "`.`productDataProductVpeID`,
						`" . TABLE_STOCK_PRODUCT_DATA . "`.`productDataProductID`,
						`" . TABLE_STOCK_PRODUCT_DATA . "`.`productDataProductParentID`,
						`" . TABLE_STOCK_PRODUCT_DATA . "`.`productDataProductLevelID`,
						`" . TABLE_STOCK_PRODUCT_DATA . "`.`productDataProductRelationID`,
						`" . TABLE_STOCK_PRODUCT_DATA . "`.`productDataProductGroupID`,
						`" . TABLE_STOCK_PRODUCT_DATA . "`.`productDataProductionPrice`,
						`" . TABLE_STOCK_PRODUCT_DATA . "`.`productDataPartNumber_bctr`,
						`" . TABLE_STOCK_PRODUCT_DATA . "`.`productDataProductName_bctr`,
						`" . TABLE_STOCK_PRODUCT_DATA . "`.`productDataPartNumber_b3`,
						`" . TABLE_STOCK_PRODUCT_DATA . "`.`productDataProductName_b3`,
						`" . TABLE_STOCK_PRODUCT_DATA . "`.`productDataActive`,

						`" . TABLE_ORDER_CATEGORIES . "`.`orderCategoriesID`,
						`" . TABLE_ORDER_CATEGORIES . "`.`orderCategoriesParentID`,
						`" . TABLE_ORDER_CATEGORIES . "`.`orderCategoriesLevelID`,
						`" . TABLE_ORDER_CATEGORIES . "`.`orderCategoriesRelationID`,
						`" . TABLE_ORDER_CATEGORIES . "`.`orderCategoriesGroupID`,
						`" . TABLE_ORDER_CATEGORIES . "`.`orderCategoriesShortName`,
						`" . TABLE_ORDER_CATEGORIES . "`.`orderCategoriesName`,
						`" . TABLE_ORDER_CATEGORIES . "`.`orderCategoriesPartNumber`,
						`" . TABLE_ORDER_CATEGORIES . "`.`orderCategoriesName_bctr`,
						`" . TABLE_ORDER_CATEGORIES . "`.`orderCategoriesPartNumber_bctr`,
						`" . TABLE_ORDER_CATEGORIES . "`.`orderCategoriesName_b3`,
						`" . TABLE_ORDER_CATEGORIES . "`.`orderCategoriesPartNumber_b3`,
						`" . TABLE_ORDER_CATEGORIES . "`.`orderCategoriesSort`,
						`" . TABLE_ORDER_CATEGORIES . "`.`orderCategoriesActive`,
						`" . TABLE_ORDER_CATEGORIES . "`.`orderCategoriesUseInStock`,
						`" . TABLE_ORDER_CATEGORIES . "`.`orderCategoriesSort2`,

						`" . TABLE_GTIN_CODES . "`.`codesGtinID`,
						`" . TABLE_GTIN_CODES . "`.`codesGtinType`,
						`" . TABLE_GTIN_CODES . "`.`codesGtinNumber`,
						`" . TABLE_GTIN_CODES . "`.`codesGtinProductName`,
						`" . TABLE_GTIN_CODES . "`.`codesGtinProductNumber`

					FROM `" . TABLE_STOCK_PRODUCT_DATA . "`

					LEFT JOIN `" . TABLE_ORDER_CATEGORIES . "`
					ON(`" . TABLE_STOCK_PRODUCT_DATA . "`.`productDataProductID` = `" . TABLE_ORDER_CATEGORIES . "`.`orderCategoriesID`)

					LEFT JOIN `" . TABLE_GTIN_CODES . "`
					ON(`" . TABLE_STOCK_PRODUCT_DATA . "`.`productDataPartNumber_bctr` = `" . TABLE_GTIN_CODES . "`.`codesGtinProductNumber`)

					WHERE 1
						AND `" . TABLE_STOCK_PRODUCT_DATA . "`.`productDataProductRelationID` != ''
						AND `" . TABLE_STOCK_PRODUCT_DATA . "`.`productDataProductID` = `" . TABLE_STOCK_PRODUCT_DATA . "`.`productDataProductGroupID`
						" . $where . "

					ORDER BY
						`" . TABLE_STOCK_PRODUCT_DATA . "`.`productDataProductRelationID`
			";
		$rs_getProductsData = $dbConnection->db_query($sql_getProductsData);
		$arrProductsData = array();
		while($ds_getProductsData = mysqli_fetch_assoc($rs_getProductsData)){
			foreach(array_keys($ds_getProductsData) as $field){
				$arrProductsData[$ds_getProductsData["orderCategoriesID"]][$field] = $ds_getProductsData[$field];
			}
		}
	// EOF GET PRODUCTS DATA FROM `PRODUCTS_DATA`

?>
<?php
	require_once('inc/headerHTML.inc.php');
	$thisTitle = "Waren-Bestandslisten";
	$headerHTML = preg_replace("/{###TITLE###}/", strip_tags($thisTitle), $headerHTML);
	echo $headerHTML;

	require_once(FILE_MENUE_TOP);
?>
<div id="mainArea">
	<div id="mainContent">
		<div id="menueSidebarToggleArea">
			<img src="layout/icons/iconToggle.png" class="buttonToggleSidebarMenue" width="10" height="10" alt="" title="Seitennavigation ein-/ausblenden" />
			<div id="menueSidebarToggleContent">
			<?php require_once(FILE_MENUE_SIDEBAR); ?>
			<div class="clear"></div>
			</div>
		</div>
		<div id="contentArea2">
			<div id="contentAreaElements">
				<h1><?php if(SHOW_PAGE_TITLE_ICONS) { echo '<img src="' . PATH_ICONS_MENUE_TITLES . 'editStock.png" alt="" />'; } ?> <?php echo $thisTitle; ?></h1>

				<div class="contentDisplay">
					<div id="searchFilterArea">
						<form name="formFilterSearch" method="post" action="<?php echo $_SERVER["PHP_SELF"]; ?>">
						<table border="0" cellpadding="0" cellspacing="0" class="searchFilterContent">
							<tr>
								<td>
									<label for="searchProductNummer">Artikelnummer:</label>
									<input type="text" name="searchProductNummer" id="searchProductNummer" class="inputField_70" />
								</td>
								<td>
									<label for="searchProductName">Artikelname:</label>
									<input type="text" name="searchProductName" id="searchProductName" class="inputField_120" />
									<input type="hidden" name="editOrdersArtikelID" id="editOrdersArtikelID" value="" />
								</td>
								<td>
									<label for="searchProductEAN">EAN-Nummer:</label>
									<input type="text" name="searchProductEAN" id="searchProductEAN" class="inputField_120" />
								</td>
								<td>
									<label for="searchProductCategory">Artikelkategorie:</label>
									<select name="searchProductCategory" id="searchProductCategory" class="inputSelect_120" >
										<option value=""></option>
										<?php
											if(!empty($arrProductCategories)) {
												foreach($arrProductCategories as $thisProductCategoriesKey => $thisProductCategoriesValue){
													$selected = '';
													if($thisProductCategoriesKey == $_REQUEST["searchProductCategory"]){
														$selected = ' selected="selected" ';
													}
													echo '<option value="' . $thisProductCategoriesKey . '" ' . $selected . '>' . htmlentities(utf8_decode($thisProductCategoriesValue["productDataProductName"])) . '</option>';
												}
											}
										?>
									</select>
								</td>
								<td>
									<label for="displayDeactivatedProducts">deaktivierte Anzeigen:</label>
									<?php
										$checked = '';
										if($_REQUEST["displayDeactivatedProducts"] == '1'){
											$checked = ' checked="checked" ';
										}

									?>
									<input type="checkbox" name="displayDeactivatedProducts" id="displayDeactivatedProducts" value="1" <?php echo $checked; ?> />
								</td>
								<td>
									<label for="searchWord">Suchbegriff:</label>
									<input type="text" name="searchWord" id="searchWord" class="inputField_120" />
								</td>
								<td>
									<input type="submit" name="submitFormFilterSearch" id="submitFormFilterSearch" class="inputButton0" value="Suchen" />
								</td>
								<td class="separator">xxx</td>
								<td><a href="<?php echo PAGE_PRODUCTS_STOCK_EDIT; ?>?editID=NEW">Neues Produkt einf&uuml;gen</a></td>
							</tr>
						</table>
						</form>
					</div>

					<?php
						if(!empty($arrProductsData)){

							echo '<table border="0" cellpadding="0" cellspacing="0" width="100%" class="displayOrders">';
							echo '<thead>';
							echo '<tr>';

							echo '<th>#</th>';

							if($_COOKIE["isAdmin"] == '1xx'){
								echo '<th>CID</th>';
								echo '<th>PID</th>';
								echo '<th>LID</th>';
								echo '<th>RID</th>';
								echo '<th>GID</th>';
							}

							#echo '<th>Kurzname</th>';
							#echo '<th>Name</th>';

							echo '<th>Art-Nr.</th>';
							echo '<th>Art-Name</th>';
							echo '<th>EAN-Nr.</th>';

							echo '<th>Lager?</th>';
							#echo '<th>Aktiv?</th>';
							echo '<th>Aktion</th>';

							echo '</tr>';

							echo '</thead>';

							echo '<tbody>';

							$countRow = 0;
							$marker = "";
							foreach($arrProductsData as $thisProductsDataKey => $thisProductsDataValue){
								if($marker != $thisProductsDataValue["productDataProductRelationID"]){
									echo '<tr>';
									echo '<td colspan="12" class="tableRowTitle1">';
									#echo $thisProductsDataValue["orderCategoriesName"];
									echo $arrProductCategories[$thisProductsDataValue["productDataProductRelationID"]]["productDataProductName"];
									echo '</td>';
									echo '</tr>';
									$marker = $thisProductsDataValue["productDataProductRelationID"];
								}

								if($countRow%2 == 0){ $rowClass = 'row0'; }
								else { $rowClass = 'row1'; }

								if($thisProductsDataValue["productDataActive"] != '1'){
									$rowClass = 'row11';
								}

								echo '<tr class="' . $rowClass . '">';

								echo '<td style="text-align:right;" title="' . $thisProductsDataValue["productDataID"] . '">' .  ($countRow+1) . '.</td>';

								if($_COOKIE["isAdmin"] == '1xx'){
									echo '<td style="white-space:nowrap;">';
									echo $thisProductsDataValue["productDataProductID"];
									echo '</td>';

									echo '<td style="white-space:nowrap;">';
									echo $thisProductsDataValue["productDataProductParentID"];
									echo '</td>';

									echo '<td style="white-space:nowrap;">';
									echo $thisProductsDataValue["productDataProductLevelID"];
									echo '</td>';

									echo '<td style="white-space:nowrap;">';
									echo $thisProductsDataValue["productDataProductRelationID"];
									echo '</td>';

									echo '<td style="white-space:nowrap;">';
									echo $thisProductsDataValue["productDataProductGroupID"];
									echo '</td>';
								}

								#echo '<td style="white-space:nowrap;">';
								#echo $thisProductsDataValue["productDataProductShortName"];
								#echo '</td>';

								#echo '<td style="white-space:nowrap;">';
								#echo $thisProductsDataValue["productDataProductName"];
								#echo '</td>';

								echo '<td style="white-space:nowrap;">';
								echo '<table border="0" cellpadding="0" cellspacing="0" class="noBorder" style="margin:0;padding:0">';
								echo '<tr style="border-bottom:1px dotted #333;">';
								echo '<td>' . '<b>BCTR</b>: </td>';
								echo '<td style="white-space:nowrap;">' . $thisProductsDataValue["productDataPartNumber_bctr"] . '</td>';
								echo '</tr>';
								echo '<tr>';
								echo '<td>' . '<b>B3</b>: </td>';
								echo '<td style="white-space:nowrap;">' . $thisProductsDataValue["productDataPartNumber_b3"] . '</td>';
								echo '</tr>';
								echo '</table>';
								echo '</td>';

								echo '<td style="white-space:nowrap;">';
								echo '<table border="0" cellpadding="0" cellspacing="0" class="noBorder" style="margin:0;padding:0">';
								echo '<tr style="border-bottom:1px dotted #333;">';
								echo '<td>' . '<b>BCTR</b>: </td>';
								echo '<td style="white-space:nowrap;">' . $thisProductsDataValue["productDataProductName_bctr"] . '</td>';
								echo '</tr>';
								echo '<tr>';
								echo '<td>' . '<b>B3</b>: </td>';
								echo '<td style="white-space:nowrap;">' . $thisProductsDataValue["productDataProductName_b3"] . '</td>';
								echo '</tr>';
								echo '</table>';
								echo '</td>';

								echo '<td>';
								echo $thisProductsDataValue["codesGtinNumber"];
								echo '</td>';

								echo '<td>';
								#echo $thisProductsDataValue["orderCategoriesUseInStock"];
								echo 'xxx';
								echo '</td>';

								#echo '<td>';
								#echo $thisProductsDataValue["orderCategoriesActive"];
								#echo '</td>';

								echo '<td>';
								if($arrGetUserRights["editStocks"]) {
									echo '<span class="toolItem"><a href="' . PAGE_PRODUCTS_STOCK_EDIT . '?editID='.$thisProductsDataValue["productDataID"] . '&amp;productID='.$thisProductsDataValue["productDataProductID"] . '"><img src="layout/icons/iconEdit.gif" width="16" height="16" title="Produktion bearbeiten (Datensatz '.$ds["ordersID"].')" alt="Bearbeiten" /></a></span>';
								}
								else {
									echo '<span class="toolItem"><img src="layout/spacer.gif" width="16" height="16" alt="" /></span>';
								}
								echo '</td>';

								echo '</tr>';

								$countRow++;
							}

							echo '</tbody>';

							echo '</table>';
						}
						else {
							echo '<p class="infoArea">Es wurden keine Daten gefunden!</p>';
						}
					?>

				</div>
			</div>
		</div>
	</div>
</div>
<script language="javascript" type="text/javascript">
	$(document).ready(function() {

		/*
		$('#searchWord').keyup(function () {
			loadSuggestions('searchProduct', [{'triggerElement': '#searchWord', 'fieldText': '#searchWord'}], 1);
		});
		$('#editOrdersArtikelBezeichnung').keyup(function () {
			loadSuggestions('searchProductName', [{'triggerElement': '#editOrdersArtikelBezeichnung', 'fieldNumber': '#editOrdersArtikelNummer', 'fieldName': '#editOrdersArtikelBezeichnung', 'fieldID': '#editOrdersArtikelID'}], 0);
		});
		$('#editOrdersArtikelNummer').keyup(function () {
			loadSuggestions('searchProductNumber', [{'triggerElement': '#editOrdersArtikelNummer', 'fieldNumber': '#editOrdersArtikelNummer', 'fieldName': '#editOrdersArtikelBezeichnung', 'fieldID': '#editOrdersArtikelID'}], 1);
		});
		*/
		colorRowMouseOver('.displayProducts tbody tr');
		$('.buttonNotice').click(function () {
			loadNotice($(this));
		});
		$('.buttonLoadProductDetails').click(function(){
			loadProductDetails($(this).attr('alt'));
		});
		$(".displayProducts .displayProductImage img").css('display', 'none').show().lazyload({
			container: $(".displayProductImage"),
			effect : "fadeIn"
		});

		$(function() {
			$.datepicker.setDefaults($.datepicker.regional[""]);
			$('#newStockDate').datepicker($.datepicker.regional["de"]);
			$('#newStockDate').datepicker("option", "maxDate", "0" );
			colorRowMouseOver('.displayOrders tbody tr');
			$('.buttonShopOrderInfo').css('cursor', 'pointer');
			$('.buttonShopOrderInfo').click(function () {
				loadOrderDetails($(this), '<?php echo BASEPATH; ?>', 'Details des Verbrauchs');
			});
		});
	});
</script>

<?php require_once('inc/footerHTML.inc.php'); ?>