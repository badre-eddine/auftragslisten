<?php
	require_once('inc/requires.inc.php');

	if(!$arrGetUserRights["editAddresses"] && !$arrGetUserRights["displayAddresses"]) {
		header('location: ' . PAGE_EXIT_LOCATION);
		exit;
	}

	$thisAddressType = 1;

	// BOF DEFINE DUTY FIELDS
		$arrFormDutyFields = array(
			// "editSalesmenKundennummer" => "Kundennummer",
			// "editSalesmenFirmenname" => "Firmenname",
			// "editSalesmenTyp" => "Kunden-Typ",
			// "editSalesmenGruppe[]" => "Kunden-Gruppe"
			// "editSalesmenGruppe" => "Kunden-Gruppe"
		);
		// CREATE JSON DUTY FIELD
		$jsonFormDutyFields = createJson($arrFormDutyFields);
	// BOF DEFINE DUTY FIELDS

	if($_REQUEST["editID"] == "")  {
		// $_REQUEST["editID"] = "NEW";
		// header("location: displayOrders.php");
		// exit;
	}

	$jswindowMessage = '';
	$warningMessage = '';
	$errorMessage = '';
	$successMessage = '';
	$infoMessage = '';

	$dbConnection = new DB_Connection();
	$db_open = $dbConnection->db_connect();

	// BOF READ ALL DATAS
		$where = "";
		if($_REQUEST["searchPLZ"] != "") {
			$where = " AND (
						`addressesZipcode` LIKE '" . $_REQUEST["searchPLZ"] . "%'
				)
			";
		}

		else if($_REQUEST["searchWord"] != "") {
			$where = " AND (
							`addressesZipcode` LIKE '" . $_REQUEST["searchWord"] . "%'
							OR
							`addressesName` LIKE '%" . $_REQUEST["searchWord"] . "%'
							OR
							`addressesNameTitle` LIKE '%" . $_REQUEST["searchWord"] . "%'
							OR
							`addressesCity` LIKE '%" . $_REQUEST["searchWord"] . "%'
						) ";
		}

		$sql = "SELECT
					`" . TABLE_ADDRESS_DATAS . "`.`addressesID`,
					`" . TABLE_ADDRESS_DATAS . "`.`addressesName`,
					`" . TABLE_ADDRESS_DATAS . "`.`addressesNameTitle`,
					`" . TABLE_ADDRESS_DATAS . "`.`addressesAddress`,
					`" . TABLE_ADDRESS_DATAS . "`.`addressesPostbox`,
					`" . TABLE_ADDRESS_DATAS . "`.`addressesZipcode`,
					`" . TABLE_ADDRESS_DATAS . "`.`addressesCity`,
					`" . TABLE_ADDRESS_DATAS . "`.`addressesArea`,
					`" . TABLE_ADDRESS_DATAS . "`.`addressesPhone`,
					`" . TABLE_ADDRESS_DATAS . "`.`addressesFax`,
					`" . TABLE_ADDRESS_DATAS . "`.`addressesMail`,
					`" . TABLE_ADDRESS_DATAS . "`.`addressesInternet`,
					`" . TABLE_ADDRESS_DATAS . "`.`addressesAddressType`

					FROM `" . TABLE_ADDRESS_DATAS . "`

					WHERE 1
						AND `" . TABLE_ADDRESS_DATAS . "`.`addressesAddressType` = " . $thisAddressType . "
				";
		$sql .= $where;

		$sql .= " ORDER BY `" . TABLE_ADDRESS_DATAS . "`.`addressesZipcode` ASC, `" . TABLE_ADDRESS_DATAS . "`.`addressesName` ASC ";
#dd('sql');
		$rs = $dbConnection->db_query($sql);
		$thisNumRows = $dbConnection->db_getMysqlNumRows($rs);

		if($thisNumRows > 0) {

			$pagesCount = ceil($dbConnection->db_getMysqlNumRows($rs) / MAX_SALESMEN_PER_PAGE);

			if($pagesCount > 1) {
				if($_REQUEST["page"] == "" || $_REQUEST["page"] == 0) {
					$_REQUEST["page"] = 1;
				}
			}
			else {
				$_REQUEST["page"] = 1;
			}

			if(MAX_SALESMEN_PER_PAGE > 0) {
				$sql .= " LIMIT " . (($_REQUEST["page"] - 1) * MAX_SALESMEN_PER_PAGE) . ", " . MAX_SALESMEN_PER_PAGE." ";
			}

			$rs = $dbConnection->db_query($sql);

			$arrAddressDatas = array();
			while($ds = mysqli_fetch_assoc($rs)) {
				foreach(array_keys($ds) as $field) {
					$arrAddressDatas[$ds["addressesID"]][$field] = $ds[$field];
				}
			}
		}
		else if($thisNumRows < 1) {
			$warningMessage .= ' ES wurden keine Daten gefunden!' . '<br />';
		}

	// EOF READ ALL DATAS

?>
<?php
	require_once('inc/headerHTML.inc.php');

	$thisTitle = 'Adressdaten anzeigen';
	if($thisAddressType == '1') {
		$thisTitle .= ': <span class="headerSelectedEntry">KFZ-Zulassungsstellen</span>';
	}

	$headerHTML = preg_replace("/{###TITLE###}/", strip_tags($thisTitle), $headerHTML);
	echo $headerHTML;

	require_once(FILE_MENUE_TOP);
?>

<div id="mainArea">
	<div id="mainContent">
		<?php require_once(FILE_MENUE_SIDEBAR); ?>
		<div id="contentArea">

			<div id="contentAreaElements">
				<h1><?php if(SHOW_PAGE_TITLE_ICONS) { echo '<img src="' . PATH_ICONS_MENUE_TITLES . 'addresses.png' . '" alt="" />'; } ?> <?php echo $thisTitle; ?></h1>

				<div class="adminInfo">Datensatz-ID: <?php echo $addressDatas["salesmenID"]; ?></div>
				<div class="contentDisplay">
				<?php
					if($_REQUEST["editID"] == "") {
				?>
				<div id="searchFilterArea">
					<form name="formFilterSearch" method="get" action="<?php echo $_SERVER["PHP_SELF"]; ?>">
						<table border="0" cellpadding="0" cellspacing="0" width="" class="searchFilterContent">
							<tr>
								<td>
									<img src="layout/menueIcons/menueQuicklinks/googleMaps.png" class="buttonLoadMap" alt="Gesamtkarte" title="Gesamtkarte anzeigen" />
								</td>
								<!--
								<td>
									<label for="searchSalesmenNumber">Kundennummer:</label>
									<input type="text" name="searchSalesmenNumber" id="searchSalesmenNumber" class="inputField_40" value="" />
								</td>
								<td>
									<label for="searchSalesmenName">Kundenname:</label>
									<input type="text" name="searchSalesmenName" id="searchSalesmenName" class="inputField_160" value="" />
								</td>
								-->
								<td>
									<label for="searchPLZ">PLZ:</label>
									<input type="text" name="searchPLZ" id="searchPLZ" class="inputField_40" />
								</td>
								<td>
									<label for="searchWord">Suchbegriff:</label>
									<input type="text" name="searchWord" id="searchWord" class="inputField_100" />
								</td>
								<td>
									<input type="hidden" name="xxxeditID" id="xxxeditID" value="" />
									<input type="submit" name="submitSearch" class="inputButton0" value="Suchen" />
								</td>
							</tr>
						</table>
					</form>
				</div>

				<?php
					}
				?>
				<?php displayMessages(); ?>

					<?php
						if(!empty($arrAddressDatas)) {

							if($pagesCount > 1 && $_REQUEST["displayOrderStatus"] == "") {
								include(FILE_MENUE_PAGES);
							}

							echo '<table cellpadding="0" cellspacing="0" border="0" width="100%" class="displayOrders">
									<thead>
										<tr>
											<th style="width:45px;text-align:right;">#</th>
											<th>Name</th>
											<th>Titel</th>
											<th>Anschrift</th>
											<th>PLZ</th>
											<th>Ort</th>
											<th>Gebiet</th>
											<th>Telefon</th>
											<!--<th>Typ</th>-->
											<th>Info</th>
										</tr>
									</thead>
									<tbody>
							';



							$count = 0;
							foreach($arrAddressDatas as $thisKey => $thisValue) {
								if($count%2 == 0){ $rowClass = 'row0'; }
								else { $rowClass = 'row1'; }

								echo '<tr class="'.$rowClass.'">';
									echo '<td style="text-align:right;"><span style="font-weight:bold;cursor:pointer;" title="Datensatz-ID: '.($thisValue["addressesID"]).'">'.($count + 1).'.</span></td>';
									echo '<td>';
									echo $thisValue["addressesName"];
									echo '</td>';
									echo '<td>';
									echo $thisValue["addressesNameTitle"];
									echo '</td>';
									echo '<td style="white-space:nowrap;">';
									echo $thisValue["addressesAddress"];
									echo '<br />';
									echo $thisValue["addressesPostbox"];
									echo '</td>';

									echo '<td>';
									echo $thisValue["addressesZipcode"];
									echo '</td>';

									echo '<td style="white-space:nowrap;">';
									echo $thisValue["addressesCity"];;
									echo '</td>';

									echo '<td style="white-space:nowrap;">';
									echo $thisValue["addressesArea"];;
									echo '</td>';

									echo '<td style="white-space:nowrap;">';
									echo 'Tel.: ' . $thisValue["addressesPhone"];
									echo '<br />';
									echo 'Fax: ' . $thisValue["addressesFax"];
									echo '</td>';

									#echo '<td style="white-space:nowrap;">';
									#echo $thisValue["addressesAddressType"];;
									#echo '</td>';

									echo '<td style="white-space:nowrap;">';
										if($thisValue["addressesMail"] != "") {
											echo '<span class="toolItem"><a href="mailto:'.$thisValue["addressesMail"].'"><img src="layout/icons/iconMail.gif" width="16" height="14" title="&quot;'.($thisValue["addressesName"]). '&quot; per Mail kontaktieren" alt="Mailkontakt" /></a></span>';
										}
										else {
											echo '<span class="toolItem"><img src="layout/spacer.gif" width="16" height="16" alt="" /></span>';
										}

										if($thisValue["addressesInternet"] != "") {
											echo '<span class="toolItem"><a href="http://'.$thisValue["addressesInternet"].'" target="_blank"><img src="layout/icons/iconWeb.gif" width="16" height="16" title="Homepage von &quot;' . ($thisValue["addressesName"]). '&quot; aufrufen" alt="Homepage" /></a></span>';
										}
										else {
											echo '<span class="toolItem"><img src="layout/spacer.gif" width="16" height="16" alt="" /></span>';
										}
									echo '</td>';
								echo '</tr>';
								$count++;
							}
							echo '	</tbody>
								</table>
							';

							if($pagesCount > 1 && $_REQUEST["displayOrderStatus"] == "") {
								include(FILE_MENUE_PAGES);
							}

						}
						else {

						}

					?>

				</div>
			</div>
		</div>
		<div class="clear"></div>
	</div>
</div>

<script language="javascript" type="text/javascript">
	$(document).ready(function() {
		<?php if($_REQUEST["editID"] != '' && $_REQUEST["editID"] != 'NEW'){ ?>
		$('body').attr('onload', 'initialize();');
		<?php } ?>

		$('.buttonLoadMap').click(function() {
			loadGoogleMapAddresses('<?php echo $thisAddressType; ?>');
		});
		<?php if($_REQUEST["openMap"] == true){ ?>
			loadGoogleMapAddresses('<?php echo $thisAddressType; ?>');
		<?php } ?>

		<?php
			// if($_REQUEST["editID"] != "NEW")
			if(1)
			{
		?>
		$(function() {
			$('#tabs').tabs();
		});
		<?php
			}
		?>
		colorRowMouseOver('.displayOrders tbody tr');
		$('.buttonNotice').click(function () {
			loadNotice($(this));
		});
		setFormDutyFields(<?php echo $jsonFormDutyFields; ?>);

	});
</script>

<?php require_once('inc/footerHTML.inc.php'); ?>