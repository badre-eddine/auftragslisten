<?php
	require_once('inc/requires.inc.php');

	if(!$arrGetUserRights["externalLinks"]) {
		header('location: ' . PAGE_EXIT_LOCATION);
		exit;
	}

	$jswindowMessage = '';
	$warningMessage = '';
	$errorMessage = '';
	$successMessage = '';
	$infoMessage = '';

	$dbConnection = new DB_Connection();
	$db_open = $dbConnection->db_connect();

	// BOF DEFINE EXTERNAL LINKS
	$arrExternalLinks[$i] = array();
	$arrExternalLinksGroup = array();
	$i = -1;
	$i++;
	$arrExternalLinksGroup[$i] = 'Interne Online-Shops';
	$arrExternalLinks[$i][] = array('LINK' => 'http://www.burhan-ctr.de/', 'TEXT' => 'burhan-ctr.de', 'TARGET' => '_blank');
	$arrExternalLinks[$i][] = array('LINK' => 'http://www.b3-werbepartner.de/', 'TEXT' => 'b3-werbepartner.de', 'TARGET' => '_blank');
	$arrExternalLinks[$i][] = array('LINK' => 'http://alfahosting.de/kunden/index.php/Benutzer:Login/', 'TEXT' => 'alfahosting.de', 'TARGET' => '_blank');

	$i++;
	$arrExternalLinksGroup[$i] = 'Externe Online-Shops';
	$arrExternalLinks[$i][] = array('LINK' => 'http://sellercentral.amazon.de/', 'TEXT' => 'Amazon', 'TARGET' => '_blank');
	$arrExternalLinks[$i][] = array('LINK' => 'http://signin.ebay.de/ws/eBayISAPI.dll?SignIn', 'TEXT' => 'eBay', 'TARGET' => '_blank');

	$i++;
	$arrExternalLinksGroup[$i] = 'Soziale Netzwerke';
	$arrExternalLinks[$i][] = array('LINK' => 'http://www.facebook.de/B3.Werbepartner', 'TEXT' => 'Facebook/B3', 'TARGET' => '_blank');
	$arrExternalLinks[$i][] = array('LINK' => 'http://www.facebook.de/Burhan.Car.Trade.Requirement', 'TEXT' => 'Facebook/Burhan', 'TARGET' => '_blank');
	$arrExternalLinks[$i][] = array('LINK' => 'http://twitter.com/B3_Werbepartner', 'TEXT' => 'Twitter/B3', 'TARGET' => '_blank');
	$arrExternalLinks[$i][] = array('LINK' => 'http://twitter.com/Burhan_CTR', 'TEXT' => 'Twitter/Burhan', 'TARGET' => '_blank');
	$arrExternalLinks[$i][] = array('LINK' => 'http://www.youtube.com/channel/UCIHJU1XPSnTgV3AAgN4DeRQ', 'TEXT' => 'YouTube/Burhan', 'TARGET' => '_blank');
	$arrExternalLinks[$i][] = array('LINK' => 'https://myspace.com/burhan_ctr', 'TEXT' => 'MySpace/Burhan', 'TARGET' => '_blank');
	$arrExternalLinks[$i][] = array('LINK' => 'https://plus.google.com/114741124923997755317', 'TEXT' => 'Google+/Burhan', 'TARGET' => '_blank');


	$i++;
	$arrExternalLinksGroup[$i] = 'Intern';
	#$arrExternalLinks[$i][] = array('LINK' => '../mediawiki/', 'TEXT' => 'Burhan-Wiki', 'TARGET' => '_blank');
	$arrExternalLinks[$i][] = array('LINK' => 'http://www.burhan-ctr.de/Burhan_Kundenerfassung/', 'TEXT' => 'Kunden-Erfassung', 'TARGET' => '_blank');

	$i++;
	$arrExternalLinksGroup[$i] = 'Lieferanten-Shops';
	$arrExternalLinks[$i][] = array('LINK' => 'http://www.best-offer-trading.com/', 'TEXT' => 'Best-Offer', 'TARGET' => '_blank');
	$arrExternalLinks[$i][] = array('LINK' => 'http://www.eichner-org.com/Shop?lang=de&amp;id=27', 'TEXT' => 'Eichner', 'TARGET' => '_blank');
	// $arrExternalLinks[$i][] = array('LINK' => 'http://www.hepla.de/', 'TEXT' => 'Hepla', 'TARGET' => '_blank');
	$arrExternalLinks[$i][] = array('LINK' => 'http://hepla-grosshandel.shop-website.de/', 'TEXT' => 'Hepla', 'TARGET' => '_blank');
	$arrExternalLinks[$i][] = array('LINK' => 'http://www.lm-accessoires.com/', 'TEXT' => 'L+M Accessoires', 'TARGET' => '_blank');
	$arrExternalLinks[$i][] = array('LINK' => 'http://www.spranz.de/', 'TEXT' => 'Spranz', 'TARGET' => '_blank');
	$arrExternalLinks[$i][] = array('LINK' => 'http://www.tokai-at-igro.com/', 'TEXT' => 'Tokai', 'TARGET' => '_blank');
	$arrExternalLinks[$i][] = array('LINK' => 'http://www.elektronica.de/', 'TEXT' => 'TOM', 'TARGET' => '_blank');

	$i++;
	$arrExternalLinksGroup[$i] = 'Sonstige';
	$arrExternalLinks[$i][] = array('LINK' => 'http://www.gs1-germany.de/', 'TEXT' => 'EAN-Nummern-Vergabe', 'TARGET' => '_blank');
	$arrExternalLinks[$i][] = array('LINK' => 'http://www.convent-zollagentur.de/', 'TEXT' => 'Zollagentur', 'TARGET' => '_blank');
	$arrExternalLinks[$i][] = array('LINK' => 'http://autohaendler-in-deutschland.de/', 'TEXT' => 'Autohändler', 'TARGET' => '_blank');

	$i++;
	$arrExternalLinksGroup[$i] = 'Material-Shops';
	$arrExternalLinks[$i][] = array('LINK' => 'http://www.viking.de/', 'TEXT' => 'Viking', 'TARGET' => '_blank');
	$arrExternalLinks[$i][] = array('LINK' => 'http://www.groener.de/', 'TEXT' => 'Drucker-Folie', 'TARGET' => '_blank');
	$arrExternalLinks[$i][] = array('LINK' => 'http://www.mh-grafischer-handel.de/', 'TEXT' => 'Heywinkel KG (EPSON)', 'TARGET' => '_blank');

	$i++;
	$arrExternalLinksGroup[$i] = 'Konkurrenz';
	$arrExternalLinks[$i][] = array('LINK' => 'http://www.ahb-international.com/', 'TEXT' => 'AHB', 'TARGET' => '_blank');
	$arrExternalLinks[$i][] = array('LINK' => 'http://www.hermann-direkt.de/', 'TEXT' => 'Hermann', 'TARGET' => '_blank');
	$arrExternalLinks[$i][] = array('LINK' => 'http://www.realgarant-shop.de/', 'TEXT' => 'Real-Garant', 'TARGET' => '_blank');
	$arrExternalLinks[$i][] = array('LINK' => 'http://www.utsch.com/', 'TEXT' => 'Utsch', 'TARGET' => '_blank');
	$arrExternalLinks[$i][] = array('LINK' => 'http://www.gutschild.de/', 'TEXT' => 'Gutschild', 'TARGET' => '_blank');
	$arrExternalLinks[$i][] = array('LINK' => 'http://www.helmut-seitz.de/', 'TEXT' => 'Seitz', 'TARGET' => '_blank');
	$arrExternalLinks[$i][] = array('LINK' => 'http://www.wuerth.de/', 'TEXT' => 'Würth', 'TARGET' => '_blank');

	// EOF DEFINE EXTERNAL LINKS

?>
<?php
	require_once('inc/headerHTML.inc.php');
	$thisTitle = "Externe Links";
	$headerHTML = preg_replace("/{###TITLE###}/", strip_tags($thisTitle), $headerHTML);
	echo $headerHTML;

	require_once(FILE_MENUE_TOP);

?>

<div id="mainArea">
	<div id="mainContent">
		<?php require_once(FILE_MENUE_SIDEBAR); ?>
<div id="contentArea">
	<div id="contentAreaElements">
		<h1><?php if(SHOW_PAGE_TITLE_ICONS) { echo '<img src="' . PATH_ICONS_MENUE_TITLES . 'externalLinks.png" alt="" />'; } ?> <?php echo $thisTitle; ?></h1>

		<div class="contentDisplay">
			<div>

			<?php
				if($warningMessage != "") {	echo '<p class="warningArea">'.$warningMessage.'</p>'; }
				if($errorMessage != "") {	echo '<p class="errorArea">'.$errorMessage.'</p>'; }
				if($successMessage != "") {	echo '<p class="successArea">'.$successMessage.'</p>'; }
				if($infoMessage != "") {	echo '<p class="infoArea">'.$infoMessage.'</p>'; }

				if(!empty($arrExternalLinksGroup)) {
					foreach($arrExternalLinksGroup as $thisGroupKey => $thisGroupValue) {
						echo '<div class="externalLinks">';
						echo '<h2>' . $thisGroupValue . '</h2>';
						if(!empty($arrExternalLinks)) {
							foreach($arrExternalLinks[$thisGroupKey] as $thisLinkKey => $thisLinkValue) {
								echo '<div class="thumbshotItem">';
								echo '<p><a href="' . $thisLinkValue["LINK"] . '" title="' . $thisLinkValue["LINK"] . '" target="_blank">' . htmlentities(utf8_decode($thisLinkValue["TEXT"])) . '</a></p>';
								echo '<a href="' . $thisLinkValue["LINK"] . '" target="_blank">';
								if(file_exists(DIRECTORY_EXTERNAL_LINK_IMAGES . getUrlImageName($thisLinkValue["LINK"]))) {
									$thisImageSrc = DIRECTORY_EXTERNAL_LINK_IMAGES . getUrlImageName($thisLinkValue["LINK"]);
								}
								else {
									$thisImageSrc = preg_replace("/{###LINK###}/",  $thisLinkValue["LINK"], PATH_THUMBSHOT_SRC);
									copy($thisImageSrc, DIRECTORY_EXTERNAL_LINK_IMAGES . getUrlImageName($thisLinkValue["LINK"]));
								}
								clearstatcache();
								echo '<img src="' . $thisImageSrc . '" title="' . htmlentities(utf8_decode($thisLinkValue["TEXT"])) . '" width="90" height="60" border="1" alt="" />';
								echo '</a>';
								echo '</div>';

							}
						}
						echo '<div class="clear"></div>';
						echo '</div>';
						echo '<hr />';
					}
				}

			?>
			</div>
		</div>
	</div>
</div>
<div class="clear"></div>
</div>
</div>
<script language="javascript" type="text/javascript">
	$(document).ready(function() {});
</script>

<?php require_once('inc/footerHTML.inc.php'); ?>