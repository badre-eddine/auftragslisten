<?php
	require_once('inc/requires.inc.php');

	DEFINE ('KZH_EK_PRICE', 0.59);

	if(!$arrGetUserRights["editProvisionDatas"] && !$arrGetUserRights["displayProvisionDatas"]) {
		header('location: ' . PAGE_EXIT_LOCATION);
		exit;
	}

	$jswindowMessage = '';
	$warningMessage = '';
	$errorMessage = '';
	$successMessage = '';
	$infoMessage = '';

	if($_GET["downloadFile"] != "") {
		$thisDownloadFolder = DIRECTORY_CREATED_DOCUMENTS_GROUPS;
		if($_GET["thisDocumentType"] == 'AB' || $_GET["thisDocumentType"] == 'RE'){
			$thisDownloadFolder = DIRECTORY_CREATED_DOCUMENTS_CUSTOMERS;
		}
		require_once('classes/downloadFile.class.php');
		$thisDownload = new downloadFile($thisDownloadFolder, $_GET["downloadFile"]);
		$errorMessage .= $thisDownload->startDownload();
	}

	$dbConnection = new DB_Connection();
	$db_open = $dbConnection->db_connect();

	// BOF READ VERTRETER
		$arrSalesmenDatas = getSalesmen();
	// EOF READ VERTRETER

	// BOF READ BANK TYPES
		$arrBankAccountTypeDatas = getBankAccountTypes();
	// EOF READ BANK TYPES

	// BOF READ BESTELL ARTEN
		$arrOrderTypeDatas = getOrderTypes();
	// EOF READ BESTELL ARTEN

	// BOF READ BESTELL STATUS TYPEN
		$arrOrderStatusTypeDatas = getOrderStatusTypes();
	// EOF READ BESTELL STATUS TYPEN

	// BOF READ DOCUMENT TYPE DATAS
		$arrPaymentStatusTypeDatas = getPaymentStatusTypes();
	// EOF READ DOCUMENT TYPE DATAS

	// BOF READ CUSTOMER GROUP DATAS
		$arrCustomerGroupDatas = getCustomerGroups();
	// EOF READ CUSTOMER GROUP DATAS

	// DEFINE('TABLE_ORDER_THIS', TABLE_ORDER_INVOICES);
	// DEFINE('TABLE_ORDER_THIS_DETAILS', TABLE_ORDER_INVOICES_DETAILS);

	// DEFINE('TABLE_ORDER_THIS', TABLE_ORDER_CONFIRMATIONS);
	// DEFINE('TABLE_ORDER_THIS_DETAILS', TABLE_ORDER_CONFIRMATIONS_DETAILS);

	if(($_POST["submitFormProvision"] != '' || $_POST["createDocumentProvision"] != '') && $_POST["exportOrdersGroupID"] > 0 && !empty($_POST["arrProvisionValues"])) {
		if(file_exists(DIRECTORY_CREATED_DOCUMENTS_GROUPS . $_POST["tempHtmlFileName"])) {
			#$successMessage .= 'Die tempor&auml;re Datei &quot;' . basename(DIRECTORY_CREATED_DOCUMENTS_GROUPS . $_POST["tempHtmlFileName"]) . '&quot; wurde gefunden.' . '<br />';
		}
		else  {
			#$errorMessage .= 'Die tempor&auml;re Datei &quot;' . basename(DIRECTORY_CREATED_DOCUMENTS_GROUPS . $_POST["tempHtmlFileName"]) . '&quot; konnte nicht gefunden werden.' . '<br />';
		}

		$thisCreatedDocumentNumber = "PR-".date('ym') . '_' . $arrCustomerGroupDatas[$_POST["exportOrdersGroupID"]]["customerGroupsName"];

		if($_POST["createDocumentProvision"] != '') {

			$fp = fopen(DIRECTORY_CREATED_DOCUMENTS_GROUPS . $_POST["tempHtmlFileName"], 'r');
			$contentPDF = fread($fp, filesize(DIRECTORY_CREATED_DOCUMENTS_GROUPS . $_POST["tempHtmlFileName"]));
			fclose($fp);

			$valueSum = 0;
			$valueMwst = 0;
			$valueTotal = 0;

			$contentPDF = preg_replace('/<input(.*)\/>/ismU', '', $contentPDF);

			foreach($_POST["arrProvisionValues"] as $thisKey => $thisValue) {
				$valueSum += str_replace(",", ".", $thisValue["groupProvisionValue"]);

				$contentPDF = preg_replace('/<!-- BOF VK_PRICE_'.$thisKey.' -->(.*)<!-- EOF VK_PRICE_'.$thisKey.' -->/ismU', $thisValue["groupProvisionProductVkprice"], $contentPDF);
				$contentPDF = preg_replace('/<!-- BOF PROVISION_PRICE_'.$thisKey.' -->(.*)<!-- EOF PROVISION_PRICE_'.$thisKey.' -->/ismU', $thisValue["groupProvisionValue"], $contentPDF);
			}

			$contentCSV = formatCSV($contentPDF);
			$contentPDF = preg_replace('/<!-- BOF DOCUMENT_NUMBER_ORDER -->(.*)<!-- EOF DOCUMENT_NUMBER_ORDER -->/ismU', '', $contentPDF);

			$valueProvision = 0;
			if($arrSalesmanDatas["groupsProvision"] > 0){
				$valueProvision = $valueSum * ($arrSalesmanDatas["groupsProvision"]/100);
			}
			$valueMwst = ($valueSum + $valueProvision) * MWST/100;
			$valueTotal = $valueSum + $valueProvision + $valueMwst;

			$contentPDF = preg_replace('/<!-- BOF SUM_VALUE --><!-- EOF SUM_VALUE -->/ismU', number_format($valueSum, 2, ',', ''), $contentPDF);
			$contentPDF = preg_replace('/<!-- BOF PROVISION_VALUE --><!-- EOF PROVISION_VALUE -->/ismU',  number_format($valueProvision, 2, ',', ''), $contentPDF);
			$contentPDF = preg_replace('/<!-- BOF MWST_VALUE --><!-- EOF MWST_VALUE -->/ismU',  number_format($valueMwst, 2, ',', ''), $contentPDF);
			$contentPDF = preg_replace('/<!-- BOF TOTAL_VALUE --><!-- EOF TOTAL_VALUE -->/ismU', number_format($valueTotal, 2, ',', ''), $contentPDF);

			$contentPDF = preg_replace('/ style="background-color:#FEFFAF;"/', '', $contentPDF);
			$contentPDF = preg_replace('/background-color:#FEFFAF;/', '', $contentPDF);
			$contentPDF = preg_replace('/<tbody>/ismU', '', $contentPDF);
			$contentPDF = preg_replace('/<\/tbody>/ismU', '', $contentPDF);
			$contentPDF = preg_replace('/<thead>/ismU', '', $contentPDF);
			$contentPDF = preg_replace('/<\/thead>/ismU', '', $contentPDF);
			$contentPDF = preg_replace('/ row[0-9]/ismU', '', $contentPDF);

			// BOF STORE CSV
			if(file_exists(DIRECTORY_CREATED_DOCUMENTS_GROUPS . $_POST["thisCreatedCsvName"])) {
				// chmod(DIRECTORY_CREATED_DOCUMENTS_GROUPS . $_POST["thisCreatedCsvName"] , "0777");
				unlink(DIRECTORY_CREATED_DOCUMENTS_GROUPS . $_POST["thisCreatedCsvName"]);
			}
			clearstatcache();
			$fp = fopen(DIRECTORY_CREATED_DOCUMENTS_GROUPS . $_POST["thisCreatedCsvName"], 'w');
			fwrite($fp, stripslashes($contentCSV));
			fclose($fp);
			// EOF STORE CSV

			$loadPdfContent = "";
			$loadContentTemplate = implode('', file(DIRECTORY_PDF_TEMPLATES . '/template_BASIC.html'));
			require_once(DIRECTORY_PDF_TEMPLATES . 'template_TEXT_DATAS.inc.php');

			$loadPdfContent = $loadContentTemplate;
			$loadPdfContent = preg_replace("/{###CARD_DATAS###}/", $contentPDF, $loadPdfContent);

			// STYLES
			$loadTemplateStyles = implode('', file(DIRECTORY_PDF_TEMPLATES . 'template_STYLES.html'));
			$loadPdfContent = preg_replace("/{###STYLES###}/", $loadTemplateStyles, $loadPdfContent);

			$loadPdfContent = preg_replace("/{###DOCUMENT_DATAS###}/", $arrDocumentDatas["PR"], $loadPdfContent);

			$loadPdfContent = preg_replace('/<a href="(.*)">(.*)<\/a>/ismU', '$2', $loadPdfContent);
			// DOKUMENT-TYP PROVISION anlegen

			$loadedAdressDatas = array(
				"ADRESS_COMPANY" => $arrSalesmanDatas["customersFirmenname"],
				"ADRESS_MANAGER" => "",
				"ADRESS_CONTACT" => $arrSalesmanDatas["customersFirmenInhaberVorname"] . " " . $arrSalesmanDatas["customersFirmenInhaberNachname"],
				"ADRESS_STREET" => $arrSalesmanDatas["customersCompanyStrasse"].' '.$arrSalesmanDatas["customersCompanyHausnummer"],
				"ADRESS_CITY" => $arrSalesmanDatas["customersCompanyPLZ"].' '.$arrSalesmanDatas["customersCompanyOrt"],
				"ADRESS_COUNTRY" => $arrCountryTypeDatas[$arrSalesmanDatas["customersCompanyCountry"]]["countries_name"],
				"ADRESS_MAIL" => $arrSalesmanDatas["customersMail1"],
			);

			$loadedDocumentDatas = array(
				"DOCUMENT_NUMBER_RE" => "",
				"CUSTOMERS_SALESMAN" => "",
				"CUSTOMER_NUMBER" => $arrSalesmanDatas["customersKundennummer"],
				"CUSTOMERS_TAX_ACCOUNT" => $arrSalesmanDatas["customersTaxAccountID"],
				"CUSTOMER_PHONE" => "",
				"CUSTOMER_MAIL" => "",
				"CUSTOMERS_ORDER_NUMBER" => "",

				"SHIPPING_COSTS" => "",
				"SHIPPING_TYPE" => "",
				"SHIPPING_TYPE_NUMBER" => "",
				"PACKAGING_COSTS" => "",
				"CARD_CASH_ON_DELIVERY_VALUE" => "",
				"SHIPPING_DATE" => "",
				"BINDING_DATE" => "",

				"COMPANY_HEADER_IMAGE" => DIRECTORY_PDF_IMAGES . "briefkopf.jpg",
				"COMPANY_NAME_LONG" => COMPANY_NAME_LONG,
				"COMPANY_PHONE" => COMPANY_PHONE,
				"COMPANY_FAX" => COMPANY_FAX,
				"COMPANY_INTERNET" => COMPANY_INTERNET,
				"COMPANY_MANAGER" => COMPANY_MANAGER,
				"COMPANY_MAIL" => COMPANY_MAIL,
				"COMPANY_STREET_NAME" => COMPANY_STREET_NAME,
				"COMPANY_STREET_NUMBER" => COMPANY_STREET_NUMBER,
				"COMPANY_ZIPCODE" => COMPANY_ZIPCODE,
				"COMPANY_CITY" => COMPANY_CITY,
				"COMPANY_TAX_NUMBER" => COMPANY_TAX_NUMBER,
				"COMPANY_UID_NUMBER" => COMPANY_UID_NUMBER,
				"COMPANY_VENUE" => COMPANY_VENUE,

				"BANK_ACCOUNT_TYPE" => $arrBankAccountTypeDatas['KSK']["bankAccountTypesShortName"],
				"BANK_ACCOUNT_CODE" => $arrBankAccountTypeDatas['KSK']["bankAccountTypesBankCode"],
				"BANK_ACCOUNT_NAME" => $arrBankAccountTypeDatas['KSK']["bankAccountTypesName"],
				"BANK_ACCOUNT_NUMBER" => $arrBankAccountTypeDatas['KSK']["bankAccountTypesAccountNumber"],

				"INTRO_TEXT" => $arrContentDatas["PR"]["INTRO_TEXT"],
				"OUTRO_TEXT" => $arrContentDatas["PR"]["OUTRO_TEXT"],
				"EDITOR_TEXT" => $arrContentDatas["PR"]["EDITOR_TEXT"],
				"REGARDS_TEXT" => $arrContentDatas["PR"]["REGARDS_TEXT"],
				"SALUTATION_TEXT" => $arrContentDatas["PR"]["SALUTATION_TEXT"],
				"PAYMENT_CONDITIONS" => $arrContentDatas["PR"]["PAYMENT_CONDITIONS"],
				"PAYMENT_CONDITION" => $arrPaymentConditionDatas["PR"]["paymentConditionsName"],
				"PAYMENT_TYPE" => "",
			);

			$loadedDocumentDatas["ADDRESS_INVOICE"] = "";
			$loadedDocumentDatas["ADDRESS_DELIVERY"] = "";
			$loadedDocumentDatas["INVOICE_SKONTO"] = "";

			$loadedDocumentDatas["WARNING_INVOICE_IS_PAID"] = "";

			$loadedDocumentDatas["CREATION_DATE"] = formatDate(date('Y-m-d'), 'display');

			$loadedDocumentDatas["DOCUMENT_NUMBER"] = $thisCreatedDocumentNumber;

			$loadSubjectText = 'Gutschrift-Nr.: ' . '{###DOCUMENT_NUMBER###}';

			$loadSubject = ''.$loadSubjectText. '';

			$loadPdfContent = preg_replace("/{###SUBJECT###}/", $loadSubject, $loadPdfContent);

			if(!empty($loadedAdressDatas)) {
				foreach($loadedAdressDatas as $thisKey => $thisValue) {
					if($thisValue != "") {
						$loadPdfContent = preg_replace("/{###".$thisKey."###}/", $thisValue . "<br />", $loadPdfContent);
					}
					else {
						$loadPdfContent = preg_replace("/{###".$thisKey."###}/", "", $loadPdfContent);
					}
				}
			}

			if(!empty($loadedDocumentDatas)) {
				// if(constant('BANK_ACCOUNT_' . $_POST["selectBankAccount"]. '_SHOW_WARNING')){
				$loadPdfContent = preg_replace("/{###BANK_ACCOUNT_WARNING###}/", "", $loadPdfContent);
				foreach($loadedDocumentDatas as $thisKey => $thisValue) {
					$loadPdfContent = preg_replace("/{###".$thisKey."###}/", $thisValue, $loadPdfContent);
					if(trim($thisValue) == '') {
						$loadPdfContent = preg_replace("/<!-- BOF_".$thisKey." -->(.*)<!-- EOF_".$thisKey." -->/", '', $loadPdfContent);
					}
				}
			}

			$loadPdfContent = preg_replace('/class="displayOrders"/', '', $loadPdfContent);
			$loadPdfContent = removeUnnecessaryChars($loadPdfContent);

			$pdfContentPage = $loadPdfContent;

			// BOF createPDF
			require_once(DIRECTORY_HTML2PDF . 'html2pdf.class.php');

			/*
			if(file_exists(DIRECTORY_CREATED_DOCUMENTS_GROUPS . $thisCreatedPdfName)) {
				try {
					chmod(DIRECTORY_CREATED_DOCUMENTS_GROUPS . $thisCreatedPdfName , "0777");
					unlink(DIRECTORY_CREATED_DOCUMENTS_GROUPS . $thisCreatedPdfName);
				}
				catch (Exception $e) {}
			}
			clearstatcache();
			*/

			try {
				// $html2pdf = new HTML2PDF('P', 'A4', 'de', true, 'UTF-8', array(0, 0, 0, 0));
				// $html2pdf = new HTML2PDF('L', 'A4', 'de', true, 'UTF-8', array(22, 5, 5, 5));
				$html2pdf = new HTML2PDF('P', 'A4', 'de', true, 'UTF-8', array(10, 10, 10, 10));
				// $html2pdf = new HTML2PDF('P', 'A4', 'de');
				// $html2pdf = new HTML2PDF('P', 'A4', 'de', true, 'UTF-8');
				// $html2pdf->setModeDebug();
				// $html2pdf->setDefaultFont('Arial');
				// $html2pdf->createIndex('Inhaltsverzeichnis', 12, 8, false, true, 2);

				if(file_exists(DIRECTORY_CREATED_DOCUMENTS_GROUPS . $_POST["thisCreatedPdfName"])) {
					// chmod(DIRECTORY_CREATED_DOCUMENTS_GROUPS . $_POST["thisCreatedPdfName"] , "0777");
					unlink(DIRECTORY_CREATED_DOCUMENTS_GROUPS . $_POST["thisCreatedPdfName"]);
				}
				clearstatcache();

				ob_start();
				echo $pdfContentPage;
				$contentPDF = ob_get_clean();
				$html2pdf->writeHTML($contentPDF, isset($_GET['vuehtml']));
				$html2pdf->Output($_POST["thisCreatedPdfName"], 'F', DIRECTORY_CREATED_DOCUMENTS_GROUPS);

				if(file_exists(DIRECTORY_CREATED_DOCUMENTS_GROUPS . $_POST["thisCreatedPdfName"])) {
					#unlink($_POST["thisCreatedPdfName"]);
					/*
					$sql_update = array();
					foreach($thisDocumentOrdersIDs as $thisKey => $thisValue) {
						$sql_update[] = " `ordersToDocumentsOrderID` = '" . $thisValue . "' ";
					}

					$sql = "UPDATE `" . TABLE_ORDERS_TO_DOCUMENTS . "`
								SET `ordersToDocumentsStatus` = 'created'
								WHERE ". implode(" OR ", $sql_update) . "
					";

					$rs = $dbConnection->db_query($sql);
					*/
					}
				clearstatcache();
			}
			catch(HTML2PDF_exception $e) {
				echo $e;
				exit;
			}
			if(file_exists(DIRECTORY_CREATED_DOCUMENTS_GROUPS . $_POST["thisCreatedPdfName"])) {

				$successMessage .= 'Die PDF-Datei &quot;' . $_POST["thisCreatedPdfName"] . '&quot wurde generiert. ' . '<br />';
				$showPdfDownloadLink = true;
			}
			else {
				$errorMessage .= 'Die PDF-Datei &quot;' . $_POST["thisCreatedPdfName"] . '&quot konnte nicht generiert werden. ' . '<br />';
				$showPdfDownloadLink = false;
			}

			clearstatcache();
			// EOF createPDF
		}

		// BOF STORE DATAS
			$arrTemp = array();
			$deleteWhere = '';
			foreach($_POST["arrProvisionValues"] as $thisKey => $thisValue) {
				$arrTemp[] = $thisValue["orderDocumentsNumber"];
			}
			if(!empty($arrTemp)){
				$arrTemp = array_unique($arrTemp);
				$arrDocumentContentDocumentNumbers = implode(";",$arrTemp);
				foreach($arrTemp as $thisKey => $thisValue) {
					$deleteWhere .= " OR `groupsProvisionsContentDocumentNumber` LIKE '%" . $thisValue . "%'";
				}
			}

			$sql = "DELETE
				`" . TABLE_GROUPS_PROVISIONS . "`,
				`" . TABLE_GROUPS_PROVISIONS_DETAILS . "`

				FROM `" . TABLE_GROUPS_PROVISIONS . "`

				LEFT JOIN `" . TABLE_GROUPS_PROVISIONS_DETAILS . "`
				ON(`" . TABLE_GROUPS_PROVISIONS . "`.`groupsProvisionsID` = `" . TABLE_GROUPS_PROVISIONS_DETAILS . "`.`groupsProvisionsDetailsProvisionsID`)

				WHERE
					`" . TABLE_GROUPS_PROVISIONS . "`.`groupsProvisionsDocumentNumber` = '" . $thisCreatedDocumentNumber . "'
					" . $deleteWhere . "
			";

			$rs = $dbConnection->db_query($sql);


			$sql = "INSERT INTO `" . TABLE_GROUPS_PROVISIONS . "` (

						`groupsProvisionsID`,
						`groupsProvisionsDocumentNumber`,
						`groupsProvisionsGroupID`,
						`groupsProvisionsDocumentDate`,
						`groupsProvisionsSum`,
						`groupsProvisionsMwst`,
						`groupsProvisionsMwstValue`,
						`groupsProvisionsTotal`,
						`groupsProvisionsStatus`,
						`groupsProvisionsDocumentPath`,
						`groupsProvisionsContentDocumentNumber`
					)
					VALUES (
						'%',
						'" . $thisCreatedDocumentNumber . "',
						'" . $_POST["exportOrdersGroupID"] . "',
						'" . $loadedDocumentDatas["CREATION_DATE"] . "',
						'" . $valueSum . "',
						'" . MWST . "',
						'" . $valueMwst . "',
						'" . $valueTotal . "',
						'1',
						'" . DIRECTORY_CREATED_DOCUMENTS_GROUPS . $_POST["thisCreatedPdfName"] . "',
						'" . $arrDocumentContentDocumentNumbers . "'
					)
				";
			$rs = $dbConnection->db_query($sql);

			if($rs) {
				$successMessage .= ' Der Datensatz wurde gespeichert.' .'<br />';
				$thisInsertID = mysqli_insert_id();
			}
			else {
				$errorMessage .= ' Der Datensatz konnte nicht gespeichert werden.' .'<br />';
			}

			$sql = "INSERT IGNORE INTO `" . TABLE_GROUPS_PROVISIONS_DETAILS . "` (
						`groupsProvisionsDetailsID`,
						`groupsProvisionsDetailsProvisionsID`,
						`groupsProvisionsDetailsDocumentsDetailID`,
						`groupsProvisionsDetailsDocumentNumber`,
						`groupsProvisionsDetailsProductOrderDate`,
						`groupsProvisionsDetailsProductAmount`,
						`groupsProvisionsDetailsProductNumber`,
						`groupsProvisionsDetailsProductName`,
						`groupsProvisionsDetailsProductRecipientCustomerNumber`,
						`groupsProvisionsDetailsProductRecipientName`,
						`groupsProvisionsDetailsProductRecipientZipcode`,
						`groupsProvisionsDetailsProductRecipientCity`,
						`groupsProvisionsDetailsProductVkPreis`,
						`groupsProvisionsDetailsProductProvisionPrice`
					)
					VALUES
			";



			foreach($_POST["arrProvisionValues"] as $thisKey => $thisValue) {

				$arrSQL[] = " (
							'%',
							'" . $thisInsertID . "',
							'" . $thisValue["orderDocumentDetailID"] . "',
							'" . $thisValue["orderDocumentsNumber"] . "',
							'" . ($thisValue["orderDocumentsOrderDate"]) . "',
							'" . $thisValue["orderDocumentDetailProductQuantity"] . "',
							'" . $thisValue["orderDocumentDetailProductNumber"] . "',
							'" . addslashes($thisValue["orderDocumentDetailProductName"]) . "',
							'" . $thisValue["orderDocumentsCustomerNumber"] . "',
							'" . addslashes($thisValue["orderDocumentsAddressCompany"]) . "',
							'" . $thisValue["orderDocumentsAddressZipcode"] . "',
							'" . $thisValue["orderDocumentsAddressCity"] . "',
							'" . str_replace(',', '.', $thisValue["groupProvisionProductVkprice"]) . "',
							'" . str_replace(',', '.', $thisValue["groupProvisionValue"]) . "'
						)
					";
			}

			if(!empty($arrSQL)){
				$sql = $sql . implode(", ", $arrSQL);
				$rs = $dbConnection->db_query($sql);
			}

			#$_POST["exportOrdersGroupID"] = '';
			// BOF DELETE TEMP HTML FILE
			if(file_exists(DIRECTORY_CREATED_DOCUMENTS_GROUPS . $_POST["tempHtmlFileName"])) {
				unlink(DIRECTORY_CREATED_DOCUMENTS_GROUPS . $_POST["tempHtmlFileName"]);
			}
			clearstatcache();
			// EOF DELETE TEMP HTML FILE

		// BOF STORE DATAS

	}
?>
<?php
	require_once('inc/headerHTML.inc.php');
	$thisTitle = "Berechnung der Kundengruppen-Provision";
	if($_POST["exportOrdersGroupID"] != "") {
		$thisTitle .= ' - <span class="headerSelectedEntry">Gruppe ' . $arrCustomerGroupDatas[$_POST["exportOrdersGroupID"]]["customerGroupsName"] . '</span>';
	}
	$headerHTML = preg_replace("/{###TITLE###}/", strip_tags($thisTitle), $headerHTML);
	echo $headerHTML;

	require_once(FILE_MENUE_TOP);
?>

<div id="mainArea">
	<div id="mainContent">
		<?php require_once(FILE_MENUE_SIDEBAR); ?>
		<div id="contentArea">
			<a name="top"></a>
			<div id="contentAreaElements">
				<h1><?php if(SHOW_PAGE_TITLE_ICONS) { echo '<img src="' . PATH_ICONS_MENUE_TITLES . 'export.png" alt="" />'; } ?> <?php echo $thisTitle; ?></h1>

				<div id="searchFilterArea">
					<form name="formSearchDatas" method="post" action="<?php echo $_SERVER["PHP_SELF"]; ?>">
					<table border="0" cellpadding="0" cellspacing="0" width="" class="searchFilterContent">
						<tr>
							<td>
								<label for="exportOrdersGroupID">Kundengruppe:</label>
								<select id="exportOrdersGroupID" name="exportOrdersGroupID" class="inputSelect_300">
									<option value=""> - </option>
									<?php
										if(!empty($arrCustomerGroupDatas)) {
											foreach($arrCustomerGroupDatas as $thisKey => $thisValue) {
												$selected = '';
												if($thisKey == $_POST["exportOrdersGroupID"]) {
													$selected = ' selected ';
												}
												echo '
													<option value="' . $thisKey . '" '.$selected.' >' . ($arrCustomerGroupDatas[$thisKey]["customerGroupsName"]) . '</option>';
											}
										}
									?>
								</select>
							</td>
							<td> von
								<?php
									if($_POST["exportOrdersDateFrom"] != '') {
										$fromDate = $_POST["exportOrdersDateFrom"];
									}
									else {
										$fromDate = formatDate(date("Y-m-d", mktime(1, 1, 1, (date("m") - 1), (1), date("Y"))), "display");
									}
								?>
								<input type="text" name="exportOrdersDateFrom" id="exportOrdersDateFrom" maxlength="2" class="inputField_70" readonly value="<?php echo $fromDate; ?>" />
								bis
								<?php
									if($_POST["exportOrdersDateTo"] != '') {
										$toDate = $_POST["exportOrdersDateTo"];
									}
									else {
										$toDate = formatDate(date("Y-m-d", mktime(1, 1, 1, date("m"), (0), date("Y"))), "display");
									}
								?>
								<input type="text" name="exportOrdersDateTo" id="exportOrdersDateTo" maxlength="2" class="inputField_70" readonly value="<?php echo $toDate; ?>" />
							</td>
							<!--
							<td>
								<input type="checkbox" name="displayUnExportedLastMonth" value="1" <?php if($_POST["displayUnExportedLastMonth"] == '1'){ echo ' checked="checked" '; } ?> /> <span class="infotext">Unberechnete Provisionen des Vormonats anzeigen?</span>
							</td>
							-->
							<td>
								<input type="hidden" name="editID" id="editID" value="" />
								<input type="submit" name="submitExport" class="inputButton0" value="Gruppen-Auftr&auml;ge anzeigen" />
							</td>
						</tr>
					</table>
					<input type="hidden" name="submitExportForm" id="submitExportForm" value="1" />
					</form>
				</div>

				<div class="contentDisplay">
					<?php
						if($_POST["createDocumentProvision"] != '') {
							if($showPdfDownloadLink) {
								// echo '<p class="dataTableContent"><b>DOWNLOAD-LINK: </b><a href="'.DIRECTORY_CREATED_DOCUMENTS_GROUPS . $thisCreatedPdfName.'" target="_blank">'.$thisCreatedPdfName.'</a></p>';
								echo '<p class="dataTableContent"><b>DOWNLOAD-LINK: </b><img src="layout/icons/iconPDF.gif" height="16" width="16" alt="" /> <a href="?downloadFile=' . $_POST["thisCreatedPdfName"].'">'.utf8_decode($_POST["thisCreatedPdfName"]).'</a></p>';
								#echo '<p class="dataTableContent"><b>DOWNLOAD-LINK: </b><img src="layout/icons/iconEXCEL.gif" height="16" width="16" alt="" /> <a href="?downloadFile=' . $_POST["thisCreatedCsvName"].'">'.utf8_decode($_POST["thisCreatedCsvName"]).'</a></p>';
							}
							echo '<hr />';
							// $_POST["editID"] = "";
						}
					?>
					<?php
						if($_POST["submitExportForm"] == 1 && $_POST["exportOrdersGroupID"] != "" && $_POST["submitFormProvision"] == '') {
							echo '<table cellpadding="0" cellspacing="0" width="">';
							echo '<tr>';
							echo '<td><b>Gruppen-Name: </b></td>';
							echo '<td>' . $arrCustomerGroupDatas[$_POST["exportOrdersGroupID"]]["customerGroupsName"] . '</td>';
							echo '</tr>';
							echo '</table>';

							echo '<hr />';

							// BOF CREATE SQL FOR PROVISIONS FROM INVOICES DEPENDING ON SALESMAN AREAS

							$sql_basic = "
										SELECT
											`tempTable`.`groupsProvisionsDetailsID`,
											`tempTable`.`groupsProvisionsDetailsProvisionsID`,
											`tempTable`.`groupsProvisionsDetailsDocumentsDetailID`,
											`tempTable`.`groupsProvisionsDetailsDocumentNumber`,
											`tempTable`.`groupsProvisionsDetailsProductOrderDate`,
											`tempTable`.`groupsProvisionsDetailsProductAmount`,
											`tempTable`.`groupsProvisionsDetailsProductNumber`,
											`tempTable`.`groupsProvisionsDetailsProductName`,
											`tempTable`.`groupsProvisionsDetailsProductRecipientCustomerNumber`,
											`tempTable`.`groupsProvisionsDetailsProductRecipientName`,
											`tempTable`.`groupsProvisionsDetailsProductRecipientZipcode`,
											`tempTable`.`groupsProvisionsDetailsProductRecipientCity`,
											`tempTable`.`groupsProvisionsDetailsProductVkPreis`,
											`tempTable`.`groupsProvisionsDetailsProductProvisionPrice`,

											`tempTable`.`orderDocumentsID`,
											`tempTable`.`orderDocumentsNumber`,
											`tempTable`.`orderDocumentsType`,
											`tempTable`.`orderDocumentsOrdersIDs`,
											`tempTable`.`orderDocumentsCustomerNumber`,
											`tempTable`.`orderDocumentsCustomersOrderNumber`,
											`tempTable`.`orderDocumentsProcessingDate`,
											`tempTable`.`orderDocumentsDocumentDate`,
											`tempTable`.`orderDocumentsOrderDate`,
											`tempTable`.`orderDocumentsInvoiceDate`,
											`tempTable`.`orderDocumentsCreditDate`,
											`tempTable`.`orderDocumentsDeliveryDate`,
											`tempTable`.`orderDocumentsBindingDate`,
											`tempTable`.`orderDocumentsSalesman`,
											`tempTable`.`orderDocumentsKommission`,
											`tempTable`.`orderDocumentsAddressCompany`,
											`tempTable`.`orderDocumentsAddressName`,
											`tempTable`.`orderDocumentsAddressStreet`,
											`tempTable`.`orderDocumentsAddressStreetNumber`,
											`tempTable`.`orderDocumentsAddressZipcode`,
											`tempTable`.`orderDocumentsAddressCity`,
											`tempTable`.`orderDocumentsAddressCountry`,
											`tempTable`.`orderDocumentsAddressMail`,
											`tempTable`.`orderDocumentsSumPrice`,
											`tempTable`.`orderDocumentsDiscount`,
											`tempTable`.`orderDocumentsDiscountType`,
											`tempTable`.`orderDocumentsDiscountPercent`,
											`tempTable`.`orderDocumentsMwst`,
											`tempTable`.`orderDocumentsMwstPrice`,
											`tempTable`.`orderDocumentsShippingCosts`,
											`tempTable`.`orderDocumentsPackagingCosts`,
											`tempTable`.`orderDocumentsCashOnDelivery`,
											`tempTable`.`orderDocumentsTotalPrice`,
											`tempTable`.`orderDocumentsBankAccount`,
											`tempTable`.`orderDocumentsPaymentCondition`,
											`tempTable`.`orderDocumentsPaymentType`,
											`tempTable`.`orderDocumentsSkonto`,
											`tempTable`.`orderDocumentsDeliveryType`,
											`tempTable`.`orderDocumentsDeliveryTypeNumber`,
											`tempTable`.`orderDocumentsSubject`,
											`tempTable`.`orderDocumentsContent`,
											`tempTable`.`orderDocumentsDocumentPath`,
											`tempTable`.`orderDocumentsTimestamp`,
											`tempTable`.`orderDocumentsStatus`,
											`tempTable`.`orderDocumentsSendMail`,

											`tempTable`.`orderDocumentDetailOrderID`,
											`tempTable`.`orderDocumentDetailDocumentID`,
											`tempTable`.`orderDocumentDetailID`,
											`tempTable`.`orderDocumentDetailOrderType`,
											`tempTable`.`orderDocumentDetailProductNumber`,
											`tempTable`.`orderDocumentDetailProductName`,
											`tempTable`.`orderDocumentDetailProductQuantity`,
											`tempTable`.`orderDocumentDetailProductSinglePrice`,
											`tempTable`.`orderDocumentDetailPosType`,
											`tempTable`.`orderDocumentDetailProductKategorieID`,

											`tempTable`.`documentsToDocumentsCreatedDocumentNumber`,

											`tempTable`.`concatGroup`

										FROM (
												SELECT

													`" . TABLE_CUSTOMERS. "`.`customersID`,
													`" . TABLE_CUSTOMERS. "`.`customersKundennummer`,
													`" . TABLE_CUSTOMERS. "`.`customersFirmenname`,
													`" . TABLE_CUSTOMERS. "`.`customersFirmennameZusatz`,
													`" . TABLE_CUSTOMERS. "`.`customersFirmenInhaberVorname`,
													`" . TABLE_CUSTOMERS. "`.`customersFirmenInhaberNachname`,
													`" . TABLE_CUSTOMERS. "`.`customersFirmenInhaberAnrede`,
													`" . TABLE_CUSTOMERS. "`.`customersFirmenInhaber2Vorname`,
													`" . TABLE_CUSTOMERS. "`.`customersFirmenInhaber2Nachname`,
													`" . TABLE_CUSTOMERS. "`.`customersFirmenInhaber2Anrede`,
													`" . TABLE_CUSTOMERS. "`.`customersAnsprechpartner1Vorname`,
													`" . TABLE_CUSTOMERS. "`.`customersAnsprechpartner1Nachname`,
													`" . TABLE_CUSTOMERS. "`.`customersAnsprechpartner1Anrede`,
													`" . TABLE_CUSTOMERS. "`.`customersAnsprechpartner2Vorname`,
													`" . TABLE_CUSTOMERS. "`.`customersAnsprechpartner2Nachname`,
													`" . TABLE_CUSTOMERS. "`.`customersAnsprechpartner2Anrede`,
													`" . TABLE_CUSTOMERS. "`.`customersTelefon1`,
													`" . TABLE_CUSTOMERS. "`.`customersTelefon2`,
													`" . TABLE_CUSTOMERS. "`.`customersMobil1`,
													`" . TABLE_CUSTOMERS. "`.`customersMobil2`,
													`" . TABLE_CUSTOMERS. "`.`customersFax1`,
													`" . TABLE_CUSTOMERS. "`.`customersFax2`,
													`" . TABLE_CUSTOMERS. "`.`customersMail1`,
													`" . TABLE_CUSTOMERS. "`.`customersMail2`,
													`" . TABLE_CUSTOMERS. "`.`customersHomepage`,
													`" . TABLE_CUSTOMERS. "`.`customersCompanyStrasse`,
													`" . TABLE_CUSTOMERS. "`.`customersCompanyHausnummer`,
													`" . TABLE_CUSTOMERS. "`.`customersCompanyCountry`,
													`" . TABLE_CUSTOMERS. "`.`customersCompanyPLZ`,
													`" . TABLE_CUSTOMERS. "`.`customersCompanyOrt`,
													`" . TABLE_CUSTOMERS. "`.`customersLieferadresseFirmenname`,
													`" . TABLE_CUSTOMERS. "`.`customersLieferadresseStrasse`,
													`" . TABLE_CUSTOMERS. "`.`customersLieferadresseHausnummer`,
													`" . TABLE_CUSTOMERS. "`.`customersLieferadressePLZ`,
													`" . TABLE_CUSTOMERS. "`.`customersLieferadresseOrt`,
													`" . TABLE_CUSTOMERS. "`.`customersLieferadresseLand`,
													`" . TABLE_CUSTOMERS. "`.`customersRechnungsadresseFirmenname`,
													`" . TABLE_CUSTOMERS. "`.`customersRechnungsadresseStrasse`,
													`" . TABLE_CUSTOMERS. "`.`customersRechnungsadresseHausnummer`,
													`" . TABLE_CUSTOMERS. "`.`customersRechnungsadressePLZ`,
													`" . TABLE_CUSTOMERS. "`.`customersRechnungsadresseOrt`,
													`" . TABLE_CUSTOMERS. "`.`customersRechnungsadresseLand`,
													`" . TABLE_CUSTOMERS. "`.`customersBankName`,
													`" . TABLE_CUSTOMERS. "`.`customersKontoinhaber`,
													`" . TABLE_CUSTOMERS. "`.`customersBankKontonummer`,
													`" . TABLE_CUSTOMERS. "`.`customersBankLeitzahl`,
													`" . TABLE_CUSTOMERS. "`.`customersBankIBAN`,
													`" . TABLE_CUSTOMERS. "`.`customersBankBIC`,
													`" . TABLE_CUSTOMERS. "`.`customersBezahlart`,
													`" . TABLE_CUSTOMERS. "`.`customersZahlungskondition`,
													`" . TABLE_CUSTOMERS. "`.`customersRabatt`,
													`" . TABLE_CUSTOMERS. "`.`customersUseProductMwst`,
													`" . TABLE_CUSTOMERS. "`.`customersUseProductDiscount`,
													`" . TABLE_CUSTOMERS. "`.`customersUstID`,
													`" . TABLE_CUSTOMERS. "`.`customersVertreterID`,
													`" . TABLE_CUSTOMERS. "`.`customersVertreterName`,
													`" . TABLE_CUSTOMERS. "`.`customersUseSalesmanDeliveryAdress`,
													`" . TABLE_CUSTOMERS. "`.`customersUseSalesmanInvoiceAdress`,
													`" . TABLE_CUSTOMERS. "`.`customersTyp`,
													`" . TABLE_CUSTOMERS. "`.`customersGruppe`,
													`" . TABLE_CUSTOMERS. "`.`customersNotiz`,
													`" . TABLE_CUSTOMERS. "`.`customersUserID`,
													`" . TABLE_CUSTOMERS. "`.`customersTimeCreated`,
													`" . TABLE_CUSTOMERS. "`.`customersActive`,
													`" . TABLE_CUSTOMERS. "`.`customersDatasUpdated`,
													`" . TABLE_CUSTOMERS. "`.`customersTaxAccountID`,

													`" . TABLE_GROUPS_PROVISIONS_DETAILS . "`.`groupsProvisionsDetailsID`,
													`" . TABLE_GROUPS_PROVISIONS_DETAILS . "`.`groupsProvisionsDetailsProvisionsID`,
													`" . TABLE_GROUPS_PROVISIONS_DETAILS . "`.`groupsProvisionsDetailsDocumentsDetailID`,
													`" . TABLE_GROUPS_PROVISIONS_DETAILS . "`.`groupsProvisionsDetailsDocumentNumber`,
													`" . TABLE_GROUPS_PROVISIONS_DETAILS . "`.`groupsProvisionsDetailsProductOrderDate`,
													`" . TABLE_GROUPS_PROVISIONS_DETAILS . "`.`groupsProvisionsDetailsProductAmount`,
													`" . TABLE_GROUPS_PROVISIONS_DETAILS . "`.`groupsProvisionsDetailsProductNumber`,
													`" . TABLE_GROUPS_PROVISIONS_DETAILS . "`.`groupsProvisionsDetailsProductName`,
													`" . TABLE_GROUPS_PROVISIONS_DETAILS . "`.`groupsProvisionsDetailsProductRecipientCustomerNumber`,
													`" . TABLE_GROUPS_PROVISIONS_DETAILS . "`.`groupsProvisionsDetailsProductRecipientName`,
													`" . TABLE_GROUPS_PROVISIONS_DETAILS . "`.`groupsProvisionsDetailsProductRecipientZipcode`,
													`" . TABLE_GROUPS_PROVISIONS_DETAILS . "`.`groupsProvisionsDetailsProductRecipientCity`,
													`" . TABLE_GROUPS_PROVISIONS_DETAILS . "`.`groupsProvisionsDetailsProductVkPreis`,
													`" . TABLE_GROUPS_PROVISIONS_DETAILS . "`.`groupsProvisionsDetailsProductProvisionPrice`,

													`" . TABLE_ORDER_CONFIRMATIONS . "`.`orderDocumentsID`,
													`" . TABLE_ORDER_CONFIRMATIONS . "`.`orderDocumentsNumber`,
													`" . TABLE_ORDER_CONFIRMATIONS . "`.`orderDocumentsType`,
													`" . TABLE_ORDER_CONFIRMATIONS . "`.`orderDocumentsOrdersIDs`,
													`" . TABLE_ORDER_CONFIRMATIONS . "`.`orderDocumentsCustomerNumber`,
													`" . TABLE_ORDER_CONFIRMATIONS . "`.`orderDocumentsCustomersOrderNumber`,
													`" . TABLE_ORDER_CONFIRMATIONS . "`.`orderDocumentsProcessingDate`,
													`" . TABLE_ORDER_CONFIRMATIONS . "`.`orderDocumentsDocumentDate`,
													`" . TABLE_ORDER_CONFIRMATIONS . "`.`orderDocumentsOrderDate`,
													`" . TABLE_ORDER_CONFIRMATIONS . "`.`orderDocumentsInvoiceDate`,
													`" . TABLE_ORDER_CONFIRMATIONS . "`.`orderDocumentsCreditDate`,
													`" . TABLE_ORDER_CONFIRMATIONS . "`.`orderDocumentsDeliveryDate`,
													`" . TABLE_ORDER_CONFIRMATIONS . "`.`orderDocumentsBindingDate`,
													`" . TABLE_ORDER_CONFIRMATIONS . "`.`orderDocumentsSalesman`,
													`" . TABLE_ORDER_CONFIRMATIONS . "`.`orderDocumentsKommission`,
													`" . TABLE_ORDER_CONFIRMATIONS . "`.`orderDocumentsAddressCompany`,
													`" . TABLE_ORDER_CONFIRMATIONS . "`.`orderDocumentsAddressName`,
													`" . TABLE_ORDER_CONFIRMATIONS . "`.`orderDocumentsAddressStreet`,
													`" . TABLE_ORDER_CONFIRMATIONS . "`.`orderDocumentsAddressStreetNumber`,
													`" . TABLE_ORDER_CONFIRMATIONS . "`.`orderDocumentsAddressZipcode`,
													`" . TABLE_ORDER_CONFIRMATIONS . "`.`orderDocumentsAddressCity`,
													`" . TABLE_ORDER_CONFIRMATIONS . "`.`orderDocumentsAddressCountry`,
													`" . TABLE_ORDER_CONFIRMATIONS . "`.`orderDocumentsAddressMail`,
													`" . TABLE_ORDER_CONFIRMATIONS . "`.`orderDocumentsSumPrice`,
													`" . TABLE_ORDER_CONFIRMATIONS . "`.`orderDocumentsDiscount`,
													`" . TABLE_ORDER_CONFIRMATIONS . "`.`orderDocumentsDiscountType`,
													`" . TABLE_ORDER_CONFIRMATIONS . "`.`orderDocumentsDiscountPercent`,
													`" . TABLE_ORDER_CONFIRMATIONS . "`.`orderDocumentsMwst`,
													`" . TABLE_ORDER_CONFIRMATIONS . "`.`orderDocumentsMwstPrice`,
													`" . TABLE_ORDER_CONFIRMATIONS . "`.`orderDocumentsShippingCosts`,
													`" . TABLE_ORDER_CONFIRMATIONS . "`.`orderDocumentsPackagingCosts`,
													`" . TABLE_ORDER_CONFIRMATIONS . "`.`orderDocumentsCashOnDelivery`,
													`" . TABLE_ORDER_CONFIRMATIONS . "`.`orderDocumentsTotalPrice`,
													`" . TABLE_ORDER_CONFIRMATIONS . "`.`orderDocumentsBankAccount`,
													`" . TABLE_ORDER_CONFIRMATIONS . "`.`orderDocumentsPaymentCondition`,
													`" . TABLE_ORDER_CONFIRMATIONS . "`.`orderDocumentsPaymentType`,
													`" . TABLE_ORDER_CONFIRMATIONS . "`.`orderDocumentsSkonto`,
													`" . TABLE_ORDER_CONFIRMATIONS . "`.`orderDocumentsDeliveryType`,
													`" . TABLE_ORDER_CONFIRMATIONS . "`.`orderDocumentsDeliveryTypeNumber`,
													`" . TABLE_ORDER_CONFIRMATIONS . "`.`orderDocumentsSubject`,
													`" . TABLE_ORDER_CONFIRMATIONS . "`.`orderDocumentsContent`,
													`" . TABLE_ORDER_CONFIRMATIONS . "`.`orderDocumentsDocumentPath`,
													`" . TABLE_ORDER_CONFIRMATIONS . "`.`orderDocumentsTimestamp`,
													`" . TABLE_ORDER_CONFIRMATIONS . "`.`orderDocumentsStatus`,
													`" . TABLE_ORDER_CONFIRMATIONS . "`.`orderDocumentsSendMail`,

													`" . TABLE_ORDER_CONFIRMATIONS_DETAILS . "`.`orderDocumentDetailID`,
													`" . TABLE_ORDER_CONFIRMATIONS_DETAILS . "`.`orderDocumentDetailDocumentID`,
													`" . TABLE_ORDER_CONFIRMATIONS_DETAILS . "`.`orderDocumentDetailOrderID`,
													`" . TABLE_ORDER_CONFIRMATIONS_DETAILS . "`.`orderDocumentDetailOrderType`,
													`" . TABLE_ORDER_CONFIRMATIONS_DETAILS . "`.`orderDocumentDetailProductNumber`,
													`" . TABLE_ORDER_CONFIRMATIONS_DETAILS . "`.`orderDocumentDetailProductName`,
													`" . TABLE_ORDER_CONFIRMATIONS_DETAILS . "`.`orderDocumentDetailProductKategorieID`,
													`" . TABLE_ORDER_CONFIRMATIONS_DETAILS . "`.`orderDocumentDetailProductQuantity`,
													`" . TABLE_ORDER_CONFIRMATIONS_DETAILS . "`.`orderDocumentDetailProductSinglePrice`,
													`" . TABLE_ORDER_CONFIRMATIONS_DETAILS . "`.`orderDocumentDetailPosType`,
													`" . TABLE_ORDER_CONFIRMATIONS_DETAILS . "`.`orderDocumentDetailGroupingID`,

													'' AS `documentsToDocumentsCreatedDocumentNumber`,

													CONCAT(`" . TABLE_ORDER_CONFIRMATIONS . "`.`orderDocumentsNumber`, '-', `" . TABLE_ORDER_CONFIRMATIONS_DETAILS . "`.`orderDocumentDetailOrderID`, '-', `" . TABLE_ORDER_CONFIRMATIONS_DETAILS . "`.`orderDocumentDetailID`) AS `concatGroup`

												FROM `" . TABLE_CUSTOMERS. "`

												INNER JOIN `" . TABLE_ORDER_CONFIRMATIONS . "`
												ON(`" . TABLE_CUSTOMERS. "`.`customersKundennummer` = `" . TABLE_ORDER_CONFIRMATIONS . "`.`orderDocumentsCustomerNumber`)

												LEFT JOIN `" . TABLE_ORDER_CONFIRMATIONS_DETAILS . "`
												ON(`" . TABLE_ORDER_CONFIRMATIONS . "`.`orderDocumentsID` = `" . TABLE_ORDER_CONFIRMATIONS_DETAILS . "`.`orderDocumentDetailDocumentID`)

												LEFT JOIN `" . TABLE_GROUPS_PROVISIONS_DETAILS . "`
													ON(
													`" . TABLE_ORDER_CONFIRMATIONS . "`.`orderDocumentsNumber` = `" . TABLE_GROUPS_PROVISIONS_DETAILS . "`.`groupsProvisionsDetailsDocumentNumber`
													AND
													`" . TABLE_ORDER_CONFIRMATIONS_DETAILS . "`.`orderDocumentDetailID` = `" . TABLE_GROUPS_PROVISIONS_DETAILS . "`.`groupsProvisionsDetailsDocumentsDetailID`
												)

												WHERE
													(
														`" . TABLE_CUSTOMERS. "`.`customersGruppe` = '" . $_POST["exportOrdersGroupID"] . "'
														OR `" . TABLE_CUSTOMERS. "`.`customersGruppe` LIKE '" . $_POST["exportOrdersGroupID"] . ";%'
														OR `" . TABLE_CUSTOMERS. "`.`customersGruppe` LIKE '%;" . $_POST["exportOrdersGroupID"] . "'
														OR `" . TABLE_CUSTOMERS. "`.`customersGruppe` LIKE '%;" . $_POST["exportOrdersGroupID"] . ";%'
													)
													AND `" . TABLE_ORDER_CONFIRMATIONS_DETAILS . "`.`orderDocumentDetailPosType` = 'product'

												HAVING (
													`orderDocumentsProcessingDate` BETWEEN '{###DATE_EXPORT_START###}' AND '{###DATE_EXPORT_END###}'
												)

												UNION

												SELECT

												`" . TABLE_CUSTOMERS. "`.`customersID`,
												`" . TABLE_CUSTOMERS. "`.`customersKundennummer`,
												`" . TABLE_CUSTOMERS. "`.`customersFirmenname`,
												`" . TABLE_CUSTOMERS. "`.`customersFirmennameZusatz`,
												`" . TABLE_CUSTOMERS. "`.`customersFirmenInhaberVorname`,
												`" . TABLE_CUSTOMERS. "`.`customersFirmenInhaberNachname`,
												`" . TABLE_CUSTOMERS. "`.`customersFirmenInhaberAnrede`,
												`" . TABLE_CUSTOMERS. "`.`customersFirmenInhaber2Vorname`,
												`" . TABLE_CUSTOMERS. "`.`customersFirmenInhaber2Nachname`,
												`" . TABLE_CUSTOMERS. "`.`customersFirmenInhaber2Anrede`,
												`" . TABLE_CUSTOMERS. "`.`customersAnsprechpartner1Vorname`,
												`" . TABLE_CUSTOMERS. "`.`customersAnsprechpartner1Nachname`,
												`" . TABLE_CUSTOMERS. "`.`customersAnsprechpartner1Anrede`,
												`" . TABLE_CUSTOMERS. "`.`customersAnsprechpartner2Vorname`,
												`" . TABLE_CUSTOMERS. "`.`customersAnsprechpartner2Nachname`,
												`" . TABLE_CUSTOMERS. "`.`customersAnsprechpartner2Anrede`,
												`" . TABLE_CUSTOMERS. "`.`customersTelefon1`,
												`" . TABLE_CUSTOMERS. "`.`customersTelefon2`,
												`" . TABLE_CUSTOMERS. "`.`customersMobil1`,
												`" . TABLE_CUSTOMERS. "`.`customersMobil2`,
												`" . TABLE_CUSTOMERS. "`.`customersFax1`,
												`" . TABLE_CUSTOMERS. "`.`customersFax2`,
												`" . TABLE_CUSTOMERS. "`.`customersMail1`,
												`" . TABLE_CUSTOMERS. "`.`customersMail2`,
												`" . TABLE_CUSTOMERS. "`.`customersHomepage`,
												`" . TABLE_CUSTOMERS. "`.`customersCompanyStrasse`,
												`" . TABLE_CUSTOMERS. "`.`customersCompanyHausnummer`,
												`" . TABLE_CUSTOMERS. "`.`customersCompanyCountry`,
												`" . TABLE_CUSTOMERS. "`.`customersCompanyPLZ`,
												`" . TABLE_CUSTOMERS. "`.`customersCompanyOrt`,
												`" . TABLE_CUSTOMERS. "`.`customersLieferadresseFirmenname`,
												`" . TABLE_CUSTOMERS. "`.`customersLieferadresseStrasse`,
												`" . TABLE_CUSTOMERS. "`.`customersLieferadresseHausnummer`,
												`" . TABLE_CUSTOMERS. "`.`customersLieferadressePLZ`,
												`" . TABLE_CUSTOMERS. "`.`customersLieferadresseOrt`,
												`" . TABLE_CUSTOMERS. "`.`customersLieferadresseLand`,
												`" . TABLE_CUSTOMERS. "`.`customersRechnungsadresseFirmenname`,
												`" . TABLE_CUSTOMERS. "`.`customersRechnungsadresseStrasse`,
												`" . TABLE_CUSTOMERS. "`.`customersRechnungsadresseHausnummer`,
												`" . TABLE_CUSTOMERS. "`.`customersRechnungsadressePLZ`,
												`" . TABLE_CUSTOMERS. "`.`customersRechnungsadresseOrt`,
												`" . TABLE_CUSTOMERS. "`.`customersRechnungsadresseLand`,
												`" . TABLE_CUSTOMERS. "`.`customersBankName`,
												`" . TABLE_CUSTOMERS. "`.`customersKontoinhaber`,
												`" . TABLE_CUSTOMERS. "`.`customersBankKontonummer`,
												`" . TABLE_CUSTOMERS. "`.`customersBankLeitzahl`,
												`" . TABLE_CUSTOMERS. "`.`customersBankIBAN`,
												`" . TABLE_CUSTOMERS. "`.`customersBankBIC`,
												`" . TABLE_CUSTOMERS. "`.`customersBezahlart`,
												`" . TABLE_CUSTOMERS. "`.`customersZahlungskondition`,
												`" . TABLE_CUSTOMERS. "`.`customersRabatt`,
												`" . TABLE_CUSTOMERS. "`.`customersUseProductMwst`,
												`" . TABLE_CUSTOMERS. "`.`customersUseProductDiscount`,
												`" . TABLE_CUSTOMERS. "`.`customersUstID`,
												`" . TABLE_CUSTOMERS. "`.`customersVertreterID`,
												`" . TABLE_CUSTOMERS. "`.`customersVertreterName`,
												`" . TABLE_CUSTOMERS. "`.`customersUseSalesmanDeliveryAdress`,
												`" . TABLE_CUSTOMERS. "`.`customersUseSalesmanInvoiceAdress`,
												`" . TABLE_CUSTOMERS. "`.`customersTyp`,
												`" . TABLE_CUSTOMERS. "`.`customersGruppe`,
												`" . TABLE_CUSTOMERS. "`.`customersNotiz`,
												`" . TABLE_CUSTOMERS. "`.`customersUserID`,
												`" . TABLE_CUSTOMERS. "`.`customersTimeCreated`,
												`" . TABLE_CUSTOMERS. "`.`customersActive`,
												`" . TABLE_CUSTOMERS. "`.`customersDatasUpdated`,
												`" . TABLE_CUSTOMERS. "`.`customersTaxAccountID`,

												`" . TABLE_GROUPS_PROVISIONS_DETAILS . "`.`groupsProvisionsDetailsID`,
												`" . TABLE_GROUPS_PROVISIONS_DETAILS . "`.`groupsProvisionsDetailsProvisionsID`,
												`" . TABLE_GROUPS_PROVISIONS_DETAILS . "`.`groupsProvisionsDetailsDocumentsDetailID`,
												`" . TABLE_GROUPS_PROVISIONS_DETAILS . "`.`groupsProvisionsDetailsDocumentNumber`,
												`" . TABLE_GROUPS_PROVISIONS_DETAILS . "`.`groupsProvisionsDetailsProductOrderDate`,
												`" . TABLE_GROUPS_PROVISIONS_DETAILS . "`.`groupsProvisionsDetailsProductAmount`,
												`" . TABLE_GROUPS_PROVISIONS_DETAILS . "`.`groupsProvisionsDetailsProductNumber`,
												`" . TABLE_GROUPS_PROVISIONS_DETAILS . "`.`groupsProvisionsDetailsProductName`,
												`" . TABLE_GROUPS_PROVISIONS_DETAILS . "`.`groupsProvisionsDetailsProductRecipientCustomerNumber`,
												`" . TABLE_GROUPS_PROVISIONS_DETAILS . "`.`groupsProvisionsDetailsProductRecipientName`,
												`" . TABLE_GROUPS_PROVISIONS_DETAILS . "`.`groupsProvisionsDetailsProductRecipientZipcode`,
												`" . TABLE_GROUPS_PROVISIONS_DETAILS . "`.`groupsProvisionsDetailsProductRecipientCity`,
												`" . TABLE_GROUPS_PROVISIONS_DETAILS . "`.`groupsProvisionsDetailsProductVkPreis`,
												`" . TABLE_GROUPS_PROVISIONS_DETAILS . "`.`groupsProvisionsDetailsProductProvisionPrice`,

												`" . TABLE_ORDER_INVOICES . "`.`orderDocumentsID`,
												`" . TABLE_ORDER_INVOICES . "`.`orderDocumentsNumber`,
												`" . TABLE_ORDER_INVOICES . "`.`orderDocumentsType`,
												`" . TABLE_ORDER_INVOICES . "`.`orderDocumentsOrdersIDs`,
												`" . TABLE_ORDER_INVOICES . "`.`orderDocumentsCustomerNumber`,
												`" . TABLE_ORDER_INVOICES . "`.`orderDocumentsCustomersOrderNumber`,
												`" . TABLE_ORDER_INVOICES . "`.`orderDocumentsProcessingDate`,
												`" . TABLE_ORDER_INVOICES . "`.`orderDocumentsDocumentDate`,
												`" . TABLE_ORDER_INVOICES . "`.`orderDocumentsOrderDate`,
												`" . TABLE_ORDER_INVOICES . "`.`orderDocumentsInvoiceDate`,
												`" . TABLE_ORDER_INVOICES . "`.`orderDocumentsCreditDate`,
												`" . TABLE_ORDER_INVOICES . "`.`orderDocumentsDeliveryDate`,
												`" . TABLE_ORDER_INVOICES . "`.`orderDocumentsBindingDate`,
												`" . TABLE_ORDER_INVOICES . "`.`orderDocumentsSalesman`,
												`" . TABLE_ORDER_INVOICES . "`.`orderDocumentsKommission`,
												`" . TABLE_ORDER_INVOICES . "`.`orderDocumentsAddressCompany`,
												`" . TABLE_ORDER_INVOICES . "`.`orderDocumentsAddressName`,
												`" . TABLE_ORDER_INVOICES . "`.`orderDocumentsAddressStreet`,
												`" . TABLE_ORDER_INVOICES . "`.`orderDocumentsAddressStreetNumber`,
												`" . TABLE_ORDER_INVOICES . "`.`orderDocumentsAddressZipcode`,
												`" . TABLE_ORDER_INVOICES . "`.`orderDocumentsAddressCity`,
												`" . TABLE_ORDER_INVOICES . "`.`orderDocumentsAddressCountry`,
												`" . TABLE_ORDER_INVOICES . "`.`orderDocumentsAddressMail`,
												`" . TABLE_ORDER_INVOICES . "`.`orderDocumentsSumPrice`,
												`" . TABLE_ORDER_INVOICES . "`.`orderDocumentsDiscount`,
												`" . TABLE_ORDER_INVOICES . "`.`orderDocumentsDiscountType`,
												`" . TABLE_ORDER_INVOICES . "`.`orderDocumentsDiscountPercent`,
												`" . TABLE_ORDER_INVOICES . "`.`orderDocumentsMwst`,
												`" . TABLE_ORDER_INVOICES . "`.`orderDocumentsMwstPrice`,
												`" . TABLE_ORDER_INVOICES . "`.`orderDocumentsShippingCosts`,
												`" . TABLE_ORDER_INVOICES . "`.`orderDocumentsPackagingCosts`,
												`" . TABLE_ORDER_INVOICES . "`.`orderDocumentsCashOnDelivery`,
												`" . TABLE_ORDER_INVOICES . "`.`orderDocumentsTotalPrice`,
												`" . TABLE_ORDER_INVOICES . "`.`orderDocumentsBankAccount`,
												`" . TABLE_ORDER_INVOICES . "`.`orderDocumentsPaymentCondition`,
												`" . TABLE_ORDER_INVOICES . "`.`orderDocumentsPaymentType`,
												`" . TABLE_ORDER_INVOICES . "`.`orderDocumentsSkonto`,
												`" . TABLE_ORDER_INVOICES . "`.`orderDocumentsDeliveryType`,
												`" . TABLE_ORDER_INVOICES . "`.`orderDocumentsDeliveryTypeNumber`,
												`" . TABLE_ORDER_INVOICES . "`.`orderDocumentsSubject`,
												`" . TABLE_ORDER_INVOICES . "`.`orderDocumentsContent`,
												`" . TABLE_ORDER_INVOICES . "`.`orderDocumentsDocumentPath`,
												`" . TABLE_ORDER_INVOICES . "`.`orderDocumentsTimestamp`,
												`" . TABLE_ORDER_INVOICES . "`.`orderDocumentsStatus`,
												`" . TABLE_ORDER_INVOICES . "`.`orderDocumentsSendMail`,

												`" . TABLE_ORDER_INVOICES_DETAILS . "`.`orderDocumentDetailID`,
												`" . TABLE_ORDER_INVOICES_DETAILS . "`.`orderDocumentDetailDocumentID`,
												`" . TABLE_ORDER_INVOICES_DETAILS . "`.`orderDocumentDetailOrderID`,
												`" . TABLE_ORDER_INVOICES_DETAILS . "`.`orderDocumentDetailOrderType`,
												`" . TABLE_ORDER_INVOICES_DETAILS . "`.`orderDocumentDetailProductNumber`,
												`" . TABLE_ORDER_INVOICES_DETAILS . "`.`orderDocumentDetailProductName`,
												`" . TABLE_ORDER_INVOICES_DETAILS . "`.`orderDocumentDetailProductKategorieID`,
												`" . TABLE_ORDER_INVOICES_DETAILS . "`.`orderDocumentDetailProductQuantity`,
												`" . TABLE_ORDER_INVOICES_DETAILS . "`.`orderDocumentDetailProductSinglePrice`,
												`" . TABLE_ORDER_INVOICES_DETAILS . "`.`orderDocumentDetailPosType`,
												`" . TABLE_ORDER_INVOICES_DETAILS . "`.`orderDocumentDetailGroupingID`,


												`" . TABLE_DOCUMENTS_TO_DOCUMENTS . "`.`documentsToDocumentsCreatedDocumentNumber`,

												CONCAT(`" . TABLE_ORDER_INVOICES . "`.`orderDocumentsNumber`, '-', `" . TABLE_ORDER_INVOICES_DETAILS . "`.`orderDocumentDetailOrderID`, '-', `" . TABLE_ORDER_INVOICES_DETAILS . "`.`orderDocumentDetailID`) AS `concatGroup`

												FROM `" . TABLE_CUSTOMERS. "`

												INNER JOIN `" . TABLE_ORDER_INVOICES . "`
												ON(`" . TABLE_CUSTOMERS. "`.`customersKundennummer` = `" . TABLE_ORDER_INVOICES . "`.`orderDocumentsCustomerNumber`)

												LEFT JOIN `" . TABLE_DOCUMENTS_TO_DOCUMENTS . "`
												ON(`" . TABLE_ORDER_INVOICES . "`.`orderDocumentsNumber` = `" . TABLE_DOCUMENTS_TO_DOCUMENTS . "`.`documentsToDocumentsCreatedDocumentNumber`)

												LEFT JOIN `" . TABLE_ORDER_INVOICES_DETAILS . "`
												ON(`" . TABLE_ORDER_INVOICES . "`.`orderDocumentsID` = `" . TABLE_ORDER_INVOICES_DETAILS . "`.`orderDocumentDetailDocumentID`)

												LEFT JOIN `" . TABLE_GROUPS_PROVISIONS_DETAILS . "`
												ON(
													`" . TABLE_ORDER_INVOICES . "`.`orderDocumentsID` = `" . TABLE_GROUPS_PROVISIONS_DETAILS . "`.`groupsProvisionsDetailsDocumentNumber`
													AND
													`" . TABLE_ORDER_INVOICES_DETAILS . "`.`orderDocumentDetailID` = `" . TABLE_GROUPS_PROVISIONS_DETAILS . "`.`groupsProvisionsDetailsDocumentsDetailID`
												)

												WHERE
													(
														`" . TABLE_CUSTOMERS. "`.`customersGruppe` = '" . $_POST["exportOrdersGroupID"] . "'
														OR `" . TABLE_CUSTOMERS. "`.`customersGruppe` LIKE '" . $_POST["exportOrdersGroupID"] . ";%'
														OR `" . TABLE_CUSTOMERS. "`.`customersGruppe` LIKE '%;" . $_POST["exportOrdersGroupID"] . "'
														OR `" . TABLE_CUSTOMERS. "`.`customersGruppe` LIKE '%;" . $_POST["exportOrdersGroupID"] . ";%'
													)
													AND `" . TABLE_ORDER_INVOICES_DETAILS . "`.`orderDocumentDetailPosType` = 'product'

												HAVING (
													`orderDocumentsProcessingDate` BETWEEN '{###DATE_EXPORT_START###}' AND '{###DATE_EXPORT_END###}'
													AND `documentsToDocumentsCreatedDocumentNumber` IS NULL
												)


											) AS `tempTable`

											GROUP BY `tempTable`.`concatGroup`


							";
$sql_order = "
									ORDER BY `orderDocumentsDocumentDate` ASC, `orderDocumentsNumber` ASC

							";

						// EOF CREATE SQL FOR PROVISIONS FROM INVOICES DEPENDING ON SALESMAN AREAS

					/*
					WHERE `" . TABLE_ORDER_CONFIRMATIONS . "`.`orderDocumentsProcessingDate`
											BETWEEN '" . formatDate($_POST["exportDateStart"], "store") . "' AND '" . formatDate($_POST["exportDateEnd"], "store")."'

					*/
							$sql = $sql_basic;
							$dateStart = formatDate($_POST["exportOrdersDateFrom"], "store");
							$dateEnd = formatDate($_POST["exportOrdersDateTo"], "store");
							$sql = preg_replace("/{###DATE_EXPORT_START###}/", $dateStart, $sql);
							$sql = preg_replace("/{###DATE_EXPORT_END###}/", $dateEnd, $sql);
							$sqlAdd = '';
							$_POST["displayUnExportedLastMonth"] = 0;
							if($_POST["displayUnExportedLastMonth"] == '1') {
								$sqlAdd = $sql_basic;
								$dateStartLastMonth = date("Y-m-d", mktime(0, 0, 0, (date("m", strtotime($dateStart)) - 1), 1, date("Y", strtotime($dateStart))));
								$dateEndLastMonth = date("Y-m-d", mktime(0, 0, 0, (date("m", strtotime($dateStart)) - 0), 0, date("Y", strtotime($dateStart))));
								$sqlAdd = preg_replace("/{###DATE_EXPORT_START###}/", $dateStartLastMonth, $sqlAdd);
								$sqlAdd = preg_replace("/{###DATE_EXPORT_END###}/", $dateEndLastMonth, $sqlAdd);
								$sql .= ' UNION ' . $sqlAdd . ' HAVING `tempTable`.`groupsProvisionsDetailsID` IS NULL ';
							}

							$sql .= $sql_order;

							$rs = $dbConnection->db_query($sql);

							$countTotalRows = $dbConnection->db_getMysqlNumRows($rs);

							$contentExportPDF = '';
							$contentExportCSV = '';

							if($countTotalRows > 0) {
								echo '<form name="formProvision" action="' . $_SERVER["PHP_SELF"]. '" method="post" enctype="multipart/form-data">';
								echo '<input type="hidden" name="exportOrdersGroupID" value="' . $_REQUEST["exportOrdersGroupID"] . '" />';
								$contentExportPDF .= '<table border="0" cellpadding="0" cellspacing="0" width="0" class="displayOrders" id="displayCardDatas" >';
								$contentExportPDF .= '<thead>';
								$contentExportPDF .= '<tr>';
								$contentExportPDF .= '<th>Pos</th>';
								$contentExportPDF .= '<!-- BOF DOCUMENT_NUMBER_ORDER -->';
								$contentExportPDF .= '<th>Auftragsnummer</th>';
								#$contentExportPDF .= '<th>Produktionsstatus</th>';
								$contentExportPDF .= '<!-- EOF DOCUMENT_NUMBER_ORDER -->';
								$contentExportPDF .= '<th>Datum</th>';
								$contentExportPDF .= '<th>K-Nr.</th>';
								$contentExportPDF .= '<th>Firma</th>';
								$contentExportPDF .= '<th>Ort</th>';
								$contentExportPDF .= '<th>Artikel</th>';
								$contentExportPDF .= '<th>Menge</th>';
								$contentExportPDF .= '<th>VK-Preis</th>';
								if($arrSalesmanDatas["groupsProvision"] > 0){
									$contentExportPDF .= '<th>Gesamt</th>';
								}
								else {
									$contentExportPDF .= '<th>Gutschrift</th>';
								}
								$contentExportPDF .= '</tr>';
								$contentExportPDF .= '</thead>';
								$contentExportPDF .= '<tbody>';


								$countRow = 0;
								$displayTotalSum = 0;
								$thisParsedUrl = parse_url($_SERVER["REQUEST_URI"]);
								while($ds = mysqli_fetch_assoc($rs)){
									if($countRow%2 == 0){ $rowClass = 'row0'; }
									else { $rowClass = 'row1'; }
									//  '.$rowClass.'
									$contentExportPDF .= '<tr class="displayCardDatasItemRows">';
									$contentExportPDF .= '<td style="text-align:right;">';
									$contentExportPDF .= '<b>' . ($countRow + 1) . '.</b>';
									$contentExportPDF .=  '<input type="hidden" name="arrProvisionValues['.$countRow.'][orderDocumentDetailID]" value="' . $ds["orderDocumentDetailID"] . '" />';
									$contentExportPDF .= '</td>';
									$contentExportPDF .= '<!-- BOF DOCUMENT_NUMBER_ORDER -->';
									$contentExportPDF .= '<td style="background-color:#FEFFAF; white-space:nowrap;">';
									$contentExportPDF .= '' . $ds["orderDocumentsNumber"] . '';
									#$contentExportPDF .= '<a href="' . PAGE_DOWNLOAD_DOCUMENTS . '?searchBoxFile=' . $ds["orderDocumentsNumber"] . '">' . '<b>' . $ds["orderDocumentsNumber"] . '</b>' . '</a>';
									#$contentExportPDF .= '<a href="' . PAGE_DOWNLOAD_DOCUMENTS . '?searchBoxFile= . ' . $ds["orderDocumentsNumber"] . '">' . '<img src="layout/icons/iconPDF.gif" width="16" height="16" title="Dokument &quot;' . $ds["orderDocumentsNumber"]  . '&quot; herunterladen" alt="Download" /></a>';
									$contentExportPDF .= '<a href="' . $thisParsedUrl["path"] . '?downloadFile=' . basename($ds["orderDocumentsDocumentPath"]) . '&thisDocumentType=' . $ds["orderDocumentsType"] . '">' . '<img src="layout/icons/icon' . getFileType(basename($ds["orderDocumentsDocumentPath"])) . '.gif" width="16" height="16" title="Dokument &quot;' . $ds["orderDocumentsNumber"] . '&quot; herunterladen" alt="Download" /></a>';
									$contentExportPDF .= '<input type="hidden" name="arrProvisionValues['.$countRow.'][orderDocumentsNumber]" value="' . $ds["orderDocumentsNumber"] . '" />';
									$contentExportPDF .= '</td>';
									#$contentExportPDF .= '<td>';
									#$contentExportPDF .= $arrPaymentStatusTypeDatas[$ds["orderDocumentsStatus"]]["paymentStatusTypesName"];
									#$contentExportPDF .= '</td>';
									$contentExportPDF .= '<!-- EOF DOCUMENT_NUMBER_ORDER -->';
									$contentExportPDF .= '<td>';
									if($ds["orderDocumentsOrderDate"] > 0) {
										$thisDate = $ds["orderDocumentsOrderDate"];
									}
									else {
										$thisDate = $ds["orderDocumentsProcessingDate"];
									}
									$thisDate = $ds["orderDocumentsProcessingDate"];
									$contentExportPDF .= formatDate($thisDate, 'display');
									$contentExportPDF .= '<input type="hidden" name="arrProvisionValues['.$countRow.'][orderDocumentsOrderDate]" value="' . $thisDate . '" />';
									$contentExportPDF .= '</td>';
									$contentExportPDF .= '<td style="background-color:#FEFFAF;">';
									#$contentExportPDF .= '<a href="' . PAGE_EDIT_CUSTOMER . '?editCustomerNumber=' . $ds["orderDocumentsCustomerNumber"]. '"><b title="Zu den Kundendaten">' . $ds["orderDocumentsCustomerNumber"]. '</b></a>';
									$contentExportPDF .= '' . $ds["orderDocumentsCustomerNumber"]. '';
									$contentExportPDF .= '<input type="hidden" name="arrProvisionValues['.$countRow.'][orderDocumentsCustomerNumber]" value="' . $ds["orderDocumentsCustomerNumber"] . '" />';
									$contentExportPDF .= '</td>';
									$contentExportPDF .= '<td>';
									$contentExportPDF .= wordwrap($ds["orderDocumentsAddressCompany"], 14, "<br />", false);
									$contentExportPDF .= '<input type="hidden" name="arrProvisionValues['.$countRow.'][orderDocumentsAddressCompany]" value="' . $ds["orderDocumentsAddressCompany"] . '" />';
									$contentExportPDF .= '</td>';
									$contentExportPDF .= '<td>';
									$contentExportPDF .= $ds["orderDocumentsAddressZipcode"]. ' ' . $ds["orderDocumentsAddressCity"];
									$contentExportPDF .= '<input type="hidden" name="arrProvisionValues['.$countRow.'][orderDocumentsAddressZipcode]" value="' . $ds["orderDocumentsAddressZipcode"] . '" />';
									$contentExportPDF .= '<input type="hidden" name="arrProvisionValues['.$countRow.'][orderDocumentsAddressCity]" value="' . $ds["orderDocumentsAddressCity"] . '" />';
									$contentExportPDF .= '</td>';
									$contentExportPDF .= '<td style="background-color:#FEFFAF;">';
									$contentExportPDF .= specialSubstr($ds["orderDocumentDetailProductName"], 36);
									$contentExportPDF .= '<input type="hidden" name="arrProvisionValues['.$countRow.'][orderDocumentDetailProductName]" value="' . specialSubstr($ds["orderDocumentDetailProductName"]) . '" />';
									$contentExportPDF .= '<input type="hidden" name="arrProvisionValues['.$countRow.'][orderDocumentDetailProductNumber]" value="' . $ds["orderDocumentDetailProductNumber"] . '" />';
									$contentExportPDF .= '</td>';
									$contentExportPDF .= '<td style="text-align:right;" class="cellQuantity">';
									$contentExportPDF .= '<b>' . $ds["orderDocumentDetailProductQuantity"] . '</b>';
									$contentExportPDF .= '<input type="hidden" name="arrProvisionValues['.$countRow.'][orderDocumentDetailProductQuantity]" value="' . $ds["orderDocumentDetailProductQuantity"] . '" />';
									$contentExportPDF .= '</td>';
									$contentExportPDF .= '<td style="text-align:right; white-space:nowrap;" class="cellVkprice">';
									$contentExportPDF .= '<!-- BOF VK_PRICE_'.$countRow.' -->';
										if($ds["orderDocumentDetailProductSinglePrice"] < 0) {
											$ds["orderDocumentDetailProductSinglePrice"] = 0;
										}
										$contentExportPDF .= '<input type="text" class="inputField_40" style="text-align:right;" name="arrProvisionValues['.$countRow.'][groupProvisionProductVkprice]" value="' . number_format($ds["orderDocumentDetailProductSinglePrice"], 2, ',' ,'') . '"/> EUR';
									$contentExportPDF .= '<!-- EOF VK_PRICE_'.$countRow.' -->';
									$contentExportPDF .= '</td>';
									$contentExportPDF .= '<td style="text-align:right; white-space:nowrap;background-color:#FEFFAF;" class="cellProvision">';

									if($ds["groupsProvisionsDetailsProductProvisionPrice"] == '' || $ds["groupsProvisionsDetailsProductProvisionPrice"] == null) {
										$ds["groupsProvisionsDetailsProductProvisionPrice"] == 0;
									}
									if($arrSalesmanDatas["groupsProvision"] > 0){
										// $thisProvision = $ds["orderDocumentDetailProductQuantity"] * ($ds["orderDocumentDetailProductSinglePrice"] * $arrSalesmanDatas["groupsProvision"]/100);
										$thisProvision = $ds["orderDocumentDetailProductQuantity"] * $ds["orderDocumentDetailProductSinglePrice"];
									}
									else {
										$thisProvision = 0;
										if(substr($ds["orderDocumentDetailProductKategorieID"], 0, 3) == '001') {
											$thisProvision = $ds["orderDocumentDetailProductQuantity"] * ($ds["orderDocumentDetailProductSinglePrice"] - KZH_EK_PRICE);
										}
									}
									if($ds["groupsProvisionsDetailsProductProvisionPrice"] > 0) {
										$thisProvision = $ds["groupsProvisionsDetailsProductProvisionPrice"];
									}

									if($thisProvision < 0) {
										$thisProvision = 0;
									}
									$displayTotalSum += $thisProvision;
									$contentExportPDF .= '<!-- BOF PROVISION_PRICE_'.$countRow.' -->';
									$contentExportPDF .= '<input type="text" class="inputField_50" style="text-align:right;" name="arrProvisionValues['.$countRow.'][groupProvisionValue]" value="' . number_format($thisProvision, 2, ',', '') . '"/>';
									$contentExportPDF .= '<!-- EOF PROVISION_PRICE_'.$countRow.' -->';
									$contentExportPDF .= ' EUR';
									$contentExportPDF .= '</td>';
									$contentExportPDF .= '</tr>';

									$contentExportPDF .= '<tr style="page-break-before:auto;" class="pagebreak">';
									$contentExportPDF .= '<td></td>';
									$contentExportPDF .= '<!-- BOF DOCUMENT_NUMBER_ORDER -->';
									$contentExportPDF .= '<td></td>';
									$contentExportPDF .= '<!-- EOF DOCUMENT_NUMBER_ORDER -->';
									$contentExportPDF .= '<td colspan="8"></td>';
									$contentExportPDF .= '</tr>';
									$countRow++;
								}

								$contentExportPDF .= '<tr>';
								$contentExportPDF .= '<td>&nbsp;</td>';
								$contentExportPDF .= '<!-- BOF DOCUMENT_NUMBER_ORDER -->';
								$contentExportPDF .= '<td>&nbsp;</td>';
								$contentExportPDF .= '<!-- EOF DOCUMENT_NUMBER_ORDER -->';
								$contentExportPDF .= '<td colspan="8">&nbsp;</td>';
								$contentExportPDF .= '</tr>';

								$contentExportPDF .= '<tr>';
								$contentExportPDF .= '<td>&nbsp;</td>';
								$contentExportPDF .= '<!-- BOF DOCUMENT_NUMBER_ORDER -->';
								$contentExportPDF .= '<td>&nbsp;</td>';
								$contentExportPDF .= '<!-- EOF DOCUMENT_NUMBER_ORDER -->';
								$contentExportPDF .= '<td colspan="5">&nbsp;</td>';
								$contentExportPDF .= '<td colspan="2"><b>Zwischensumme:</b></td>';
								$contentExportPDF .= '<td style="white-space:nowrap;text-align:right;"><!-- BOF SUM_VALUE --><b><span id="displayTotalSum">'.number_format($displayTotalSum, 2, ',', '') .'</span></b><!-- EOF SUM_VALUE --> EUR</td>';
								$contentExportPDF .= '</tr>';

								$valueProvision = 0;
								if($arrSalesmanDatas["groupsProvision"] > 0){
									$contentExportPDF .= '<tr>';
									$contentExportPDF .= '<td>&nbsp;</td>';
									$contentExportPDF .= '<!-- BOF DOCUMENT_NUMBER_ORDER -->';
									$contentExportPDF .= '<td>&nbsp;</td>';
									$contentExportPDF .= '<!-- EOF DOCUMENT_NUMBER_ORDER -->';
									$contentExportPDF .= '<td colspan="5">&nbsp;</td>';
									$contentExportPDF .= '<td colspan="2"><b>Gutschrift (' . $arrSalesmanDatas["groupsProvision"]. ' %):</b></td>';
									$valueProvision = $displayTotalSum * ($arrSalesmanDatas["groupsProvision"]/100);
									$contentExportPDF .= '<td style="white-space:nowrap;text-align:right;"><!-- BOF PROVISION_VALUE --><b><span id="displayProvision">'.number_format($valueProvision, 2, ',', '') .'</span></b><!-- EOF PROVISION_VALUE --> EUR</td>';
									$contentExportPDF .= '</tr>';
								}

								$contentExportPDF .= '<tr>';
								$contentExportPDF .= '<td>&nbsp;</td>';
								$contentExportPDF .= '<!-- BOF DOCUMENT_NUMBER_ORDER -->';
								$contentExportPDF .= '<td>&nbsp;</td>';
								$contentExportPDF .= '<!-- EOF DOCUMENT_NUMBER_ORDER -->';
								$contentExportPDF .= '<td colspan="5">&nbsp;</td>';
								$contentExportPDF .= '<td colspan="2"><b>MwSt. (' . MWST. ' %):</b></td>';
								$valueMwst = ($displayTotalSum + $valueProvision) * MWST/100;
								$contentExportPDF .= '<td style="white-space:nowrap;text-align:right;"><!-- BOF MWST_VALUE --><b><span id="displayMWST">'.number_format($valueMwst, 2, ',', '') .'</span></b><!-- EOF MWST_VALUE --> EUR</td>';
								$contentExportPDF .= '</tr>';

								$contentExportPDF .= '<tr>';
								$contentExportPDF .= '<td>&nbsp;</td>';
								$contentExportPDF .= '<!-- BOF DOCUMENT_NUMBER_ORDER -->';
								$contentExportPDF .= '<td>&nbsp;</td>';
								$contentExportPDF .= '<!-- EOF DOCUMENT_NUMBER_ORDER -->';
								$contentExportPDF .= '<td colspan="5">&nbsp;</td>';
								$contentExportPDF .= '<td colspan="2" id="sumline">&nbsp;</td>';
								$contentExportPDF .= '<td id="sumline"> </td>';
								$contentExportPDF .= '</tr>';

								$contentExportPDF .= '<tr>';
								$contentExportPDF .= '<td>&nbsp;</td>';
								$contentExportPDF .= '<!-- BOF DOCUMENT_NUMBER_ORDER -->';
								$contentExportPDF .= '<td>&nbsp;</td>';
								$contentExportPDF .= '<!-- EOF DOCUMENT_NUMBER_ORDER -->';
								$contentExportPDF .= '<td colspan="5">&nbsp;</td>';
								$contentExportPDF .= '<td colspan="2"><b>Gesamt:</b></td>';
								$contentExportPDF .= '<td style="white-space:nowrap;text-align:right;"><!-- BOF TOTAL_VALUE --><b><span id="displayTotal">'.number_format($displayTotalSum + $valueProvision + $valueMwst, 2, ',', '') .'</span></b><!-- EOF TOTAL_VALUE --> EUR</td>';
								$contentExportPDF .= '</tr>';

								$contentExportPDF .= '</tbody>';
								$contentExportPDF .= '</table>';

								echo $contentExportPDF;

								$pdfContentPage = $contentExportPDF;
								$pdfContentPage = removeUnnecessaryChars($pdfContentPage);

								// BOF WRITE TEMP HTML FILE
								$thisCreatedBaseName = 'Provision_' . date('Y-m-d') . '_' . convertChars($arrCustomerGroupDatas[$_POST["exportOrdersGroupID"]]["customerGroupsName"]) . '_' . $arrCustomerGroupDatas[$_POST["exportOrdersGroupID"]]["customerGroupsID"];
								$thisCreatedPdfName = $thisCreatedBaseName . '.pdf';
								$thisCreatedCsvName = $thisCreatedBaseName . '.csv';
								$tempHtmlFileName = "_temp_".$thisCreatedPdfName.'.html';
								if(file_exists(DIRECTORY_CREATED_DOCUMENTS_GROUPS . $tempHtmlFileName)) {
									unlink(DIRECTORY_CREATED_DOCUMENTS_GROUPS . $tempHtmlFileName);
								}
								clearstatcache();
								$fp = fopen(DIRECTORY_CREATED_DOCUMENTS_GROUPS . $tempHtmlFileName, 'w');
								fwrite($fp, stripslashes($pdfContentPage));
								fclose($fp);
								if(file_exists(DIRECTORY_CREATED_DOCUMENTS_GROUPS . $tempHtmlFileName)) {
									#$successMessage .= 'Die tempor&auml;re Datei &quot;' . basename(DIRECTORY_CREATED_DOCUMENTS_GROUPS . $tempHtmlFileName) . '&quot; wurde erstellt.' . '<br />';
								}
								else  {
									#$errorMessage .= 'Die tempor&auml;re Datei &quot;' . basename(DIRECTORY_CREATED_DOCUMENTS_GROUPS . $tempHtmlFileName) . '&quot; konnte nicht erstellt werden.' . '<br />';
								}
								// EOF WRITE TEMP HTML FILE

								echo '<input type="hidden" name="thisCreatedPdfName" value="' . $thisCreatedPdfName . '" />';
								echo '<input type="hidden" name="thisCreatedCsvName" value="' . $thisCreatedCsvName . '" />';
								echo '<input type="hidden" name="tempHtmlFileName" value="' . $tempHtmlFileName . '" />';
								echo '<input type="hidden" name="exportOrdersGroupID" value="' . $_POST["exportOrdersGroupID"] . '" />';

								echo '<div class="actionButtonsArea">';
								echo '<input type="submit" class="inputButton3" name="submitFormProvision" value="Daten NUR speichern" /> ';
								#echo '<input type="submit" class="inputButton3" name="createDocumentProvision" value="Speichern und PDF-Datei erzeugen" />';
								echo '</div>';
								echo '</form>';

								echo '<br />';
							}
							else {
								$errorMessage .= ' F&uuml;r diesen Vertreter liegen keine offenen Provisionen vor.';
							}
						}
					?>
					<?php displayMessages(); ?>
				</div>

			</div>
		</div>
		<div class="clear"></div>
	</div>
</div>
<script language="javascript" type="text/javascript">
	$(document).ready(function() {
		$(function() {
			$.datepicker.setDefaults($.datepicker.regional[""]);
			$('#exportOrdersDateFrom').datepicker($.datepicker.regional["de"]);
			$('#exportOrdersDateTo').datepicker($.datepicker.regional["de"]);
		});
		colorRowMouseOver('#displayCardDatas tbody tr');

		function sumProvisions() {
			// alert($(this).val());
			var totalSum = 0;
			var tempSum = 0;
			var mwst = <?php echo MWST; ?>;
			var mwstValue = 0;
			var total = 0;
			$('#displayCardDatas .cellProvision input').each(function(index, value){
				// alert(index + ': ' + $(value).val());
				tempSum = $(value).val();
				tempSum = tempSum.replace(',', '.');
				tempSum = parseFloat(tempSum);
				tempSum = tempSum * 1;
				if(tempSum < 0){
					tempSum = 0;
					$(value).val(0);
				}
				totalSum += tempSum;
			});
			provisionValue = 0;
			<?php
				if($arrSalesmanDatas["groupsProvision"] > 0){
			?>
			provisionValue = totalSum * <?php echo $arrSalesmanDatas["groupsProvision"]; ?> / 100;
			<?php
				}
			?>
			mwstValue = (totalSum + provisionValue) * mwst/100;
			total = (totalSum + provisionValue) + mwstValue;

			totalSum = parseFloat(totalSum);
			totalSum = totalSum.toFixed(2);
			totalSum = totalSum.replace('.', ',');

			provisionValue = parseFloat(provisionValue);
			provisionValue = provisionValue.toFixed(2);
			provisionValue = provisionValue.replace('.', ',');

			mwstValue = parseFloat(mwstValue);
			mwstValue = mwstValue.toFixed(2);
			mwstValue = mwstValue.replace('.', ',');

			total = parseFloat(total);
			total = total.toFixed(2);
			total = total.replace('.', ',');

			$('#displayTotalSum').text(totalSum);
			$('#displayProvision').text(provisionValue);
			$('#displayMWST').text(mwstValue);
			$('#displayTotal').text(total);
		}

		$('#displayCardDatas .cellVkprice input').keyup(function(){
			var thisVkPrice = $(this).val();
			thisVkPrice = thisVkPrice.replace(',', '.');
			var thisProvision = 0;
			var thisQuantity = $(this).parent().parent().find('.cellQuantity input').val();
			thisProvision = thisQuantity * (thisVkPrice - <?php echo KZH_EK_PRICE; ?>);
			thisProvision = thisProvision.toFixed(2)
			$(this).parent().parent().find('.cellProvision input').val(thisProvision);
			sumProvisions();
		});
		$('#displayCardDatas .cellProvision input').keyup(function(){
			sumProvisions();
		});

	});
</script>
<?php require_once('inc/footerHTML.inc.php'); ?>