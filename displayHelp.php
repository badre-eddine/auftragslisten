<?php
	require_once('inc/requires.inc.php');

	$arrGetUserRights["displayHelp"] = true;
	if(!$arrGetUserRights["displayHelp"]) {
		header('location: ' . PAGE_EXIT_LOCATION);
		exit;
	}

	$jswindowMessage = '';
	$warningMessage = '';
	$errorMessage = '';
	$successMessage = '';
	$infoMessage = '';


	$dbConnection = new DB_Connection();
	$db_open = $dbConnection->db_connect();

	// BOF READ VERTRETER
		$arrSalesmenDatas = getSalesmen();
	// EOF READ VERTRETER

	$defaultORDER = "BESTELLDATUM";
	$defaultSORT = "ASC";

	if($_POST["searchInterval"] == ""){
		$_POST["searchInterval"] = "WEEK";
	}

	// BOF READ BESTELL ARTEN
		$arrOrderTypeDatas = getOrderTypes();
	// EOF READ BESTELL ARTEN
?>
<?php
	require_once('inc/headerHTML.inc.php');
	$thisTitle = "Hilfe";

	$headerHTML = preg_replace("/{###TITLE###}/", strip_tags($thisTitle), $headerHTML);
	echo $headerHTML;

	require_once(FILE_MENUE_TOP);

	$sqlSortField = "";
	$sqlSortDirection = "";
	$sqlWhere = "";

?>

<div id="mainArea">
	<div id="mainContent">
		<?php require_once(FILE_MENUE_SIDEBAR); ?>
		<div id="contentArea">
			<a name="top"></a>
			<div id="contentAreaElements">
				<h1><?php if(SHOW_PAGE_TITLE_ICONS) { echo '<img src="' . PATH_ICONS_MENUE_TITLES . 'help.png" alt="" />'; } ?> <?php echo $thisTitle; ?></h1>

				<h2>Server-Absturz</h2>
				<p class="warningArea">Im Zweifellsfall, Volker Isken kontaktieren!</p>
				<div class="adminEditArea">
				<ul>
					<li>
						<h3>&Uuml;berpr&uuml;fen, ob die notwendigen Dienste laufen:</h3>
						<ol>
							<li>Auf Thorstens rechtem Monitor die Verkn&uuml;pfung &quot;<b>Auftragslisten.rdp</b>&quot; starten.</li>
							<li>Es &ouml;ffnet sich ein Fenster.<br />
								<ul>
									<li>Als Benutzername erscheint automatisch: &quot;Auftragslisten-PC\Auftragslisten&quot;.<br />Dann Bei Computer: &quot;<b>192.168.2.54</b>&quot; eingeben oder ausw&auml;hlen und &quot;<b>Verbinden</b>&quot; klicken.</li>
									<li>Anschlie&szlig;end bei Kennwort: &quot;<b>Auftragslisten</b>&quot; eingeben und &quot;<b>OK</b>&quot; klicken</li>
								</ul>
								Erscheint dieses Fenster nicht, ist davon auszugehen, dass der Auftragslisten-Server nicht gestartet ist.</li>
							<li>Nach erfolgreichem Login erscheint eine Standard-Windows-Oberfl&auml;che.</li>
							<li>Nun links unten auf &quot;<b>Start</b>&quot; gehen. Dann folgendes ausw&auml;hlen:<br />
								&quot;Start&quot; &rArr; &quot;Systemsteuerung&quot; &rArr; &quot;Verwaltung&quot; &rArr; &quot;Dienste&quot;
							</li>
							<li>In dem sich &ouml;ffnendem Fenster wird eine Liste der Dienste angezeigt.<br />
								Die relevanten Dienste sind &quot;<b>Apache2.2</b>&quot; und &quot;<b>MySQL</b>&quot;.<br />
							</li>
							<li>Pr&uuml;fen, ob beide Dienste gestartet sind (Status: &quot;Gestartet&quot;).<br />
								Falls nicht, den Dienst anklicken und oben links auf &quot;<b>Den Dienst starten</b>&quot; klicken.<br />
								Der Dienst wird dann wieder neu gestartet.
							</li>
							<li>Nun bitte wenige Minuten abwarten, bis die Dienste laufen.</li>
						</ol>
					</li>
				</ul>
				</div>
				<hr />
				<div class="adminEditArea">
				<ul>
					<li>
						<h3>Neu-Start des Auftragslisten-Servers</h3>
						<ol>
							<li>Pr&uuml;fen, ob der Server im Server-Raum eingeschaltet ist.<br />
								Der Server ist mit &quot;<b>Auftragslisten-Server</b>&quot; gekennzeichnet.</li>
							<li>Auf Thorstens rechtem Monitor die Verkn&uuml;pfung &quot;<b>VMware vSphere Client</b>&quot; starten.</li>
							<li>Es &ouml;ffnet sich ein Fenster, hier mit folgenden Daten einloggen:<br /><br />
								<ul>
									<li>IP: &quot;<b>192.168.2.119</b>&quot;</li>
									<li>Login: &quot;<b>thorsten</b>&quot;</li>
									<li>Passwort: &quot;<b>thorsten</b>&quot;</li>
								</ul>
								<br />
								Erscheint dieses Fenster nicht, ist davon auszugehen, dass der Auftragslisten-Server nicht in Betrieb ist.
							</li>
							<li>Nach erfolgreichem Login erscheint eine Oberfl&auml;che mit einem Verzeichnis-Baum links oben, mit dem Wurzelelement &quot;<b>192.168.2.119</b>&quot;.<br />
								Darunter sollten mehrere Eintr&auml;ge stehen.<br />
								Steht dort kein Eintrag, dann auf das &quot;<b>Plus (+)</b>&quot;-Symbol klicken, dann sollten mehrere Eintr&auml;ge erscheinen.
							</li>
							<li>Der relevante Eintrag ist der Eintrag mit der Bezeichnung &quot;<b>Auftragslisten</b>&quot;.<br /><br />
								<ul>
									<li>Steht vor dem Eintrag ein &quot;<b>gr&uuml;ner Pfeil</b>&quot;, so ist der Auftragslisten-Server gestartet.</li>
									<li>Steht vor dem Eintrag kein &quot;<b>gr&uuml;ner Pfeil</b>&quot;, dann mit der <b>rechten</b> Maus-Taste auf den Eintrag &quot;<b>Auftragslisten</b>&quot; klicken.<br />
										<ul>
										<li>Nun in dem erscheinenden Kontext-Men&uuml; folgendes ausw&auml;hlen:<br />
											&quot;Betrieb&quot; &rArr; &quot;Einschalten&quot;.<br /><br />Der Auftragslisten-Server wird dann wieder neu gestartet.</li>
										</ul>
									</li>
								</ul>
							</li>
							<!--
							<li>Sind die Optionen unter Betrieb deaktiviert und nicht anklickbar, dann &quot;<b>VMware vSphere Client</b>&quot; beenden:<br />Unter &quot;Datei&quot; &rArr; &quot;Beenden&quot; klicken und erneut mit folgenden Daten einloggen und, wie unter Punkt 5 beschrieben, beschrieben vorgehen:<br /><br />
								<ul>
									<li>IP: &quot;<b>192.168.2.119</b>&quot;</li>
									<li>Login: &quot;<b>root</b>&quot;</li>
									<li>Passwort: &quot;<b>polonion4</b>&quot;</li>
								</ul>
							</li>
							-->
							<li>Nach erfolgreichem &quot;Einschalten&quot; nun bitte wenige Minuten abwarten, bis der Auftragslisten-Server hochgefahren ist.</li>
						</ol>
					</li>
				</ul>
				</div>
			</div>
		</div>
		<div class="clear"></div>
	</div>
</div>
<script language="javascript" type="text/javascript">
	$(document).ready(function() {
		$(function() {
			colorRowMouseOver('.displayOrders tbody tr');
			$('.buttonShopOrderInfo').css('cursor', 'pointer');
			$('.buttonShopOrderInfo').click(function () {
				loadOrderDetails($(this), '<?php echo BASEPATH; ?>', 'Details der Bestellung');
			});

			// BOF GET DATAS FOR GRAPH CASH
			<?php
				/*
				if(!empty($arrDatasGraph)){
					$arrDatasGraph["countItems"] = array_reverse($arrDatasGraph["countItems"], true);
					$arrDatasGraph["totalPrice"] = array_reverse($arrDatasGraph["totalPrice"], true);
					$arrLabelsGraph = array_reverse($arrLabelsGraph , true);
				}
				*/
			?>

			// createGraph('cvs_dataCountItems', [<?php echo implode(",", array_values($arrDatasGraph["countItems"])); ?>], [<?php echo "'" . implode("','", $arrLabelsGraph) . "'"; ?>], 'Line', [['green','red']]);
			// createGraph('cvs_dataTotalPrice', [<?php echo implode(",", array_values($arrDatasGraph["totalPrice"])); ?>], [<?php echo "'" . implode("','", $arrLabelsGraph) . "'"; ?>], 'Line', [['green','red']]);

		// EOF GET DATAS FOR GRAPH CASH
		});
		$(function() {
			$('#tabs').tabs();
		});
	});
</script>
<?php require_once('inc/footerHTML.inc.php'); ?>