<?php

//phpinfo();exit();
	session_start();
// error_reporting(E_ALL & ~(E_STRICT|E_NOTICE));
	error_reporting(E_ALL);
ini_set('display_errors', 1);
	if(!isset($_GET["isMandator"])) {
		require_once('config/configMandator.inc.php');
	}

	if(!defined("MANDATOR")) {
		DEFINE("MANDATOR", "bctr");
	}
	require_once('config/configFiles.inc.php');
	require_once('config/configBasic.inc.php');
	require_once('config/configTables.inc.php');
	require_once('inc/functions.inc.php');

	if($_SESSION["usersID"] == "") {
		session_unset();
	}

	$showWindowEvent = false;

	$jswindowMessage = "";
	$warningMessage = '';
	$errorMessage = '';
	$successMessage = '';
	$infoMessage = '';

	// BOF DEFINE DUTY FIELDS
		$arrFormDutyFields = array(
			"login" => "Login",
			"password" => "Passwort",
			"mandator" => "Mandator"
		);
		// CREATE JSON DUTY FIELD
		$jsonFormDutyFields = createJson($arrFormDutyFields);
	// BOF DEFINE DUTY FIELDS

	$dbConnection = new DB_Connection();
	$db_open = $dbConnection->db_connect();



	// $sql = "select * from `" . TABLE_USERS_ONLINE . "`";

	// 			$rs = $dbConnection->db_query($sql);
	// 			$ds = mysqli_fetch_array($rs);
	// print_r($ds);exit();
	// LOGOUT FROM ADMIN
		if(isset($_GET["mode"]) && $_GET["mode"] == "logoutUser") {
			// session_start();

			if($_SESSION["usersID"] != "") {
				$sql = "INSERT INTO `" . TABLE_USERS_ONLINE . "` (
								`usersOnlineUserID`,
								`usersOnlineDateTime`,
								`usersOnlineMandator`,
								`usersOnlineType`
							)
							VALUES (
									'".$_SESSION["usersID"]."',
									NOW(),
									'".$_COOKIE["mandator"]."',
									'logout'
								)";

				$rs = $dbConnection->db_query($sql);
			}
			//echo $sql;
			//exit;

			$cookieNameMandator = "mandator";
			$cookieNameUserID = "usersID";
			$cookieValueMandator = $_SESSION["mandator"];
			if(DEBUG) { $cookieValueMandator = 'bctr'; }
			$cookieValueUserID  = $_SESSION["usersID"];

			$cookieTime = time()-60*60*24*30;
			$cookiePath = BASEPATH;
			$cookieDomain = "." . $_SERVER["HTTP_HOST"] . "/" . DIR_INSTALLATION_NAME;
			$cookieSecure = false;
			$cookieHttponly = true;

			// $cookieTime = 0;
			// setcookie($cookieName, $cookieValue, $cookieTime, $cookiePath, $cookieSecure, $cookieHttponly);
			// setcookie($cookieName, $cookieValue, $cookieTime, $cookiePath);
			setcookie($cookieNameMandator, $cookieValueMandator, $cookieTime, $cookiePath);
			setcookie($cookieNameUserID, $cookieValueUserID, $cookieTime, $cookiePath);

			session_destroy();

			unset($_SESSION);
			unset($_COOKIE);

			session_start();

			header("Location: " . PAGE_INDEX . '?logoutSuccess=' . $cookieValueMandator);
			exit;
		}
		if(isset($_GET["logoutSuccess"]) && $_GET["logoutSuccess"] != '') {
			$successMessage .= 'Sie sind jetzt aus der Warenwirtschaft von ' . strtoupper($_GET["logoutSuccess"]) . ' ausgeloggt. '.'<br />';
		}
	// END LOGOUT FROM ADMIN

	if(isset($_POST["submitForm"]) && $_POST["submitForm"] != "") {
		if (!empty($_POST["login"]) && !empty($_POST["password"]) && !empty($_POST["mandator"])) {
			if ($db_open) {

// 	echo "dd";
// exit();
				$sql = "SELECT
							`usersID`,
							`usersLogin`,
							`usersPassword`

							FROM `" . TABLE_USERS . "`
							WHERE `usersLogin` = '".mysqli_real_escape_string($db_open,$_POST["login"])."'
							AND `usersPassword` = '".md5(mysqli_real_escape_string($db_open,$_POST["password"]).$saltString)."'
							AND `usersActive` = '1'
							LIMIT 1
						";
				$rs = $dbConnection->db_query($sql);

				while($ds = mysqli_fetch_array($rs))
				{
// echo $sql;exit();
					
					$_SESSION["usersID"] = $ds["usersID"];
					$_SESSION["mandator"] = $_POST["mandator"];

					// BOF ADD FOR MANDATOR_VERSION
						if($_POST["mandator"] !="") {
							// echo "mandator: " . $_POST["mandator"]."<br>";
							// $_COOKIE["mandator"]	= $_POST["mandator"];
							$cookieNameMandator = "mandator";
							$cookieNameUserID = "usersID";
							$cookieValueMandator = $_POST["mandator"];
							// if(DEBUG) { $cookieValueMandator = 'bctr'; }
							$cookieValueUserID =  $ds["usersID"];
							$cookieTime = time()+60*60*24*30;
							$cookiePath = BASEPATH;
							$cookieDomain = "." . $_SERVER["HTTP_HOST"] . "/" . DIR_INSTALLATION_NAME;
							$cookieSecure = false;
							$cookieHttponly = true;

							// $cookieTime = 0;
							// setcookie($cookieName, $cookieValue, $cookieTime, $cookiePath, $cookieSecure, $cookieHttponly);
							// setcookie($cookieName, $cookieValue, $cookieTime, $cookiePath);
							setcookie($cookieNameMandator, $cookieValueMandator, $cookieTime);
							setcookie($cookieValueUserID, $cookieValueUserID, $cookieTime);
							// $_SESSION["mandator"] = $_COOKIE["mandator"];
							$_SESSION["mandator"] = $_POST["mandator"];
						}
					// EOF ADD FOR MANDATOR_VERSION
				}

				if($_SESSION["usersID"] != "") {
					
					$sql = "INSERT INTO `" . TABLE_USERS_ONLINE . "` (

									`usersOnlineUserID`,
									`usersOnlineDateTime`,
									`usersOnlineMandator`,
									`usersOnlineType`
								)
								VALUES (

										'".$_SESSION["usersID"]."',
										NOW(),
										'".$_POST["mandator"]."',
										'login'
									)";

					$rs = $dbConnection->db_query($sql);
				}
				else {
						$errorMessage .= ' Ihre Zugangsdaten sind nicht korrekt. '.'<br />';
				}
			}
			else  {
				$errorMessage .= ' Keine Verbindung m&ouml;glich! '.'<br />';
			}
		}
		else {
			$errorMessage .= ' Bitte geben Sie die Zugangsdaten korrekt ein! '.'<br />';
		}
	}

	if(!empty($_SESSION["usersID"])) {
		$arrGetUserRights = readUserRights();
 
		if($arrGetUserRights["adminArea"]){
		
			$_SESSION["isAdmin"] = true;
			#$_COOKIE["isAdmin"] = true;
			setcookie("isAdmin", true, 0);
		}

		if($arrGetUserRights["switchMandator"]){
			$_SESSION["showSwitchMandator"] = true;
			#$_COOKIE["showSwitchMandator"] = true;
			setcookie("showSwitchMandator", true, 0);
		}

		if($_REQUEST["redirectURL"] != '') {
			header("Location: " . $_REQUEST["redirectURL"] . '');
		}
		else {
			header("Location: " . PAGE_AFTER_LOGIN . '?loginSuccess=true');
		}
		exit;
	}
?>


<?php
	require_once('inc/headerHTML.inc.php');
	$thisTitle = "Warenwirtschaft: Login";
	$headerHTML = preg_replace("/{###TITLE###}/", strip_tags($thisTitle), $headerHTML);
	echo $headerHTML;

	require_once(FILE_MENUE_TOP);

	echo $_SESSION["usersID"];
?>
<div id="mainArea">
	<div id="mainContent">
		<?php require_once(FILE_MENUE_SIDEBAR); ?>
		<div id="contentArea">
			<div id="contentAreaElements">
				<h1><?php if(SHOW_PAGE_TITLE_ICONS) { echo '<img src="' . PATH_ICONS_MENUE_TITLES . 'login.png" alt="" />'; } ?> <?php echo $thisTitle; ?></h1>
				<div class="contentDisplay">

					<?php displayMessages(); ?>
					<p>&nbsp;</p>
					<p>&nbsp;</p>
					<div align="center">
						<table class="adminEditArea" border="0" cellspacing="0" cellpadding="2">
							<tr>
								<td align="center">
									<p class="infoArea">Um sich einzuloggen, geben Sie bitte Ihr Login und Ihr Passwort ein und klicken dann auf "Login".</p>
									<p>&nbsp;</p>
									<form name="formLogin" method="post" action="<?php echo $_SERVER["PHP_SELF"]?>">
										<?php if($_REQUEST["redirectURL"] != '') { ?>
										<input type="hidden" name="redirectURL" value="<?php echo $_REQUEST["redirectURL"]; ?>" />
										<?php } ?>
										<table class="" border="0" cellspacing="0" cellpadding="10">
											<tr>
												<td><label for="login">Login:</label></td>
												<td><input class="inputField_20" type="text" name="login" id="login" size="10" style="width:135px" maxlength="20" readonly="readonly" /></td>
												<td rowspan="2" align="right" valign="top">
													<img alt="Logo" id="logoMandator" title="" border="0" width="270" height="36" src="layout/login_default.jpg" />
												</td>
											</tr>
											<tr>
												<td><label for="password">Passwort:</label></td>
												<td><input class="inputField_20" type="password" name="password" id="password" maxlength="20" style="width:135px" size="10" readonly="readonly" /></td>
											</tr>
											<tr>
												<td><b>Mandant: <span class="dutyField">(*)</span></b></td>
												<td style="white-space:nowrap; text-align:center;"><input type="radio" class="radioMandator" name="mandator" id="mandator_1" value="bctr" checked="checked"  /> <label for="mandator_1">Burhan-CTR</label></td>
												<td style="white-space:nowrap; text-align:center;"><input type="radio" class="radioMandator" name="mandator" id="mandator_2" value="b3"  /> <label for="mandator_2" >B3 - Werbepartner</label></td>
											</tr>
										</table>

										<input class="inputButton1 inputButtonGreen" type="submit" name="submitForm" value="Login" />
										<?php
											// onclick="return checkFormDutyFields(this.form.name, <?xxx echo $jsonFormDutyFields; xxxx>);"
										?>
									</form>
								</td>
							</tr>
						</table>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<script language="javascript" type="text/javascript">
	$(document).ready(function() {

		setFocus('formLogin', 'login');
		mandator = $('.radioMandator:checked').val();
		$('#logoMandator').attr('src', 'layout/login_' + mandator + '.jpg');
		$('#headerAreaContent').css('background-image', 'url(layout/headerBG_' + mandator + '.png)');
		$('#headerAreaLogo').css('background-image', 'url(layout/headerLogo_' + mandator + '.png)');
		setFormDutyFields(<?php echo $jsonFormDutyFields; ?>);
		if(navigator.cookieEnabled) {
			$('#login').removeAttr('readonly');
			$('#password').removeAttr('readonly');
			// $('.radioMandator').removeAttr('disabled');
			$('.inputButton1').removeAttr('disabled');

			$('.radioMandator').click(function() {
				var mandator = $('.radioMandator:checked').val();
				$('#logoMandator').attr('src', 'layout/login_' + mandator + '.jpg');
				$('#headerAreaContent').css('background-image', 'url(layout/headerBG_' + mandator + '.png)');
				$('#headerAreaLogo').css('background-image', 'url(layout/headerLogo_' + mandator + '.png)');
			});
		}
	});
</script>

<?php require_once('inc/footerHTML.inc.php'); ?>