<?php
	require_once('inc/requires.inc.php');

	if(!$arrGetUserRights["serverInfo"]) {
		header('location: ' . PAGE_EXIT_LOCATION);
		exit;
	}

	$jswindowMessage = "";
	$warningMessage = '';
	$errorMessage = '';
	$successMessage = '';
	$infoMessage = '';

	$dbConnection = new DB_Connection();
	$db_open = $dbConnection->db_connect();

	$userDatas = getUserDatas();

?>
<?php
	require_once('inc/headerHTML.inc.php');
	$thisTitle = "Datei-Explorer";
	$headerHTML = preg_replace("/{###TITLE###}/", strip_tags($thisTitle), $headerHTML);
	echo $headerHTML;

	require_once(FILE_MENUE_TOP);
?>
<div id="mainArea">
	<div id="mainContent">
		<?php require_once(FILE_MENUE_SIDEBAR); ?>
		<div id="contentArea">
			<div id="contentAreaElements">
				<h1><?php if(SHOW_PAGE_TITLE_ICONS) { echo '<img src="' . PATH_ICONS_MENUE_TITLES . 'sitemap.png" alt="" />'; } ?> <?php echo $thisTitle; ?></h1>

				<div class="contentDisplay">
					<?php displayMessages(); ?>
					<iframe class="iFrameModule" src="<?php echo FILE_EXPLORER; ?>"></iframe>
				</div>
			</div>
		</div>
		<div class="clear"></div>
	</div>
</div>
<?php
	if($arrGetUserRights["editPersonnel"]) {
?>
<script language="javascript" type="text/javascript">
	$(document).ready(function() {
		$(function() {
			$( "#datepicker" ).datepicker();
			$.datepicker.setDefaults($.datepicker.regional[""]);
			$('#editPersonnelVacationsStart').datepicker($.datepicker.regional["de"]);
			$('#editPersonnelVacationsEnd').datepicker($.datepicker.regional["de"]);
			$('#editPersonnelJobStart').datepicker($.datepicker.regional["de"]);
			$('#editPersonnelJobEnd').datepicker($.datepicker.regional["de"]);
			//$('#editPersonnelBirthday').datepicker($.datepicker.regional["de"]);
			$('#editPersonnelDiseaseStart').datepicker($.datepicker.regional["de"]);
			$('#editPersonnelDiseaseEnd').datepicker($.datepicker.regional["de"]);
			$('#editPersonnelOvertimeDate').datepicker($.datepicker.regional["de"]);
		});
		$(function() {
			$('#tabs').tabs();
		});
		setFormDutyFields(<?php echo $jsonFormDutyFields; ?>);
		colorRowMouseOver('.displayOrders tbody tr');

		$('#editPersonnelZipcode').keyup(function () {
			loadSuggestions('searchPlzCity', [{'triggerElement': '#editPersonnelZipcode', 'fieldCity': '#editPersonnelCity', 'fieldZipCode': '#editPersonnelZipcode'}], 1);
		});
		$('#editPersonnelCity').keyup(function () {
			loadSuggestions('searchCityPlz', [{'triggerElement': '#editPersonnelCity', 'fieldCity': '#editPersonnelCity', 'fieldZipCode': '#editPersonnelZipcode'}], 1);
		});

		$('#editPersonnelBankName').keyup(function(){
			loadSuggestions('searchBankName', [{'triggerElement': '#editPersonnelBankName', 'fieldCode': '#editPersonnelBankLeitzahl', 'fieldName': '#editPersonnelBankName', 'fieldBIC': '#editPersonnelBankBIC', 'fieldIBAN': '#editPersonnelBankIBAN'}], 1);
		});
		$('#editPersonnelBankLeitzahl').keyup(function(){
			loadSuggestions('searchBankCode', [{'triggerElement': '#editPersonnelBankLeitzahl', 'fieldCode': '#editPersonnelBankLeitzahl', 'fieldName': '#editPersonnelBankName', 'fieldBIC': '#editPersonnelBankBIC', 'fieldIBAN': '#editPersonnelBankIBAN'}], 1);
		});
	});
</script>
<?php
	}
?>
<?php require_once('inc/footerHTML.inc.php'); ?>