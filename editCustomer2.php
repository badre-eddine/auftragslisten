<?php
	require_once('inc/requires.inc.php');
	 
	if(!$arrGetUserRights["editCustomers"] && !$arrGetUserRights["displayCustomers"]) {
		header('location: ' . PAGE_EXIT_LOCATION);
		exit;
	}

	$dbConnection = new DB_Connection();
	$db_open = $dbConnection->db_connect();

	$jswindowMessage = '';
	$warningMessage = '';
	$errorMessage = '';
	$successMessage = '';
	$infoMessage = '';

	// BOF GET USER DATAS
		$arrUserDatas = getUsers();
		$userDatas = getUserDatas(); 
	// EOF GET USER DATAS

	// BOF USED TABLES
		$arrTables = array(
				'RK' => TABLE_ORDER_RK,
				'AN' => TABLE_ORDER_AN,
				'AB' => TABLE_ORDER_AB,
				'RE' => TABLE_ORDER_RE,
				'LS' => TABLE_ORDER_LS,
				'GU' => TABLE_ORDER_GU,
				'MA' => TABLE_ORDER_MA,
				'M1' => TABLE_ORDER_M1,
				'M2' => TABLE_ORDER_M2,
				'BR' => TABLE_ORDER_BR,
			);

	// EOF DELETE DOCUMENT 
	if(isset($_GET['documentType']) && isset($_GET['documentNumber']))
	{
		$tableOrderDocument = $arrTables[$_GET['documentType']] ;
		$DocumentID = $_GET['documentNumber'];
		
		$delete_sql = "
				DELETE FROM `" . $tableOrderDocument . "`
				WHERE `orderDocumentsID` = '" . $DocumentID . "'
			";

		 $dbConnection->db_query($delete_sql);
 
		
		header("Location:".$_SERVER['HTTP_REFERER']."");
	}		
	if(isset($_GET['delcustomersPriceListsProductsDataID']) && isset($_GET['editID']))
	{
 		$DocumentID = $_GET['delcustomersPriceListsProductsDataID'];
		
		$delete_sql = "
				DELETE FROM `common_customerspricelistsproductsdata`
				WHERE `customersPriceListsProductsDataID` = '" . $DocumentID . "'
			";

		 $dbConnection->db_query($delete_sql);
 
		
		header("Location:".$_SERVER['HTTP_REFERER']."");
	}		
	// EOF USED TABLES

	// BOF DOWNLOAD FILE
		if($_GET["downloadFile"] != "" && $_GET["documentType"] != "") {

			if($_GET["documentType"] == 'KA') {
				$fileDirectory = DIRECTORY_UPLOAD_FILES . '';
			}
			else if($_GET["documentType"] == 'PR') {
				$fileDirectory = DIRECTORY_CREATED_DOCUMENTS_SALESMEN . '';
			}
			else if($_GET["documentType"] == 'PDF') {
			 
				$fileDirectory =  DIRECTORY_CREATED_DOCUMENTS_PDF . '';
			}
			else {
				$fileDirectory = DIRECTORY_CREATED_DOCUMENTS_CUSTOMERS;
			}
			$_GET["downloadFile"] = utf8_decode(urldecode($_GET["downloadFile"]));
			
			 

			require_once('classes/downloadFile.class.php');
			$thisDownload = new downloadFile($fileDirectory, $_GET["downloadFile"]);
			$errorMessage .= $thisDownload->startDownload();
		}
	// EOF DOWNLOAD FILE

	// BOF DELETE FILE
		if($_GET["deleteFile"] != "" && $_GET["deleteFile"] != "") {
			if($_GET["documentType"] == 'KA') {
				$fileDirectory = DIRECTORY_UPLOAD_FILES . '';
				$fileTable = TABLE_CUSTOMERS_LAYOUT_FILES;
			}
			else {
				$fileDirectory = DIRECTORY_CREATED_DOCUMENTS_CUSTOMERS;
				$fileTable = TABLE_CREATED_DOCUMENTS;
			}

			#$thisDeleteFile = utf8_encode($_GET["deleteFile"]);
			$thisDeleteFile = ($_GET["deleteFile"]);
			$sql = "
				DELETE FROM `" . $fileTable . "`
				WHERE `createdDocumentsType` = '" . $_GET["documentType"] . "'
					AND `createdDocumentsTitle` = '" . $thisDeleteFile . "'
			";

			$rs = $dbConnection->db_query($sql);

			if($rs){
				$successMessage .= 'Die Datei &quot;' . $fileDirectory . $thisDeleteFile . '&quot; wurde aus der Datenbank entfernt.' . '<br />';
			}
			else {
				$errorMessage .= 'Die Datei &quot;' . $fileDirectory . $thisDeleteFile . '&quot; konnte leider nicht aus der Datenbank entfernt werden.' . '<br />';
			}

			$thisDeleteFile = utf8_decode(urldecode($_GET["deleteFile"]));
			if(file_exists($fileDirectory . $thisDeleteFile)) {
				$resultUnlink = unlink($fileDirectory . $thisDeleteFile);
				if($resultUnlink) {
					$successMessage .= 'Die Datei &quot;' . $fileDirectory . utf8_encode($thisDeleteFile) . '&quot; wurde aus dem Verzeichnis entfernt.' . '<br />';
				}
				else {
					$errorMessage .= 'Die Datei &quot;' . $fileDirectory . utf8_encode($thisDeleteFile) . '&quot; konnte leider nicht aus dem Verzeichnis entfernt werden.' . '<br />';
				}
			}
			else {
				$errorMessage .= 'Die Datei &quot;' . $fileDirectory . utf8_encode($thisDeleteFile) . '&quot; existiert nicht und kann daher nicht entfernt werden.' . '<br />';
			}
		}
	// EOF DELETE FILE

	// BOF DEFINE DUTY FIELDS
		$arrFormDutyFields = array(
			// "editCustomersKundennummer" => "Kundennummer",
			"editCustomersFirmenname" => "Firmenname",
			"editCustomersTyp" => "Kunden-Typ",
			"editCustomersFirmenadresseStrasse" => "Firmenanschrift-Straße",
			"editCustomersFirmenadresseHausnummer" => "Firmenanschrift-Hausnummer",
			"editCustomersFirmenadressePLZ" => "Firmenanschrift-PLZ",
			"editCustomersFirmenadresseOrt" => "Firmenanschrift-Ort",
			// "editCustomersGruppe[]" => "Kunden-Gruppe"
			// "editCustomersGruppe" => "Kunden-Gruppe"
		);

		// CREATE JSON DUTY FIELD
		$jsonFormDutyFields = createJson($arrFormDutyFields);
		#dd('jsonFormDutyFields');
	// BOF DEFINE DUTY FIELDS

	if($_REQUEST["tab"] == "")  {
		$_REQUEST["tab"] = "tabs-1";
	}
	if($_REQUEST["editID"] == "")  {
		// $_REQUEST["editID"] = "NEW";
		// header("location: displayOrders.php");
		// exit;
	}

	// BOF READ VERTRETER
		$arrAgentDatas = getAgents();
		$arrAgentDatas2 = getAgents2();
	// EOF READ VERTRETER

	// BOF READ SALUTATIONS
		$arrSalutationTypeDatas = getSalutationTypes();
	// EOF READ SALUTATIONS

	// BOF READ PAYMENT TYPES
		$arrPaymentTypeDatas = getPaymentTypes();
	// EOF READ PAYMENT TYPES

	// BOF READ PAYMENT CONDITIONS
		$arrPaymentConditionDatas = getPaymentConditions();
	// EOF READ PAYMENT CONDITIONS

	// BOF READ BESTELL ARTEN
		$arrOrderTypeDatas = getOrderTypes();
	// EOF READ BESTELL ARTEN

	// BOF READ ADDITIONAL COSTS
		$arrAdditionalCostsDatas = getAdditionalCosts();
	// EOF READ ADDITIONAL COSTS

	// BOF READ COUNTRIES
		$arrCountryTypeDatas = getCountryTypes();
	// EOF READ COUNTRIES

	// BOF READ CUSTOMER TYPES
		$arrCustomerTypeDatas = getCustomerTypes();
	// EOF READ CUSTOMER TYPES

	// BOF READ DOCUMENT TYPE DATAS
		$arrDocumentTypeDatas = getDocumentTypes();
	// EOF READ DOCUMENT TYPE DATAS

	// BOF READ DOCUMENT TYPE DATAS
		$arrPaymentStatusTypeDatas = getPaymentStatusTypes();
	// EOF READ DOCUMENT TYPE DATAS

	// BOF READ CUSTOMER GROUP DATAS
		$arrCustomerGroupDatas = getCustomerGroups();
	// EOF READ CUSTOMER GROUP DATAS

	// BOF READ BESTELL STATUS TYPEN
		$arrOrderStatusTypeDatas = getOrderStatusTypes();
		$arrOrderStatusTypeDatasExtern = getOrderStatusTypesExtern();
	// EOF READ BESTELL STATUS TYPEN

	// BOF READ ORDER SOURCE TYPES
		$arrOrdersSourceTypeDatas = getOrdersSourceTypes();
	// EOF READ ORDER SOURCE TYPES

	// BOF GET SALESMAN TO ZIPCODE
		#$arrRelationSalesmenZipcodeDatas = getRelationSalesmenZipcode();
		$arrRelationSalesmenZipcodeDatas = getRelationSalesmenZipcode2();
	// EOF GET SALESMAN TO ZIPCODE

	// BOF READ CONTACT TYPE DATAS
		$arrCustomerContactTypeDatas = getContactTypes();
	// EOF READ CONTACT TYPE DATAS

	// BOF READ PRICE LIST DATAS
		$arrCustomersPriceListDatas = getCustomersPriceLists();
	// EOF READ PRICE LIST DATAS

	// BOF SEND MAIL WITH ATTACHED FILE
		if($_POST["sendAttachedDocument"] == '1') {
			$thisCreatedPdfName = $_POST["mailDocumentFilename"];
			$_POST["selectSubject"] = $arrDocumentTypeDatas[$_REQUEST["documentType"]]["createdDocumentsTypesName"] . ' Nr.: ' . '{###DOCUMENT_NUMBER###}';
			$generatedDocumentNumber = substr($_POST["mailDocumentFilename"], 0, 13);

			require_once(DIRECTORY_HTML_MIME_MAIL . 'htmlMimeMail.php');
			require_once(DIRECTORY_MAIL_TEMPLATES . 'template_BASIC.inc.php');
			require_once(DIRECTORY_MAIL_TEMPLATES . 'template_TEXT_DATAS.inc.php');
			##require_once("inc/mail.inc.php");

			// BOF SEND MAIL
			require_once("classes/createMail.class.php");

			// BOF CREATE SUBJECT
				$thisSubject = $_POST["selectSubject"];
				if($_POST["sendAttachedMailSubject"] != '') {
					$thisSubject = $_POST["sendAttachedMailSubject"] . ', ' . $thisSubject;
				}
			// EOF CREATE SUBJECT

			// BOF GET ATTACHED FILE
				if($_REQUEST["documentType"] == 'KA') {
					$pathDocumentFolder = DIRECTORY_UPLOAD_FILES;
					$thisCreatedPdfName = utf8_decode($thisCreatedPdfName);
				}
				else if($_REQUEST["documentType"] == 'EX') {
					$pathDocumentFolder = DIRECTORY_EXPORT_FILES;
					$thisCreatedPdfName = utf8_decode($thisCreatedPdfName);
				}
				else if($_REQUEST["documentType"] == 'SX') {
					$pathDocumentFolder = DIRECTORY_CREATED_DOCUMENTS_SALESMEN;
					$thisCreatedPdfName = utf8_decode($thisCreatedPdfName);
				}
				else if($_REQUEST["documentType"] == 'SP') {
					$pathDocumentFolder = DIRECTORY_ATTACHMENT_DOCUMENTS;
					$thisCreatedPdfName = utf8_decode($thisCreatedPdfName);
					// BOF CREATE PDF WITH CUSTOMER_NUMBER
						#$sourcePDF = PATH_SEPA_FORM;
						$sourcePDF = BASEPATH . PATH_SEPA_FORM;
						$targetPDF = DIRECTORY_ATTACHMENT_DOCUMENTS . $_POST["mailDocumentFilename"];
						if(file_exists($targetPDF)){
							unlink($targetPDF);
						}
						$arrTemp = explode("_", $_POST["mailDocumentFilename"]);
						$thisCustomerNumber = $arrTemp[0];

						$fp = fopen($sourcePDF, 'r');
						$contentPDF = fread($fp, filesize($sourcePDF));
						fclose($fp);

						$thisPattern = "{###KUNDENNUMMER###}";
						$thisReplace = str_pad($thisCustomerNumber, strlen($thisPattern));
						$contentPDF = preg_replace("/".$thisPattern."/", $thisReplace, $contentPDF);

						$targetPDF = DIRECTORY_ATTACHMENT_DOCUMENTS . $_POST["mailDocumentFilename"];
						$fp = fopen($targetPDF, 'w');
						fwrite($fp, $contentPDF);
						fclose($fp);
					// EOF CREATE PDF WITH CUSTOMER_NUMBER
				}
				else {
					$pathDocumentFolder = DIRECTORY_CREATED_DOCUMENTS_CUSTOMERS;
				}

				$arrAttachmentFiles = array (
					rawurldecode($pathDocumentFolder . $thisCreatedPdfName) => str_replace(" ", "_", rawurldecode(myUnHtmlEntities($thisCreatedPdfName)))
				);
			// EOF GET ATTACHED FILE

			$createMail = new createMail(
								$thisSubject,													// TEXT:	MAIL_SUBJECT
								$_POST["documentType"],											// STRING:	DOCUMENT TYPE
								$generatedDocumentNumber,										// STRING:	DOCUMENT NUMBER
								$arrAttachmentFiles,											// ARRAY:	ATTACHMENT FILES
								$_POST["selectCustomersRecipientMail"],	 						// STRING:	RECIPIENTS
								$arrMailContentDatas[$_POST["selectMailtextTemplates"]],		// MAIL_TEXT_TEMPLATE
								$mailTemplate["html"],											// MAIL_BODY_TEMPLATE
								'',																// STRING:	ADDITIONAL TEXT
								$_POST["sendAttachedMailText"],									// STRING:	SEND ATTACHED TEXT
								true,															// BOOLEAN:	SEND ATTACHED DOCUMENT
								DIRECTORY_MAIL_IMAGES,											// DIRECTORY_MAIL_IMAGES
								$_POST["selectMailtextSender"]									// STRING: SENDER
							);

			$createMailResult = $createMail->returnResult();
			$sendMailToClient = $createMailResult;

			#$tmpErrorResult = $createMail->returnErrors();

			#if(!$sendMailToUs) { $errorMessage .= ' Die Mail-Kopie konnte nicht an ' . implode(', ', $arrOrderMail) . ' versendet werden! '.'<br />'; }
			#else { $successMessage .= ' Die Mail-Kopie wurde an ' . implode(', ', $arrOrderMail) . ' versendet! '.'<br />'; }

			if(!$sendMailToClient) { $errorMessage .= ' Die Mail konnte nicht an ' . $_POST["selectCustomersRecipientName"] . ' (' . $_POST["selectCustomersRecipientMail"] . ') versendet werden! '.'<br />'; }
			else {
				$successMessage .= ' Die Mail wurde an ' . $_POST["selectCustomersRecipientName"] . ' (' . $_POST["selectCustomersRecipientMail"] . ') versendet! '.'<br />';
			}
			if($_REQUEST["documentType"] == 'SP') {
				if(file_exists($targetPDF)){
					unlink($targetPDF);
				}
			}
		}
	// EOF SEND MAIL WITH ATTACHED FILE

	// BOF STORE PHONE MARKETING
		if($_POST["storePhoneMarketing"] != "") {

			$sql_storePhoneMarketing = "
					REPLACE INTO `" . TABLE_CUSTOMERS_PHONE_MARKETING . "` (
						
						`customersPhoneMarketingCustomerNumber`,
						`customersPhoneMarketingCustomerID`,
						`customersPhoneMarketingContactDate`,
						`customersPhoneMarketingContactTime`,
						`customersPhoneMarketingCampaign`,
						`customersPhoneMarketingConversationPerson`,
						`customersPhoneMarketingConversationContent`,
						`customersPhoneMarketingRemindDate`,
						`customersPhoneMarketingCallbackDate`,
						`customersPhoneMarketingVisitRequest`,
						`customersPhoneMarketingNotice`,
						`customersPhoneMarketingUser`
					)
					VALUES (
						
						'" . addslashes($_POST["editCustomersKundennummer"]) . "',
						'" . addslashes($_POST["editCustomersID"]) . "',
						'" . formatDate($_POST["phoneMarketingContactDate"], "store") . "',
						'" . $_POST["phoneMarketingContactTime"] . "',
						'" . addslashes($_POST["phoneMarketingCampaign"]) . "',
						'" . addslashes($_POST["phoneMarketingConversationPerson"]) . "',
						'" . addslashes($_POST["phoneMarketingConversationContent"]) . "',
						'" . formatDate($_POST["phoneMarketingRemindDate"], "store") . "',
						'" . formatDate($_POST["phoneMarketingCallbackDate"], "store") . "',
						'" . addslashes($_POST["phoneMarketingVisitRequest"]) . "',
						'" . addslashes($_POST["phoneMarketingNotice"]) . "',
						'" . $_SESSION["usersID"] . "'
					)
				";

			$rs_storePhoneMarketing = $dbConnection->db_query($sql_storePhoneMarketing);

			if($rs_storePhoneMarketing) {
				$successMessage .= ' Das Gesprächsprotokoll wurde gespeichert. ' .'<br />';
			}
			else {
				$errorMessage .= ' Das Gesprächsprotokoll konnte nicht gespeichert werden. ' . '<br />';
			}
		}
	// BOF STORE PHONE MARKETING


	// BOF STORE FILES
		if($_POST["storeFiles"] != "") {
			// BOF STORE FILES
			if(!empty($_FILES)) {

				$sql_insert = array();
				$fileIsEmpty = true;

				$countFiles++;

				for($i = 0 ; $i < count($_FILES["editCustomersPrintFile"]["name"]) ; $i++) {
					$arrTemp = pathinfo($_FILES["editCustomersPrintFile"]["name"][$i]);

					if($_FILES["editCustomersPrintFile"]["name"][$i] != "") {
						if(strtolower($arrTemp["extension"]) == "pdf") {

							/* file name condition desactivation */
							/*if(preg_match("/^([0-9]{5}|[A-Z\-]{3}[0-9]{5})/", $arrTemp["filename"] )) {*/
								
								$thisPathSource = $_FILES["editCustomersPrintFile"]["tmp_name"][$i];
								$thisPathTarget = DIRECTORY_UPLOAD_FILES . convertChars($_FILES["editCustomersPrintFile"]["name"][$i]);
								$thisFileTime = filemtime(stripslashes($thisPathSource));
								#$thisDocumentNumber = "KA-" . date("ym", $thisFileTime) . str_repeat("0", (6 - strlen($countFiles))) . $countFiles;
								$thisDocumentNumber = "KA-" . date('ymdHis');
								$sql_insert[] = "( 
									'KA',
									'" . $thisDocumentNumber . "',
									'".$_POST["editCustomersKundennummer2"]."',
									'".basename($thisPathTarget)."',
									'".$thisPathTarget."',
									'-',
									'".$_SESSION["usersID"]."',
									'".date("Y-m-d H:i:s", filemtime($thisPathSource))."'
								)";

								$thisDeleteFile = utf8_decode(urldecode($thisPathTarget));

								if(file_exists(DIRECTORY_UPLOAD_FILES . basename($thisDeleteFile))){
									$infoMessage .= ' Es existiert eine Datei mit diesem Namen. Diese wird &uuml;berschrieben. ' .'<br />';
									$resultUnlink = unlink(DIRECTORY_UPLOAD_FILES . basename($thisDeleteFile));
									if($resultUnlink) {
										$successMessage .= 'Die existierende Datei &quot;' . utf8_encode(basename($thisDeleteFile)) . '&quot; wurde entfernt.' . '<br />';
									}
									else {
										$errorMessage .= 'Die existierende Datei &quot;' . utf8_encode(basename($thisDeleteFile)) . '&quot; konnte leider nicht entfernt werden.' . '<br />';
									}
									$thisDeleteFile = ($_FILES["editCustomersPrintFile"]["name"][$i]);
									$thisDeleteFile = $thisPathTarget;
									$xxx_sql = "DELETE
												FROM `" . TABLE_CREATED_DOCUMENTS ."`
												WHERE `createdDocumentsFilename` = '" . DIRECTORY_UPLOAD_FILES . basename($thisDeleteFile) . "'
										";

									$sql = "DELETE
												FROM `" . TABLE_CUSTOMERS_LAYOUT_FILES ."`
												WHERE `createdDocumentsFilename` = '" . DIRECTORY_UPLOAD_FILES . basename($thisDeleteFile) . "'
										";

									$rs = $dbConnection->db_query($sql);
									if($rs){
										$successMessage .= 'Die schon existierende Datei &quot;' . basename($thisDeleteFile) . '&quot; wurde aus der Datenbank entfernt.' . '<br />';
									}
									else {
										$errorMessage .= 'Die Datei existierende &quot;' . basename($thisDeleteFile) . '&quot; konnte leider nicht aus der Datenbank entfernt werden.' . '<br />';
									}
								}

								#$resultCopy = copy($thisPathSource, DIRECTORY_UPLOAD_FILES . utf8_decode($_FILES["editCustomersPrintFile"]["name"][$i]));
								$resultCopy = copy($thisPathSource, $thisPathTarget);
								if($resultCopy) {
									$successMessage .= ' Die hochgeladenene Datei &quot;' . utf8_encode(basename($thisDeleteFile)) . '&quot; wurde ins Auftragslisten-Verzeichnis kopiert. ' .'<br />';
								}
								else {
									$errorMessage .= ' Die hochgeladenene Datei &quot;' . utf8_encode(basename($thisDeleteFile)) . '&quot; konnte nicht ins Auftragslisten-Verzeichnis kopiert werden. ' .'<br />';
								}
								$countFiles++;
								$fileIsEmpty = false;
							/*}
							else {
								$errorMessage .= ' Die hochgeladenene Datei &quot;' . utf8_encode($arrTemp["basename"]) . '&quot; enth&auml;lt kein g&uuml;ltiges Kundennummer-Muster am Anfang des Dateinamens. ' .'<br />';
							}*/
						}
						else {
							$errorMessage .= ' Die hochgeladenene Datei &quot;' . utf8_encode($arrTemp["basename"]) . '&quot; ist keine PDF-Datei. ' .'<br />';
						}
					}
					else {

					}
				}
				if(!empty($sql_insert)) {
					$xxx_sql = "INSERT INTO `" . TABLE_CREATED_DOCUMENTS ."` ( 
										`createdDocumentsType`,
										`createdDocumentsNumber`,
										`createdDocumentsCustomerNumber`,
										`createdDocumentsTitle`,
										`createdDocumentsFilename`,
										`createdDocumentsContent`,
										`createdDocumentsUserID`,
										`createdDocumentsTimeCreated`
									)
									VALUES " .	implode(',', $sql_insert);

					$sql = "INSERT INTO `" . TABLE_CUSTOMERS_LAYOUT_FILES ."` (
										 
										`createdDocumentsType`,
										`createdDocumentsNumber`,
										`createdDocumentsCustomerNumber`,
										`createdDocumentsTitle`,
										`createdDocumentsFilename`,
										`createdDocumentsContent`,
										`createdDocumentsUserID`,
										`createdDocumentsTimeCreated`
									)
									VALUES " .	implode(',', $sql_insert);

					$rs = $dbConnection->db_query($sql);

					if($rs) {
						$successMessage .= ' Die Datei wurde gespeichert. ' .'<br />';
					}
					else {
						$errorMessage .= ' Die Datei konnte nicht gespeichert werden. ' . '<br />';
					}
				}
			}
			if($fileIsEmpty) {
				$errorMessage .= ' Sie haben keine Dateien ausgew&auml;hlt. ' . '<br />';
			}
			// EOF STORE FILES
			#$_REQUEST["editID"] = $_POST["editCustomersKundennummer2"];
			$_REQUEST["editID"] = $_POST["editID"];
		}
	// BOF STORE FILES

	// BOF STORE DATAS
		if($_POST["storeDatas"] != "" || $_POST["storeDatas2"] != "") {

			$doAction = checkFormDutyFields($arrFormDutyFields);

			// BOF CHECK NAME FIELDS
				$arrCheckSum = array();

				$pattern_name = "^[ A-Za-zÄÖÜäüößáàéèó'\.\-]{1,}$";
				$pattern_salutation = "[0-9]{1,}";

				// if($_POST["editCustomersFirmenInhaberVorname"] != "" || $_POST["editCustomersFirmenInhaberNachname"] != "" || $_POST["editCustomersFirmenInhaberAnrede"] != "") {
				if($_POST["editCustomersFirmenInhaberVorname"] != "" || $_POST["editCustomersFirmenInhaberNachname"] != "" || $_POST["editCustomersFirmenInhaberAnrede"] != "") {
					#$arrCheckSum["FirmenInhaber"][] = preg_replace("/" . $pattern_name . "/", 1, $_POST["editCustomersFirmenInhaberVorname"]);
					$arrCheckSum["FirmenInhaber"][] = preg_replace("/" . $pattern_name . "/", 1, $_POST["editCustomersFirmenInhaberNachname"]);
					$arrCheckSum["FirmenInhaber"][] = preg_replace("/" . $pattern_salutation . "/", 1, $_POST["editCustomersFirmenInhaberAnrede"]);
				}

				if($_POST["editCustomersFirmenInhaber2Vorname"] != "" || $_POST["editCustomersFirmenInhaber2Nachname"] != "" || $_POST["editCustomersFirmenInhaber2Anrede"] != "") {
					#$arrCheckSum["FirmenInhaber2"][] = preg_replace("/" . $pattern_name . "/", 1, $_POST["editCustomersFirmenInhaber2Vorname"]);
					$arrCheckSum["FirmenInhaber2"][] = preg_replace("/" . $pattern_name . "/", 1, $_POST["editCustomersFirmenInhaber2Nachname"]);
					$arrCheckSum["FirmenInhaber2"][] = preg_replace("/" . $pattern_salutation . "/", 1, $_POST["editCustomersFirmenInhaber2Anrede"]);
				}

				if($_POST["editCustomersAnsprechpartner1Vorname"] != "" || $_POST["editCustomersAnsprechpartner1Nachname"] != "" || $_POST["editCustomersAnsprechpartner1Anrede"] != "") {
					#$arrCheckSum["Ansprechpartner1"][] = preg_replace("/" . $pattern_name . "/", 1, $_POST["editCustomersAnsprechpartner1Vorname"]);
					$arrCheckSum["Ansprechpartner1"][] = preg_replace("/" . $pattern_name . "/", 1, $_POST["editCustomersAnsprechpartner1Nachname"]);
					$arrCheckSum["Ansprechpartner1"][] = preg_replace("/" . $pattern_salutation . "/", 1, $_POST["editCustomersAnsprechpartner1Anrede"]);
				}

				if($_POST["editCustomersAnsprechpartner2Vorname"] != "" || $_POST["editCustomersAnsprechpartner2Nachname"] != "" || $_POST["editCustomersAnsprechpartner2Anrede"] != "") {
					#$arrCheckSum["Ansprechpartner2"][] = preg_replace("/" . $pattern_name . "/", 1, $_POST["editCustomersAnsprechpartner2Vorname"]);
					$arrCheckSum["Ansprechpartner2"][] = preg_replace("/" . $pattern_name . "/", 1, $_POST["editCustomersAnsprechpartner2Nachname"]);
					$arrCheckSum["Ansprechpartner2"][] = preg_replace("/" . $pattern_salutation . "/", 1, $_POST["editCustomersAnsprechpartner2Anrede"]);
				}

				$isCheckSumOk = true;
				if(!empty($arrCheckSum)) {
					foreach($arrCheckSum as $thisKey => $thisValue) {
						if(array_sum($thisValue)/2 != 1) {
							$isCheckSumOk = false;
						}
						// echo 'check_sum: ' . array_sum($thisValue)%3 . '<br />';
					}
				}
			// EOF CHECK NAME FIELDS


			// BOF CHECK MAIL ADDRESS
				$checkCustomerEmail_1 = true;
				$checkCustomerEmail_2 = true;
				$checkCustomerEmail = true;

				if(trim($_POST["editCustomersMail1"] != '')) {
					$_POST["editCustomersMail1"] = cleanMail($_POST["editCustomersMail1"]);
					$checkCustomerEmail_1 = checkEmailStructure($_POST["editCustomersMail1"]);
				}
				if(trim($_POST["editCustomersMail2"] != '')) {
					$_POST["editCustomersMail2"] = cleanMail($_POST["editCustomersMail2"]);
					$checkCustomerEmail_2 = checkEmailStructure($_POST["editCustomersMail2"]);
				}
				if(!$checkCustomerEmail_1 || !$checkCustomerEmail_2){
					$checkCustomerEmail = false;
				}
			// EOF CHECK MAIL ADDRESS

			// BOF CHECK IF CUSTOMER ALREADY EXISTS
				// if($_REQUEST["editID"] == "NEW") {
				if($_REQUEST["editID"] == "NEW" && $_COOKIE["isAdmin"] == '1') {

					$arrCompanyData["street"] 		= addslashes(formatStreet($_POST["editCustomersCompanyStrasse"])) . ($_POST["editCustomersCompanyHausnummer"]);
					$arrCompanyData["zipcode"]		= ($_POST["editCustomersCompanyPLZ"]);
					$arrCompanyData["countryID"]	= ($_POST["editCustomersCompanyCountry"]);
					$arrCompanyData["mail1"]		= ($_POST["editCustomersMail1"]);
					$arrCompanyData["mail2"]		= ($_POST["editCustomersMail2"]);

					$arrCheckIfCustomerAlreadyExists	= checkIfCustomerAlreadyExists($arrCompanyData);
				}
			// EOF CHECK IF CUSTOMER ALREADY EXISTS

			if($doAction && $isCheckSumOk && $checkCustomerEmail) {
				if($_POST["Rechnungsadresse_Firmenadresse"] == '1'){
					#$_POST["editCustomersRechnungsadresseKundennummer"] = $_POST["editCustomersKundennummer"];
					$_POST["editCustomersRechnungsadresseFirmenname"] = $_POST["editCustomersFirmenname"];
					$_POST["editCustomersRechnungsadresseFirmennameZusatz"] = $_POST["editCustomersFirmennameZusatz"];
					$_POST["editCustomersRechnungsadresseStrasse"] = $_POST["editCustomersCompanyStrasse"];
					$_POST["editCustomersRechnungsadresseHausnummer"] = $_POST["editCustomersCompanyHausnummer"];
					$_POST["editCustomersRechnungsadressePLZ"] = $_POST["editCustomersCompanyPLZ"];
					$_POST["editCustomersRechnungsadresseOrt"] = $_POST["editCustomersCompanyOrt"];
					$_POST["editCustomersRechnungsadresseLand"] = $_POST["editCustomersCompanyCountry"];
				}
				if($_POST["Lieferadresse_Firmenadresse"] == '1'){
					#$_POST["editCustomersLieferadresseKundennummer"] = $_POST["editCustomersKundennummer"];
					$_POST["editCustomersLieferadresseFirmenname"] = $_POST["editCustomersFirmenname"];
					$_POST["editCustomersLieferadresseFirmennameZusatz"] = $_POST["editCustomersFirmennameZusatz"];
					$_POST["editCustomersLieferadresseStrasse"] = $_POST["editCustomersCompanyStrasse"];
					$_POST["editCustomersLieferadresseHausnummer"] = $_POST["editCustomersCompanyHausnummer"];
					$_POST["editCustomersLieferadressePLZ"] = $_POST["editCustomersCompanyPLZ"];
					$_POST["editCustomersLieferadresseOrt"] = $_POST["editCustomersCompanyOrt"];
					$_POST["editCustomersLieferadresseLand"] = $_POST["editCustomersCompanyCountry"];
				}

				$thisEditCustomersKundennummer = $_POST["editCustomersKundennummer"];
				$generatedCustomerNumber = "";
				if($_POST["editCustomersID"] == "") {
					$_POST["editCustomersID"] = "%";
					// BOF CREATE COSTUMER NUMBER
					if(strlen($_POST["editCustomersKundennummer"]) < LENGTH_CUSTOMER_NUMBER) {
						$generatedCustomerNumber = generateCustomerNumber('customers', $_POST["editCustomersTyp"], $_POST["editCustomersCompanyCountry"], $_POST["editCustomersCompanyPLZ"]);

						$jswindowMessage .= 'Die generierte Kundennummer für ' . $_POST["editCustomersFirmenname"] . ' lautet: ' . $generatedCustomerNumber;

						$thisEditCustomersKundennummer = $generatedCustomerNumber;
					}
					// EOF CREATE COSTUMER NUMBER
					$sqlInsertType = "INSERT";
				}
				else {
					$sqlInsertType = "REPLACE";
				}

				if(trim($_POST["editCustomersVertreterName"]) == "") {
					$_POST["editCustomersVertreterID"] = "";
				}

				// BOF GET CUSTOMER TAX ACCOUNT NUMBER
					if($_POST["customersTaxAccountID"] == '') {
						$customersTaxAccountID = getNewCustomerTaxAccountNumber($_POST["editCustomersCompanyCountry"], $arrCountryTypeDatas[$_POST["editCustomersCompanyCountry"]]["countries_iso_code_2"], $_POST["editCustomersTyp"]);
					}
					else {
						$customersTaxAccountID = $_POST["customersTaxAccountID"];
					}
				// EOF GET CUSTOMER TAX ACCOUNT NUMBER

				if($_POST["editCustomersBadPaymentBehavior"] == '') { $_POST["editCustomersBadPaymentBehavior"] = '0';}
				if($_POST["editCustomersPaymentOnlyPrepayment"] == '') { $_POST["editCustomersPaymentOnlyPrepayment"] = '0';}

				$thisCustomersHomepage = trim($_POST["editCustomersHomepage"]);
				if($thisCustomersHomepage == ""){
					if(preg_match("/^info@/", $_POST["editCustomersMail1"])){
						$thisCustomersHomepage = str_replace("info@", "www.", $_POST["editCustomersMail1"]);
					}
					else if(preg_match("/^info@/", $_POST["editCustomersMail2"])){
						$thisCustomersHomepage = str_replace("info@", "www.", $_POST["editCustomersMail2"]);
					}
				}

				$thisCustomersHomepage = cleanUrl($thisCustomersHomepage);

				if($_POST["editCustomersCompanyNotExists"] == ''){
					$_POST["editCustomersCompanyNotExists"] = '0';
				}
				if($_POST["editCustomersPriceList"] == ''){
					$_POST["editCustomersPriceList"] = '1';
				}

				$thisEditCustomersBezahlart = $_POST["editCustomersBezahlart"];
				$thisEditCustomersZahlungskondition = $_POST["editCustomersZahlungskondition"];

				if($customerDatas["customersPaymentOnlyPrepayment"] == '1'){
					$thisEditCustomersBezahlart = '2';
					$thisEditCustomersZahlungskondition = '2';
				}

				// BOF GET GEO LOCATION
					$arrCheckGeoLocationDatas = array();
					$arrCheckGeoLocationDatas["customersCompanyPLZ"]		= $_POST["editCustomersCompanyPLZ"];
					$arrCheckGeoLocationDatas["customersCompanyOrt"]		= $_POST["editCustomersCompanyOrt"];
					$arrCheckGeoLocationDatas["customersCompanyStrasse"]	= formatStreet($_POST["editCustomersCompanyStrasse"]);
					$arrCheckGeoLocationDatas["customersCompanyHausnummer"] = $_POST["editCustomersCompanyHausnummer"];
					$arrCheckGeoLocationDatas["customersCompanyCountry"]	= $arrCountryTypeDatas[$_POST["editCustomersCompanyCountry"]]["countries_name"];

					$arrLocationDatas = getGoogleLocationDatas($arrCheckGeoLocationDatas);

					if($arrLocationDatas["LAT"] != "" && $arrLocationDatas["LNG"] != ""){
						$thisCustomersCompanyLocation = $arrLocationDatas["LAT"] . ":" . $arrLocationDatas["LNG"];
					}
					else {
						$thisCustomersCompanyLocation = $_POST["editCustomersCompanyLocation"];
					}
		// EOF GET GEO LOCATION
		// echo $_POST["editCustomersVertreterID"];exit();
		$_POST["editCustomersVertreterID"] = intval($_POST["editCustomersVertreterID"]);
				if(!(isset($_POST["editCustomersUseProductMwst"]) && in_array($_POST["editCustomersUseProductMwst"],[1,0])))
					$_POST["editCustomersUseProductMwst"] = 0;
				if(!(isset($_POST["editCustomersUseProductDiscount"]) && in_array($_POST["editCustomersUseProductDiscount"],[1,0])))
					$_POST["editCustomersUseProductDiscount"] = 0;
				if(!(isset($_POST["editCustomersUseSalesmanInvoiceAdress"]) && in_array($_POST["editCustomersUseSalesmanInvoiceAdress"],[1,0])))
					$_POST["editCustomersUseSalesmanInvoiceAdress"] = 0;
				if(!(isset($_POST["editCustomersUseSalesmanDeliveryAdress"]) && in_array($_POST["editCustomersUseSalesmanDeliveryAdress"],[1,0])))
					$_POST["editCustomersUseSalesmanDeliveryAdress"] = 0;
				if(!(isset($_POST["editCustomersInvoicesNotPurchased"]) && in_array($_POST["editCustomersInvoicesNotPurchased"],[1,0])))
					$_POST["editCustomersInvoicesNotPurchased"] = 0;
				if(!(isset($_POST["editCustomersSepaExists"]) && in_array($_POST["editCustomersSepaExists"],[1,0])))
					$_POST["editCustomersSepaExists"] = 0;
				if(!(isset($_POST["editCustomersOriginalVertreterID"]) && in_array($_POST["editCustomersOriginalVertreterID"])))
					$_POST["editCustomersOriginalVertreterID"] = 0;
				if(!(isset($_POST["editCustomersGetNoCollectedInvoice"]) && in_array($_POST["editCustomersGetNoCollectedInvoice"],[1,0])))
					$_POST["editCustomersGetNoCollectedInvoice"] = 0;
				if(!(isset($_POST["editCustomersNoDeliveries"]) && in_array($_POST["editCustomersNoDeliveries"],[1,0])))
					$_POST["editCustomersNoDeliveries"] = 0;
				if(!(isset($_POST["editCustomersSendPaperInvoice"]) && in_array($_POST["editCustomersSendPaperInvoice"],[1,0])))
					$_POST["editCustomersSendPaperInvoice"] = 0;
				if(!(isset($_POST["editCustomersSendNoFaktoringInfo"]) && in_array($_POST["editCustomersSendNoFaktoringInfo"],[1,0])))
					$_POST["editCustomersSendNoFaktoringInfo"] = 0;
				if(!(isset($_POST["editCustomersRequestOrderNumber"]) && in_array($_POST["editCustomersRequestOrderNumber"],[1,0])))
					$_POST["editCustomersRequestOrderNumber"] = 0;
				


				// echo "-".$_POST["editCustomersOriginalVertreterID"]."-";	exit();
				$sql = "
					" . $sqlInsertType . " INTO `" . TABLE_CUSTOMERS . "`(";
				if($_POST["editCustomersID"]!="%")
					$sql.=	"`customersID`,";
				$sql.=  "



						
				`customersKundennummer`,
						`customersFirmenname`,
						`customersFirmennameZusatz`,
						`customersFirmenInhaberVorname`,
						`customersFirmenInhaberNachname`,
						`customersFirmenInhaberAnrede`,
						`customersFirmenInhaber2Vorname`,
						`customersFirmenInhaber2Nachname`,
						`customersFirmenInhaber2Anrede`,
						`customersAnsprechpartner1Vorname`,
						`customersAnsprechpartner1Nachname`,
						`customersAnsprechpartner1Anrede`,
						`customersAnsprechpartner2Vorname`,
						`customersAnsprechpartner2Nachname`,
						`customersAnsprechpartner2Anrede`,
						`customersTelefon1`,
						`customersTelefon2`,
						`customersMobil1`,
						`customersMobil2`,
						`customersFax1`,
						`customersFax2`,
						`customersMail1`,
						`customersMail2`,
						`customersHomepage`,
						`customersCompanyStrasse`,
						`customersCompanyHausnummer`,
						`customersCompanyCountry`,
						`customersCompanyPLZ`,
						`customersCompanyOrt`,

						`customersLieferadresseKundennummer`,
						`customersLieferadresseFirmenname`,
						`customersLieferadresseFirmennameZusatz`,
						`customersLieferadresseStrasse`,
						`customersLieferadresseHausnummer`,
						`customersLieferadressePLZ`,
						`customersLieferadresseOrt`,
						`customersLieferadresseLand`,

						`customersRechnungsadresseKundennummer`,
						`customersRechnungsadresseFirmenname`,
						`customersRechnungsadresseFirmennameZusatz`,
						`customersRechnungsadresseStrasse`,
						`customersRechnungsadresseHausnummer`,
						`customersRechnungsadressePLZ`,
						`customersRechnungsadresseOrt`,
						`customersRechnungsadresseLand`,

						`customersKontoinhaber`,
						`customersBankName`,
						`customersBankKontonummer`,
						`customersBankLeitzahl`,
						`customersBankIBAN`,
						`customersBankBIC`,
						`customersBezahlart`,
						`customersZahlungskondition`,
						`customersRabatt`,
						`customersRabattType`,
						`customersUseProductMwst`,
						`customersUseProductDiscount`,
						`customersUstID`,
						`customersVertreterID`,
						`customersVertreterName`,
						`customersUseSalesmanDeliveryAdress`,
						`customersUseSalesmanInvoiceAdress`,
						`customersTyp`,
						`customersGruppe`,
						`customersNotiz`,
						`customersActive`,
						`customersUserID`,
						`customersTimeCreated`,
						`customersDatasUpdated`,
						`customersTaxAccountID`,
						`customersInvoicesNotPurchased`,
						`customersBadPaymentBehavior`,
						`customersEntryDate`,

						`customersSepaExists`,
						`customersSepaValidUntilDate`,
						`customersSepaRequestedDate`,
						`customersCompanyNotExists`,

						`customersCompanyLocation`,

						`customersPriceListsID`,

						`customersOriginalVertreterID`,

						`customersGetNoCollectedInvoice`,

						`customersNoDeliveries`,

						`customersSendPaperInvoice`,

						`customersSendNoFaktoringInfo`,

						`customersGroupCompanyNumber`,

						`customersPaymentOnlyPrepayment`,
						`customersRequestOrderNumber`
					)
					VALUES (";
					if ($_POST["editCustomersID"] != "%")
						$sql.="	'".$_POST["editCustomersID"]."',";
					$sql.="	'".$thisEditCustomersKundennummer."',
						'".addslashes($_POST["editCustomersFirmenname"])."',
						'".addslashes($_POST["editCustomersFirmennameZusatz"])."',
						'".addslashes($_POST["editCustomersFirmenInhaberVorname"])."',
						'".addslashes($_POST["editCustomersFirmenInhaberNachname"])."',
						'".$_POST["editCustomersFirmenInhaberAnrede"]."',
						'".addslashes($_POST["editCustomersFirmenInhaber2Vorname"])."',
						'".addslashes($_POST["editCustomersFirmenInhaber2Nachname"])."',
						'".$_POST["editCustomersFirmenInhaber2Anrede"]."',
						'".addslashes($_POST["editCustomersAnsprechpartner1Vorname"])."',
						'".addslashes($_POST["editCustomersAnsprechpartner1Nachname"])."',
						'".$_POST["editCustomersAnsprechpartner1Anrede"]."',
						'".addslashes($_POST["editCustomersAnsprechpartner2Vorname"])."',
						'".addslashes($_POST["editCustomersAnsprechpartner2Nachname"])."',
						'".$_POST["editCustomersAnsprechpartner2Anrede"]."',
						'".cleanPhoneNumbers($_POST["editCustomersTelefon1"], $_POST["editCustomersCompanyCountry"])."',
						'".cleanPhoneNumbers($_POST["editCustomersTelefon2"], $_POST["editCustomersCompanyCountry"])."',
						'".cleanPhoneNumbers($_POST["editCustomersMobil1"], $_POST["editCustomersCompanyCountry"])."',
						'".cleanPhoneNumbers($_POST["editCustomersMobil2"], $_POST["editCustomersCompanyCountry"])."',
						'".cleanPhoneNumbers($_POST["editCustomersFax1"], $_POST["editCustomersCompanyCountry"])."',
						'".cleanPhoneNumbers($_POST["editCustomersFax2"], $_POST["editCustomersCompanyCountry"])."',
						'".($_POST["editCustomersMail1"])."',
						'".($_POST["editCustomersMail2"])."',
						'". $thisCustomersHomepage ."',
						'".addslashes(formatStreet($_POST["editCustomersCompanyStrasse"]))."',
						'".($_POST["editCustomersCompanyHausnummer"])."',
						'".($_POST["editCustomersCompanyCountry"])."',
						'".($_POST["editCustomersCompanyPLZ"])."',
						'".addslashes($_POST["editCustomersCompanyOrt"])."',


						'".addslashes($_POST["customersLieferadresseKundennummer"])."',
						'".addslashes($_POST["editCustomersLieferadresseFirmenname"])."',
						'".addslashes($_POST["editCustomersLieferadresseFirmennameZusatz"])."',
						'".addslashes(formatStreet($_POST["editCustomersLieferadresseStrasse"]))."',
						'".($_POST["editCustomersLieferadresseHausnummer"])."',
						'".($_POST["editCustomersLieferadressePLZ"])."',
						'".addslashes($_POST["editCustomersLieferadresseOrt"])."',
						'".$_POST["editCustomersLieferadresseLand"]."',


						'".addslashes($_POST["customersRechnungsadresseKundennummer"])."',
						'".addslashes($_POST["editCustomersRechnungsadresseFirmenname"])."',
						'".addslashes($_POST["editCustomersRechnungsadresseFirmennameZusatz"])."',
						'".addslashes(formatStreet($_POST["editCustomersRechnungsadresseStrasse"]))."',
						'".($_POST["editCustomersRechnungsadresseHausnummer"])."',
						'".($_POST["editCustomersRechnungsadressePLZ"])."',
						'".addslashes($_POST["editCustomersRechnungsadresseOrt"])."',
						'".$_POST["editCustomersRechnungsadresseLand"]."',
						'".addslashes($_POST["editCustomersKontoinhaber"])."',
						'".addslashes($_POST["editCustomersBankName"])."',
						'".cleanChars($_POST["editCustomersBankKontonummer"])."',
						'".cleanChars($_POST["editCustomersBankLeitzahl"])."',
						'".cleanChars($_POST["editCustomersBankIBAN"])."',
						'".cleanChars($_POST["editCustomersBankBIC"])."',
						'".$thisEditCustomersBezahlart."',
						'".$thisEditCustomersZahlungskondition."',
						'".str_replace(",", ".", $_POST["editCustomersRabatt"])."',
						'".$_POST["editCustomersRabattType"]."',
						'".$_POST["editCustomersUseProductMwst"]."',
						'".$_POST["editCustomersUseProductDiscount"]."',
						'".preg_replace("/[ \-\.\/_]/", "", $_POST["editCustomersUstID"])."',
						'".$_POST["editCustomersVertreterID"]."',
						'".addslashes($_POST["editCustomersVertreterName"])."',
						'".$_POST["editCustomersUseSalesmanDeliveryAdress"]."',
						'".$_POST["editCustomersUseSalesmanInvoiceAdress"]."',
						'".$_POST["editCustomersTyp"]."',
						'".implode(';', $_POST["editCustomersGruppe"])."',
						'".addslashes($_POST["editCustomersNotiz"])."',
						'".$_POST["editCustomersActive"]."',
						'".$_SESSION["usersID"]."',
						NOW(),
						'1',
						'".($customersTaxAccountID)."',
						'".$_POST["editCustomersInvoicesNotPurchased"]."',
						'".$_POST["editCustomersBadPaymentBehavior"]."',
						'".formatDate($_POST["editCustomersEntryDate"], 'store')."',

						'".$_POST["editCustomersSepaExists"]."',
						'".formatDate($_POST["editCustomersSepaValidUntilDate"], 'store')."',
						'".formatDate($_POST["editCustomersSepaRequestedDate"], 'store')."',
						'".$_POST["editCustomersCompanyNotExists"]."',

						'".$thisCustomersCompanyLocation."',
						'".$_POST["editCustomersPriceList"]."',

						'".$_POST["editCustomersOriginalVertreterID"]."',

						'".$_POST["editCustomersGetNoCollectedInvoice"]."',

						'".$_POST["editCustomersNoDeliveries"]."',

						'".$_POST["editCustomersSendPaperInvoice"]."',

						'".$_POST["editCustomersSendNoFaktoringInfo"]."',

						'".addslashes($_POST["editCustomersGroupCompanyNumber"])."',

						'".$_POST["editCustomersPaymentOnlyPrepayment"]."',
						'".$_POST["editCustomersRequestOrderNumber"]."'
					)
				";
				$rs = $dbConnection->db_query($sql);
				
				$getInsertID = $dbConnection->db_getInsertID();

				if($rs) {
			
					$successMessage .= ' Der Kunden-Datensatz wurde gespeichert. ' .'<br />';

					// BOF STORE COMPANY RELATIONS
						if($_POST["editCustomersParentCompanyID"] != '' && $_POST["editCustomersParentCompanyNumber"] != ''){
							$sql = "
									REPLACE INTO `" . TABLE_CUSTOMERS_RELATIONS . "` (
										`customersRelationsParentCustomerID`,
										`customersRelationsParentCustomerNumber`,
										`customersRelationsChildCustomerID`,
										`customersRelationsChildCustomerNumber`
									)
									VALUES (
										'" . $_POST["editCustomersParentCompanyID"]. "',
										'" . $_POST["editCustomersParentCompanyNumber"]. "',
										'" . $_POST["editCustomersID"]. "',
										'" . $_POST["editCustomersKundennummer"]. "'
									)
								";
							$rs = $dbConnection->db_query($sql);
							if($rs) {
								$successMessage .= ' Die Kunden-Relation wurde gespeichert. ' .'<br />';
							}
							else {
								$errorMessage .= ' Die Kunden-Relation konnte nicht gespeichert werden. ' .'<br />';
							}
						}
						else if(($_POST["editCustomersParentCompanyNumber"]) == ''){
							$sql = "
									DELETE FROM `" . TABLE_CUSTOMERS_RELATIONS . "`
										WHERE 1
											AND `customersRelationsChildCustomerNumber` = '" . $_POST["editCustomersKundennummer"]. "'
								";
							$rs = $dbConnection->db_query($sql);
							if($rs) {
								#$successMessage .= ' Die Kunden-Relation wurde entfernt. ' .'<br />';
							}
							else {
								$errorMessage .= ' Die Kunden-Relation konnte nicht entfernt werden. ' .'<br />';
							}
						}
					// BOF STORE COMPANY RELATIONS
				}
				else {
					$errorMessage .= ' Der Kunden-Datensatz konnte nicht gespeichert werden. ' .'<br />';
				}

				// $_REQUEST["editID"] = $thisEditCustomersKundennummer;
				if($sqlInsertType == "INSERT") { $_REQUEST["editID"] = $getInsertID; }
				else { $_REQUEST["editID"] = $_POST["editCustomersID"]; }
			}
			else {
				$_REQUEST["editID"] = $_POST["editCustomersID"];
				if($_POST["editCustomersID"] == "") {
					$_REQUEST["editID"] = 'NEW';
				}

				if(!$doAction) {
					$errorMessage .= ' Der Datensatz konnte nicht gespeichert werden. Bitte f&uuml;llen Sie alle Pflichtfelder aus. ' .'<br />';
					$jswindowMessage .= ' Der Datensatz konnte nicht gespeichert werden. Bitte f&uuml;llen Sie alle Pflichtfelder aus. ' .'<br />';
				}
				if(!$isCheckSumOk) {
					$errorMessage .= ' Der Datensatz konnte nicht gespeichert werden. Bitte geben Sie die Namen korrekt ein. ' .'<br />';
					$jswindowMessage .= ' Der Datensatz konnte nicht gespeichert werden. Bitte geben Sie die Namen korrekt ein. ' .'<br />';
				}
				if(!$checkCustomerEmail) {
					$errorMessage .= ' Die eingegebene Mail-Adresse ist nicht korrekt.' . '<br />';
					$jswindowMessage .= ' Die eingegebene Mail-Adresse ist nicht korrekt.' . '<br />';
				}

				if(!$isCheckSumOk || !$checkCustomerEmail) {
					#$customerDatas["customersFirmenname"]
					foreach($_REQUEST as $thisRequestKey => $thisRequestValue){
						if(preg_match("/^editCustomers/", $thisRequestKey)){
							$newRequestKey = preg_replace("/^editCustomers/", "customers", $thisRequestKey);
							#dd('newRequestKey');
							$customerDatas[$newRequestKey] = ($thisRequestValue);
						}
					}
				}
			}
		}
	// EOF STORE DATAS

	// BOF DELETE DATAS
		if($_POST["deleteDatas"] != "") {
			// BOF DELETE FROM CUSTOMERS
				$sql = "DELETE FROM `" . TABLE_CUSTOMERS . "` WHERE `customersID` = '".$_POST["editCustomersID"]."'";

				$rs = $dbConnection->db_query($sql);
			// EOF DELETE FROM CUSTOMERS

			if($rs) {
				$successMessage .= ' Der Kunden-Datensatz wurde endgültig gelöscht. '.'<br />';

				// BOF DELETE FROM CUSTOMERS RELATIONS
					$sql = "
							DELETE FROM `" . TABLE_CUSTOMERS_RELATIONS . "`
								WHERE 1
									AND `customersRelationsChildCustomerNumber` = '" . $_POST["editCustomersKundennummer"]. "'
						";
					$rs = $dbConnection->db_query($sql);
					if($rs) {
						#$successMessage .= ' Die Kunden-Relation wurde entfernt. ' .'<br />';
					}
					else {
						$errorMessage .= ' Die Kunden-Relation konnte nicht entfernt werden. ' .'<br />';
					}
				// EOF DELETE FROM CUSTOMERS RELATIONS

				// BOF DELETE FROM SALESMEN
					$sql = "DELETE FROM `bctr_salesmen`
								WHERE 1
									AND `salesmenKundennummer` = '" . $_POST["editCustomersKundennummer"]. "'

						";
					$rs = $dbConnection->db_query($sql);
					if($rs) {
						#$successMessage .= ' Der Kunde wurde aus den Vertreterlisten entfernt. ' .'<br />';
					}
					else {
						#$errorMessage .= ' Der Kunde konnte nicht aus den Vertreterlisten entfernt werden. ' .'<br />';
					}
				// EOF DELETE FROM SALESMEN

				// BOF USE ZIPCODE FOR NEW RESULT
					// $_REQUEST["searchPLZ"] = $_REQUEST["editCustomersCompanyPLZ"];
				// EOF USE ZIPCODE FOR NEW RESULT

				// BOF DELETE FROM PHONE MARKETING
					$sql = "
							DELETE FROM `" . TABLE_CUSTOMERS_PHONE_MARKETING . "`
								WHERE 1
									AND `customersPhoneMarketingCustomerNumber` = '" . $_POST["editCustomersKundennummer"]. "'
						";
					$rs = $dbConnection->db_query($sql);
					if($rs) {
						#$successMessage .= ' Die Kunden-Marketing-Daten wurden entfernt. ' .'<br />';
					}
					else {
						#$errorMessage .= ' Die Kunden-Marketing-Daten konnten nicht entfernt werden. ' .'<br />';
					}
				// EOF DELETE FROM PHONE MARKETING

			}
			else {
				$errorMessage .= ' Der Kunden-Datensatz konnte nicht gelöscht werden. '.'<br />';
			}
		}
	// EOF DELETE DATAS

	// BOF STORE CALENDAR DATAS
		if($_POST["storeCalendar"] != "" && !empty($_POST["addCustomerCalendarDates"])){
			if($_POST["addCustomerCalendarDates"][0]["DESCRIPTION"] != '' && $_POST["addCustomerCalendarDates"][0]["DATE"] != ''){

				$arrNewCustomerCalendarDates = array();
				$selectedAddCalendarDate = formatDate($_POST["addCustomerCalendarDates"][0]["DATE"], 'store');

				if($_POST["addCustomerCalendarDatesInterval"] != ''){
					// BOF GET DATES FROM SELECTED DATE PER INTERVAL FOR ONE YEAR
						$maxAddCalendarInterval = 1; // YEAR

						$selectedAddCalendarDateTime = strtotime($selectedAddCalendarDate);
						$maxAddCalendarDate = date("Y-m-d", mktime(0, 0, 0, date("m", $selectedAddCalendarDateTime), date("d", $selectedAddCalendarDateTime), (date("Y", $selectedAddCalendarDateTime) + $maxAddCalendarInterval)));

						$thisAddCalendarDate = $selectedAddCalendarDate;
						$countAddCalendarInterval = 0;

						while($thisAddCalendarDate < $maxAddCalendarDate){
							$thisAddCalendarDate = date("Y-m-d", mktime(0, 0, 0, (date("m", $selectedAddCalendarDateTime) + ($_POST["addCustomerCalendarDatesInterval"] * $countAddCalendarInterval)), date("d", $selectedAddCalendarDateTime), date("Y", $selectedAddCalendarDateTime)));
							$arrNewCustomerCalendarDates[] = $thisAddCalendarDate;

							$countAddCalendarInterval++;
							if($countAddCalendarInterval < 1){
								break;
							}
						}
					// EOF GET DATES FROM SELECTED DATE PER INTERVAL FOR ONE YEAR
				}
				else {
					$arrNewCustomerCalendarDates[] = $selectedAddCalendarDate;
				}

				$arrSqlNewCustomerCalendarDates = array();

				if(!empty($arrNewCustomerCalendarDates)){
					$arrNewCustomerCalendarDates = array_unique($arrNewCustomerCalendarDates);
					foreach($arrNewCustomerCalendarDates as $thisNewCustomerCalendarDate){
						$arrSqlNewCustomerCalendarDates[] = "
								(
									'%',
									'" . $_POST["addCustomerCalendarDates"][0]["CUSTOMERID"] . "',
									'" . $_POST["addCustomerCalendarDates"][0]["CUSTOMERNUMBER"] . "',
									'" . $thisNewCustomerCalendarDate . "',
									'0',
									'none',
									'0',
									'" . addslashes($_POST["addCustomerCalendarDates"][0]["DESCRIPTION"]) . "',
									'0',
									'".$_SESSION["usersID"]."'
								)
							";
					}


					$sql = "
							INSERT INTO `" . TABLE_CUSTOMERS_CALENDAR . "` (
														
														`customersCalendarCustomersID`,
														`customersCalendarCustomersNumber`,
														`customersCalendarDate`,
														`customersCalendarDateRepeat`,
														`customersCalendarRepeatType`,
														`customersCalendarRepeatValue`,
														`customersCalendarDescription`,
														`customersCalendarReminderMailSended`,
														`customersCalendarUserID`
													)
													VALUES
													" . implode(", ", $arrSqlNewCustomerCalendarDates) . "";

				}

				
					$sql = "
						INSERT INTO `" . TABLE_CUSTOMERS_CALENDAR . "` (
													
													`customersCalendarCustomersID`,
													`customersCalendarCustomersNumber`,
													`customersCalendarDate`,
													`customersCalendarDateRepeat`,
													`customersCalendarRepeatType`,
													`customersCalendarRepeatValue`,
													`customersCalendarDescription`,
													`customersCalendarReminderMailSended`,
													`customersCalendarUserID`
												)
												VALUES (
													
													'" . $_POST["addCustomerCalendarDates"][0]["CUSTOMERID"] . "',
													'" . $_POST["addCustomerCalendarDates"][0]["CUSTOMERNUMBER"] . "',
													'" . formatDate($_POST["addCustomerCalendarDates"][0]["DATE"], 'store') . "',
													'0',
													'none',
													'0',
													'" . addslashes($_POST["addCustomerCalendarDates"][0]["DESCRIPTION"]) . "',
													'0',
													'".$_SESSION["usersID"]."'
												)
						";
				

				$rs = $dbConnection->db_query($sql);

				if($rs){
					$successMessage .= 'Die Termin-Daten wurden gespeichert.' . '<br />';
					unset($_POST["addCustomerCalendarDates"]);
				}
				else {
					$errorMessage .= 'Die Termin-Daten konnten nicht gespeichert werden.' . '<br />';
				}
			}
			else {
				$errorMessage .= 'Sie haben die Termin-Daten nicht korrekt eingegeben.' . '<br />';
			}
		}
	// EOF STORE CALENDAR DATAS

	// BOF STORE CONTACT DATAS
		if($_POST["storeContacts"] != ""){
			if(!empty($_POST["editCustomerContacts"])){
				// BOF STORE EDITED CONTACT DATAS
					$arrSql = array();
					foreach($_POST["editCustomerContacts"] as $thisKey => $thisValue){
						$arrSql[] = "
									(
										'" . $_POST["editCustomerContacts"][$thisKey]["ID"] . "',
										'" . $_POST["editCustomerContacts"][$thisKey]["ContactID"] . "',
										'" . $_POST["editCustomerContacts"][$thisKey]["Salutation"] . "',
										'" . $_POST["editCustomerContacts"][$thisKey]["FirstName"] . "',
										'" . $_POST["editCustomerContacts"][$thisKey]["SecondName"] . "',
										'" . $_POST["editCustomerContacts"][$thisKey]["Function"] . "',
										'" . $_POST["editCustomerContacts"][$thisKey]["Phone"] . "',
										'" . $_POST["editCustomerContacts"][$thisKey]["Fax"] . "',
										'" . $_POST["editCustomerContacts"][$thisKey]["Mobile"] . "',
										'" . $_POST["editCustomerContacts"][$thisKey]["Mail"] . "',
										'1'
									)
							";
					}
			/*if(!empty($arrSql)){
					$sql = "
						INSERT INTO `" . TABLE_CUSTOMERS_CONTACTS. "`(	
												`customersContactsID`,
												`customersContactsContactID`,
												`customersContactsSalutation`,
												`customersContactsFirstName`,
												`customersContactsSecondName`,
												`customersContactsFunction`,
												`customersContactsPhone`,
												`customersContactsMobile`,
												`customersContactsFax`,
												`customersContactsMail`,
												`customersContactsActive`

											)
											VALUES
							";
						$sql .= implode(", ", $arrSql);
						$rs = $dbConnection->db_query($sql);

						if($rs){
							$successMessage .= 'Die Kontaktdaten wurden aktualisiert.' . '<br />';
						}
						else {
							$errorMessage .= 'Die Kontaktdaten konnten nicht aktualisiert werden.' . '<br />';
						}
					}*/
				}
			// EOF STORE EDITED CONTACT DATAS

			// BOF STORE NEW CONTACT DATAS
				$arrSql = array();
				foreach($_POST["addCustomerContacts"] as $thisKey => $thisValue){
					if($_POST["addCustomerContacts"][$thisKey]["SecondName"] != ""){
						$arrSql[] = "
								(
									'" . $_POST["addCustomerContacts"][$thisKey]["ContactID"] . "',
									'" . $_POST["addCustomerContacts"][$thisKey]["Salutation"] . "',
									'" . $_POST["addCustomerContacts"][$thisKey]["FirstName"] . "',
									'" . $_POST["addCustomerContacts"][$thisKey]["SecondName"] . "',
									'" . $_POST["addCustomerContacts"][$thisKey]["Function"] . "',
									'" . $_POST["addCustomerContacts"][$thisKey]["Phone"] . "',
									'" . $_POST["addCustomerContacts"][$thisKey]["Fax"] . "',
									'" . $_POST["addCustomerContacts"][$thisKey]["Mobile"] . "',
									'" . $_POST["addCustomerContacts"][$thisKey]["Mail"] . "',
									'1'
								)
						";
					}
				}
				if(!empty($arrSql)){
					$sql = "
						INSERT INTO `" . TABLE_CUSTOMERS_CONTACTS. "`(
											`customersContactsContactID`,
											`customersContactsSalutation`,
											`customersContactsFirstName`,
											`customersContactsSecondName`,
											`customersContactsFunction`,
											`customersContactsPhone`,
											`customersContactsMobile`,
											`customersContactsFax`,
											`customersContactsMail`,
											`customersContactsActive`
										)
										VALUES
						";
					$sql .= implode(", ", $arrSql);
					$rs = $dbConnection->db_query($sql);

					if($rs){
						$successMessage .= 'Die Kontaktdaten wurden gespeichert.' . '<br />';
					}
					else {
						$errorMessage .= 'Die Kontaktdaten konnten nicht gespeichert werden.' . '<br />';
					}
				}

			// EOF STORE NEW CONTACT DATAS
		}
	// EOF STORE CONTACT DATAS

	// BOF DELETE CONTACT DATAS
		if((int)$_REQUEST["deleteContactsID"] > 0){
			$sql = "DELETE FROM `" . TABLE_CUSTOMERS_CONTACTS. "` WHERE `customersContactsID` = '" . $_REQUEST["deleteContactsID"] . "'";

			$rs = $dbConnection->db_query($sql);

			if($rs){
				$successMessage .= 'Der Kontakt wurde entfernt.' . '<br />';
			}
			else {
				$errorMessage .= 'Der Kontakt konnte nicht entfernt werden.' . '<br />';
			}
		}
	// EOF DELETE CONTACT DATAS

	// BOF DELETE PHONE MARKTING ITEM
		if($_GET["deletePhoneMarketingItem"] > 0 && $_GET["editID"] > 0){
			$sql = "DELETE FROM `" . TABLE_CUSTOMERS_PHONE_MARKETING. "` WHERE `customersPhoneMarketingID` = '" . $_REQUEST["deletePhoneMarketingItem"] . "' AND `customersPhoneMarketingCustomerID` = '" . $_REQUEST["editID"] . "'";

			$rs = $dbConnection->db_query($sql);

			if($rs){
				$successMessage .= 'Der Termin wurde entfernt.' . '<br />';
			}
			else {
				$errorMessage .= 'Der Termin konnte nicht entfernt werden.' . '<br />';
			}
		}
	// EOF DELETE PHONE MARKTING ITEM

	// BOF DELETE CALENDAR DATAS
		if((int)$_REQUEST["deleteCustomerCalendarID"] > 0){
			$sql = "DELETE FROM `" . TABLE_CUSTOMERS_CALENDAR. "` WHERE `customersCalendarID` = '" . $_REQUEST["deleteCustomerCalendarID"] . "'";

			$rs = $dbConnection->db_query($sql);

			if($rs){
				$successMessage .= 'Der Termin wurde entfernt.' . '<br />';
			}
			else {
				$errorMessage .= 'Der Termin konnte nicht entfernt werden.' . '<br />';
			}
		}
	// EOF DELETE CALENDAR DATAS

	// BOF READ ALL DATAS
		$where = "";

		if($_REQUEST["editCustomerNumber"] != "") {
			$where = " AND `customersKundennummer` = '" . $_REQUEST["editCustomerNumber"] . "' ";
			// $where = " AND `customersKundennummer` LIKE '" . $_REQUEST["editCustomerNumber"] . "%' ";
			$_REQUEST["editID"] = "";
		}
		else if($_REQUEST["searchCustomerNumber"] != "") {
			// $where = " AND `customersKundennummer` = '" . $_REQUEST["searchCustomerNumber"] . "' ";
			$where = " AND `customersKundennummer` LIKE '" . $_REQUEST["searchCustomerNumber"] . "%' ";
			$_REQUEST["editID"] = "";
		}
		else if($_REQUEST["searchCustomerName"] != "") {
			$where = "
				AND (
					`customersFirmenname` LIKE '%" . addslashes($_REQUEST["searchCustomerName"]) . "%'
					OR
					`customersFirmenInhaberNachname` LIKE '" . addslashes($_REQUEST["searchCustomerName"]) . "%'
					OR
					`customersFirmenInhaber2Nachname` LIKE '" . addslashes($_REQUEST["searchCustomerName"]) . "%'
				)

				";
			$_REQUEST["editID"] = "";
		}

		else if($_REQUEST["searchPLZ"] != "") {
			$where = " AND (
						`customersLieferadressePLZ` LIKE '" . $_REQUEST["searchPLZ"] . "%'
						OR
						`customersRechnungsadressePLZ` LIKE '" . $_REQUEST["searchPLZ"] . "%'
						OR
						`customersCompanyPLZ` LIKE '" . $_REQUEST["searchPLZ"] . "%'
				)
			";
			$_REQUEST["editID"] = "";
		}
		else if($_REQUEST["searchFIBU"] != "") {
			$where = " AND `customersTaxAccountID` = '" . $_REQUEST["searchFIBU"] . "%' ";
			$_REQUEST["editID"] = "";
		}

		else if($_REQUEST["searchNoMail"] == "1") {
			$where = " AND (
							`customersMail1` = ''
							AND	`customersMail2` = ''
						)
				";
			$_REQUEST["editID"] = "";
		}

		else if($_REQUEST["searchWord"] != "") {
			$where = " AND (
							`customersLieferadressePLZ` LIKE '" . $_REQUEST["searchWord"] . "%'
							OR
							`customersRechnungsadressePLZ` LIKE '" . $_REQUEST["searchWord"] . "%'
							OR
							`customersCompanyPLZ` LIKE '" . $_REQUEST["searchWord"] . "%'
							OR
							`customersLieferadresseOrt` LIKE '%" . addslashes($_REQUEST["searchWord"]) . "%'
							OR
							`customersRechnungsadresseOrt` LIKE '%" . addslashes($_REQUEST["searchWord"]) . "%'
							OR
							`customersCompanyOrt` LIKE '%" . addslashes($_REQUEST["searchWord"]) . "%'
							OR
							`customersKundennummer` LIKE '" . $_REQUEST["searchWord"] . "%'
							OR
							`customersFirmenname` LIKE '%" . addslashes($_REQUEST["searchWord"]) . "%'
							OR
							`customersFirmennameZusatz` LIKE '%" . addslashes($_REQUEST["searchWord"]) . "%'
							OR
							`customersMail1` LIKE '%" . $_REQUEST["searchWord"] . "%'
							OR
							`customersMail2` LIKE '%" . $_REQUEST["searchWord"] . "%'
							OR
							`customersFirmenInhaberNachname` LIKE '%" . addslashes($_REQUEST["searchWord"]) . "%'
							OR
							`customersFirmenInhaber2Nachname` LIKE '%" . addslashes($_REQUEST["searchWord"]) . "%'
							OR
							`customersFirmenInhaberVorname` LIKE '%" . addslashes($_REQUEST["searchWord"]) . "%'
							OR
							`customersFirmenInhaber2Vorname` LIKE '%" . addslashes($_REQUEST["searchWord"]) . "%'
							OR
							`customersGroupCompanyNumber` LIKE '%" . addslashes($_REQUEST["searchWord"]) . "%'
						) ";
			$_REQUEST["editID"] = "";
		}
		else if ($_GET["searchBoxCustomer"] != "") {
			$where = " AND (
							`customersKundennummer` LIKE '" . addslashes($_GET["searchBoxCustomer"]) . "%'
							OR
							`customersFirmenname` LIKE '%" . addslashes($_GET["searchBoxCustomer"]) . "%'
							OR
							`customersFirmennameZusatz` LIKE '%" . addslashes($_GET["searchBoxCustomer"]) . "%'
							OR
							`customersCompanyPLZ` LIKE '" . addslashes($_GET["searchBoxCustomer"]) . "%'
							OR
							`customersFirmenInhaberNachname` LIKE '%" . addslashes($_GET["searchBoxCustomer"]) . "%'
							OR
							`customersFirmenInhaber2Nachname` LIKE '%" . addslashes($_GET["searchBoxCustomer"]) . "%'
							OR
							`customersFirmenInhaberVorname` LIKE '%" . addslashes($_GET["searchBoxCustomer"]) . "%'
							OR
							`customersFirmenInhaber2Vorname` LIKE '%" . addslashes($_GET["searchBoxCustomer"]) . "%'
							OR
							`customersMail1` LIKE '%" . addslashes($_GET["searchBoxCustomer"]) . "%'
							OR
							`customersMail2` LIKE '%" . addslashes($_GET["searchBoxCustomer"]) . "%'
							OR
							`customersGroupCompanyNumber` LIKE '%" . addslashes($_GET["searchBoxCustomer"]) . "%'
							OR
							`customersTaxAccountID` = '" . addslashes($_GET["searchBoxCustomer"]) . "%'

						) ";

			$_REQUEST["editID"] = "";
		}
		else if($_REQUEST["searchCustomerGroup"] != "") {
			$xxx_where = " AND (
					`customersGruppe` = '" . $_REQUEST["searchCustomerGroup"] . "'
					OR
					`customersGruppe` LIKE '" . $_REQUEST["searchCustomerGroup"] . ";%'
					OR
					`customersGruppe` LIKE '%;" . $_REQUEST["searchCustomerGroup"] . "'
					OR
					`customersGruppe` LIKE '%;" . $_REQUEST["searchCustomerGroup"] . ";%'
				)
			";

			$where = " AND ( FIND_IN_SET('" . $_REQUEST["searchCustomerGroup"] . "', REPLACE(`customersGruppe`, ';', ',')) > 0 ) ";

			if($_REQUEST["searchCustomerCountry"] != "") {
				$where .= " AND `customersCompanyCountry` = '" . $_GET["searchCustomerCountry"] . "'";
			}

			$_REQUEST["editID"] = "";
		}
		else if($_REQUEST["searchCustomerType"] != "") {
			$where = " AND (
					`customersTyp` = '" . $_REQUEST["searchCustomerType"] . "'
					OR
					`customersTyp` LIKE '" . $_REQUEST["searchCustomerType"] . ";%'
					OR
					`customersTyp` LIKE '%;" . $_REQUEST["searchCustomerType"] . "'
					OR
					`customersTyp` LIKE '%;" . $_REQUEST["searchCustomerType"] . ";%'
				)
			";
			$_REQUEST["editID"] = "";
		}
		else if($_REQUEST["searchCustomersActive"] != "") {
			if($_REQUEST["searchCustomersActive"] == "1") {
				$where = " AND `customersActive` = '1' ";
			}
			else {
				$where = " AND `customersActive` != '1' ";
			}
		}

		if($_REQUEST["editID"] == "") {

			$sql = "SELECT
						/* SQL_CALC_FOUND_ROWS */

						`customersID`,
						`customersKundennummer`,
						`customersFirmenname`,
						`customersFirmennameZusatz`,
						`customersFirmenInhaberVorname`,
						`customersFirmenInhaberNachname`,
						`customersFirmenInhaberAnrede`,
						`customersFirmenInhaber2Vorname`,
						`customersFirmenInhaber2Nachname`,
						`customersFirmenInhaber2Anrede`,
						`customersAnsprechpartner1Vorname`,
						`customersAnsprechpartner1Nachname`,
						`customersAnsprechpartner1Anrede`,
						`customersAnsprechpartner2Vorname`,
						`customersAnsprechpartner2Nachname`,
						`customersAnsprechpartner2Anrede`,
						`customersTelefon1`,
						`customersTelefon2`,
						`customersMobil1`,
						`customersMobil2`,
						`customersFax1`,
						`customersFax2`,
						`customersMail1`,
						`customersMail2`,
						`customersHomepage`,
						`customersCompanyStrasse`,
						`customersCompanyHausnummer`,
						`customersCompanyCountry`,
						`customersCompanyPLZ`,
						`customersCompanyOrt`,

						`customersLieferadresseKundennummer`,
						`customersLieferadresseFirmenname`,
						`customersLieferadresseFirmennameZusatz`,
						`customersLieferadresseStrasse`,
						`customersLieferadresseHausnummer`,
						`customersLieferadressePLZ`,
						`customersLieferadresseOrt`,
						`customersLieferadresseLand`,

						`customersRechnungsadresseKundennummer`,
						`customersRechnungsadresseFirmenname`,
						`customersRechnungsadresseFirmennameZusatz`,
						`customersRechnungsadresseStrasse`,
						`customersRechnungsadresseHausnummer`,
						`customersRechnungsadressePLZ`,
						`customersRechnungsadresseOrt`,
						`customersRechnungsadresseLand`,
						`customersKontoinhaber`,
						`customersBankName`,
						`customersBankKontonummer`,
						`customersBankLeitzahl`,
						`customersBankIBAN`,
						`customersBankBIC`,
						`customersBezahlart`,
						`customersZahlungskondition`,
						`customersRabatt`,
						`customersRabattType`,
						`customersUseProductMwst`,
						`customersUseProductDiscount`,
						`customersUstID`,
						`customersVertreterID`,
						`customersVertreterName`,
						`customersUseSalesmanDeliveryAdress`,
						`customersUseSalesmanInvoiceAdress`,
						`customersTyp`,
						`customersGruppe`,
						`customersNotiz`,
						`customersActive`,
						`customersTaxAccountID`,
						`customersCompanyNotExists`,
						`customersCompanyLocation`,
						`customersIsVisibleForUserID`,
						`customersPriceListsID`,

						`customersOriginalVertreterID`,

						`customersGetNoCollectedInvoice`,

						`customersGroupCompanyNumber`

						FROM `" . TABLE_CUSTOMERS . "`

						WHERE 1
					";
			$sql .= $where;

			$sql .= "
						ORDER BY
							/*
							`customersKundennummer`
							*/
							LENGTH(`customersKundennummer`) ASC,
							`customersCompanyPLZ` ASC,
							`customersCompanyStrasse` ASC,
							`customersCompanyHausnummer` ASC
				";

			// BOF GET COUNT ALL ROWS
				$sql_getAllRows = "
						SELECT

							COUNT(`" . TABLE_CUSTOMERS . "`.`customersKundennummer`) AS `countAllRows`

						FROM `" . TABLE_CUSTOMERS . "`

						WHERE 1
							" . $where . "
					";

				$rs_getAllRows = $dbConnection->db_query($sql_getAllRows);
				list(
					$countAllRows
				) = mysqli_fetch_array($rs_getAllRows);
			// EOF GET COUNT ALL ROWS

			if($_REQUEST["page"] == "" || $_REQUEST["page"] == 0) {
				$_REQUEST["page"] = 1;
			}

			if(MAX_CUSTOMERS_PER_PAGE > 0) {
				$sql .= " LIMIT " . (($_REQUEST["page"] - 1) * MAX_CUSTOMERS_PER_PAGE) . ", " . MAX_CUSTOMERS_PER_PAGE." ";
			}

			$rs = $dbConnection->db_query($sql);

			// OLD BOF GET ALL ROWS
			#$sql_totalRows = "SELECT FOUND_ROWS() AS `countRows`";
			#$rs_totalRows = $dbConnection->db_query($sql_totalRows);
			#list($thisNumRows) = mysqli_fetch_array($rs_totalRows);
			// OLD EOF GET ALL ROWS

			$thisNumRows = $countAllRows;

			if($thisNumRows == 1) {
				while($ds = mysqli_fetch_assoc($rs)){
					$_REQUEST["editID"] = $ds["customersID"];
				}
			}
			else if($thisNumRows > 1) {
				$pagesCount = ceil($thisNumRows / MAX_CUSTOMERS_PER_PAGE);

				$arrCustomerDatas = array();
				while($ds = mysqli_fetch_assoc($rs)) {
					foreach(array_keys($ds) as $field) {
						$arrCustomerDatas[$ds["customersID"]][$field] = $ds[$field];
					}
				}
			}
			else if($thisNumRows < 1) {
				$warningMessage .= ' Es wurden keine Daten gefunden!' . '<br />';
			}
		}
	// EOF READ ALL DATAS

	// BOF READ SELECTED DATAS
		#if($_REQUEST["editID"] != "") {
		if($_REQUEST["editID"] != "" && $_REQUEST["editID"] != "NEW") {
			$sql = "SELECT
						`customersID`,
						`customersKundennummer`,
						`customersFirmenname`,
						`customersFirmennameZusatz`,
						`customersFirmenInhaberVorname`,
						`customersFirmenInhaberNachname`,
						`customersFirmenInhaberAnrede`,
						`customersFirmenInhaber2Vorname`,
						`customersFirmenInhaber2Nachname`,
						`customersFirmenInhaber2Anrede`,
						`customersAnsprechpartner1Vorname`,
						`customersAnsprechpartner1Nachname`,
						`customersAnsprechpartner1Anrede`,
						`customersAnsprechpartner2Vorname`,
						`customersAnsprechpartner2Nachname`,
						`customersAnsprechpartner2Anrede`,
						`customersTelefon1`,
						`customersTelefon2`,
						`customersMobil1`,
						`customersMobil2`,
						`customersFax1`,
						`customersFax2`,
						`customersMail1`,
						`customersMail2`,
						`customersHomepage`,
						`customersCompanyStrasse`,
						`customersCompanyHausnummer`,
						`customersCompanyCountry`,
						`customersCompanyPLZ`,
						`customersCompanyOrt`,

						`customersLieferadresseKundennummer`,
						`customersLieferadresseFirmenname`,
						`customersLieferadresseFirmennameZusatz`,
						`customersLieferadresseStrasse`,
						`customersLieferadresseHausnummer`,
						`customersLieferadressePLZ`,
						`customersLieferadresseOrt`,
						`customersLieferadresseLand`,

						`customersRechnungsadresseKundennummer`,
						`customersRechnungsadresseFirmenname`,
						`customersRechnungsadresseFirmennameZusatz`,
						`customersRechnungsadresseStrasse`,
						`customersRechnungsadresseHausnummer`,
						`customersRechnungsadressePLZ`,
						`customersRechnungsadresseOrt`,
						`customersRechnungsadresseLand`,
						`customersKontoinhaber`,
						`customersBankName`,
						`customersBankKontonummer`,
						`customersBankLeitzahl`,
						`customersBankIBAN`,
						`customersBankBIC`,
						`customersBezahlart`,
						`customersZahlungskondition`,
						`customersRabatt`,
						`customersRabattType`,
						`customersUseProductMwst`,
						`customersUseProductDiscount`,
						`customersUstID`,
						`customersVertreterID`,
						`customersVertreterName`,
						`customersUseSalesmanDeliveryAdress`,
						`customersUseSalesmanInvoiceAdress`,
						`customersTyp`,
						`customersGruppe`,
						`customersNotiz`,
						`customersActive`,
						`customersDatasUpdated`,
						`customersTaxAccountID`,

						`customersInvoicesNotPurchased`,
						`customersBadPaymentBehavior`,
						`customersEntryDate`,

						`customersSepaExists`,
						`customersSepaValidUntilDate`,
						`customersSepaRequestedDate`,

						`customersCompanyNotExists`,

						`customersCompanyLocation`,
						`customersIsVisibleForUserID`,
						`customersPriceListsID`,

						`customersOriginalVertreterID`,

						`customersGetNoCollectedInvoice`,

						`customersNoDeliveries`,

						`customersSendPaperInvoice`,

						`customersSendNoFaktoringInfo`,

						`customersGroupCompanyNumber`,

						`customersPaymentOnlyPrepayment`,
						`customersRequestOrderNumber`

						FROM `" . TABLE_CUSTOMERS . "`

						WHERE 1
							AND `customersID` = '".$_REQUEST["editID"]."'
				";
						// WHERE `customersID` = '".$_REQUEST["editID"]."'

				$rs = $dbConnection->db_query($sql);

				$foundCustomer = $dbConnection->db_getMysqlNumRows($rs);
				
				if($foundCustomer == 0){
					$_REQUEST["editID"] = '';
					header('location: ' . PAGE_EDIT_CUSTOMER);
				}
				
				
				$customerDatas = array();
				list(
					$customerDatas["customersID"],
					$customerDatas["customersKundennummer"],
					$customerDatas["customersFirmenname"],
					$customerDatas["customersFirmennameZusatz"],
					$customerDatas["customersFirmenInhaberVorname"],
					$customerDatas["customersFirmenInhaberNachname"],
					$customerDatas["customersFirmenInhaberAnrede"],
					$customerDatas["customersFirmenInhaber2Vorname"],
					$customerDatas["customersFirmenInhaber2Nachname"],
					$customerDatas["customersFirmenInhaber2Anrede"],
					$customerDatas["customersAnsprechpartner1Vorname"],
					$customerDatas["customersAnsprechpartner1Nachname"],
					$customerDatas["customersAnsprechpartner1Anrede"],
					$customerDatas["customersAnsprechpartner2Vorname"],
					$customerDatas["customersAnsprechpartner2Nachname"],
					$customerDatas["customersAnsprechpartner2Anrede"],
					$customerDatas["customersTelefon1"],
					$customerDatas["customersTelefon2"],
					$customerDatas["customersMobil1"],
					$customerDatas["customersMobil2"],
					$customerDatas["customersFax1"],
					$customerDatas["customersFax2"],
					$customerDatas["customersMail1"],
					$customerDatas["customersMail2"],
					$customerDatas["customersHomepage"],
					$customerDatas["customersCompanyStrasse"],
					$customerDatas["customersCompanyHausnummer"],
					$customerDatas["customersCompanyCountry"],
					$customerDatas["customersCompanyPLZ"],
					$customerDatas["customersCompanyOrt"],

					$customerDatas["customersLieferadresseKundennummer"],
					$customerDatas["customersLieferadresseFirmenname"],
					$customerDatas["customersLieferadresseFirmennameZusatz"],
					$customerDatas["customersLieferadresseStrasse"],
					$customerDatas["customersLieferadresseHausnummer"],
					$customerDatas["customersLieferadressePLZ"],
					$customerDatas["customersLieferadresseOrt"],
					$customerDatas["customersLieferadresseLand"],

					$customerDatas["customersRechnungsadresseKundennummer"],
					$customerDatas["customersRechnungsadresseFirmenname"],
					$customerDatas["customersRechnungsadresseFirmennameZusatz"],
					$customerDatas["customersRechnungsadresseStrasse"],
					$customerDatas["customersRechnungsadresseHausnummer"],
					$customerDatas["customersRechnungsadressePLZ"],
					$customerDatas["customersRechnungsadresseOrt"],
					$customerDatas["customersRechnungsadresseLand"],
					$customerDatas["customersKontoinhaber"],
					$customerDatas["customersBankName"],
					$customerDatas["customersBankKontonummer"],
					$customerDatas["customersBankLeitzahl"],
					$customerDatas["customersBankIBAN"],
					$customerDatas["customersBankBIC"],
					$customerDatas["customersBezahlart"],
					$customerDatas["customersZahlungskondition"],
					$customerDatas["customersRabatt"],
					$customerDatas["customersRabattType"],
					$customerDatas["customersUseProductMwst"],
					$customerDatas["customersUseProductDiscount"],
					$customerDatas["customersUstID"],
					$customerDatas["customersVertreterID"],
					$customerDatas["customersVertreterName"],
					$customerDatas["customersUseSalesmanDeliveryAdress"],
					$customerDatas["customersUseSalesmanInvoiceAdress"],
					$customerDatas["customersTyp"],
					$customerDatas["customersGruppe"],
					$customerDatas["customersNotiz"],
					$customerDatas["customersActive"],
					$customerDatas["customersDatasUpdated"],
					$customerDatas["customersTaxAccountID"],
					$customerDatas["customersInvoicesNotPurchased"],
					$customerDatas["customersBadPaymentBehavior"],
					$customerDatas["customersEntryDate"],

					$customerDatas["customersSepaExists"],
					$customerDatas["customersSepaValidUntilDate"],
					$customerDatas["customersSepaRequestedDate"],

					$customerDatas["customersCompanyNotExists"],

					$customerDatas["customersCompanyLocation"],
					$customerDatas["customersIsVisibleForUserID"],
					$customerDatas["customersPriceListsID"],

					$customerDatas["customersOriginalVertreterID"],

					$customerDatas["customersGetNoCollectedInvoice"],

					$customerDatas["customersNoDeliveries"],

					$customerDatas["customersSendPaperInvoice"],

					$customerDatas["customersSendNoFaktoringInfo"],

					$customerDatas["customersGroupCompanyNumber"],

					$customerDatas["customersPaymentOnlyPrepayment"],
					$customerDatas["customersRequestOrderNumber"]

			) = mysqli_fetch_array($rs);
			
			//die(var_dump($customerDatas));

							if(!empty($customerDatas["customersNotiz"])){
									$jswindowMessage .= ' • Notizen: '.$customerDatas["customersNotiz"].'' . "\\n";
							}
#dd('customerDatas');
			// BOF ADD VAT-ID
			if($customerDatas["customersRechnungsadresseLand"] != 81 && $customerDatas["customersRechnungsadresseLand"] != '' && $customerDatas["customersUseSalesmanInvoiceAdress"] != '1' ){
				#$arrFormDutyFields["editCustomersUstID"] = "UstID";
				if(formatVatID($customerDatas["customersUstID"] == '')){
					$jswindowMessage .= 'Bitte geben Sie eine UmsatzsteuerID (UstID) an!';
					$warningMessage .= 'Bitte geben Sie eine UmsatzsteuerID (UstID) an!';
				}
			}
			$jsonFormDutyFields = createJson($arrFormDutyFields);
		// EOF ADD VAT-ID


			if((!$doAction || !$isCheckSumOk || !$checkCustomerEmail) && $_POST["storeDatas"] != "") {
				foreach($_POST as $thisKey => $thisValue) {
					// $customerDatas["customersActive"] = $_POST["editCustomersActive"];
					$thisKeyNew = preg_replace("/editCustomers/", "customers", $thisKey);
					// echo $thisKey . ' | ' . $thisKeyNew . "<br />";
					$customerDatas[$thisKeyNew] = $_POST[$thisKey];
				}
			}

			// BOF CHECK IF CUSTOMER IS USED
				$arrSubSQL = array();
				foreach($arrTables as $thisTableKey => $thisTableValue){
					$arrSubSQL[] = "
							SELECT
								COUNT(`orderDocumentsCustomerNumber`) AS `checkCustomerNumber`
								FROM `" . $thisTableValue . "`
								WHERE 1
									AND `orderDocumentsCustomerNumber` = '" . $customerDatas["customersKundennummer"] . "'
						";
				}
				$sql = "
					SELECT
						SUM(`tempTable`.`checkCustomerNumber`) AS `checkCustomerNumberIsUsed`

						FROM (
						" . implode(' UNION ALL ', $arrSubSQL) . "
						) AS `tempTable`
					";

				$rs = $dbConnection->db_query($sql);
							
				list($checkCustomerNumberIsUsed) = mysqli_fetch_array($rs);
			// EOF OF CHECK IF CUSTOMER IS USED

			// BOF GET COMPANY RELATIONS - MOTHER COMPANY
				// 32007 -> 32008 TEST
				$arrCustomerParentCompanyDatas = array();
				if($_REQUEST["editID"] != "" && $_REQUEST["editID"] != "NEW" && $customerDatas["customersID"] != '') {
					$sql = "
							SELECT
								`" . TABLE_CUSTOMERS_RELATIONS . "`.`customersRelationsParentCustomerID`,
								`" . TABLE_CUSTOMERS_RELATIONS . "`.`customersRelationsParentCustomerNumber`,

								`" . TABLE_CUSTOMERS . "`.`customersFirmenname`,
								`" . TABLE_CUSTOMERS . "`.`customersCompanyStrasse`,
								`" . TABLE_CUSTOMERS . "`.`customersCompanyHausnummer`,
								`" . TABLE_CUSTOMERS . "`.`customersCompanyPLZ`,
								`" . TABLE_CUSTOMERS . "`.`customersCompanyOrt`,
								`" . TABLE_CUSTOMERS . "`.`customersCompanyCountry`

								FROM `" . TABLE_CUSTOMERS_RELATIONS . "`
								LEFT JOIN `" . TABLE_CUSTOMERS . "`
								ON(`" . TABLE_CUSTOMERS_RELATIONS . "`.`customersRelationsParentCustomerID` = `" . TABLE_CUSTOMERS . "`.`customersID`)

								WHERE 1
									AND `customersRelationsChildCustomerID` = " . $customerDatas["customersID"] . "
									AND `customersRelationsChildCustomerNumber` = '" . $customerDatas["customersKundennummer"] . "'
						";
					$rs = $dbConnection->db_query($sql);
				 
					if($dbConnection->db_getMysqlNumRows($rs) > 0){
						list(
							$arrCustomerParentCompanyDatas["parentCompanyID"],
							$arrCustomerParentCompanyDatas["parentCompanyNumber"],
							$arrCustomerParentCompanyDatas["parentCompanyName"],
							$arrCustomerParentCompanyDatas["parentCompanyStrasse"],
							$arrCustomerParentCompanyDatas["parentCompanyHausnummer"],
							$arrCustomerParentCompanyDatas["parentCompanyPLZ"],
							$arrCustomerParentCompanyDatas["parentCompanyOrt"],
							$arrCustomerParentCompanyDatas["parentCompanyCountry"]
						) = mysqli_fetch_array($rs);
					}
				}
			// EOF GET COMPANY RELATIONS - MOTHER COMPANY

			// BOF GET COMPANY RELATIONS - CHILD COMPANIES
				$arrCustomerChildCompanyDatas = array();
				if($_REQUEST["editID"] != "" && $_REQUEST["editID"] != "NEW" && $customerDatas["customersID"] != '') {
					$sql = "
							SELECT
								`" . TABLE_CUSTOMERS_RELATIONS . "`.`customersRelationsChildCustomerID`,
								`" . TABLE_CUSTOMERS_RELATIONS . "`.`customersRelationsChildCustomerNumber`,

								`" . TABLE_CUSTOMERS . "`.`customersFirmenname`,
								`" . TABLE_CUSTOMERS . "`.`customersCompanyStrasse`,
								`" . TABLE_CUSTOMERS . "`.`customersCompanyHausnummer`,
								`" . TABLE_CUSTOMERS . "`.`customersCompanyPLZ`,
								`" . TABLE_CUSTOMERS . "`.`customersCompanyOrt`,
								`" . TABLE_CUSTOMERS . "`.`customersCompanyCountry`

								FROM `" . TABLE_CUSTOMERS_RELATIONS . "`
								LEFT JOIN `" . TABLE_CUSTOMERS . "`
								ON(`" . TABLE_CUSTOMERS_RELATIONS . "`.`customersRelationsChildCustomerID` = `" . TABLE_CUSTOMERS . "`.`customersID`)

								WHERE 1
									AND `customersRelationsParentCustomerID` = " . $customerDatas["customersID"] . "
									AND `customersRelationsParentCustomerNumber` = '" . $customerDatas["customersKundennummer"] . "'
						";
					$rs = $dbConnection->db_query($sql);

					while($ds = mysqli_fetch_assoc($rs)){
						$arrCustomerChildCompanyDatas[$ds["customersRelationsChildCustomerID"]]["childCompanyID"] = $ds["customersRelationsChildCustomerID"];
						$arrCustomerChildCompanyDatas[$ds["customersRelationsChildCustomerID"]]["childCompanyNumber"] = $ds["customersRelationsChildCustomerNumber"];
						$arrCustomerChildCompanyDatas[$ds["customersRelationsChildCustomerID"]]["childCompanyName"] = $ds["customersFirmenname"];
						$arrCustomerChildCompanyDatas[$ds["customersRelationsChildCustomerID"]]["childCompanyStrasse"] = $ds["customersCompanyStrasse"];
						$arrCustomerChildCompanyDatas[$ds["customersRelationsChildCustomerID"]]["childCompanyHausnummer"] = $ds["customersCompanyHausnummer"];
						$arrCustomerChildCompanyDatas[$ds["customersRelationsChildCustomerID"]]["childCompanyPLZ"] = $ds["customersCompanyPLZ"];
						$arrCustomerChildCompanyDatas[$ds["customersRelationsChildCustomerID"]]["childCompanyOrt"] = $ds["customersCompanyOrt"];
						$arrCustomerChildCompanyDatas[$ds["customersRelationsChildCustomerID"]]["childCompanyCountry"] = $ds["customersCompanyCountry"];
					}
				}
			// EOF GET COMPANY RELATIONS - CHILD COMPANIES
		}
	// EOF READ SELECTED DATAS

	// BOF READ PHONE CODES
		if($_REQUEST["editID"] != "" && $_REQUEST["editID"] != "NEW") {
			$arrPhoneCodes = getPhoneCodes(array(
								array("countryID" => $customerDatas["customersCompanyCountry"], "zipCode" => $customerDatas["customersCompanyPLZ"]),
								array("countryID" => $customerDatas["customersLieferadresseLand"], "zipCode" => $customerDatas["customersLieferadressePLZ"]),
								array("countryID" => $customerDatas["customersRechnungsadresseLand"], "zipCode" => $customerDatas["customersRechnungsadressePLZ"])
							)
						);
		}
	// EOF READ PHONE CODES

	// BOF GET PDF-DOCUMENTS
		#if($_REQUEST["tab"] == "tabs-3") {
		if(1) {
			if($_REQUEST["editID"] != "" && $_REQUEST["editID"] != "NEW") {
				$xxx_sql = "
					SELECT
						`" . TABLE_CREATED_DOCUMENTS . "`.`createdDocumentsID`,
						`" . TABLE_CREATED_DOCUMENTS . "`.`createdDocumentsType`,
						`" . TABLE_CREATED_DOCUMENTS . "`.`createdDocumentsNumber`,
						`" . TABLE_CREATED_DOCUMENTS . "`.`createdDocumentsCustomerNumber`,
						`" . TABLE_CREATED_DOCUMENTS . "`.editCustomersIDustomerNumber` = '".$customerDatas["customersKundennummer"]."'
							AND `" . TABLE_CREATED_DOCUMENTS . "`.`createdDocumentsType` = 'KA'
						ORDER BY `createdDocumentsNumber` DESC
					";

				$sql = "
					SELECT
						`" . TABLE_CUSTOMERS_LAYOUT_FILES . "`.`createdDocumentsID`,
						`" . TABLE_CUSTOMERS_LAYOUT_FILES . "`.`createdDocumentsType`,
						`" . TABLE_CUSTOMERS_LAYOUT_FILES . "`.`createdDocumentsNumber`,
						`" . TABLE_CUSTOMERS_LAYOUT_FILES . "`.`createdDocumentsCustomerNumber`,
						`" . TABLE_CUSTOMERS_LAYOUT_FILES . "`.`createdDocumentsFilename`,
						`" . TABLE_CUSTOMERS_LAYOUT_FILES . "`.`createdDocumentsTimeCreated`,

						`" . TABLE_CREATED_DOCUMENTS_TYPES . "`.`createdDocumentsTypesID`,
						`" . TABLE_CREATED_DOCUMENTS_TYPES . "`.`createdDocumentsTypesName`,
						`" . TABLE_CREATED_DOCUMENTS_TYPES . "`.`createdDocumentsTypesShortName`

						FROM `" . TABLE_CUSTOMERS_LAYOUT_FILES . "`

						LEFT JOIN `" . TABLE_CREATED_DOCUMENTS_TYPES . "`
						ON(`" . TABLE_CUSTOMERS_LAYOUT_FILES . "`.`createdDocumentsType` = `" . TABLE_CREATED_DOCUMENTS_TYPES . "`.`createdDocumentsTypesShortName`)

						WHERE 1
							AND `" . TABLE_CUSTOMERS_LAYOUT_FILES . "`.`createdDocumentsCustomerNumber` = '".$customerDatas["customersKundennummer"]."'
							AND `" . TABLE_CUSTOMERS_LAYOUT_FILES . "`.`createdDocumentsType` = 'KA'
						ORDER BY `createdDocumentsNumber` DESC
				";

				$rs = $dbConnection->db_query($sql);

				while($ds = mysqli_fetch_assoc($rs)) {
					foreach(array_keys($ds) as $field) {
						$arrPdfDatas[$ds["createdDocumentsTypesName"]][$ds["createdDocumentsID"]][$field] = $ds[$field];
					}
				}
			}
		}
	// EOF GET PDF-DOCUMENTS
	
	if(isset($_POST["addArticle"]) && !isset($_GET['customersPriceListsProductsDataID']))
	{
		
		$article = $_POST['article'];
		$quant = $_POST['quant'];
		$stckprice = $_POST['stckprice']; 

		if( 
			isset($article) &&
			isset($quant) &&
			isset($stckprice)
		){
			if( 
			!empty($article) &&
			!empty($quant) &&
			!empty($stckprice)
			){
				
				$sql = "
				INSERT INTO `" . TABLE_CUSTOMERS_PRICE_LISTS_PRODUCTS_DATA . "`(
					
					`customersPriceListsProductsDataProductsNumber`,
					`customersPriceListsProductsDataCustomerNumber`,
					`customersPriceListsProductsDataCustomerID`,
					`customersPriceListsProductsDataProductsQuantity`,
					`customersPriceListsProductsDataProductsPrice`,
					`sachbearbeiter`
						)
						VALUES (
							
						
							'" . $article . "',
							'" . $customerDatas["customersKundennummer"] . "',
							'" . $customerDatas["customersKundennummer"] . "',
							'" . $quant . "',
							'" . $stckprice . "',
							'" .$userDatas['usersFirstName'].' '.$userDatas['usersLastName'] ."'
							
						)
				";
			 
			
				$rs = $dbConnection->db_query($sql);
			 
				if($rs) {
					$successMessage .= ' Neu Preise wurde gespeichert.' .'<br />';
				}
				else {
					$errorMessage .= ' Neu Preise konnte nicht gespeichert werden.' . '<br />';
				}
			

		}

		}

	}
 
	if(isset($_POST["addArticle"]) && isset($_GET['customersPriceListsProductsDataID']))
	{
		
		$article = $_POST['article'];
		$quant = $_POST['quant'];
		$stckprice = $_POST['stckprice']; 

		if( 
			isset($article) &&
			isset($quant) &&
			isset($stckprice)
		){
			if( 
			!empty($article) &&
			!empty($quant) &&
			!empty($stckprice)
			){
				
				$sql = "
					UPDATE `common_customerspricelistsproductsdata`
					SET customersPriceListsProductsDataProductsNumber='" . $article . "',
					customersPriceListsProductsDataProductsNumber='" . $article . "',
					customersPriceListsProductsDataProductsQuantity='" . $quant . "',
					customersPriceListsProductsDataProductsPrice='" . $stckprice . "',
					sachbearbeiter=	'" .$userDatas['usersFirstName'].' '.$userDatas['usersLastName'] ."'

					
					WHERE customersPriceListsProductsDataID='" . $_GET['customersPriceListsProductsDataID'] . "' ";
			
				$rs = $dbConnection->db_query($sql);
			 
				if($rs) {
					$successMessage .= ' Neu Prise wurde gespeichert.' .'<br />';

														 }
				else {
					$errorMessage .= ' Neu Prise konnte nicht gespeichert werden.' . '<br />';
				}
			

		}

		}

	}

	// BOF GET CREATED DOCUMENTS
		if($_REQUEST["tab"] == "tabs-6" || $_REQUEST["tab"] == "tabs-4" || $_REQUEST["tab"] == "tabs-5") {
			if($_REQUEST["editID"] != "" && $_REQUEST["editID"] != "NEW") {
				$arrSQL = array();

				foreach($arrTables as $thisTableKey => $thisTableValue){

					$arrSQL[] = "SELECT
								`" . $thisTableValue . "`.`orderDocumentsNumber`,

								'" . $thisTableKey . "' AS `tableDocumentType`,

								`" . $thisTableValue . "`.`orderDocumentsID`,
								`" . $thisTableValue . "`.`orderDocumentsType`,
								`" . $thisTableValue . "`.`orderDocumentsOrdersIDs`,
								`" . $thisTableValue . "`.`orderDocumentsCustomerNumber`,
								`" . $thisTableValue . "`.`orderDocumentsCustomersOrderNumber`,
								`" . $thisTableValue . "`.`orderDocumentsProcessingDate`,
								`" . $thisTableValue . "`.`orderDocumentsDocumentDate`,
								`" . $thisTableValue . "`.`orderDocumentsDeliveryDate`,
								`" . $thisTableValue . "`.`orderDocumentsBindingDate`,
								`" . $thisTableValue . "`.`orderDocumentsSalesman`,
								`" . $thisTableValue . "`.`orderDocumentsAddressCompany`,
								`" . $thisTableValue . "`.`orderDocumentsAddressName`,
								`" . $thisTableValue . "`.`orderDocumentsAddressStreet`,
								`" . $thisTableValue . "`.`orderDocumentsAddressStreetNumber`,
								`" . $thisTableValue . "`.`orderDocumentsAddressZipcode`,
								`" . $thisTableValue . "`.`orderDocumentsAddressCity`,
								`" . $thisTableValue . "`.`orderDocumentsAddressCountry`,
								`" . $thisTableValue . "`.`orderDocumentsAddressMail`,
								`" . $thisTableValue . "`.`orderDocumentsSumPrice`,
								`" . $thisTableValue . "`.`orderDocumentsDiscount`,
								`" . $thisTableValue . "`.`orderDocumentsDiscountType`,
								`" . $thisTableValue . "`.`orderDocumentsDiscountPercent`,
								`" . $thisTableValue . "`.`orderDocumentsMwst`,
								`" . $thisTableValue . "`.`orderDocumentsMwstPrice`,
								`" . $thisTableValue . "`.`orderDocumentsShippingCosts`,
								`" . $thisTableValue . "`.`orderDocumentsPackagingCosts`,
								`" . $thisTableValue . "`.`orderDocumentsCashOnDelivery`,
								`" . $thisTableValue . "`.`orderDocumentsTotalPrice`,
								`" . $thisTableValue . "`.`orderDocumentsBankAccount`,
								`" . $thisTableValue . "`.`orderDocumentsPaymentCondition`,
								`" . $thisTableValue . "`.`orderDocumentsPaymentType`,
								`" . $thisTableValue . "`.`orderDocumentsDeliveryType`,
								`" . $thisTableValue . "`.`orderDocumentsDeliveryTypeNumber`,
								`" . $thisTableValue . "`.`orderDocumentsSubject`,
								`" . $thisTableValue . "`.`orderDocumentsContent`,
								`" . $thisTableValue . "`.`orderDocumentsDocumentPath`,
								`" . $thisTableValue . "`.`orderDocumentsTimestamp`,
								`" . $thisTableValue . "`.`orderDocumentsStatus`,
								`" . $thisTableValue . "`.`orderDocumentsSendMail`,

								`" . $thisTableValue . "`.`orderDocumentsInterestPercent`,
								`" . $thisTableValue . "`.`orderDocumentsChargesPrice`,
								`" . $thisTableValue . "`.`orderDocumentsInterestPrice`,

								`" . $thisTableValue . "`.`orderDocumentsIsCollectiveInvoice`,

								`" . $thisTableValue . "`.`orderDocumentsInvoiceRecipientData`,
								`" . $thisTableValue . "`.`orderDocumentsDeliveryRecipientData`,

								`" . $thisTableValue . "Details`.`orderDocumentDetailKommission`


							FROM `" . $thisTableValue . "`

							LEFT JOIN `" . $thisTableValue . "Details`
							ON(`" . $thisTableValue . "`.`orderDocumentsID` = `" . $thisTableValue . "Details`.`orderDocumentDetailDocumentID`)

							WHERE 1
								AND `" . $thisTableValue . "`.`orderDocumentsCustomerNumber` = '".$customerDatas["customersKundennummer"]."'

								GROUP BY `" . $thisTableValue . "`.`orderDocumentsNumber`
					";

					// BOF GET DOCUMENTS THAT SALESMAN GET FOR THEIR CUSTOMERS
						$arrSQL[] = "
								SELECT
									`" . $thisTableValue . "`.`orderDocumentsNumber`,

									'" . $thisTableKey . "' AS `tableDocumentType`,

									`" . $thisTableValue . "`.`orderDocumentsID`,
									`" . $thisTableValue . "`.`orderDocumentsType`,
									`" . $thisTableValue . "`.`orderDocumentsOrdersIDs`,
									`" . $thisTableValue . "`.`orderDocumentsCustomerNumber`,
									`" . $thisTableValue . "`.`orderDocumentsCustomersOrderNumber`,
									`" . $thisTableValue . "`.`orderDocumentsProcessingDate`,
									`" . $thisTableValue . "`.`orderDocumentsDocumentDate`,
									`" . $thisTableValue . "`.`orderDocumentsDeliveryDate`,
									`" . $thisTableValue . "`.`orderDocumentsBindingDate`,
									`" . $thisTableValue . "`.`orderDocumentsSalesman`,
									CONCAT(
										`" . $thisTableValue . "`.`orderDocumentsAddressCompany`,
										'<br /><span class=\"remarksArea\"><b>Kunde: </b>',
										`customerCustomers`.`customersFirmenname`,
										' ',
										`customerCustomers`.`customersFirmennameZusatz`,
										' (',
										`" . $thisTableValue . "`.`orderDocumentsCustomerNumber`,
										')',
										'</span>'
									) AS `orderDocumentsAddressCompany`,
									`" . $thisTableValue . "`.`orderDocumentsAddressName`,
									`" . $thisTableValue . "`.`orderDocumentsAddressStreet`,
									`" . $thisTableValue . "`.`orderDocumentsAddressStreetNumber`,
									`" . $thisTableValue . "`.`orderDocumentsAddressZipcode`,
									`" . $thisTableValue . "`.`orderDocumentsAddressCity`,
									`" . $thisTableValue . "`.`orderDocumentsAddressCountry`,
									`" . $thisTableValue . "`.`orderDocumentsAddressMail`,
									`" . $thisTableValue . "`.`orderDocumentsSumPrice`,
									`" . $thisTableValue . "`.`orderDocumentsDiscount`,
									`" . $thisTableValue . "`.`orderDocumentsDiscountType`,
									`" . $thisTableValue . "`.`orderDocumentsDiscountPercent`,
									`" . $thisTableValue . "`.`orderDocumentsMwst`,
									`" . $thisTableValue . "`.`orderDocumentsMwstPrice`,
									`" . $thisTableValue . "`.`orderDocumentsShippingCosts`,
									`" . $thisTableValue . "`.`orderDocumentsPackagingCosts`,
									`" . $thisTableValue . "`.`orderDocumentsCashOnDelivery`,
									`" . $thisTableValue . "`.`orderDocumentsTotalPrice`,
									`" . $thisTableValue . "`.`orderDocumentsBankAccount`,
									`" . $thisTableValue . "`.`orderDocumentsPaymentCondition`,
									`" . $thisTableValue . "`.`orderDocumentsPaymentType`,
									`" . $thisTableValue . "`.`orderDocumentsDeliveryType`,
									`" . $thisTableValue . "`.`orderDocumentsDeliveryTypeNumber`,
									`" . $thisTableValue . "`.`orderDocumentsSubject`,
									`" . $thisTableValue . "`.`orderDocumentsContent`,
									`" . $thisTableValue . "`.`orderDocumentsDocumentPath`,
									`" . $thisTableValue . "`.`orderDocumentsTimestamp`,
									`" . $thisTableValue . "`.`orderDocumentsStatus`,
									`" . $thisTableValue . "`.`orderDocumentsSendMail`,

									`" . $thisTableValue . "`.`orderDocumentsInterestPercent`,
									`" . $thisTableValue . "`.`orderDocumentsChargesPrice`,
									`" . $thisTableValue . "`.`orderDocumentsInterestPrice`,

									`" . $thisTableValue . "`.`orderDocumentsIsCollectiveInvoice`,

									`" . $thisTableValue . "`.`orderDocumentsInvoiceRecipientData`,
									`" . $thisTableValue . "`.`orderDocumentsDeliveryRecipientData`,

									GROUP_CONCAT(`" . $thisTableValue . "Details`.`orderDocumentDetailKommission` SEPARATOR '###') AS `orderDocumentDetailKommission`

								FROM `" . $thisTableValue . "`

								LEFT JOIN `" . $thisTableValue . "Details`
								ON(`" . $thisTableValue . "`.`orderDocumentsID` = `" . $thisTableValue . "Details`.`orderDocumentDetailDocumentID`)

								LEFT JOIN `" . TABLE_CUSTOMERS . "` AS `customerSalesman`
								ON(`" . $thisTableValue . "`.`orderDocumentsSalesman` = `customerSalesman`.`customersID` AND `" . $thisTableValue . "`.`orderDocumentsSalesman` > 0)

								LEFT JOIN `" . TABLE_CUSTOMERS . "` AS `customerCustomers`
								ON(`" . $thisTableValue . "`.`orderDocumentsCustomerNumber` = `customerCustomers`.`customersKundennummer`)

								WHERE 1
									AND `customerSalesman`.`customersKundennummer` = '".$customerDatas["customersKundennummer"]."'

									GROUP BY `" . $thisTableValue . "`.`orderDocumentsNumber`
						";
					// EOF GET DOCUMENTS THAT SALESMAN GET FOR THEIR CUSTOMERS
				}
				if(!empty($arrSQL)) {
					$sql = implode(" UNION ALL ", $arrSQL) . " ORDER BY `tableDocumentType` DESC, `orderDocumentsProcessingDate` DESC, `orderDocumentsNumber` DESC ;";
				} 
				$rs = $dbConnection->db_query($sql);

				while($ds = mysqli_fetch_assoc($rs)) {
					$arrTemp = array();
					foreach(array_keys($ds) as $field) {
						$arrTemp[$field] = $ds[$field];
					}
					$arrDocumentDatas[$ds["tableDocumentType"]][$ds["orderDocumentsID"]] = $arrTemp;
				}
			}
		}
	// EOF GET CREATED DOCUMENTS

	// BOF GET SALESMAN ADRESS
	#if($customerDatas["customersUseSalesmanDeliveryAdress"] == '1' || $customerDatas["customersUseSalesmanInvoiceAdress"] == '1') {
	if($customerDatas["customersVertreterID"] > 0 || $customerDatas["customersUseSalesmanDeliveryAdress"] == '1' || $customerDatas["customersUseSalesmanInvoiceAdress"] == '1') {
		$sql = "SELECT
						`customersID`,
						`customersKundennummer`,
						`customersFirmenname`,
						`customersFirmennameZusatz`,
						`customersFirmenInhaberVorname`,
						`customersFirmenInhaberNachname`,
						`customersFirmenInhaberAnrede`,

						`customersFirmenInhaber2Vorname`,
						`customersFirmenInhaber2Nachname`,
						`customersFirmenInhaber2Anrede`,

						`customersCompanyStrasse`,
						`customersCompanyHausnummer`,
						`customersCompanyCountry`,
						`customersCompanyPLZ`,
						`customersCompanyOrt`,
						`customersLieferadresseFirmenname`,
						`customersLieferadresseFirmennameZusatz`,
						`customersLieferadresseStrasse`,
						`customersLieferadresseHausnummer`,
						`customersLieferadressePLZ`,
						`customersLieferadresseOrt`,
						`customersLieferadresseLand`,
						`customersRechnungsadresseFirmenname`,
						`customersRechnungsadresseFirmennameZusatz`,
						`customersRechnungsadresseStrasse`,
						`customersRechnungsadresseHausnummer`,
						`customersRechnungsadressePLZ`,
						`customersRechnungsadresseOrt`,
						`customersRechnungsadresseLand`,

						`customersTyp`,
						`customersPriceListsID`

						FROM `" . TABLE_CUSTOMERS . "`

						WHERE 1
							AND `customersID` = '".$customerDatas["customersVertreterID"]."'

						LIMIT 1
		";

		$rs = $dbConnection->db_query($sql);

		while($ds = mysqli_fetch_assoc($rs)) {
			#foreach(array_keys($ds) as $field) {
				if($customerDatas["customersVertreterID"] > 0 && $ds["customersTyp"] != 11){
					$customerDatas["customersPriceListsID"] = $ds["customersPriceListsID"];
					$salesmanDatas["customersPriceListsID"] = $ds["customersPriceListsID"];
					$salesmanDatas["customersTyp"] = $ds["customersTyp"];
				}

				if($customerDatas["customersUseSalesmanDeliveryAdress"] == '1') {
					$customerDatas["customersLieferadresseKundennummer"] = $ds["customersLieferadresseKundennummer"];
					$customerDatas["customersLieferadresseFirmenname"] = $ds["customersLieferadresseFirmenname"];
					$customerDatas["customersLieferadresseFirmennameZusatz"] = $ds["customersLieferadresseFirmennameZusatz"];
					$customerDatas["customersLieferadresseStrasse"] = $ds["customersLieferadresseStrasse"];
					$customerDatas["customersLieferadresseHausnummer"] = $ds["customersLieferadresseHausnummer"];
					$customerDatas["customersLieferadressePLZ"] = $ds["customersLieferadressePLZ"];
					$customerDatas["customersLieferadresseOrt"] = $ds["customersLieferadresseOrt"];
					$customerDatas["customersLieferadresseLand"] = $ds["customersLieferadresseLand"];
				}
				if($customerDatas["customersUseSalesmanInvoiceAdress"] == '1') {
					$customerDatas["customersRechnungsadresseKundennummer"] = $ds["customersRechnungsadresseKundennummer"];
					$customerDatas["customersRechnungsadresseFirmenname"] = $ds["customersRechnungsadresseFirmenname"];
					$customerDatas["customersRechnungsadresseFirmennameZusatz"] = $ds["customersRechnungsadresseFirmennameZusatz"];
					$customerDatas["customersRechnungsadresseStrasse"] = $ds["customersRechnungsadresseStrasse"];
					$customerDatas["customersRechnungsadresseHausnummer"] = $ds["customersRechnungsadresseHausnummer"];
					$customerDatas["customersRechnungsadressePLZ"] = $ds["customersRechnungsadressePLZ"];
					$customerDatas["customersRechnungsadresseOrt"] = $ds["customersRechnungsadresseOrt"];
					$customerDatas["customersRechnungsadresseLand"] = $ds["customersRechnungsadresseLand"];
				}
			#}
		}
	}
	// BOF GET SALESMAN ADRESS

	// BOF GET CONNECTED DOCUMENTS
	if($_REQUEST["tab"] == "tabs-6" || $_REQUEST["tab"] == "tabs-4" || $_REQUEST["tab"] == "tabs-5") {
		if($_REQUEST["editID"] != "" && $_REQUEST["editID"] != "NEW") {
			$arrSubSQL = array();
			$count = 0;
			foreach($arrTables as $thisTableKey => $thisTableValue){
				$arrSubSQL[$count] = "SELECT
	
								`" . $thisTableValue . "`.`orderDocumentsID`,
								`" . $thisTableValue . "`.`orderDocumentsCustomerNumber`,
								`" . $thisTableValue . "`.`orderDocumentsNumber`,
								`" . $thisTableValue . "`.`orderDocumentsDocumentDate`,
								`" . $thisTableValue . "`.`orderDocumentsOrderDate`,
								`" . $thisTableValue . "`.`orderDocumentsType`,
								`" . $thisTableValue . "`.`orderDocumentsOrdersIDs`,
								`" . $thisTableValue . "`.`orderDocumentsStatus`,
								`" . $thisTableValue . "`.`orderDocumentsDocumentPath`,
								'" . $thisTableValue . "' AS `nameTable`,
					";

				if($thisTableKey != 'BR'){
					$arrSubSQL[$count] .= "
								`" . $thisTableValue . "Details`.`orderDocumentDetailKommission`,
								`" . $thisTableValue . "Details`.`orderDocumentDetailPrintText`,

								`" . $thisTableValue . "`.`orderDocumentsTotalPrice`,

								`" . $thisTableValue . "`.`orderDocumentsInterestPercent`,
								`" . $thisTableValue . "`.`orderDocumentsChargesPrice`,
								`" . $thisTableValue . "`.`orderDocumentsInterestPrice`,

								`" . $thisTableValue . "`.`orderDocumentsIsCollectiveInvoice`,

								`" . TABLE_RELATED_DOCUMENTS . "`.`relatedDocuments_AN`,
								`" . TABLE_RELATED_DOCUMENTS . "`.`relatedDocuments_AB`,
								`" . TABLE_RELATED_DOCUMENTS . "`.`relatedDocuments_RE`,
								`" . TABLE_RELATED_DOCUMENTS . "`.`relatedDocuments_LS`,
								`" . TABLE_RELATED_DOCUMENTS . "`.`relatedDocuments_GU`,
								`" . TABLE_RELATED_DOCUMENTS . "`.`relatedDocuments_MA`,
								`" . TABLE_RELATED_DOCUMENTS . "`.`relatedDocuments_M1`,
								`" . TABLE_RELATED_DOCUMENTS . "`.`relatedDocuments_M2`,
								`" . TABLE_RELATED_DOCUMENTS . "`.`relatedDocuments_M3`,
								`" . TABLE_RELATED_DOCUMENTS . "`.`relatedDocuments_RK`,
								`" . TABLE_RELATED_DOCUMENTS . "`.`relatedDocuments_collectiveABs`,
					";
				}
				else {
					$arrSubSQL[$count] .= "
								'' AS `orderDocumentDetailKommission`,
								'' AS `orderDocumentDetailPrintText`,

								'' AS `orderDocumentsTotalPrice`,

								'' AS `orderDocumentsInterestPercent`,
								'' AS `orderDocumentsChargesPrice`,
								'' AS `orderDocumentsInterestPrice`,

								'' AS `orderDocumentsIsCollectiveInvoice`,

								'' AS `relatedDocuments_AN`,
								'' AS `relatedDocuments_AB`,
								'' AS `relatedDocuments_RE`,
								'' AS `relatedDocuments_LS`,
								'' AS `relatedDocuments_GU`,
								'' AS `relatedDocuments_MA`,
								'' AS `relatedDocuments_M1`,
								'' AS `relatedDocuments_M2`,
								'' AS `relatedDocuments_M3`,
								'' AS `relatedDocuments_RK`,
								'' AS `relatedDocuments_collectiveABs`,
					";
				}
				$arrSubSQL[$count] .= "
								CONCAT(
									`" . $thisTableValue . "`.`orderDocumentsAddressCompany`,
									'<br /><span class=\"remarksArea\"><b>Kunde: </b>',
									`customerCustomers`.`customersFirmenname`,
									' ',
									`customerCustomers`.`customersFirmennameZusatz`,
									' (',
									`" . $thisTableValue . "`.`orderDocumentsCustomerNumber`,
									')',
									'</span>'
								) AS `orderDocumentsAddressCompany`

							FROM `" . $thisTableValue . "`

							LEFT JOIN `" . TABLE_CUSTOMERS. "` AS `customerCustomers`
							ON(`" . $thisTableValue . "`.`orderDocumentsCustomerNumber` = `customerCustomers`.`customersKundennummer`)

				";
				if($thisTableKey != 'BR'){
					$arrSubSQL[$count] .= "
							LEFT JOIN `" . $thisTableValue . "Details`
							ON(`" . $thisTableValue . "`.`orderDocumentsID` = `" . $thisTableValue . "Details`.`orderDocumentDetailDocumentID`)

							LEFT JOIN `" . TABLE_RELATED_DOCUMENTS . "`
							ON(`" . $thisTableValue . "`.`orderDocumentsNumber` = `" . TABLE_RELATED_DOCUMENTS . "`.`relatedDocuments_" . $thisTableKey . "`)
						";
				}
				$arrSubSQL[$count] .= "
							WHERE 1
								AND `" . $thisTableValue . "`.`orderDocumentsCustomerNumber` = '" . $customerDatas["customersKundennummer"] . "'
					";
				$count++;
			}

			// BOF GET DOCUMENTS THAT SALESMAN GET FOR THEIR CUSTOMERS
			foreach($arrTables as $thisTableKey => $thisTableValue){
				if($thisTableKey != 'BR'){
					$arrSubSQL[$count] = "

							SELECT

								`" . $thisTableValue . "`.`orderDocumentsID`,
								`" . $thisTableValue . "`.`orderDocumentsCustomerNumber`,
								`" . $thisTableValue . "`.`orderDocumentsNumber`,
								`" . $thisTableValue . "`.`orderDocumentsDocumentDate`,
								`" . $thisTableValue . "`.`orderDocumentsOrderDate`,
								`" . $thisTableValue . "`.`orderDocumentsType`,
								`" . $thisTableValue . "`.`orderDocumentsOrdersIDs`,
								`" . $thisTableValue . "`.`orderDocumentsStatus`,
								`" . $thisTableValue . "`.`orderDocumentsDocumentPath`,
								'" . $thisTableValue . "' AS `nameTable`,

								'' AS `orderDocumentDetailKommission`,
								'' AS `orderDocumentDetailPrintText`,

								`" . $thisTableValue . "`.`orderDocumentsTotalPrice`,

								`" . $thisTableValue . "`.`orderDocumentsInterestPercent`,
								`" . $thisTableValue . "`.`orderDocumentsChargesPrice`,
								`" . $thisTableValue . "`.`orderDocumentsInterestPrice`,

								`" . $thisTableValue . "`.`orderDocumentsIsCollectiveInvoice`,

								`" . TABLE_RELATED_DOCUMENTS . "`.`relatedDocuments_AN`,
								`" . TABLE_RELATED_DOCUMENTS . "`.`relatedDocuments_AB`,
								`" . TABLE_RELATED_DOCUMENTS . "`.`relatedDocuments_RE`,
								`" . TABLE_RELATED_DOCUMENTS . "`.`relatedDocuments_LS`,
								`" . TABLE_RELATED_DOCUMENTS . "`.`relatedDocuments_GU`,
								`" . TABLE_RELATED_DOCUMENTS . "`.`relatedDocuments_MA`,
								`" . TABLE_RELATED_DOCUMENTS . "`.`relatedDocuments_M1`,
								`" . TABLE_RELATED_DOCUMENTS . "`.`relatedDocuments_M2`,
								`" . TABLE_RELATED_DOCUMENTS . "`.`relatedDocuments_M3`,
								`" . TABLE_RELATED_DOCUMENTS . "`.`relatedDocuments_RK`,
								`" . TABLE_RELATED_DOCUMENTS . "`.`relatedDocuments_collectiveABs`,

								CONCAT(
									`" . $thisTableValue . "`.`orderDocumentsAddressCompany`,
									'<br /><span class=\"remarksArea\"><b>Kunde: </b>',
									`customerCustomers`.`customersFirmenname`,
									`customerCustomers`.`customersFirmennameZusatz`,
									' (',
									`" . $thisTableValue . "`.`orderDocumentsCustomerNumber`,
									')',
									'</span>'
								) AS `orderDocumentsAddressCompany`

							FROM `" . TABLE_CUSTOMERS . "` AS `customerSalesman`

							LEFT JOIN `" . $thisTableValue . "`
							ON(`customerSalesman`.`customersID` = `" . $thisTableValue . "`.`orderDocumentsSalesman`)

							LEFT JOIN `" . TABLE_CUSTOMERS . "` AS `customerCustomers`
							ON(`" . $thisTableValue . "`.`orderDocumentsCustomerNumber` = `customerCustomers`.`customersKundennummer`)

							LEFT JOIN `" . TABLE_RELATED_DOCUMENTS . "`
							ON(`" . $thisTableValue . "`.`orderDocumentsNumber` = `" . TABLE_RELATED_DOCUMENTS . "`.`relatedDocuments_" . $thisTableKey . "`)

							WHERE 1
								AND `customerSalesman`.`customersKundennummer` = '".$customerDatas["customersKundennummer"]."'
						";
					$count++;
				}
			}
			// EOF GET DOCUMENTS THAT SALESMAN GET FOR THEIR CUSTOMERS

			$sql = "SELECT

					`tempTable`.`orderDocumentsID`,
					`tempTable`.`orderDocumentsCustomerNumber`,
					`tempTable`.`orderDocumentsNumber`,
					`tempTable`.`orderDocumentsDocumentDate`,
					`tempTable`.`orderDocumentsOrderDate`,
					`tempTable`.`orderDocumentsType`,
					`tempTable`.`orderDocumentsOrdersIDs`,
					`tempTable`.`nameTable`,
					`tempTable`.`orderDocumentsStatus`,
					`tempTable`.`orderDocumentsDocumentPath` AS `createdDocumentsFilename`,
					`tempTable`.`orderDocumentsAddressCompany`,

					`tempTable`.`orderDocumentDetailKommission`,
					`tempTable`.`orderDocumentDetailPrintText`,

					`tempTable`.`orderDocumentsTotalPrice`,

					`tempTable`.`orderDocumentsInterestPercent`,
					`tempTable`.`orderDocumentsChargesPrice`,
					`tempTable`.`orderDocumentsInterestPrice`,

					`tempTable`.`orderDocumentsIsCollectiveInvoice`,

					`tempTable`.`relatedDocuments_AN`,
					`tempTable`.`relatedDocuments_AB`,
					`tempTable`.`relatedDocuments_RE`,
					`tempTable`.`relatedDocuments_LS`,
					`tempTable`.`relatedDocuments_GU`,
					`tempTable`.`relatedDocuments_MA`,
					`tempTable`.`relatedDocuments_M1`,
					`tempTable`.`relatedDocuments_M2`,
					`tempTable`.`relatedDocuments_M3`,
					`tempTable`.`relatedDocuments_RK`,
					`tempTable`.`relatedDocuments_collectiveABs`,

					`" . TABLE_DOCUMENTS_TO_DOCUMENTS . "`.`documentsToDocumentsOriginDocumentNumber`,
					`" . TABLE_DOCUMENTS_TO_DOCUMENTS . "`.`documentsToDocumentsCreatedDocumentNumber`,
					`" . TABLE_DOCUMENTS_TO_DOCUMENTS . "`.`documentsToDocumentsRelationType`


					FROM (
					" . implode(" UNION ALL ", $arrSubSQL) . "
					) AS `tempTable`

						LEFT JOIN `" . TABLE_DOCUMENTS_TO_DOCUMENTS . "`
						ON(`tempTable`.`orderDocumentsNumber` = `" . TABLE_DOCUMENTS_TO_DOCUMENTS . "`.`documentsToDocumentsOriginDocumentNumber`)


					";

					// GROUP BY CONCAT(`tempTable`.`orderDocumentsNumber`, ';', `" . TABLE_DOCUMENTS_TO_DOCUMENTS . "`.`documentsToDocumentsCreatedDocumentNumber`)
			//die($sql);
			$rs = $dbConnection->db_query($sql);
#dd('sql');
			$arrConnectedDocumentIds = array();
			$arrDocumentNumbers = array();
			$arrRelatedDocumentNumbers = array();
			$countItem = 0;
			while($ds = mysqli_fetch_assoc($rs)) {
				$arrTemp = explode(";", $ds["orderDocumentsOrdersIDs"]);
				$arrTemp = array_unique($arrTemp);
				$arrDocumentNumbers[] = $ds["orderDocumentsNumber"];
				#$arrDocumentRelatedNumbers[] = getRelatedDocuments(array($ds["orderDocumentsNumber"]));

				if($ds["orderDocumentsNumber"] != ''){
					if($ds["orderDocumentsType"] != 'BR'){
						#$arrDocumentRelatedNumbers[] = getRelatedDocuments(array($ds["orderDocumentsNumber"]));
						$arrDocumentRelatedNumbers[$countItem] = array(
							"collectiveABs" => $ds["relatedDocuments_collectiveABs"],
							"M3" => $ds["relatedDocuments_M3"],
							"M2" => $ds["relatedDocuments_M2"],
							"M1" => $ds["relatedDocuments_M1"],
							"MA" => $ds["relatedDocuments_MA"],
							"GU" => $ds["relatedDocuments_GU"],
							"LS" => $ds["relatedDocuments_LS"],
							"RE" => $ds["relatedDocuments_RE"],
							"AB" => $ds["relatedDocuments_AB"],
							"AN" => $ds["relatedDocuments_AN"],
							"RK" => $ds["relatedDocuments_RK"],
						);
					}
				}
#dd('ds');
				$arrTemp = explode(";", $ds["orderDocumentsOrdersIDs"]);
				$arrTemp = array_unique($arrTemp);
				$thisProductionKey = implode("#", $arrTemp);
				$arrConnectedDocumentIds[$ds["orderDocumentsNumber"]] = array(
							"table" => $ds["nameTable"],
							"documentID" => $ds["orderDocumentsID"],
							"relationType" => $ds["documentsToDocumentsRelationType"],
							"productionIDs" => $ds["orderDocumentsOrdersIDs"],
							"documentsType" => $ds["orderDocumentsType"],
							"documentsStatus" => $ds["orderDocumentsStatus"],
							"documentsDocumentDate" => $ds["orderDocumentsDocumentDate"],
							"documentsOrderDate" => $ds["orderDocumentsOrderDate"],
							"documentsAddressCompany" => $ds["orderDocumentsAddressCompany"],
							"documentsFilename" => $ds["createdDocumentsFilename"],
							"documentsKommission" => $ds["orderDocumentDetailKommission"],
							"documentsPrintText" => $ds["orderDocumentDetailPrintText"],
							"documentsTotalPrice" => $ds["orderDocumentsTotalPrice"],
							"documentsIsCollectiveInvoice" => $ds["orderDocumentsIsCollectiveInvoice"],
							"documentsInterestPercent" => $ds["orderDocumentsInterestPercent"],
							"documentsChargesPrice" => $ds["orderDocumentsChargesPrice"],
							"documentsInterestPrice" => $ds["orderDocumentsInterestPrice"]
					);

				foreach(array_keys($ds) as $field){
					$arrResultDocumentNumbers[$ds["nameTable"]][$thisProductionKey][$ds["orderDocumentsID"]][$field] = $ds[$field];
				}
				$countItem++;
			}
#dd('arrDocumentRelatedNumbers');
#dd('arrConnectedDocumentIds');
			$arrTemp = $arrDocumentRelatedNumbers;

			$arrDocumentRelatedNumbers = array();

			if(!empty($arrTemp)){
				foreach($arrTemp as $thisKey => $thisValue){
					// BOF BULD STRING FOR ARRAY-KEY TO GET UNIQUE DATAS
					$strTemp = '';
					foreach($thisValue as $thisValueKey => $thisValueValue){
						$strTemp .= $thisValueValue;
					}

					if($thisValue["collectiveABs"] != ""){
						$strTemp = $thisValue["collectiveABs"];
					}
					// EOF BULD STRING FOR ARRAY-KEY TO GET UNIQUE DATAS

					// USE MD5 AS INDEX TO GET UNIQUE DATAS
					$arrDocumentRelatedNumbers[md5($strTemp)] = $arrTemp[$thisKey];
				}
			}
			$arrTemp = $arrDocumentRelatedNumbers;
			$arrDocumentRelatedNumbersNew = array();
			if(!empty($arrDocumentRelatedNumbers)){
				foreach($arrDocumentRelatedNumbers as $arrDocumentRelatedNumbersKey => $arrDocumentRelatedNumbersValue){
					foreach($arrDocumentRelatedNumbersValue as $arrDocumentRelatedNumberKey => $arrDocumentRelatedNumberValue){
						if($arrDocumentRelatedNumberValue != "") {
							if($arrDocumentRelatedNumberKey == "collectiveABs"){
								$arrCollectiveABs = explode(";", $arrDocumentRelatedNumberValue);
								$arrDocumentRelatedNumbersNew[$arrDocumentRelatedNumbersKey][$arrDocumentRelatedNumberKey] = $arrCollectiveABs;
							}
							else if($arrDocumentRelatedNumberKey == "LS" && preg_match("/;/", $arrDocumentRelatedNumberValue)){
								$arrCollectiveLSs = explode(";", $arrDocumentRelatedNumberValue);
								$arrDocumentRelatedNumbersNew[$arrDocumentRelatedNumbersKey][$arrDocumentRelatedNumberKey] = $arrCollectiveLSs;
							}
							else if($arrDocumentRelatedNumberKey == "GU" && preg_match("/;/", $arrDocumentRelatedNumberValue)){
								$arrCollectiveLSs = explode(";", $arrDocumentRelatedNumberValue);
								$arrDocumentRelatedNumbersNew[$arrDocumentRelatedNumbersKey][$arrDocumentRelatedNumberKey] = $arrCollectiveLSs;
							}
							else {
								$arrDocumentRelatedNumbersNew[$arrDocumentRelatedNumbersKey][$arrDocumentRelatedNumberKey][] = $arrDocumentRelatedNumberValue;
							}
						}
					}
					if(!empty($arrDocumentRelatedNumbersNew[$arrDocumentRelatedNumbersKey]["collectiveABs"])){
						$arrDocumentRelatedNumbersNew[$arrDocumentRelatedNumbersKey]["AB"] = $arrDocumentRelatedNumbersNew[$arrDocumentRelatedNumbersKey]["collectiveABs"];
						unset($arrDocumentRelatedNumbersNew[$arrDocumentRelatedNumbersKey]["collectiveABs"]);
					}
				}
			}
			$arrDocumentRelatedNumbers = $arrDocumentRelatedNumbersNew;
			#dd('arrDocumentRelatedNumbers');
			#dd('arrDocumentRelatedNumbersNew');
		}
	}
	// EOF GET CONNECTED DOCUMENTS
?>
<?php
	require_once('inc/headerHTML.inc.php');
	if($_REQUEST["editID"] == "NEW") {
		$thisTitle = 'Kunden anlegen';
		$thisIcon = 'addCustomer.png';

		if($arrGetUserRights["importOnlineAcquisition"] && $_REQUEST["importCustomerDatas"] != ''){
			$importCustomerDatas = ($_REQUEST["importCustomerDatas"]);
			$arrImportCustomerDatas = unserialize(rawurldecode($importCustomerDatas));
			unset($arrImportCustomerDatas["customersID"]);
			unset($arrImportCustomerDatas["customersUserID"]);
			$customerDatas = $arrImportCustomerDatas;
		}
	}
	else {
		$thisTitle = 'Kundendaten verwalten';
		$thisIcon = 'customers.png';
		if($customerDatas["customersKundennummer"] != '') {
			$thisTitle .= ': <span class="headerSelectedEntry">' . $customerDatas["customersKundennummer"] . ' &bull; ' . htmlentities(($customerDatas["customersFirmenname"])) . ' - ' . htmlentities(($customerDatas["customersFirmennameZusatz"])) . '</span>';
		}
	}
	$headerHTML = preg_replace("/{###TITLE###}/", strip_tags($thisTitle), $headerHTML);
	echo $headerHTML;

	require_once(FILE_MENUE_TOP);
?>

<div id="mainArea">
	<div id="mainContent">
		<?php require_once(FILE_MENUE_SIDEBAR); ?>
		<div id="contentArea">

			<div id="contentAreaElements">
				<h1><?php if(SHOW_PAGE_TITLE_ICONS) { echo '<img src="' . PATH_ICONS_MENUE_TITLES . $thisIcon . '" alt="" />'; } ?> <?php echo $thisTitle; ?></h1>
<?php
	if($arrUserDatas["usersLogin"] == 'thorsten'){
		# echo '<a href="editCustomerNEU.php?searchBoxCustomer=' . $_GET["searchBoxCustomer"] . '">NEUE VERSION</a>';
	}
?>
				<?php if($_REQUEST["editID"] != "NEW") { ?>
					<div class="menueActionsArea">
						<?php if($arrGetUserRights["editCustomers"]) { ?><a href="<?php echo PAGE_EDIT_CUSTOMER; ?>?editID=NEW" class="linkButton" title="Neuen Kunden anlegen" >Neukunden anlegen</a><?php } ?>
						<?php if($_REQUEST["editID"] != "") { ?>
						<?php if($arrGetUserRights["editOrders"]) {	?><a href="<?php echo PAGE_EDIT_PROCESS; ?>?editCustomersID=<?php echo $customerDatas["customersID"]; ?>&amp;editID=NEW" class="linkButton" title="Neue Produktion anlegen" >Produktion</a><?php } ?>
						<?php

							// BOF DISABLE DATA
								$disableLink = '';
								$disableClass = '';
								$disableTitle = '';
								$arrDisabledData = array();

								if(!empty($arrDocumentTypeDatas)){
									foreach($arrDocumentTypeDatas as $arrDocumentTypeDatasKey => $arrDocumentTypeDatasValue){
										$arrDisabledData[$arrDocumentTypeDatasKey] = array(
											'disableLink' => '',
											'disableClass' => '',
											'disableTitle' => ''
										);
									}
								}
								// if($customerDatas["customersDatasUpdated"] != '1') {
								if($customerDatas["customersDatasUpdated"] != '1' || ($_REQUEST["editID"] != "NEW" && $customerDatas["customersActive"] != '1')) {
									$disableLink = ' onclick="return false" ';
									$disableClass = ' disabled ';
									$disableTitle = ' gesperrt ';

									if(!empty($arrDocumentTypeDatas)){
										foreach($arrDocumentTypeDatas as $arrDocumentTypeDatasKey => $arrDocumentTypeDatasValue){
											if(in_array($arrDocumentTypeDatasKey, array("AB", "RE", "GU"))){
												$arrDisabledData[$arrDocumentTypeDatasKey] = array(
													'disableLink' => ' onclick="return false" ',
													'disableClass' => ' disabled ',
													'disableTitle' => ' gesperrt '
												);
											}
										}
									}
								}

								// BOF CHECK UstID / VAT-ID
								if($customerDatas["customersRechnungsadresseLand"] != 81 && $customerDatas["customersRechnungsadresseLand"] != '' && $customerDatas["customersUseSalesmanInvoiceAdress"] != '1' ){
									if(formatVatID($customerDatas["customersUstID"] == '')){
										$disableLink = ' onclick="return false" ';
										$disableClass = ' disabled ';
										$disableTitle = ' gesperrt ';

										if(!empty($arrDocumentTypeDatas)){
											foreach($arrDocumentTypeDatas as $arrDocumentTypeDatasKey => $arrDocumentTypeDatasValue){
												if(in_array($arrDocumentTypeDatasKey, array("AB", "RE", "GU"))){
													$arrDisabledData[$arrDocumentTypeDatasKey] = array(
														'disableLink' => ' onclick="return false" ',
														'disableClass' => ' disabled ',
														'disableTitle' => ' gesperrt '
													);
												}
											}
										}
									}
								}
								// EOF CHECK UstID / VAT-ID
								#dd('arrDisabledData');
							// EOF DISABLE DATA

						?>
						<?php if($arrGetUserRights["editOffers"]) { ?>
							<a href="<?php echo PAGE_CREATE_OFFER; ?>&amp;editID=<?php echo $customerDatas["customersKundennummer"]; ?>" class="linkButton <?php echo $arrDisabledData["AN"]["disableClass"]; ?>" title="Neues Angebot schreiben <?php echo $arrDisabledData["AN"]["disableTitle"]; ?>" <?php echo $arrDisabledData["AN"]["disableLink"]; ?> >Angebot</a>
						<?php } ?>
						<?php if($arrGetUserRights["editConfirmations"]) { ?>
							<a href="<?php echo PAGE_CREATE_CONFIRMATION; ?>&amp;editID=<?php echo $customerDatas["customersKundennummer"]; ?>" class="linkButton <?php echo $arrDisabledData["AB"]["disableClass"]; ?>" title="Neue Auftragsbest&auml;tigung schreiben <?php echo $arrDisabledData["AB"]["disableTitle"]; ?>" <?php echo $arrDisabledData["AB"]["disableLink"]; ?> >Auftragsbest&auml;tigung</a>
						<?php } ?>
						<?php if($arrGetUserRights["editInvoices"]) { ?>
							<a href="<?php echo PAGE_CREATE_INVOICE; ?>&amp;editID=<?php echo $customerDatas["customersKundennummer"]; ?>" class="linkButton <?php echo $arrDisabledData["RE"]["disableClass"]; ?>" title="Neue Rechnung schreiben <?php echo $arrDisabledData["RE"]["disableTitle"]; ?>" <?php echo $arrDisabledData["RE"]["disableLink"]; ?> >Rechnung</a>
						<?php } ?>
						<?php if($arrGetUserRights["adminArea"] == '1' && $arrGetUserRights["editInvoices"] == '1' && $customerDatas["customersGetNoCollectedInvoice"] == '1' && in_array($customerDatas["customersTyp"], array(3, 4, 6))) { ?>
							<a href="<?php echo PAGE_CREATE_COLLECTIVE_INVOICE; ?>?editID=<?php echo $customerDatas["customersKundennummer"]; ?>" class="linkButton <?php echo $arrDisabledData["RE"]["disableClass"]; ?>" title="Neue Sammel-Rechnung schreiben <?php echo $arrDisabledData["RE"]["disableTitle"]; ?>" <?php echo $arrDisabledData["RE"]["disableLink"]; ?> >Sammel-Rechnung</a>
						<?php } ?>

						<?php if($arrGetUserRights["editCredits"]) { ?>
							<a href="<?php echo PAGE_CREATE_CREDIT; ?>&amp;editID=<?php echo $customerDatas["customersKundennummer"]; ?>" class="linkButton <?php echo $arrDisabledData["GU"]["disableClass"]; ?>" title="Neue Gutschrift schreiben <?php echo $arrDisabledData["GU"]["disableTitle"]; ?>" <?php echo $arrDisabledData["GU"]["disableLink"]; ?> >Gutschrift</a>
						<?php } ?>

						<?php if($arrGetUserRights["editLetters"]) { ?>
							<a href="<?php echo PAGE_CREATE_LETTER; ?>&amp;editID=<?php echo $customerDatas["customersKundennummer"]; ?>" class="linkButton <?php echo $arrDisabledData["BR"]["disableClass"]; ?>" title="Neuen Brief schreiben <?php echo $arrDisabledData["BR"]["disableTitle"]; ?>" <?php echo $arrDisabledData["BR"]["disableLink"]; ?> >Brief</a>
						<?php } ?>
						

						<?php } ?>
						<div class="clear"></div>
					</div>
				<?php } ?>
				<div class="adminInfo">Datensatz-ID: <?php echo $customerDatas["customersID"]; ?></div>
				<div class="contentDisplay">
				<?php if($_REQUEST["editID"] == "") { ?>
				<div id="searchFilterArea">
					<form name="formFilterSearch" method="get" action="<?php echo $_SERVER["PHP_SELF"]; ?>">
						<table border="0" cellpadding="0" cellspacing="0" class="searchFilterContent">
							<tr>
								<td>
									<label for="searchCustomerNumber">Kundennr:</label>
									<input type="text" name="searchCustomerNumber" id="searchCustomerNumber" class="inputField_70" value="" />
								</td>
								<td>
									<label for="searchCustomerName">Kundenname:</label>
									<input type="text" name="searchCustomerName" id="searchCustomerName" class="inputField_100" value="" />
								</td>
								<td>
									<label for="searchPLZ">PLZ:</label>
									<input type="text" name="searchPLZ" id="searchPLZ" class="inputField_40" />
								</td>
								<td>
									<label for="searchFIBU">FiBu-NR:</label>
									<input type="text" name="searchFIBU" id="searchFIBU" class="inputField_40" />
								</td>
								<td>
									<label for="searchCustomerType">Typ:</label>
									<select name="searchCustomerType" id="searchCustomerType" class="inputSelect_100">
										<option value=""> </option>
										<?php
											if(!empty($arrCustomerTypeDatas)){
												foreach($arrCustomerTypeDatas as $thisKey => $thisValue){
													$selected = '';
													if($thisValue["customerTypesID"] == $_REQUEST["searchCustomerType"]){
														$selected = ' selected="selected" ';
													}
													echo '<option value="' . $thisValue["customerTypesID"] . '" ' . $selected . ' >' . htmlentities(($thisValue["customerTypesName"])) . '</option>';
												}
											}
										?>
									</select>
								</td>
								<td>
									<label for="searchCustomerGroup">Gruppe:</label>
									<select name="searchCustomerGroup" id="searchCustomerGroup" class="inputSelect_100">
										<option value=""> </option>
										<?php
											if(!empty($arrCustomerGroupDatas)){
												foreach($arrCustomerGroupDatas as $thisKey => $thisValue){
													$selected = '';
													if($thisValue["customerGroupsID"] == $_REQUEST["searchCustomerGroup"]){
														$selected = ' selected="selected" ';
													}
													echo '<option value="' . $thisValue["customerGroupsID"] . '" ' . $selected . ' >' . htmlentities($thisValue["customerGroupsName"]) . '</option>';
												}
											}
										?>
									</select>
								</td>
								<?php if($_COOKIE["isAdmin"] == '1'){ ?>
								<td>
									<label for="searchCustomersActive">Aktiv:</label>
									<select name="searchCustomersActive" id="searchCustomersActive" class="inputSelect_100">
										<option value=""></option>
										<option value="1" <?php if($_REQUEST["searchCustomersActive"] == "1"){ echo ' selected="selected" '; } ?>>aktiv</option>
										<option value="0" <?php if($_REQUEST["searchCustomersActive"] == "0"){ echo ' selected="selected" '; } ?>>nicht aktiv</option>
									</select>
								</td>
								<?php } ?>
								<td>
									<label for="searchCustomerCountry">Land:</label>
									<select name="searchCustomerCountry" id="searchCustomerCountry" class="inputSelect_100">
										<option value=""> </option>
										<?php
											if(!empty($arrCountryTypeDatas)) {
												foreach($arrCountryTypeDatas as $thisKey => $thisValue) {
													$selected = '';
													if($thisKey == $_REQUEST["searchCustomerCountry"]) {
														$selected = ' selected="selected" ';
													}
													$thisFlag = '';
													if($arrCountryTypeDatas[$thisKey]["countriesUnionsUnionType"] == 'EU'){
														$thisFlag = 'EU';
													}
													echo '<option class="unionType_' . $thisFlag . '" value="' . $thisKey . '" '.$selected.' >' . htmlentities(($arrCountryTypeDatas[$thisKey]["countries_name"])). ' [' . $arrCountryTypeDatas[$thisKey]["countries_iso_code_2"] . ']</option>';
												}
											}
										?>
									</select>
								</td>
								<?php if($arrGetUserRights["adminArea"] == '1'){ ?>
								<td>
									<label for="searchNoMail">Ohne Mail:</label>
									<input type="checkbox" name="searchNoMail" id="searchNoMail" value="1" />
								</td>
								<?php } ?>
								<td>
									<label for="searchWord">Suchbegriff:</label>
									<input type="text" name="searchWord" id="searchWord" class="inputField_100" />
								</td>
								<td>
									<input type="hidden" name="xxxeditID" id="xxxeditID" value="" />
									<input type="submit" name="submitSearch" class="inputButton0" value="Suchen" />
								</td>
							</tr>
						</table>
					</form>
				</div>
				<?php } ?>
				<?php displayMessages(); ?>

				<?php
					if($_REQUEST["editID"] == ""){
						if(!empty($arrCustomerDatas)) {
							if($pagesCount > 1 && $_REQUEST["displayOrderStatus"] == "") {
								include(FILE_MENUE_PAGES);
							}

							echo '<table cellpadding="0" cellspacing="0" border="0" width="100%" class="displayOrders">
									<colgroup>
										<col />
										<col />
										<col />
										<col />
										<col />
										<col />
										<col />
										<col />
										<col />
										<col />
										<col />
										<col />
										<col />
										<col />
										<col />
										<col />
									</colgroup>
									<thead>
										<tr>
											<th style="width:45px;text-align:right;">#</th>
											<th>KNR</th>
											<th>Firmenname</th>
											<th>Betriebs-Nr.</th>
											<th>Inhaber</th>
											<th>Land</th>
											<th>PLZ</th>
											<th>Ort</th>
											<th>Straße</th>
											<th>Telefon</th>
											<th>Typ</th>
											<th>PreisListe</th>
											<th>Gruppe</th>
											<th>Vertreter</th>
											<th>Gebiet von ADM / HV</th>
											<th>Details</th>
										</tr>
									</thead>
									<tbody>
							';

							$count = 0;
							foreach($arrCustomerDatas as $thisKey => $thisValue) {
								if($count%2 == 0){ $rowClass = 'row0'; }
								else { $rowClass = 'row1'; }

								$thisStyle = '';
								if($thisValue["customersActive"] != '1') {
									$thisStyle = ' style="color:#FF0000; font-style:italic;" ';
								}
								if($thisValue["customersCompanyNotExists"] == '1') {
									$thisStyle = ' style="text-decoration:line-through;color:#AAA;" ';
								}

								echo '<tr ' . $thisStyle . ' class="'.$rowClass.'">';
									echo '<td style="text-align:right;"><span style="font-weight:bold;cursor:pointer;" title="Datensatz-ID: '.($thisValue["customersID"]).'">'.($count + 1).'.</span></td>';
									echo '<td style="text-align:center;';

									if($thisValue["customersKundennummer"] == $_REQUEST["searchBoxCustomer"]){
										echo 'font-weight:bold;background-color:#B5FF00;';
									}
									echo '">';
									if($arrGetUserRights["editCustomers"] || $arrGetUserRights["displayCustomers"]) {
										echo '<a href="' . PAGE_EDIT_CUSTOMER . '?editID=' . $thisValue["customersID"] . '" title="Kunden bearbeiten (KNR '.$thisValue["customersKundennummer"].')" >';
									}
									echo $thisValue["customersKundennummer"];
									if($arrGetUserRights["editCustomers"] || $arrGetUserRights["displayCustomers"]) {
										echo '</a>';
									}
									echo '</td>';
									echo '<td style="white-space:nowrap;text-align:left;">';
									echo preg_replace("/\//", " / ", htmlentities(($thisValue["customersFirmenname"])));
									if($thisValue["customersFirmennameZusatz"] != '') {
										echo '<br /><span style="font-size:11px;"> &bull; ' . htmlentities(($thisValue["customersFirmennameZusatz"])) . '</span>';
									}
									echo '</td>';

									echo '<td>';
									echo htmlentities(($thisValue["customersGroupCompanyNumber"]));
									echo '</td>';

									echo '<td style="white-space:nowrap;text-align:left;">';
									echo trim(preg_replace("/:/ismU", "", preg_replace("/Inh\./ismU", "", $thisValue["customersFirmenInhaberVorname"].' '.$thisValue["customersFirmenInhaberNachname"])));
									echo '</td>';

									echo '<td>';
									echo $arrCountryTypeDatas[$thisValue["customersCompanyCountry"]]["countries_iso_code_3"];
									echo '</td>';

									echo '<td style="white-space:nowrap;">';
									echo $thisValue["customersCompanyPLZ"];
									echo '</td>';

									echo '<td>';
									echo $thisValue["customersCompanyOrt"];
									echo '</td>';

									echo '<td style="white-space:nowrap;">';
									echo $thisValue["customersCompanyStrasse"] . ' ' , $thisValue["customersCompanyHausnummer"];
									echo '</td>';

									echo '<td style="white-space:nowrap;">';
									echo formatPhoneNumber($thisValue["customersTelefon1"]);
									echo '</td>';

									echo '<td style="cursor:pointer;">';
									echo '<span title="' . $arrCustomerTypeDatas[$thisValue["customersTyp"]]["customerTypesName"] . '">';
									echo $arrCustomerTypeDatas[$thisValue["customersTyp"]]["customerTypesShortName"];
									echo '</span>';
									echo '</td>';

									$thisCustomersPriceListsID = $thisValue["customersPriceListsID"];
									if($arrAgentDatas2[$thisValue["customersVertreterID"]]["customersPriceListsID"] > 0){
										$thisCustomersPriceListsID = $arrAgentDatas2[$thisValue["customersVertreterID"]]["customersPriceListsID"];
									}
									$thisCellStyle = "";
									if($thisCustomersPriceListsID == "1"){
										$thisCellStyle = "color:#990000;";
									}
									else if($thisCustomersPriceListsID == "2"){
										$thisCellStyle = "color:#009900;";
									}
									$thisCellStyle .= "font-size:11px;";
									echo '<td style="' . $thisCellStyle . '">';
									echo '<span title="' . $arrCustomersPriceListDatas[$thisCustomersPriceListsID]["customerPriceListsDescription"] . '">';
									echo $arrCustomersPriceListDatas[$thisCustomersPriceListsID]["customersPriceListsName"];
									echo '</span>';
									echo '</td>';

									echo '<td style="text-align:center;font-size:11px;">';
									$thisArrCustomerGroups = explode(";", stripslashes($thisValue["customersGruppe"]));
									if(!empty($thisArrCustomerGroups)) {
										for($i = 0; $i < count($thisArrCustomerGroups) ; $i++) {
											if($arrCustomerGroupDatas[$thisArrCustomerGroups[$i]]["customerGroupsName"] != '') {
												echo '<span class="nowrap"> &bull; ' . $arrCustomerGroupDatas[$thisArrCustomerGroups[$i]]["customerGroupsName"] . '</span>';
											}
										}
									}
									echo '</td>';
									echo '<td style="white-space:nowrap;font-size:11px;">';
									if($thisValue["customersVertreterID"] > 0) {

										if($arrGetUserRights["editCustomers"] || $arrGetUserRights["displayCustomers"]) {
											echo '<a href="' . PAGE_EDIT_CUSTOMER . '?editID=' . $thisValue["customersVertreterID"] . '" title="Vertreter bearbeiten (Datensatz '.$arrAgentDatas2[$thisValue["customersVertreterID"]]["customersKundennummer"].')" >';
										}
										echo htmlentities(($arrAgentDatas2[$thisValue["customersVertreterID"]]["customersFirmenname"]));
										if($arrGetUserRights["editCustomers"] || $arrGetUserRights["displayCustomers"]) {
											echo '</a>';
										}
									}
									echo '</td>';

									echo '<td style="white-space:nowrap;font-size:11px;">';
									if($thisValue["customersCompanyCountry"] == 81){
										#$thisSalesman = $arrRelationSalesmenZipcodeDatas[substr($thisValue["customersCompanyPLZ"], 0, 2)]["kundenname"];
										/*
										if($thisSalesman == ""){
											$thisSalesman = $arrRelationSalesmenZipcodeDatas[substr($thisValue["customersCompanyPLZ"], 0, 3)]["kundenname"];
										}
										if($thisSalesman == ""){
											$thisSalesman = $arrRelationSalesmenZipcodeDatas[substr($thisValue["customersCompanyPLZ"], 0, 4)]["kundenname"];
										}
										if($thisSalesman == ""){
											$thisSalesman = $arrRelationSalesmenZipcodeDatas[substr($thisValue["customersCompanyPLZ"], 0, 5)]["kundenname"];
										}
										echo $thisSalesman;
										*/

										$arrThisAreaSalesman = $arrRelationSalesmenZipcodeDatas[substr($thisValue["customersCompanyPLZ"], 0, 2)];
										if(empty($arrThisAreaSalesman)){
											$arrThisAreaSalesman = $arrRelationSalesmenZipcodeDatas[substr($thisValue["customersCompanyPLZ"], 0, 3)];
										}
										if(empty($arrThisAreaSalesman)){
											$arrThisAreaSalesman = $arrRelationSalesmenZipcodeDatas[substr($thisValue["customersCompanyPLZ"], 0, 4)];
										}
										if(empty($arrThisAreaSalesman)){
											$arrThisAreaSalesman = $arrRelationSalesmenZipcodeDatas[substr($thisValue["customersCompanyPLZ"], 0, 5)];
										}

										if(!empty($arrThisAreaSalesman)){
											foreach($arrThisAreaSalesman as $thisAreaSalemanData){
												echo ' &bull; ' . $thisAreaSalemanData["kundenname"] . ' (K-NR: ' . $thisAreaSalemanData["kundennummer"] . ')';
												echo '<br />';
											}
										}
									}
									echo '</td>';
									echo '<td style="white-space:nowrap;">';
										if($thisValue["customersNotiz"] != "") {
											echo '<span class="toolItem">';
											echo '<img src="layout/icons/iconRTF.gif" class="buttonNotice" width="16" height="16" alt="Bemerkung" />';
											echo '<div class="displayNoticeArea" style="display:none; visibility:hidden;">';
											echo wordwrap($thisValue["customersNotiz"], 70, '<br />', false);
											echo '</div>';
											echo '</span>';
										}
										else {
											echo '<span class="toolItem"><img src="layout/spacer.gif" width="16" height="16" alt="" /></span>';
										}
										if($arrGetUserRights["editCustomers"] || $arrGetUserRights["displayCustomers"]) {
											echo '<span class="toolItem"><a href="' . PAGE_EDIT_CUSTOMER . '?editID=' . $thisValue["customersID"] . '"><img src="layout/icons/iconEdit.gif" width="16" height="16" title="Kunden bearbeiten (KNR '.$thisValue["customersKundennummer"].')" alt="Bearbeiten" /></a></span>';
										}
										else {
											echo '<span class="toolItem"><img src="layout/spacer.gif" width="16" height="16" alt="" /></span>';
										}

										if($arrGetUserRights["displayOrders"] || $arrGetUserRights["editOrders"]) {
											echo '<span class="toolItem"><a href="' . PAGE_EDIT_PROCESS . '?editCustomersID=' . $thisValue["customersID"] . '&amp;editID=NEW"><img src="layout/menueIcons/menueSidebar/addProcess.png" width="16" height="16" title="Neue Produktion f&uuml;r diesen Kunden anlegen)" alt="Neue Produktion anlegen" /></a></span>';
										}

										if($thisValue["customersMail1"] != "") {
											echo '<span class="toolItem"><a href="mailto:'.$thisValue["customersMail1"].'"><img src="layout/icons/iconMail.gif" width="16" height="14" title="Kunde &quot;'.htmlentities(($thisValue["customersFirmenname"])). '&quot; (KNR: '.$thisValue["customersKundennummer"].') per Mail kontaktieren" alt="per Mail kontaktieren" /></a></span>';
										}
										else {
											echo '<span class="toolItem"><img src="layout/spacer.gif" width="16" height="16" alt="" /></span>';
										}

										if($thisValue["customersHomepage"] != "") {
											echo '<span class="toolItem"><a href="http://'.$thisValue["customersHomepage"].'" target="_blank"><img src="layout/icons/iconWeb.gif" width="16" height="16" title="Homepage des Kunden aufrufen" alt="Homepage" /></a></span>';
										}
										else {
											echo '<span class="toolItem"><img src="layout/spacer.gif" width="16" height="16" alt="" /></span>';
										}

										// BOF SHOW IN GOOGLE MAPS
											if($thisValue["customersCompanyLocation"] != "") {
												$thisGoogleMapsURL = PATH_GOOGLE_MAPS_URL_LATLON;
												$arrCustomerLocationData = explode(":", $thisValue["customersCompanyLocation"]);
												$thisGoogleMapsURL = preg_replace("/{###LAT###}/", $arrCustomerLocationData[0], $thisGoogleMapsURL );
												$thisGoogleMapsURL = preg_replace("/{###LNG###}/", $arrCustomerLocationData[1], $thisGoogleMapsURL );
												echo '<span class="toolItem"><a href="' . $thisGoogleMapsURL . '" target="_blank"><img src="layout/menueIcons/menueSidebar/googleMaps.png" width="16" height="16" title="' . $thisGoogleMapsURL . '" alt="GoogleMaps" /></a></span>';
											}
											else {
												$thisGoogleMapsURL = PATH_GOOGLE_MAPS;
												$thisGoogleMapsURL = preg_replace("/{###STREET_NAME###}/", urlencode($thisValue["customersCompanyStrasse"]), $thisGoogleMapsURL );
												$thisGoogleMapsURL = preg_replace("/{###STREET_NUMBER###}/", urlencode($thisValue["customersCompanyHausnummer"]), $thisGoogleMapsURL );
												$thisGoogleMapsURL = preg_replace("/{###CITY_NAME###}/", urlencode($thisValue["customersCompanyOrt"]), $thisGoogleMapsURL );
												$thisGoogleMapsURL = preg_replace("/{###CITY_ZIP_CODE###}/", urlencode($thisValue["customersCompanyPLZ"]), $thisGoogleMapsURL );
												echo '<span class="toolItem"><a href="' . $thisGoogleMapsURL . '" target="_blank"><img src="layout/menueIcons/menueSidebar/googleMaps.png" width="16" height="16" title="' . $thisGoogleMapsURL . '" alt="GoogleMaps" /></a></span>';
											}
										// EOF SHOW IN GOOGLE MAPS

										// BOF SEARCH VIA GOOGLE
											$thisGoogleSearchUrl = 'https://www.google.de/search?q=';
											$thisGoogleSearchParams = '';
											$thisGoogleSearchParams .= '' . $thisValue["customersFirmenname"];
											$thisGoogleSearchParams .= ' ' . $thisValue["customersCompanyPLZ"];
											$thisGoogleSearchParams .= ' ' . $thisValue["customersCompanyOrt"];
											$thisGoogleSearchParams .= ' ' . $thisValue["customersCompanyStrasse"];
											$thisGoogleSearchParams .= ' ' . $thisValue["customersCompanyHausnummer"];
											$thisGoogleSearchParams = urlencode($thisGoogleSearchParams);
											$thisGoogleSearchUrl .= $thisGoogleSearchParams;
											echo '<span class="toolItem"><a href="'.$thisGoogleSearchUrl.'" target="_blank"><img src="layout/icons/iconGoogle1.png" width="16" height="16" target="_blank" title="Kunden bei Google suchen" alt="Google-Suche" /></a></span>';
										// EOF SEARCH VIA GOOGLE

									echo '</td>';
								echo '</tr>';
								$count++;
							}
							echo '	</tbody>
								</table>
							';

							if($pagesCount > 1 && $_REQUEST["displayOrderStatus"] == "") {
								include(FILE_MENUE_PAGES);
							}
						}
						else {

						}
					}
				?>

				<?php if($_REQUEST["editID"] != ""){ ?>
					<div id="tabs" class="ui-tabs ui-widget ui-widget-content ui-corner-all">
						<div class="adminEditArea">
							<?php if($_REQUEST["editID"] != "NEW") { ?>
							<?php
								$thisPageUrl = $_SERVER["PHP_SELF"] . '?editID=' . $_REQUEST["editID"];
							?>
							<ul class="ui-tabs-nav ui-helper-reset ui-helper-clearfix ui-widget-header ui-corner-all">
								<li class="ui-state-default ui-corner-top"><a href="<?php echo $thisPageUrl; ?>&amp;tab=tabs-1#tabs-1">Kunden-Daten</a></li>

								<?php if($customerDatas["customersNotiz"] != '') { ?>
								<li class="ui-state-default ui-corner-top tabImportant"><a href="<?php echo $thisPageUrl; ?>&amp;tab=tabs-7#tabs-7">Notizen</a></li>
								<?php } ?>


								<li class="ui-state-default ui-corner-top"><a href="<?php echo $thisPageUrl; ?>&amp;tab=tabs-2#tabs-2">Bisherige Bestellungen</a></li>

								<li class="ui-state-default ui-corner-top"><a href="<?php echo $thisPageUrl; ?>&amp;tab=tabs-12#tabs-12">Bisherige Preise</a></li>
								<li class="ui-state-default ui-corner-top"><a href="<?php echo $thisPageUrl; ?>&amp;tab=tabs-13#tabs-13">Sonder Preise</a></li>

								<?php if($_COOKIE["isAdmin"] == 'xxx') { ?>
								<li class="ui-state-default ui-corner-top"><a href="<?php echo PAGE_DISPLAY_ORDERS_OVERVIEW; ?>?searchCustomerNumber=<?php echo $customerDatas["customersKundennummer"]; ?>">Bisherige Produktionen</a></li>
								<?php } ?>

								<li class="ui-state-default ui-corner-top"><a href="<?php echo $thisPageUrl; ?>&amp;tab=tabs-3#tabs-3">Korrektur-Abz&uuml;ge (<?php echo count($arrPdfDatas["Korrektur-Abzug"]); ?>)</a></li>
								<li class="ui-state-default ui-corner-top"><a href="<?php echo $thisPageUrl; ?>&amp;tab=tabs-6#tabs-6">DPD-Sendungen</a></li>
								<li class="ui-state-default ui-corner-top"><a href="<?php echo $thisPageUrl; ?>&amp;tab=tabs-11#tabs-11">TR-Lieferungen</a></li>
								<?php if(!empty($arrDocumentDatas)) { ?>
								<?php } ?>
								<li class="ui-state-default ui-corner-top"><a href="<?php echo $thisPageUrl; ?>&amp;tab=tabs-4#tabs-4">Dokumente</a></li>
								<li class="ui-state-default ui-corner-top"><a href="<?php echo $thisPageUrl; ?>&amp;tab=tabs-5#tabs-5">Dokument-Beziehungen</a></li>

								<li class="ui-state-default ui-corner-top tabImportant"><a href="<?php echo $thisPageUrl; ?>&amp;tab=tabs-10#tabs-10">Erinnerungen</a></li>
								<li class="ui-state-default ui-corner-top"><a href="<?php echo $thisPageUrl; ?>&amp;tab=tabs-14#tabs-14">Gespr&auml;che</a></li>
								<li class="ui-state-default ui-corner-top"><a href="<?php echo $thisPageUrl; ?>&amp;tab=tabs-9#tabs-9">Kontakte</a></li>
								<?php if(in_array($customerDatas["customersTyp"], array('2', '4'))) { ?>
								<li class="ui-state-default ui-corner-top"><a href="<?php echo $thisPageUrl; ?>&amp;tab=tabs-8#tabs-8">Provisionen</a></li>
								<?php } ?>
							</ul>
							<?php } ?>
							<?php if($_REQUEST["tab"] == "tabs-1") { ?>
							<div id="tabs-1">
								<?php displayMessages(); ?>
								<form name="formEditCustomerDatas" id="formEditCustomerDatas" method="post" action="<?php echo $_SERVER["PHP_SELF"]; ?>#tabs-1" enctype="multipart/form-data" >
									<input type="hidden" name="editCustomersID" value="<?php echo $customerDatas["customersID"]; ?>" />
									<input type="hidden" name="tab" value="<?php echo $_REQUEST["tab"]; ?>" />
									<!--
									<input type="hidden" name="editCustomersActive" value="<?php echo $customerDatas["customersActive"]; ?>">
									-->
									<br /><p class="infoArea">Pflichtfelder m&uuml;ssen ausgef&uuml;llt werden!</p>
									<fieldset class="colored1">
										<legend  onclick="myFunction()">Kundendaten</legend>
										<?php
											if($customerDatas["customersBadPaymentBehavior"] == '1' || $customerDatas["customersInvoicesNotPurchased"] == '1' || $customerDatas["customersPaymentOnlyPrepayment"] == '1'){
												echo '<p class="warningArea">';
													echo '<img src="layout/icons/iconAttention.png" width="16" height="16" alt="Achtung" /> ';
													if($customerDatas["customersPaymentOnlyPrepayment"] == '1' ){
														echo ' • Zahlung bei diesem Kunden <b>nur per Vorkasse</b>!' ;
													}
													if($customerDatas["customersBadPaymentBehavior"] == '1' ){
														echo ' • Kunde zahlt unpünktlich!';
													}
													if($customerDatas["customersInvoicesNotPurchased"] == '1' ){
														echo ' • kein FAKTORING-Ankauf!';
													}
												echo '</p>';
											}
										?>
										<table border="0" width="100%" cellspacing="0" cellpadding="0">
											<tr>
												<td style="width:200px;"><b>Kundennummer:</b></td>
												<td>
													<?php
														/*
														if($_REQUEST["editID"] == "NEW") {
															echo '<a href="javascript:void(\'0\');" onclick="createCustomerNumber(\'formEditCustomerDatas\', \'editCustomersKundennummer\', \'editCustomersKundennummer\'); "class="linkButton" id="generateCustomerNumberButton">Kundennummer generieren</a>';
															$readonly = '';
														}
														else {
															$readonly = ' readonly="readonly" ';
														}
														*/

														if($_REQUEST["editID"] == "NEW") {
															$readonly = ' readonly="readonly" ';
															#$description = 'Soll eine <b>Kundennummer automatisch generiert</b> werden, geben Sie bitte die <b>ersten <br />beiden Ziffern der Postleitzahl</b> ein. <br />Die neue Kundennummer wird dann <b>automatisch nach dem Speichern</b> angelegt.';
															$description = 'Soll eine <b>Kundennummer automatisch generiert</b> werden, <br />dieses <b>Feld bitte leer</b> lassen. <br />Die neue Kundennummer wird dann <b>automatisch nach dem Speichern</b> angelegt.';
														}
														else if($customerDatas["customersKundennummer"] != ''){
															$readonly = ' readonly="readonly" ';
															$description = '';
														}
													?>
													<input type="text" name="editCustomersKundennummer" id="editCustomersKundennummer" class="inputField_100" <?php if($_REQUEST["editID"] == "NEW") { echo ' minlength="2" '; } ?> value="<?php echo $customerDatas["customersKundennummer"]; ?>" maxlength="<?php echo (LENGTH_CUSTOMER_NUMBER + 3); ?>" <?php echo $readonly; ?> />

													<?php if($description != '') { echo '<p class="infoArea" style="float:right; width:auto;" >' . $description . '</p>'; } ?>

													<?php
														$checked = '';
														if($_REQUEST["editID"] == "NEW") {
															$checked = '';
														}
														else {
															$checked = '';
															if($customerDatas["customersNoDeliveries"] == '1'){
																$checked = ' checked="checked" ';
															}
														}
													?>
													<br />
													<input type="checkbox" value="1" name="editCustomersNoDeliveries" <?php echo $checked; ?> /><b>Kunde nicht beliefern?</b>
													<?php
														if($_REQUEST["editID"] != "NEW") {
															if($customerDatas["customersNoDeliveries"] == '1'){
																echo ' <span style="color:#CC0000; font-weight:bold;">[Kunde nicht mehr beliefern]</span>';
																$jswindowMessage .= 'ACHTUNG: Dieser Kunde darf nicht mehr beliefert werden!' . "\\n";
															}
															else {
																echo ' <span style="color:#009900; font-weight:bold;">[Kunde darf beliefert werden]</span>';
															}
														}
													?>
													<br />

													<?php
														$checked = ' checked="checked" ';
														if($_REQUEST["editID"] == "NEW") {
															$checked = ' checked="checked" ';
														}
														else {
															$checked = '';
															if($customerDatas["customersActive"] == '1'){
																$checked = ' checked="checked" ';
															}
														}
													?>
													<input type="checkbox" value="1" name="editCustomersActive" <?php echo $checked; ?> /><b>Kunde aktivieren?</b>
													<?php
														if($_REQUEST["editID"] != "NEW") {
															if($customerDatas["customersActive"] == '1'){
																echo ' <span style="color:#009900; font-weight:bold;">[Kunde ist aktiviert]</span>';
															}
															else {
																echo ' <span style="color:#CC0000; font-weight:bold;">[Kunde ist nicht aktiviert]</span>';
															}
														}
													?>
												</td>
											</tr>
											
											<tr>
												<td><b>Auftragsnummer:</b></td>
												<td>
													<?php
														$checked = ' ';
														if($_REQUEST["editID"] == "NEW") {
															$checked = ' ';
														}
														else {
															$checked = '';
															if($customerDatas["customersRequestOrderNumber"] == '1'){
																$checked = ' checked="checked" ';
															}
														}
													?>
													<input type="checkbox" name="editCustomersRequestOrderNumber" value="1" <?php echo $checked; ?>/><b>Kunde verlangt eigene Auftragsnummer?</b>
													<?php
														if($_REQUEST["editID"] != "NEW") {
															if($customerDatas["customersRequestOrderNumber"] != '1'){
																echo ' <span style="color:#009900; font-weight:bold;">[Kunde verlangt keine eigene Auftragsnummer]</span>';
															}
															else {
																echo ' <span style="color:#CC0000; font-weight:bold;">[Kunde verlangt eigene Auftragsnummer]</span>';
															}
														}
													?>
												</td>
											</tr>

											<tr>
												<td><b>Kunden-Existenz:</b></td>
												<td>
													<?php
														$checked = ' ';
														if($_REQUEST["editID"] == "NEW") {
															$checked = ' ';
														}
														else {
															$checked = '';
															if($customerDatas["customersCompanyNotExists"] == '1'){
																$checked = ' checked="checked" ';
															}
														}
													?>
													<input type="checkbox" name="editCustomersCompanyNotExists" value="1" <?php echo $checked; ?>/><b>Kunde existiert NICHT?</b>
													<?php
														if($_REQUEST["editID"] != "NEW") {
															if($customerDatas["customersCompanyNotExists"] != '1'){
																echo ' <span style="color:#009900; font-weight:bold;">[Der Kunde existiert]</span>';
															}
															else {
																echo ' <span style="color:#CC0000; font-weight:bold;">[Der Kunde existiert nicht mehr]</span>';
															}
														}
													?>
												</td>
											</tr>

											<tr>
												<td><b>Rechnungs-Form:</b></td>
												<td>
													<?php
														$checked = ' checked="checked" ';
														if($_REQUEST["editID"] == "NEW") {
															$checked = '';
														}
														else {
															$checked = '';
															if($customerDatas["customersSendPaperInvoice"] == '1'){
																$checked = ' checked="checked" ';
															}
														}
													?>
													<input type="checkbox" value="1" name="editCustomersSendPaperInvoice" <?php echo $checked; ?> /> <b>in Papierform?</b>
													<?php
														if($_REQUEST["editID"] != "NEW") {
															if($customerDatas["customersSendPaperInvoice"] == '1'){
																echo ' <span style="color:#009900; font-weight:bold;">[Kunde m&ouml;chte Rechnung auch in Papierform]</span>';
															}
															else {
																echo ' <span style="color:#CC0000; font-weight:bold;">[Kunde m&ouml;chte Rechnung nicht in Papierform]</span>';
															}
														}
													?>
													<?php
														if($_REQUEST["editID"] != "NEW" && $customerDatas["customersCompanyCountry"] != "81" && preg_match("/^[0-9]/", $customerDatas["customersKundennummer"])){
															echo '<p class="errorArea">' . 'ACHTUNG, die Kundennummer entspricht nicht dem Format f&uuml;r Auslandskunden!!!' . '</p>';
														}
													?>
												</td>
											</tr>
											<?php if(1){ ?>
											<tr>
												<td><b>Faktoring-Info:</b></td>
												<td>
													<?php
														$checked = ' checked="checked" ';
														if($_REQUEST["editID"] == "NEW") {
															$checked = '';
														}
														else {
															$checked = '';
															if($customerDatas["customersSendNoFaktoringInfo"] == '1'){
																$checked = ' checked="checked" ';
															}
														}
													?>
													<input type="checkbox" value="1" name="editCustomersSendNoFaktoringInfo" <?php echo $checked; ?> /> <b>kein Faktoring-Info?</b>
													<?php
														if($_REQUEST["editID"] != "NEW") {
															if($customerDatas["customersSendNoFaktoringInfo"] == '1'){
																echo ' <span style="color:#CC0000; font-weight:bold;">[Faktoring-Info wird nicht mitgesendet]</span>';
															}
															else {
																echo ' <span style="color:#009900; font-weight:bold;">[Faktoring-Info wird mitgesendet]</span>';
															}
														}
													?>
												</td>
											</tr>
											<?php } else { ?>
											<input type="hidden" value="<?php echo $customerDatas["customersSendNoFaktoringInfo"]; ?>" name="editCustomersSendNoFaktoringInfo" />
											<?php } ?>

											<tr>
												<td><b>Fibu-Konto:</b></td>
												<td>
													<input type="text" name="customersTaxAccountID" id="customersTaxAccountID" class="inputField_100 readonly" readonly="readonly" value="<?php echo $customerDatas["customersTaxAccountID"]; ?>" />
													<span class="infotext">Wird automatisch nach Vorgaben des Steuerberaters erzeugt.</span>
												</td>
											</tr>
											<?php if($_COOKIE["mandator"] == 'bctr') { ?>
											<!--
											<tr>
												<td><b>Kunden kopieren:</b></td>
												<td>
													<input type="checkbox" name="copyCustomerToExternMandatory" name="copyCustomerToExternMandatory" checked="checked" value="b3" /> Kundendaten zu <b>B3</b> &uuml;bernehmen?
												</td>
											</tr>
											-->
											<?php } ?>
											<tr>
												<td><b>Kunden-Preisliste:</b></td>
												<td>
													<?php
														$thisCellStyle = "";
														if($customerDatas["customersPriceListsID"] > 0){
															if($customerDatas["customersPriceListsID"] == "1"){
																$thisCellStyle = "background-color:#FF7F84;";
															}
															else if($customerDatas["customersPriceListsID"] == "2"){
																$thisCellStyle = "background-color:#00FF00;";
															}
														}
													?>
													<?php if($_REQUEST["editID"] != "NEW"){ ?>
														<?php if(in_array($customerDatas["customersTyp"], array("3", "4"))){ ?>
															<select style="<?php echo $thisCellStyle; ?> !important;" name="editCustomersPriceList" id="editCustomersPriceList" class="inputSelect_510">
																<?php
																	if(!empty($arrCustomersPriceListDatas)) {
																		foreach($arrCustomersPriceListDatas as $thisKey => $thisValue) {
																			if($thisKey == "1"){
																				$thisCellStyle = "background-color:#FF7F84;";
																			}
																			else if($thisKey == "2"){
																				$thisCellStyle = "background-color:#00FF00;";
																			}
																			$selected = '';
																			if($thisKey == $customerDatas["customersPriceListsID"] && $customerDatas["customersPriceListsID"] != "") {
																				$selected = ' selected="selected" ';
																			}
																			else if($arrCustomersPriceListDatas[$thisKey]["customersPriceListsIsStandard"] == "1"){
																				$selected = ' selected="selected" ';
																			}
																			echo '
																				<option style="' . $thisCellStyle . 'font-weight:bold;" value="' . $thisKey . '" '.$selected.' >' . htmlentities($arrCustomersPriceListDatas[$thisKey]["customersPriceListsName"]). '' . '</option>
																			';
																		}
																	}
																?>
															</select>
														<?php } else if($salesmanDatas["customersPriceListsID"] > 0) { ?>
															<div class="inputArea1" style="<?php echo $thisCellStyle; ?> !important;font-weight:bold;">
															<?php
																echo 'Kunde erh&auml;lt ';
																echo $arrCustomersPriceListDatas[$salesmanDatas["customersPriceListsID"]]["customersPriceListsName"];
																echo ' ';
																echo '&uuml;ber ';
																echo '<span style="font-size:bold;white-space:nowrap;">' . $arrAgentDatas2[$customerDatas["customersVertreterID"]]["customersFirmenname"] . '</span>';
															?>
															</div>
														<?php } else { ?>
																<div class="inputArea1" style="<?php echo $thisCellStyle; ?> !important;font-weight:bold;">
																<?php
																	echo $arrCustomersPriceListDatas[$customerDatas["customersPriceListsID"]]["customersPriceListsName"];
																?>
																</div>
														<?php } ?>
													<?php } ?>
													<?php if($customerDatas["customersTyp"] == 4) { ?>
														<div class="infotext">Dieser Wert wird bei Endkunden automatisch &uuml;bernommen, wenn dem Kunden dieser<br /> <b>Handelsvertreter / Wiederverk&auml;ufer als Vertreter</b> zugeordnet wird.</div>
													<?php } else { ?>
														<div class="infotext">Wird bei Endkunden automatisch erzeugt, abh&auml;ngig davon, ob diesem Kunden ein<br /> <b>Handelsvertreter / Wiederverk&auml;ufer mit Sondervereinbarung</b> zugeordnet ist oder nicht.</div>
													<?php } ?>

												</td>
											</tr>
											<?php
												$thisCellStyle = "";
												if($customerDatas["customersPriceListsID"] > 0){
													if($customerDatas["customersPriceListsID"] == "1"){
														$thisCellStyle = "color:#900;";
													}
													else if($customerDatas["customersPriceListsID"] == "2"){
														$thisCellStyle = "color:#006600;";
													}
												}
											?>
											<tr>
												<td><b>Firmenname:</b></td>
												<td><input style="<?php echo $thisCellStyle; ?>font-weight:bold;" type="text" name="editCustomersFirmenname"id="editCustomersFirmenname" class="inputField_510" value="<?php echo ($customerDatas["customersFirmenname"]); ?>" /></td>
											</tr>
											<tr>
												<td><b>Firmenname-Zusatz:</b></td>
												<td>
													<input type="text" name="editCustomersFirmennameZusatz" id="editCustomersFirmennameZusatz" class="inputField_510" value="<?php echo ($customerDatas["customersFirmennameZusatz"]); ?>" />
													<br /><span class="infotext">bei Amazon-Kunden hier &quot;Name des K&auml;ufers&quot; eintragen!!</span>
												</td>
											</tr>

											<tr>
												<td><b>Betriebsnr.:</b></td>
												<td id="areaCustomersGroupCompanyNumber">
													<input type="text" name="editCustomersGroupCompanyNumber" id="editCustomersGroupCompanyNumber" class="inputField_70" value="<?php echo ($customerDatas["customersGroupCompanyNumber"]); ?>" />
													<span id="loadedParentGroupCompanyNumber"></span>
												</td>
											</tr>

											<tr>
												<td><b>Vertreter dieses Kunden:</b></td>
												<td>
											<?php

												if($customerDatas["customersVertreterID"] > 0) {
													if($arrAgentDatas2[$customerDatas["customersVertreterID"]]["customersFirmenname"] !="") {
														$thisSalesmanValues = '';
														$thisSalesmanValues .= substr($arrAgentDatas2[$customerDatas["customersVertreterID"]]["customersFirmenname"], 0, 50);
														$thisSalesmanValues .= ' | ';
														$thisSalesmanValues .= $arrAgentDatas2[$customerDatas["customersVertreterID"]]["customersKundennummer"];
														$thisSalesmanValues .= ' [';
														$thisSalesmanValues .= substr($arrCustomerTypeDatas[$arrAgentDatas2[$customerDatas["customersVertreterID"]]["customersTyp"]]["customerTypesName"], 0, 10);
														$thisSalesmanValues .= ']';

														if($arrAgentDatas2[$customerDatas["customersVertreterID"]]["customersGroupCompanyNumber"] != ''){
															$thisSalesmanValues .= ' - Betr.-Nr: ';
															$thisSalesmanValues .= $arrAgentDatas2[$customerDatas["customersVertreterID"]]["customersGroupCompanyNumber"];
														}

														//

														if($salesmanDatas["customersPriceListsID"] > 0) {
															$thisCellStyle = "";
															if($customerDatas["customersPriceListsID"] > 0){
																if($customerDatas["customersPriceListsID"] == "1"){
																	$thisCellStyle = "color:#900;";
																}
																else if($customerDatas["customersPriceListsID"] == "2"){
																	$thisCellStyle = "color:#006600;";
																}
															}
														}
													}
												}
												else {
													$thisSalesmanValues = "";
												}

												echo '<input style="' . $thisCellStyle . 'font-weight:bold;" type="text" name="editCustomersVertreterName" id="editCustomersVertreterName" class="inputField_510" value="' . $thisSalesmanValues . '" />';
												echo '<input type="hidden" name="editCustomersVertreterID" id="editCustomersVertreterID" value="' . ($customerDatas["customersVertreterID"]) . '" />';
												echo '<input type="hidden" name="editCustomersOriginalVertreterID" id="editCustomersOriginalVertreterID" value="' . ($customerDatas["customersOriginalVertreterID"]) . '" />';

												if($customerDatas["customersVertreterID"] > 0){
													echo '<a href="' . PAGE_EDIT_CUSTOMER . '?searchCustomerNumber=' . $arrAgentDatas2[$customerDatas["customersVertreterID"]]["customersKundennummer"] . '">';
													echo '<img src="layout/menueIcons/menueQuicklinks/distribution.png" width="16" height="16" style="margin: 0 0 -4px 4px;" title="Die Kundendaten dieses Vertreters anzeigen" alt="Vertreter anzeigen" /> ';
													echo '</a>';
												}

												#echo ' <img src="layout/menueIcons/menueQuicklinks/distribution.png" width="20" height="20" class="buttonOpenZipcodesToSalesman" /> ';

												if($_REQUEST["editID"] != "NEW" && $customerDatas["customersCompanyCountry"] == '81'){
													// BOF GET SALESMAN TO ZIPCODE
														$thisPlzArea = substr($customerDatas["customersCompanyPLZ"], 0, 2);
														#$arrThisAreaSalesman = getSalesmanInZipcodeArea($arrRelationSalesmenZipcodeDatas, $customerDatas["customersCompanyPLZ"]);
														$arrThisAreaSalesman = getSalesmanInZipcodeArea2($arrRelationSalesmenZipcodeDatas, $customerDatas["customersCompanyPLZ"]);
													// EOF GET SALESMAN TO ZIPCODE

													echo '<br />';
													#echo '<p class="infoArea" style="width:502px;" ><b>Unser Au&szlig;endienst-Vertreter im Gebiet ' . $thisPlzArea . ': </b> ' . $arrRelationSalesmenZipcodeDatas[$thisPlzArea]["kundenname"] . ' (K-NR: ' . $arrRelationSalesmenZipcodeDatas[$thisPlzArea]["kundennummer"] . ')</p>';

													$thisStyleHeight = '';
													if(!empty($arrThisAreaSalesman)){
														$thisStyleHeight = 'height:' . ((1 + count($arrThisAreaSalesman)) * 14). 'px;';
													}
													echo '<p class="infoArea" style="width:502px;' . $thisStyleHeight . '" >';
													#echo '<b>Unser Au&szlig;endienst-Vertreter im Gebiet ' . $arrThisAreaSalesman["kundenPLZ"] . ': </b> ' . $arrThisAreaSalesman["kundenname"] . ' (K-NR: ' . $arrThisAreaSalesman["kundennummer"] . ')';

													echo '<b>Unser Au&szlig;endienst-Vertreter im Gebiet ' . $thisPlzArea . ': </b>';
													if(!empty($arrThisAreaSalesman)){
														foreach($arrThisAreaSalesman as $thisAreaSalemanData){
															echo '<br />';
															echo ' &bull; ' . $thisAreaSalemanData["kundenname"];
															echo ' (K-NR: ';

															echo ' <a href="' . PAGE_EDIT_CUSTOMER . '?searchCustomerNumber=' . $thisAreaSalemanData["kundennummer"] . '" title="Kundendaten anzeigen">';
															echo $thisAreaSalemanData["kundennummer"];
															echo '</a>';
															echo  ')';
														}
													}

													if($arrThisAreaSalesman["kundennummer"] != ""){
														echo '<a href="' . PAGE_EDIT_CUSTOMER . '?searchCustomerNumber=' . $arrThisAreaSalesman["kundennummer"] . '">';
														echo '<img src="layout/menueIcons/menueQuicklinks/distribution.png" width="16" height="16" style="margin: 0 0 -4px 4px;" title="Die Kundendaten dieses Vertreters anzeigen" alt="Vertreter anzeigen" /> ';
														echo '</a>';
													}
													echo '</p>';
												}
											?>
												</td>
											</tr>
											<tr>
												<td><b>Kunden-Typ:</b></td>
												<td>
													<select name="editCustomersTyp" id="editCustomersTyp" class="inputSelect_510">
														<option value=""> - </option>
														<?php
															if(!empty($arrCustomerTypeDatas)) {
																foreach($arrCustomerTypeDatas as $thisKey => $thisValue) {
																	$selected = '';
																	if($thisKey == $customerDatas["customersTyp"]) {
																		$selected = ' selected="selected" ';
																	}
																	echo '
																		<option value="' . $thisKey . '" '.$selected.' >' . htmlentities(($arrCustomerTypeDatas[$thisKey]["customerTypesName"])). ' (' . $arrCustomerTypeDatas[$thisKey]["customerTypesShortName"] . ')' . '</option>
																	';
																}
															}
														?>
													</select>
												</td>
											</tr>
											<?php if(in_array($customerDatas["customersTyp"], array(3, 4, 6))) { ?>
											<tr>
												<td><b>Rechnungsarten:</b></td>
												<td>
													<div class="inputArea1">
														<?php if($arrGetUserRights["adminArea"] == '1' && $arrGetUserRights["editInvoices"] == '1') { ?>
														<span class="checkBoxElement1" >
															<input type="checkbox" name="editCustomersGetNoCollectedInvoice" id="editCustomersGetNoCollectedInvoice" <?php if($customerDatas["customersGetNoCollectedInvoice"] == '1' ) { echo 'checked="checked" '; $questionMark = ''; } else { $questionMark = '?'; } ?> value="1" /> Kunde kann Sammelrechnungen erhalten <?php echo $questionMark; ?>
														</span>
														<span class="checkBoxElementSeparator">&nbsp;</span>
														<?php } ?>
														<span class="checkBoxElement1" <?php if($customerDatas["customersGetNoCollectedInvoice"] != '1' ) { echo 'style="background-color:#FF7F84;" '; } else { echo 'style="background-color:#00FF00;" '; } ?>>
															<?php if($customerDatas["customersGetNoCollectedInvoice"] != '1' ) { ?>
																Kunde w&uuml;nscht keine Sammelrechnungen!
															<?php } else { ?>
																Kunde kann Sammelrechnungen erhalten.
															<?php } ?>
														</span>
														<div class="clear"></div>
													</div>
												</td>
											</tr>
											<?php } ?>
											<tr>
												<td><b>Mutter-Firma:</b></td>
												<td>
													<input type="text" name="editCustomersParentCompanyNumber" id="editCustomersParentCompanyNumber" class="inputField_70" value="<?php echo $arrCustomerParentCompanyDatas["parentCompanyNumber"]; ?>" />
													<input type="hidden" name="editCustomersParentCompanyID" id="editCustomersParentCompanyID" value="<?php echo $arrCustomerParentCompanyDatas["parentCompanyID"]; ?>" />
													<?php
														if(!empty($arrCustomerParentCompanyDatas)){
															echo ' <span class="inputArea">';
															echo '<a href="' . PAGE_EDIT_CUSTOMER . '?editCustomerNumber=' . $arrCustomerParentCompanyDatas["parentCompanyNumber"] . '" title="Kunden-Daten anzeigen">' . $arrCustomerParentCompanyDatas["parentCompanyName"] . '</a>';
															echo ' &bull; ' . $arrCustomerParentCompanyDatas["parentCompanyStrasse"] . ' ' . $arrCustomerParentCompanyDatas["parentCompanyHausnummer"];
															echo ' &bull; ' . $arrCustomerParentCompanyDatas["parentCompanyPLZ"] . ' ' . $arrCustomerParentCompanyDatas["parentCompanyOrt"];
															echo ' &bull; ' . $arrCountryTypeDatas[$arrCustomerParentCompanyDatas["parentCompanyCountry"]]["countries_name"];
															echo '</span>';
														}
													?>
												</td>
											</tr>
											<tr>
												<td><b>Tochter-Firmen:</b></td>
												<td>
													<?php
														if(!empty($arrCustomerChildCompanyDatas)){
															echo '<ul style="margin-left:0;padding-left:10px;line-height:12px;">';
															foreach($arrCustomerChildCompanyDatas as $thisKey => $thisValue){
																echo '<li style="margin-left:0;padding-left:0;">';
																	echo ' <span class="infotext">';
																	echo '<a href="' . PAGE_EDIT_CUSTOMER . '?editCustomerNumber=' . $thisValue["childCompanyNumber"] . '" title="Kunden-Daten anzeigen">' . $thisValue["childCompanyNumber"] . ' &bull; ' . $thisValue["childCompanyName"] . '</a>';
																	echo ' <br /> ' . $thisValue["childCompanyStrasse"] . ' ' . $thisValue["childCompanyHausnummer"];
																	echo ' &bull; ' . $thisValue["childCompanyPLZ"] . ' ' . $thisValue["childCompanyOrt"];
																	echo ' &bull; ' . $arrCountryTypeDatas[$thisValue["childCompanyCountry"]]["countries_name"];
																	echo '</span>';
																echo '</li>';
															}
															echo '</ul>';
														}
													?>
												</td>
											</tr>
											<tr>
												<td><b>Kunde seit:</b></td>
												<td>
													<input type="text" name="editCustomersEntryDate" id="editCustomersEntryDate" maxlength="2" class="inputField_100" readonly="readonly" value="<?php if($customerDatas["customersEntryDate"] == "") { echo date("d.m.Y"); } else { echo formatDate($customerDatas["customersEntryDate"], "display"); } ?>" />
												</td>
											</tr>
											<?php if ($_REQUEST["editID"] != "NEW") { ?>

											<?php
												if($customerDatas["customersBadPaymentBehavior"] == '1' || $customerDatas["customersInvoicesNotPurchased"] == '1' || $customerDatas["customersPaymentOnlyPrepayment"] == '1'){
													$jswindowMessage .= 'ACHTUNG:' . "\\n";
													if($customerDatas["customersBadPaymentBehavior"] == '1' ){
														$jswindowMessage .= ' - Kunde zahlt unpünktlich!' . "\\n";
													}
													

													if($customerDatas["customersInvoicesNotPurchased"] == '1' ){
														$jswindowMessage .= ' - kein FAKTORING-Ankauf!' . "\\n";
													}
													if($customerDatas["customersPaymentOnlyPrepayment"] == '1' ){
														$jswindowMessage .= ' - Zahlung nur per Vorkasse!' . "\\n";
													}
												}
											?>

											<tr>
												<td><b>Zahlungsmoral:</b></td>
												<td>
													<div class="inputArea1">
														<?php
															$styleActive = 'background-color:#FF7F84;';
															$styleNotActive = 'background-color:#00FF00;';
														?>
														<span class="checkBoxElement1" <?php if($customerDatas["customersPaymentOnlyPrepayment"] == '1' ) { echo 'style="' . $styleActive . '" '; } else { echo 'style="' . $styleNotActive . '" '; } ?>>
															<input type="checkbox" name="editCustomersPaymentOnlyPrepayment" id="editCustomersPaymentOnlyPrepayment" <?php if($customerDatas["customersPaymentOnlyPrepayment"] == '1' ) { echo 'checked="checked" '; $questionMark = ''; } else { $questionMark = '?'; } ?> value="1" /> Nur Per Vorkasse <?php echo $questionMark; ?>
														</span>
														<span class="checkBoxElementSeparator">&nbsp;</span>
														<span class="checkBoxElement1" <?php if($customerDatas["customersInvoicesNotPurchased"] == '1' ) { echo 'style="' . $styleActive . '" '; } else { echo 'style="' . $styleNotActive . '" '; } ?>>
															<input type="checkbox" name="editCustomersInvoicesNotPurchased" id="editCustomersInvoicesNotPurchased" <?php if($customerDatas["customersInvoicesNotPurchased"] == '1' ) { echo 'checked="checked" '; $questionMark = ''; } else { $questionMark = '?'; } ?> value="1" />kein FAKTORING-Ankauf <?php echo $questionMark; ?>
														</span>
														<span class="checkBoxElementSeparator">&nbsp;</span>
														<span class="checkBoxElement1" <?php if($customerDatas["customersBadPaymentBehavior"] == '1' ) { echo 'style="' . $styleActive . '" '; } else { echo 'style="' . $styleNotActive . '" '; } ?>>
															<input type="checkbox" name="editCustomersBadPaymentBehavior" id="editCustomersBadPaymentBehavior" <?php if($customerDatas["customersBadPaymentBehavior"] == '1' ) { echo 'checked="checked" '; $questionMark = ''; } else { $questionMark = '?'; } ?> value="1" /> Kunde zahlt unp&uuml;nktlich <?php echo $questionMark; ?>
														</span>
														<div class="clear"></div>
													</div>
												</td>
											</tr>
											<?php } ?>
											<tr>
												<td><b>Firmeninhaber 1:</b></td>
												<td>
													<select name="editCustomersFirmenInhaberAnrede" id="editCustomersFirmenInhaberAnrede" class="inputSelect_70">
														<option value=""> - </option>
														<?php
															if(!empty($arrSalutationTypeDatas)) {
																foreach($arrSalutationTypeDatas as $thisKey => $thisValue) {
																	$selected = '';
																	if($thisKey == $customerDatas["customersFirmenInhaberAnrede"]) {
																		$selected = ' selected="selected" ';
																	}
																	echo '
																		<option value="' . $thisKey . '" '.$selected.' >' . htmlentities($arrSalutationTypeDatas[$thisKey]["salutationTypesName"]). '</option>
																	';
																}
															}
														?>
													</select> -
													<input type="text" name="editCustomersFirmenInhaberVorname" id="editCustomersFirmenInhaberVorname" class="inputField_205" value="<?php echo ($customerDatas["customersFirmenInhaberVorname"]); ?>" /> &nbsp;
													<input type="text" name="editCustomersFirmenInhaberNachname" id="editCustomersFirmenInhaberNachname" class="inputField_205" value="<?php echo ($customerDatas["customersFirmenInhaberNachname"]); ?>" />
													<br /><span class="infotext">(Anrede / Vorname / Nachname)</span>
												</td>
											</tr>

											<tr>
												<td><b>Firmeninhaber 2:</b></td>
												<td>
													<select name="editCustomersFirmenInhaber2Anrede" id="editCustomersFirmenInhaber2Anrede" class="inputSelect_70">
														<option value=""> - </option>
														<?php
															if(!empty($arrSalutationTypeDatas)) {
																foreach($arrSalutationTypeDatas as $thisKey => $thisValue) {
																	$selected = '';
																	if($thisKey == $customerDatas["customersFirmenInhaber2Anrede"]) {
																		$selected = ' selected="selected" ';
																	}
																	echo '
																		<option value="' . $thisKey . '" '.$selected.' >' . htmlentities($arrSalutationTypeDatas[$thisKey]["salutationTypesName"]). '</option>
																	';
																}
															}
														?>
													</select> -
													<input type="text" name="editCustomersFirmenInhaber2Vorname" id="editCustomersFirmenInhaber2Vorname" class="inputField_205" value="<?php echo ($customerDatas["customersFirmenInhaber2Vorname"]); ?>" /> &nbsp;
													<input type="text" name="editCustomersFirmenInhaber2Nachname" id="editCustomersFirmenInhaber2Nachname" class="inputField_205" value="<?php echo ($customerDatas["customersFirmenInhaber2Nachname"]); ?>" />
													<br /><span class="infotext">(Anrede / Vorname / Nachname)</span>
												</td>
											</tr>

											<tr>
												<td><b>UmsatzsteuerID (UstID):</b></td>
												<td>
													<input type="text" name="editCustomersUstID" id="editCustomersUstID" class="inputField_120" value="<?php echo $customerDatas["customersUstID"]; ?>" />
													<?php
														#dd('customerDatas');
														$thisCustomersUstID = formatVatID($customerDatas["customersUstID"]);
														if($thisCustomersUstID) {
															#DEFINE('COMPANY_VAT_LIVE_CHECK', true);
															$thisCustomersUstID = formatVatID($customerDatas["customersUstID"]);
															$customers_vat_id_message = '';
															if(preg_match("/^DE/", $customerDatas["customersUstID"])){
																require_once('inc/vat_validation.class.php');
																$vatID = new vat_validation($thisCustomersUstID, $arrCountryTypeDatas[$customerDatas["customersCompanyCountry"]]);
																$customers_vat_id_status = $vatID->vat_info['vat_id_status'];
																$customers_vat_id_message = $vatID->getVatStatusResult($customers_vat_id_status);
																$customers_vat_id_message = strip_tags($customers_vat_id_message);
															}
															else if($arrCountryTypeDatas[$customerDatas["customersRechnungsadresseLand"]]["countriesUnionsUnionType"] == 'EU'){
																require_once("inc/checkForeignCountryVatID.inc.php");
																$arrCheckForeignCountryVatID = checkForeignCountryVatID('HTTPS', COMPANY_UID_NUMBER, $thisCustomersUstID, '', '', '', '', '');
																$customers_vat_id_status = $arrCheckForeignCountryVatID["StatusMessage"];
																$customers_vat_id_message = $customers_vat_id_status;
															}
															else {
																require_once('inc/vat_validation.class.php');
																$vatID = new vat_validation($thisCustomersUstID, $arrCountryTypeDatas[$customerDatas["customersCompanyCountry"]]);
																$customers_vat_id_status = $vatID->vat_info['vat_id_status'];
																$customers_vat_id_message = $vatID->getVatStatusResult($customers_vat_id_status);
																$customers_vat_id_message = strip_tags($customers_vat_id_message);
															}

															echo '<span style="color:#FF0000;"><b>' . $customers_vat_id_message . '</b></span>';
														}
													?>
												</td>
											</tr>
											<tr>
												<td><b>Kunden-Gruppe:</b></td>
												<td>
													<div class="inputArea1">
													<?php
														if(!empty($arrCustomerGroupDatas)) {
															$thisArrCustomerGroups = explode(";", stripslashes($customerDatas["customersGruppe"]));
															foreach($arrCustomerGroupDatas as $thisKey => $thisValue) {
																$checked = '';
																$thisStyle = '';
																if(!empty($thisArrCustomerGroups)) {
																	if(in_array($thisKey, $thisArrCustomerGroups)) {
																		$checked = ' checked ';
																		$thisStyle = 'font-weight:bold;background-color:#00FF00;';
																	}
																}
																echo '<span class="checkBoxElement1" style="' . $thisStyle . '">';
																echo '<input type="checkbox" name="editCustomersGruppe[]" value="' . $thisKey . '" '.$checked.'/> ';
																echo $arrCustomerGroupDatas[$thisKey]["customerGroupsName"];
																echo '</span>';
															}
														}
													?>
													</div>
												</td>
											</tr>
											<tr>
												<td><b>Firmenansprechpartner 1:</b></td>
												<td>
													<select name="editCustomersAnsprechpartner1Anrede" id="editCustomersAnsprechpartner1Anrede" class="inputSelect_70">
														<option value=""> - </option>
														<?php
															if(!empty($arrSalutationTypeDatas)) {
																foreach($arrSalutationTypeDatas as $thisKey => $thisValue) {
																	$selected = '';
																	if($thisKey == $customerDatas["customersAnsprechpartner1Anrede"]) {
																		$selected = ' selected="selected" ';
																	}
																	echo '
																		<option value="' . $thisKey . '" '.$selected.' >' . htmlentities($arrSalutationTypeDatas[$thisKey]["salutationTypesName"]). '</option>
																	';
																}
															}
														?>
													</select> -
													<input type="text" name="editCustomersAnsprechpartner1Vorname" id="editCustomersAnsprechpartner1Vorname" class="inputField_205" value="<?php echo ($customerDatas["customersAnsprechpartner1Vorname"]); ?>" /> &nbsp;
													<input type="text" name="editCustomersAnsprechpartner1Nachname" id="editCustomersAnsprechpartner1Nachname" class="inputField_205" value="<?php echo ($customerDatas["customersAnsprechpartner1Nachname"]); ?>" />
													<br /><span class="infotext">(Anrede / Vorname / Nachname)</span>
												</td>
											</tr>
											<tr>
												<td><b>Firmenansprechpartner 2:</b></td>
												<td>
													<select name="editCustomersAnsprechpartner2Anrede" id="editCustomersAnsprechpartner2Anrede" class="inputSelect_70">
														<option value=""> - </option>
														<?php
															if(!empty($arrSalutationTypeDatas)) {
																foreach($arrSalutationTypeDatas as $thisKey => $thisValue) {
																	$selected = '';
																	if($thisKey == $customerDatas["customersAnsprechpartner2Anrede"]) {
																		$selected = ' selected="selected" ';
																	}
																	echo '
																		<option value="' . $thisKey . '" '.$selected.' >' . htmlentities($arrSalutationTypeDatas[$thisKey]["salutationTypesName"]). '</option>
																	';
																}
															}
														?>
													</select> -
													<input type="text" name="editCustomersAnsprechpartner2Vorname" id="editCustomersAnsprechpartner2Vorname" class="inputField_205" value="<?php echo ($customerDatas["customersAnsprechpartner2Vorname"]); ?>" /> &nbsp;
													<input type="text" name="editCustomersAnsprechpartner2Nachname" id="editCustomersAnsprechpartner2Nachname" class="inputField_205" value="<?php echo ($customerDatas["customersAnsprechpartner2Nachname"]); ?>" />
													<br /><span class="infotext">(Anrede / Vorname / Nachname)</span>
												</td>
											</tr>
											<tr>
												<td><b>Telefon 1:</b></td>
												<td>
													<input type="text" name="editCustomersTelefon1" id="editCustomersTelefon1" class="inputField_510" value="<?php echo formatPhoneNumber($customerDatas["customersTelefon1"], $arrPhoneCodes); ?>" />
													<?php
														if($customerDatas["customersTelefon1"] != "") {
															echo createPhoneCallLink($customerDatas["customersTelefon1"]);
														}
													?>
												</td>
											</tr>
											<tr>
												<td><b>Telefon 2:</b></td>
												<td>
													<input type="text" name="editCustomersTelefon2" id="editCustomersTelefon2" class="inputField_510" value="<?php echo formatPhoneNumber($customerDatas["customersTelefon2"], $arrPhoneCodes); ?>" />
													<?php
														if($customerDatas["customersTelefon2"] != "") {
															echo createPhoneCallLink($customerDatas["customersTelefon2"]);
														}
													?>
												</td>
											</tr>
											<tr>
												<td><b>Mobil 1:</b></td>
												<td><input type="text" name="editCustomersMobil1" id="editCustomersMobil1" class="inputField_510" value="<?php echo formatPhoneNumber($customerDatas["customersMobil1"]); ?>" /></td>
											</tr>
											<tr>
												<td><b>Mobil 2:</b></td>
												<td><input type="text" name="editCustomersMobil2" id="editCustomersMobil2" class="inputField_510" value="<?php echo formatPhoneNumber($customerDatas["customersMobil2"]); ?>" /></td>
											</tr>
											<tr>
												<td><b>Fax 1:</b></td>
												<td><input type="text" name="editCustomersFax1" id="editCustomersFax1" class="inputField_510" value="<?php echo formatPhoneNumber($customerDatas["customersFax1"], $arrPhoneCodes); ?>" /></td>
											</tr>
											<tr>
												<td><b>Fax 2:</b></td>
												<td><input type="text" name="editCustomersFax2" id="editCustomersFax2" class="inputField_510" value="<?php echo formatPhoneNumber($customerDatas["customersFax2"], $arrPhoneCodes); ?>" /></td>
											</tr>
											<tr>
												<td><b>Mail-Adresse 1:</b></td>
												<td>
													<input type="text" name="editCustomersMail1" id="editCustomersMail1" class="inputField_510" value="<?php echo ($customerDatas["customersMail1"]); ?>" />
													<?php
														if($customerDatas["customersMail1"] != "") {
															echo ' <a href="mailto:' . $customerDatas["customersMail1"] . '"><img src="layout/icons/iconMail.gif" width="16" height="14" alt="Mail-Kontakt" title="Dem Kunden eine E-Mail schreiben" /></a>';
														}
													?>
												</td>
											</tr>
											<tr>
												<td><b>Mail-Adresse 2:</b></td>
												<td>
													<input type="text" name="editCustomersMail2" id="editCustomersMail2" class="inputField_510" value="<?php echo ($customerDatas["customersMail2"]); ?>" />
													<?php
														if($customerDatas["customersMail2"] != "") {
															echo ' <a href="mailto:' . $customerDatas["customersMail2"] . '"><img src="layout/icons/iconMail.gif" width="16" height="14" alt="Mail-Kontakt" title="Dem Kunden eine E-Mail schreiben" /></a>';
														}
													?>
												</td>
											</tr>
											<tr>
												<td><b>Homepage:</b></td>
												<td>
													<input type="text" name="editCustomersHomepage" id="editCustomersHomepage" class="inputField_510" value="<?php echo ($customerDatas["customersHomepage"]); ?>" />
													<?php
														if($customerDatas["customersHomepage"] != ''){
															echo '<a href="http://' . $customerDatas["customersHomepage"] . '" target="_blank"><img src="layout/icons/iconWeb.gif" width="16" height="16" alt="Homepage" title="Zur Homepage" /></a>';
														}
													?>
												</td>
											</tr>
										</table>
									</fieldset>

									<fieldset id="Firmenadresse">
										<legend>Firmenanschrift</legend>
										<p>
										<?php
											$checked = '';
											if(
												$customerDatas["customersUseSalesmanInvoiceAdress"] != '1'
												&& $customerDatas["customersRechnungsadresseFirmenname"] == $customerDatas["customersFirmenname"]
												&& $customerDatas["customersRechnungsadresseFirmennameZusatz"] == $customerDatas["customersFirmennameZusatz"]
												&& $customerDatas["customersRechnungsadresseStrasse"] == $customerDatas["customersCompanyStrasse"]
												&& $customerDatas["customersRechnungsadresseHausnummer"] == $customerDatas["customersCompanyHausnummer"]
												&& $customerDatas["customersRechnungsadressePLZ"] == $customerDatas["customersCompanyPLZ"]
												&& $customerDatas["customersRechnungsadresseOrt"] == $customerDatas["customersCompanyOrt"]
												&& $customerDatas["customersRechnungsadresseLand"] == $customerDatas["customersCompanyCountry"]
											) { $checked = ' checked="checked" '; }
										?>
										<input type="checkbox" name="Rechnungsadresse_Firmenadresse" id="Rechnungsadresse_Firmenadresse" value="1" <?php echo $checked; ?> /> <b>Firmenadresse als Rechnungsadresse verwenden?</b>

										<?php
											$checked = '';
											if(
												$customerDatas["editCustomersUseSalesmanDeliveryAdress"] != '1'
												&& $customerDatas["customersLieferadresseFirmenname"] == $customerDatas["customersFirmenname"]
												&& $customerDatas["customersLieferadresseFirmennameZusatz"] == $customerDatas["customersFirmennameZusatz"]
												&& $customerDatas["customersLieferadresseStrasse"] == $customerDatas["customersCompanyStrasse"]
												&& $customerDatas["customersLieferadresseHausnummer"] == $customerDatas["customersCompanyHausnummer"]
												&& $customerDatas["customersLieferadressePLZ"] == $customerDatas["customersCompanyPLZ"]
												&& $customerDatas["customersLieferadresseOrt"] == $customerDatas["customersCompanyOrt"]
												&& $customerDatas["customersLieferadresseLand"] == $customerDatas["customersCompanyCountry"]
											) { $checked = ' checked="checked" '; }
										?>
										<input type="checkbox" name="Lieferadresse_Firmenadresse" id="Lieferadresse_Firmenadresse" value="1" <?php echo $checked; ?> /> <b>Firmenadresse als Lieferadresse verwenden?</b>
										</p>
										<table border="0" width="100%" cellspacing="0" cellpadding="0">
											<tr>
												<td style="width:200px;"><b>Stra&szlig;e / Hausnummer:</b></td>
												<td>
													<input type="text" name="editCustomersCompanyStrasse" id="editCustomersFirmenadresseStrasse" class="inputField_427" value="<?php echo ($customerDatas["customersCompanyStrasse"]); ?>" /> /
													<input type="text" name="editCustomersCompanyHausnummer" id="editCustomersFirmenadresseHausnummer" class="inputField_70" value="<?php echo ($customerDatas["customersCompanyHausnummer"]); ?>" />
												</td>
											</tr>
											<tr>
												<td><b>PLZ / Ort:</b></td>
												<td>
													<input type="text" name="editCustomersCompanyPLZ" id="editCustomersFirmenadressePLZ" class="inputField_70" value="<?php echo ($customerDatas["customersCompanyPLZ"]); ?>" /> /
													<input type="text" name="editCustomersCompanyOrt" id="editCustomersFirmenadresseOrt" class="inputField_427" value="<?php echo ($customerDatas["customersCompanyOrt"]); ?>" /></td>
											</tr>
											<tr>
												<td><b>Land:</b></td>
												<td>
													<select name="editCustomersCompanyCountry" id="editCustomersFirmenadresseLand" class="inputSelect_510" <?php if ($_REQUEST["editID"] == "NEW") {} ?> >
														<option value=""> - </option>
														<?php
															if(!empty($arrCountryTypeDatas)) {
																foreach($arrCountryTypeDatas as $thisKey => $thisValue) {
																	$selected = '';
																	if($_REQUEST["editID"] == "NEW" && $thisKey == 81) {
																		$selected = ' selected="selected" ';
																	}
																	else if($thisKey == $customerDatas["customersCompanyCountry"]) {
																		$selected = ' selected="selected" ';
																	}
																	$thisFlag = '';
																	if($arrCountryTypeDatas[$thisKey]["countriesUnionsUnionType"] == 'EU'){
																		$thisFlag = 'EU';
																	}
																	echo '<option class="unionType_' . $thisFlag . '" value="' . $thisKey . '" '.$selected.' >' . htmlentities(($arrCountryTypeDatas[$thisKey]["countries_name"])). ' [' . $arrCountryTypeDatas[$thisKey]["countries_iso_code_2"] . ']</option>';
																}
															}
														?>
													</select>
												</td>
											</tr>
											<tr>
												<td><b>Geo-Koordinaten:</b></td>
												<td>
													<?php if($arrGetUserRights["adminArea"] == 1){ ?>
													<input type="text" name="editCustomersCompanyLocation" class="inputSelect_510" value="<?php echo $customerDatas["customersCompanyLocation"]; ?>" readonly="readonly" />
													<?php } else { ?>
													<input type="hidden" name="editCustomersCompanyLocation" value="<?php echo $customerDatas["customersCompanyLocation"]; ?>" />
													<?php
														#echo $customerDatas["customersCompanyLocation"];
														$thisCustomersCompanyLocation = $customerDatas["customersCompanyLocation"];
														if($thisCustomersCompanyLocation != ''){
															$arrCustomersCompanyLocation = explode(':', $thisCustomersCompanyLocation);
															echo '<b>LAT</b>: ' . $arrCustomersCompanyLocation[0] . ' | ' . '<b>LON</b>: ' . $arrCustomersCompanyLocation[1];
														}
													?>
													<?php } ?>

													<?php
														// BOF SHOW IN GOOGLE MAPS
															if($customerDatas["customersCompanyLocation"] != ''){
																$thisGoogleMapsURL = PATH_GOOGLE_MAPS_URL_LATLON;
																$arrCustomerLocationData = explode(":", $customerDatas["customersCompanyLocation"]);
																$thisGoogleMapsURL = preg_replace("/{###LAT###}/", $arrCustomerLocationData[0], $thisGoogleMapsURL );
																$thisGoogleMapsURL = preg_replace("/{###LNG###}/", $arrCustomerLocationData[1], $thisGoogleMapsURL );
																echo '<span style="padding:0 4px 0 4px;"><a href="' . $thisGoogleMapsURL . '" target="_blank"><img src="layout/menueIcons/menueSidebar/googleMaps.png" width="16" height="16" title="Standort bei GoogleMaps anzeigen" alt="GoogleMaps" /></a></span>';
															}
															else {
																$thisGoogleMapsURL = PATH_GOOGLE_MAPS;
																$thisGoogleMapsURL = preg_replace("/{###STREET_NAME###}/", urlencode($customerDatas["customersCompanyStrasse"]), $thisGoogleMapsURL );
																$thisGoogleMapsURL = preg_replace("/{###STREET_NUMBER###}/", urlencode($customerDatas["customersCompanyHausnummer"]), $thisGoogleMapsURL );
																$thisGoogleMapsURL = preg_replace("/{###CITY_NAME###}/", urlencode($customerDatas["customersCompanyOrt"]), $thisGoogleMapsURL );
																$thisGoogleMapsURL = preg_replace("/{###CITY_ZIP_CODE###}/", urlencode($customerDatas["customersCompanyPLZ"]), $thisGoogleMapsURL );
																echo '<span style="padding:0 4px 0 4px;"><a href="' . $thisGoogleMapsURL . '" target="_blank"><img src="layout/menueIcons/menueSidebar/googleMaps.png" width="16" height="16" title="' . $thisGoogleMapsURL . '" alt="GoogleMaps" /></a></span>';
															}
														// EOF SHOW IN GOOGLE MAPS

														// BOF SEARCH VIA GOOGLE
															$thisGoogleSearchUrl = 'https://www.google.de/search?q=';
															$thisGoogleSearchParams = '';
															$thisGoogleSearchParams .= '' . $customerDatas["customersFirmenname"];
															$thisGoogleSearchParams .= ' ' . $customerDatas["customersCompanyPLZ"];
															$thisGoogleSearchParams .= ' ' . $customerDatas["customersCompanyOrt"];
															$thisGoogleSearchParams .= ' ' . $customerDatas["customersCompanyStrasse"];
															$thisGoogleSearchParams .= ' ' . $customerDatas["customersCompanyHausnummer"];
															$thisGoogleSearchParams = urlencode($thisGoogleSearchParams);
															$thisGoogleSearchUrl .= $thisGoogleSearchParams;
															echo '<span style="padding:0 4px 0 4px;"><a href="'.$thisGoogleSearchUrl.'" target="_blank"><img src="layout/icons/iconGoogle1.png" width="16" height="16" target="_blank" title="Kunden bei Google suchen" alt="Google-Suche" /></a></span>';
														// EOF SEARCH VIA GOOGLE
													?>
												</td>
											</tr>

										</table>
									</fieldset>

									<fieldset id="Rechnungsadresse">
										<legend>Rechnungsanschrift</legend>
										<div>
											<?php
												if($customerDatas["customersUseSalesmanInvoiceAdress"] == '1'){
													$checked = ' checked="checked" ';
													$disabled = ' disabled="disabled" ';
												}
												else {
													$checked = '';
													$disabled = '';
												}
											?>
											<p>
											<input type="checkbox" name="editCustomersUseSalesmanInvoiceAdress" id="editCustomersUseSalesmanInvoiceAdress" value="1" <?php echo $checked; ?> /> <label for="editCustomersUseSalesmanInvoiceAdress">Anschrift des Vertreters verwenden?</label>
											</p>
										</div>
										<table border="0" width="100%" cellspacing="0" cellpadding="0">
											<?php if($_COOKIE["isAdmin"] == '1') { ?>
											<tr>
												<td style="width:200px;"><b>Rechnungsadresse-KNR:</b></td>
												<td>
													<input type="text" name="editCustomersRechnungsadresseKundennummer" id="editCustomersRechnungsadresseKundennummer" class="inputField_100" value="<?php echo ($customerDatas["customersRechnungsadresseKundennummer"]); ?>" <?php echo $disabled; ?> />
												</td>
											</tr>
											<?php } ?>
											<tr>
												<td style="width:200px;"><b>Firma / Name:</b></td>
												<td>
													<input type="text" name="editCustomersRechnungsadresseFirmenname" id="editCustomersRechnungsadresseFirmenname" class="inputField_510" value="<?php echo ($customerDatas["customersRechnungsadresseFirmenname"]); ?>" <?php echo $disabled; ?> />
												</td>
											</tr>
											<tr>
												<td style="width:200px;"><b>Firma - Zusatz:</b></td>
												<td>
													<input type="text" name="editCustomersRechnungsadresseFirmennameZusatz" id="editCustomersRechnungsadresseFirmennameZusatz" class="inputField_510" value="<?php echo ($customerDatas["customersRechnungsadresseFirmennameZusatz"]); ?>" <?php echo $disabled; ?> />
												</td>
											</tr>
											<tr>
												<td><b>Stra&szlig;e / Hausnummer:</b></td>
												<td>
													<input type="text" name="editCustomersRechnungsadresseStrasse" id="editCustomersRechnungsadresseStrasse" class="inputField_427" value="<?php echo ($customerDatas["customersRechnungsadresseStrasse"]); ?>" <?php echo $disabled; ?> /> /
													<input type="text" name="editCustomersRechnungsadresseHausnummer" id="editCustomersRechnungsadresseHausnummer" class="inputField_70" value="<?php echo ($customerDatas["customersRechnungsadresseHausnummer"]); ?>" <?php echo $disabled; ?> />
												</td>
											</tr>
											<tr>
												<td><b>PLZ / Ort:</b></td>
												<td>
													<input type="text" name="editCustomersRechnungsadressePLZ" id="editCustomersRechnungsadressePLZ" class="inputField_70" value="<?php echo ($customerDatas["customersRechnungsadressePLZ"]); ?>" <?php echo $disabled; ?> /> /
													<input type="text" name="editCustomersRechnungsadresseOrt" id="editCustomersRechnungsadresseOrt" class="inputField_427" value="<?php echo ($customerDatas["customersRechnungsadresseOrt"]); ?>" <?php echo $disabled; ?> /></td>
											</tr>
											<tr>
												<td><b>Land:</b></td>
												<td>
													<select name="editCustomersRechnungsadresseLand" id="editCustomersRechnungsadresseLand" class="inputSelect_510" <?php echo $disabled; ?> >
														<option value=""> - </option>
														<?php
															if(!empty($arrCountryTypeDatas)) {
																foreach($arrCountryTypeDatas as $thisKey => $thisValue) {
																	$selected = '';
																	if($_REQUEST["editID"] == "NEW" && $thisKey == 81) {
																		$selected = ' selected="selected" ';
																	}
																	else if($thisKey == $customerDatas["customersRechnungsadresseLand"]) {
																		$selected = ' selected="selected" ';
																	}
																	$thisFlag = '';
																	if($arrCountryTypeDatas[$thisKey]["countriesUnionsUnionType"] == 'EU'){
																		$thisFlag = 'EU';
																	}
																	echo '<option class="unionType_' . $thisFlag . '" value="' . $thisKey . '" '.$selected.' >' . htmlentities(($arrCountryTypeDatas[$thisKey]["countries_name"])). ' [' . $arrCountryTypeDatas[$thisKey]["countries_iso_code_2"] . ']</option>';
																}
															}
														?>
													</select>
												</td>
											</tr>
										</table>
									</fieldset>

									<fieldset id="Lieferadresse">
										<legend>Lieferanschrift</legend>
										<div>
											<?php
												if($customerDatas["customersUseSalesmanDeliveryAdress"] == '1'){
													$checked = ' checked="checked" ';
													$disabled = ' disabled="disabled" ';
												}
												else {
													$checked = '';
													$disabled = '';
												}
											?>
											<p>
											<input type="checkbox" name="editCustomersUseSalesmanDeliveryAdress" id="editCustomersUseSalesmanDeliveryAdress" value="1" <?php echo $checked; ?> /> <label for="editCustomersUseSalesmanDeliveryAdress">Anschrift des Vertreters verwenden?</label>
											</p>
										</div>
										<table border="0" width="100%" cellspacing="0" cellpadding="0">
											<?php if($_COOKIE["isAdmin"] == '1') { ?>
											<tr>
												<td style="width:200px;"><b>Lieferadresse-KNR:</b></td>
												<td>
													<input type="text" name="editCustomersLieferadresseKundennummer" id="editCustomersLieferadresseKundennummer" class="inputField_100" value="<?php echo ($customerDatas["customersLieferadresseKundennummer"]); ?>" <?php echo $disabled; ?> />
												</td>
											</tr>
											<?php } ?>
											<tr>
												<td style="width:200px;"><b>Firma / Name:</b></td>
												<td>
													<input type="text" name="editCustomersLieferadresseFirmenname" id="editCustomersLieferadresseFirmenname" class="inputField_510" value="<?php echo ($customerDatas["customersLieferadresseFirmenname"]); ?>" <?php echo $disabled; ?> />
												</td>
											</tr>
											<tr>
												<td style="width:200px;"><b>Firma - Zusatz:</b></td>
												<td>
													<input type="text" name="editCustomersLieferadresseFirmennameZusatz" id="editCustomersLieferadresseFirmennameZusatz" class="inputField_510" value="<?php echo ($customerDatas["customersLieferadresseFirmennameZusatz"]); ?>" <?php echo $disabled; ?> />
												</td>
											</tr>
											<tr>
												<td><b>Stra&szlig;e / Hausnummer:</b></td>
												<td>
													<input type="text" name="editCustomersLieferadresseStrasse" id="editCustomersLieferadresseStrasse" class="inputField_427" value="<?php echo ($customerDatas["customersLieferadresseStrasse"]); ?>" <?php echo $disabled; ?> /> /
													<input type="text" name="editCustomersLieferadresseHausnummer" id="editCustomersLieferadresseHausnummer" class="inputField_70" value="<?php echo ($customerDatas["customersLieferadresseHausnummer"]); ?>" <?php echo $disabled; ?> />
												</td>
											</tr>
											<tr>
												<td><b>PLZ / Ort:</b></td>
												<td>
													<input type="text" name="editCustomersLieferadressePLZ" id="editCustomersLieferadressePLZ" class="inputField_70" value="<?php echo ($customerDatas["customersLieferadressePLZ"]); ?>" <?php echo $disabled; ?> /> /
													<input type="text" name="editCustomersLieferadresseOrt" id="editCustomersLieferadresseOrt" class="inputField_427" value="<?php echo ($customerDatas["customersLieferadresseOrt"]); ?>" <?php echo $disabled; ?> />
												</td>
											</tr>
											<tr>
												<td><b>Land:</b></td>
												<td>
													<select name="editCustomersLieferadresseLand" id="editCustomersLieferadresseLand" class="inputSelect_510" <?php echo $disabled; ?> >
														<option value=""> - </option>
														<?php
															if(!empty($arrCountryTypeDatas)) {
																foreach($arrCountryTypeDatas as $thisKey => $thisValue) {
																	$selected = '';
																	if($_REQUEST["editID"] == "NEW" && $thisKey == 81) {
																		$selected = ' selected="selected" ';
																	}
																	else if($thisKey == $customerDatas["customersLieferadresseLand"]) {
																		$selected = ' selected="selected" ';
																	}
																	$thisFlag = '';
																	if($arrCountryTypeDatas[$thisKey]["countriesUnionsUnionType"] == 'EU'){
																		$thisFlag = 'EU';
																	}
																	echo '<option class="unionType_' . $thisFlag . '" value="' . $thisKey . '" '.$selected.' >' . htmlentities(($arrCountryTypeDatas[$thisKey]["countries_name"])). ' [' . $arrCountryTypeDatas[$thisKey]["countries_iso_code_2"] . ']</option>';
																}
															}
														?>
													</select>
												</td>
											</tr>
										</table>
									</fieldset>

									<fieldset>
										<legend>Bankdaten</legend>
										<?php
											if($customerDatas["customersBadPaymentBehavior"] == '1' || $customerDatas["customersInvoicesNotPurchased"] == '1' || $customerDatas["customersPaymentOnlyPrepayment"] == '1'){
												echo '<p class="warningArea">';
													echo '<img src="layout/icons/iconAttention.png" width="16" height="16" alt="Achtung" /> ';
													if($customerDatas["customersPaymentOnlyPrepayment"] == '1' ){
														echo ' • Zahlung bei diesem Kunden <b>nur per Vorkasse</b>!' ;
													}
													if($customerDatas["customersBadPaymentBehavior"] == '1' ){
														echo ' • Kunde zahlt unpünktlich!';
													}
													if($customerDatas["customersInvoicesNotPurchased"] == '1' ){
														echo ' • kein FAKTORING-Ankauf!';
													}
												echo '</p>';
											}
										?>
										<table border="0" width="100%" cellspacing="0" cellpadding="0">
											<tr>
												<td><b>Bezahlart:</b></td>
												<td>
													<?php
														if($customerDatas["customersPaymentOnlyPrepayment"] == '1'){

														}
													?>
													<select name="editCustomersBezahlart" id="editCustomersBezahlart" class="inputSelect_510">
														<?php if($customerDatas["customersPaymentOnlyPrepayment"] != '1'){ ?>
														<option value=""> - </option>
														<?php } ?>
														<?php
															if(!empty($arrPaymentTypeDatas)) {
																foreach($arrPaymentTypeDatas as $thisKey => $thisValue) {
																	$selected = '';
																	if($_REQUEST["editID"] == "NEW" && $thisKey == 1) {
																		$selected = ' selected="selected" ';
																	}
																	else if($customerDatas["customersPaymentOnlyPrepayment"] == '1'){
																		if($thisKey == '2'){
																			$selected = ' selected="selected" ';
																		}
																	}
																	else if($thisKey == $customerDatas["customersBezahlart"]) {
																		$selected = ' selected="selected" ';
																	}
																	if(($customerDatas["customersPaymentOnlyPrepayment"] == '1' && $thisKey == '2') || $customerDatas["customersPaymentOnlyPrepayment"] != '1'){
																		echo '
																			<option value="' . $thisKey . '" '.$selected.' >' . htmlentities($arrPaymentTypeDatas[$thisKey]["paymentTypesName"]). '</option>
																		';
																	}
																}
															}
														?>
													</select>
													<?php
														#if($_REQUEST["editID"] != "NEW"){
														if(1){
															echo '<span style="padding: 0 0 0 2px;"><img src="layout/icons/mailAttachement.png" class="buttonSendDocument" data_mail="" rel="' . $customerDatas["customersKundennummer"] . '_' . date("Y-m-d") . '_' . basename(PATH_SEPA_FORM). '#SP" width="16" height="16" title="SEPA-Formular direkt per Mail an Kunden versenden" alt="SEPA-Formular versenden" /></span>';
														}
													?>
												</td>
											</tr>
											<tr id="areaSepaData" style="display:none;">
												<td><b>SEPA-Formular:</b></td>
												<td style="white-space: nowrap;">
													<div style="border:1px solid #FF0000;height:20px;line-height:18px;" class="inputArea1" >
														<?php
															$checked = '';
															if($customerDatas["customersSepaExists"] == "1"){
																$checked = ' checked="checked" ';
																$thisStyle = '';
															}
														?>
														<span class="checkBoxElement1" <?php if($customerDatas["customersSepaExists"] == "1" ) { echo 'style="background-color:#00FF00;" '; } else { echo 'style="background-color:#FF7F84;" '; } ?>><input type="checkbox" name="editCustomersSepaExists" id="editCustomersSepaExists" value="1" <?php echo $checked; ?> /> liegt vor?</span>
														<span class="checkBoxElement1">g&uuml;ltig bis: <input type="text" style="height:16px;" name="editCustomersSepaValidUntilDate" id="editCustomersSepaValidUntilDate" maxlength="2" class="inputField_100" readonly="readonly" value="<?php echo formatDate($customerDatas["customersSepaValidUntilDate"], "display"); ?>" /> </span>
														<span class="checkBoxElement1">angefordert am: <input type="text" style="height:16px;" name="editCustomersSepaRequestedDate" id="editCustomersSepaRequestedDate" maxlength="2" class="inputField_100" readonly="readonly" value="<?php echo formatDate($customerDatas["customersSepaRequestedDate"], "display"); ?>" /> </span>
													</div>
												</td>
											</tr>
											<tr>
												<td><b>Zahlungskondition:</b></td>
												<td>
													<select name="editCustomersZahlungskondition" id="editCustomersZahlungskondition" class="inputSelect_510">
														<?php if($customerDatas["customersPaymentOnlyPrepayment"] != '1'){ ?>
														<option value=""> - </option>
														<?php } ?>
														<?php
															if(!empty($arrPaymentConditionDatas)) {
																foreach($arrPaymentConditionDatas as $thisKey => $thisValue) {
																	$selected = '';
																	if($_REQUEST["editID"] == "NEW" && $thisKey == 5) {
																		$selected = ' selected="selected" ';
																	}
																	else if($customerDatas["customersPaymentOnlyPrepayment"] == '1'){
																		if($thisKey == '2'){
																			$selected = ' selected="selected" ';
																		}
																	}
																	else if($thisKey == $customerDatas["customersZahlungskondition"]) {
																		$selected = ' selected="selected" ';
																	}
																	if(($customerDatas["customersPaymentOnlyPrepayment"] == '1' && $thisKey == '2') || $customerDatas["customersPaymentOnlyPrepayment"] != '1'){
																		echo '
																			<option value="' . $thisKey . '" '.$selected.' >' . htmlentities($arrPaymentConditionDatas[$thisKey]["paymentConditionsName"]). '</option>
																		';
																	}
																}
															}
														?>
													</select>
												</td>
											</tr>
											<tr>
												<td style="width:200px;"><b>Kontoinhaber:</b></td>
												<td><input type="text" name="editCustomersKontoinhaber" id="editCustomersKontoinhaber" class="inputField_510" value="<?php echo ($customerDatas["customersKontoinhaber"]); ?>" /></td>
											</tr>
											<tr>
												<td style="width:200px;"><b>Bankname:</b></td>
												<td><input type="text" name="editCustomersBankName" id="editCustomersBankName" class="inputField_510" value="<?php echo ($customerDatas["customersBankName"]); ?>" /></td>
											</tr>
											<tr>
												<td><b>Bankleitzahl:</b></td>
												<td><input type="text" name="editCustomersBankLeitzahl" id="editCustomersBankLeitzahl" class="inputField_510" value="<?php echo ($customerDatas["customersBankLeitzahl"]); ?>" /></td>
											</tr>
											<tr>
												<td><b>Kontonummer:</b></td>
												<td><input type="text" name="editCustomersBankKontonummer" id="editCustomersBankKontonummer" class="inputField_510" value="<?php echo ($customerDatas["customersBankKontonummer"]); ?>" /></td>
											</tr>
											<tr>
												<td><b>IBAN:</b></td>
												<td><input type="text" name="editCustomersBankIBAN" id="editCustomersBankIBAN" class="inputField_510" value="<?php echo ($customerDatas["customersBankIBAN"]); ?>" /></td>
											</tr>
											<tr>
												<td><b>BIC:</b></td>
												<td><input type="text" name="editCustomersBankBIC" id="editCustomersBankBIC" class="inputField_510" value="<?php echo ($customerDatas["customersBankBIC"]); ?>" /></td>
											</tr>
										</table>
									</fieldset>

									<fieldset>
										<legend>Sonstige Daten</legend>
										<table border="0" width="100%" cellspacing="0" cellpadding="0">
											<tr>
												<td style="width:200px;"><b>Standard-Rabatt:</b></td>
												<td>
													<?php
														if($customerDatas["customersRabatt"] == "") {
															$customerDatas["customersRabatt"] = 0;
														}
													?>
													<input type="text" name="editCustomersRabatt" id="editCustomersRabatt" class="inputField_40" value="<?php echo number_format($customerDatas["customersRabatt"], 2, ',', ''); ?>" />

													<select name="editCustomersRabattType" id="editCustomersRabattType" class="inputSelect_40">
														<option value="percent" style="font-weight:bold;" <?php if($customerDatas["customersRabattType"] == 'percent') { echo ' selected="selected" '; } ?> >%</option>
														<option value="fixed" style="font-weight:bold;" <?php if($customerDatas["customersRabattType"] == 'fixed') { echo ' selected="selected" '; } ?>>&euro;</option>
													</select>
													(Rabatt auf gesamten Nettobetrag)
												</td>
											</tr>
											<!--
											<tr>
												<td><b>Artikel-Rabatte verwenden:</b></td>
												<td>
													<?php
														if($customerDatas["customersUseProductDiscount"] == 1){
															$checked = ' checked="checked" ';
														}
														else {
															$checked = '';
														}
													?>
													<input type="checkbox" name="editCustomersUseProductDiscount" id="editCustomersUseProductDiscount" value="1" <?php echo $checked; ?> /> ja
												</td>
											</tr>
											<tr>
												<td><b>Artikel-MwSt. verwenden:</b></td>
												<td>
													<?php
														if($customerDatas["customersUseProductMwst"] == 1){
															$checked = ' checked="checked" ';
														}
														else {
															$checked = '';
														}
													?>
													<input type="checkbox" name="editCustomersUseProductMwst" id="editCustomersUseProductMwst" value="1" <?php echo $checked; ?> /> ja
												</td>
											</tr>
											-->
											<tr>
												<td><b>Bemerkungen / Notizen:</b></td>
												<td><textarea name="editCustomersNotiz" id="editCustomersNotiz" rows="10" cols="20" class="inputTextarea_510x140" ><?php echo ($customerDatas["customersNotiz"]); ?></textarea></td>
											</tr>
										</table>
									</fieldset>
									<div class="actionButtonsArea">
										<?php if($arrGetUserRights["editCustomers"]) { ?>

											<?php if($_REQUEST["editID"] != "NEW") { ?>
											<input type="submit" class="inputButton1 inputButtonGreen" name="storeDatas" value="Speichern" onclick="return checkFormDutyFields(this.form.name, <?php echo $jsonFormDutyFields; ?>);" />
											<?php } else { ?>
											<input type="submit" class="inputButton1 inputButtonGreen" name="storeDatas" value="Speichern" onclick="return checkCustomerData(this.form.name, <?php echo $jsonFormDutyFields; ?>);" />
											<?php } ?>
										<?php
											if($_REQUEST["editID"] != "NEW") {
												if($checkCustomerNumberIsUsed > 0){
													$disableLink = '';
													$disableClass = '';
													$disableTitle = '';
													if($customerDatas["customersDatasUpdated"] != '1') {
														$disableLink = ' onclick="return false" ';
														$disableClass = ' disabled ';
														$disableTitle = ' gesperrt ';
													}
												}
										?>

										<?php if($checkCustomerNumberIsUsed < 1){ ?>
										&nbsp;
										<input type="submit" class="inputButton1 inputButtonRed" name="deleteDatas" value="Endg&uuml;ltig l&ouml;schen" onclick="return showWarning('Wollen Sie diesen Eintrag wirklich endgültig entfernen???\nOder soll der Kunde nur deaktiviert werden?');" />
										<?php } ?>
										&nbsp;
										<?php
											}
										?>
										<input type="submit" class="inputButton1 inputButtonOrange" name="resetDatas" value="Abbrechen" onclick="return showWarning('Wollen Sie diesen Vorgang wirklich abbrechen??? ');" />
										<?php } ?>
									</div>
								</form>
								<?php
									// BOF GET SALESMAN TO ZIPCODE
									#$arrRelationSalesmenZipcodeDatas = getRelationSalesmenZipcode();
									$arrRelationSalesmenZipcodeDatas = getRelationSalesmenZipcode2();

									// BOF NEW SORT OF ZIPCODES
										$arrRelationSalesmenZipcodeDatasSorted = array();
										// BOF ADD BURHAN TO NON ADM-AREAS
											$arrRelationSalesmenZipcodeDatasSorted["00000"]["00"][13301] = array(
																"kundenname" => "BURHAN CTR",
																"kundennummer" => "48464",
																"kundenID" => 13301,
																"kundenPLZ" => "00",
																"kundenPLZaktiv" => 0
															);
										// EOF ADD BURHAN TO NON ADM-AREAS

										if(!empty($arrRelationSalesmenZipcodeDatas)){
											foreach($arrRelationSalesmenZipcodeDatas as $thisKey => $thisValue){
												$thisKeyNew = $thisKey . str_repeat("0", (5 - strlen($thisKey)));
												$arrRelationSalesmenZipcodeDatasSorted[$thisKeyNew][$thisKey] = $arrRelationSalesmenZipcodeDatas[$thisKey];
											}
											ksort($arrRelationSalesmenZipcodeDatasSorted);
										}
									// EOF NEW SORT OF ZIPCODES

									// BOF ADD BURHAN TO NON ADM-AREAS
									if(!1){
										$arrDoubleZipcodes = getDoubleZipcodes();
										if(!empty($arrDoubleZipcodes)){
											foreach($arrDoubleZipcodes as $thisKey => $thisValue){
												if(empty($arrRelationSalesmenZipcodeDatas[$thisKey])){
													$arrRelationSalesmenZipcodeDatas[$thisKey] = array(
													#$arrRelationSalesmenZipcodeDatas[$thisKey][13301] = array(
														"kundenname" => "BURHAN CTR e. K.",
														"kundennummer" => "48464",
														"kundenID" => 13301,
														"kundenPLZ" => $thisKey,
														"kundenPLZaktiv" => 0
													);
												}
											}
										}
										#ksort($arrRelationSalesmenZipcodeDatas);
									}
									// EOF ADD BURHAN TO NON ADM-AREAS

									if(!empty($arrRelationSalesmenZipcodeDatas)){
										echo '<div class="sideInfo">';
											echo '<div class="sideInfoHeader">PLZ | Au&szlig;endienst-Mitarbeiter</div>';
											echo '<div class="sideInfoContent">';
											$count = 0;

											$thisZipCodeMarker = '';

											foreach($arrRelationSalesmenZipcodeDatasSorted as $arrRelationSalesmenZipcodeDatas){
												foreach($arrRelationSalesmenZipcodeDatas as $thisPlzKey => $thisPlzValue){
													foreach($thisPlzValue as $thisValue){
														if($count%2 == 0){ $rowClass = 'row0'; }
														else { $rowClass = 'row1'; }
														$thisStyle = '';
														if($thisZipCodeMarker != substr($thisPlzKey, 0, 2)){
															$thisZipCodeMarker = substr($thisPlzKey, 0, 2);
															$thisStyle = 'border-top:2px solid #F00; ';
															echo '<div class="sideInfoItem" style="' . $thisStyle . 'font-weight:bold;letter-spacing:1px;background-color:#F5FF9F;">';
																echo '<div class="sideInfoTitle">';
																echo 'PLZ-Gebiet ' . $thisPlzKey;
																echo '</div>';
																echo '<div class="clear"></div>';
															echo '</div>';
														}

														echo '<div style="" class="sideInfoItem ' . $rowClass . '" id="cid_' . $thisValue["kundenID"] . '_' . $thisPlzKey . '" rel="' . $thisValue["kundenID"] . '" title="Diesen Au&szlig;endienst-Mitarbeiter per Klick als Vertreter &uuml;bernehmen" >';
															echo '<div class="sideInfoTitle" style="float:left;width:20px;">' . $thisPlzKey . '</div>';
															echo '<div class="sideInfoText" style="white-space:nowrap;overflow:hidden;">' . $thisValue["kundenname"] . '</div>';
															echo '<div class="clear"></div>';
														echo '</div>';
														$count++;
													}
												}
											}
											echo '</div>';
										echo '</div>';
									}
									// EOF GET SALESMAN TO ZIPCODE
								?>
							</div>
							<?php } ?>

							<?php if($_REQUEST["editID"] != "NEW") { ?>

							<?php if($_REQUEST["tab"] == "tabs-7") { ?>
							<?php if($customerDatas["customersNotiz"] != '') { ?>
									<div id="tabs-7">
										<fieldset>
											<legend>Notizen</legend>
											<?php echo nl2br(htmlentities(($customerDatas["customersNotiz"]))); ?>
										</fieldset>
									</div>
							<?php } ?>
							<?php } ?>

							<?php if(!empty($arrDocumentDatas)) { ?>
							<?php if($_REQUEST["tab"] == "tabs-4") { ?>
							<div id="tabs-4">
								<fieldset style="width:90% !important;">
									<legend>Dokumente</legend>
									<?php
										if(!empty($arrDocumentDatas)) {
											foreach($arrDocumentDatas as $thisDocumentKey => $thisDocumentValue){

												if($thisDocumentKey == 'BR') { $thisDocumentUrl = PAGE_DISPLAY_LETTER; }
												else if($thisDocumentKey == 'RE') { $thisDocumentUrl = PAGE_DISPLAY_INVOICE; }
												else if($thisDocumentKey == 'AN') { $thisDocumentUrl = PAGE_DISPLAY_OFFER; }
												else if($thisDocumentKey == 'MA') { $thisDocumentUrl = PAGE_DISPLAY_REMINDER; }
												else if($thisDocumentKey == 'M1') { $thisDocumentUrl = PAGE_DISPLAY_FIRST_DEMAND; }
												else if($thisDocumentKey == 'M2') { $thisDocumentUrl = PAGE_DISPLAY_SECOND_DEMAND; }
												else if($thisDocumentKey == 'AB') { $thisDocumentUrl = PAGE_DISPLAY_CONFIRMATION; }
												else if($thisDocumentKey == 'LS') { $thisDocumentUrl = PAGE_DISPLAY_DELIVERY; }
												else if($thisDocumentKey == 'GU') { $thisDocumentUrl = PAGE_DISPLAY_CREDIT; }

												echo '<h2>' . $arrDocumentTypeDatas[$thisDocumentKey]["createdDocumentsTypesName2"] . ' (' . $thisDocumentKey . ')</h2>';

												echo '<div class="displayDocumentsArea" >';

												echo '<table border="0" cellpadding="0" cellspacing="0" style="width:99%;" class="displayOrders">';

												echo '<colgroup>';
												echo '<col style="width:30px !important;">';
												echo '<col style="width:66px !important;">';
												echo '<col style="width:66px !important;">';
												echo '<col />';
												echo '<col style="width:110px !important;">';
												echo '<col />';
												echo '<col />';
												echo '<col style="width:100px !important;">';
												echo '<col style="width:60px !important;">';
												echo '</colgroup>';

												echo '<tr>';
												echo '<th style="width:30px !important;text-align:right;">#</th>';
												echo '<th style="width:66px !important;">Datum</th>';
												echo '<th style="width:66px !important;">&Auml;nderung</th>';
												#echo '<th style="width:45px;">Kund.Nr</th>';
												echo '<th>Dokument-Empf&auml;ngername</th>';
												echo '<th style="width:110px !important;">Dokument &auml;ndern</th>';

												if($thisDocumentKey != "BR"){
													echo '<th style="width:50px !important;">Summe</th>';
													#echo '<th style="width:200px !important;">Dokument umwandeln / kopieren</th>';
													echo '<th style="width:110px !important;">Dokument umwandeln</th>';
												}
												else {
													echo '<th style="width:70px !important;">&nbsp;</th>';
													echo '<th style="width:90px !important;">&nbsp;</th>';
												}
												echo '<th style="width:100px !important;">Zahl-Status</th>';
												echo '<th style="width:60px !important;">Info</th>';
												echo '</tr>';

												$count = 0;
												foreach($thisDocumentValue as $thisKey => $thisValue){

													// BOF GET DOCUMENT RECIPIENT MAIL ADRESS
														$thisDocumentsInvoiceRecipientData = unserialize($thisDocumentValue[$thisKey]["orderDocumentsInvoiceRecipientData"]);
														$thisDocumentsDeliveryRecipientData = unserialize($thisDocumentValue[$thisKey]["orderDocumentsDeliveryRecipientData"]);

														$thisDocumentInvoiceRecipientMail = $thisDocumentsInvoiceRecipientData["selectCustomersRecipientMail"];
														$thisDocumentDeliveryRecipientMail = $thisDocumentsDeliveryRecipientData["selectCustomersRecipientMail"];

														if($thisDocumentInvoiceRecipientMail == ''){
															if($customerDatas["customersMail1"] != "") {
																$thisDocumentInvoiceRecipientMail = $customerDatas["customersMail1"];
															}
															else if($customerDatas["customersMail2"] != "") {
																$thisDocumentInvoiceRecipientMail = $customerDatas["customersMail2"];
															}
															else {
																$thisDocumentInvoiceRecipientMail = '';
															}
														}
													// EOF GET DOCUMENT RECIPIENT MAIL ADRESS

													$thisDocumentDateYear = substr($thisDocumentValue[$thisKey]["orderDocumentsProcessingDate"], 0, 4);
													#if($thisDocumentDateYear > (date("Y") - 2)){
													if(1){
														if($count%2 == 0){ $rowClass = 'row0'; }
														else { $rowClass = 'row1'; }
														$rowClass2 = '';
														if($thisDocumentValue[$thisKey]["orderDocumentsStatus"] == "1") {
															$rowClass2 = 'row2';
														}
														else if($thisDocumentValue[$thisKey]["orderDocumentsStatus"] == "2") {
															$rowClass2 = 'row3';
														}
														else if($thisDocumentValue[$thisKey]["orderDocumentsStatus"] == "3") {
															$rowClass2 = 'row6';
															$rowClass = 'row6';
														}
														else if($thisDocumentValue[$thisKey]["orderDocumentsStatus"] == "4") {
															$rowClass2 = 'row2';
														}
														else if($thisDocumentValue[$thisKey]["orderDocumentsStatus"] == "6") {
															$rowClass2 = 'row6';
														}
														else if($thisDocumentValue[$thisKey]["orderDocumentsStatus"] == "6") {
															$rowClass2 = 'row7';
														}
														else if($thisDocumentValue[$thisKey]["orderDocumentsStatus"] == "10") {
															$rowClass2 = 'row3';
														}
														else if($thisDocumentValue[$thisKey]["orderDocumentsStatus"] == "5") {
															// $rowClass2 = 'row5';
															$rowClass2 = 'row3';
														}

														echo '<tr class="'.$rowClass.'">';

														echo '<td style="text-align:right;"><b>' . ($count + 1). '.</b></td>';

														echo '<td>' . formatDate($thisDocumentValue[$thisKey]["orderDocumentsDocumentDate"], 'display') . '</td>';
														echo '<td>' . formatDate($thisDocumentValue[$thisKey]["orderDocumentsProcessingDate"], 'display') . '</td>';

														#echo '<td><a href="' . PAGE_EDIT_CUSTOMER . '?editCustomerNumber=' . $thisDocumentValue[$thisKey]["orderDocumentsCustomerNumber"] . '" title="Kunden anzeigen">' . $thisDocumentValue[$thisKey]["orderDocumentsCustomerNumber"] . '</a></td>';

														echo '<td>';
														
														if($thisDocumentValue[$thisKey]["orderDocumentsIsCollectiveInvoice"] == '1'){
															echo '<span style="color:#FF0000;font-weight:bold;font-size:10px;">';
															if($thisDocumentKey == 'AB'){ echo 'Die AB ist Teil einer SAMMELRECHNUNG'; }
															else { echo 'SAMMELRECHNUNG'; }
															echo '</span><br />';
														}
														#echo $thisDocumentValue[$thisKey]["orderDocumentsAddressCompany"];
														if($thisDocumentValue[$thisKey]["orderDocumentDetailKommission"] != ""){
															#echo '<br />xxx<span class="remarksArea">';
															#echo '<b>Komission</b>:' . $thisDocumentValue[$thisKey]["orderDocumentDetailKommission"];
															#echo '</span>';
														}
														#dd('arrConnectedDocumentIds');
														$thisDocumentNumber = $thisDocumentValue[$thisKey]["orderDocumentsNumber"];
														#dd('thisDocumentNumber');
														if($arrConnectedDocumentIds[$thisDocumentNumber]["documentsAddressCompany"] != ""){
															echo $arrConnectedDocumentIds[$thisDocumentNumber]["documentsAddressCompany"];
														}
														if($arrConnectedDocumentIds[$thisDocumentNumber]["documentsKommission"] != ""){
															echo '<br /><span class="remarksArea">';
															echo '<b>Komission</b>:' . $arrConnectedDocumentIds[$thisDocumentNumber]["documentsKommission"];
															echo '</span>';
														}
														if($arrConnectedDocumentIds[$thisDocumentNumber]["documentsPrintText"] != ""){
															echo '<br /><span class="remarksArea">';
															echo '<b>Aufdruck</b>:' . $arrConnectedDocumentIds[$thisDocumentNumber]["documentsPrintText"];
															echo '</span>';
														}
														echo '</td>';

														echo '<td>';
															echo '<form name="formSubmitEditDocument" method="post" action="' . PAGE_CREATE_DOCUMENT . '">';
															echo '<table width="100" cellpadding="0" cellspacing="0" class="noBorder">';
															echo '<tr>';
																echo '<td>';
																	echo $thisDocumentValue[$thisKey]["orderDocumentsNumber"];
																echo '</td>';

																echo '<td>';

																	$isDocEditable = checkIfDocumentIsEditable($thisDocumentKey, $thisDocumentValue[$thisKey]["orderDocumentsNumber"]);
																	if($isDocEditable){
																		echo '<input type="hidden" name="editID" value="' . $thisDocumentValue[$thisKey]["orderDocumentsCustomerNumber"] . '">';
																		echo '<input type="hidden" name="originDocumentType" value="' . $thisDocumentKey . '">';
																		echo '<input type="hidden" name="originDocumentID" value="' . $thisKey . '">';
																		echo '<input type="hidden" name="originDocumentNumber" value="' . $thisDocumentValue[$thisKey]["orderDocumentsNumber"] . '">';
																		echo '<input type="hidden" name="editDocType" value="' . $thisDocumentKey . '">';
																		echo '<input type="image" src="layout/icons/iconEdit.gif" title="' . $arrDocumentTypeDatas[$thisDocumentKey]["createdDocumentsTypesName"] . ' ' . $thisDocumentValue[$thisKey]["orderDocumentsDocumentNumber"] . ' &auml;ndern" />';
																		echo '<a href="deleteOrderDocuments.php?documentType='. $thisDocumentValue[$thisKey]['tableDocumentType'].'&documentNumber='.$thisDocumentValue[$thisKey]['orderDocumentsID'].'" 
																				onclick="return confirm(\'Wollen Sie diesen Vorgang wirklich löschen??? ?\');" >
																				<img src="layout/icons/iconDelete.png" title="' . $arrDocumentTypeDatas[$thisDocumentKey]["createdDocumentsTypesName"] . ' ' . $thisDocumentValue[$thisKey]["orderDocumentsDocumentNumber"] . ' Löschen" /></a>';
																		 

																	}
																	else {
																		echo '<img src="layout/icons/iconNotOk.png" width="16" height="16" alt="&Auml;nderung nicht mehr m&ouml;glich!" title="&Auml;nderung nicht mehr m&ouml;glich!" />';
																	}
																echo '</td>';
															echo '</tr>';
															echo '</table>';
															echo '</form>';
														echo '</td>';

														if($thisDocumentKey != "BR"){
														# if($thisDocumentValue[$thisKey]["orderDocumentsIsCollectiveInvoice"] != '1' && $thisDocumentKey != "BR"){
															echo '<td style="text-align:right;white-space:nowrap;">' . convertDecimal($thisDocumentValue[$thisKey]["orderDocumentsTotalPrice"], 'display') . ' &euro;</td>';
															echo '<td>';
															echo $thisDocumentValue[$thisKey]["orderDocumentsNumber"];
													  		echo '<a href="' . $thisDocumentUrl . '?searchDocumentNumber=' . $thisDocumentValue[$thisKey]["orderDocumentsNumber"] . '">';
															echo ' <img src="layout/icons/convertDocument.png" width="16" height="16" title="Dokument kopieren oder umwandeln" alt="Dokument kopieren oder umwandeln" />';
															echo '</a>';
															echo '</td>';
														}
														else {
															echo '<td>';
															echo '</td>';
															echo '<td>';
															echo '</td>';
														}
														echo '<td class="'.$rowClass2.'">';
														#echo $thisDocumentValue[$thisKey]["orderDocumentsStatus"];
														echo $arrPaymentStatusTypeDatas[$thisDocumentValue[$thisKey]["orderDocumentsStatus"]]["paymentStatusTypesName"];
														echo '</td>';

														echo '<td>';
														echo '<span class="toolItem">';
														#echo 'xxx<a href="' . $_SERVER["PHP_SELF"] . '?editCustomerNumber=' . $thisDocumentValue[$thisKey]["orderDocumentsCustomerNumber"] . '&amp;editID=' . $_REQUEST["editID"] . '&amp;downloadFile=' . urlencode(basename($thisDocumentValue[$thisKey]["orderDocumentsDocumentPath"])) . '&amp;documentType=' . $thisDocumentKey . '">' . '<img src="layout/icons/icon' . getFileType(basename($thisDocumentValue[$thisKey]["orderDocumentsDocumentPath"])) . '.gif" width="16" height="16" title="Dokument herunterladen" alt="Download" /></a>';
														echo '<a href="' . $_SERVER["PHP_SELF"] . '?editCustomerNumber=' . $customerDatas["customersKundennummer"] . '&amp;editID=' . $_REQUEST["editID"] . '&amp;downloadFile=' . urlencode(basename($thisDocumentValue[$thisKey]["orderDocumentsDocumentPath"])) . '&amp;documentType=' . $thisDocumentKey . '">' . '<img src="layout/icons/icon' . getFileType(basename($thisDocumentValue[$thisKey]["orderDocumentsDocumentPath"])) . '.gif" width="16" height="16" title="Dokument herunterladen" alt="Download" /></a>';
														echo '</span>';
														echo '<span class="toolItem">';
														echo '<img src="layout/icons/iconEye.gif" class="buttonLoadOrderDetails" width="16" height="16" title="Details ansehen" alt="' . $thisDocumentKey . '#' . $arrDocumentTypeDatas[$thisDocumentKey]["createdDocumentsTypesName2"] . '#' . $thisKey . '" />';
														echo '</span>';
														echo '<span class="toolItem">';
														echo '<img src="layout/icons/mailAttachement.png" class="buttonSendDocument" data_mail="' . $thisDocumentInvoiceRecipientMail . '" rel="' . urlencode(basename($thisDocumentValue[$thisKey]["orderDocumentsDocumentPath"])) . '#' . $thisDocumentKey . '" width="16" height="16" title="Dokument direkt per Mail versenden" alt="Dokument versenden" />';
														echo '</span>';

														echo '</td>';
														echo '</tr>';
														$count++;
													}
												}

												echo '</table>';
												echo '</div>';

												echo '<hr />';
											}
										}
										else {
											$infoMessage .= 'Es sind keine Dokumente vorhanden.' . '<br />';
											displayMessages();
										}
									?>
									</fieldset>
								</div>
								<?php } ?>
								<?php } ?>

								<?php if($_REQUEST["tab"] == "tabs-3") { ?>
								<div id="tabs-3">
									<fieldset>
										<legend>Korrektur-Abz&uuml;ge</legend>
									<?php
										#$arrTest = getLayoutFiles(array($customerDatas["customersKundennummer"]));
										if(!empty($arrPdfDatas)) {
											foreach($arrPdfDatas as $thisKey1 => $thisValue1) {

												#echo '<h2>Korrektur-Abz&uuml;ge</h2>';
												#echo '<div class="displayDocumentsArea">';
												echo '<table border="0" cellpadding="0" cellspacing="0" width="100%" class="border">';
												echo '<tr>';
												#echo '<th style="width:90px;">Nummer</th>';
												echo '<th style="width:80px;">Datum</th>';
												echo '<th>Dateiname</th>';
												echo '<th style="width:60px;">Info</th>';
												#echo '<th style="width:50px;">Umwandeln in</th>';
												#echo '<th style="width:50px;">Kopieren in</th>';
												echo '</tr>';

												$count = 0;
												foreach($thisValue1 as $thisKey2 => $thisValue2) {
													if($count%2 == 0) {
														$thisClass = "row0";
													}
													else {
														$thisClass = "row1";
													}
													$thisFilename = $thisValue2["createdDocumentsFilename"];
													/*
													$arrTemp = explode("_", $thisValue2["createdDocumentsFilename"]);
													$arrThisFilename = array();
													for($i = 0 ; $i < (count($arrTemp) - 1); $i++) {
														$arrThisFilename[$i] = $arrTemp[$i];
													}
													$thisFilename = implode("_", $arrThisFilename);
													*/
													echo '<tr class="'.$thisClass.'">';
													#echo '<td>'.$thisValue2["createdDocumentsNumber"].'</td>';
													echo '<td style="white-space:nowrap;">'.preg_replace("/ /", ", ", formatDate($thisValue2["createdDocumentsTimeCreated"], 'display')).'</td>';
													echo '<td>';
														# substr(basename($thisFilename), 0, 100);
													echo '<span style="font-size:10px;">' . (basename($thisFilename)) . '</span>';
													echo '</td>';
													echo '<td style="text-align:center;white-space:nowrap !important;">';
													echo '<span class="toolItem">';
														echo '<a href="?editID=' . $_REQUEST["editID"] . '&amp;documentType=' . $thisValue2["createdDocumentsType"] . '&amp;downloadFile='.urlencode(basename($thisValue2["createdDocumentsFilename"])).'" title="Datei anzeigen">';
														if(pathinfo($thisValue2["createdDocumentsFilename"], PATHINFO_EXTENSION) == "doc") {
															$thisImageName = "iconDOC.gif";
														}
														else if(pathinfo($thisValue2["createdDocumentsFilename"], PATHINFO_EXTENSION) == "zip") {
															$thisImageName = "iconZIP.gif";
														}
														else if(pathinfo($thisValue2["createdDocumentsFilename"], PATHINFO_EXTENSION) == "pdf") {
															$thisImageName = "iconPDF.gif";
														}
														else if(pathinfo($thisValue2["createdDocumentsFilename"], PATHINFO_EXTENSION) == "tiff") {
															$thisImageName = "iconTIFF.gif";
														}
														else if(pathinfo($thisValue2["createdDocumentsFilename"], PATHINFO_EXTENSION) == "eps") {
															$thisImageName = "iconEPS.gif";
														}
														else if(pathinfo($thisValue2["createdDocumentsFilename"], PATHINFO_EXTENSION) == "gif") {
															$thisImageName = "iconGIF.gif";
														}
														else {
															$thisImageName = "iconALL.gif";
														}

														echo '<img src="layout/icons/'.$thisImageName.'" alt="" />';
														echo '</a>';
														echo '</span>';

														echo '<span class="toolItem">';
														echo '<a href="?editID=' . $_REQUEST["editID"] . '&amp;documentType=' . $thisValue2["createdDocumentsType"] . '&amp;deleteFile=' . urlencode(basename($thisValue2["createdDocumentsFilename"])) . '" onclick="return showWarning(\'Wollen Sie diese Datei wirklich entfernen?\')"><img src="layout/icons/iconDelete.png" width="16" height="16" alt="" title="Diese Datei l&ouml;schen" /></a>';
														echo '</span>';

														echo '<span class="toolItem">';
														echo '<img src="layout/icons/mailAttachement.png" class="buttonSendDocument" data_mail="" rel="' . urlencode(basename($thisValue2["createdDocumentsFilename"])) . '#KA" width="16" height="16" title="' . urlencode(basename($thisValue2["createdDocumentsFilename"])) . '&quot; direkt per Mail versenden" alt="Dokument versenden" />';
														echo '</span>';
													echo '</td>';

													/*
														echo '<td>';
														#echo '<form name="formSubmitConvertDocument" method="post" action="' . PAGE_CREATE_DOCUMENT . '">';
														echo '<table width="150" cellpadding="0" cellspacing="0" class="noBorder">';
														echo '<tr>';
														echo '<td>';
														#echo '<input type="hidden" name="editID" value=" ' . $arrDocumentDatas[$thisKey]["orderDocumentsCustomerNumber"] . ' ">';
														#echo '<input type="hidden" name="originDocumentType" value="' . $_REQUEST["documentType"] . '">';
														#echo '<input type="hidden" name="originDocumentID" value=" ' . $thisKey . ' ">';
														#echo '<select name="convertDocType" class="inputSelect_120">';
														#echo createSelectDocTypes($_REQUEST["documentType"], 'convert');
														#echo '</select>';
														echo '</td>';
														echo '<td>';
														// echo '<a href="' . PAGE_CREATE_THIS . '?editID=' . $arrDocumentDatas[$thisKey]["orderDocumentsCustomerNumber"] . '&amp;originDocumentID=' . $thisKey . '&amp;originDocumentType=' . $_REQUEST["documentType"] . '"><img src="layout/icons/convertDocument.png" width="16" height="16" title="Dokument umwandeln in" alt="Dokument umwandeln" /></a>';
														#echo '<input type="image" src="layout/icons/convertDocument.png" title="' . $arrDocumentTypeDatas[$_REQUEST["documentType"]]["createdDocumentsTypesName"] . ' umwandeln">';
														echo '</td>';
														echo '</tr>';
														echo '</table>';
														#echo '</form>';
														echo '</td>';
														echo '<td>';
														#echo '<form name="formSubmitConvertDocument" method="post" action="' . PAGE_CREATE_DOCUMENT . '">';
														echo '<table width="150" cellpadding="0" cellspacing="0" class="noBorder">';
														echo '<tr>';
														echo '<td>';
														#echo '<input type="hidden" name="editID" value=" ' . $arrDocumentDatas[$thisKey]["orderDocumentsCustomerNumber"] . ' ">';
														#echo '<input type="hidden" name="originDocumentType" value=" ' . $_REQUEST["documentType"] . ' ">';
														#echo '<input type="hidden" name="originDocumentID" value=" ' . $thisKey . ' ">';
														#echo '<select name="copyDocType" class="inputSelect_120">';
														#echo createSelectDocTypes($_REQUEST["documentType"], 'copy');
														#echo '</select>';
														echo '</td>';
														echo '<td>';
														// echo '<a href="' . PAGE_CREATE_THIS . '?editID=' . $arrDocumentDatas[$thisKey]["orderDocumentsCustomerNumber"] . '&amp;originDocumentID=' . $thisKey . '&amp;originDocumentType=' . $_REQUEST["documentType"] . '"><img src="layout/icons/copyDocument.png" width="16" height="16" title="Dokument kopieren" alt="Dokument kopieren" /></a>';
														#echo '<input type="image" src="layout/icons/copyDocument.png" title="' . $arrDocumentTypeDatas[$_REQUEST["documentType"]]["createdDocumentsTypesName"] . ' umwandeln">';
														echo '</tr>';
														echo '</table>';
														#echo '</form>';
														echo '</td>';
													*/
													echo '</tr>';
													$count++;
												}

												echo '</table>';
												#echo '</div>';
												echo '<hr />';
											}
										}
										else {
											$infoMessage .= 'Es sind keine Korrektur-Abz&uuml;ge vorhanden.' . '<br />';
											displayMessages();
										}
									?>
								</fieldset>

								<?php if($arrGetUserRights["editCustomers"]) { ?>
									<?php if(1){ ?>
									<form name="formUploadCustomerFiles" method="post" action="<?php echo $_SERVER["PHP_SELF"]; ?>" enctype="multipart/form-data" >
										<fieldset>
											<legend>Korrektur-Abz&uuml;ge hochladen</legend>
											<table border="0" width="100%" cellspacing="0" cellpadding="0">
												<tr>
													<td style="width:200px;"><b>Lokale Datei(en) ausw&auml;hlen:</b></td>
													<td>
														<?php
															for($i = 0 ; $i < (MAX_FILE_UPLOADS + 1) ; $i++) {
																echo '<p style="border-bottom: 1px dotted #666; padding: 2px 0 2px 0; ">';
																echo '<input type="file" size="50" name="editCustomersPrintFile['.$i.']" class="inputFile_510" />';
																echo '</p>';
															}
														?>
													</td>
												</tr>
											</table>
											<input type="hidden" name="editCustomersKundennummer2" value="<?php echo $customerDatas["customersKundennummer"]; ?>" />
											<input type="hidden" name="editID" value="<?php echo $_REQUEST["editID"]; ?>" />
											<input type="hidden" name="tab" value="<?php echo $_REQUEST["tab"]; ?>" />
										</fieldset>

										<div class="actionButtonsArea">
											<input type="submit" class="inputButton1 inputButtonGreen" name="storeFiles" value="Hochladen" onclick="return showWarning(' Wollen Sie diese Datei wirklich hochladen??? ');" />
											&nbsp;
											<input type="submit" class="inputButton1 inputButtonOrange" name="resetFiles" value="Abbrechen" onclick="return showWarning(' Wollen Sie diesen Vorgang wirklich abbrechen??? ');" />
										</div>
									</form>
									<?php } else { ?>
									<p class="warningArea">Korrektur-Abz&uuml;ge hochladen nur unter dem Mandanten BCTR m&ouml;glich!</p>
									<?php } ?>
								<?php } ?>
							</div>
							<?php } ?>

							<?php if($_REQUEST["tab"] == "tabs-12") { ?>
								<fieldset>
									<legend>Bisherige Preise</legend>
									<?php
										$sql = "
												SELECT

													`tempTable`.`orderDocumentsCustomerNumber`,
													`tempTable`.`orderDocumentsID`,
													`tempTable`.`orderDocumentsNumber`,
													`tempTable`.`orderDocumentsType`,

													GROUP_CONCAT(DISTINCT DATE_FORMAT(`tempTable`.`orderDocumentsDocumentDate`, '%Y-%m') ORDER BY `tempTable`.`orderDocumentsDocumentDate`) AS `orderDocumentsDocumentDate`,
													`tempTable`.`orderDocumentsOrderDate`,

													`tempTable`.`orderDocumentDetailProductNumber`,
													`tempTable`.`orderDocumentDetailProductName`,
													`tempTable`.`orderDocumentDetailProductKategorieID`,

													GROUP_CONCAT(DISTINCT `tempTable`.`orderDocumentDetailProductQuantity` ORDER BY `tempTable`.`orderDocumentDetailProductQuantity`) AS `orderDocumentDetailProductQuantity`,

													`tempTable`.`orderDocumentDetailProductColorsCount`,
													`tempTable`.`orderDocumentDetailProductPrintType`,
													`tempTable`.`orderDocumentDetailsWithBorder`,
													`tempTable`.`orderDocumentDetailsWithClearPaint`,

													`tempTable`.`orderDocumentDetailProductSinglePrice`

												FROM (

													SELECT
															*

														FROM `" . TABLE_ORDER_CONFIRMATIONS . "`

														LEFT JOIN `" . TABLE_ORDER_CONFIRMATIONS_DETAILS . "`
														ON(`" . TABLE_ORDER_CONFIRMATIONS . "`.`orderDocumentsID` = `" . TABLE_ORDER_CONFIRMATIONS_DETAILS . "`.`orderDocumentDetailDocumentID`)

														WHERE 1
															AND (
																/*
																`" . TABLE_ORDER_CONFIRMATIONS . "`.`orderDocumentsCustomerNumber` = '" . $customerDatas["customersKundennummer"] . "'
																OR
																*/
																`" . TABLE_ORDER_CONFIRMATIONS . "`.`orderDocumentsAddressCompanyCustomerNumber` = '" . $customerDatas["customersKundennummer"] . "'

															)
															AND `" . TABLE_ORDER_CONFIRMATIONS_DETAILS . "`.`orderDocumentDetailPosType` = 'product'
															AND `" . TABLE_ORDER_CONFIRMATIONS_DETAILS . "`.`orderDocumentDetailProductSinglePrice` > 0


													UNION


													SELECT
															*

														FROM `" . TABLE_ORDER_INVOICES . "`

														LEFT JOIN `" . TABLE_ORDER_INVOICES_DETAILS . "`
														ON(`" . TABLE_ORDER_INVOICES . "`.`orderDocumentsID` = `" . TABLE_ORDER_INVOICES_DETAILS . "`.`orderDocumentDetailDocumentID`)

														WHERE 1
															AND (
																/*
																`" . TABLE_ORDER_INVOICES . "`.`orderDocumentsCustomerNumber` = '" . $customerDatas["customersKundennummer"] . "'
																OR
																*/
																`" . TABLE_ORDER_INVOICES . "`.`orderDocumentsAddressCompanyCustomerNumber` = '" . $customerDatas["customersKundennummer"] . "'

															)
															AND `" . TABLE_ORDER_INVOICES_DETAILS . "`.`orderDocumentDetailPosType` = 'product'
															AND `" . TABLE_ORDER_INVOICES_DETAILS . "`.`orderDocumentDetailProductSinglePrice` > 0
															AND `" . TABLE_ORDER_INVOICES . "`.`orderDocumentsIsCollectiveInvoice` != '1'

												) AS `tempTable`

												WHERE 1
													AND `tempTable`.`orderDocumentDetailProductKategorieID` > 0

												GROUP BY CONCAT(
													`orderDocumentDetailProductNumber`,
													`orderDocumentDetailProductSinglePrice`
												)

												ORDER BY
													`orderDocumentDetailProductKategorieID`,
													`orderDocumentDetailProductNumber`

											";
										$rs = $dbConnection->db_query($sql);

										$countItems = $dbConnection->db_getMysqlNumRows($rs);

										if($countItems > 0){
									?>
									<table border="0" cellpadding="0" cellspacing="0" width="100%" class="displayOrders">
										<colgroup>
											<col />
											<?php if($_COOKIE["isAdmin"] == '1') { ?>
											<col />
											<?php } ?>
											<col />
											<col />
											<col />
											<!--<col />-->
											<col />
										</colgroup>
										<thead>
											<tr>
												<th style="width:45px;text-align:right;">#</th>
												<?php if($_COOKIE["isAdmin"] == '1') { ?>
												<th>CID</th>
												<?php } ?>
												<th>Art.-Nr.</th>
												<th>Art.-Name</th>
												<th>Mengen</th>
												<!--<th>Datum</th>-->
												<th>Art.-Preis</th>
											</tr>
										</thead>

										<tbody>
											<?php
												$countRow = 0;
												$thisMarker = '';
												while($ds = mysqli_fetch_assoc($rs)){
													if($countRow%2 == 0){ $rowClass = 'row0'; }
													else { $rowClass = 'row1'; }

													$thisStyle = '';
													if($thisMarker != $ds["orderDocumentDetailProductKategorieID"]){
														$thisStyle = 'border-top:2px solid #000;';
														$thisMarker = $ds["orderDocumentDetailProductKategorieID"];
													}

													echo '<tr class="' . $rowClass . '" style="' . $thisStyle . '">';

													echo '<td style="text-align:right;">' .  ($countRow+1) . '.</td>';

													if($_COOKIE["isAdmin"] == '1') {
														echo '<td style="font-weight:bold;">';
														echo $ds["orderDocumentDetailProductKategorieID"];
														echo '</td>';
													}

													echo '<td style="font-weight:bold;">';
													echo $ds["orderDocumentDetailProductNumber"];
													echo '</td>';

													echo '<td>';
													echo $ds["orderDocumentDetailProductName"];
													echo '</td>';

													echo '<td>';
													$thisArrOrderDocumentDetailProductQuantity = explode(",", $ds["orderDocumentDetailProductQuantity"]);
													if(count($thisArrOrderDocumentDetailProductQuantity) > 1){
														$thisOrderDocumentDetailProductQuantity = $thisArrOrderDocumentDetailProductQuantity[0] . ' - ' . $thisArrOrderDocumentDetailProductQuantity[(count($thisArrOrderDocumentDetailProductQuantity) - 1)];
													}
													else{
														$thisOrderDocumentDetailProductQuantity = $ds["orderDocumentDetailProductQuantity"];
													}

													// echo preg_replace("/,/", ", ", $ds["orderDocumentDetailProductQuantity"]);
													echo $thisOrderDocumentDetailProductQuantity;
													echo '</td>';

													#echo '<td>';
													#echo $ds["orderDocumentsDocumentDate"];
													#echo '</td>';

													echo '<td style="text-align:right;font-weight:bold;background-color:#FEFFAF;">';
													echo number_format($ds["orderDocumentDetailProductSinglePrice"], 2, ',', '.') . ' &euro;';
													echo '</td>';

													echo '</tr>';
													$countRow++;
												}
											?>

										</tbody>
									</table>
									<?php
										}
										else{
											echo '<p class="infoArea">Es wurden keine Daten gefunden. Eventuell hat dieser Kunde &uuml;ber einen Vertreter bestellt.</p>';
										}
									?>
								</fieldset>
							<?php } ?>

							<?php if($_REQUEST["tab"] == "tabs-13") { ?>

								<?php
									$sql_getCustomersProductsPrices = "
										SELECT
											`customersPriceListsProductsDataID`,
											`customersPriceListsProductsDataCustomerNumber`,
											`customersPriceListsProductsDataCustomerID`,
											`customersPriceListsProductsDataProductsNumber`,
											`customersPriceListsProductsDataProductsQuantity`,
											`customersPriceListsProductsDataProductsPrice`,
											`sachbearbeiter`

										FROM `" . TABLE_CUSTOMERS_PRICE_LISTS_PRODUCTS_DATA . "`

										WHERE 1
											AND `customersPriceListsProductsDataCustomerNumber` = '" . $customerDatas["customersKundennummer"] . "'

									";
							 
								?>
								<fieldset>
									<legend>Artikel-Preise</legend>

									<?php



								// BOF GET ORDER PRODUCTS / CATEGORIES kathy
									if($_COOKIE["isAdmin"] == '1'){
										$arrLoadedProducts = getOrderProducts('all', MANDATOR, $thisCusomerGroupID);
								}
								else {
										$arrOrderCategoriesTypeDatas = getOrderCategories();
								}
								// EOF GET ORDER PRODUCTS / CATEGORIES


										$rs_getCustomersProductsPrices = $dbConnection->db_query($sql_getCustomersProductsPrices);

										$countItems = $dbConnection->db_getMysqlNumRows($rs_getCustomersProductsPrices);

										if($countItems > 0){
											?>
											<table border="0" cellpadding="0" cellspacing="0" width="100%" class="displayOrders">
												<tr>
													<th>#</th>
													<th>Artikel-Nr</th>
													<th>Artikel-Name</th>
													<th>Menge</th>
													<th>St&uuml;ckpreis € </th>
													<th>Sachbearbeiter</th>
													<th>Änderen</th> 
												</tr>

												<?php
													$countRow = 0;
													while($ds_getCustomersProductsPrices = mysqli_fetch_assoc($rs_getCustomersProductsPrices)){

														if($countRow%2 == 0){ $rowClass = 'row0'; }
														else { $rowClass = 'row1'; }

														$thisStyle = '';
														#if($thisMarker != $ds["orderDocumentDetailProductKategorieID"]){
															#$thisStyle = 'border-top:2px solid #000;';
															#$thisMarker = $ds["orderDocumentDetailProductKategorieID"];
														#}





														echo '<tr class="' . $rowClass . '" style="' . $thisStyle . '">';
															echo '<td style="text-align:right;">' .  ($countRow+1) . '.</td>';
															echo '<td>';
															echo $ds_getCustomersProductsPrices["customersPriceListsProductsDataProductsNumber"];
															echo '</td>';

															echo '<td>';
															foreach($arrLoadedProducts as $thisCategoryKey => $thisCategoryData){
																foreach($thisCategoryData["products"] as $thisProductKey => $thisProductValue){
																	 
																		if($thisProductValue['productsProductNumber'] == $ds_getCustomersProductsPrices["customersPriceListsProductsDataProductsNumber"])
																		{
																			echo htmlentities($thisProductValue["productsName"]) . ' / ' . htmlentities($thisCategoryData["categoriesName"]) . ' / '. $thisProductValue["productsID"];
																		}
																}
															}
															echo $ds_getCustomersProductsPrices["customersPriceListsProductsDataProductsName"];
															echo '</td>';

															echo '<td>';
															echo $ds_getCustomersProductsPrices["customersPriceListsProductsDataProductsQuantity"];
															echo '</td>';

															echo '<td>';
															echo $ds_getCustomersProductsPrices["customersPriceListsProductsDataProductsPrice"].' €';
															echo '</td>';

															echo '<td>';
															echo $ds_getCustomersProductsPrices["sachbearbeiter"];
															echo '</td>';

															echo '<td>';
								 
															echo '<span class="toolItem">
																<a href="editCustomer2.php?editID=' . $_GET["editID"] . '&customersPriceListsProductsDataID=' . $ds_getCustomersProductsPrices["customersPriceListsProductsDataID"] . '&tab=tabs-13#tabs-13"><img src="layout/icons/iconEdit.gif" width="16" height="16" title="Preise  bearbeiten" alt="Bearbeiten" /></a></span>';
															echo '<span class="toolItem">
																<a href="editCustomer2.php?editID='.$_GET["editID"].'&delcustomersPriceListsProductsDataID=' . $ds_getCustomersProductsPrices["customersPriceListsProductsDataID"] . '&tab=tabs-13#tabs-13"><img src="layout/icons/iconDelete.png" width="16" height="16" title="Preise löschen" alt="Bearbeiten" /></a></span>';
															
															echo '</td>'; 

														echo '</tr>';

														$countRow++;
													}
												?>
											</table>
											<?php
										}
										else{
											echo '<p class="infoArea">Es sind noch keine Preise hinterlegt!</p>';
										}
									?>

			
							</fieldset>
			<form method="POST" action="">
			<fieldset>
				<legend>Neu Sonder-Preise anlegen</legend>
					<table border="0" width="100%" cellspacing="0" cellpadding="0">
						<tr>
							<td width="200"><b>Produkt:</b></td>
							<td>
						
								<?php 
									if($_GET['editID'] != '')
									{
										$editID = $_GET['editID'];
										$query_stock = "SELECT * FROM `common_customerspricelistsproductsdata` where customersPriceListsProductsDataID = '".$_GET['customersPriceListsProductsDataID']."' ";
										$rs_getSto = $dbConnection->db_query($query_stock);
										$ds_getSto =  mysqli_fetch_row($rs_getSto); 
										$quant = $ds_getSto[4];
										$stckprice = $ds_getSto[5];
										$product = $ds_getSto[3];
										 

									}

									
									
								?>
			
							<select name="article" id="" class="inputSelect_510" onchange="insertProductDatas(this.form.name, this.id, '[editOrdersArtikelNummer,editOrdersArtikelBezeichnung,editOrdersArtikelID]');">
									<option value="0">  --- Bitte w&auml;hlen --- </option>
									<?php							  
										if(!empty($arrLoadedProducts)){										
											foreach($arrLoadedProducts as $thisCategoryKey => $thisCategoryData){
											
										//	print_r($thisCategoryData);exit();								
												echo '<option class="level_1" value="' . $thisCategoryKey . '">' . $thisCategoryKey . '|' . htmlentities($thisCategoryData["categoriesName"]) . '</option>';
												
												if(!empty($thisCategoryData["products"])){
													foreach($thisCategoryData["products"] as $thisProductKey => $thisProductValue){
														$selected = '';
														if(
															$thisProductKey == $orderDatas["ordersArtikelID"]
															|| $thisProductKey == $orderDatas["ordersArtikelKategorieID"]
														){
															$selected = ' selected="selected" ';
														}elseif($_GET['editID'] != ''){
															if(htmlentities($thisProductValue["productsProductNumberUse"]) == $product)
															{
																$selected = ' selected="selected" ';
															}
														}
														echo '<option class="level_2" value="'.htmlentities($thisProductValue["productsProductNumberUse"]).'"' . $selected . ' >' . htmlentities($thisProductValue["productsName"]) . ' / ' . htmlentities($thisCategoryData["categoriesName"]) . ' / ' . $thisProductValue["productsProductNumberUse"] . ' / ' . $thisProductValue["productsID"] . '</option>';
													}
												}									
											}
										}
									?>
								</select>

							</td>
						</tr>
					

					
					
						<tr>
							<td width="200"><b>Menge:</b></td>
							<td>
								<input type="text" name="quant" class="inputField_510" value="<?php echo $quant; ?>" />
								<input type="hidden" name="editID" class="inputField_510" value="<?php echo $_GET['editID'];  ?>" />
							</td>
						</tr>
						<tr>
							<td width="200"><b>Stuck Price:</b></td>
							<td>
								<input type="text" name="stckprice" class="inputField_510" value="<?php echo $stckprice; ?>" />
							</td>
						</tr>
					
				
					</table>

				<div class="actionButtonsArea">
				<input type="hidden" name="jsonReturnParams" value="<?php echo htmlentities($_REQUEST["jsonReturnParams"]); ?>" />

				<?php
					// if($orderDatas["ordersAuslieferungsDatum"] > 0) {
					if(1){

					}
					#else {
				?>
				<?php if($_COOKIE["isAdmin"] == '1' || $orderDatas["ordersStatus"] != '4' || $orderDatas["ordersAuslieferungsDatum"] == date('Y-m-d')) { ?>
				<input type="submit" class="inputButton1 inputButtonGreen" name="addArticle" value="Speichern" />
				&nbsp;
				<?php } ?>
				<?php if($orderDatas["ordersStatus"] != '4') { ?>
				<!--<input type="submit" class="inputButton3 inputButtonGreen" name="addAticle" value="Speichern und weiteren Vorgang hinzuf&uuml;gen" onclick="return checkFormDutyFields(this.form.name, <?php echo $jsonFormDutyFields; ?>);" />
				&nbsp;
				<input type="submit" class="inputButton3 inputButtonGreen" name="addArticleAndShow" value="Speichern und zu Kundendaten gehen" onclick="return checkFormDutyFields(this.form.name, <?php echo $jsonFormDutyFields; ?>);" />
				<?php } ?>-->
				<?php
					#}
					if($checkProductionIsUsed < 1 && $_REQUEST["editID"] != "NEW") {
						if(strtolower(MANDATOR) == strtolower($orderDatas["ordersMandant"])) {
				?>
				&nbsp;
				<input type="submit" class="inputButton1 inputButtonRed" name="deleteDatas" value="Entfernen" onclick="return showWarning(' Wollen Sie diesen Eintrag endgültig entfernen??? ');" />
				<?php
						}
						else {
							// PRODUKTION KANN NUR ENTFERNT WERDEN, WENN PRODUKTION NICHT IN DOKUMENT AB etc. VERWENDET WURDE
							// UND WENN PRODUKTIONS-MANDANT MIT DEM EINGELOGGTEN MANDANTEN ÜBEREINSTIMMT!!! kathy :artikel price vom linie 5362 bis 5481
						}
					}
				?>
				&nbsp;
				<input type="submit" class="inputButton1 inputButtonOrange" name="resetDatas" value="Abbrechen" onclick="return showWarning(' Wollen Sie diesen Vorgang wirklich abbrechen??? ');" />
			</div>
			</fieldset>
			</form>
 

								
							<?php } ?>

							<?php if($_REQUEST["tab"] == "tabs-14") { ?>
								<div id="tabs-14">
									<?php displayMessages(); ?>
									<form name="formEditPhoneMarketingDatas" id="formEditPhoneMarketingDatas" method="post" action="<?php echo $_SERVER["PHP_SELF"]; ?>#tabs-1" enctype="multipart/form-data" >
										<input type="hidden" name="editID" value="<?php echo $customerDatas["customersID"]; ?>" />
										<input type="hidden" name="editCustomersID" value="<?php echo $customerDatas["customersID"]; ?>" />
										<input type="hidden" name="editCustomersKundennummer" value="<?php echo $customerDatas["customersKundennummer"]; ?>" />
										<input type="hidden" name="tab" value="<?php echo $_REQUEST["tab"]; ?>" />

										<?php
											// BOF GET SELECTED PHONE MARKETING DATE
												if($_REQUEST["editPhoneMarketingItem"] > 0){
													dd('_REQUEST');
													$sql_getSelectedPhoneMarketingData = "
															SELECT
																`customersPhoneMarketingID`,
																`customersPhoneMarketingCustomerNumber`,
																`customersPhoneMarketingCustomerID`,
																`customersPhoneMarketingContactDate`,
																`customersPhoneMarketingContactTime`,
																`customersPhoneMarketingCampaign`,
																`customersPhoneMarketingConversationPerson`,
																`customersPhoneMarketingConversationContent`,
																`customersPhoneMarketingRemindDate`,
																`customersPhoneMarketingCallbackDate`,
																`customersPhoneMarketingVisitRequest`,
																`customersPhoneMarketingNotice`,
																`customersPhoneMarketingUser`
															FROM `" . TABLE_CUSTOMERS_PHONE_MARKETING . "`

															WHERE 1
																AND `customersPhoneMarketingCustomerNumber` = '" . $customerDatas["customersKundennummer"] . "'
																AND `customersPhoneMarketingID` = '" . $_REQUEST["editPhoneMarketingItem"] . "'

															LIMIT 1
														";
													$rs_getSelectedPhoneMarketingData = $dbConnection->db_query($sql_getSelectedPhoneMarketingData);

													$arrSelectedPhoneMarketingData = array();

													list(
														$arrSelectedPhoneMarketingData["customersPhoneMarketingID"],
														$arrSelectedPhoneMarketingData["customersPhoneMarketingCustomerNumber"],
														$arrSelectedPhoneMarketingData["customersPhoneMarketingCustomerID"],
														$arrSelectedPhoneMarketingData["customersPhoneMarketingContactDate"],
														$arrSelectedPhoneMarketingData["customersPhoneMarketingContactTime"],
														$arrSelectedPhoneMarketingData["customersPhoneMarketingCampaign"],
														$arrSelectedPhoneMarketingData["customersPhoneMarketingConversationPerson"],
														$arrSelectedPhoneMarketingData["customersPhoneMarketingConversationContent"],
														$arrSelectedPhoneMarketingData["customersPhoneMarketingRemindDate"],
														$arrSelectedPhoneMarketingData["customersPhoneMarketingCallbackDate"],
														$arrSelectedPhoneMarketingData["customersPhoneMarketingVisitRequest"],
														$arrSelectedPhoneMarketingData["customersPhoneMarketingNotice"],
														$arrSelectedPhoneMarketingData["customersPhoneMarketingUser"]

													) = mysqli_fetch_array($rs_getSelectedPhoneMarketingData);

													dd('arrSelectedPhoneMarketingData');
												}
											// EOF GET SELECTED PHONE MARKETING DATE
										?>

										<fieldset>
											<legend>Gespr&auml;chsprotokolle / Telefonmarketing</legend>

											<?php
												$thisPhoneMarketingID = '%';
												$thisPhoneMarketingContactDate = date("d.m.Y");
												$thisPhoneMarketingContactTime = date("H:i");

												if(!empty($arrSelectedPhoneMarketingData)){
													$thisPhoneMarketingID = $arrSelectedPhoneMarketingData["customersPhoneMarketingID"];
													$thisPhoneMarketingContactDate = formatDate($arrSelectedPhoneMarketingData["customersPhoneMarketingContactDate"], "display");
													$thisPhoneMarketingContactTime = substr($arrSelectedPhoneMarketingData["customersPhoneMarketingContactTime"], 0, 5);
												}
											?>
											<input type="hidden" name="phoneMarketingID" value="<?php echo $thisPhoneMarketingID; ?>" />

											<table border="0" width="100%" cellspacing="0" cellpadding="0">
												<tr>
													<td style="width:200px;"><b>Datum / Uhrzeit:</b></td>
													<td>
														<span><input type="text" name="phoneMarketingContactDate" id="phoneMarketingContactDate" class="inputField_70" value="<?php echo $thisPhoneMarketingContactDate; ?>" readonly="readonly" /></span>
														 - <span><input type="text" name="phoneMarketingContactTime" id="phoneMarketingContactTime" class="inputField_70" value="<?php echo $thisPhoneMarketingContactTime; ?>" readonly="readonly" /></span>
														<br />
														<span class="infotext">(Datum - Uhrzeit)</span>
													</td>
												</tr>
												<tr>
													<td style="width:200px;"><b>Kampagne:</b></td>
													<td><textarea name="phoneMarketingCampaign" cols="6" rows="2" class="inputTextarea_510x60"><?php echo htmlentities($arrSelectedPhoneMarketingData["customersPhoneMarketingCampaign"]); ?></textarea></td>
												</tr>
												<tr>
													<td style="width:200px;"><b>Gespr&auml;chspartner:</b></td>
													<td><input type="text" name="phoneMarketingConversationPerson" class="inputField_510" value="<?php echo htmlentities($arrSelectedPhoneMarketingData["customersPhoneMarketingConversationPerson"]); ?>" /></td>
												</tr>
												<tr>
													<td style="width:200px;"><b>Rufnummer:</b></td>
													<td>
														<table cellpadding="0" cellspacing="0" style="width:510px;">
														<colgroup>
															<col style="width:50px;" />
															<col style="width:200px;" />
															<col />
															<col style="width:50px;" />
															<col style="width:200px;" />
														</colgroup>
														<?php
															if($customerDatas["customersTelefon1"] != '' || $customerDatas["customersTelefon2"] != ''){
																echo '<tr>';
																if($customerDatas["customersTelefon1"] != ''){
																	echo '<td>Tel. 1:</td><td style="letter-spacing:1px;"><b>' . createPhoneCallLink($customerDatas["customersTelefon1"]) . ' ' . $customerDatas["customersTelefon1"] . '</b></td>';
																} else { echo '<td colspan="2"></td>'; }
																echo '<td class="spacer"></td>';
																if($customerDatas["customersTelefon2"] != ''){
																	echo '<td>Tel. 2:</td><td style="letter-spacing:1px;"><b>' . createPhoneCallLink($customerDatas["customersTelefon2"]) . ' ' . $customerDatas["customersTelefon2"] . '</b></td>';
																} else { echo '<td colspan="2"></td>'; }
																echo '</tr>';
															}

															if($customerDatas["customersMobil1"] != '' || $customerDatas["customersMobil2"] != ''){
																echo '<tr>';
																if($customerDatas["customersMobil1"] != ''){
																	echo '<td>Mobil 1:</td><td style="letter-spacing:1px;"><b>' . createPhoneCallLink($customerDatas["customersMobil1"]) . ' ' . $customerDatas["customersMobil1"] . '</b></td>';
																} else { echo '<td colspan="2"></td>'; }
																echo '<td class="spacer"></td>';
																if($customerDatas["customersMobil2"] != ''){
																	echo '<td>Mobil 2:</td><td style="letter-spacing:1px;"><b>' . createPhoneCallLink($customerDatas["customersMobil2"]) . ' ' . $customerDatas["customersMobil2"] . '</b></td>';
																} else { echo '<td colspan="2"></td>'; }
																echo '</tr>';
															}
														?>
														</table>
													</td>
												</tr>
												<tr>
													<td style="width:200px;"><b>Vertreter:</b></td>
													<td>
														<b><?php echo htmlentities($arrAgentDatas2[$customerDatas["customersVertreterID"]]["customersFirmenname"]); ?>
														[Knr: <?php echo htmlentities($arrAgentDatas2[$customerDatas["customersVertreterID"]]["customersKundennummer"]); ?>]</b>
													</td>
												</tr>
												<tr>
													<td style="width:200px;"><b>Gespr&auml;chsinhalt:</b></td>
													<td><textarea name="phoneMarketingConversationContent" cols="6" rows="2" class="inputTextarea_510x60"><?php echo htmlentities($arrSelectedPhoneMarketingData["customersPhoneMarketingConversationContent"]); ?></textarea></td>
												</tr>
												<tr>
													<td style="width:200px;"><b>Wiedervorlage am:</b></td>
													<td><input type="text" name="phoneMarketingRemindDate" id="phoneMarketingRemindDate" class="inputField_70" value="<?php echo htmlentities($arrSelectedPhoneMarketingData["customersPhoneMarketingRemindDate"]); ?>" readonly="readonly" /></td>
												</tr>
												<tr>
													<td style="width:200px;"><b>R&uuml;ckruf erw&uuml;nscht am:</b></td>
													<td><input type="text" name="phoneMarketingCallbackDate" id="phoneMarketingCallbackDate" class="inputField_70" value="<?php echo htmlentities($arrSelectedPhoneMarketingData["customersPhoneMarketingCallbackDate"]); ?>" readonly="readonly" /></td>
												</tr>
												<tr>
													<td style="width:200px;"><b>Vertreterbesuch erw&uuml;nscht:</b></td>
													<td>
														<span><input type="radio" name="phoneMarketingVisitRequest" value="1" <?php if($arrSelectedPhoneMarketingData["customersPhoneMarketingVisitRequest"] == '1'){ echo ' checked="checked" '; } ?> /> erw&uuml;nscht</span>
														<span><input type="radio" name="phoneMarketingVisitRequest" value="0" <?php if($arrSelectedPhoneMarketingData["customersPhoneMarketingVisitRequest"] == '0'){ echo ' checked="checked" '; } ?> /> nicht erw&uuml;nscht</span>
													</td>
												</tr>
												<tr>
													<td style="width:200px;"><b>Bemerkungen:</b></td>
													<td><textarea name="phoneMarketingNotice" cols="6" rows="2" class="inputTextarea_510x60"><?php echo htmlentities($arrSelectedPhoneMarketingData["customersPhoneMarketingNotice"]); ?></textarea></td>
												</tr>
											</table>

											<div class="actionButtonsArea">
												<input type="submit" class="inputButton1 inputButtonGreen" name="storePhoneMarketing" value="Speichern" onclick="return showWarning(' Wollen Sie diese Daten wirklich speichern??? ');" />
												&nbsp;
												<input type="submit" class="inputButton1 inputButtonOrange" name="resetPhoneMarketing" value="Abbrechen" onclick="return showWarning(' Wollen Sie diesen Vorgang wirklich abbrechen??? ');" />
											</div>
										</fieldset>

										<fieldset>
											<legend>Letzte Auftr&auml;ge</legend>

											<?php
												// BOF GET LAST ORDERS
													$sql_getLastOrders = "
														SELECT
															`tempTable`.`ordersID`,
															`tempTable`.`ordersKundennummer`,
															`tempTable`.`ordersCustomerID`,
															`tempTable`.`ordersStatus`,
															`tempTable`.`ordersSourceType`,
															`tempTable`.`ordersBestellDatum`,
															`tempTable`.`ordersStornoDatum`,
															`tempTable`.`ordersFreigabeDatum`,
															`tempTable`.`ordersFreigabeArt`,
															`tempTable`.`ordersBelichtungsDatum`,
															`tempTable`.`ordersProduktionsDatum`,
															`tempTable`.`ordersAuslieferungsDatum`,
															`tempTable`.`ordersLieferwoche`,
															`tempTable`.`ordersLieferDatum`,
															`tempTable`.`ordersKundenName`,
															`tempTable`.`ordersKommission`,
															`tempTable`.`ordersPLZ`,
															`tempTable`.`ordersOrt`,
															`tempTable`.`ordersArtikelID`,
															`tempTable`.`ordersArtikelNummer`,
															`tempTable`.`ordersArtikelBezeichnung`,
															`tempTable`.`ordersArtikelKategorieID`,
															`tempTable`.`ordersArtikelMenge`,
															`tempTable`.`ordersArtikelPrintColorsCount`,
															`tempTable`.`ordersArtikelWithBorder`,
															`tempTable`.`ordersArtikelWithClearPaint`,
															`tempTable`.`ordersAufdruck`,
															`tempTable`.`ordersAdditionalArtikelID`,
															`tempTable`.`ordersAdditionalArtikelNummer`,
															`tempTable`.`ordersAdditionalArtikelBezeichnung`,
															`tempTable`.`ordersAdditionalArtikelKategorieID`,
															`tempTable`.`ordersAdditionalArtikelMenge`,
															`tempTable`.`ordersSinglePreis`,
															`tempTable`.`ordersTotalPreis`,
															`tempTable`.`ordersDruckFarbe`,
															`tempTable`.`ordersAdditionalCosts`,
															`tempTable`.`ordersAdditionalCostsID`,
															`tempTable`.`ordersVertreter`,
															`tempTable`.`ordersVertreterID`,
															`tempTable`.`ordersMailAdress`,
															`tempTable`.`ordersContactOthers`,
															`tempTable`.`ordersMandant`,
															`tempTable`.`ordersOrderType`,
															`tempTable`.`ordersPerExpress`,
															`tempTable`.`ordersBankAccountType`,
															`tempTable`.`ordersPaymentType`,
															`tempTable`.`ordersPaymentStatusType`,
															`tempTable`.`ordersNotizen`,
															`tempTable`.`ordersInfo`,
															`tempTable`.`ordersDruckerName`,
															`tempTable`.`ordersUserID`,
															`tempTable`.`ordersTimeCreated`,
															`tempTable`.`ordersArchive`,
															`tempTable`.`ordersUseNoStock`,
															`tempTable`.`ordersDirectSale`,
															`tempTable`.`ordersReklamationsgrund`,

															`tempTable`.`ordersProductionPrintingPlant`,

															`tempTable`.`ordersPrintPlateNumber`

															FROM (
																SELECT
																	`" . TABLE_ORDERS . "`.`ordersID`,
																	`" . TABLE_ORDERS . "`.`ordersKundennummer`,
																	`" . TABLE_ORDERS . "`.`ordersCustomerID`,
																	`" . TABLE_ORDERS . "`.`ordersStatus`,
																	`" . TABLE_ORDERS . "`.`ordersSourceType`,
																	`" . TABLE_ORDERS . "`.`ordersBestellDatum`,
																	`" . TABLE_ORDERS . "`.`ordersStornoDatum`,
																	`" . TABLE_ORDERS . "`.`ordersFreigabeDatum`,
																	`" . TABLE_ORDERS . "`.`ordersFreigabeArt`,
																	`" . TABLE_ORDERS . "`.`ordersBelichtungsDatum`,
																	`" . TABLE_ORDERS . "`.`ordersProduktionsDatum`,
																	`" . TABLE_ORDERS . "`.`ordersAuslieferungsDatum`,
																	`" . TABLE_ORDERS . "`.`ordersLieferwoche`,
																	`" . TABLE_ORDERS . "`.`ordersLieferDatum`,
																	`" . TABLE_ORDERS . "`.`ordersKundenName`,
																	`" . TABLE_ORDERS . "`.`ordersKommission`,
																	`" . TABLE_ORDERS . "`.`ordersPLZ`,
																	`" . TABLE_ORDERS . "`.`ordersOrt`,
																	`" . TABLE_ORDERS . "`.`ordersArtikelID`,
																	`" . TABLE_ORDERS . "`.`ordersArtikelNummer`,
																	`" . TABLE_ORDERS . "`.`ordersArtikelBezeichnung`,
																	`" . TABLE_ORDERS . "`.`ordersArtikelKategorieID`,
																	`" . TABLE_ORDERS . "`.`ordersArtikelMenge`,
																	`" . TABLE_ORDERS . "`.`ordersArtikelPrintColorsCount`,
																	`" . TABLE_ORDERS . "`.`ordersArtikelWithBorder`,
																	`" . TABLE_ORDERS . "`.`ordersArtikelWithClearPaint`,
																	`" . TABLE_ORDERS . "`.`ordersAufdruck`,
																	`" . TABLE_ORDERS . "`.`ordersAdditionalArtikelID`,
																	`" . TABLE_ORDERS . "`.`ordersAdditionalArtikelNummer`,
																	`" . TABLE_ORDERS . "`.`ordersAdditionalArtikelBezeichnung`,
																	`" . TABLE_ORDERS . "`.`ordersAdditionalArtikelKategorieID`,
																	`" . TABLE_ORDERS . "`.`ordersAdditionalArtikelMenge`,
																	`" . TABLE_ORDERS . "`.`ordersSinglePreis`,
																	`" . TABLE_ORDERS . "`.`ordersTotalPreis`,
																	`" . TABLE_ORDERS . "`.`ordersDruckFarbe`,
																	`" . TABLE_ORDERS . "`.`ordersAdditionalCosts`,
																	`" . TABLE_ORDERS . "`.`ordersAdditionalCostsID`,
																	`" . TABLE_ORDERS . "`.`ordersVertreter`,
																	`" . TABLE_ORDERS . "`.`ordersVertreterID`,
																	`" . TABLE_ORDERS . "`.`ordersMailAdress`,
																	`" . TABLE_ORDERS . "`.`ordersContactOthers`,
																	`" . TABLE_ORDERS . "`.`ordersMandant`,
																	`" . TABLE_ORDERS . "`.`ordersOrderType`,
																	`" . TABLE_ORDERS . "`.`ordersPerExpress`,
																	`" . TABLE_ORDERS . "`.`ordersBankAccountType`,
																	`" . TABLE_ORDERS . "`.`ordersPaymentType`,
																	`" . TABLE_ORDERS . "`.`ordersPaymentStatusType`,
																	`" . TABLE_ORDERS . "`.`ordersNotizen`,
																	`" . TABLE_ORDERS . "`.`ordersInfo`,
																	`" . TABLE_ORDERS . "`.`ordersDruckerName`,
																	`" . TABLE_ORDERS . "`.`ordersUserID`,
																	`" . TABLE_ORDERS . "`.`ordersTimeCreated`,
																	`" . TABLE_ORDERS . "`.`ordersArchive`,
																	`" . TABLE_ORDERS . "`.`ordersUseNoStock`,
																	`" . TABLE_ORDERS . "`.`ordersDirectSale`,
																	`" . TABLE_ORDERS . "`.`ordersReklamationsgrund`,
																	`" . TABLE_ORDERS . "`.`ordersProductionPrintingPlant`,

																	`" . TABLE_ORDERS . "`.`ordersPrintPlateNumber`

																FROM `" . TABLE_ORDERS . "`

																WHERE 1
																	AND `" . TABLE_ORDERS . "`.`ordersKundennummer` = '" . $customerDatas["customersKundennummer"] . "'

																/* ORDER BY `" . TABLE_ORDERS . "`.`ordersBestellDatum` DESC */

															UNION

																SELECT
																	`" . TABLE_ORDERS . "`.`ordersID`,
																	`" . TABLE_ORDERS . "`.`ordersKundennummer`,
																	`" . TABLE_ORDERS . "`.`ordersCustomerID`,
																	`" . TABLE_ORDERS . "`.`ordersStatus`,
																	`" . TABLE_ORDERS . "`.`ordersSourceType`,
																	`" . TABLE_ORDERS . "`.`ordersBestellDatum`,
																	`" . TABLE_ORDERS . "`.`ordersStornoDatum`,
																	`" . TABLE_ORDERS . "`.`ordersFreigabeDatum`,
																	`" . TABLE_ORDERS . "`.`ordersFreigabeArt`,
																	`" . TABLE_ORDERS . "`.`ordersProduktionsDatum`,
																	`" . TABLE_ORDERS . "`.`ordersBelichtungsDatum`,
																	`" . TABLE_ORDERS . "`.`ordersAuslieferungsDatum`,
																	`" . TABLE_ORDERS . "`.`ordersLieferwoche`,
																	`" . TABLE_ORDERS . "`.`ordersLieferDatum`,
																	`" . TABLE_ORDERS . "`.`ordersKundenName`,
																	`" . TABLE_ORDERS . "`.`ordersKommission`,
																	`" . TABLE_ORDERS . "`.`ordersPLZ`,
																	`" . TABLE_ORDERS . "`.`ordersOrt`,
																	`" . TABLE_ORDERS . "`.`ordersArtikelID`,
																	`" . TABLE_ORDERS . "`.`ordersArtikelNummer`,
																	`" . TABLE_ORDERS . "`.`ordersArtikelBezeichnung`,
																	`" . TABLE_ORDERS . "`.`ordersArtikelKategorieID`,
																	`" . TABLE_ORDERS . "`.`ordersArtikelMenge`,
																	`" . TABLE_ORDERS . "`.`ordersArtikelPrintColorsCount`,
																	`" . TABLE_ORDERS . "`.`ordersArtikelWithBorder`,
																	`" . TABLE_ORDERS . "`.`ordersArtikelWithClearPaint`,
																	`" . TABLE_ORDERS . "`.`ordersAufdruck`,
																	`" . TABLE_ORDERS . "`.`ordersAdditionalArtikelID`,
																	`" . TABLE_ORDERS . "`.`ordersAdditionalArtikelNummer`,
																	`" . TABLE_ORDERS . "`.`ordersAdditionalArtikelBezeichnung`,
																	`" . TABLE_ORDERS . "`.`ordersAdditionalArtikelKategorieID`,
																	`" . TABLE_ORDERS . "`.`ordersAdditionalArtikelMenge`,
																	`" . TABLE_ORDERS . "`.`ordersSinglePreis`,
																	`" . TABLE_ORDERS . "`.`ordersTotalPreis`,
																	`" . TABLE_ORDERS . "`.`ordersDruckFarbe`,
																	`" . TABLE_ORDERS . "`.`ordersAdditionalCosts`,
																	`" . TABLE_ORDERS . "`.`ordersAdditionalCostsID`,
																	`" . TABLE_ORDERS . "`.`ordersVertreter`,
																	`" . TABLE_ORDERS . "`.`ordersVertreterID`,
																	`" . TABLE_ORDERS . "`.`ordersMailAdress`,
																	`" . TABLE_ORDERS . "`.`ordersContactOthers`,
																	`" . TABLE_ORDERS . "`.`ordersMandant`,
																	`" . TABLE_ORDERS . "`.`ordersOrderType`,
																	`" . TABLE_ORDERS . "`.`ordersPerExpress`,
																	`" . TABLE_ORDERS . "`.`ordersBankAccountType`,
																	`" . TABLE_ORDERS . "`.`ordersPaymentType`,
																	`" . TABLE_ORDERS . "`.`ordersPaymentStatusType`,
																	`" . TABLE_ORDERS . "`.`ordersNotizen`,
																	`" . TABLE_ORDERS . "`.`ordersInfo`,
																	`" . TABLE_ORDERS . "`.`ordersDruckerName`,
																	`" . TABLE_ORDERS . "`.`ordersUserID`,
																	`" . TABLE_ORDERS . "`.`ordersTimeCreated`,
																	`" . TABLE_ORDERS . "`.`ordersArchive`,
																	`" . TABLE_ORDERS . "`.`ordersUseNoStock`,
																	`" . TABLE_ORDERS . "`.`ordersDirectSale`,
																	`" . TABLE_ORDERS . "`.`ordersReklamationsgrund`,
																	`" . TABLE_ORDERS . "`.`ordersProductionPrintingPlant`,

																	`" . TABLE_ORDERS . "`.`ordersPrintPlateNumber`

																FROM `" . TABLE_ORDERS . "`

																LEFT JOIN `" . TABLE_CUSTOMERS . "` AS `customerSalesman`
																ON(`" . TABLE_ORDERS . "`.`ordersVertreterID` = `customerSalesman`.`customersID` AND `" . TABLE_ORDERS . "`.`ordersVertreterID` > 0)

																WHERE 1
																	AND `customerSalesman`.`customersKundennummer` = '" . $customerDatas["customersKundennummer"] . "'

																/*ORDER BY `ordersBestellDatum` DESC*/
															) AS `tempTable`

															ORDER BY
																`ordersBestellDatum` DESC,
																`ordersArtikelBezeichnung` ASC

															LIMIT 5
														";

													$rs_getLastOrders = $dbConnection->db_query($sql_getLastOrders);

													$countItems = $dbConnection->db_getMysqlNumRows($rs_getLastOrders);

													if($countItems > 0){
														echo '
															<table border="0" cellpadding="0" cellspacing="0" width="100%" class="displayOrders">
																<tr>
																	<th>#</th>
																	<th>Datum</th>
																	<th>Menge</th>
																	<th>Artikel</th>
																	<th>Artikel-Nr</th>
																	<th>Status</th>
																	<th>Bestellart</th>
																</tr>
															';

														$countRow = 0;

														while($ds_getLastOrders = mysqli_fetch_assoc($rs_getLastOrders)){

															if($countRow%2 == 0) { $rowClass = 'row0'; }
															else { $rowClass = 'row1'; }

															if($ds_getLastOrders["ordersStatus"] == "1") {
																// $rowClass = 'row5';
															}
															else if($ds_getLastOrders["ordersStatus"] == "3") {
																$rowClass = 'row9';
															}
															else if($ds_getLastOrders["ordersStatus"] == "4") {
																$rowClass = 'row3';
															}
															else if($ds_getLastOrders["ordersStatus"] == "6") {
																$rowClass = 'row6';
															}
															else if($ds_getLastOrders["ordersStatus"] == "7") {
																$rowClass = 'row2';
															}

															echo '<tr class="' . $rowClass . '">';
																echo '<td style="text-align:right;">';
																echo '<b>' . ($countRow + 1) . '</b>';
																echo '</td>';

																echo '<td>';
																echo formatDate($ds_getLastOrders["ordersBestellDatum"], 'display');
																echo '</td>';

																echo '<td style="text-align:right;">';
																echo $ds_getLastOrders["ordersArtikelMenge"];
																echo '</td>';

																echo '<td>';
																echo $ds_getLastOrders["ordersArtikelBezeichnung"];
																echo '</td>';

																echo '<td>';
																echo $ds_getLastOrders["ordersArtikelNummer"];
																echo '</td>';

																echo '<td>';
																echo $arrOrderStatusTypeDatas[$ds_getLastOrders["ordersStatus"]]["orderStatusTypesName"];
																echo '</td>';

																echo '<td>';
																echo $arrOrderTypeDatas[$ds_getLastOrders["ordersOrderType"]]["orderTypesName"];
																echo '</td>';

															echo '</tr>';

															$countRow++;
														}

														echo '</table>';
													}
													else {
														echo '<p class="infoArea">Keine Daten vorhanden!</p>';
													}
												// BOF GET LAST ORDERS
											?>

										</fieldset>

										<fieldset>
											<legend>Letzte Kontakte</legend>

											<?php

												// BOF GET ALL PHONE MARKETING DATES
													$sql_getPhomeMarketingData = "
															SELECT
																`customersPhoneMarketingID`,
																`customersPhoneMarketingCustomerNumber`,
																`customersPhoneMarketingCustomerID`,
																`customersPhoneMarketingContactDate`,
																`customersPhoneMarketingContactTime`,
																`customersPhoneMarketingCampaign`,
																`customersPhoneMarketingConversationPerson`,
																`customersPhoneMarketingConversationContent`,
																`customersPhoneMarketingRemindDate`,
																`customersPhoneMarketingCallbackDate`,
																`customersPhoneMarketingVisitRequest`,
																`customersPhoneMarketingNotice`,
																`customersPhoneMarketingUser`
															FROM `" . TABLE_CUSTOMERS_PHONE_MARKETING . "`

															WHERE 1
																AND `customersPhoneMarketingCustomerNumber` = '" . $customerDatas["customersKundennummer"] . "'

															ORDER BY
																`customersPhoneMarketingID` DESC

															LIMIT 5
														";

													$rs_getPhomeMarketingData = $dbConnection->db_query($sql_getPhomeMarketingData);

													$countItems = $dbConnection->db_getMysqlNumRows($rs_getPhomeMarketingData);

													if($countItems > 0){
														echo '
															<table border="0" cellpadding="0" cellspacing="0" width="100%" class="displayOrders">
																<tr>
																	<th>#</th>
																	<th>Datum</th>
																	<th>Gespr&auml;chspartner</th>
																	<th>Gespr&auml;chsinhalt</th>
																	<th>Kampagne</th>
																	<th>Bemerkungen</th>
																	<th>Wiedervorlage</th>
																	<th>Besuch?</th>
																	<th>R&uuml;ckruf?</th>
																	<th>Benutzer</th>
																	<th></th>
																</tr>
														';

														$countRow = 0;

														while($ds_getPhomeMarketingData = mysqli_fetch_assoc($rs_getPhomeMarketingData)){
															if($countRow%2 == 0) { $rowClass = 'row0'; }
															else { $rowClass = 'row1'; }

															echo '<tr class="' . $rowClass . '">';
															echo '<td style="text-align:right;">';
															echo '<b>' . ($countRow + 1) . '.</b>';
															echo '</td>';

															echo '<td style="white-space:nowrap;">';
															echo formatDate($ds_getPhomeMarketingData["customersPhoneMarketingContactDate"], 'display');
															echo ',<br />' . substr($ds_getPhomeMarketingData["customersPhoneMarketingContactTime"], 0, -3) . ' Uhr';
															echo '</td>';

															echo '<td style="white-space:nowrap;">';
															echo htmlentities($ds_getPhomeMarketingData["customersPhoneMarketingConversationPerson"]);
															echo '</td>';

															echo '<td>';
															echo htmlentities($ds_getPhomeMarketingData["customersPhoneMarketingConversationContent"]);
															echo '</td>';

															echo '<td>';
															echo htmlentities($ds_getPhomeMarketingData["customersPhoneMarketingCampaign"]);
															echo '</td>';

															echo '<td>';
															echo htmlentities($ds_getPhomeMarketingData["customersPhoneMarketingNotice"]);
															echo '</td>';

															echo '<td>';
															echo formatDate($ds_getPhomeMarketingData["customersPhoneMarketingRemindDate"], "display");
															echo '</td>';

															echo '<td style="text-align:center;">';
															if($ds_getPhomeMarketingData["customersPhoneMarketingVisitRequest"] == '1'){
																$thisImage = "iconOk.png";
																$thisAlt = "Besuch erw&uuml;nscht";
															}
															else {
																$thisImage = "iconNotOk.png";
																$thisAlt = "Besuch nicht erw&uuml;nscht";
															}
															echo '<img src="layout/icons/' . $thisImage . '" alt="' . $thisAlt . '" title="' . $thisAlt . '" />';
															echo '</td>';

															echo '<td>';
															echo formatDate($ds_getPhomeMarketingData["customersPhoneMarketingCallbackDate"], "display");
															echo '</td>';

															echo '<td style="white-space:nowrap;">';
															echo htmlentities($arrUserDatas[$ds_getPhomeMarketingData["customersPhoneMarketingUser"]]["usersFirstName"] . ' ' . $arrUserDatas[$ds_getPhomeMarketingData["customersPhoneMarketingUser"]]["usersLastName"]);
															echo '</td>';

															echo '<td>';

															echo '<span class="toolItem">';
															echo '<a href="?editPhoneMarketingItem=' . $ds_getPhomeMarketingData["customersPhoneMarketingID"] . '&amp;tab=' . $_REQUEST["tab"] . '&amp;editID=' . $customerDatas["customersID"] . '" onclick="return showWarning(\'Soll der Eintrag wirklich bearbeitet werden?\');" >';
															echo '<img src="layout/icons/iconEdit.gif" alt="Bearbeiten" title="Diesen Eintrag bearbeiten?" />';
															echo '</a>';
															echo '</span>';

															echo '<span class="toolItem">';
															echo '<a href="?deletePhoneMarketingItem=' . $ds_getPhomeMarketingData["customersPhoneMarketingID"] . '&amp;tab=' . $_REQUEST["tab"] . '&amp;editID=' . $customerDatas["customersID"] . '" onclick="return showWarning(\'Soll der Eintrag wirklich entfernt werden?\');" >';
															echo '<img src="layout/icons/iconDelete.png" alt="Entfernen" title="Diesen Eintrag entfernen?" />';
															echo '</a>';
															echo '</span>';

															echo '</td>';

															echo '</tr>';
															$countRow++;
														}

														echo '</table>';
													}
													else {
														echo '<p class="infoArea">Keine Daten vorhanden!</p>';
													}
												// BOF GET ALL PHONE MARKETING DATES
											?>
										</fieldset>

									</form>
								</div>
							<?php } ?>

							<?php if($_REQUEST["tab"] == "tabs-11") { ?>
								<fieldset>
									<legend>Container-Lieferungen</legend>
									<?php
										$sql = "
											SELECT

												`containerListsID`,
												`containerListsFilename`,
												`containerListsProductionsDateStart`,
												`containerListsProductionsDateEnd`,
												`containerListsDepartureKW`,
												`containerListsArrivalKW`,
												`containerListsDeliveryKW`,
												`containerListsCustomerNumber`,
												`containerListsCustomerName`,
												`containerListsCustomerKommission`,
												`containerListsSalesmanNumber`,
												`containerListsSalesmanName`,
												`containerListsProduct`,
												`containerListsQuantity`,
												`containerListsTirArkasi`,
												`containerListsCita`,
												`containerListsExtra`
												FROM `" . TABLE_CONTAINER_LISTS . "`
												WHERE 1
													AND `containerListsCustomerNumber` = '" . $customerDatas["customersKundennummer"] . "'

												ORDER BY
													`containerListsProductionsDateStart` DESC,
													`containerListsCustomerNumber`
											";
										$rs = $dbConnection->db_query($sql);
									?>
									<table border="0" cellpadding="0" cellspacing="0" width="100%" class="displayOrders">
										<colgroup>
											<col />
											<col />
											<col />
											<col />
											<col />
											<col />
											<col />
											<col />
											<col />
											<col />
											<col />

											<!--
											<col />
											-->

										</colgroup>
										<thead>
											<tr>
												<th style="width:45px;text-align:right;">#</th>
												<th class="sortColumn">K-Nr</th>
												<th class="sortColumn">Kunde</th>
												<th class="sortColumn">Kommission</th>
												<th class="sortColumn">Menge</th>
												<th class="sortColumn">Produkt</th>
												<th class="sortColumn" colspan="2">Produktion von - bis</th>
												<th class="sortColumn">Abfahrt</th>
												<th class="sortColumn">Ankunft</th>
												<th class="sortColumn">Versand</th>
												<th class="sortColumn">Transport</th>

												<!--
												<th class="sortColumn">Liste</th>
												-->

											</tr>
										</thead>

										<tbody>
											<?php
												$count = 0;

												$thisMarker = '';
												while($ds = mysqli_fetch_assoc($rs)){
													#dd('ds');
													if($count%2 == 0){ $rowClass = 'row0'; }
													else { $rowClass = 'row1'; }

													if($thisMarker != $ds["containerListsProductionsDateEnd"]){
														$thisMarker = $ds["containerListsProductionsDateEnd"];
														#echo '<tr class="tableRowTitle1">';
														#echo '<td colspan="12">' . 'PRODUKTION ' . formatDate($ds["containerListsProductionsDateEnd"], "display") . '</td>';
														#echo '</tr>';
													}

													echo '<tr class="'.$rowClass.'">';

														echo '<td style="text-align:right;"><b>' . ($count + 1) .'.</b></td>';

														echo '<td style="font-weight:bold;">';
														echo $ds["containerListsCustomerNumber"];
														echo '</td>';

														echo '<td style="white-space:nowrap;">';
														$thisContainerListsCustomerName = $ds["containerListsCustomerName"];
														$thisContainerListsCustomerName = transliterateTurkishChars($thisContainerListsCustomerName, true);
														echo $thisContainerListsCustomerName;
														echo '</td>';

														echo '<td style="white-space:nowrap;">';
														$thisContainerListsCustomerKommission = $ds["containerListsCustomerKommission"];
														$thisContainerListsCustomerKommission = transliterateTurkishChars($thisContainerListsCustomerKommission, true);
														echo $thisContainerListsCustomerKommission;
														echo '</td>';

														echo '<td style="text-align:right;font-weight:bold;">';
														echo $ds["containerListsQuantity"];
														echo '</td>';

														echo '<td style="white-space:nowrap;">';
														$thisContainerListProduct = $ds["containerListsProduct"];
														$thisContainerListProduct = translateDeliveryData($thisContainerListProduct, "TR", "DE");
														$thisContainerListProduct = transliterateTurkishChars($thisContainerListProduct, false);

														$thisContainerListProductAdd = $ds["containerListsCita"];
														$thisContainerListProductAdd = translateDeliveryData($thisContainerListProductAdd, "TR", "DE");
														$thisContainerListProductAdd = transliterateTurkishChars($thisContainerListProductAdd, false);

														echo $thisContainerListProduct;

														if($thisContainerListProductAdd != ''){
															echo ' (' . $thisContainerListProductAdd . ')';
														}
														echo '</td>';

														echo '<td>';
														echo formatDate($ds["containerListsProductionsDateStart"], "display");
														echo '</td>';

														echo '<td>';
														echo formatDate($ds["containerListsProductionsDateEnd"], "display");
														echo '</td>';

														echo '<td style="white-space:nowrap;">';
														$thisContainerListsDepartureKW = $ds["containerListsDepartureKW"];
														if(strlen($thisContainerListsDepartureKW) == 1){
															$thisContainerListsDepartureKW = '0' . $thisContainerListsDepartureKW;
														}
														echo 'ca. ' . $thisContainerListsDepartureKW . '. KW';
														echo '</td>';

														echo '<td style="white-space:nowrap;">';
														$thisContainerListsArrivalKW = $ds["containerListsArrivalKW"];
														if(strlen($thisContainerListsArrivalKW) == 1){
															$thisContainerListsArrivalKW = '0' . $thisContainerListsArrivalKW;
														}
														echo 'ca. ' . $thisContainerListsArrivalKW . '. KW';
														echo '</td>';

														echo '<td style="white-space:nowrap;font-weight:bold;">';
														$thisContainerListsDeliveryKW = $ds["containerListsDeliveryKW"];
														if(strlen($thisContainerListsDeliveryKW) == 1){
															$thisContainerListsDeliveryKW = '0'.$thisContainerListsDeliveryKW;
														}
														echo 'ca. ' . $thisContainerListsDeliveryKW . '. KW';
														echo '</td>';

														echo '<td style="white-space:nowrap;text-align:center;">';
														if($ds["containerListsTirArkasi"] != ''){
															echo '<img width="14" height="14" src="layout/icons/redDot.png" alt="Hinten LKW (Vorkasse / Nachnahme)" title="Hinten LKW (Vorkasse / Nachnahme)" />';
														}
														echo '</td>';

														#echo '<td style="white-space:nowrap">';
														#echo $ds["containerListsFilename"];
														#echo '</td>';

													echo '</tr>';
													$count++;
												}
											?>
										<tbody>
									</table>
								</fieldset>
							<?php } ?>

							<?php
									displayMessages();
								}
							?>


							<?php if($_REQUEST["editID"] != "NEW") { ?>
							<?php if($_REQUEST["tab"] == "tabs-2") { ?>
							<div id="tabs-2">
								<fieldset style="width:90% !important;">
									<legend>Bisherige Produktionen</legend>

									<div class="colorsLegend">
										<div class="legendItem">
											<b>F&auml;rbung:</b>
										</div>

										<div class="legendItem">
											<b>Zeilen:</b>
										</div>
										<div class="legendItem">
											<span class="legendColorField" id="colorField4"></span>
											<span class="legendDescription">Aktuelle Zeile</span>
											<div class="clear"></div>
										</div>
										<div class="legendSeperator">&nbsp;</div>
										<!--
										<div class="legendItem">
											<span class="legendColorField" id="colorField7"></span>
											<span class="legendDescription">Freigegeben</span>
											<div class="clear"></div>
										</div>
										-->

										<div class="legendItem">
											<span class="legendColorField" id="colorField8"></span>
											<span class="legendDescription">Belichtet</span>
											<div class="clear"></div>
										</div>
										<div class="legendItem">
											<span class="legendColorField" id="colorField1"></span>
											<span class="legendDescription">in Produktion</span>
											<div class="clear"></div>
										</div>
										<div class="legendItem">
											<span class="legendColorField" id="colorField3"></span>
											<span class="legendDescription">Versendet</span>
											<div class="clear"></div>
										</div>

										<div class="legendSeperator">&nbsp;</div>

										<div class="legendItem">
											<a href="<?php echo PAGE_DISPLAY_ORDERS_OVERVIEW; ?>?searchCustomerNumber=<?php echo $customerDatas["customersKundennummer"]; ?>" class="linkButton">Zur Produktionsliste wechseln</a>
										</div>

										<div class="clear"></div>
									</div>
									<?php
										$sql = "
											SELECT
												`tempTable`.`ordersID`,
												`tempTable`.`ordersKundennummer`,
												`tempTable`.`ordersCustomerID`,
												`tempTable`.`ordersStatus`,
												`tempTable`.`ordersSourceType`,
												`tempTable`.`ordersBestellDatum`,
												`tempTable`.`ordersStornoDatum`,
												`tempTable`.`ordersFreigabeDatum`,
												`tempTable`.`ordersFreigabeArt`,
												`tempTable`.`ordersBelichtungsDatum`,
												`tempTable`.`ordersProduktionsDatum`,
												`tempTable`.`ordersAuslieferungsDatum`,
												`tempTable`.`ordersLieferwoche`,
												`tempTable`.`ordersLieferDatum`,
												`tempTable`.`ordersKundenName`,
												`tempTable`.`ordersKommission`,
												`tempTable`.`ordersPLZ`,
												`tempTable`.`ordersOrt`,
												`tempTable`.`ordersArtikelID`,
												`tempTable`.`ordersArtikelNummer`,
												`tempTable`.`ordersArtikelBezeichnung`,
												`tempTable`.`ordersArtikelKategorieID`,
												`tempTable`.`ordersArtikelMenge`,
												`tempTable`.`ordersArtikelPrintColorsCount`,
												`tempTable`.`ordersArtikelWithBorder`,
												`tempTable`.`ordersArtikelWithClearPaint`,
												`tempTable`.`ordersAufdruck`,
												`tempTable`.`ordersAdditionalArtikelID`,
												`tempTable`.`ordersAdditionalArtikelNummer`,
												`tempTable`.`ordersAdditionalArtikelBezeichnung`,
												`tempTable`.`ordersAdditionalArtikelKategorieID`,
												`tempTable`.`ordersAdditionalArtikelMenge`,
												`tempTable`.`ordersSinglePreis`,
												`tempTable`.`ordersTotalPreis`,
												`tempTable`.`ordersDruckFarbe`,
												`tempTable`.`ordersAdditionalCosts`,
												`tempTable`.`ordersAdditionalCostsID`,
												`tempTable`.`ordersVertreter`,
												`tempTable`.`ordersVertreterID`,
												`tempTable`.`ordersMailAdress`,
												`tempTable`.`ordersContactOthers`,
												`tempTable`.`ordersMandant`,
												`tempTable`.`ordersOrderType`,
												`tempTable`.`ordersPerExpress`,
												`tempTable`.`ordersBankAccountType`,
												`tempTable`.`ordersPaymentType`,
												`tempTable`.`ordersPaymentStatusType`,
												`tempTable`.`ordersNotizen`,
												`tempTable`.`ordersInfo`,
												`tempTable`.`ordersDruckerName`,
												`tempTable`.`ordersUserID`,
												`tempTable`.`ordersTimeCreated`,
												`tempTable`.`ordersArchive`,
												`tempTable`.`ordersUseNoStock`,
												`tempTable`.`editOrdersArtikelGewicht`,
												`tempTable`.`editOrdersArtikelMaterial`,
												`tempTable`.`editOrdersArtikelGesamtGewichtK`,
												`tempTable`.`editOrdersVerpackungseinheiten`,
												`tempTable`.`ordersDirectSale`,
												`tempTable`.`ordersReklamationsgrund`,
	`tempTable`.`sachbearbeiter`,
	`tempTable`.`aufdruckPDF`,
												`tempTable`.`ordersProductionPrintingPlant`,

												`tempTable`.`ordersPrintPlateNumber`,


												`" . TABLE_SUPPLIER_ORDERS . "`.`supplierOrdersID`,
												`" . TABLE_SUPPLIER_ORDERS . "`.`supplierOrdersAmount`,
												`" . TABLE_SUPPLIER_ORDERS . "`.`supplierOrdersProductNumber`,
												`" . TABLE_SUPPLIER_ORDERS . "`.`supplierOrdersProductName`,
												`" . TABLE_SUPPLIER_ORDERS . "`.`supplierOrdersOrderDate`,
												`" . TABLE_SUPPLIER_ORDERS . "`.`supplierOrdersDeliveryDate`,

												`" . TABLE_SUPPLIERS . "`.`suppliersFirmenname`,

												`" . TABLE_PRINT_PRODUCTION_ORDERS . "`.`printProductionOrdersID`,
												`" . TABLE_PRINT_PRODUCTION_ORDERS . "`.`printProductionOrdersProductDatas`,

												`" . TABLE_PRODUCTIONS_TRANSFER . "`.`ordersProductionsTransferID`,

												`" . TABLE_PRODUCTIONS_TRANSFER . "`.`ordersProductionsTransferOrdersID`,

												`" . TABLE_PRODUCTIONS_TRANSFER . "`.`ordersProductionsTransferDeliveryRedDot`,
												`" . TABLE_PRODUCTIONS_TRANSFER . "`.`ordersProductionsTransferDeliveryNoDPD`,
												`" . TABLE_PRODUCTIONS_TRANSFER . "`.`ordersProductionsTransferDeliveryContainerDate`,

												`" . TABLE_PRODUCTIONS_TRANSFER . "`.`ordersProductionsTransferMailSubmitDateTime`,
												`" . TABLE_PRODUCTIONS_TRANSFER . "`.`ordersProductionsTransferDeliveryPrintDate`,

												`" . TABLE_PRODUCTIONS_TRANSFER . "`.`ordersProductionsTransferOrdersProductQuantity`,
												`" . TABLE_PRODUCTIONS_TRANSFER . "`.`ordersProductionsTransferOrdersProductOriginalQuantity`,

												`" . TABLE_PRODUCTIONS_TRANSFER . "`.`ordersProductionsTransferProductionStatus`,
												`" . TABLE_PRODUCTIONS_TRANSFER . "`.`ordersProductionsTransferProductionTrackingNumbers`,
												`" . TABLE_PRODUCTIONS_TRANSFER . "`.`ordersProductionsTransferProductionModified`

												FROM (
													SELECT
														`" . TABLE_ORDERS . "`.`ordersID`,
														`" . TABLE_ORDERS . "`.`ordersKundennummer`,
														`" . TABLE_ORDERS . "`.`ordersCustomerID`,
														`" . TABLE_ORDERS . "`.`ordersStatus`,
														`" . TABLE_ORDERS . "`.`ordersSourceType`,
														`" . TABLE_ORDERS . "`.`ordersBestellDatum`,
														`" . TABLE_ORDERS . "`.`ordersStornoDatum`,
														`" . TABLE_ORDERS . "`.`ordersFreigabeDatum`,
														`" . TABLE_ORDERS . "`.`ordersFreigabeArt`,
														`" . TABLE_ORDERS . "`.`ordersBelichtungsDatum`,
														`" . TABLE_ORDERS . "`.`ordersProduktionsDatum`,
														`" . TABLE_ORDERS . "`.`ordersAuslieferungsDatum`,
														`" . TABLE_ORDERS . "`.`ordersLieferwoche`,
														`" . TABLE_ORDERS . "`.`ordersLieferDatum`,
														`" . TABLE_ORDERS . "`.`ordersKundenName`,
														`" . TABLE_ORDERS . "`.`ordersKommission`,
														`" . TABLE_ORDERS . "`.`ordersPLZ`,
														`" . TABLE_ORDERS . "`.`ordersOrt`,
														`" . TABLE_ORDERS . "`.`ordersArtikelID`,
														`" . TABLE_ORDERS . "`.`ordersArtikelNummer`,
														`" . TABLE_ORDERS . "`.`ordersArtikelBezeichnung`,
														`" . TABLE_ORDERS . "`.`ordersArtikelKategorieID`,
														`" . TABLE_ORDERS . "`.`ordersArtikelMenge`,
														`" . TABLE_ORDERS . "`.`ordersArtikelPrintColorsCount`,
														`" . TABLE_ORDERS . "`.`ordersArtikelWithBorder`,
														`" . TABLE_ORDERS . "`.`ordersArtikelWithClearPaint`,
														`" . TABLE_ORDERS . "`.`ordersAufdruck`,
														`" . TABLE_ORDERS . "`.`ordersAdditionalArtikelID`,
														`" . TABLE_ORDERS . "`.`ordersAdditionalArtikelNummer`,
														`" . TABLE_ORDERS . "`.`ordersAdditionalArtikelBezeichnung`,
														`" . TABLE_ORDERS . "`.`ordersAdditionalArtikelKategorieID`,
														`" . TABLE_ORDERS . "`.`ordersAdditionalArtikelMenge`,
														`" . TABLE_ORDERS . "`.`ordersSinglePreis`,
														`" . TABLE_ORDERS . "`.`ordersTotalPreis`,
														`" . TABLE_ORDERS . "`.`ordersDruckFarbe`,
														`" . TABLE_ORDERS . "`.`ordersAdditionalCosts`,
														`" . TABLE_ORDERS . "`.`ordersAdditionalCostsID`,
														`" . TABLE_ORDERS . "`.`ordersVertreter`,
														`" . TABLE_ORDERS . "`.`ordersVertreterID`,
														`" . TABLE_ORDERS . "`.`ordersMailAdress`,
														`" . TABLE_ORDERS . "`.`ordersContactOthers`,
														`" . TABLE_ORDERS . "`.`ordersMandant`,
														`" . TABLE_ORDERS . "`.`ordersOrderType`,
														`" . TABLE_ORDERS . "`.`ordersPerExpress`,
														`" . TABLE_ORDERS . "`.`ordersBankAccountType`,
														`" . TABLE_ORDERS . "`.`ordersPaymentType`,
														`" . TABLE_ORDERS . "`.`ordersPaymentStatusType`,
														`" . TABLE_ORDERS . "`.`ordersNotizen`,
														`" . TABLE_ORDERS . "`.`ordersInfo`,
														`" . TABLE_ORDERS . "`.`ordersDruckerName`,
														`" . TABLE_ORDERS . "`.`ordersUserID`,
														`" . TABLE_ORDERS . "`.`ordersTimeCreated`,
														`" . TABLE_ORDERS . "`.`ordersArchive`,
														`" . TABLE_ORDERS . "`.`ordersUseNoStock`,
														`" . TABLE_ORDERS . "`.`editOrdersArtikelGewicht`,
														`" . TABLE_ORDERS . "`.`editOrdersArtikelMaterial`,
														`" . TABLE_ORDERS . "`.`editOrdersArtikelGesamtGewichtK`,
														`" . TABLE_ORDERS . "`.`editOrdersVerpackungseinheiten`,
														`" . TABLE_ORDERS . "`.`ordersDirectSale`,
														`" . TABLE_ORDERS . "`.`ordersReklamationsgrund`,
														`" . TABLE_ORDERS . "`.`ordersProductionPrintingPlant`,
`" . TABLE_ORDERS . "`.`sachbearbeiter`,
`" . TABLE_ORDERS . "`.`aufdruckPDF`,
														`" . TABLE_ORDERS . "`.`ordersPrintPlateNumber`

													FROM `" . TABLE_ORDERS . "`

													WHERE 1
														AND `" . TABLE_ORDERS . "`.`ordersKundennummer` = '" . $customerDatas["customersKundennummer"] . "'

													/* ORDER BY `" . TABLE_ORDERS . "`.`ordersBestellDatum` DESC */

												UNION

													SELECT
														`" . TABLE_ORDERS . "`.`ordersID`,
														`" . TABLE_ORDERS . "`.`ordersKundennummer`,
														`" . TABLE_ORDERS . "`.`ordersCustomerID`,
														`" . TABLE_ORDERS . "`.`ordersStatus`,
														`" . TABLE_ORDERS . "`.`ordersSourceType`,
														`" . TABLE_ORDERS . "`.`ordersBestellDatum`,
														`" . TABLE_ORDERS . "`.`ordersStornoDatum`,
														`" . TABLE_ORDERS . "`.`ordersFreigabeDatum`,
														`" . TABLE_ORDERS . "`.`ordersFreigabeArt`,
														`" . TABLE_ORDERS . "`.`ordersProduktionsDatum`,
														`" . TABLE_ORDERS . "`.`ordersBelichtungsDatum`,
														`" . TABLE_ORDERS . "`.`ordersAuslieferungsDatum`,
														`" . TABLE_ORDERS . "`.`ordersLieferwoche`,
														`" . TABLE_ORDERS . "`.`ordersLieferDatum`,
														`" . TABLE_ORDERS . "`.`ordersKundenName`,
														`" . TABLE_ORDERS . "`.`ordersKommission`,
														`" . TABLE_ORDERS . "`.`ordersPLZ`,
														`" . TABLE_ORDERS . "`.`ordersOrt`,
														`" . TABLE_ORDERS . "`.`ordersArtikelID`,
														`" . TABLE_ORDERS . "`.`ordersArtikelNummer`,
														`" . TABLE_ORDERS . "`.`ordersArtikelBezeichnung`,
														`" . TABLE_ORDERS . "`.`ordersArtikelKategorieID`,
														`" . TABLE_ORDERS . "`.`ordersArtikelMenge`,
														`" . TABLE_ORDERS . "`.`ordersArtikelPrintColorsCount`,
														`" . TABLE_ORDERS . "`.`ordersArtikelWithBorder`,
														`" . TABLE_ORDERS . "`.`ordersArtikelWithClearPaint`,
														`" . TABLE_ORDERS . "`.`ordersAufdruck`,
														`" . TABLE_ORDERS . "`.`ordersAdditionalArtikelID`,
														`" . TABLE_ORDERS . "`.`ordersAdditionalArtikelNummer`,
														`" . TABLE_ORDERS . "`.`ordersAdditionalArtikelBezeichnung`,
														`" . TABLE_ORDERS . "`.`ordersAdditionalArtikelKategorieID`,
														`" . TABLE_ORDERS . "`.`ordersAdditionalArtikelMenge`,
														`" . TABLE_ORDERS . "`.`ordersSinglePreis`,
														`" . TABLE_ORDERS . "`.`ordersTotalPreis`,
														`" . TABLE_ORDERS . "`.`ordersDruckFarbe`,
														`" . TABLE_ORDERS . "`.`ordersAdditionalCosts`,
														`" . TABLE_ORDERS . "`.`ordersAdditionalCostsID`,
														`" . TABLE_ORDERS . "`.`ordersVertreter`,
														`" . TABLE_ORDERS . "`.`ordersVertreterID`,
														`" . TABLE_ORDERS . "`.`ordersMailAdress`,
														`" . TABLE_ORDERS . "`.`ordersContactOthers`,
														`" . TABLE_ORDERS . "`.`ordersMandant`,
														`" . TABLE_ORDERS . "`.`ordersOrderType`,
														`" . TABLE_ORDERS . "`.`ordersPerExpress`,
														`" . TABLE_ORDERS . "`.`ordersBankAccountType`,
														`" . TABLE_ORDERS . "`.`ordersPaymentType`,
														`" . TABLE_ORDERS . "`.`ordersPaymentStatusType`,
														`" . TABLE_ORDERS . "`.`ordersNotizen`,
														`" . TABLE_ORDERS . "`.`ordersInfo`,
														`" . TABLE_ORDERS . "`.`ordersDruckerName`,
														`" . TABLE_ORDERS . "`.`ordersUserID`,
														`" . TABLE_ORDERS . "`.`ordersTimeCreated`,
														`" . TABLE_ORDERS . "`.`ordersArchive`,
														`" . TABLE_ORDERS . "`.`ordersUseNoStock`,
														`" . TABLE_ORDERS . "`.`editOrdersArtikelGewicht`,
														`" . TABLE_ORDERS . "`.`editOrdersArtikelMaterial`,
														`" . TABLE_ORDERS . "`.`editOrdersArtikelGesamtGewichtK`,
														`" . TABLE_ORDERS . "`.`editOrdersVerpackungseinheiten`,
														`" . TABLE_ORDERS . "`.`ordersDirectSale`,
														`" . TABLE_ORDERS . "`.`ordersReklamationsgrund`,
														`" . TABLE_ORDERS . "`.`ordersProductionPrintingPlant`,
`" . TABLE_ORDERS . "`.`sachbearbeiter`,
`" . TABLE_ORDERS . "`.`aufdruckPDF`,
														`" . TABLE_ORDERS . "`.`ordersPrintPlateNumber`

													FROM `" . TABLE_ORDERS . "`

													LEFT JOIN `" . TABLE_CUSTOMERS . "` AS `customerSalesman`
													ON(`" . TABLE_ORDERS . "`.`ordersVertreterID` = `customerSalesman`.`customersID` AND `" . TABLE_ORDERS . "`.`ordersVertreterID` > 0)

													WHERE 1
														AND `customerSalesman`.`customersKundennummer` = '" . $customerDatas["customersKundennummer"] . "'

													/*ORDER BY `ordersBestellDatum` DESC*/
												) AS `tempTable`


												LEFT JOIN `" . TABLE_SUPPLIER_ORDERS . "`
												ON(`tempTable`.`ordersID` = `" . TABLE_SUPPLIER_ORDERS . "`.`supplierOrdersOrderProcessID`)

												LEFT JOIN `" . TABLE_SUPPLIERS . "`
												ON(`" . TABLE_SUPPLIER_ORDERS . "`.`supplierOrdersSupplierID` = `" . TABLE_SUPPLIERS . "`.`suppliersID`)

												LEFT JOIN `" . TABLE_PRODUCTIONS_TRANSFER . "`
												ON(`tempTable`.`ordersID` = `" . TABLE_PRODUCTIONS_TRANSFER . "`.`ordersProductionsTransferOrdersID`)

												LEFT JOIN `" . TABLE_PRINT_PRODUCTION_ORDERS . "`
												ON(`tempTable`.`ordersID` = `" . TABLE_PRINT_PRODUCTION_ORDERS . "`.`printProductionOrdersOrderID`)

												ORDER BY
													`ordersBestellDatum` DESC,
													`ordersArtikelBezeichnung` ASC
											";

										$rs = $dbConnection->db_query($sql);

										echo '<table border="1" cellpadding="0" cellspacing="0" width="99%" style="width:99%;" class="displayOrders">';

										echo '<colgroup>';
										echo '<col />';
										// echo '<col />';
										echo '<col />';
										echo '<col />';
										echo '<col />';
										echo '<col />';
										echo '<col />';
										echo '<col />';
										echo '<col />';
										echo '<col />';
										echo '<col />';
										echo '<col />';
										echo '<col />';
										#echo '<col />';
										#echo '<col />';
										echo '<col />';
										#echo '<col />';
										// echo '<col />';
										echo '<col />';
										echo '<col />';
										echo '<col />';
										echo '<col />';
										echo '</colgroup>';

										echo '<thead>';
										echo '<tr >';
											echo '<th style="width:45px;text-align:right;">#</th>';
											// echo '<th>K-Nr</th>';
											echo '<th>Eingang</th>';
											echo '<th>Belichtet</th>';
											echo '<th>Druck</th>';
											echo '<th>Freigabe</th>';
											echo '<th>Geliefert</th>';
											echo '<th>Name</th>';
											echo '<th>Status DE</th>';
											echo '<th>Status TR</th>';
											echo '<th>Art.-Nr.</th>';
											echo '<th>Art.-Name</th>';
											echo '<th>Menge</th>';
											echo '<th>Material</th>';
											echo '<th>Art-Gewicht(gr)</th>';
											echo '<th>Gsmt-Gewicht(kg)</th>';
											echo '<th>Verpakung</th>';
											#echo '<th>Einzelpreis</th>';
											#echo '<th>Gesamtpreis</th>';
											echo '<th>Druckfarbe</th>';
											#echo '<th>Zusatzkosten</th>';
											// echo '<th>Drucker</th>';
											echo '<th>Bestellart</th>';
											echo '<th>Quelle</th>';
											echo '<th>Mandant</th>';
											echo '<th>Info</th>';
											echo '<th>Sachbearbeiter</th>';
										echo '</tr>';
										echo '</thead>';
										echo '<tbody>';
										$count = 0;
										$orderPriceSum = 0;
										while($ds = mysqli_fetch_array($rs)) {
											 
												if($count%2 == 0) {
													$classRow = 'row0';
												}
												else {
													$classRow = 'row1';
												}

												if($ds["ordersStatus"] == "1") {
													// $classRow = 'row5';
												}
												else if($ds["ordersStatus"] == "3") {
													#$classRow = 'row2';
													$classRow = 'row9';
												}
												else if($ds["ordersStatus"] == "4") {
													$classRow = 'row3';
												}
												else if($ds["ordersStatus"] == "6") {
													$classRow = 'row6';
												}
												else if($ds["ordersStatus"] == "7") {
													#$classRow = 'row9';
													$classRow = 'row2';
												}

												echo '<tr class="'.$classRow.'">';
												echo '<td style="text-align:right"><b>'.($count + 1).'.</b></td>';
												// echo '<td style="text-align:center;">'.$ds["ordersKundennummer"].'</td>';
												echo '<td style="text-align:left;font-size:11px;font-weight:bold;">';
												echo formatDate($ds["ordersBestellDatum"], "display");

												echo '<br />';

												$thisIconImage = '';
												$thisIconClass = '';
												if($ds["ordersOrderType"] == 3){
													$thisIconImage = '<img src="layout/icons/iconAttention.png" width="14" height="14" alt="" /> ';
													$thisIconClass = 'class="iconAttention"';
												}
												else if($ds["ordersOrderType"] == 1){
													$thisIconImage = '<img src="layout/icons/iconNewOrder.png" width="14" height="14" alt="" /> ';
													$thisIconClass = '';
												}
												echo '<span style="font-size:11px;white-space:nowrap;" ' . $thisIconClass . ' >';
												echo $thisIconImage;

												echo $arrOrderTypeDatas[$ds["ordersOrderType"]]["orderTypesName"];
												echo '</span>';

												echo '</td>';
												echo '<td style="text-align:center;font-size:11px;">'.formatDate($ds["ordersBelichtungsDatum"], "display").'</td>';

												/*
												echo '<td style="text-align:center;">';
												echo formatDate($ds["ordersProduktionsDatum"], "display");
												if($ds["printProductionOrdersID"] > 0){
													#echo ' <span style="font-weight:bold;background-color:#FF0000;color:#FFF;cursor:pointer;" title="Produktion in der T&uuml;rkei">TR</span>';
												}
												if($ds["ordersProductionPrintingPlant"] != "DE"){
													echo ' <span style="font-weight:bold;background-color:#FF0000;color:#FFF;cursor:pointer;" title="Produktion in der T&uuml;rkei">' . $ds["ordersProductionPrintingPlant"] . '</span>';
												}
												echo '</td>';
												*/

												echo '<td style="white-space:nowrap;text-align:right;font-size:11px;">';
												if($ds["printProductionOrdersID"] > 0){
													#echo ' <span class="countryTR" title="Produktion in der T&uuml;rkei">TR</span>';
												}
												#dd('ds');
												#if($ds["ordersProductionPrintingPlant"] != "DE" || $ds["ordersProductionsTransferOrdersID"] != ""){
												if($ds["ordersProductionsTransferOrdersID"] != "" || $ds["ordersProductionPrintingPlant"] == "TR"){
													echo '<div>';
													if($ds["ordersProductionsTransferOrdersID"] != ""){
														echo '<span class="countryTR" title="Produktion in der T&uuml;rkei">TR</span> ';
														echo substr(formatDate($ds["ordersProductionsTransferMailSubmitDateTime"], "display"), 0, 10);
													} else if($ds["ordersProductionPrintingPlant"] == "TR"){
														echo '<span class="countryTR" title="Produktion in der T&uuml;rkei">TR</span> ';
														echo substr(formatDate($ds["ordersProduktionsDatum"], "display"), 0, 10);
													}
													echo '</div>';
												}

												/*
												if($arrGetUserRights["transferPrintProduction"] || (in_array($ds["ordersStatus"], array(2, 3, 7)) && $arrGetUserRights["transferPrintProduction"] && $ds["ordersProductionsTransferOrdersID"] == '') || $_COOKIE["isAdmin"] == '1'){
													#if($ds["ordersStatus"] == 2 && $ds["printProductionOrdersID"] == '' && $arrGetUserRights["transferPrintProduction"]){
													# if($ds["ordersStatus"] == 2 && $arrGetUserRights["transferPrintProduction"]){
													// FELD ordersIsExported
													$newExportStatusID = 7;
													$thisRel = '';
													$thisRel .= $ds["ordersID"];
													$thisRel .= '|';
													$thisRel .= $ds["ordersKundennummer"];
													$thisRel .= '|';
													$thisRel .= $ds["ordersStatus"];
													$thisRel .= '|';
													$thisRel .= $newExportStatusID;

													$thisRel .= '|';
													$thisRel .= $ds["ordersArtikelMenge"];

													echo '<img src="layout/icons/exportOrder.png" style="cursor:pointer;" class="buttonExportOrder" width="16" height="16" rel="' . $thisRel . '" alt="Export" title="Produktion in die T&uuml;rkei transferieren" />';
												}
												*/

												$thisOrdersProduktionsDatum = formatDate($ds["ordersProduktionsDatum"], "display");
												if($thisOrdersProduktionsDatum != '' && $ds["ordersProductionsTransferOrdersProductQuantity"] != $ds["ordersArtikelMenge"] && $ds["ordersProductionPrintingPlant"] == "DE"){
												#if($thisOrdersProduktionsDatum != ''){
													echo '<div>';
													echo '<span class="countryDE" title="Produktion in Deutschland">DE</span> ';
													echo $thisOrdersProduktionsDatum;
													echo '</div>';
												}

												echo '</td>';
												echo '<td style="text-align:center;font-size:11px;">'.formatDate($ds["ordersFreigabeDatum"], "display").'</td>';
												echo '<td style="text-align:center;font-size:11px;">'.formatDate($ds["ordersAuslieferungsDatum"], "display").'</td>';
												echo '<td style="font-size:11px;">';
												echo htmlentities(($ds["ordersKundenName"]));
												if($ds["ordersKommission"] != '') {
													echo '<div class="remarksArea" style="color:#FF0000;">';
													echo '<b>Kommission:</b> ' . htmlentities(($ds["ordersKommission"]));
													echo '</div>';
												}
												if($ds["ordersAufdruck"] != '') {
													echo '<div class="remarksArea">';
													echo '<b>Aufdruck:</b> ' . htmlentities(($ds["ordersAufdruck"]));
													echo '</div>';
												}
												if($ds["ordersPerExpress"] == '1') {
													echo '<div class="remarksArea">';
													echo '<b style="color:#FF0000;">als Express-Bestellung</b>';
													echo '</div>';
												}
												if($ds["ordersDirectSale"] == '1') {
													echo '<div class="remarksArea">';
													echo '<b style="color:#FF0000;">Direktverkauf durch den Vertreter</b>';
													echo '</div>';
												}
												if($ds["ordersInfo"] != '') {
													echo '<div class="remarksArea" style="color:#FF0000;">';
													echo '<b>Info:</b> ' . $ds["ordersInfo"];
													echo '</div>';
												}

												if($ds["ordersNotizen"] != '') {
													echo '<div class="remarksArea">';
													echo '<b>Bemerkung:</b><br />' . $ds["ordersNotizen"];
													echo '</div>';
												}
												// BOF COMPLAINT
												if($ds["ordersReklamationsgrund"] != '') {
													echo '<div class="remarksArea">';
													echo '<b style="color:#FF0000;">Reklamiert:</b> ' . $ds["ordersReklamationsgrund"];
													echo '</div>';
												}
												// EOF COMPLAINT
												echo '</td>';
												echo '<td><span title="'.($arrOrderStatusTypeDatas[$ds["ordersStatus"]]["orderStatusTypesName"]).'">'.($arrOrderStatusTypeDatas[$ds["ordersStatus"]]["orderStatusTypesShortName"]).'</span></td>';

												echo '<td>';
												if($ds["ordersProductionsTransferProductionStatus"] != ''){
													echo '<span title="'.($arrOrderStatusTypeDatasExtern[$ds["ordersProductionsTransferProductionStatus"]]["orderStatusTypesName"]).'">'.($arrOrderStatusTypeDatasExtern[$ds["ordersProductionsTransferProductionStatus"]]["orderStatusTypesShortName"]).'</span>';
												}
												echo '</td>';


												echo '<td style="text-align:center;font-size:11px;">'.$ds["ordersArtikelNummer"].'</td>';

												echo '<td style="text-align:left;font-size:11px;">';

												echo htmlentities(($ds["ordersArtikelBezeichnung"]));

												if($ds["ordersAdditionalArtikelMenge"] > 0) {
													echo '<br /><b>Zusatzleisten:</b> ' . ($ds["ordersAdditionalArtikelMenge"]);
													}
												
												


												


												// BOF EXTERNAL PRODUCT ORDER
													if($ds["supplierOrdersID"] > 0){
														echo '';
														echo '<span class="remarksArea" style="color:#FF0000;">';
														echo '<br />';
														echo '<b>Ware (' .  $ds["supplierOrdersAmount"] . ' St&uuml;ck) am ' .  formatDate($ds["supplierOrdersOrderDate"], 'display') . ' bestellt bei ' .  $ds["suppliersFirmenname"] . '</b>';
														echo '</span>';
														if($ds["supplierOrdersDeliveryDate"] > 0){
															echo '<span class="remarksArea" style="color:#000099;">';
															echo '<br />';
															echo '<b>Ware geliefert am ' .   formatDate($ds["supplierOrdersDeliveryDate"], 'display') . '.</b>';
															echo '</span>';
														}
													}
												// EOF EXTERNAL PRODUCT ORDER

												echo '</td>';

												#echo '<td style="text-align:right;">'.$ds["ordersArtikelMenge"].'</td>';

												echo '<td style="white-space:nowrap;text-align:right;">';
												if($ds["printProductionOrdersProductDatas"] != ""){
													$arrPrintProductionOrdersProductDatas = unserialize($ds["printProductionOrdersProductDatas"]);
													#dd('arrPrintProductionOrdersProductDatas');
													$ds["ordersProductionsTransferOrdersProductQuantity"] = $arrPrintProductionOrdersProductDatas["ordersArtikelMenge"];
												}
												echo '<b>' . ($ds["ordersArtikelMenge"]) . '</b>';
												if(!in_array($ds["ordersStatus"], array(1, 2, 6))){
													if($ds["ordersProductionsTransferOrdersProductQuantity"] > 0){
														echo '<br /><span class="countryTR">TR:</span> '.($ds["ordersProductionsTransferOrdersProductQuantity"]).'';
													}
													if(($ds["ordersArtikelMenge"] - $ds["ordersProductionsTransferOrdersProductQuantity"]) > 0){
														echo '<br /><span class="countryDE">DE:</span> '.($ds["ordersArtikelMenge"] - $ds["ordersProductionsTransferOrdersProductQuantity"]).'';
													}
												}
												echo '</td>';
												echo '<td style="text-align:right"><b>'.$ds["editOrdersArtikelMaterial"].'.</b></td>';
												echo '<td style="text-align:right"><b>'.$ds["editOrdersArtikelGewicht"].'.</b></td>';
												echo '<td style="text-align:right"><b>'.$ds["editOrdersArtikelGesamtGewichtK"].'.</b></td>';
												echo '<td style="text-align:right"><b>'.$ds["editOrdersVerpackungseinheiten"].'.</b></td>';
												// echo '<td style="text-align:right"><b>'.$ds["editOrdersArtikelMaterial"].'.</b></td>';

												#echo '<td style="text-align:right;">'.number_format($ds["ordersSinglePreis"], 2).' &euro;</td>';
												#echo '<td style="text-align:right;">'.number_format($ds["ordersTotalPreis"], 2).' &euro;</td>';
												echo '<td style="text-align:left;white-space:nowrap;">';
												echo ' &middot; ' . preg_replace("/\//", "<br /> &middot; ", $ds["ordersDruckFarbe"]);
												if($ds["ordersArtikelWithBorder"] == '1'){
													echo '<br />' . ' &middot; <b style="color:#FF0000;">mit Umrandung</b>';
												}
												if($ds["ordersArtikelWithClearPaint"] == '1'){
													echo '<br />' . ' &middot; <b style="color:#FF0000;">mit Klarlack</b>';
												}
												if($ds["ordersPrintPlateNumber"] != ''){
													echo '<br />' . ' Klischee-Nr.: <b style="color:#FF0000;">' . $ds["ordersPrintPlateNumber"] . '</b>';
												}
												echo '</td>';
												/*
												echo '<td style="text-align:right;">';
												if($ds["ordersAdditionalCosts"] > 0) {
													$orderPriceSum += $ds["ordersAdditionalCosts"];
													echo $arrAdditionalCostsDatas[$ds["ordersAdditionalCostsID"]]["additionalCostsShortName"].' <span style="white-space:nowrap;">'.number_format($ds["ordersAdditionalCosts"], 2).' &euro;</span>';
												}
												else {
													echo '-';
												}
												echo '</td>';
												*/

												echo '<td style="text-align:right;font-size:11px;">'.$arrOrderTypeDatas[$ds["ordersOrderType"]]["orderTypesShortName"].'</td>';
												echo '<td style="text-align:left;font-size:11px;">';
												echo $arrOrdersSourceTypeDatas[$ds["ordersSourceType"]]["ordersSourceTypesName"];
												if($ds["ordersVertreter"] != ''){
													echo '<br /><span class="remarksArea" style="white-space:nowrap;">' . htmlentities(($ds["ordersVertreter"])) . '</span>';
												}
												echo '</td>';

												echo '<td>';
												echo '<b>' . $ds["ordersMandant"] . '</b>';
												echo '</td>';

												echo '<td>';
												// if($arrGetUserRights["editOrders"] && $ds["ordersStatus"] < 4) {
												if(1){
													echo '<span class="toolItem"><a href="' . PAGE_EDIT_PROCESS . '?editID='.$ds["ordersID"]. '"><img src="layout/icons/iconEdit.gif" width="16" height="16" title="Produktion bearbeiten (Datensatz '.$ds["ordersID"].')" alt="Bearbeiten" /></a></span>';
												if($ds["aufdruckPDF"] != '')
												{
													echo '<span class="toolItem"><a href="editCustomer2.php?downloadFile='.$ds["aufdruckPDF"]. '&documentType=PDF"><img src="layout/icons/iconPDF.gif" width="16" height="16" title="Druck Auftrag (Datensatz '.$ds["ordersID"].')" alt="Bearbeiten" /></a></span>';

												}
													#?downloadFile=xxx&documentType=PDF
												}
												echo '</td>';


													echo '<td>';
												// if($arrGetUserRights["editOrders"] && $ds["ordersStatus"] < 4) {
												if(1){
													echo  '<b>' .$ds["sachbearbeiter"]. '</b>';


												}
												echo '</td>';

												echo '</tr>';

												$orderPriceSum += $ds["ordersTotalPreis"];
												$count++;
										}
										echo '</tbody>';
										echo '</table>';

										echo '<table border="0" cellpadding="0" cellspacing="0">';
										#echo '<tr>';
										#echo '<td><b>Bestellwert insgesamt:</b></td><td>'.number_format($orderPriceSum, 2).' &euro;</td>';
										#echo '</tr>';
										echo '<tr>';
										echo '<td><b>Bestellungen insgesamt:</b></td><td>'.$count.'</td>';
										echo '</tr>';
										echo '</table>';
									?>
								</fieldset>
							</div>
							<?php } ?>
							<?php } ?>

							<?php if($_REQUEST["editID"] != "NEW") { ?>
							<?php if($_REQUEST["tab"] == "tabs-6") { ?>
								<div id="tabs-6">
									<fieldset>
										<legend>Auslieferungen</legend>
										<p class="infoArea" style="line-height:20px;">
											<span style="padding-right:20px;white-space:nowrap;">&bull; <span class="row12">blaue Zeile</span> : Aufkleber von DPD nicht eingescant bzw. noch nicht verschickt.</span>
											<span style="padding-right:20px;white-space:nowrap;">&bull; <span class="row11">durchgestrichene Zeile</span> : Aufkleber nicht verwendet</span>
										</p>
											<?php
												$sql = "
														SELECT
															`" . TABLE_DELIVERY_DATAS . "`.`Paketnummer`,
															`" . TABLE_DELIVERY_DATAS . "`.`Datum`,
															`" . TABLE_DELIVERY_DATAS . "`.`Produkt_Service`,
															`" . TABLE_DELIVERY_DATAS . "`.`Gewicht`,
															`" . TABLE_DELIVERY_DATAS . "`.`Referenznr_1`,
															`" . TABLE_DELIVERY_DATAS . "`.`Referenznr_2`,
															`" . TABLE_DELIVERY_DATAS . "`.`Referenznr_3`,
															`" . TABLE_DELIVERY_DATAS . "`.`Referenznr_4`,
															`" . TABLE_DELIVERY_DATAS . "`.`ID_Sendung`,
															`" . TABLE_DELIVERY_DATAS . "`.`ID_Versender_Scan`,
															`" . TABLE_DELIVERY_DATAS . "`.`Versanddatum`,
															`" . TABLE_DELIVERY_DATAS . "`.`Zustelldatum`,
															`" . TABLE_DELIVERY_DATAS . "`.`Firma_Empfaenger`,
															`" . TABLE_DELIVERY_DATAS . "`.`Name_1`,
															`" . TABLE_DELIVERY_DATAS . "`.`Zu_Haenden`,
															`" . TABLE_DELIVERY_DATAS . "`.`Adresse_1`,
															`" . TABLE_DELIVERY_DATAS . "`.`Adresse_2`,
															`" . TABLE_DELIVERY_DATAS . "`.`Land_1`,
															`" . TABLE_DELIVERY_DATAS . "`.`Region_1`,
															`" . TABLE_DELIVERY_DATAS . "`.`PLZ_1`,
															`" . TABLE_DELIVERY_DATAS . "`.`Stadt_1`,
															`" . TABLE_DELIVERY_DATAS . "`.`Tel_1`,
															`" . TABLE_DELIVERY_DATAS . "`.`Email_1`,
															`" . TABLE_DELIVERY_DATAS . "`.`Ref_Adresse_1`,
															`" . TABLE_DELIVERY_DATAS . "`.`Anrede_1`,
															`" . TABLE_DELIVERY_DATAS . "`.`Fax_1`,
															`" . TABLE_DELIVERY_DATAS . "`.`Bemerkung_Adresse`,
															`" . TABLE_DELIVERY_DATAS . "`.`Firma_Absender`,
															`" . TABLE_DELIVERY_DATAS . "`.`Name_2`,
															`" . TABLE_DELIVERY_DATAS . "`.`Adresse_Strasse_1`,
															`" . TABLE_DELIVERY_DATAS . "`.`Adresse_Strasse_2`,
															`" . TABLE_DELIVERY_DATAS . "`.`Land_2`,
															`" . TABLE_DELIVERY_DATAS . "`.`Region_2`,
															`" . TABLE_DELIVERY_DATAS . "`.`PLZ_2`,
															`" . TABLE_DELIVERY_DATAS . "`.`Stadt_2`,
															`" . TABLE_DELIVERY_DATAS . "`.`Tel_2`,
															`" . TABLE_DELIVERY_DATAS . "`.`Email_2`,
															`" . TABLE_DELIVERY_DATAS . "`.`Ref_Adresse_2`,
															`" . TABLE_DELIVERY_DATAS . "`.`Anrede_2`,
															`" . TABLE_DELIVERY_DATAS . "`.`Fax_2`,
															`" . TABLE_DELIVERY_DATAS . "`.`COD_Verwendungszweck`,
															`" . TABLE_DELIVERY_DATAS . "`.`COD_Betrag`,
															`" . TABLE_DELIVERY_DATAS . "`.`COD_Waehrung`,
															`" . TABLE_DELIVERY_DATAS . "`.`COD_Inkasoart`,
															`" . TABLE_DELIVERY_DATAS . "`.`AI_Laenge_cm`,
															`" . TABLE_DELIVERY_DATAS . "`.`AI_Breite_cm`,
															`" . TABLE_DELIVERY_DATAS . "`.`AI_Hoehe_cm`,
															`" . TABLE_DELIVERY_DATAS . "`.`AI_enthaelt_Begleitdokumente`,
															`" . TABLE_DELIVERY_DATAS . "`.`AI_Pakentinhalt`,
															`" . TABLE_DELIVERY_DATAS . "`.`AI_Betrag`,
															`" . TABLE_DELIVERY_DATAS . "`.`AI_Waehrung`,
															`" . TABLE_DELIVERY_DATAS . "`.`AI_Steuernummer`,
															`" . TABLE_DELIVERY_DATAS . "`.`AI_Kommentar`,
															`" . TABLE_DELIVERY_DATAS . "`.`AI_RE_Firma`,
															`" . TABLE_DELIVERY_DATAS . "`.`AI_RE_Adresse_1_Strasse`,
															`" . TABLE_DELIVERY_DATAS . "`.`AI_RE_Land`,
															`" . TABLE_DELIVERY_DATAS . "`.`AI_RE_Region`,
															`" . TABLE_DELIVERY_DATAS . "`.`AI_RE_PLZ`,
															`" . TABLE_DELIVERY_DATAS . "`.`AI_RE_Stadt`,
															`" . TABLE_DELIVERY_DATAS . "`.`AI_BI_Kommentar_1`,
															`" . TABLE_DELIVERY_DATAS . "`.`AI_BI_Inhalt`,
															`" . TABLE_DELIVERY_DATAS . "`.`AI_BI_Begleitdokumente`,
															`" . TABLE_DELIVERY_DATAS . "`.`Proaktive_Benachrichtigung`,
															`" . TABLE_DELIVERY_DATAS . "`.`HV_Betrag`,
															`" . TABLE_DELIVERY_DATAS . "`.`HV_Waehrung`,
															`" . TABLE_DELIVERY_DATAS . "`.`HV_Wareninhalt`,
															`" . TABLE_DELIVERY_DATAS . "`.`ID_Check_Name`,
															`" . TABLE_DELIVERY_DATAS . "`.`ABT_Gebaeude`,
															`" . TABLE_DELIVERY_DATAS . "`.`ABT_Stockwerk`,
															`" . TABLE_DELIVERY_DATAS . "`.`ABT_Abteilung`,
															`" . TABLE_DELIVERY_DATAS . "`.`SSCC_NVE`,
															`" . TABLE_DELIVERY_DATAS . "`.`Bankleitzahl`,
															`" . TABLE_DELIVERY_DATAS . "`.`Name_der_Bank`,
															`" . TABLE_DELIVERY_DATAS . "`.`Kontonummer`,
															`" . TABLE_DELIVERY_DATAS . "`.`Kontoinhaber`,
															`" . TABLE_DELIVERY_DATAS . "`.`IBAN`,
															`" . TABLE_DELIVERY_DATAS . "`.`BIC`,
															`" . TABLE_DELIVERY_DATAS . "`.`Shop_Kundennummer`,
															`" . TABLE_DELIVERY_DATAS . "`.`Shop_Firma`,
															`" . TABLE_DELIVERY_DATAS . "`.`Shop_Name`,
															`" . TABLE_DELIVERY_DATAS . "`.`Shop_Addresse_1`,
															`" . TABLE_DELIVERY_DATAS . "`.`Shop_Addresse_2`,
															`" . TABLE_DELIVERY_DATAS . "`.`Shop_Land`,
															`" . TABLE_DELIVERY_DATAS . "`.`Shop_Region`,
															`" . TABLE_DELIVERY_DATAS . "`.`Shop_PLZ`,
															`" . TABLE_DELIVERY_DATAS . "`.`Shop_Ort`,
															`" . TABLE_DELIVERY_DATAS . "`.`Shop_Telefon`,
															`" . TABLE_DELIVERY_DATAS . "`.`Mandant_ID`,
															`" . TABLE_DELIVERY_DATAS . "`.`Mandant_Name`,
															`" . TABLE_DELIVERY_DATAS . "`.`Benutzername`,
															`" . TABLE_DELIVERY_DATAS . "`.`Journal_ID`,
															`" . TABLE_DELIVERY_DATAS . "`.`Zustellung`,
															DATE_FORMAT(DATE_ADD(`" . TABLE_DELIVERY_DATAS . "`.`Versanddatum`, INTERVAL 12 DAY), '%Y-%m-%d') AS `Checkdatum`,

															GROUP_CONCAT(
																DISTINCT CONCAT(
																	IF(`deliveryDatasStatusImport_1`.`SCAN_CODE` IS NULL, '', `deliveryDatasStatusImport_1`.`SCAN_CODE`),
																	',',
																	IF(`deliveryDatasStatusImport_2`.`SCAN_CODE` IS NULL, '', `deliveryDatasStatusImport_2`.`SCAN_CODE`)
																)
															) AS `SCAN_CODES`

														FROM `" . TABLE_DELIVERY_DATAS . "`

														LEFT JOIN `" . TABLE_DELIVERY_DATAS_STATUS_IMPORT . "` AS `deliveryDatasStatusImport_1`
														ON(
															`" . TABLE_DELIVERY_DATAS . "`.`Paketnummer` = `deliveryDatasStatusImport_1`.`PARCELNO`
															AND (
																`" . TABLE_DELIVERY_DATAS . "`.`PLZ_1` = `deliveryDatasStatusImport_1`.`CONSIGNEE_ZIP`
																OR
																`" . TABLE_DELIVERY_DATAS . "`.`Referenznr_1` = `deliveryDatasStatusImport_1`.`CUSTOMER_REFERENCE`
															)
														)

														LEFT JOIN `" . TABLE_DELIVERY_DATAS_STATUS_IMPORT . "` AS `deliveryDatasStatusImport_2`
														ON(
															`deliveryDatasStatusImport_1`.`PARCELNO` = `deliveryDatasStatusImport_2`.`PARCELNO`
															AND
															`deliveryDatasStatusImport_1`.`SOURCE_FILE` = `deliveryDatasStatusImport_2`.`SOURCE_FILE`
															AND
															`deliveryDatasStatusImport_2`.`CONSIGNEE_ZIP` = ''
														)

														WHERE 1
															AND `Ref_Adresse_1` = '" . $customerDatas["customersKundennummer"] . "'

														GROUP BY
															CONCAT(
																`" . TABLE_DELIVERY_DATAS . "`.`Paketnummer`,
																`" . TABLE_DELIVERY_DATAS . "`.`Versanddatum`
															)

														ORDER BY
															`" . TABLE_DELIVERY_DATAS . "`.`Versanddatum` DESC,
															`" . TABLE_DELIVERY_DATAS . "`.`Paketnummer` DESC
													";
												$rs = $dbConnection->db_query($sql);

												$count = 0;

												$imgAttention = ' <img src="layout/icons/iconAttention.png" width="14" height="14" alt="!!" title="DPD hat keinen Zustell-Scan gemacht." />';


												$countRows = $dbConnection->db_getMysqlNumRows($rs);

												if($countRows > 0){
												?>
												<table border="0" cellpadding="0" cellspacing="0" width="100%" class="displayOrders">
													<colgroup>
														<col />
														<col />
														<col />
														<!--
														<col />
														<col />
														-->
														<col />
														<col />
														<col />
														<col />
														<col />
														<col />
														<col />
														<!--
														<col />
														-->
														<col />
														<col />
														<col />
														<col />
														<col />
														<col />
														<col />
													</colgroup>
													<thead>
														<tr>
															<th style="width:45px;text-align:right;">#</th>
															<th class="sortColumn">Paketnummer</th>
															<th class="sortColumn">K-Nr</th>
															<!--
															<th class="sortColumn">Typ</th>
															<th class="sortColumn">Gewicht</th>
															-->
															<th class="sortColumn">Referenz</th>
															<th class="sortColumn">Versand</th>
															<th class="sortColumn">Scan</th>
															<th class="sortColumn">Zustellung</th>
															<th>Empf&auml;nger</th>
															<th>Adresse</th>
															<th>Land</th>
															<!--
															<th>Region</th>
															-->
															<th>PLZ</th>
															<th>Stadt</th>

															<th>Paket</th>
															<th>Verwendungszweck</th>
															<th>Betrag</th>
															<th>Inkassoart</th>

															<th>Benutzer</th>
														</tr>
													</thead>

													<tbody>
												<?php
												while($ds = mysqli_fetch_assoc($rs)) { 
													if($count%2 == 0){ $rowClass = 'row0'; }
													else { $rowClass = 'row1'; }

													$ds["SCAN_CODES"] = preg_replace("/[,]{1,}$/", "", $ds["SCAN_CODES"]);
													if(array_key_exists("SCAN_CODES", $ds) && $ds["Zustellung"] == '' && $ds["SCAN_CODES"] == ''){
														$thisCheckDate = checkActiveParcelNumbers($ds["Paketnummer"], $ds["Versanddatum"]);
														if(date("Y-m-d") > $thisCheckDate){
															$rowClass = 'row11';
														}
														else {
															$rowClass = 'row12';
														}
													}

													if($ds["Zustellung"] != ''){
														$rowClass = 'row3';
														if(preg_match('/#/', $ds["Zustellung"])){
															$thisDeliveryData = formatLoadedDeliveryDatas($ds["Zustellung"], '#');
														}
														else if(preg_match('/|/', $ds["Zustellung"])){
															$thisDeliveryData = formatLoadedDeliveryDatas($ds["Zustellung"], '|');
														}

														// BOF LOAD EXTERNAL DELIVERY DATAS
															if(preg_match("/\[Kein Zustell-Scan\]/", $ds["Zustellung"])){
																$rowClass = 'row2';
																// $imgAttention = ' <img src="layout/icons/iconAttention.png" width="14" height="14" alt="!!" title="DPD hat keinen Zustell-Scan gemacht." />';
																if(preg_match('/#/', $ds["Zustellung"])){
																	$arrTempDeliveryData = explode('#', $ds["Zustellung"]);
																}
																else if(preg_match('/|/', $ds["Zustellung"])){
																	$arrTempDeliveryData = explode('|', $ds["Zustellung"]);
																}
																$deliveryDate = $arrTempDeliveryData[0];
																$deliveryTimestamp = strtotime($deliveryDate);
																$timestampInterval = 21 * (24 * 60 * 60); // 21 Tage zurück
																$todayTimestamp = time();

																if(($todayTimestamp - $deliveryTimestamp) <= $timestampInterval){
																	// $thisDeliveryData = '<span class="loadThisDeliveryData" id="loadDeliveryDate_' . $ds["Paketnummer"] . '_' . $count . '" ref="' . $ds["Paketnummer"] . ':' . $ds["Versanddatum"] . ':' . $count . '">' . formatDate($arrTempDeliveryData[0], 'display') . '</span>#<span id="loadDeliveryName_' . $ds["Paketnummer"] . '_' . $count . '">' . $arrTempDeliveryData[1] . ' ' . $imgAttention . '</span>';

																	#$thisRef = '' . $ds["Paketnummer"] . ':' . $ds["Versanddatum"] . ':' . $count . ':' . $ds["PLZ_1"] . ':' . $ds["Referenznr_1"];
																	$thisRef = '' . $ds["Paketnummer"] . ':' . $ds["Versanddatum"] . ':' . $count . ':' . $ds["PLZ_1"] . ':' . $ds["Ref_Adresse_1"];
																	$thisDeliveryData = '<span class="loadThisDeliveryData" id="loadDeliveryDate_' . $ds["Paketnummer"] . '_' . $count . '" ref="' . $thisRef . '">' . formatDate($arrTempDeliveryData[0], 'display') . '</span>#<span id="loadDeliveryName_' . $ds["Paketnummer"] . '_' . $count . '">' . $arrTempDeliveryData[1] . ' ' . $imgAttention . '</span>';
																}
																else {
																	$thisDeliveryData .= ' ' . $imgAttention;
																}
															}
														// EOF LOAD EXTERNAL DELIVERY DATAS
													}
													if($ds["Zustellung"] == ''){
														$thisDeliveryData = '-#-';
														// BOF LOAD EXTERNAL DELIVERY DATAS
															// $thisDeliveryData = '<span class="loadThisDeliveryData" id="loadDeliveryDate_' . $ds["Paketnummer"] . '_' . $count . '" ref="' . $ds["Paketnummer"] . ':' . $ds["Versanddatum"] . ':' . $count . '">-</span>#<span id="loadDeliveryName_' . $ds["Paketnummer"] . '_' . $count . '">-</span>';

															$thisRef = '' . $ds["Paketnummer"] . ':' . $ds["Versanddatum"] . ':' . $count . ':' . $ds["PLZ_1"];
															$thisDeliveryData = '<span class="loadThisDeliveryData" id="loadDeliveryDate_' . $ds["Paketnummer"] . '_' . $count . '" ref="' . $thisRef . '">-</span>#<span id="loadDeliveryName_' . $ds["Paketnummer"] . '_' . $count . '">-</span>';
														// EOF LOAD EXTERNAL DELIVERY DATAS
													}

													echo '<tr class="'.$rowClass.'">';

													echo '<td style="text-align:right;"><b>' . ($count + 1) .'.</b></td>';

													echo '<td style="white-space:nowrap;">';
													echo '<a href="' . PAGE_SEARCH_TRACKING . '?searchTrackingNumber=' . $ds["Paketnummer"] . '">' . $ds["Paketnummer"] . '</a>';
													echo getPrintProductionPlace($ds["Paketnummer"], "flag");
													echo '</td>';

													echo '<td>';
													echo '<a href="' . PAGE_EDIT_CUSTOMER . '?editCustomerNumber=' . $ds["Ref_Adresse_1"] . '">' . $ds["Ref_Adresse_1"] . '</a>';
													echo '</td>';

													#echo '<td>' . $ds["Produkt_Service"] .'</td>';

													#echo '<td>' . $ds["Gewicht"] .'</td>';

													echo '<td style="white-space:nowrap;">';
													echo $ds["Referenznr_1"];
													if($ds["Referenznr_2"] != ''){
														echo ' / ';
														echo $ds["Referenznr_2"];
													}
													if($ds["Referenznr_3"] != ''){
														echo ' / ';
														echo $ds["Referenznr_3"];
													}
													if($ds["Referenznr_4"] != ''){
														echo ' / ';
														echo $ds["Referenznr_4"];
													}
													echo '</td>';

													echo '<td>' . formatDate($ds["Versanddatum"], 'display') .'</td>';

													echo '<td style="white-space:nowrap;">';
													// $arrDpdScanCodes
													if($ds["SCAN_CODES"] != ''){
														$arrThisScanCodes = explode(",", $ds["SCAN_CODES"]);
														$arrThisScanCodes = array_filter($arrThisScanCodes);
														sort($arrThisScanCodes);
														$thisLastScanCode = implode("", array_slice($arrThisScanCodes, -1, 1, true));
														echo '<span style="font-size:11px;" title="' . $arrDpdScanCodes[$thisLastScanCode] . ' [' . $thisLastScanCode . ']' . '">';
														echo $thisLastScanCode;
														echo '</span>';
													}
													echo '</td>';

													echo '<td style="white-space:nowrap;min-width:250px;">';

													/*
													// echo $imgAttention . ' ';
													$thisDeliveryDate = "";
													$thisDeliveryName = "";
													if(preg_match('/\|/', $ds["Zustellung"])){
														$arrThisZustellung = explode("|", $ds["Zustellung"]);
														$thisDeliveryDate = $arrThisZustellung[0];
														$thisDeliveryName = $arrThisZustellung[1];
													}
													else if(preg_match('/#/', $ds["Zustellung"])){
														$arrThisZustellung = explode("#", $ds["Zustellung"]);
														$thisDeliveryDate = $arrThisZustellung[0];
														$thisDeliveryName = $arrThisZustellung[4];
														$arrTemp = explode(':', $thisDeliveryName);
														$thisDeliveryName = $arrTemp[(count($arrTemp) - 1)];
														$thisDeliveryName = trim($thisDeliveryName);
													}

													if($thisDeliveryDate != ""){
														echo formatDate($thisDeliveryDate, 'display');
														echo ' | ' . $thisDeliveryName;
													}
													*/

													if(preg_match("/#/", $thisDeliveryData)){
														if(!preg_match('/class="loadThisDeliveryData"/', $thisDeliveryData)){
															$pattern = '([0-9]{4})-([0-9]{2})-([0-9]{2}) ([0-9]{2}:[0-9]{2})';
															$replace = '$3.$2.$1 $4 Uhr';
															$thisDeliveryData = preg_replace("/".$pattern."/", $replace, $thisDeliveryData);

															$pattern = '([0-9]{4})-([0-9]{2})-([0-9]{2})';
															$replace = '$3.$2.$1';
															$thisDeliveryData = preg_replace("/".$pattern."/", $replace, $thisDeliveryData);
														}
														echo preg_replace('/#/', ' | ', ($thisDeliveryData));
													}

													echo '</td>';

													echo '<td style="white-space:nowrap;">' . htmlentities(($ds["Firma_Empfaenger"] .' ' . $ds["Name_1"]));
													if($ds["Zu_Haenden"] != '') {
														echo 'z. Hd. ' . htmlentities(($ds["Zu_Haenden"]));
													}
													echo '</td>';

													echo '<td style="white-space:nowrap;">' . $ds["Adresse_1"] .' ' . $ds["Adresse_2"] .'</td>';

													echo '<td>' . $ds["Land_1"] .'</td>';

													#echo '<td>' . $ds["Region_1"] .'</td>';

													echo '<td style="white-space:nowrap;">' . $ds["PLZ_1"] .'</td>';

													echo '<td style="white-space:nowrap;">' . $ds["Stadt_1"] .'</td>';

													echo '<td style="white-space:nowrap;">' . $ds["Produkt_Service"] .'</td>';
													echo '<td style="white-space:nowrap;">' . $ds["COD_Verwendungszweck"] .'</td>';
													echo '<td style="white-space:nowrap;">' . $ds["COD_Betrag"] . ' ' . $ds["COD_Waehrung"] .'</td>';
													echo '<td style="white-space:nowrap;">' . $ds["COD_Inkasoart"] .'</td>';

													echo '<td style="white-space:nowrap;">' . $ds["Benutzername"] .'</td>';
													echo '</tr>';
													$count++;
												}
											?>
											</tbody>
										</table>
										<?php } else { ?>
										<p class="infoArea">Es liegen keine Daten vor!</p>
										<?php } ?>
									</fieldset>
								</div>
							<?php } ?>
							<?php } ?>

							<?php if($_REQUEST["editID"] != "NEW") { ?>
							<?php if(!empty($arrDocumentDatas)) { ?>
							<?php if($_REQUEST["tab"] == "tabs-5") { ?>
								<div id="tabs-5">
									<fieldset style="width:90% !important;">
										<legend>Dokument-Beziehungen</legend>
										<?php
											if(!empty($arrDocumentRelatedNumbers)){
												foreach($arrDocumentRelatedNumbers as $thisArrDocumentNumbers){
													$countRow = 0;

													echo '<div class="displayDocumentsArea" >';

													if($_COOKIE["isAdmin"] == '1'){
														#echo '<div class="clusterize">';
													}

													echo '<table border="0" cellpadding="0" cellspacing="0" style="width:99%;" class="displayOrders">';

													echo '<colgroup>';
													echo '<col />';
													echo '<col />';
													echo '<col />';
													echo '<col />';

													#echo '<col />';
													#echo '<col />';
													echo '<col />';
													echo '<col />';
													echo '<col />';
													echo '<col />';
													echo '</colgroup>';

													#echo '<thead>';
													echo '<tr class="' . $rowClass . '">';
													echo '<th style="width:100px;">Dokument-Nr</th>';
													echo '<th>Dokument-Empf&auml;nger</th>';
													echo '<th style="width:100px;">Dokumentdatum</th>';
													echo '<th style="width:100px;">Auftragsdatum</th>';

													#echo '<th>DB-ID</th>';
													#echo '<th>docType</th>';
													echo '<th style="width:120px;">Summe</th>';
													echo '<th style="width:120px;">Zahl-Status</th>';
													echo '<th style="width:20px;">PDF</th>';
													echo '<th style="width:160px;">Zugeh&ouml;rige Produktionen</th>';
													echo '</tr>';
													#echo '</thead>';


													$thisDocumentComment = '';
													#echo '<tbody class="clusterize-content">';
													foreach($thisArrDocumentNumbers as $thisDocumentKey => $thisDocumentNumbers){
														$isSplittedDelivery = false;
														if(preg_match("/;/", $thisDocumentNumbers["LS"])) {
															$isSplittedDelivery = true;
															$arrTemp = explode(";", $thisDocumentNumbers["LS"]);
															$thisDocumentNumbers["LS"] = '';
															$countTemp = 1;
															foreach($arrTemp as $thisTempValue){
																$thisDocumentNumbers["LS".$countTemp] = $thisTempValue;
																$countTemp++;
															}
														}
														if($arrConnectedDocumentIds[$thisDocumentNumbers[0]]["documentsType"] != 'BR' && count($thisDocumentNumbers) > 0 ){
															foreach($thisDocumentNumbers as $thisDocumentNumber){
																#dd('arrConnectedDocumentIds');

																if(!empty($arrConnectedDocumentIds[$thisDocumentNumber])){
																	if($countRow%2 == 0){ $rowClass = 'row1'; }
																	else { $rowClass = 'row4'; }
																	$rowClass2 = '';
																	if($arrConnectedDocumentIds[$thisDocumentNumber]["documentsStatus"] == "1") {
																		$rowClass2 = 'row2';
																	}
																	else if($arrConnectedDocumentIds[$thisDocumentNumber]["documentsStatus"] == "2") {
																		$rowClass2 = 'row3';
																	}
																	else if($arrConnectedDocumentIds[$thisDocumentNumber]["documentsStatus"] == "3") {
																		$rowClass2 = 'row6';
																		$rowClass = 'row6';
																	}
																	else if($arrConnectedDocumentIds[$thisDocumentNumber]["documentsStatus"] == "4") {
																		$rowClass2 = 'row2';
																	}
																	else if($arrConnectedDocumentIds[$thisDocumentNumber]["documentsStatus"] == "6") {
																		$rowClass2 = 'row6';
																	}
																	else if($arrConnectedDocumentIds[$thisDocumentNumber]["documentsStatus"] == "6") {
																		$rowClass2 = 'row7';
																	}
																	else if($arrConnectedDocumentIds[$thisDocumentNumber]["documentsStatus"] == "10") {
																		$rowClass2 = 'row3';
																	}
																	else if($arrConnectedDocumentIds[$thisDocumentNumber]["documentsStatus"] == "5") {
																		// $rowClass2 = 'row5';
																		$rowClass2 = 'row3';
																	}

																	if($thisDocumentKey == 'AN'){
																		$rowClass2 = '';
																	}

																	if($thisDocumentComment == ''){
																		$thisDocumentComment = getRelatedDocumentsComment(array($thisDocumentNumber), '');
																		if($thisDocumentComment != ''){
																			echo '<tr class="' . $rowClass . '">';
																			echo '<td colspan="8">';
																			echo '<p class="infoArea" style="margin:0;padding:1px;border-width:1px;">'. $thisDocumentComment . '</p>';
																			echo '</td>';
																			echo '</tr>';
																		}
																	}

																	echo '<tr class="' . $rowClass . '">';
																	echo '<td>';

																	$thisDocumentsPage = PAGE_ALL_INVOICES;
																	if($arrConnectedDocumentIds[$thisDocumentNumber]["documentsType"] == 'LS'){
																		$thisDocumentsPage = PAGE_DISPLAY_DELIVERY;
																	}
																	else if($arrConnectedDocumentIds[$thisDocumentNumber]["documentsType"] == 'AN'){
																		$thisDocumentsPage = PAGE_DISPLAY_OFFER;
																	}
																	else if($arrConnectedDocumentIds[$thisDocumentNumber]["documentsType"] == 'RE'){
																		$thisDocumentsPage = PAGE_ALL_INVOICES;
																	}
																	else if($arrConnectedDocumentIds[$thisDocumentNumber]["documentsType"] == 'AB'){
																		$thisDocumentsPage = PAGE_ALL_CONFIRMATIONS;
																	}
																	else if($arrConnectedDocumentIds[$thisDocumentNumber]["documentsType"] == 'GU'){
																		$thisDocumentsPage = PAGE_ALL_CREDITS;
																	}
																	echo '<a href="' . $thisDocumentsPage . '?searchDocumentNumber=' . $thisDocumentNumber . '">';
																	echo $thisDocumentNumber;
																	if($isSplittedDelivery && $arrConnectedDocumentIds[$thisDocumentNumber]["documentsType"] == 'LS'){ echo ' (<b>Teillieferung</b>)';}
																	echo '</a>';
																	echo '</td>';

																	echo '<td>';
																	if($arrConnectedDocumentIds[$thisDocumentNumber]["documentsIsCollectiveInvoice"] == '1'){
																		echo '<div style="margin:0 0 -3px 0;padding:0;color:#FF0000;font-weight:bold;font-size:10px;">';
																		if($arrConnectedDocumentIds[$thisDocumentNumber]["documentsType"] == 'AB'){ echo 'Die AB ist Teil einer SAMMELRECHNUNG' . ' (' . implode(';', $thisArrDocumentNumbers["RE"]) . ')'; }
																		else { echo 'SAMMELRECHNUNG'; }
																		echo '</div>';
																		#dd('thisDocumentNumbers');
																	}
																	echo $arrConnectedDocumentIds[$thisDocumentNumber]["documentsAddressCompany"];
																	if($arrConnectedDocumentIds[$thisDocumentNumber]["documentsKommission"] != ""){
																		echo '<br /><span class="remarksArea">';
																		echo '<b>Komission</b>:' . $arrConnectedDocumentIds[$thisDocumentNumber]["documentsKommission"];
																		echo '</span>';
																	}
																	echo '</td>';

																	echo '<td>';
																	echo formatDate($arrConnectedDocumentIds[$thisDocumentNumber]["documentsDocumentDate"], 'display');
																	echo '</td>';

																	echo '<td>';
																	echo formatDate($arrConnectedDocumentIds[$thisDocumentNumber]["documentsOrderDate"], 'display');
																	echo '</td>';

																	#echo '<td>';
																	#echo $arrConnectedDocumentIds[$thisDocumentNumber]["documentID"];
																	#echo '</td>';
																	#echo '<td>';
																	#echo $arrConnectedDocumentIds[$thisDocumentNumber]["documentsType"];
																	#echo '</td>';

																	echo '<td style="text-align:right;">';

																	if(in_array($arrConnectedDocumentIds[$thisDocumentNumber]["documentsType"], array('M1', 'M2'))){
																		echo number_format(($arrConnectedDocumentIds[$thisDocumentNumber]["documentsTotalPrice"] + $arrConnectedDocumentIds[$thisDocumentNumber]["documentsChargesPrice"] + $arrConnectedDocumentIds[$thisDocumentNumber]["documentsInterestPrice"]), 2, ",", ".");
																		#echo number_format($arrConnectedDocumentIds[$thisDocumentNumber]["orderDocumentsInterestPercent"], 2, ",", ".");
																	}
																	else {
																		echo number_format($arrConnectedDocumentIds[$thisDocumentNumber]["documentsTotalPrice"], 2, ",", ".");
																	}
																	echo '</td>';

																	echo '<td class="' . $rowClass2 . '">';
																	#if($arrConnectedDocumentIds[$thisDocumentNumber]["documentsType"] == 'AB' || $arrConnectedDocumentIds[$thisDocumentNumber]["documentsType"] == 'RE'){
																	#if(in_array($arrConnectedDocumentIds[$thisDocumentNumber]["documentsType"], array('AB', 'RE')){
																	if($thisDocumentKey != 'AN'){
																		echo $arrPaymentStatusTypeDatas[$arrConnectedDocumentIds[$thisDocumentNumber]["documentsStatus"]]["paymentStatusTypesName"];
																	}
																	else {
																		echo ' - ';
																	}
																	echo '</td>';

																	echo '<td>';
																	echo '<span class="toolItem">';
																	#echo '<a href="' . $_SERVER["PHP_SELF"] . '?editCustomerNumber=' . $thisDocumentValue[$thisKey]["orderDocumentsCustomerNumber"] . '&amp;editID=' . $_REQUEST["editID"] . '&amp;downloadFile=' . urlencode(basename($thisDocumentNumber . '_KNR-' . $thisDocumentValue[$thisKey]["orderDocumentsCustomerNumber"] . '.pdf')) . '&amp;documentType=' . $arrConnectedDocumentIds[$thisDocumentNumber]["documentsType"] . '">' . '<img src="layout/icons/icon' . getFileType(basename($thisDocumentValue[$thisKey]["orderDocumentsDocumentPath"])) . '.gif" width="16" height="16" title="Dokument herunterladen" alt="Download" /></a>';
																	#echo '<a href="' . $_SERVER["PHP_SELF"] . '?editCustomerNumber=' . $customerDatas["customersKundennummer"] . '&amp;editID=' . $_REQUEST["editID"] . '&amp;downloadFile=' . urlencode(basename($thisDocumentNumber . '_KNR-' . $thisDocumentValue[$thisKey]["orderDocumentsCustomerNumber"] . '.pdf')) . '&amp;documentType=' . $arrConnectedDocumentIds[$thisDocumentNumber]["documentsType"] . '">' . '<img src="layout/icons/icon' . getFileType(basename($thisDocumentValue[$thisKey]["orderDocumentsDocumentPath"])) . '.gif" width="16" height="16" title="Dokument herunterladen" alt="Download" /></a>';
																	echo '<a href="' . $_SERVER["PHP_SELF"] . '?editCustomerNumber=' . $customerDatas["customersKundennummer"] . '&amp;editID=' . $_REQUEST["editID"] . '&amp;downloadFile=' . urlencode(basename($arrConnectedDocumentIds[$thisDocumentNumber]["documentsFilename"])) . '&amp;documentType=' . $arrConnectedDocumentIds[$thisDocumentNumber]["documentsType"] . '">' . '<img src="layout/icons/icon' . getFileType(basename(basename($arrConnectedDocumentIds[$thisDocumentNumber]["documentsFilename"]))) . '.gif" width="16" height="16" title="Dokument herunterladen" alt="Download" /></a>';
																	echo '</span>';
																	echo '</td>';

																	echo '<td>';
																	$useRow = 0;
																	if($arrConnectedDocumentIds[$thisDocumentNumber]["documentsIsCollectiveInvoice"] == '1' && $thisDocumentKey == 'RE'){
																		$useRow = 1;
																	}

																	$showProductionIds = false;
																	if($arrConnectedDocumentIds[$thisDocumentNumber]["documentsIsCollectiveInvoice"] == '1' && $countRow >= $useRow){
																		$showProductionIds = true;
																	}
																	else if($arrConnectedDocumentIds[$thisDocumentNumber]["documentsIsCollectiveInvoice"] != '1' && $countRow == $useRow){
																		$showProductionIds = true;
																	}

																	#if($countRow >= $useRow){
																	if($showProductionIds){
																		if($arrConnectedDocumentIds[$thisDocumentNumber]["documentsIsCollectiveInvoice"] == '1'){
																			#echo '<span style="color:#FF0000;font-weight:bold;font-size:10px;">SAMMELRECHNUNG</span>';
																		}
																		if(1){
																			#echo $arrConnectedDocumentIds[$thisDocumentNumber]["productionIDs"];
																			echo '<span class="toolItem">';
																			if($thisDocumentKey != "AN"){
																				#$arrProductionIDs = explode("#", $thisProductionID);
																				$arrProductionIDs = explode(";", $arrConnectedDocumentIds[$thisDocumentNumber]["productionIDs"] );
																				$arrProductionIDs = array_unique($arrProductionIDs);
																				if(!empty($arrProductionIDs)) {
																					foreach($arrProductionIDs as $thisValue) {
																						if($thisValue > 0) {
																							echo '<span style="white-space:nowrap;"><a href="' . PAGE_EDIT_PROCESS . '?editID=' . $thisValue . '"><img src="layout/icons/iconEdit.gif" width="16" height="16" title="Produktion bearbeiten (Datensatz ' . $thisValue . ')" alt="Bearbeiten" /></a></span>';
																						}
																					}
																				}
																			}
																			echo '</span>';
																		}
																	}
																	echo '</td>';

																	echo '</tr>';
																	$countRow++;
																}
															}
														}
													}
													#echo '</tbody>';
													echo '</table>';
													if($_COOKIE["isAdmin"] == '1'){
														#echo '</div>';
													}
													echo '</div>';
													echo '<hr />';
												}
											}
											else {
												$infoMessage .= 'Es sind keine Dokumente vorhanden.' . '<br />';
												displayMessages();
											}
										?>
									</fieldset>
								</div>
							<?php } ?>
							<?php } ?>
							<?php } ?>

							<?php if($_REQUEST["editID"] != "NEW") { ?>
							<?php if($_REQUEST["tab"] == "tabs-8") { ?>
							<?php if(in_array($customerDatas["customersTyp"], array('2', '4'))) { ?>
									<div id="tabs-8">
										<fieldset>
											<legend>Provisionen</legend>
							<?php
										$sql_xxx = "SELECT
													`salesmenProvisionsDocumentNumber`,

													`salesmenProvisionsID`,
													`salesmenProvisionsSalesmanID`,
													`salesmenProvisionsDocumentDate`,
													`salesmenProvisionsSum`,
													`salesmenProvisionsMwst`,
													`salesmenProvisionsMwstValue`,
													`salesmenProvisionsTotal`,
													`salesmenProvisionsStatus`,
													`salesmenProvisionsDocumentPath`,
													`salesmenProvisionsContentDocumentNumber`,
													`createdDocumentsID`,
													`createdDocumentsType`,
													`createdDocumentsNumber`,
													`createdDocumentsCustomerNumber`,
													`createdDocumentsTitle`,
													`createdDocumentsFilename`,
													`createdDocumentsContent`,
													`createdDocumentsUserID`,
													`createdDocumentsTimeCreated`
													FROM `" . TABLE_SALESMEN_PROVISIONS . "`

													LEFT JOIN `" . TABLE_CREATED_DOCUMENTS . "`
													ON(`" . TABLE_SALESMEN_PROVISIONS . "`.`salesmenProvisionsDocumentNumber` = `" . TABLE_CREATED_DOCUMENTS . "`.`createdDocumentsNumber`)

													WHERE `" . TABLE_SALESMEN_PROVISIONS . "`.`salesmenProvisionsSalesmanID` = " . $customerDatas["customersID"] . "

													GROUP BY `" . TABLE_SALESMEN_PROVISIONS . "`.`salesmenProvisionsDocumentNumber`

													HAVING `" . TABLE_CREATED_DOCUMENTS . "`.`createdDocumentsID` IS NOT NULL

													ORDER BY `" . TABLE_SALESMEN_PROVISIONS . "`.`salesmenProvisionsDocumentNumber` DESC, `" . TABLE_SALESMEN_PROVISIONS . "`.`salesmenProvisionsDocumentDate` DESC
											";

										$sql = "SELECT
													`" . TABLE_SALESMEN_PROVISIONS . "`.`salesmenProvisionsID`,
													`" . TABLE_SALESMEN_PROVISIONS . "`.`salesmenProvisionsDocumentNumber`,
													`" . TABLE_SALESMEN_PROVISIONS . "`.`salesmenProvisionsSalesmanID`,
													`" . TABLE_SALESMEN_PROVISIONS . "`.`salesmenProvisionsDocumentDate`,
													`" . TABLE_SALESMEN_PROVISIONS . "`.`salesmenProvisionsSum`,
													`" . TABLE_SALESMEN_PROVISIONS . "`.`salesmenProvisionsMwst`,
													`" . TABLE_SALESMEN_PROVISIONS . "`.`salesmenProvisionsMwstValue`,
													`" . TABLE_SALESMEN_PROVISIONS . "`.`salesmenProvisionsTotal`,
													`" . TABLE_SALESMEN_PROVISIONS . "`.`salesmenProvisionsStatus`,
													`" . TABLE_SALESMEN_PROVISIONS . "`.`salesmenProvisionsDocumentPath`,
													`" . TABLE_SALESMEN_PROVISIONS . "`.`salesmenProvisionsContentDocumentNumber`,

													`" . TABLE_CREATED_DOCUMENTS . "`.`createdDocumentsID`,
													`" . TABLE_CREATED_DOCUMENTS . "`.`createdDocumentsType`,
													`" . TABLE_CREATED_DOCUMENTS . "`.`createdDocumentsNumber`,
													`" . TABLE_CREATED_DOCUMENTS . "`.`createdDocumentsCustomerNumber`,
													`" . TABLE_CREATED_DOCUMENTS . "`.`createdDocumentsTitle`,
													`" . TABLE_CREATED_DOCUMENTS . "`.`createdDocumentsFilename`,
													`" . TABLE_CREATED_DOCUMENTS . "`.`createdDocumentsContent`,
													`" . TABLE_CREATED_DOCUMENTS . "`.`createdDocumentsUserID`,
													`" . TABLE_CREATED_DOCUMENTS . "`.`createdDocumentsTimeCreated`

													FROM `" . TABLE_SALESMEN_PROVISIONS . "`

													LEFT JOIN `" . TABLE_CREATED_DOCUMENTS . "`
													ON(`" . TABLE_SALESMEN_PROVISIONS . "`.`salesmenProvisionsDocumentNumber` = `" . TABLE_CREATED_DOCUMENTS . "`.`createdDocumentsNumber`)

													WHERE 1
														AND `" . TABLE_SALESMEN_PROVISIONS . "`.`salesmenProvisionsSalesmanID` = " . $customerDatas["customersID"] . "

													/* GROUP BY `" . TABLE_SALESMEN_PROVISIONS . "`.`salesmenProvisionsDocumentNumber` */

													HAVING `" . TABLE_CREATED_DOCUMENTS . "`.`createdDocumentsID` IS NOT NULL

													/*ORDER BY `" . TABLE_SALESMEN_PROVISIONS . "`.`salesmenProvisionsDocumentNumber` DESC, `" . TABLE_CREATED_DOCUMENTS . "`.`createdDocumentsTimeCreated` DESC */
													ORDER BY `" . TABLE_SALESMEN_PROVISIONS . "`.`salesmenProvisionsDocumentNumber` DESC, `" . TABLE_SALESMEN_PROVISIONS . "`.`salesmenProvisionsDocumentDate` DESC
											";

										$rs = $dbConnection->db_query($sql);

										$count = 0;

										echo '<table border="1" cellpadding="0" cellspacing="0" width="0" class="displayOrders">';

												echo '<colgroup>';
												echo '<col />';
												echo '<col />';
												echo '<col />';
												echo '<col />';
												echo '<col />';
												echo '<col />';
												echo '<col />';
												echo '</colgroup>';

												echo '<tr>';
												echo '<th style="width:45px;">#</th>';
												echo '<th>Dokument-Nr</th>';
												echo '<th>Datum</th>';
												echo '<th>Summe</th>';
												echo '<th>MwSt.</th>';
												echo '<th>Gesamtbetrag</th>';
												echo '<th style="width:70px;">Info</th>';
												echo '</tr>';

										$thisMarker = '';
										while($ds = mysqli_fetch_assoc($rs)) {
											if($count%2 == 0){ $rowClass = 'row0'; }
											else { $rowClass = 'row1'; }

											if($thisMarker != $ds["salesmenProvisionsDocumentNumber"]) {
												$thisMarker = $ds["salesmenProvisionsDocumentNumber"];
												echo '<tr class="'.$rowClass.'">';
												echo '<td style="text-align:right;"><b>' . ($count + 1) .'.</b></td>';
												echo '<td>';
												echo $ds["salesmenProvisionsDocumentNumber"];
												echo '</td>';
												echo '<td>';
												echo substr(formatDate($ds["createdDocumentsTimeCreated"], 'display'), 0, 10);
												echo '</td>';
												echo '<td style="text-align:right;">';
												echo number_format($ds["salesmenProvisionsSum"], 2, ',', '') . '&euro;';
												echo '</td>';
												echo '<td style="text-align:right;">';
												echo number_format($ds["salesmenProvisionsMwstValue"], 2, ',', '') . '&euro;' . ' (' . number_format($ds["salesmenProvisionsMwst"], 2, ',', '') . '%)';
												echo '</td>';
												echo '<td style="text-align:right;">';
												echo number_format($ds["salesmenProvisionsTotal"], 2, ',', '') . '&euro;';
												echo '</td>';
												echo '<td>';
												echo '<span class="toolItem">';
												echo '<a href="' . $_SERVER["PHP_SELF"] . '?editCustomerNumber=' . $customerDatas["customersKundennummer"] . '&amp;editID=' . $customerDatas["customersID"] . '&amp;downloadFile=' . urlencode(basename($ds["createdDocumentsFilename"])) . '&amp;documentType=PR">' . '<img src="layout/icons/icon' . getFileType(basename($ds["createdDocumentsFilename"])) . '.gif" width="16" height="16" title="Dokument herunterladen" alt="Download" /></a>';
												echo '</span>';
												echo '<span class="toolItem">';
												echo '<img src="layout/icons/mailAttachement.png" class="buttonSendDocument" data_mail="" rel="' . urlencode(basename($ds["createdDocumentsFilename"])) . '#' . 'PR' . '" width="16" height="16" title="Dokument direkt per Mail versenden" alt="Dokument versenden" />';
												echo '</span>';
												echo '</td>';
												echo '</tr>';
												$count++;
											}
										}
										echo '</table>';
							?>
									</fieldset>
								</div>
							<?php } ?>
							<?php } ?>

							<?php if($_REQUEST["tab"] == "tabs-10") { ?>
							<div id="tabs-10">
								<?php
									$sql = "
										SELECT
											`customersCalendarID`,
											`customersCalendarCustomersID`,
											`customersCalendarCustomersNumber`,
											`customersCalendarDate`,
											`customersCalendarDateRepeat`,
											`customersCalendarRepeatType`,
											`customersCalendarRepeatValue`,
											`customersCalendarDescription`,
											`customersCalendarReminderMailSended`,
											`customersCalendarUserID`

											FROM `" . TABLE_CUSTOMERS_CALENDAR . "`
											WHERE 1
												AND `customersCalendarCustomersID` = '" . $_REQUEST["editID"] . "'

												ORDER BY `customersCalendarDate` DESC
										";

										$rs = $dbConnection->db_query($sql);
										$countItems = $dbConnection->db_getMysqlNumRows($rs);
								?>
								<form name="formAddCustomerCalendarDate" method="post" action="<?php echo $_SERVER["PHP_SELF"]; ?>#tabs-10" enctype="multipart/form-data" >
									<?php if($countItems > 0){ ?>
									<fieldset>
										<legend>Termin-Erinnerungen</legend>
										<table border="0" width="100%" cellspacing="0" cellpadding="0" class="displayOrders">
											<tr>
												<th style="width:45px;">#</th>
												<th style="width:100px;">Termin</th>
												<th>Beschreibung</th>
												<th style="width:45px;">Info</th>
											</tr>
											<?php
												$count = 0;
												while($ds = mysqli_fetch_assoc($rs)){
													if($count%2 == 0){ $rowClass = 'row0'; }
													else { $rowClass = 'row1'; }

													if($ds["customersCalendarDate"]){

													}

													echo '<tr ' . $thisStyle . ' class="'.$rowClass.'">';
													echo '<td style="text-align:right;">';
													echo '<b>' . ($count + 1) . '.</b>';
													echo '</td>';
													echo '<td>';
													echo formatDate($ds["customersCalendarDate"], 'display');
													echo '</td>';
													echo '<td>';
													echo $ds["customersCalendarDescription"];
													echo '</td>';
													echo '<td>';
													echo '<a href="' . $_SERVER["PHP_SELF"]. '?editID=' . $_REQUEST["editID"] . '&amp;deleteCustomerCalendarID=' . $ds["customersCalendarID"] . '&amp;tab=tabs-10#tabs-10" onclick="return showWarning(\'Soll der Termin wirklich entfernt werden?\');" ><img src="layout/icons/iconDelete.png" width="16" height="16" alt="Termin entfernen" title="Diesen Termin entfernen" /></a>';
													echo '</td>';
													echo '</tr>';
													$count++;
												}
											?>
											</table>
										</fieldset>
										<?php } ?>
										<hr />
										<fieldset>
											<legend>Neue Termin-Erinnerung</legend>
											<table border="0" width="100%" cellspacing="0" cellpadding="0" class="displayOrders">
												<tr>
													<th style="width:45px;">#</th>
													<th style="width:100px;">Termin</th>
													<th>Beschreibung</th>
													<th style="width:45px;">Intervall</th>
												</tr>
											<?php
												for($i = 0 ; $i < 1 ; $i++){
													if($i%2 == 0){ $rowClass = 'row0'; }
													else { $rowClass = 'row1'; }
													echo '<tr ' . $thisStyle . ' class="'.$rowClass.'">';
													echo '<td style="text-align:right;">';
													echo '<b>' . ($i + 1) . '.</b>';
													echo '<input type="hidden" name="addCustomerCalendarDates[' . $i . '][ID]" value="' . $i . '" />';
													echo '<input type="hidden" name="addCustomerCalendarDates[' . $i . '][CUSTOMERID]" value="' . $_REQUEST["editID"] . '" />';
													echo '<input type="hidden" name="addCustomerCalendarDates[' . $i . '][CUSTOMERNUMBER]" value="' . $customerDatas["customersKundennummer"] . '" />';
													echo '</td>';
													echo '<td>';
													echo '<input type="text" name="addCustomerCalendarDates[' . $i . '][DATE]" id="addCustomerCalendarDates" class="inputField_70" value="' . $_POST["addCustomerCalendarDates"][0]["DATE"] . '" />';
													echo '</td>';
													echo '<td>';
													echo '<input type="text" name="addCustomerCalendarDates[' . $i . '][DESCRIPTION]" class="inputField_600" value="' . $_POST["addCustomerCalendarDates"][0]["DESCRIPTION"] . '" />';
													echo '</td>';
													echo '<td>';
													echo '<select name="addCustomerCalendarDatesInterval" class="inputSelect_130">';
													echo '<option value="" selected="selected">Einmaliger Termin</option>';
													for($i = 1 ; $i < 7 ; $i++){
														echo '<option value="' . $i . '">alle ' . $i . ' Monate</option>';
													}
													echo '</select>';
													echo '</td>';
													echo '</tr>';
												}
											?>
										</table>
										<input type="hidden" name="editID" value="<?php echo $_REQUEST["editID"]; ?>" />
										<input type="hidden" name="tab" value="<?php echo $_REQUEST["tab"]; ?>" />
									</fieldset>

									<div class="actionButtonsArea">
										<input type="submit" class="inputButton1 inputButtonGreen" name="storeCalendar" value="Termin speichern" />
										&nbsp;
										<input type="submit" class="inputButton1 inputButtonOrange" name="resetCalendar" value="Abbrechen" onclick="return showWarning(' Wollen Sie diesen Vorgang wirklich abbrechen??? ');" />
									</div>
								</form>
							</div>
							<?php } ?>

							<?php if($_REQUEST["tab"] == "tabs-9") { ?>
							<div id="tabs-9">
								<?php
									$sql = "
										SELECT
											`customersContactsID`,
											`customersContactsContactID`,
											`customersContactsSalutation`,
											`customersContactsFirstName`,
											`customersContactsSecondName`,
											`customersContactsFunction`,
											`customersContactsPhone`,
											`customersContactsMobile`,
											`customersContactsFax`,
											`customersContactsMail`,
											`customersContactsActive`

											FROM `" . TABLE_CUSTOMERS_CONTACTS. "`

											WHERE 1
												AND `customersContactsContactID` = " . $_REQUEST["editID"] . "

												ORDER BY `customersContactsSecondName`
									";

									$rs = $dbConnection->db_query($sql);
									$countItems = $dbConnection->db_getMysqlNumRows($rs);
								?>
								<form name="formAddContacts" method="post" action="<?php echo $_SERVER["PHP_SELF"]; ?>#tabs-9" enctype="multipart/form-data" >
									<?php if($countItems > 0){ ?>
									<fieldset>
										<legend>Kunden-Kontakte</legend>
										<table border="0" width="100%" cellspacing="0" cellpadding="0" class="displayOrders">
											<tr>
												<th>#</th>
												<th>Anrede</th>
												<th>Vorname</th>
												<th>Nachname</th>
												<th>Funktion</th>
												<th>Telefon</th>
												<th>Fax</th>
												<th>Mobil</th>
												<th>E-Mail</th>
												<th>Info</th>
											</tr>
											<?php
												$count = 0;
												while($ds = mysqli_fetch_assoc($rs)){
													if($count%2 == 0){ $rowClass = 'row0'; }
													else { $rowClass = 'row1'; }

										echo '<tr ' . $thisStyle . ' class="'.$rowClass.'">';
										echo '<td style="text-align:right;">';
										echo '<b>' . ($count + 1) . '.</b>';
										echo '<input type="hidden" name="editCustomerContacts[' . $ds["customersContactsID"] . '][ID]" class="" value="' . $ds["customersContactsID"] . '" />';
										echo '<input type="hidden" name="editCustomerContacts[' . $ds["customersContactsID"] . '][ContactID]" class="" value="' . $ds["customersContactsContactID"] . '" />';
										// $_REQUEST["editID"]
										echo '</td>';
										echo '<td style="white-space:nowrap;">';
										echo '<select name="editCustomerContacts[' . $ds["customersContactsID"] . '][Salutation]" class="inputSelect_70">';
										echo '<option value=""> - </option>';
											if(!empty($arrSalutationTypeDatas)) {
												foreach($arrSalutationTypeDatas as $thisKey => $thisValue) {
													$selected = '';
													if($thisKey == $ds["customersContactsSalutation"]) {
														$selected = ' selected="selected" ';
													}
													echo '<option value="' . $thisKey . '" '.$selected.' >' . ($arrSalutationTypeDatas[$thisKey]["salutationTypesName"]). '</option>';
												}
											}
										echo '</select> - ';
										echo '</td>';
										echo '<td>';
										echo '<input type="text" name="editCustomerContacts[' . $ds["customersContactsID"] . '][FirstName]" class="inputField_160" value="' . $ds["customersContactsFirstName"] . '" />';
										echo '</td>';
										echo '<td>';
										echo '<input type="text" name="editCustomerContacts[' . $ds["customersContactsID"] . '][SecondName]" class="inputField_160" value="' . $ds["customersContactsSecondName"] . '" />';
										echo '</td>';
										echo '<td>';
										echo '<select name="editCustomerContacts[' . $ds["customersContactsID"] . '][Function]" class="inputSelect_100">';
										echo '<option value=""> - </option>';
																if(!empty($arrCustomerContactTypeDatas)) {
																foreach($arrCustomerContactTypeDatas as $thisKey => $thisValue) {
																	$selected = '';
																	if($thisKey == $ds["customersContactsFunction"]) {
																		$selected = ' selected="selected" ';
																	}
																	echo '<option value="' . $thisKey . '" '.$selected.' >' . ($arrCustomerContactTypeDatas[$thisKey]["contactTypeName"]). '</option>';
																}
															}
													echo '</select>';
													echo '</td>';
													echo '<td><input type="text" name="editCustomerContacts[' . $ds["customersContactsID"] . '][Phone]" class="inputField_160" value="' . $ds["customersContactsPhone"]. '" /></td>';
													echo '<td><input type="text" name="editCustomerContacts[' . $ds["customersContactsID"] . '][Fax]" class="inputField_160" value="' . $ds["customersContactsFax"]. '" /></td>';
													echo '<td><input type="text" name="editCustomerContacts[' . $ds["customersContactsID"] . '][Mobile]" class="inputField_160" value="' . $ds["customersContactsMobile"]. '" /></td>';
													echo '<td style="white-space:nowrap;">';
													echo '<input type="text" name="editCustomerContacts[' . $ds["customersContactsID"] . '][Mail]" class="inputField_160" value="' . $ds["customersContactsMail"]. '" />';
													if($ds["customersContactsMail"] != ""){
														echo ' <a href="mailto:' . $ds["customersContactsMail"] . '" ><img src="layout/icons/iconMail.gif" width="16" height="14" alt="Mail senden" title="Diesen Kontakt per Mail kontaktieren" /></a>';
													}
													else {
														echo ' <img src="layout/icons/spacer.gif" width="16" height="14" alt="" title="" />';
													}
													echo '</td>';
													echo '<td>';
													echo '<a href="' . $_SERVER["PHP_SELF"]. '?editID=' . $_REQUEST["editID"] . '&amp;deleteContactsID=' . $ds["customersContactsID"] . '#tabs-9" onclick="return showWarning(\'Soll der Kontakt wirklich entfernt werden?\');" ><img src="layout/icons/iconDelete.png" width="16" height="16" alt="Kontakt entfernen" title="Diesen Kontakt entfernen" /></a>';
													echo '</td>';
													echo '</tr>';

													$count++;
												}
											?>
										</table>
									</fieldset>
									<?php } ?>
									<hr />
									<fieldset>
										<legend>Neue Kontakte hinzuf&uuml;gen</legend>
										<table border="0" width="100%" cellspacing="0" cellpadding="0" class="displayOrders">
											<tr>
												<th>#</th>
												<th>Anrede</th>
												<th>Vorname</th>
												<th>Nachname</th>
												<th>T&auml;tigkeit</th>
												<th>Telefon</th>
												<th>Fax</th>
												<th>Mobil</th>
												<th>E-Mail</th>
												<th>Info</th>
											</tr>
											<?php
												$count = 0;
												for($i = 0; $i < 2 ; $i++){
													if($count%2 == 0){ $rowClass = 'row0'; }
													else { $rowClass = 'row1'; }

													echo '<tr ' . $thisStyle . ' class="'.$rowClass.'">';
													echo '<td style="text-align:right;">';
													echo '<b>' . ($count + 1) . '.</b>';
													echo '<input type="hidden" name="addCustomerContacts['.$i.'][ID]" class="" value="' . $i . '" />';
													echo '<input type="hidden" name="addCustomerContacts['.$i.'][ContactID]" class="" value="' . $_REQUEST["editID"] . '" />';
													// $_REQUEST["editID"]
													echo '</td>';
													echo '<td style="white-space:nowrap;">';
													echo '<select name="addCustomerContacts['.$i.'][Salutation]" class="inputSelect_70">';
													echo '<option value=""> - </option>';

														if(!empty($arrSalutationTypeDatas)) {
															foreach($arrSalutationTypeDatas as $thisKey => $thisValue) {
																echo '<option value="' . $thisKey . '" >' . ($arrSalutationTypeDatas[$thisKey]["salutationTypesName"]). '</option>';
															}
														}
													echo '</select> - ';
													echo '</td>';
													echo '<td>';
													echo '<input type="text" name="addCustomerContacts['.$i.'][FirstName]" class="inputField_160" value="" />';
													echo '</td>';
													echo '<td>';
													echo '<input type="text" name="addCustomerContacts['.$i.'][SecondName]" class="inputField_160" value="" />';
													echo '</td>';
													echo '<td>';
													echo '<select name="addCustomerContacts['.$i.'][Function]" class="inputSelect_100">';
													echo '<option value=""> - </option>';

															if(!empty($arrCustomerContactTypeDatas)) {
																foreach($arrCustomerContactTypeDatas as $thisKey => $thisValue) {
																	echo '<option value="' . $thisKey . '" >' . ($arrCustomerContactTypeDatas[$thisKey]["contactTypeName"]). '</option>';
																}
															}

													echo '</select>';
													echo '</td>';
													echo '<td><input type="text" name="addCustomerContacts['.$i.'][Phone]" class="inputField_160" value="" /></td>';
													echo '<td><input type="text" name="addCustomerContacts['.$i.'][Fax]" class="inputField_160" value="" /></td>';
													echo '<td><input type="text" name="addCustomerContacts['.$i.'][Mobile]" class="inputField_160" value="" /></td>';
													echo '<td style="white-space:nowrap;">';
													echo '<input type="text" name="addCustomerContacts['.$i.'][Mail]" class="inputField_160" value="" />';
													echo ' <img src="layout/icons/spacer.gif" width="16" height="14" alt="" title="" />';
													echo '</td>';
													echo '<td>

													</td>';
													echo '</tr>';

													$count++;
												}
											?>
										</table>
										<input type="hidden" name="editCustomersKundennummer3" value="<?php echo $customerDatas["customersKundennummer"]; ?>" />
										<input type="hidden" name="editID" value="<?php echo $_REQUEST["editID"]; ?>" />
									</fieldset>

									<div class="actionButtonsArea">
										<input type="submit" class="inputButton1 inputButtonGreen" name="storeContacts" value="Kontakt speichern" />
										&nbsp;
										<input type="submit" class="inputButton1 inputButtonOrange" name="resetContacts" value="Abbrechen" onclick="return showWarning(' Wollen Sie diesen Vorgang wirklich abbrechen??? ');" />
									</div>
									<input type="hidden" name="tab" value="<?php echo $_REQUEST["tab"]; ?>" />
								</form>
							</div>
							<?php } ?>
							<?php } ?>
						</div>
					</div>
					<?php
						}
					?>

				</div>
			</div>
		</div>
		<div class="clear"></div>
	</div>
</div>

<script language="javascript" type="text/javascript">
//function myFunction() {
		  //var person = prompt("Please enter your text", "");
		  // if (person != null) {
		  //   document.getElementById("demo").innerHTML =
		  //   "Hello " + person + "! How are you today?";
		  // }
	//	}



	<!--
	/* <![CDATA[ */
	$(document).ready(function() {
		<?php if(isset($_GET["tab"]) && $_GET["tab"]=="tabs-1"){ ?>
		myFunction()
		<?php } ?>
		$('#searchCustomerNumber').keyup(function () {
			loadSuggestions('searchCustomerNumber', [{'triggerElement': '#searchCustomerNumber', 'fieldNumber': '#searchCustomerNumber', 'fieldName': '#searchCustomerName', 'fieldCity': '', 'fieldZipCode': ''}], 1);
		});
		$('#searchCustomerName').keyup(function () {
			loadSuggestions('searchCustomerName', [{'triggerElement': '#searchCustomerName', 'fieldNumber': '#searchCustomerNumber', 'fieldName': '#searchCustomerName', 'fieldCity': '', 'fieldZipCode': ''}], 1);
		});

		$('#editCustomersFirmenadressePLZ').keyup(function () {
			loadSuggestions('searchPlzCity', [{'triggerElement': '#editCustomersFirmenadressePLZ', 'fieldCity': '#editCustomersFirmenadresseOrt', 'fieldZipCode': '#editCustomersFirmenadressePLZ'}], 1);
		});
		$('#editCustomersFirmenadresseOrt').keyup(function () {
			loadSuggestions('searchCityPlz', [{'triggerElement': '#editCustomersFirmenadresseOrt', 'fieldCity': '#editCustomersFirmenadresseOrt', 'fieldZipCode': '#editCustomersFirmenadressePLZ'}], 1);
		});

		$('#editCustomersParentCompanyNumber').keyup(function () {
			loadSuggestions('searchCustomerNumberAndID', [{'triggerElement': '#editCustomersParentCompanyNumber', 'fieldNumber': '#editCustomersParentCompanyNumber', 'fieldID': '#editCustomersParentCompanyID'}], 1);
		});

		/*
		$('#editCustomersFirmenadresseStrasse').keyup(function () {
			loadSuggestions('searchStreetName', [{'triggerElement': '#editCustomersFirmenadresseStrasse', 'fieldStreet': '#editCustomersFirmenadresseStrasse'}], 0);
		});
		$('#editCustomersLieferadresseStrasse').keyup(function () {
			loadSuggestions('searchStreetName', [{'triggerElement': '#editCustomersLieferadresseStrasse', 'fieldStreet': '#editCustomersLieferadresseStrasse'}], 0);
		});
		$('#editCustomersRechnungsadresseStrasse').keyup(function () {
			loadSuggestions('searchStreetName', [{'triggerElement': '#editCustomersRechnungsadresseStrasse', 'fieldStreet': '#editCustomersRechnungsadresseStrasse'}], 0);
		});
		*/

		$('#editCustomersLieferadressePLZ').keyup(function () {
			loadSuggestions('searchPlzCity', [{'triggerElement': '#editCustomersLieferadressePLZ', 'fieldCity': '#editCustomersLieferadresseOrt', 'fieldZipCode': '#editCustomersLieferadressePLZ'}], 1);
		});
		$('#editCustomersLieferadresseOrt').keyup(function () {
			loadSuggestions('searchCityPlz', [{'triggerElement': '#editCustomersLieferadresseOrt', 'fieldCity': '#editCustomersLieferadresseOrt', 'fieldZipCode': '#editCustomersLieferadressePLZ'}], 1);
		});

		$('#editCustomersRechnungsadressePLZ').keyup(function () {
			loadSuggestions('searchPlzCity', [{'triggerElement': '#editCustomersRechnungsadressePLZ', 'fieldCity': '#editCustomersRechnungsadresseOrt', 'fieldZipCode': '#editCustomersRechnungsadressePLZ'}], 1);
		});
		$('#editCustomersRechnungsadresseOrt').keyup(function () {
			loadSuggestions('searchCityPlz', [{'triggerElement': '#editCustomersRechnungsadresseOrt', 'fieldCity': '#editCustomersRechnungsadresseOrt', 'fieldZipCode': '#editCustomersRechnungsadressePLZ'}], 1);
		});
		$('#searchPLZ').keyup(function () {
			loadSuggestions('searchPLZ', [{'triggerElement': '#searchPLZ', 'fieldZipCode': '#searchPLZ'}], 1);
		});

		$('#Rechnungsadresse_Firmenadresse').live('click', function(){
			if($(this).attr('checked') == 'checked') {
				// copyFields('#formEditCustomerDatas', '#Firmenadresse', '#Rechnungsadresse', '#Rechnungsadresse_Firmenadresse');
			}
			//$('#Rechnungsadresse').toggle();
		});
		$('#Lieferadresse_Firmenadresse').live('click', function(){
			if($(this).attr('checked') == 'checked') {
				// copyFields('#formEditCustomerDatas', '#Firmenadresse', '#Lieferadresse', '#Lieferadresse_Firmenadresse');
			}
			//$('#Lieferadresse').toggle();
		});

		$('#editCustomersBankName').keyup(function(){
			loadSuggestions('searchBankName', [{'triggerElement': '#editCustomersBankName', 'fieldCode': '#editCustomersBankLeitzahl', 'fieldName': '#editCustomersBankName', 'fieldBIC': '#editCustomersBankBIC', 'fieldIBAN': '#editCustomersBankIBAN'}], 1);
		});
		$('#editCustomersBankLeitzahl').keyup(function(){
			loadSuggestions('searchBankCode', [{'triggerElement': '#editCustomersBankLeitzahl', 'fieldCode': '#editCustomersBankLeitzahl', 'fieldName': '#editCustomersBankName', 'fieldBIC': '#editCustomersBankBIC', 'fieldIBAN': '#editCustomersBankIBAN'}], 1);
		});

		// BOF LOAD OTLG CUSTOMER DATA
			<?php if($_REQUEST["editID"] != "" && $_REQUEST["editID"] != "NEW") { ?>
			<?php
				$thisCustomerGroupID = 8; // OTLG
			?>
			// BOF GET PARENT OTLG CUSTOMER DATA
				function getParentGroupCompanyNumber(thisCustomerGroupID, mode){
					var thisCustomersGroupCompanyNumber = $('#editCustomersGroupCompanyNumber').val();

					var loadURL = 'inc/loadGroupCustomerData.inc.php';
					loadURL += '?groupID=' + thisCustomerGroupID;
					loadURL += '&mode=' + mode;

					if(thisCustomersGroupCompanyNumber != ''){
						thisCustomersGroupCompanyNumber = thisCustomersGroupCompanyNumber.replace(/ /g, '_');
						loadURL += '&groupCompanyNumber=' + thisCustomersGroupCompanyNumber;
					}
					$('#loadedParentGroupCompanyNumber').load(loadURL, function(response, status, xhr){
						if (status == 'error') {
							var msg = 'Die Daten konnten nicht geladen werden. Fehler-Code: ';
							$('#loadedParentGroupCompanyNumber').html(msg + xhr.status + ' ' + xhr.statusText);
						}
						else {
							if($('#loadedParentGroupCompanyNumber').html() != '') {

							}
						}
					});
				}

				$('#editCustomersGroupCompanyNumber').keyup(function(){
					getParentGroupCompanyNumber(<?php echo $thisCustomerGroupID; ?>, 'this');
				});
			// EOF GET PARENT OTLG CUSTOMER DATA

			// BOF LOAD THIS OTLG CUSTOMER DATA
				function loadCustomerGroupData(thisCustomerGroupID, mode){
					var content = '';
					$('#sideInfoGroup').remove();

					var thisCustomersGroupCompanyNumber = $('#editCustomersGroupCompanyNumber').val();

					content += '<div id="sideInfoGroup">';
					content += '<hr \/>';
					content += '<div class="sideInfoHeader">VZ-Nr | OTLG-Vertiebszentrum<\/div>';
					content += '<div class="sideInfoContent" id="sideInfoGroupContent"><\/div>';
					content += '<\/div>';

					$('#sideInfoGroupContent').empty().html('<span class="loadingArea"><img class="loader" src="layout/ajax-loader.gif" \/> Bitte haben Sie ein wenig Geduld. Die Daten werden geladen...<\/span>');

					$('.sideInfo').append(content);

					var loadURL = 'inc/loadGroupCustomerData.inc.php';
					loadURL += '?groupID=' + thisCustomerGroupID;
					loadURL += '&mode=' + mode;

					if(thisCustomersGroupCompanyNumber != ''){
						thisCustomersGroupCompanyNumber = thisCustomersGroupCompanyNumber.replace(/ /g, '_');
						loadURL += '&groupCompanyNumber=' + thisCustomersGroupCompanyNumber;
					}

					$('#sideInfoGroupContent').load(loadURL, function(response, status, xhr){
						if (status == 'error') {
							var msg = 'Die Daten konnten nicht geladen werden. Fehler-Code: ';
							$('#sideInfoGroupContent').html(msg + xhr.status + ' ' + xhr.statusText);
						}
						else {
							if($('#sideInfoGroupContent').html() != '') {

							}
						}
					});
				}

				$('input[name*="editCustomersGruppe"]').click(function() {
					var thisCustomerGroupValue = $(this).val();
					if(thisCustomerGroupValue == '<?php echo $thisCustomerGroupID; ?>'){
						var thisCustomerGroupChecked = $(this).attr('checked');
						if(thisCustomerGroupChecked == 'checked'){
							loadCustomerGroupData(<?php echo $thisCustomerGroupID; ?>, 'all');
							$('#editCustomersGroupCompanyNumber').css('border', '1px solid #F00');
							alert('Bitte die Betriebsnummer im rot markierten Feld eingeben und den richtigen RE-Empfänger eintragen!\nDie OTLG-Vertiebszentren werden rechts angezeigt!');
						}
						else{
							$('#sideInfoGroup').remove();
							$('#editCustomersGroupCompanyNumber').css('border', '');
						}
					}
				});
			// BOF LOAD THIS OTLG CUSTOMER DATA

			// BOF LOAD THIS OTLG CUSTOMER DATA ON START
				<?php if(in_array($thisCustomerGroupID, $thisArrCustomerGroups)) { ?>
					loadCustomerGroupData(<?php echo $thisCustomerGroupID; ?>, 'all');
					getParentGroupCompanyNumber(<?php echo $thisCustomerGroupID; ?>, 'this');
				<?php } ?>
			// EOF LOAD THIS OTLG CUSTOMER DATA ON START
			<?php } ?>
		// EOF LOAD OTLG CUSTOMER DATA


		$('#editCustomersVertreterName').keyup(function() {
			loadSuggestions('searchSalesmen', [{'triggerElement': '#editCustomersVertreterName', 'fieldName': '#editCustomersVertreterName', 'fieldID': '#editCustomersVertreterID'}], 1);
		});
		$('#editCustomersVertreterName').focus(function() {
			if(($this).val() == '') { $('#editCustomersVertreterID').val(''); }
		});

		$('#editCustomersUseSalesmanDeliveryAdress').click(function() {
			var salesmanID = $('#editCustomersVertreterID').val();
			loadSalesmansAdress(salesmanID, 'delivery', '#editCustomersUseSalesmanDeliveryAdress');
		});
		$('#editCustomersUseSalesmanInvoiceAdress').click(function() {
			var salesmanID = $('#editCustomersVertreterID').val();
			loadSalesmansAdress(salesmanID, 'invoice', '#editCustomersUseSalesmanInvoiceAdress');
		});

		$('.buttonLoadOrderDetails').click(function() {
			var arrThisData = $(this).attr('alt').split('#');
			loadOrderDocumentDetails(arrThisData[2], arrThisData[1], arrThisData[0]);
		});

		$('.buttonConvertDocument').mouseenter(function() {

		});
		$('.buttonCopyDocument').mouseenter(function() {

		});

		$('.buttonSendDocument').click(function() {
			var mailDocumentDatas = $(this).attr('rel');

			var mailAdress = $(this).attr('data_mail');
			if(mailAdress == ''){
				mailAdress = '<?php if($customerDatas["customersMail1"] != "") { echo $customerDatas["customersMail1"]; } else if($customerDatas["customersMail2"] != "") { echo $customerDatas["customersMail2"]; } else { echo ''; }?>';
			}
			sendAttachedDocument($(this), mailDocumentDatas, '<?php echo $thisDocumentValue[$thisKey]["orderDocumentsCustomerNumber"]; ?>', '<?php echo $_REQUEST["editID"]; ?>', '', '<?php echo $_SERVER["PHP_SELF"]; ?>', mailAdress);
		});

		<?php if($customerDatas["customersKundennummer"] == "") { ?>
		$('#editCustomersKundennummer').keyup(function() {
			checkIfCustomerNumberAlreadyExists($(this), '<?php echo LENGTH_CUSTOMER_NUMBER; ?>');
		});
		<?php } ?>

		<?php if($_REQUEST["editID"] != "NEW") { ?>
		$(function() {
			// $('#tabs').tabs();
		});
		<?php
			if($_REQUEST["editID"] != "") {
				#if($customerDatas["customersDatasUpdated"] != '1') {
				if($customerDatas["customersDatasUpdated"] != '1' && $_REQUEST["tab"] == "tabs-1") {
					#echo 'window.alert("Bitte kontrollieren Sie diese Daten mit den Daten des alten Auftragslisten-Programms und speichern Sie diese!\nAnsonsten ist dieser Kunde zur weiteren Verwendung \n(Schreiben von Angeboten, Aufträgen, Rechnungen etc. ) \ngesperrt! ");';
					$jswindowMessage .= "Bitte kontrollieren Sie diese Daten mit den Daten des alten Auftragslisten-Programms und speichern Sie diese!" . "<br />";
					$jswindowMessage .= "Ansonsten ist dieser Kunde zur weiteren Verwendung " . "<br />";
					$jswindowMessage .= "(Schreiben von Angeboten, Aufträgen, Rechnungen etc.) " . "<br />";
					$jswindowMessage .= "gesperrt!" . "<br />";
				}
			}
		?>
		<?php } ?>

		$('.sideInfoItem').live('click', function () {
			// var plzSalesmanID = $(this).attr('id');
			var plzSalesmanID = $(this).attr('rel');
			var plzSalesmanName = $(this).find('.sideInfoText').text();
			$('#editCustomersVertreterName').val(plzSalesmanName);
			$('#editCustomersVertreterID').val(plzSalesmanID);
		});

		colorRowMouseOver('.displayOrders tbody tr');
		$('.buttonNotice').click(function () {
			loadNotice($(this));
		});
		setFormDutyFields(<?php echo $jsonFormDutyFields; ?>);

		$('#editCustomersInvoicesNotPurchased').click(function() {
			if($(this).attr('checked') == 'checked'){
				$(this).parent().css('background-color', '#FF7F84');
			}
			else {
				$(this).parent().css('background-color', '#00FF00');
			}
		});
		$('#editCustomersBadPaymentBehavior').click(function() {
			if($(this).attr('checked') == 'checked'){
				$(this).parent().css('background-color', '#FF7F84');
			}
			else {
				$(this).parent().css('background-color', '#00FF00');
			}
		});
		$('#editCustomersPaymentOnlyPrepayment').click(function() {
			if($(this).attr('checked') == 'checked'){
				$(this).parent().css('background-color', '#FF7F84');
			}
			else {
				$(this).parent().css('background-color', '#00FF00');
			}
		});
		$('#editCustomersSepaExists').click(function() {
			if($(this).attr('checked') == 'checked'){
				$(this).parent().css('background-color', '#00FF00');
			}
			else {
				$(this).parent().css('background-color', '#FF7F84');
			}
		});
		
		$(function() {

			$.datepicker.setDefaults($.datepicker.regional[""]);
			$('#addCustomerCalendarDates').datepicker($.datepicker.regional["de"]);
			$('#editCustomersEntryDate').datepicker($.datepicker.regional["de"]);
			$('#editCustomersEntryDate').datepicker("option", "maxDate", "0" );
			$('#editCustomersSepaValidUntilDate').datepicker($.datepicker.regional["de"]);
			$('#editCustomersSepaRequestedDate').datepicker($.datepicker.regional["de"]);

			$('#phoneMarketingContactDate').datepicker($.datepicker.regional["de"]);
			$('#phoneMarketingContactDate').datepicker("option", "maxDate", "0" );



			var timepickerConfig = {
				timeSeparator: ':',
				showLeadingZero: true,
				showMinutesLeadingZero: true,
				showOn: 'focus',
				hours: {
					starts: 8,
					ends: 21
				},
				minutes: {
					starts: 0,
					ends: 59,
					interval: 5
				},
				rows: 2,
				showHours: true,
				showMinutes: true,
				hourText: 'Stunde',
				minuteText: 'Minute',
				showPeriod: false,
				showPeriodLabels: false,
			};
			$('#phoneMarketingContactTime').timepicker(timepickerConfig);

			$('#phoneMarketingRemindDate').datepicker($.datepicker.regional["de"]);
			$('#phoneMarketingRemindDate').datepicker("option", "minDate", "0" );
			$('#phoneMarketingCallbackDate').datepicker($.datepicker.regional["de"]);
			$('#phoneMarketingCallbackDate').datepicker("option", "minDate", "0" );

			var htmlButtonClearField = '<span class="buttonClearField"><img src="layout/icons/iconDelete.png" width="16" height="16" alt="" title="Eingabefeld leeren" /><\/span>';
			$('#editCustomersEntryDate').parent().append(htmlButtonClearField);
			$('#editCustomersSepaValidUntilDate').parent().append(htmlButtonClearField);
			$('#editCustomersSepaRequestedDate').parent().append(htmlButtonClearField);
			$('#editCustomersParentCompanyNumber').parent().append(htmlButtonClearField);

			// $('#phoneMarketingContactDate').parent().append(htmlButtonClearField);
			// $('#phoneMarketingContactTime').parent().append(htmlButtonClearField);
			$('#phoneMarketingRemindDate').parent().append(htmlButtonClearField);
			$('#phoneMarketingCallbackDate').parent().append(htmlButtonClearField);

			$('.buttonClearField').click(function () {
				$(this).parent().find('input').val('');
				$(this).parent().find('input').attr('checked', false);
			});
		});
		/*
		$('.sideInfo').draggable({
			drag: function(event, ui) {
				$('.sideInfo').css('cursor', 'move');
			}
        });
		*/
		$('.sideInfoHeader').dblclick(function(){
			// $('.sideInfoContent').toggle();
			$(this).next('.sideInfoContent').toggle();
		});

		// BOF SEPA FORM DATA
		if($('#editCustomersBezahlart').val() == '4' || $('#editCustomersBezahlart').val() == '6'){
			$('#areaSepaData').show();
		}
		$('#editCustomersBezahlart').change(function(){
			if($('#editCustomersBezahlart').val() == '4' || $('#editCustomersBezahlart').val() == '6'){
				$('#areaSepaData').show();
			}
			else {
				$('#areaSepaData').hide();
			}
		});
		// EOF SEPA FORM DATA

		//
		<?php if(1){ ?>
		$('#editCustomersRechnungsadresseLand').live('change', function() {
			var thisCustomersRechnungsadresseLandID = $(this).val();
			if(thisCustomersRechnungsadresseLandID != 81){
				window.alert('Bitte geben Sie für Auslandskunden eine UmsatzsteuerID (UstID) ein!');
			}
		});
		$('#editCustomersFirmenadresseLand').live('change', function() {
			var thisCustomersFirmenadresseLandID = $(this).val();
			if(thisCustomersFirmenadresseLandID != 81 && $('#Rechnungsadresse_Firmenadresse').attr('checked')){
				window.alert('Bitte geben Sie für Auslandskunden eine UmsatzsteuerID (UstID) ein!');
				// Rechnungsadresse_Firmenadresse
			}
		});

		$('#Rechnungsadresse_Firmenadresse').live('click', function() {
			var thisCustomersFirmenadresseLandID = $('#editCustomersFirmenadresseLand').val();
			var thisRechnungsadresse_FirmenadresseChecked = $('#Rechnungsadresse_Firmenadresse').attr('checked');
			if(thisCustomersFirmenadresseLandID != 81 && thisRechnungsadresse_FirmenadresseChecked == 'checked'){
				window.alert('Bitte geben Sie für Auslandskunden eine UmsatzsteuerID (UstID) ein!');
				// Rechnungsadresse_Firmenadresse
			}
		});
		<?php } ?>

		<?php if($_COOKIE["isAdmin"] == '1') { ?>
		$('#editCustomersRechnungsadresseKundennummer').keyup(function () {
			loadSuggestions('searchCustomerNumber', [{'triggerElement': '#editCustomersRechnungsadresseKundennummer', 'fieldNumber': '#editCustomersRechnungsadresseKundennummer', 'fieldName': '#editCustomersRechnungsadresseFirmenname', 'fieldCity': '#editCustomersRechnungsadresseOrt', 'fieldZipCode': '#editCustomersRechnungsadressePLZ'}], 1);
			$('.searchResultItem').live('click', function(){
				var thisRecipientCustomerNumber = $(this).find('.customerNumber').text();
				var thisRecipientCustomerID = $(this).find('.customerID').text();
				reloadCustomerAdressFields('searchCustomerAdressDatas', 'invoice', [{'thisCustomerID': thisRecipientCustomerID, 'triggerElement': '#editCustomersRechnungsadresseKundennummer', 'fieldNumber': '#editCustomersRechnungsadresseKundennummer', 'fieldName': '#editCustomersRechnungsadresseFirmenname', 'fieldNameAdd': '#editCustomersRechnungsadresseFirmennameZusatz', 'fieldCity': '#editCustomersRechnungsadresseOrt', 'fieldZipCode': '#editCustomersRechnungsadressePLZ', 'fieldStreet': '#editCustomersRechnungsadresseStrasse', 'fieldStreetNumber': '#editCustomersRechnungsadresseHausnummer', 'fieldCountry': '#editCustomersRechnungsadresseLand'}]);
			});
		});
		$('#editCustomersLieferadresseKundennummer').keyup(function () {
			loadSuggestions('searchCustomerNumber', [{'triggerElement': '#editCustomersLieferadresseKundennummer', 'fieldNumber': '#editCustomersLieferadresseKundennummer', 'fieldName': '#editCustomersLieferadresseFirmenname', 'fieldCity': '#editCustomersLieferadresseOrt', 'fieldZipCode': '#editCustomersLieferadressePLZ'}], 1);
			$('.searchResultItem').live('click', function(){
				var thisRecipientCustomerNumber = $(this).find('.customerNumber').text();
				var thisRecipientCustomerID = $(this).find('.customerID').text();
				reloadCustomerAdressFields('searchCustomerAdressDatas', 'delivery', [{'thisCustomerID': thisRecipientCustomerID, 'triggerElement': '#editCustomersLieferadresseKundennummer', 'fieldNumber': '#editCustomersLieferadresseKundennummer', 'fieldName': '#editCustomersLieferadresseFirmenname', 'fieldNameAdd': '#editCustomersLieferadresseFirmennameZusatz', 'fieldCity': '#editCustomersLieferadresseOrt', 'fieldZipCode': '#editCustomersLieferadressePLZ', 'fieldStreet': '#editCustomersLieferadresseStrasse', 'fieldStreetNumber': '#editCustomersLieferadresseHausnummer', 'fieldCountry': '#editCustomersLieferadresseLand'}]);
			});
		});
		<?php } ?>

		blink('.iconAttention', 1000, 0.1, 400);

		// BOF LOAD EXTERNAL DELIVERY DATAS
			<?php if($_REQUEST["tab"] == "tabs-6"){ ?>

			function loadDeliveryDatas(trackingID, trackingDate, trackingRow, trackingZipcode, thisTrackingCustomerNumber){
				var loadURL = 'inc/loadDeliveryDatas.inc.php?searchTrackingID=' + trackingID + '&searchTrackingDate=' + trackingDate + '&searchTrackingZipcode=' + trackingZipcode + '&searchTrackingCustomerNumber=' + thisTrackingCustomerNumber;

				$('#loadDeliveryDate_' + trackingID + '_' + trackingRow).html('<img class="loader" src="layout/ajax-loader.gif" alt="" height="10" width="10" \/> <span style="font-style:italic;font-size:10px;color:#FF0000;">Daten-Abruf von DPD<\/span>');
				$('#loadDeliveryName_' + trackingID + '_' + trackingRow).html('<img class="loader" src="layout/ajax-loader.gif" alt="" height="10" width="10" \/> <span style="font-style:italic;font-size:10px;color:#FF0000;">Daten-Abruf von DPD<\/span>');

				$.get(loadURL, function(result) {
					var arrResult = result.split('#');
					$('#loadDeliveryDate_' + trackingID + '_' + trackingRow).html('');
					$('#loadDeliveryName_' + trackingID + '_' + trackingRow).html('');
					var arrResult = result.split('#');
					var imgAttention = '';

					if(result.length > 10){
						var thisClass = 'row3';
						if(arrResult[1] == '[Kein Zustell-Scan]'){
							thisClass = 'row2';
							imgAttention = '<?php echo $imgAttention; ?>';
						}
						$('#loadDeliveryDate_' + trackingID + '_' + trackingRow).parent().attr('class', thisClass);
						$('#loadDeliveryName_' + trackingID + '_' + trackingRow).parent().attr('class', thisClass);
					}

					<?php if($_COOKIE["isAdmin"] == "1") { ?>
					var thisDate = arrResult[0];
					var arrDate = thisDate.split('-');
					var formattedDate = arrDate[2] + '.' + arrDate[1] + '.' + arrDate[0];
					<?php } ?>

					$('#loadDeliveryDate_' + trackingID + '_' + trackingRow).html(arrResult[0] + ' ' + imgAttention);
					$('#loadDeliveryName_' + trackingID + '_' + trackingRow).html(arrResult[1] + ' ' + imgAttention);

					// $('#loadDeliveryDate_' + trackingID + '_' + trackingRow).attr('class', 'loadedThisDeliveryData');
					$('#loadDeliveryDate_' + trackingID + '_' + trackingRow).removeAttr('class');

					startLoadDeliveryDatas();
				});
			}

			function startLoadDeliveryDatas(){
				if($('.loadThisDeliveryData').length > 0){
					if($('.loadThisDeliveryData').first().attr('ref')){
						var thisTrackingData = $('.loadThisDeliveryData').first().attr('ref');
						var arrThisTrackingData = thisTrackingData.split(':');
						var thisTrackingID = arrThisTrackingData[0];
						var thisTrackingDate = arrThisTrackingData[1];
						var thisTrackingRow = arrThisTrackingData[2];
						var thisTrackingZipcode = arrThisTrackingData[3];
						var thisTrackingCustomerNumber = arrThisTrackingData[4];

						loadDeliveryDatas(thisTrackingID, thisTrackingDate, thisTrackingRow, thisTrackingZipcode, thisTrackingCustomerNumber);
					}
				}
			}

			try{
				startLoadDeliveryDatas();
			}
			catch(err) {
				alert(err);
			}
			finally {

			}
			<?php } ?>
		// EOF LOAD EXTERNAL DELIVERY DATAS
	});

	/* ]]> */
	// -->
</script>

<?php require_once('inc/footerHTML.inc.php'); ?>