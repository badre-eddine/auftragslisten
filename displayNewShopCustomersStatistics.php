<?php
	require_once('inc/requires.inc.php');

	if(!$arrGetUserRights["displayOnlineOrdersStatistics"]) {
		header('location: ' . PAGE_EXIT_LOCATION);
		exit;
	}

	$jswindowMessage = '';
	$warningMessage = '';
	$errorMessage = '';
	$successMessage = '';
	$infoMessage = '';


	$dbConnection = new DB_Connection();
	$db_open = $dbConnection->db_connect();

	$dbConnection_ExternShop = new DB_Connection(DB_HOST_EXTERN_SHOP, '', DB_NAME_EXTERN_SHOP, DB_USER_EXTERN_SHOP, DB_PASSWORD_EXTERN_SHOP);
	$dbOpen_ExternShop = $dbConnection_ExternShop->db_connect();

	// BOF READ VERTRETER
		$arrSalesmenDatas = getSalesmen();
	// EOF READ VERTRETER

	$defaultORDER = "customers_date_added";
	$defaultSORT = "ASC";

	if($_POST["searchInterval"] == ""){
		$_POST["searchInterval"] = "WEEK";
	}

	// BOF READ BESTELL ARTEN
		$arrOrderTypeDatas = getOrderTypes();
	// EOF READ BESTELL ARTEN
?>
<?php
	require_once('inc/headerHTML.inc.php');
	$thisTitle = "Neukunden im Shop &quot;" . constant("PATH_ONLINE_SHOP_" . strtoupper(MANDATOR)) . "&quot;";

	if($_POST["searchInterval"] == 'WEEK') {
		$thisTitle .= ': <span class="headerSelectedEntry">pro Woche</span>';
	}
	else if($_POST["searchInterval"] == 'MONTH') {
		$thisTitle .= ': <span class="headerSelectedEntry">pro Monat</span>';
	}
	else if($_POST["searchInterval"] == 'QUARTER') {
		$thisTitle .= ': <span class="headerSelectedEntry">pro Vierteljahr</span>';
	}
	else if($_POST["searchInterval"] == 'YEAR') {
		$thisTitle .= ': <span class="headerSelectedEntry">pro Jahr</span>';
	}

	$headerHTML = preg_replace("/{###TITLE###}/", strip_tags($thisTitle), $headerHTML);
	echo $headerHTML;

	require_once(FILE_MENUE_TOP);

	$sqlSortField = "";
	$sqlSortDirection = "";
	$sqlWhere = "";

?>

<div id="mainArea">
	<div id="mainContent">
		<?php require_once(FILE_MENUE_SIDEBAR); ?>
		<div id="contentArea">
			<a name="top"></a>
			<div id="contentAreaElements">
				<h1><?php if(SHOW_PAGE_TITLE_ICONS) { echo '<img src="' . PATH_ICONS_MENUE_TITLES . 'statistics.png" alt="" />'; } ?> <?php echo $thisTitle; ?></h1>

				<div id="searchFilterArea">
					<form name="formSearchDatas" method="post" action="<?php echo $_SERVER["PHP_SELF"]; ?>">
					<table border="0" cellpadding="0" cellspacing="0" width="" class="searchFilterContent">
						<tr>
							<td>
								<label for="searchInterval">Zeitraum:</label>
								<select name="searchInterval" id="searchInterval" class="inputField_130">
									<option value=""></option>
									<option value="WEEK" <?php if($_POST["searchInterval"] == 'WEEK'){ echo ' selected="selected" '; } ?> >pro Woche</option>
									<option value="MONTH" <?php if($_POST["searchInterval"] == 'MONTH'){ echo ' selected="selected" '; } ?> >pro Monat</option>
									<option value="QUARTER" <?php if($_POST["searchInterval"] == 'QUARTER'){ echo ' selected="selected" '; } ?> >pro Vierteljahr</option>
									<option value="YEAR" <?php if($_POST["searchInterval"] == 'YEAR'){ echo ' selected="selected" '; } ?> >pro Jahr</option>
								 </select>
							</td>
							<td>
								<input type="hidden" name="editID" id="editID" value="" />
								<input type="submit" name="submitSearch" class="inputButton0" value="Anzeigen" />
							</td>
						</tr>
					</table>
					<input type="hidden" name="submitSearchForm" id="submitSearchForm" value="1" />
					</form>
				</div>

				<div class="contentDisplay">
					<?php

						//if($_POST["submitExportForm"] == 1) {
						if(1) {
							// BOF GET DATAS
							$where = "";
							$dateField = "";

							if($_POST["searchInterval"] == "WEEK"){
								$where .= "";
								$dateField = "
									CONCAT(
										IF(DATE_FORMAT(`" . TABLE_SHOP_CUSTOMERS . "`.`customers_date_added`, '%m') = '12' AND DATE_FORMAT(`" . TABLE_SHOP_CUSTOMERS . "`.`customers_date_added`, '%v') = '01', (DATE_FORMAT(`" . TABLE_SHOP_CUSTOMERS . "`.`customers_date_added`, '%Y') + 1),  DATE_FORMAT(`" . TABLE_SHOP_CUSTOMERS . "`.`customers_date_added`, '%Y')),
										'#',
										DATE_FORMAT(`" . TABLE_SHOP_CUSTOMERS . "`.`customers_date_added`, '%v')
									) AS `interval`
								";
							}
							else if($_POST["searchInterval"] == "MONTH"){
								// DATE_FORMAT(`" . TABLE_ORDER_INVOICES . "`.`customers_date_added`, '%M') AS `interval`,
								$where .= "";

								$dateField = "
									CONCAT(
										DATE_FORMAT(`" . TABLE_SHOP_CUSTOMERS . "`.`customers_date_added`, '%Y'),
										'#',
										DATE_FORMAT(`" . TABLE_SHOP_CUSTOMERS . "`.`customers_date_added`, '%m')
									) AS `interval`
								";
							}
							else if($_POST["searchInterval"] == "QUARTER"){
								$where .= "";

								$dateField = "
									CONCAT(
										DATE_FORMAT(`" . TABLE_SHOP_CUSTOMERS . "`.`customers_date_added`, '%Y'),
										'#',
										IF(DATE_FORMAT(`" . TABLE_SHOP_CUSTOMERS . "`.`customers_date_added`, '%m') = '01' OR DATE_FORMAT(`" . TABLE_SHOP_CUSTOMERS . "`.`customers_date_added`, '%m') = '02' OR DATE_FORMAT(`" . TABLE_SHOP_CUSTOMERS . "`.`customers_date_added`, '%m') = '03', '1. Quartal',
											IF(DATE_FORMAT(`" . TABLE_SHOP_CUSTOMERS . "`.`customers_date_added`, '%m') = '04' OR DATE_FORMAT(`" . TABLE_SHOP_CUSTOMERS . "`.`customers_date_added`, '%m') = '05' OR DATE_FORMAT(`" . TABLE_SHOP_CUSTOMERS . "`.`customers_date_added`, '%m') = '06', '2. Quartal',
												IF(DATE_FORMAT(`" . TABLE_SHOP_CUSTOMERS . "`.`customers_date_added`, '%m') = '07' OR DATE_FORMAT(`" . TABLE_SHOP_CUSTOMERS . "`.`customers_date_added`, '%m') = '08' OR DATE_FORMAT(`" . TABLE_SHOP_CUSTOMERS . "`.`customers_date_added`, '%m') = '09', '3. Quartal',
													IF(DATE_FORMAT(`" . TABLE_SHOP_CUSTOMERS . "`.`customers_date_added`, '%m') = '10' OR DATE_FORMAT(`" . TABLE_SHOP_CUSTOMERS . "`.`customers_date_added`, '%m') = '11' OR DATE_FORMAT(`" . TABLE_SHOP_CUSTOMERS . "`.`customers_date_added`, '%m') = '12', '4. Quartal',
														''
													)
												)
											)
										)
									) AS `interval`
								";

							}
							else if($_POST["searchInterval"] == "YEAR"){
								$where .= "";

								$dateField = "
									CONCAT(
										DATE_FORMAT(`" . TABLE_SHOP_CUSTOMERS . "`.`customers_date_added`, '%Y'),
										'#',
										DATE_FORMAT(`" . TABLE_SHOP_CUSTOMERS . "`.`customers_date_added`, '%Y')
									) AS `interval`
								";

							}

							$arrIntervals = array();
							$arrDatas = array();

							$sql = "
									SELECT
										" . $dateField . ",
										COUNT(`" . TABLE_SHOP_CUSTOMERS . "`.`customers_id`) AS `countItems`

									FROM `" . TABLE_SHOP_CUSTOMERS . "`
									WHERE 1
										" . $where . "

									GROUP BY `interval`

									ORDER BY `interval` DESC
								";

							$rs = $dbConnection_ExternShop->db_query($sql);

							$countTotalRows = $dbConnection_ExternShop->db_getMysqlNumRows($rs);

							while($ds = mysqli_fetch_assoc($rs)) {
								$arrIntervals[] = $ds["interval"];
								$arrLabelsGraph[$ds["interval"]] = $ds["interval"];
								$arrDatasGraph["countItems"][$ds["interval"]] = $ds["countItems"];
								// $arrDatasGraph["totalPrice"][$ds["interval"]] = $ds["totalPrice"];
								foreach(array_keys($ds) as $field){
									$arrDatas[$ds["interval"]][$field] = $ds[$field];
								}
							}

							if(!empty($arrIntervals)){
								$arrIntervals = array_unique($arrIntervals);
								arsort($arrIntervals);
							}

							if($countTotalRows > 0) {
							?>
								<!-- BOF GRAPH ELEMENTS -->
								<script type="text/javascript" language="javascript" src="<?php echo PATH_RGRAPH; ?>libraries/RGraph.common.core.js" ></script>
								<script type="text/javascript" language="javascript" src="<?php echo PATH_RGRAPH; ?>libraries/RGraph.bar.js" ></script>
								<script type="text/javascript" language="javascript" src="<?php echo PATH_RGRAPH; ?>libraries/RGraph.line.js" ></script>

								<div class="adminEditArea">
									<h2>Anzahl der Neukunden im Shop</h2>
									<canvas id="cvs_dataCountItems" width="900" height="250">[No canvas support]</canvas>

									<!--
									<hr />
									<h2>Summe der Neukunden</h2>
									<canvas id="cvs_dataTotalPrice" width="900" height="250">[No canvas support]</canvas>
									-->
								</div>

								<!-- EOF GRAPH ELEMENTS -->

							<?php
								echo '<table width="100%" cellpadding="0" cellspacing="0" class="displayOrders">';

								echo '<thead>';
								echo '<tr>';
								echo '<th style="width:45px">#</th>';
								echo '<th style="width:80px">Zeitraum</th>';
								echo '<th colspan="2">Anzahl</th>';
								// echo '<th>Betrag</th>';
								echo '<th style="width:50px">Info</th>';
								echo '</tr>';
								echo '</thead>';

								echo '<tbody>';

								$countRow = 0;
								$thisMarker = "";

								foreach($arrIntervals as $thisIntervalKey => $thisIntervalValue) {
									if($arrDatas[$thisIntervalValue]['countItems'] > 0){
										if($countRow == 0) {

										}

										$rowClass = 'row0';
										if($countRow%2 == 0) {
											$rowClass = 'row1';
										}
										$arrTemp = explode('#', $thisIntervalValue);
										if($thisMarker != $arrTemp[0]) {
											$thisMarker = $arrTemp[0];
											echo '<tr><td colspan="8" class="tableRowTitle1">' . $arrTemp[0] . '</td></tr>';
										}

										echo '<tr class="'.$rowClass.'">';

										echo '<td style="text-align:right;"><b>';
										echo ($countRow + 1);
										echo '.</b></td>';
										echo '<td>';

										if($_POST["searchInterval"] == "MONTH"){
											$thisMonth = getTimeNames($arrTemp[1], 'month', 'long');
											echo $thisMonth;
										}
										else {
											if($_POST["searchInterval"] == "WEEK"){ echo 'KW '; }
											echo $arrTemp[1];
										}
										echo '</td>';

										echo '<td style="text-align:right;">';
										echo '' . $arrDatas[$thisIntervalValue]['countItems'] . '';
										echo '</td>';

										echo '<td style="text-align:left;background-color:#FEFFAF;">';
										echo '<span style="padding:0 20px 0 0"><img src="layout/bgRed.gif" width="' . $arrDatas[$thisIntervalValue]['countItems'] . '" height="4" /></span>';
										echo '</td>';

										// echo '<td style="text-align:right;background-color:#FEFFAF;">';
										// echo '' . number_format($arrDatas[$thisIntervalValue]['invoices']['totalPrice'], 2, ',', '.') . ' &euro;';
										// echo '</td>';

										echo '<td>';
										echo '<span class="toolItem">';
										echo '<img src="layout/icons/iconInfo.png" class="buttonShopOrderInfo" alt="Details" title="Detailinformationen ansehen">';
										echo '<div class="orderDetailsContent" style="display:none; visibility:hidden;">';
										echo '</div>';
										echo '</span>';
										echo '</td>';
										echo '</tr>';
										$countRow++;
									}
								}
								echo '</tbody>';
								echo '</table>';
							}
							else {
								$warningMessage .= 'F&uuml;r den gew&auml;hlten Zeitraum wurden keine Daten gefunden.';
							}
							// EOF GET DATAS
						}
					?>
					<?php displayMessages(); ?>
				</div>

			</div>
		</div>
		<div class="clear"></div>
	</div>
</div>
<script language="javascript" type="text/javascript">
	$(document).ready(function() {
		$(function() {
			colorRowMouseOver('.displayOrders tbody tr');
			$('.buttonShopOrderInfo').css('cursor', 'pointer');
			$('.buttonShopOrderInfo').click(function () {
				loadOrderDetails($(this), '<?php echo BASEPATH; ?>', 'Details der Bestellung');
			});

			// BOF GET DATAS FOR GRAPH CASH
			<?php
				if(!empty($arrDatasGraph)){
					$arrDatasGraph["countItems"] = array_reverse($arrDatasGraph["countItems"], true);
					$arrDatasGraph["totalPrice"] = array_reverse($arrDatasGraph["totalPrice"], true);
					$arrLabelsGraph = array_reverse($arrLabelsGraph , true);
				}
			?>

			createGraph('cvs_dataCountItems', [<?php echo implode(",", array_values($arrDatasGraph["countItems"])); ?>], [<?php echo "'" . implode("','", $arrLabelsGraph) . "'"; ?>], 'Line', [['green','red']]);
			// createGraph('cvs_dataTotalPrice', [<?php echo implode(",", array_values($arrDatasGraph["totalPrice"])); ?>], [<?php echo "'" . implode("','", $arrLabelsGraph) . "'"; ?>], 'Line', [['green','red']]);

		// EOF GET DATAS FOR GRAPH CASH
		});
		$(function() {
			$('#tabs').tabs();
		});
	});
</script>

<?php
	$dbConnection_ExternShop->db_close();
?>

<?php require_once('inc/footerHTML.inc.php'); ?>