<?php
	require_once('inc/requires.inc.php');

	$_REQUEST["isCollectiveInvoice"] = 1;
	$_REQUEST["documentType"] = 'RE';

	if($_REQUEST["isCollectiveInvoice"] == '1'){
		require_once('inc/formAddOrderConfirmations.inc.php');
		require_once('inc/loadCollectiveInvoiceDatas.inc.php');
	}
	else {
		require_once('inc/loadCardDatas.inc.php');
		require_once('inc/formAddProduct.inc.php');
	}

	require_once DIRECTORY_CKEDITOR . "ckeditor.php";

	require_once(DIRECTORY_PDF_TEMPLATES . '/template_TEXT_DATAS.inc.php');
	require_once(DIRECTORY_MAIL_TEMPLATES . 'template_TEXT_DATAS.inc.php');

	if($_REQUEST["searchCustomerNumber"] != "") {
		$_REQUEST["editID"] = $_REQUEST["searchCustomerNumber"];
	}

	$arrQueryVars = getUrlQueryString($_SERVER["REQUEST_URI"]);
	foreach($arrQueryVars as $thisKey => $thisValue) {
		$_REQUEST[$thisKey] = $thisValue;
	}
	if($_REQUEST["editDocType"] != '') {
		$_REQUEST["documentType"] = $_REQUEST["editDocType"];
	}
	if($_REQUEST["convertDocType"] != '') {
		$_REQUEST["documentType"] = $_REQUEST["convertDocType"];
	}
	if($_REQUEST["copyDocType"] != '') {
		$_REQUEST["documentType"] = $_REQUEST["copyDocType"];
	}

	if($_REQUEST["documentType"] == "") {
		header('location: ' . PAGE_EXIT_LOCATION);
		exit;
	}
	else if($_REQUEST["documentType"] == 'RE' && !$arrGetUserRights["editInvoices"]) {
		header('location: ' . PAGE_EXIT_LOCATION);
		exit;
	}
	else if($_REQUEST["documentType"] == 'AB' && !$arrGetUserRights["editConfirmations"]) {
		header('location: ' . PAGE_EXIT_LOCATION);
		exit;
	}
	else if($_REQUEST["documentType"] == 'AN' && !$arrGetUserRights["editOffers"]) {
		header('location: ' . PAGE_EXIT_LOCATION);
		exit;
	}
	else if($_REQUEST["documentType"] == 'BR' && !$arrGetUserRights["editLetters"]) {
		header('location: ' . PAGE_EXIT_LOCATION);
		exit;
	}
	else if(($_REQUEST["documentType"] == 'MA' || $_REQUEST["documentType"] == 'M1' || $_REQUEST["documentType"] == 'M2' || $_REQUEST["documentType"] == 'M3') && !$arrGetUserRights["editReminders"]) {
		header('location: ' . PAGE_EXIT_LOCATION);
		exit;
	}
	else if($_REQUEST["documentType"] == 'LS' && !$arrGetUserRights["editDeliveries"]) {
		header('location: ' . PAGE_EXIT_LOCATION);
		exit;
	}

// BOF xxx TEST DOC NUMBER
$generatedDocumentNumber = "RE-1000000000";
if($_REQUEST["isCollectiveInvoice"] == '1'){
	$generatedDocumentNumber = "RE-1000000000";
	$_POST["editDocType"] = 'RE';
	$_POST["originDocumentNumber"] = "RE-1000000000";
}
// EOF xxx TEST DOC NUMBER

	// BOF DOWNLOAD FILE
	if($arrQueryVars["downloadFile"] != "") {
		require_once('classes/downloadFile.class.php');
		$thisDownload = new downloadFile(DIRECTORY_CREATED_DOCUMENTS_CUSTOMERS, $arrQueryVars["downloadFile"]);
		$errorMessage .= $thisDownload->startDownload();
	}
	// EOF DOWNLOAD FILE

	$jswindowMessage = "";
	$warningMessage = '';
	$errorMessage = '';
	$successMessage = '';
	$infoMessage = '';

	$dbConnection = new DB_Connection();
	$db_open = $dbConnection->db_connect();

	// BOF CHECK IF DOCUMENT WAS ALREAD CONVERTED TO THIS DOCUMENT-TYPE
	$documentTypeExists = false;
	if($_POST['convertDocType'] != '' && $_POST['originDocumentNumber'] != '' && $_POST['convertDocType'] != 'LS' && $_POST['originDocumentNumber'] != 'LS'){
		$arrRelatedDocuments = getRelatedDocuments(array($_POST['originDocumentNumber']), '');
		if($arrRelatedDocuments[$_POST['convertDocType']] != ''){
			$documentTypeExists = true;
			$arrThisRedirectUrl = parse_url($_SERVER["HTTP_REFERER"]);
			$thisRedirectUrl = $arrThisRedirectUrl["path"];
			header('location: ' . $thisRedirectUrl . '?error=true&errorOriginDocumentNumber='.$_POST['originDocumentNumber'] . '&errorDocumentType='.$_POST['convertDocType'] . '&errorConvertDocumentNumber='.$arrRelatedDocuments[$_POST['convertDocType']]);
			exit;
		}
	}
	// EOF CHECK IF DOCUMENT WAS ALREAD CONVERTED TO THIS DOCUMENT-TYPE

	// BOF READ VERTRETER
		$arrAgentDatas = getAgents();
		$arrAgentDatas2 = getAgents2();
	// EOF READ VERTRETER

	// BOF READ PAYMENT TYPES
		$arrPaymentTypeDatas = getPaymentTypes();
	// EOF READ PAYMENT TYPES

	// BOF READ DOCUMENT TYPE DATAS
		$arrPaymentStatusTypeDatas = getPaymentStatusTypes();
		#dd('arrPaymentStatusTypeDatas');
		$arrPaymentStatusTypeDatasToShortName = array();
		if(!empty($arrPaymentStatusTypeDatas)){
			foreach($arrPaymentStatusTypeDatas as $thisKey => $thisValue){
				$arrPaymentStatusTypeDatasToShortName[$thisValue["paymentStatusTypesShortName"]] = $thisKey;
			}
		}
	// EOF READ DOCUMENT TYPE DATAS

	// BOF READ PAYMENT CONDITIONS
		$arrPaymentConditionDatas = getPaymentConditions();
	// EOF READ PAYMENT CONDITIONS

	// BOF READ BESTELL ARTEN
		$arrOrderTypeDatas = getOrderTypes();
	// EOF READ BESTELL ARTEN

	// BOF READ ADDITIONAL COSTS
		$arrAdditionalCostsDatas = getAdditionalCosts();
	// EOF READ ADDITIONAL COSTS

	// BOF READ BANK TYPES
		$arrBankAccountTypeDatas = getBankAccountTypes();
	// EOF READ BANK TYPES

	// BOF READ ORDER CATEGORIES
		$arrOrderCategoriesTypeDatas = getOrderCategories();
	// EOF READ ORDER CATEGORIES

	// BOF READ COUNTRIES
		$arrCountryTypeDatas = getCountryTypes();
	// EOF READ COUNTRIES

	// BOF READ DOCUMENT TYPE DATAS
		$arrDocumentTypeDatas = getDocumentTypes();
	// EOF READ DOCUMENT TYPE DATAS

	// BOF READ CUSTOMER GROUP DATAS
		$arrCustomerGroupDatas = getCustomerGroups();
	// EOF READ CUSTOMER GROUP DATAS

	// BOF READ CUSTOMER TYPES
		$arrCustomerTypeDatas = getCustomerTypes();
	// EOF READ CUSTOMER TYPES

	// BOF GET USER DATAS
		$userDatas = getUserDatas();
		if($userDatas["usersLogin"] == 'thorsten'){
			DEFINE('USE_DEBITING_DATE', false);
		}
		else {
			DEFINE('USE_DEBITING_DATE', false);
		}
	// EOF GET USER DATAS

	$thisSubject = $arrDocumentTypeDatas[$_REQUEST["documentType"]]["createdDocumentsTypesName"];

	if($_REQUEST["isCollectiveInvoice"] == '1'){
		$thisSubject = "Sammel-" . $thisSubject;
	}

	if(isset($_REQUEST["searchCustomerNumber"]) && trim($_REQUEST["searchCustomerNumber"]) != '' ) {
		$_REQUEST["editID"] = trim($_REQUEST["searchCustomerNumber"]);
	}
	else if(isset($_REQUEST["searchCustomerNumber"]) && trim($_REQUEST["searchCustomerNumber"]) == '' ) {
		$errorMessage = ' Bitte wählen Sie erst einen Kunden aus. ' . '<br />';
	}
	else if(trim($_REQUEST["editID"] != '' )) {
		$_REQUEST["editID"] = trim($_REQUEST["editID"]);
	}

	// BOF USE DATAS IF BACK BUTTON WAS CLICKED
	if($_REQUEST["returnDatas"] != '') {
		unset($_REQUEST["createDocumentNow"]);
		unset($_POST["createDocumentNow"]);
		if(file_exists($_POST["pdfTempName"])) {
			unlink($_POST["pdfTempName"]);
		}
		clearstatcache();
		// $_REQUEST["editID"] = "";

		$postSerializedDatas = (urldecode($_POST["postSerializedDatas"]));
		$arrPostSerializedDatas = unserialize($postSerializedDatas);

		if($_REQUEST["isCollectiveInvoice"] == '1'){
			$arrReturnedSelectedOrderConfirmations = array();
			if(!empty($arrPostSerializedDatas["selectConfirmationRow"])){
				foreach($arrPostSerializedDatas["selectConfirmationRow"] as $thisConfirmationRowKey => $thisConfirmationRowValue){
					if($thisConfirmationRowValue == '1'){
						$arrReturnedSelectedOrderConfirmations[$thisConfirmationRowKey] = $arrPostSerializedDatas["selectConfirmationDocumentNumber"][$thisConfirmationRowKey];
					}
				}
			}
		}
		else {
			$arrReturnedProductDatas = array();
			$arrSerializedProductDatas = array();
			$arrSerializedSelectedProductDatas = array();

			if(!empty($arrPostSerializedDatas["selectOrdersDetails"])){
				$arrSerializedProductDatas = $arrPostSerializedDatas["selectOrdersDetails"];
				$arrSerializedSelectedProductDatas = $arrPostSerializedDatas["selectOrdersIDs"];
			}
			else if(!empty($arrPostSerializedDatas["addProductDetails"])){
				#$arrSerializedProductDatas = $arrPostSerializedDatas["addProductDetails"];
				$arrSerializedSelectedProductDatas = array();

				$arrUseIDs = array_keys($arrPostSerializedDatas["addProductsCount"]);
				$arrTempDatas = $arrPostSerializedDatas["addProductDetails"];
				$arrSerializedProductDatas = array();
				if(!empty($arrUseIDs) && !empty($arrTempDatas)){
					foreach($arrTempDatas as $thisTempDatasKey => $thisTempDatasValue){
						foreach($arrUseIDs as $thisIdKey){
							$arrSerializedProductDatas[$thisIdKey][$thisTempDatasKey] = $thisTempDatasValue[$thisIdKey];
						}
					}
				}
			}

			if(!empty($arrSerializedProductDatas)){
				foreach($arrSerializedProductDatas as $thisKey => $thisValue) {
					if($thisValue["ordersID"] > 0) {
						$thisArrayKey = $thisValue["ordersID"];
					}
					else {
						$thisArrayKey = $thisKey;
					}
					if($arrSerializedSelectedProductDatas[$thisKey] != ''){
						$arrReturnedProductDatas[$thisArrayKey]["itemChecked"] = '1';
					}

					$arrReturnedProductDatas[$thisArrayKey]["product"] = array(
							"orderDocumentBestellDatum" => $arrSerializedProductDatas[$thisKey]["ordersBestellDatum"],
							"ordersVertreterName" => $arrSerializedProductDatas[$thisKey]["ordersVertreterName"],
							"orderDocumentFreigabeDatum" => $arrSerializedProductDatas[$thisKey]["ordersFreigabeDatum"],
							"orderDocumentDetailID" => '',
							"orderDocumentDetailDocumentID" => '',
							"orderDocumentDetailOrderID" => $arrSerializedProductDatas[$thisKey]["ordersID"],
							"orderDocumentDetailPos" => $countRow,
							"orderDocumentDetailProductNumber" => $arrSerializedProductDatas[$thisKey]["ordersArtikelNummer"],
							"orderDocumentDetailProductName" => $arrSerializedProductDatas[$thisKey]["ordersArtikelBezeichnung"],
							"orderDocumentDetailProductKategorieID" => $arrSerializedProductDatas[$thisKey]["ordersArtikelKategorieID"],
							"orderDocumentDetailProductQuantity" => $arrSerializedProductDatas[$thisKey]["ordersArtikelMenge"],
							"orderDocumentDetailProductColorsText" => ($arrSerializedProductDatas[$thisKey]["ordersArtikelBezeichnungZusatz"]),
							"orderDocumentDetailProductColorsCount" => $arrSerializedProductDatas[$thisKey]["ordersArtikelPrintColorsCount"],
							"orderDocumentDetailsWithBorder" => $arrSerializedProductDatas[$thisKey]["ordersArtikelMitUmrandung"],
							"orderDocumentDetailPrintText" => $arrSerializedProductDatas[$thisKey]["ordersAufdruck"],
							"orderDocumentDetailKommission" => $arrSerializedProductDatas[$thisKey]["ordersKommission"],
							"orderDocumentDetailNotiz" => $arrSerializedProductDatas[$thisKey]["ordersNotiz"],
							// "orderDocumentDetailNotiz" => $arrSerializedProductDatas[$thisKey]["ordersInfo"],
							"orderDocumentPerExpress" => $arrSerializedProductDatas[$thisKey]["orderShippingExpressValue"],
							"orderDocumentDetailProductSinglePrice" => $arrSerializedProductDatas[$thisKey]["ordersSinglePreis"],
							"orderDocumentDetailProductTotalPrice" => '',
							"orderDocumentDetailMwst" => MWST,
							"orderDocumentDetailDiscount" => '',
							"orderDocumentDetailDirectSale" => $arrSerializedProductDatas[$thisKey]["orderPerDirectSale"],
							"orderDocumentDetailPosType" => 'product',
							"orderDocumentDetailOrderType" => $arrSerializedProductDatas[$thisKey]["ordersOrderType"],
							"orderDocumentDetailGroupingID" => $arrSerializedProductDatas[$thisKey]["ordersID"],
							"ordersAdditionalCostsID" => $arrSerializedProductDatas[$thisKey]["ordersAdditionalCostsID"]
						);


					if($arrSerializedProductDatas[$thisKey]["ordersArtikelMenge"] > 0) {

						$arrReturnedProductDatas[$thisArrayKey]["additionalProducts"] = array(
								"orderDocumentBestellDatum" => '',
								"ordersVertreterName" => '',
								"orderDocumentFreigabeDatum" => '',
								"orderDocumentDetailID" => '',
								"orderDocumentDetailDocumentID" => '',
								"orderDocumentDetailOrderID" => $arrSerializedProductDatas[$thisKey]["ordersID"],
								"orderDocumentDetailPos" => $countRow,
								"orderDocumentDetailProductNumber" => $arrSerializedProductDatas[$thisKey]["ordersAdditionalArtikelNummer"],
								"orderDocumentDetailProductName" => $arrSerializedProductDatas[$thisKey]["ordersAdditionalArtikelBezeichnung"],
								"orderDocumentDetailProductKategorieID" => $arrSerializedProductDatas[$thisKey]["ordersAdditionalArtikelKategorieID"],
								"orderDocumentDetailProductQuantity" => $arrSerializedProductDatas[$thisKey]["ordersAdditionalArtikelMenge"],
								"orderDocumentDetailProductColorsText" => '',
								"orderDocumentDetailProductColorsCount" => 0,
								"orderDocumentDetailsWithBorder" => '',
								"orderDocumentDetailPrintText" => '',
								"orderDocumentDetailKommission" => '',
								"orderDocumentDetailNotiz" => '',
								"orderDocumentPerExpress" => $arrSerializedProductDatas[$thisKey]["orderPerShippingExpress"],
								"orderDocumentDetailProductSinglePrice" => '',
								"orderDocumentDetailProductTotalPrice" => '',
								"orderDocumentDetailMwst" => MWST,
								"orderDocumentDetailDiscount" => '',
								"orderDocumentDetailDirectSale" => '',
								"orderDocumentDetailPosType" => 'additionalProducts',
								"orderDocumentDetailOrderType" => '',
								"orderDocumentDetailGroupingID" => $arrSerializedProductDatas[$thisKey]["ordersID"],
								"ordersAdditionalCostsID" => ''
							);
					}

					// if($arrSerializedProductDatas[$thisKey]["ordersOrderType"] == '1' && $arrSerializedProductDatas[$thisKey]["ordersArtikelPrintColorsCount"] > 0) {
					if($arrSerializedProductDatas[$thisKey]["ordersArtikelPrintColorsCount"] > 0) {
						$arrReturnedProductDatas[$thisArrayKey]["printCosts"] = array(
								"orderDocumentBestellDatum" => '',
								"ordersVertreterName" => '',
								"orderDocumentFreigabeDatum" => '',
								"orderDocumentDetailID" => '',
								"orderDocumentDetailDocumentID" => '',
								"orderDocumentDetailOrderID" => $arrSerializedProductDatas[$thisKey]["ordersID"],
								"orderDocumentDetailPos" => $countRow,
								"orderDocumentDetailProductNumber" => "SATZK",
								"orderDocumentDetailProductName" => "einmalige Satzkosten pro Farbe  (entfallen bei Nachbestellung)",
								"orderDocumentDetailProductKategorieID" => '',
								"orderDocumentDetailProductQuantity" => $arrSerializedProductDatas[$thisKey]["orderDocumentDetailProductColorsCount"],
								"orderDocumentDetailProductColorsText" => '',
								"orderDocumentDetailProductColorsCount" => 0,
								"orderDocumentDetailsWithBorder" => '',
								"orderDocumentDetailPrintText" => '',
								"orderDocumentDetailKommission" => '',
								"orderDocumentDetailNotiz" => '',
								"orderDocumentPerExpress" => $arrSerializedProductDatas[$thisKey]["orderPerShippingExpress"],
								"orderDocumentDetailProductSinglePrice" => $arrSerializedProductDatas[$thisKey]["ordersAdditionalCosts"],
								"orderDocumentDetailProductTotalPrice" => '',
								"orderDocumentDetailMwst" => MWST,
								"orderDocumentDetailDiscount" => '',
								"orderDocumentDetailDirectSale" => '',
								"orderDocumentDetailPosType" => 'printCosts',
								"orderDocumentDetailOrderType" => '',
								"orderDocumentDetailGroupingID" => $arrSerializedProductDatas[$thisKey]["ordersID"],
								"ordersAdditionalCostsID" => $arrSerializedProductDatas[$thisKey]["ordersAdditionalCostsID"]
							);
					}

					if($arrSerializedProductDatas[$thisKey]["orderPerShippingExpress"] == '1') {
						$arrReturnedProductDatas[$thisArrayKey]["expressCosts"] = array(
								"orderDocumentBestellDatum" => '',
								"ordersVertreterName" => '',
								"orderDocumentFreigabeDatum" => '',
								"orderDocumentDetailID" => '',
								"orderDocumentDetailDocumentID" => '',
								"orderDocumentDetailOrderID" => $arrSerializedProductDatas[$thisKey]["ordersID"],
								"orderDocumentDetailPos" => $countRow,
								"orderDocumentDetailProductNumber" => "EXP",
								"orderDocumentDetailProductName" => "Expressaufschlag",
								"orderDocumentDetailProductKategorieID" => '',
								"orderDocumentDetailProductQuantity" => $arrSerializedProductDatas[$thisKey]["orderDocumentDetailProductColorsCount"],
								"orderDocumentDetailProductColorsText" => '',
								"orderDocumentDetailProductColorsCount" => 0,
								"orderDocumentDetailsWithBorder" => '',
								"orderDocumentDetailPrintText" => '',
								"orderDocumentDetailKommission" => '',
								"orderDocumentDetailNotiz" => '',
								"orderDocumentPerExpress" => $arrSerializedProductDatas[$thisKey]["orderPerShippingExpress"],
								"orderDocumentDetailProductSinglePrice" => 0.20,
								"orderDocumentDetailProductTotalPrice" => '',
								"orderDocumentDetailMwst" => MWST,
								"orderDocumentDetailDiscount" => '',
								"orderDocumentDetailDirectSale" => '',
								"orderDocumentDetailPosType" => 'expressCosts',
								"orderDocumentDetailOrderType" => '',
								"orderDocumentDetailGroupingID" => $arrSerializedProductDatas[$thisKey]["ordersID"],
								"ordersAdditionalCostsID" => ''
							);
					}
				}

				foreach($arrReturnedProductDatas[$thisArrayKey] as $thisTempKey => $thisTempValue){
					if(is_array($thisTempValue)){
						foreach($thisTempValue as $thisTempKey2 => $thisTempValue2){
							#echo 'en:'.mb_detect_encoding($thisTempValue2) . '<br />';

							if(mb_detect_encoding($thisTempValue2) != "UTF-8"){
								#echo 'en:'.mb_detect_encoding($thisTempValue2) . '<br />';
								#$thisTempValue2 = mb_convert_encoding($thisTempValue2, 'UTF-8', mb_detect_encoding($thisTempValue2));
								#$thisTempValue2 = utf8_decode($thisTempValue2);
								#$thisTempValue2 = htmlentities($thisTempValue2);
								#$thisTempValue2 = "utf8_decode(thisTempValue2)";
								#$thisTempValue2 = mb_detect_encoding($thisTempValue2);
								#$arrReturnedProductDatas[$thisArrayKey][$thisTempKey][$thisTempKey2] = $thisTempValue2;
							}
							else {
								#$arrReturnedProductDatas[$thisArrayKey][$thisTempKey][$thisTempKey2] = (utf8_decode($thisTempValue2));
							}
						}
					}
				}
			}
		}
	}
	// EOF USE DATAS IF BACK BUTTON WAS CLICKED

	if($_REQUEST["cancelDatas"]) {
		$_REQUEST["editID"] = "";
	}

	// BOF GET CUSTOMER DATAS
	if($_REQUEST["editID"] != ""){
		$sql = "SELECT
					`customersID`,
					`customersKundennummer`,
					`customersFirmenname`,
					`customersFirmennameZusatz`,
					`customersFirmenInhaberVorname`,
					`customersFirmenInhaberNachname`,
					`customersFirmenInhaberAnrede`,
					`customersFirmenInhaber2Vorname`,
					`customersFirmenInhaber2Nachname`,
					`customersFirmenInhaber2Anrede`,
					`customersAnsprechpartner1Vorname`,
					`customersAnsprechpartner1Nachname`,
					`customersAnsprechpartner1Anrede`,
					`customersAnsprechpartner2Vorname`,
					`customersAnsprechpartner2Nachname`,
					`customersAnsprechpartner2Anrede`,
					`customersTelefon1`,
					`customersTelefon2`,
					`customersMobil1`,
					`customersMobil2`,
					`customersFax1`,
					`customersFax2`,
					`customersMail1`,
					`customersMail2`,
					`customersHomepage`,
					`customersLieferadresseFirmenname`,
					`customersLieferadresseFirmennameZusatz`,
					`customersLieferadresseStrasse`,
					`customersLieferadresseHausnummer`,
					`customersLieferadressePLZ`,
					`customersLieferadresseOrt`,
					`customersLieferadresseLand`,
					`customersRechnungsadresseFirmenname`,
					`customersRechnungsadresseFirmennameZusatz`,
					`customersRechnungsadresseStrasse`,
					`customersRechnungsadresseHausnummer`,
					`customersRechnungsadressePLZ`,
					`customersRechnungsadresseOrt`,
					`customersRechnungsadresseLand`,
					`customersBankName`,
					`customersBankKontonummer`,
					`customersBankLeitzahl`,
					`customersBankIBAN`,
					`customersBankBIC`,
					`customersBezahlart`,
					`customersZahlungskondition`,
					`customersRabatt`,
					`customersRabattType`,
					`customersVertreterID`,
					`customersUseSalesmanDeliveryAdress`,
					`customersUseSalesmanInvoiceAdress`,
					`customersTyp`,
					`customersGruppe`,
					`customersNotiz`,
					`customersUserID`,
					`customersTimeCreated`,
					`customersActive`,
					`customersDatasUpdated`,
					`customersTaxAccountID`,
					`customersUstID`,
					`customersInvoicesNotPurchased`,
					`customersBadPaymentBehavior`,

					`customersSepaExists`,
					`customersSepaValidUntilDate`,
					`customersSepaRequestedDate`

				 FROM `" . TABLE_CUSTOMERS . "`
				 WHERE `customersKundennummer` = '". $_REQUEST["editID"]. "'
		";
		$rs = $dbConnection->db_query($sql);

		if ($dbConnection->db_getMysqlNumRows($rs) < 1) {
			$errorMessage .= ' Ein Kunde mit der Kundennummer existiert nicht. Bitte legen Sie diesen Kunden erst an. ';
			if($arrGetUserRights["editCustomers"]) {
				$errorMessage .=  '<a href="' . PAGE_EDIT_CUSTOMER . '?editID=NEW" class="linkButton2" title="Neuen Kunden anlegen" >Neukunden anlegen</a>';
				$errorMessage .=  ' <br /><br /><div class="clear"></div>';
			}
			$_REQUEST["editID"] = '';
		}
		else {
			list(
					$customersData["customersID"],
					$customersData["Kundennummer"],
					$customersData["Firmenname"],
					$customersData["FirmennameZusatz"],
					$customersData["FirmenInhaberVorname"],
					$customersData["FirmenInhaberNachname"],
					$customersData["FirmenInhaberAnrede"],
					$customersData["FirmenInhaber2Vorname"],
					$customersData["FirmenInhaber2Nachname"],
					$customersData["FirmenInhaber2Anrede"],
					$customersData["Ansprechpartner1Vorname"],
					$customersData["Ansprechpartner1Nachname"],
					$customersData["Ansprechpartner1Anrede"],
					$customersData["Ansprechpartner2Vorname"],
					$customersData["Ansprechpartner2Nachname"],
					$customersData["Ansprechpartner2Anrede"],
					$customersData["Telefon1"],
					$customersData["Telefon2"],
					$customersData["Mobil1"],
					$customersData["Mobil2"],
					$customersData["Fax1"],
					$customersData["Fax2"],
					$customersData["Mail1"],
					$customersData["Mail2"],
					$customersData["Homepage"],
					$customersData["LieferadresseFirmenname"],
					$customersData["LieferadresseFirmennameZusatz"],
					$customersData["LieferadresseStrasse"],
					$customersData["LieferadresseHausnummer"],
					$customersData["LieferadressePLZ"],
					$customersData["LieferadresseOrt"],
					$customersData["LieferadresseLand"],
					$customersData["RechnungsadresseFirmenname"],
					$customersData["RechnungsadresseFirmennameZusatz"],
					$customersData["RechnungsadresseStrasse"],
					$customersData["RechnungsadresseHausnummer"],
					$customersData["RechnungsadressePLZ"],
					$customersData["RechnungsadresseOrt"],
					$customersData["RechnungsadresseLand"],
					$customersData["BankName"],
					$customersData["BankKontonummer"],
					$customersData["BankLeitzahl"],
					$customersData["BankIBAN"],
					$customersData["BankBIC"],
					$customersData["Bezahlart"],
					$customersData["Zahlungskondition"],
					$customersData["Rabatt"],
					$customersData["RabattType"],
					$customersData["VertreterID"],
					$customersData["UseSalesmanDeliveryAdress"],
					$customersData["UseSalesmanInvoiceAdress"],
					$customersData["Typ"],
					$customersData["Gruppe"],
					$customersData["Notiz"],
					$customersData["UserID"],
					$customersData["TimeCreated"],
					$customersData["Active"],
					$customersData["DatasUpdated"],
					$customersData["TaxAccountID"],
					$customersData["UstID"],
					$customersData["InvoicesNotPurchased"],
					$customersData["BadPaymentBehavior"],

					$customersData["SepaExists"],
					$customersData["SepaValidUntilDate"],
					$customersData["SepaRequestedDate"]

			) = mysqli_fetch_array($rs);

			$customersData["Manager"] =  $customersData["FirmenInhaberVorname"] . ' ' . $customersData["FirmenInhaberNachname"];
			if($customersData["FirmenInhaber2Nachname"] != '') {
				$customersData["Manager"] .= ' & ' . $customersData["FirmenInhaber2Vorname"] . ' ' . $customersData["FirmenInhaber2Nachname"];
			}
			$customersData["Contact"] =  $customersData["Ansprechpartner1Vorname"] . ' ' . $customersData["Ansprechpartner1Nachname"];

			if(!empty($customersData)) {
				foreach($customersData as $thisKey => $thisvalue) {
					$customersData[$thisKey] = ($thisvalue);
				}
			}
		}
	}
	// EOF GET CUSTOMER DATAS

	// BOF GET COPY OR CONVERT DOCUMENT DATAS
	if(($_POST["convertDocType"] != '' || $_POST["copyDocType"] != '' || $_POST["editDocType"] != '') && $_POST["originDocumentID"] && $_POST["originDocumentType"] && $_POST["editID"]) {
		// BOF DEFINE DOCUMENT TABLES DEPENDING ON DOCUMENT TYPE
		if($_POST["originDocumentType"] == 'AN') {
			DEFINE('PAGE_CREATE_LOAD', 'createDocumentOffer.php');
		}
		if($_POST["originDocumentType"] == 'AB') {
			DEFINE('PAGE_CREATE_LOAD', 'createDocumentConfirmation.php');
		}
		if($_POST["originDocumentType"] == 'LS') {
			DEFINE('PAGE_CREATE_LOAD', 'createDocumentDelivery.php');
		}
		if($_POST["originDocumentType"] == 'RE') {
			DEFINE('PAGE_CREATE_LOAD', 'createDocumentInvoice.php');
		}
		if($_POST["originDocumentType"] == 'GU') {
			DEFINE('PAGE_CREATE_LOAD', 'createDocumentCredit.php');
		}
		if($_POST["originDocumentType"] == 'MA') {
			DEFINE('PAGE_CREATE_LOAD', 'createDocumentReminder.php');
		}
		if($_POST["originDocumentType"] == 'M1') {
			DEFINE('PAGE_CREATE_LOAD', 'createDocumentFirstDemand.php');
		}
		if($_POST["originDocumentType"] == 'M2') {
			DEFINE('PAGE_CREATE_LOAD', 'createDocumentSecondDemand.php');
		}
		if($_POST["originDocumentType"] == 'M3') {
			DEFINE('PAGE_CREATE_LOAD', 'createDocumentThirdDemand.php');
		}
		if($_POST["originDocumentType"] == 'BR') {
			DEFINE('PAGE_CREATE_LOAD', 'createDocumentLetter.php');
		}
		DEFINE('TABLE_ORDER_LOAD', constant('TABLE_ORDER_' . $_POST["originDocumentType"]));
		DEFINE('TABLE_ORDER_LOAD_DETAILS', constant('TABLE_ORDER_' . $_POST["originDocumentType"].'_DETAILS'));
		// EOF DEFINE DOCUMENT TABLES DEPENDING ON DOCUMENT TYPE

		$sql = "
			SELECT
				`" . TABLE_ORDER_LOAD . "`.`orderDocumentsID`,
				`" . TABLE_ORDER_LOAD . "`.`orderDocumentsNumber`,
				`" . TABLE_ORDER_LOAD . "`.`orderDocumentsType`,
				`" . TABLE_ORDER_LOAD . "`.`orderDocumentsOrdersIDs`,
				`" . TABLE_ORDER_LOAD . "`.`orderDocumentsCustomerNumber`,
				`" . TABLE_ORDER_LOAD . "`.`orderDocumentsCustomersOrderNumber`,
				`" . TABLE_ORDER_LOAD . "`.`orderDocumentsProcessingDate`,
				`" . TABLE_ORDER_LOAD . "`.`orderDocumentsDocumentDate`,
				`" . TABLE_ORDER_LOAD . "`.`orderDocumentsOrderDate`,
				`" . TABLE_ORDER_LOAD . "`.`orderDocumentsInvoiceDate`,
				`" . TABLE_ORDER_LOAD . "`.`orderDocumentsCreditDate`,
				`" . TABLE_ORDER_LOAD . "`.`orderDocumentsDeliveryDate`,
				`" . TABLE_ORDER_LOAD . "`.`orderDocumentsBindingDate`,
				`" . TABLE_ORDER_LOAD . "`.`orderDocumentsDebitingDate`,
				`" . TABLE_ORDER_LOAD . "`.`orderDocumentsSalesman`,
				`" . TABLE_ORDER_LOAD . "`.`orderDocumentsAddressCompany`,
				`" . TABLE_ORDER_LOAD . "`.`orderDocumentsAddressName`,
				`" . TABLE_ORDER_LOAD . "`.`orderDocumentsAddressStreet`,
				`" . TABLE_ORDER_LOAD . "`.`orderDocumentsAddressStreetNumber`,
				`" . TABLE_ORDER_LOAD . "`.`orderDocumentsAddressZipcode`,
				`" . TABLE_ORDER_LOAD . "`.`orderDocumentsAddressCity`,
				`" . TABLE_ORDER_LOAD . "`.`orderDocumentsAddressCountry`,
				`" . TABLE_ORDER_LOAD . "`.`orderDocumentsAddressMail`,
				`" . TABLE_ORDER_LOAD . "`.`orderDocumentsSumPrice`,
				`" . TABLE_ORDER_LOAD . "`.`orderDocumentsDiscount`,
				`" . TABLE_ORDER_LOAD . "`.`orderDocumentsDiscountType`,
				`" . TABLE_ORDER_LOAD . "`.`orderDocumentsDiscountPercent`,
				`" . TABLE_ORDER_LOAD . "`.`orderDocumentsMwst`,
				`" . TABLE_ORDER_LOAD . "`.`orderDocumentsMwstPrice`,
				`" . TABLE_ORDER_LOAD . "`.`orderDocumentsMwstType`,
				`" . TABLE_ORDER_LOAD . "`.`orderDocumentsShippingCosts`,
				`" . TABLE_ORDER_LOAD . "`.`orderDocumentsShippingCostsPackages`,
				`" . TABLE_ORDER_LOAD . "`.`orderDocumentsShippingCostsPerPackage`,
				`" . TABLE_ORDER_LOAD . "`.`orderDocumentsPackagingCosts`,
				`" . TABLE_ORDER_LOAD . "`.`orderDocumentsCashOnDelivery`,
				`" . TABLE_ORDER_LOAD . "`.`orderDocumentsTotalPrice`,
				`" . TABLE_ORDER_LOAD . "`.`orderDocumentsBankAccount`,
				`" . TABLE_ORDER_LOAD . "`.`orderDocumentsPaymentCondition`,
				`" . TABLE_ORDER_LOAD . "`.`orderDocumentsPaymentType`,
				`" . TABLE_ORDER_LOAD . "`.`orderDocumentsDeliveryType`,
				`" . TABLE_ORDER_LOAD . "`.`orderDocumentsDeliveryTypeNumber`,
				`" . TABLE_ORDER_LOAD . "`.`orderDocumentsSubject`,
				`" . TABLE_ORDER_LOAD . "`.`orderDocumentsContent`,
				`" . TABLE_ORDER_LOAD . "`.`orderDocumentsDocumentPath`,
				`" . TABLE_ORDER_LOAD . "`.`orderDocumentsTimestamp`,
				`" . TABLE_ORDER_LOAD . "`.`orderDocumentsStatus`,
				`" . TABLE_ORDER_LOAD . "`.`orderDocumentsSendMail`,

				`" . TABLE_ORDER_LOAD . "`.`orderDocumentsIsCollectiveInvoice`,

				`" . TABLE_ORDER_LOAD_DETAILS . "`.`orderDocumentDetailID`,
				`" . TABLE_ORDER_LOAD_DETAILS . "`.`orderDocumentDetailDocumentID`,
				`" . TABLE_ORDER_LOAD_DETAILS . "`.`orderDocumentDetailOrderID`,
				`" . TABLE_ORDER_LOAD_DETAILS . "`.`orderDocumentDetailOrderType`,
				`" . TABLE_ORDER_LOAD_DETAILS . "`.`orderDocumentDetailPos`,
				`" . TABLE_ORDER_LOAD_DETAILS . "`.`orderDocumentDetailProductNumber`,
				`" . TABLE_ORDER_LOAD_DETAILS . "`.`orderDocumentDetailProductName`,
				`" . TABLE_ORDER_LOAD_DETAILS . "`.`orderDocumentDetailProductKategorieID`,
				`" . TABLE_ORDER_LOAD_DETAILS . "`.`orderDocumentDetailProductQuantity`,
				`" . TABLE_ORDER_LOAD_DETAILS . "`.`orderDocumentDetailProductColorsText`,
				`" . TABLE_ORDER_LOAD_DETAILS . "`.`orderDocumentDetailProductColorsCount`,
				`" . TABLE_ORDER_LOAD_DETAILS . "`.`orderDocumentDetailsWithBorder`,
				`" . TABLE_ORDER_LOAD_DETAILS . "`.`orderDocumentDetailPrintText`,
				`" . TABLE_ORDER_LOAD_DETAILS . "`.`orderDocumentDetailKommission`,
				`" . TABLE_ORDER_LOAD_DETAILS . "`.`orderDocumentDetailNotiz`,
				`" . TABLE_ORDER_LOAD_DETAILS . "`.`orderDocumentDetailProductSinglePrice`,
				`" . TABLE_ORDER_LOAD_DETAILS . "`.`orderDocumentDetailProductTotalPrice`,
				`" . TABLE_ORDER_LOAD_DETAILS . "`.`orderDocumentDetailMwst`,
				`" . TABLE_ORDER_LOAD_DETAILS . "`.`orderDocumentDetailDiscount`,
				`" . TABLE_ORDER_LOAD_DETAILS . "`.`orderDocumentDetailDirectSale`,
				`" . TABLE_ORDER_LOAD_DETAILS . "`.`orderDocumentDetailPosType`,
				`" . TABLE_ORDER_LOAD_DETAILS . "`.`orderDocumentDetailGroupingID`,

				`" . TABLE_ORDERS . "`.`ordersReklamationsgrund`

				FROM `" . TABLE_ORDER_LOAD . "`

				LEFT JOIN `" . TABLE_ORDER_LOAD_DETAILS . "`
				ON(`" . TABLE_ORDER_LOAD . "`.`orderDocumentsID` = `" . TABLE_ORDER_LOAD_DETAILS . "`.`orderDocumentDetailDocumentID`)

				LEFT JOIN `" . TABLE_ORDERS . "`
				ON(`" . TABLE_ORDER_LOAD_DETAILS . "`.`orderDocumentDetailOrderID` = `" . TABLE_ORDERS . "`.`ordersID`)

				WHERE `" . TABLE_ORDER_LOAD . "`.`orderDocumentsID` = '" . trim($_POST["originDocumentID"]) . "'
					AND `" . TABLE_ORDER_LOAD . "`.`orderDocumentsCustomerNumber` = '" . trim($_POST["editID"]) . "'
		";

		$rs = $dbConnection->db_query($sql);

		$documentContainsComplaint = false;
		while($ds = mysqli_fetch_assoc($rs)) {
			foreach(array_keys($ds) as $field) {
				if(preg_match("/^orderDocuments/", $field)) {
					$arrLoadDocumentDatas[$field] = $ds[$field];
				}
				else {
					$arrLoadDocumentDetailDatas[$ds["orderDocumentDetailOrderID"]][$ds["orderDocumentDetailPosType"]][$field] = ($ds[$field]);
				}
				if($ds["orderDocumentsIsCollectiveInvoice"] == '1'){
					$_REQUEST["isCollectiveInvoice"] = '1';
				}
			}
			$arrLoadDocumentDetailGroupIDs[] = $ds["orderDocumentDetailOrderID"];
			if($ds["ordersReklamationsgrund"] != ''){
				$documentContainsComplaint = true;
			}
		}
		if($documentContainsComplaint && $_REQUEST["convertDocType"] == 'MA'){
			$jswindowMessage .= ' ACHTUNG: Die Rechnung enthält eine reklamierte Bestellung! ';
			$warningMessage .= ' ACHTUNG: Die Rechnung enth&auml;lt eine reklamierte Bestellung! ' . "<br />";
		}
		if(!empty($arrLoadDocumentDetailGroupIDs)) {
			$arrLoadDocumentDetailGroupIDs = array_unique($arrLoadDocumentDetailGroupIDs);
		}
		if($_REQUEST["isCollectiveInvoice"] == '1'){
			require_once('inc/formAddOrderConfirmations.inc.php');
			require_once('inc/loadCollectiveInvoiceDatas.inc.php');
		}
	}
	// EOF GET COPY OR CONVERT DOCUMENT DATAS

	// BOF GET SALESMAN ADRESS
	// if($customersData["VertreterID"] != '' || $customersData["UseSalesmanDeliveryAdress"] == '1' || $customersData["UseSalesmanInvoiceAdress"] == '1') {
	if($_POST["selectSalesman"] != "" || $customersData["VertreterID"] != '' || $customersData["UseSalesmanDeliveryAdress"] == '1' || $customersData["UseSalesmanInvoiceAdress"] == '1') {
		$thisSalesmanID = $customersData["VertreterID"];
		if($arrLoadDocumentDatas["orderDocumentsSalesman"] > 0) {
			$thisSalesmanID = $arrLoadDocumentDatas["orderDocumentsSalesman"];
		}
		if($_POST["selectSalesman"] != "") {
			$thisSalesmanID = $_POST["selectSalesman"];
		}

		$sql = "SELECT
						`customersID`,
						`customersKundennummer`,
						`customersFirmenname`,
						`customersFirmennameZusatz`,
						`customersFirmenInhaberVorname`,
						`customersFirmenInhaberNachname`,
						`customersFirmenInhaberAnrede`,

						`customersAnsprechpartner1Vorname`,
						`customersAnsprechpartner1Nachname`,
						`customersAnsprechpartner1Anrede`,
						`customersAnsprechpartner2Vorname`,
						`customersAnsprechpartner2Nachname`,
						`customersAnsprechpartner2Anrede`,

						`customersCompanyStrasse`,
						`customersCompanyHausnummer`,
						`customersCompanyCountry`,
						`customersCompanyPLZ`,
						`customersCompanyOrt`,
						IF(`customersMail1` != '', `customersMail1`, `customersMail2`) AS `customersMail`,
						IF(`customersFax1` != '', `customersFax1`, `customersFax2`) AS `customersFax`,

						`customersLieferadresseFirmenname`,
						`customersLieferadresseFirmennameZusatz`,
						`customersLieferadresseStrasse`,
						`customersLieferadresseHausnummer`,
						`customersLieferadressePLZ`,
						`customersLieferadresseOrt`,
						`customersLieferadresseLand`,
						`customersRechnungsadresseFirmenname`,
						`customersRechnungsadresseFirmennameZusatz`,
						`customersRechnungsadresseStrasse`,
						`customersRechnungsadresseHausnummer`,
						`customersRechnungsadressePLZ`,
						`customersRechnungsadresseOrt`,
						`customersRechnungsadresseLand`,
						`customersDatasUpdated`,

						`customersBankName`,
						`customersBankKontonummer`,
						`customersBankLeitzahl`,
						`customersBankIBAN`,
						`customersBankBIC`,

						`customersBezahlart`,
						`customersZahlungskondition`,
						`customersRabatt`,
						`customersRabattType`,
						`customersTyp`,

						`customersTaxAccountID`,
						`customersUstID`,

						`customersInvoicesNotPurchased`,
						`customersBadPaymentBehavior`,

						`customersSepaExists`,
						`customersSepaValidUntilDate`,
						`customersSepaRequestedDate`


						FROM `" . TABLE_CUSTOMERS . "`

						WHERE `customersID` = '".$thisSalesmanID."'

						LIMIT 1
		";

		$rs = $dbConnection->db_query($sql);

		while($ds = mysqli_fetch_assoc($rs)) {
			foreach(array_keys($ds) as $field) {
				$salesmanDatas["salesmanKundennummer"] = $ds["customersKundennummer"];
				$salesmanDatas["salesmanTaxAccountID"] = $ds["customersTaxAccountID"];
				$salesmanDatas["salesmanUstID"] = $ds["customersUstID"];
				$salesmanDatas["salesmanInvoicesNotPurchased"] = $ds["customersInvoicesNotPurchased"];
				$salesmanDatas["salesmanBadPaymentBehavior"] = $ds["customersBadPaymentBehavior"];
				$salesmanDatas["salesmanFirmenname"] = $ds["customersFirmenname"];
				$salesmanDatas["salesmanFirmennameZusatz"] = $ds["customersFirmennameZusatz"];
				$salesmanDatas["salesmanMail"] = $ds["customersMail"];
				$salesmanDatas["salesmanFax"] = $ds["customersFax"];
				$salesmanDatas["salesmanBezahlart"] = $ds["customersBezahlart"];
				$salesmanDatas["salesmanZahlungskondition"] = $ds["customersZahlungskondition"];
				$salesmanDatas["salesmanRabatt"] = $ds["customersRabatt"];
				$salesmanDatas["salesmanRabattType"] = $ds["customersRabattType"];
				$salesmanDatas["salesmanTyp"] = $ds["customersTyp"];
				$salesmanDatas["salesmanManager"] =  $ds["customersFirmenInhaberVorname"] . ' ' . $ds["customersFirmenInhaberNachname"];
				$salesmanDatas["salesmanContact"] =  $ds["customersAnsprechpartner1Vorname"] . ' ' . $ds["customersAnsprechpartner1Nachname"];

				$salesmanDatas["salesmanSepaExists"] = $ds["customersSepaExists"];
				$salesmanDatas["salesmanSepaValidUntilDate"] = $ds["customersSepaValidUntilDate"];
				$salesmanDatas["salesmanSepaRequestedDate"] = $ds["customersSepaRequestedDate"];

				if($customersData["UseSalesmanDeliveryAdress"] == '1') {
					$salesmanDatas["customersLieferadresseFirmenname"] = $ds["customersLieferadresseFirmenname"];
					$salesmanDatas["customersLieferadresseFirmennameZusatz"] = $ds["customersLieferadresseFirmennameZusatz"];
					$salesmanDatas["customersLieferadresseStrasse"] = $ds["customersLieferadresseStrasse"];
					$salesmanDatas["customersLieferadresseHausnummer"] = $ds["customersLieferadresseHausnummer"];
					$salesmanDatas["customersLieferadressePLZ"] = $ds["customersLieferadressePLZ"];
					$salesmanDatas["customersLieferadresseOrt"] = $ds["customersLieferadresseOrt"];
					$salesmanDatas["customersLieferadresseLand"] = $ds["customersLieferadresseLand"];
				}
				if($customersData["UseSalesmanInvoiceAdress"] == '1') {
					$salesmanDatas["customersRechnungsadresseFirmenname"] = $ds["customersRechnungsadresseFirmenname"];
					$salesmanDatas["customersRechnungsadresseFirmennameZusatz"] = $ds["customersRechnungsadresseFirmennameZusatz"];
					$salesmanDatas["customersRechnungsadresseStrasse"] = $ds["customersRechnungsadresseStrasse"];
					$salesmanDatas["customersRechnungsadresseHausnummer"] = $ds["customersRechnungsadresseHausnummer"];
					$salesmanDatas["customersRechnungsadressePLZ"] = $ds["customersRechnungsadressePLZ"];
					$salesmanDatas["customersRechnungsadresseOrt"] = $ds["customersRechnungsadresseOrt"];
					$salesmanDatas["customersRechnungsadresseLand"] = $ds["customersRechnungsadresseLand"];

					$salesmanDatas["customersBankName"] = $ds["customersBankName"];
					$salesmanDatas["customersBankKontonummer"] = $ds["customersBankKontonummer"];
					$salesmanDatas["customersBankLeitzahl"] = $ds["customersBankLeitzahl"];
					$salesmanDatas["customersBankIBAN"] = $ds["customersBankIBAN"];
					$salesmanDatas["customersBankBIC"] = $ds["customersBankBIC"];
					$salesmanDatas["customersTaxAccountID"] = $ds["customersTaxAccountID"];
					$salesmanDatas["customersUstID"] = $ds["customersUstID"];
				}
			}
		}
	}
	// BOF GET SALESMAN ADRESS


	// BOF READ DOCUMENT DATAS
	if($_REQUEST["editID"] != "" && !isset($_POST["storeDatas"]) && !isset($_POST["storeAndSendDatas"])){

		$loadedAdressDatas = array(
			"ADRESS_COMPANY_CUSTOMER_NUMBER" => $_POST["selectCustomersRecipientName"],

			"ADRESS_COMPANY" => $_POST["selectCustomersRecipientName"],
			"ADRESS_MANAGER" => "",
			"ADRESS_CONTACT" => "",
			"ADRESS_STREET" => $_POST["selectCustomersRecipientStreet"].' '.$_POST["selectCustomersRecipientStreetNumber"],
			"ADRESS_CITY" => $_POST["selectCustomersRecipientZipcode"].' '.$_POST["selectCustomersRecipientCity"],
			"ADRESS_COUNTRY" => $arrCountryTypeDatas[$_POST["selectCustomersRecipientCountry"]]["countries_name"],
			"ADRESS_MAIL" => $_POST["selectCustomersRecipientMail"],
			"ADRESS_FAX" => formatPhoneNumber($_POST["selectCustomersRecipientFax"],''),
		);
		if($_POST["selectCustomersRecipientNameAdd"] != '') {
			$loadedAdressDatas["ADRESS_COMPANY"] .= '<br>' . $_POST["selectCustomersRecipientNameAdd"];
		}

		if($_POST["selectOrdersIDsFirstIndex"] != '') {
			$selectOrdersIDsFirstIndex = $_POST["selectOrdersIDsFirstIndex"];
		}
		else {
			$selectOrdersIDsFirstIndex = (key($_POST["selectOrdersIDs"])!==false) ? key($_POST["selectOrdersIDs"]) : null;
		}

		if($_POST["selectCustomersRecipientManager"] != "") {
			$loadedAdressDatas["ADRESS_MANAGER"] = 'Inh. ' . $_POST["selectCustomersRecipientManager"];
		}
		if($_POST["selectCustomersRecipientContact"] != "") {
			$loadedAdressDatas["ADRESS_CONTACT"] = 'z. Hd. ' . $_POST["selectCustomersRecipientContact"];
		}

		$loadedDocumentDatas = array(
			// "CUSTOMER_NUMBER" => $_POST["selectCustomersNumber"],
			"CUSTOMER_PHONE" => $customersData["Telefon1"],
			"CUSTOMER_MAIL" => $customersData["Mail1"],
			"CUSTOMERS_ORDER_NUMBER" => $_POST["selectCustomersOrderNumber"],

			"SHIPPING_COSTS" => number_format($_POST["selectShippingCosts"], 2, '.', ''),
			"SHIPPING_TYPE" => $_POST["selectDeliveryType"],
			"SHIPPING_TYPE_NUMBER" => $_POST["selectDeliveryTypeNumber"],
			"PACKAGING_COSTS" => number_format($_POST["selectPackagingCosts"], 2, '.', ''),
			"CARD_CASH_ON_DELIVERY_VALUE" => number_format(SHIPPING_CASH_ON_DELIVERY, 2, '.', ''),
			"SHIPPING_DATE" => $_POST["selectDeliveryDate"],
			"BINDING_DATE" => $_POST["selectBindingDate"],
			"INVOICE_DATE" => $_POST["selectInvoiceDate"],

			"COMPANY_HEADER_IMAGE" => DIRECTORY_PDF_IMAGES . "briefkopf.jpg",
			"COMPANY_NAME_LONG" => COMPANY_NAME_LONG,
			"COMPANY_PHONE" => COMPANY_PHONE,
			"COMPANY_FAX" => COMPANY_FAX,
			"COMPANY_INTERNET" => COMPANY_INTERNET,
			"COMPANY_MANAGER" => COMPANY_MANAGER,
			"COMPANY_MAIL" => COMPANY_MAIL,
			"COMPANY_STREET_NAME" => COMPANY_STREET_NAME,
			"COMPANY_STREET_NUMBER" => COMPANY_STREET_NUMBER,
			"COMPANY_ZIPCODE" => COMPANY_ZIPCODE,
			"COMPANY_CITY" => COMPANY_CITY,
			"COMPANY_TAX_NUMBER" => COMPANY_TAX_NUMBER,
			"COMPANY_UID_NUMBER" => COMPANY_UID_NUMBER,
			"COMPANY_VENUE" => COMPANY_VENUE,

			"BANK_ACCOUNT_TYPE" => $arrBankAccountTypeDatas[$_POST["selectBankAccount"]]["bankAccountTypesShortName"],
			"BANK_ACCOUNT_CODE" => $arrBankAccountTypeDatas[$_POST["selectBankAccount"]]["bankAccountTypesBankCode"],
			"BANK_ACCOUNT_NAME" => $arrBankAccountTypeDatas[$_POST["selectBankAccount"]]["bankAccountTypesName"],
			"BANK_ACCOUNT_NUMBER" => $arrBankAccountTypeDatas[$_POST["selectBankAccount"]]["bankAccountTypesAccountNumber"],
			"BANK_ACCOUNT_IBAN" => $arrBankAccountTypeDatas[$_POST["selectBankAccount"]]["bankAccountTypesIBAN"],
			"BANK_ACCOUNT_BIC" => $arrBankAccountTypeDatas[$_POST["selectBankAccount"]]["bankAccountTypesBIC"],
			"INTRO_TEXT" => $arrContentDatas[$_REQUEST["documentType"]]["INTRO_TEXT"],
			"OUTRO_TEXT" => $arrContentDatas[$_REQUEST["documentType"]]["OUTRO_TEXT"],
			"EDITOR_TEXT" => $arrContentDatas[$_REQUEST["documentType"]]["EDITOR_TEXT"],
			"REGARDS_TEXT" => $arrContentDatas[$_REQUEST["documentType"]]["REGARDS_TEXT"],
			"SALUTATION_TEXT" => $arrContentDatas[$_REQUEST["documentType"]]["SALUTATION_TEXT"],
			"PAYMENT_CONDITIONS" => '<br>' . $arrContentDatas[$_REQUEST["documentType"]]["PAYMENT_CONDITIONS"],
			"PAYMENT_CONDITION" => '<span class="important">' . $arrPaymentConditionDatas[$_POST["selectPaymentCondition"]]["paymentConditionsName"] . '</span>',
			"PAYMENT_TYPE" => '<span class="important">' . $arrPaymentTypeDatas[$_POST["selectPaymentType"]]["paymentTypesName"] . '</span>',
		);

		if($_POST["selectCashOnDelivery"] == 0 && $_POST["selectPaymentType"] == 3){
			$loadedDocumentDatas["PAYMENT_TYPE"] .= ' <span class="important">(ohne Nachnahme-Geb&uuml;hr)</span>';
		}

		if($_POST["selectPaymentType"] == 2 && $_POST["selectPaymentStatus"] != '2' && (($_POST["editDocType"] == 'RE' || $_REQUEST["documentType"] == 'RE') || ($_POST["editDocType"] == 'AB' || $_REQUEST["documentType"] == 'AB')) && $_POST["convertDocType"] == ''){
			if($_POST["selectPaymentCondition"] != '9'){
				$loadedDocumentDatas["PAYMENT_TYPE"] .= '. Bitte &uuml;berweisen Sie den Zahlungsbetrag bis zum {###ADVANCE_PAYMENT_DEADLINE_DATE###}.<br>Andernfalls wird die Ware per Nachnahme (plus ' . number_format(SHIPPING_CASH_ON_DELIVERY, 2, ",", "") . ' Euro Geb&uuml;hr) verschickt.';
			}
			else if($_POST["selectPaymentCondition"] == '9'){
				$loadedDocumentDatas["PAYMENT_TYPE"] .= '. Bitte &uuml;berweisen Sie den Zahlungsbetrag bis zum {###ADVANCE_PAYMENT_DEADLINE_DATE###}.<br>Andernfalls wird der Auftrag storniert.';
				#$loadedDocumentDatas["PAYMENT_TYPE"] .= ' Bei Stornierungen, die vor der Druckfreigabe der von uns geleisteten Grafikarbeiten erfolgen, berechnen wir eine Stornogeb&uuml;hr in H&ouml;he von ' . CANCELLATION_FEE . '';
				$loadedDocumentDatas["PAYMENT_TYPE"] .= ' Falls Grafikarbeiten geleistet wurden, berechnen wir eine Stornogeb&uuml;hr in H&ouml;he von ' . CANCELLATION_FEE . '';
			}
		}

		if($_POST["editDocType"] == 'GU' || $_POST["convertDocType"] == 'GU' || $_REQUEST["documentType"] == 'GU'){
			$loadedDocumentDatas["PAYMENT_CONDITIONS"] = '';
			$loadedDocumentDatas["PAYMENT_CONDITION"] = '';
			$loadedDocumentDatas["PAYMENT_TYPE"] = '';
		}

		#echo $loadedDocumentDatas["PAYMENT_TYPE"];
		if($_POST["documentType"] == "BR") {
			$loadedDocumentDatas["SALUTATION_TEXT"] = '';
			$loadedDocumentDatas["REGARDS_TEXT"] = '';
		}


		if($_POST["selectPaymentStatus"] != "") {
			#$loadedDocumentDatas["PAYMENT_TYPE"] = $arrPaymentTypeDatas[$_POST["selectPaymentStatus"]]["paymentTypesName"];
		}

		$loadedDocumentDatas["INVOICE_SKONTO"] = "";

		$loadedDocumentDatas["WARNING_INVOICE_IS_PAID"] = "";
		if($_POST["selectPaymentStatus"] == '2') {
			#$loadedDocumentDatas["WARNING_INVOICE_IS_PAID"] = "<p><b>ACHTUNG:</b> Die Rechnung ist schon per Vorkasse bezahlt!</p>";
			#if($_POST["convertDocType"] != '' && ($_POST["convertDocType"] == 'RE' || $_POST["convertDocType"] == 'LS')){
			#if($_POST["convertDocType"] != '' && ($_POST["convertDocType"] == 'RE' || $_POST["convertDocType"] == 'LS')){
			if($_POST["convertDocType"] != '' && ($_POST["convertDocType"] == 'AB' || $_POST["convertDocType"] == 'RE' || $_POST["convertDocType"] == 'LS')){
				$loadedDocumentDatas["WARNING_INVOICE_IS_PAID"] = '<p><span class="adressAdditional"><b>ACHTUNG:</b> Die Rechnung ist schon per Vorkasse bezahlt!</span></p>';
			}
			else if($_POST["convertDocType"] != '' && ($_POST["convertDocType"] == 'GU')){
				$loadedDocumentDatas["WARNING_INVOICE_IS_PAID"] = '<p><span class="adressAdditional"><b>ACHTUNG:</b> Die Rechnung gilt als bezahlt!</span></p>';
			}
			else if($_POST["editDocType"] != '' && ($_POST["editDocType"] == 'AB' || $_POST["editDocType"] == 'RE' || $_POST["editDocType"] == 'LS')){
				$loadedDocumentDatas["WARNING_INVOICE_IS_PAID"] = '<p><span class="adressAdditional"><b>ACHTUNG:</b> Die Rechnung ist schon bezahlt!</span></p>';
			}
			else if($_POST["editDocType"] != '' && ($_POST["editDocType"] == 'GU')){
				$loadedDocumentDatas["WARNING_INVOICE_IS_PAID"] = '<p><span class="adressAdditional"><b>ACHTUNG:</b> Die Rechnung ist bezahlt!</span></p>';
			}
			else if($_POST["documentType"] == 'RE' && $_POST["editDocType"] == '' && $_POST["convertDocType"] == ''){
				$loadedDocumentDatas["WARNING_INVOICE_IS_PAID"] = '<p><span class="adressAdditional"><b>ACHTUNG:</b> Die Rechnung ist schon bezahlt!</span></p>';
			}
		}

		if($_POST["selectOrderDate"] != "") {
			$loadedDocumentDatas["ORDER_DATE"] = $_POST["selectOrderDate"];
		}
		else if($arrLoadDocumentDatas["orderDocumentsOrderDate"] != "") {
			$loadedDocumentDatas["ORDER_DATE"] = formatDate($arrLoadDocumentDatas["orderDocumentsOrderDate"], 'display');
		}
		else if(formatDate($_POST["selectOrdersDetails"][$_POST["selectOrdersIDs"][$selectOrdersIDsFirstIndex]]["ordersFreigabeDatum"], "display") != ''){
			$loadedDocumentDatas["ORDER_DATE"] = formatDate($_POST["selectOrdersDetails"][$_POST["selectOrdersIDs"][$selectOrdersIDsFirstIndex]]["ordersFreigabeDatum"], "display");
		}
		else {
			$loadedDocumentDatas["ORDER_DATE"] = formatDate($_POST["selectOrdersDetails"][$_POST["selectOrdersIDs"][$selectOrdersIDsFirstIndex]]["ordersBestellDatum"], "display");
		}

		$loadedDocumentDatas["DOCUMENT_DATE"] = $_POST["selectDocumentDate"];
		$arrTemp = explode('.', $_POST["selectDocumentDate"]);
		$loadedDocumentDatas["DEMAND_DEADLINE_DATE"] = date("d.m.Y", mktime(0, 0, 0, $arrTemp[1], ($arrTemp[0] + 7), $arrTemp[2]));
		$loadedDocumentDatas["ADVANCE_PAYMENT_DEADLINE_DATE"] = date("d.m.Y", mktime(0, 0, 0, $arrTemp[1], ($arrTemp[0] + 28), $arrTemp[2]));

		if($_POST["selectPaymentCondition"] == '9'){
			$loadedDocumentDatas["ADVANCE_PAYMENT_DEADLINE_DATE"] = date("d.m.Y", mktime(0, 0, 0, $arrTemp[1], ($arrTemp[0] + $arrPaymentConditionDatas[$_POST["selectPaymentCondition"]]["paymentConditionsDaysLimit"]), $arrTemp[2]));
		}

		#$loadedDocumentDatas["CREATION_DATE"] = date("d.m.Y");
		$loadedDocumentDatas["CREATION_DATE"] = $_POST["selectDocumentDate"];

		$loadedDocumentDatas["DOCUMENT_NUMBER_AN"] = "";
		$loadedDocumentDatas["DOCUMENT_NUMBER_AB"] = "";
		$loadedDocumentDatas["DOCUMENT_NUMBER_LS"] = "";
		$loadedDocumentDatas["DOCUMENT_NUMBER_RE"] = "";
		$loadedDocumentDatas["DOCUMENT_NUMBER_MA"] = "";
		$loadedDocumentDatas["DOCUMENT_NUMBER_M1"] = "";
		$loadedDocumentDatas["DOCUMENT_NUMBER_M2"] = "";
		$loadedDocumentDatas["DOCUMENT_NUMBER_M3"] = "";
		$loadedDocumentDatas["DOCUMENT_NUMBER_IK"] = "";
		$loadedDocumentDatas["DOCUMENT_NUMBER_GU"] = "";
		$loadedDocumentDatas["DOCUMENT_NUMBER_BR"] = "";

		// BOF RELATION DOCUMENTS
		if($_POST["convertDocType"] != '' || $_POST["copyDocType"] != '' || $_POST["editDocType"] != '') {
			$arrRelatedDocumentNumbers = getRelatedDocuments(array($_POST["originDocumentNumber"]));
			#dd('arrRelatedDocumentNumbers');

			$loadedDocumentDatas["DOCUMENT_NUMBER_" . substr($_POST["originDocumentNumber"], 0, 2)] = $_POST["originDocumentNumber"];
			if(!empty($arrRelatedDocumentNumbers)){
				$arrRelatedDocumentNumbers = array_unique($arrRelatedDocumentNumbers);
				foreach($arrRelatedDocumentNumbers as $thisKey => $thisValue){
					$loadedDocumentDatas["DOCUMENT_NUMBER_" . $thisKey] = $thisValue;
				}
			}
		}
		// EOF RELATION DOCUMENTS

		$loadedDocumentDatas["CUSTOMER_VAT_ID"] = '';
		$loadedDocumentDatas["CUSTOMERS_TAX_ACCOUNT"] = '';

		if((int)$customersData["VertreterID"] > 0 && $customersData["VertreterID"] != 13301 )  {
			// CHANGED 2013-06-11 thorsten
			$loadedDocumentDatas["CUSTOMERS_SALESMAN"] = trim($salesmanDatas["salesmanFirmenname"]);

			// if($salesmanDatas["salesmanTyp"] == '3') {
			if($customersData["UseSalesmanInvoiceAdress"] == '1') {
				$loadedDocumentDatas["CUSTOMERS_TAX_ACCOUNT"] = trim($salesmanDatas["salesmanTaxAccountID"]);
				if($salesmanDatas["customersRechnungsadresseLand"] > 0 && $salesmanDatas["customersRechnungsadresseLand"] != '81'){
					$loadedDocumentDatas["CUSTOMER_VAT_ID"] = trim($salesmanDatas["customersUstID"]);
				}
				$loadedDocumentDatas["CUSTOMER_NUMBER"] = trim($salesmanDatas["salesmanKundennummer"]);
				$loadedDocumentDatas["SALESMANS_CUSTOMER"] = trim($customersData["Firmenname"]);
				if($customersData["FirmennameZusatz"] != '') {
					$loadedDocumentDatas["SALESMANS_CUSTOMER"] .= ' ' . $customersData["FirmennameZusatz"]. '';
				}
				$loadedDocumentDatas["SALESMANS_CUSTOMER"] = wordwrap($loadedDocumentDatas["SALESMANS_CUSTOMER"], 20, "<br />", false);
				$loadedDocumentDatas["SALESMANS_CUSTOMER"] .= ' (' . $customersData["Kundennummer"] . ')';
			}
			else {
				$loadedDocumentDatas["CUSTOMERS_TAX_ACCOUNT"] = trim($customersData["TaxAccountID"]);
				if($customersData["RechnungsadresseLand"] > 0 && $customersData["RechnungsadresseLand"] == '81'){
					$loadedDocumentDatas["CUSTOMER_VAT_ID"] = trim($customersData["UstID"]);
				}
				$loadedDocumentDatas["CUSTOMER_NUMBER"] = $_POST["selectCustomersNumber"];
				$loadedDocumentDatas["SALESMANS_CUSTOMER"] = '';
			}
		}
		else {
			$loadedDocumentDatas["CUSTOMERS_SALESMAN"] = '';
			if($_POST["selectSalesman"] > 0){
				$loadedDocumentDatas["CUSTOMERS_SALESMAN"] = $arrAgentDatas2[$_POST["selectSalesman"]]["customersFirmenname"];
				// $arrAgentDatas2[$thisKey]["customersKundennummer"]
			};
			$loadedDocumentDatas["SALESMANS_CUSTOMER"] = '';
			$loadedDocumentDatas["CUSTOMER_NUMBER"] = $_POST["selectCustomersNumber"];
			$loadedDocumentDatas["CUSTOMERS_TAX_ACCOUNT"] = trim($customersData["TaxAccountID"]);
			if($customersData["RechnungsadresseLand"] > 0 && $customersData["RechnungsadresseLand"] != '81'){
				$loadedDocumentDatas["CUSTOMER_VAT_ID"] = trim($customersData["UstID"]);
			}
			#displayDebug('customersData');
		}
		$loadedDocumentDatas["ADDRESS_INVOICE"] = "";
		$loadedDocumentDatas["ADDRESS_DELIVERY"] = "";

		if($_REQUEST["documentType"] == "LS") {
			$loadedDocumentDatas["ADDRESS_INVOICE"] .= '<span class="adressAdditional"><b>Rechnungsadresse:</b> ';
			/*
			if($customersData["UseSalesmanInvoiceAdress"] == '1') {
				$loadedDocumentDatas["ADDRESS_INVOICE"] .=	$salesmanDatas["salesmanFirmenname"] . ' ' . $salesmanDatas["salesmanFirmennameZusatz"] . ' &middot; ' . $salesmanDatas["customersRechnungsadresseStrasse"] . ' ' . $salesmanDatas["customersRechnungsadresseHausnummer"] . ' &middot; ' .
															$salesmanDatas["customersRechnungsadressePLZ"] . ' ' . $salesmanDatas["customersRechnungsadresseOrt"] . ' &middot; ' .
															$arrCountryTypeDatas[$salesmanDatas["customersRechnungsadresseLand"]]["countries_name"];
			}
			else {
				$loadedDocumentDatas["ADDRESS_INVOICE"] .=  $customersData["RechnungsadresseFirmenname"] . ' ' . $customersData["RechnungsadresseFirmennameZusatz"] . ' &middot; ' . $customersData["RechnungsadresseStrasse"] . ' ' . $customersData["RechnungsadresseHausnummer"] . ' &middot; ' .
															$customersData["RechnungsadressePLZ"] . ' ' . $customersData["RechnungsadresseOrt"] . ' &middot; ' .
															$arrCountryTypeDatas[$customersData["RechnungsadresseLand"]]["countries_name"];
			}
			*/

			$loadedDocumentDatas["ADDRESS_INVOICE"] .= $_REQUEST["selectCustomersSecondaryRecipientName"];
			if(trim($_REQUEST["selectCustomersSecondaryRecipientNameAdd"]) != '') { $loadedDocumentDatas["ADDRESS_INVOICE"] .= ' &middot; ' . $_REQUEST["selectCustomersSecondaryRecipientNameAdd"]; }
			if(trim($_REQUEST["selectCustomersSecondaryRecipientContact"]) != '') { $loadedDocumentDatas["ADDRESS_INVOICE"] .= ' &middot; ' . $_REQUEST["selectCustomersSecondaryRecipientContact"]; }
			if(trim($_REQUEST["selectCustomersSecondaryRecipientManager"]) != '') { $loadedDocumentDatas["ADDRESS_INVOICE"] .= ' &middot; ' . $_REQUEST["selectCustomersSecondaryRecipientManager"]; }
			$loadedDocumentDatas["ADDRESS_INVOICE"] .= ' &middot; ' . $_REQUEST["selectCustomersSecondaryRecipientStreet"] . ' ' . $_REQUEST["selectCustomersSecondaryRecipientStreetNumber"];
			$loadedDocumentDatas["ADDRESS_INVOICE"] .= ' &middot; ' . $_REQUEST["selectCustomersSecondaryRecipientZipcode"] . ' ' . $_REQUEST["selectCustomersSecondaryRecipientCity"];
			$loadedDocumentDatas["ADDRESS_INVOICE"] .= ' &middot; ' . $arrCountryTypeDatas[$_REQUEST["selectCustomersSecondaryRecipientCountry"]]["countries_name"];

			$loadedDocumentDatas["ADDRESS_INVOICE"] .= '</span><br>';
		}
		$thisImportToOrders = array();
		if($_REQUEST["documentType"] == "AB" || $_REQUEST["documentType"] == "AN" || $_REQUEST["documentType"] == "RE" || $_REQUEST["documentType"] == "MA" || $_REQUEST["documentType"] == "M1" || $_REQUEST["documentType"] == "M2"  || $_REQUEST["documentType"] == "M3") {

			$loadedDocumentDatas["ADDRESS_DELIVERY"] .= '<span class="adressAdditional"><b>Lieferadresse:</b> ';

			/*
			if($customersData["UseSalesmanDeliveryAdress"] == '1') {
				$loadedDocumentDatas["ADDRESS_DELIVERY"] .= $salesmanDatas["salesmanFirmenname"] . ' ' . $salesmanDatas["salesmanFirmennameZusatz"] . ' &middot; ' . $salesmanDatas["customersLieferadresseStrasse"] . ' ' . $salesmanDatas["customersLieferadresseHausnummer"] . ' &middot; ' .
															$salesmanDatas["customersLieferadressePLZ"] . ' ' . $salesmanDatas["customersLieferadresseOrt"] . ' &middot; ' .
															$arrCountryTypeDatas[$salesmanDatas["customersLieferadresseLand"]]["countries_name"];

				$thisImportToOrders["PLZ"] = $salesmanDatas["customersLieferadressePLZ"];
				$thisImportToOrders["ORT"] = $salesmanDatas["customersLieferadresseOrt"];
			}
			else {
				$loadedDocumentDatas["ADDRESS_DELIVERY"] .= $customersData["LieferadresseFirmenname"] . ' ' . $customersData["LieferadresseFirmennameZusatz"] . ' &middot; ' . $customersData["LieferadresseStrasse"] . ' ' . $customersData["LieferadresseHausnummer"] . ' &middot; ' .
															$customersData["LieferadressePLZ"] . ' ' . $customersData["LieferadresseOrt"] . ' &middot; ' .
															$arrCountryTypeDatas[$customersData["LieferadresseLand"]]["countries_name"];

				$thisImportToOrders["PLZ"] = $customersData["LieferadressePLZ"];
				$thisImportToOrders["ORT"] = $customersData["LieferadresseOrt"];
			}
			*/

			$loadedDocumentDatas["ADDRESS_DELIVERY"] .= $_REQUEST["selectCustomersSecondaryRecipientName"];
			if(trim($_REQUEST["selectCustomersSecondaryRecipientNameAdd"]) != '') { $loadedDocumentDatas["ADDRESS_DELIVERY"] .= ' &middot; ' . $_REQUEST["selectCustomersSecondaryRecipientNameAdd"]; }
			if(trim($_REQUEST["selectCustomersSecondaryRecipientContact"]) != '') { $loadedDocumentDatas["ADDRESS_DELIVERY"] .= ' &middot; ' . $_REQUEST["selectCustomersSecondaryRecipientContact"]; }
			if(trim($_REQUEST["selectCustomersSecondaryRecipientManager"]) != '') { $loadedDocumentDatas["ADDRESS_DELIVERY"] .= ' &middot; ' . $_REQUEST["selectCustomersSecondaryRecipientManager"]; }
			$loadedDocumentDatas["ADDRESS_DELIVERY"] .= ' &middot; ' . $_REQUEST["selectCustomersSecondaryRecipientStreet"] . ' ' . $_REQUEST["selectCustomersSecondaryRecipientStreetNumber"];
			$loadedDocumentDatas["ADDRESS_DELIVERY"] .= ' &middot; ' . $_REQUEST["selectCustomersSecondaryRecipientZipcode"] . ' ' . $_REQUEST["selectCustomersSecondaryRecipientCity"];
			$loadedDocumentDatas["ADDRESS_DELIVERY"] .= ' &middot; ' . $arrCountryTypeDatas[$_REQUEST["selectCustomersSecondaryRecipientCountry"]]["countries_name"];

			$loadedDocumentDatas["ADDRESS_DELIVERY"] .= '</span><br><br>';
		}

		$loadedDocumentDatas["BANK_DATAS"] = "";
		if($_POST["selectPaymentType"] == '4' || $_POST["selectPaymentType"] == '6'){
			$loadedDocumentDatas["BANK_DATAS"] .= '<br><br><span class="adressAdditional"><b>Bankverbindung:</b> ';
			if($customersData["UseSalesmanInvoiceAdress"] == '1') {
				$loadedDocumentDatas["BANK_DATAS"] .=
					'' .
					$salesmanDatas["customersBankName"] .
					' &middot; BLZ: ' . $salesmanDatas["customersBankLeitzahl"] .
					' &middot; Konto: ' . $salesmanDatas["customersBankKontonummer"] .
					' &middot; IBAN: ' . $salesmanDatas["customersBankIBAN"] .
					' &middot; BIC: ' . $salesmanDatas["customersBankBIC"];
			}
			else {
				$loadedDocumentDatas["BANK_DATAS"] .=
					'' .
					$customersData["BankName"] .
					' &middot; BLZ: ' . $customersData["BankLeitzahl"] .
					' &middot; Konto: ' . $customersData["BankKontonummer"] .
					' &middot; IBAN: ' . $customersData["BankIBAN"] .
					' &middot; BIC: ' . $customersData["BankBIC"];
			}

			if(USE_DEBITING_DATE){
				$loadedDocumentDatas["BANK_DATAS"] .= '<br />' . 'Die Abbuchung erfolgt am ' . formatDate($_REQUEST["selectDebitingDate"], 'display');
			}

			$loadedDocumentDatas["BANK_DATAS"] .= '</span><br><br>';
		}

		// BOF AD FOOTER NOTICE
		if(MANDATOR == 'b3' && (in_array($customersData["Typ"], array("3", "4")) || in_array($salesmanDatas["salesmanTyp"], array("3", "4")))){
			// BOF CUSTOMER TYPES
			/*
			1 	Endkunde					EK 	1
			2 	Vertreter					VT 	1
			3 	Wiederverkäufer				WV 	1
			4 	Handelsvertreter			HV 	1
			5 	Gebrauchtwagenhändler		GH 	1
			6 	Vertragshändler				VH 	1
			7 	Amazon-Kunde				AMZ 1
			8 	ebay-Kunde					EBY 1
			9 	Außendienst-Mitarbeiter		ADM	1
			10 	EX-Außendienst-Mitarbeiter	XAM	1
			*/
			// EOF CUSTOMER TYPES
			$loadedDocumentDatas["FOOTER_NOTICE"] = $arrContentDatas[$_REQUEST["documentType"]]["FOOTER_NOTICE"];
		}
		else {
			$loadedDocumentDatas["FOOTER_NOTICE"] = "";
		}
		// EOF AD FOOTER NOTICE
	}
	// EOF READ DOCUMENT DATAS

	if($_POST["storeDatas"] != "" || $_POST["storeAndSendDatas"] != "") {
		// BOF DEFINE DOCUMENT TABLES DEPENDING ON DOCUMENT TYPE
		DEFINE('TABLE_ORDER_THIS', constant('TABLE_ORDER_' . $_POST["documentType"]));
		DEFINE('TABLE_ORDER_THIS_DETAILS', constant('TABLE_ORDER_' . $_POST["documentType"].'_DETAILS'));
		// EOF DEFINE DOCUMENT TABLES DEPENDING ON DOCUMENT TYPE

		if($_POST["pdfTempName"] != '' && file_exists($_POST["pdfTempName"])) {
			// BOF GET DOCUMENT CONTENT
				#$fp = fopen($_POST["pdfTempName"], 'r');
				#$fc = fread($fp, filesize($_POST["pdfTempName"]));
				#fclose($fp);
				$fc = "";
				clearstatcache();
			// BOF GET DOCUMENT CONTENT

			$thisPaymentDeadline = '';

			// BOF STORE DOCUMENT DATAS
				$sql = "INSERT INTO
							`" . TABLE_ORDER_THIS . "` (
								`orderDocumentsID`,
								`orderDocumentsNumber`,
								`orderDocumentsType`,
								`orderDocumentsOrdersIDs`,
								`orderDocumentsCustomerNumber`,
								`orderDocumentsCustomersOrderNumber`,

								`orderDocumentsProcessingDate`,
								`orderDocumentsDocumentDate`,
								`orderDocumentsOrderDate`,
								`orderDocumentsInvoiceDate`,
								`orderDocumentsCreditDate`,
								`orderDocumentsDeliveryDate`,
								`orderDocumentsBindingDate`,
								`orderDocumentsDebitingDate`,

								`orderDocumentsSalesman`,

								`orderDocumentsAddressCompanyCustomerNumber`,

								`orderDocumentsAddressCompany`,
								`orderDocumentsAddressName`,
								`orderDocumentsAddressStreet`,
								`orderDocumentsAddressStreetNumber`,
								`orderDocumentsAddressZipcode`,
								`orderDocumentsAddressCity`,
								`orderDocumentsAddressCountry`,
								`orderDocumentsAddressMail`,

								`orderDocumentsSumPrice`,
								`orderDocumentsDiscount`,
								`orderDocumentsDiscountType`,
								`orderDocumentsDiscountPercent`,
								`orderDocumentsMwst`,
								`orderDocumentsMwstPrice`,
								`orderDocumentsMwstType`,
								`orderDocumentsShippingCosts`,
								`orderDocumentsShippingCostsPackages`,
								`orderDocumentsShippingCostsPerPackage`,
								`orderDocumentsPackagingCosts`,
								`orderDocumentsCashOnDelivery`,
								`orderDocumentsTotalPrice`,

								`orderDocumentsBankAccount`,

								`orderDocumentsPaymentCondition`,
								`orderDocumentsPaymentType`,

								`orderDocumentsDeliveryType`,
								`orderDocumentsDeliveryTypeNumber`,

								`orderDocumentsSubject`,
								`orderDocumentsContent`,

								`orderDocumentsDocumentPath`,
								`orderDocumentsTimestamp`,
								`orderDocumentsStatus`,

								`orderDocumentsSendMail`,

								`orderDocumentsInterestPercent`,
								`orderDocumentsInterestPrice`,
								`orderDocumentsChargesPrice`,

								`orderDocumentsDeadlineDate`,

								`orderDocumentsCustomerGroupID`,

								`orderDocumentsIsCollectiveInvoice`
							)

							VALUES (
								'%',
								'" . $_POST["documentNumber"] . "',
								'" . $_POST["documentType"] . "',
								'".implode(";", array_unique($_REQUEST["arrItemIDs"])) . "',
								'" . $_POST["selectCustomersNumber"] . "',
								'" . $_POST["selectCustomersOrderNumber"] . "',

								NOW(),
								'" . formatDate($_POST["selectDocumentDate"], 'store') . "',
								'" . formatDate($_POST["selectOrderDate"], 'store') . "',
								'" . formatDate($_POST["selectInvoiceDate"], 'store') . "',
								'" . formatDate($_POST["selectCreditDate"], 'store') . "',
								'" . formatDate($_POST["selectDeliveryDate"], 'store') . "',
								'" . formatDate($_POST["selectBindingDate"], 'store') . "',
								'" . formatDate($_POST["selectDebitingDate"], 'store') . "',

								'" . $_POST["selectSalesman"] . "',

								'" . $_POST["selectCustomersRecipientCustomerNumber"] . "',

								'" . addslashes($_POST["selectCustomersRecipientName"] . ' ' . $_POST["selectCustomersRecipientNameAdd"]) . "',
								'" . addslashes($_POST["selectCustomersRecipientManager"]) . "',
								'" . addslashes($_POST["selectCustomersRecipientStreet"]) . "',
								'" . $_POST["selectCustomersRecipientStreetNumber"] . "',
								'" . $_POST["selectCustomersRecipientZipcode"] . "',
								'" . $_POST["selectCustomersRecipientCity"] . "',
								'" . $_POST["selectCustomersRecipientCountry"] . "',
								'" . $_POST["selectCustomersRecipientMail"] . "',

								'" . convertDecimal($_POST["arrPriceDatas"]["basketTotalPrice"], 'store') . "',
								'" . convertDecimal($_POST["arrPriceDatas"]["basketDiscountValue"], 'store') . "',
								'" . $_POST["arrPriceDatas"]["basketDiscountType"] . "',
								'" . convertDecimal($_POST["arrPriceDatas"]["basketDiscountPercent"], 'store') . "',
								'" . convertDecimal($_POST["selectMwSt"], 'store') . "',
								'" . convertDecimal($_POST["arrPriceDatas"]["basketMmstPrice"], 'store') . "',
								'" . ($_POST["selectMwStType"]) . "',
								'" . (convertDecimal($_POST["selectShippingCosts"], 'store')) * (convertDecimal($_POST["selectPackagingCostsMultiplier"], 'store')) . "',
								'" . (convertDecimal($_POST["selectPackagingCostsMultiplier"], 'store')) . "',
								'" . (convertDecimal($_POST["selectShippingCosts"], 'store')) . "',
								'" . convertDecimal($_POST["selectPackagingCosts"], 'store') . "',
								'" . convertDecimal($_POST["selectCashOnDelivery"], 'store') . "',
								'" . convertDecimal($_POST["arrPriceDatas"]["basketCompletePrice"], 'store') . "',

								'" . $_POST["selectBankAccount"] . "',

								'" . $_POST["selectPaymentCondition"] . "',
								'" . $_POST["selectPaymentType"] . "',

								'" . $_POST["selectDeliveryType"] . "',
								'" . $_POST["selectDeliveryTypeNumber"] . "',

								'" . $_POST["documentSubject"] . "',
								'" . $_POST["documentEditorText"] . "',
								'" . $_POST["pdfTempName"] . "',
								NOW(),
								'" . $_POST["selectPaymentStatus"] . "',

								'0',

								'" . convertDecimal($_POST["arrPriceDatas"]["basketInterestPercent"], 'store') . "',
								'" . convertDecimal($_POST["arrPriceDatas"]["basketInterestPrice"], 'store') . "',
								'" . convertDecimal($_POST["arrPriceDatas"]["basketChargesPrice"], 'store') . "',

								IF('" . $_POST["selectPaymentType"] . "' = '1', DATE_ADD('" . formatDate($_POST["selectDocumentDate"], 'store') . "', INTERVAL 10 DAY),
									IF('" . $_POST["selectPaymentType"] . "' = '5', DATE_ADD('" . formatDate($_POST["selectDocumentDate"], 'store') . "', INTERVAL 14 DAY),
										IF('" . $_POST["selectPaymentType"] . "' = '6', DATE_ADD('" . formatDate($_POST["selectDocumentDate"], 'store') . "', INTERVAL 30 DAY),
											DATE_ADD('" . formatDate($_POST["selectDocumentDate"], 'store') . "', INTERVAL 7 DAY)
										)
									)
								),

								'',

								'" . $_REQUEST["isCollectiveInvoice"] . "'
							)
				";
				// '" . $_POST["documentOrdersIDs"] . "'

				// '" . $_POST["selectProcessingDate"] . "',
				#dd('sql');

				$rs = $dbConnection->db_query($sql);
				if($rs) {
					$successMessage .= ' Die Dokumentdaten wurden gespeichert.' .'<br />';
				}
				else {
					$errorMessage .= ' Die Dokumentdaten konnte nicht gespeichert werden.' .'<br />';
				}
			// EOF STORE DOCUMENT DATAS

			// GET THIS INSERT ID
				$insertedDocumentID = mysqli_insert_id();

			// BOF CREATE DOCUMENT NUMBER AND UPDATE TABLE
				if($_POST["editDocType"] != '') {
					$generatedDocumentNumber = $_POST["originDocumentNumber"];
				}
				else {
					#$generatedDocumentNumber = generateDocumentNumber(constant("BASIC_NUMBER_" . $_REQUEST["documentType"]), $_POST["documentType"], date(ym), TABLE_ORDER_THIS);
					// BOF GENERATE DOCUMENT NUMBER ON CURRENT YEAR AND MONTH
					$generatedDocumentNumber = generateDocumentNumber(constant("BASIC_NUMBER_" . $_REQUEST["documentType"]), $_POST["documentType"], date(ym), TABLE_ORDER_THIS);
					// EOF GENERATE DOCUMENT NUMBER ON CURRENT YEAR AND MONTH

					// BOF GENERATE DOCUMENT NUMBER ON DOCUMENT DATE YEAR AND MONTH
					if($_POST["useDocumentDateForDocumentNumber"] == '1' && $userDatas["usersLogin"] == 'thorsten'){
						#$tempDate = strtotime(formatDate($_POST["selectDocumentDate"], 'store'));
						#$thisDate_ym = date("ym", $tempDate);
						#$testGeneratedDocumentNumber = generateDocumentNumber(constant("BASIC_NUMBER_" . $_REQUEST["documentType"]), $_POST["documentType"], $thisDate_ym, TABLE_ORDER_THIS);
					}
					// EOF GENERATE DOCUMENT NUMBER ON DOCUMENT DATE YEAR AND MONTH
				}

				$thisCreatedPdfName = $generatedDocumentNumber . '_KNR-' . $_POST["selectCustomersNumber"] . '.pdf';

				// BOF DELETE EXISTING ENTRY IF DOCUMENT-NUMBER EXISTS
					$sql = "SELECT
								`orderDocumentsID` AS `deleteID`
								FROM
								`" . TABLE_ORDER_THIS . "`
								WHERE `orderDocumentsNumber` = '" . $generatedDocumentNumber. "'
					";
					$rs = $dbConnection->db_query($sql);
					list($deleteID) = mysqli_fetch_array($rs);

					if($deleteID != '') {
						$sql = "DELETE FROM `" . TABLE_ORDER_THIS . "` WHERE `orderDocumentsID` = '" . $deleteID. "'";
						$rs = $dbConnection->db_query($sql);

						$sql = "DELETE FROM `" . TABLE_ORDER_THIS_DETAILS . "` WHERE `orderDocumentDetailDocumentID` = '" . $deleteID. "'";
						$rs = $dbConnection->db_query($sql);
					}
				// EOF DELETE EXISTING ENTRY IF DOCUMENT-NUMBER EXISTS

				$sql_update = "UPDATE
							`" . TABLE_ORDER_THIS . "`
							SET `orderDocumentsNumber` = '" . $generatedDocumentNumber. "',
								`orderDocumentsDocumentPath` = '" . DIRECTORY_CREATED_DOCUMENTS_CUSTOMERS . $thisCreatedPdfName. "'
							WHERE `orderDocumentsID` = '" . $insertedDocumentID . "'
				";
				$rs_update = $dbConnection->db_query($sql_update);
			// EOF CREATE DOCUMENT NUMBER AND UPDATE TABLE

			// BOF STORE DOCUMENT DATAS DETAILS
				if(!empty($_POST["arrItemIDs"])) {
					$sql_insertValues = array();
					$arrOrderIds = array();
					foreach($_POST["arrItemIDs"] as $thisKey => $thisValue) {
						if(!isset($thisLastInsertID)) {
							$thisLastInsertID = 0;
						}
						$sql_insert = "INSERT INTO `" . TABLE_ORDER_THIS_DETAILS . "` (
													`orderDocumentDetailID`,
													`orderDocumentDetailDocumentID`,
													`orderDocumentDetailOrderID`,
													`orderDocumentDetailOrderType`,
													`orderDocumentDetailPos`,

													`orderDocumentDetailProductNumber`,
													`orderDocumentDetailProductName`,
													`orderDocumentDetailProductKategorieID`,
													`orderDocumentDetailProductQuantity`,
													`orderDocumentDetailProductColorsText`,
													`orderDocumentDetailProductColorsCount`,
													`orderDocumentDetailsWithBorder`,
													`orderDocumentDetailPrintText`,
													`orderDocumentDetailKommission`,
													`orderDocumentDetailNotiz`,
													`orderDocumentDetailProductSinglePrice`,
													`orderDocumentDetailProductTotalPrice`,

													`orderDocumentDetailMwst`,
													`orderDocumentDetailDiscount`,
													`orderDocumentDetailDirectSale`,
													`orderDocumentDetailPosType`,
													`orderDocumentDetailGroupingID`

												)
												VALUES (
													'%',
													'" . $insertedDocumentID . "',
													'" . $_POST["arrItemDatas"][$thisKey]["basketItemId"] . "',
													'" . $_POST["arrItemDatas"][$thisKey]["basketItemOrderType"] . "',
													'" . $_POST["arrItemDatas"][$thisKey]["basketItemPos"] . "',

													'" . addslashes($_POST["arrItemDatas"][$thisKey]["basketItemModel"]) . "',
													'" . (preg_replace("/,[ ]{0,}[0-9]{1,}\-farbiger Druck/", "", preg_replace("/\[[a-zA-Z]{0,}\]$/", "", preg_replace("/, ohne Druck/", "", addslashes(strip_tags(preg_replace("/<br>/", " ", $_POST["arrItemDatas"][$thisKey]["basketItemDescription"]))))))) . "',
													'" . $_POST["arrItemDatas"][$thisKey]["basketItemProductKategorieID"] . "',
													'" . $_POST["arrItemDatas"][$thisKey]["basketItemQuantity"] . "',
													'" . addslashes(preg_replace("/Druckfarben: /", "", strip_tags($_POST["arrItemDatas"][$thisKey]["basketItemColorsText"]))) . "',
													'" . $_POST["arrItemDatas"][$thisKey]["basketItemColorsCount"] . "',
													'" . $_POST["arrItemDatas"][$thisKey]["basketItemWithBorder"] . "',
													'" . addslashes(preg_replace("/ - Mit Umrandung/", "", preg_replace("/Aufdruck: /", "", strip_tags($_POST["arrItemDatas"][$thisKey]["basketItemPrintText"])))) . "',
													'" . addslashes(preg_replace("/Kommission: /", "", strip_tags($_POST["arrItemDatas"][$thisKey]["basketItemKommission"]))) . "',
													'" . addslashes(preg_replace("/Notiz: /", "", strip_tags($_POST["arrItemDatas"][$thisKey]["basketItemNotiz"]))) . "',
													'" . convertDecimal($_POST["arrItemDatas"][$thisKey]["basketItemSinglePrice"], 'store') . "',
													'" . convertDecimal($_POST["arrItemDatas"][$thisKey]["basketItemTotalPrice"], 'store') . "',

													'" . convertDecimal($_POST["arrPriceDatas"]["basketMmst"], 'store') . "',
													'" . convertDecimal($_POST["arrPriceDatas"]["basketDiscount"], 'store') . "',
													'" . $_POST["arrItemDatas"][$thisKey]["basketItemPerDirectSale"] . "',
													'" . $_POST["arrItemDatas"][$thisKey]["basketItemPosType"] . "',
													'" . $thisLastInsertID ."'
												)
						";

						$rs_insert = $dbConnection->db_query($sql_insert);

						if($_POST["arrItemDatas"][$thisKey]["basketItemPosType"] == 'product') {
							$thisLastInsertID = $dbConnection->db_getInsertID();
							$sql_update = "UPDATE `" . TABLE_ORDER_THIS_DETAILS . "`
													SET `orderDocumentDetailGroupingID` = '" . $thisLastInsertID . "'
													WHERE `orderDocumentDetailID` = '" . $thisLastInsertID . "'
							";
							$rs_update = $dbConnection->db_query($sql_update);
						}
						$arrOrderIds[] = $_POST["arrItemDatas"][$thisKey]["basketItemId"];
					}
				}
			// EOF STORE DOCUMENT DATAS DETAILS

			// BOF STORE CONVERT/COPY RELATION
			if($_POST["originDocumentNumber"] != '') {
				if($_POST["convertDocType"] != '') {
					$thisRelationType = 'convert';
				}
				else if($_POST["copyDocType"] != '') {
					$thisRelationType = 'copy';
				}
				else if($_POST["editDocType"] != '') {
					$thisRelationType = 'edit';
				}
				$sql = "INSERT INTO `" . TABLE_DOCUMENTS_TO_DOCUMENTS . "` (
										`documentsToDocumentsOriginDocumentNumber`,
										`documentsToDocumentsCreatedDocumentNumber`,
										`documentsToDocumentsRelationType`,
										`documentsToDocumentsCreatedDocumentType`
									)
									VALUES (
										'" . $_POST["originDocumentNumber"] . "',
										'" . $generatedDocumentNumber . "',
										'" . $thisRelationType . "',
										'" . $_POST["documentType"] . "'
									)
				";
				$rs = $dbConnection->db_query($sql);

				// BOF RELATION DOCUMENTS
					if($thisRelationType == 'convert'){
						if(substr($_POST["originDocumentNumber"], 0, 2) != substr($generatedDocumentNumber, 0, 2)){
							$thisGetRelated_sql = "
								SELECT
									`relatedDocuments_" . substr($_POST["originDocumentNumber"], 0, 2) . "`
									FROM `" . TABLE_RELATED_DOCUMENTS . "`
									WHERE `relatedDocuments_" . substr($_POST["originDocumentNumber"], 0, 2) . "` = '" . $_POST["originDocumentNumber"] . "';
							";
							$thisGetRelated_rs = $dbConnection->db_query($thisGetRelated_sql);

							if($dbConnection->db_getMysqlNumRows($thisGetRelated_rs) > 0){
								$thisRelated_sql = "
									UPDATE
										`" . TABLE_RELATED_DOCUMENTS . "`
										SET
											`relatedDocuments_" . substr($generatedDocumentNumber, 0, 2) . "` = '" . $generatedDocumentNumber . "'

										WHERE 1
											AND `relatedDocuments_" . substr($_POST["originDocumentNumber"], 0, 2) . "` LIKE '%" . $_POST["originDocumentNumber"] . "%'

								";
							}
							else {
								$thisRelated_sql = "
									INSERT
										INTO `" . TABLE_RELATED_DOCUMENTS . "` (
												`relatedDocuments_" . substr($_POST["originDocumentNumber"], 0, 2) . "`,
												`relatedDocuments_" . substr($generatedDocumentNumber, 0, 2) . "`
											)
											VALUES (
												'" . $_POST["originDocumentNumber"] . "',
												'" . $generatedDocumentNumber . "'
											)
								";
							}
							$thisRelated_rs = $dbConnection->db_query($thisRelated_sql);
						}
						else if(substr($_POST["originDocumentNumber"], 0, 2) == substr($generatedDocumentNumber, 0, 2)) {
							$thisRelated_sql = "
								UPDATE
									`" . TABLE_RELATED_DOCUMENTS . "`
									SET `relatedDocuments_" . substr($_POST["originDocumentNumber"], 0, 2) . "` = CONCAT(`relatedDocuments_" . substr($_POST["originDocumentNumber"], 0, 2) . "`, ';', '" . $generatedDocumentNumber . "')
									WHERE 1
										AND  `relatedDocuments_" . substr($_POST["originDocumentNumber"], 0, 2) . "` LIKE '%" . $_POST["originDocumentNumber"] . "%'
							";
							$thisRelated_rs = $dbConnection->db_query($thisRelated_sql);
						}
					}
				// EOF RELATION DOCUMENTS

			}
			else if($_REQUEST["editDocType"] == '' && $_REQUEST["convertDocType"] == '' && $_REQUEST["copyDocType"] == '' && $_REQUEST["documentType"] != "" && $_REQUEST["documentType"] != "BR") {
				$thisRelated_sql = "
						INSERT
							INTO `" . TABLE_RELATED_DOCUMENTS . "` (
									`relatedDocuments_" . substr($generatedDocumentNumber, 0, 2) . "`
								)
								VALUES (
									'" . $generatedDocumentNumber . "'
								)
					";
					$thisRelated_rs = $dbConnection->db_query($thisRelated_sql);

			}
			// EOF STORE CONVERT/COPY RELATION

			// BOF STORE DATAS FOR COLLECTIVE INVOICE
				if($_REQUEST["isCollectiveInvoice"] == '1'){
					$arrRelatedDocuments_collectiveABs = array();
					if(!empty($_POST["arrItemDatas"])){
						foreach($_POST["arrItemDatas"] as $thisItemDataKey => $thisItemDataValue){
							$arrRelatedDocuments_collectiveABs[] = $thisItemDataValue["basketItemModel"];
						}
					}
					if(!empty($arrRelatedDocuments_collectiveABs)){
						$strRelatedDocuments_collectiveABs = implode(";", $arrRelatedDocuments_collectiveABs);

						if($_REQUEST["documentType"] == "RE"){
							// BOF UPDATE TABLE AB SET IS COLLECTIVE INVOICE
							$sql_updateOrderConfirmations = "
									UPDATE `bctr_orderconfirmations`
										SET `orderDocumentsIsCollectiveInvoice` = '1'
									WHERE 1
										AND (
											`orderDocumentsNumber` = '" . implode("' OR `orderDocumentsNumber` = '", $arrRelatedDocuments_collectiveABs) . "'
										)
								";
							$rs_updateOrderConfirmations = $dbConnection->db_query($sql_updateOrderConfirmations);
							// EOF UPDATE TABLE AB SET IS COLLECTIVE INVOICE

							// BOF UPDATE SET RELATED DOCUMENTS WITH COLLECTIVE ABs
							$sql_updateRelatedDocuments = "
									UPDATE `bctr_relateddocuments`
										SET
											`relatedDocuments_collectiveABs` = '" . $strRelatedDocuments_collectiveABs . "',
											`relatedDocuments_RE` = '" . $generatedDocumentNumber. "'
										WHERE 1
											AND (
												`relatedDocuments_AB` = '" . implode("' OR `relatedDocuments_AB` = '", $arrRelatedDocuments_collectiveABs) . "'
											)
								";
							$rs_updateRelatedDocuments = $dbConnection->db_query($sql_updateRelatedDocuments);
							// EOF UPDATE SET RELATED DOCUMENTS WITH COLLECTIVE ABs

							// BOF UPDATE SET DOCUMENTS TO DOCUMENTS DOCUMENTS WITH COLLECTIVE ABs
							foreach($arrRelatedDocuments_collectiveABs as $thisCollectiveAB){
								$sql_updateDocumentsToDocuments = "
										INSERT INTO `" . TABLE_DOCUMENTS_TO_DOCUMENTS . "` (
													`documentsToDocumentsOriginDocumentNumber`,
													`documentsToDocumentsCreatedDocumentNumber`,
													`documentsToDocumentsRelationType`,
													`documentsToDocumentsCreatedDocumentType`
												)
												VALUES (
													'" . $thisCollectiveAB . "',
													'" . $generatedDocumentNumber . "',
													'" . $thisRelationType . "',
													'" . $_POST["documentType"] . "'
												)
									";
								$rs_updateDocumentsToDocuments = $dbConnection->db_query($sql_updateDocumentsToDocuments);
							}
							// EOF UPDATE SET DOCUMENTS TO DOCUMENTS DOCUMENTS WITH COLLECTIVE ABs

							// BOF GET COLLECTIVE PATHS OF ABs FOR MAIL ATTACHMENT
							$sql_getConfirmationPDFs = "
									SELECT
										`orderDocumentsDocumentPath`

									FROM `bctr_orderconfirmations`

									WHERE 1
										AND (
											`orderDocumentsNumber` = '" . implode("' OR `orderDocumentsNumber` = '", $arrRelatedDocuments_collectiveABs) . "'
										)
								";
							$rs_getConfirmationPDFs = $dbConnection->db_query($sql_getConfirmationPDFs);
							// EOF GET COLLECTIVE PATHS OF ABs FOR MAIL ATTACHMENT

						}
					}
				}
			// EOF STORE DATAS FOR COLLECTIVE INVOICE

			// BOF SET PAYMENT STATUS IN RELATED DOCUMENTS
				$arrRelatedDocuments = getRelatedDocuments(array($generatedDocumentNumber), '');

				dd('arrRelatedDocuments');
				if(!empty($arrRelatedDocuments)){
					foreach($arrRelatedDocuments as $thisRelatedDocument){
						$thisRelatedDocumentType = substr($thisRelatedDocument, 0, 2);
						#if($thisRelatedDocumentType != $_REQUEST["documentType"] && ($thisRelatedDocumentType == 'RE' || $thisRelatedDocumentType == 'MA' || $thisRelatedDocumentType == 'M1' || $thisRelatedDocumentType == 'M2' || $thisRelatedDocumentType == 'M3' || $thisRelatedDocumentType == 'IK')){
						// if($thisRelatedDocumentType != $_REQUEST["documentType"] && ($thisRelatedDocumentType == 'LS' || $thisRelatedDocumentType == 'AB' || $thisRelatedDocumentType == 'RE' || $thisRelatedDocumentType == 'MA' || $thisRelatedDocumentType == 'M1' || $thisRelatedDocumentType == 'M2' || $thisRelatedDocumentType == 'M3' || $thisRelatedDocumentType == 'IK')){
						if($thisRelatedDocumentType != $_REQUEST["documentType"] && in_array($thisRelatedDocumentType, array('LS', 'AB', 'RE', 'MA', 'M1', 'M2', 'M3', 'IK'))){

							$sql = "UPDATE
										`" . constant('TABLE_ORDER_' . $thisRelatedDocumentType) . "`
										SET `" . constant('TABLE_ORDER_' . $thisRelatedDocumentType) . "`.`orderDocumentsStatus` = '" . $_POST["selectPaymentStatus"] . "'
										WHERE 1
											AND `" . constant('TABLE_ORDER_' . $thisRelatedDocumentType) . "`.`orderDocumentsNumber` = '" . $thisRelatedDocument . "'

							";
#dd('sql');
$debugContent = "";
$debugContent .= $sql . "\n";
$debugContent .= "thisRelatedDocumentType: " . $thisRelatedDocumentType . "\n";
$debugContent .= "thisRelatedDocument: " . $thisRelatedDocument . "\n";
$debugContent .= "_REQUEST[documentType]: " . $_REQUEST["documentType"] . "\n";
$debugContent .= "_POST[selectPaymentStatus]: " . $_POST["selectPaymentStatus"] . "\n";
$debugContent .= "--------------" . "\n\n\n";
$fp = fopen("createDocumentLog.txt", "a+");
fwrite($fp, $debugContent);
fclose($fp);


							$rs = $dbConnection->db_query($sql);
							if($rs) {
								$successMessage .= ' Der Status (&quot;' . $thisRelatedDocument . '&quot;) wurde gespeichert.' .'<br />';
							}
							else {
								$errorMessage .= ' Der Status (&quot;' . $thisRelatedDocument . '&quot;) konnte nicht gespeichert werden.' . '<br />';
							}
						}
					}
				}
			// EOF SET PAYMENT STATUS IN RELATED DOCUMENTS


			// BOF SET ORDER STATUS IF CONFIRMATION WAS CONVERTED TO INVOICE
			if(($_POST["originDocumentType"] == 'AB' && $_POST["convertDocType"] == 'RE') || ($_POST["originDocumentType"] == 'LS' && $_POST["convertDocType"] == 'RE')){
				#if($_POST["selectPaymentType"] != '2'){
				// AUSSER BEI VORKASSS -> PRODUKTION AUF VERSENDET BEI RECHNUNGSERSTELLUNG
				// 2:VORKASSE ; 3:NACHNAME
				#if($_POST["selectPaymentType"] != '2' && $_POST["selectPaymentType"] != '3'){
				if($_POST["selectPaymentType"] != '2'){
					if(!empty($arrOrderIds)) {
						$arrOrderIds = array_unique($arrOrderIds);
						foreach($arrOrderIds as $thisOrderID) {
							$sql_update = "UPDATE
									`" . TABLE_ORDERS . "`
									SET `ordersAuslieferungsDatum` = '" . formatDate($_POST["selectDeliveryDate"], 'store') . "',
										`ordersStatus` = 4
									WHERE `ordersID` = '" . $thisOrderID . "';
							";
							$rs_update = $dbConnection->db_query($sql_update);

							if($rs_update) {
								$successMessage .= ' Der Status der zugeh&ouml;rigen Produktion wurde auf &quot;<b>versendet</b>&quot; gesetzt.' . '<br />';
							}
							else {
								$errorMessage .= ' Der Status der zugeh&ouml;rigen Produktion konnte konnte nicht auf &quot;<b>versendet</b>&quot; gesetzt werden.' .'<br />';
							}
						}
					}
				}
				else {
					$infoMessage .= ' Der Status der zugeh&ouml;rigen Produktion wurde  nicht auf &quot;<b>versendet</b>&quot; gesetzt, da eine Vorkasse-Rechnung erstellt wurde.' .'<br />';
				}
			}

			// EOF SET ORDER STATUS IF CONFIRMATION WAS CONVERTED TO INVOICE

			// BOF SET INVOICE STATUS IF INVOICE WAS CONVERTED TO REMINDER OR CREDIT
				if(($_POST["originDocumentType"] == 'RE' || $_POST["originDocumentType"] == 'MA' || $_POST["originDocumentType"] == 'M1' || $_POST["originDocumentType"] == 'M2' || $_POST["originDocumentType"] == 'M3' || $_POST["originDocumentType"] == 'IK') && ($_POST["convertDocType"] == 'GU' || $_POST["convertDocType"] == 'MA' || $_POST["convertDocType"] == 'M1' || $_POST["convertDocType"] == 'M2' || $_POST["convertDocType"] == 'M3' || $_POST["convertDocType"] == 'IK')){
					$newInvoiceStatus = '';
					if($_POST["convertDocType"] == 'GU') {
						$newInvoiceStatus = '5';
						$newConfirmationStatus = '5';
					}
					else if($_POST["convertDocType"] == 'MA') {
						$newInvoiceStatus = '6';
						$newConfirmationStatus = '6';
						$sql = "UPDATE `" . TABLE_CUSTOMERS . "` SET `customersBadPaymentBehavior` = '1' WHERE `customersKundennummer` = '" . $_POST["selectCustomersNumber"] . "'; ";
						$rs = $dbConnection->db_query($sql);
					}
					else if($_POST["convertDocType"] == 'M1') {
						$newInvoiceStatus = '7';
						$newConfirmationStatus = '7';
						$sql = "UPDATE `" . TABLE_CUSTOMERS . "` SET `customersBadPaymentBehavior` = '1' WHERE `customersKundennummer` = '" . $_POST["selectCustomersNumber"] . "'; ";
						$rs = $dbConnection->db_query($sql);
					}
					else if($_POST["convertDocType"] == 'M2') {
						$newInvoiceStatus = '8';
						$newConfirmationStatus = '8';
						$sql = "UPDATE `" . TABLE_CUSTOMERS . "` SET `customersBadPaymentBehavior` = '1' WHERE `customersKundennummer` = '" . $_POST["selectCustomersNumber"] . "'; ";
						$rs = $dbConnection->db_query($sql);
					}
					else if($_POST["convertDocType"] == 'M3') {
						$newInvoiceStatus = '11';
						$newConfirmationStatus = '11';
						$sql = "UPDATE `" . TABLE_CUSTOMERS . "` SET `customersBadPaymentBehavior` = '1' WHERE `customersKundennummer` = '" . $_POST["selectCustomersNumber"] . "'; ";
						$rs = $dbConnection->db_query($sql);
					}
					else if($_POST["convertDocType"] == 'IK') {
						$newInvoiceStatus = '9';
						$newConfirmationStatus = '9';
						$sql = "UPDATE `" . TABLE_CUSTOMERS . "` SET `customersBadPaymentBehavior` = '1' WHERE `customersKundennummer` = '" . $_POST["selectCustomersNumber"] . "'; ";
						$rs = $dbConnection->db_query($sql);
					}

					$arrRelatedDocuments = getRelatedDocuments(array($_POST["originDocumentNumber"]));

					if(!empty($arrRelatedDocuments)){

						$thisRelatedInvoice = '';
						foreach($arrRelatedDocuments as $thisRelatedDocuments){
							if(substr($thisRelatedDocuments, 0, 2) == 'RE'){
								$thisRelatedInvoice = $thisRelatedDocuments;
							}
						}

						if($thisRelatedInvoice != ''){
							$sql = "UPDATE
										`" . TABLE_ORDER_INVOICES . "`

										SET
											`" . TABLE_ORDER_INVOICES . "`.`orderDocumentsStatus` = '" . $newInvoiceStatus . "'

										WHERE `" . TABLE_ORDER_INVOICES . "`.`orderDocumentsNumber` = '" . $thisRelatedInvoice . "';
								";

							$rs = $dbConnection->db_query($sql);
							if($rs) {
								$successMessage .= ' Der Status der Rechnung &quot;' . $thisRelatedInvoice . '&quot; wurde ge&auml;ndert.' .'<br />';
							}
							else {
								$errorMessage .= ' Der Status der Rechnung &quot;' . $thisRelatedInvoice . '&quot; konnte nicht ge&auml;ndert werden.' .'<br />';
							}
						}
					}
					else {
						#$errorMessage .= ' Der Status der zugeh&ouml;rigen Rechnung &quot;' . $_POST["originDocumentNumber"] . '&quot; konnte nicht ge&auml;ndert werden,<br />da keine Rechnung gefunden wurde.' .'<br />';
					}
				}
			// EOF SET INVOICE STATUS IF INVOICE WAS CONVERTED TO REMINDER OR CREDIT

			// BOF SET PAYMENTSTATUS TO PAID IF TOTALPRICE = 0
			if($_REQUEST["documentType"] == "RE" || $_REQUEST["originDocumentType"] == "RE" || $_REQUEST["convertDocType"] == "RE" ){
				$sql = "UPDATE `" . TABLE_ORDER_INVOICES . "` SET `orderDocumentsStatus` = 2 WHERE `orderDocumentsTotalPrice` = 0 AND `orderDocumentsStatus` = 1; ";
				$rs = $dbConnection->db_query($sql);
			}
			// BOF SET PAYMENTSTATUS TO PAID IF TOTALPRICE = 0

			// BOF INSERT INTO PRODUCTION IF OFFER CONVERTED TO CONFIRMATION
			if($_POST["originDocumentType"] == 'AN' && $_POST["convertDocType"] == 'AB') {
				#KUNDENNUMMER: $_POST["editID"]
				# $arrLoadDocumentDatas
				# $arrLoadDocumentDetailDatas
				#echo $_POST["originDocumentType"] . ' xxx ' .  $_POST["convertDocType"]  . '<br />';
				#echo '<pre>';
				#print_r($arrLoadDocumentDetailDatas);
				#echo '</pre>';
				#exit;
			}
			// EOF INSERT INTO PRODUCTION IF OFFER CONVERTED TO CONFIRMATION

			// BOF WRITE TEMP HTML FILE
			/*
			$tempHtmlFileName = "_temp_".$thisCreatedPdfName.'.html';
			if(file_exists(DIRECTORY_CREATED_DOCUMENTS_CUSTOMERS . $tempHtmlFileName)) {
				unlink(DIRECTORY_CREATED_DOCUMENTS_CUSTOMERS . $tempHtmlFileName);
			}
			clearstatcache();
			$fp = fopen(DIRECTORY_CREATED_DOCUMENTS_CUSTOMERS . $tempHtmlFileName, 'w');
			fwrite($fp, stripslashes($pdfContentPage));
			fclose($fp);
			*/
			// EOF WRITE TEMP HTML FILE

			$sql = "DELETE FROM `" . TABLE_CREATED_DOCUMENTS . "` WHERE `createdDocumentsNumber` = '" . $generatedDocumentNumber . "'";

			$sql = "
					DELETE

						`" . TABLE_CREATED_DOCUMENTS . "`
						`" . TABLE_ORDERS_TO_DOCUMENTS . "`

					FROM `" . TABLE_CREATED_DOCUMENTS . "`

					LEFT JOIN `" . TABLE_ORDERS_TO_DOCUMENTS . "`
					ON(`" . TABLE_CREATED_DOCUMENTS . "`.`createdDocumentsID` = `" . TABLE_ORDERS_TO_DOCUMENTS . "`.`ordersToDocumentsDocumentID`)

					WHERE `" . TABLE_CREATED_DOCUMENTS . "`.`createdDocumentsNumber` = '" . $generatedDocumentNumber . "'
			";

			#$rs = $dbConnection->db_query($sql);

			$sql = "REPLACE INTO `" . TABLE_CREATED_DOCUMENTS . "` (
								`createdDocumentsID`,
								`createdDocumentsType`,
								`createdDocumentsNumber`,
								`createdDocumentsCustomerNumber`,
								`createdDocumentsTitle`,
								`createdDocumentsFilename`,
								`createdDocumentsContent`,
								`createdDocumentsUserID`,
								`createdDocumentsTimeCreated`,
								`createdDocumentsOrderIDs`
							)
							VALUES (
								'%',
								'" . $_POST["documentType"] . "',
								'" . $generatedDocumentNumber . "',
								'" . $_POST["selectCustomersNumber"] . "',
								'" . $generatedDocumentNumber . "',
								'" . DIRECTORY_CREATED_DOCUMENTS_CUSTOMERS . $thisCreatedPdfName . "',
								'',
								'" . $_SESSION["usersID"] . "',
								NOW(),
								'".implode(";", $_REQUEST["arrItemIDs"]) . "'
							)
			";

			// '".$_POST["documentOrdersIDs"] . "'

			$rs = $dbConnection->db_query($sql);

			$thisInsertID = $dbConnection->db_getInsertID();

			$thisDocumentOrdersIDs = explode(";", $_POST["documentOrdersIDs"]);
			if(!empty($thisDocumentOrdersIDs)) {
				$sql_replace = array();
				foreach($thisDocumentOrdersIDs as $thisKey => $thisValue) {
					$sql_replace[] = "	(
											'%',
											'".$thisValue."',
											'".$thisInsertID."',
											'unknown',
											'" . $_POST["documentType"] . "',
											NOW()
										)";
				}
				if(!empty($sql_replace)) {
					$sql = "REPLACE INTO `" . TABLE_ORDERS_TO_DOCUMENTS . "` (
										`ordersToDocumentsID`,
										`ordersToDocumentsOrderID`,
										`ordersToDocumentsDocumentID`,
										`ordersToDocumentsStatus`,
										`ordersToDocumentsType`,
										`ordersToDocumentsCreationTime`
									) VALUES " . implode(",", $sql_replace) . " ;";

				}
				$rs = $dbConnection->db_query($sql);
			}

			// BOF createPDF
				require_once(DIRECTORY_HTML2PDF . 'html2pdf.class.php');

				/*
				if(file_exists(DIRECTORY_CREATED_DOCUMENTS_CUSTOMERS . $thisCreatedPdfName)) {
					try {
						chmod(DIRECTORY_CREATED_DOCUMENTS_CUSTOMERS . $thisCreatedPdfName , "0777");
						unlink(DIRECTORY_CREATED_DOCUMENTS_CUSTOMERS . $thisCreatedPdfName);
					}
					catch (Exception $e) {}
				}
				clearstatcache();
				*/

				try {
					// $html2pdf = new HTML2PDF('P', 'A4', 'de', true, 'UTF-8', array(0, 0, 0, 0));
					$html2pdf = new HTML2PDF('P', 'A4', 'de', true, 'UTF-8', array(22, 5, 5, 5));
					// $html2pdf = new HTML2PDF('P', 'A4', 'de');
					// $html2pdf = new HTML2PDF('P', 'A4', 'de', true, 'UTF-8');
					// $html2pdf->setModeDebug();
					// $html2pdf->setDefaultFont('Arial');
					// $html2pdf->createIndex('Inhaltsverzeichnis', 12, 8, false, true, 2);

					if(file_exists(DIRECTORY_CREATED_DOCUMENTS_CUSTOMERS . $thisCreatedPdfName)) {
						// chmod(DIRECTORY_CREATED_DOCUMENTS_CUSTOMERS . $thisCreatedPdfName , "0777");
						unlink(DIRECTORY_CREATED_DOCUMENTS_CUSTOMERS . $thisCreatedPdfName);
					}
					clearstatcache();

					// BOF READ TEMP HTML FILE
					// if(file_exists(DIRECTORY_CREATED_DOCUMENTS_CUSTOMERS . $tempHtmlFileName)) {
					if(file_exists($_POST["pdfTempName"])) {
						$pdfContentPage = "";
						$fp = fopen($_POST["pdfTempName"], 'r');
						$pdfContentPage = fread($fp, filesize($_POST["pdfTempName"]));
						fclose($fp);
						$pdfContentPage = stripslashes($pdfContentPage);

						//echo $_POST["pdfTempName"] . '<br>';
						//echo htmlentities($pdfContentPage);
						//exit;
						$loadEditorContent = $_POST["documentEditorText"];
						$loadEditorContent = urldecode($loadEditorContent);
						$pdfContentPage = preg_replace('/{###EDITOR_TEXT###}/', '<div id="documentEditorText">' . $loadEditorContent  . '</div>', $pdfContentPage);
						#$pdfContentPage = preg_replace('/{###EDITOR_TEXT###}/', '<div id="documentEditorText">' . $_POST["documentEditorText"] . '</div>', $pdfContentPage);
						$pdfContentPage = preg_replace("/{###DOCUMENT_NUMBER###}/", $generatedDocumentNumber, $pdfContentPage);

						if(!DEBUG) {
							unlink($_POST["pdfTempName"]);
						}
					}
					clearstatcache();
					// EOF READ TEMP HTML FILE

					// BOF TEST PAGE SPLIT
						if($userDatas["usersLogin"] != 'xxxthorsten'){
							$pdfContentPage = preg_replace('/#pdfContentArea/', '', $pdfContentPage);
							$pdfContentPage = preg_replace('/<div id="pdfContentArea">/', '', $pdfContentPage);
							$pdfContentPage = preg_replace('/<\/div><page_header>/ismU', '<page_header>', $pdfContentPage);
						}
					// BOF TEST PAGE SPLIT

					ob_start();
					echo $pdfContentPage;
					$contentPDF = ob_get_clean();

					$html2pdf->writeHTML($contentPDF, isset($_GET['vuehtml']));
					$html2pdf->Output($thisCreatedPdfName, 'F', DIRECTORY_CREATED_DOCUMENTS_CUSTOMERS);

					if(file_exists($thisCreatedPdfName)) {
						unlink($thisCreatedPdfName);

						$sql_update = array();
						foreach($thisDocumentOrdersIDs as $thisKey => $thisValue) {
							$sql_update[] = " `ordersToDocumentsOrderID` = '" . $thisValue . "' ";
						}

						$sql = "UPDATE `" . TABLE_ORDERS_TO_DOCUMENTS . "`
									SET `ordersToDocumentsStatus` = 'created'
									WHERE ". implode(" OR ", $sql_update) . "
						";

						$rs = $dbConnection->db_query($sql);
					}
					clearstatcache();
				}
				catch(HTML2PDF_exception $e) {
					echo $e;
					exit;
				}
				$_POST["selectBankAccount"] = "";

				if(file_exists(DIRECTORY_CREATED_DOCUMENTS_CUSTOMERS . $thisCreatedPdfName)) {
					$successMessage .= 'Die PDF-Datei &quot;' . $thisCreatedPdfName . '&quot wurde generiert. '.'<br />';
					$showPdfDownloadLink = true;
				}
				else {
					$errorMessage .= 'Die PDF-Datei &quot;' . $thisCreatedPdfName . '&quot konnte nicht generiert werden. '.'<br />';
				}
				clearstatcache();

			// EOF createPDF

			// BOF SEND MAIL
				require_once(DIRECTORY_HTML_MIME_MAIL . 'htmlMimeMail.php');
				require_once(DIRECTORY_MAIL_TEMPLATES . 'template_BASIC.inc.php');
				require_once(DIRECTORY_MAIL_TEMPLATES . 'template_TEXT_DATAS.inc.php');
				require_once("classes/createMail.class.php");

				if($_REQUEST["documentType"] == 'KA') {
					$pathDocumentFolder = DIRECTORY_UPLOAD_FILES;
					$thisCreatedPdfName = utf8_decode($thisCreatedPdfName);
				}
				else if($_REQUEST["documentType"] == 'EX') {
					$pathDocumentFolder = DIRECTORY_EXPORT_FILES;
					$thisCreatedPdfName = utf8_decode($thisCreatedPdfName);
				}
				else if($_REQUEST["documentType"] == 'SX') {
					$pathDocumentFolder = DIRECTORY_CREATED_DOCUMENTS_SALESMEN;
					$thisCreatedPdfName = utf8_decode($thisCreatedPdfName);
				}
				else {
					$pathDocumentFolder = DIRECTORY_CREATED_DOCUMENTS_CUSTOMERS;
				}
				// BOF SEND DELIVERY-NOTICE
				if($_REQUEST["documentType"] == 'RE' && $_REQUEST["sendDeliverNoticeMail"] == '1' && $_POST["selectCustomersRecipientMail"] != ''){
					// BOF SEND MAIL TO CUSTOMER
					unset($createMail);
					$thisDeliverNoticeSubject = '';
					$thisDeliverNoticeSubject .= $arrMailContentDatas["VB"]["MAIL_TITLE"];
					if($_POST["selectSalesmansCustomerInfo"] != "") {
						$thisDeliverNoticeSubject .= ' | ' . $_POST["selectSalesmansCustomerInfo"];
						$arrMailContentDatas['VB']['MAIL_CONTENT'] .= '<br /><br /><b>Kunde</b>: ' . $_POST["selectSalesmansCustomerInfo"];
					}
					$thisDeliverNoticeSubject .= ' | ' . '{###DOCUMENT_AB_NUMBER###}';

					$createMail = new createMail(
										$thisDeliverNoticeSubject,					// TEXT:	MAIL_SUBJECT
										'VB',										// STRING:	DOCUMENT TYPE
										$generatedDocumentNumber,					// STRING:	DOCUMENT NUMBER
										null,										// ARRAY:	ATTACHMENT FILES
										$_POST["selectCustomersRecipientMail"],	 	// STRING:	RECIPIENTS
										$arrMailContentDatas["VB"],					// MAIL_TEXT_TEMPLATE
										$mailTemplate["html"],						// MAIL_BODY_TEMPLATE
										'',											// STRING:	ADDITIONAL TEXT
										'',											// STRING:	SEND ATTACHED TEXT
										true,										// BOOLEAN:	SEND ATTACHED DOCUMENT
										DIRECTORY_MAIL_IMAGES						// DIRECTORY_MAIL_IMAGES
									);
					if($_POST["selectDeliveryType"] != '' && $_POST["selectDeliveryTypeNumber"] != ''){
						#$arrDeliverDatas = array();
						#$arrDeliverDatas["TYPE"] = '';
						#$arrDeliverDatas["TRACKING_NUMBER"] = '';
						#$arrDeliverDatas["TRACKING_LINK"] = '';
						#$createMail->setDeliveryDatas($arrDeliverDatas);
					}

					$createMailResult = $createMail->returnResult();
					$sendMailDeliveryNoticeToClient = $createMailResult;

					if(isset($sendMailDeliveryNoticeToClient)) {
						$thisSelectCustomersRecipientMail .= "";
						if($_POST["selectCustomersRecipientMail"] != ""){
							$thisSelectCustomersRecipientMail .= $_POST["selectCustomersRecipientMail"];
						}
						if($_POST["selectCustomersRecipientMailCopy"] != "" && $_POST["selectCustomersRecipientMail"] != ""){
							$thisSelectCustomersRecipientMail .= ', ';
						}
						if($_POST["selectCustomersRecipientMailCopy"] != ""){
							$thisSelectCustomersRecipientMail .= $_POST["selectCustomersRecipientMailCopy"];
						}
						if(!$sendMailDeliveryNoticeToClient) { $errorMessage .= ' Die Versand-Benachrichtigung konnte nicht an ' . $thisSelectCustomersRecipientMail . ' versendet werden! '.'<br />'; }
						else { $successMessage .= ' Die Versand-Benachrichtigung wurde an ' . $thisSelectCustomersRecipientMail . ' versendet! '.'<br />'; }
					}
					// EOF SEND MAIL TO CUSTOMER
				}
				// EOF SEND DELIVERY-NOTICE

				if($_POST["storeAndSendDatas"] != "") {
					if(DEBUG) { $warningMessage .= 'DEBUG-MODUS: Nur TEST-Mail-Versand m&ouml;glich. '.'<br />'; }

					##require_once("inc/mail.inc.php");

					// BOF SEND MAIL


					// BOF CREATE SUBJECT
					$thisSubject = $_POST["selectSubject"];
					if($_POST["sendAttachedMailSubject"] != '') {
						$thisSubject = $_POST["sendAttachedMailSubject"] . ', ' . $thisSubject;
					}
					// EOF CREATE SUBJECT

					// BOF GET ATTACHED FILE
					$arrAttachmentFiles = array (
						rawurldecode($pathDocumentFolder . $thisCreatedPdfName) => str_replace(" ", "_", rawurldecode(myUnHtmlEntities($thisCreatedPdfName)))
					);
					// EOF GET ATTACHED FILE

					// BOF SEND MAIL TO CUSTOMER
					unset($createMail);
					$createMail = new createMail(
										$thisSubject . ' [' . $_POST["selectCustomersNumber"] . ' | ' .  $_POST["selectCustomersName"] . ']',														// TEXT:	MAIL_SUBJECT
										$_POST["documentType"],											// STRING:	DOCUMENT TYPE
										$generatedDocumentNumber,										// STRING:	DOCUMENT NUMBER
										$arrAttachmentFiles,											// ARRAY:	ATTACHMENT FILES
										$_POST["selectCustomersRecipientMail"],	 						// STRING:	RECIPIENTS
										$arrMailContentDatas[$_REQUEST["documentType"]],				// MAIL_TEXT_TEMPLATE
										$mailTemplate["html"],											// MAIL_BODY_TEMPLATE
										'',																// STRING:	ADDITIONAL TEXT
										$_POST["sendAttachedMailText"],									// STRING:	SEND ATTACHED TEXT
										true,															// BOOLEAN:	SEND ATTACHED DOCUMENT
										DIRECTORY_MAIL_IMAGES											// DIRECTORY_MAIL_IMAGES
									);

					$createMailResult = $createMail->returnResult();
					$sendMailToClient = $createMailResult;
					// EOF SEND MAIL TO CUSTOMER

					// BOF SEND MAIL COPY
					if($_POST["selectCustomersRecipientMailCopy"] != ""){
						unset($createMail);
						$createMail = new createMail(
											$thisSubject . ' (KOPIE) [' . $_POST["selectCustomersNumber"] . ' | ' .  $_POST["selectCustomersName"] . ']',										// TEXT:	MAIL_SUBJECT
											$_POST["documentType"],											// STRING:	DOCUMENT TYPE
											$generatedDocumentNumber,										// STRING:	DOCUMENT NUMBER
											$arrAttachmentFiles,											// ARRAY:	ATTACHMENT FILES
											$_POST["selectCustomersRecipientMailCopy"],	 					// STRING:	RECIPIENTS
											$arrMailContentDatas[$_REQUEST["documentType"]],				// MAIL_TEXT_TEMPLATE
											$mailTemplate["html"],											// MAIL_BODY_TEMPLATE
											'',																// STRING:	ADDITIONAL TEXT
											$_POST["sendAttachedMailText"],									// STRING:	SEND ATTACHED TEXT
											true,															// BOOLEAN:	SEND ATTACHED DOCUMENT
											DIRECTORY_MAIL_IMAGES											// DIRECTORY_MAIL_IMAGES
										);

						$createMailResult = $createMail->returnResult();
						$sendMailToUs = $createMailResult;
					}
					// EOF SEND MAIL COPY

					if(isset($sendMailToUs)) {
						if(!$sendMailToUs) { $errorMessage .= ' Die Mail-Kopie konnte nicht an ' . $_POST["selectCustomersRecipientMailCopy"] . ' versendet werden! '.'<br />'; }
						else { $successMessage .= ' Die Mail-Kopie wurde an ' . $_POST["selectCustomersRecipientMailCopy"] . ' versendet! '.'<br />'; }
					}

					if(!$sendMailToClient) {
						$errorMessage .= ' Die Mail konnte nicht an ' . $_POST["selectCustomersRecipientName"] . ' (' . $_POST["selectCustomersRecipientMail"] . ') versendet werden! '.'<br />';
						if(!$checkCustomerEmail) { $errorMessage .= ' Die Mail-Adresse ist nicht korrekt. '; }
					}
					else {
						$successMessage .= ' Die Mail wurde an ' . $_POST["selectCustomersRecipientName"] . ' (' . $_POST["selectCustomersRecipientMail"] . ') versendet! '.'<br />';
						$sql_update = array();
						foreach($thisDocumentOrdersIDs as $thisKey => $thisValue) {
							$sql_update[] = " `ordersToDocumentsOrderID` = '" . $thisValue . "' ";
						}

						$sql = "UPDATE `" . TABLE_ORDERS_TO_DOCUMENTS . "`
									SET `ordersToDocumentsStatus` = 'send'
									WHERE ". implode(" OR ", $sql_update) . "
						";

						$rs = $dbConnection->db_query($sql);
					}
				}
			// EOF SEND MAIL
		}
		else {
			$errorMessage .= 'Die PDF-Basis-Datei &quot;' . $_POST["pdfTempName"] . '&quot konnte nicht temporär gespeichert werden. '.'<br />';
		}
		clearstatcache();
	}
	// BOF GET NEEDED ADDRESS DATAS DEPENDING ON DOCUMENT TYPE
	// if($_REQUEST["documentType"] == 'AN' || $_REQUEST["documentType"] == 'AB' || $_REQUEST["documentType"] == 'RE' || $_REQUEST["documentType"] == 'BR' || $_REQUEST["documentType"] == 'GU' || $_REQUEST["documentType"] == 'MA' || $_REQUEST["documentType"] == 'M1' || $_REQUEST["documentType"] == 'M2'  || $_REQUEST["documentType"] == 'M3'  || $_REQUEST["documentType"] == 'IK') {
	if(in_array($_REQUEST["documentType"], array('AN', 'AB', 'RE', 'BR', 'GU', 'MA', 'M1', 'M2', 'M3', 'IK'))) {
		$selectCustomersRecipientCustomerNumber = $customersData["salesmanKundennummer"];
		$selectCustomersRecipientName 			= $customersData["RechnungsadresseFirmenname"];
		$selectCustomersRecipientNameAdd 		= $customersData["RechnungsadresseFirmennameZusatz"];
		$selectCustomersRecipientManager 		= $customersData["Manager"];
		$selectCustomersRecipientContact 		= $customersData["Contact"];
		$selectCustomersRecipientStreet 		= $customersData["RechnungsadresseStrasse"];
		$selectCustomersRecipientStreetNumber 	= $customersData["RechnungsadresseHausnummer"];
		$selectCustomersRecipientZipcode 		= $customersData["RechnungsadressePLZ"];
		$selectCustomersRecipientCity 			= $customersData["RechnungsadresseOrt"];
		$selectCustomersRecipientCountry 		= $customersData["RechnungsadresseLand"];
		if(trim($customersData["Mail1"]) != "") {
			$selectCustomersRecipientMail 		= $customersData["Mail1"];
		}
		else if(trim($customersData["Mail2"]) != "") {
			$selectCustomersRecipientMail = $customersData["Mail2"];
		}
		else {
			$selectCustomersRecipientMail = '';
		}

		if(trim($customersData["Fax1"]) != "") {
			$selectCustomersRecipientFax = $customersData["Fax1"];
		}
		else if(trim($customersData["Fax2"]) != "") {
			$selectCustomersRecipientFax = $customersData["Fax2"];
		}
		else {
			$selectCustomersRecipientFax = '';
		}

		$selectCustomersRecipientCustomerNumber = $customersData["Kundennummer"];

		$selectDiscount = $customersData["Rabatt"];
		$selectDiscountType = $customersData["RabattType"];
		$selectPaymentType = $customersData["Bezahlart"];
		$selectPaymentCondition = $customersData["Zahlungskondition"];

		$selectCustomersRecipientInvoicesNotPurchased = $customersData["InvoicesNotPurchased"];
		$selectCustomersRecipientBadPaymentBehavior = $customersData["BadPaymentBehavior"];

		$selectCustomersRecipientSepaExists = $customersData["SepaExists"];
		$selectCustomersRecipientSepaValidUntilDate = $customersData["SepaValidUntilDate"];
		$selectCustomersRecipientSepaRequestedDate = $customersData["SepaRequestedDate"];

		// -----------------------------------------------------------------------

		$selectCustomersSecondaryRecipientCustomerNumber = $customersData["Kundennummer"];
		$selectCustomersSecondaryRecipientName = $customersData["LieferadresseFirmenname"];
		$selectCustomersSecondaryRecipientNameAdd = $customersData["LieferadresseFirmennameZusatz"];
		$selectCustomersSecondaryRecipientManager = $customersData["Manager"];
		$selectCustomersSecondaryRecipientContact = $customersData["Contact"];
		$selectCustomersSecondaryRecipientStreet = $customersData["LieferadresseStrasse"];
		$selectCustomersSecondaryRecipientStreetNumber = $customersData["LieferadresseHausnummer"];
		$selectCustomersSecondaryRecipientZipcode = $customersData["LieferadressePLZ"];
		$selectCustomersSecondaryRecipientCity = $customersData["LieferadresseOrt"];
		$selectCustomersSecondaryRecipientCountry = $customersData["LieferadresseLand"];
	}

	else if($_REQUEST["documentType"] == 'LS') {
		$selectCustomersRecipientCustomerNumber = $customersData["Kundennummer"];
		$selectCustomersRecipientName = $customersData["LieferadresseFirmenname"];
		$selectCustomersRecipientNameAdd = $customersData["LieferadresseFirmennameZusatz"];
		$selectCustomersRecipientManager = $customersData["Manager"];
		$selectCustomersRecipientContact = $customersData["Contact"];
		$selectCustomersRecipientStreet = $customersData["LieferadresseStrasse"];
		$selectCustomersRecipientStreetNumber = $customersData["LieferadresseHausnummer"];
		$selectCustomersRecipientZipcode = $customersData["LieferadressePLZ"];
		$selectCustomersRecipientCity = $customersData["LieferadresseOrt"];
		$selectCustomersRecipientCountry = $customersData["LieferadresseLand"];

		if(trim($customersData["Mail1"]) != "") {
			$selectCustomersRecipientMail = $customersData["Mail1"];
		}
		else if(trim($customersData["Mail2"]) != "") {
			$selectCustomersRecipientMail = $customersData["Mail2"];
		}
		else {
			$selectCustomersRecipientMail = '';
		}

		if(trim($customersData["Fax1"]) != "") {
			$selectCustomersRecipientFax = $customersData["Fax1"];
		}
		else if(trim($customersData["Fax2"]) != "") {
			$selectCustomersRecipientFax = $customersData["Fax2"];
		}
		else {
			$selectCustomersRecipientFax = '';
		}

		$selectDiscount = $customersData["Rabatt"];
		$selectDiscountType = $customersData["RabattType"];
		$selectPaymentType = $customersData["Bezahlart"];
		$selectPaymentCondition = $customersData["Zahlungskondition"];

		$selectCustomersRecipientInvoicesNotPurchased = $customersData["InvoicesNotPurchased"];
		$selectCustomersRecipientBadPaymentBehavior = $customersData["BadPaymentBehavior"];

		$selectCustomersRecipientSepaExists = $customersData["SepaExists"];
		$selectCustomersRecipientSepaValidUntilDate = $customersData["SepaValidUntilDate"];
		$selectCustomersRecipientSepaRequestedDate = $customersData["SepaRequestedDate"];

		// -----------------------------------------------------------------------

		$selectCustomersSecondaryRecipientCustomerNumber = $customersData["Kundennummer"];
		$selectCustomersSecondaryRecipientName = $customersData["RechnungsadresseFirmenname"];
		$selectCustomersSecondaryRecipientNameAdd = $customersData["RechnungsadresseFirmennameZusatz"];
		$selectCustomersSecondaryRecipientManager = $customersData["Manager"];
		$selectCustomersSecondaryRecipientContact = $customersData["Contact"];
		$selectCustomersSecondaryRecipientStreet = $customersData["RechnungsadresseStrasse"];
		$selectCustomersSecondaryRecipientStreetNumber = $customersData["RechnungsadresseHausnummer"];
		$selectCustomersSecondaryRecipientZipcode = $customersData["RechnungsadressePLZ"];
		$selectCustomersSecondaryRecipientCity = $customersData["RechnungsadresseOrt"];
		$selectCustomersSecondaryRecipientCountry = $customersData["RechnungsadresseLand"];
	}

	$selectCustomersRecipientMailCopy = $salesmanDatas["salesmanMail"];

	$selectCustomersRecipientNotice = '';
	$selectCustomersSecondaryRecipientNotice = '';
	/*
	$selectCustomersRecipientName = $customersData["Firmenname"];
	$selectCustomersRecipientNameAdd = $customersData["FirmennameZusatz"];
	$customersData["RechnungsadresseStrasse"];
	$customersData["RechnungsadresseHausnummer"];
	$customersData["RechnungsadressePLZ"];
	$customersData["RechnungsadresseOrt"];
	$customersData["RechnungsadresseLand"];
	if(trim($customersData["Mail1"]) != "") {
		$selectCustomersRecipientMail = $customersData["Mail1"];
	}
	else if(trim($customersData["Mail2"]) != "") {

	*/
	if($_REQUEST["documentType"] == 'LS') {
		if($customersData["UseSalesmanDeliveryAdress"] == '1') {
			$selectCustomersRecipientNotice = 'Es wird die Liefer-Adresse des Vertreters verwendet!';
			$selectCustomersRecipientMail = $salesmanDatas["salesmanMail"];
			$selectCustomersRecipientFax = $salesmanDatas["salesmanFax"];
			$selectCustomersNumber = $salesmanDatas["salesmanKundennummer"];
			$selectCustomersRecipientCustomerNumber = $salesmanDatas["salesmanKundennummer"];
			$selectCustomersRecipientName = $salesmanDatas["salesmanFirmenname"];
			$selectCustomersRecipientNameAdd = $salesmanDatas["salesmanFirmennameZusatz"];
			$selectCustomersRecipientManager = $salesmanDatas["salesmanManager"];
			$selectCustomersRecipientContact = $salesmanDatas["salesmanContact"];
			$selectCustomersRecipientStreet = $salesmanDatas["customersLieferadresseStrasse"];
			$selectCustomersRecipientStreetNumber = $salesmanDatas["customersLieferadresseHausnummer"];
			$selectCustomersRecipientZipcode = $salesmanDatas["customersLieferadressePLZ"];
			$selectCustomersRecipientCity = $salesmanDatas["customersLieferadresseOrt"];
			$selectCustomersRecipientCountry = $salesmanDatas["customersLieferadresseLand"];

			#$selectCustomersRecipientInvoicesNotPurchased = $salesmanDatas["salesmanDatasInvoicesNotPurchased"];
			#$selectCustomersRecipientBadPaymentBehavior = $salesmanDatas["salesmanDatasBadPaymentBehavior"];

			#$selectCustomersRecipientSepaExists = $salesmanDatas["salesmanSepaExists"];
			#$selectCustomersRecipientSepaValidUntilDate = $salesmanDatas["salesmanSepaValidUntilDate"];
			#$selectCustomersRecipientSepaRequestedDate = $salesmanDatas["salesmanSepaRequestedDate"];
		}
		if($customersData["UseSalesmanInvoiceAdress"] == '1') {
			$selectCustomersSecondaryRecipientNotice = 'Es wird die Rechnungs-Adresse des Vertreters verwendet!';
			$selectCustomersSecondaryRecipientMail = $salesmanDatas["salesmanMail"];
			$selectCustomersSecondaryNumber = $salesmanDatas["salesmanKundennummer"];
			$selectCustomersSecondaryRecipientCustomerNumber = $salesmanDatas["salesmanKundennummer"];
			$selectCustomersSecondaryRecipientName = $salesmanDatas["salesmanFirmenname"];
			$selectCustomersSecondaryRecipientNameAdd = $salesmanDatas["salesmanFirmennameZusatz"];
			$selectCustomersSecondaryRecipientManager = $salesmanDatas["salesmanManager"];
			$selectCustomersSecondaryRecipientContact = $salesmanDatas["salesmanContact"];
			$selectCustomersSecondaryRecipientStreet = $salesmanDatas["customersRechnungsadresseStrasse"];
			$selectCustomersSecondaryRecipientStreetNumber = $salesmanDatas["customersRechnungsadresseHausnummer"];
			$selectCustomersSecondaryRecipientZipcode = $salesmanDatas["customersRechnungsadressePLZ"];
			$selectCustomersSecondaryRecipientCity = $salesmanDatas["customersRechnungsadresseOrt"];
			$selectCustomersSecondaryRecipientCountry = $salesmanDatas["customersRechnungsadresseLand"];

			$selectCustomersRecipientInvoicesNotPurchased = $salesmanDatas["salesmanDatasInvoicesNotPurchased"];
			$selectCustomersRecipientBadPaymentBehavior = $salesmanDatas["salesmanDatasBadPaymentBehavior"];

			$selectCustomersRecipientSepaExists = $salesmanDatas["salesmanSepaExists"];
			$selectCustomersRecipientSepaValidUntilDate = $salesmanDatas["salesmanSepaValidUntilDate"];
			$selectCustomersRecipientSepaRequestedDate = $salesmanDatas["salesmanSepaRequestedDate"];
		}
	}
	else {
		if($customersData["UseSalesmanInvoiceAdress"] == '1') {
			$selectCustomersRecipientNotice = 'Es wird die Rechnungs-Adresse des Vertreters verwendet!';
			$selectCustomersRecipientMail = $salesmanDatas["salesmanMail"];
			$selectCustomersRecipientFax = $salesmanDatas["salesmanFax"];
			$selectCustomersNumber = $salesmanDatas["salesmanKundennummer"];
			$selectCustomersRecipientCustomerNumber = $salesmanDatas["salesmanKundennummer"];
			$selectCustomersRecipientName = $salesmanDatas["salesmanFirmenname"];
			$selectCustomersRecipientNameAdd = $salesmanDatas["salesmanFirmennameZusatz"];
			$selectCustomersRecipientManager = $salesmanDatas["salesmanManager"];
			$selectCustomersRecipientContact = $salesmanDatas["salesmanContact"];
			$selectCustomersRecipientStreet = $salesmanDatas["customersRechnungsadresseStrasse"];
			$selectCustomersRecipientStreetNumber = $salesmanDatas["customersRechnungsadresseHausnummer"];
			$selectCustomersRecipientZipcode = $salesmanDatas["customersRechnungsadressePLZ"];
			$selectCustomersRecipientCity = $salesmanDatas["customersRechnungsadresseOrt"];
			$selectCustomersRecipientCountry = $salesmanDatas["customersRechnungsadresseLand"];
			$selectDiscount = $salesmanDatas["salesmanRabatt"] ;
			$selectDiscountType = $salesmanDatas["salesmanRabattType"] ;
			$selectPaymentType = $salesmanDatas["salesmanBezahlart"];
			$selectPaymentCondition = $salesmanDatas["salesmanZahlungskondition"];

			$selectCustomersRecipientInvoicesNotPurchased = $salesmanDatas["salesmanDatasInvoicesNotPurchased"];
			$selectCustomersRecipientBadPaymentBehavior = $salesmanDatas["salesmanDatasBadPaymentBehavior"];

			$selectCustomersRecipientSepaExists = $salesmanDatas["salesmanSepaExists"];
			$selectCustomersRecipientSepaValidUntilDate = $salesmanDatas["salesmanSepaValidUntilDate"];
			$selectCustomersRecipientSepaRequestedDate = $salesmanDatas["salesmanSepaRequestedDate"];
		}
		if($customersData["UseSalesmanDeliveryAdress"] == '1') {
			$selectCustomersSecondaryRecipientNotice = 'Es wird die Liefer-Adresse des Vertreters verwendet!';
			$selectCustomersSecondaryRecipientMail = $salesmanDatas["salesmanMail"];
			$selectCustomersSecondaryNumber = $salesmanDatas["salesmanKundennummer"];
			$selectCustomersSecondaryRecipientCustomerNumber = $salesmanDatas["salesmanKundennummer"];
			$selectCustomersSecondaryRecipientName = $salesmanDatas["salesmanFirmenname"];
			$selectCustomersSecondaryRecipientNameAdd = $salesmanDatas["salesmanFirmennameZusatz"];
			$selectCustomersSecondaryRecipientManager = $salesmanDatas["salesmanManager"];
			$selectCustomersSecondaryRecipientContact = $salesmanDatas["salesmanContact"];
			$selectCustomersSecondaryRecipientStreet = $salesmanDatas["customersLieferadresseStrasse"];
			$selectCustomersSecondaryRecipientStreetNumber = $salesmanDatas["customersLieferadresseHausnummer"];
			$selectCustomersSecondaryRecipientZipcode = $salesmanDatas["customersLieferadressePLZ"];
			$selectCustomersSecondaryRecipientCity = $salesmanDatas["customersLieferadresseOrt"];
			$selectCustomersSecondaryRecipientCountry = $salesmanDatas["customersLieferadresseLand"];
		}
	}

	// EOF GET NEEDED ADDRESS DATAS DEPENDING ON DOCUMENT TYPE

	// BOF USE ORIGIN DATAS
		if($_POST["convertDocType"] == 'MA' || $_POST["convertDocType"] == 'M1' || $_POST["convertDocType"] == 'M2' || $_POST["convertDocType"] == 'M3' || $_POST["convertDocType"] == 'IK'){
			// $selectInvoiceDate = $arrLoadDocumentDatas["orderDocumentsProcessingDate"];
			$selectInvoiceDate = $arrLoadDocumentDatas["orderDocumentsInvoiceDate"];
		}

		if(!empty($arrLoadDocumentDatas)){
			#$selectCustomersOrderNumber
			#$selectCustomersRecipientMail
			#$selectCustomersRecipientMailCopy
			#$selectCustomersRecipientFax
			$selectDeliveryType = $arrLoadDocumentDatas["orderDocumentsDeliveryType"];
			$selectShippingCosts = str_replace(",", ".", $arrLoadDocumentDatas["orderDocumentsShippingCosts"]);
			$selectShippingCostsPackages = str_replace(",", ".", $arrLoadDocumentDatas["orderDocumentsShippingCostsPackages"]);
			$selectShippingCostsPerPackage = str_replace(",", ".", $arrLoadDocumentDatas["orderDocumentsShippingCostsPerPackage"]);
			$selectPackagingCosts = str_replace(",", ".", $arrLoadDocumentDatas["orderDocumentsPackagingCosts"]);
			$selectCashOnDelivery = str_replace(",", ".", $arrLoadDocumentDatas["orderDocumentsCashOnDelivery"]);
			$selectDiscount = str_replace(",", ".", $arrLoadDocumentDatas["orderDocumentsDiscount"]);
			$selectDiscountType = $arrLoadDocumentDatas["orderDocumentsDiscountType"];
			if($selectDiscountType == 'percent'){
				$selectDiscount = str_replace(",", ".", $arrLoadDocumentDatas["orderDocumentsDiscountPercent"]);
			}
			$selectMwSt = str_replace(",", ".", $arrLoadDocumentDatas["orderDocumentsMwst"]);
			$selectMwStType = $arrLoadDocumentDatas["orderDocumentsMwstType"];
			$selectPaymentType = $arrLoadDocumentDatas["orderDocumentsPaymentType"];
			$selectPaymentCondition = $arrLoadDocumentDatas["orderDocumentsPaymentCondition"];
			$selectPaymentStatus = $arrLoadDocumentDatas["orderDocumentsStatus"];
			$selectBankAccount = $arrLoadDocumentDatas["orderDocumentsBankAccount"];
			$selectDocumentsContent = $arrLoadDocumentDatas["orderDocumentsContent"];
			#$selectCustomersNumber = $arrLoadDocumentDatas["orderDocumentsContent"];
			#$selectCustomersRecipientName = $arrLoadDocumentDatas["orderDocumentsAddressCompany"];
			$selectCustomersOrderNumber = $arrLoadDocumentDatas["orderDocumentsCustomersOrderNumber"];
			#$selectDeliveryDate
			#$selectDeliveryTypeNumber
			$selectOrderDate = $arrLoadDocumentDatas["orderDocumentsOrderDate"];
			$selectDebitingDate = $arrLoadDocumentDatas["orderDocumentsDebitingDate"];

			// BOF SET PAYMENT STATUS TYPE FOR SPECIAL CONVERTED DOCUMENTS
				$selectPaymentStatusNew = "";
				if($_POST["editDocType"] == "" && in_array($_POST["convertDocType"], array("MA", "M1", "M2", "M3", "GU"))){
					$selectPaymentStatusNew = $arrPaymentStatusTypeDatasToShortName[$_POST["convertDocType"]];
				}
				if($selectPaymentStatusNew != ""){
					$selectPaymentStatus = $selectPaymentStatusNew;
				}
			// EOF SET PAYMENT STATUS TYPE FOR SPECIAL CONVERTED DOCUMENTS
		}
	// EOF USE ORIGIN DATAS

	// BOF USE SEND DATAS IF BACK BUTTON WAS USED
		if(!empty($arrPostSerializedDatas)){
			$selectCustomersOrderNumber = $arrPostSerializedDatas["selectCustomersOrderNumber"];
			$selectCustomersRecipientMail = $arrPostSerializedDatas["selectCustomersRecipientMail"];
			$selectCustomersRecipientMailCopy = $arrPostSerializedDatas["selectCustomersRecipientMailCopy"];
			$selectCustomersRecipientFax = $arrPostSerializedDatas["selectCustomersRecipientFax"];
			$selectDeliveryType = $arrPostSerializedDatas["selectDeliveryType"];
			$selectShippingCosts = str_replace(",", ".", $arrPostSerializedDatas["selectShippingCosts"]);
			$selectShippingCostsPackages = str_replace(",", ".", $arrPostSerializedDatas["selectPackagingCostsMultiplier"]);
			$selectShippingCostsPerPackage = str_replace(",", ".", $arrPostSerializedDatas["selectShippingCostsPerPackage"]);
			$selectPackagingCosts = str_replace(",", ".", $arrPostSerializedDatas["selectPackagingCosts"]);
			$selectCashOnDelivery = str_replace(",", ".", $arrPostSerializedDatas["selectCashOnDelivery"]);
			$selectDiscount = str_replace(",", ".", $arrPostSerializedDatas["selectDiscount"]);
			$selectDiscountType = $arrPostSerializedDatas["selectDiscountType"];
			if($selectDiscountType == 'percent'){
				# $selectDiscount = '';
			}
			$selectMwSt = str_replace(",", ".", $arrPostSerializedDatas["selectMwSt"]);
			$selectMwStType = $arrPostSerializedDatas["selectMwStType"];
			$selectPaymentType = $arrPostSerializedDatas["selectPaymentType"];
			$selectPaymentCondition = $arrPostSerializedDatas["selectPaymentCondition"];
			$selectPaymentStatus = $arrPostSerializedDatas["selectPaymentStatus"];
			$selectBankAccount = $arrPostSerializedDatas["selectBankAccount"];
			$selectDocumentsContent = $arrPostSerializedDatas["documentEditorText"];
			#$selectCustomersNumber
			$selectDeliveryDate = $arrPostSerializedDatas["selectDeliveryDate"];
			$selectDeliveryTypeNumber = $arrPostSerializedDatas["selectDeliveryTypeNumber"];
			$selectOrderDate = $arrPostSerializedDatas["selectOrderDate"];
			$selectDebitingDate = $arrPostSerializedDatas["selectDebitingDate"];
		}
	// EOF USE SEND DATAS IF BACK BUTTON WAS USED

	// BOF DON'T SEND MAIL COPY TO US (MANDATOR) IF SALESMAN IS MANDATOR
		if(
			in_array($_REQUEST["selectSalesman"], array("5684", "13301"))
			 ||
			in_array($arrLoadDocumentDatas["orderDocumentsSalesman"], array("5684", "13301"))
			 ||
			in_array($customersData["VertreterID"], array("5684", "13301"))
			) {
			$selectCustomersRecipientMailCopy = '';
		}
	// EOF DON'T SEND MAIL COPY TO US (MANDATOR) IF SALESMAN IS MANDATOR
?>
<?php
	require_once('inc/headerHTML.inc.php');

	$thisTitle = $arrDocumentTypeDatas[$_REQUEST["documentType"]]["createdDocumentsTypesName"] . '';
	if($_REQUEST["originDocumentType"] != '') {
		if($_REQUEST["convertDocType"] != '') {
			$thisTitle .= ' aus ' . $arrDocumentTypeDatas[$_REQUEST["originDocumentType"]]["createdDocumentsTypesName"] . ' ' . $arrLoadDocumentDatas["orderDocumentsNumber"] . ' umwandeln ' ;
		}
		else if($_REQUEST["copyDocType"] != '') {
			$thisTitle .= ' aus ' . $arrDocumentTypeDatas[$_REQUEST["originDocumentType"]]["createdDocumentsTypesName"] . ' ' . $arrLoadDocumentDatas["orderDocumentsNumber"] . ' kopieren ' ;
		}
		else if($_REQUEST["editDocType"] != '') {
			$thisTitle .= ' ' . $arrLoadDocumentDatas["orderDocumentsNumber"] . ' &auml;ndern ' ;
		}
	}
	else { $thisTitle .= ' erstellen'; }

	if($_REQUEST["isCollectiveInvoice"] == '1'){
		$thisTitle = "SAMMEL-" . $thisTitle;
	}
	$headerHTML = preg_replace("/{###TITLE###}/", ($thisTitle), $headerHTML);
	echo $headerHTML;

	require_once(FILE_MENUE_TOP);
?>
<div id="mainArea">
	<div id="mainContent">
		<?php require_once(FILE_MENUE_SIDEBAR); ?>
		<div id="contentArea">
			<div id="contentAreaElements">
				<?php
				if($_REQUEST["editID"] != '') {
					$thisTitle .= ': <span  class="headerSelectedEntry">' . $_REQUEST["editID"] . ' &bull; ' . $customersData["Firmenname"] . '</span>';
				}
				?>
				<h1><?php if(SHOW_PAGE_TITLE_ICONS) { echo '<img src="' . PATH_ICONS_MENUE_TITLES . 'create.png" alt="" />'; } ?> <?php echo ($thisTitle); ?></h1>

				<?php
					if(preg_match("/;/", $arrRelatedDocumentNumbers['LS'])){
						echo '<p class="warningArea"><b>ACHTUNG:</b> Auftrag besteht aus Teillieferungen!!!! <b>' . preg_replace("/;/", " / ", $arrRelatedDocumentNumbers['LS']) . '</b></p>';
					}
				?>

				<div class="xxcontentDisplay">
				<?php
				if($_POST["createDocument"] != "" && $_REQUEST["editID"] == "")				{
					$errorMessage .= ' Sie haben nicht alle notwendigen Daten eingegeben. '.'<br />';
				}
				?>
				<?php
					if($_POST["originDocumentType"] == 'AN' && $_POST["convertDocType"] == 'AB') {
						/*
						if($userDatas["usersLogin"] != 'thorsten'){
							$errorMessage .= ' Bitte diesen Vorgang an THORSTEN geben!!!!. '.'<br />';
							displayMessages();
							exit;
						}
						else {
							echo "TEST<br>";
							$sql = "
									INSERT INTO `" . TABLE_ORDERS . "`
										`ordersID`,
										`ordersKundennummer`,
										`ordersCustomerID`,
										`ordersStatus`,
										`ordersSourceType`,
										`ordersBestellDatum`,
										`ordersLieferwoche`,
										`ordersKundenName`,
										`ordersKommission`,
										`ordersInfo`,
										`ordersPLZ`,
										`ordersOrt`,
										`ordersArtikelID`,
										`ordersArtikelNummer`,
										`ordersArtikelBezeichnung`,
										`ordersArtikelKategorieID`,
										`ordersArtikelMenge`,
										`ordersArtikelPrintColorsCount`,
										`ordersArtikelWithBorder`,
										`ordersAufdruck`,
										`ordersAdditionalArtikelID`,
										`ordersAdditionalArtikelNummer`,
										`ordersAdditionalArtikelBezeichnung`,
										`ordersAdditionalArtikelKategorieID`,
										`ordersAdditionalArtikelMenge`,
										`ordersDruckFarbe`,
										`ordersAdditionalCosts`,
										`ordersAdditionalCostsID`,
										`ordersVertreter`,
										`ordersVertreterID`,
										`ordersContactOthers`,
										`ordersMandant`,
										`ordersOrderType`,
										`ordersPerExpress`,
										`ordersBankAccountType`,
										`ordersPaymentType`,
										`ordersUserID`,
										`ordersTimeCreated`,
										`ordersArchive`

										VALUES
								";

							if(!empty($_POST["selectOrdersDetails"])){
								$arrSQL = array();
								$thisDefaultLieferwoche = (date("W") + MIN_SHIPPING_TIME);
								if($thisDefaultLieferwoche > 52) {
									$thisDefaultLieferwoche = $thisDefaultLieferwoche - 52;
								}

								foreach($_POST["selectOrdersDetails"] as $thisKey => $thisValue) {
									$arrSQL[] = "(
													'%',
													'" . $_POST["selectCustomersNumber"] . "',
													`ordersCustomerID`,
													1,
													9,
													'" . $_POST["selectDocumentDate"] . "',
													" . $thisDefaultLieferwoche . "',
													'" . $_POST["selectCustomersName"] . "',
													'" . $thisValue["ordersKommission"] . "',
													'" . $thisImportToOrders["PLZ"] . "',
													'" . $thisImportToOrders["PLZ"] . "',
													'0',
													'" . $thisValue["ordersArtikelNummer"] . "',
													'" . $thisValue["ordersArtikelBezeichnung"] . "',
													'" . $thisValue["ordersArtikelKategorieID"] . "',
													'" . $thisValue["ordersArtikelMenge"] . "',
													'" . $thisValue["ordersArtikelPrintColorsCount"] . "',
													'" . $thisValue["ordersArtikelMitUmrandung"] . "',
													'" . $thisValue["ordersAufdruck"] . "',
													'" . $thisValue["ordersAdditionalArtikelID"] . "',
													'',
													'',
													'" . $thisValue["ordersAdditionalArtikelKategorieID"] . "',
													'" . $thisValue["ordersAdditionalArtikelMenge"] . "',
													'" . $thisValue["ordersArtikelBezeichnungZusatz"] . "',
													'" . $thisValue["ordersAdditionalCosts"] . "',
													'" . $thisValue["ordersAdditionalCostsID"] . "',
													'" . $arrAgentDatas2[$_POST["selectSalesman"]]["customersKundennummer"] . "',
													'" . $_POST["selectSalesman"] . "',
													'" . $_POST["selectCustomersRecipientMail"] . "',
													'" . MANDATOR . "',
													'" . $thisValue["ordersOrderType"] . "',
													'" . $thisValue["orderPerShippingExpress"] . "',
													'" . $_POST["selectBankAccount"] . "',
													'" . $_POST["selectPaymentCondition"] . "',
													" . $userDatas["usersID"] . "',
													NOW(),
													'0'
												)
										";
								}
								$sql .= implode(',', $arrSQL);
								echo $sql . '<br />';
							}

						}*/
					}

				?>

				<?php
				if($_POST["storeDatas"] != "" || $_POST["storeAndSendDatas"] != "") {
					$openPdfFile = true;
					$printPdfFile = true;
					if($printPdfFile || $openPdfFile) {
						echo '<script language="javascript" type="text/javascript">';
						echo 'openPDF("' . DIRECTORY_CREATED_DOCUMENTS_CUSTOMERS . $thisCreatedPdfName . '", ' , $printPdfFile . ', "printPDF_' . $_REQUEST["documentType"] . '");';
						echo '</script>';

					}
					if($showPdfDownloadLink) {
						// echo '<p class="dataTableContent"><b>DOWNLOAD-LINK: </b><a href="'.DIRECTORY_CREATED_DOCUMENTS_CUSTOMERS . $thisCreatedPdfName.'" target="_blank">'.$thisCreatedPdfName.'</a></p>';
						echo '<p class="dataTableContent"><b>DOWNLOAD-LINK: </b> <img src="layout/icons/icon' . getFileType(basename($thisCreatedPdfName)) . '.gif" height="16" width="16" alt="" /> <a href="' . $_SERVER["REQUEST_URI"] . '?documentType=' . $_REQUEST["documentType"] . '&downloadFile=' . $thisCreatedPdfName.'">'.$thisCreatedPdfName.'</a></p>';
					}
					echo '<hr />';
					$_REQUEST["editID"] = "";
				}
				?>
				<?php displayMessages(); ?>
				<?php
					if($_REQUEST["editID"] == "") {
				?>
				<p>Bitte w&auml;hlen Sie zuerst den zu bearbeitenden Kunden aus.</p>
				<div id="searchFilterArea">
					<form name="formFilterSearch" method="post" action="<?php echo $_SERVER["REQUEST_URI"]; ?>">
					<table border="0" cellpadding="0" cellspacing="0" width="" class="searchFilterContent">
						<tr>
							<td>
								<label for="searchCustomerNumber">Kundennummer:</label>
								<input type="text" name="searchCustomerNumber" id="searchCustomerNumber" class="inputField_100" value="" />
							</td>
							<td>
								<label for="searchCustomerName">Kundenname:</label>
								<input type="text" name="searchCustomerName" id="searchCustomerName" class="inputField_160" readonly="readonly" value="" />
							</td>
							<td>
								<input type="hidden" name="editID" id="editID" value="<?php echo $_REQUEST["searchCustomerNumber"]; ?>" />
								<input type="hidden" name="documentType" value="<?php echo $_REQUEST["documentType"]; ?>" />
								<input type="submit" value="<?php echo $_REQUEST["documentType"]; ?>-Erstellung fortsetzen" title="<?php echo $arrDocumentTypeDatas[$_REQUEST["documentType"]]["createdDocumentsTypesName"]; ?> erstellen" class="inputButton1 inputButtonGreen" name="createDocumentConfig" />
							</td>
						</tr>
					</table>
					</form>
				</div>
				<div class="clear"></div>
				<p><a href="<?php echo PAGE_EDIT_CUSTOMER; ?>?searchCustomerNumber=<?php echo $_POST["selectCustomersNumber"]; ?>" class="linkButton">Kundendaten ansehen</a></p>
				<div class="clear"></div>
				<?php
					}
					$customerDatasOK = true;
					if($_REQUEST["editID"] != "" && $_REQUEST["createDocumentNow"] == "" && $_REQUEST["documentType"] != ""){
						if($customersData["DatasUpdated"] != '1') {
							$errorMessage .= "Bitte kontrollieren Sie die Kunden-Daten mit den Daten des alten Auftragslisten-Programms und speichern Sie diese! Ansonsten ist dieser Kunde zur weiteren Verwendung gesperrt! <br />";
							$customerDatasOK = false;
						}
						if($customersData["VertreterID"] > 0 && $salesmanDatas["customersDatasUpdated"] == '1') {
							$errorMessage .= "Bitte kontrollieren Sie die Daten des diesem Kunden zugeordneten Vertreters mit den Daten des alten Auftragslisten-Programms und speichern Sie diese! Ansonsten ist dieser Kunde zur weiteren Verwendung gesperrt! <br />";
							$customerDatasOK = false;
						}
					}

					if($customerDatasOK && $_REQUEST["editID"] != "" && $_REQUEST["createDocumentNow"] == "" && $_REQUEST["documentType"] != ""){
				?>
				<div class="xxadminEditArea">
					<div id="selectOrders">
						<form name="formGetOrdersForDocument" method="post" action="<?php echo $_SERVER["REQUEST_URI"]; ?>" enctype="multipart/form-data">
						<?php
						// if(($_REQUEST["documentType"] == "AB" || $_REQUEST["documentType"] == "RE" ) && $_POST["originDocumentType"] == '')

						// BOF LOAD DATAS FROM ORIGIN DOCUMENT
						if(($_POST["convertDocType"] != '' || $_POST["copyDocType"] != '' || $_POST["editDocType"] != '') && $_POST["editDocType"] != 'BR' && $_POST["originDocumentID"] != '' && $_POST["originDocumentType"] != '' && $_POST["editID"] != '') {
							// BOF CREATE ORDER SELECT FORM
								dd('arrLoadDocumentDatas');
								dd('arrLoadDocumentDetailDatas');


								if($_REQUEST["isCollectiveInvoice"] == '1'){
									$loadOrderConfirmationsForm = loadOrderConfirmationsForm('edit');
									echo "xxx edit" . $loadOrderConfirmationsForm;
								}
								else {
									if(!empty($arrLoadDocumentDetailGroupIDs) || !empty($arrNewProductDatas)) {
										echo '<div class="adminEditArea">';
										#echo '<p class="infoArea">Bitte w&auml;hlen Sie die zu &uuml;bernehmenden Vorg&auml;nge aus.</p>';
										// echo '<p><a href="' . PAGE_EDIT_PROCESS . '?editID=NEW" class="linkButton" title="Weiteren Produktion anlegen" ><span>Produktion anlegen </span></a></p>';
										#echo '<div class="clear"></div>';
										#echo '<hr />';
										echo '<fieldset>';
										echo '<legend>Artikel-Daten aus dem aktuellen Dokument</legend>';

										echo '<div id="addProductFormArea">';
										$count = 0;
										// displayDebug("arrOrderDetailDatas");
										echo '<input type="hidden" name="originDocumentNumber" value="' . $arrLoadDocumentDatas["orderDocumentsNumber"] . '">';
										echo '<input type="hidden" name="orderDocumentsID" value="' . $arrLoadDocumentDatas["orderDocumentsID"] . '">';
										echo '<input type="hidden" name="originDocumentID" value="' . $_REQUEST["originDocumentID"] . '">';

										if($_POST["editDocType"] != '') {
											echo '<input type="hidden" name="editDocType" value="' . $_REQUEST["editDocType"] . '">';
										}
										if($_POST["convertDocType"] != '') {
											echo '<input type="hidden" name="convertDocType" value="' . $_REQUEST["convertDocType"] . '">';
										}
										if($_POST["copyDocType"] != '') {
											echo '<input type="hidden" name="copyDocType" value="' . $_REQUEST["copyDocType"] . '">';
										}

										echo '<input type="hidden" name="originDocumentType" value="' . $_REQUEST["originDocumentType"] . '">';

										#dd('arrLoadDocumentDetailDatas');
										foreach($arrLoadDocumentDetailGroupIDs as $thisGroupID) {
											if($count%2 == 0) { $classRow = 'row0'; }
											else { $classRow = 'row1'; }

											if(!empty($arrReturnedProductDatas) && $_REQUEST["returnDatas"] != '') {
												$arrLoadDocumentDetailDatas = $arrReturnedProductDatas;
											}
											// BOF GET PRODUCT EK-PRICES
												if(!$dbConnection_Extern){
													$dbConnection_Extern = new DB_Connection(DB_HOST_EXTERN_SHOP, '', DB_NAME_EXTERN_SHOP, DB_USER_EXTERN_SHOP, DB_PASSWORD_EXTERN_SHOP);
													$db_openExtern = $dbConnection_Extern->db_connect();
												}
												$arrLoadDocumentDetailDatas[$thisGroupID]["product"]["productEkPrice"] = getEkPrice($arrLoadDocumentDetailDatas[$thisGroupID]["orderDocumentDetailProductNumber"], $arrLoadDocumentDetailDatas[$thisGroupID]["orderDocumentDetailProductQuantity"]);
											// EOF GET PRODUCT EK-PRICES
											#d('arrLoadDocumentDetailDatas');
											echo loadAddProductForm('edit', $arrLoadDocumentDetailDatas[$thisGroupID], 1, $classRow, $thisGroupID );
											$count++;
										}
										echo '</div>';
										echo '</fieldset>';

										if($_POST["editDocType"] == 'AN') {
											if($userDatas["usersLogin"] != 'xxxthorsten'){
												$loadAddProductForm = loadAddProductForm();
												echo '<div class="actionButtonsArea">';
												echo '<div class="linkButton"><span class="buttonAddProduct"> Produkt hinzuf&uuml;gen</span></div>';
												echo '<div class="clear"></div>';
												echo '</div>';
											}
										}

										echo '</div/>';
									}
									else if($_REQUEST["originDocumentType"] == '') {
										echo '<p class="warningArea">F&uuml;r diesen Kunden sind keine Vorg&auml;nge <b>unter diesem Mandaten</b> eingetragen.<br />Bitte <b>erst pr&uuml;fen ob eine Produktion unter einem anderen Mandanten angelegt worden ist</b>.<br />Wenn nicht, erstellen Sie erst dann einen Produktions-Vorgang.</p>';
										echo '<p><a href="' . PAGE_EDIT_PROCESS . '?editID=NEW" class="linkButton" title="Neuen Produktions-Vorgang anlegen" ><span>Neuen Produktions-Vorgang anlegen</span></a></p>';
										echo '<div class="clear"></div>';
										echo '<hr />';
									}
								}
							// EOF CREATE ORDER SELECT FORM
						}
						// EOF LOAD DATAS FROM ORIGIN DOCUMENT

						// BOF LOAD DATAS FROM ORDER PRODUCTIONS
						if(($_REQUEST["documentType"] == "AB" || $_REQUEST["documentType"] == "RE")) {
							// BOF GET ORDER DATAS
							$sql = "
								SELECT
									`ordersID`,
									`ordersKundennummer`,
									`ordersCustomerID`,
									`ordersStatus`,
									`ordersSourceType`,
									`ordersBestellDatum`,
									`ordersFreigabeDatum`,
									`ordersBelichtungsDatum`,
									`ordersAuslieferungsDatum`,
									`ordersLieferwoche`,
									`ordersLieferDatum`,
									`ordersKundenName`,
									`ordersKommission`,
									`ordersInfo`,
									`ordersPLZ`,
									`ordersOrt`,
									`ordersArtikelID`,
									`ordersArtikelNummer`,
									`ordersArtikelBezeichnung`,
									`ordersArtikelKategorieID`,
									`ordersArtikelMenge`,
									`ordersArtikelPrintColorsCount`,
									`ordersArtikelWithBorder`,
									`ordersAufdruck`,
									`ordersAdditionalArtikelID`,
									`ordersAdditionalArtikelNummer`,
									`ordersAdditionalArtikelBezeichnung`,
									`ordersAdditionalArtikelKategorieID`,
									`ordersAdditionalArtikelMenge`,
									`ordersSinglePreis`,
									`ordersTotalPreis`,
									`ordersDruckFarbe`,
									`ordersAdditionalCosts`,
									`ordersAdditionalCostsID`,
									`ordersVertreter`,
									`ordersVertreterID`,
									`ordersMailAdress`,
									`ordersMandant`,
									`ordersOrderType`,
									`ordersPerExpress`,
									`ordersBankAccountType`,
									`ordersPaymentType`,
									`ordersPaymentStatusType`,
									`ordersNotizen`,
									`ordersDruckerName`,
									`ordersUserID`,
									`ordersTimeCreated`,
									`ordersArchive`,
									`ordersUseNoStock`,
									`ordersDirectSale`,
									`ordersReklamationsgrund`

									FROM `" . TABLE_ORDERS . "`
								";

							// BOF NEW
							// if($userDatas["usersLogin"] == "thorsten"){
							if(1){
							if($_REQUEST["documentType"] == "AB" || $_REQUEST["documentType"] == "RE") {
								$sql .= "	LEFT JOIN `" . TABLE_ORDER_CONFIRMATIONS_DETAILS . "`
											ON(`" . TABLE_ORDERS . "`.`ordersID` = `" . TABLE_ORDER_CONFIRMATIONS_DETAILS . "`.`orderDocumentDetailOrderID`)

											LEFT JOIN `" . TABLE_ORDER_INVOICES_DETAILS . "`
											ON(`" . TABLE_ORDERS . "`.`ordersID` = `" . TABLE_ORDER_INVOICES_DETAILS . "`.`orderDocumentDetailOrderID`)

											WHERE 1
												AND `" . TABLE_ORDERS . "`.`ordersKundennummer` = '".$_REQUEST["editID"]."'
												AND `" . TABLE_ORDER_INVOICES_DETAILS . "`.`orderDocumentDetailOrderID` IS NULL
												AND `" . TABLE_ORDER_CONFIRMATIONS_DETAILS . "`.`orderDocumentDetailOrderID` IS NULL
												AND `" . TABLE_ORDERS . "`.`ordersArchive` != '1'
								";
							}
							}
							// EOF NEW

							$sql .= "

									AND `" . TABLE_ORDERS . "`.`ordersMandant` = '" . strtoupper(MANDATOR). "'

									/* AND `ordersBestellDatum` > '2013-03-15' */

									GROUP BY `" . TABLE_ORDERS . "`.`ordersID`

									ORDER BY `ordersBestellDatum` DESC

									/* LIMIT 10 */
							";

							$rs = $dbConnection->db_query($sql);

							$dbConnection_Extern = new DB_Connection(DB_HOST_EXTERN_SHOP, '', DB_NAME_EXTERN_SHOP, DB_USER_EXTERN_SHOP, DB_PASSWORD_EXTERN_SHOP);
							$db_openExtern = $dbConnection_Extern->db_connect();

							$countRow = 1;
							while($ds = mysqli_fetch_assoc($rs)) {
								$arrOrderDetailIDs[] = $ds["ordersID"];
								#foreach(array_keys($ds) as $field) {
									#$arrOrderDetailDatasOLD[$ds["ordersID"]][$field] = $ds[$field];
								#}

								$arrOrderDetailDatas[$ds["ordersID"]]["product"] = array(
														"orderDocumentBestellDatum" => $ds["ordersBestellDatum"],
														"ordersVertreterName" => $ds["ordersVertreter"],
														"orderDocumentFreigabeDatum" => $ds["ordersFreigabeDatum"],
														"orderDocumentDetailID" => '',
														"orderDocumentDetailDocumentID" => '',
														"orderDocumentDetailOrderID" => $ds["ordersID"],
														"orderDocumentDetailPos" => $countRow,
														"orderDocumentDetailProductNumber" => $ds["ordersArtikelNummer"],
														"orderDocumentDetailProductName" => $ds["ordersArtikelBezeichnung"],
														"orderDocumentDetailProductKategorieID" => $ds["ordersArtikelKategorieID"],
														"orderDocumentDetailProductQuantity" => $ds["ordersArtikelMenge"],
														"orderDocumentDetailProductColorsText" => $ds["ordersDruckFarbe"],
														"orderDocumentDetailProductColorsCount" => $ds["ordersArtikelPrintColorsCount"],
														"orderDocumentDetailsWithBorder" => $ds["ordersArtikelWithBorder"],
														"orderDocumentDetailPrintText" => $ds["ordersAufdruck"],
														"orderDocumentDetailKommission" => $ds["ordersKommission"],
														//"orderDocumentDetailNotiz" => $ds["ordersNotiz"],
														"orderDocumentDetailNotiz" => $ds["ordersInfo"],
														"orderDocumentPerExpress" => $ds["ordersPerExpress"],
														"orderDocumentDetailProductSinglePrice" => '',
														"orderDocumentDetailProductTotalPrice" => '',
														"orderDocumentDetailMwst" => MWST,
														"orderDocumentDetailDiscount" => '',
														"orderDocumentDetailDirectSale" => $ds["ordersDirectSale"],
														"orderDocumentDetailPosType" => 'product',
														"orderDocumentDetailOrderType" => $ds["ordersOrderType"],
														"orderDocumentDetailGroupingID" => $ds["ordersID"],
														"ordersAdditionalCostsID" => $ds["ordersAdditionalCostsID"],
														"orderDocumentDetailProductSinglePrice" => $ds["ordersAdditionalCosts"]
									);


								if($ds["ordersAdditionalArtikelMenge"] > 0) {
									$arrOrderDetailDatas[$ds["ordersID"]]["additionalProducts"] = array(
														"orderDocumentBestellDatum" => '',
														"ordersVertreterName" => '',
														"orderDocumentFreigabeDatum" => '',
														"orderDocumentDetailID" => '',
														"orderDocumentDetailDocumentID" => '',
														"orderDocumentDetailOrderID" => $ds["ordersID"],
														"orderDocumentDetailPos" => $countRow,
														"orderDocumentDetailProductNumber" => $ds["ordersAdditionalArtikelNummer"],
														"orderDocumentDetailProductName" => $ds["ordersAdditionalArtikelBezeichnung"],
														"orderDocumentDetailProductKategorieID" => $ds["ordersAdditionalArtikelKategorieID"],
														"orderDocumentDetailProductQuantity" => $ds["ordersAdditionalArtikelMenge"],
														"orderDocumentDetailProductColorsText" => '',
														"orderDocumentDetailProductColorsCount" => 0,
														"orderDocumentDetailsWithBorder" => '',
														"orderDocumentDetailPrintText" => '',
														"orderDocumentDetailKommission" => '',
														"orderDocumentDetailNotiz" => '',
														"orderDocumentPerExpress" => $ds["ordersPerExpress"],
														"orderDocumentDetailProductSinglePrice" => '',
														"orderDocumentDetailProductTotalPrice" => '',
														"orderDocumentDetailMwst" => MWST,
														"orderDocumentDetailDiscount" => '',
														"orderDocumentDetailDirectSale" => '',
														"orderDocumentDetailPosType" => 'additionalProducts',
														"orderDocumentDetailOrderType" => '',
														"orderDocumentDetailGroupingID" => $ds["ordersID"],
														"ordersAdditionalCostsID" => '',
														"orderDocumentDetailProductSinglePrice" => ''
										);
								}

								// if($ds["ordersOrderType"] == '1' && $ds["ordersArtikelPrintColorsCount"] > 0) {
								if($ds["ordersArtikelPrintColorsCount"] > 0) {
									$arrOrderDetailDatas[$ds["ordersID"]]["printCosts"] = array(
														"orderDocumentBestellDatum" => '',
														"ordersVertreterName" => '',
														"orderDocumentFreigabeDatum" => '',
														"orderDocumentDetailID" => '',
														"orderDocumentDetailDocumentID" => '',
														"orderDocumentDetailOrderID" => $ds["ordersID"],
														"orderDocumentDetailPos" => $countRow,
														"orderDocumentDetailProductNumber" => "SATZK",
														"orderDocumentDetailProductName" => "einmalige Satzkosten pro Farbe  (entfallen bei Nachbestellung)",
														"orderDocumentDetailProductKategorieID" => '',
														"orderDocumentDetailProductQuantity" => $ds["orderDocumentDetailProductColorsCount"],
														"orderDocumentDetailProductColorsText" => '',
														"orderDocumentDetailProductColorsCount" => 0,
														"orderDocumentDetailsWithBorder" => '',
														"orderDocumentDetailPrintText" => '',
														"orderDocumentDetailKommission" => '',
														"orderDocumentDetailNotiz" => '',
														"orderDocumentPerExpress" => $ds["ordersPerExpress"],
														"orderDocumentDetailProductSinglePrice" => '',
														"orderDocumentDetailProductTotalPrice" => '',
														"orderDocumentDetailMwst" => MWST,
														"orderDocumentDetailDiscount" => '',
														"orderDocumentDetailDirectSale" => '',
														"orderDocumentDetailPosType" => 'printCosts',
														"orderDocumentDetailOrderType" => '',
														"orderDocumentDetailGroupingID" => $ds["ordersID"],
														"ordersAdditionalCostsID" => $ds["ordersAdditionalCostsID"],
														"orderDocumentDetailProductSinglePrice" => $ds["ordersAdditionalCosts"]
										);
								}

								if($ds["ordersPerExpress"] == '1') {
									$arrOrderDetailDatas[$ds["ordersID"]]["expressCosts"] = array(
														"orderDocumentBestellDatum" => '',
														"ordersVertreterName" => '',
														"orderDocumentFreigabeDatum" => '',
														"orderDocumentDetailID" => '',
														"orderDocumentDetailDocumentID" => '',
														"orderDocumentDetailOrderID" => $ds["ordersID"],
														"orderDocumentDetailPos" => $countRow,
														"orderDocumentDetailProductNumber" => "EXP",
														"orderDocumentDetailProductName" => "Expressaufschlag",
														"orderDocumentDetailProductKategorieID" => '',
														"orderDocumentDetailProductQuantity" => $ds["orderDocumentDetailProductColorsCount"],
														"orderDocumentDetailProductColorsText" => '',
														"orderDocumentDetailProductColorsCount" => 0,
														"orderDocumentDetailsWithBorder" => '',
														"orderDocumentDetailPrintText" => '',
														"orderDocumentDetailKommission" => '',
														"orderDocumentDetailNotiz" => '',
														"orderDocumentPerExpress" => $ds["ordersPerExpress"],
														"orderDocumentDetailProductSinglePrice" => 0.20,
														"orderDocumentDetailProductTotalPrice" => '',
														"orderDocumentDetailMwst" => MWST,
														"orderDocumentDetailDiscount" => '',
														"orderDocumentDetailDirectSale" => '',
														"orderDocumentDetailPosType" => 'expressCosts',
														"orderDocumentDetailOrderType" => '',
														"orderDocumentDetailGroupingID" => $ds["ordersID"],
														"ordersAdditionalCostsID" => '',
														"orderDocumentDetailProductSinglePrice" => ''
										);
								}

								// BOF GET PRODUCT PRICES
									$ordersSinglePreis = 0;
									$sql_getSinglePrice = "
											SELECT
												`" . TABLE_PERSONAL_OFFERS_BY_CUSTOMERS . DEFAULT_PRICE_GROUP . "`.`personal_offer` AS `ordersSinglePreis`

												FROM (
													SELECT
														`" . TABLE_PRODUCTS . "`.`products_id` AS `products_id`
														FROM `" . TABLE_PRODUCTS . "`
														WHERE `" . TABLE_PRODUCTS . "`.`products_model` = '" . $ds["ordersArtikelNummer"] . "'
															AND `" . TABLE_PRODUCTS . "`.`products_status` = 1

														GROUP BY `" . TABLE_PRODUCTS . "`.`products_id`

													UNION

													SELECT
														`" . TABLE_PRODUCTS_ATTRIBUTES . "`.`products_id`
														FROM `" . TABLE_PRODUCTS_ATTRIBUTES . "`
														LEFT JOIN `" . TABLE_PRODUCTS . "`
														ON (`" . TABLE_PRODUCTS_ATTRIBUTES . "`.`products_id` = `" . TABLE_PRODUCTS . "`.`products_id`)
														WHERE `" . TABLE_PRODUCTS_ATTRIBUTES . "`.`attributes_model` = '" . $ds["ordersArtikelNummer"] . "'
															AND `" . TABLE_PRODUCTS . "`.`products_status` = 1

														GROUP BY `" . TABLE_PRODUCTS_ATTRIBUTES . "`.`products_id`
												) AS `temp`

												LEFT JOIN `" . TABLE_PERSONAL_OFFERS_BY_CUSTOMERS . DEFAULT_PRICE_GROUP . "`
												ON(`temp`.`products_id` = `" . TABLE_PERSONAL_OFFERS_BY_CUSTOMERS . DEFAULT_PRICE_GROUP . "`.`products_id`)

												WHERE  `" . TABLE_PERSONAL_OFFERS_BY_CUSTOMERS . DEFAULT_PRICE_GROUP . "`.`quantity` <= '" . $ds["ordersArtikelMenge"] . "'
													AND `" . TABLE_PERSONAL_OFFERS_BY_CUSTOMERS . DEFAULT_PRICE_GROUP . "`.`quantity` > 0
													AND `" . TABLE_PERSONAL_OFFERS_BY_CUSTOMERS . DEFAULT_PRICE_GROUP . "`.`personal_offer` > 0

												LIMIT 1
										";

									$rs_getSinglePrice = $dbConnection_Extern->db_query($sql_getSinglePrice);

									list($ordersSinglePreis) = mysqli_fetch_array($rs_getSinglePrice);
									if($ordersSinglePreis == ''){$ordersSinglePreis = 0;}
									$arrOrderDetailDatas[$ds["ordersID"]]["product"]["orderDocumentDetailProductSinglePrice"] = $ordersSinglePreis;

									if($dbConnection_Extern->db_displayErrors() != "") {
										$errorMessage .= $dbConnection_Extern->db_displayErrors(). '<br />';
									}
								// EOF GET PRODUCT PRICES

								// BOF GET PRODUCT EK-PRICES
									$arrOrderDetailDatas[$ds["ordersID"]]["product"]["productEkPrice"] = getEkPrice($ds["ordersArtikelNummer"], $ds["ordersArtikelMenge"]);
								// EOF GET PRODUCT EK-PRICES
							}

							$dbConnection_Extern->db_close($db_openExtern);
							// EOF GET ORDER DATAS
							#displayDebug("arrOrderDetailDatasOLD");
							// BOF CREATE ORDER SELECT FORM
							if(!empty($arrOrderDetailIDs) && ($_REQUEST["documentType"] == 'AB')) {
								if($_POST["editDocType"] != '' && $_POST["editDocType"] != 'BR' && $_POST["editID"] != '') {
									$displayProductionOrders = false;
								}
								else {
									$displayProductionOrders = true;
								}

								if(!$displayProductionOrders) {
									echo '<p><a href="javascript:void(0);" onclick="$(\'#showProductionOrdersArea\').toggle();" class="linkButton" id="buttonShowProductionOrders" title="Offene Produktionen anzeigen und zum Dokument hinzuf&uuml;gen" >Offene Produktionen anzeigen und zum Dokument hinzuf&uuml;gen</a></p>';
									echo '<div class="clear"></div><br />';
									echo '<div id="showProductionOrdersArea" style="display:none;">';
								}

								echo '<div class="adminEditArea">';
								echo '<p class="infoArea">Bitte w&auml;hlen Sie die zu &uuml;bernehmenden Vorg&auml;nge aus.</p>';
								echo '<p><a href="' . PAGE_EDIT_PROCESS . '?editID=NEW" class="linkButton" title="Weitere Produktion anlegen" ><span>Weitere Produktion anlegen</span></a></p>';
								echo '<div class="clear"></div>';
								echo '<hr />';
								echo '<fieldset>';
								echo '<legend>Artikel-Daten aus Produktionen</legend>';
								echo '<div id="selectProductFormArea">';
								$count = 0;
								#displayDebug("arrOrderDetailDatas");
								foreach($arrOrderDetailIDs as $thisOrderDetailIDs) {
									if($count%2 == 0) { $classRow = 'row0';}
									else { $classRow = 'row1'; }
									if(!empty($arrReturnedProductDatas[$thisOrderDetailIDs])) {
										$arrOrderDetailDatas[$thisOrderDetailIDs] = $arrReturnedProductDatas[$thisOrderDetailIDs];
									}
									echo loadAddProductForm('select', $arrOrderDetailDatas[$thisOrderDetailIDs], 1, $classRow, $count, 1 );
									$count++;
								}
								echo '</div>';
								echo '</fieldset>';

								// if($_REQUEST["documentType"] == "AN")
								if($_REQUEST["documentType"] == "RE") {
									echo '<div class="actionButtonsArea">';
									echo '<div class="linkButton"><span class="buttonAddProduct"> Produkt hinzuf&uuml;gen (nicht in Produktion)</span></div>';
									echo '<div class="clear"></div>';
									echo '</div>';
								}

								echo '</div>';
								if(!$displayProductionOrders) {
									echo '</div>';
								}
							}
							else if($_REQUEST["originDocumentType"] == '') {
								if($_REQUEST["isCollectiveInvoice"] == '1'){
									#$loadOrderConfirmationsForm = loadOrderConfirmationsForm('add');
									#echo "x" . $loadOrderConfirmationsForm;
								}
								else {
									if($_REQUEST["documentType"] == "AB") {
										$thisMessage = "Für diesen Kunden sind keine Vorgänge unter diesem Mandaten eingetragen.\\nBitte erst prüfen ob eine Produktion unter einem anderen Mandanten angelegt worden ist.\\nWenn nicht, erstellen Sie erst dann einen Produktions-Vorgang." . "\\n";
										$jswindowMessage .= $thisMessage;
										echo '<p class="warningArea">' . nl2br(preg_replace("/\\\\n/", "\n", $thisMessage)) . '<span></span></p>';
									}
									echo '<p><a href="' . PAGE_EDIT_PROCESS . '?editID=NEW" class="linkButton" title="Neuen Produktions-Vorgang anlegen" ><span>Neuen Produktions-Vorgang anlegen</span></a></p>';
									echo '<div class="clear"></div>';
									echo '<hr />';
								}

								if($_REQUEST["documentType"] == "RE") {
									if($_REQUEST["isCollectiveInvoice"] == '1'){
										// BOF LOAD ABs FOR COLLECTIVE INVOICE
										echo '<div class="adminEditArea">';
										echo '<fieldset>';
										echo '<legend>ABs ohne Rechnungen</legend>';
										$loadOrderConfirmationsForm = loadOrderConfirmationsForm('add', $arrReturnedSelectedOrderConfirmations);
										echo $loadOrderConfirmationsForm;
										echo '</fieldset>';
										echo '</div>';
										// EOF LOAD ABs FOR COLLECTIVE INVOICE
									}
									else {
										// BOF LOAD ADD PRODUCT FORM
										echo '<div class="adminEditArea">';

										// <img src="layout/icons/iconAdd.png" alt="Produkt hinzuf&uuml;gen" title="Produkt hinzuf&uuml;gen" />
										echo '<fieldset>';
										echo '<legend>Artikel-Daten (nicht in Produktion)</legend>';
										echo '<div id="addProductFormArea">';
										if(empty($arrReturnedProductDatas)) {
											$loadAddProductForm = loadAddProductForm('add');
											echo $loadAddProductForm;
										}
										else {
											foreach($arrReturnedProductDatas as $thisProductDatasKey => $thisProductDatasValue) {
												echo loadAddProductForm('add', $thisProductDatasValue, 0, $classRow, $thisKey);
											}
											$loadAddProductForm = loadAddProductForm('add');
											echo $loadAddProductForm;
										}
										echo '</div>';
										echo '</fieldset>';

										echo '<div class="actionButtonsArea">';
										echo '<div class="linkButton"><span class="buttonAddProduct"> Produkt hinzuf&uuml;gen (nicht in Produktion)</span></div>';
										echo '<div class="clear"></div>';
										echo '</div>';

										if(!empty($arrOrderDetailIDs) && ($_REQUEST["documentType"] == 'RE')) {
											if($_POST["editDocType"] == '' && $_POST["convertDocType"] == '' && $_GET["editID"] != '') {
												echo '<div class="actionButtonsArea">';
												echo '<p><a href="javascript:void(0);" onclick="$(\'#showProductionOrdersArea\').toggle();" class="linkButton" id="buttonShowProductionOrders" title="Offene Produktionen anzeigen und zum Dokument hinzuf&uuml;gen" >Offene Produktionen anzeigen und zum Dokument hinzuf&uuml;gen</a></p>';
												echo '</div>';
												echo '<div class="clear"></div><br />';
												echo '<div id="showProductionOrdersArea" style="display:none;">';
												echo '<div class="adminEditArea">';
												echo '<p class="infoArea">Bitte w&auml;hlen Sie die zu &uuml;bernehmenden Vorg&auml;nge aus.</p>';
												echo '<p><a href="' . PAGE_EDIT_PROCESS . '?editID=NEW" class="linkButton" title="Weitere Produktion anlegen" ><span>Weitere Produktion anlegen</span></a></p>';
												echo '<div class="clear"></div>';
												echo '<hr />';
												echo '<fieldset>';
												echo '<legend>Artikel-Daten aus Produktionen</legend>';
												echo '<div id="selectProductFormArea">';
												$count = 0;
												#displayDebug("arrOrderDetailDatas");
												foreach($arrOrderDetailIDs as $thisOrderDetailIDs) {
													if($count%2 == 0) { $classRow = 'row0';}
													else { $classRow = 'row1'; }
													if(!empty($arrReturnedProductDatas[$thisOrderDetailIDs])) {
														$arrOrderDetailDatas[$thisOrderDetailIDs] = $arrReturnedProductDatas[$thisOrderDetailIDs];
													}
													echo loadAddProductForm('select', $arrOrderDetailDatas[$thisOrderDetailIDs], 1, $classRow, $count );
													$count++;
												}
												echo '</div>';
												echo '</fieldset>';

												echo '</div>';
											}
										}

										echo '</div>';
										// EOF LOAD ADD PRODUCT FORM
									}
								}
							}
							// EOF CREATE ORDER SELECT FORM
						}
						// BOF LOAD DATAS FROM ORDER PRODUCTIONS

						// BOF OFFER DATAS
						if(($_REQUEST["documentType"] == "AN" || $_REQUEST["documentType"] == "GU") && $_POST["originDocumentType"] == '') {
						// if(($_REQUEST["documentType"] == "AN" || $_REQUEST["documentType"] == "GU")) {
							// BOF LOAD ADD PRODUCT FORM
							echo '<div class="adminEditArea">';

							// <img src="layout/icons/iconAdd.png" alt="Produkt hinzuf&uuml;gen" title="Produkt hinzuf&uuml;gen" />
							echo '<fieldset>';
							echo '<legend>Artikel-Daten f&uuml;r das Angebot</legend>';
							echo '<div id="addProductFormArea">';
							if(empty($arrReturnedProductDatas)) {
								$loadAddProductForm = loadAddProductForm('add');
								echo $loadAddProductForm;
							}
							else {
								foreach($arrReturnedProductDatas as $thisProductDatasKey => $thisProductDatasValue) {
									echo loadAddProductForm('add', $thisProductDatasValue, 0, $classRow, $thisKey);
								}
								$loadAddProductForm = loadAddProductForm('add');
								#echo $loadAddProductForm;
							}
							echo '</div>';
							echo '</fieldset>';

							echo '<div class="actionButtonsArea">';
							echo '<div class="linkButton"><span class="buttonAddProduct">Produkt hinzuf&uuml;gen</span></div>';
							echo '<div class="clear"></div>';
							echo '</div>';

							echo '</div>';
							// EOF LOAD ADD PRODUCT FORM
						}
						// EOF OFFER DATAS
						else {}
					?>
					<?php displayMessages(); ?>
						<div class="adminEditArea">
							<?php
								if($_REQUEST["documentType"] == 'BR'){
							?>
							<fieldset>
								<legend>Brieftext</legend>
								<table border="0" cellpadding="0" cellspacing="0" width="100%">
									<tr>
										<!--
										<td style="width:200px;"><b>Brieftext:</b></td>
										-->
										<td colspan="2">
											<?php
												$CKEditor = new CKEditor();
												$CKEditor->returnOutput = true;
												$CKEditor->basePath = DIRECTORY_CKEDITOR;
												$CKEditor->config['height'] = 300;
												$CKEditor->config['width'] = 800;
												// $CKEditor->config['entities'] = false;
												$CKEditor->config['entities_latin'] = false;
												$CKEditor->textareaAttributes = array("cols" => 80, "rows" => 10);

												$config['toolbar'] = array(
													array('Bold', 'Italic', 'Underline', 'Superscript', 'Subscript', 'Table', 'HorizontalRule', 'SpecialChar' ),
													array('Styles','Format', 'NumberedList','BulletedList','-','Outdent','Indent','-','Blockquote','-','Source'),
													// array( 'Cut','Copy','Paste','PasteText','PasteFromWord','-','Undo','Redo' )
												);

												$config['skin'] = 'office2003'; // 'v2', 'office2003', 'kama'
												// $CKEditor->editor("additionalText", $initialValue);

												if($selectDocumentsContent != ''){
													$thisDocumentContent = $selectDocumentsContent;
													$thisDocumentContent = urldecode($thisDocumentContent);
												}
												else {
													$thisDocumentContent = '';
													$thisDocumentContent .= $arrContentDatas['BR']["SALUTATION_TEXT"];
													$thisDocumentContent .= '<br><br><br><br><br>';
													$thisDocumentContent .= $arrContentDatas['BR']["REGARDS_TEXT"];
												}

												$loadTextEditArea = $CKEditor->editor("documentEditorText", $thisDocumentContent, $config);
												echo $loadTextEditArea;
											?>
											<?php

												echo '<input type="hidden" name="selectDocumentsContent" value="' . $selectDocumentsContent . '" />';
												echo '<input type="hidden" name="originDocumentNumber" value="' . $arrLoadDocumentDatas["orderDocumentsNumber"] . '">';
												echo '<input type="hidden" name="orderDocumentsID" value="' . $arrLoadDocumentDatas["orderDocumentsID"] . '">';

												if($_POST["editDocType"] != '') {
													echo '<input type="hidden" name="editDocType" value="' . $_REQUEST["editDocType"] . '">';
												}
												if($_POST["convertDocType"] != '') {
													echo '<input type="hidden" name="convertDocType" value="' . $_REQUEST["convertDocType"] . '">';
												}
												if($_POST["copyDocType"] != '') {
													echo '<input type="hidden" name="copyDocType" value="' . $_REQUEST["copyDocType"] . '">';
												}
											?>
										</td>
									</tr>
								</table>
							</fieldset>
							<?php
								}
							?>

							<fieldset>
								<legend>Kundendaten</legend>
								<p><a href="<?php echo PAGE_EDIT_CUSTOMER; ?>?editID=<?php echo $customersData["customersID"]; ?>" onclick="return showWarning('Beim Verlassen dieser Seite werden alle bisher hier eingegebenen Daten nicht gespeichert.');" class="linkButton">Kundendaten ansehen</a></p>
								<table border="0" cellpadding="0" cellspacing="0" width="100%">
									<tr>
										<td style="width:200px;"><b>Kundennummer:</b></td>
										<td>
											<input type="text" name="selectCustomersNumber" id="selectCustomersNumber" class="inputField_100" readonly="readonly" value="<?php echo $_REQUEST["editID"]; ?>" />
											<input type="hidden" name="selectCustomersID" id="selectCustomersID" value="<?php echo $customersData["customersID"]; ?>" />
										</td>
									</tr>
									<tr>
										<td><b>Zahlungsmoral:</b></td>
										<td>
											<span class="checkBoxElement1" <?php if($selectCustomersRecipientInvoicesNotPurchased == '1' ) { echo 'style="background-color:#FF7F84;" '; } else { echo 'style="background-color:#00FF00;" '; } ?>>
												<?php if($cselectCustomersRecipientInvoicesNotPurchased == '1' ) { echo 'Kein Rechnungsankauf von TEBA '; } else { echo 'Rechnungsankauf von TEBA ist ok'; } ?>
											</span>
											<span class="checkBoxElementSeparator">&nbsp;</span>
											<span class="checkBoxElement1" <?php if($selectCustomersRecipientBadPaymentBehavior == '1' ) { echo 'style="background-color:#FF7F84;" '; } else { echo 'style="background-color:#00FF00;" '; } ?>>
												<?php if($selectCustomersRecipientBadPaymentBehavior == '1' ) { echo 'Kunde zahlt unp&uuml;nktlich '; } else { echo 'Kunde zahlt p&uuml;nktlich '; } ?>
											</span>
										</td>
									</tr>
									<tr>
										<td style="width:200px;"><b>Kundenname:</b></td>
										<td>
											<input type="text" name="selectCustomersName" id="selectCustomersName" class="inputField_510" readonly="readonly" value="<?php echo $customersData["Firmenname"]; ?>" />
											<br />
											<input type="text" name="selectCustomersNameAdd" id="selectCustomersNameAdd" class="inputField_510" readonly="readonly" value="<?php echo $customersData["FirmennameZusatz"]; ?>" />
											<div class="infotext">
												<b>Kundentyp:</b> <?php echo $arrCustomerTypeDatas[$customersData["Typ"]]["customerTypesName"]; ?>
												-
												<b>Kundengruppe:</b>
												<?php
													if(!empty($arrCustomerGroupDatas)) {
														$thisArrCustomerGroups = explode(";", stripslashes($customersData["Gruppe"]));
														foreach($arrCustomerGroupDatas as $thisKey => $thisValue) {
															$checked = '';
															if(!empty($thisArrCustomerGroups)) {
																if(in_array($thisKey, $thisArrCustomerGroups)) {
																	echo $arrCustomerGroupDatas[$thisKey]["customerGroupsName"];
																}
															}
														}
													}
													if($customersData["Gruppe"] == '') {
														echo ' keiner Gruppe zugeordnet ';
													}
												?>

											</div>
										</td>
									</tr>
									<?php
										if($_REQUEST["documentType"] != '') {
									?>
									<tr>
										<td style="width:200px;"><b>Auftragsnummer des Kunden:</b></td>
										<td><input type="text" name="selectCustomersOrderNumber" class="inputField_510" value="<?php if($selectCustomersOrderNumber !=""){ echo $selectCustomersOrderNumber; } else { echo ""; } ?>" /></td>
									</tr>
									<?php
										}
									?>
									<tr>
										<td><b>Vertreter dieses Kunden:</b></td>
										<td>
											<select name="selectSalesman" id="selectSalesman" class="inputSelect_510">
												<option value="0"> - </option>
												<?php
													if(!empty($arrAgentDatas2)) {
														foreach($arrAgentDatas2 as $thisKey => $thisValue) {
															$selected = '';
															// if($customerDatas["customersTyp"] > 0) {
															/*
															if($arrLoadDocumentDatas["orderDocumentsSalesman"] > 0) {
																if($thisKey == $arrLoadDocumentDatas["orderDocumentsSalesman"]) {
																	$selected = ' selected="selected" ';
																}
															}
															else {
																if($_REQUEST["selectSalesman"] > 0){
																	if($thisKey == $_REQUEST["selectSalesman"]) {
																		$selected = ' selected="selected" ';
																	}
																}
																else {
																	if($thisKey == $customersData["VertreterID"]) {
																		$selected = ' selected="selected" ';
																	}
																}
															}
															*/
															####
															if($_REQUEST["selectSalesman"] > 0){
																if($thisKey == $_REQUEST["selectSalesman"]) {
																	$selected = ' selected="selected" ';
																}
															}
															else {
																if($arrLoadDocumentDatas["orderDocumentsSalesman"] > 0) {
																	if($thisKey == $arrLoadDocumentDatas["orderDocumentsSalesman"]) {
																		$selected = ' selected="selected" ';
																	}
																}
																else {
																	if($thisKey == $customersData["VertreterID"]) {
																		$selected = ' selected="selected" ';
																	}
																}
															}

															#####

															echo '
																<option value="' . $thisKey . '" '.$selected.' >' . $selectCustomersSalesman . '' . ($arrAgentDatas2[$thisKey]["customersFirmenname"]). ' | '.$arrAgentDatas2[$thisKey]["customersKundennummer"].'</option>';
														}
													}
												?>
											</select>
										</td>
									</tr>
								</table>
							</fieldset>

							<fieldset>
								<legend>Dokumentdaten</legend>
								<table border="0" cellpadding="0" cellspacing="0" width="100%">
									<tr>
										<td style="width:200px;"><b>Dokumentdatum:</b></td>
										<td>
											<?php
												if($_REQUEST["editDocType"] != ''){
											?>
											<input type="text" name="selectDocumentDate" id="selectDocumentDate" class="inputField_100" readonly="readonly" value="<?php if($arrLoadDocumentDatas["orderDocumentsDocumentDate"] > 0){ echo formatDate($arrLoadDocumentDatas["orderDocumentsDocumentDate"], 'display'); } else { echo date('d.m.Y'); } ?>" />
											<?php
												}
												else {
											?>
											<input type="text" name="selectDocumentDate" id="selectDocumentDate" class="inputField_100" readonly="readonly" value="<?php echo date('d.m.Y'); ?>" />
											<?php
												}
											?>
										</td>
									</tr>
									<?php
									// BOF GENERATE DOCUMENT NUMBER ON DOCUMENT DATE YEAR AND MONTH
										if($userDatas["usersLogin"] == 'thorsten'){
											echo '<tr>';
											echo '<td><b>Dokument-Nummer:</b></td>';
											echo '<td>';
											echo '<p class="infoArea" style="width:504px;">';
											echo '<input type="checkbox" name="useDocumentDateForDocumentNumber" id="useDocumentDateForDocumentNumber" value="1" /> Dokumentnummer basierend auf Dokumentdatum generieren?<br />(z. B. um im Januar eine Rechnung f&uuml;r Dezember des Vorjahres zu schreiben.)';
											echo '</p>';
											echo '</td>';
										}
									// EOF GENERATE DOCUMENT NUMBER ON DOCUMENT DATE YEAR AND MONTH
									?>
									<tr>
										<td style="width:200px;"><b>Auftragsdatum:</b></td>
										<td>
											<!--
											<input type="text" name="selectOrderDate" id="selectOrderDate" class="inputField_100" readonly="readonly" value="<?php if($arrLoadDocumentDatas["orderDocumentsOrderDate"] > 0){ echo formatDate($arrLoadDocumentDatas["orderDocumentsOrderDate"], 'display'); } else { echo ""; } ?>" />
											-->
											<input type="text" name="selectOrderDate" id="selectOrderDate" class="inputField_100" readonly="readonly" value="<?php if($selectOrderDate > 0){ echo formatDate($selectOrderDate, 'display'); } else { echo formatDate($arrLoadDocumentDatas["orderDocumentsDocumentDate"], 'display'); } ?>" />
										</td>
									</tr>
									<?php
										if($_POST["convertDocType"] == 'MA' || $_POST["convertDocType"] == 'M1' || $_POST["convertDocType"] == 'M2' || $_POST["editDocType"] == 'MA' || $_POST["editDocType"] == 'M1' || $_POST["editDocType"] == 'M2'){
									?>
									<tr>
										<td style="width:200px;"><b>Rechnungssdatum:</b></td>
										<td>
											<!--
											<input type="text" name="selectOrderDate" id="selectOrderDate" class="inputField_100" readonly="readonly" value="<?php if($arrLoadDocumentDatas["orderDocumentsOrderDate"] > 0){ echo formatDate($arrLoadDocumentDatas["orderDocumentsOrderDate"], 'display'); } else { echo ""; } ?>" />
											-->
											<?php
												if($_POST["convertDocType"] == 'MA' || $_POST["convertDocType"] == 'M1' || $_POST["convertDocType"] == 'M2' || $_POST["convertDocType"] == 'M3' || $_POST["convertDocType"] == 'IK'){
											?>
											<input type="text" name="selectInvoiceDate" id="selectInvoiceDate" class="inputField_100" readonly="readonly" value="<?php if($selectInvoiceDate > 0){ echo formatDate($selectInvoiceDate, 'display'); } else { echo formatDate($arrLoadDocumentDatas["orderDocumentsProcessingDate"], 'display'); } ?>" />
											<?php
												}
												if($_POST["editDocType"] == 'MA' || $_POST["editDocType"] == 'M1' || $_POST["editDocType"] == 'M2' || $_POST["editDocType"] == 'M3' || $_POST["editDocType"] == 'IK'){
											?>
											<input type="text" name="selectInvoiceDate" id="selectInvoiceDate" class="inputField_100" readonly="readonly" value="<?php if($selectInvoiceDate > 0){ echo formatDate($selectInvoiceDate, 'display'); } else { echo formatDate($arrLoadDocumentDatas["orderDocumentsInvoiceDate"], 'display'); } ?>" />
											<?php
												}
											?>
										</td>
									</tr>
									<?php
										}
									?>
								</table>
							</fieldset>

							<fieldset>
								<?php
								#dd('customersData');
									if($_REQUEST["documentType"] != 'LS'){
										$thisLegendTitle = 'Rechnungs-';
									}
									else if($_REQUEST["documentType"] == 'LS'){
										$thisLegendTitle = 'Lieferungs-';
									}
								?>
								<legend><?php echo $thisLegendTitle; ?>Empf&auml;ngerdaten</legend>
								<?php
									if($selectCustomersRecipientNotice != '') {
										echo '<p class="infoArea">' .  $selectCustomersRecipientNotice . '</p>';
									}
								?>
								<table border="0" cellpadding="0" cellspacing="0" width="100%">
									<tr>
										<td style="width:200px;"><b>Mailadresse:</b></td>
										<td>
											<input type="text" name="selectCustomersRecipientMail" id="selectCustomersRecipientMail" class="inputField_510" value="<?php echo $selectCustomersRecipientMail; ?>" />
											<?php
												if($selectCustomersRecipientMail != '' && !checkEmailStructure($selectCustomersRecipientMail)) {
													echo '<br /><span class="infotext" style="color:#FF0000">Die in den Kundendaten eingetragene Mail-Adresse ist fehlerhaft!!!!!</span>';
												}
											?>
											<?php
												// BOF SEND DELIVERY-NOTICE PER MAIL
												if($_REQUEST["documentType"] == 'RE'){
													#$checked = '';
													$checked = ' checked="checked" ';
													// BOF DON'T SEND DELIVERY-NOTICE TO US (MANDATOR) IF SALESMAN IS MANDATOR
													if(
														in_array($_REQUEST["selectSalesman"], array("5684", "13301"))
														 ||
														in_array($arrLoadDocumentDatas["orderDocumentsSalesman"], array("5684", "13301"))
														 ||
														in_array($customersData["VertreterID"], array("5684", "13301"))
														 ||
														in_array($customersData["customersID"], array("5684", "13301"))
													) {
														$checked = '';
													}
													// EOF DON'T SEND DELIVERY-NOTICE TO US (MANDATOR) IF SALESMAN IS MANDATOR

													// BOF DON'T SEND DELIVERY-NOTICE IF RE IS EDITED
													if($_REQUEST["editDocType"] == "RE" && $_REQUEST["originDocumentType"] == "RE" && $_REQUEST["documentType"] == "RE"){
														$checked = '';
													}
													// BOF DON'T SEND DELIVERY-NOTICE IF RE IS EDITED

													// BOF DON'T SEND DELIVERY NOTICE IF DOCUMENT IS COLLECTIVE INVOICE
														if($_REQUEST["isCollectiveInvoice"] == '1'){
															$checked = '';
														}
													// EOF DON'T SEND DELIVERY NOTICE IF DOCUMENT IS COLLECTIVE INVOICE

													echo '<br />
															<p class="infoArea" style="width:504px;">
																<input type="checkbox" name="sendDeliverNoticeMail" value="1" ' . $checked . '/> <b>Versand-Nachricht an diese Mail-Adresse senden?</b>
															</p>';
												}
												// EOF SEND DELIVERY-NOTICE PER MAIL
											?>
										</td>
									</tr>
									<tr>
										<td style="width:200px;"><b style="color: #FF0000">Mail-Kopie an ?:</b></td>
										<td>
											<input type="text" name="selectCustomersRecipientMailCopy" id="selectCustomersRecipientMailCopy" class="inputField_510" value="<?php if($selectCustomersRecipientMail != $selectCustomersRecipientMailCopy) { echo $selectCustomersRecipientMailCopy; } ?>" />
											<?php
												if($selectCustomersRecipientMailCopy != '' && !checkEmailStructure($selectCustomersRecipientMailCopy)) {
													echo '<br /><span class="infotext" style="color:#FF0000">Die in den Kundendaten eingetragene Mail-Adresse ist fehlerhaft!!!!!</span>';
												}
											?>
										</td>
									</tr>

									<?php if(1){ ?>
									<tr>
										<td style="width:200px;"><b>Firmen-KNR:</b></td>
										<td>
											<input type="text" name="selectCustomersRecipientCustomerNumber" id="selectCustomersRecipientCustomerNumber" class="inputField_510" value="<?php echo $selectCustomersRecipientCustomerNumber; ?>" />
										</td>
									</tr>
									<?php } ?>

									<?php
										// BOF CREATE DELIVERY-FAX
										if($_REQUEST["documentType"] == 'RE'){
									?>
									<tr>
										<td style="width:200px;"><b>Faxnummer:</b></td>
										<td>
											<input type="text" name="selectCustomersRecipientFax" id="selectCustomersRecipientFax" class="inputField_510" value="<?php echo $selectCustomersRecipientFax; ?>" />
										</td>
									</tr>
									<?php
										}
										// EOF CREATE DELIVERY-FAX
									?>
									<tr>
										<td><b>Firmenname:</b></td>
										<td>
											<input type="text" name="selectCustomersRecipientName" id="selectCustomersRecipientName" class="inputField_510" value="<?php echo $selectCustomersRecipientName; ?>" />
										</td>
									</tr>
									<tr>
										<td><b>Firmenname-Zusatz:</b></td>
										<td>
											<input type="text" name="selectCustomersRecipientNameAdd" id="selectCustomersRecipientNameAdd" class="inputField_510" value="<?php echo $selectCustomersRecipientNameAdd; ?>" />
										</td>
									</tr>
									<tr>
										<td><b>Inhaber:</b></td>
										<td>
											<input type="text" name="selectCustomersRecipientManager" id="selectCustomersRecipientManager" class="inputField_510" value="<?php echo $selectCustomersRecipientManager; ?>" />
										</td>
									</tr>
									<tr>
										<td><b>Kontaktperson:</b></td>
										<td>
											<input type="text" name="selectCustomersRecipientContact" id="selectCustomersRecipientContact" class="inputField_510" value="<?php echo $selectCustomersRecipientContact; ?>" />
										</td>
									</tr>
									<tr>
										<td><b>Stra&szlig;e / -Hausnummer:</b></td>
										<td>
											<input type="text" name="selectCustomersRecipientStreet" id="selectCustomersRecipientStreet" class="inputField_427" value="<?php echo $selectCustomersRecipientStreet; ?>" />
											/ <input type="text" name="selectCustomersRecipientStreetNumber" id="selectCustomersRecipientStreetNumber" class="inputField_70" value="<?php echo $selectCustomersRecipientStreetNumber; ?>" />
										</td>
									</tr>
									<tr>
										<td><b>PLZ / -Ort:</b></td>
										<td>
											<input type="text" name="selectCustomersRecipientZipcode" id="selectCustomersRecipientZipcode" class="inputField_70" value="<?php echo $selectCustomersRecipientZipcode; ?>" />
											/ <input type="text" name="selectCustomersRecipientCity" id="selectCustomersRecipientCity" class="inputField_427" value="<?php echo $selectCustomersRecipientCity; ?>" />
										</td>
									</tr>
									<tr>
										<td><b>Land:</b></td>
										<td>
											<select name="selectCustomersRecipientCountry" id="selectCustomersRecipientCountry" class="inputSelect_510">
												<option value=""> - </option>
												<?php
													if(!empty($arrCountryTypeDatas)) {
														foreach($arrCountryTypeDatas as $thisKey => $thisValue) {
															$selected = '';
															if($thisKey == $selectCustomersRecipientCountry) {
																$selected = ' selected="selected" ';
															}
															echo '
																<option value="' . $thisKey . '" '.$selected.' >' . ($arrCountryTypeDatas[$thisKey]["countries_name"]). '</option>
															';
														}
													}
												?>
											</select>
										</td>
									</tr>
								</table>
							</fieldset>

							<?php
								if(1){
							?>

							<fieldset>
								<?php
									if($_REQUEST["documentType"] == 'LS'){
										$thisLegendTitle = 'Rechnungs-';
									}
									else if($_REQUEST["documentType"] != 'LS'){
										$thisLegendTitle = 'Lieferungs-';
									}
								?>
								<legend style="color:#FF0000;"><?php echo $thisLegendTitle; ?>Empf&auml;ngerdaten</legend>
								<?php
									if($selectCustomersSecondaryRecipientNotice != '') {
										echo '<p class="infoArea">' .  $selectCustomersSecondaryRecipientNotice . '</p>';
									}
								?>
								<table border="0" cellpadding="0" cellspacing="0" width="100%">

									<?php if(1){ ?>
									<tr>
										<td style="width:200px;"><b>Firmen-KNR:</b></td>
										<td>
											<input type="text" name="selectCustomersSecondaryRecipientCustomerNumber" id="selectCustomersSecondaryRecipientCustomerNumber" class="inputField_510" value="<?php echo $selectCustomersSecondaryRecipientCustomerNumber; ?>" />
										</td>
									</tr>
									<?php } ?>

									<tr style="border-color:#FF0000 !important;">
										<td style="width:200px;"><b>Firmenname:</b></td>
										<td>
											<input type="text" name="selectCustomersSecondaryRecipientName" id="selectCustomersSecondaryRecipientName" class="inputField_510" value="<?php echo $selectCustomersSecondaryRecipientName; ?>" style="border-color:#FF0000 !important;" />
										</td>
									</tr>
									<tr style="border-color:#FF0000 !important;">
										<td><b>Firmenname-Zusatz:</b></td>
										<td>
											<input type="text" name="selectCustomersSecondaryRecipientNameAdd" id="selectCustomersSecondaryRecipientNameAdd" class="inputField_510" value="<?php echo $selectCustomersSecondaryRecipientNameAdd; ?>" style="border-color:#FF0000 !important;" />
										</td>
									</tr>
									<tr style="border-color:#FF0000 !important;">
										<td><b>Inhaber:</b></td>
										<td>
											<input type="text" name="selectCustomersSecondaryRecipientManager" id="selectCustomersSecondaryRecipientManager" class="inputField_510" value="<?php echo $selectCustomersSecondaryRecipientManager; ?>" style="border-color:#FF0000 !important;" />
										</td>
									</tr>
									<tr style="border-color:#FF0000 !important;">
										<td><b>Kontaktperson:</b></td>
										<td>
											<input type="text" name="selectCustomersSecondaryRecipientContact" id="selectCustomersSecondaryRecipientContact" class="inputField_510" value="<?php echo $selectCustomersSecondaryRecipientContact; ?>" style="border-color:#FF0000 !important;" />
										</td>
									</tr>
									<tr style="border-color:#FF0000 !important;">
										<td><b>Stra&szlig;e / -Hausnummer:</b></td>
										<td>
											<input type="text" name="selectCustomersSecondaryRecipientStreet" id="selectCustomersSecondaryRecipientStreet" class="inputField_427" value="<?php echo $selectCustomersSecondaryRecipientStreet; ?>" style="border-color:#FF0000 !important;" />
											/ <input type="text" name="selectCustomersSecondaryRecipientStreetNumber" id="selectCustomersSecondaryRecipientStreetNumber" class="inputField_70" value="<?php echo $selectCustomersSecondaryRecipientStreetNumber; ?>" style="border-color:#FF0000 !important;" />
										</td>
									</tr>
									<tr style="border-color:#FF0000 !important;">
										<td><b>PLZ / -Ort:</b></td>
										<td>
											<input type="text" name="selectCustomersSecondaryRecipientZipcode" id="selectCustomersSecondaryRecipientZipcode" class="inputField_70" value="<?php echo $selectCustomersSecondaryRecipientZipcode; ?>" style="border-color:#FF0000 !important;" />
											/ <input type="text" name="selectCustomersSecondaryRecipientCity" id="selectCustomersSecondaryRecipientCity" class="inputField_427" value="<?php echo $selectCustomersSecondaryRecipientCity; ?>" style="border-color:#FF0000 !important;" />
										</td>
									</tr>
									<tr style="border-color:#FF0000 !important;">
										<td><b>Land:</b></td>
										<td>
											<select name="selectCustomersSecondaryRecipientCountry" id="selectCustomersSecondaryRecipientCountry" class="inputSelect_510" style="border-color:#FF0000 !important;" >
												<option value=""> - </option>
												<?php
													if(!empty($arrCountryTypeDatas)) {
														foreach($arrCountryTypeDatas as $thisKey => $thisValue) {
															$selected = '';
															if($thisKey == $selectCustomersSecondaryRecipientCountry) {
																$selected = ' selected="selected" ';
															}
															echo '
																<option value="' . $thisKey . '" '.$selected.' >' . ($arrCountryTypeDatas[$thisKey]["countries_name"]). '</option>
															';
														}
													}
												?>
											</select>
										</td>
									</tr>
								</table>
							</fieldset>
							<?php
								}
							?>

							<?php
								if(($_POST["editDocType"] == 'MA' || $_POST["editDocType"] == 'M1' || $_POST["editDocType"] == 'M2' || $_POST["editDocType"] == 'M3' || $_POST["editDocType"] == 'IK') || ($_POST["convertDocType"] == 'MA' || $_POST["convertDocType"] == 'M1' || $_POST["convertDocType"] == 'M2' || $_POST["convertDocType"] == 'M3' || $_POST["convertDocType"] == 'IK')){
							?>
							<?php
								if($userDatas["usersLogin"] != "xxxthorsten"){
									if($arrLoadDocumentDatas["orderDocumentsStatus"] == '4'){

							?>
							<?php

								$sql = "
									SELECT
												`orderPaymentID`,
												`orderPaymentOrderID`,
												`orderPaymentOrderNumber`,
												`orderPaymentDate`,
												`orderPaymentBankAccountID`,
												`orderPaymentValue`,
												`orderPaymentNotiz`

											FROM `" . TABLE_ORDER_INVOICE_PAYMENTS . "`

											WHERE 1
												AND `orderPaymentOrderNumber` = '" . $arrRelatedDocuments["RE"]. "'
									";
								$rs = $dbConnection->db_query($sql);
								$arrPaymentPartDatas = array();
								$paymentPaidPartValue = 0;
								while($ds = mysqli_fetch_assoc($rs)){
									foreach(array_keys($ds) as $field){
										$arrPaymentPartDatas[$ds["orderPaymentID"]][$field] = $ds[$field];
									}
									$paymentPaidPartValue += $ds["orderPaymentValue"];
								}
								$paymentRestPartValue = $arrLoadDocumentDatas["orderDocumentsTotalPrice"] - $paymentPaidPartValue;
							?>
							<fieldset>
								<legend>Teilzahlungs-Betr&auml;ge</legend>
								<p class="warningArea">
									Die Rechnung ist teilbezahlt!
									<br />
									<b>Rechnungsbetrag:</b> <?php echo number_format($arrLoadDocumentDatas["orderDocumentsTotalPrice"], 2, ",", "."); ?> &euro;
									<input type="hidden" name="selectPaymentTotalPrice" value="<?php echo $arrLoadDocumentDatas["orderDocumentsTotalPrice"]; ?>" />
									<span> &bull; </span>
									<b>Bezahlter Betrag:</b> <?php echo number_format($paymentPaidPartValue, 2, ",", "."); ?> &euro;
									<input type="hidden" name="selectPaymentPartValue" value="<?php echo $paymentPaidPartValue; ?>" />
									<span> &bull; </span>
									<b>Fehlbetrag:</b> <?php echo number_format($paymentRestPartValue, 2, ",", "."); ?> &euro;
									<input type="hidden" name="selectPaymentRestValue" value="<?php echo $paymentRestPartValue; ?>" />
								</p>
							</fieldset>
							<?php
									}
								}
							?>
							<fieldset>
								<legend>Mahn-Betr&auml;ge</legend>
								<table border="0" cellpadding="0" cellspacing="0" width="100%">
									<tr>
										<td style="width:200px;"><b>Mahnzins:</b></td>
										<?php
											$thisReminderInterestPercent = constant('REMINDER_INTEREST_' . $_POST["convertDocType"]);
											if($selectReminderInterestPercent > 0) { $thisReminderInterestPercent = $selectReminderInterestPercent; }
										?>
										<td><input type="text" name="selectReminderInterestPercent" class="inputField_40" value="<?php echo number_format($thisReminderInterestPercent, 2, ',', ''); ?>" /> %</td>
									</tr>
									<tr>
										<td style="width:200px;"><b>Mahngeb&uuml;hr:</b></td>
										<?php
											$thisReminderChargesPrice = constant('REMINDER_CHARGES_' . $_POST["convertDocType"]);
											if($selectReminderChargesPrice > 0) { $thisReminderChargesPrice = $selectReminderChargesPrice; }
										?>
										<td><input type="text" name="selectReminderChargesPrice" class="inputField_40" value="<?php echo number_format($thisReminderChargesPrice, 2, ',', ''); ?>" /> &euro;</td>
									</tr>
								</table>
							</fieldset>
							<?php
								}
							?>

							<fieldset>
								<legend>Versand-Daten</legend>
								<table border="0" cellpadding="0" cellspacing="0" width="100%">
									<?php
										if($_REQUEST["documentType"] == 'AN') {
										$thisBindingDate = date("Y-m-d", mktime(0, 0, 0, (date('m') + OFFERS_BINDING_PERIOD), date('d'), date('Y')));
									?>
									<tr>
										<td style="width:200px;"><b>Bindefrist:</b></td>
										<td><input type="text" name="selectBindingDate" id="selectBindingDate" class="inputField_100" readonly="readonly" value="<?php echo formatDate($thisBindingDate, 'display'); ?>" /></td>
									</tr>
									<?php
										}
									?>
									<?php
										//if($_REQUEST["documentType"] == 'LS') {
										if(1){
									?>

									<tr>
										<td style="width:200px;"><b>Lieferdatum:</b></td>
										<td>
											<?php
												if($selectDeliveryDate == ""){ $selectDeliveryDate = date('d.m.Y'); }
												if($_REQUEST["editDocType"] == '' && ($_REQUEST["documentType"] == 'LS' || $_REQUEST["documentType"] == 'RE')) {
											?>
											<input type="text" name="selectDeliveryDate" id="selectDeliveryDate" class="inputField_100" readonly="readonly" value="<?php echo $selectDeliveryDate; ?>" />
											<?php
												}
												else if($_REQUEST["editDocType"] != ''){
											?>
											<input type="text" name="selectDeliveryDate" id="selectDeliveryDate" class="inputField_100" readonly="readonly" value="<?php echo formatDate($arrLoadDocumentDatas["orderDocumentsDeliveryDate"], 'display'); ?>" />
											<?php
												}
												else {
											?>
											<input type="text" name="selectDeliveryDate" id="selectDeliveryDate" class="inputField_100" readonly="readonly" value="" />
											<?php
												}
											?>
										</td>
									</tr>
									<?php
										}
									?>
									<tr>
										<td style="width:200px;"><b>Versandart:</b></td>
										<td><input type="text" name="selectDeliveryType" class="inputField_510" readonly="readonly" value="<?php if($selectDeliveryType !="" && $selectDeliveryType > 0 ){ echo $selectDeliveryType; } else { echo SHIPPING_TYPE; }  ?>" /></td>
									</tr>

									<?php
										if($_REQUEST["documentType"] == 'LS' || $_REQUEST["documentType"] == 'RE') {
									?>
									<tr>
										<td style="width:200px;"><b>Versandnummer:</b></td>
										<td>
											<input type="text" name="selectDeliveryTypeNumber" class="inputField_510" value="<?php if($selectDeliveryTypeNumber !=""){ echo $selectDeliveryTypeNumber; } else { echo ""; } ?>" />
											<br />
											<p class="infoArea" style="width:504px">Wenn mehrere Nummern, bitte Nummern mit Semikolon (;) trennen!!!</p>
										</td>
									</tr>
									<?php
										}
									?>
									<tr>
										<td style="width:200px;"><b>Versandkosten:</b></td>
										<?php
											$thisShippingCosts = 0 ;
											#$thisShippingCosts = SHIPPING_COSTS;
											if($selectShippingCosts > 0) { $thisShippingCosts = $selectShippingCosts; }
											if($selectShippingCostsPackages > 0 && $selectShippingCostsPerPackage > 0) { $thisShippingCosts = $selectShippingCostsPerPackage; }
										?>
										<td>
											<select name="selectPackagingCostsMultiplier" class="inputSelect_40">
												<?php
													for($i = 1 ; $i < 500 ; $i++) {
														$selected = '';
														if($selectShippingCostsPackages > 0 && $selectShippingCostsPackages == $i){
															$selected = ' selected="selected" ';
														}
														echo '<option value="' . $i . '" ' . $selected . '>' . $i . '</option>';
													}
												?>
											</select>

											 <b style="padding:0 10px 0 10px;">mal</b>
											<input type="text" name="selectShippingCosts" class="inputField_40" value="<?php echo number_format($thisShippingCosts, 2, ',', ''); ?>" /> &euro;
										</td>
									</tr>
									<tr>
										<td style="width:200px;"><b>Verpackungskosten:</b></td>
										<?php
											$thisPackagingCosts = 0 ;
											// $thisShippingCosts = SHIPPING_PACKAGING_COSTS;
											if($selectPackagingCosts > 0) { $thisPackagingCosts = $selectPackagingCosts; }
										?>
										<td><input type="text" name="selectPackagingCosts" class="inputField_40" value="<?php echo number_format($thisPackagingCosts, 2, ',', ''); ?>" /> &euro;</td>
									</tr>
								</table>
							</fieldset>

							<fieldset>
								<legend>Zahlungs-Daten</legend>
								<table border="0" cellpadding="0" cellspacing="0" width="100%">
									<?php
										if($_REQUEST["documentType"] != '') {
									?>
									<tr>
										<td style="width:200px;"><b>Rabatt:</b></td>
										<td>
											<input type="text" name="selectDiscount" class="inputField_40" value="<?php echo number_format($selectDiscount, 2, ',', ''); ?>" />
											<select name="selectDiscountType" class="inputSelect_40">
												<option value="percent" <?php if($selectDiscountType == "percent") { echo ' selected="selected" '; } ?> >%</option>
												<option value="fixed" <?php if($selectDiscountType == "fixed") { echo ' selected="selected" '; } ?> >&euro;</option>
											</select>
										</td>
									</tr>
									<tr>
										<td style="width:200px;"><b>MwSt.:</b></td>
										<?php
											$thisMWST = MWST;
											if($selectMwSt != "") { $thisMWST = $selectMwSt; }
										?>
										<td>
											<?php
												if(1){
													#dd('customersData');
													echo '<span>';
													echo '<select name="selectMwStType" class="inputSelect_205" style="border-color:#FF0000 !important;">';

													$selectMwstInclusive = '';
													$selectMwstExclusive = ' selected="selected" ';
													$thisMwStWarning = 'ACHTUNG: Bei Ebay- und Amazon-Kunden MwSt. immer <b>inklusiv</b>!';

													if($selectMwStType != ""){
														if($selectMwStType == "inclusive"){
															$selectMwstInclusive = ' selected="selected" ';
															$selectMwstExclusive = '';
															$thisMwStWarning = 'ACHTUNG: Bei Ebay- und Amazon-Kunden MwSt. immer <b>inklusiv</b>!';
														}
													}
													else {
														if(in_array($customersData["Typ"], array('7', '8'))){
															$selectMwstInclusive = ' selected="selected" ';
															$selectMwstExclusive = '';
															$thisMwStWarning = 'ACHTUNG: Bei Ebay- und Amazon-Kunden MwSt. immer <b>inklusiv</b>!';
														}
													}

													echo '<option value="inclusive" ' . $selectMwstInclusive . ' >Gesamtpreis inklusive MwSt.</option>';
													echo '<option value="exclusive" ' . $selectMwstExclusive . ' >Gesamtpreis zuz&uuml;glich MwSt.</option>';

													echo '</select>';
													echo '</span>';
												}
											?>
											<span>
												<input type="text" name="selectMwSt" class="inputField_40" value="<?php echo number_format($thisMWST, 2, ',', ''); ?>" /> %
											</span>
											<?php
												if($thisMwStWarning != ''){
													echo '<p class="infoArea" style="width:504px;">' . $thisMwStWarning . '</p>';
												}
											?>
										</td>
									</tr>
									<tr>
										<td style="width:200px;"><b>Zahlstatus:</b></td>
										<td>
											<select name="selectPaymentStatus" id="selectPaymentStatus" class="inputSelect_300">
												<?php
													if(!empty($arrPaymentStatusTypeDatas)) {
														foreach($arrPaymentStatusTypeDatas as $thisKey => $thisValue){
															$selected = '';
															if($selectPaymentStatus != '' || $arrLoadDocumentDatas["orderDocumentsStatus"]){
																if($thisKey == $selectPaymentStatus){
																	$selected = ' selected="selected" ';
																}
																else if($thisKey == $arrLoadDocumentDatas["orderDocumentsStatus"]){
																	$selected = ' selected="selected" ';
																}
															}
															else if($thisKey == '1'){
																$selected = ' selected="selected" ';
															}

															echo '<option value="' . $thisKey . '" ' . $selected . ' >' . $thisValue["paymentStatusTypesName"] . '</option>';
														}
													}
												?>
											</select>
										</td>
									</tr>
									<tr>
										<td style="width:200px;"><b>Zahlart:</b></td>
										<td>
											<select name="selectPaymentType" id="selectPaymentType" class="inputSelect_300">
												<?php
													if(!empty($arrPaymentTypeDatas)) {
														foreach($arrPaymentTypeDatas as $thisKey => $thisValue){
															$selected = '';
															if($selectPaymentType != '') {
																if($thisKey == $selectPaymentType){
																	$selected = ' selected="selected" ';
																}
															}
															else if($thisKey == '1'){
																$selected = ' selected="selected" ';
															}

															echo '<option value="' . $thisKey . '" ' . $selected . ' >' . $thisValue["paymentTypesName"] . '</option>';
														}
													}
												?>
											</select>
											<?php
												if(in_array($selectPaymentType, array("4", "6"))){
													$thisSepaInfo = '';
													$thisStyleBackground = '';
													if($selectCustomersRecipientSepaExists != "1" || $selectCustomersRecipientSepaValidUntilDate < date("Y-m-d")){
														$thisSepaInfo .= '<b>Kein SEPA-Formular vorhanden</b>';
														$thisStyleBackground = 'background-color:#FF7F84;';
													}
													else {
														$thisSepaInfo .=  '<b>SEPA-Formular vorhanden</b>';
														$thisStyleBackground = 'background-color:#00FF00;';
													}
													if($selectCustomersRecipientSepaValidUntilDate > 0){
														$thisSepaInfo .= ', g&uuml;ltig bis: ' . formatDate($selectCustomersRecipientSepaValidUntilDate, "display") . '';
													}
													if($selectCustomersRecipientSepaRequestedDate > 0){
														$thisSepaInfo .= ', neues angefordert am: ' . formatDate($selectCustomersRecipientSepaRequestedDate, "display") . '';
													}
													echo '<p class="infoArea" style="width:504px;' . $thisStyleBackground . '">' . $thisSepaInfo . '</p>';
												}
											?>

											<?php
												$thisCashOnDelivery = SHIPPING_CASH_ON_DELIVERY;
												if($selectCashOnDelivery != '') { $thisCashOnDelivery = $selectCashOnDelivery; }
											?>
											<span id="shippingCashOnDeliveryArea" style="display:none;"> - <b>Nachname-Geb&uuml;hr:</b> <input type="text" name="selectCashOnDelivery" class="inputField_40" value="<?php echo number_format($thisCashOnDelivery, 2, ",", ""); ?>" /> &euro;</span>
											<?php
												if(USE_DEBITING_DATE){
											?>
											<span id="debitingDateArea" style="display:none;"> - <b>Abbuchung am:</b> <input type="text" name="selectDebitingDate" id="selectDebitingDate" class="inputField_100" readonly="readonly" value="<?php if($selectDebitingDate > 0){ echo formatDate($selectDebitingDate, 'display'); } else { echo date('d.m.Y', mktime(0, 0, 0, date("m"), (date("d") + 14), date("Y"))); } ?>" /></span>
											<?php
												}
											?>
										</td>
									</tr>
									<tr>
										<td style="width:200px;"><b>Konditionen:</b></td>
										<td>
											<select name="selectPaymentCondition" class="inputSelect_510">
												<?php
													if(!empty($arrPaymentConditionDatas)) {
														foreach($arrPaymentConditionDatas as $thisKey => $thisValue){
															$selected = '';
															if($selectPaymentCondition != '' || $arrLoadDocumentDatas["orderDocumentsPaymentCondition"]) {
																if($thisKey == $selectPaymentCondition){
																	$selected = ' selected="selected" ';
																}
																else if($thisKey == $arrLoadDocumentDatas["orderDocumentsPaymentCondition"]){
																	$selected = ' selected="selected" ';
																}
															}
															else if($thisKey == '5'){
																$selected = ' selected="selected" ';
															}

															echo '<option value="' . $thisKey . '" ' . $selected . ' >' . $thisValue["paymentConditionsName"] . '</option>';
														}
													}
												?>
											</select>
										</td>
									</tr>
									<!--
									<tr>
										<td>Lieferdatum:</td>
										<td><input type="text" name="selectDateDelivery" class="inputField_510" value="" /></td>
									</tr>
									-->
									<?php
										}
										#dd('arrLoadDocumentDatas');
									?>
									<tr>
										<td style="width:200px;"><b>Bankverbindung:</td>
										<td>
											<select name="selectBankAccount" class="inputSelect_510">
												<?php
													if(!empty($arrBankAccountTypeDatas)) {
														foreach($arrBankAccountTypeDatas as $thisKey => $thisValue){
															$selected = '';
															if($selectBankAccount != '' || $arrLoadDocumentDatas["orderDocumentsBankAccount"] != '') {
																if($thisValue["bankAccountTypesShortName"] == $selectBankAccount){
																	$selected = ' selected="selected" ';
																}
																else if($thisValue["bankAccountTypesShortName"] == $arrLoadDocumentDatas["orderDocumentsBankAccount"]){
																	$selected = ' selected="selected" ';
																}
															}
															else if($thisValue["bankAccountTypesDefault"] == '1'){
																$selected = ' selected="selected" ';
															}
															echo '<option value="' . $thisValue["bankAccountTypesShortName"]. '" ' . $selected . ' >' . $thisValue["bankAccountTypesName"] . ' (Konto-Nr: ' . $thisValue["bankAccountTypesAccountNumber"] . ')</option>';
														}
													}
												?>
											</select>
										</td>
									</tr>

									<!--
									<tr>
										<td>Gutschrift:</td>
										<td>xxxxxx</td>
									</tr>
								-->
								</table>
							</fieldset>
							<?php
								// if($userDatas["usersLogin"] == 'xxxxthorsten'){
								if(1){
							?>
							<fieldset>
								<legend>Mailtext</legend>
								<!--
								<textarea name="selectAdditionalMailtext" class="inputTextarea_800x140" readonly="readonly">
								<?php
									# echo $arrMailContentDatas['AN']['MAIL_CONTENT'];
								?>
								</textarea>
								-->
								<?php
									$CKEditor = new CKEditor();
									$CKEditor->returnOutput = true;
									$CKEditor->basePath = DIRECTORY_CKEDITOR;
									$CKEditor->config['height'] = 220;
									$CKEditor->config['width'] = 800;
									$CKEditor->config['resize_minWidth'] = 800;
									$CKEditor->config['resize_maxWidth'] = 800;
									// $CKEditor->config['resize_enabled'] = false;
									// $CKEditor->config['entities'] = false;
									$CKEditor->config['entities_latin'] = false;
									$CKEditor->textareaAttributes = array("cols" => 80, "rows" => 10);

									$config['toolbar'] = array(
										array('Bold', 'Italic', 'Underline', 'Superscript', 'Subscript', 'HorizontalRule', 'SpecialChar' ),
										array('NumberedList','BulletedList'),
										// array( 'Cut','Copy','Paste','PasteText','PasteFromWord','-','Undo','Redo' )
									);

									$config['skin'] = 'office2003'; // 'v2', 'office2003', 'kama'
									// $CKEditor->editor("additionalText", $initialValue);

									$thisDocumentContent = '';
									if($_POST["selectAdditionalMailtext"] != '') {
										$thisDocumentContent .= ($_POST["selectAdditionalMailtext"]);
										#$testVar = mb_detect_encoding($_POST["selectAdditionalMailtext"]);
										#dd('testVar');
									}
									else {
										$thisDocumentContent .= $arrMailContentDatas[$_REQUEST["documentType"]]['MAIL_SALUTATION'];
										$thisDocumentContent .= $arrMailContentDatas[$_REQUEST["documentType"]]['MAIL_CONTENT'];
										$thisDocumentContent .= $arrMailContentDatas[$_REQUEST["documentType"]]['MAIL_REGARDS'];
										if($_POST["editDocType"] != "") {
											$thisDocumentContent = preg_replace("/{###DOCUMENT_NUMBER###}/", $_POST["originDocumentNumber"], $thisDocumentContent);
										}
										else {
											$thisDocumentContent = preg_replace("/{###DOCUMENT_NUMBER###}/", constant("BASIC_NUMBER_" . $_REQUEST["documentType"]), $thisDocumentContent);
										}
										#$thisDocumentContent = preg_replace("/{###DOCUMENT_NUMBER###}/", constant("BASIC_NUMBER_" . $_REQUEST["documentType"]), $thisDocumentContent);
									}

									$loadTextEditArea = $CKEditor->editor("selectAdditionalMailtext", $thisDocumentContent, $config);
									echo $loadTextEditArea;
								?>
							</fieldset>
							<?php
								}
							?>
						</div>
						<?php

				?>
				<?php
					if($_REQUEST["isCollectiveInvoice"] == '1'){
						echo 'Sammelrechnung: ';
						echo '<input type="text" name="isCollectiveInvoice" value="1" />';
					}
				?>
				<div class="actionButtonsArea">
					<input type="hidden" name="documentType" value="<?php echo $_REQUEST["documentType"]; ?>" />
					<input type="hidden" name="editID" id="editID" value="<?php echo $_REQUEST["editID"]; ?>" />

					<input type="submit" class="inputButton1 inputButtonOrange" name="cancelDatas" value="Abbrechen" onclick="return showWarning(' Wollen Sie diesen Vorgang wirklich abbrechen??? '); "/>
					<?php if(!empty($arrOrderDetailIDs) || true) { ?>
					&nbsp;
					<?php
						if($_REQUEST["editDocType"] != ''){
							$createMode = '&auml;ndern';
						}
						else {
							$createMode = 'erstellen';
						}
					?>
					<script language="javascript" type="text/javascript">
						function checkIfOrderProductIsSelected(formName) {
							var form = window.document.forms[formName];
							var elementSelected = true;
							alert($('.selectOrderIDs').length);
							if($('.selectOrderID').length > 0 && $('.addProductsCountUse').length == 0){
								elementSelected = false;
								$('.selectOrderID').each(function(index, value) {
									if(elementSelected == false && $(value).attr("checked") == 'checked') { elementSelected = true; }
								});
							}
							else if($('.selectOrderID').length == 0 && $('.addProductsCountUse').length > 0){
								elementSelected = false;
								$('.addProductsCountUse').each(function(index, value) {
									if(elementSelected == false && $(value).attr("checked") == 'checked') { elementSelected = true; }
								});
							}
							if (!elementSelected) {
								thisReturn = false;
								alert(' Sie haben keines der Produkte (' + $('.selectOrderID').length + ') für das Dokument gewählt.' );
							}
							else {
								thisReturn = window.confirm(' Sind die eingegebenen Daten alle korrekt? ');
							}
							alert('selectOrderID: ' + $('.selectOrderID').length + ' | ' + 'addProductsCountUse: ' + $('.addProductsCountUse').length);
							return thisReturn;
						}
					</script>

					<input type="submit" title="<?php echo $_REQUEST["documentType"]; ?> mit den eingegebenen Daten <?php echo $createMode; ?>" value=" Weiter zur <?php echo $arrDocumentTypeDatas[$_REQUEST["documentType"]]["createdDocumentsTypesName"] ?>-Vorschau" class="inputButton3 inputButtonGreen" name="createDocumentNow" onclick="return showWarning(' Sind die eingegebenen Daten alle korrekt? ');"" />
					<?php
					if($userDatas["usersLogin"] == 'thorsten'){
					?>
					<input type="submit" title="<?php echo $_REQUEST["documentType"]; ?> mit den eingegebenen Daten <?php echo $createMode; ?>" value=" TEST Weiter zur <?php echo $arrDocumentTypeDatas[$_REQUEST["documentType"]]["createdDocumentsTypesName"] ?>-Vorschau" class="inputButton3 inputButtonGreen" name="createDocumentNow" onclick="return checkIfOrderProductIsSelected('formGetOrdersForDocument');" />
					<?php
					}
					?>
					<?php } ?>
				</div>
			</form>
		</div>
		</div>
		<?php
			}
			//if($_REQUEST["editID"] != "" && $_POST["selectBankAccount"] == "") {
				//echo '<p class="warningArea">Daten sind nicht komplett.</p>';
			//}
		?>


	<div class="xxadminEditArea">
	<div class="xxadminEditArea">
	<?php
		// BOF DOCUMENT PREVIEW
		if($_POST["createDocumentNow"] != "" && $_REQUEST["editID"] != "") {
			if($_POST["selectAdditionalMailtext"] != ''){$_POST["selectAdditionalMailtext"] = htmlentities(utf8_decode($_POST["selectAdditionalMailtext"]));}

	?>
	<form name="formCreateDocument" method="post" action="<?php echo $_SERVER["REQUEST_URI"]; ?>" enctype="multipart/form-data">
	<h2>Dokument-Vorschau</h2>
	<?php
		// BOF CREATE PDF CONTENT AND PREVIEW DATAS
		$loadPdfContent = "";
		$loadContentTemplate = implode('', file(DIRECTORY_PDF_TEMPLATES . '/template_BASIC.html'));

		$loadCardTemplate = implode('', file(DIRECTORY_PDF_TEMPLATES . '/template_CARD_DATAS.html'));
		if($_REQUEST["documentType"] == 'LS') {
			$loadCardTemplate = preg_replace("/<!-- BOF PRICE_DATAS -->(.*)<!-- EOF PRICE_DATAS -->/ismU", "", $loadCardTemplate);
		}
		$loadContentTemplate = preg_replace("/{###DOCUMENT_DATAS###}/", $arrDocumentDatas[$_REQUEST["documentType"]], $loadContentTemplate);

		if($_REQUEST["isCollectiveInvoice"] == '1'){
			if(!empty($_POST["selectConfirmationRow"])){
				$arrUseDatas = array();
				$countItem = 0;
				foreach($_POST["selectConfirmationRow"] as $thisSelectConfirmationRowKey => $thisSelectConfirmationRowValue){
					if($thisSelectConfirmationRowValue == '1'){
						#dd('thisSelectConfirmationRowKey');

						$arrUseDatas[$countItem]["ordersArtikelNummer"] = $_POST["selectConfirmationDocumentNumber"][$thisSelectConfirmationRowKey];
						$arrUseDatas[$countItem]["ordersArtikelMenge"] = $_POST["selectConfirmationQuantity"][$thisSelectConfirmationRowKey];
						$arrUseDatas[$countItem]["ordersSinglePreis"] = $_POST["selectConfirmationTotalSumNetto"][$thisSelectConfirmationRowKey];
						$arrUseDatas[$countItem]["ordersSinglePreis2"] = $_POST["selectConfirmationTotalSumBrutto"][$thisSelectConfirmationRowKey];
						$arrUseDatas[$countItem]["ordersArtikelBezeichnung"] = $_POST["selectConfirmationCustomer"][$thisSelectConfirmationRowKey];
						$countItem++;
					}
				}
				$loadCollectiveInvoiceDatas = loadCollectiveInvoiceDatas($loadCardTemplate, array_keys($arrUseDatas), $arrUseDatas);

				$loadPdfContent = $loadContentTemplate;

				// echo $loadPdfContent;
				$loadPdfContent = preg_replace("/{###CARD_DATAS###}/", $loadCollectiveInvoiceDatas, $loadPdfContent);

				// STYLES
				$loadTemplateStyles = implode('', file(DIRECTORY_PDF_TEMPLATES . 'template_STYLES.html'));

				$loadPdfContent = preg_replace("/{###STYLES###}/", $loadTemplateStyles, $loadPdfContent);

				if(!empty($loadedAdressDatas)) {
					foreach($loadedAdressDatas as $thisKey => $thisValue) {
						if($thisValue != "") {
							$loadPdfContent = preg_replace("/{###".$thisKey."###}/", $thisValue . "<br />", $loadPdfContent);
						}
						else {
							$loadPdfContent = preg_replace("/{###".$thisKey."###}/", "", $loadPdfContent);
						}
					}
				}

				if(!empty($loadedDocumentDatas)) {
					// if(constant('BANK_ACCOUNT_' . $_POST["selectBankAccount"]. '_SHOW_WARNING')){
					if($arrBankAccountTypeDatas[$arrOrderDetailDatagetRelatedDocumentss[$thisTakeOrderKey]["ordersBankAccountType"]]["bankAccountTypesShowWarning"] == '1') {
						$loadPdfContent = preg_replace("/{###BANK_ACCOUNT_WARNING###}/", $arrContentDatas[$_REQUEST["documentType"]]['BANK_ACCOUNT_WARNING'], $loadPdfContent);
					}
					else {
						$loadPdfContent = preg_replace("/{###BANK_ACCOUNT_WARNING###}/", "", $loadPdfContent);
					}
					foreach($loadedDocumentDatas as $thisKey => $thisValue) {
						$loadPdfContent = preg_replace("/{###".$thisKey."###}/", $thisValue, $loadPdfContent);
						if(trim($thisValue) == '') {
							$loadPdfContent = preg_replace("/<!-- BOF_".$thisKey." -->(.*)<!-- EOF_".$thisKey." -->/", '', $loadPdfContent);
						}
					}
				}

				$loadSubjectText = $thisSubject . ' Nr.: ' . '{###DOCUMENT_NUMBER###}';

				$loadSubject = ''.$loadSubjectText. '<br><br>';

				$loadPdfContent = preg_replace("/{###SUBJECT###}/", $loadSubject, $loadPdfContent);
				$loadPdfContent = removeUnnecessaryChars($loadPdfContent);

				$pdfDisplayContent = $loadPdfContent;

				if($_POST["editDocType"] != "") {
					$pdfDisplayContent = preg_replace("/{###DOCUMENT_NUMBER###}/", $_POST["originDocumentNumber"], $pdfDisplayContent);
				}
				else {
					$pdfDisplayContent = preg_replace("/{###DOCUMENT_NUMBER###}/", constant("BASIC_NUMBER_" . $_REQUEST["documentType"]), $pdfDisplayContent);
				}
				$pdfDisplayContent = preg_replace("/{###DOCUMENT_NUMBER###}/", constant("BASIC_NUMBER_" . $_REQUEST["documentType"]), $pdfDisplayContent);
				$pdfCreateContent = $loadPdfContent;

				// BOF INSERT SALESMANS_CUSTOMER_INFO
				$loadedDocumentDatas["SALESMANS_CUSTOMER_INFO"] = "";
				if($_POST["documentType"] == "RE"){
					if($loadedDocumentDatas["SALESMANS_CUSTOMER"] != ""){
						$loadedDocumentDatas["SALESMANS_CUSTOMER_INFO"] = wordunwrap($loadedDocumentDatas["SALESMANS_CUSTOMER"]);
						$pdfDisplayContent .= '<input type="hidden" name="selectSalesmansCustomerInfo" value="' . $loadedDocumentDatas["SALESMANS_CUSTOMER_INFO"] . '" />';
					}
				}
				// EOF INSERT SALESMANS_CUSTOMER_INFO

				// EOF CREATE PDF CONTENT AND PREVIEW DATAS
			}
		}

		else {
			$arrUseSelectDatas = array();
			$arrUseAddDatas = array();

			if(!empty($_POST["addProductsCount"]) && !empty($_POST["addProductDetails"])) {
				$arrUseIDs = array_keys($_POST["addProductsCount"]);

				// $arrUseDatas = $_POST["addProductDetails"];
				$arrTempDatas = $_POST["addProductDetails"];

				$arrUseDatas = array();

				if(!empty($arrUseIDs) && !empty($arrTempDatas)){
					foreach($arrTempDatas as $thisTempDatasKey => $thisTempDatasValue){
						foreach($arrUseIDs as $thisIdKey){
							$arrUseDatas[$thisIdKey][$thisTempDatasKey] = $thisTempDatasValue[$thisIdKey];
							if($thisTempDatasKey == "ordersID" && $thisTempDatasValue[$thisIdKey] == '' ){
								$arrUseDatas[$thisIdKey][$thisTempDatasKey] = $thisIdKey;
							}
						}
					}
				}
				$arrUseAddDatas = $arrUseDatas;
			}
			else if(!empty($_POST["selectOrdersIDs"]) && !empty($_POST["selectOrdersDetails"])) {
				$arrUseIDs = $_POST["selectOrdersIDs"];
				$arrUseDatas = $_POST["selectOrdersDetails"];
				$arrUseSelectDatas = $arrUseDatas;
			}

			if($_POST["editDocType"] == 'AN') {
				$arrUseExistingDatas = array();
				foreach($_POST["selectOrdersIDs"] as $thisKey => $thisValue){
					$arrUseExistingDatas[$thisKey] = $_POST["selectOrdersDetails"][$thisValue];
				}
				$arrUseDatas = array_merge($arrUseExistingDatas, $arrUseAddDatas);
				$arrUseIDs = array_keys($arrUseDatas);
			}

			$loadCardDatas = loadCardDatas($loadCardTemplate, $arrUseIDs, $arrUseDatas);

			$loadPdfContent = $loadContentTemplate;

			// echo $loadCardDatas;
			$loadPdfContent = preg_replace("/{###CARD_DATAS###}/", $loadCardDatas, $loadPdfContent);

			// STYLES
			$loadTemplateStyles = implode('', file(DIRECTORY_PDF_TEMPLATES . 'template_STYLES.html'));

			$loadPdfContent = preg_replace("/{###STYLES###}/", $loadTemplateStyles, $loadPdfContent);

			if(!empty($loadedAdressDatas)) {
				foreach($loadedAdressDatas as $thisKey => $thisValue) {
					if($thisValue != "") {
						$loadPdfContent = preg_replace("/{###".$thisKey."###}/", $thisValue . "<br />", $loadPdfContent);
					}
					else {
						$loadPdfContent = preg_replace("/{###".$thisKey."###}/", "", $loadPdfContent);
					}
				}
			}

			if(!empty($loadedDocumentDatas)) {
				// if(constant('BANK_ACCOUNT_' . $_POST["selectBankAccount"]. '_SHOW_WARNING')){
				if($arrBankAccountTypeDatas[$arrOrderDetailDatagetRelatedDocumentss[$thisTakeOrderKey]["ordersBankAccountType"]]["bankAccountTypesShowWarning"] == '1') {
					$loadPdfContent = preg_replace("/{###BANK_ACCOUNT_WARNING###}/", $arrContentDatas[$_REQUEST["documentType"]]['BANK_ACCOUNT_WARNING'], $loadPdfContent);
				}
				else {
					$loadPdfContent = preg_replace("/{###BANK_ACCOUNT_WARNING###}/", "", $loadPdfContent);
				}
				foreach($loadedDocumentDatas as $thisKey => $thisValue) {
					$loadPdfContent = preg_replace("/{###".$thisKey."###}/", $thisValue, $loadPdfContent);
					if(trim($thisValue) == '') {
						$loadPdfContent = preg_replace("/<!-- BOF_".$thisKey." -->(.*)<!-- EOF_".$thisKey." -->/", '', $loadPdfContent);
					}
				}
			}

			$loadSubjectText = $thisSubject . ' Nr.: ' . '{###DOCUMENT_NUMBER###}';

			if($_REQUEST["documentType"] == 'BR') {
				$loadSubject = '
					<input type="text" name="documentSubject" style="width:800px;font-weight:bold;height:20px;background-color:#EEE;color:#000;border: 1px solid #333;" value="'. $loadSubjectText . '" readonly="readonly" />
					<br><br>
				';
			}
			else {
				$loadSubject = ''.$loadSubjectText. '<br><br>';
			}
			$loadPdfContent = preg_replace("/{###SUBJECT###}/", $loadSubject, $loadPdfContent);
			$loadPdfContent = removeUnnecessaryChars($loadPdfContent);

			$pdfDisplayContent = $loadPdfContent;

			if($_POST["editDocType"] != "") {
				$pdfDisplayContent = preg_replace("/{###DOCUMENT_NUMBER###}/", $_POST["originDocumentNumber"], $pdfDisplayContent);
			}
			else {
				$pdfDisplayContent = preg_replace("/{###DOCUMENT_NUMBER###}/", constant("BASIC_NUMBER_" . $_REQUEST["documentType"]), $pdfDisplayContent);
			}
			$pdfDisplayContent = preg_replace("/{###DOCUMENT_NUMBER###}/", constant("BASIC_NUMBER_" . $_REQUEST["documentType"]), $pdfDisplayContent);
			$pdfCreateContent = $loadPdfContent;

			// BOF INSERT SALESMANS_CUSTOMER_INFO
				$loadedDocumentDatas["SALESMANS_CUSTOMER_INFO"] = "";
				if($_POST["documentType"] == "RE"){
					if($loadedDocumentDatas["SALESMANS_CUSTOMER"] != ""){
						$loadedDocumentDatas["SALESMANS_CUSTOMER_INFO"] = wordunwrap($loadedDocumentDatas["SALESMANS_CUSTOMER"]);
						$pdfDisplayContent .= '<input type="hidden" name="selectSalesmansCustomerInfo" value="' . $loadedDocumentDatas["SALESMANS_CUSTOMER_INFO"] . '" />';
					}
				}
			// EOF INSERT SALESMANS_CUSTOMER_INFO

			// BOF LOAD EDITOR WITH CONTENT
			if($_POST["documentType"] == "BR") {
				if($_POST["selectDocumentsContent"] != "") {
					$loadEditorContent = $_POST["selectDocumentsContent"];
				}
				else {
					$loadEditorContent = 'Bitte geben Sie hier Ihren Text ein ...';
				}
				$loadEditorContent = $_POST["documentEditorText"];

				$CKEditor = new CKEditor();
				$CKEditor->returnOutput = true;
				$CKEditor->basePath = DIRECTORY_CKEDITOR;
				$CKEditor->config['height'] = 300;
				$CKEditor->config['width'] = 800;
				// $CKEditor->config['entities'] = false;
				$CKEditor->config['entities_latin'] = false;
				$CKEditor->textareaAttributes = array("cols" => 80, "rows" => 10);
				/*
				$config['toolbar'] = array(
					array( 'Source', '-', 'Bold', 'Italic', 'Underline', 'Strike' ),
					array( 'Image', 'Link', 'Unlink', 'Anchor' )
				);
				// This is actually the default value.
				config.toolbar_Full =
				[
					{ name: 'document',    items : [ 'Source','-','Save','NewPage','DocProps','Preview','Print','-','Templates' ] },
					{ name: 'clipboard',   items : [ 'Cut','Copy','Paste','PasteText','PasteFromWord','-','Undo','Redo' ] },
					{ name: 'editing',     items : [ 'Find','Replace','-','SelectAll','-','SpellChecker', 'Scayt' ] },
					{ name: 'forms',       items : [ 'Form', 'Checkbox', 'Radio', 'TextField', 'Textarea', 'Select', 'Button', 'ImageButton', 'HiddenField' ] },
					'/',
					{ name: 'basicstyles', items : [ 'Bold','Italic','Underline','Strike','Subscript','Superscript','-','RemoveFormat' ] },
					{ name: 'paragraph',   items : [ 'NumberedList','BulletedList','-','Outdent','Indent','-','Blockquote','CreateDiv','-','JustifyLeft','JustifyCenter','JustifyRight','JustifyBlock','-','BidiLtr','BidiRtl' ] },
					{ name: 'links',       items : [ 'Link','Unlink','Anchor' ] },
					{ name: 'insert',      items : [ 'Image','Flash','Table','HorizontalRule','Smiley','SpecialChar','PageBreak' ] },
					'/',
					{ name: 'styles',      items : [ 'Styles','Format','Font','FontSize' ] },
					{ name: 'colors',      items : [ 'TextColor','BGColor' ] },
					{ name: 'tools',       items : [ 'Maximize', 'ShowBlocks','-','About' ] }
				];
				*/
				$config['toolbar'] = array(
					array('Bold', 'Italic', 'Underline', 'Superscript', 'Subscript', 'Table', 'HorizontalRule', 'SpecialChar' ),
					array('Styles','Format', 'NumberedList','BulletedList','-','Outdent','Indent','-','Blockquote','-','Source'),
					// array( 'Cut','Copy','Paste','PasteText','PasteFromWord','-','Undo','Redo' )
				);

				$config['skin'] = 'office2003'; // 'v2', 'office2003', 'kama'
				// $CKEditor->editor("additionalText", $initialValue);

				$loadTextEditArea = $CKEditor->editor("documentEditorText", ''.$loadEditorContent, $config);
				$_POST["documentEditorText"] = urlencode($loadEditorContent);
				// $loadTextEditArea = "";
				// $loadTextEditArea = html_entity_decode($loadTextEditArea);
				$pdfDisplayContent = preg_replace("/{###EDITOR_TEXT###}/", $loadTextEditArea, $pdfDisplayContent);
			}
			// EOF LOAD EDITOR WITH CONTENT
		}


		// BOF REMOVE PDF TAGS IN PDF DISPLAY CONTENT
			$pdfDisplayContent = preg_replace('/<page(.*)>/ismU', '', $pdfDisplayContent);
			$pdfDisplayContent = preg_replace('/<\/page(.*)>/ismU', '', $pdfDisplayContent);
		// EOF REMOVE PDF TAGS IN PDF DISPLAY CONTENT

		// BOF REMOVE INPUT FIELDS IN PDF CREATION CONTENT
			$pdfCreateContent = preg_replace('/<input (.*) \/>/ismU', '', $pdfCreateContent);
			// <input type="hidden" name="arrItemDatas[4369][basketItemId]" value="4369" />
		// EOF REMOVE INPUT FIELDS IN PDF CREATION CONTENT

		// EOF CREATE PDF CONTENT AND PREVIEW DATAS

		// BOF REPLACE &euro; WITH EUR
			// &euro; IS NOT DISPLAYED IN PDF (?????)
			$pdfDisplayContent = preg_replace("/&euro;/", "EUR", $pdfDisplayContent);
			$pdfCreateContent = preg_replace("/&euro;/", "EUR", $pdfCreateContent);
		// EOF REPLACE &euro; WITH EUR

		// BOF DISPLAY PDF PREVIEW CONTENT
		echo '<div id="displayCreateDocument" style="padding:10px; background-color:#FFF;">';
		# echo htmlentities($pdfDisplayContent);
		echo $pdfDisplayContent;
		echo '</div>';

		if($_REQUEST["documentType"] == 'RE' && $_REQUEST["selectPaymentType"] == '3'){
			//if($userDatas["usersLogin"] == 'thorsten'){
			if(1){
				#echo '<hr />';
				#echo '<h2>TEST FAX-DOKUMENT</h2>';
				#error_reporting(E_ALL);
				require_once('inc/createDeliveryFaxDocument.inc.php');
				#echo '<hr />';
			}
		}
		// EOF DISPLAY PDF PREVIEW CONTENT

		echo '<hr />';

		// BOF DISPLAY PDF CREATION CONTENT
		#echo '<div id="displayCreateDocument" style="padding:10px; background-color:#FFF;">';
		#echo $pdfCreateContent;
		#echo htmlentities($pdfCreateContent);
		#echo '</div>';
		//EOF DISPLAY PDF CREATION CONTENT

		// BOF WRITE PDF CREATION CONTENT TO TMP FILE
			$pdfTempName = tempnam('tmp', 'tempname');
			$pdfTempFile = fopen($pdfTempName, 'w');
			fwrite($pdfTempFile, $pdfCreateContent);
			fclose($pdfTempFile);

			if($pdfTempName != '' && file_exists($pdfTempName)) {
				if($userDatas["usersLogin"] == 'thorsten'){
					#$pdfTempName = '<a href="' . $pdfTempName . '">' . $pdfTempName . '</a>';
					$successMessage .= ' Die temporäre Datei &quot;' . $pdfTempName . '&quot; wurde generiert. ' . '<br />';
				}
			}
			else {
				$errorMessage .= ' Die temporäre Datei ' . $pdfTempName . ' konnte nicht generiert werden. ' . '<br />';
			}
			clearstatcache();
		// EOF WRITE PDF CREATION CONTENT TO TMP FILE
		if($pdfTempName != '' && file_exists($pdfTempName)) {
			echo '<input type="hidden" name="pdfTempName" value="' . $pdfTempName . '">';
			// echo '<input type="hidden" name="pdfTempContent" value="' . addslashes(htmlentities($pdfCreateContent)) . '">';
	?>

	<br>
	<script type="text/javascript" language="javascript">
		window.alert('Bitte kontrollieren Sie, ob die Vorschau korrekt ist.\nBitte erst speichern, wenn die Daten in Ordnung sind.');
	</script>

	<div class="actionButtonsArea">
		<?php
			# echo '<a href="javascript:history.go(-1);" onclick="return showWarning(\' Wollen Sie diesen Vorgang wirklich abändern???? \')" > - </a>';
			if(!empty($_POST)) {
				foreach($_POST as $thisKey => $thisValue){
					if(!is_array($thisValue)) {
						echo '<input type="hidden" name="' . $thisKey . '" value="' . $thisValue . '" />';
						// echo '<input type="hidden" name="' . $thisKey . '" value="' . addslashes($thisValue) . '" />';
					}
				}
				echo '<input type="hidden" name="postSerializedDatas" value="'.urlencode(serialize($_POST)).'" />';
			}
			echo '<input type="hidden" name="selectSubject" value="' . $loadSubjectText . '" />';
		?>
		<input type="submit" class="inputButton1 inputButtonRed" name="returnDatas" value="Zur&uuml;ck" onclick="return showWarning(' Wollen Sie diesen Vorgang wirklich abändern???? '); "/>
		&nbsp;

		<?php
			// if(!empty($_POST["selectOrdersIDs"])){
			if(1){
		?>

		<input type="submit" class="inputButton1 inputButtonGreen" name="storeDatas" value="PDF nur Speichern" onclick="return showWarning(' Soll das Dokument wirklich nur gespeichert und nicht gesendet werden? ');" />
		&nbsp;

		<?php
			// if($_POST["selectCustomersRecipientMail"] != "" && $_REQUEST["documentType"] != 'LS' && $_REQUEST["documentType"] != 'RE') {
			if($_POST["selectCustomersRecipientMail"] != "" || $_POST["selectCustomersRecipientMailCopy"] != "") {
				$buttonDisabled = '';
				$buttonTitle = '';
				$buttonValue = '';
				$buttonStyle = '';
				if($_POST["selectCustomersRecipientMail"] == "" && $_POST["selectCustomersRecipientMailCopy"] == "") {
					$buttonDisabled = ' disabled="disabled" ';
					$buttonTitle = 'Keine Mail-Adresse angegeben...';
					$buttonValue = '';
				}
				if($_POST["selectCustomersRecipientMail"] == "" && $_POST["selectCustomersRecipientMailCopy"] != "") {
					$buttonDisabled = '';
					$buttonTitle = 'Es ist keine Kundenadresse hinterlegt.' . "\n" . 'Es kann nur die Mail-Kopie versendet werden.';
					$buttonValue = 'PDF speichern UND NUR KOPIE versenden';
					$buttonStyle = ' style="width:300px !important" ';
				}
				if($_POST["selectCustomersRecipientMail"] != "" && $_POST["selectCustomersRecipientMailCopy"] == "") {
					$buttonDisabled = '';
					$buttonTitle = '';
					$buttonValue = 'PDF speichern UND versenden';
					$buttonStyle = ' style="width:220px !important" ';
				}
				if($_POST["selectCustomersRecipientMail"] != "" && $_POST["selectCustomersRecipientMailCopy"] != "") {
					$buttonDisabled = '';
					$buttonTitle = '';
					$buttonValue = 'PDF speichern UND versenden';
					$buttonStyle = ' style="width:220px !important" ';
				}
		?>
		<input type="submit" class="inputButton1 inputButtonOrange" name="storeAndSendDatas" <?php echo $buttonStyle; ?> <?php echo $buttonDisabled; ?> title="<?php echo $buttonTitle; ?>" value="<?php echo $buttonValue; ?>" onclick="return showWarning(' Soll das Dokument wirklich gespeichert und gesendet werden? ');" />
		&nbsp;
		<?php
			}
		?>
		<?php
			}
			else {
				$jswindowMessage .= " Sie haben keinen Artikel angegeben! " . "\n";
			}
		?>
		<input type="submit" class="inputButton1 inputButtonRed" name="cancelDatas" value="Abbrechen" onclick="return showWarning(' Wollen Sie diesen Vorgang wirklich abbrechen??? '); "/>
	</div>
		<input type="hidden" name="documentSubject" value="<?php echo $_REQUEST["documentSubject"]; ?>" />
		<input type="hidden" name="documentType" value="<?php echo $_REQUEST["documentType"]; ?>" />
		<input type="hidden" name="editID" id="editID" value="<?php echo $_REQUEST["editID"]; ?>" />
		<input type="hidden" name="documentNumber" id="documentNumber" value="<?php echo constant("BASIC_NUMBER_" . $_REQUEST["documentType"]); ?>" />
		<input type="hidden" name="selectOrdersIDsFirstIndex" id="selectOrdersIDsFirstIndex" value="<?php echo $selectOrdersIDsFirstIndex; ?>" />
		<input type="hidden" name="documentOrdersIDs" id="documentOrdersIDs" value="<?php echo implode(';', array_values($arrUseIDs)); ?>" />
	</form>
	<?php
			}
			clearstatcache();
		}
		// EOF DOCUMENT PREVIEW
	?>
	<?php
		echo '<hr />';
		if($warningMessage != "") {	echo '<p class="warningArea">'.$warningMessage.'</p>'; }
		if($errorMessage != "") {	echo '<p class="errorArea">'.$errorMessage.'</p>'; }
		if($successMessage != "") {	echo '<p class="successArea">'.$successMessage.'</p>'; }
		if($infoMessage != "") {	echo '<p class="infoArea">'.$infoMessage.'</p>'; }
		$warningMessage = "";
		$errorMessage = "";
		$successMessage = "";
		$infoMessage = "";
	?>
	</div>
</div>
</div>
</div>
<div class="clear"></div>
</div>
</div>

<script language="javascript" type="text/javascript">
	$(document).ready(function() {
		setFocus('formFilterSearch', 'searchCustomerNumber');

		$('#editOrdersArtikelBezeichnung').keyup(function () {
			loadSuggestions('searchProductName', [{'triggerElement': '#editOrdersArtikelBezeichnung', 'fieldNumber': '#editOrdersArtikelNummer', 'fieldName': '#editOrdersArtikelBezeichnung', 'fieldID': '#editOrdersArtikelID'}], 1);
		});
		$('#editOrdersArtikelNummer').keyup(function () {
			loadSuggestions('searchProductName', [{'triggerElement': '#editOrdersArtikelNummer', 'fieldNumber': '#editOrdersArtikelNummer', 'fieldName': '#editOrdersArtikelBezeichnung', 'fieldID': '#editOrdersArtikelID'}], 1);
		});
		$('#editOrdersPLZ').keyup(function () {
			loadSuggestions('searchPlzCity', [{'triggerElement': '#editOrdersPLZ', 'fieldCity': '#editOrdersOrt', 'fieldZipCode': '#editOrdersPLZ'}], 1);
		});
		$('#editOrdersOrt').keyup(function () {
			loadSuggestions('searchCityPlz', [{'triggerElement': '#editOrdersOrt', 'fieldCity': '#editOrdersOrt', 'fieldZipCode': '#editOrdersPLZ'}], 1);
		});
		/*
		$('#editOrdersArtikelMenge').keyup(function () {
			getProductPrices('#editOrdersArtikelMenge', '#editOrdersSinglePreis', '#editOrdersTotalPreis', '#editOrdersArtikelID');
		});
		*/
		<?php if($_POST["convertDocType"] == '') { ?>
		$('input[name*="ordersArtikelMenge"]').live('keyup', function() {
			var thisMenge = $(this).parent().parent().find('input[name*="ordersArtikelMenge"]').val();
			var thisArtikelID = $(this).parent().parent().find('input[name*="ordersArtikelNummer"]').val();
			if(thisArtikelID != '' && thisMenge > 0) {
				var thisElementSinglePrice = $(this).parent().parent().find('input[name*="ordersSinglePreis"]');
				getProductPrices(thisMenge, thisElementSinglePrice, thisArtikelID);
			}
		});
		<?php } ?>

		$('#searchCustomerNumber').keyup(function () {
			loadSuggestions('searchCustomerNumber', [{'triggerElement': '#searchCustomerNumber', 'fieldNumber': '#searchCustomerNumber', 'fieldName': '#searchCustomerName', 'fieldCity': '', 'fieldZipCode': ''}], 1);
		});
		$('#searchCustomerName').keyup(function () {
			loadSuggestions('searchCustomerName', [{'triggerElement': '#searchCustomerName', 'fieldNumber': '#searchCustomerNumber', 'fieldName': '#searchCustomerName', 'fieldCity': '', 'fieldZipCode': ''}], 1);
		});
		$('.buttonNotice').click(function () {
			loadNotice($(this));
		});

		$('#selectCustomersNumber').keyup(function () {
			//loadSuggestions('searchDocumentAddressDatas', [{'triggerElement': '#selectCustomersNumber', 'documentType': '<?php echo $_REQUEST["documentType"] ?>', 'fieldNumber': '#selectCustomersNumber', 'fieldName': '#selectCustomersName', 'fieldCity': '#editOrdersOrt', 'fieldZipCode': '#editOrdersPLZ', 'fieldSalesmanName': '#editOrdersVertreterName', 'fieldSalesmanID': '#selectSalesman', 'fieldMail':'#selectCustomersRecipientMail'}], 1);
		});

		<?php if($loadAddProductForm != '') { ?>
		$('.buttonAddProduct').click(function () {
			loadAddFormProduct($(this), '<?php echo addslashes($loadAddProductForm); ?>');
		});
		<?php } ?>

		$('.selectShippingExpress').live('click',function () {
			var thisChecked = $(this).attr('checked');
			if(thisChecked == 'checked') {
				$(this).parent().find('.expressCostsArea').show();
			}
			else {
				$(this).parent().find('.expressCostsArea').hide();
			}
			//
		});

		if($('#selectPaymentType').val() == '3') {
			$('#shippingCashOnDeliveryArea').show();
		}
		$('#selectPaymentType').change(function(){
			if($(this).val() == '3') {
				$('#shippingCashOnDeliveryArea').show();
			}
			else {
				$('#shippingCashOnDeliveryArea').hide();
			}
		});

		<?php
			if(USE_DEBITING_DATE){
		?>
		if($('#selectPaymentType').val() == '4') {
			$('#debitingDateArea').show();
		}
		$('#selectPaymentType').change(function(){
			if($(this).val() == '4') {
				$('#debitingDateArea').show();
			}
			else {
				$('#debitingDateArea').hide();
			}
		});
		<?php
			}
		?>

		// $('.buttonDeleteProduct').live('click', function() {
		$(document).on('click', '.buttonDeleteProduct', function() {
			deleteFormProductItem($(this));
		});

		setFormDutyFields(<?php echo $jsonFormDutyFields; ?>);

		function clearInputField(fieldToClear){
			$(fieldToClear).val('');
		}
		$('#selectSalesman').change(function(){
			clearInputField('#selectCustomersRecipientMailCopy');
			// clearInputField('#selectCustomersRecipientMail');
		});

		colorRowMouseOver('.displayOrders tbody tr');
		$(function() {
			$( "#datepicker" ).datepicker();
			$.datepicker.setDefaults($.datepicker.regional[""]);
			$('#selectOfferDate').datepicker($.datepicker.regional["de"]);
			$('#selectConfirmationDate').datepicker($.datepicker.regional["de"]);
			$('#selectDeliveryDate').datepicker($.datepicker.regional["de"]);
			$('#selectInvoiceDate').datepicker($.datepicker.regional["de"]);
			$('#selectCreditDate').datepicker($.datepicker.regional["de"]);
			$('#selectReminderDate').datepicker($.datepicker.regional["de"]);
			$('#selectLetterDate').datepicker($.datepicker.regional["de"]);
			$('#selectBindingDate').datepicker($.datepicker.regional["de"]);
			$('#selectDocumentDate').datepicker($.datepicker.regional["de"]);
			$('#selectOrderDate').datepicker($.datepicker.regional["de"]);
			$('#selectDebitingDate').datepicker($.datepicker.regional["de"]);

			var htmlButtonClearField = '<span class="buttonClearField"><img src="layout/icons/iconDelete.png" width="16" height="16" alt="" title="Eingabefeld leeren" /></span>';
			$('#selectDeliveryDate').parent().append(htmlButtonClearField);
			$('#selectDocumentDate').parent().append(htmlButtonClearField);
			$('#selectDebitingDate').parent().append(htmlButtonClearField);
			$('#selectOrderDate').parent().append(htmlButtonClearField);
			$('#selectCustomersRecipientManager').parent().append(htmlButtonClearField);
			$('#selectCustomersRecipientContact').parent().append(htmlButtonClearField);
			$('#selectCustomersRecipientMail').parent().append(htmlButtonClearField);
			$('#selectCustomersRecipientMailCopy').parent().append(htmlButtonClearField);
			$('#selectCustomersRecipientFax').parent().append(htmlButtonClearField);
			$('#selectCustomersRecipientNameAdd').parent().append(htmlButtonClearField);
			$('#selectCustomersSecondaryRecipientManager').parent().append(htmlButtonClearField);
			$('#selectCustomersSecondaryRecipientContact').parent().append(htmlButtonClearField);
			$('#selectCustomersSecondaryRecipientMailCopy').parent().append(htmlButtonClearField);
			$('#selectCustomersSecondaryRecipientNameAdd').parent().append(htmlButtonClearField);

			$('.buttonClearField').click(function () {
				$(this).parent().find('input').val('');
			});
		});

		$('.selectOrderID').live('click', function() {
			var thisChecked = $(this).attr('checked');
			if(thisChecked == 'checked') {
				var thisFreigabeDatum = $(this).parent().parent().find('.ordersFreigabeDatum').text();
				var thisBestellDatum = $(this).parent().parent().find('.ordersBestellDatum').text();
				var thisUseDate = '';

				if(thisFreigabeDatum != '') {
					thisUseDate = thisFreigabeDatum;
				}
				else {
					thisUseDate = thisBestellDatum;
				}
				$('#selectOrderDate').val(thisUseDate);
			}
		});
		<?php
			if($_REQUEST["editDocType"] == '' && $_REQUEST["convertDocType"] == '') {
				//$arrMandatoryDatas = getMandatories();
		?>
			// var confirmResult = window.confirm('Wollen Sie wirklich ein Dokument unter <?php echo strtoupper(MANDATOR); ?> für den Kunden "<?php echo addslashes($customersData["Firmenname"]); ?>" erstellen?');
			// if(!confirmResult){
				// window.location.href = "";
			// }
		<?php
			}
		?>
	});
</script>

<?php
	if($userDatas["usersLogin"] == 'thorsten'){

	}
?>
<?php require_once('inc/footerHTML.inc.php'); ?>