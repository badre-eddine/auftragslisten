<?php
	require_once('inc/requires.inc.php');

	if(!$arrGetUserRights["editOrders"]) {
		header('location: ' . PAGE_EXIT_LOCATION);
		exit;
	}

	$jswindowMessage = '';
	$warningMessage = '';
	$errorMessage = '';
	$successMessage = '';
	$infoMessage = '';

	$dbConnection = new DB_Connection();
	$db_open = $dbConnection->db_connect();

	
	// BOF READ PRINTERS
		$arrPrinterDatas = getPrinters();

		$userDatas = getUserDatas(); 

		if($userDatas["usersLogin"] == 'thorsten'){
			DEFINE('USE_DEBITING_DATE', false);
		}
		else {
			DEFINE('USE_DEBITING_DATE', false);
		}
	// EOF READ PRINTERS

	// BOF READ PRINTTYPES
		$arrPrintTypeDatas = getPrintTypes();
	// EOF READ PRINTTYPES

	// BOF READ VERTRETER
		$arrAgentDatas = getAgents();
		$arrAgentDatas2 = getAgents2();
	// EOF READ VERTRETER

	// BOF READ PAYMENT TYPES
		$arrPaymentTypeDatas = getPaymentTypes();
	// EOF READ PAYMENT TYPES

	// BOF READ PAYMENT STATUS TYPES
		$arrPaymentStatusTypeDatas = getPaymentStatusTypes();
	// EOF READ PAYMENT STATUS TYPES

	// BOF READ BESTELL ARTEN
		$arrOrderTypeDatas = getOrderTypes();
	// EOF READ BESTELL ARTEN

	// BOF READ BESTELL STATUS TYPEN
		$arrOrderStatusTypeDatas = getOrderStatusTypes();
	// EOF READ BESTELL STATUS TYPEN

	// BOF READ ADDITIONAL COSTS
		$arrAdditionalCostsDatas = getAdditionalCosts();
	// EOF READ ADDITIONAL COSTS

	// BOF READ ORDER SOURCE TYPES
		$arrOrdersSourceTypeDatas = getOrdersSourceTypes();
	// EOF READ ORDER SOURCE TYPES

	// BOF READ BANK TYPES
		$arrBankAccountTypeDatas = getBankAccountTypes();
	// EOF READ BANK TYPES

	// BOF READ ORDER CATEGORIES
		// $arrOrderCategoriesTypeDatas = getOrderCategories();
	// EOF READ ORDER CATEGORIES

	// BOF READ PRINT COLORS
		$arrPrintColors = getPrintColors();
	// EOF READ PRINT COLORS

	// BOF READ PRINT COLORS
		$arrSupplierDatas = getSuppliers();
	// EOF READ PRINT COLORS

	// BOF READ COUNTRIES
		$arrCountryTypeDatas = getCountryTypes();
	// EOF READ COUNTRIES


	if($_GET["downloadFile"] != "") {
		require_once('classes/downloadFile.class.php');
		$thisDownload = new downloadFile(DIRECTORY_CREATED_DOCUMENTS_CUSTOMERS, $_GET["downloadFile"]);
		$errorMessage .= $thisDownload->startDownload();
	}

	// BOF DEFINE DUTY FIELDS
		$arrFormDutyFields = array(
			// "editOrderPrintPlateNumber" => "Druck-Auftrag",
			"editOrdersBestellDatum" => "Bestelldatum",
			"editOrdersKundennummer" => "Kundennummer",
			"editOrdersKundenName" => "Kundenname",
			// "editOrdersPLZ" => "PLZ",
			// "editOrdersOrt" => "Ort",
			// "editOrdersLand" => "Land",
			"editOrdersArtikelNummer" => "Artikel-Nummer",
			"editOrdersArtikelKategorieID" => "Artikel-Kategorie",
			"editOrdersArtikelBezeichnung" => "Artikel-Bezeichnung",
			"editOrdersArtikelMenge" => "Artikel-Menge",
			// "editOrdersArtikelPrintColorsCount" => "Anzahl Druckfarben",
			//"editOrdersSinglePreis" => "Artikel-Einzelpreis",
			//"editOrdersTotalPreis" => "Artikel-Gesamtpreis",
			// "editOrdersDruckFarbe" => "Artikel-Druckfarbe",
			"editOrdersAdditionalCostsID" => "Artikel-Druckart",
			"editOrdersStatus" => "Bestell-Status",
			"editOrdersSourceType" => "Bestell-Quelle",
			"editOrdersArtikelMenge" => "Bestell-Quelle",
			"druckAuftrag" => "Druck-Auftrag",
			"editOrdersOrderType" => "Bestell-Art"
		);
		// CREATE JSON DUTY FIELD
		$jsonFormDutyFields = createJson($arrFormDutyFields);
	// BOF DEFINE DUTY FIELDS



	if($_REQUEST["editID"] == "") {
		// $_REQUEST["editID"] = "NEW";
		// header("location: displayOrders.php");
		// exit;
	}

	//new inset 

	if(isset($_POST["addArticle"]))
	{
		$qty = $_POST['qty'];
		$editID = $_POST['editID'];
		$fr = $_POST['fr'];
		$article = $_POST['article'];
		$dateRec = $_POST['dateRec'];
		$note = $_POST['note'];
		$stockCountry = $_POST['stockCountry'];

		if( isset($qty) &&
			isset($fr) &&
			isset($article) &&
			isset($dateRec) &&
			isset($stockCountry) &&
			isset($note)
		){
			if( !empty($qty) &&
			!empty($fr) &&
			!empty($article) &&
			!empty($dateRec) &&
			!empty($stockCountry) &&
			!empty($note)
			){
				
				$sql = "
				INSERT INTO `common_stock`(
					 
						`article`,
						`qty`,
						`fournisseur`,
						`stockCountry`,
						`date_recp`,
						`note`,
						`bearbeiter`
						)
						VALUES (
							 
						
							'" . $article . "',
							'" . $qty . "',
							'" . $fr . "',
							'" . $stockCountry . "',
							'" . $dateRec . "',
							'" . $note . "',
							'" .$userDatas['usersFirstName'].' '.$userDatas['usersLastName'] ."'
							 
						)
				";
					 
				if($editID != '')
				{
					$sql = "
					UPDATE `common_stock`
					SET article='" . $article . "',
						qty='" . $qty . "',
						fournisseur='" . $fr . "',
						stockCountry='" . $stockCountry . "',
						date_recp='" . $dateRec . "',
						note='" . $note . "',
						bearbeiter=	'" .$userDatas['usersFirstName'].' '.$userDatas['usersLastName'] ."'

					 
					WHERE id_article='" . $editID . "' ";
				} 
			 
				$rs = $dbConnection->db_query($sql);

				if($rs) {
					$successMessage .= ' Die Warenbestellung wurde gespeichert.' .'<br />';
				}
				else {
					$errorMessage .= ' Die Warenbestellung konnte nicht gespeichert werden.' . '<br />';
				}
		}

		}

	}
	if(isset($_POST["addArticleAndShow"]))
	{
		$qty = $_POST['qty'];
		$fr = $_POST['fr'];
		$article = $_POST['article'];
		$dateRec = $_POST['dateRec'];
		$note = $_POST['note'];

		if( isset($qty) &&
			isset($fr) &&
			isset($article) &&
			isset($dateRec) &&
			isset($note)
		){
			if( !empty($qty) &&
			!empty($fr) &&
			!empty($article) &&
			!empty($dateRec) &&
			!empty($note)
			){
			 
				$sql = "
					INSERT INTO `common_stock`(
						 
							`article`,
							`qty`,
							`fournisseur`,
							`date_recp`
							)
							VALUES (
								 
								'" . $article . "',
								'" . $qty . "',
								'" . $fr . "',
								'" . $dateRec . "'
								 
							)
					";
				$rs = $dbConnection->db_query($sql);

				if($rs) {
					$successMessage .= ' Die Warenbestellung wurde gespeichert.' .'<br />';

					//header('location:showAllStock.php');
				}
				else {
					$errorMessage .= ' Die Warenbestellung konnte nicht gespeichert werden.' . '<br />';
				}
		}

		}

	}

	
	
	// BOF GET ORDER PRODUCTS / CATEGORIES
		if($_COOKIE["isAdmin"] == '1'){
			 $arrLoadedProducts = getOrderProducts('all', MANDATOR, $thisCusomerGroupID);
		}
		else {
			 $arrOrderCategoriesTypeDatas = getOrderCategories();
		}
	// EOF GET ORDER PRODUCTS / CATEGORIES

	if($_REQUEST["storeDatasAndGotToCustomer"] != ""){
		header('location:' . PAGE_EDIT_CUSTOMER . '?editCustomerNumber=' . $_REQUEST["editOrdersKundennummer"]);
	}
?>
<?php
	require_once('inc/headerHTML.inc.php');
	

	switch ($_GET["stockcountry"]) {
		case 'de':
			$thisTitle = "Lager ansehen DE";
			$queryWhere = "where stockCountry = 'de'";
			break;
		case 'tr':
			$thisTitle = "Lager ansehen TR";
			$queryWhere = "where stockCountry = 'tr'";
			break;
		
		default:
		$thisTitle = "Lager ansehen DE + TR";
		$queryWhere = "";
			break;
	}
 

	
	if($_REQUEST["editID"] != ""){
		if($_REQUEST["editID"] == "NEW"){
			$thisTitle .= ': <span class="headerSelectedEntry">Neues Produkt</span>';
		}
		else {
			$thisTitle .= ': <span class="headerSelectedEntry">' . htmlentities($arrSelectedProductsCategory["stockProductsName"]) . ' &bull; ' . htmlentities($arrSelectedProductsCategory["stockProductsProductNumber"]) . '</span>';
		}
	}

	$headerHTML = preg_replace("/{###TITLE###}/", strip_tags($thisTitle), $headerHTML);
	echo $headerHTML;

	require_once(FILE_MENUE_TOP);

?>

<?php require_once(FILE_MENUE_SIDEBAR); ?>
<div id="contentArea">
	<a name="top"></a>
	<div id="contentAreaElements">
		<h1><?php if(SHOW_PAGE_TITLE_ICONS) { echo '<img src="' . PATH_ICONS_MENUE_TITLES . 'shipping.png" alt="" />'; } ?> <?php echo $thisTitle; ?></h1>

		<?php 
		$curPageName = substr($_SERVER["SCRIPT_NAME"],strrpos($_SERVER["SCRIPT_NAME"],"/")+1); 
		if($curPageName == 'addStock.php' || $_GET['editID'] != '')
		{
			
		}else{
			displayMessages();
		}

	 	?>
		<?php if($_REQUEST["editID"] == "" && $arrGetUserRights["editStocks"] == '1') { ?>
			<div class="menueActionsArea">
				<a href="addStock.php" class="linkButton">Neu Ware anlegen </a>
				<a href="showStock.php?stockcountry=all" class="linkButton">Lager DE + TR </a>
				<a href="showStock.php?stockcountry=de" class="linkButton">Lager DE </a>
				<a href="showStock.php?stockcountry=tr" class="linkButton">Lager TR </a>
				<div class="clear"></div>
			</div>
		<?php } ?>

		<div class="contentDisplay">
			
			<?php 
					
						displayMessages();
					
			
					 ?>
 
			<?php
				if(1==1)
				{
			?>
		 
			<div class="adminEditArea">

			<p class="warningArea">Pflichtfelder <span class="dutyField">(*)</span> m&uuml;ssen ausgef&uuml;llt werden!</p>

			<form method="post" action="<?php echo $_SERVER["PHP_SELF"]; ?>" >
			

			<fieldset>
				<legend>Neu Ware anlegen</legend>
				<table border="0" width="100%" cellspacing="0" cellpadding="0">
					<tr>
						<td width="200"><b>Produkt:</b></td>
						<td>
					
							 <?php 
								 if($_GET['editID'] != '')
								 {
									 $editID = $_GET['editID'];
									 $query_stock = "SELECT *  FROM `common_stock`  where id_article = '".$editID."' ";
									 $rs_getSto = $dbConnection->db_query($query_stock);
									 $ds_getSto =  mysqli_fetch_row($rs_getSto); 
									 $note = isset($ds_getSto[6]) ? $ds_getSto[6]  : '';
									 $product = isset($ds_getSto[2]) ? $ds_getSto[2]  : '';
									 $lieferant = isset($ds_getSto[5]) ? $ds_getSto[5]  : '';
									 $LagerOrt = isset($ds_getSto[1]) ? $ds_getSto[1]  : '';
									 $Qty = isset($ds_getSto[3]) ? $ds_getSto[3]  : '';
									 $DateRec = isset($ds_getSto[4]) ? $ds_getSto[4]  : '';
									 //var_dump($ds_getSto);

								 }

								
								 
							 ?>
		 
						<select name="article" id="" class="inputSelect_510" onchange="insertProductDatas(this.form.name, this.id, '[editOrdersArtikelNummer,editOrdersArtikelBezeichnung,editOrdersArtikelID]');">
								<option value="0">  --- Bitte w&auml;hlen --- </option>
								<?php							  
									if(!empty($arrLoadedProducts)){										
										foreach($arrLoadedProducts as $thisCategoryKey => $thisCategoryData){
										 
									//	print_r($thisCategoryData);exit();								
											echo '<option class="level_1" value="' . $thisCategoryKey . '">' . $thisCategoryKey . '|' . htmlentities($thisCategoryData["categoriesName"]) . '</option>';
											
											if(!empty($thisCategoryData["products"])){
												foreach($thisCategoryData["products"] as $thisProductKey => $thisProductValue){
													$selected = '';
													if(
														$thisProductKey == $orderDatas["ordersArtikelID"]
														|| $thisProductKey == $orderDatas["ordersArtikelKategorieID"]
													){
														$selected = ' selected="selected" ';
													}elseif($_GET['editID'] != ''){
														if(htmlentities($thisProductValue["productsName"]) == $product)
														{
															$selected = ' selected="selected" ';
														}
													}
													echo '<option class="level_2" value="'.htmlentities($thisProductValue["productsName"]).'"' . $selected . ' >' . htmlentities($thisProductValue["productsName"]) . ' / ' . htmlentities($thisCategoryData["categoriesName"]) . ' / ' . $thisProductValue["productsProductNumberUse"] . ' / ' . $thisProductValue["productsID"] . '</option>';
												}
											}									
										}
									}
								?>
							</select>

					 	</td>
					</tr>
					<tr>
						<td width="200"><b>Eingangsdatum:</b></td>
						<td>
							<?php
								$thisDate = formatDate($orderDatas["ordersBestellDatum"], "split");
							?>
							<input type="text" name="dateRec" id="editOrdersBestellDatum" maxlength="2" class="inputField_100" readonly="readonly" value="<?php echo $DateRec; ?>"   />
<!-- 							if($orderDatas["ordersBestellDatum"] == "" || $orderDatas["ordersBestellDatum"] == "0000-00-00") { echo date("d.m.Y"); } else { echo formatDate($orderDatas["ordersBestellDatum"], "display"); }
 -->						</td>
					</tr>

					<tr>
						<td width="200"><b>Lieferant:</b></td>
						<td>
							<!--<input type="text" name="fr" class="inputField_510" value="?php echo $orderDatas["ordersSourceName"]; ?>" />-->
							<select name="fr" id="" class="inputSelect_510">
								<option value="0">  --- Bitte w&auml;hlen --- </option>
								<option value="Burhan-GmbH" <?php if($lieferant == 'Burhan-GmbH'){ echo 'selected'; }?> >Burhan-GmbH</option> 
								
						</select>
						</td>
					</tr>
				 
					<tr>
						<td width="200"><b>Lager Ort:</b></td>
						<td>
						<select name="stockCountry" id="" class="inputSelect_510">
								<option <?php if($LagerOrt == 'de'){ echo 'selected'; }?> value="de">DE </option> 
								<option <?php if($LagerOrt == 'tr'){ echo 'selected'; }?> value="tr">TR </option> 
						</select>
						</td>
					</tr>
					<tr>
						<td width="200"><b>Menge:</b></td>
						<td>
							<input type="text" name="qty" class="inputField_510" value="<?php echo $Qty; ?>" />
							<input type="hidden" name="editID" class="inputField_510" value="<?php echo $_GET['editID'];  ?>" />
						</td>
					</tr>
					<tr>
						<td width="200"><b>Notizen:</b></td>
						<td>
							<input type="text" name="note" class="inputField_510"   value="<?php echo $note; ?>" />
						</td>
					</tr>
				 
				   
				 
					 
					
				  
					 
				</table>
			</fieldset>
 
	
			
	
		
			<div class="actionButtonsArea">
				<input type="hidden" name="jsonReturnParams" value="<?php echo htmlentities($_REQUEST["jsonReturnParams"]); ?>" />

				<?php
					// if($orderDatas["ordersAuslieferungsDatum"] > 0) {
					if(1){

					}
					#else {
				?>
				<?php if($_COOKIE["isAdmin"] == '1' || $orderDatas["ordersStatus"] != '4' || $orderDatas["ordersAuslieferungsDatum"] == date('Y-m-d')) { ?>
				<input type="submit" class="inputButton1 inputButtonGreen" name="addArticle" value="Speichern" />
				&nbsp;
				<?php } ?>
				<?php if($orderDatas["ordersStatus"] != '4') { ?>
				<!--<input type="submit" class="inputButton3 inputButtonGreen" name="addAticle" value="Speichern und weiteren Vorgang hinzuf&uuml;gen" onclick="return checkFormDutyFields(this.form.name, <?php echo $jsonFormDutyFields; ?>);" />
				&nbsp;
				<input type="submit" class="inputButton3 inputButtonGreen" name="addArticleAndShow" value="Speichern und zu Kundendaten gehen" onclick="return checkFormDutyFields(this.form.name, <?php echo $jsonFormDutyFields; ?>);" />
				<?php } ?>-->
				<?php
					#}
					if($checkProductionIsUsed < 1 && $_REQUEST["editID"] != "NEW") {
						if(strtolower(MANDATOR) == strtolower($orderDatas["ordersMandant"])) {
				?>
				&nbsp;
				<input type="submit" class="inputButton1 inputButtonRed" name="deleteDatas" value="Entfernen" onclick="return showWarning(' Wollen Sie diesen Eintrag endgültig entfernen??? ');" />
				<?php
						}
						else {
							// PRODUKTION KANN NUR ENTFERNT WERDEN, WENN PRODUKTION NICHT IN DOKUMENT AB etc. VERWENDET WURDE
							// UND WENN PRODUKTIONS-MANDANT MIT DEM EINGELOGGTEN MANDANTEN ÜBEREINSTIMMT!!!
						}
					}
				?>
				&nbsp;
				<input type="submit" class="inputButton1 inputButtonOrange" name="resetDatas" value="Abbrechen" onclick="return showWarning(' Wollen Sie diesen Vorgang wirklich abbrechen??? ');" />
			</div>
			</form>
			</div>
			<?php
				}
			?>
		</div>
	</div>
</div>
</div>
</div>
<script language="javascript" type="text/javascript">	
	<!--
	/* <![CDATA[ */
	$(document).ready(function() {
		$(function() {
			$('#editOrdersKundennummer').keyup(function () {
				// loadSuggestions('searchCustomerNumberDatas', [{'triggerElement': '#editOrdersKundennummer', 'fieldNumber': '#editOrdersKundennummer', 'fieldName': '#editOrdersKundenName', 'fieldCity': '#editOrdersOrt', 'fieldZipCode': '#editOrdersPLZ'}], 1);
				loadSuggestions('searchCustomerNumberDatas', [{'triggerElement': '#editOrdersKundennummer', 'fieldNumber': '#editOrdersKundennummer', 'fieldName': '#editOrdersKundenName', 'fieldCity': '#editOrdersOrt', 'fieldCountry': '#editOrdersLand', 'fieldZipCode': '#editOrdersPLZ', 'fieldSalesmanName': '#editOrdersVertreterName', 'fieldSalesmanID': '#editOrdersVertreterID', 'fieldMail':'#editOrdersMailAdress'}], 1);
			});
			$('#editOrdersKundenName').keyup(function () {
				// loadSuggestions('searchCustomerNumberDatas', [{'triggerElement': '#editOrdersKundenName', 'fieldNumber': '#editOrdersKundennummer', 'fieldName': '#editOrdersKundenName', 'fieldCity': '#editOrdersOrt', 'fieldZipCode': '#editOrdersPLZ'}], 1);
			});
			$('#editOrdersArtikelBezeichnung').keyup(function () {
				loadSuggestions('searchProductName', [{'triggerElement': '#editOrdersArtikelBezeichnung', 'fieldNumber': '#editOrdersArtikelNummer', 'fieldName': '#editOrdersArtikelBezeichnung', 'fieldID': '#editOrdersArtikelID', 'fieldCatID': '#editOrdersArtikelKategorieID'}], 1);
			});
			$('#editOrdersArtikelNummer').keyup(function () {
				loadSuggestions('searchProductNumber', [{'triggerElement': '#editOrdersArtikelNummer', 'fieldNumber': '#editOrdersArtikelNummer', 'fieldName': '#editOrdersArtikelBezeichnung', 'fieldID': '#editOrdersArtikelID', 'fieldCatID': '#editOrdersArtikelKategorieID'}], 1);
			});
			$('#editOrdersPLZ').keyup(function () {
				loadSuggestions('searchPlzCity', [{'triggerElement': '#editOrdersPLZ', 'fieldCity': '#editOrdersOrt', 'fieldZipCode': '#editOrdersPLZ'}], 1);
			});
			$('#editOrdersOrt').keyup(function () {
				loadSuggestions('searchCityPlz', [{'triggerElement': '#editOrdersOrt', 'fieldCity': '#editOrdersOrt', 'fieldZipCode': '#editOrdersPLZ'}], 1);
			});
			$('#editOrdersArtikelMenge').keyup(function () {
				getProductPrices('#editOrdersArtikelMenge', '#editOrdersSinglePreis', '#editOrdersTotalPreis', '#editOrdersArtikelID');
			});
			setFormDutyFields(<?php echo $jsonFormDutyFields; ?>);

			$('#editOrdersPaymentType').change(function() {
				selectOtherOption('#' + $(this).attr('id'), '#editOrdersBankAccountType');
			});
			$('#editOrdersArtikelKategorieID').change(function() {
				gewicht = $('#editOrdersArtikelKategorieID').find(':selected').data("gewicht");
				$('#editOrdersArtikelGewicht').val(gewicht);
			});

			jQuery.fn.gewicht_calculator = function(param) {
				gesamGgewicht = $('#editOrdersArtikelKategorieID').find(':selected').data("gewicht")*$('#editOrdersArtikelMenge').val();
				$('#editOrdersArtikelGesamtGewicht').val(gesamGgewicht);
				gesamGgewichtKg = (gesamGgewicht/1000).toFixed(2);
				Verpackungseinheiten =  Math.ceil((gesamGgewichtKg / 20)) ;
				reste = (gesamGgewichtKg - (Verpackungseinheiten*20)).toFixed(2);
				$('#editOrdersArtikelGesamtGewichtK').val(gesamGgewichtKg);
				$('#editOrdersVerpackungseinheiten').val(Verpackungseinheiten);
			}
			$('#editOrdersArtikelMenge').keyup(function() {
				$(document).gewicht_calculator();
			});
			

			$('#editOrdersVertreterName').keyup(function() {
				loadSuggestions('searchSalesmen', [{'triggerElement': '#editOrdersVertreterName', 'fieldName': '#editOrdersVertreterName', 'fieldID': '#editOrdersVertreterID'}], 1);
			});
			// $('#editOrdersArtikelKategorieID').change(function() {
			$('#editOrdersAdditionalArtikelMenge').keyup(function() {
				selectRelatedArtikel('#editOrdersArtikelKategorieID', '#editOrdersAdditionalArtikelKategorieID', '#editOrdersAdditionalArtikelMenge');
			});
			$('#editOrdersArtikelPrintColorsCount').change(function(){
				var countColors = $(this).val();
				$('.checkPrintColors').removeAttr('checked');
				if(countColors == 0){
					// $('#editOrdersAdditionalCostsID').val(10);
					$('#editOrdersAdditionalCostsID').val(1);
					$('#printColorID_14').attr('checked', 'true');
				}
				else {
					$('#editOrdersAdditionalCostsID').val(7);
					$('#printColorID_12').attr('checked', 'true');
				}
			});

			$('.checkPrintColors').click(function() {
				var thisChecked = $(this).attr('checked');
				if(thisChecked == 'checked'){
					$(this).parent().parent().css('background-color', '#CBFFBF');
					$(this).parent().parent().css('font-weight', 'bold');
					$(this).parent().parent().find('select').css('font-weight', 'bold');
					$(this).parent().parent().find('input').css('font-weight', 'bold');
				}
				else {
					$(this).parent().parent().css('background-color', '');
					$(this).parent().parent().css('font-weight', '');
					$(this).parent().parent().find('select').css('font-weight', '');
					$(this).parent().parent().find('input').css('font-weight', '');
				}
			});

			<?php if($_REQUEST["editID"] != "") { ?>			
			getOrderProductCategoriesNeu('<?php echo $thisCusomerGroupID; ?>', 'all', '#editOrdersMandant', '#editOrdersArtikelKategorieID', '<?php echo $orderDatas["ordersArtikelKategorieID"]; ?>');
			// getOrderProductCategoriesNeu('<?php echo $thisCusomerGroupID; ?>', '002', '#editOrdersMandant', '#editOrdersAdditionalArtikelKategorieID', '<?php echo $orderDatas["ordersAdditionalArtikelKategorieID"]; ?>');

			<?php } ?>

			// $( ".datepicker" ).datepicker({ maxDate: '0' });
			$.datepicker.setDefaults($.datepicker.regional[""]);
			$('#editOrdersBestellDatum').datepicker($.datepicker.regional["de"]);
			$('#editOrdersBestellDatum').datepicker("option", "maxDate", "0" );
			$('#editOrdersStornoDatum').datepicker($.datepicker.regional["de"]);
			$('#editOrdersStornoDatum').datepicker("option", "maxDate",  );
			$('#editOrdersFreigabeDatum').datepicker($.datepicker.regional["de"]);
			$('#editOrdersFreigabeDatum').datepicker("option", "maxDate",  );
			$('#editOrdersBelichtungsDatum').datepicker($.datepicker.regional["de"]);
			$('#editOrdersBelichtungsDatum').datepicker("option", "maxDate",  );
			$('#editOrdersProduktionsDatum').datepicker($.datepicker.regional["de"]);
			$('#editOrdersProduktionsDatum').datepicker("option", "maxDate",  );
			$('#editOrdersAuslieferungsDatum').datepicker($.datepicker.regional["de"]);
			$('#editOrdersAuslieferungsDatum').datepicker("option", "maxDate",  );
			$('#editOrdersLieferDatum').datepicker($.datepicker.regional["de"]);
			$('#editOrdersLieferDatum').datepicker("option", "maxDate",  );
			$('#materialOrderdBySupplierOrderDate').datepicker($.datepicker.regional["de"]);
			$('#materialOrderdBySupplierOrderDate').datepicker("option", "maxDate",  );
			$('#materialOrderdBySupplierDeliveryDate').datepicker($.datepicker.regional["de"]);
			$('#materialOrderdBySupplierDeliveryDate').datepicker("option", "maxDate",  );

			var htmlButtonClearField = '<span class="buttonClearField"><img src="layout/icons/iconDelete.png" width="16" height="16" alt="" title="Eingabefeld leeren" \/><\/span>';
			$('#editOrdersStornoDatum').parent().append(htmlButtonClearField);
			$('#editOrdersFreigabeDatum').parent().append(htmlButtonClearField);
			$('#editOrdersBelichtungsDatum').parent().append(htmlButtonClearField);
			$('#editOrdersProduktionsDatum').parent().append(htmlButtonClearField);
			$('#editOrdersAuslieferungsDatum').parent().append(htmlButtonClearField);
			$('#editOrdersLieferDatum').parent().append(htmlButtonClearField);
			$('#materialOrderdBySupplierOrderDate').parent().append(htmlButtonClearField);
			$('#materialOrderdBySupplierDeliveryDate').parent().append(htmlButtonClearField);
			$('#editOrdersDruckFarbe').parent().parent().append(htmlButtonClearField);
			$('.buttonClearField').click(function () {
				$(this).parent().find('input').val('');
				$(this).parent().find('input').attr('checked', false);
			});

			$('.buttonRemoveSalesman').click(function () {
				$('#editOrdersVertreterName').val('');
				$('#editOrdersVertreterID').val('');
				$('#editOrdersMailAdress').val('');
				$('#editOrdersContactOthers').val('');
			});

			// BOF UNBEDRUCKT
				$('#printColorID_14').click(function(){
					if($(this).attr('checked') == "checked"){
						$('#editOrdersAdditionalCostsID').val('1');
					}
					else {
						$('#editOrdersAdditionalCostsID').val('');
					}
				});
			// EOF UNBEDRUCKT

			// BOF DIGITALDRUCK
				$('#printColorID_13').click(function(){
					if($(this).attr('checked') == "checked"){
						$('#editOrdersAdditionalCostsID').val('6');
					}
					else {
						$('#editOrdersAdditionalCostsID').val('');
					}
				});
			// EOF DIGITALDRUCK

			// BOF DIGITALDRUCK
				$('#printColorID_18').click(function(){
					if($(this).attr('checked') == "checked"){
						$('#editOrdersAdditionalCostsID').val('5');
					}
					else {
						$('#editOrdersAdditionalCostsID').val('');
					}
				});
			// EOF DIGITALDRUCK
		});
	});
	/* ]]> */
	// -->

	/* <![CDATA[ */
	/*
	$('#editOrdersArtikelKategorieID').select2({
		formatResult: function(object) {
			var $el = $(object.element),
				product = $el.text(),
				category = $el.attr('data-category');

			return '<b>' + product + '<\/b>' + ' <i>' + category + '<\/i>';
		}
	});
	*/
	/* ]]> */

</script>

<?php require_once('inc/footerHTML.inc.php'); ?>