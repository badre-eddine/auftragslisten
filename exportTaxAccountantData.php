<?php
	require_once('inc/requires.inc.php');

	if(!$arrGetUserRights["editFinancialDatas"]) {
		header('location: ' . PAGE_EXIT_LOCATION);
		exit;
	}

	$exportYearStart = 14; // 2015

	$jswindowMessage = '';
	$warningMessage = '';
	$errorMessage = '';
	$successMessage = '';
	$infoMessage = '';

	$monthInterval = 2; // 1: last month | 2: next to last month .....

	function convertCharsForDATEV($string){
		$contentExportCSV = $string;

		$getEncodingContentExportCSV = mb_detect_encoding($contentExportCSV);

		$sourceEncoding = 'UTF-8';
		$targetEncoding = 'ISO-8859-1'; // ISO-8859-1 | ASCII | CP437
		$targetEncoding = 'ASCII'; // ISO-8859-1 | ASCII | CP437

		if($getEncodingContentExportCSV == $sourceEncoding){
			#$contentExportCSV = iconv($sourceEncoding, $targetEncoding, $contentExportCSV);
			## $contentExportCSV = mb_convert_encoding($contentExportCSV, $targetEncoding, $sourceEncoding);
			$contentExportCSV = utf8_decode($contentExportCSV);
		}
		$getEncodingContentExportCSV = mb_detect_encoding($contentExportCSV);

		return $contentExportCSV;
	}

	if($_GET["downloadFile"] != "") {
		require_once('classes/downloadFile.class.php');
		if($_GET["downloadType"] == "EX") {
			$thisDownloadDir = PATH_EXPORT_FILES_TAX_ACCOUNTANT;
		}
		else if($_GET["downloadType"] == "RE" || $_GET["downloadType"] == "GU") {
			$thisDownloadDir = DIRECTORY_CREATED_DOCUMENTS_CUSTOMERS;
		}
		$thisDownload = new downloadFile($thisDownloadDir, $_GET["downloadFile"]);
		$errorMessage .= $thisDownload->startDownload();
	}

	$dbConnection = new DB_Connection();
	$db_open = $dbConnection->db_connect();

	// BOF SEND MAIL WITH ATTACHED DOCUMENT
		if($_POST["sendAttachedDocument"] == '1') {
			$thisCreatedPdfName = $_POST["mailDocumentFilename"];
			// $_POST["selectSubject"] = $arrDocumentTypeDatas[$_REQUEST["documentType"]]["createdDocumentsTypesName"] . ' Nr.: ' . '{###DOCUMENT_NUMBER###}';
			$_POST["selectSubject"] = 'Datenexport: ' . $thisCreatedPdfName;
			$generatedDocumentNumber = $_POST["mailDocumentFilename"];

			require_once(DIRECTORY_HTML_MIME_MAIL . 'htmlMimeMail.php');
			require_once(DIRECTORY_MAIL_TEMPLATES . 'template_BASIC.inc.php');
			require_once(DIRECTORY_MAIL_TEMPLATES . 'template_TEXT_DATAS.inc.php');
			## require_once("inc/mail.inc.php");

			// BOF SEND MAIL
				require_once("classes/createMail.class.php");
				// BOF CREATE SUBJECT
				$thisSubject = $_POST["selectSubject"];
				if($_POST["sendAttachedMailSubject"] != '') {
					$thisSubject = $_POST["sendAttachedMailSubject"] . ', ' . $thisSubject;
				}
			// EOF CREATE SUBJECT

			// BOF GET ATTACHED FILE
				if($_REQUEST["documentType"] == 'KA') {
					$pathDocumentFolder = DIRECTORY_UPLOAD_FILES;
					$thisCreatedPdfName = utf8_decode($thisCreatedPdfName);
				}
				else if($_REQUEST["documentType"] == 'EX') {
					$pathDocumentFolder = PATH_EXPORT_FILES_TAX_ACCOUNTANT;
					$thisCreatedPdfName = utf8_decode($thisCreatedPdfName);
				}
				else if($_REQUEST["documentType"] == 'SX') {
					$pathDocumentFolder = DIRECTORY_CREATED_DOCUMENTS_SALESMEN;
					$thisCreatedPdfName = utf8_decode($thisCreatedPdfName);
				}
				else {
					$pathDocumentFolder = DIRECTORY_CREATED_DOCUMENTS_CUSTOMERS;
				}

				$arrAttachmentFiles = array (
					rawurldecode($pathDocumentFolder . $thisCreatedPdfName) => str_replace(" ", "_", rawurldecode(myUnHtmlEntities($thisCreatedPdfName)))
				);
			// EOF GET ATTACHED FILE

			// BOF SEND MAIL
				$createMail = new createMail(
									$thisSubject,													// TEXT:	MAIL_SUBJECT
									$_POST["documentType"],											// STRING:	DOCUMENT TYPE
									$generatedDocumentNumber,										// STRING:	DOCUMENT NUMBER
									$arrAttachmentFiles,											// ARRAY:	ATTACHMENT FILES
									$_POST["selectCustomersRecipientMail"],	 										// STRING:	RECIPIENTS
									$arrMailContentDatas[$_POST["selectMailtextTemplates"]],		// MAIL_TEXT_TEMPLATE
									$mailTemplate["html"],											// MAIL_BODY_TEMPLATE
									'',																// STRING:	ADDITIONAL TEXT
									$_POST["sendAttachedMailText"],									// STRING:	SEND ATTACHED TEXT
									true,															// BOOLEAN:	SEND ATTACHED DOCUMENT
									DIRECTORY_MAIL_IMAGES,											// DIRECTORY_MAIL_IMAGES
									$_POST["selectMailtextSender"]									// STRING: SENDER
								);

				$createMailResult = $createMail->returnResult();
				$sendMailToClient = $createMailResult;
			// EOF SEND MAIL

			#if(!$sendMailToUs) { $errorMessage .= ' Die Mail-Kopie konnte nicht an ' . implode(', ', $arrOrderMail) . ' versendet werden! '.'<br />'; }
			#else { $successMessage .= ' Die Mail-Kopie wurde an ' . implode(', ', $arrOrderMail) . ' versendet! '.'<br />'; }

			if(!$sendMailToClient) { $errorMessage .= ' Die Mail konnte nicht an ' . $_POST["selectCustomersRecipientName"] . ' (' . $_POST["selectCustomersRecipientMail"] . ') versendet werden! '.'<br />'; }
			else {
				$successMessage .= ' Die Mail wurde an ' . $_POST["selectCustomersRecipientName"] . ' (' . $_POST["selectCustomersRecipientMail"] . ') versendet! '.'<br />';
			}
		}
	// EOF SEND MAIL WITH ATTACHED DOCUMENT



	$defaultORDER = "ordersBestellDatum";
	$defaultSORT = "ASC";

	DEFINE('ACCOUNT_HOME_COUNTRY', '8400');
	DEFINE('ACCOUNT_EU_COUNTRY', '8125');
	DEFINE('ACCOUNT_FOREIGN_COUNTRY', '8120');

	// BOF READ VERTRETER
		$arrAgentDatas = getAgents();
		$arrAgentDatas2 = getAgents2();
	// EOF READ VERTRETER

	// BOF READ BESTELL ARTEN
		$arrOrderTypeDatas = getOrderTypes();
	// EOF READ BESTELL ARTEN

	// BOF READ BESTELL STATUS TYPEN
		$arrOrderStatusTypeDatas = getOrderStatusTypes();
	// EOF READ BESTELL STATUS TYPEN

	// BOF READ DOCUMENT TYPE DATAS
		$arrPaymentStatusTypeDatas = getPaymentStatusTypes();
	// EOF READ DOCUMENT TYPE DATAS

	// BOF READ COUNTRIES
		$arrCountryTypeDatas = getCountryTypes();
	// EOF READ COUNTRIES
?>
<?php
	require_once('inc/headerHTML.inc.php');
	$thisTitle = "DATEV- /Steuerberater-Daten exportieren";

	$headerHTML = preg_replace("/{###TITLE###}/", strip_tags($thisTitle), $headerHTML);
	echo $headerHTML;

	require_once(FILE_MENUE_TOP);

	// BOF GET LAST EXPORT DATA
		$arrLastExportData = array();
		$sql = "
				SELECT
					`FILE_NAME` AS `MAX_FILE_NAME`,
					`FILE_EXPORT_DATE` AS `MAX_FILE_EXPORT_DATE`,
					`FILE_DATE_FROM` AS `MAX_FILE_DATE_FROM`,
					`FILE_DATE_TO` AS `MAX_FILE_DATE_TO`
				FROM `" . TABLE_EXPORT_DATEV_DATA . "`
				WHERE 1
				ORDER BY `MAX_FILE_DATE_TO` DESC
				LIMIT 1
			";

		$rs = $dbConnection->db_query($sql);

		while($ds = mysqli_fetch_assoc($rs )){
			foreach(array_keys($ds) as $field){
				$arrLastExportData[$field] = $ds[$field];
			}
		}
	// EOF GET LAST EXPORT DATA

	$sqlSortField = "";
	$sqlSortDirection = "";
	$sqlWhere = "";

	// BOF GET DOCUMENTS
		if($_POST["submitExportForm"] == 1 && $_POST["exportDateStart"] != ""  && $_POST["exportDateEnd"] != "") {
			$thisCreatedCsvName = convertChars('Buchungsliste') . '_' . MANDATOR . '_' . $_POST["exportDateStart"] . '_' . $_POST["exportDateEnd"] . '.csv';

			// BOF CREATE SQL FOR EXPORT INVOICES
			$sql = "SELECT
						`" . TABLE_ORDER_INVOICES . "`.`orderDocumentsNumber`,
						`" . TABLE_ORDER_INVOICES . "`.`orderDocumentsType`,
						`" . TABLE_ORDER_INVOICES . "`.`orderDocumentsCustomerNumber`,
						`" . TABLE_ORDER_INVOICES . "`.`orderDocumentsProcessingDate`,
						`" . TABLE_ORDER_INVOICES . "`.`orderDocumentsDocumentDate`,
						`" . TABLE_ORDER_INVOICES . "`.`orderDocumentsOrderDate`,
						`" . TABLE_ORDER_INVOICES . "`.`orderDocumentsAddressCountry`,
						`" . TABLE_ORDER_INVOICES . "`.`orderDocumentsTotalPrice`,
						`" . TABLE_ORDER_INVOICES . "`.`orderDocumentsBankAccount`,
						`" . TABLE_ORDER_INVOICES . "`.`orderDocumentsPaymentCondition`,
						`" . TABLE_ORDER_INVOICES . "`.`orderDocumentsPaymentType`,
						`" . TABLE_ORDER_INVOICES . "`.`orderDocumentsSkonto`,
						`" . TABLE_ORDER_INVOICES . "`.`orderDocumentsTimestamp`,
						`" . TABLE_ORDER_INVOICES . "`.`orderDocumentsStatus`,

						`customers`.`customersKundennummer`,
						`customers`.`customersTaxAccountID`,
						`customers`.`customersFirmenname`,
						`customers`.`customersUstID`,
						`customers`.`customersCompanyCountry`,

						`recipient`.`customersKundennummer` AS `recipientKundennummer`,
						`recipient`.`customersTaxAccountID` AS `recipientTaxAccountID`,
						`recipient`.`customersFirmenname` AS `recipientFirmenname`,
						`recipient`.`customersUstID` AS `recipientUstID`,
						`recipient`.`customersCompanyCountry` AS `recipientCompanyCountry`,
						
						IF(`" . TABLE_RELATED_DOCUMENTS . "`.`relatedDocuments_AB` IS NULL, '', `" . TABLE_RELATED_DOCUMENTS . "`.`relatedDocuments_AB`) AS `relatedDocumentsNumber`

						FROM `" . TABLE_ORDER_INVOICES . "`
						
						LEFT JOIN `" . TABLE_RELATED_DOCUMENTS . "`
						ON(`" . TABLE_ORDER_INVOICES . "`.`orderDocumentsNumber` = `" . TABLE_RELATED_DOCUMENTS . "`.`relatedDocuments_RE`)

						LEFT JOIN `" . TABLE_CUSTOMERS . "` AS `customers`
						ON(`" . TABLE_ORDER_INVOICES . "`.`orderDocumentsCustomerNumber` = `customers`.`customersKundennummer`)

						LEFT JOIN `" . TABLE_CUSTOMERS . "` AS `recipient`
						ON(`" . TABLE_ORDER_INVOICES . "`.`orderDocumentsAddressCompanyCustomerNumber` = `recipient`.`customersKundennummer`)

						WHERE `" . TABLE_ORDER_INVOICES . "`.`orderDocumentsDocumentDate`
							BETWEEN '" . formatDate($_POST["exportDateStart"], "store") . "' AND '" . formatDate($_POST["exportDateEnd"], "store")."'

						UNION

						SELECT
						`" . TABLE_ORDER_CREDITS . "`.`orderDocumentsNumber`,
						`" . TABLE_ORDER_CREDITS . "`.`orderDocumentsType`,
						`" . TABLE_ORDER_CREDITS . "`.`orderDocumentsCustomerNumber`,
						`" . TABLE_ORDER_CREDITS . "`.`orderDocumentsProcessingDate`,
						`" . TABLE_ORDER_CREDITS . "`.`orderDocumentsDocumentDate`,
						`" . TABLE_ORDER_CREDITS . "`.`orderDocumentsOrderDate`,
						`" . TABLE_ORDER_CREDITS . "`.`orderDocumentsAddressCountry`,
						(`" . TABLE_ORDER_CREDITS . "`.`orderDocumentsTotalPrice` * '-1') AS `orderDocumentsTotalPrice`,
						`" . TABLE_ORDER_CREDITS . "`.`orderDocumentsBankAccount`,
						`" . TABLE_ORDER_CREDITS . "`.`orderDocumentsPaymentCondition`,
						`" . TABLE_ORDER_CREDITS . "`.`orderDocumentsPaymentType`,
						`" . TABLE_ORDER_CREDITS . "`.`orderDocumentsSkonto`,
						`" . TABLE_ORDER_CREDITS . "`.`orderDocumentsTimestamp`,
						`" . TABLE_ORDER_CREDITS . "`.`orderDocumentsStatus`,

						`customers`.`customersKundennummer`,
						`customers`.`customersTaxAccountID`,
						`customers`.`customersFirmenname`,
						`customers`.`customersUstID`,
						`customers`.`customersCompanyCountry`,

						`recipient`.`customersKundennummer` AS `recipientKundennummer`,
						`recipient`.`customersTaxAccountID` AS `recipientTaxAccountID`,
						`recipient`.`customersFirmenname` AS `recipientFirmenname`,
						`recipient`.`customersUstID` AS `recipientUstID`,
						`recipient`.`customersCompanyCountry` AS `recipientCompanyCountry`,
						
						IF(`" . TABLE_RELATED_DOCUMENTS . "`.`relatedDocuments_RE` IS NULL, '', `" . TABLE_RELATED_DOCUMENTS . "`.`relatedDocuments_RE`) AS `relatedDocumentsNumber`

						FROM `" . TABLE_ORDER_CREDITS . "`
						
						LEFT JOIN `" . TABLE_RELATED_DOCUMENTS . "`
						ON(`" . TABLE_ORDER_CREDITS . "`.`orderDocumentsNumber` = `" . TABLE_RELATED_DOCUMENTS . "`.`relatedDocuments_GU`)

						LEFT JOIN `" . TABLE_CUSTOMERS . "` AS `customers`
						ON(`" . TABLE_ORDER_CREDITS . "`.`orderDocumentsCustomerNumber` = `customers`.`customersKundennummer`)

						LEFT JOIN `" . TABLE_CUSTOMERS . "` AS `recipient`
						ON(`" . TABLE_ORDER_CREDITS . "`.`orderDocumentsAddressCompanyCustomerNumber` = `recipient`.`customersKundennummer`)

						WHERE `" . TABLE_ORDER_CREDITS . "`.`orderDocumentsDocumentDate`
							BETWEEN '" . formatDate($_POST["exportDateStart"], "store") . "' AND '" . formatDate($_POST["exportDateEnd"], "store")."'


						ORDER BY `orderDocumentsDocumentDate` ASC, `orderDocumentsNumber` ASC
				";
				// EOF CREATE SQL FOR EXPORT INVOICES
	#dd('sql');
				$rs = $dbConnection->db_query($sql);

				$countTotalRows = $dbConnection->db_getMysqlNumRows($rs);
				$infoMessage .= 'Es wurden ' . $countTotalRows . ' Datens&auml;tze aus der Datenbank ausgelesen. ' . '<br />';
				$contentExportPDF = '';
				$contentExportCSV = '';

				if($countTotalRows > 0) {

				$contentExportPDF .= '<table width="100%" border="1">';

				$contentExportPDF .= '<tr>';
				$contentExportPDF .= '<th style="width:30pt">BETRAG</th>';
				$contentExportPDF .= '<th style="width:80pt">ERLOESKONTO</th>';
				$contentExportPDF .= '<th style="width:80pt">BELEGNR</th>';
				$contentExportPDF .= '<th style="width:80pt">BELEGNR2</th>';
				$contentExportPDF .= '<th style="width:250pt">BELEGDATUM</th>';
				$contentExportPDF .= '<th style="width:40pt">FIBUKONTO</th>';
				$contentExportPDF .= '<th style="width:70pt">KNR</th>';
				$contentExportPDF .= '<th style="width:40pt">KUNDE</th>';
				$contentExportPDF .= '<th style="width:80pt">KUNDE_KNR</th>';
				$contentExportPDF .= '<th style="width:80pt">KNR_KUNDE</th>';
				$contentExportPDF .= '<th style="width:80pt">EU_ID</th>';
				$contentExportPDF .= '</tr>';

				$countRow = 0;
				while($ds = mysqli_fetch_assoc($rs)) {
					$contentExportRow = '';
					if($countRow == 0) {

					}

					// BOF CANGE CHARSET DS
						if(!empty($ds)){
							foreach($ds as $dsKey => $dsValue){
								$ds[$dsKey] = convertCharsForDATEV($dsValue);
							}
						}
					// EOF CANGE CHARSET DS

					$thisBackgroundColor = '#FFF';
					if($countRow%2 == 0) {
						$thisBackgroundColor = '#EEE';
					}

					$contentExportRow .= '<tr>';

					$contentExportRow .= '<td>';
					$contentExportRow .= number_format($ds["orderDocumentsTotalPrice"], 2, ',', '');
					$contentExportRow .= '</td>';
					$contentExportRow .= '<td>';
					if($ds["customersCompanyCountry"] == '81'){
						$thisAccount = ACCOUNT_HOME_COUNTRY;
					}
					else {
						// if($arrCountryTypeDatas[$ds["customersCompanyCountry"]]['isEU'] == '1'){
						if($arrCountryTypeDatas[$ds["recipientCompanyCountry"]]['isEU'] == '1'){
							$thisAccount = ACCOUNT_EU_COUNTRY;
						}
						else {
							$thisAccount = ACCOUNT_FOREIGN_COUNTRY;
						}
					}
					$contentExportRow .= $thisAccount;
					$contentExportRow .= '</td>';

					$contentExportRow .= '<td>';
					$contentExportRow .= substr($ds["orderDocumentsNumber"], 3);
					$contentExportRow .= '</td>';
					
					$contentExportRow .= '<td>';
					$contentExportRow .= str_replace("-", "", $ds["relatedDocumentsNumber"]);
					$contentExportRow .= '</td>';

					$contentExportRow .= '<td>';
					$contentExportRow .= substr(formatDate($ds["orderDocumentsDocumentDate"], 'display'), 0, 5);
					$contentExportRow .= '</td>';

					$contentExportRow .= '<td>';
					#$contentExportRow .= $ds["customersTaxAccountID"];
					$contentExportRow .= $ds["recipientTaxAccountID"];
					$contentExportRow .= '</td>';

					$contentExportRow .= '<td>';
					#$contentExportRow .= $ds["customersKundennummer"];
					$contentExportRow .= $ds["recipientKundennummer"];
					$contentExportRow .= '</td>';

					$contentExportRow .= '<td>';
					#$contentExportRow .= $ds["customersFirmenname"];
					$contentExportRow .= $ds["recipientFirmenname"];
					$contentExportRow .= '</td>';

					$contentExportRow .= '<td>';
					$thisFieldMaxLength = 60;
					$thisFieldContent = '';
					#$thisFieldKundennummer = ' [' . $ds["customersKundennummer"] . ']';
					$thisFieldKundennummer = ' [' . $ds["recipientKundennummer"] . ']';
					$thisFieldKundennummerLength = strlen($thisFieldKundennummer);
					#$thisFieldFirmenname = $ds["customersFirmenname"];
					$thisFieldFirmenname = $ds["recipientFirmenname"];

					$thisFieldContent = substr($thisFieldFirmenname, 0, ($thisFieldMaxLength - strlen($thisFieldKundennummer))) . $thisFieldKundennummer;
					#if($ds["customersKundennummer"] == '47332'){
						#dd('thisFieldMaxLength');
						#dd('thisFieldFirmenname');
						#dd('thisFieldKundennummer');
						#dd('thisFieldKundennummerLength');
						#dd('thisFieldContent');
					#}
					$contentExportRow .= $thisFieldContent;
					$contentExportRow .= '</td>';

					$contentExportRow .= '<td>';
					#$contentExportRow .= $ds["customersKundennummer"] . ' - ' . $ds["customersFirmenname"] . '';
					$contentExportRow .= $ds["recipientKundennummer"] . ' - ' . $ds["recipientFirmenname"] . '';
					$contentExportRow .= '</td>';

					$contentExportRow .= '<td>';
					#if($arrCountryTypeDatas[$ds["customersCompanyCountry"]]['isEU'] == '1' && $ds["customersCompanyCountry"] != '81'){
						#$contentExportRow .= $ds["customersUstID"];
					#}
					if($arrCountryTypeDatas[$ds["recipientCompanyCountry"]]['isEU'] == '1' && $ds["recipientCompanyCountry"] != '81'){
						$contentExportRow .= $ds["recipientUstID"];
					}
					$contentExportRow .= '</td>';

					$contentExportRow .= '</tr>';

					$contentExportPDF .= $contentExportRow;
					# dd('contentExportRow');
					$countRow++;

					// BOF STORE INTO DB

						// BOF CREATE MD5-KEY
								/*
								$sql_createMD5 = "
									UPDATE
										`" . TABLE_EXPORT_DATEV_DATA . "`
										SET `DATA_MD5` = MD5(CONCAT(`BETRAG`, `ERLOESKONTO`, `BELEGNR`, `BELEGDATUM`, `FIBUKONTO`, `KNR`))

										WHERE 1
											AND `DATA_MD5` = ''
									";
									#$rs_createMD5 = $dbConnection->db_query($sql_createMD5);
								*/
							$md5 = '';
							$md5 .= $ds["orderDocumentsTotalPrice"];
							$md5 .= $thisAccount;
							$md5 .= substr($ds["orderDocumentsNumber"], 3);
							$md5 .= substr(formatDate($ds["orderDocumentsDocumentDate"], 'display'), 0, 5);
							$md5 .= addslashes($ds["recipientTaxAccountID"]);
							$md5 .= addslashes($ds["recipientKundennummer"]);
							$md5 = md5($md5);
						// EOF CREATE MD5-KEY


						$sql_insertIntoDB = "
								INSERT INTO `" . TABLE_EXPORT_DATEV_DATA . "` (
									`BETRAG`,
									`ERLOESKONTO`,
									`BELEGNR`,
									`BELEGDATUM`,
									`FIBUKONTO`,
									`KNR`,
									`KUNDE`,
									`KUNDE_KNR`,
									`KNR_KUNDE`,
									`EU_ID`,

									`DOC_TYPE`,
									`DOC_NR`,
									`FILE_NAME`,
									`FILE_EXPORT_DATE`,
									`FILE_DATE_FROM`,
									`FILE_DATE_TO`,

									`DATA_MD5`
								)
								VALUES (
									'" . $ds["orderDocumentsTotalPrice"] . "',
									'" . $thisAccount . "',
									'" . substr($ds["orderDocumentsNumber"], 3) . "',
									'" . substr(formatDate($ds["orderDocumentsDocumentDate"], 'display'), 0, 5) . "',
									'" . addslashes($ds["recipientTaxAccountID"]) . "',
									'" . addslashes($ds["recipientKundennummer"]) . "',
									'" . addslashes($ds["recipientFirmenname"]) . "',
									'" . addslashes($thisFieldContent) . "',
									'" . addslashes($ds["recipientKundennummer"] . ' - ' . $ds["recipientFirmenname"]) . "',
									'" . $ds["recipientUstID"] . "',

									'" . substr($ds["orderDocumentsNumber"], 0, 2) . "',
									'" . $ds["orderDocumentsNumber"] . "',

									'" . addslashes($thisCreatedCsvName) . "',
									NOW(),
									'" . formatDate($_POST["exportDateStart"], "store") . "',
									'" . formatDate($_POST["exportDateEnd"], "store") . "',

									'" . $md5 . "'
								)
							";
							#dd('sql_insertIntoDB');
							$rs_insertIntoDB = $dbConnection->db_query($sql_insertIntoDB);
					// EOF STORE INTO DB
				}
				$contentExportPDF .= '</table>';

				$infoMessage .= 'Die erzeugte Datei enth&auml;lt ' . $countRow . ' Datens&auml;tze. ' . '<br />';

				if($countTotalRows > $countRow){
					$errorMessage .= 'Achtung: Die erzeugte Datei enth&auml;lt ' . ($countTotalRows - $countRow) . ' weniger Datens&auml;tze als aus der Datenbank ausgelesen wurden. ' . '<br />';
				}
				else {
					$successMessage .= 'Die Anzahl der Daten in der erzeugten Datei und die der aus der Datenbank ausgelesen Zeilen stimmt &uuml;berein. ' . '<br />';
				}

				$pdfContentPage = $contentExportPDF;

				//echo $pdfContentPage;
				//exit;

				$pdfContentPage = removeUnnecessaryChars($pdfContentPage);
				$contentExportCSV = formatCSV($contentExportPDF, ';');

				// BOF CANGE CHARSET COMPLETE DATA
					// $contentExportCSV = convertCharsForDATEV($contentExportCSV);
				// EOF CANGE CHARSET COMPLETE DATA

				// BOF STORE CSV
				#$thisCreatedCsvName = convertChars('Buchungsliste') . '_' . MANDATOR . '_' . $_POST["exportDateStart"] . '_' . $_POST["exportDateEnd"] . '.csv';
				#echo 'thisCreatedCsvName: ' . $thisCreatedCsvName . '<br>';
				# echo $contentExportPDF;
				#echo '<pre>';
				#echo $contentExportCSV;
				#echo '</pre>';
				#exit;
				if(file_exists(PATH_EXPORT_FILES_TAX_ACCOUNTANT . $thisCreatedCsvName)) {
					// chmod(PATH_EXPORT_FILES_TAX_ACCOUNTANT . $thisCreatedCsvName , "0777");
					unlink(PATH_EXPORT_FILES_TAX_ACCOUNTANT . $thisCreatedCsvName);
				}
				clearstatcache();
				$fp = fopen(PATH_EXPORT_FILES_TAX_ACCOUNTANT . $thisCreatedCsvName, 'w');
				fwrite($fp, stripslashes($contentExportCSV));
				fclose($fp);
				// EOF STORE CSV

				$_POST["selectTemplate"] = "";

				if(file_exists(PATH_EXPORT_FILES_TAX_ACCOUNTANT . $thisCreatedCsvName)) {
					$successMessage .= 'Die CSV-Datei &quot;' . $thisCreatedCsvName . '&quot wurde generiert. ' . '<br />';
					$showPdfDownloadLink = true;
				}
				else {
					$errorMessage .= 'Die CSV-Datei &quot;' . $thisCreatedCsvName . '&quot konnte nicht generiert werden. ' . '<br />';
				}
				clearstatcache();
			// EOF createPDF
			}
			else {
				$errorMessage .= 'Es liegen keine Datens&auml;tze vor, die exportiert werden k&ouml;nnen. ' . '<br />';
			}
		}
	// EOF GET DOCUMENTS

	// BOF GET CUSTOMERS
		if($_POST["submitExportCustomers"] != ''){

			// DATEV: STRASSE: 41
			// FIRMENNAME: 50
			// $thisCreatedCsvNameCustomers = convertChars('kunden-export') . '_' . MANDATOR . '_' . $_POST["exportDateStart"] . '_' . $_POST["exportDateEnd"] . '.csv';
			$thisCreatedCsvNameCustomers = convertChars('kunden-export') . '.csv';

			if(file_exists(PATH_EXPORT_FILES_TAX_ACCOUNTANT . $thisCreatedCsvNameCustomers)) {
				// chmod(PATH_EXPORT_FILES_TAX_ACCOUNTANT . $thisCreatedCsvNameCustomers , "0777");
				unlink(PATH_EXPORT_FILES_TAX_ACCOUNTANT . $thisCreatedCsvNameCustomers);
			}

			$sql_customers = "
					SELECT
						TRIM(`" . TABLE_CUSTOMERS . "`.`customersKundennummer`) AS `Auftragslisten_KNR`,
						TRIM(`" . TABLE_CUSTOMERS . "`.`customersTaxAccountID`) AS `Auftragslisten_FIBU`,
						SUBSTRING(
							CONCAT(
								TRIM(`" . TABLE_CUSTOMERS . "`.`customersFirmenname`),
								IF(
									TRIM(`" . TABLE_CUSTOMERS . "`.`customersFirmennameZusatz`) != '',
									CONCAT(' ',	TRIM(`" . TABLE_CUSTOMERS . "`.`customersFirmennameZusatz`)),
									'')
							),
							1, 50) AS `FIRMA`,
						CONCAT(
							SUBSTRING(TRIM(`" . TABLE_CUSTOMERS . "`.`customersCompanyStrasse`), 1, (41 - (1 + LENGTH(TRIM(`" . TABLE_CUSTOMERS . "`.`customersCompanyHausnummer`))))),
							' ',
							TRIM(`" . TABLE_CUSTOMERS . "`.`customersCompanyHausnummer`)
						) AS `STRASSE`,

						TRIM(`" . TABLE_CUSTOMERS . "`.`customersCompanyPLZ`) AS `PLZ`,
						TRIM(`" . TABLE_CUSTOMERS . "`.`customersCompanyOrt`) AS `ORT`,
						`" . TABLE_COUNTRIES . "`.`countries_name_DE` AS `LAND_NAME`,
						`" . TABLE_COUNTRIES . "`.`countries_iso_code_2` AS `LAND_ISO2`,
						`" . TABLE_COUNTRIES . "`.`countries_iso_code_3` AS `LAND_ISO3`,

						`" . TABLE_CUSTOMERS . "`.`customersUstID` AS `UST_ID`

					INTO OUTFILE '" . PATH_EXPORT_FILES_TAX_ACCOUNTANT . $thisCreatedCsvNameCustomers . "'
					CHARACTER SET 'latin1'
					FIELDS TERMINATED BY ';'
					LINES TERMINATED BY '\\n'

					FROM `" . TABLE_CUSTOMERS . "`
					LEFT JOIN `" . TABLE_COUNTRIES . "`
					ON(`" . TABLE_CUSTOMERS . "`.`customersCompanyCountry` = `" . TABLE_COUNTRIES . "`.`countries_id`)

					WHERE 1
						/* AND `" . TABLE_CUSTOMERS . "`.`customersKundennummer` = '41063'	*/
				";
				if($_POST["exportCustomersAll"] != '1' && $_POST["exportCustomersDateStart"] != '' && $_POST["exportCustomersDateEnd"] != ''){
					$sql_customers .= "	AND `" . TABLE_CUSTOMERS . "`.`customersEntryDate` BETWEEN '" . formatDate($_POST["exportCustomersDateStart"], "store") . "' AND '" . formatDate($_POST["exportCustomersDateEnd"], "store")."' ";
				}
				$sql_customers .= "
					ORDER BY `" . TABLE_CUSTOMERS . "`.`customersKundennummer`

				";
			$rs_customers = $dbConnection->db_query($sql_customers);

			// BOF CONVERT CHARSET
				/*
				$fileExportCustomersSource = PATH_EXPORT_FILES_TAX_ACCOUNTANT . $thisCreatedCsvNameCustomers;
				$fileExportCustomersTarget = PATH_EXPORT_FILES_TAX_ACCOUNTANT . '_new_' . $thisCreatedCsvNameCustomers;

				if(file_exists($fileExportCustomersTarget)){
					unlink($fileExportCustomersTarget);
				}

				if($rs_customers && file_exists($fileExportCustomersSource)) {
					$fp_exportCustomersSource = fopen($fileExportCustomersSource, 'r');
					$fp_exportCustomersTarget = fopen($fileExportCustomersTarget, 'w');
					dd('fp_exportCustomersSource');
					dd('fp_exportCustomersTarget');
					if($fp_exportCustomersSource){
						$countLine = 0;
						while(($line_exportCustomers = fgets($fp_exportCustomersSource)) !== false) {
							$line_exportCustomersNew = convertCharsForDATEV($line_exportCustomers);
							fwrite($fp_exportCustomersTarget, $line_exportCustomersNew);
							$countLine++;
						}

						fclose($fp_exportCustomersSource);
						fclose($fp_exportCustomersTarget);
					}
				}
				*/
			// EOF CONVERT CHARSET
		}

	// EOF GET CUSTOMERS
?>

<div id="mainArea">
	<div id="mainContent">
		<?php require_once(FILE_MENUE_SIDEBAR); ?>
		<div id="contentArea">
			<a name="top"></a>
			<div id="contentAreaElements">
				<h1><?php if(SHOW_PAGE_TITLE_ICONS) { echo '<img src="' . PATH_ICONS_MENUE_TITLES . 'exportTaxAccountantData.png" alt="" />'; } ?> <?php echo $thisTitle; ?></h1>
				<div id="searchFilterArea">
					<form name="formExportDocumentDatas" method="post" action="<?php echo $_SERVER["PHP_SELF"]; ?>" style="float:left;">
						<table border="0" cellpadding="0" cellspacing="0" class="searchFilterContent">
							<tr>
								<td style="vertical-align:middle;"><b>RE/GU--Zeitraum: </b></td>
								<td>
									<label for="exportDateStart">von:</label>
									<input type="text" name="exportDateStart" id="exportDateStart" maxlength="2" class="inputField_70" readonly="readonly" value="<?php if($_POST["exportDateStart"] != '') { echo $_POST["exportDateStart"]; } else { echo date ('d.m.Y', mktime(0, 0, 0, (date('m') - $monthInterval), 1, date('Y') )); } ?>" />
								</td>
								<td>
									<label for="exportDateEnd">bis:</label>
									<input type="text" name="exportDateEnd" id="exportDateEnd" maxlength="2" class="inputField_70" readonly="readonly" value="<?php if($_POST["exportDateEnd"] != '') { echo $_POST["exportDateEnd"]; } else { echo date ('d.m.Y', mktime(0, 0, 0, (date('m') - ($monthInterval - 1)), 0, date('Y') )); } ?>" />
								</td>
								<td>
									<input type="hidden" name="editID" id="editID" value="" />
									<input type="submit" name="submitExport" class="inputButton0" value="Daten exportieren" onclick="showWarning('Sollen die Rechnungen und Gutschriften wirklich für den gewählten Zeitraum exportiert werden?')" />
								</td>
							</tr>
						</table>
						<input type="hidden" name="submitExportForm" id="submitExportForm" value="1" />
					</form>

					<form name="formExportCustomerDatas" method="post" action="<?php echo $_SERVER["PHP_SELF"]; ?>" style="margin:0 0 0 100px;padding-left:20px;float:left;border-left:1px solid #333;">
						<table border="0" cellpadding="0" cellspacing="0" class="searchFilterContent">
							<tr>
								<td style="vertical-align:middle;"><b>Kunden exportieren:</b></td>
								<td>
									<label for="exportCustomersDateStart">von:</label>
									<input type="text" name="exportCustomersDateStart" id="exportCustomersDateStart" maxlength="2" class="inputField_70" readonly="readonly" value="<?php if($_POST["exportCustomersDateStart"] != '') { echo $_POST["exportCustomersDateStart"]; } else { echo date ('d.m.Y', mktime(0, 0, 0, (date('m') - $monthInterval), 1, date('Y') )); } ?>" />
								</td>
								<td>
									<label for="exportCustomersDateEnd">bis:</label>
									<input type="text" name="exportCustomersDateEnd" id="exportCustomersDateEnd" maxlength="2" class="inputField_70" readonly="readonly" value="<?php if($_POST["exportCustomersDateEnd"] != '') { echo $_POST["exportCustomersDateEnd"]; } else { echo date ('d.m.Y', mktime(0, 0, 0, (date('m') - ($monthInterval - 1)), 0, date('Y') )); } ?>" />
								</td>
								<td>
									<label for="exportCustomersAll">Alle:</label>
									<input type="checkbox" name="exportCustomersAll" id="exportCustomersAll" class="inputField_70" value="1" />
								</td>
								<td style="vertical-align:middle;">
									<input type="hidden" name="editID" id="editID" value="" />
									<input type="submit" name="submitExportCustomers" class="inputButton0" value="Kunden exportieren" onclick="showWarning('Sollen die Kunden wirklich exportiert werden?')" />
								</td>
							</tr>
						</table>
					</form>
					<div class="clear"></div>
				</div>

				<div class="contentDisplay">
					<?php displayMessages(); ?>
					<?php
						if($_POST["submitExportForm"] == 1) {
							if($showPdfDownloadLink) {
								echo '<h2>RE/GU-Export</h2>';
								// echo '<p class="dataTableContent"><b>DOWNLOAD-LINK: </b><a href="'.PATH_EXPORT_FILES_TAX_ACCOUNTANT . $thisCreatedPdfName.'" target="_blank">'.$thisCreatedPdfName.'</a></p>';
								#echo '<p class="dataTableContent"><b>DOWNLOAD-LINK: </b><img src="layout/icons/iconPDF.gif" height="16" width="16" alt="" /> <a href="?downloadFile=' . $thisCreatedPdfName.'&downloadType=EX">'.utf8_decode($thisCreatedPdfName).'</a></p>';
								echo '<p class="dataTableContent"><b>DOWNLOAD-LINK: </b><img src="layout/icons/iconEXCEL.gif" height="16" width="16" alt="" /> <a href="?downloadFile=' . $thisCreatedCsvName.'&downloadType=EX">'.utf8_decode($thisCreatedCsvName).'</a></p>';

								echo '<p class="dataTableContent">';
								echo '<b>Datei versenden: </b><img src="layout/icons/mailAttachement.png" class="buttonSendDocument" rel="' . urlencode($thisCreatedCsvName) . '#EX" width="16" height="16" title="Datei direkt per Mail versenden" alt="Dokument versenden" />';
								echo '</p>';
							}
							echo '<hr />';
							// $_POST["editID"] = "";
						}

						if($rs_customers && file_exists(PATH_EXPORT_FILES_TAX_ACCOUNTANT . $thisCreatedCsvNameCustomers)) {
							echo '<h2>Kunden-Export</h2>';
							echo '<p class="dataTableContent"><b>DOWNLOAD-LINK: </b><img src="layout/icons/iconEXCEL.gif" height="16" width="16" alt="" /> <a href="?downloadFile=' . $thisCreatedCsvNameCustomers.'&downloadType=EX">'.utf8_decode($thisCreatedCsvNameCustomers).'</a></p>';
							echo '<p class="dataTableContent">';
							echo '<b>Datei versenden: </b><img src="layout/icons/mailAttachement.png" class="buttonSendDocument" rel="' . urlencode($thisCreatedCsvNameCustomers) . '#EX" width="16" height="16" title="Datei direkt per Mail versenden" alt="Dokument versenden" />';
							echo '</p>';
							echo '<hr />';
						}
					?>
				</div>
				<hr />
				<p class="infoArea" style="font-weight:normal">
					<span style="padding-right:10px;"><b>Letzte exportiere RE/GU-Daten: </b></span>
					<span style="padding-right:10px;"><b>Datei: </b><?php echo formatDate($arrLastExportData["MAX_FILE_NAME"], 'display'); ?></span>
					<span style="padding-right:10px;"><b>von: </b><?php echo formatDate($arrLastExportData["MAX_FILE_DATE_FROM"], 'display'); ?></span>
					<span style="padding-right:10px;"><b>bis: </b><?php echo formatDate($arrLastExportData["MAX_FILE_DATE_TO"], 'display'); ?></span>
					<span style="padding-right:10px;"><b>exportiert am: </b><?php echo formatDate($arrLastExportData["MAX_FILE_EXPORT_DATE"], 'display'); ?></span>
				</p>

				<?php if(1 || $_COOKIE["isAdmin"]){ ?>
				<h3>Nicht an DATEV exportierte Rechnungen und Gutschriften</h3>
				<div id="tabs">
					<ul>
						<li><a href="#tabs-RE">Nicht an DATEV exportierte REs</a></li>
						<li><a href="#tabs-GU">Nicht an DATEV exportierte GU</a></li>
					</ul>

					<div id="tabs-RE">
						<h4>Nicht an DATEV exportierte Rechnungen</h4>
						<p>Der aktuelle Monat wird nicht ber&uuml;cksichtigt.</p>
						<?php
							$sql_nonExportedDocuments =  "
								SELECT
									SQL_CALC_FOUND_ROWS

									`" . TABLE_ORDER_INVOICES . "`	.`orderDocumentsNumber`,
									`" . TABLE_ORDER_INVOICES . "`	.`orderDocumentsAddressCompanyCustomerNumber`,
									`" . TABLE_ORDER_INVOICES . "`	.`orderDocumentsDocumentPath`

									FROM `" . TABLE_ORDER_INVOICES . "`

									LEFT JOIN `" . TABLE_EXPORT_DATEV_DATA . "`
									ON(`" . TABLE_ORDER_INVOICES . "`.`orderDocumentsNumber` = `" . TABLE_EXPORT_DATEV_DATA . "`.`DOC_NR`)

									WHERE 1
										AND `" . TABLE_EXPORT_DATEV_DATA . "`.`DOC_NR` IS NULL
										AND `" . TABLE_ORDER_INVOICES . "`.`orderDocumentsNumber` NOT LIKE 'RE-" . date("ym") . "%'
										AND SUBSTRING(`" . TABLE_ORDER_INVOICES . "`.`orderDocumentsNumber`, 4, 2) > " . $exportYearStart . "

									ORDER BY `" . TABLE_ORDER_INVOICES . "`.`orderDocumentsNumber` DESC
								";

							if($_REQUEST["page"] == "" || $_REQUEST["page"] == 0) {
								$_REQUEST["page"] = 1;
							}

							if(MAX_DELIVERIES_PER_PAGE > 0) {
								$sql_nonExportedDocuments .= " LIMIT " . (($_REQUEST["page"] - 1) * MAX_DELIVERIES_PER_PAGE) . ", " . MAX_DELIVERIES_PER_PAGE." ";
							}

							$countRow = 0;

							$rs_nonExportedDocuments = $dbConnection->db_query($sql_nonExportedDocuments);

							$sql_totalRows = "SELECT FOUND_ROWS() AS `countRows`";
							$rs_totalRows = $dbConnection->db_query($sql_totalRows);
							list($countRows) = mysqli_fetch_array($rs_totalRows);

							$pagesCount = ceil($countRows / MAX_DELIVERIES_PER_PAGE);

							$marker = '';

							if($countRows > 0) {
								if($pagesCount > 1 && $_GET["loadAll"] != "true") {
									include(FILE_MENUE_PAGES);
								}

								echo '
									<table cellspacing="0" cellpadding="0" width="100%" class="displayOrders">
										<thead>
											<tr>
												<th style="width:45px;text-align:right;">#</th>
												<th style="width:200px;">DOC-NR</th>
												<th style="width:200px;">KNR</th>
												<th style="width:50px;">Info</th>
											</tr>
										</thead>

										<tbody>
									';

								while($ds_nonExportedDocuments = mysqli_fetch_assoc($rs_nonExportedDocuments)){

									if($marker != substr($ds_nonExportedDocuments["orderDocumentsNumber"], 0, 7)){
										$marker = substr($ds_nonExportedDocuments["orderDocumentsNumber"], 0, 7);
										$thisMonthName = getTimeNames(substr($marker, 5, 2), 'month', '');
										$thisTitle = $thisMonthName . '.20' . substr($marker, 3, 2);
										$thisTitle = 'RECHNUNGEN: ' . $thisTitle;
										echo '<tr class="tableRowTitle1">';
										echo '<td colspan="4">';
										echo $thisTitle;
										echo '</td>';
										echo '</tr>';
									}

									if($countRow%2 == 0){ $rowClass = 'row1'; }
									else { $rowClass = 'row0'; }

									echo '<tr class="' . $rowClass . '">';

									echo '<td style="text-align:right;">';
									echo '<b>'.($countRow + 1).'.</b> ';
									echo '</td>';

									echo '<td>';
									echo $ds_nonExportedDocuments["orderDocumentsNumber"];
									echo '</td>';

									echo '<td>';
									echo '<a href="' . PAGE_EDIT_CUSTOMER . '?searchBoxCustomer=' . $ds_nonExportedDocuments["orderDocumentsAddressCompanyCustomerNumber"] . '">' . $ds_nonExportedDocuments["orderDocumentsAddressCompanyCustomerNumber"] . '</a>';;
									echo '</td>';

									echo '<td>';
									echo '<a href="?downloadFile=' . basename($ds_nonExportedDocuments["orderDocumentsDocumentPath"]) . '&downloadType=RE" title="PDF &ouml;ffnen"><img src="layout/icons/iconPDF.gif" width="14" height="14" alt="" /></a>';
									echo '</td>';

									echo '</tr>';

									$countRow++;
								}

								echo '
										</tbody>
									</table>
									';

								if($pagesCount > 1 && $_GET["loadAll"] != "true") {
									include(FILE_MENUE_PAGES);
								}
							}
							else {
								echo '<p class="infoArea">Es liegen keinen Rechnungen vor!</p>';
							}
						?>
					</div>
					<div id="tabs-GU">
						<h4>Nicht an DATEV exportierte Gutschriften</h4>
						<p>Der aktuelle Monat wird nicht ber&uuml;cksichtigt.</p>
						<?php
							$sql_nonExportedDocuments =  "
								SELECT
									SQL_CALC_FOUND_ROWS

									`" . TABLE_ORDER_CREDITS . "`	.`orderDocumentsNumber`,
									`" . TABLE_ORDER_CREDITS . "`	.`orderDocumentsAddressCompanyCustomerNumber`,
									`" . TABLE_ORDER_CREDITS . "`	.`orderDocumentsDocumentPath`

									FROM `" . TABLE_ORDER_CREDITS . "`

									LEFT JOIN `" . TABLE_EXPORT_DATEV_DATA . "`
									ON(`" . TABLE_ORDER_CREDITS . "`.`orderDocumentsNumber` = `" . TABLE_EXPORT_DATEV_DATA . "`.`DOC_NR`)

									WHERE 1
										AND `" . TABLE_EXPORT_DATEV_DATA . "`.`DOC_NR` IS NULL
										AND `" . TABLE_ORDER_CREDITS . "`	.`orderDocumentsNumber` NOT LIKE 'GU-" . date("ym") . "%'
										AND SUBSTRING(`" . TABLE_ORDER_CREDITS . "`.`orderDocumentsNumber`, 4, 2) > " . $exportYearStart . "

									ORDER BY `" . TABLE_ORDER_CREDITS . "`.`orderDocumentsNumber` DESC
								";

							if($_REQUEST["page"] == "" || $_REQUEST["page"] == 0) {
								$_REQUEST["page"] = 1;
							}

							if(MAX_DELIVERIES_PER_PAGE > 0) {
								$sql_nonExportedDocuments .= " LIMIT " . (($_REQUEST["page"] - 1) * MAX_DELIVERIES_PER_PAGE) . ", " . MAX_DELIVERIES_PER_PAGE." ";
							}

							$countRow = 0;

							$rs_nonExportedDocuments = $dbConnection->db_query($sql_nonExportedDocuments);

							$sql_totalRows = "SELECT FOUND_ROWS() AS `countRows`";
							$rs_totalRows = $dbConnection->db_query($sql_totalRows);
							list($countRows) = mysqli_fetch_array($rs_totalRows);

							$pagesCount = ceil($countRows / MAX_DELIVERIES_PER_PAGE);

							$marker = '';

							if($countRows > 0) {
								if($pagesCount > 1 && $_GET["loadAll"] != "true") {
									include(FILE_MENUE_PAGES);
								}

								echo '
									<table cellspacing="0" cellpadding="0" width="100%" class="displayOrders">
										<thead>
											<tr>
												<th style="width:45px;text-align:right;">#</th>
												<th style="width:200px;">DOC-NR</th>
												<th style="width:200px;">KNR</th>
												<th style="width:50px;">Info</th>
											</tr>
										</thead>

										<tbody>
									';

								while($ds_nonExportedDocuments = mysqli_fetch_assoc($rs_nonExportedDocuments)){

									if($marker != substr($ds_nonExportedDocuments["orderDocumentsNumber"], 0, 7)){
										$marker = substr($ds_nonExportedDocuments["orderDocumentsNumber"], 0, 7);
										$thisMonthName = getTimeNames(substr($marker, 5, 2), 'month', '');
										$thisTitle = $thisMonthName . '.20' . substr($marker, 3, 2);
										$thisTitle = 'GUTSCHRIFTEN: ' . $thisTitle;
										echo '<tr class="tableRowTitle1">';

										echo '<td colspan="4">';
										echo $thisTitle;
										echo '</td>';

										echo '</tr>';

									}

									if($countRow%2 == 0){ $rowClass = 'row1'; }
									else { $rowClass = 'row0'; }

									echo '<tr class="' . $rowClass . '">';

									echo '<td style="text-align:right;">';
									echo '<b>'.($countRow + 1).'.</b> ';
									echo '</td>';

									echo '<td>';
									echo $ds_nonExportedDocuments["orderDocumentsNumber"];
									echo '</td>';

									echo '<td>';
									echo '<a href="' . PAGE_EDIT_CUSTOMER . '?searchBoxCustomer=' . $ds_nonExportedDocuments["orderDocumentsAddressCompanyCustomerNumber"] . '">' . $ds_nonExportedDocuments["orderDocumentsAddressCompanyCustomerNumber"] . '</a>';;
									echo '</td>';

									echo '<td>';
									echo '<a href="?downloadFile=' . basename($ds_nonExportedDocuments["orderDocumentsDocumentPath"]) . '&downloadType=GU" title="PDF &ouml;ffnen"><img src="layout/icons/iconPDF.gif" width="14" height="14" alt="" /></a>';
									echo '</td>';

									echo '</tr>';

									$countRow++;
								}

								echo '
										</tbody>
									</table>
									';

								if($pagesCount > 1 && $_GET["loadAll"] != "true") {
									include(FILE_MENUE_PAGES);
								}
							}
							else {
								echo '<p class="infoArea">Es liegen keinen Gutschriften vor!</p>';
							}
						?>
					</div>
				</div>
				<?php } ?>
			</div>
		</div>
		<div class="clear"></div>
	</div>
</div>
<script language="javascript" type="text/javascript">
	$(document).ready(function() {
		$(function() {
			$.datepicker.setDefaults($.datepicker.regional[""]);
			$('#exportDateStart').datepicker($.datepicker.regional["de"]);
				$('#exportDateStart').datepicker("option", "maxDate", "0" );
			$('#exportDateEnd').datepicker($.datepicker.regional["de"]);
				$('#exportDateEnd').datepicker("option", "maxDate", "0" );

			$('#exportCustomersDateStart').datepicker($.datepicker.regional["de"]);
				$('#exportCustomersDateStart').datepicker("option", "maxDate", "0" );
			$('#exportCustomersDateEnd').datepicker($.datepicker.regional["de"]);
				$('#exportCustomersDateEnd').datepicker("option", "maxDate", "0" );
		});

		$('.buttonSendDocument').click(function() {
			var mailDocumentFilename = $(this).attr('rel');
			// var mailAdress = 'info@kanzlei-feldmeier.de';
			// var mailAdress = '<?php echo MAIL_ADDRESS_FROM; ?>';
			// var mailAdress = 'a.moneck@steuerberater-meyer.de';
			var mailAdress = '<?php echo MAIL_ADDRESS_TAX_ACCOUNTANT; ?>';
			sendAttachedDocument($(this), mailDocumentFilename, '<?php echo $thisDocumentValue[$thisKey]["orderDocumentsCustomerNumber"]; ?>', '<?php echo $_REQUEST["editID"]; ?>', '', '<?php echo $_SERVER["REQUEST_URI"]; ?>', mailAdress);
		});

		$(function() {
			$('#tabs').tabs();
		});
	});
</script>
<?php require_once('inc/footerHTML.inc.php'); ?>