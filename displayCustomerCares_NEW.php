<?php
	require_once('inc/requires.inc.php');

	if(!$arrGetUserRights["displayDistribution"]) {
		header('location: ' . PAGE_EXIT_LOCATION);
		exit;
	}

	function showActivityStatusData(){
		$content = '';
		echo $content;
	}

	$jswindowMessage = '';
	$warningMessage = '';
	$errorMessage = '';
	$successMessage = '';
	$infoMessage = '';

	$dbConnection = new DB_Connection();
	$db_open = $dbConnection->db_connect();

	// BOF READ ORDER TYPES
		$arrMandatoriesDatas = getMandatories();
		if($_POST["searchMandatory"] == ''){
			$_POST["searchMandatory"] = MANDATOR;
		}
	// EOF READ ORDER TYPES

	// BOF READ SALESMEN WITH ZIP CODES
		$arrSalesmenWithActiveAreasData = getSalesmenWithActiveAreas();
	// EOF READ SALESMEN WITH ZIP CODES

	$defaultORDER = "orderDocumentsDocumentDate";
	$defaultSORT = "ASC";

	if($_POST["searchInterval"] == ""){
		$_POST["searchInterval"] = "DAY";
	}
	if($_POST["searchDisplay"] == ""){
		$_POST["searchDisplay"] = "AREA_TOTAL";
	}

	$todayYear = date("Y");
	if($_POST["searchYear"] == ""){
		$_POST["searchYear"] = $todayYear;
	}
?>
<?php
	require_once('inc/headerHTML.inc.php');
	$thisTitle = "Kunden-Betreuung";

	if($_POST["searchDisplay"] == 'AREA_TOTAL' && $_POST["searchSalesman"] != 'SALESMEN_ALL'){
		$thisTitle .= ': <span class="headerSelectedEntry">ALLE VERTRETER</span>';
	}
	else if($_REQUEST["searchSalesman"] != ''){
		$thisTitle .= ': <span class="headerSelectedEntry">' . $arrSalesmenWithActiveAreasData[$_REQUEST["searchSalesman"]]["customersKundennummer"] . ' &bull; ' . $arrSalesmenWithActiveAreasData[$_REQUEST["searchSalesman"]]["customersFirmenname"] . '</span>';
	}


	$headerHTML = preg_replace("/{###TITLE###}/", strip_tags($thisTitle), $headerHTML);
	echo $headerHTML;

	require_once(FILE_MENUE_TOP);

	$sqlSortField = "";
	$sqlSortDirection = "";
	$sqlWhere = "";
?>

		<div id="menueSidebarToggleArea">
			<img src="layout/icons/iconToggle.png" class="buttonToggleSidebarMenue" width="10" height="10" alt="" title="Seitennavigation ein-/ausblenden" />
			<div id="menueSidebarToggleContent">
			<?php require_once(FILE_MENUE_SIDEBAR); ?>
			<div class="clear"></div>
			</div>
		</div>
		<div id="contentArea2">
			<a name="top"></a>
			<div id="contentAreaElements">
				<h1><?php if(SHOW_PAGE_TITLE_ICONS) { echo '<img src="' . PATH_ICONS_MENUE_TITLES . 'acquisition.png" alt="" />'; } ?> <?php echo $thisTitle; ?></h1>
				<?php
					echo '<div align="right"><a href="' . PATH_ONLINE_ACQUISITION . '" target="_blank">Zur Online-Kundenerfassung</a></div>';
				?>
				<div id="searchFilterArea">
					<form name="formSearchDatas" method="post" action="<?php echo $_SERVER["PHP_SELF"]; ?>">
					<table border="0" cellpadding="0" cellspacing="0" class="searchFilterContent">
						<tr>
							<td>
								<label for="searchInterval">Zeitraum:</label>
								<select name="searchInterval" id="searchInterval" class="inputSelect_130">
									<option value="DAY" <?php if($_POST["searchInterval"] == 'DAY'){ echo ' selected="selected" '; } ?> >pro Tag</option>
									<option value="WEEK" <?php if($_POST["searchInterval"] == 'WEEK'){ echo ' selected="selected" '; } ?> >pro Woche</option>
									<option value="MONTH" <?php if($_POST["searchInterval"] == 'MONTH'){ echo ' selected="selected" '; } ?> >pro Monat</option>
									<option value="QUARTER" <?php if($_POST["searchInterval"] == 'QUARTER'){ echo ' selected="selected" '; } ?> >pro Vierteljahr</option>
									<option value="YEAR" <?php if($_POST["searchInterval"] == 'YEAR'){ echo ' selected="selected" '; } ?> >pro Jahr</option>
								 </select>
							</td>

							<td>
								<label for="searchInterval">Jahr:</label>
								<select name="searchYear" id="searchYear" class="inputSelect_130">
									<!-- <option value="ALL" <?php if($_POST["searchYear"] == "ALL"){ echo ' selected="selected" '; } ?>>Alle Jahre</option> -->
									<?php
										for($i = $todayYear ; $i > 2012 ; $i--){
											$selected = '';
											if($i == $_POST["searchYear"]){
												$selected = ' selected="selected" ';
											}
											echo '<option value="' . $i . '" ' . $selected . ' >' . $i . '</option>';
										}
									?>
								</select>
							</td>

							<td>
								<label for="searchSalesman">Vertreter mit PLZ:</label>
								<select name="searchSalesman" id="searchSalesman" class="inputSelect_510">
									<option value=""> -- Bitte w&auml;hlen -- </option>
									<option value="SALESMEN_ALL" <?php if($_POST["searchSalesman"] == 'SALESMEN_ALL'){ echo ' selected="selected" '; } ?>>Alle Vertreter</option>
									<?php if($_COOKIE["isAdmin"] == '1'){ ?>
									<option value="PLZ_ALL" <?php if($_POST["searchSalesman"] == 'PLZ_ALL'){ echo ' selected="selected" '; } ?>> Alle Gebiete </option>
									<?php } ?>
									<?php
										if(!empty($arrSalesmenWithActiveAreasData)){
											foreach($arrSalesmenWithActiveAreasData as $thisSalesmanKey => $thisSalesmanValue){
												$selected = '';
												if($_REQUEST["searchSalesman"] == $thisSalesmanKey){
													$selected = 'selected="selected" ';
												}
												echo '<option value="' . $thisSalesmanKey . '"' . $selected . '>' . $thisSalesmanValue["salesmenFirmenname"] . ' - PLZ: ' . preg_replace("/;/", ", ", $thisSalesmanValue["salesmenAreas"]) . '</option>';
											}
										}
									?>

								</select>
							</td>

							<td>
								<label for="searchDisplay">Ansicht:</label>
								<select name="searchDisplay" id="searchDisplay" class="inputSelect_200">
									<?php if($_POST["searchSalesman"] != ''){ ?>
										<?php
											#if($_POST["searchSalesman"] != 'PLZ_ALL'){
											if(1){
										?>
										<option value="AREA_TOTAL" <?php if($_POST["searchDisplay"] == 'AREA_TOTAL'){ echo ' selected="selected" '; } ?> >Gesamt (alle Gebiete)</option>
										<option value="AREA_SALESMAN" <?php if($_POST["searchDisplay"] == 'AREA_SALESMAN'){ echo ' selected="selected" '; } ?> >nach einzelnen Gebieten</option>
										<!--
										<option value="PLZ_3" <?php if($_POST["searchDisplay"] == 'PLZ_3'){ echo ' selected="selected" '; } ?> >nach 3-stelliger PLZ</option>
										-->
										<?php } else  if($_POST["searchSalesman"] == 'PLZ_ALL') { ?>
										<!--
										<option value="PLZ_TOTAL" <?php if($_POST["searchDisplay"] == 'PLZ_TOTAL'){ echo ' selected="selected" '; } ?> >Gesamt</option>
										<option value="PLZ_2" <?php if($_POST["searchDisplay"] == 'PLZ_2'){ echo ' selected="selected" '; } ?> >nach 2-stelliger PLZ</option>
										<option value="PLZ_3" <?php if($_POST["searchDisplay"] == 'PLZ_3'){ echo ' selected="selected" '; } ?> >nach 3-stelliger PLZ</option>
										-->
										<?php } ?>
									<?php } ?>
								</select>
							</td>

							<td>
								<input type="hidden" name="editID" id="editID" value="" />
								<input type="submit" name="submitSearch" class="inputButton0" value="Anzeigen" />
							</td>
						</tr>
					</table>
					<input type="hidden" name="submitSearchForm" id="submitSearchForm" value="1" />
					</form>
				</div>

				<div class="contentDisplay">

					<?php
						$arrCustomerActivityStatus = array();
						if($_REQUEST["searchSalesman"] != ""){

							// BOF DEFINE SALESMEN SEARCH LOOP
								$arrSearchSalesmenIDs = array();
								if($_POST["searchSalesman"] == 'SALESMEN_ALL'){
									$arrSearchSalesmenIDs = array_keys($arrSalesmenWithActiveAreasData);
								}
								else {
									$arrSearchSalesmenIDs = array($_REQUEST["searchSalesman"]);
								}
							// EOF DEFINE SALESMEN SEARCH LOOP

							if(!empty($arrSearchSalesmenIDs)){
								foreach($arrSearchSalesmenIDs as $thisSalesmanID){
									// BOF CONSTRUCT WHERE CLAUSE DEPENDING ON SELECTED DATA
										$arrWhere = array();
										$dateField = "";
										$where = "";
										$whereZipCodeDouble = "";
										$whereZipCodeTriple = "";
										$order = "";
										$group = "";
										$areaField = "";

										$where .= "
											AND `" . TABLE_CUSTOMERS_ACTIVITY_STATUS . "`.`customerActivityStatusDate` > DATE_SUB('" . SALEMESMEN_AREA_CONTRACT_START_DATE . "', INTERVAL 2 DAY)
										";

										if($_POST["searchInterval"] == "DAY"){
											$dateField = "
												`" . TABLE_CUSTOMERS_ACTIVITY_STATUS . "`.`customerActivityStatusDate` AS `interval`
											";
											$where .= "
												AND DATE_FORMAT(`" . TABLE_CUSTOMERS_ACTIVITY_STATUS . "`.`customerActivityStatusDate`, '%Y') = '" . $_POST["searchYear"] . "'
											";
										}
										if($_POST["searchInterval"] == "WEEK"){
											$dateField = "
												`" . TABLE_CUSTOMERS_ACTIVITY_STATUS . "`.`customerActivityStatusDate` AS `interval`
											";

											// BOF GET SUNDAY OF EVERY WEEK
												$where .= "
													AND DAYOFWEEK(`" . TABLE_CUSTOMERS_ACTIVITY_STATUS . "`.`customerActivityStatusDate`) = 1
												";
											// EOF GET SUNDAY OF EVERY WEEK

											$where .= "
												AND DATE_FORMAT(`" . TABLE_CUSTOMERS_ACTIVITY_STATUS . "`.`customerActivityStatusDate`, '%Y') = '" . $_POST["searchYear"] . "'
											";
										}
										else if($_POST["searchInterval"] == "MONTH"){
											$dateField = "
												`" . TABLE_CUSTOMERS_ACTIVITY_STATUS . "`.`customerActivityStatusDate` AS `interval`
											";

											// BOF GET LAST DAY OF EVERY MONTH
												$where .= " AND ( ";
												for($i = 1 ; $i < 13 ; $i++){
													$thisMonth = $i;
													if($thisMonth < 10){
														$thisMonth = "0" . $thisMonth;
													}
													$thisLastDayOfMonth = date("t", mktime(0, 0, 0, $thisMonth, 1, $_POST["searchYear"]));
													if($i > 1) {
														$where .= " OR ";
													}
													$where .= " `" . TABLE_CUSTOMERS_ACTIVITY_STATUS . "`.`customerActivityStatusDate` = '" . $_POST["searchYear"] . "-" . $thisMonth . "-" . $thisLastDayOfMonth . "' ";
												}
												$where .= " ) ";
											// EOF GET LAST DAY OF EVERY MONTH

											$where .= "
												AND DATE_FORMAT(`" . TABLE_CUSTOMERS_ACTIVITY_STATUS . "`.`customerActivityStatusDate`, '%Y') = '" . $_POST["searchYear"] . "'
											";
										}
										else if($_POST["searchInterval"] == "QUARTER"){
											$dateField = "
												`" . TABLE_CUSTOMERS_ACTIVITY_STATUS . "`.`customerActivityStatusDate` AS `interval`
											";

											// BOF GET LAST DAY OF EVERY QUARTER
												$where .= " AND ( ";
												for($i = 3 ; $i < 13 ; $i = $i + 3){
													$thisMonth = $i;
													if($thisMonth < 10){
														$thisMonth = "0" . $thisMonth;
													}
													$thisLastDayOfMonth = date("t", mktime(0, 0, 0, $thisMonth, 1, $_POST["searchYear"]));
													if($i > 3) {
														$where .= " OR ";
													}
													$where .= " `" . TABLE_CUSTOMERS_ACTIVITY_STATUS . "`.`customerActivityStatusDate` = '" . $_POST["searchYear"] . "-" . $thisMonth . "-" . $thisLastDayOfMonth . "' ";
												}
												$where .= " ) ";
											// EOF GET LAST DAY OF EVERY QUARTER

											$where .= "
												AND DATE_FORMAT(`" . TABLE_CUSTOMERS_ACTIVITY_STATUS . "`.`customerActivityStatusDate`, '%Y') = '" . $_POST["searchYear"] . "'
											";
										}
										else if($_POST["searchInterval"] == "YEAR"){
											$dateField = "
												`" . TABLE_CUSTOMERS_ACTIVITY_STATUS . "`.`customerActivityStatusDate` AS `interval`
											";

											// BOF GET LAST DAY OF EVERY YEAR
												$thisLastDayOfMonth = date("t", mktime(0, 0, 0, 12, 1, $_POST["searchYear"]));
												$where .= " AND `" . TABLE_CUSTOMERS_ACTIVITY_STATUS . "`.`customerActivityStatusDate` = '" . $_POST["searchYear"] . "-12-" . $thisLastDayOfMonth . "' ";
											// EOF GET LAST DAY OF EVERY YEAR
										}

										$arrWhereZipCodeDouble = array();
										$arrWhereZipCodeTriple = array();

										$searchAreas = $arrSalesmenWithActiveAreasData[$thisSalesmanID]["salesmenAreas"];

										$arrSearchZipCodes = explode(";", $searchAreas);

										foreach($arrSearchZipCodes as $thisSearchZipCode){
											if(strlen($thisSearchZipCode) == 2){
												$arrWhereZipCodeDouble[] = " `customerActivityStatusZipCodeDouble` = '" . $thisSearchZipCode . "'";
											}
											else if(strlen($thisSearchZipCode) == 3){
												$arrWhereZipCodeTriple[] = " `customerActivityStatusZipCodeTriple` = '" . $thisSearchZipCode . "'";
											}
										}
										if(!empty($arrWhereZipCodeDouble) && !empty($arrWhereZipCodeTriple)){
											$arrWhere = array_merge($arrWhereZipCodeDouble, $arrWhereZipCodeTriple);
										}
										else {
											if(!empty($arrWhereZipCodeDouble)){
												$arrWhere = $arrWhereZipCodeDouble;
											}
											else if(!empty($arrWhereZipCodeTriple)){
												$arrWhere = $arrWhereZipCodeTriple;
											}
										}
									// EOF CONSTRUCT WHERE CLAUSE DEPENDING ON SELECTED DATA

									// BOF GET DATA
										$arrIntervals = array();
										$arrDatas = array();

										$sqlCustomerCareStatus_TEMPLATE = "
													SELECT
														{###DATE_FIELD###},

														`customerActivityStatusDate`,
														`customerActivityStatusZipCodeDouble`,
														`customerActivityStatusZipCodeTriple`,
														{###AREA_FIELD###}

														`customerActivityStatusType` AS `statusType`,
														SUM(`customerActivityStatusCount`) AS `statusCount`

														FROM `" . TABLE_CUSTOMERS_ACTIVITY_STATUS . "`
														WHERE 1
															{###WHERE###}

														{###GROUP###}

														 {###ORDER###}
											";


										if($_POST["searchDisplay"] == 'AREA_TOTAL'){
											$sqlCustomerCareStatusTemp = '';

											$group = " GROUP BY CONCAT(`interval`, `customerActivityStatusType`) ";
											$order = " ORDER BY `interval` DESC, `statusType` DESC";

											$areaField = '';

											if(!empty($arrWhere)){
												$where .= "
													AND ( " . implode(" OR ", $arrWhere). " )
												";
											}
											$sqlCustomerCareStatusTemp = $sqlCustomerCareStatus_TEMPLATE;

											$sqlCustomerCareStatusTemp = preg_replace("/{###DATE_FIELD###}/", $dateField, $sqlCustomerCareStatusTemp);
											$sqlCustomerCareStatusTemp = preg_replace("/{###WHERE###}/", $where, $sqlCustomerCareStatusTemp);
											$sqlCustomerCareStatusTemp = preg_replace("/{###GROUP###}/", $group, $sqlCustomerCareStatusTemp);
											$sqlCustomerCareStatusTemp = preg_replace("/{###ORDER###}/", $order, $sqlCustomerCareStatusTemp);
											$sqlCustomerCareStatusTemp = preg_replace("/{###AREA_FIELD###}/", $areaField, $sqlCustomerCareStatusTemp);
										}
										else if($_POST["searchDisplay"] == 'PLZ_2'){
											$sqlCustomerCareStatusTemp = '';

											$group = " GROUP BY CONCAT(`interval`, `customerActivityStatusZipCodeDouble`, `customerActivityStatusType`) ";
											$order = " ORDER BY `customerActivityStatusZipCodeDouble`, `interval` DESC, `statusType` DESC";
											$areaField = '';

											if(!empty($arrWhere)){
												$where .= "
													AND ( " . implode(" OR ", $arrWhere). " )
												";
											}
											$sqlCustomerCareStatusTemp = $sqlCustomerCareStatus_TEMPLATE;

											$sqlCustomerCareStatusTemp = preg_replace("/{###DATE_FIELD###}/", $dateField, $sqlCustomerCareStatusTemp);
											$sqlCustomerCareStatusTemp = preg_replace("/{###WHERE###}/", $where, $sqlCustomerCareStatusTemp);
											$sqlCustomerCareStatusTemp = preg_replace("/{###GROUP###}/", $group, $sqlCustomerCareStatusTemp);
											$sqlCustomerCareStatusTemp = preg_replace("/{###ORDER###}/", $order, $sqlCustomerCareStatusTemp);
											$sqlCustomerCareStatusTemp = preg_replace("/{###AREA_FIELD###}/", $areaField, $sqlCustomerCareStatusTemp);
										}
										else if($_POST["searchDisplay"] == 'PLZ_3'){
											$sqlCustomerCareStatusTemp = '';

											$group = " GROUP BY CONCAT(`interval`, `customerActivityStatusZipCodeTriple`, `customerActivityStatusType`) ";
											$order = " ORDER BY `customerActivityStatusZipCodeTriple`, `interval` DESC, `statusType` DESC";
											$areaField = '';

											if(!empty($arrWhere)){
												$where .= "
													AND ( " . implode(" OR ", $arrWhere). " )
												";
											}
											$sqlCustomerCareStatusTemp = $sqlCustomerCareStatus_TEMPLATE;

											$sqlCustomerCareStatusTemp = preg_replace("/{###DATE_FIELD###}/", $dateField, $sqlCustomerCareStatusTemp);
											$sqlCustomerCareStatusTemp = preg_replace("/{###WHERE###}/", $where, $sqlCustomerCareStatusTemp);
											$sqlCustomerCareStatusTemp = preg_replace("/{###GROUP###}/", $group, $sqlCustomerCareStatusTemp);
											$sqlCustomerCareStatusTemp = preg_replace("/{###ORDER###}/", $order, $sqlCustomerCareStatusTemp);
											$sqlCustomerCareStatusTemp = preg_replace("/{###AREA_FIELD###}/", $areaField, $sqlCustomerCareStatusTemp);
										}
										else if($_POST["searchDisplay"] == 'AREA_SALESMAN' || $_POST["searchSalesman"] == 'SALESMEN_ALL'){
											$sqlCustomerCareStatusTemp = '';
											$sqlCustomerCareStatusTempDouble = '';
											$sqlCustomerCareStatusTempTriple = '';


											$order = " ORDER BY `customerActivityStatusArea`, `interval` DESC, `statusType` DESC";

											if(!empty($arrWhereZipCodeDouble)){
												$group = " GROUP BY CONCAT(`interval`, `customerActivityStatusZipCodeDouble`, `customerActivityStatusType`) ";
												$areaField = " `customerActivityStatusZipCodeDouble` AS `customerActivityStatusArea`, ";
												$whereZipCodeDouble = $where . "
													AND ( " . implode(" OR ", $arrWhereZipCodeDouble). " )
												";

												$sqlCustomerCareStatusTempDouble = $sqlCustomerCareStatus_TEMPLATE;
												$sqlCustomerCareStatusTempDouble = preg_replace("/{###WHERE###}/", $whereZipCodeDouble, $sqlCustomerCareStatusTempDouble);
												$sqlCustomerCareStatusTempDouble = preg_replace("/{###GROUP###}/", $group, $sqlCustomerCareStatusTempDouble);
												$sqlCustomerCareStatusTempDouble = preg_replace("/{###ORDER###}/", "", $sqlCustomerCareStatusTempDouble);
												$sqlCustomerCareStatusTempDouble = preg_replace("/{###AREA_FIELD###}/", $areaField, $sqlCustomerCareStatusTempDouble);
											}
											if(!empty($arrWhereZipCodeTriple)){
												$group = " GROUP BY CONCAT(`interval`, `customerActivityStatusZipCodeTriple`, `customerActivityStatusType`) ";
												$areaField = " `customerActivityStatusZipCodeTriple` AS `customerActivityStatusArea`, ";
												$whereZipCodeTriple = $where . "
													AND ( " . implode(" OR ", $arrWhereZipCodeTriple). " )
												";

												$sqlCustomerCareStatusTempTriple = $sqlCustomerCareStatus_TEMPLATE;
												$sqlCustomerCareStatusTempTriple = preg_replace("/{###WHERE###}/", $whereZipCodeTriple, $sqlCustomerCareStatusTempTriple);
												$sqlCustomerCareStatusTempTriple = preg_replace("/{###GROUP###}/", $group, $sqlCustomerCareStatusTempTriple);
												$sqlCustomerCareStatusTempTriple = preg_replace("/{###ORDER###}/", "", $sqlCustomerCareStatusTempTriple);
												$sqlCustomerCareStatusTempTriple = preg_replace("/{###AREA_FIELD###}/", $areaField, $sqlCustomerCareStatusTempTriple);
											}
											if($sqlCustomerCareStatusTempDouble != '' && $sqlCustomerCareStatusTempTriple != ''){
												$sqlCustomerCareStatusTemp = $sqlCustomerCareStatusTempDouble . " UNION " . $sqlCustomerCareStatusTempTriple;
											}
											else {
												if($sqlCustomerCareStatusTempDouble != ''){
													$sqlCustomerCareStatusTemp = $sqlCustomerCareStatusTempDouble;
												}
												else if($sqlCustomerCareStatusTempTriple != ''){
													$sqlCustomerCareStatusTemp = $sqlCustomerCareStatusTempTriple;
												}
											}
											$sqlCustomerCareStatusTemp = "
														SELECT
														*
														FROM (
															" . $sqlCustomerCareStatusTemp . "
														) AS `tempTable`
														{###ORDER###}
												";
											$sqlCustomerCareStatusTemp = preg_replace("/{###DATE_FIELD###}/", $dateField, $sqlCustomerCareStatusTemp);
											$sqlCustomerCareStatusTemp = preg_replace("/{###ORDER###}/", $order, $sqlCustomerCareStatusTemp);
										}

										$sqlCustomerCareStatus = $sqlCustomerCareStatusTemp;

										$rsCustomerCareStatus = $dbConnection->db_query($sqlCustomerCareStatus);

										$countItem = 0;

										while($ds = mysqli_fetch_assoc($rsCustomerCareStatus)) {
											foreach(array_keys($ds) as $field){
												if($_POST["searchDisplay"] == 'AREA_TOTAL'){
													$thisIndex = 'AREA_TOTAL';
													$arrCustomerActivityStatus[$thisSalesmanID][$thisIndex][$ds["interval"]][$ds["statusType"]][$field] = $ds[$field];
												}
												else if($_POST["searchDisplay"] == 'PLZ_2'){
													$thisIndex = $ds["customerActivityStatusZipCodeDouble"];
													$arrCustomerActivityStatus[$thisSalesmanID][$thisIndex][$ds["interval"]][$ds["statusType"]][$field] = $ds[$field];
												}
												else if($_POST["searchDisplay"] == 'PLZ_3'){
													$thisIndex = $ds["customerActivityStatusZipCodeTriple"];
													$arrCustomerActivityStatus[$thisSalesmanID][$thisIndex][$ds["interval"]][$ds["statusType"]][$field] = $ds[$field];
												}
												else if($_POST["searchDisplay"] == 'AREA_SALESMAN'){
													$thisIndex = $ds["customerActivityStatusArea"];
													$arrCustomerActivityStatus[$thisSalesmanID][$thisIndex][$ds["interval"]][$ds["statusType"]][$field] = $ds[$field];
												}
											}
											$arrIntervals[] = $ds["interval"];
										}
										if(!empty($arrIntervals)){
											$arrIntervals = array_unique($arrIntervals);
											$arrIntervals = array_values($arrIntervals);
										}
									// EOF GET DATA
								}

								if($_REQUEST["searchSalesman"] != ""){
									echo '<p class="infoArea">PLZ-Gebiete: ';
									if($_REQUEST["searchSalesman"] != "SALESMEN_ALL" && $_REQUEST["searchSalesman"] != "PLZ_ALL") {
										echo '' . preg_replace("/;/", " &bull; ", $arrSalesmenWithActiveAreasData[$_REQUEST["searchSalesman"]]["salesmenAreas"]);
									}
									else {
										echo preg_replace("/;/", " &bull; ", $searchAreas);
									}
									echo '</p>';
								}
}
}

						if(!empty($arrCustomerActivityStatus)){
							echo '<table width="100%" cellpadding="0" cellspacing="0" class="displayOrders">';

							echo '<thead>';

							echo '<tr>';
							echo '<th style="width:45px">#</th>';
							echo '<th style="width:90px">Datum</th>';

							echo '<th style="width:360px;">Status rot (letzte Bestellung &auml;lter als letzte 6 Monate)</th>';
							echo '<th style="width:360px;">Status blau (innerhalb 6 Monaten Neukunden)</th>';
							echo '<th style="width:360px;">Status gr&uuml;n (letzte Bestellung innerhalb letzter 6 Monate)</th>';
							echo '<th style="width:90px">Gesamtkunden</th>';
							echo '</tr>';

							echo '</thead>';

							echo '<tbody>';

							$countRow = 0;
							$thisAreaMarker = "";
							$thisSalesmanMarker = "";

							$arrTendencyStartValues = array('red' => 100, 'blue' => 0, 'green' => 0); // %

							foreach($arrCustomerActivityStatus as $thisSalesmenKey => $thisSalesmenData) {
								foreach($thisSalesmenData as $thisArea => $thisAreaData) {
									if($thisSalesmanMarker != $thisSalesmenKey){
										$thisSalesmanMarker = $thisSalesmenKey;
										echo '<tr>';
										echo '<td colspan="6" class="row8" style="background-color:#02F9F9">';
										echo '<b>' . $arrSalesmenWithActiveAreasData[$thisSalesmenKey]["salesmenFirmenname"] . '</b>';
										echo ' - PLZ-Gebiete: ' . preg_replace("/;/", " &bull; ", $arrSalesmenWithActiveAreasData[$thisSalesmenKey]["salesmenAreas"]);
										echo '</td>';
										echo '</tr>';
									}

									if($thisAreaMarker != $thisArea){
										if($thisArea != "AREA_TOTAL"){
											$thisAreaMarker = $thisArea;
											echo '<tr class="row3" style="font-weight:bold;">';
											echo '<td colspan="10">';
											echo 'PLZ: ';
											echo $thisArea;
											echo '</td>';
											echo '</tr>';
										}
									}
									foreach($thisAreaData as $thisInterval => $thisCustomerActivityStatusData) {
										$thisArrIntervalsKey = array_search($thisInterval, $arrIntervals);
										$nextArrIntervalsKey = $thisArrIntervalsKey + 1;

										$rowClass = 'row0';
										if($countRow%2 == 0) {
											$rowClass = 'row1';
										}

										$rowStyle = '';
										if($thisInterval < SALEMESMEN_AREA_CONTRACT_START_DATE){
											$rowStyle = 'opacity:1;';
										}

										echo '<tr style="border-bottom: 2px solid #003300;'.$rowStyle.'" class="'.$rowClass.'">';

										echo '<td style="text-align:right;"><b>';
										echo ($countRow + 1);
										echo '.</b></td>';

										echo '<td style="text-align:right;"><b>';
										echo formatDate($thisInterval, 'display');
										echo '</b></td>';

										$thisTotalCount = 0;
										if(!empty($thisCustomerActivityStatusData)){
											foreach($thisCustomerActivityStatusData as $thisStatusType => $thisStatusTypeData){
												$thisTotalCount += $thisStatusTypeData["statusCount"];
											}
											$scaleLenth = 200;
											$arrOrderStautusType = array('red', 'blue', 'green');
											#foreach($thisCustomerActivityStatusData as $thisStatusType => $thisStatusTypeData){
											foreach($arrOrderStautusType as $thisStatusType){
												$thisStatusCount = $thisCustomerActivityStatusData[$thisStatusType]["statusCount"];
												$thisStatusCountPercent = round(($thisCustomerActivityStatusData[$thisStatusType]["statusCount"] / $thisTotalCount) * 100, 2);
												$nextStatusCountPercent = round(($thisAreaData[$arrIntervals[$nextArrIntervalsKey]][$thisStatusType]["statusCount"] / $thisTotalCount) * 100, 2);

												if($arrIntervals[$nextArrIntervalsKey] == ''){
													$nextStatusCountPercent = $arrTendencyStartValues[$thisStatusType];
												}

												$thisTendenyImage = "tendency_zero.png";
												if($nextStatusCountPercent == $thisStatusCountPercent){
													$thisTendenyImage = "tendency_zero.png";
												}
												else if($nextStatusCountPercent < $thisStatusCountPercent){
													$thisTendenyImage = "tendency_up.png";
												}
												else if($nextStatusCountPercent > $thisStatusCountPercent){
													$thisTendenyImage = "tendency_down.png";
												}
												#$arrTendency[$thisStatusType] = $thisStatusCountPercent;

												if($thisStatusType == 'green'){
													$thisBackgroundColor = '#00FF00';
												}
												else if($thisStatusType == 'blue'){
													$thisBackgroundColor = '#0000FF';
												}
												else if($thisStatusType == 'red'){
													$thisBackgroundColor = '#FF0000';
												}

												echo '<td style="white-space:nowrap;">';
												echo '<div style="white-space:nowrap;width:360px;margin:0;padding:0">';
												echo '<div style="float:left;background-color:#FFFFFF;width:' . $scaleLenth . 'px;border:1px solid #000000;margin:0;padding:0;text-align:left;">';
												$thisScaleLength = round($scaleLenth * $thisStatusCountPercent / 100);
												if($thisScaleLength < 1){
													$thisScaleLength = 1;
												}
												echo '<div style="height:12px;background-color:' . $thisBackgroundColor . ';width:' . $thisScaleLength . 'px;margin:0;padding:0;">';
												echo '</div>';
												echo '</div>';
												echo '<div style="float:right;width:130px;margin:0;padding:0 20px 0 0;text-align:right;white-space:nowrap;">';
												echo $thisStatusCount . ' (' . number_format($thisStatusCountPercent, 2, ",", ".") . '%)';
												echo '<img src="layout/icons/' . $thisTendenyImage . '" width="14" height="14" alt="Tendenz" title="Tendenz" style="padding-left:10px;margin-bottom:-2px;" />';
												echo '</div>';
												echo '<div class="clear"></div>';
												echo '</div>';
												echo '</td>';
											}
										}


										echo '<td style="text-align:right;">';
										echo number_format($thisTotalCount, 0, ',', '.');
										echo '</td>';

										echo '</tr>';
										$countRow++;
									}
								}
							}

							echo '</tbody>';

							echo '</table>';
						}
						else {
							$infoMessage .= ' Es liegen keine Daten vor!' . '<br />';
						}
						displayMessages();
					?>
					<?php displayMessages(); ?>
				</div>

			</div>
		</div>

<script language="javascript" type="text/javascript">
	$(document).ready(function() {
		$(function() {
			colorRowMouseOver('.displayOrders tbody tr');
			$('.buttonShopOrderInfo').css('cursor', 'pointer');
			$('.buttonShopOrderInfo').click(function () {
				loadOrderDetails($(this), '<?php echo BASEPATH; ?>', 'Details der Bestellung');
			});
		});

		$('.buttonToggleSidebarMenue').click(function() {
			$('#menueSidebarToggleContent').toggle();
		});

		$('.blink').parent().parent().parent('tr').css('background-color', '#FFBFC8 !important');
		$('.blink').parent().parent().parent().find('td').css('background-color', '#FFBFC8 !important');

		$(function() {
			$('#tabs').tabs();
		});
	});
</script>
<?php require_once('inc/footerHTML.inc.php'); ?>