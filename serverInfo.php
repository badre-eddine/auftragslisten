<?php

	require_once('inc/requires.inc.php');

	if(!$arrGetUserRights["serverInfo"]) {
		header('location: ' . PAGE_EXIT_LOCATION);
		exit;
	}
?>
<?php
	require_once('inc/headerHTML.inc.php');
	$thisTitle = "PHP-INFO";
	$headerHTML = preg_replace("/{###TITLE###}/", strip_tags($thisTitle), $headerHTML);
	echo $headerHTML;

	require_once(FILE_MENUE_TOP);
?>
<div id="mainArea">
	<div id="mainContent">
		<?php
			require_once(FILE_MENUE_SIDEBAR);
		?>
		<div id="contentArea">
			<div id="contentAreaElements">
				<h1><?php echo $thisTitle; ?></h1>
					<?php
						echo phpinfo();
					?>
			</div>
		</div>
		<div class="clear"></div>
	</div>
	<div class="clear"></div>
</div>

<script language="javascript" type="text/javascript">
	$(document).ready(function() {
		$(function() {
			$( "#datepicker" ).datepicker();
		});
	});
</script>

<?php
	if($message != "")
	{
?>
<script language="javascript" type="text/javascript">
	window.alert('<?php echo $message; ?>');
</script>
<?php
	}

	if($dbConnection->db_displayErrors() != "") {
		$errorMessage = $dbConnection->db_displayErrors(). '<br />';
		if($errorMessage != "") {	echo '<p class="errorArea">'.$errorMessage.'</p>'; }
	}
	$dbConnection->db_close();

	require_once('inc/footerHTML.inc.php');
?>