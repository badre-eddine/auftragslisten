<?php
	require_once('inc/requires.inc.php');

	if(!$arrGetUserRights["adminArea"]) {
		header('location: ' . PAGE_EXIT_LOCATION);
		exit;
	}

	$jswindowMessage = "";
	$warningMessage = '';
	$errorMessage = '';
	$successMessage = '';
	$infoMessage = '';

	$dbConnection = new DB_Connection();
	$db_open = $dbConnection->db_connect();

	$userDatas = getUserDatas();

?>
<?php
	require_once('inc/headerHTML.inc.php');
	$thisTitle = "TEST-SKRIPT";
	$headerHTML = preg_replace("/{###TITLE###}/", strip_tags($thisTitle), $headerHTML);
	echo $headerHTML;

	require_once(FILE_MENUE_TOP);
?>
<div id="mainArea">
	<div id="mainContent">
		<?php require_once(FILE_MENUE_SIDEBAR); ?>
		<div id="contentArea">
			<div id="contentAreaElements">
				<h1><?php if(SHOW_PAGE_TITLE_ICONS) { echo '<img src="' . PATH_ICONS_MENUE_TITLES . 'export.png" alt="" />'; } ?> <?php echo $thisTitle; ?></h1>

				<div class="contentDisplay">
					<p>......TEST INHALT......</p>
					<?php displayMessages(); ?>
					<?php
						if($userDatas["usersLogin"] == 'thorsten'){
							/*
							$thisAction = 'GET_NONCONVERTED_DOCUMENTS';
							$thisAction = 'GET_CONVERTED_DOCUMENTS';
							$thisAction = 'UPDATE_DISTRIBUTION_CUSTOMERS';
							$thisAction = 'GET_NONCONVERTED_DOCUMENTS';
							$thisAction = 'SEND_DATA_TO_PRINTER';
							$thisAction = 'TEST_NEW_MAIL_CLASS';
							$thisAction = 'READ_PDF';
							$thisAction = 'CREATE_PDF_CONTENT';
							$thisAction = 'SEND_DATA_TO_PRINTER';
							$thisAction = 'CREATE_SQL_CHANGE_CUSTOMER_NUMBERS';
							#$thisAction = 'EXPORT_MAIL_DATAS_DEPENDING_ON_GROUP';

							$thisAction = 'EXPORT_DOCUMENT_DATAS_DEPENDING_ON_ADM';

							$thisAction = 'CHANGE_COMPLETE_CUSTOMER_NUMBER';

							$thisAction = 'CREATE_COLLECTIVE_INVOICES';
							$thisAction = 'CHECK_VAT_ID';
							$thisAction = 'LOAD_DPD';
							$thisAction = 'PDF_TO_TEXT';
							$thisAction = 'GENERATE_LINKS';
							$thisAction = 'DOWNLOAD_FONTS';
							*/


							#$thisAction = 'FORMAT_FACTORING_TXT';
							#

							$thisAction = '';
							$thisAction = 'CREATE_SQL_UPDATE_PRODUCT_IDS';

							$thisAction = '';	
						

							// BOF CREATE_SQL_UPDATE_PRODUCT_IDS
							if($thisAction == 'CREATE_SQL_UPDATE_PRODUCT_IDS'){
								require_once("inc/_tempUpdateProductIds.inc.php");
							}
							// EOF CREATE_SQL_UPDATE_PRODUCT_IDS

							// BOF FORMAT_FACTORING_TXT
							if($thisAction == 'FORMAT_FACTORING_TXT'){
								require_once("inc/_temp_formatFactoringTxt.inc.php");
							}
							// EOF FORMAT_FACTORING_TXT

							// BOF DOWNLOAD_FONTS
							if($thisAction == 'DOWNLOAD_FONTS'){
								$fontsDir = "fonts/";
								$arrFonts = array(
												"ARBLI__.TTF",
												"ARI.TTF",
												"ARIAL BLACK.TTF",
												"ARIAL ROUNDED MT BOLD FETT (2).TTF",
												"ARIAL SPECIAL G1.TTF",
												"ARIAL.TTF",
												"ARIALBD.TTF",
												"ARIALBI.TTF",
												"ARIALI.TTF",
												"ARIALN.TTF",
												"ARIALNB.TTF",
												"ARIALNBI.TTF",
												"ARIALNI.TTF",
												"ARIALUNI.TTF",
												"ARIBLK.TTF",
												"ARLRDBD.TTF",
												"GOODTIME.ttf",
												"UFONTS.COM_ARIAL-SPECIAL-G1-BOLD-2.TTF"
										);
								if(!empty($arrFonts)){
									foreach($arrFonts as $thisFont){
										echo '<a href="' . $fontsDir . $thisFont . '" target="_blank">' . $thisFont . '</a><br />';
									}
								}
							}
							// EOF DOWNLOAD_FONTS

							// BOF GET CREATE_PRINT_PRODUCTION_FILES
							if($thisAction == 'CREATE_PRINT_PRODUCTION_FILES'){
								require_once("inc/tempPrintProductionFiles.inc.php");
							}
							// EOF GET CREATE_PRINT_PRODUCTION_FILES

							// BOF GET RELATED DOCUMENTS
							if($thisAction == 'GET_NONCONVERTED_DOCUMENTS' || $thisAction == 'GET_CONVERTED_DOCUMENTS'){
								require_once("inc/tempGetRelatedDocuments.inc.php");
							}
							// EOF GET RELATED DOCUMENTS

							// BOF CREATE_SQL_CHANGE_CUSTOMER_NUMBERS
							if($thisAction == 'CREATE_SQL_CHANGE_CUSTOMER_NUMBERS'){
								require_once("inc/tempChangeCustomers.inc.php");
							}
							// EOF CREATE_SQL_CHANGE_CUSTOMER_NUMBERS

							// BOF UPDATE CUSTOMERS IN MODUL DISTRIBUTION
							if($thisAction == 'UPDATE_DISTRIBUTION_CUSTOMERS'){
								require_once("inc/tempUpdateDistributionCustomers.inc.php");
							}
							// EOF UPDATE CUSTOMERS IN MODUL DISTRIBUTION

							// BOF SEND DATA DIRECTLY TO PRINTER
							if($thisAction == 'SEND_DATA_TO_PRINTER'){
								require_once("inc/tempSendDataToPrinter.inc.php");
							}
							// EOF SEND DATA DIRECTLY TO PRINTER

							// BOF CHANGE COMPLETE CUSTOMER NUMBER
							if($thisAction == 'CHANGE_COMPLETE_CUSTOMER_NUMBER'){
								require_once("inc/_tempChangeCompleteCustomerNumber.inc.php");
							}
							// EOF CHANGE COMPLETE CUSTOMER NUMBER
							/*
							$zip = new ZipArchive();
							$filename = "test112.zip";

							if ($zip->open($filename, ZIPARCHIVE::CREATE)!==TRUE) {
								exit("cannot open <$filename>\n");
							}

							$zip->addFromString("testfilephp.txt" . time(), "#1 This is a test string added as testfilephp.txt.\n");
							$zip->addFromString("testfilephp2.txt" . time(), "#2 This is a test string added as testfilephp2.txt.\n");
							$zip->addFile("/testfromfile.php");
							echo "numfiles: " . $zip->numFiles . "\n";
							echo "status:" . $zip->status . "\n";
							$zip->close();
							*/

							// BOF READ PDF
							if($thisAction == 'READ_PDF'){
								require_once("inc/tempReadPdf.inc.php");
							}
							// EOF READ PDF

							// BOF PDF_TO_TEXT
							if($thisAction == 'PDF_TO_TEXT'){
								#$pdfFile = "documents_common/printProductionFiles/belichtung_2015-08-24_03.pdf";
								$pdfFile = "documents_bctr/documentsCreated/customers/test_2014-02-18_17-25-12.pdf";
								$pdfFile = "documents_bctr/documentsCreated/customers/RE-1604002105_KNR-21239.pdf";
								if(file_exists($pdfFile)){
									echo '<p class="successArea">Datei gefunden</p>';

									require_once(PATH_PDF2TEXT);
									$pdfToTextContent = pdf2text($pdfFile);
									#$pdfToTextEncoding = iconv_get_encoding($pdfFile);
									$pdfToTextEncoding = mb_detect_encoding($pdfFile);
									$pdfToTextContent = mb_convert_encoding($pdfToTextContent, "UTF-8", mb_internal_encoding());

									$pdfToTextContent = preg_replace("/:\\n/", ": ", $pdfToTextContent);
									#$pdfToTextContent = utf8_decode($pdfToTextContent);
									echo '<h2>Inhalt der PDF-Datei &quot;' . basename($pdfFile) . '&quot;</h2>';
									echo '<pre>';
									# dd('pdfToTextContent');
									echo $pdfToTextContent;
									echo '</pre>';

								}
								else{
									echo '<p class="warningArea">Datei nicht gefunden</p>';
								}
							}
							// EOF PDF_TO_TEXT

							// BOF PDF_TO_TEXT
							if($thisAction == 'GENERATE_LINKS'){
								// BASIC URL 1: http://www.automobilmeisterwerkstatt.de/partner.php?plz=0
								// BASIC URL 2: http://www.automobilmeisterwerkstatt.de/partner.php?plz=1&plz2=2
								$basicUrl_plz1 = "http://www.automobilmeisterwerkstatt.de/partner.php?";
								for($plz1 = 0 ; $plz1 < 10 ; $plz1++){
									$thisUrlPlz1 = $basicUrl_plz1 . 'plz=' . $plz1;
									echo '' . $thisUrlPlz1 . '<br />';
									for($plz2 = 0 ; $plz2 < 10 ; $plz2++){
										$thisUrlPlz2 = $thisUrlPlz1 . '&plz2=' . $plz2;
										echo '' . $thisUrlPlz2 . '<br />';
									}
								}
							}
							// EOF PDF_TO_TEXT

							// BOF CREATE PDF
							if($thisAction == 'CREATE_PDF_CONTENT'){

							}
							// EOF CREATE PDF

							// BOF EXPORT MAIL DEPENDING ON CUSTOMER GROUP
								if($thisAction == 'EXPORT_MAIL_DATAS_DEPENDING_ON_GROUP'){
									#require_once("inc/tempExportMailDatasDependingOnGroup.inc.php");
								}
							// BOF EXPORT MAIL DEPENDING ON CUSTOMER GROUP
								if($thisAction == 'EXPORT_DOCUMENT_DATAS_DEPENDING_ON_ADM'){
									#require_once("inc/tempExportDocumentDatasDependingOnAdm.inc.php");
								}
							// BOF EXPORT MAIL DEPENDING ON CUSTOMER GROUP

							// BOF LOAD_DPD
							/*
							if($thisAction == 'LOAD_DPD'){
								function apiGetDPDTracking($trackingNumber){
									$myApiURL = 'https://cloud-stage.dpd.com/api/v1/ParcelLifeCycle/' . $trackingNumber;

									$httpHeader = array(
										  'Version : ' . '100',
										  'Language : ' . 'de_DE',
										  'PartnerCredentials-Name : ' . 'DPD Sandbox',
										  'PartnerCredentials-Token : ' . '06445364853584D75564',
										  'UserCredentials-cloudUserID : ' . '1699',
										  'UserCredentials-Token : ' . '344171614948306A3769',
									);

									$myRestApiGetCall = curl_init();
									curl_setopt($myRestApiGetCall, CURLOPT_URL, $myApiURL);
									curl_setopt($myRestApiGetCall, CURLOPT_RETURNTRANSFER, true);
									curl_setopt($myRestApiGetCall, CURLOPT_SSL_VERIFYHOST, false);
									curl_setopt($myRestApiGetCall, CURLOPT_SSL_VERIFYPEER, false);
									curl_setopt($myRestApiGetCall, CURLOPT_HTTPGET, TRUE);
									curl_setopt($myRestApiGetCall, CURLOPT_HTTPHEADER, $httpHeader);

									$getParcelLifeCycleResponse = curl_exec($myRestApiGetCall);
									return $getParcelLifeCycleResponse;
								}
								$trackingStatus = apiGetDPDTracking('01485004296802');

								echo $trackingStatus;
							}
							*/
							// EOF LOAD_DPD


						}
						displayMessages();
					?>
				</div>
			</div>
		</div>
		<div class="clear"></div>
	</div>
</div>
<?php
	if($arrGetUserRights["editPersonnel"]) {
?>
<script language="javascript" type="text/javascript">
	$(document).ready(function() {
		$(function() {
			$( "#datepicker" ).datepicker();
			$.datepicker.setDefaults($.datepicker.regional[""]);
			$('#editPersonnelVacationsStart').datepicker($.datepicker.regional["de"]);
			$('#editPersonnelVacationsEnd').datepicker($.datepicker.regional["de"]);
			$('#editPersonnelJobStart').datepicker($.datepicker.regional["de"]);
			$('#editPersonnelJobEnd').datepicker($.datepicker.regional["de"]);
			//$('#editPersonnelBirthday').datepicker($.datepicker.regional["de"]);
			$('#editPersonnelDiseaseStart').datepicker($.datepicker.regional["de"]);
			$('#editPersonnelDiseaseEnd').datepicker($.datepicker.regional["de"]);
			$('#editPersonnelOvertimeDate').datepicker($.datepicker.regional["de"]);
		});
		$(function() {
			$('#tabs').tabs();
		});
		setFormDutyFields(<?php echo $jsonFormDutyFields; ?>);
		colorRowMouseOver('.displayOrders tbody tr');

		$('#editPersonnelZipcode').keyup(function () {
			loadSuggestions('searchPlzCity', [{'triggerElement': '#editPersonnelZipcode', 'fieldCity': '#editPersonnelCity', 'fieldZipCode': '#editPersonnelZipcode'}], 1);
		});
		$('#editPersonnelCity').keyup(function () {
			loadSuggestions('searchCityPlz', [{'triggerElement': '#editPersonnelCity', 'fieldCity': '#editPersonnelCity', 'fieldZipCode': '#editPersonnelZipcode'}], 1);
		});

		$('#editPersonnelBankName').keyup(function(){
			loadSuggestions('searchBankName', [{'triggerElement': '#editPersonnelBankName', 'fieldCode': '#editPersonnelBankLeitzahl', 'fieldName': '#editPersonnelBankName', 'fieldBIC': '#editPersonnelBankBIC', 'fieldIBAN': '#editPersonnelBankIBAN'}], 1);
		});
		$('#editPersonnelBankLeitzahl').keyup(function(){
			loadSuggestions('searchBankCode', [{'triggerElement': '#editPersonnelBankLeitzahl', 'fieldCode': '#editPersonnelBankLeitzahl', 'fieldName': '#editPersonnelBankName', 'fieldBIC': '#editPersonnelBankBIC', 'fieldIBAN': '#editPersonnelBankIBAN'}], 1);
		});
	});
</script>
<?php
	}
?>
<?php require_once('inc/footerHTML.inc.php'); ?>