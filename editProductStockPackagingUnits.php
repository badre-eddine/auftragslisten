<?php
	require_once('inc/requires.inc.php');
	
	if(!$arrGetUserRights["displayStocks"] && !$arrGetUserRights["editStocks"]) {
		header('location: ' . PAGE_EXIT_LOCATION);
		exit;
	}
	
	$jswindowMessage = "";
	$warningMessage = '';
	$errorMessage = '';
	$successMessage = '';
	$infoMessage = '';

	$dbConnection = new DB_Connection();
	$db_open = $dbConnection->db_connect();	

	// BOF READ VERTRETER
		$arrAgentDatas = getAgents();
		$arrAgentDatas2 = getAgents2();
	// EOF READ VERTRETER

	// BOF READ PAYMENT TYPES
		$arrPaymentTypeDatas = getPaymentTypes();
	// EOF READ PAYMENT TYPES

	// BOF READ CUSTOMER GROUP DATAS
		$arrCustomerGroupDatas = getCustomerGroups();
	// EOF READ CUSTOMER GROUP DATAS

	// BOF READ CUSTOMER TYPES
		$arrCustomerTypeDatas = getCustomerTypes();
	// EOF READ CUSTOMER TYPES
	
	// BOF STORE PRODUCTS_VPE	
		if($_POST["storeDatas"] != ""){
			$thisStoreID = $_POST["editID"];
			
			dd('thisStoreID');
			if($thisStoreID == 'NEW'){
				$thisStoreID = '%';
			}
			$sql_storeVPE = "
				REPLACE INTO `" . TABLE_PRODUCT_VPE . "` (
						`productVpeID`,
						`productVpeName`,
						`productVpeSort`,
						`productVpeActive`,
						`productVpeFactor`
				)
				VALUES (
						'" . $thisStoreID . "',
						'" . $_POST["editPackagingUnitDatasName"] . "',
						'" . $_POST["editPackagingUnitDatasSort"] . "',
						'" . $_POST["editPackagingUnitDatasStatus"] . "',
						'" . $_POST["editPackagingUnitDatasFactor"] . "'
				)
			";
			$rs_storeVPE = $dbConnection->db_query($sql_storeVPE);

			if($rs_storeVPE){
				$successMessage .= 'Die VE wurde gespeichert.' . '<br />';
			}
			else {
				$errorMessage .= 'Die VE konnte nicht gespeichert werden.' . '<br />';
			}
			unset($_REQUEST["editID"]);
		}
	// EOF STORE PRODUCTS_VPE
	

	
	// BOF CANCEL PRODUCTS_VPE	
		if($_POST["cancelDatas"] != ""){
			unset($_REQUEST["editID"]);
		}
	// EOF CANCEL PRODUCTS_VPE
	
	// BOF DELETE PRODUCTS_VPE
		if($_POST["deleteDatas"] != ""){
			$sql_deleteVPE = "
					DELETE  
						FROM `" . TABLE_PRODUCT_VPE . "`
						WHERE 1
							AND `productVpeID` = '" . $_POST["editID"] . "'
				";
			$rs_deleteVPE = $dbConnection->db_query($sql_deleteVPE);
			if($rs_deleteVPE){
				$successMessage .= 'Die VE wurde entfernt.' . '<br />';
			}
			else {
				$errorMessage .= 'Die VE konnte nicht entfernt werden.' . '<br />';
			}
			unset($_REQUEST["editID"]);
		}
	// EOF DELETE PRODUCTS_VPE
	
	// BOF GET PRODUCTS_VPE	
		$where = "";
		
		if($_REQUEST["editID"] != '' && $_REQUEST["editID"] != 'NEW'){
			$where = " AND `productVpeID` = '" . $_REQUEST["editID"] . "' ";
		}
	
		$sql_getVPE = "
				SELECT
						 `productVpeID`,
						 `productVpeName`,
						 `productVpeSort`,
						 `productVpeActive`,
						 `productVpeFactor`
					FROM `" . TABLE_PRODUCT_VPE . "`
					WHERE 1
						" . $where . "
						
					ORDER BY
						`productVpeSort`,
						`productVpeName`
			";	
		$rs_getVPE = $dbConnection->db_query($sql_getVPE);

		$arrPackagingUnitData = array();
		$arrSelectedPackagingUnitData = array();
		while($ds_getVPE = mysqli_fetch_assoc($rs_getVPE)){			
			if($_REQUEST["editID"] == ''){				
				foreach(array_keys($ds_getVPE) as $field){
					$arrPackagingUnitData[$ds_getVPE["productVpeID"]][$field] = $ds_getVPE[$field];
				}
			}
			else if($_REQUEST["editID"] != 'NEW'){			
				foreach(array_keys($ds_getVPE) as $field){
					$arrSelectedPackagingUnitData[$field] = $ds_getVPE[$field];
				}
			}
		}	
		#dd('arrPackagingUnitData');
		#dd('arrSelectedPackagingUnitData');
	// EOF GET PRODUCTS_VPE
?>
<?php
	require_once('inc/headerHTML.inc.php');
	$thisTitle = "Artikel-Verpackungseinheiten";
	
	$headerHTML = preg_replace("/{###TITLE###}/", strip_tags($thisTitle), $headerHTML);
	echo $headerHTML;

	require_once(FILE_MENUE_TOP);
	
?>	

<div id="mainArea">
	<div id="mainContent">
		<?php require_once(FILE_MENUE_SIDEBAR); ?>
		<div id="contentArea">
			<a name="top"></a>
			<div id="contentAreaElements">
				<h1><?php if(SHOW_PAGE_TITLE_ICONS) { echo '<img src="' . PATH_ICONS_MENUE_TITLES . 'shipping.png" alt="" />'; } ?> <?php echo $thisTitle; ?></h1>
			
				<?php displayMessages(); ?>
				<?php if($_REQUEST["editID"] == "" && $arrGetUserRights["editStocks"] == '1') { ?>
					<div class="menueActionsArea">
						<a href="<?php echo PAGE_PRODUCTS_STOCK_PACKAGING_UNIT; ?>?editID=NEW" class="linkButton">Neue VE anlegen</a>
						<div class="clear"></div>
					</div>
				<?php } ?>
				
				<div class="adminInfo">Datensatz-ID: <?php echo $_REQUEST["editID"]; ?></div>
				
				<div class="contentDisplay">
				
					<?php					
						if($_REQUEST["editID"] == ''){
							$countRow = 0;
							
							if(!empty($arrPackagingUnitData)){
								echo '<table cellpadding="0" cellspacing="0" border="0" width="100%" class="displayOrders">
									<tr>
										<th style="width:45px;text-align:right;">#</th>
										<th>VPE-Bezeichnung</th>
										<th>Sortierung</th>
										<th>Status</th>
										<th>Faktor</th>
										<th>Info</th>
									</tr>						
								';
							
							
								foreach($arrPackagingUnitData as $thisPackagingUnitKey => $thisPackagingUnitValue){
									if($countRow%2 == 0){ $rowClass = 'row0'; }
									else { $rowClass = 'row1'; }
									
									echo '<tr ' . $thisStyle . ' class="'.$rowClass.'">';
									
									echo '<td style="text-align:right;">';
									echo '<b>' . ($countRow + 1). '.</b>';
									echo '</td>';
									
									echo '<td>';
									echo $thisPackagingUnitValue["productVpeName"];
									echo '</td>';
									
									echo '<td>';
									echo $thisPackagingUnitValue["productVpeSort"];
									echo '</td>';
									
									echo '<td>';
									// echo $thisPackagingUnitValue["productVpeActive"];
									if($thisPackagingUnitValue["productVpeActive"] == '1'){
										$thisStatusImage = 'iconOk.png';
										$thisStatusTitle = 'aktiv';
									}
									else {
										$thisStatusImage = 'iconNotOk.png';
										$thisStatusTitle = 'nicht aktiv';
									}
									
									echo '<img src="layout/icons/' . $thisStatusImage . '" width="16" height="16" alt="' . $thisStatusTitle . '" title="' . $thisStatusTitle . '" />';
									echo '</td>';
									
									echo '<td>';
									echo $thisPackagingUnitValue["productVpeFactor"];
									echo '</td>';
									
									echo '<td>';						
									if($arrGetUserRights["editStocks"]) {
										echo '<span class="toolItem"><a href="' . PAGE_PRODUCTS_STOCK_PACKAGING_UNIT . '?editID=' . $thisPackagingUnitValue["productVpeID"] . '"><img src="layout/icons/iconEdit.gif" width="16" height="16" title="VE  bearbeiten" alt="Bearbeiten" /></a></span>';
									}
									else {
										echo '<span class="toolItem"><img src="layout/spacer.gif" width="16" height="16" alt="" /></span>';
									}
									echo '</td>';
									
									echo '</tr>';
									
									$countRow++;
								}
								
								echo '</table>';
							}
						}
						else {
							?>
							
							<form name="formEditPackagingUnitDatas" id="formEditPackagingUnitDatas" method="post" action="<?php echo $_SERVER["PHP_SELF"]; ?>#tabs-1" enctype="multipart/form-data" >
								<input type="hidden" name="editID" value="<?php echo $_REQUEST["editID"]; ?>" />							
								
								<fieldset class="colored1">
									<legend>VE-Daten</legend>										
									<table border="0" width="100%" cellspacing="0" cellpadding="0">
										<tr>
											<td style="width:200px;"><b>Name:</b></td>
											<td><input type="text" name="editPackagingUnitDatasName" class="inputField_510" value="<?php echo $arrSelectedPackagingUnitData["productVpeName"]; ?>" /></td>
										</tr>
										<tr>
											<td style="width:200px;"><b>Faktor:</b></td>
											<td><input type="text" name="editPackagingUnitDatasFactor" class="inputField_70" value="<?php echo $arrSelectedPackagingUnitData["productVpeFactor"]; ?>" /></td>
										</tr>
										<tr>
											<td style="width:200px;"><b>Status:</b></td>
											<td>
												<?php
													$checked = '';
													if($arrSelectedPackagingUnitData["productVpeActive"] == '1'){
														$checked = ' checked="checked" ';
														
													}
												?>
												<input type="checkbox" name="editPackagingUnitDatasStatus" value="1" <?php echo $checked; ?> /> Aktiv?
											</td>
										</tr>
									</table>
								</fieldset>
							
							
								<div class="actionButtonsArea">
									<?php if($arrGetUserRights["editStocks"] == '1') { ?>
									<input type="submit" class="inputButton1 inputButtonGreen" name="storeDatas" value="Speichern" />			
									&nbsp;
									<input type="submit" class="inputButton1 inputButtonRed" name="deleteDatas" value="Endg&uuml;ltig l&ouml;schen" onclick="return showWarning('Wollen Sie diesen Eintrag wirklich endgültig entfernen???\nOder soll der Eintrag nur deaktiviert werden?');" />
									&nbsp;
									<input type="submit" class="inputButton1 inputButtonOrange" name="resetDatas" value="Abbrechen" onclick="return showWarning('Wollen Sie diesen Vorgang wirklich abbrechen??? ');" />
									<?php } ?>
								</div>
							</form>
							<?php						
						}
					?>
				</div>
				</div>
			<div class="clear"></div>
		</div>
	</div>
</div>

<script language="javascript" type="text/javascript">

</script>
<?php require_once('inc/footerHTML.inc.php'); ?>