<?php
	require_once('inc/requires.inc.php');

	if(!$arrGetUserRights["importDatas"]) {
		header('location: ' . PAGE_EXIT_LOCATION);
		exit;
	}

	$jswindowMessage = "";
	$warningMessage = '';
	$errorMessage = '';
	$successMessage = '';
	$infoMessage = '';

	$dbConnection = new DB_Connection();
	$db_open = $dbConnection->db_connect();
?>

<?php
	require_once('inc/headerHTML.inc.php');
	$thisTitle = '&quot;' . COMPANY_INTERNET . '&quot; - Shop-Artikel-Import';
	$headerHTML = preg_replace("/{###TITLE###}/", strip_tags($thisTitle), $headerHTML);
	echo $headerHTML;

	require_once(FILE_MENUE_TOP);
?>
<div id="mainArea">
	<div id="mainContent">
		<?php require_once(FILE_MENUE_SIDEBAR); ?>
		<div id="contentArea">
			<div id="contentAreaElements">
				<h1><?php if(SHOW_PAGE_TITLE_ICONS) { echo '<img src="' . PATH_ICONS_MENUE_TITLES . 'shop.png" alt="" />'; } ?> <?php echo $thisTitle; ?></h1>
				<div class="contentDisplay">

				<?php displayMessages(); ?>
				<form name="formImportShopTables" method="post" action="<?php echo $_SERVER["PHP_SELF"]; ?>">
					<div class="actionButtonsArea">
						<input type="submit" class="inputButton0" name="submitDatas" value="Shop-Tabellen-Import starten" />
					</div>
				</form>
				<?php
					if($_POST["submitDatas"] != ''){
						set_time_limit(0);

						$infoMessage .= ' START: ' . date("Y-m-d H:i:s") . ' ' .'<br />';
						displayMessages();

						$arrDefinedShopOnlineTables = array();

						$arrDefinedShopOnlineTables[] = "TABLE_CATEGORIES";
						$arrDefinedShopOnlineTables[] = "TABLE_CATEGORIES_DESCRIPTION";
						$arrDefinedShopOnlineTables[] = "TABLE_PRODUCTS_TO_CATEGORIES";
						$arrDefinedShopOnlineTables[] = "TABLE_PRODUCTS";
						$arrDefinedShopOnlineTables[] = "TABLE_PRODUCTS_DESCRIPTION";
						$arrDefinedShopOnlineTables[] = "TABLE_PRODUCTS_ATTRIBUTES";
						$arrDefinedShopOnlineTables[] = "TABLE_PRODUCTS_OPTIONS";
						$arrDefinedShopOnlineTables[] = "TABLE_PRODUCTS_OPTIONS_VALUES";
						$arrDefinedShopOnlineTables[] = "TABLE_PRODUCTS_VPE";
						$arrDefinedShopOnlineTables[] = "TABLE_PERSONAL_OFFERS_BY_CUSTOMERS";

						#dd('arrDefinedShopOnlineTables');

						if(!empty($arrDefinedShopOnlineTables)){
							$arrTemp = array();
							foreach($arrDefinedShopOnlineTables as $thisDefinedShopOnlineTable){
								$thisDefinedShopOnlineTable = constant($thisDefinedShopOnlineTable);
								$thisDefinedShopOnlineTable = preg_replace("/" . MANDATOR . '_shop_' . "/", "", $thisDefinedShopOnlineTable);
								#dd('thisDefinedShopOnlineTable');
								$arrTemp[] = $thisDefinedShopOnlineTable;
							}
							$arrDefinedShopOnlineTables = $arrTemp;
						}

						// BOF READ ALL ONLINE SHOP TABLES
							$arrUseShopOnlineTables = array();
							$dbConnection_ExternShop = new DB_Connection(DB_HOST_EXTERN_SHOP, '', DB_NAME_EXTERN_SHOP, DB_USER_EXTERN_SHOP, DB_PASSWORD_EXTERN_SHOP);
							$dbOpen_ExternShop = $dbConnection_ExternShop->db_connect();

							$sql_extern_getTableNames = "SHOW TABLES";
							$sql_extern_getTableColumnsTemplate = "SHOW COLUMNS FROM `{###TABLE_NAME###}`";
							$sql_extern_getTableDescriptionTemplate = "DESCRIBE `{###TABLE_NAME###}`";

							$rs_extern_getTableNames = $dbConnection_ExternShop->db_query($sql_extern_getTableNames);

							if($rs_extern_getTableNames) {
								$successMessage .= ' Die Online-Shop-Tabellennamen wurden ausgelesen.' .'<br />';
							}
							else {
								$errorMessage .= ' Die Online-Shop-Tabellennamen konnten nicht ausgelesen werden' . '<br />';
							}

							$count = 0;

							$arrTemp = array();
							while($ds_extern_getTableNames = mysqli_fetch_assoc($rs_extern_getTableNames)){

								foreach(array_keys($ds_extern_getTableNames) as $field){
									$thisValue = $ds_extern_getTableNames[$field];
								}

								foreach($arrDefinedShopOnlineTables as $thisDefinedShopOnlineTable){
									if(!in_array($thisValue, $arrTemp)){
										if($thisDefinedShopOnlineTable == $thisValue || preg_match("/^" . $thisDefinedShopOnlineTable . "[0-9]{1,}/", $thisValue)){
											if(!preg_match("/_SICH/", $thisValue)){
												$arrUseShopOnlineTables[$count]["ONLINE"] = $thisValue;
												$arrUseShopOnlineTables[$count]["Auftragslisten"] = MANDATOR . '_shop_' . $thisValue;
												$arrUseShopOnlineTables[$count]["tmpAuftragslisten"] = '_tmp_' . MANDATOR . '_shop_' . $thisValue;
												$arrUseShopOnlineTables[$count]["oldAuftragslisten"] = '_old_' . MANDATOR . '_shop_' . $thisValue;
												$arrUseShopOnlineTables[$count]["TYPE"] = 'TABLE';
												$count++;
												$arrTemp[] = $thisValue;
											}
										}
									}
								}
							}

						// EOF READ ALL ONLINE SHOP TABLES

						displayMessages();

						// BOF CREATE TABLES IF NOT EXIST
							$arrCreateViews = array();

							$sql_extern_getTableStructureTemplate = "SHOW CREATE TABLE `{###TABLE_NAME###}`";
							#dd('arrUseShopOnlineTables');
							if(!empty($arrUseShopOnlineTables)){
								foreach($arrUseShopOnlineTables as $thisTableKey => $thisUseShopOnlineTables){
									$sql_extern_getTableStructure = $sql_extern_getTableStructureTemplate;
									$sql_extern_getTableStructure = str_replace("{###TABLE_NAME###}", $thisUseShopOnlineTables["ONLINE"], $sql_extern_getTableStructure);

									$rs_extern_getTableStructure = $dbConnection_ExternShop->db_query($sql_extern_getTableStructure);
									if($rs_extern_getTableStructure) {
										$successMessage .= ' Die Online-Shop-Tabellenstrukturen wurden ausgelesen.' .'<br />';
									}
									else {
										$errorMessage .= ' Die Online-Shop-Tabellenstrukturen konnten nicht ausgelesen werden' . '<br />';
									}


									while($ds_extern_getTableStructure = mysqli_fetch_assoc($rs_extern_getTableStructure)){

										$sql_local_deleteTable = "DROP TABLE IF EXISTS `" . $thisUseShopOnlineTables["tmpAuftragslisten"] . "`";
										$rs_local_deleteTable = $dbConnection->db_query($sql_local_deleteTable);

										if($rs_local_deleteTable) {
											$successMessage .= ' Die tempor&auml;re Auftragslisten-Shop-Tabelle `' . $thisUseShopOnlineTables["tmpAuftragslisten"] . '` wurde entfernt.' .'<br />';
										}
										else {
											$errorMessage .= ' Die tempor&auml;re Auftragslisten-Shop-Tabelle `' . $thisUseShopOnlineTables["tmpAuftragslisten"] . '` konnte nicht entfernt werden' . '<br />';
										}

										if(isset($ds_extern_getTableStructure["Table"])){
											if($ds_extern_getTableStructure["Create Table"] != ''){
												$sql_local_setTableStructure = $ds_extern_getTableStructure["Create Table"];
												$sql_local_setTableStructure = preg_replace("/(CREATE TABLE `)" . $thisUseShopOnlineTables["ONLINE"] . "(`)/", "$1".$thisUseShopOnlineTables["tmpAuftragslisten"]."$2", $sql_local_setTableStructure);
												$sql_local_setTableStructure = preg_replace("/^CREATE TABLE/", "CREATE TABLE IF NOT EXISTS", $sql_local_setTableStructure);
												#dd('sql_local_setTableStructure');

												$rs_local_setTableStructure = $dbConnection->db_query($sql_local_setTableStructure);
												if($rs_local_setTableStructure) {
													$successMessage .= ' Die tempor&auml;re Auftragslisten-Shop-Tabelle `' . $thisUseShopOnlineTables["tmpAuftragslisten"] . '` wurde erzeugt.' .'<br />';
												}
												else {
													$errorMessage .= ' Die tempor&auml;re Auftragslisten-Shop-Tabelle `' . $thisUseShopOnlineTables["tmpAuftragslisten"] . '` konnte nicht erzeugt werden!' . '<br />';
												}
											}
											else {
												$arrUseShopOnlineTables[$thisTableKey]["TYPE"] = 'VIEW';
												#unset($arrUseShopOnlineTables[$thisTableKey]);
												$warningMessage .= ' Die Online-Shop-Tabelle `' . $thisUseShopOnlineTables["ONLINE"] . '` konnte nicht gefunden werden!' . '<br />';
											}
										}
										else if(isset($ds_extern_getTableStructure["View"])){

											$sql_local_setViewStructure = $ds_extern_getTableStructure["Create View"];

											$thisTableNameBasicTemp = preg_replace("/[0-9]{1,}$/", "", $thisUseShopOnlineTables["tmpAuftragslisten"]);
											$thisTableNameBasicOnline = preg_replace("/[0-9]{1,}$/", "", $thisUseShopOnlineTables["ONLINE"]);
											$thisTableNameBasicAuftragslisten = preg_replace("/[0-9]{1,}$/", "", $thisUseShopOnlineTables["Auftragslisten"]);

											$rs_match = preg_match_all("/`" . $thisTableNameBasicOnline . "[0-9]{1,}`/", $sql_local_setViewStructure, $arrFound);

											if(!empty($arrFound[0])){
												$arrFoundTables = $arrFound[0];
												$arrFoundTables = array_unique($arrFoundTables);
												$thisTableNameSource = $arrFoundTables[1];
												$thisTableNameTarget = $arrFoundTables[0];
											}

											$sql_local_setViewStructure = preg_replace("/(CREATE)(.*)(VIEW)/", "$1 $3", $sql_local_setViewStructure);
											#$sql_local_setViewStructure = preg_replace("/(`)" . $thisUseShopOnlineTables["ONLINE"] . "(`)/", "$1".$thisUseShopOnlineTables["tmpAuftragslisten"]."$2", $sql_local_setViewStructure);

											$arrUseShopOnlineTables[$thisTableKey]["TYPE"] = 'VIEW';
											#unset($arrUseShopOnlineTables[$thisTableKey]);

											$arrCreateViews[] = array(
												"TABLE_KEY" => $thisTableKey,
												"TABLE_SOURCE" => $thisTableNameSource,
												"TABLE_TARGET" => $thisTableNameTarget,
												"TABLE_BASIC_ONLINE" => $thisTableNameBasicOnline,
												"TABLE_BASIC_TMP" => $thisTableNameBasicTemp,
												"TABLE_BASIC_Auftragslisten" => $thisTableNameBasicAuftragslisten,
												"CREATE_VIEW" => $sql_local_setViewStructure,
												"DROP_TABLE" => "DROP TABLE IF EXISTS " . $thisTableNameTarget . " ",
												"CREATE_TABLE" => "CREATE TABLE IF NOT EXISTS " . $thisTableNameTarget . " LIKE " . $thisTableNameSource . " ",
												"INSERT_TABLE_DATAS" => "INSERT  " . $thisTableNameTarget . " SELECT * FROM " . $thisTableNameSource . " ",
											);

											$warningMessage .= ' Die Online-Shop-Tabelle `' . $thisUseShopOnlineTables["ONLINE"] . '` ist eine VIEW und keine TABALLE, wird aber als Tabelle angelegt!' . '<br />';
										}
										flush();
										displayMessages();
									}
								}
							}

						// EOF CREATE TABLES IF NOT EXIST

						displayMessages();

						// BOF GET TABLE DATA
							#$sql_extern_getTableDataTemplate = "SHOW INSERT SELECT * FROM `{###TABLE_NAME###}` ";
							$sql_extern_getTableDataTemplate = "SELECT * FROM `{###TABLE_NAME###}` WHERE 1";
							#$sql_local_insertTableDataTemplate = "INSERT IGNORE INTO `{###TABLE_NAME###}` ({###TABLE_FIELDS###}) VALUES({###TABLE_VALUES###});";
							$sql_local_insertTableDataTemplate = "REPLACE INTO `{###TABLE_NAME###}` ({###TABLE_FIELDS###}) VALUES({###TABLE_VALUES###});";

							function myAddSlashes($string) {
								$newString = $string;
								$newString = addslashes($newString);
								return $newString;
							}

							if(!empty($arrUseShopOnlineTables)){
								foreach($arrUseShopOnlineTables as $thisUseShopOnlineTables){
									if($thisUseShopOnlineTables["TYPE"] == 'TABLE'){
										$sql_extern_getTableData = $sql_extern_getTableDataTemplate;
										$sql_extern_getTableData = str_replace("{###TABLE_NAME###}", $thisUseShopOnlineTables["ONLINE"], $sql_extern_getTableData);

										$rs_extern_getTableData = $dbConnection_ExternShop->db_query($sql_extern_getTableData);

										if($rs_extern_getTableData) {
											$successMessage .= ' Die Daten wurden aus der Online-Shop-Tabelle `' . $thisUseShopOnlineTables["ONLINE"] . '` ausgelesen.' .'<br />';
										}
										else {
											$errorMessage .= ' Die Daten konnten nicht aus der Online-Shop-Tabelle `' . $thisUseShopOnlineTables["ONLINE"] . '` ausgelesen werden' . '<br />';
										}

										flush();
										displayMessages();

										while($ds_extern_getTableData = mysqli_fetch_assoc($rs_extern_getTableData)){
											#dd('ds_extern_getTableData');
											$arrFields = array_keys($ds_extern_getTableData);
											$arrValues = array_values($ds_extern_getTableData);
											$arrValues = array_map("myAddSlashes", $arrValues);
											#dd('arrFields');
											#dd('arrValues');
											$sql_local_insertTableData = $sql_local_insertTableDataTemplate;
											$sql_local_insertTableData = preg_replace("/{###TABLE_NAME###}/", $thisUseShopOnlineTables["tmpAuftragslisten"], $sql_local_insertTableData);
											$sql_local_insertTableData = preg_replace("/{###TABLE_FIELDS###}/", "`" . implode("`, `", $arrFields) . "`", $sql_local_insertTableData);
											$sql_local_insertTableData = preg_replace("/{###TABLE_VALUES###}/", "'" . implode("', '", $arrValues) . "'", $sql_local_insertTableData);

											$rs_local_insertTableData = $dbConnection->db_query($sql_local_insertTableData);
											if($rs_local_insertTableData) {
												$successMessage .= ' Die Daten wurden in die tempor&auml;re Auftragslisten-Shop-Tabelle `' . $thisUseShopOnlineTables["tmpAuftragslisten"] . '` eingelesen.' .'<br />';
											}
											else {
												$errorMessage .= ' Die Daten konnten nicht in die tempor&auml;re Auftragslisten-Shop-Tabelle `' . $thisUseShopOnlineTables["tmpAuftragslisten"] . '` eingelesen werden' . '<br />';
											}
										}
										flush();
										displayMessages();
									}
								}
							}
						// EOF GET TABLE DATA

						displayMessages();

						// BOF INSERT/REPLACE EXISTING TABLE DATA
							$sql_local_insertExistingTableDataTemplate = "REPLACE INTO `{###TABLE_NAME_TO###}` SELECT * FROM `{###TABLE_NAME_FROM###}` WHERE 1";
							if(!empty($arrUseShopOnlineTables)){
								foreach($arrUseShopOnlineTables as $thisUseShopOnlineTables){
									if($thisUseShopOnlineTables["TYPE"] == 'TABLE'){
										$sql_local_insertExistingTableData = $sql_local_insertExistingTableDataTemplate;
										$sql_local_insertExistingTableData = str_replace("{###TABLE_NAME_TO###}", $thisUseShopOnlineTables["tmpAuftragslisten"], $sql_local_insertExistingTableData);
										$sql_local_insertExistingTableData = str_replace("{###TABLE_NAME_FROM###}", $thisUseShopOnlineTables["Auftragslisten"], $sql_local_insertExistingTableData);

										#$rs_local_insertExistingTableData = $dbConnection_ExternShop->db_query($sql_local_insertExistingTableData);
										$rs_local_insertExistingTableData = $dbConnection->db_query($sql_local_insertExistingTableData);

										if($rs_local_insertExistingTableData) {
											$successMessage .= ' Die existierenden Daten aus der Auftragslisten-Shop-Tabelle `' . $thisUseShopOnlineTables["Auftragslisten"] . '` wurden in die Auftragslisten-Shop-Tabelle `' . $thisUseShopOnlineTables["tmpAuftragslisten"] . '` importiert.' . '<br />';
										}
										else {
											$errorMessage .= ' Die existierenden Daten aus der Auftragslisten-Shop-Tabelle `' . $thisUseShopOnlineTables["Auftragslisten"] . '` konnten nicht in die Auftragslisten-Shop-Tabelle `' . $thisUseShopOnlineTables["tmpAuftragslisten"] . '` importiert werden.' . '<br />';
										}
									}
									flush();
									displayMessages();
								}
							}
						// EOF INSERT/REPLACE EXISTING TABLE DATA

						displayMessages();

						// BOF RENAME TABLES
							if(!empty($arrUseShopOnlineTables)){
								foreach($arrUseShopOnlineTables as $thisUseShopOnlineTables){
									if($thisUseShopOnlineTables["TYPE"] == 'TABLE'){
										$sql_local_deleteTableOld = "DROP TABLE IF EXISTS `" . $thisUseShopOnlineTables["oldAuftragslisten"] . "`";
										$rs_local_deleteTableOld = $dbConnection->db_query($sql_local_deleteTableOld);
										if($rs_local_deleteTableOld) {
											$successMessage .= ' Die Auftragslisten-Shop-Tabelle `' . $thisUseShopOnlineTables["oldAuftragslisten"] . '` wurde entfernt' . '<br />';
										}
										else {
											$errorMessage .= ' Die Auftragslisten-Shop-Tabelle konnte nicht `' . $thisUseShopOnlineTables["oldAuftragslisten"] . '` entfernt werden' . '<br />';
										}

										$sql_local_renameTableCurrent = "RENAME TABLE `" . $thisUseShopOnlineTables["Auftragslisten"] . "` TO `" . $thisUseShopOnlineTables["oldAuftragslisten"] . "`";
										$rs_local_renameTableCurrent = $dbConnection->db_query($sql_local_renameTableCurrent);
										if($rs_local_renameTableCurrent) {
											$successMessage .= ' Die Auftragslisten-Shop-Tabelle `' . $thisUseShopOnlineTables["Auftragslisten"] . '` wurde in `' . $thisUseShopOnlineTables["oldAuftragslisten"] . '` umbenannt' . '<br />';
										}
										else {
											$errorMessage .= ' Die Auftragslisten-Shop-Tabelle `' . $thisUseShopOnlineTables["Auftragslisten"] . '` konnte nicht in `' . $thisUseShopOnlineTables["oldAuftragslisten"] . '` umbenannt werden' . '<br />';
										}


										$sql_local_renameTableNew = "RENAME TABLE `" . $thisUseShopOnlineTables["tmpAuftragslisten"] . "` TO `" . $thisUseShopOnlineTables["Auftragslisten"] . "`";
										$rs_local_renameTableNew = $dbConnection->db_query($sql_local_renameTableNew);
										if($rs_local_renameTableNew) {
											$successMessage .= ' Die Auftragslisten-Shop-Tabelle `' . $thisUseShopOnlineTables["tmpAuftragslisten"] . '` wurde in `' . $thisUseShopOnlineTables["Auftragslisten"] . '` umbenannt' . '<br />';
										}
										else {
											$errorMessage .= ' Die Auftragslisten-Shop-Tabelle `' . $thisUseShopOnlineTables["tmpAuftragslisten"] . '` konnte nicht in `' . $thisUseShopOnlineTables["Auftragslisten"] . '` umbenannt werden' . '<br />';
										}
									}
									flush();
									displayMessages();
								}
							}
						// EOF RENAME TABLES

						// BOF CREATE ONLINE VIEWS AS LOKAL TABLE AND INSERT DATAS
							if(!empty($arrCreateViews)){

								foreach($arrCreateViews as $thisViewKey => $thisViewData){
									$sql_dropTable = $thisViewData["DROP_TABLE"];
									$sql_dropTable = preg_replace("/" . $thisViewData["TABLE_BASIC_ONLINE"] . "/", $thisViewData["TABLE_BASIC_Auftragslisten"], $sql_dropTable);

									$rs_dropTable = $dbConnection->db_query($sql_dropTable);
									if($rs_dropTable) {
										$successMessage .= ' Die Auftragslisten-Shop-Tabelle ' . $thisViewData["TABLE_TARGET"] . ' wurde entfernt.' .'<br />';
									}
									else {
										$errorMessage .= ' Die Auftragslisten-Shop-Tabelle ' . $thisViewData["TABLE_TARGET"] . ' konnte nicht entfernt werden!' . '<br />';
									}

									$sql_createViewAsTable = $thisViewData["CREATE_TABLE"];
									$sql_createViewAsTable = preg_replace("/" . $thisViewData["TABLE_BASIC_ONLINE"] . "/", $thisViewData["TABLE_BASIC_Auftragslisten"], $sql_createViewAsTable);

									$rs_createViewAsTable = $dbConnection->db_query($sql_createViewAsTable);
									if($rs_createViewAsTable) {
										$successMessage .= ' Die Auftragslisten-Shop-Tabelle ' . $thisViewData["TABLE_TARGET"] . ' wurde als Tabelle erzeugt.' .'<br />';
									}
									else {
										$errorMessage .= ' Die Auftragslisten-Shop-Tabelle ' . $thisViewData["TABLE_TARGET"] . ' konnte nicht als Tabelle erzeugt werden!' . '<br />';
									}

									$sql_insertDatas = $thisViewData["INSERT_TABLE_DATAS"];
									$sql_insertDatas = preg_replace("/" . $thisViewData["TABLE_BASIC_ONLINE"] . "/", $thisViewData["TABLE_BASIC_Auftragslisten"], $sql_insertDatas);

									$rs_insertDatas = $dbConnection->db_query($sql_insertDatas);
									if($rs_insertDatas) {
										$successMessage .= ' Die Daten wurden in die Auftragslisten-Shop-Tabelle ' . $thisViewData["TABLE_TARGET"] . ' eingelesen.' .'<br />';
									}
									else {
										$errorMessage .= ' Die Daten konnten nicht in die Auftragslisten-Shop-Tabelle ' . $thisViewData["TABLE_TARGET"] . ' eingelesen werden!' . '<br />';
									}
								}
								flush();
								displayMessages();
							}
						// EOF CREATE ONLINE VIEWS AS LOKAL TABLE AND INSERT DATAS

						displayMessages();

						$infoMessage .= ' ENDE: ' . date("Y-m-d H:i:s") . ' ' .'<br />';
					}

				?>
				<?php displayMessages(); ?>
				</div>
			</div>
		</div>
		<div class="clear"></div>
	</div>
</div>

<script language="javascript" type="text/javascript">
	$(document).ready(function() {
		colorRowMouseOver('.displayOrders tbody tr');
		// toggleAreas();
	});
</script>

<?php require_once('inc/footerHTML.inc.php'); ?>