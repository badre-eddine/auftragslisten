<?php
	require_once('inc/requires.inc.php');
	ini_set('memory_limit', '512M');


	if(!$arrGetUserRights["exportCustomers"]) {
		header('location: ' . PAGE_EXIT_LOCATION);
		exit;
	}

	$jswindowMessage = "";
	$warningMessage = '';
	$errorMessage = '';
	$successMessage = '';
	$infoMessage = '';

	$dbConnection = new DB_Connection();
	$db_open = $dbConnection->db_connect();

	$userDatas = getUserDatas();

	// BOF GET SALESMEN
		$arrSalesmenZipcodeDatas = getRelationSalesmenZipcode();
	// EOF GET SALESMEN


	// BOF GET CUSTOMER GROUPS
	$arrCustomerGroups = getCustomerGroups();
	if(empty($arrCustomerGroups)){
		$arrCustomerGroups = array();
	}
	$arrCustomerGroups[0] = array("customerGroupsID" => 7,  "customerGroupsName" => "NO_GROUP");
	// EOF GET CUSTOMER GROUPS

	// BOF GET CUSTOMER TYPES
	$arrCustomerTypes = getCustomerTypes();
	// EOF GET CUSTOMER TYPES

	$thisStoreDir = DIRECTORY_EXPORT_FILES . "exportedMailAdressPerGroup/";
	if(!is_dir($thisStoreDir)){ mkdir($thisStoreDir); }
	
	// BOF GET MYSQL EXPORT PATH BECAUSE OF "secure_file_priv"
		$sql_getMysqlExportPath = "SHOW VARIABLES LIKE 'secure_file_priv' ";
		$rs_getMysqlExportPath = $dbConnection->db_query($sql_getMysqlExportPath);
		list($thisMysqlExportVariableName, $thisMysqlExportPath) = mysqli_fetch_array($rs_getMysqlExportPath);
		if($thisMysqlExportPath != ''){
			#$thisStoreDir = $thisMysqlExportPath;
		}
	// EOF GET MYSQL EXPORT PATH BECAUSE OF "secure_file_priv"
	
	$arrExportedFiles = array();
	foreach($arrCustomerGroups as $thisCustomerGroupKey => $thisCustomerGroupValue){
		$thisExportFile = $thisStoreDir . $thisCustomerGroupValue["customerGroupsName"] . ".csv";
		$arrExportedFiles[] = $thisExportFile;
	}
	$arrExportedFiles[] = $thisStoreDir . "no_Auftragslisten_orders" . ".csv";

	if($_POST["submitFormExportDatas"] != ''){

		foreach($_POST["selectExportGroup"] as $thisCustomerGroupKey){
			$thisExportFileBaseName = $arrCustomerGroups[$thisCustomerGroupKey]["customerGroupsName"];
			if($thisCustomerGroupKey == 'no_Auftragslisten_orders'){
				$thisExportFileBaseName = $thisCustomerGroupKey;
			}
			$thisExportFile = $thisStoreDir . $thisExportFileBaseName . ".csv";
			
			// BOF DELETE EXISTING FILES
				if(file_exists($thisExportFile)){
					$rs_unlink = unlink($thisExportFile);
				}	
				/*
				$command = "rm " . $thisExportFile;
				#$rs_command = exec($command, $arrOutput);
				$rs_command = shell_exec($command);				
				#dd('rs_command');
				#dd('arrOutput');
				*/				
			// BOF DELETE EXISTING FILES

			if($thisCustomerGroupKey >= 0 && $thisCustomerGroupKey != 'no_Auftragslisten_orders'){
				$thisWhere = "";

				if($thisCustomerGroupKey > 0){
					$xxx_thisWhere = " AND (
										`" . TABLE_CUSTOMERS . "`.`customersGruppe` = '" . $thisCustomerGroupKey . "' OR
										`" . TABLE_CUSTOMERS . "`.`customersGruppe` LIKE '" . $thisCustomerGroupKey . ";%' OR
										`" . TABLE_CUSTOMERS . "`.`customersGruppe` LIKE '%;" . $thisCustomerGroupKey . "' OR
										`" . TABLE_CUSTOMERS . "`.`customersGruppe` LIKE '%;" . $thisCustomerGroupKey . ";%'
						) ";

					$thisWhere = " AND ( FIND_IN_SET('" . $thisCustomerGroupKey . "', REPLACE(`" . TABLE_CUSTOMERS . "`.`customersGruppe`, ';', ',')) > 0 ) ";
				}
				else if($thisCustomerGroupKey != 'no_Auftragslisten_orders'){
					$thisWhere = "
							AND (
								(`customersGruppe` IS NULL OR `customersGruppe` = '' OR `customersGruppe` = 0)
								AND (
									`customersTyp` = 1
									OR
									`customersTyp` = 5
									OR
									`customersTyp` = 6
								)
							)
					";
				}

				$sql = "
					SELECT
						`tempTable`.`customersMail`
						/*
						,
						`tempTable`.`customersGruppe`,
						'" . $arrCustomerGroups[$thisCustomerGroupKey]["customerGroupsName"] . "'
						*/

						/* INTO OUTFILE '" . $thisExportFile . "' FIELDS TERMINATED BY ';' LINES TERMINATED BY '\n' */
					FROM (
						SELECT
								`" . TABLE_CUSTOMERS . "`.`customersMail1` AS `customersMail`,
								`" . TABLE_CUSTOMERS . "`.`customersGruppe`

							FROM `" . TABLE_CUSTOMERS . "`
							WHERE 1
								" . $thisWhere . "
								AND `" . TABLE_CUSTOMERS . "`.`customersMail1`  != ''

						UNION

						SELECT
								`" . TABLE_CUSTOMERS . "`.`customersMail2` AS `customersMail`,
								`" . TABLE_CUSTOMERS . "`.`customersGruppe`
							FROM `" . TABLE_CUSTOMERS . "`
							WHERE 1
								" . $thisWhere . "
								AND `" . TABLE_CUSTOMERS . "`.`customersMail2`  != ''
					) AS `tempTable`
				";


			}
			else if($thisCustomerGroupKey == 'no_Auftragslisten_orders'){
				$sql = "
						SELECT

								CONCAT(
									IF(`tempTable`.`customersMail1` != '', `tempTable`.`customersMail1`, ''),
									IF(`tempTable`.`customersMail1` != '' && `tempTable`.`customersMail2` != '', '\\n', ''),
									IF(`tempTable`.`customersMail2` != '', `tempTable`.`customersMail2`, '')
								) AS `customersMail`

								/*
								`tempTable`.`PLZ_AREA`,
								`tempTable`.`customersKundennummer`,
								`tempTable`.`customersFirmenname`,
								CONCAT(`tempTable`.`customersMail1`, ';', `tempTable`.`customersMail2`) AS `customersMail`,
								`tempTable`.`customersActive`,
								`tempTable`.`customersCompanyPLZ`,

								`tempTable`.`customerTypesID`,
								`tempTable`.`customerTypesName`,
								`tempTable`.`customerTypesShortName`,

								`tempTable`.`bctr_orderDocumentsCustomerNumber`,
								`tempTable`.`bctr_orderDocumentsDocumentDate`,
								`tempTable`.`bctr_orderDocumentsNumber`,

								`tempTable`.`b3_orderDocumentsCustomerNumber`,
								`tempTable`.`b3_orderDocumentsDocumentDate`,
								`tempTable`.`b3_orderDocumentsNumber`,

								`tempTable`.`ordersID`,
								`tempTable`.`ordersKundennummer`
								*/

								/* INTO OUTFILE '" . $thisExportFile . "' FIELDS TERMINATED BY ';' LINES TERMINATED BY '\n' */

								FROM (

							-- // BOF Auftragslisten CUSTOMERS NO INVOICES BCTR AND B3

								SELECT
									`common_customers`.`customersID`,
									`common_customers`.`customersKundennummer`,
									`common_customers`.`customersFirmenname`,
									IF(`common_customers`.`customersMail1` IS NULL, '', `common_customers`.`customersMail1`) AS `customersMail1`,
									IF(`common_customers`.`customersMail2` IS NULL, '', `common_customers`.`customersMail2`) AS `customersMail2`,
									`common_customers`.`customersActive`,
									`common_customers`.`customersCompanyPLZ`,
									SUBSTRING(`common_customers`.`customersCompanyPLZ`, 1, 2) AS `PLZ_AREA`,
									`common_customers`.`customersTyp`,


									`common_customerTypes`.`customerTypesID`,
									`common_customerTypes`.`customerTypesName`,
									`common_customerTypes`.`customerTypesShortName`,

									`bctr_orderInvoices`.`orderDocumentsCustomerNumber` AS `bctr_orderDocumentsCustomerNumber`,
									`bctr_orderInvoices`.`orderDocumentsDocumentDate` AS `bctr_orderDocumentsDocumentDate`,
									`bctr_orderInvoices`.`orderDocumentsNumber` AS `bctr_orderDocumentsNumber`,

									`b3_orderInvoices`.`orderDocumentsCustomerNumber` AS `b3_orderDocumentsCustomerNumber`,
									`b3_orderInvoices`.`orderDocumentsDocumentDate` AS `b3_orderDocumentsDocumentDate`,
									`b3_orderInvoices`.`orderDocumentsNumber` AS `b3_orderDocumentsNumber`,

									`common_ordersProcess`.`ordersID`,
									`common_ordersProcess`.`ordersKundennummer`

								FROM `common_customers`

								LEFT JOIN `common_customerTypes`
								ON(`common_customers`.`customersTyp` = `common_customerTypes`.`customerTypesID`)

								LEFT JOIN `bctr_orderInvoices`
								ON(`common_customers`.`customersKundennummer` = `bctr_orderInvoices`.`orderDocumentsCustomerNumber`)

								LEFT JOIN `b3_orderInvoices`
								ON(`common_customers`.`customersKundennummer` = `b3_orderInvoices`.`orderDocumentsCustomerNumber`)

								LEFT JOIN `common_ordersProcess`
								ON(`common_customers`.`customersKundennummer` = `common_ordersProcess`.`ordersKundennummer`)

								WHERE 1
									AND `common_customers`.`customersActive` = '1'
									AND `common_customers`.`customersCompanyCountry` = '81'
									AND (
										`common_customers`.`customersMail1` != ''
										OR `common_customers`.`customersMail2` != ''
									)
									AND (
										`common_customerTypes`.`customerTypesID` = '1'
										OR `common_customerTypes`.`customerTypesID` = '5'
										OR `common_customerTypes`.`customerTypesID` = '6'
									)
									AND `bctr_orderInvoices`.`orderDocumentsNumber` IS NULL
									AND `b3_orderInvoices`.`orderDocumentsNumber` IS NULL
									AND `common_ordersProcess`.`ordersKundennummer` IS NULL


							-- // EOF Auftragslisten CUSTOMERS NO INVOICES BCTR AND B3

							) AS `tempTable`

							GROUP BY
								`tempTable`.`customersKundennummer`

							ORDER BY
								`tempTable`.`customersCompanyPLZ` ASC
					";
			}

			if($sql != ''){
				$rs = $dbConnection->db_query($sql);
				$fp_export = fopen($thisExportFile, "a+");
				while($ds = mysqli_fetch_assoc($rs)){
					fwrite($fp_export, $ds["customersMail"] . "\n");
				}
				fclose($fp_export);
			}
		}

		/*
			SELECT
				`AuftragslistenCustomerMailAdress`,
				COUNT(`AuftragslistenCustomerGroupID`) AS `countID`
				FROM `_AuftragslistenCustomerMailExport`
				WHERE 1
				GROUP BY `AuftragslistenCustomerMailAdress`
			HAVING `countID` > 1

			-- -----------------------

			SELECT
				*
				FROM `_AuftragslistenCustomerMailExport`
			-> 7375

			-- -----------------------
			SELECT
				*
				FROM `_AuftragslistenCustomerMailExport`
				LEFT JOIN `newsletter_recipients`
				ON(`_AuftragslistenCustomerMailExport`.`AuftragslistenCustomerMailAdress` = `newsletter_recipients`.`customers_email_address`)
			-> 6758
			-- -----------------------
			SELECT
				*
				FROM `_AuftragslistenCustomerMailExport`
				LEFT JOIN `newsletter_recipients`
				ON(`_AuftragslistenCustomerMailExport`.`AuftragslistenCustomerMailAdress` = `newsletter_recipients`.`customers_email_address`)


				HAVING `newsletter_recipients`.`customers_email_address` IS NULL
			-- -----------------------

			SELECT
				*
				FROM `_AuftragslistenCustomerMailExport`
				WHERE `AuftragslistenCustomerMailAdress`
				NOT IN (
					SELECT
						`customers_email_address`
						FROM `newsletter_recipients`
						WHERE 1
				)
			-- -----------------------
			DELETE
				`_AuftragslistenCustomerMailExport`
				FROM `_AuftragslistenCustomerMailExport`
				INNER JOIN `newsletter_recipients`
				ON(`_AuftragslistenCustomerMailExport`.`AuftragslistenCustomerMailAdress` = `newsletter_recipients`.`customers_email_address`)


		-- -----------------------
		-- ANAG		`AuftragslistenCustomerGroupID`:3 -> shopGROUPID: 6
		-- NO_GROUP `AuftragslistenCustomerGroupID`:0 -> shopGROUPID: 3
		-- AGN `AuftragslistenCustomerGroupID`:7 ->  shopGROUPID: 17
		-- AGN `AuftragslistenCustomerGroupID`:1 ->  shopGROUPID: 15
		SELECT `AuftragslistenCustomerMailAdress` FROM `_AuftragslistenCustomerMailExport` WHERE 1 AND `AuftragslistenCustomerGroupID` = 3

		--
		SELECT
			*
			FROM `newsletter_recipients`
			INNER JOIN `_AuftragslistenCustomerMailExport`
			ON(`newsletter_recipients`.`customers_email_address` = `_AuftragslistenCustomerMailExport`.`AuftragslistenCustomerMailAdress`)

		-- -----------------------

		UPDATE
			`newsletter_recipients`
			INNER JOIN `_AuftragslistenCustomerMailExport`
			ON(`newsletter_recipients`.`customers_email_address` = `_AuftragslistenCustomerMailExport`.`AuftragslistenCustomerMailAdress`)

			SET `customers_status` =
				IF(`AuftragslistenCustomerGroupID` = 3, 6,
					IF(`AuftragslistenCustomerGroupID` = 7, 17,
						IF(`AuftragslistenCustomerGroupID` = 1, 15,
							IF(`AuftragslistenCustomerGroupID` = 0, 3,
								`customers_status`
							)
						)
					)
				)
		-- -----------------------
		UPDATE
			`newsletter_recipients`
			INNER JOIN `_AuftragslistenCustomerMailExport`
			ON(`newsletter_recipients`.`customers_email_address` = `_AuftragslistenCustomerMailExport`.`AuftragslistenCustomerMailAdress`)

			SET `customers_status` = CASE
				WHEN `AuftragslistenCustomerGroupID` = 3 THEN 6
				WHEN `AuftragslistenCustomerGroupID` = 7 THEN 17
				WHEN `AuftragslistenCustomerGroupID` = 1 THEN 15
				WHEN `AuftragslistenCustomerGroupID` = 0 THEN 3
				ELSE `customers_status`
			END
		*/
	}

	// BOF DOWNLOAD FILE
	if($_GET["downloadFile"] != "") {
		require_once('classes/downloadFile.class.php');
		$thisDownload = new downloadFile($thisStoreDir, $_GET["downloadFile"]);
		$errorMessage .= $thisDownload->startDownload();
	}
	// EOF DOWNLOAD FILE
?>

<?php
	require_once('inc/headerHTML.inc.php');
	$thisTitle = "Kunden-Mailadressen Export nach Gruppen gesplittet";
	$headerHTML = preg_replace("/{###TITLE###}/", strip_tags($thisTitle), $headerHTML);
	echo $headerHTML;

	require_once(FILE_MENUE_TOP);
?>
<div id="mainArea">
	<div id="mainContent">
		<?php require_once(FILE_MENUE_SIDEBAR); ?>
		<div id="contentArea">
			<div id="contentAreaElements">
				<h1><?php if(SHOW_PAGE_TITLE_ICONS) { echo '<img src="' . PATH_ICONS_MENUE_TITLES . 'export.png" alt="" />'; } ?> <?php echo $thisTitle; ?></h1>

				<div class="contentDisplay">
					<div id="searchFilterArea">
					<form name="formExportDatas" method="post" action="<?php echo $_SERVER["REQUEST_URI"]; ?>" enctype="multipart/form-data">
						<table border="0" cellpadding="0" cellspacing="0" width="" class="searchFilterContent">
							<tr>
								<td><b>Kunden-Gruppe:</b></td>
								<td>
									<select class="inputSelect_427" name="selectExportGroup[]">
										<option value=""> ALLE GRUPPEN </option>
										<?php
											if(!empty($arrCustomerGroups)){
												foreach($arrCustomerGroups as $thisCustomerGroupKey => $thisCustomerGroupValue) {
													$selected = '';
													if(in_array($thisCustomerGroupKey, $_POST["selectExportGroup"])){
														$selected = ' selected="selected" ';
													}
													echo '<option value="' . $thisCustomerGroupKey . '" ' . $selected . ' >' . $thisCustomerGroupValue["customerGroupsName"] . '</option>';
												}
											}
											echo '<option value="no_Auftragslisten_orders" ' . $selected . ' >' . 'Kunden ohne Bestellungen' . '</option>';
										?>
									</select>
								</td>
								<!--
								<td style="width:200px;"><input type="checkbox" name="searchCustomerProducts" <?php if($_POST["searchCustomerProducts"] == '1'){ echo ' checked="checked" '; } ?> value="1" /> <b>inkl. Kunden-Produkte?</b></td>
								<td>&nbsp;</td>

								<td style="width:200px;"><input type="checkbox" name="searchCustomerLastOrder" <?php if($_POST["searchCustomerLastOrder"] == '1'){ echo ' checked="checked" '; } ?> value="1" /> <b>inkl. letzte KZH-Bestellung?</b></td>
								<td>&nbsp;</td>
								-->
								<td><input type="submit" name="submitFormExportDatas" class="inputButton1" value="Export starten" /></td>
							</tr>
						</table>
					</form>
					</div>
					<?php displayMessages(); ?>
					<?php
						if(!empty($arrExportedFiles)){
							foreach($arrExportedFiles as $thisExportedFiles){
								$downloadFilePath = $thisStoreDir . basename($thisExportedFiles);

								if(file_exists($downloadFilePath)) {
									$downloadPath = str_replace(BASEPATH, "", $downloadFilePath);
									$thisDownloadLink = '<p class="dataTableContent"><b>DOWNLOAD-LINK: </b><img src="layout/icons/icon' . strtoupper(getFileType(basename($downloadPath))) . '.gif" height="16" width="16" alt="" />
															<a href="' . $_SERVER["REQUEST_URI"] . '?documentType=&downloadFile=' . basename($downloadPath) . '">' . basename($downloadPath) . '</a> (Stand: ' . date("d.m.Y H:i:s", filemtime($downloadPath)) . ')
														</p>';
									echo $thisDownloadLink;
								}
								clearstatcache();
							}
						}
					?>
				</div>
			</div>
		</div>
		<div class="clear"></div>
	</div>
</div>
<?php
	if($arrGetUserRights["editPersonnel"]) {
?>
<script language="javascript" type="text/javascript">
	$(document).ready(function() {
		$(function() {
			$( "#datepicker" ).datepicker();
			$.datepicker.setDefaults($.datepicker.regional[""]);
			$('#editPersonnelVacationsStart').datepicker($.datepicker.regional["de"]);
		});
		$(function() {
			$('#tabs').tabs();
		});
		setFormDutyFields(<?php echo $jsonFormDutyFields; ?>);
		colorRowMouseOver('.displayOrders tbody tr');
	});
</script>
<?php
	}
?>
<?php require_once('inc/footerHTML.inc.php'); ?>