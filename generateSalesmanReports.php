<?php
	require_once('inc/requires.inc.php');

	if(!$arrGetUserRights["exportCustomers"]) {
		header('location: ' . PAGE_EXIT_LOCATION);
		exit;
	}

	$jswindowMessage = "";
	$warningMessage = '';
	$errorMessage = '';
	$successMessage = '';
	$infoMessage = '';

	$dbConnection = new DB_Connection();
	$db_open = $dbConnection->db_connect();

	$userDatas = getUserDatas();

	$zipFileName = "fahrtenbuecher";
	$downloadFolder = "fahrtenbuecher/";

	$downloadFilePath = DIRECTORY_EXPORT_FILES . $zipFileName.'.zip';

	// BOF GET SALESMEN
		$arrPersonnelDatas = getPersonnelDatas(7);
	// EOF GET SALESMEN

	// BOF DOWNLOAD FILE
	if($_GET["downloadFile"] != "") {
		require_once('classes/downloadFile.class.php');
		$thisDownload = new downloadFile(DIRECTORY_EXPORT_FILES, $_GET["downloadFile"]);
		$errorMessage .= $thisDownload->startDownload();
	}
	// EOF DOWNLOAD FILE

	if($_POST["searchSalesman"] != '' && $_POST["searchYear"] != '' && $_POST["submitSearch"] != '' ){
		putenv("ZIP=" . ZIP_PATH);
		putenv("UNZIP=" . UNZIP_PATH);

		if(!is_dir(DIRECTORY_EXPORT_FILES . $downloadFolder)){
			mkdir(DIRECTORY_EXPORT_FILES . $downloadFolder);
		}

		error_reporting(E_ALL && ~E_NOTICE);
		#error_reporting(E_ALL);

		require_once (PATH_PHP_EXCEL . 'Classes/PHPExcel.php');

		$arrSalesmanDatas = array();

		if(!empty($arrPersonnelDatas)){
			if($_POST["searchSalesman"] == 'all'){
				foreach($arrPersonnelDatas as $thisKey => $thisValue){
					if($thisValue["personnelActive"] == "1"){
						$arrSalesmanDatas[] = $thisValue["personnelFirstName"] . " " . $thisValue["personnelLastName"];
					}
				}
			}
			else {
				$arrSalesmanDatas[] = $arrPersonnelDatas[$_POST["searchSalesman"]]["personnelFirstName"] . " " . $arrPersonnelDatas[$_POST["searchSalesman"]]["personnelLastName"];
			}
		}

		// BOF CONFIG BASIC DATAS FOR EXCEL
		$arrUseMonths = array(
			"01" => "Januar",
			"02" => "Februar",
			"03" => "März",
			"04" => "April",
			"05" => "Mai",
			"06" => "Juni",
			"07" => "Juli",
			"08" => "August",
			"09" => "September",
			"10" => "Oktober",
			"11" => "November",
			"12" => "Dezember",
		);

		$arrSelectedYears = array($_POST["searchYear"]);

		$excelBasicFileName = "Fahrtenbuch_{###SALESMAN###}_{###YEAR###}-{###MONTH###}.xls";
		$excelSheetName = "SEITE {###SEITE###}";
		$excelNumberOfSheets = 22;
		$excelRowHeight = 17;
		$excelLastHeaderRow = 10;

		$excelLogoPath = 'C:/Auftragslisten_SERVER/webroot/Auftragslisten/documents_common/documentsDownload/burhan_logo_excel.png';

		$excelTemplatePath = "documents_common/documentsDownload/Fahrtenbuch_template.xls";
		#$excelFileExcelVersion = 'Excel5';
		#$excelFileExcelVersion = 'Excel2007';
		$excelFileExcelVersion = PHPExcel_IOFactory::identify($excelTemplatePath);

		$arrReplaceFields = array(
				"A1" => "Fahrtenbuch {###MONAT_NAME###} {###JAHR###} - Seite {###SEITE###}",
				"H3" => "SUMME {###MONAT_ZAHL###}.{###JAHR###} - Seite {###SEITE###}",
				"B3" => "{###NAME###}",
			);
		// EOF CONFIG BASIC DATAS FOR EXCEL
#dd('arrPersonnelDatas');
#dd('arrSalesmanDatas');

		// BOF GENERATE EXCEL
		if(!empty($arrSalesmanDatas)){
			$arrCreatedFileNames = array();
			foreach($arrSalesmanDatas as $thisSalesman){
				$thisStoreDir = DIRECTORY_EXPORT_FILES . $downloadFolder . convertChars($thisSalesman) . '/';
				if(!is_dir($thisStoreDir)){
					mkdir($thisStoreDir);
				}
				foreach($arrUseMonths as $thisMonthKey => $thisMonthValue){

					$objPHPExcel = new PHPExcel();

					$fileNameCreate = $excelBasicFileName;

					$fileNameCreate = preg_replace("/{###SALESMAN###}/", convertChars($thisSalesman), $fileNameCreate);
					$fileNameCreate = preg_replace("/{###YEAR###}/", $_POST["searchYear"], $fileNameCreate);
					$fileNameCreate = preg_replace("/{###MONTH###}/", $thisMonthKey, $fileNameCreate);

					$arrCreatedFileNames[] = $fileNameCreate;

					$objReader = PHPExcel_IOFactory::createReader($excelFileExcelVersion);
					$objReader->setIncludeCharts(true);
					$objPHPExcel = $objReader->load($excelTemplatePath);

					// BOF ADD SHEETS
					$arrSheets = $objPHPExcel->getSheetNames();
					/*
					if($excelNumberOfSheets > count($arrSheets)){
						$numberAddSheets = ($excelNumberOfSheets - count($arrSheets));
						for($i = (count($arrSheets) + 1) ; $i < ($excelNumberOfSheets + 1) ; $i++){
							$thisWorksheet = $objPHPExcel->setActiveSheetIndex(0);
							$addWorksheet = clone $thisWorksheet;
							#$addWorksheet->setTitle(preg_replace("/{###SEITE###}/", ($i), $excelSheetName));
							#$objPHPExcel->addSheet($addWorksheet, $i);
						}
					}
					*/
					// EOF ADD SHEETS

					$arrSheets = $objPHPExcel->getSheetNames();

					if(!empty($arrSheets)){
						foreach($arrSheets as $thisSheetKey => $thisSheetValue){
							$thisWorksheet = $objPHPExcel->setActiveSheetIndex($thisSheetKey);

							// BOF HEADER IMAGE
							#$logo = new PHPExcel_Worksheet_HeaderFooterDrawing();
							#$logo->setName('Logo');
							#$logo->setDescription('Logo');
							#$logo->setName('Logo');
							#$logo->setPath($excelLogoPath); //Path is OK & tested under PHP
							#$logo->setHeight(300);
							#$thisWorksheet->getHeaderFooter()->addImage($logo, PHPExcel_Worksheet_HeaderFooter::IMAGE_HEADER_RIGHT);
							#$currentHeader = $thisWorksheet->getHeaderFooter()->getOddHeader();
							#$thisWorksheet->getHeaderFooter()->setOddHeader($currentHeader.'&C&G');
							// EOF HEADER IMAGE

							foreach($arrReplaceFields as $thisReplaceFieldKey => $thisReplaceFieldValue){
								$thisReplaceFieldValue = $thisReplaceFieldValue;
								$thisReplaceFieldValue = preg_replace("/{###NAME###}/", $thisSalesman, $thisReplaceFieldValue);
								$thisReplaceFieldValue = preg_replace("/{###MONAT_NAME###}/", $thisMonthValue, $thisReplaceFieldValue);
								$thisReplaceFieldValue = preg_replace("/{###MONAT_ZAHL###}/", $thisMonthKey, $thisReplaceFieldValue);
								$thisReplaceFieldValue = preg_replace("/{###JAHR###}/", $_POST["searchYear"], $thisReplaceFieldValue);
								$thisReplaceFieldValue = preg_replace("/{###SEITE###}/", ($thisSheetKey + 1), $thisReplaceFieldValue);

								$thisWorksheet->setCellValue($thisReplaceFieldKey, $thisReplaceFieldValue);
								clearstatcache();
							}
							$newSheetName = $excelSheetName;
							$newSheetName = preg_replace("/{###SEITE###}/", ($thisSheetKey + 1), $excelSheetName);
							$thisWorksheet->setTitle($newSheetName);
							clearstatcache();
							$countRow = 1;
							foreach($thisWorksheet->getRowDimensions() as $rd) {
								if($countRow > $excelLastHeaderRow){
									$rd->setRowHeight($excelRowHeight);
								}
								$countRow++;
								clearstatcache();
							}
							/*
							foreach(range('A','Z') as $columnID) {
								$thisWorksheet->getColumnDimension($columnID)
											->setAutoSize(true);
							}
							*/
						}
						clearstatcache();
						$objPHPExcel->setActiveSheetIndex(0);
						clearstatcache();
					}

					/*
					$newSheet = clone $objPHPExcel->getSheetByName("sheet_template");
					$newSheet->setTitle('New Sheet');
					$newSheetIndex = 1;
					$objPHPExcel->addSheet($newSheet,$newSheetIndex);
					----------
					$objPHPExcel->setActiveSheetIndex(0)
							->setCellValue('A1', 'Hello')
							->setCellValue('B1', 'World!');
					*/

					if(file_exists($thisStoreDir . $fileNameCreate)){ unlink($thisStoreDir . $fileNameCreate); }
					$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, $excelFileExcelVersion);
					$excelResult = $objWriter->save($thisStoreDir . $fileNameCreate);

					$excelResult = file_exists($thisStoreDir . $fileNameCreate);

					if($excelResult) {
						$successMessage .= ' Die Excel-Datei &quot; ' . $fileNameCreate . ' &quot; wurden erstellt.' .'<br />';
					}
					else {
						$errorMessage .= ' Die Excel-Datei &quot; ' . $fileNameCreate . ' &quot; konnte nicht erstellt werden.' .'<br />';
					}
					clearstatcache();
					$objPHPExcel->disconnectWorksheets();
					unset($objPHPExcel);
				}
				clearstatcache();
			}
			$downloadFile = false;


			if(file_exists(DIRECTORY_EXPORT_FILES . $zipFileName . '.zip')){
				unlink(DIRECTORY_EXPORT_FILES . $zipFileName . '.zip');
			}

			$createCommand = ZIP_PATH.' a '.DIRECTORY_EXPORT_FILES . $zipFileName.'.zip ' . DIRECTORY_EXPORT_FILES . $downloadFolder . '';
			$arrExecResults[] = $createCommand;
			$arrExecResults[] = exec($createCommand, $error);
			if(!empty($error)) {
				$arrExecResults[] = array('1. ZIP: error', $error);
			}
			/*
			if(!empty($arrCreatedFileNames)){
				foreach($arrCreatedFileNames as $thisFileName) {
					if(file_exists(DIRECTORY_EXPORT_FILES . $downloadFolder . $thisFileName)){
						$createCommand = ZIP_PATH.' a -tzip '.DIRECTORY_EXPORT_FILES . $downloadFolder . $zipFileName.'.zip '.DIRECTORY_EXPORT_FILES . $downloadFolder  . $downloadFolder.$thisFileName.'';
						$arrExecResults[] = $createCommand;
						$arrExecResults[] = exec($createCommand, $error);
						if(!empty($error)) {
							$arrExecResults[] = array('1. ZIP: error', $error);
						}
						unlink(DIRECTORY_EXPORT_FILES . $downloadFolder  . $downloadFolder . $thisFileName);
					}
				}
			}
			*/
			if(DIRECTORY_EXPORT_FILES . $downloadFolder != ''){
				if(is_dir(DIRECTORY_EXPORT_FILES . $downloadFolder)){
					$createCommand = 'rmdir "' . DIRECTORY_EXPORT_FILES . $downloadFolder . '" /s /q';
					$arrExecResults[] = $createCommand;
					$arrExecResults[] = exec($createCommand, $error);
					if(!empty($error)) {
						$arrExecResults[] = array('2. DEL: error', $error);
					}
				}
			}
		}
		else {
			$errorMessage . ' Es wurden keine Vertreter gefunden! ' . '<br />';
		}
		// EOF GENERATE EXCEL

	}
?>
<?php
	require_once('inc/headerHTML.inc.php');
	$thisTitle = "Fahrberichte f&uuml;r Vertreter generieren";
	$headerHTML = preg_replace("/{###TITLE###}/", strip_tags($thisTitle), $headerHTML);
	echo $headerHTML;

	require_once(FILE_MENUE_TOP);
?>
<div id="mainArea">
	<div id="mainContent">
		<?php require_once(FILE_MENUE_SIDEBAR); ?>
		<div id="contentArea">
			<div id="contentAreaElements">
				<h1><?php if(SHOW_PAGE_TITLE_ICONS) { echo '<img src="' . PATH_ICONS_MENUE_TITLES . 'export.png" alt="" />'; } ?> <?php echo $thisTitle; ?></h1>

				<div id="searchFilterArea">
					<form name="formFilterSearch" method="post" action="<?php echo $_SERVER["REDIRECT_URL"]; ?>">
						<table border="0" cellpadding="0" cellspacing="0" width="" class="searchFilterContent">
							<tr>
								<td>
									<label for="searchYear">Jahr:</label>
									<select name="searchYear" class="inputSelect_70">
										<option value=""></option>
										<?php
											$todayYear = intval(date("Y"));
											$todayMonth = intval(date("m"));
											for($k = $todayYear ; $k < ($todayYear + 2) ; $k++){
												$thisYear = $k;
												$thisOptionClass = 'row0';
												if($thisYear == $todayYear){ $thisOptionClass = 'row3'; }
												else if($thisYear%2 == 0){ $thisOptionClass = 'row1'; }

												$thisValue = $thisYear ;
												$selected = '';
												if($thisValue == $_POST["searchYear"]){
													$selected = ' selected="selected" ';
												}
												echo '<option class="' . $thisOptionClass . '" value="' . $thisValue . '" ' . $selected . '>' . $thisYear . '</option>';
											}
										?>
									</select>
								</td>
								<td>
									<label for="searchSalesman">Vertreter:</label>
									<select type="text" name="searchSalesman" id="searchSalesman" class="inputSelect_120">
										<option value="all"> ALLE </option>
										<?php
											if(!empty($arrPersonnelDatas)){
												foreach($arrPersonnelDatas as $thisKey => $thisValue){
													#echo '<option value="' . $thisKey . '">' . $thisValue["personnelFirstName"] . " " . $thisValue["personnelLastName"] . '</option>';
												}
											}
										?>
									</select>
									 <span class="infotext">(mehrfache Auswahl m&ouml;glich)</span>
								</td>
								<td>
									<input type="submit" name="submitSearch" class="inputButton0" value="Fahrberichte erstellen" />
								</td>
							</tr>
						</table>
					</form>
				</div>

				<div class="contentDisplay">
					<p></p>
					<?php
						if($warningMessage != "") {	echo '<p class="warningArea">'.$warningMessage.'</p>'; }
						if($errorMessage != "") {	echo '<p class="errorArea">'.$errorMessage.'</p>'; }
						if($successMessage != "") {	echo '<p class="successArea">'.$successMessage.'</p>'; }
						if($infoMessage != "") {	echo '<p class="infoArea">'.$infoMessage.'</p>'; }

						if(file_exists($downloadFilePath)) {
							$downloadPath = str_replace(BASEPATH, "", $downloadFilePath);
							$thisDownloadLink = '<p class="dataTableContent"><b>DOWNLOAD-LINK: </b><img src="layout/icons/icon' . getFileType(basename($downloadPath)) . '.gif" height="16" width="16" alt="" />
													<a href="' . $_SERVER["REQUEST_URI"] . '?documentType=&downloadFile=' . basename($downloadPath) . '">' . basename($downloadPath) . '</a> (Stand: ' . date("d.m.Y H:i:s", filemtime($downloadPath)) . ')
												</p>';
							echo $thisDownloadLink;
						}
						clearstatcache();

						if(!empty($arrExecResults)){
							dd('arrExecResults');
						}
					?>
				</div>
			</div>
		</div>
		<div class="clear"></div>
	</div>
</div>

<script language="javascript" type="text/javascript">
	$(document).ready(function() {
		$(function() {
			$( "#datepicker" ).datepicker();
			$.datepicker.setDefaults($.datepicker.regional[""]);
		});
		$(function() {
			$('#tabs').tabs();
		});
		setFormDutyFields(<?php echo $jsonFormDutyFields; ?>);
		colorRowMouseOver('.displayOrders tbody tr');
	});
</script>
<?php require_once('inc/footerHTML.inc.php'); ?>