<?php
	require_once('inc/requires.inc.php');

	if(!$arrGetUserRights["displayDistribution"]) {
		header('location: ' . PAGE_EXIT_LOCATION);
		exit;
	}

	$jswindowMessage = '';
	$warningMessage = '';
	$errorMessage = '';
	$successMessage = '';
	$infoMessage = '';


	$dbConnection = new DB_Connection();
	$db_open = $dbConnection->db_connect();

	// BOF READ VERTRETER
		$arrSalesmenDatas = getSalesmen();
		$arrSalesmenDatas2 = getSalesmen2();
	// EOF READ VERTRETER

	$defaultORDER = "customersEntryDate";
	$defaultSORT = "ASC";

	if($_POST["searchInterval"] == ""){
		$_POST["searchInterval"] = "WEEK";
	}
	if($_POST["searchSource"] == ""){
		$_POST["searchSource"] = "CONFIRMATIONS";
	}

	// BOF READ MANDATORIES
		$arrMandatoryDatas = getMandatories();
	// EOF READ MANDATORIES

	// BOF READ BESTELL ARTEN
		$arrOrderTypeDatas = getOrderTypes();
	// EOF READ BESTELL ARTEN

	// BOF GET DOUBLE ZIP CODES
		$arrDoubleZipcodes = getDoubleZipcodes();
		$arrUnusedDoubleZipcodes = getDoubleZipcodes('0');
	// BOF GET DOUBLE ZIP CODES

	// BOF GET RELATION SALESMEN ZIPCODE
	if($_POST["searchPLZ"] != ''){
		$arrRelationSalesmenZipcodeDatas = getRelationSalesmenZipcode($_POST["searchPLZ"], 'SALESMEN_ALL');
	}
	else {
		$arrRelationSalesmenZipcodeDatas = getRelationSalesmenZipcode('', 'SALESMEN_ALL');
	}
	if(!empty($arrRelationSalesmenZipcodeDatas)){
		$arrRelationSalesmenZipcodeDatas2 = array();
		foreach($arrRelationSalesmenZipcodeDatas as $thisValue){
			$arrRelationSalesmenZipcodeDatas2[$thisValue["kundenID"]][$thisValue["kundenPLZ"]] = $thisValue;
		}
	}

	/*
	if($_POST["searchSalesman"] != '' || $_POST["searchPLZ"] != ''){
		$thisDataKey = $_POST["searchSalesman"];
		if($_POST["searchPLZ"] != ''){
			$arrTemp = array_keys($arrRelationSalesmenZipcodeDatas2);
			$thisDataKey = $arrTemp[0];
		}

		$arrSalesmanZipcodes = array_keys($arrRelationSalesmenZipcodeDatas2[$thisDataKey]);
		$arrTemp = array();
		foreach($arrSalesmanZipcodes as $thisValue){
			$arrTemp[$thisValue] = $arrDoubleZipcodes[$thisValue];
		}
		$arrDoubleZipcodes = $arrTemp;
	}
	*/
	if($_POST["searchSalesman"] != ''){
		$thisDataKey = $_POST["searchSalesman"];
		if($_POST["searchPLZ"] != ''){
			$arrTemp = array_keys($arrRelationSalesmenZipcodeDatas2);
			$thisDataKey = $arrTemp[0];
		}

		$arrSalesmanZipcodes = array_keys($arrRelationSalesmenZipcodeDatas2[$thisDataKey]);
		$arrTemp = array();
		foreach($arrSalesmanZipcodes as $thisValue){
			$arrTemp[$thisValue] = $arrDoubleZipcodes[$thisValue];
		}
		$arrDoubleZipcodes = $arrTemp;
	}
	if($_POST["searchPLZ"] != ''){
		$arrTemp = array();
		$arrTemp = $arrDoubleZipcodes[$_POST["searchPLZ"]];
		$arrDoubleZipcodes = array();
		$arrDoubleZipcodes[$_POST["searchPLZ"]] = $arrTemp;
	}
	// BOF GET RELATION SALESMEN ZIPCODE
?>
<?php
	require_once('inc/headerHTML.inc.php');
	$thisTitle = "Kennzeichenhalter pro PLZ";

	if($_POST["searchInterval"] == 'WEEK') {
		$thisTitle .= ': <span class="headerSelectedEntry">pro Woche</span>';
	}
	else if($_POST["searchInterval"] == 'MONTH') {
		$thisTitle .= ': <span class="headerSelectedEntry">pro Monat</span>';
	}
	else if($_POST["searchInterval"] == 'QUARTER') {
		$thisTitle .= ': <span class="headerSelectedEntry">pro Vierteljahr</span>';
	}
	else if($_POST["searchInterval"] == 'YEAR') {
		$thisTitle .= ': <span class="headerSelectedEntry">pro Jahr</span>';
	}

	if($_POST["searchSalesman"] != ''){
		$thisTitle .= ' - <span class="headerSelectedEntry">Gebiet von ' . $arrSalesmenDatas[$_POST["searchSalesman"]]["customersFirmenname"] . '</span>';
	}

	$headerHTML = preg_replace("/{###TITLE###}/", strip_tags($thisTitle), $headerHTML);
	echo $headerHTML;

	require_once(FILE_MENUE_TOP);

	$sqlSortField = "";
	$sqlSortDirection = "";
	$sqlWhere = "";

?>

<div id="mainArea">
	<div id="mainContent">
		<?php require_once(FILE_MENUE_SIDEBAR); ?>
		<div id="contentArea">
			<a name="top"></a>
			<div id="contentAreaElements">
				<h1><?php if(SHOW_PAGE_TITLE_ICONS) { echo '<img src="' . PATH_ICONS_MENUE_TITLES . 'statistics.png" alt="" />'; } ?> <?php echo $thisTitle; ?></h1>
				<!--
				<p class="infoArea">Diese Daten werden aus den geschriebenen Auftragsbest&auml;tigungen von <b>B3 UND Burhan</b> erstellt.</p>
				-->
				<?php
					if(!empty($arrUnusedDoubleZipcodes)){
						$arrTemp = array();
						foreach($arrUnusedDoubleZipcodes as $thisUnusedDoubleZipcodeKey => $thisUnusedDoubleZipcodeValue){
							$arrTemp[] = $thisUnusedDoubleZipcodeKey;
						}
						$unusedZipcodes = implode(', ', $arrTemp);
				?>
				<p class="infoArea">Folgende PLZ sind nicht in Gebrauch: <?php echo $unusedZipcodes; ?></p>
				<?php
					}
				?>
				<div id="searchFilterArea">
					<form name="formSearchDatas" method="post" action="<?php echo $_SERVER["PHP_SELF"]; ?>">
					<table border="0" cellpadding="0" cellspacing="0" width="" class="searchFilterContent">
						<tr>
							<td>
								<label for="searchMandantory">Mandant:</label>
								<select name="searchMandantory" id="searchMandantory" class="inputField_130">
									<option value="ALL">Alle</option>
									<?php
										if(!empty($arrMandatoryDatas)){
											foreach($arrMandatoryDatas as $thisKey => $thisValue){
												$selected = '';
												if($thisValue["mandatoriesShortName"] == $_POST["searchMandantory"]){
													$selected = ' selected="" ';
												}
												echo '<option value="' . $thisValue["mandatoriesShortName"] . '" ' . $selected . ' >' . $thisValue["mandatoriesName"] . '</option>';
											}
										}
									?>
								 </select>
							</td>
							<td>
								<label for="searchSource">Quelle:</label>
								<select name="searchSource" id="searchSource" class="inputField_200">
									<option value="CONFIRMATIONS" <?php if($_POST["searchSource"] == 'CONFIRMATIONS'){ echo ' selected="selected" ';} ?> >Geschriebene Auftr&auml;ge</option>
									<option value="PROCESS" <?php if($_POST["searchSource"] == 'PROCESS'){ echo ' selected="selected" ';} ?> >In Bearbeitung</option>
								 </select>
							</td>
							<td>
								<label for="searchInterval">Zeitraum:</label>
								<select name="searchInterval" id="searchInterval" class="inputField_130">
									<option value=""></option>
									<option value="WEEK" <?php if($_POST["searchInterval"] == 'WEEK'){ echo ' selected="selected" '; } ?> >pro Woche</option>
									<option value="MONTH" <?php if($_POST["searchInterval"] == 'MONTH'){ echo ' selected="selected" '; } ?> >pro Monat</option>
									<option value="QUARTER" <?php if($_POST["searchInterval"] == 'QUARTER'){ echo ' selected="selected" '; } ?> >pro Vierteljahr</option>
									<option value="YEAR" <?php if($_POST["searchInterval"] == 'YEAR'){ echo ' selected="selected" '; } ?> >pro Jahr</option>
								 </select>
							</td>
							<td>
								<label for="searchSalesman">Vertreter:</label>
								<select name="searchSalesman" id="searchSalesman" class="inputField_130">
									<option value="">Alle</option>
									<?php
										if(!empty($arrSalesmenDatas2)){
											foreach($arrSalesmenDatas2 as $thisKey => $thisValue){
												$selected = '';
												if($thisValue["customersID"] == $_POST["searchSalesman"]){
													$selected = ' selected="" ';
												}
												$thisClass = 'personnelActive';
												$thisAddText = ' (aktives Gebiet)';
												if($thisValue["salesmenAreasActive"] != "1") {
													$thisClass = 'personnelNotActive';
													$thisAddText = ' (Gebiet nicht aktiv)';
												}
												echo '<option class="' . $thisClass . '" value="' . $thisValue["customersID"] . '" ' . $selected . ' >' . $thisValue["salesmenFirmenname"] . $thisAddText . '</option>';
											}
										}
									?>
								 </select>
							</td>

							<td>
								<label for="searchPLZ">PLZ:</label>
								<input name="searchPLZ" id="searchPLZ" class="inputField_20" maxlength="2" value="<?php echo $_POST["searchPLZ"]; ?>" />
							</td>

							<td>
								<input type="hidden" name="editID" id="editID" value="" />
								<input type="submit" name="submitSearch" class="inputButton0" value="Anzeigen" />
							</td>
							<td class="legendSeperator">&nbsp;</td>
							<td><a href="<?php echo PAGE_SALESMEN_FLASH_MAP; ?>"><img src="<?php echo PATH_ICONS_MENUE_QUICKLINKS; ?>iconMapPLZ.png" width="24" height="24" title="PLZ-Karte zeigen" alt="PLZ-Karte zeigen" /></a></td>
						</tr>
					</table>
					<input type="hidden" name="submitSearchForm" id="submitSearchForm" value="1" />
					</form>
				</div>

				<div class="colorsLegend">
					<div class="legendItem">
						<b>F&auml;rbung:</b>
					</div>
					<div class="legendItem">
						<span class="legendColorField valueMaximum"> </span></span>
						<span class="legendDescription"><b>Maximum-Wert(e) pro Zeitraum</b></span>
						<div class="clear"></div>
					</div>
					<div class="legendItem">
						<span class="legendColorField valueMinimum"> </span></span>
						<span class="legendDescription"><b>Minimum-Wert(e) pro Zeitraum</b></span>
						<div class="clear"></div>
					</div>
					<div class="legendItem">
						<span class="legendColorField valueOverAverage"> </span></span>
						<span class="legendDescription"><b>&Uuml;berdurchschnittliche Wert(e) pro Zeitraum</b></span>
						<div class="clear"></div>
					</div>
					<div class="clear"></div>
				</div>

				<div class="contentDisplay">
					<?php
						if(1) {
							// BOF GET DATAS
								$where = "";
								$having = "";
								$dateField = "";

								// BOF GET DATAS SOURCE CONFIRMATIONS
								if($_POST["searchSource"] == 'CONFIRMATIONS') {
									if(!empty($arrSalesmanZipcodes) && $_POST["searchPLZ"] == ''){
										$having .= " HAVING (`PLZ-Gebiete` = '" . implode("' OR `PLZ-Gebiete` = '", $arrSalesmanZipcodes) . "' )";
									}
									else {
										#$having .= " HAVING (`PLZ-Gebiete` = '" . $_POST["searchPLZ"] . "' )";
									}

									if($_POST["searchInterval"] == "WEEK"){
										$where .= "";
										$dateField = "
											CONCAT(
												IF(DATE_FORMAT(`orderDocumentsDocumentDate`, '%m') = '12' AND DATE_FORMAT(`orderDocumentsDocumentDate`, '%v') = '01', (DATE_FORMAT(`orderDocumentsDocumentDate`, '%Y') + 1),  DATE_FORMAT(`orderDocumentsDocumentDate`, '%Y')),
												'#',
												DATE_FORMAT(`orderDocumentsDocumentDate`, '%v')
											) AS `interval`
										";
									}
									else if($_POST["searchInterval"] == "MONTH"){
										// DATE_FORMAT(`" . TABLE_ORDER_INVOICES . "`.`customersEntryDate`, '%M') AS `interval`,
										$where .= "";

										$dateField = "
											CONCAT(
												DATE_FORMAT(`orderDocumentsDocumentDate`, '%Y'),
												'#',
												DATE_FORMAT(`orderDocumentsDocumentDate`, '%m')
											) AS `interval`
										";
									}
									else if($_POST["searchInterval"] == "QUARTER"){
										$where .= "";

										$dateField = "
											CONCAT(
												DATE_FORMAT(`orderDocumentsDocumentDate`, '%Y'),
												'#',
												IF(DATE_FORMAT(`orderDocumentsDocumentDate`, '%m') = '01' OR DATE_FORMAT(`orderDocumentsDocumentDate`, '%m') = '02' OR DATE_FORMAT(`orderDocumentsDocumentDate`, '%m') = '03', '1. Quartal',
													IF(DATE_FORMAT(`orderDocumentsDocumentDate`, '%m') = '04' OR DATE_FORMAT(`orderDocumentsDocumentDate`, '%m') = '05' OR DATE_FORMAT(`orderDocumentsDocumentDate`, '%m') = '06', '2. Quartal',
														IF(DATE_FORMAT(`orderDocumentsDocumentDate`, '%m') = '07' OR DATE_FORMAT(`orderDocumentsDocumentDate`, '%m') = '08' OR DATE_FORMAT(`orderDocumentsDocumentDate`, '%m') = '09', '3. Quartal',
															IF(DATE_FORMAT(`orderDocumentsDocumentDate`, '%m') = '10' OR DATE_FORMAT(`orderDocumentsDocumentDate`, '%m') = '11' OR DATE_FORMAT(`orderDocumentsDocumentDate`, '%m') = '12', '4. Quartal',
																''
															)
														)
													)
												)
											) AS `interval`
										";

									}
									else if($_POST["searchInterval"] == "YEAR"){
										$where .= "";

										$dateField = "
											CONCAT(
												DATE_FORMAT(`orderDocumentsDocumentDate`, '%Y'),
												'#',
												DATE_FORMAT(`orderDocumentsDocumentDate`, '%Y')
											) AS `interval`
										";

									}
									if($_POST["searchSalesman"] != "") {

									}
									else if($_POST["searchPLZ"] != ""){
										$where .= " AND SUBSTRING(`orderDocumentsAddressZipcode`, 1, 2) = '" . $_POST["searchPLZ"] .  "' ";
									}

									$where .= " AND `orderDocumentsAddressCountry` = '81' ";

									$arrIntervals = array();
									$arrDatas = array();

									$thisOrderTable = preg_replace("/" . MANDATOR. "/","", TABLE_ORDER_CONFIRMATIONS);
									$thisOrderTableDetails = preg_replace("/" . MANDATOR. "/","", TABLE_ORDER_CONFIRMATIONS_DETAILS);

									$arrSearchMandantory = array();
									if($_POST["searchMandantory"] == '' || $_POST["searchMandantory"] == 'ALL'){
										if(!empty($arrMandatoryDatas)){
											foreach($arrMandatoryDatas as $thisKey => $thisValue){
												$arrSearchMandantory[] = $thisValue["mandatoriesShortName"];
											}
										}
									}
									else {
										$arrSearchMandantory[] = $_POST["searchMandantory"];
									}

									$arrSql_Mandantory = array();
									if(!empty($arrSearchMandantory)){
										foreach($arrSearchMandantory as $thisMandatory){
											$arrSql_Mandantory[] = "SELECT
													SUBSTRING(`orderDocumentsAddressZipcode`, 1, 2) AS `PLZ-Gebiete`,
													" . $dateField . ",
													`orderDocumentsAddressCountry` AS `PLZ-Land`,
													SUM(`orderDocumentDetailProductQuantity`) AS `countItems`

													FROM `" . $thisMandatory . $thisOrderTable . "`
													LEFT JOIN `" . $thisMandatory . $thisOrderTableDetails . "`
													ON( `" . $thisMandatory . $thisOrderTable . "`.`orderDocumentsID` = `" . $thisMandatory . $thisOrderTableDetails . "`.`orderDocumentDetailDocumentID`)
													WHERE 1
														AND `orderDocumentDetailProductKategorieID` LIKE '001%'
														" . $where . "
													GROUP BY CONCAT(SUBSTRING(`orderDocumentsAddressZipcode`, 1, 2), '#', `interval`)
												";
										}
									}

									$sql = "
										SELECT
											`tempTable`.`PLZ-Gebiete`,
											`tempTable`.`interval`,
											SUM(`tempTable`.`countItems`) AS `countItems`

											FROM (
												" . implode(" UNION ", $arrSql_Mandantory) . "
											) AS `tempTable`

											GROUP BY CONCAT(`tempTable`.`PLZ-Gebiete`, '#', `tempTable`.`interval`)
										";
									$sql .= $having;

									$sql .= "
											ORDER BY `interval` DESC
										";
								}
								// EOF GET DATAS SOURCE CONFIRMATIONS

								// BOF GET DATAS SOURCE PROCESS
								if($_POST["searchSource"] == 'PROCESS') {
									if(!empty($arrSalesmanZipcodes) && $_POST["searchPLZ"] == ''){
										$having .= " HAVING (`PLZ-Gebiete` = '" . implode("' OR `PLZ-Gebiete` = '", $arrSalesmanZipcodes) . "' )";
									}
									else {
										#$having .= " HAVING (`PLZ-Gebiete` = '" . $_POST["searchPLZ"] . "' )";
									}

									if($_POST["searchInterval"] == "WEEK"){
										$where .= "";
										$dateField = "
											CONCAT(
												DATE_FORMAT(`ordersBestellDatum`, '%Y'),
												'#',
												DATE_FORMAT(`ordersBestellDatum`, '%u')
											) AS `interval`
										";
									}
									else if($_POST["searchInterval"] == "MONTH"){
										$where .= "";

										$dateField = "
											CONCAT(
												DATE_FORMAT(`ordersBestellDatum`, '%Y'),
												'#',
												DATE_FORMAT(`ordersBestellDatum`, '%m')
											) AS `interval`
										";
									}
									else if($_POST["searchInterval"] == "QUARTER"){
										$where .= "";

										$dateField = "
											CONCAT(
												DATE_FORMAT(`ordersBestellDatum`, '%Y'),
												'#',
												IF(DATE_FORMAT(`ordersBestellDatum`, '%m') = '01' OR DATE_FORMAT(`ordersBestellDatum`, '%m') = '02' OR DATE_FORMAT(`ordersBestellDatum`, '%m') = '03', '1. Quartal',
													IF(DATE_FORMAT(`ordersBestellDatum`, '%m') = '04' OR DATE_FORMAT(`ordersBestellDatum`, '%m') = '05' OR DATE_FORMAT(`ordersBestellDatum`, '%m') = '06', '2. Quartal',
														IF(DATE_FORMAT(`ordersBestellDatum`, '%m') = '07' OR DATE_FORMAT(`ordersBestellDatum`, '%m') = '08' OR DATE_FORMAT(`ordersBestellDatum`, '%m') = '09', '3. Quartal',
															IF(DATE_FORMAT(`ordersBestellDatum`, '%m') = '10' OR DATE_FORMAT(`ordersBestellDatum`, '%m') = '11' OR DATE_FORMAT(`ordersBestellDatum`, '%m') = '12', '4. Quartal',
																''
															)
														)
													)
												)
											) AS `interval`
										";

									}
									else if($_POST["searchInterval"] == "YEAR"){
										$where .= "";

										$dateField = "
											CONCAT(
												DATE_FORMAT(`ordersBestellDatum`, '%Y'),
												'#',
												DATE_FORMAT(`ordersBestellDatum`, '%Y')
											) AS `interval`
										";

									}
									if($_POST["searchSalesman"] != "") {

									}
									else if($_POST["searchPLZ"] != ""){
										$where .= " AND SUBSTRING(`ordersPLZ`, 1, 2) = '" . $_POST["searchPLZ"] .  "' ";
									}

									$where .= " AND `ordersLand` = '81' ";

									$arrIntervals = array();
									$arrDatas = array();

									if($_POST["searchMandantory"] == '' || $_POST["searchMandantory"] == 'ALL'){
										$where .= '';
									}
									else {
										$where .= " AND `ordersMandant` = '" . $_POST["searchMandantory"] . "' ";
									}


									$sql = "
										SELECT
											`tempTable`.`PLZ-Gebiete`,
											SUM(`tempTable`.`countItems`) AS `countItems`,
											`tempTable`.`interval`

											FROM (
												SELECT
													SUBSTRING(`ordersPLZ`, 1, 2) AS `PLZ-Gebiete`,
													" . $dateField . ",
													`ordersLand` AS `PLZ-Land`,
													SUM(`ordersArtikelMenge`) AS `countItems`


													FROM `" . TABLE_ORDERS . "`

													WHERE 1
														AND `ordersArtikelKategorieID` LIKE '001%'
														" . $where . "
														GROUP BY CONCAT(SUBSTRING(`ordersPLZ`, 1, 2), '#', `interval`)

											) AS `tempTable`

											GROUP BY CONCAT(`tempTable`.`PLZ-Gebiete`, '#', `tempTable`.`interval`)
										";
									$sql .= $having;

									$sql .= "
											ORDER BY `interval` DESC
										";
								}
								// EOF GET DATAS SOURCE PROCESS
								#dd('sql');
								$rs = $dbConnection->db_query($sql);

								$countTotalRows = $dbConnection->db_getMysqlNumRows($rs);

								while($ds = mysqli_fetch_assoc($rs)) {
									$arrIntervals[] = $ds["interval"];
									$arrLabelsGraph[$ds["interval"]] = $ds["interval"];
									$arrDatasGraph["countItems"][$ds["interval"]] = $ds["countItems"];
									// $arrDatasGraph["totalPrice"][$ds["interval"]] = $ds["totalPrice"];
									foreach(array_keys($ds) as $field){
										$arrDatas[$ds["interval"]][$ds["PLZ-Gebiete"]][$field] = $ds[$field];
									}
								}

								if(!empty($arrIntervals)){
									$arrIntervals = array_unique($arrIntervals);
									arsort($arrIntervals);
								}

								if($countTotalRows > 0) {
								?>
									<!-- BOF GRAPH ELEMENTS -->
									<!--
									<script type="text/javascript" language="javascript" src="<?php echo PATH_RGRAPH; ?>libraries/RGraph.common.core.js" ></script>
									<script type="text/javascript" language="javascript" src="<?php echo PATH_RGRAPH; ?>libraries/RGraph.bar.js" ></script>
									<script type="text/javascript" language="javascript" src="<?php echo PATH_RGRAPH; ?>libraries/RGraph.line.js" ></script>

									<div class="adminEditArea">
										<h2>Anzahl der Neukunden im Auftragslisten</h2>
										<canvas id="cvs_dataCountItems" width="900" height="250">[No canvas support]</canvas>
									</div>
									-->
									<!-- EOF GRAPH ELEMENTS -->

								<?php
									echo '<table width="100%" cellpadding="0" cellspacing="0" class="displayOrders">';

									echo '<thead>';
									echo '<tr>';
									echo '<th style="width:45px" rowspan="2">#</th>';
									echo '<th style="width:80px" rowspan="2">Zeitraum</th>';
									echo '<th style="width:50px" rowspan="2">Gesamt</th>';
									echo '<th style="width:50px" rowspan="2">Durchschnitt<br />pro PLZ</th>';
									echo '<th colspan="' . count($arrDoubleZipcodes) . '">Kennzeichenhalter pro PLZ-Gebiet</th>';
									// echo '<th>Betrag</th>';

									echo '<th style="width:50px" rowspan="2">Info</th>';
									echo '</tr>';

									echo '<tr>';

									foreach($arrDoubleZipcodes as $thisDoubleZipcodeKey => $thisDoubleZipcodeValue){
										$thisSalesman = '';
										if(!empty($arrRelationSalesmenZipcodeDatas[$thisDoubleZipcodeKey])) {
											if($arrRelationSalesmenZipcodeDatas[$thisDoubleZipcodeKey]["kundenPLZaktiv"] != "1"){
												$thisSalesman = "ALTES ";
											}
											$thisSalesman .= "Gebiet von " . $arrRelationSalesmenZipcodeDatas[$thisDoubleZipcodeKey]["kundenname"] . ':' . "\n";
										}
										echo '<th style="width:50px; cursor: pointer;" title="PLZ ' . $thisDoubleZipcodeKey . '' . "\n". '' . $thisSalesman . '' . $arrDoubleZipcodes[$thisDoubleZipcodeKey]["zipcodesDoubleDescription"]. '">' . $thisDoubleZipcodeKey . '</th>';
									}
									echo '</tr>';
									echo '</thead>';

									echo '<tbody>';

									$countRow = 0;
									$thisMarker = "";
									$thisCountItemsTotalColumn = 0;
									$arrCountItemsTotalColumn = array();
									foreach($arrIntervals as $thisIntervalKey => $thisIntervalValue) {
											if($countRow == 0) {

											}

											$rowClass = 'row0';
											if($countRow%2 == 0) {
												$rowClass = 'row1';
											}
											$arrTemp = explode('#', $thisIntervalValue);
											if($thisMarker != $arrTemp[0]) {
												$thisMarker = $arrTemp[0];
												echo '<tr><td colspan="' . (count($arrDoubleZipcodes) + 5) . '" class="tableRowTitle1">' . $arrTemp[0] . '</td></tr>';
											}

											echo '<tr class="'.$rowClass.'">';

											echo '<td style="text-align:right;"><b>';
											echo ($countRow + 1);
											echo '.</b></td>';

											echo '<td style="white-space:nowrap;">';
											if($_POST["searchInterval"] == "MONTH"){
												$thisMonth = getTimeNames($arrTemp[1], 'month', 'long');
												echo $thisMonth;
											}
											else {
												if($_POST["searchInterval"] == "WEEK"){ echo 'KW '; }
												echo $arrTemp[1];
											}
											echo '</td>';

											// BOF GET TOTAL, MAX AND MIN
											$countItemsMIN = 9999999999;
											$countItemsMAX = 0;
											$thisCountItemsTotalRow = 0;
											foreach($arrDoubleZipcodes as $thisDoubleZipcodeKey => $thisDoubleZipcodeValue){
												$thisCountItems = $arrDatas[$thisIntervalValue][$thisDoubleZipcodeKey]["countItems"];
												if($thisCountItems == ''){ $thisCountItems = 0; }
												if($thisCountItems < $countItemsMIN) { $countItemsMIN = $thisCountItems; }
												if($thisCountItems > $countItemsMAX) { $countItemsMAX = $thisCountItems; }
												if($thisCountItems > 0) {$thisCountItemsTotalRow += $thisCountItems; }
											}
											// EOF GET TOTAL, MAX AND MIN

											echo '<td style="text-align:right;background-color:#FEFFAF;"><b>';
											echo ($thisCountItemsTotalRow);
											echo '</b></td>';

											echo '<td style="text-align:right;background-color:#FEFFAF;"><b>';
											$thisCountItemsAverageRow = $thisCountItemsTotalRow / count($arrDoubleZipcodes);
											echo number_format($thisCountItemsAverageRow, "2", ",", "");
											echo '</b></td>';

											foreach($arrDoubleZipcodes as $thisDoubleZipcodeKey => $thisDoubleZipcodeValue){
												$thisPLZ = $i;
												if($thisPLZ < 10){ $thisPLZ = '0' . $thisPLZ; }
												$thisSalesman = '';
												if(!empty($arrRelationSalesmenZipcodeDatas[$thisDoubleZipcodeKey])) {
													if($arrRelationSalesmenZipcodeDatas[$thisDoubleZipcodeKey]["kundenPLZaktiv"] != "1"){
														$thisSalesman = "ALTES ";
													}
													$thisSalesman .= "Gebiet von " . $arrRelationSalesmenZipcodeDatas[$thisDoubleZipcodeKey]["kundenname"] . ':' . "\n";
												}
												echo '<td style="text-align:right; width:50px; cursor: pointer;" title="PLZ ' . $thisDoubleZipcodeKey . '' . "\n". '' . $thisSalesman . '' . $arrDoubleZipcodes[$thisDoubleZipcodeKey]["zipcodesDoubleDescription"]. '">';
												$thisCountItems = $arrDatas[$thisIntervalValue][$thisDoubleZipcodeKey]["countItems"];
												if($thisCountItems == ''){$thisCountItems = 0;}

												$arrCountItemsTotalColumn[$thisDoubleZipcodeKey] += $thisCountItems;

												$thisMinMaxClass ="valueNormal";
												if($_POST["searchPLZ"] == ''){
													if($thisCountItems == $countItemsMAX) {
														$thisMinMaxClass ="valueMaximum";
													}
													else if($thisCountItems == $countItemsMIN) {
														$thisMinMaxClass ="valueMinimum";
													}
													else if($thisCountItems >= $thisCountItemsAverageRow) {
														$thisMinMaxClass ="valueOverAverage";
													}
												}

												echo '<span class="' . $thisMinMaxClass . '">' . $thisCountItems . '</span>';
												echo '</td>';
											}

											/*
											echo '<td style="text-align:right;background-color:#FEFFAF;">';
											echo '' . $arrDatas[$thisIntervalValue]['countItems'] . '';
											echo '</td>';
											*/
											// echo '<td style="text-align:right;background-color:#FEFFAF;">';
											// echo '' . number_format($arrDatas[$thisIntervalValue]['invoices']['totalPrice'], 2, ',', '.') . ' &euro;';
											// echo '</td>';

											echo '<td>';
											echo '<span class="toolItem">';
											echo '<img src="layout/icons/iconInfo.png" class="buttonShopOrderInfo" alt="Details" title="Detailinformationen ansehen">';
											echo '<div class="orderDetailsContent" style="display:none; visibility:hidden;">';
											echo '</div>';
											echo '</span>';
											echo '</td>';
											echo '</tr>';
											$thisCountItemsTotalColumn += $thisCountItemsTotalRow;
											$countRow++;

									}
									echo '<tr>';
									echo '<td colspan="' . (count($arrDoubleZipcodes) + 5) . '" class="tableRowTitle2"><hr /></td>';
									echo '</tr>';
									echo '<tr>';
									echo '<td colspan="2"><b>GESAMT</b></td>';
									echo '<td style="text-align:right;background-color:#FEFFAF;"><b>';
									echo $thisCountItemsTotalColumn;
									echo '</td>';
									echo '<td style="text-align:right;background-color:#FEFFAF;"><b>';
									$thisCountItemsAverageRow = $thisCountItemsTotalColumn / count($arrDoubleZipcodes);
									echo number_format($thisCountItemsAverageRow, "2", ",", "");
									echo '</td>';
									foreach($arrDoubleZipcodes as $thisDoubleZipcodeKey => $thisDoubleZipcodeValue){
										echo '<td>';
										echo $arrCountItemsTotalColumn[$thisDoubleZipcodeKey];
										echo '</td>';
									}
									echo '</tr>';
									echo '</tbody>';
									echo '</table>';
								}
								else {
									$warningMessage .= 'F&uuml;r den gew&auml;hlten Zeitraum wurden keine Daten gefunden.';
								}
								// EOF GET DATAS
						}
					?>
					<?php displayMessages(); ?>
				</div>

			</div>
		</div>
		<div class="clear"></div>
	</div>
</div>
<script language="javascript" type="text/javascript">
	$(document).ready(function() {
		$(function() {
			colorRowMouseOver('.displayOrders tbody tr');
			$('.buttonShopOrderInfo').css('cursor', 'pointer');
			$('.buttonShopOrderInfo').click(function () {
				// loadOrderDetails($(this), '<?php echo BASEPATH; ?>', 'Details');
			});

			// BOF GET DATAS FOR GRAPH CASH
			<?php
				if(!empty($arrDatasGraph)){
					$arrDatasGraph["countItems"] = array_reverse($arrDatasGraph["countItems"], true);
					$arrDatasGraph["totalPrice"] = array_reverse($arrDatasGraph["totalPrice"], true);
					$arrLabelsGraph = array_reverse($arrLabelsGraph , true);
				}
			?>

			createGraph('cvs_dataCountItems', [<?php echo implode(",", array_values($arrDatasGraph["countItems"])); ?>], [<?php echo "'" . implode("','", $arrLabelsGraph) . "'"; ?>], 'Line', [['green','red']]);
			// createGraph('cvs_dataTotalPrice', [<?php echo implode(",", array_values($arrDatasGraph["totalPrice"])); ?>], [<?php echo "'" . implode("','", $arrLabelsGraph) . "'"; ?>], 'Line', [['green','red']]);

		// EOF GET DATAS FOR GRAPH CASH
		});
		$(function() {
			$('#tabs').tabs();
		});
	});
</script>
<?php require_once('inc/footerHTML.inc.php'); ?>