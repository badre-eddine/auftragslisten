<?php
	require_once('inc/requires.inc.php');

	if(!$arrGetUserRights["exportDatas"]) {
		header('location: ' . PAGE_EXIT_LOCATION);
		exit;
	}

	$jswindowMessage = '';
	$warningMessage = '';
	$errorMessage = '';
	$successMessage = '';
	$infoMessage = '';

	$dbConnection = new DB_Connection();
	$db_open = $dbConnection->db_connect();

	// BOF GET ALL TABLES
		$sql = "SHOW TABLE STATUS";
		$rs = $dbConnection->db_query($sql);

		while($ds = mysqli_fetch_assoc($rs)){
			$arrExistingTables[$ds["Name"]] = $ds["Comment"];
		}
	// EOF GET ALL TABLES

	if($_POST["submitFormExportDatas"] != "") {
		// BOF EXPORT FILE
		if($_POST["selectExportTable"] != "") {
			// $filenameExport = $_POST["selectExportTable"] . '_' . date("Y-m-d_H-i-s") . '.csv';
			$fileNameExport = $_POST["selectExportTable"] . '.csv';
			$filePathExport = DIRECTORY_EXPORT_FILES . $fileNameExport;
			if(file_exists($filePathExport)){
				unlink($filePathExport);
			}
			clearstatcache();

			$arrFieldSeperator = array(
				"tab"			=> "\t",
				"semikolon"		=> ";",
				"komma"			=> ",",
			);

			$arrFieldTerminator = array(
				"nothing"			=> "\t",
				"doubleQuotes"			=> "\"",
				"singleQuotes"			=> "\'",
			);

			$arrLineSeperator = array(
				"returnNewLine"			=> "\r\n",
				"newLine"			=> "\n",
			);

			// BOF GET TABLE COLUMN NAMES
			if($_POST["selectFieldNamesInFirstRow"] == '1') {
				$sql = "SELECT
							`COLUMN_NAME`
						FROM `information_schema`.`COLUMNS`
						WHERE `TABLE_NAME` = '" . $_POST["selectExportTable"] . "'
							AND `TABLE_SCHEMA` = '" . DB_NAME . "'
						ORDER BY `ORDINAL_POSITION` ASC
				";
				$rs = $dbConnection->db_query($sql);
				if($rs){
					while($ds = mysqli_fetch_array($rs)){
						$arrColumnNames[] = strtoupper($ds["COLUMN_NAME"]);
					}
				}
				else {
					$errorMessage .= 'Die Spaltennamen der Tabelle &quot;' . $_POST["selectExportTable"] . '&quot; konnte nicht ausgelesen werden! ' . '<br />';
				}
			}
			// EOF GET TABLE COLUMN NAMES

			$sql = "";
			if(!empty($arrColumnNames)){
				$sql .= "
					SELECT '" . implode("','", $arrColumnNames). "'
					UNION
				";
			}

			$sql .= "SELECT
						*
						FROM `" . $_POST["selectExportTable"] . "`

						INTO OUTFILE '" . $filePathExport . "'
						FIELDS ESCAPED BY '\\\'
						TERMINATED BY '" . $arrFieldSeperator[$_POST["selectFieldSeperator"]] . "'
			";
			if($_POST["selectFieldTerminator"] != 'nothing') {
				$sql .= "	OPTIONALLY ENCLOSED BY '" . $arrFieldTerminator[$_POST["selectFieldTerminator"]] . "' ";
			}
			$sql .= "	LINES TERMINATED BY '" . $arrLineSeperator[$_POST["selectLineSeperator"]] . "' ";

			$rs = $dbConnection->db_query($sql);
			if(!$rs){
				$errorMessage .= 'Die Tabelle &quot;' . $_POST["selectExportTable"] . '&quot; konnte nicht in die Datei &quot;' . $fileNameExport . '&quot; exportiert werden! ' . '<br />';
			}
			else {
				require_once('classes/downloadFile.class.php');
				$thisDownload = new downloadFile(DIRECTORY_EXPORT_FILES, $fileNameExport);
				$errorMessage .= $thisDownload->startDownload();
			}
		}
		else {
			$errorMessage .= 'Sie haben keine Tabelle ausgew&auml;hlt! ' . '<br />';
		}
		// EOF EXPORT FILE
	}

?>
<?php
	require_once('inc/headerHTML.inc.php');
	$thisTitle = "Daten-Export";
	$headerHTML = preg_replace("/{###TITLE###}/", strip_tags($thisTitle), $headerHTML);
	echo $headerHTML;

	require_once(FILE_MENUE_TOP);
?>
<div id="mainArea">
	<div id="mainContent">
		<?php require_once(FILE_MENUE_SIDEBAR); ?>
		<div id="contentArea">
			<div id="contentAreaElements">
				<h1><?php if(SHOW_PAGE_TITLE_ICONS) { echo '<img src="' . PATH_ICONS_MENUE_TITLES . 'import.png" alt="" />'; } ?> <?php echo $thisTitle; ?></h1>

				<div class="adminEditArea">
				<form name="formImportDatas" method="post" action="<?php echo $_SERVER["REQUEST_URI"]; ?>" enctype="multipart/form-data">
					<fieldset>
					<table border="0" cellpadding="0" cellspacing="0" width="100%" class="">
						<tr>
							<td style="width:200px;"><b>Datenbank-Tabelle:</b></td>
							<td>
								<select class="inputSelect_427" name="selectExportTable">
									<option value=""> - Bitte w&auml;hlen - </option>
									<?php
										if(!empty($arrExistingTables)){
											asort($arrExistingTables);
											foreach($arrExistingTables as $thisTableName => $thisTableComment) {
												if(!preg_match("/^_/", $thisTableName) && (preg_match("/^common_/", $thisTableName) || preg_match("/^" . strtolower(MANDATOR) . "_/", $thisTableName))) {
													echo '<option value="' . $thisTableName . '">' . $thisTableComment . ' (TABELLE &quot;' . $thisTableName . '&quot;)</option>';
												}
											}
										}
									?>
								</select>
							</td>
						</tr>
						<tr>
							<td style="width:200px;"><b>Feld-Trenner:</b></td>
							<td>
								<select class="inputSelect_427" name="selectFieldSeperator">
									<option value="tab" selected="selected" >Tabulator</option>
									<option value="semikolon">Semikolon</option>
									<option value="komma">Komma</option>
								</select>
							</td>
						</tr>
						<tr>
							<td style="width:200px;"><b>Feld-Begrenzer:</b></td>
							<td>
								<select class="inputSelect_427" name="selectFieldTerminator">
									<option value="nothing" selected="selected" >Keine</option>
									<option value="doubleQuotes">Doppelte Anf&uuml;hrungszeichen</option>
									<option value="singleQuotes">Einfache Anf&uuml;hrungszeichen</option>
								</select>
							</td>
						</tr>
						<tr>
							<td style="width:200px;"><b>Zeilen-Trenner:</b></td>
							<td>
								<select class="inputSelect_427" name="selectLineSeperator">
									<option value="returnNewLine" selected="selected" >\r\n</option>
									<option value="newLine">\n</option>
								</select>
							</td>
						</tr>
						<tr>
							<td style="width:200px;"><b>Spalten-Namen:</b></td>
							<td>
								<input type="checkbox" name="selectFieldNamesInFirstRow" checked="checked" value="1" /> Spalten-Namen in erste Zeile setzen?
							</td>
						</tr>
					</table>
					</fieldset>
					<div class="actionButtonsArea">
						<input type="submit" name="submitFormExportDatas" class="inputButton1" value="Export starten" />
					</div>
				</form>
				</div>
				<?php displayMessages(); ?>
			</div>
		</div>
		<div class="clear"></div>
	</div>
	<div class="clear"></div>
</div>

<script language="javascript" type="text/javascript">
	$(document).ready(function() {
		$(function() {
			$( "#datepicker" ).datepicker();
		});
		colorRowMouseOver('.displayOrders tbody tr');
	});
</script>

<?php require_once('inc/footerHTML.inc.php'); ?>