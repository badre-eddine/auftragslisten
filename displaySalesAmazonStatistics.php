<?php
	require_once('inc/requires.inc.php');

	if(!$arrGetUserRights["displayOnlineOrdersStatistics"]) {
		header('location: ' . PAGE_EXIT_LOCATION);
		exit;
	}

	$jswindowMessage = '';
	$warningMessage = '';
	$errorMessage = '';
	$successMessage = '';
	$infoMessage = '';


	$dbConnection = new DB_Connection();
	$db_open = $dbConnection->db_connect();

	// BOF READ VERTRETER
		$arrSalesmenDatas = getSalesmen();
	// EOF READ VERTRETER

	$defaultORDER = "BESTELLDATUM";
	$defaultSORT = "ASC";

	if($_POST["searchInterval"] == ""){
		$_POST["searchInterval"] = "WEEK";
	}

	// BOF READ BESTELL ARTEN
		$arrOrderTypeDatas = getOrderTypes();
	// EOF READ BESTELL ARTEN
?>
<?php
	require_once('inc/headerHTML.inc.php');
	$thisTitle = "Amazon-Bestellungen";

	if($_POST["searchInterval"] == 'WEEK') {
		$thisTitle .= ': <span class="headerSelectedEntry">pro Woche</span>';
	}
	else if($_POST["searchInterval"] == 'MONTH') {
		$thisTitle .= ': <span class="headerSelectedEntry">pro Monat</span>';
	}
	else if($_POST["searchInterval"] == 'QUARTER') {
		$thisTitle .= ': <span class="headerSelectedEntry">pro Vierteljahr</span>';
	}
	else if($_POST["searchInterval"] == 'YEAR') {
		$thisTitle .= ': <span class="headerSelectedEntry">pro Jahr</span>';
	}

	$headerHTML = preg_replace("/{###TITLE###}/", strip_tags($thisTitle), $headerHTML);
	echo $headerHTML;

	require_once(FILE_MENUE_TOP);

	$sqlSortField = "";
	$sqlSortDirection = "";
	$sqlWhere = "";

?>

<div id="mainArea">
	<div id="mainContent">
		<?php require_once(FILE_MENUE_SIDEBAR); ?>
		<div id="contentArea">
			<a name="top"></a>
			<div id="contentAreaElements">
				<h1><?php if(SHOW_PAGE_TITLE_ICONS) { echo '<img src="' . PATH_ICONS_MENUE_TITLES . 'statistics.png" alt="" />'; } ?> <?php echo $thisTitle; ?></h1>

				<div id="searchFilterArea">
					<form name="formSearchDatas" method="post" action="<?php echo $_SERVER["PHP_SELF"]; ?>">
					<table border="0" cellpadding="0" cellspacing="0" class="searchFilterContent">
						<tr>
							<td>
								<label for="searchInterval">Zeitraum:</label>
								<select name="searchInterval" id="searchInterval" class="inputField_130">
									<option value=""></option>
									<option value="WEEK" <?php if($_POST["searchInterval"] == 'WEEK'){ echo ' selected="selected" '; } ?> >pro Woche</option>
									<option value="MONTH" <?php if($_POST["searchInterval"] == 'MONTH'){ echo ' selected="selected" '; } ?> >pro Monat</option>
									<option value="QUARTER" <?php if($_POST["searchInterval"] == 'QUARTER'){ echo ' selected="selected" '; } ?> >pro Vierteljahr</option>
									<option value="YEAR" <?php if($_POST["searchInterval"] == 'YEAR'){ echo ' selected="selected" '; } ?> >pro Jahr</option>
								 </select>
							</td>
							<td>
								<input type="hidden" name="editID" id="editID" value="" />
								<input type="submit" name="submitSearch" class="inputButton0" value="Anzeigen" />
							</td>
						</tr>
					</table>
					<input type="hidden" name="submitSearchForm" id="submitSearchForm" value="1" />
					</form>
				</div>

				<div class="contentDisplay">
					<?php
						//if($_POST["submitExportForm"] == 1) {
						if(1) {
							// BOF GET DATAS
							$where = "";
							$dateField = "";

							if($_POST["searchInterval"] == "WEEK"){
								$where .= "";
								$dateField = "
									CONCAT(
										IF(DATE_FORMAT(`{###TABLE_NAME###}`.`{###TABLE_DATE_FIELD###}`, '%m') = '12' AND DATE_FORMAT(`{###TABLE_NAME###}`.`{###TABLE_DATE_FIELD###}`, '%v') = '01', (DATE_FORMAT(`{###TABLE_NAME###}`.`{###TABLE_DATE_FIELD###}`, '%Y') + 1),  DATE_FORMAT(`{###TABLE_NAME###}`.`{###TABLE_DATE_FIELD###}`, '%Y')),
										'#',
										DATE_FORMAT(`{###TABLE_NAME###}`.`{###TABLE_DATE_FIELD###}`, '%v')
									) AS `interval`
								";
							}
							else if($_POST["searchInterval"] == "MONTH"){
								// DATE_FORMAT(`" . TABLE_ORDER_INVOICES . "`.`BESTELLDATUM`, '%M') AS `interval`,
								$where .= "";

								$dateField = "
									CONCAT(
										DATE_FORMAT(`{###TABLE_NAME###}`.`{###TABLE_DATE_FIELD###}`, '%Y'),
										'#',
										DATE_FORMAT(`{###TABLE_NAME###}`.`{###TABLE_DATE_FIELD###}`, '%m')
									) AS `interval`
								";
							}
							else if($_POST["searchInterval"] == "QUARTER"){
								$where .= "";

								$dateField = "
									CONCAT(
										DATE_FORMAT(`{###TABLE_NAME###}`.`{###TABLE_DATE_FIELD###}`, '%Y'),
										'#',
										IF(DATE_FORMAT(`{###TABLE_NAME###}`.`{###TABLE_DATE_FIELD###}`, '%m') = '01' OR DATE_FORMAT(`{###TABLE_NAME###}`.`{###TABLE_DATE_FIELD###}`, '%m') = '02' OR DATE_FORMAT(`{###TABLE_NAME###}`.`{###TABLE_DATE_FIELD###}`, '%m') = '03', '1. Quartal',
											IF(DATE_FORMAT(`{###TABLE_NAME###}`.`{###TABLE_DATE_FIELD###}`, '%m') = '04' OR DATE_FORMAT(`{###TABLE_NAME###}`.`{###TABLE_DATE_FIELD###}`, '%m') = '05' OR DATE_FORMAT(`{###TABLE_NAME###}`.`{###TABLE_DATE_FIELD###}`, '%m') = '06', '2. Quartal',
												IF(DATE_FORMAT(`{###TABLE_NAME###}`.`{###TABLE_DATE_FIELD###}`, '%m') = '07' OR DATE_FORMAT(`{###TABLE_NAME###}`.`{###TABLE_DATE_FIELD###}`, '%m') = '08' OR DATE_FORMAT(`{###TABLE_NAME###}`.`{###TABLE_DATE_FIELD###}`, '%m') = '09', '3. Quartal',
													IF(DATE_FORMAT(`{###TABLE_NAME###}`.`{###TABLE_DATE_FIELD###}`, '%m') = '10' OR DATE_FORMAT(`{###TABLE_NAME###}`.`{###TABLE_DATE_FIELD###}`, '%m') = '11' OR DATE_FORMAT(`{###TABLE_NAME###}`.`{###TABLE_DATE_FIELD###}`, '%m') = '12', '4. Quartal',
														''
													)
												)
											)
										)
									) AS `interval`
								";

							}
							else if($_POST["searchInterval"] == "YEAR"){
								$where .= "";

								$dateField = "
									CONCAT(
										DATE_FORMAT(`{###TABLE_NAME###}`.`{###TABLE_DATE_FIELD###}`, '%Y'),
										'#',
										DATE_FORMAT(`{###TABLE_NAME###}`.`{###TABLE_DATE_FIELD###}`, '%Y')
									) AS `interval`
								";

							}

							$arrIntervals = array();
							$arrDatas = array();

							$sql = "
								SELECT
									" . preg_replace("/{###TABLE_NAME###}/", TABLE_AMAZON_ORDERS, preg_replace("/{###TABLE_DATE_FIELD###}/", "BESTELLDATUM", $dateField)) . ",

									COUNT(`" . TABLE_AMAZON_ORDERS . "`.`COUNT`) AS `countOrders`,
									SUM(`" . TABLE_AMAZON_ORDERS . "`.`MENGE`) AS `countItems`

									FROM `" . TABLE_AMAZON_ORDERS . "`
									/*
									LEFT JOIN `" . TABLE_AMAZON_ORDERS_TRANSACTIONS . "`
									ON(`" . TABLE_AMAZON_ORDERS . "`.`AMAZON-BESTELLNUMMER` = `" . TABLE_AMAZON_ORDERS_TRANSACTIONS . "`.`Transaktionsnummer`)
									*/
									WHERE 1
										" . $where . "

									GROUP BY `interval`

									ORDER BY `interval` DESC
								";

							$rs = $dbConnection->db_query($sql);

							$countTotalRows = $dbConnection->db_getMysqlNumRows($rs);

							while($ds = mysqli_fetch_assoc($rs)) {
								$arrIntervals[] = $ds["interval"];
								$arrLabelsGraph[$ds["interval"]] = $ds["interval"];
								$arrDatasGraph["countItems"][$ds["interval"]] = $ds["countItems"];
								$arrDatasGraph["countOrders"][$ds["interval"]] = $ds["countOrders"];
								// $arrDatasGraph["totalPrice"][$ds["interval"]] = $ds["totalPrice"];
								foreach(array_keys($ds) as $field){
									$arrDatas[$ds["interval"]][$field] = $ds[$field];
								}
							}

							$sql = "
								SELECT
									" . preg_replace("/{###TABLE_NAME###}/", TABLE_AMAZON_ORDERS_TRANSACTIONS, preg_replace("/{###TABLE_DATE_FIELD###}/", "Datum_Uhrzeit", $dateField)) . ",

									SUM(`" . TABLE_AMAZON_ORDERS_TRANSACTIONS . "`.`Gesamt`) AS `sumTotalTransactions`,
									SUM(`" . TABLE_AMAZON_ORDERS_TRANSACTIONS . "`.`Menge`) AS `sumItemsTransactions`,
									IF(`" . TABLE_AMAZON_ORDERS_TRANSACTIONS . "`.`Typ` = 'Bestellung', 'BESTELLUNG',
										IF(`" . TABLE_AMAZON_ORDERS_TRANSACTIONS . "`.`Typ` = 'Übertrag', 'UEBERTRAG',
											IF(`" . TABLE_AMAZON_ORDERS_TRANSACTIONS . "`.`Typ` = 'Servicegebühr', 'SERVICEGEBUEHR',
												'NN'
											)
										)
									) AS `sumTypeTransactions`

									FROM `" . TABLE_AMAZON_ORDERS_TRANSACTIONS . "`

									WHERE 1
										" . $where . "

									GROUP BY CONCAT(`interval`, `Typ`)

									ORDER BY `interval` DESC
								";
							$rs = $dbConnection->db_query($sql);

							while($ds = mysqli_fetch_assoc($rs)) {
								if($ds["sumTypeTransactions"] == 'BESTELLUNG'){
									$arrDatasGraph["sumItemsTransactions"][$ds["interval"]] = $ds["sumItemsTransactions"];
									$arrDatasGraph["sumTotalTransactions"][$ds["interval"]] = $ds["sumTotalTransactions"];
									// $arrDatasGraph["totalPrice"][$ds["interval"]] = $ds["totalPrice"];
								}
								foreach(array_keys($ds) as $field){
									#$arrDatas[$ds["interval"]][$field] = $ds[$field];
									$arrDatas[$ds["interval"]][$ds["sumTypeTransactions"]][$field] = $ds[$field];
								}
							}
							##dd('arrDatasGraph');
							##dd('arrDatas');

							if(!empty($arrIntervals)){
								$arrIntervals = array_unique($arrIntervals);
								arsort($arrIntervals);
							}

							if($countTotalRows > 0) {
							?>
								<!-- BOF GRAPH ELEMENTS -->
								<script type="text/javascript" language="javascript" src="<?php echo PATH_RGRAPH; ?>libraries/RGraph.common.core.js" ></script>
								<script type="text/javascript" language="javascript" src="<?php echo PATH_RGRAPH; ?>libraries/RGraph.bar.js" ></script>
								<script type="text/javascript" language="javascript" src="<?php echo PATH_RGRAPH; ?>libraries/RGraph.line.js" ></script>

								<div id="tabs">
									<ul>
										<li><a href="#tabs-1">Anzahl der Bestellungen</a></li>
										<li><a href="#tabs-2">Summe der Betr&auml;ge</a></li>
										<li><a href="#tabs-3">Summe der Artikel</a></li>
									</ul>
									<div id="tabs-1">
										<div class="adminEditArea">
											<h2>Anzahl der Bestellungen</h2>
											<canvas id="cvs_dataCountOrders" width="900" height="250">[No canvas support]</canvas>
										</div>
									</div>
									<div id="tabs-2">
										<div class="adminEditArea">
											<h2>Summe der Betr&auml;ge</h2>
											<canvas id="cvs_dataSumTotal" width="900" height="250">[No canvas support]</canvas>
										</div>
									</div>
									<div id="tabs-3">
										<div class="adminEditArea">
											<h2>Summe der Artikel</h2>
											<canvas id="cvs_dataCountItems" width="900" height="250">[No canvas support]</canvas>
										</div>
									</div>
								</div>
								<!-- EOF GRAPH ELEMENTS -->

							<?php
								echo '<table width="100%" cellpadding="0" cellspacing="0" class="displayOrders">';

								echo '<tr>';
								echo '<th style="width:45px">#</th>';
								echo '<th style="width:80px">Zeitraum</th>';
								echo '<th>Anzahl der Bestellungen</th>';
								echo '<th>St&uuml;ckzahl</th>';
								echo '<th>Bestell-Betrag</th>';
								echo '<th>Konto-&Uuml;bertrag</th>';
								echo '<th>Service-Geb&uuml;hr</th>';
								echo '<th style="width:50px">Info</th>';
								echo '</tr>';

								$countRow = 0;
								$thisMarker = "";

								foreach($arrIntervals as $thisIntervalKey => $thisIntervalValue) {
									if($arrDatas[$thisIntervalValue]['countItems'] > 0){
										if($countRow == 0) {

										}

										$rowClass = 'row0';
										if($countRow%2 == 0) {
											$rowClass = 'row1';
										}
										$arrTemp = explode('#', $thisIntervalValue);
										if($thisMarker != $arrTemp[0]) {
											$thisMarker = $arrTemp[0];
											echo '<tr><td colspan="8" class="tableRowTitle1">' . $arrTemp[0] . '</td></tr>';
										}

										echo '<tr class="'.$rowClass.'">';

										echo '<td style="text-align:right;"><b>';
										echo ($countRow + 1);
										echo '.</b></td>';
										echo '<td>';

										if($_POST["searchInterval"] == "MONTH"){
											$thisMonth = getTimeNames($arrTemp[1], 'month', 'long');
											echo $thisMonth;
										}
										else {
											if($_POST["searchInterval"] == "WEEK"){ echo 'KW '; }
											echo $arrTemp[1];
										}
										echo '</td>';
										echo '<td style="text-align:right;background-color:#FEFFAF;">';
										echo '' . $arrDatas[$thisIntervalValue]['countOrders'] . '';
										echo '</td>';

										echo '<td style="text-align:right;background-color:#FFF;">';
										echo '' . number_format($arrDatas[$thisIntervalValue]['countItems'], '0', '', '') . '';
										echo '</td>';

										echo '<td style="text-align:right;background-color:#FEFFAF;">';
										echo '' . number_format($arrDatas[$thisIntervalValue]["BESTELLUNG"]['sumTotalTransactions'], 2, ',', '.') . ' &euro;';
										echo '</td>';

										echo '<td style="text-align:right;background-color:#FEFFAF;">';
										echo '' . number_format($arrDatas[$thisIntervalValue]["UEBERTRAG"]['sumTotalTransactions'], 2, ',', '.') . ' &euro;';
										echo '</td>';

										echo '<td style="text-align:right;background-color:#FEFFAF;">';
										echo '' . number_format($arrDatas[$thisIntervalValue]["SERVICEGEBUEHR"]['sumTotalTransactions'], 2, ',', '.') . ' &euro;';
										echo '</td>';

										echo '<td>';
										echo '<span class="toolItem">';
										echo '<img src="layout/icons/iconInfo.png" class="buttonShopOrderInfo" alt="Details" title="Detailinformationen ansehen">';
										echo '<div class="orderDetailsContent" style="display:none; visibility:hidden;">';
										echo '</div>';
										echo '</span>';
										echo '</td>';
										echo '</tr>';
										$countRow++;
									}
								}
								echo '</table>';
							}
							else {
								$warningMessage .= 'F&uuml;r den gew&auml;hlten Zeitraum wurden keine Daten gefunden.';
							}
							// EOF GET DATAS
						}
					?>
					<?php displayMessages(); ?>
				</div>

			</div>
		</div>
		<div class="clear"></div>
	</div>
</div>
<script language="javascript" type="text/javascript">
	$(document).ready(function() {
		$(function() {
			colorRowMouseOver('.displayOrders tbody tr');
			$('.buttonShopOrderInfo').css('cursor', 'pointer');
			$('.buttonShopOrderInfo').click(function () {
				loadOrderDetails($(this), '<?php echo BASEPATH; ?>', 'Details der Bestellung');
			});

			// BOF GET DATAS FOR GRAPH CASH
			<?php
				if(!empty($arrDatasGraph)){
					$arrDatasGraph["countItems"] = array_reverse($arrDatasGraph["countItems"], true);
					$arrDatasGraph["countOrders"] = array_reverse($arrDatasGraph["countOrders"], true);
					$arrDatasGraph["sumTotalTransactions"] = array_reverse($arrDatasGraph["sumTotalTransactions"], true);
					$arrLabelsGraph = array_reverse($arrLabelsGraph , true);
				}
			?>

			createGraph('cvs_dataCountItems', [<?php echo implode(",", array_values($arrDatasGraph["countItems"])); ?>], [<?php echo "'" . implode("','", $arrLabelsGraph) . "'"; ?>], 'Line', [['green','red']]);
			createGraph('cvs_dataCountOrders', [<?php echo implode(",", array_values($arrDatasGraph["countOrders"])); ?>], [<?php echo "'" . implode("','", $arrLabelsGraph) . "'"; ?>], 'Line', [['green','red']]);
			createGraph('cvs_dataSumTotal', [<?php echo implode(",", array_values($arrDatasGraph["sumTotalTransactions"])); ?>], [<?php echo "'" . implode("','", $arrLabelsGraph) . "'"; ?>], 'Line', [['green','red']]);

		// EOF GET DATAS FOR GRAPH CASH
		});
		$(function() {
			$('#tabs').tabs();
		});
	});
</script>
<?php require_once('inc/footerHTML.inc.php'); ?>