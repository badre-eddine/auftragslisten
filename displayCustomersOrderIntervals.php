<?php
	require_once('inc/requires.inc.php');

	if(!$arrGetUserRights["displayOnlineOrders"] && !$arrGetUserRights["importOnlineOrders"] && !$arrGetUserRights["displaySales"] && !$arrGetUserRights["displayDistribution"]) {
		header('location: ' . PAGE_EXIT_LOCATION);
		exit;
	}

	if($_POST["displayOrdersCount"] !=""){
		$displayOrdersCount = $_POST["displayOrdersCount"];
	}
	else {
		$displayOrdersCount = DEFAULT_ONLINE_ORDERS_MONTHS;
	}

	if($_REQUEST["tab"] == "") {
		$_REQUEST["tab"] = "tabs-1";
	}

	/*
	$arrPaymentTypes = array (
		"banktransfer" => ("banktransfer"),
		"cash" => ("cash"),
		"cc" => ("cc"),
		"cod" => ("cod"),
		"invoice" => ("invoice"),
		"moneyorder" => ("moneyorder"),
	);
	*/

	$jswindowMessage = "";
	$warningMessage = '';
	$errorMessage = '';
	$successMessage = '';
	$infoMessage = '';

	$dbConnection = new DB_Connection();
	$db_open = $dbConnection->db_connect();

	// BOF GET SALESMAN ZIPCODES
		$arrRelationSalesmenZipcode = getRelationSalesmenZipcode2();
	// EOF GET SALESMAN ZIPCODES

	// BOF READ COUNTRIES
		$arrCountryTypeDatas = getCountryTypes();
	// EOF READ COUNTRIES

	// BOF GET MANDATORIES
		$arrMandatories = getMandatoriesShort();
	// EOF GET MANDATORIES
?>

<?php
	require_once('inc/headerHTML.inc.php');
	if($_REQUEST["tab"] == "tabs-1"){
		$thisTitle = "Bestell-Intervalle der Kunden";
	}
	else if($_REQUEST["tab"] == "tabs-2"){
		$thisTitle = "Kunden, die nur einmal bestellt haben";
	}
	else if($_REQUEST["tab"] == "tabs-3"){
		$thisTitle = "Kunden, die keinmal bestellt haben";
	}
	else if($_REQUEST["tab"] == "tabs-4"){
		$thisTitle = "Shop-Kunden, die keinmal bestellt haben";
	}

	$headerHTML = preg_replace("/{###TITLE###}/", strip_tags($thisTitle), $headerHTML);
	echo $headerHTML;

	require_once(FILE_MENUE_TOP);
?>

<div id="menueSidebarToggleArea">
	<img src="layout/icons/iconToggle.png" class="buttonToggleSidebarMenue" width="10" height="10" alt="" title="Seitennavigation ein-/ausblenden" />
	<div id="menueSidebarToggleContent">
	<?php require_once(FILE_MENUE_SIDEBAR); ?>
	<div class="clear"></div>
	</div>
</div>
<div id="contentArea2">
	<div id="contentAreaElements">
		<h1><?php if(SHOW_PAGE_TITLE_ICONS) { echo '<img src="' . PATH_ICONS_MENUE_TITLES . 'calendar.png" alt="" />'; } ?> <?php echo $thisTitle; ?></h1>
		<div class="contentDisplay">

		<?php
			$todayDate = date("Y-m-d");
			
			$intervalDaysStart = 30;
			$intervalDaysEnd = 30;
			
			$intervalMonthsStart = 3;
			$intervalMonthsEnd = 3;			
			
			// $startDate = date("Y-m-d", mktime(0, 0, 0, date("m"), (date("d") - $intervalDaysStart), date("Y")));			
			// $endDate = date("Y-m-d", mktime(0, 0, 0, date("m"), (date("d") + $intervalDaysEnd), date("Y")));
			
			$startDate = date("Y-m-d", mktime(0, 0, 0, (date("m") - $intervalMonthsStart) , (date("d")), date("Y")));
			$endDate = date("Y-m-d", mktime(0, 0, 0, (date("m") + $intervalMonthsEnd), (date("d")), date("Y")));

			if($_REQUEST["searchDateStart"] != ''){
				$startDate = formatDate($_REQUEST["searchDateStart"], 'store');
			}
			if($_REQUEST["searchDateEnd"] != ''){
				$endDate = formatDate($_REQUEST["searchDateEnd"], 'store');
			}
			$intervalDays = number_format((strtotime($endDate) - strtotime($startDate)) / (60 * 60 * 24), 0, ',', '.');
		?>
			<div id="tabs" class="ui-tabs ui-widget ui-widget-content ui-corner-all">
				<?php
					$thisPageUrl = $_SERVER["PHP_SELF"];
				?>
				<ul class="ui-tabs-nav ui-helper-reset ui-helper-clearfix ui-widget-header ui-corner-all">
					<li class="ui-state-default ui-corner-top"><a href="<?php echo $thisPageUrl; ?>?tab=tabs-1#tabs-1">Bestell-Intervalle der Kunden</a></li>
					<li class="ui-state-default ui-corner-top"><a href="<?php echo $thisPageUrl; ?>?tab=tabs-2#tabs-2">Kunden einmal bestellt</a></li>
					<li class="ui-state-default ui-corner-top"><a href="<?php echo $thisPageUrl; ?>?tab=tabs-3#tabs-3">Kunden ohne Bestellung</a></li>
					<li class="ui-state-default ui-corner-top"><a href="<?php echo $thisPageUrl; ?>?tab=tabs-4#tabs-4">Shop-Kunden ohne Bestellung</a></li>
				</ul>

				<?php if($_REQUEST["tab"] == "tabs-1") { ?>
				<div id="tabs-1">
					<div id="searchFilterArea">
						<form name="formFilterSearch" method="get" action="<?php echo $_SERVER["PHP_SELF"]; ?>">
							<table border="0" cellpadding="0" cellspacing="0" class="searchFilterContent">
								<tr>
									<td>
										<label for="searchDateStart">von:</label>
										<input type="text" name="searchDateStart" id="searchDateStart" class="inputField_70" value="<?php echo formatDate($startDate, 'display'); ?>" />
									</td>
									<td>
										<label for="searchDateEnd">bis:</label>
										<input type="text" name="searchDateEnd" id="searchDateEnd" class="inputField_70" value="<?php echo formatDate($endDate, 'display'); ?>" />
									</td>
									<td>
										<input type="hidden" name="xxxeditID" id="xxxeditID" value="" />
										<input type="submit" name="submitSearch" class="inputButton0" value="Suchen" />
									</td>
								</tr>
							</table>
						</form>
					</div>

					<p class="infoArea">In der Berechnung werden geschriebene Auftragsbest&auml;tigungen und nur KZH zu Grunde gelegt!<br />Angezeigt werden Daten, mit dem vorraussichtlichen Bestelldatum zwischen <?php echo formatDate($startDate, 'display'); ?> und <?php echo formatDate($endDate, 'display'); ?></p>

					<?php displayMessages(); ?>

					<?php
						// SET [GLOBAL | SESSION] group_concat_max_len = val;

						// BOF GET DATE OF LAST TABLE UPDATE
							$sql ="SELECT UPDATE_TIME
										FROM `information_schema`.`tables`
										WHERE 1
											AND TABLE_SCHEMA = '" . DB_NAME . "'
											AND TABLE_NAME = '" . TABLE_CUSTOMERS_ORDERS_INTERVALS . "'
								";
							$rs = $dbConnection->db_query($sql);
							list($tableUpdateTime) = mysqli_fetch_array($rs);

							$tableUpdateTimeDisplay = $tableUpdateTime;

							$tableUpdateTime = substr($tableUpdateTime, 0, 10);
						// EOF GET DATE OF LAST TABLE UPDATE

						// echo '<p><b>Stand: </b>' . formatDate($tableUpdateTimeDisplay, 'display') . '</p>';
					
						if(1 || $todayDate > $tableUpdateTime){
							$sql = "
								SELECT
										`" . TABLE_ORDER_CONFIRMATIONS . "`.`orderDocumentsCustomerNumber`,
										COUNT(`" . TABLE_ORDER_CONFIRMATIONS . "`.`orderDocumentsCustomerNumber`) AS `countOrders`,
										GROUP_CONCAT(`" . TABLE_ORDER_CONFIRMATIONS . "`.`orderDocumentsDocumentDate` SEPARATOR '#') AS `documentDates`,
										/* GROUP_CONCAT(`" . TABLE_ORDER_CONFIRMATIONS . "`.`orderDocumentsOrderDate` SEPARATOR '#') AS `orderDates`, */
										GROUP_CONCAT(IF(`" . TABLE_ORDER_CONFIRMATIONS . "`.`orderDocumentsOrderDate` != '0000-00-00', `" . TABLE_ORDER_CONFIRMATIONS . "`.`orderDocumentsOrderDate`, `" . TABLE_ORDER_CONFIRMATIONS . "`.`orderDocumentsDocumentDate`) SEPARATOR '#') AS `orderDates`,
										GROUP_CONCAT(`" . TABLE_ORDER_CONFIRMATIONS_DETAILS . "`.`orderDocumentDetailProductName` SEPARATOR '#') AS `orderProductNames`,
										GROUP_CONCAT(`" . TABLE_ORDER_CONFIRMATIONS_DETAILS . "`.`orderDocumentDetailPosType` SEPARATOR '#') AS `orderType`,
										GROUP_CONCAT(`" . TABLE_ORDER_CONFIRMATIONS_DETAILS . "`.`orderDocumentDetailProductQuantity` SEPARATOR '#') AS `orderProductQuantities`

									FROM `" . TABLE_ORDER_CONFIRMATIONS . "`

									LEFT JOIN `" . TABLE_ORDER_CONFIRMATIONS_DETAILS . "`
									ON(`" . TABLE_ORDER_CONFIRMATIONS . "`.`orderDocumentsID` = `" . TABLE_ORDER_CONFIRMATIONS_DETAILS . "`.`orderDocumentDetailDocumentID` AND SUBSTRING(`" . TABLE_ORDER_CONFIRMATIONS_DETAILS . "`.`orderDocumentDetailProductKategorieID`, 1, 3) = '001')

									WHERE 1
										AND `" . TABLE_ORDER_CONFIRMATIONS_DETAILS . "`.`orderDocumentDetailPosType` = 'product'

									GROUP BY `" . TABLE_ORDER_CONFIRMATIONS . "`.`orderDocumentsCustomerNumber`

									HAVING `countOrders` > 1
								";

							$rs = $dbConnection->db_query($sql);

							$arrInsertSql = array();
							while($ds = mysqli_fetch_assoc($rs)){

								$thisSqlInsert = "";

								$arrThisDocumentDates = explode('#', $ds["documentDates"]);
								if(!empty($arrThisDocumentDates)){
									$arrThisDocumentDates = array_unique($arrThisDocumentDates);
									asort($arrThisDocumentDates);
								}

								if(count($arrThisDocumentDates) > 1){

									$arrThisDocumentDates = explode('#', $ds["documentDates"]);
									if(!empty($arrThisDocumentDates)){
										$arrThisDocumentDates = array_unique($arrThisDocumentDates);
										sort($arrThisDocumentDates);
										$thisAverageIntervalMilliSeconds = 0;
										$thisAverageIntervalDays = 0;
										$thisAverageIntervalMonths = 0;
										for($i = 0 ; $i < (count($arrThisDocumentDates) - 1) ; $i++){
											$thisAverageIntervalMilliSeconds += (strtotime($arrThisDocumentDates[($i + 1)]) - strtotime($arrThisDocumentDates[$i]));
										}

										// $thisAverageIntervalMilliSeconds = $thisAverageIntervalMilliSeconds / $ds["countOrders"];

										$thisAverageIntervalDays = ceil($thisAverageIntervalMilliSeconds / (3600 * 24) );
										$thisAverageIntervalMonths = ceil($thisAverageIntervalDays / (30) );
									}

									$thisDateLastOrder = $arrThisDocumentDates[(count($arrThisDocumentDates) - 1)];
									$thisSecondToLastOrder = $arrThisDocumentDates[(count($arrThisDocumentDates) - 2)];
									$thisProbableNextOrder = date("Y-m-d", (strtotime($arrThisDocumentDates[(count($arrThisDocumentDates) - 1)]) + $thisAverageIntervalMilliSeconds));

									$sql_insert = "
											REPLACE INTO `" . TABLE_CUSTOMERS_ORDERS_INTERVALS . "` (
															`customersOrdersIntervalsCustomerNumber`,
															`customersOrdersIntervalsDateLastOrder`,
															`customersOrdersIntervalsSecondToLastOrder`,
															`customersOrdersIntervalsProbableNextOrder`,
															`customersOrdersIntervalsAverageIntervalDays`,
															`customersOrdersIntervalsAverageIntervalMilliseconds`,
															`customersOrdersIntervalsAverageIntervalMonths`,
															`customersOrdersIntervalsCountOrders`
														)
														VALUES (
															'" . $ds["orderDocumentsCustomerNumber"] . "',
															'" . $thisDateLastOrder . "',
															'" . $thisSecondToLastOrder . "',
															'" . $thisProbableNextOrder . "',
															'" . $thisAverageIntervalDays . "',
															'" . $thisAverageIntervalMilliSeconds . "',
															'" . $thisAverageIntervalMonths . "',
															'" . $ds["countOrders"] . "'
														)
										";

									$rs_insert = $dbConnection->db_query($sql_insert);
								}
							}
						}

						$sql = "
							SELECT
									`" . TABLE_CUSTOMERS_ORDERS_INTERVALS . "`.`customersOrdersIntervalsCustomerNumber`,
									`" . TABLE_CUSTOMERS_ORDERS_INTERVALS . "`.`customersOrdersIntervalsDateLastOrder`,
									`" . TABLE_CUSTOMERS_ORDERS_INTERVALS . "`.`customersOrdersIntervalsSecondToLastOrder`,
									`" . TABLE_CUSTOMERS_ORDERS_INTERVALS . "`.`customersOrdersIntervalsProbableNextOrder`,
									`" . TABLE_CUSTOMERS_ORDERS_INTERVALS . "`.`customersOrdersIntervalsAverageIntervalDays`,
									`" . TABLE_CUSTOMERS_ORDERS_INTERVALS . "`.`customersOrdersIntervalsAverageIntervalMilliseconds`,
									`" . TABLE_CUSTOMERS_ORDERS_INTERVALS . "`.`customersOrdersIntervalsAverageIntervalMonths`,
									`" . TABLE_CUSTOMERS_ORDERS_INTERVALS . "`.`customersOrdersIntervalsCountOrders`,

									`customers`.`customersKundennummer`,
									`customers`.`customersFirmenname`,
									`customers`.`customersFirmennameZusatz`,
									`customers`.`customersCompanyPLZ`,
									`customers`.`customersCompanyOrt`,
									`customers`.`customersCompanyCountry`,
									`customers`.`customersCompanyStrasse`,
									`customers`.`customersCompanyHausnummer`,

									`customers`.`customersTelefon1`,
									`customers`.`customersTelefon2`,
									`customers`.`customersMobil1`,
									`customers`.`customersMobil2`,
									`customers`.`customersFax1`,
									`customers`.`customersFax2`,
									`customers`.`customersMail1`,
									`customers`.`customersMail2`,
									`customers`.`customersHomepage`,
									`customers`.`customersCompanyLocation`,

									`salesmen`.`customersID` AS `salesmenID`,
									`salesmen`.`customersKundennummer` AS `salesmenKundennummer`,
									`salesmen`.`customersFirmenname` AS `salesmenFirmenname`,
									`salesmen`.`customersFirmennameZusatz` AS `salesmenFirmennameZusatz`,
									`salesmen`.`customersCompanyPLZ` AS `salesmenCompanyPLZ`,
									`salesmen`.`customersCompanyOrt` AS `salesmenCompanyOrt`,
									`salesmen`.`customersCompanyStrasse` AS `salesmenCompanyStrasse`,
									`salesmen`.`customersCompanyHausnummer` AS `salesmenCompanyHausnummer`,
									
									`salesmen`.`customersTelefon1` AS `salesmenTelefon1`,
									`salesmen`.`customersTelefon2` AS `salesmenTelefon2`,
									`salesmen`.`customersMobil1` AS `salesmenMobil1`,
									`salesmen`.`customersMobil2` AS `salesmenMobil2`,
									`salesmen`.`customersFax1` AS `salesmenFax1`,
									`salesmen`.`customersFax2` AS `salesmenFax2`,
									`salesmen`.`customersMail1` AS `salesmenMail1`,
									`salesmen`.`customersMail2` AS `salesmenMail2`

								FROM `" . TABLE_CUSTOMERS_ORDERS_INTERVALS . "`

								LEFT JOIN `" . TABLE_CUSTOMERS . "` AS `customers`
								ON(`" . TABLE_CUSTOMERS_ORDERS_INTERVALS . "`.`customersOrdersIntervalsCustomerNumber` = `customers`.`customersKundennummer`)

								LEFT JOIN `" . TABLE_CUSTOMERS . "` AS `salesmen`
								ON(`customers`.`customersVertreterID` = `salesmen`.`customersID`)

								WHERE 1
									AND `" . TABLE_CUSTOMERS_ORDERS_INTERVALS . "`.`customersOrdersIntervalsProbableNextOrder` BETWEEN '" . $startDate . "' AND '" . $endDate . "'

								ORDER BY `" . TABLE_CUSTOMERS_ORDERS_INTERVALS . "`.`customersOrdersIntervalsProbableNextOrder` ASC
							";

						$rs = $dbConnection->db_query($sql);
						$displayOrdersCount = $dbConnection->db_getMysqlNumRows($rs);

						if($displayOrdersCount > 0) {
							echo '<table cellspacing="0" cellpadding="0" width="100%" class="displayOrders">';
							echo '<thead>';
							echo '<tr>';
							echo '<th rowspan="2" style="text-align:right;">#</th>';
							echo '<th colspan="5" style="text-align:center;">Bestelldaten</th>';
							echo '<th rowspan="2">K-NR</th>';
							echo '<th rowspan="2">Firma</th>';
							echo '<th rowspan="2">PLZ</th>';
							echo '<th rowspan="2">Ort</th>';
							echo '<th rowspan="2">Anschrift</th>';
							echo '<th rowspan="2">Kontakt</th>';
							echo '<th rowspan="2">Vertreter / Kunde von</th>';
							echo '<th rowspan="2">Gebiet von</th>';
							echo '<th rowspan="2">Info</th>';
							echo '</tr>';
							echo '<tr>';
							echo '<th>letzte</th>';
							echo '<th>vorletzte</th>';
							echo '<th>n&auml;chste ca.</th>';
							echo '<th style="width:200px;" colspan="2">Intervall-Durchschnitt</th>';
							#echo '<th style="width:20px;">Anz. Best.</th>';
							echo '</tr>';
							echo '</thead>';


							echo '<tbody>';

							$markerYear = '';
							$markerMonth = '';
							$marker = '';
							$countRow = 0;

							while ($ds = mysqli_fetch_assoc($rs)) {
								if($countRow%2 == 0){ $rowClass = 'row1'; }
								else { $rowClass = 'row0'; }

								$thisStyle = '';
								if($marker != $ds["customersOrdersIntervalsProbableNextOrder"]){
									if($countRow != 0){
										$thisStyle = 'border-top:2px solid #333';
									}
									$marker = $ds["customersOrdersIntervalsProbableNextOrder"];
								}

								echo '<tr class="' . $rowClass . '" style="' . $thisStyle . '">';

								echo '<td style="text-align:right;">';
								echo '<b>'.($countRow + 1).'.</b> ';
								echo '</td>';

								echo '<td>';
								echo formatDate($ds["customersOrdersIntervalsDateLastOrder"], 'display');
								echo '</td>';

								echo '<td>';
								echo formatDate($ds["customersOrdersIntervalsSecondToLastOrder"], 'display');
								echo '</td>';

								echo '<td>';
								echo formatDate($ds["customersOrdersIntervalsProbableNextOrder"], 'display');
								echo '</td>';

								echo '<td style="text-align:right;white-space:nowrap;">';
								echo 'ca. ' . $ds["customersOrdersIntervalsAverageIntervalDays"] . ' Tage';
								echo '</td>';
								echo '<td style="text-align:right;white-space:nowrap;">';
								echo '(ca. ' . $ds["customersOrdersIntervalsAverageIntervalMonths"] . ' Mon.)';
								echo '</td>';
								
								#echo '<td style="text-align:right;white-space:nowrap;">';
								#echo $ds["customersOrdersIntervalsCountOrders"] . 'x';
								#echo '</td>';

								echo '<td><b>';
								echo '<a href="' . PAGE_EDIT_CUSTOMER . '?searchCustomerNumber=' . $ds["customersOrdersIntervalsCustomerNumber"] . '">';
								echo $ds["customersOrdersIntervalsCustomerNumber"];
								echo '</b></a>';
								echo '</td>';

								echo '<td>';
								echo '<b>' . htmlentities($ds["customersFirmenname"]) . '</b>';
								if($ds["customersFirmennameZusatz"] != ''){
									echo '<br />' . htmlentities($ds["customersFirmennameZusatz"]);
								}
								echo '</td>';

								echo '<td style="white-space:nowrap;">';
								echo '<a href="' . PAGE_EDIT_CUSTOMER . '?searchBoxCustomer=' . $ds["customersCompanyPLZ"] . '" title="im Kundenbestand nach PLZ suchen" target="_blank">';
								echo $ds["customersCompanyPLZ"];
								echo '</a>';
								echo '</td>';

								echo '<td style="white-space:nowrap;">';
								echo htmlentities($ds["customersCompanyOrt"]);
								echo '</td>';

								echo '<td>';
								echo htmlentities($ds["customersCompanyStrasse"] . ' ' . $ds["customersCompanyHausnummer"]);
								echo '</td>';

								echo '<td style="white-space:nowrap;">';

								if($ds["customersTelefon1"] != ''){
									echo '<span class="dataList"> &bull; <b><img src="layout/icons/iconPhone2.png" width="14" height="14" alt="Tel." title="Tel." /></b>: ' . formatPhoneNumber($ds["customersTelefon1"]) . '</span>';
								}
								if($ds["customersTelefon2"] != ''){
									echo '<span class="dataList"> &bull; <b><img src="layout/icons/iconPhone2.png" width="14" height="14" alt="Tel." title="Tel." /></b>: ' . formatPhoneNumber($ds["customersTelefon2"]) . '</span>';
								}
								if($ds["customersMobil1"] != ''){
									echo '<span class="dataList"> &bull; <b><img src="layout/icons/iconMobile.png" width="14" height="14" alt="Mobil" title="Mobil" /></b>: ' . formatPhoneNumber($ds["customersMobil1"]) . '</span>';
								}
								if($ds["customersMobil2"] != ''){
									echo '<span class="dataList"> &bull; <b><img src="layout/icons/iconMobile.png" width="14" height="14" alt="Mobil" title="Mobil" /></b>: ' . formatPhoneNumber($ds["customersMobil2"]) . '</span>';
								}
								if($ds["customersMail1"] != ''){
									echo '<span class="dataList"> &bull; <b><img src="layout/icons/iconMail2.png" width="14" height="14" alt="Mail" title="Mail" /></b>: <a href="mailto:' . $ds["customersMail1"] . '">' . ($ds["customersMail1"]) . '</a></span>';
								}
								if($ds["customersMail2"] != ''){
									echo '<span class="dataList"> &bull; <b><img src="layout/icons/iconMail2.png" width="14" height="14" alt="Mail" title="Mail" /></b>: <a href="mailto:' . $ds["customersMail2"] . '">' . ($ds["customersMail2"]) . '</a></span>';
								}

								echo '</td>';

								echo '<td style="font-size:11px;">';
								if($ds["salesmenKundennummer"] != '48464'){
									echo '<b><a href="' . PAGE_EDIT_CUSTOMER . '?searchCustomerNumber=' . $ds["salesmenKundennummer"] . '">' . $ds["salesmenKundennummer"] . '</a></b><br />';
									echo '<b>' . htmlentities($ds["salesmenFirmenname"]) . '</b><br />';
									if($ds["salesmenFirmennameZusatz"] != ""){
										echo '<b>' . htmlentities($ds["salesmenFirmennameZusatz"]) . '</b><br />';
									}
									echo htmlentities($ds["salesmenCompanyStrasse"] . ' ' . $ds["salesmenCompanyHausnummer"]) . '<br />';
									echo htmlentities($ds["salesmenCompanyPLZ"] . ' ' . $ds["salesmenCompanyOrt"]) . '<br />';
									
									/*
									if($ds["salesmenTelefon1"] != ''){
										echo '<span class="dataList"> &bull; <b><img src="layout/icons/iconPhone2.png" width="14" height="14" alt="Tel." title="Tel." /></b>: ' . formatPhoneNumber($ds["salesmenTelefon1"]) . '</span>';
									}
									if($ds["salesmenTelefon2"] != ''){
										echo '<span class="dataList"> &bull; <b><img src="layout/icons/iconPhone2.png" width="14" height="14" alt="Tel." title="Tel." /></b>: ' . formatPhoneNumber($ds["salesmenTelefon2"]) . '</span>';
									}
									if($ds["salesmenMobil1"] != ''){
										echo '<span class="dataList"> &bull; <b><img src="layout/icons/iconMobile.png" width="14" height="14" alt="Mobil" title="Mobil" /></b>: ' . formatPhoneNumber($ds["salesmenMobil1"]) . '</span>';
									}
									if($ds["salesmenMobil2"] != ''){
										echo '<span class="dataList"> &bull; <b><img src="layout/icons/iconMobile.png" width="14" height="14" alt="Mobil" title="Mobil" /></b>: ' . formatPhoneNumber($ds["salesmenMobil2"]) . '</span>';
									}
									if($ds["salesmenMail1"] != ''){
										echo '<span class="dataList"> &bull; <b><img src="layout/icons/iconMail2.png" width="14" height="14" alt="Mail" title="Mail" /></b>: <a href="mailto:' . $ds["salesmenMail1"] . '">' . ($ds["salesmenMail1"]) . '</a></span>';
									}
									if($ds["salesmenMail2"] != ''){
										echo '<span class="dataList"> &bull; <b><img src="layout/icons/iconMail2.png" width="14" height="14" alt="Mail" title="Mail" /></b>: <a href="mailto:' . $ds["salesmenMail2"] . '">' . ($ds["salesmenMail2"]) . '</a></span>';
									}
									*/
								}
								echo '</td>';

								echo '<td style="white-space:nowrap;">';
									if($ds["customersCompanyCountry"] == '81'){
										$thisZipcodeDoubleDigit = substr($ds["customersCompanyPLZ"], 0, 2);
										foreach($arrRelationSalesmenZipcode[$thisZipcodeDoubleDigit] as $thisRelationSalesmenZipcodeData){
											// $thisRelationSalesmenZipcodeData["kundenPLZaktiv"]
											echo '<span class="dataList"> &bull; ' . $thisRelationSalesmenZipcodeData["kundenname"] . ' [' . '<a href="' . PAGE_EDIT_CUSTOMER . '?searchBoxCustomer=' . $thisRelationSalesmenZipcodeData["kundennummer"] . '">' . $thisRelationSalesmenZipcodeData["kundennummer"] . '] ' . '</a></span>';
										}
									}
								echo '</td>';

								echo '<td style="white-space:nowrap;">';
								if($ds["customersHomepage"] != "") {
									echo '<span class="toolItem"><a href="http://'.$ds["customersHomepage"].'" target="_blank"><img src="layout/icons/iconWeb.gif" width="16" height="16" title="Homepage des Kunden aufrufen" alt="Homepage" /></a></span>';
								}
								else {
									echo '<span class="toolItem"><img src="layout/spacer.gif" width="16" height="16" alt="" /></span>';
								}

								// BOF SHOW IN GOOGLE MAPS
									if($ds["customersCompanyLocation"] != "") {
										$thisGoogleMapsURL = PATH_GOOGLE_MAPS_URL_LATLON;
										$arrCustomerLocationData = explode(":", $ds["customersCompanyLocation"]);
										$thisGoogleMapsURL = preg_replace("/{###LAT###}/", $arrCustomerLocationData[0], $thisGoogleMapsURL );
										$thisGoogleMapsURL = preg_replace("/{###LNG###}/", $arrCustomerLocationData[1], $thisGoogleMapsURL );
										echo '<span class="toolItem"><a href="' . $thisGoogleMapsURL . '" target="_blank"><img src="layout/menueIcons/menueSidebar/googleMaps.png" width="16" height="16" title="' . $thisGoogleMapsURL . '" alt="GoogleMaps" /></a></span>';
									}
									else {
										$thisGoogleMapsURL = PATH_GOOGLE_MAPS;
										$thisGoogleMapsURL = preg_replace("/{###STREET_NAME###}/", urlencode($ds["customersCompanyStrasse"]), $thisGoogleMapsURL );
										$thisGoogleMapsURL = preg_replace("/{###STREET_NUMBER###}/", urlencode($ds["customersCompanyHausnummer"]), $thisGoogleMapsURL );
										$thisGoogleMapsURL = preg_replace("/{###CITY_NAME###}/", urlencode($ds["customersCompanyOrt"]), $thisGoogleMapsURL );
										$thisGoogleMapsURL = preg_replace("/{###CITY_ZIP_CODE###}/", urlencode($ds["customersCompanyPLZ"]), $thisGoogleMapsURL );
										echo '<span class="toolItem"><a href="' . $thisGoogleMapsURL . '" target="_blank"><img src="layout/menueIcons/menueSidebar/googleMaps.png" width="16" height="16" title="' . $thisGoogleMapsURL . '" alt="GoogleMaps" /></a></span>';
									}
								// EOF SHOW IN GOOGLE MAPS

								// BOF SEARCH VIA GOOGLE
									$thisGoogleSearchUrl = 'https://www.google.de/search?q=';
									$thisGoogleSearchParams = '';
									$thisGoogleSearchParams .= '' . $ds["customersFirmenname"];
									$thisGoogleSearchParams .= ' ' . $ds["customersCompanyPLZ"];
									$thisGoogleSearchParams .= ' ' . $ds["customersCompanyOrt"];
									$thisGoogleSearchParams .= ' ' . $ds["customersCompanyStrasse"];
									$thisGoogleSearchParams .= ' ' . $ds["customersCompanyHausnummer"];
									$thisGoogleSearchParams = urlencode($thisGoogleSearchParams);
									$thisGoogleSearchUrl .= $thisGoogleSearchParams;
									echo '<span class="toolItem"><a href="'.$thisGoogleSearchUrl.'" target="_blank"><img src="layout/icons/iconGoogle1.png" width="16" height="16" target="_blank" title="Kunden bei Google suchen" alt="Google-Suche" /></a></span>';
								// EOF SEARCH VIA GOOGLE

								echo '</td>';

								/*
								echo '<td style="text-align:right;">';
									echo '<span class="toolItem">';
									echo '<img src="layout/icons/iconInfo.png" class="buttonShopOrderInfo" alt="Details" title="Detailinformationen zu dieser Bestellung ansehen">';
									echo '<div class="orderDetailsContent" style="display:none; visibility:hidden;">';

									$arrTempProductNames = explode("#", $ds["orderProductNames"]);
									$arrTempProductQuantities = explode("#", $ds["orderProductQuantities"]);

									if(count($arrTempProductNames) > 0){
										echo '<table border="1" cellpadding="0" cellspacing="0" class="orderDetails">';
										echo '<tr>';
										echo '<th style="width:50px;">#</th>';
										echo '<th>Datum</th>';
										echo '<th>Menge</th>';
										echo '<th>Kennzeichenhalter</th>';
										echo '</tr>';
										for($i = 0 ; $i < count($arrTempProductNames); $i++){
											echo '<tr>';
											echo '<td style="text-align:right;"><b>' . ($i + 1) . '.</b></td>';
											echo '<td style="text-align:right;">' . formatDate($arrThisDocumentDates[$i], 'display') . '</td>';
											echo '<td style="text-align:right;">' . $arrTempProductQuantities[$i] . '</td>';
											echo '<td style="text-align:right;">' . $arrTempProductNames[$i] . '</td>';
											echo '</tr>';
										}
										echo '</table>';
									}
									echo '</div>';
									echo '</span>';
								echo '</td>';
								*/

								echo '</tr>';

								$countRow++;

							}
							echo '</tbody>';

							echo '</table>';
						}
						else {
							$infoMessage .= 'Moment liegen keine Daten vor. ' . '</br>';
						}
					?>

					<?php displayMessages(); ?>

				</div>
				<?php } ?>

				<?php if($_REQUEST["tab"] == "tabs-2") { ?>
				<div id="tabs-2">
					<p class="infoArea">In der Berechnung werden geschriebene Auftragsbest&auml;tigungen zu Grunde gelegt!<br />Es besteht die M&ouml;glichkeit, dass Kunden doppelt angelegt sind und dieser Kunde &uuml;ber eine andere Kundennummer bestellt hat!</p>
					<div id="searchFilterArea">
						<form name="formFilterSearch" method="get" action="<?php echo $_SERVER["PHP_SELF"]; ?>">
							<table border="0" cellpadding="0" cellspacing="0" class="searchFilterContent">
								<tr>
									<td>
										<label for="searchPLZ">PLZ:</label>
										<input type="text" name="searchPLZ" id="searchPLZ" class="inputField_70" value="<?php echo $_REQUEST["searchPLZ"]; ?>" />
									</td>
									<td>
										<input type="hidden" name="tab" value="tabs-2" />
										<input type="submit" name="submitSearch" class="inputButton0" value="Suchen" />
									</td>
								</tr>
							</table>
						</form>
					</div>

					<?php
						$sqlWhere = "";

						if($_REQUEST["searchPLZ"] != ""){
							$sqlWhere = " AND `customers`.`customersCompanyPLZ` LIKE '" . $_REQUEST["searchPLZ"] . "%' ";
						}

						if(!empty($arrMandatories)){
							$arrSql_getCustomersOneOrder = array();

							foreach($arrMandatories as $thisMandatoryKey => $thisMandatoryValue){
								$thisDocumentTable = preg_replace("/^" . MANDATOR . "_/", $thisMandatoryKey . "_", TABLE_ORDER_CONFIRMATIONS);

								$arrSql_getCustomersOneOrder[] = "
										SELECT
										`" . $thisDocumentTable . "`.`orderDocumentsID`,
										`customers`.`customersKundennummer`,
										`customers`.`customersFirmenname`,
										`customers`.`customersFirmennameZusatz`,
										`customers`.`customersCompanyPLZ`,
										`customers`.`customersCompanyOrt`,
										`customers`.`customersCompanyCountry`,
										`customers`.`customersCompanyStrasse`,
										`customers`.`customersCompanyHausnummer`,

										`customers`.`customersTelefon1`,
										`customers`.`customersTelefon2`,
										`customers`.`customersMobil1`,
										`customers`.`customersMobil2`,
										`customers`.`customersFax1`,
										`customers`.`customersFax2`,
										`customers`.`customersMail1`,
										`customers`.`customersMail2`,
										`customers`.`customersHomepage`,
										`customers`.`customersGruppe`,

										`customers`.`customersCompanyLocation`,

										`" . $thisDocumentTable . "`.`orderDocumentsCustomerNumber`,
										`" . $thisDocumentTable . "`.`orderDocumentsDocumentDate`,

										`salesmen`.`customersID` AS `salesmenID`,
										`salesmen`.`customersKundennummer` AS `salesmenKundennummer`,
										`salesmen`.`customersFirmenname` AS `salesmenFirmenname`,
										`salesmen`.`customersFirmennameZusatz` AS `salesmenFirmennameZusatz`,
										`salesmen`.`customersCompanyPLZ` AS `salesmenCompanyPLZ`,
										`salesmen`.`customersCompanyOrt` AS `salesmenCompanyOrt`,
										`salesmen`.`customersCompanyStrasse` AS `salesmenCompanyStrasse`,
										`salesmen`.`customersCompanyHausnummer` AS `salesmenCompanyHausnummer`,
										`salesmen`.`customersTelefon1` AS `salesmenTelefon1`,
										`salesmen`.`customersTelefon2` AS `salesmenTelefon2`,
										`salesmen`.`customersMobil1` AS `salesmenMobil1`,
										`salesmen`.`customersMobil2` AS `salesmenMobil2`,
										`salesmen`.`customersFax1` AS `salesmenFax1`,
										`salesmen`.`customersFax2` AS `salesmenFax2`,
										`salesmen`.`customersMail1` AS `salesmenMail1`,
										`salesmen`.`customersMail2` AS `salesmenMail2`

									FROM `" . TABLE_CUSTOMERS . "` AS `customers`

									LEFT JOIN `" . TABLE_CUSTOMERS . "` AS `salesmen`
									ON(`customers`.`customersVertreterID` = `salesmen`.`customersID`)

									INNER JOIN `" . $thisDocumentTable . "`
									ON(`customers`.`customersKundennummer` = `" . $thisDocumentTable . "`.`orderDocumentsCustomerNumber`)

									WHERE 1
										AND `customers`.`customersActive` = '1'
										AND `customers`.`customersCompanyCountry` = '81'
										AND `customers`.`customersTyp` IN('1', '5', '6')
										/* AND `customers`.`customersTyp` IN('1', '2', '3', '4', '5', '6') */
									";
							}
						}

						if(!empty($arrSql_getCustomersOneOrder)){
							$sql_getCustomersOneOrder = "
									SELECT
											*,
											COUNT(`tempTable`.`orderDocumentsID`) AS `countOrders`

										FROM (
											" . implode(" UNION ", $arrSql_getCustomersOneOrder) . "
										) AS `tempTable`

										GROUP BY
											`tempTable`.`customersKundennummer`

										HAVING `countOrders` = '1'

										ORDER BY
											`tempTable`.`orderDocumentsDocumentDate` ASC,
											`tempTable`.`customersKundennummer` ASC
								";

						}

						// BOF GET COUNT ALL ROWS
							$sql_getAllRows = "
									SELECT
										COUNT(`tempTable2`.`customersKundennummer`) AS `countAllRows`

										FROM (
											" . $sql_getCustomersOneOrder . "
										) AS `tempTable2`

								";

							$rs_getAllRows = $dbConnection->db_query($sql_getAllRows);
							list(
								$countAllRows
							) = mysqli_fetch_array($rs_getAllRows);
						// EOF GET COUNT ALL ROWS

						if($_REQUEST["page"] == "" || $_REQUEST["page"] == 0) {
							$_REQUEST["page"] = 1;
						}
						if(MAX_ORDERS_PER_PAGE > 0) {
							$sql_getCustomersOneOrder .= " LIMIT " . (($_REQUEST["page"] - 1) * MAX_ORDERS_PER_PAGE) . ", " . MAX_ORDERS_PER_PAGE." ";
						}

						$rs_getCustomersOneOrder = $dbConnection->db_query($sql_getCustomersOneOrder);

						$countRows = $countAllRows;

						$pagesCount = ceil($countRows / MAX_ORDERS_PER_PAGE);

						if($countRows > 0){
							echo '<p class="infoArea">Gefundene Kunden mit nur einer Bestellung: ' . $countRows . '</p>';

							if($pagesCount > 1) {
								include(FILE_MENUE_PAGES);
							}

							echo '<table cellspacing="0" cellpadding="0" width="100%" class="displayOrders">';
							echo '<thead>';
							echo '<tr>';

							echo '<th>#</th>';
							echo '<th>Bestellung</th>';
							echo '<th>KNR</th>';
							echo '<th>Firma</th>';
							echo '<th>PLZ</th>';
							echo '<th>Ort</th>';
							echo '<th>Adresse</th>';
							echo '<th>Kontakt</th>';
							echo '<th>Vertreter / Kunde von</th>';
							echo '<th>Gebiet von</th>';
							echo '<th>Info</th>';

							echo '</tr>';
							echo '</thead>';
							echo '<tbody>';

							$countRow = 0;

							while($ds_getCustomersOneOrder = mysqli_fetch_assoc($rs_getCustomersOneOrder)){
								if($countRow%2 == 0){ $rowClass = 'row1'; }
								else { $rowClass = 'row0'; }

								$thisStyle = '';

								echo '<tr class="' . $rowClass . '" style="' . $thisStyle . '">';

								echo '<td style="text-align:right;">';
								echo '<b>'.($countRow + 1).'.</b> ';
								echo '</td>';

								echo '<td>';
								echo formatDate($ds_getCustomersOneOrder["orderDocumentsDocumentDate"], 'display');
								echo '</td>';

								echo '<td style="font-weight:bold;">';
								echo '<a href="' . PAGE_EDIT_CUSTOMER . '?searchCustomerNumber=' . $ds_getCustomersOneOrder["customersKundennummer"] . '">';
								echo $ds_getCustomersOneOrder["customersKundennummer"];
								echo '</a>';
								echo '</td>';

								echo '<td>';
								echo '<b>' . htmlentities($ds_getCustomersOneOrder["customersFirmenname"]) . '</b>';
								echo '<br />' . htmlentities($ds_getCustomersOneOrder["customersFirmennameZusatz"]);
								echo '</td>';

								echo '<td>';
								echo '<a href="' . PAGE_EDIT_CUSTOMER . '?searchBoxCustomer=' . $ds_getCustomersOneOrder["customersCompanyPLZ"] . '" title="im Kundenbestand nach PLZ suchen" target="_blank">';
								echo $ds_getCustomersOneOrder["customersCompanyPLZ"];
								echo '</a>';
								echo '</td>';

								echo '<td>';
								echo htmlentities($ds_getCustomersOneOrder["customersCompanyOrt"]);
								echo '</td>';

								echo '<td>';
								echo htmlentities($ds_getCustomersOneOrder["customersCompanyStrasse"] . ' ' . $ds_getCustomersOneOrder["customersCompanyHausnummer"]);
								echo '</td>';

								echo '<td>';
								if($ds_getCustomersOneOrder["customersTelefon1"] != ''){
									echo '<span class="dataList"> &bull; <b><img src="layout/icons/iconPhone2.png" width="14" height="14" alt="Tel." title="Tel." /></b>: ' . formatPhoneNumber($ds_getCustomersOneOrder["customersTelefon1"]) . '</span>';
								}
								if($ds_getCustomersOneOrder["customersTelefon2"] != ''){
									echo '<span class="dataList"> &bull; <b><img src="layout/icons/iconPhone2.png" width="14" height="14" alt="Tel." title="Tel." /></b>: ' . formatPhoneNumber($ds_getCustomersOneOrder["customersTelefon2"]) . '</span>';
								}
								if($ds_getCustomersOneOrder["customersMobil1"] != ''){
									echo '<span class="dataList"> &bull; <b><img src="layout/icons/iconMobile.png" width="14" height="14" alt="Mobil" title="Mobil" /></b>: ' . formatPhoneNumber($ds_getCustomersOneOrder["customersMobil1"]) . '</span>';
								}
								if($ds_getCustomersOneOrder["customersMobil2"] != ''){
									echo '<span class="dataList"> &bull; <b><img src="layout/icons/iconMobile.png" width="14" height="14" alt="Mobil" title="Mobil" /></b>: ' . formatPhoneNumber($ds_getCustomersOneOrder["customersMobil2"]) . '</span>';
								}
								if($ds_getCustomersOneOrder["customersMail1"] != ''){
									echo '<span class="dataList"> &bull; <b><img src="layout/icons/iconMail2.png" width="14" height="14" alt="Mail" title="Mail" /></b>: <a href="mailto:' . $ds_getCustomersOneOrder["customersMail1"] . '">' . ($ds_getCustomersOneOrder["customersMail1"]) . '</a></span>';
								}
								if($ds_getCustomersOneOrder["customersMail2"] != ''){
									echo '<span class="dataList"> &bull; <b><img src="layout/icons/iconMail2.png" width="14" height="14" alt="Mail" title="Mail" /></b>: <a href="mailto:' . $ds_getCustomersOneOrder["customersMail2"] . '">' . ($ds_getCustomersOneOrder["customersMail2"]) . '</a></span>';
								}
								echo '</td>';

								echo '<td style="font-size:11px;">';
								if($ds_getCustomersOneOrder["salesmenKundennummer"] != '48464'){
									echo '<b><a href="' . PAGE_EDIT_CUSTOMER . '?searchCustomerNumber=' . $ds_getCustomersOneOrder["salesmenKundennummer"] . '">' . $ds_getCustomersOneOrder["salesmenKundennummer"] . '</a></b><br />';
									echo '<b>' . htmlentities($ds_getCustomersOneOrder["salesmenFirmenname"]) . '</b><br />';
									if($ds_getCustomersOneOrder["salesmenFirmennameZusatz"] != ""){
										echo '<b>' . htmlentities($ds_getCustomersOneOrder["salesmenFirmennameZusatz"]) . '</b><br />';
									}
									echo htmlentities($ds_getCustomersOneOrder["salesmenCompanyStrasse"] . ' ' . $ds_getCustomersOneOrder["salesmenCompanyHausnummer"]) . '<br />';
									echo htmlentities($ds_getCustomersOneOrder["salesmenCompanyPLZ"] . ' ' . $ds_getCustomersOneOrder["salesmenCompanyOrt"]) . '<br />';
								
									/*
									if($ds_getCustomersOneOrder["salesmenTelefon1"] != ''){
										echo '<span class="dataList"> &bull; <b><img src="layout/icons/iconPhone2.png" width="14" height="14" alt="Tel." title="Tel." /></b>: ' . formatPhoneNumber($ds_getCustomersOneOrder["salesmenTelefon1"]) . '</span>';
									}
									if($ds_getCustomersOneOrder["salesmenTelefon2"] != ''){
										echo '<span class="dataList"> &bull; <b><img src="layout/icons/iconPhone2.png" width="14" height="14" alt="Tel." title="Tel." /></b>: ' . formatPhoneNumber($ds_getCustomersOneOrder["salesmenTelefon2"]) . '</span>';
									}
									if($ds_getCustomersOneOrder["salesmenMobil1"] != ''){
										echo '<span class="dataList"> &bull; <b><img src="layout/icons/iconMobile.png" width="14" height="14" alt="Mobil" title="Mobil" /></b>: ' . formatPhoneNumber($ds_getCustomersOneOrder["salesmenMobil1"]) . '</span>';
									}
									if($ds_getCustomersOneOrder["salesmenMobil2"] != ''){
										echo '<span class="dataList"> &bull; <b><img src="layout/icons/iconMobile.png" width="14" height="14" alt="Mobil" title="Mobil" /></b>: ' . formatPhoneNumber($ds_getCustomersOneOrder["salesmenMobil2"]) . '</span>';
									}
									if($ds_getCustomersOneOrder["salesmenMail1"] != ''){
										echo '<span class="dataList"> &bull; <b><img src="layout/icons/iconMail2.png" width="14" height="14" alt="Mail" title="Mail" /></b>: <a href="mailto:' . $ds_getCustomersOneOrder["salesmenMail1"] . '">' . ($ds_getCustomersOneOrder["salesmenMail1"]) . '</a></span>';
									}
									if($ds_getCustomersOneOrder["salesmenMail2"] != ''){
										echo '<span class="dataList"> &bull; <b><img src="layout/icons/iconMail2.png" width="14" height="14" alt="Mail" title="Mail" /></b>: <a href="mailto:' . $ds_getCustomersOneOrder["salesmenMail2"] . '">' . ($ds_getCustomersOneOrder["salesmenMail2"]) . '</a></span>';
									}
									*/
								}
								echo '</td>';

								echo '<td style="white-space:nowrap;">';
									if($ds_getCustomersOneOrder["customersCompanyCountry"] == '81'){
										$thisZipcodeDoubleDigit = substr($ds_getCustomersOneOrder["customersCompanyPLZ"], 0, 2);
										foreach($arrRelationSalesmenZipcode[$thisZipcodeDoubleDigit] as $thisRelationSalesmenZipcodeData){
											// $thisRelationSalesmenZipcodeData["kundenPLZaktiv"]
											echo '<span class="dataList"> &bull; ' . $thisRelationSalesmenZipcodeData["kundenname"] . ' [' . '<a href="' . PAGE_EDIT_CUSTOMER . '?searchBoxCustomer=' . $thisRelationSalesmenZipcodeData["kundennummer"] . '">' . $thisRelationSalesmenZipcodeData["kundennummer"] . '] ' . '</a></span>';
										}
									}
								echo '</td>';

								echo '<td style="white-space:nowrap;">';
								if($ds_getCustomersOneOrder["customersHomepage"] != "") {
									echo '<span class="toolItem"><a href="http://'.$ds_getCustomersOneOrder["customersHomepage"].'" target="_blank"><img src="layout/icons/iconWeb.gif" width="16" height="16" title="Homepage des Kunden aufrufen" alt="Homepage" /></a></span>';
								}
								else {
									echo '<span class="toolItem"><img src="layout/spacer.gif" width="16" height="16" alt="" /></span>';
								}

								// BOF SHOW IN GOOGLE MAPS
									if($ds_getCustomersOneOrder["customersCompanyLocation"] != "") {
										$thisGoogleMapsURL = PATH_GOOGLE_MAPS_URL_LATLON;
										$arrCustomerLocationData = explode(":", $ds_getCustomersOneOrder["customersCompanyLocation"]);
										$thisGoogleMapsURL = preg_replace("/{###LAT###}/", $arrCustomerLocationData[0], $thisGoogleMapsURL );
										$thisGoogleMapsURL = preg_replace("/{###LNG###}/", $arrCustomerLocationData[1], $thisGoogleMapsURL );
										echo '<span class="toolItem"><a href="' . $thisGoogleMapsURL . '" target="_blank"><img src="layout/menueIcons/menueSidebar/googleMaps.png" width="16" height="16" title="' . $thisGoogleMapsURL . '" alt="GoogleMaps" /></a></span>';
									}
									else {
										$thisGoogleMapsURL = PATH_GOOGLE_MAPS;
										$thisGoogleMapsURL = preg_replace("/{###STREET_NAME###}/", urlencode($ds_getCustomersOneOrder["customersCompanyStrasse"]), $thisGoogleMapsURL );
										$thisGoogleMapsURL = preg_replace("/{###STREET_NUMBER###}/", urlencode($ds_getCustomersOneOrder["customersCompanyHausnummer"]), $thisGoogleMapsURL );
										$thisGoogleMapsURL = preg_replace("/{###CITY_NAME###}/", urlencode($ds_getCustomersOneOrder["customersCompanyOrt"]), $thisGoogleMapsURL );
										$thisGoogleMapsURL = preg_replace("/{###CITY_ZIP_CODE###}/", urlencode($ds_getCustomersOneOrder["customersCompanyPLZ"]), $thisGoogleMapsURL );
										echo '<span class="toolItem"><a href="' . $thisGoogleMapsURL . '" target="_blank"><img src="layout/menueIcons/menueSidebar/googleMaps.png" width="16" height="16" title="' . $thisGoogleMapsURL . '" alt="GoogleMaps" /></a></span>';
									}
								// EOF SHOW IN GOOGLE MAPS

								// BOF SEARCH VIA GOOGLE
									$thisGoogleSearchUrl = 'https://www.google.de/search?q=';
									$thisGoogleSearchParams = '';
									$thisGoogleSearchParams .= '' . $ds_getCustomersOneOrder["customersFirmenname"];
									$thisGoogleSearchParams .= ' ' . $ds_getCustomersOneOrder["customersCompanyPLZ"];
									$thisGoogleSearchParams .= ' ' . $ds_getCustomersOneOrder["customersCompanyOrt"];
									$thisGoogleSearchParams .= ' ' . $ds_getCustomersOneOrder["customersCompanyStrasse"];
									$thisGoogleSearchParams .= ' ' . $ds_getCustomersOneOrder["customersCompanyHausnummer"];
									$thisGoogleSearchParams = urlencode($thisGoogleSearchParams);
									$thisGoogleSearchUrl .= $thisGoogleSearchParams;
									echo '<span class="toolItem"><a href="'.$thisGoogleSearchUrl.'" target="_blank"><img src="layout/icons/iconGoogle1.png" width="16" height="16" target="_blank" title="Kunden bei Google suchen" alt="Google-Suche" /></a></span>';
								// EOF SEARCH VIA GOOGLE

								echo '</td>';

								echo '</tr>';

								$countRow++;
							}

							echo '</tbody>';
							echo '</table>';

							if($pagesCount > 1) {
								include(FILE_MENUE_PAGES);
							}
						}
						else {
							$infoMessage .= 'Moment liegen keine Daten vor. ' . '</br>';
						}
					?>
					<?php displayMessages(); ?>
				</div>
				<?php } ?>

				<?php if($_REQUEST["tab"] == "tabs-3") { ?>
				<div id="tabs-3">
					<p class="infoArea">In der Berechnung werden geschriebene Auftragsbest&auml;tigungen zu Grunde gelegt!<br />Es besteht die M&ouml;glichkeit, dass Kunden doppelt angelegt sind und dieser Kunde &uuml;ber eine andere Kundennummer bestellt hat!</p>
					<div id="searchFilterArea">
						<form name="formFilterSearch" method="get" action="<?php echo $_SERVER["PHP_SELF"]; ?>">
							<table border="0" cellpadding="0" cellspacing="0" class="searchFilterContent">
								<tr>
									<td>
										<label for="searchPLZ">PLZ:</label>
										<input type="text" name="searchPLZ" id="searchPLZ" class="inputField_70" value="<?php echo $_REQUEST["searchPLZ"]; ?>" />
									</td>
									<td>
										<input type="hidden" name="tab" value="tabs-3" />
										<input type="submit" name="submitSearch" class="inputButton0" value="Suchen" />
									</td>
								</tr>
							</table>
						</form>
					</div>

					<?php
						$sqlWhere = "";

						if($_REQUEST["searchPLZ"] != ""){
							$sqlWhere = " AND `customers`.`customersCompanyPLZ` LIKE '" . $_REQUEST["searchPLZ"] . "%' ";
						}

						if(!empty($arrMandatories)){
							$arrSql_getCustomersNoOrders = array();

							foreach($arrMandatories as $thisMandatoryKey => $thisMandatoryValue){
								$thisDocumentTable = preg_replace("/^" . MANDATOR . "_/", $thisMandatoryKey . "_", TABLE_ORDER_CONFIRMATIONS);

								$arrSql_getCustomersNoOrders[] = "
										SELECT

												`" . $thisDocumentTable . "`.`orderDocumentsID`,
												`customers`.`customersKundennummer`,
												`customers`.`customersFirmenname`,
												`customers`.`customersFirmennameZusatz`,
												`customers`.`customersCompanyPLZ`,
												`customers`.`customersCompanyOrt`,
												`customers`.`customersCompanyCountry`,
												`customers`.`customersCompanyStrasse`,
												`customers`.`customersCompanyHausnummer`,

												`customers`.`customersTelefon1`,
												`customers`.`customersTelefon2`,
												`customers`.`customersMobil1`,
												`customers`.`customersMobil2`,
												`customers`.`customersFax1`,
												`customers`.`customersFax2`,
												`customers`.`customersMail1`,
												`customers`.`customersMail2`,
												`customers`.`customersHomepage`,
												`customers`.`customersGruppe`,

												`salesmen`.`customersID` AS `salesmenID`,
												`salesmen`.`customersKundennummer` AS `salesmenKundennummer`,
												`salesmen`.`customersFirmenname` AS `salesmenFirmenname`,
												`salesmen`.`customersFirmennameZusatz` AS `salesmenFirmennameZusatz`,
												`salesmen`.`customersCompanyPLZ` AS `salesmenCompanyPLZ`,
												`salesmen`.`customersCompanyOrt` AS `salesmenCompanyOrt`,
												`salesmen`.`customersCompanyStrasse` AS `salesmenCompanyStrasse`,
												`salesmen`.`customersCompanyHausnummer` AS `salesmenCompanyHausnummer`,
												`salesmen`.`customersTelefon1` AS `salesmenTelefon1`,
												`salesmen`.`customersTelefon2` AS `salesmenTelefon2`,
												`salesmen`.`customersMobil1` AS `salesmenMobil1`,
												`salesmen`.`customersMobil2` AS `salesmenMobil2`,
												`salesmen`.`customersFax1` AS `salesmenFax1`,
												`salesmen`.`customersFax2` AS `salesmenFax2`,
												`salesmen`.`customersMail1` AS `salesmenMail1`,
												`salesmen`.`customersMail2` AS `salesmenMail2`,

												`" . $thisDocumentTable . "`.`orderDocumentsCustomerNumber`

											FROM `" . TABLE_CUSTOMERS . "` AS `customers`

											LEFT JOIN `" . TABLE_CUSTOMERS . "` AS `salesmen`
											ON(`customers`.`customersVertreterID` = `salesmen`.`customersID`)

											LEFT JOIN `" . $thisDocumentTable . "`
											ON(`customers`.`customersKundennummer` = `" . $thisDocumentTable . "`.`orderDocumentsCustomerNumber`)

											WHERE 1
												AND `customers`.`customersTyp` IN('1', '5', '6')
												/* AND `customers`.`customersTyp` IN('1', '2', '3', '4', '5', '6') */
												AND `customers`.`customersActive` = '1'
												AND `customers`.`customersCompanyCountry` = '81'												
												" . $sqlWhere . "
									";
							}
						}

						if(!empty($arrSql_getCustomersNoOrders)){
							$sql_getCustomersNoOrders = "
									SELECT
											*,
											COUNT(`tempTable`.`orderDocumentsID`) AS `countOrders`
										FROM (
											" . implode(" UNION ", $arrSql_getCustomersNoOrders) . "
										) AS `tempTable`

										GROUP BY
											`tempTable`.`customersKundennummer`

										HAVING
											`countOrders` = 0

										ORDER BY
											`tempTable`.`customersKundennummer`
								";
						}

						// BOF GET COUNT ALL ROWS
							$sql_getAllRows = "
									SELECT
										COUNT(`tempTable2`.`customersKundennummer`) AS `countAllRows`

										FROM (
											" . $sql_getCustomersNoOrders . "
										) AS `tempTable2`

								";
							$rs_getAllRows = $dbConnection->db_query($sql_getAllRows);
							list(
								$countAllRows
							) = mysqli_fetch_array($rs_getAllRows);
						// EOF GET COUNT ALL ROWS

						if($_REQUEST["page"] == "" || $_REQUEST["page"] == 0) {
							$_REQUEST["page"] = 1;
						}
						if(MAX_ORDERS_PER_PAGE > 0) {
							$sql_getCustomersNoOrders .= " LIMIT " . (($_REQUEST["page"] - 1) * MAX_ORDERS_PER_PAGE) . ", " . MAX_ORDERS_PER_PAGE." ";
						}

						$rs_getCustomersNoOrders = $dbConnection->db_query($sql_getCustomersNoOrders);

						$countRows = $countAllRows;

						$pagesCount = ceil($countRows / MAX_ORDERS_PER_PAGE);

						if($countRows > 0){

							echo '<p class="infoArea">Gefundene Kunden ohne Bestellung: ' . $countRows . '</p>';

							if($pagesCount > 1) {
								include(FILE_MENUE_PAGES);
							}

							echo '<table cellspacing="0" cellpadding="0" width="100%" class="displayOrders">';
							echo '<thead>';
							echo '<tr>';

							echo '<th>#</th>';
							echo '<th>KNR</th>';
							echo '<th>Firma</th>';
							echo '<th>PLZ</th>';
							echo '<th>Ort</th>';
							echo '<th>Adresse</th>';
							echo '<th>Kontakt</th>';
							echo '<th>Vertreter / Kunde von</th>';
							echo '<th>Gebiet von</th>';
							echo '<th>Info</th>';

							echo '</tr>';
							echo '</thead>';
							echo '<tbody>';

							$countRow = 0;

							while($ds_getCustomersNoOrders = mysqli_fetch_assoc($rs_getCustomersNoOrders)){
								if($countRow%2 == 0){ $rowClass = 'row1'; }
								else { $rowClass = 'row0'; }

								$thisStyle = '';

								echo '<tr class="' . $rowClass . '" style="' . $thisStyle . '">';

								echo '<td style="text-align:right;">';
								echo '<b>'.($countRow + 1).'.</b> ';
								echo '</td>';

								echo '<td style="font-weight:bold;">';
								echo '<a href="' . PAGE_EDIT_CUSTOMER . '?searchCustomerNumber=' . $ds_getCustomersNoOrders["customersKundennummer"] . '">';
								echo $ds_getCustomersNoOrders["customersKundennummer"];
								echo '</a>';
								echo '</td>';

								echo '<td>';
								echo '<b>' . htmlentities($ds_getCustomersNoOrders["customersFirmenname"]) . '</b>';
								echo '<br />' . htmlentities($ds_getCustomersNoOrders["customersFirmennameZusatz"]);
								echo '</td>';

								echo '<td>';
								echo '<a href="' . PAGE_EDIT_CUSTOMER . '?searchBoxCustomer=' . $ds_getCustomersNoOrders["customersCompanyPLZ"] . '" title="im Kundenbestand nach PLZ suchen" target="_blank">';
								echo $ds_getCustomersNoOrders["customersCompanyPLZ"];
								echo '</td>';
								echo '</a>';

								echo '<td>';
								echo htmlentities($ds_getCustomersNoOrders["customersCompanyOrt"]);
								echo '</td>';

								echo '<td>';
								echo htmlentities($ds_getCustomersNoOrders["customersCompanyStrasse"] . ' ' . $ds_getCustomersNoOrders["customersCompanyHausnummer"]);
								echo '</td>';

								echo '<td>';
								if($ds_getCustomersNoOrders["customersTelefon1"] != ''){
									echo '<span class="dataList"> &bull; <b><img src="layout/icons/iconPhone2.png" width="14" height="14" alt="Tel." title="Tel." /></b>: ' . formatPhoneNumber($ds_getCustomersNoOrders["customersTelefon1"]) . '</span>';
								}
								if($ds_getCustomersNoOrders["customersTelefon2"] != ''){
									echo '<span class="dataList"> &bull; <b><img src="layout/icons/iconPhone2.png" width="14" height="14" alt="Tel." title="Tel." /></b>: ' . formatPhoneNumber($ds_getCustomersNoOrders["customersTelefon2"]) . '</span>';
								}
								if($ds_getCustomersNoOrders["customersMobil1"] != ''){
									echo '<span class="dataList"> &bull; <b><img src="layout/icons/iconMobile.png" width="14" height="14" alt="Mobil" title="Mobil" /></b>: ' . formatPhoneNumber($ds_getCustomersNoOrders["customersMobil1"]) . '</span>';
								}
								if($ds_getCustomersNoOrders["customersMobil2"] != ''){
									echo '<span class="dataList"> &bull; <b><img src="layout/icons/iconMobile.png" width="14" height="14" alt="Mobil" title="Mobil" /></b>: ' . formatPhoneNumber($ds_getCustomersNoOrders["customersMobil2"]) . '</span>';
								}
								if($ds_getCustomersNoOrders["customersMail1"] != ''){
									echo '<span class="dataList"> &bull; <b><img src="layout/icons/iconMail2.png" width="14" height="14" alt="Mail" title="Mail" /></b>: <a href="mailto:' . $ds_getCustomersNoOrders["customersMail1"] . '">' . ($ds_getCustomersNoOrders["customersMail1"]) . '</a></span>';
								}
								if($ds_getCustomersNoOrders["customersMail2"] != ''){
									echo '<span class="dataList"> &bull; <b><img src="layout/icons/iconMail2.png" width="14" height="14" alt="Mail" title="Mail" /></b>: <a href="mailto:' . $ds_getCustomersNoOrders["customersMail2"] . '">' . ($ds_getCustomersNoOrders["customersMail2"]) . '</a></span>';
								}
								echo '</td>';

								echo '<td style="font-size:11px;">';
								if($ds_getCustomersNoOrders["salesmenKundennummer"] != '48464'){
									echo '<b><a href="' . PAGE_EDIT_CUSTOMER . '?searchCustomerNumber=' . $ds_getCustomersNoOrders["salesmenKundennummer"] . '">' . $ds_getCustomersNoOrders["salesmenKundennummer"] . '</a></b><br />';
									echo '<b>' . htmlentities($ds_getCustomersNoOrders["salesmenFirmenname"]) . '</b><br />';
									if($ds_getCustomersNoOrders["salesmenFirmennameZusatz"] != ""){
										echo '<b>' . htmlentities($ds_getCustomersNoOrders["salesmenFirmennameZusatz"]) . '</b><br />';
									}
									echo htmlentities($ds_getCustomersNoOrders["salesmenCompanyStrasse"] . ' ' . $ds_getCustomersNoOrders["salesmenCompanyHausnummer"]) . '<br />';
									echo htmlentities($ds_getCustomersNoOrders["salesmenCompanyPLZ"] . ' ' . $ds_getCustomersNoOrders["salesmenCompanyOrt"]) . '<br />';
									
									/*
									if($ds_getCustomersNoOrders["salesmenTelefon1"] != ''){
										echo '<span class="dataList"> &bull; <b><img src="layout/icons/iconPhone2.png" width="14" height="14" alt="Tel." title="Tel." /></b>: ' . formatPhoneNumber($ds_getCustomersNoOrders["salesmenTelefon1"]) . '</span>';
									}
									if($ds_getCustomersNoOrders["salesmenTelefon2"] != ''){
										echo '<span class="dataList"> &bull; <b><img src="layout/icons/iconPhone2.png" width="14" height="14" alt="Tel." title="Tel." /></b>: ' . formatPhoneNumber($ds_getCustomersNoOrders["salesmenTelefon2"]) . '</span>';
									}
									if($ds_getCustomersNoOrders["salesmenMobil1"] != ''){
										echo '<span class="dataList"> &bull; <b><img src="layout/icons/iconMobile.png" width="14" height="14" alt="Mobil" title="Mobil" /></b>: ' . formatPhoneNumber($ds_getCustomersNoOrders["salesmenMobil1"]) . '</span>';
									}
									if($ds_getCustomersNoOrders["salesmenMobil2"] != ''){
										echo '<span class="dataList"> &bull; <b><img src="layout/icons/iconMobile.png" width="14" height="14" alt="Mobil" title="Mobil" /></b>: ' . formatPhoneNumber($ds_getCustomersNoOrders["salesmenMobil2"]) . '</span>';
									}
									if($ds_getCustomersNoOrders["salesmenMail1"] != ''){
										echo '<span class="dataList"> &bull; <b><img src="layout/icons/iconMail2.png" width="14" height="14" alt="Mail" title="Mail" /></b>: <a href="mailto:' . $ds_getCustomersNoOrders["salesmenMail1"] . '">' . ($ds_getCustomersNoOrders["salesmenMail1"]) . '</a></span>';
									}
									if($ds_getCustomersNoOrders["salesmenMail2"] != ''){
										echo '<span class="dataList"> &bull; <b><img src="layout/icons/iconMail2.png" width="14" height="14" alt="Mail" title="Mail" /></b>: <a href="mailto:' . $ds_getCustomersNoOrders["salesmenMail2"] . '">' . ($ds_getCustomersNoOrders["salesmenMail2"]) . '</a></span>';
									}
									*/
								}
								echo '</td>';

								echo '<td style="white-space:nowrap;">';
									if($ds_getCustomersNoOrders["customersCompanyCountry"] == '81'){
										$thisZipcodeDoubleDigit = substr($ds_getCustomersNoOrders["customersCompanyPLZ"], 0, 2);
										foreach($arrRelationSalesmenZipcode[$thisZipcodeDoubleDigit] as $thisRelationSalesmenZipcodeData){
											// $thisRelationSalesmenZipcodeData["kundenPLZaktiv"]
											echo '<span class="dataList"> &bull; ' . $thisRelationSalesmenZipcodeData["kundenname"] . ' [' . '<a href="' . PAGE_EDIT_CUSTOMER . '?searchBoxCustomer=' . $thisRelationSalesmenZipcodeData["kundennummer"] . '">' . $thisRelationSalesmenZipcodeData["kundennummer"] . '] ' . '</a></span>';
										}
									}
								echo '</td>';

								echo '<td style="white-space:nowrap;">';
								if($ds_getCustomersNoOrders["customersHomepage"] != "") {
									echo '<span class="toolItem"><a href="http://'.$ds_getCustomersNoOrders["customersHomepage"].'" target="_blank"><img src="layout/icons/iconWeb.gif" width="16" height="16" title="Homepage des Kunden aufrufen" alt="Homepage" /></a></span>';
								}
								else {
									echo '<span class="toolItem"><img src="layout/spacer.gif" width="16" height="16" alt="" /></span>';
								}

								// BOF SHOW IN GOOGLE MAPS
									if($ds_getCustomersNoOrders["customersCompanyLocation"] != "") {
										$thisGoogleMapsURL = PATH_GOOGLE_MAPS_URL_LATLON;
										$arrCustomerLocationData = explode(":", $ds_getCustomersNoOrders["customersCompanyLocation"]);
										$thisGoogleMapsURL = preg_replace("/{###LAT###}/", $arrCustomerLocationData[0], $thisGoogleMapsURL );
										$thisGoogleMapsURL = preg_replace("/{###LNG###}/", $arrCustomerLocationData[1], $thisGoogleMapsURL );
										echo '<span class="toolItem"><a href="' . $thisGoogleMapsURL . '" target="_blank"><img src="layout/menueIcons/menueSidebar/googleMaps.png" width="16" height="16" title="' . $thisGoogleMapsURL . '" alt="GoogleMaps" /></a></span>';
									}
									else {
										$thisGoogleMapsURL = PATH_GOOGLE_MAPS;
										$thisGoogleMapsURL = preg_replace("/{###STREET_NAME###}/", urlencode($ds_getCustomersNoOrders["customersCompanyStrasse"]), $thisGoogleMapsURL );
										$thisGoogleMapsURL = preg_replace("/{###STREET_NUMBER###}/", urlencode($ds_getCustomersNoOrders["customersCompanyHausnummer"]), $thisGoogleMapsURL );
										$thisGoogleMapsURL = preg_replace("/{###CITY_NAME###}/", urlencode($ds_getCustomersNoOrders["customersCompanyOrt"]), $thisGoogleMapsURL );
										$thisGoogleMapsURL = preg_replace("/{###CITY_ZIP_CODE###}/", urlencode($ds_getCustomersNoOrders["customersCompanyPLZ"]), $thisGoogleMapsURL );
										echo '<span class="toolItem"><a href="' . $thisGoogleMapsURL . '" target="_blank"><img src="layout/menueIcons/menueSidebar/googleMaps.png" width="16" height="16" title="' . $thisGoogleMapsURL . '" alt="GoogleMaps" /></a></span>';
									}
								// EOF SHOW IN GOOGLE MAPS

								// BOF SEARCH VIA GOOGLE
									$thisGoogleSearchUrl = 'https://www.google.de/search?q=';
									$thisGoogleSearchParams = '';
									$thisGoogleSearchParams .= '' . $ds_getCustomersNoOrders["customersFirmenname"];
									$thisGoogleSearchParams .= ' ' . $ds_getCustomersNoOrders["customersCompanyPLZ"];
									$thisGoogleSearchParams .= ' ' . $ds_getCustomersNoOrders["customersCompanyOrt"];
									$thisGoogleSearchParams .= ' ' . $ds_getCustomersNoOrders["customersCompanyStrasse"];
									$thisGoogleSearchParams .= ' ' . $ds_getCustomersNoOrders["customersCompanyHausnummer"];
									$thisGoogleSearchParams = urlencode($thisGoogleSearchParams);
									$thisGoogleSearchUrl .= $thisGoogleSearchParams;
									echo '<span class="toolItem"><a href="'.$thisGoogleSearchUrl.'" target="_blank"><img src="layout/icons/iconGoogle1.png" width="16" height="16" target="_blank" title="Kunden bei Google suchen" alt="Google-Suche" /></a></span>';
								// EOF SEARCH VIA GOOGLE

								echo '</td>';

								echo '</tr>';

								$countRow++;
							}

							echo '</tbody>';
							echo '</table>';

							if($pagesCount > 1) {
								include(FILE_MENUE_PAGES);
							}
						}
						else {
							$infoMessage .= 'Moment liegen keine Daten vor. ' . '</br>';
						}
					?>
					<?php displayMessages(); ?>
				</div>
				<?php } ?>

				<?php if($_REQUEST["tab"] == "tabs-4") { ?>
				<div id="tabs-4">

					<div id="searchFilterArea">
						<form name="formFilterSearch" method="get" action="<?php echo $_SERVER["PHP_SELF"]; ?>">
							<table border="0" cellpadding="0" cellspacing="0" class="searchFilterContent">
								<tr>
									<td>
										<label for="searchPLZ">PLZ:</label>
										<input type="text" name="searchPLZ" id="searchPLZ" class="inputField_70" value="<?php echo $_REQUEST["searchPLZ"]; ?>" />
									</td>
									<td>
										<input type="hidden" name="tab" value="tabs-4" />
										<input type="submit" name="submitSearch" class="inputButton0" value="Suchen" />
									</td>
								</tr>
							</table>
						</form>
					</div>

					<?php
						$dbConnection_ExternShop = new DB_Connection(DB_HOST_EXTERN_SHOP, '', DB_NAME_EXTERN_SHOP, DB_USER_EXTERN_SHOP, DB_PASSWORD_EXTERN_SHOP);
						$dbOpen_ExternShop = $dbConnection_ExternShop->db_connect();
						#dd('dbConnection_ExternShop');
						#dd('dbOpen_ExternShop');

						$sqlWhere = "";

						if($_REQUEST["searchPLZ"] != ""){
							$sqlWhere = " AND `" . TABLE_SHOP_ADDRESS_BOOK . "`.`entry_postcode` LIKE '" . $_REQUEST["searchPLZ"] . "%' ";
						}

						$sql_getExternalShopNoOrders = "
								SELECT

									`" . TABLE_SHOP_ADDRESS_BOOK . "`.`entry_company`,
									`" . TABLE_SHOP_CUSTOMERS . "`.`customers_manager`,
									`" . TABLE_SHOP_ADDRESS_BOOK . "`.`entry_firstname`,
									`" . TABLE_SHOP_ADDRESS_BOOK . "`.`entry_lastname`,
									REPLACE(`" . TABLE_SHOP_ADDRESS_BOOK . "`.`entry_street_address`, '.', ' . ') AS `entry_street_address`,
									`" . TABLE_SHOP_ADDRESS_BOOK . "`.`entry_postcode`,
									`" . TABLE_SHOP_ADDRESS_BOOK . "`.`entry_country_id`,
									REPLACE(`" . TABLE_SHOP_ADDRESS_BOOK . "`.`entry_city`, '.', ' . ') AS `entry_city`,
									`" . TABLE_SHOP_ADDRESS_BOOK . "`.`entry_suburb`,
									`" . TABLE_SHOP_ADDRESS_BOOK . "`.`address_date_added`,
									`" . TABLE_SHOP_CUSTOMERS . "`.`customers_firstname`,
									`" . TABLE_SHOP_CUSTOMERS . "`.`customers_lastname`,
									`" . TABLE_SHOP_CUSTOMERS . "`.`customers_email_address`,
									`" . TABLE_SHOP_CUSTOMERS . "`.`customers_telephone`,
									`" . TABLE_SHOP_CUSTOMERS . "`.`customers_fax`,
									`" . TABLE_SHOP_CUSTOMERS . "`.`customers_type`,
									`" . TABLE_SHOP_CUSTOMERS . "`.`customers_cid`,
									`" . TABLE_SHOP_CUSTOMERS . "`.`customers_id`

								FROM `" . TABLE_SHOP_CUSTOMERS . "`

								LEFT JOIN `" . TABLE_SHOP_ADDRESS_BOOK . "`
								ON(`" . TABLE_SHOP_CUSTOMERS . "`.`customers_default_address_id` = `" . TABLE_SHOP_ADDRESS_BOOK . "`.`address_book_id`)
								
								LEFT JOIN `" . TABLE_SHOP_ORDERS . "`
								ON(`" . TABLE_SHOP_CUSTOMERS . "`.`customers_id` = `" . TABLE_SHOP_ORDERS . "`.`customers_id`)

								WHERE 1
									AND (																					
										`" . TABLE_SHOP_CUSTOMERS . "`.`customers_cid` IS NULL										
										/*
										OR												
										`" . TABLE_SHOP_ORDERS . "`.`customers_id` IS NULL
										*/
									)
									AND `" . TABLE_SHOP_CUSTOMERS . "`.`customers_status` = '3'
									AND `" . TABLE_SHOP_ADDRESS_BOOK . "`.`entry_company` NOT LIKE '%Burhan%'

									" . $sqlWhere . "

								ORDER BY
									`" . TABLE_SHOP_ADDRESS_BOOK . "`.`address_date_added` ASC,
									`" . TABLE_SHOP_ADDRESS_BOOK . "`.`entry_postcode` ASC

							";

						// BOF GET COUNT ALL ROWS
							$sql_getAllRows = "
									SELECT
											COUNT(`" . TABLE_SHOP_CUSTOMERS . "`.`customers_id`) AS `countAllRows`

										FROM `" . TABLE_SHOP_CUSTOMERS . "`

										LEFT JOIN `" . TABLE_SHOP_ADDRESS_BOOK . "`
										ON(`" . TABLE_SHOP_CUSTOMERS . "`.`customers_default_address_id` = `" . TABLE_SHOP_ADDRESS_BOOK . "`.`address_book_id`)
										
										LEFT JOIN `" . TABLE_SHOP_ORDERS . "`
										ON(`" . TABLE_SHOP_CUSTOMERS . "`.`customers_id` = `" . TABLE_SHOP_ORDERS . "`.`customers_id`)

										WHERE 1
											AND (																					
												`" . TABLE_SHOP_CUSTOMERS . "`.`customers_cid` IS NULL										
												/*
												OR												
												`" . TABLE_SHOP_ORDERS . "`.`customers_id` IS NULL
												*/
											)
											AND `" . TABLE_SHOP_CUSTOMERS . "`.`customers_status` = '3'
											AND `" . TABLE_SHOP_ADDRESS_BOOK . "`.`entry_company` NOT LIKE '%Burhan%'

											" . $sqlWhere . "
								";
							
							$rs_getAllRows = $dbConnection_ExternShop->db_query($sql_getAllRows);
							list(
								$countAllRows
							) = mysqli_fetch_array($rs_getAllRows);
						// EOF GET COUNT ALL ROWS

						if($_REQUEST["page"] == "" || $_REQUEST["page"] == 0) {
							$_REQUEST["page"] = 1;
						}
						if(MAX_ORDERS_PER_PAGE > 0) {
							$sql_getExternalShopNoOrders .= " LIMIT " . (($_REQUEST["page"] - 1) * MAX_ORDERS_PER_PAGE) . ", " . MAX_ORDERS_PER_PAGE." ";
						}

						$rs_getExternalShopNoOrders = $dbConnection_ExternShop->db_query($sql_getExternalShopNoOrders);

						$countRows = $countAllRows;

						$pagesCount = ceil($countRows / MAX_ORDERS_PER_PAGE);

						if($countRows > 0){
							echo '
								<p class="infoArea">Gefundene Shop-Kunden ohne Bestellung: ' . $countRows . '<br />
								Es ist aber m&ouml;glich, dass Kunden telefonisch oder per Mail bestellt haben,
								aber aufgrund einer anderen verwendeten Mailadresse der Kundennummern-Abgleich zwischen Auftragslisten und Online-Shop nicht greift.
								</p>
							';

							if($pagesCount > 1) {
								include(FILE_MENUE_PAGES);
							}

							echo '<table cellspacing="0" cellpadding="0" width="100%" class="displayOrders">';
							echo '<thead>';
							echo '<tr>';

							echo '<th>#</th>';
							echo '<th>Registriert</th>';
							#echo '<th>KNR</th>';
							echo '<th>Firma</th>';
							echo '<th>PLZ</th>';
							echo '<th>Ort</th>';
							echo '<th>Land</th>';
							echo '<th>Adresse</th>';
							echo '<th>Kontakt</th>';
							echo '<th>Gebiet von</th>';
							echo '<th>Info</th>';

							echo '</tr>';
							echo '</thead>';
							echo '<tbody>';

							$countRow = 0;

							while($ds_getExternalShopNoOrders = mysqli_fetch_assoc($rs_getExternalShopNoOrders)){

								if($countRow%2 == 0){ $rowClass = 'row1'; }
								else { $rowClass = 'row0'; }

								$thisStyle = '';

								echo '<tr class="' . $rowClass . '" style="' . $thisStyle . '">';

								echo '<td style="text-align:right;">';
								echo '<b>'.($countRow + 1).'.</b> ';
								echo '</td>';


								echo '<td>';
								echo formatDate(substr($ds_getExternalShopNoOrders["address_date_added"], 0, 10), 'display');
								echo '</td>';

								#echo '<td style="font-weight:bold;">';
								#echo '<a href="' . PAGE_EDIT_CUSTOMER . '?searchCustomerNumber=' . $ds_getExternalShopNoOrders["customers_cid"] . '">';
								#echo $ds_getExternalShopNoOrders["customers_cid"];
								#echo '</a>';
								#echo '</td>';

								echo '<td>';
								echo '<b>' . htmlentities(ucwords($ds_getExternalShopNoOrders["entry_company"])) . '</b>';
								if($ds_getExternalShopNoOrders["customers_manager"] != '' && $ds_getExternalShopNoOrders["customers_manager"] != $ds_getExternalShopNoOrders["entry_company"]){
									echo '<br />' . htmlentities(ucwords($ds_getExternalShopNoOrders["customers_manager"]));
								}
								echo '</td>';

								echo '<td>';
								if(preg_match("/[0-9]{1,}[a-zA-Z]{1,}$/", $ds_getExternalShopNoOrders["entry_postcode"])){
									$ds_getExternalShopNoOrders["entry_postcode"] = preg_replace("/([0-9]{1,})([a-zA-Z]{1,})$/", "$1 $2", $ds_getExternalShopNoOrders["entry_postcode"]);
								}
								echo '<a href="' . PAGE_EDIT_CUSTOMER . '?searchBoxCustomer=' . $ds_getExternalShopNoOrders["entry_postcode"] . '" title="im Kundenbestand nach PLZ suchen" target="_blank">';
								echo $ds_getExternalShopNoOrders["entry_postcode"];
								echo '</a>';
								echo '</td>';

								echo '<td>';
								echo htmlentities(ucwords($ds_getExternalShopNoOrders["entry_city"]));
								if($ds_getExternalShopNoOrders["entry_suburb"] != '' && $ds_getExternalShopNoOrders["entry_suburb"] != $ds_getExternalShopNoOrders["entry_city"]){
									echo ' / ' . htmlentities(ucwords($ds_getExternalShopNoOrders["entry_suburb"]));
								}
								echo '</td>';

								echo '<td>';
								echo htmlentities($arrCountryTypeDatas[$ds_getExternalShopNoOrders["entry_country_id"]]["countries_name_DE"]);
								echo '</td>';

								echo '<td>';
								echo htmlentities(ucwords($ds_getExternalShopNoOrders["entry_street_address"]));
								echo '</td>';

								echo '<td>';
								if($ds_getExternalShopNoOrders["customers_telephone"] != ''){
									echo '<span class="dataList"> &bull; <b><img src="layout/icons/iconPhone2.png" width="14" height="14" alt="Tel." title="Tel." /></b>: ' . formatPhoneNumber($ds_getExternalShopNoOrders["customers_telephone"]) . '</span>';
								}
								if($ds_getExternalShopNoOrders["customers_fax"] != ''){
									echo '<span class="dataList"> &bull; <b><img src="layout/icons/iconFax.png" width="14" height="14" alt="Fax" title="Fax" /></b>: ' . formatPhoneNumber($ds_getExternalShopNoOrders["customers_fax"]) . '</span>';
								}
								if($ds_getExternalShopNoOrders["customers_email_address"] != ''){
									echo '<span class="dataList"> &bull; <b><img src="layout/icons/iconMail2.png" width="14" height="14" alt="Mail" title="Mail" /></b>: <a href="mailto:' . $ds_getExternalShopNoOrders["customers_email_address"] . '">' . $ds_getExternalShopNoOrders["customers_email_address"] . '</a></span>';
								}
								echo '</td>';

								echo '<td style="white-space:nowrap;">';
									if($ds_getExternalShopNoOrders["entry_country_id"] == '81'){
										$thisZipcodeDoubleDigit = substr($ds_getExternalShopNoOrders["entry_postcode"], 0, 2);
										foreach($arrRelationSalesmenZipcode[$thisZipcodeDoubleDigit] as $thisRelationSalesmenZipcodeData){
											// $thisRelationSalesmenZipcodeData["kundenPLZaktiv"]
											echo '<span class="dataList"> &bull; ' . $thisRelationSalesmenZipcodeData["kundenname"] . ' [' . '<a href="' . PAGE_EDIT_CUSTOMER . '?searchBoxCustomer=' . $thisRelationSalesmenZipcodeData["kundennummer"] . '">' . $thisRelationSalesmenZipcodeData["kundennummer"] . '] ' . '</a></span>';
										}
									}
								echo '</td>';

								echo '<td style="white-space:nowrap;">';

								// BOF SHOW IN GOOGLE MAPS
									$thisGoogleMapsUrl = 'https://maps.google.de/maps?q=';
									$thisGoogleMapsParams = '';
									$thisGoogleMapsParams .= '' . $ds_getExternalShopNoOrders["entry_postcode"];
									$thisGoogleMapsParams .= ' ' . $ds_getExternalShopNoOrders["entry_city"];
									$thisGoogleMapsParams .= ' ' . $ds_getExternalShopNoOrders["entry_street_address"];
									$thisGoogleMapsParams = urlencode($thisGoogleMapsParams);
									$thisGoogleMapsUrl .= $thisGoogleMapsParams;
									echo '<span class="toolItem"><a href="' . $thisGoogleMapsUrl . '" target="_blank"><img src="layout/menueIcons/menueSidebar/googleMaps.png" width="16" height="16" title="' . $thisGoogleMapsURL . '" alt="GoogleMaps" /></a></span>';
								// EOF SHOW IN GOOGLE MAPS

								// BOF SEARCH VIA GOOGLE
									$thisGoogleSearchUrl = 'https://www.google.de/search?q=';
									$thisGoogleSearchParams = '';
									$thisGoogleSearchParams .= '' . $ds_getExternalShopNoOrders["entry_company"];
									$thisGoogleSearchParams .= ' ' . $ds_getExternalShopNoOrders["entry_postcode"];
									$thisGoogleSearchParams .= ' ' . $ds_getExternalShopNoOrders["entry_city"];
									$thisGoogleSearchParams .= ' ' . $ds_getExternalShopNoOrders["entry_street_address"];
									$thisGoogleSearchParams = urlencode($thisGoogleSearchParams);
									$thisGoogleSearchUrl .= $thisGoogleSearchParams;
									echo '<span class="toolItem"><a href="'.$thisGoogleSearchUrl.'" target="_blank"><img src="layout/icons/iconGoogle1.png" width="16" height="16" target="_blank" title="Kunden bei Google suchen" alt="Google-Suche" /></a></span>';
								// EOF SEARCH VIA GOOGLE

								echo '</td>';

								echo '</tr>';

								$countRow++;
							}
							echo '</tbody>';
							echo '</table>';

							if($pagesCount > 1) {
								include(FILE_MENUE_PAGES);
							}
						}
						else {
							$infoMessage .= 'Moment liegen keine Daten vor. ' . '</br>';
						}

					?>
					<?php displayMessages(); ?>
				</div>
				<?php } ?>

			</div>
		</div>
	</div>
</div>

<script language="javascript" type="text/javascript">
	$(document).ready(function() {
		$('.buttonShopOrderInfo').css('cursor', 'pointer');
		$('.buttonShopOrderInfo').click(function () {
			loadOrderDetails($(this), '<?php echo BASEPATH; ?>', 'Details der Bestellung');
		});
		colorRowMouseOver('.displayOrders tbody tr');
		// toggleAreas();

		$(function() {
			$.datepicker.setDefaults($.datepicker.regional[""]);
			$('#searchDateStart').datepicker($.datepicker.regional["de"]);
			//$('#searchDateStart').datepicker("option", "maxDate", "0" );

			$('#searchDateEnd').datepicker($.datepicker.regional["de"]);
			//$('#searchDateEnd').datepicker("option", "maxDate", "0" );
		});

		$('.buttonToggleSidebarMenue').click(function() {
			$('#menueSidebarToggleContent').toggle();
		});
	});
</script>

<?php require_once('inc/footerHTML.inc.php'); ?>