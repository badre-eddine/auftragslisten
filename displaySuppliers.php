<?php
	require_once('inc/requires.inc.php');

	if(!$arrGetUserRights["editSuppliers"] && !$arrGetUserRights["displaySuppliers"]) {
		header('location: ' . PAGE_EXIT_LOCATION);
		exit;
	}

	if($_GET["downloadFile"] != "" && $_GET["documentType"] != "") {
		require_once('classes/downloadFile.class.php');
		$fileDirectory = DIRECTORY_CREATED_DOCUMENTS_CUSTOMERS;
		$thisDownload = new downloadFile($fileDirectory, $_GET["downloadFile"]);
		$errorMessage .= $thisDownload->startDownload();
	}

	// BOF DEFINE DUTY FIELDS
		$arrFormDutyFields = array(
			"editSuppliersFirmenname" => "Firmenname",
			"editSuppliersTyp" => "Kunden-Typ",
			// "editSuppliersGruppe[]" => "Kunden-Gruppe"
			// "editSuppliersGruppe" => "Kunden-Gruppe"
		);
		// CREATE JSON DUTY FIELD
		$jsonFormDutyFields = createJson($arrFormDutyFields);
	// BOF DEFINE DUTY FIELDS

	if($_REQUEST["editID"] == "")  {
		// $_REQUEST["editID"] = "NEW";
		// header("location: displayOrders.php");
		// exit;
	}

	$jswindowMessage = '';
	$warningMessage = '';
	$errorMessage = '';
	$successMessage = '';
	$infoMessage = '';

	$dbConnection = new DB_Connection();
	$db_open = $dbConnection->db_connect();

	// BOF READ VERTRETER
		$arrAgentDatas = getAgents();
		$arrAgentDatas2 = getAgents2();
	// EOF READ VERTRETER

	// BOF READ SALUTATIONS
		$arrSalutationTypeDatas = getSalutationTypes();
	// EOF READ SALUTATIONS

	// BOF READ PAYMENT TYPES
		$arrPaymentTypeDatas = getPaymentTypes();
	// EOF READ PAYMENT TYPES

	// BOF READ PAYMENT CONDITIONS
		$arrPaymentConditionDatas = getPaymentConditions();
	// EOF READ PAYMENT CONDITIONS

	// BOF READ BESTELL ARTEN
		$arrOrderTypeDatas = getOrderTypes();
	// EOF READ BESTELL ARTEN

	// BOF READ ADDITIONAL COSTS
		$arrAdditionalCostsDatas = getAdditionalCosts();
	// EOF READ ADDITIONAL COSTS

	// BOF READ COUNTRIES
		$arrCountryTypeDatas = getCountryTypes();
	// EOF READ COUNTRIES

	// BOF READ CUSTOMER TYPES
		$arrSupplierTypeDatas = getSupplierTypes();
	// EOF READ CUSTOMER TYPES

	// BOF READ DOCUMENT TYPE DATAS
		$arrDocumentTypeDatas = getDocumentTypes();
	// EOF READ DOCUMENT TYPE DATAS

	// BOF READ CUSTOMER GROUP DATAS
		$arrSupplierGroupDatas = getSupplierGroups();
	// EOF READ CUSTOMER GROUP DATAS

	// BOF STORE DATAS
	if($_POST["storeDatas"] != "") {

		$doAction = checkFormDutyFields($arrFormDutyFields);

		// BOF CHECK NAME FIELDS
		$arrCheckSum = array();
		if($_POST["editSuppliersFirmenInhaberVorname"] != "" || $_POST["editSuppliersFirmenInhaberNachname"] != "" || $_POST["editSuppliersFirmenInhaberAnrede"] != "") {
			$arrCheckSum["FirmenInhaber"][] = preg_replace("/^[ A-Za-zäüöß\.\-]{1,}$/", 1, $_POST["editSuppliersFirmenInhaberVorname"]);
			$arrCheckSum["FirmenInhaber"][] = preg_replace("/^[ A-Za-zäüöß\.\-]{1,}$/", 1, $_POST["editSuppliersFirmenInhaberNachname"]);
			$arrCheckSum["FirmenInhaber"][] = preg_replace("/[0-9]{1,}/", 1, $_POST["editSuppliersFirmenInhaberAnrede"]);
		}

		if($_POST["editSuppliersAnsprechpartner1Vorname"] != "" || $_POST["editSuppliersAnsprechpartner1Nachname"] != "" || $_POST["editSuppliersAnsprechpartner1Anrede"] != "") {
			$arrCheckSum["Ansprechpartner1"][] = preg_replace("/^[ A-Za-zäüöß\.\-]{1,}$/", 1, $_POST["editSuppliersAnsprechpartner1Vorname"]);
			$arrCheckSum["Ansprechpartner1"][] = preg_replace("/^[ A-Za-zäüöß\.\-]{1,}$/", 1, $_POST["editSuppliersAnsprechpartner1Nachname"]);
			$arrCheckSum["Ansprechpartner1"][] = preg_replace("/[0-9]{1,}/", 1, $_POST["editSuppliersAnsprechpartner1Anrede"]);
		}

		if($_POST["editSuppliersAnsprechpartner2Vorname"] != "" || $_POST["editSuppliersAnsprechpartner2Nachname"] != "" || $_POST["editSuppliersAnsprechpartner2Anrede"] != "") {
			$arrCheckSum["Ansprechpartner2"][] = preg_replace("/^[ A-Za-zäüöß\.\-]{1,}$/", 1, $_POST["editSuppliersAnsprechpartner2Vorname"]);
			$arrCheckSum["Ansprechpartner2"][] = preg_replace("/^[ A-Za-zäüöß\.\-]{1,}$/", 1, $_POST["editSuppliersAnsprechpartner2Nachname"]);
			$arrCheckSum["Ansprechpartner2"][] = preg_replace("/[0-9]{1,}/", 1, $_POST["editSuppliersAnsprechpartner2Anrede"]);
		}

		$isCheckSumOk = true;
		if(!empty($arrCheckSum)) {
			foreach($arrCheckSum as $thisKey => $thisValue) {
				if(array_sum($thisValue)/3 != 1) {
					$isCheckSumOk = false;
				}
				// echo 'check_sum: ' . array_sum($thisValue)%3 . '<br />';
			}
		}
		// EOF CHECK NAME FIELDS

		// BOF CHECK MAIL ADDRESS
		$checkCustomerEmail = true;
		if(trim($_POST["editSuppliersMail1"] != '')) {
			$checkCustomerEmail = checkEmailStructure($_POST["editSuppliersMail1"]);
		}
		if(trim($_POST["editSuppliersMail2"] != '')) {
			$checkCustomerEmail = checkEmailStructure($_POST["editSuppliersMail2"]);
		}
		// EOF CHECK MAIL ADDRESS

		if($doAction && $isCheckSumOk && $checkCustomerEmail) {

			$thisEditSuppliersKundennummer = $_POST["editSuppliersKundennummer"];
			if($_POST["editSuppliersID"] == "") {
				$_POST["editSuppliersID"] = "%";
				// BOF CREATE COSTUMER NUMBER
				if(strlen($_POST["editSuppliersKundennummer"]) < LENGTH_CUSTOMER_NUMBER) {
					/*
					$thisEditSuppliersKundennummerPrefix = PREFIX_SUPPLIER_NUMBER . substr($_POST["editSuppliersCompanyPLZ"], 0, 2);
					$sql = "SELECT
								IF(
									(MAX(REPLACE(`suppliersKundennummer`, '" . $thisEditSuppliersKundennummerPrefix . "', '')) + 1) IS NULL, 1,
									(MAX(REPLACE(`suppliersKundennummer`, '" . $thisEditSuppliersKundennummerPrefix . "', '')) + 1)
								) AS `searchResult`

						FROM `".TABLE_SUPPLIERS . "`

						WHERE `suppliersKundennummer` LIKE '" . $thisEditSuppliersKundennummerPrefix . "%'
					";
					$rs = $dbConnection->db_query($sql);

					list($searchResult) = mysqli_fetch_array($rs);

					$generatedSupplierNumber = $thisEditSuppliersKundennummerPrefix;

					if(strlen($searchResult) < LENGTH_CUSTOMER_NUMBER) {
						$generatedSupplierNumber .= str_repeat('0', (LENGTH_CUSTOMER_NUMBER - (strlen($searchResult) + strlen(preg_replace("/[a-zA-Z_\-]/", "", $thisEditSuppliersKundennummerPrefix))))).$searchResult;
					}
					else {
						$generatedSupplierNumber .= $searchResult;
					}
					*/
					##
					$generatedSupplierNumber = generateCustomerNumber('suppliers', '', '', $_POST["editSuppliersCompanyPLZ"]);
					##

					$jswindowMessage .= 'Die generierte Kundennummer für ' . $_POST["editSuppliersFirmenname"] . ' lautet: ' . $generatedSupplierNumber;

					$thisEditSuppliersKundennummer = $generatedSupplierNumber;
				}
				// EOF CREATE COSTUMER NUMBER
				$sqlInsertType = "INSERT";
			}
			else {
				$sqlInsertType = "REPLACE";
			}

			$sql = "
				" . $sqlInsertType . " INTO `" . TABLE_SUPPLIERS . "`(
					`suppliersID`,
					`suppliersKundennummer`,
					`suppliersFirmenname`,
					`suppliersFirmennameZusatz`,
					`suppliersFirmaArtikelnummerKurzform`,
					`suppliersOurCustomerNumber`,
					`suppliersFirmenInhaberVorname`,
					`suppliersFirmenInhaberNachname`,
					`suppliersFirmenInhaberAnrede`,
					`suppliersFirmenInhaber2Vorname`,
					`suppliersFirmenInhaber2Nachname`,
					`suppliersFirmenInhaber2Anrede`,
					`suppliersAnsprechpartner1Vorname`,
					`suppliersAnsprechpartner1Nachname`,
					`suppliersAnsprechpartner1Anrede`,
					`suppliersAnsprechpartner2Vorname`,
					`suppliersAnsprechpartner2Nachname`,
					`suppliersAnsprechpartner2Anrede`,
					`suppliersTelefon1`,
					`suppliersTelefon2`,
					`suppliersMobil1`,
					`suppliersMobil2`,
					`suppliersFax1`,
					`suppliersFax2`,
					`suppliersMail1`,
					`suppliersMail2`,
					`suppliersHomepage`,
					`suppliersCompanyStrasse`,
					`suppliersCompanyHausnummer`,
					`suppliersCompanyCountry`,
					`suppliersCompanyPLZ`,
					`suppliersCompanyOrt`,
					`suppliersLieferadresseStrasse`,
					`suppliersLieferadresseHausnummer`,
					`suppliersLieferadressePLZ`,
					`suppliersLieferadresseOrt`,
					`suppliersLieferadresseLand`,
					`suppliersRechnungsadresseStrasse`,
					`suppliersRechnungsadresseHausnummer`,
					`suppliersRechnungsadressePLZ`,
					`suppliersRechnungsadresseOrt`,
					`suppliersRechnungsadresseLand`,
					`suppliersKontoinhaber`,
					`suppliersBankName`,
					`suppliersBankKontonummer`,
					`suppliersBankLeitzahl`,
					`suppliersBankIBAN`,
					`suppliersBankBIC`,
					`suppliersBezahlart`,
					`suppliersZahlungskondition`,
					`suppliersRabatt`,
					`suppliersUseProductMwst`,
					`suppliersUseProductDiscount`,
					`suppliersUstID`,
					`suppliersVertreterID`,
					`suppliersUseSalesmanDeliveryAdress`,
					`suppliersUseSalesmanInvoiceAdress`,
					`suppliersTyp`,
					`suppliersGruppe`,
					`suppliersNotiz`,
					`suppliersActive`,
					`suppliersUserID`,
					`suppliersTimeCreated`,
					`suppliersShopLoginUser`,
					`suppliersShopLoginPassword`
				)
				VALUES (
					'".$_POST["editSuppliersID"]."',
					'".$thisEditSuppliersKundennummer."',
					'".($_POST["editSuppliersFirmenname"])."',
					'".($_POST["editSuppliersFirmennameZusatz"])."',
					'".($_POST["editSuppliersFirmaArtikelnummerKurzform"])."',
					'".($_POST["editSuppliersOurCustomerNumber"])."',
					'".($_POST["editSuppliersFirmenInhaberVorname"])."',
					'".($_POST["editSuppliersFirmenInhaberNachname"])."',
					'".$_POST["editSuppliersFirmenInhaberAnrede"]."',
					'".($_POST["editSuppliersFirmenInhaber2Vorname"])."',
					'".($_POST["editSuppliersFirmenInhaber2Nachname"])."',
					'".$_POST["editSuppliersFirmenInhaber2Anrede"]."',
					'".($_POST["editSuppliersAnsprechpartner1Vorname"])."',
					'".($_POST["editSuppliersAnsprechpartner1Nachname"])."',
					'".$_POST["editSuppliersAnsprechpartner1Anrede"]."',
					'".($_POST["editSuppliersAnsprechpartner2Vorname"])."',
					'".($_POST["editSuppliersAnsprechpartner2Nachname"])."',
					'".$_POST["editSuppliersAnsprechpartner2Anrede"]."',
					'".cleanPhoneNumbers($_POST["editSuppliersTelefon1"], $_POST["editSuppliersCompanyCountry"])."',
					'".cleanPhoneNumbers($_POST["editSuppliersTelefon2"], $_POST["editSuppliersCompanyCountry"])."',
					'".cleanPhoneNumbers($_POST["editSuppliersMobil1"], $_POST["editSuppliersCompanyCountry"])."',
					'".cleanPhoneNumbers($_POST["editSuppliersMobil2"], $_POST["editSuppliersCompanyCountry"])."',
					'".cleanPhoneNumbers($_POST["editSuppliersFax1"], $_POST["editSuppliersCompanyCountry"])."',
					'".cleanPhoneNumbers($_POST["editSuppliersFax2"], $_POST["editSuppliersCompanyCountry"])."',
					'".($_POST["editSuppliersMail1"])."',
					'".($_POST["editSuppliersMail2"])."',
					'".preg_replace("/http:\/\//", "", $_POST["editSuppliersHomepage"])."',
					'".($_POST["editSuppliersCompanyStrasse"])."',
					'".($_POST["editSuppliersCompanyHausnummer"])."',
					'".($_POST["editSuppliersCompanyCountry"])."',
					'".($_POST["editSuppliersCompanyPLZ"])."',
					'".($_POST["editSuppliersCompanyOrt"])."',
					'".($_POST["editSuppliersLieferadresseStrasse"])."',
					'".($_POST["editSuppliersLieferadresseHausnummer"])."',
					'".($_POST["editSuppliersLieferadressePLZ"])."',
					'".($_POST["editSuppliersLieferadresseOrt"])."',
					'".$_POST["editSuppliersLieferadresseLand"]."',
					'".($_POST["editSuppliersRechnungsadresseStrasse"])."',
					'".($_POST["editSuppliersRechnungsadresseHausnummer"])."',
					'".($_POST["editSuppliersRechnungsadressePLZ"])."',
					'".($_POST["editSuppliersRechnungsadresseOrt"])."',
					'".$_POST["editSuppliersRechnungsadresseLand"]."',
					'".$_POST["editSuppliersKontoinhaber"]."',
					'".($_POST["editSuppliersBankName"])."',
					'".cleanChars($_POST["editSuppliersBankKontonummer"])."',
					'".cleanChars($_POST["editSuppliersBankLeitzahl"])."',
					'".cleanChars($_POST["editSuppliersBankIBAN"])."',
					'".cleanChars($_POST["editSuppliersBankBIC"])."',
					'".$_POST["editSuppliersBezahlart"]."',
					'".$_POST["editSuppliersZahlungskondition"]."',
					'".str_replace(",", ".", $_POST["editSuppliersRabatt"])."',
					'".$_POST["editSuppliersUseProductMwst"]."',
					'".$_POST["editSuppliersUseProductDiscount"]."',
					'".cleanChars($_POST["editSuppliersUstID"])."',
					'".$_POST["editSuppliersVertreterID"]."',
					'".$_POST["editSuppliersUseSalesmanDeliveryAdress"]."',
					'".$_POST["editSuppliersUseSalesmanInvoiceAdress"]."',
					'".$_POST["editSuppliersTyp"]."',
					'".implode(';', $_POST["editSuppliersGruppe"])."',
					'".($_POST["editSuppliersNotiz"])."',
					'".$_POST["editSuppliersActive"]."',
					'".$_SESSION["usersID"]."',
					NOW(),
					'".$_POST["editSuppliersShopLoginUser"]."',
					'".$_POST["editSuppliersShopLoginPassword"]."'
				)
			";

			$rs = $dbConnection->db_query($sql);
			$getInsertID = $dbConnection->db_getInsertID();

			if($rs) {
				$successMessage .= ' Der Datensatz wurde gespeichert. ' .'<br />';
			}
			else {
				$errorMessage .= ' Der Datensatz konnte nicht gespeichert werden. ' .'<br />';
			}
			// $_REQUEST["editID"] = $thisEditSuppliersKundennummer;
			if($sqlInsertType == "INSERT") { $_REQUEST["editID"] = $getInsertID; }
			else { $_REQUEST["editID"] = $_POST["editSuppliersID"]; }
		}
		else {
			$_REQUEST["editID"] = $_POST["editSuppliersID"];
			if($_POST["editSuppliersID"] == "") {
				$_REQUEST["editID"] = 'NEW';
			}
			if(!$doAction) {
				$errorMessage .= ' Der Datensatz konnte nicht gespeichert werden. Bitte f&uuml;llen Sie alle Pflichtfelder aus. ' .'<br />';
				$jswindowMessage .= ' Der Datensatz konnte nicht gespeichert werden. Bitte f&uuml;llen Sie alle Pflichtfelder aus. ' .'<br />';
			}
			if(!$isCheckSumOk) {
				$errorMessage .= ' Der Datensatz konnte nicht gespeichert werden. Bitte geben Sie die Namen korrekt ein. ' .'<br />';
				$jswindowMessage .= ' Der Datensatz konnte nicht gespeichert werden. Bitte geben Sie die Namen korrekt ein. ' .'<br />';
			}
			if(!$checkCustomerEmail) {
				$errorMessage .= ' Die eingegebene Mail-Adresse ist nicht korrekt.' . '<br />';
			}
		}
	}
	// EOF STORE DATAS

	// BOF DELETE DATAS
		if($_POST["deleteDatas"] != "") {
			$sql = "DELETE FROM `" . TABLE_SUPPLIERS . "` WHERE `ordersID` = '".$_POST["editSuppliersID"]."'";
			$rs = $dbConnection->db_query($sql);

			if($rs) {
				$successMessage .= ' Der Datensatz wurde endgültig gelöscht. '.'<br />';
			}
			else {
				$errorMessage .= ' Der Datensatz konnte nicht gelöscht werden. '.'<br />';
			}
		}
	// EOF DELETE DATAS

	// BOF READ ALL DATAS

		$where = "";


		if($_REQUEST["searchSupplierNumber"] != "") {
			$where = " AND `suppliersKundennummer` = '" . $_REQUEST["searchSupplierNumber"] . "' ";
			// $where = " AND `suppliersKundennummer` LIKE '" . $_REQUEST["searchSupplierNumber"] . "%' ";
			$_REQUEST["editID"] = "";
		}
		else if($_REQUEST["searchSupplierCategorie"] != "") {
			$where = " AND `suppliersTyp` = '" . $_REQUEST["searchSupplierCategorie"] . "' ";
			$_REQUEST["editID"] = "";
		}
		else if($_REQUEST["searchSupplierName"] != "") {
			$where = " AND `suppliersFirmenname` LIKE '%" . $_REQUEST["searchSupplierName"] . "%' ";
			$_REQUEST["editID"] = "";
		}
		else if($_REQUEST["searchSupplierNumber"] != "") {
			// $where = " AND `suppliersKundennummer` = '" . $_REQUEST["searchSupplierNumber"] . "' ";
			$where = " AND `suppliersKundennummer` LIKE '" . $_REQUEST["searchSupplierNumber"] . "%' ";
			$_REQUEST["editID"] = "";
		}
		else if($_REQUEST["editCustomerNumber"] != "") {
			$where = " AND `suppliersKundennummer` = '" . $_REQUEST["editCustomerNumber"] . "' ";
			// $where = " AND `suppliersKundennummer` LIKE '" . $_REQUEST["editCustomerNumber"] . "%' ";
			$_REQUEST["editID"] = "";
		}
		else if($_REQUEST["searchPLZ"] != "") {
			$where = " AND (
						`suppliersLieferadressePLZ` LIKE '" . $_REQUEST["searchPLZ"] . "%'
						OR
						`suppliersRechnungsadressePLZ` LIKE '" . $_REQUEST["searchPLZ"] . "%'
						OR
						`suppliersCompanyPLZ` LIKE '" . $_REQUEST["searchPLZ"] . "%'
				)
			";
			$_REQUEST["editID"] = "";
		}

		else if($_REQUEST["searchWord"] != "") {
			$where = " AND (
							`suppliersLieferadressePLZ` LIKE '" . $_REQUEST["searchWord"] . "%'
							OR
							`suppliersRechnungsadressePLZ` LIKE '" . $_REQUEST["searchWord"] . "%'
							OR
							`suppliersCompanyPLZ` LIKE '" . $_REQUEST["searchWord"] . "%'
							OR
							`suppliersLieferadresseOrt` LIKE '%" . $_REQUEST["searchWord"] . "%'
							OR
							`suppliersRechnungsadresseOrt` LIKE '%" . $_REQUEST["searchWord"] . "%'
							OR
							`suppliersCompanyOrt` LIKE '%" . $_REQUEST["searchWord"] . "%'
							OR
							`suppliersKundennummer` LIKE '" . $_REQUEST["searchWord"] . "%'
							OR
							`suppliersFirmenname` LIKE '%" . $_REQUEST["searchWord"] . "%'

						) ";
			$_REQUEST["editID"] = "";
		}
		else if ($_GET["searchBoxSupplier"] != "") {
			$where = " AND (
							`suppliersKundennummer` LIKE '" . $_GET["searchBoxSupplier"] . "%'
							OR
							`suppliersFirmenname` LIKE '%" . $_GET["searchBoxSupplier"] . "%'

						) ";

			$_REQUEST["editID"] = "";
		}

		if($_REQUEST["editID"] == "") {

			$sql = "SELECT
						`suppliersID`,
						`suppliersKundennummer`,
						`suppliersFirmenname`,
						`suppliersFirmennameZusatz`,
						`suppliersFirmaArtikelnummerKurzform`,
						`suppliersOurCustomerNumber`,
						`suppliersFirmenInhaberVorname`,
						`suppliersFirmenInhaberNachname`,
						`suppliersFirmenInhaberAnrede`,
						`suppliersFirmenInhaber2Vorname`,
						`suppliersFirmenInhaber2Nachname`,
						`suppliersFirmenInhaber2Anrede`,
						`suppliersAnsprechpartner1Vorname`,
						`suppliersAnsprechpartner1Nachname`,
						`suppliersAnsprechpartner1Anrede`,
						`suppliersAnsprechpartner2Vorname`,
						`suppliersAnsprechpartner2Nachname`,
						`suppliersAnsprechpartner2Anrede`,
						`suppliersTelefon1`,
						`suppliersTelefon2`,
						`suppliersMobil1`,
						`suppliersMobil2`,
						`suppliersFax1`,
						`suppliersFax2`,
						`suppliersMail1`,
						`suppliersMail2`,
						`suppliersHomepage`,
						`suppliersCompanyStrasse`,
						`suppliersCompanyHausnummer`,
						`suppliersCompanyCountry`,
						`suppliersCompanyPLZ`,
						`suppliersCompanyOrt`,
						`suppliersLieferadresseStrasse`,
						`suppliersLieferadresseHausnummer`,
						`suppliersLieferadressePLZ`,
						`suppliersLieferadresseOrt`,
						`suppliersLieferadresseLand`,
						`suppliersRechnungsadresseStrasse`,
						`suppliersRechnungsadresseHausnummer`,
						`suppliersRechnungsadressePLZ`,
						`suppliersRechnungsadresseOrt`,
						`suppliersRechnungsadresseLand`,
						`suppliersKontoinhaber`,
						`suppliersBankName`,
						`suppliersBankKontonummer`,
						`suppliersBankLeitzahl`,
						`suppliersBankIBAN`,
						`suppliersBankBIC`,
						`suppliersBezahlart`,
						`suppliersZahlungskondition`,
						`suppliersRabatt`,
						`suppliersUseProductMwst`,
						`suppliersUseProductDiscount`,
						`suppliersUstID`,
						`suppliersVertreterID`,
						`suppliersUseSalesmanDeliveryAdress`,
						`suppliersUseSalesmanInvoiceAdress`,
						`suppliersTyp`,
						`suppliersGruppe`,
						`suppliersNotiz`,
						`suppliersActive`,
						`suppliersShopLoginUser`,
						`suppliersShopLoginPassword`

						FROM `" . TABLE_SUPPLIERS . "`

						WHERE 1
					";
			$sql .= $where;

			$sql .= "
						ORDER BY `suppliersTyp`, `suppliersFirmenname`
			";

			$rs = $dbConnection->db_query($sql);
			$thisNumRows = $dbConnection->db_getMysqlNumRows($rs);

			// if($thisNumRows == 1 && ($_GET["searchBoxSupplier"] != "" || $_REQUEST["editSupplierNumber"]  != "")) {
			if($thisNumRows == 1) {
				while($ds = mysqli_fetch_assoc($rs)){
					$_REQUEST["editID"] = $ds["suppliersID"];
				}
			}
			else if($thisNumRows > 1) {

				$pagesCount = ceil($thisNumRows / MAX_CUSTOMERS_PER_PAGE);

				if($pagesCount > 1) {
					if($_REQUEST["page"] == "" || $_REQUEST["page"] == 0) {
						$_REQUEST["page"] = 1;
					}
				}
				else {
					$_REQUEST["page"] = 1;
				}

				if(MAX_CUSTOMERS_PER_PAGE > 0) {
					$sql .= " LIMIT " . (($_REQUEST["page"] - 1) * MAX_CUSTOMERS_PER_PAGE) . ", " . MAX_CUSTOMERS_PER_PAGE." ";
				}

				$rs = $dbConnection->db_query($sql);

				$arrSupplierDatas = array();
				while($ds = mysqli_fetch_assoc($rs)) {
					foreach(array_keys($ds) as $field) {
						$arrSupplierDatas[$ds["suppliersID"]][$field] = $ds[$field];
					}
				}
			}
			else if($thisNumRows < 1) {
				$warningMessage .= ' Es wurden keine Daten gefunden!' . '<br />';
			}
		}
	// EOF READ ALL DATAS


	// BOF READ SELECTED DATAS
		if($_REQUEST["editID"] != "") {
			$sql = "SELECT
						`suppliersID`,
						`suppliersKundennummer`,
						`suppliersFirmenname`,
						`suppliersFirmennameZusatz`,
						`suppliersFirmaArtikelnummerKurzform`,
						`suppliersOurCustomerNumber`,
						`suppliersFirmenInhaberVorname`,
						`suppliersFirmenInhaberNachname`,
						`suppliersFirmenInhaberAnrede`,
						`suppliersFirmenInhaber2Vorname`,
						`suppliersFirmenInhaber2Nachname`,
						`suppliersFirmenInhaber2Anrede`,
						`suppliersAnsprechpartner1Vorname`,
						`suppliersAnsprechpartner1Nachname`,
						`suppliersAnsprechpartner1Anrede`,
						`suppliersAnsprechpartner2Vorname`,
						`suppliersAnsprechpartner2Nachname`,
						`suppliersAnsprechpartner2Anrede`,
						`suppliersTelefon1`,
						`suppliersTelefon2`,
						`suppliersMobil1`,
						`suppliersMobil2`,
						`suppliersFax1`,
						`suppliersFax2`,
						`suppliersMail1`,
						`suppliersMail2`,
						`suppliersHomepage`,
						`suppliersCompanyStrasse`,
						`suppliersCompanyHausnummer`,
						`suppliersCompanyCountry`,
						`suppliersCompanyPLZ`,
						`suppliersCompanyOrt`,
						`suppliersLieferadresseStrasse`,
						`suppliersLieferadresseHausnummer`,
						`suppliersLieferadressePLZ`,
						`suppliersLieferadresseOrt`,
						`suppliersLieferadresseLand`,
						`suppliersRechnungsadresseStrasse`,
						`suppliersRechnungsadresseHausnummer`,
						`suppliersRechnungsadressePLZ`,
						`suppliersRechnungsadresseOrt`,
						`suppliersRechnungsadresseLand`,
						`suppliersKontoinhaber`,
						`suppliersBankName`,
						`suppliersBankKontonummer`,
						`suppliersBankLeitzahl`,
						`suppliersBankIBAN`,
						`suppliersBankBIC`,
						`suppliersBezahlart`,
						`suppliersZahlungskondition`,
						`suppliersRabatt`,
						`suppliersUseProductMwst`,
						`suppliersUseProductDiscount`,
						`suppliersUstID`,
						`suppliersVertreterID`,
						`suppliersUseSalesmanDeliveryAdress`,
						`suppliersUseSalesmanInvoiceAdress`,
						`suppliersTyp`,
						`suppliersGruppe`,
						`suppliersNotiz`,
						`suppliersActive`,
						`suppliersShopLoginUser`,
						`suppliersShopLoginPassword`

						FROM `" . TABLE_SUPPLIERS . "`

						WHERE `suppliersID` = '".$_REQUEST["editID"]."'
				";
						// WHERE `suppliersID` = '".$_REQUEST["editID"]."'
				$rs = $dbConnection->db_query($sql);

				$supplierDatas = array();
				list(
					$supplierDatas["suppliersID"],
					$supplierDatas["suppliersKundennummer"],
					$supplierDatas["suppliersFirmenname"],
					$supplierDatas["suppliersFirmennameZusatz"],
					$supplierDatas["suppliersFirmaArtikelnummerKurzform"],
					$supplierDatas["suppliersOurCustomerNumber"],
					$supplierDatas["suppliersFirmenInhaberVorname"],
					$supplierDatas["suppliersFirmenInhaberNachname"],
					$supplierDatas["suppliersFirmenInhaberAnrede"],
					$supplierDatas["suppliersFirmenInhaber2Vorname"],
					$supplierDatas["suppliersFirmenInhaber2Nachname"],
					$supplierDatas["suppliersFirmenInhaber2Anrede"],
					$supplierDatas["suppliersAnsprechpartner1Vorname"],
					$supplierDatas["suppliersAnsprechpartner1Nachname"],
					$supplierDatas["suppliersAnsprechpartner1Anrede"],
					$supplierDatas["suppliersAnsprechpartner2Vorname"],
					$supplierDatas["suppliersAnsprechpartner2Nachname"],
					$supplierDatas["suppliersAnsprechpartner2Anrede"],
					$supplierDatas["suppliersTelefon1"],
					$supplierDatas["suppliersTelefon2"],
					$supplierDatas["suppliersMobil1"],
					$supplierDatas["suppliersMobil2"],
					$supplierDatas["suppliersFax1"],
					$supplierDatas["suppliersFax2"],
					$supplierDatas["suppliersMail1"],
					$supplierDatas["suppliersMail2"],
					$supplierDatas["suppliersHomepage"],
					$supplierDatas["suppliersCompanyStrasse"],
					$supplierDatas["suppliersCompanyHausnummer"],
					$supplierDatas["suppliersCompanyCountry"],
					$supplierDatas["suppliersCompanyPLZ"],
					$supplierDatas["suppliersCompanyOrt"],
					$supplierDatas["suppliersLieferadresseStrasse"],
					$supplierDatas["suppliersLieferadresseHausnummer"],
					$supplierDatas["suppliersLieferadressePLZ"],
					$supplierDatas["suppliersLieferadresseOrt"],
					$supplierDatas["suppliersLieferadresseLand"],
					$supplierDatas["suppliersRechnungsadresseStrasse"],
					$supplierDatas["suppliersRechnungsadresseHausnummer"],
					$supplierDatas["suppliersRechnungsadressePLZ"],
					$supplierDatas["suppliersRechnungsadresseOrt"],
					$supplierDatas["suppliersRechnungsadresseLand"],
					$supplierDatas["suppliersKontoinhaber"],
					$supplierDatas["suppliersBankName"],
					$supplierDatas["suppliersBankKontonummer"],
					$supplierDatas["suppliersBankLeitzahl"],
					$supplierDatas["suppliersBankIBAN"],
					$supplierDatas["suppliersBankBIC"],
					$supplierDatas["suppliersBezahlart"],
					$supplierDatas["suppliersZahlungskondition"],
					$supplierDatas["suppliersRabatt"],
					$supplierDatas["suppliersUseProductMwst"],
					$supplierDatas["suppliersUseProductDiscount"],
					$supplierDatas["suppliersUstID"],
					$supplierDatas["suppliersVertreterID"],
					$supplierDatas["suppliersUseSalesmanDeliveryAdress"],
					$supplierDatas["suppliersUseSalesmanInvoiceAdress"],
					$supplierDatas["suppliersTyp"],
					$supplierDatas["suppliersGruppe"],
					$supplierDatas["suppliersNotiz"],
					$supplierDatas["suppliersActive"],
					$supplierDatas["suppliersShopLoginUser"],
					$supplierDatas["suppliersShopLoginPassword"]

			) = mysqli_fetch_array($rs);

			if((!$doAction || !$isCheckSumOk || !$checkCustomerEmail) && $_POST["storeDatas"] != "") {
				foreach($_POST as $thisKey => $thisValue) {
					// $supplierDatas["suppliersActive"] = $_POST["editSuppliersActive"];
					$thisKeyNew = preg_replace("/editSuppliers/", "suppliers", $thisKey);
					// echo $thisKey . ' | ' . $thisKeyNew . "<br />";
					$supplierDatas[$thisKeyNew] = $_POST[$thisKey];
				}
			}
		}
	// EOF READ SELECTED DATAS

	// BOF READ PHONE CODES
		if($_REQUEST["editID"] != "" && $_REQUEST["editID"] != "NEW") {
			$arrPhoneCodes = getPhoneCodes(array(
								array("countryID" => $supplierDatas["suppliersCompanyCountry"], "zipCode" => $supplierDatas["suppliersCompanyPLZ"]),
								array("countryID" => $supplierDatas["suppliersLieferadresseLand"], "zipCode" => $supplierDatas["suppliersLieferadressePLZ"]),
								array("countryID" => $supplierDatas["suppliersRechnungsadresseLand"], "zipCode" => $supplierDatas["suppliersRechnungsadressePLZ"])
							)
						);
		}
	// EOF READ PHONE CODES
?>
<?php
	require_once('inc/headerHTML.inc.php');
	if($_REQUEST["editID"] == "NEW") {
		$thisTitle = 'Lieferanten anlegen';
		$thisIcon = 'addSupplier.png';
	}
	else {
		$thisTitle = 'Lieferanten / Dienstleistungen verwalten';
		$thisIcon = 'suppliers.png';
		if($supplierDatas["suppliersKundennummer"] != '') {
			$thisTitle .= ': <span class="headerSelectedEntry">' . $supplierDatas["suppliersKundennummer"] . ' &bull; ' . $supplierDatas["suppliersFirmenname"] . '</span>';
		}
	}
	$headerHTML = preg_replace("/{###TITLE###}/", strip_tags($thisTitle), $headerHTML);
	echo $headerHTML;

	require_once(FILE_MENUE_TOP);
?>

<div id="mainArea">
	<div id="mainContent">
		<?php require_once(FILE_MENUE_SIDEBAR); ?>
		<div id="contentArea">

			<div id="contentAreaElements">
				<h1><?php if(SHOW_PAGE_TITLE_ICONS) { echo '<img src="' . PATH_ICONS_MENUE_TITLES . $thisIcon . '" alt="" />'; } ?> <?php echo $thisTitle; ?></h1>

				<?php if($_REQUEST["editID"] != "NEW") { ?>
				<div class="menueActionsArea">
					<?php if($arrGetUserRights["editSuppliers"]) { ?><a href="<?php echo PAGE_EDIT_DISTRIBUTORS; ?>?editID=NEW" class="linkButton" title="Neuen Lieferanten anlegen" >Lieferanten anlegen</a><?php } ?>
					<?php if($_REQUEST["editID"] != "") { ?>
					<?php if($arrGetUserRights["adminArea"] == '1' && $arrGetUserRights["editLetters"]) { ?>
						<a href="<?php echo PAGE_WRITE_LETTER; ?>?editID=<?php echo $supplierDatas["suppliersKundennummer"]; ?>&amp;customersSource=suppliers" class="linkButton <?php echo $disableClass; ?>" title="Neuen Brief schreiben <?php echo $disableTitle; ?>" <?php echo $disableLink; ?> >Brief</a>
					<?php } ?>

					<?php } ?>

					<div class="clear"></div>
				</div>
				<?php } ?>
				<div class="adminInfo">Datensatz-ID: <?php echo $supplierDatas["suppliersID"]; ?></div>
				<div class="contentDisplay">
				<?php if($_REQUEST["editID"] == "") { ?>
				<div id="searchFilterArea">
					<form name="formFilterSearch" method="get" action="<?php echo $_SERVER["PHP_SELF"]; ?>">
						<table border="0" cellpadding="0" cellspacing="0" class="searchFilterContent">
							<tr>
								<!--
								<td>
									<label for="searchSupplierNumber">Lieferantennummer:</label>
									<input type="text" name="searchSupplierNumber" id="searchSupplierNumber" class="inputField_40" value="" />
								</td>
								-->
								<td>
									<label for="searchSupplierCategorie">Kategorie:</label>
									<select name="searchSupplierCategorie" id="searchSupplierCategorie" class="inputSelect_200">
										<option value=""> --- Bitte w&auml;hlen --- </option>
										<?php
											if(!empty($arrSupplierTypeDatas)){
												foreach($arrSupplierTypeDatas as $thisKey => $thisValue){
													echo '<option value="' . $thisKey . '">' . $thisValue["supplierTypesName"] . '</option>';
												}
											}
										?>
									</select>

								</td>
								<td>
									<label for="searchSupplierName">Firmenname:</label>
									<input type="text" name="searchSupplierName" id="searchSupplierName" class="inputField_160" value="" />
								</td>
								<td>
									<label for="searchPLZ">PLZ:</label>
									<input type="text" name="searchPLZ" id="searchPLZ" class="inputField_40" />
								</td>
								<td>
									<label for="searchWord">Suchbegriff:</label>
									<input type="text" name="searchWord" id="searchWord" class="inputField_100" />
								</td>
								<td>
									<input type="hidden" name="xxxeditID" id="xxxeditID" value="" />
									<input type="submit" name="submitSearch" class="inputButton0" value="Suchen" />
								</td>
							</tr>
						</table>
					</form>
				</div>

				<?php } ?>
					<?php displayMessages(); ?>

					<?php
						if($_REQUEST["editID"] == "") {
							if(!empty($arrSupplierDatas)) {

								if($pagesCount > 1 && $_REQUEST["displayOrderStatus"] == "") {
									include(FILE_MENUE_PAGES);
								}

								echo '<table cellpadding="0" cellspacing="0" border="0" width="100%" class="displayOrders">
										<thead>
											<tr>
												<th style="width:45px;text-align:right;">#</th>
												<th>Lieferantennr.</th>
												<th>Firmenname</th>
												<th>Kurz</th>
												<th>Inhaber</th>
												<th>Land</th>
												<th>PLZ</th>
												<th>Ort</th>
												<th>Telefon</th>
												<!--
												<th>Typ</th>
												-->
												<th>Unsere KNR</th>
												<th>Info</th>
											</tr>
										</thead>
										<tbody>
								';



								$count = 0;
								$thisMarker = '';
								foreach($arrSupplierDatas as $thisKey => $thisValue) {
									if($count%2 == 0){ $rowClass = 'row0'; }
									else { $rowClass = 'row1'; }

									if($thisMarker != $arrSupplierTypeDatas[$thisValue["suppliersTyp"]]["supplierTypesName"]){
										echo '<tr>';
										echo '<td colspan="11" class="tableRowTitle1">';
										echo $arrSupplierTypeDatas[$thisValue["suppliersTyp"]]["supplierTypesName"];
										echo '</td>';
										echo '</tr>';
										$thisMarker = $arrSupplierTypeDatas[$thisValue["suppliersTyp"]]["supplierTypesName"];
									}

									echo '<tr class="'.$rowClass.'">';
										echo '<td style="text-align:right;"><span style="font-weight:bold;cursor:pointer;" title="Datensatz-ID: '.($thisValue["suppliersID"]).'">'.($count + 1).'.</span></td>';
										echo '<td style="text-align:center;">';
										if($arrGetUserRights["editSuppliers"] || $arrGetUserRights["displaySuppliers"]) {
											echo '<a href="' . PAGE_EDIT_DISTRIBUTORS . '?editID=' . $thisValue["suppliersID"] . '" title="Lieferanten bearbeiten (Datensatz '.$thisValue["suppliersKundennummer"].')" >';
										}
										echo $thisValue["suppliersKundennummer"];
										if($arrGetUserRights["editSuppliers"] || $arrGetUserRights["displaySuppliers"]) {
											echo '</a>';
										}
										echo '</td>';
										echo '<td style="white-space:nowrap;text-align:left;">'.htmlentities(utf8_decode($thisValue["suppliersFirmenname"])).'</td>';
										echo '<td style="white-space:nowrap;text-align:left;font-weight:bold;">'.$thisValue["suppliersFirmaArtikelnummerKurzform"].'</td>';

										echo '<td style="white-space:nowrap;text-align:left;">'.trim(preg_replace("/:/ismU", "", preg_replace("/^Inh./ismU", "", $thisValue["suppliersFirmenInhaberVorname"].' '.$thisValue["suppliersFirmenInhaberNachname"]))).'</td>';

										echo '<td>';
										echo '<span title="' . $arrCountryTypeDatas[$thisValue["suppliersCompanyCountry"]]["countries_name"] . '">' . $arrCountryTypeDatas[$thisValue["suppliersCompanyCountry"]]["countries_iso_code_3"] . '</span>';
										echo '</td>';

										echo '<td>';
										echo $thisValue["suppliersCompanyPLZ"];
										echo '</td>';

										echo '<td style="white-space:nowrap;">';
										echo htmlentities(utf8_decode($thisValue["suppliersCompanyOrt"]));
										echo '</td>';

										echo '<td style="white-space:nowrap;">';
										if($thisValue["suppliersTelefon1"] != '') {
											echo formatPhoneNumber($thisValue["suppliersTelefon1"]) . '<br />';
										}
										else if($thisValue["suppliersTelefon2"] != '') {
											echo formatPhoneNumber($thisValue["suppliersTelefon2"]) . '<br />';
										}
										if($thisValue["suppliersMobil1"] != '') {
											echo formatPhoneNumber($thisValue["suppliersMobil1"]) . '<br />';
										}
										else if($thisValue["suppliersMobil2"] != '') {
											echo formatPhoneNumber($thisValue["suppliersMobil2"]) . '<br />';
										}
										echo '</td>';


										#echo '<td style="white-space:nowrap;">';
										#echo $arrSupplierTypeDatas[$thisValue["suppliersTyp"]]["supplierTypesName"];
										#echo '</td>';

										echo '<td style="white-space:nowrap;text-align:right;font-weight:bold;">';
										echo $thisValue["suppliersOurCustomerNumber"];
										echo '</td>';

										echo '<td style="white-space:nowrap;">';
											if($thisValue["suppliersNotiz"] != "") {
												echo '<div class="toolItem">';
												echo '<img src="layout/icons/iconRtf.gif" class="buttonNotice" width="16" height="16" alt="Bemerkung" />';
												echo '<div class="displayNoticeArea" style="display:none; visibility:hidden;">';
												echo nl2br(htmlentities(utf8_decode($thisValue["suppliersNotiz"])));
												echo '</div>';
												echo '</div>';
											}
											else {
												echo '<span class="toolItem"><img src="layout/spacer.gif" width="16" height="16" alt="" /></span>';
											}
											if($arrGetUserRights["editSuppliers"] || $arrGetUserRights["displaySuppliers"]) {
												echo '<span class="toolItem"><a href="' . PAGE_EDIT_DISTRIBUTORS . '?editID=' . $thisValue["suppliersID"] . '"><img src="layout/icons/iconEdit.gif" width="16" height="16" title="Lieferanten bearbeiten (Datensatz '.$thisValue["suppliersKundennummer"].')" alt="Bearbeiten" /></a></span>';
											}
											else {
												echo '<span class="toolItem"><img src="layout/spacer.gif" width="16" height="16" alt="" /></span>';
											}
											if($thisValue["suppliersMail1"] != "") {
												echo '<span class="toolItem"><a href="mailto:'.$thisValue["suppliersMail1"].'"><img src="layout/icons/iconMail.gif" width="16" height="14" title="Lieferanten &quot;'.htmlentities(utf8_decode($thisValue["suppliersFirmenname"])). '&quot; (KNR: '.$thisValue["suppliersKundennummer"].') per Mail kontaktieren" alt="Mail" /></a></span>';
											}
											else {
												echo '<span class="toolItem"><img src="layout/spacer.gif" width="16" height="16" alt="" /></span>';
											}
											if($thisValue["suppliersHomepage"] != "") {
												echo '<span class="toolItem"><a href="http://'.$thisValue["suppliersHomepage"].'" target="_blank"><img src="layout/icons/iconWeb.gif" width="16" height="16" title="Homepage des Lieferanten aufrufen" alt="Homepage" /></a></span>';
											}
											else {
												echo '<span class="toolItem"><img src="layout/spacer.gif" width="16" height="16" alt="" /></span>';
											}
										echo '</td>';
									echo '</tr>';
									$count++;
								}
								echo '	</tbody>
									</table>
								';

								if($pagesCount > 1 && $_REQUEST["displayOrderStatus"] == "") {
									include(FILE_MENUE_PAGES);
								}

							}
							else {

							}
						}
					?>

					<?php if($_REQUEST["editID"] != "") { ?>

					<div id="tabs">
						<ul>
							<li><a href="#tabs-1">Lieferanten-Daten</a></li>
							<?php if($_REQUEST["editID"] != "NEW") { ?>
							<li><a href="#tabs-2">Briefe</a></li>
							<li><a href="#tabs-3">Rechnungserfassung</a></li>
							<?php } ?>
						</ul>
						<div class="adminEditArea">
							<div id="tabs-1">
								<form name="formEditSupplierDatas" id="formEditSupplierDatas" method="post" action="<?php echo $_SERVER["PHP_SELF"]; ?>" enctype="multipart/form-data" >
									<input type="hidden" name="editSuppliersID" value="<?php echo $supplierDatas["suppliersID"]; ?>" />
									<input type="hidden" name="editSuppliersActive" value="<?php echo $supplierDatas["suppliersActive"]; ?>" />
									<br /><p class="warningArea">Pflichtfelder  m&uuml;ssen ausgef&uuml;llt werden!</p>
									<fieldset>
										<legend>Lieferantendaten</legend>
										<table border="0" width="100%" cellspacing="0" cellpadding="0">
											<tr>
												<td style="width:200px;"><b>Lieferantennummer:</b></td>
												<td>
													<?php
														/*
														if($_REQUEST["editID"] == "NEW") {
															echo '<a href="javascript:void(\'0\');" onclick="createSupplierNumber(\'formEditSupplierDatas\', \'editSuppliersKundennummer\', \'editSuppliersKundennummer\'); "class="linkButton" id="generateSupplierNumberButton">Kundennummer generieren</a>';
															$readonly = '';
														}
														else {
															$readonly = ' readonly="readonly" ';
														}
														*/
														if($_REQUEST["editID"] == "NEW") {
															$readonly = ' readonly="readonly" ';
															#$description = 'Soll eine <b>Lieferantennummer automatisch generiert</b> werden, geben Sie bitte die <b>ersten beiden Ziffern der Postleitzahl</b> ein. <br />Die neue Lieferantennnummer wird dann <b>automatisch nach dem Speichern</b> angelegt.';
															$description = 'Soll eine <b>Lieferantennummer automatisch generiert</b> werden, <br />dieses <b>Feld bitte leer</b> lassen. <br />Die neue Lieferantennummer wird dann <b>automatisch nach dem Speichern</b> angelegt.';
														}
														else if($supplierDatas["suppliersKundennummer"] != ''){
															$readonly = ' readonly="readonly" ';
															$description = '';
														}
													?>
													<input type="text" name="editSuppliersKundennummer" id="editSuppliersKundennummer" class="inputField_100" <?php if($_REQUEST["editID"] == "NEW") { echo ' minlength="2" '; } ?> value="<?php echo $supplierDatas["suppliersKundennummer"]; ?>" <?php echo $readonly; ?> />
													<?php if($description != '') { echo '<p class="infoArea" style="float:right;">' . $description . '</p>'; } ?>
												</td>
											</tr>
											<tr>
												<td><b>Firmenname:</b></td>
												<td><input type="text" name="editSuppliersFirmenname" id="editSuppliersFirmenname" style="font-weight:bold;" class="inputField_510" value="<?php echo ($supplierDatas["suppliersFirmenname"]); ?>" /></td>
											</tr>
											<tr>
												<td><b>Firmenname Zusatz:</b></td>
												<td>
													<input type="text" name="editSuppliersFirmennameZusatz" id="editSuppliersFirmennameZusatz" class="inputField_510" value="<?php echo ($supplierDatas["suppliersFirmennameZusatz"]); ?>" />
												</td>
											</tr>
											<tr>
												<td><b>Firmenname Kurzform:</b></td>
												<td>
													<input type="text" name="editSuppliersFirmaArtikelnummerKurzform" id="editSuppliersFirmaArtikelnummerKurzform" class="inputField_510" maxlength="2" value="<?php echo ($supplierDatas["suppliersFirmaArtikelnummerKurzform"]); ?>" />
													<br /><span class="infotext">Diese Kurform steht am Ende der Bestellnummern, z. B. &quot;EI&quot; f&uuml;r EICHNER</span>
												</td>
											</tr>
											<tr>
												<td style="width:200px;"><b>Unsere Kundennummer:</b></td>
												<td>
													<input type="text" name="editSuppliersOurCustomerNumber" id="editSuppliersOurCustomerNumber" style="font-weight:bold;" class="inputField_160" value="<?php echo $supplierDatas["suppliersOurCustomerNumber"]; ?>" />
												</td>
											</tr>

											<tr>
												<td style="width:200px;"><b>Unsere Login-Daten:</b></td>
												<td>
													<div class="inputArea1">
														<span><b>Benutzer:</b> <input type="text" name="editSuppliersShopLoginUser" id="editSuppliersShopLoginUser" class="inputField_260" value="<?php echo $supplierDatas["suppliersShopLoginUser"]; ?>" /></span>
														<span><b>Passwort:</b> <input type="text" name="editSuppliersShopLoginPassword" id="editSuppliersShopLoginPassword" class="inputField_120" value="<?php echo $supplierDatas["suppliersShopLoginPassword"]; ?>" /></span>
													</div>
												</td>
											</tr>

											<tr>
												<td><b>Lieferanten-Typ:</b></td>
												<td>
													<select name="editSuppliersTyp" id="editSuppliersTyp" class="inputSelect_510">
														<option value=""> - </option>
														<?php
															if(!empty($arrSupplierTypeDatas)) {
																foreach($arrSupplierTypeDatas as $thisKey => $thisValue) {
																	$selected = '';
																	if($thisKey == $supplierDatas["suppliersTyp"]) {
																		$selected = ' selected="selected" ';
																	}
																	echo '
																		<option value="' . $thisKey . '" '.$selected.' >' . ($arrSupplierTypeDatas[$thisKey]["supplierTypesName"]). '</option>
																	';
																}
															}
														?>
													</select>
												</td>
											</tr>
											<tr>
												<td><b>Firmeninhaber:</b></td>
												<td>
													<select name="editSuppliersFirmenInhaberAnrede" id="editSuppliersFirmenInhaberAnrede" class="inputSelect_70">
														<option value=""> - </option>
														<?php
															if(!empty($arrSalutationTypeDatas)) {
																foreach($arrSalutationTypeDatas as $thisKey => $thisValue) {
																	$selected = '';
																	if($thisKey == $supplierDatas["suppliersFirmenInhaberAnrede"]) {
																		$selected = ' selected="selected" ';
																	}
																	echo '
																		<option value="' . $thisKey . '" '.$selected.' >' . ($arrSalutationTypeDatas[$thisKey]["salutationTypesName"]). '</option>
																	';
																}
															}
														?>
													</select> -
													<input type="text" name="editSuppliersFirmenInhaberVorname" id="editSuppliersFirmenInhaberVorname" class="inputField_205" value="<?php echo ($supplierDatas["suppliersFirmenInhaberVorname"]); ?>" /> &nbsp;
													<input type="text" name="editSuppliersFirmenInhaberNachname" id="editSuppliersFirmenInhaberNachname" class="inputField_205" value="<?php echo ($supplierDatas["suppliersFirmenInhaberNachname"]); ?>" />
													<br /><span class="infotext">(Anrede / Vorname / Nachname)</span>
												</td>
											</tr>
											<tr>
												<td><b>Firmeninhaber 2:</b></td>
												<td>
													<select name="editSuppliersFirmenInhaber2Anrede" id="editSuppliersFirmenInhaber2Anrede" class="inputSelect_70">
														<option value=""> - </option>
														<?php
															if(!empty($arrSalutationTypeDatas)) {
																foreach($arrSalutationTypeDatas as $thisKey => $thisValue) {
																	$selected = '';
																	if($thisKey == $supplierDatas["suppliersFirmenInhaber2Anrede"]) {
																		$selected = ' selected="selected" ';
																	}
																	echo '
																		<option value="' . $thisKey . '" '.$selected.' >' . ($arrSalutationTypeDatas[$thisKey]["salutationTypesName"]). '</option>
																	';
																}
															}
														?>
													</select> -
													<input type="text" name="editSuppliersFirmenInhaber2Vorname" id="editSuppliersFirmenInhaber2Vorname" class="inputField_205" value="<?php echo ($supplierDatas["suppliersFirmenInhaber2Vorname"]); ?>" /> &nbsp;
													<input type="text" name="editSuppliersFirmenInhaber2Nachname" id="editSuppliersFirmenInhaber2Nachname" class="inputField_205" value="<?php echo ($supplierDatas["suppliersFirmenInhaber2Nachname"]); ?>" />
													<br /><span class="infotext">(Anrede / Vorname / Nachname)</span>
												</td>
											</tr>
											<tr>
												<td><b>Firmenansprechpartner 1:</b></td>
												<td>
													<select name="editSuppliersAnsprechpartner1Anrede" id="editSuppliersAnsprechpartner1Anrede" class="inputSelect_70">
														<option value=""> - </option>
														<?php
															if(!empty($arrSalutationTypeDatas)) {
																foreach($arrSalutationTypeDatas as $thisKey => $thisValue) {
																	$selected = '';
																	if($thisKey == $supplierDatas["suppliersAnsprechpartner1Anrede"]) {
																		$selected = ' selected="selected" ';
																	}
																	echo '
																		<option value="' . $thisKey . '" '.$selected.' >' . ($arrSalutationTypeDatas[$thisKey]["salutationTypesName"]). '</option>
																	';
																}
															}
														?>
													</select> -
													<input type="text" name="editSuppliersAnsprechpartner1Vorname" id="editSuppliersAnsprechpartner1Vorname" class="inputField_205" value="<?php echo ($supplierDatas["suppliersAnsprechpartner1Vorname"]); ?>" /> &nbsp;
													<input type="text" name="editSuppliersAnsprechpartner1Nachname" id="editSuppliersAnsprechpartner1Nachname" class="inputField_205" value="<?php echo ($supplierDatas["suppliersAnsprechpartner1Nachname"]); ?>" />
													<br /><span class="infotext">(Anrede / Vorname / Nachname)</span>
												</td>
											</tr>
											<tr>
												<td><b>Firmenansprechpartner 2:</b></td>
												<td>
													<select name="editSuppliersAnsprechpartner2Anrede" id="editSuppliersAnsprechpartner2Anrede" class="inputSelect_70">
														<option value=""> - </option>
														<?php
															if(!empty($arrSalutationTypeDatas)) {
																foreach($arrSalutationTypeDatas as $thisKey => $thisValue) {
																	$selected = '';
																	if($thisKey == $supplierDatas["suppliersAnsprechpartner2Anrede"]) {
																		$selected = ' selected="selected" ';
																	}
																	echo '
																		<option value="' . $thisKey . '" '.$selected.' >' . ($arrSalutationTypeDatas[$thisKey]["salutationTypesName"]). '</option>
																	';
																}
															}
														?>
													</select> -
													<input type="text" name="editSuppliersAnsprechpartner2Vorname" id="editSuppliersAnsprechpartner2Vorname" class="inputField_205" value="<?php echo ($supplierDatas["suppliersAnsprechpartner2Vorname"]); ?>" /> &nbsp;
													<input type="text" name="editSuppliersAnsprechpartner2Nachname" id="editSuppliersAnsprechpartner2Nachname" class="inputField_205" value="<?php echo ($supplierDatas["suppliersAnsprechpartner2Nachname"]); ?>" />
													<br /><span class="infotext">(Anrede / Vorname / Nachname)</span>
												</td>
											</tr>
											<tr>
												<td><b>Telefon 1:</b></td>
												<td><input type="text" name="editSuppliersTelefon1" id="editSuppliersTelefon1" class="inputField_510" value="<?php echo formatPhoneNumber($supplierDatas["suppliersTelefon1"], $arrPhoneCodes); ?>" /></td>
											</tr>
											<tr>
												<td><b>Telefon 2:</b></td>
												<td><input type="text" name="editSuppliersTelefon2" id="editSuppliersTelefon2" class="inputField_510" value="<?php echo formatPhoneNumber($supplierDatas["suppliersTelefon2"], $arrPhoneCodes); ?>" /></td>
											</tr>
											<tr>
												<td><b>Mobil 1:</b></td>
												<td><input type="text" name="editSuppliersMobil1" id="editSuppliersMobil1" class="inputField_510" value="<?php echo formatPhoneNumber($supplierDatas["suppliersMobil1"]); ?>" /></td>
											</tr>
											<tr>
												<td><b>Mobil 2:</b></td>
												<td><input type="text" name="editSuppliersMobil2" id="editSuppliersMobil2" class="inputField_510" value="<?php echo formatPhoneNumber($supplierDatas["suppliersMobil2"]); ?>" /></td>
											</tr>
											<tr>
												<td><b>Fax 1:</b></td>
												<td><input type="text" name="editSuppliersFax1" id="editSuppliersFax1" class="inputField_510" value="<?php echo formatPhoneNumber($supplierDatas["suppliersFax1"], $arrPhoneCodes); ?>" /></td>
											</tr>
											<tr>
												<td><b>Fax 2:</b></td>
												<td><input type="text" name="editSuppliersFax2" id="editSuppliersFax2" class="inputField_510" value="<?php echo formatPhoneNumber($supplierDatas["suppliersFax2"], $arrPhoneCodes); ?>" /></td>
											</tr>
											<tr>
												<td><b>Mail-Adresse 1:</b></td>
												<td>
													<input type="text" name="editSuppliersMail1" id="editSuppliersMail1" class="inputField_510" value="<?php echo ($supplierDatas["suppliersMail1"]); ?>" />
													<?php
													if($supplierDatas["suppliersMail1"] != ""){
														echo ' <a href="mailto:' . $supplierDatas["suppliersMail1"] . '" ><img src="layout/icons/iconMail.gif" width="16" height="14" alt="Mail senden" title="Diesen Kontakt per Mail kontaktieren" /></a>';
													}
													else {
														echo ' <img src="layout/icons/spacer.gif" width="16" height="14" alt="" title="" />';
													}
													?>
												</td>
											</tr>
											<tr>
												<td><b>Mail-Adresse 2:</b></td>
												<td>
													<input type="text" name="editSuppliersMail2" id="editSuppliersMail2" class="inputField_510" value="<?php echo ($supplierDatas["suppliersMail2"]); ?>" />
													<?php
													if($supplierDatas["suppliersMail2"] != ""){
														echo ' <a href="mailto:' . $supplierDatas["suppliersMail2"] . '" ><img src="layout/icons/iconMail.gif" width="16" height="14" alt="Mail senden" title="Diesen Kontakt per Mail kontaktieren" /></a>';
													}
													else {
														echo ' <img src="layout/icons/spacer.gif" width="16" height="14" alt="" title="" />';
													}
													?>
												</td>
											</tr>
											<tr>
												<td><b>Homepage:</b></td>
												<td>
													<input type="text" name="editSuppliersHomepage" id="editSuppliersHomepage" class="inputField_510" value="<?php echo ($supplierDatas["suppliersHomepage"]); ?>" />
													<?php
														if($supplierDatas["suppliersHomepage"] != ''){
															echo '<a href="http://' . $supplierDatas["suppliersHomepage"] . '" target="_blank"><img src="layout/icons/iconWeb.gif" width="16" height="16" alt="Homepage" title="Zur Homepage" /></a>';
														}
													?>
												</td>
											</tr>
										</table>
									</fieldset>

									<fieldset id="Firmenadresse">
										<legend>Firmenanschrift</legend>
										<!--
										<p>
										<input type="checkbox" name="Rechnungsadresse_Firmenadresse" id="Rechnungsadresse_Firmenadresse" value="1" /> <b>Firmenadresse als Rechnungsadresse verwenden?</b>
										<input type="checkbox" name="Lieferadresse_Firmenadresse" id="Lieferadresse_Firmenadresse" value="1" /> <b>Firmenadresse als Lieferadresse verwenden?</b>
										</p>
										-->
										<table border="0" width="100%" cellspacing="0" cellpadding="0">
											<tr>
												<td style="width:200px;"><b>Stra&szlig;e / Hausnummer:</b></td>
												<td>
													<input type="text" name="editSuppliersCompanyStrasse" id="editSuppliersFirmenadresseStrasse" class="inputField_427" value="<?php echo ($supplierDatas["suppliersCompanyStrasse"]); ?>" /> /
													<input type="text" name="editSuppliersCompanyHausnummer" id="editSuppliersFirmenadresseHausnummer" class="inputField_70" value="<?php echo ($supplierDatas["suppliersCompanyHausnummer"]); ?>" />
												</td>
											</tr>
											<tr>
												<td><b>PLZ / Ort:</b></td>
												<td>
													<input type="text" name="editSuppliersCompanyPLZ" id="editSuppliersFirmenadressePLZ" class="inputField_70" value="<?php echo ($supplierDatas["suppliersCompanyPLZ"]); ?>" /> /
													<input type="text" name="editSuppliersCompanyOrt" id="editSuppliersFirmenadresseOrt" class="inputField_427" value="<?php echo ($supplierDatas["suppliersCompanyOrt"]); ?>" /></td>
											</tr>
											<tr>
												<td><b>Land:</b></td>
												<td>
													<select name="editSuppliersCompanyCountry" id="editSuppliersFirmenadresseLand" class="inputSelect_510">
														<option value=""> - </option>
														<?php
															if(!empty($arrCountryTypeDatas)) {
																foreach($arrCountryTypeDatas as $thisKey => $thisValue) {
																	$selected = '';
																	if($_REQUEST["editID"] == "NEW" && $thisKey == 81) {
																		$selected = ' selected="selected" ';
																	}
																	else if($thisKey == $supplierDatas["suppliersCompanyCountry"]) {
																		$selected = ' selected="selected" ';
																	}
																	echo '
																		<option value="' . $thisKey . '" '.$selected.' >' . ($arrCountryTypeDatas[$thisKey]["countries_name"]). '</option>
																	';
																}
															}
														?>
													</select>
												</td>
											</tr>
										</table>
									</fieldset>

									<!--
									<fieldset id="Rechnungsadresse">
										<legend>Rechnungsanschrift</legend>
										<div>
											<?php
												if($supplierDatas["suppliersUseSalesmanInvoiceAdress"] == '1'){
													$checked = ' checked="checked" ';
													$disabled = ' disabled="disabled" ';
												}
												else {
													$checked = '';
													$disabled = '';
												}
											?>
											<p>
											<input type="checkbox" name="editSuppliersUseSalesmanInvoiceAdress" id="editSuppliersUseSalesmanInvoiceAdress" value="1" <?php echo $checked; ?> /> <label for="editSuppliersUseSalesmanInvoiceAdress">Anschrift des Vertreters verwenden?</label>
											</p>
										</div>
										<table border="0" width="100%" cellspacing="0" cellpadding="0">
											<tr>
												<td style="width:200px;"><b>Stra&szlig;e / Hausnummer:</b></td>
												<td>
													<input type="text" name="editSuppliersRechnungsadresseStrasse" id="editSuppliersRechnungsadresseStrasse" class="inputField_427" value="<?php echo ($supplierDatas["suppliersRechnungsadresseStrasse"]); ?>" <?php echo $disabled; ?> /> /
													<input type="text" name="editSuppliersRechnungsadresseHausnummer" id="editSuppliersRechnungsadresseHausnummer" class="inputField_70" value="<?php echo ($supplierDatas["suppliersRechnungsadresseHausnummer"]); ?>" <?php echo $disabled; ?> />
												</td>
											</tr>
											<tr>
												<td><b>PLZ / Ort:</b></td>
												<td>
													<input type="text" name="editSuppliersRechnungsadressePLZ" id="editSuppliersRechnungsadressePLZ" class="inputField_70" value="<?php echo ($supplierDatas["suppliersRechnungsadressePLZ"]); ?>" <?php echo $disabled; ?> /> /
													<input type="text" name="editSuppliersRechnungsadresseOrt" id="editSuppliersRechnungsadresseOrt" class="inputField_427" value="<?php echo ($supplierDatas["suppliersRechnungsadresseOrt"]); ?>" <?php echo $disabled; ?> /></td>
											</tr>
											<tr>
												<td><b>Land:</b></td>
												<td>
													<select name="editSuppliersRechnungsadresseLand" id="editSuppliersRechnungsadresseLand" class="inputSelect_510" <?php echo $disabled; ?> >
														<option value=""> - </option>
														<?php
															if(!empty($arrCountryTypeDatas)) {
																foreach($arrCountryTypeDatas as $thisKey => $thisValue) {
																	$selected = '';
																	if($_REQUEST["editID"] == "NEW" && $thisKey == 81) {
																		$selected = ' selected ';
																	}
																	else if($thisKey == $supplierDatas["suppliersRechnungsadresseLand"]) {
																		$selected = ' selected ';
																	}
																	echo '
																		<option value="' . $thisKey . '" '.$selected.' >' . ($arrCountryTypeDatas[$thisKey]["countries_name"]). '</option>
																	';
																}
															}
														?>
													</select>
												</td>
											</tr>
										</table>
									</fieldset>

									<fieldset id="Lieferadresse">
										<legend>Lieferanschrift</legend>
										<div>
											<?php
												if($supplierDatas["suppliersUseSalesmanDeliveryAdress"] == '1'){
													$checked = ' checked="checked" ';
													$disabled = ' disabled="disabled" ';
												}
												else {
													$checked = '';
													$disabled = '';
												}
											?>
											<p>
											<input type="checkbox" name="editSuppliersUseSalesmanDeliveryAdress" id="editSuppliersUseSalesmanDeliveryAdress" value="1" <?php echo $checked; ?> /> <label for="editSuppliersUseSalesmanDeliveryAdress">Anschrift des Vertreters verwenden?</label>
											</p>
										</div>
										<table border="0" width="100%" cellspacing="0" cellpadding="0">
											<tr>
												<td style="width:200px;"><b>Stra&szlig;e / Hausnummer:</b></td>
												<td>
													<input type="text" name="editSuppliersLieferadresseStrasse" id="editSuppliersLieferadresseStrasse" class="inputField_427" value="<?php echo ($supplierDatas["suppliersLieferadresseStrasse"]); ?>" <?php echo $disabled; ?> /> /
													<input type="text" name="editSuppliersLieferadresseHausnummer" id="editSuppliersLieferadresseHausnummer" class="inputField_70" value="<?php echo ($supplierDatas["suppliersLieferadresseHausnummer"]); ?>" <?php echo $disabled; ?> />
												</td>
											</tr>
											<tr>
												<td><b>PLZ / Ort:</b></td>
												<td>
													<input type="text" name="editSuppliersLieferadressePLZ" id="editSuppliersLieferadressePLZ" class="inputField_70" value="<?php echo ($supplierDatas["suppliersLieferadressePLZ"]); ?>" <?php echo $disabled; ?> /> /
													<input type="text" name="editSuppliersLieferadresseOrt" id="editSuppliersLieferadresseOrt" class="inputField_427" value="<?php echo ($supplierDatas["suppliersLieferadresseOrt"]); ?>" <?php echo $disabled; ?> />
												</td>
											</tr>
											<tr>
												<td><b>Land:</b></td>
												<td>
													<select name="editSuppliersLieferadresseLand" id="editSuppliersLieferadresseLand" class="inputSelect_510" <?php echo $disabled; ?> >
														<option value=""> - </option>
														<?php
															if(!empty($arrCountryTypeDatas)) {
																foreach($arrCountryTypeDatas as $thisKey => $thisValue) {
																	$selected = '';
																	if($_REQUEST["editID"] == "NEW" && $thisKey == 81) {
																		$selected = ' selected ';
																	}
																	else if($thisKey == $supplierDatas["suppliersLieferadresseLand"]) {
																		$selected = ' selected ';
																	}
																	echo '
																		<option value="' . $thisKey . '" '.$selected.' >' . ($arrCountryTypeDatas[$thisKey]["countries_name"]). '</option>
																	';
																}
															}
														?>
													</select>
												</td>
											</tr>
										</table>
									</fieldset>
									-->

									<fieldset>
										<legend>Bankdaten</legend>
										<table border="0" width="100%" cellspacing="0" cellpadding="0">
											<tr>
												<td><b>Bezahlart:</b></td>
												<td>
													<select name="editSuppliersBezahlart" id="editSuppliersBezahlart" class="inputSelect_510">
														<option value=""> - </option>
														<?php
															if(!empty($arrPaymentTypeDatas)) {
																foreach($arrPaymentTypeDatas as $thisKey => $thisValue) {
																	$selected = '';
																	if($_REQUEST["editID"] == "NEW" && $thisKey == 1) {
																		$selected = ' selected="selected" ';
																	}
																	else if($thisKey == $supplierDatas["suppliersBezahlart"]) {
																		$selected = ' selected="selected" ';
																	}
																	echo '
																		<option value="' . $thisKey . '" '.$selected.' >' . ($arrPaymentTypeDatas[$thisKey]["paymentTypesName"]). '</option>
																	';
																}
															}
														?>
													</select>
												</td>
											</tr>
											<tr>
												<td><b>Zahlungskondition:</b></td>
												<td>
													<select name="editSuppliersZahlungskondition" id="editSuppliersZahlungskondition" class="inputSelect_510">
														<option value=""> - </option>
														<?php
															if(!empty($arrPaymentConditionDatas)) {
																foreach($arrPaymentConditionDatas as $thisKey => $thisValue) {
																	$selected = '';
																	if($_REQUEST["editID"] == "NEW" && $thisKey == 1) {
																		$selected = ' selected="selected" ';
																	}
																	else if($thisKey == $supplierDatas["suppliersZahlungskondition"]) {
																		$selected = ' selected="selected" ';
																	}
																	echo '
																		<option value="' . $thisKey . '" '.$selected.' >' . ($arrPaymentConditionDatas[$thisKey]["paymentConditionsName"]). '</option>
																	';
																}
															}
														?>
													</select>
												</td>
											</tr>
											<tr>
												<td style="width:200px;"><b>Kontoinhaber:</b></td>
												<td><input type="text" name="editSuppliersKontoinhaber" id="editSuppliersKontoinhaber" class="inputField_510" value="<?php echo ($supplierDatas["suppliersKontoinhaber"]); ?>" /></td>
											</tr>
											<tr>
												<td style="width:200px;"><b>Bankname:</b></td>
												<td><input type="text" name="editSuppliersBankName" id="editSuppliersBankName" class="inputField_510" value="<?php echo ($supplierDatas["suppliersBankName"]); ?>" /></td>
											</tr>
											<tr>
												<td><b>Bankleitzahl:</b></td>
												<td><input type="text" name="editSuppliersBankLeitzahl" id="editSuppliersBankLeitzahl" class="inputField_510" value="<?php echo ($supplierDatas["suppliersBankLeitzahl"]); ?>" /></td>
											</tr>
											<tr>
												<td><b>Kontonummer:</b></td>
												<td><input type="text" name="editSuppliersBankKontonummer" id="editSuppliersBankKontonummer" class="inputField_510" value="<?php echo ($supplierDatas["suppliersBankKontonummer"]); ?>" /></td>
											</tr>
											<tr>
												<td><b>IBAN:</b></td>
												<td><input type="text" name="editSuppliersBankIBAN" id="editSuppliersBankIBAN" class="inputField_510" value="<?php echo ($supplierDatas["suppliersBankIBAN"]); ?>" /></td>
											</tr>
											<tr>
												<td><b>BIC:</b></td>
												<td><input type="text" name="editSuppliersBankBIC" id="editSuppliersBankBIC" class="inputField_510" value="<?php echo ($supplierDatas["suppliersBankBIC"]); ?>" /></td>
											</tr>
										</table>
									</fieldset>

									<fieldset>
										<legend>Sonstige Daten</legend>
										<table border="0" width="100%" cellspacing="0" cellpadding="0">
											<tr>
												<td style="width:200px;"><b>Standard-Rabatt:</b></td>
												<td>
													<?php
														if($supplierDatas["suppliersRabatt"] == "") {
															$supplierDatas["suppliersRabatt"] = 0;
														}
													?>
													<input type="text" name="editSuppliersRabatt" id="editSuppliersRabatt" class="inputField_40" value="<?php echo number_format($supplierDatas["suppliersRabatt"], 2, ',', ''); ?>" />  (Rabatt auf gesamten Nettobetrag)
												</td>
											</tr>
											<tr>
												<td><b>Artikel-Rabatte verwenden:</b></td>
												<td>
													<?php
														if($supplierDatas["suppliersUseProductDiscount"] == 1){
															$checked = ' checked="checked" ';
														}
														else {
															$checked = '';
														}
													?>
													<input type="checkbox" name="editSuppliersUseProductDiscount" id="editSuppliersUseProductDiscount" value="1" <?php echo $checked; ?> /> ja
												</td>
											</tr>
											<tr>
												<td><b>Artikel-MwSt. verwenden:</b></td>
												<td>
													<?php
														if($supplierDatas["suppliersUseProductMwst"] == 1){
															$checked = ' checked="checked" ';
														}
														else {
															$checked = '';
														}
													?>
													<input type="checkbox" name="editSuppliersUseProductMwst" id="editSuppliersUseProductMwst" value="1" <?php echo $checked; ?> /> ja
												</td>
											</tr>
											<tr>
												<td><b>UstID:</b></td>
												<td><input type="text" name="editSuppliersUstID" id="editSuppliersUstID" class="inputField_100" value="<?php echo $supplierDatas["suppliersUstID"]; ?>" /></td>
											</tr>
											<tr>
												<td><b>Kunden-Gruppe:</b></td>
												<td>
													<div class="inputArea1">
													<?php
														if(!empty($arrSupplierGroupDatas)) {
															$thisArrSupplierGroups = explode(";", stripslashes($supplierDatas["suppliersGruppe"]));
															foreach($arrSupplierGroupDatas as $thisKey => $thisValue) {
																$checked = '';
																if(!empty($thisArrSupplierGroups)) {
																	if(in_array($thisKey, $thisArrSupplierGroups)) {
																		$checked = ' checked ';
																	}
																}
																echo '<span class="checkBoxElement1">';
																echo '<input type="checkbox" name="editSuppliersGruppe[]" value="' . $thisKey . '" '.$checked.'/> ';
																echo $arrSupplierGroupDatas[$thisKey]["supplierGroupsName"];
																echo '</span>';
															}
														}
													?>
													</div>
												</td>
											</tr>
											<tr>
												<td><b>Bemerkungen / Notizen:</b></td>
												<td><textarea name="editSuppliersNotiz" id="editSuppliersNotiz" rows="10" cols="20" class="inputTextarea_510x140" ><?php echo ($supplierDatas["suppliersNotiz"]); ?></textarea></td>
											</tr>
										</table>
									</fieldset>
									<div class="actionButtonsArea">
										<?php if($arrGetUserRights["editSuppliers"]) { ?>
										<input type="submit" class="inputButton1 inputButtonGreen" name="storeDatas" value="Speichern" onclick="return checkFormDutyFields(this.form.name, <?php echo $jsonFormDutyFields; ?>);" />
										<?php if($_REQUEST["editID"] != "NEW") { ?>
										<!--
										&nbsp;
										<input type="submit" class="inputButton1 inputButtonRed" name="deleteDatas" value="Entfernen" onclick="return showWarning(' Wollen Sie diesen Eintrag endgültig entfernen??? ');" />
										-->
										&nbsp;
										<?php } ?>
										<input type="submit" class="inputButton1 inputButtonOrange" name="resetDatas" value="Abbrechen" onclick="return showWarning(' Wollen Sie diesen Vorgang wirklich abbrechen??? ');" />
										<?php } ?>
									</div>
								</form>
							</div>

							<?php if($_REQUEST["editID"] != "NEW") { ?>
							<div id="tabs-2">
									<?php
										$thisTableValue = TABLE_ORDER_LETTERS;
										$sql = "
												SELECT
													`" . $thisTableValue . "`.`orderDocumentsNumber`,

													'" . $thisTableKey . "' AS `tableDocumentType`,

													`" . $thisTableValue . "`.`orderDocumentsID`,
													`" . $thisTableValue . "`.`orderDocumentsType`,
													`" . $thisTableValue . "`.`orderDocumentsOrdersIDs`,
													`" . $thisTableValue . "`.`orderDocumentsCustomerNumber`,
													`" . $thisTableValue . "`.`orderDocumentsCustomersOrderNumber`,
													`" . $thisTableValue . "`.`orderDocumentsProcessingDate`,
													`" . $thisTableValue . "`.`orderDocumentsDeliveryDate`,
													`" . $thisTableValue . "`.`orderDocumentsBindingDate`,
													`" . $thisTableValue . "`.`orderDocumentsSalesman`,
													`" . $thisTableValue . "`.`orderDocumentsAddressCompany`,
													`" . $thisTableValue . "`.`orderDocumentsAddressName`,
													`" . $thisTableValue . "`.`orderDocumentsAddressStreet`,
													`" . $thisTableValue . "`.`orderDocumentsAddressStreetNumber`,
													`" . $thisTableValue . "`.`orderDocumentsAddressZipcode`,
													`" . $thisTableValue . "`.`orderDocumentsAddressCity`,
													`" . $thisTableValue . "`.`orderDocumentsAddressCountry`,
													`" . $thisTableValue . "`.`orderDocumentsAddressMail`,
													`" . $thisTableValue . "`.`orderDocumentsSumPrice`,
													`" . $thisTableValue . "`.`orderDocumentsDiscount`,
													`" . $thisTableValue . "`.`orderDocumentsDiscountType`,
													`" . $thisTableValue . "`.`orderDocumentsDiscountPercent`,
													`" . $thisTableValue . "`.`orderDocumentsMwst`,
													`" . $thisTableValue . "`.`orderDocumentsMwstPrice`,
													`" . $thisTableValue . "`.`orderDocumentsShippingCosts`,
													`" . $thisTableValue . "`.`orderDocumentsPackagingCosts`,
													`" . $thisTableValue . "`.`orderDocumentsCashOnDelivery`,
													`" . $thisTableValue . "`.`orderDocumentsTotalPrice`,
													`" . $thisTableValue . "`.`orderDocumentsBankAccount`,
													`" . $thisTableValue . "`.`orderDocumentsPaymentCondition`,
													`" . $thisTableValue . "`.`orderDocumentsPaymentType`,
													`" . $thisTableValue . "`.`orderDocumentsDeliveryType`,
													`" . $thisTableValue . "`.`orderDocumentsDeliveryTypeNumber`,
													`" . $thisTableValue . "`.`orderDocumentsSubject`,
													`" . $thisTableValue . "`.`orderDocumentsContent`,
													`" . $thisTableValue . "`.`orderDocumentsDocumentPath`,
													`" . $thisTableValue . "`.`orderDocumentsTimestamp`,
													`" . $thisTableValue . "`.`orderDocumentsStatus`,
													`" . $thisTableValue . "`.`orderDocumentsSendMail`,

													`" . $thisTableValue . "`.`orderDocumentsIsCollectiveInvoice`

													,`" . $thisTableValue . "Details`.`orderDocumentDetailKommission`

												FROM `" . $thisTableValue . "`

												LEFT JOIN `" . $thisTableValue . "Details`
												ON(`" . $thisTableValue . "`.`orderDocumentsID` = `" . $thisTableValue . "Details`.`orderDocumentDetailDocumentID`)

												WHERE 1
													AND `" . $thisTableValue . "`.`orderDocumentsCustomerNumber` = '".$supplierDatas["suppliersKundennummer"]."'

													GROUP BY `" . $thisTableValue . "`.`orderDocumentsNumber`
											";
										#dd('sql');
										$rs = $dbConnection->db_query($sql);
										$countRows = $dbConnection->db_getMysqlNumRows($rs);

										if($countRows > 0){
											while($ds = mysqli_fetch_assoc($rs)){
												foreach(array_keys($ds) as $field) {
													$arrDocumentDatas[$ds["orderDocumentsType"]][$ds["orderDocumentsID"]][$field] = $ds[$field];
												}
											}
										}
										else {
											$errorMessage .= ' Es wurden keine Briefe gefunden. ' . '<br />';
										}
										displayMessages();
										#dd('arrDocumentDatas');
									?>

								<fieldset style="width:90% !important;">
									<legend>Dokumente</legend>
									<?php if(!empty($arrDocumentDatas)) {
										foreach($arrDocumentDatas as $thisDocumentKey => $thisDocumentValue){
											if($thisDocumentKey == 'BR') { $thisDocumentUrl = PAGE_DISPLAY_LETTER; }
											else if($thisDocumentKey == 'RE') { $thisDocumentUrl = PAGE_DISPLAY_INVOICE; }
											else if($thisDocumentKey == 'AN') { $thisDocumentUrl = PAGE_DISPLAY_OFFER; }
											else if($thisDocumentKey == 'MA') { $thisDocumentUrl = PAGE_DISPLAY_REMINDER; }
											else if($thisDocumentKey == 'AB') { $thisDocumentUrl = PAGE_DISPLAY_CONFIRMATION; }
											else if($thisDocumentKey == 'LS') { $thisDocumentUrl = PAGE_DISPLAY_DELIVERY; }
											else if($thisDocumentKey == 'GU') { $thisDocumentUrl = PAGE_DISPLAY_CREDIT; }

											echo '<h2>' . $arrDocumentTypeDatas[$thisDocumentKey]["createdDocumentsTypesName2"] . ' (' . $thisDocumentKey . ')</h2>';

											echo '<div class="displayDocumentsArea" >';
											echo '<table border="0" cellpadding="0" cellspacing="0" style="width:99%;" class="displayOrders">';
												echo '<tr>';
												echo '<th style="width:30px !important;text-align:right;">#</th>';
												echo '<th style="width:66px !important;">Datum</th>';
												#echo '<th style="width:45px;">Kund.Nr</th>';
												echo '<th>Dokument-Empf&auml;ngername</th>';
												echo '<th style="width:110px !important;">Dokument &auml;ndern</th>';

												if($thisDocumentKey != "BR"){
													echo '<th style="width:50px !important;">Summe</th>';
													#echo '<th style="width:200px !important;">Dokument umwandeln / kopieren</th>';
													echo '<th style="width:110px !important;">Dokument umwandeln</th>';
												}
												else {
													#echo '<th style="width:70px !important;">&nbsp;</th>';
													#echo '<th style="width:90px !important;">&nbsp;</th>';
												}
												#echo '<th style="width:100px !important;">Zahl-Status</th>';
												echo '<th style="width:60px !important;">Info</th>';
												echo '</tr>';

												$count = 0;
												foreach($thisDocumentValue as $thisKey => $thisValue){
													if($count%2 == 0){ $rowClass = 'row0'; }
													else { $rowClass = 'row1'; }
													$rowClass2 = '';
													if($thisDocumentValue[$thisKey]["orderDocumentsStatus"] == "1") {
														$rowClass2 = 'row2';
													}
													else if($thisDocumentValue[$thisKey]["orderDocumentsStatus"] == "2") {
														$rowClass2 = 'row3';
													}
													else if($thisDocumentValue[$thisKey]["orderDocumentsStatus"] == "3") {
														$rowClass2 = 'row6';
														$rowClass = 'row6';
													}
													else if($thisDocumentValue[$thisKey]["orderDocumentsStatus"] == "4") {
														$rowClass2 = 'row2';
													}
													else if($thisDocumentValue[$thisKey]["orderDocumentsStatus"] == "6") {
														$rowClass2 = 'row6';
													}
													else if($thisDocumentValue[$thisKey]["orderDocumentsStatus"] == "6") {
														$rowClass2 = 'row7';
													}
													else if($thisDocumentValue[$thisKey]["orderDocumentsStatus"] == "10") {
														$rowClass2 = 'row3';
													}
													else if($thisDocumentValue[$thisKey]["orderDocumentsStatus"] == "5") {
														// $rowClass2 = 'row5';
														$rowClass2 = 'row3';
													}

													echo '<tr class="'.$rowClass.'">';

													echo '<td style="text-align:right;"><b>' . ($count + 1). '.</b></td>';

													echo '<td>' . formatDate($thisDocumentValue[$thisKey]["orderDocumentsProcessingDate"], 'display') . '</td>';

													#echo '<td><a href="' . PAGE_EDIT_CUSTOMER . '?editCustomerNumber=' . $thisDocumentValue[$thisKey]["orderDocumentsCustomerNumber"] . '" title="Kunden anzeigen">' . $thisDocumentValue[$thisKey]["orderDocumentsCustomerNumber"] . '</a></td>';

													echo '<td>';
													if($thisDocumentValue[$thisKey]["orderDocumentsIsCollectiveInvoice"] == '1'){
														echo '<span style="color:#FF0000;font-weight:bold;font-size:10px;">';
														if($thisDocumentKey == 'AB'){ echo 'Die AB ist Teil einer SAMMELRECHNUNG'; }
														else { echo 'SAMMELRECHNUNG'; }
														echo '</span><br />';
													}
													echo $thisDocumentValue[$thisKey]["orderDocumentsAddressCompany"];
													if($thisDocumentValue[$thisKey]["orderDocumentDetailKommission"] != ""){
														#echo '<br />xxx<span class="remarksArea">';
														#echo '<b>Komission</b>:' . $thisDocumentValue[$thisKey]["orderDocumentDetailKommission"];
														#echo '</span>';
													}
													#dd('arrConnectedDocumentIds');
													$thisDocumentNumber = $thisDocumentValue[$thisKey]["orderDocumentsNumber"];
													#dd('thisDocumentNumber');
													if($arrConnectedDocumentIds[$thisDocumentNumber]["orderDocumentsAddressCompany"] != ""){
														echo $arrConnectedDocumentIds[$thisDocumentNumber]["orderDocumentsAddressCompany"];
													}
													if($arrConnectedDocumentIds[$thisDocumentNumber]["documentsKommission"] != ""){
														echo '<br /><span class="remarksArea">';
														echo '<b>Komission</b>:' . $arrConnectedDocumentIds[$thisDocumentNumber]["documentsKommission"];
														echo '</span>';
													}
													if($arrConnectedDocumentIds[$thisDocumentNumber]["documentsPrintText"] != ""){
														echo '<br /><span class="remarksArea">';
														echo '<b>Aufdruck</b>:' . $arrConnectedDocumentIds[$thisDocumentNumber]["documentsPrintText"];
														echo '</span>';
													}
													echo '</td>';

													echo '<td>';
														echo '<form name="formSubmitEditDocument" method="post" action="' . PAGE_WRITE_LETTER . '?customersSource=suppliers">';
														echo '<table width="100" cellpadding="0" cellspacing="0" class="noBorder">';
														echo '<tr>';
															echo '<td>';
																echo $thisDocumentValue[$thisKey]["orderDocumentsNumber"];
															echo '</td>';

															echo '<td>';
																echo '<input type="hidden" name="customersSource" value="suppliers" />';
																echo '<input type="hidden" name="editID" value="' . $thisDocumentValue[$thisKey]["orderDocumentsCustomerNumber"] . '" />';
																echo '<input type="hidden" name="originDocumentType" value="' . $thisDocumentKey . '" />';
																echo '<input type="hidden" name="originDocumentID" value="' . $thisKey . '" />';
																echo '<input type="hidden" name="originDocumentNumber" value="' . $thisDocumentValue[$thisKey]["orderDocumentsNumber"] . '" />';
																echo '<input type="hidden" name="editDocType" value="' . $thisDocumentKey . '" />';
																echo '<input type="image" src="layout/icons/iconEdit.gif" title="' . $arrDocumentTypeDatas[$thisDocumentKey]["createdDocumentsTypesName"] . ' ' . $thisDocumentValue[$thisKey]["orderDocumentsDocumentNumber"] . ' &auml;ndern" />';
															echo '</td>';
														echo '</tr>';
														echo '</table>';
														echo '</form>';
													echo '</td>';

													#if($thisDocumentKey != "BR"){
													## if($thisDocumentValue[$thisKey]["orderDocumentsIsCollectiveInvoice"] != '1' && $thisDocumentKey != "BR"){
														#echo '<td style="text-align:right;white-space:nowrap;">' . convertDecimal($thisDocumentValue[$thisKey]["orderDocumentsTotalPrice"], 'display') . ' &euro;</td>';
														#echo '<td>';
														#echo $thisDocumentValue[$thisKey]["orderDocumentsNumber"];
														#echo '<a href="' . $thisDocumentUrl . '?searchDocumentNumber=' . $thisDocumentValue[$thisKey]["orderDocumentsNumber"] . '">';
														#echo '<img src="layout/icons/iconEdit.gif" width="16" height="16" title="Dokument kopieren oder umwandeln" alt="Dokument kopieren oder umwandeln" />';
														#echo '</a>';
														#echo '</td>';
													#}
													#else {
														#echo '<td>';
														#echo '</td>';
														#echo '<td>';
														#echo '</td>';
													#}
													#echo '<td class="'.$rowClass2.'">';
													##echo $thisDocumentValue[$thisKey]["orderDocumentsStatus"];
													#echo $arrPaymentStatusTypeDatas[$thisDocumentValue[$thisKey]["orderDocumentsStatus"]]["paymentStatusTypesName"];
													#echo '</td>';

													echo '<td>';
													echo '<span class="toolItem">';
													#echo 'xxx<a href="' . $_SERVER["PHP_SELF"] . '?editCustomerNumber=' . $thisDocumentValue[$thisKey]["orderDocumentsCustomerNumber"] . '&editID=' . $_REQUEST["editID"] . '&amp;downloadFile=' . urlencode(basename($thisDocumentValue[$thisKey]["orderDocumentsDocumentPath"])) . '&amp;documentType=' . $thisDocumentKey . '">' . '<img src="layout/icons/icon' . getFileType(basename($thisDocumentValue[$thisKey]["orderDocumentsDocumentPath"])) . '.gif" width="16" height="16" title="Dokument herunterladen" alt="Download" /></a>';
													echo '<a href="' . $_SERVER["PHP_SELF"] . '?editCustomerNumber=' . $customerDatas["customersKundennummer"] . '&amp;editID=' . $_REQUEST["editID"] . '&amp;downloadFile=' . urlencode(basename($thisDocumentValue[$thisKey]["orderDocumentsDocumentPath"])) . '&amp;documentType=' . $thisDocumentKey . '">' . '<img src="layout/icons/icon' . getFileType(basename($thisDocumentValue[$thisKey]["orderDocumentsDocumentPath"])) . '.gif" width="16" height="16" title="Dokument herunterladen" alt="Download" /></a>';
													echo '</span>';
													echo '<span class="toolItem">';
													echo '<img src="layout/icons/iconEye.gif" class="buttonLoadOrderDetails" width="16" height="16" title="Details ansehen" alt="' . $thisDocumentKey . '#' .  $arrDocumentTypeDatas[$thisDocumentKey]["createdDocumentsTypesName2"] . '#' . $thisKey . '" />';
													echo '</span>';
													echo '<span class="toolItem">';
													echo '<img src="layout/icons/mailAttachement.png" class="buttonSendDocument" rel="' . urlencode(basename($thisDocumentValue[$thisKey]["orderDocumentsDocumentPath"])) . '#' . $thisDocumentKey . '" width="16" height="16" title="Dokument direkt per Mail versenden" alt="Dokument versenden" />';
													echo '</span>';

													echo '</td>';
													echo '</tr>';
													$count++;
												}

												echo '</table>';
											echo '</div>';
											echo '<hr />';
										}
									?>
									<?php } else {
										$infoMessage .= 'Es sind keine Dokumente vorhanden.' . '<br />';
										displayMessages();
									}
									?>
								</fieldset>
							</div>
							<?php } ?>


							<?php if($_REQUEST["editID"] != "NEW") { ?>
							<div id="tabs-3">
								<fieldset style="width:90% !important;">
									<legend>Dokumente</legend>
									<h2>Rechnungserfassung</h2>

									<?php
										$sql_getSupplierInvoices = "
													SELECT

														`supplierInvoicesID`,
														`supplierInvoicesDocumentDate`,
														`supplierInvoicesDocumentNumber`,
														`supplierInvoicesTotal`,
														`supplierInvoicesSupplierNumber`,
														`supplierInvoicesSupplierName`,
														`supplierInvoicesPaymentStatus`,
														`supplierInvoicesNotice`

														FROM `" . TABLE_SUPPLIER_INVOICES . "`

														WHERE 1
															AND `supplierInvoicesSupplierNumber` = '" . $supplierDatas["suppliersKundennummer"] . "'
											";
										dd('sql_getSupplierInvoices');
										$rs_getSupplierInvoices = $dbConnection->db_query($sql_getSupplierInvoices);
										$countRows = $dbConnection->db_getMysqlNumRows($rs_getSupplierInvoices);

										if($countRows > 0){
											while($ds_getSupplierInvoices = mysqli_fetch_assoc($rs_getSupplierInvoices)){
												foreach(array_keys($ds_getSupplierInvoices) as $field) {
													$arrDocumentDatas[$ds["orderDocumentsType"]][$ds["orderDocumentsID"]][$field] = $ds[$field];
												}
											}
										}
										else {
											$errorMessage .= ' Es wurden keine Lieferanten-Rechnungen gefunden gefunden. ' . '<br />';
										}
										displayMessages();
									?>
								</fieldset>
							</div>
							<?php } ?>
						</div>
					</div>
					<?php
						}
					?>
				</div>
			</div>
		</div>
		<div class="clear"></div>
	</div>
</div>

<script language="javascript" type="text/javascript">
	$(document).ready(function() {
		$('#searchSupplierNumber').keyup(function () {
			loadSuggestions('searchSupplierNumber', [{'triggerElement': '#searchSupplierNumber', 'fieldNumber': '#searchSupplierNumber', 'fieldName': '#searchSupplierName', 'fieldCity': '', 'fieldZipCode': ''}], 1);
		});
		$('#searchSupplierName').keyup(function () {
			loadSuggestions('searchSupplierName', [{'triggerElement': '#searchSupplierName', 'fieldNumber': '#searchSupplierNumber', 'fieldName': '#searchSupplierName', 'fieldCity': '', 'fieldZipCode': ''}], 1);
		});

		$('#editSuppliersFirmenadressePLZ').keyup(function () {
			loadSuggestions('searchPlzCity', [{'triggerElement': '#editSuppliersFirmenadressePLZ', 'fieldCity': '#editSuppliersFirmenadresseOrt', 'fieldZipCode': '#editSuppliersFirmenadressePLZ'}], 1);
		});
		$('#editSuppliersFirmenadresseOrt').keyup(function () {
			loadSuggestions('searchCityPlz', [{'triggerElement': '#editSuppliersFirmenadresseOrt', 'fieldCity': '#editSuppliersFirmenadresseOrt', 'fieldZipCode': '#editSuppliersFirmenadressePLZ'}], 1);
		});

		$('#searchPLZ').keyup(function () {
			loadSuggestions('searchPLZ', [{'triggerElement': '#searchPLZ', 'fieldZipCode': '#searchPLZ'}], 1);
		});


		$('#editSuppliersBankName').keyup(function(){
			loadSuggestions('searchBankName', [{'triggerElement': '#editSuppliersBankName', 'fieldCode': '#editSuppliersBankLeitzahl', 'fieldName': '#editSuppliersBankName', 'fieldBIC': '#editSuppliersBankBIC', 'fieldIBAN': '#editSuppliersBankIBAN'}], 1);
		});
		$('#editSuppliersBankLeitzahl').keyup(function(){
			loadSuggestions('searchBankCode', [{'triggerElement': '#editSuppliersBankLeitzahl', 'fieldCode': '#editSuppliersBankLeitzahl', 'fieldName': '#editSuppliersBankName', 'fieldBIC': '#editSuppliersBankBIC', 'fieldIBAN': '#editSuppliersBankIBAN'}], 1);
		});


		$(function() {
			$('#tabs').tabs();
		});
		colorRowMouseOver('.displayOrders tbody tr');
		$('.buttonNotice').click(function () {
			loadNotice($(this));
		});
		setFormDutyFields(<?php echo $jsonFormDutyFields; ?>);
	});
</script>

<?php require_once('inc/footerHTML.inc.php'); ?>