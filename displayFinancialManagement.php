<?php
	require_once('inc/requires.inc.php');

	$arrQueryVars = getUrlQueryString($_SERVER["REQUEST_URI"]);
	foreach($arrQueryVars as $thisKey => $thisValue) {
		$_REQUEST[$thisKey] = trim($thisValue);
	}
	if($_REQUEST["statusType"] == "") {
		header('location: ' . PAGE_EXIT_LOCATION);
		exit;
	}
	else if(!$arrGetUserRights["editInvoices"]) {
		header('location: ' . PAGE_EXIT_LOCATION);
		exit;
	}

	$showMenuePages = false;

	$jswindowMessage = "";
	$warningMessage = '';
	$errorMessage = '';
	$successMessage = '';
	$infoMessage = '';

	$dbConnection = new DB_Connection();
	$db_open = $dbConnection->db_connect();

	// BOF READ DOCUMENT TYPE DATAS
		$arrDocumentTypeDatas = getDocumentTypes();
	// EOF READ DOCUMENT TYPE DATAS

	// BOF READ PAYMENT STATUS TYPE DATAS
		$arrPaymentStatusTypeDatas = getPaymentStatusTypes();
	// EOF READ PAYMENT STATUS TYPE DATAS

	// BOF READ PAYMENT TYPES
		$arrPaymentTypeDatas = getPaymentTypes();
	// EOF READ PAYMENT TYPES

	// BOF READ PAYMENT CONDITIONS
		$arrPaymentConditionDatas = getPaymentConditions();
	// EOF READ PAYMENT CONDITIONS

	// BOF READ BANK TYPES
		$arrBankAccountTypeDatas = getBankAccountTypes('all');
		$arrBankAccountTypeDatasActive = getBankAccountTypes();
	// EOF READ BANK TYPES

	// BOF READ COUNTRIES
		$arrCountryTypeDatas = getCountryTypes();
	// EOF READ COUNTRIES

	// BOF GET USER DATAS
		$userDatas = getUserDatas();
	// EOF GET USER DATAS

	// BOF GET MANDATORY DATAS
		$arrMandatoriesDatas = getMandatories();
		$arrMandatoriesCustomerNumbers = array();
		if(!empty($arrMandatoriesDatas)){
			foreach($arrMandatoriesDatas as $thisMandatoriesDatas){
				$arrMandatoriesCustomerNumbers[] = $thisMandatoriesDatas["mandatoriesCustomerNumber"];
			}
		}
	// EOF GET MANDATORY DATAS

	// BOF UPDATE STATUS SPERRKONTO
		if($_REQUEST["searchSperrbetragDocumentNumber"] != ""){
			#$_REQUEST["searchDocumentNumber"] = $_REQUEST["searchSperrbetragDocumentNumber"];
		}

		if($_REQUEST["searchSperrbetragCustomerNumber"] != ""){
			#$_REQUEST["searchCustomerNumber"] = $_REQUEST["searchSperrbetragCustomerNumber"];
		}

		if($_REQUEST["updateSperrbetragDocumentNumber"] != ""){
			$_REQUEST["searchDocumentNumber"] = $_REQUEST["updateSperrbetragDocumentNumber"];
			$thisUpdateSperrbetragValue = $_REQUEST["updateSperrbetragValue"];
			$thisUpdateSperrbetragDocumentType = substr($_REQUEST["updateSperrbetragDocumentNumber"], 0, 2);
			if($thisUpdateSperrbetragValue == ''){
				$thisUpdateSperrbetragValue = '0';
			}
			$sql_updateSperrbetragDocumentNumber = "
					UPDATE `" . TABLE_RELATED_DOCUMENTS . "`
						SET `relatedDocuments_SperrbetragFAKT` = '" . $thisUpdateSperrbetragValue . "'
						WHERE 1
							AND `relatedDocuments_" . $thisUpdateSperrbetragDocumentType . "` = '" . $_REQUEST["updateSperrbetragDocumentNumber"] . "'
				";
			$rs_updateSperrbetragDocumentNumber = $dbConnection->db_query($sql_updateSperrbetragDocumentNumber);

			if($rs_updateSperrbetragDocumentNumber) {
				if($thisUpdateSperrbetragValue == '1'){
					$successMessage .= ' Der Sperrbetrag ' . FAKT_BLOCKED_AMOUNT . '% f&uuml;r &quot;' . $_REQUEST["updateSperrbetragDocumentNumber"] . '&quot; wurde gesetzt.' .'<br />';
				}
				else {
					$successMessage .= ' Der Sperrbetrag f&uuml;r &quot;' . $_REQUEST["updateSperrbetragDocumentNumber"] . '&quot; wurde entfernt.' .'<br />';
				}
			}
			else {
				$errorMessage .= 'Der Sperrbetrag (&quot;' . $_REQUEST["updateSperrbetragDocumentNumber"] . '&quot;) konnte nicht gesetzt werden.' . '<br />';
			}
		}
	// EOF UPDATE STATUS SPERRKONTO

	// BOF DEFINE CONVERT URLS
		$arrDocumentUrl["AN"] = PAGE_DISPLAY_OFFER;
		$arrDocumentUrl["AB"] = PAGE_DISPLAY_CONFIRMATION;
		$arrDocumentUrl["LS"] = PAGE_DISPLAY_DELIVERY;
		$arrDocumentUrl["RE"] = PAGE_DISPLAY_INVOICE;
		$arrDocumentUrl["GU"] = PAGE_DISPLAY_CREDIT;
		$arrDocumentUrl["MA"] = PAGE_DISPLAY_REMINDER;
		$arrDocumentUrl["M1"] = PAGE_DISPLAY_FIRST_DEMAND;
		$arrDocumentUrl["M2"] = PAGE_DISPLAY_SECOND_DEMAND;
		$arrDocumentUrl["M3"] = PAGE_DISPLAY_THIRD_DEMAND;
		$arrDocumentUrl["BR"] = PAGE_DISPLAY_LETTER;
	// EOF DEFINE CONVERT URLS

	if($arrQueryVars["downloadFile"] != "") {
		require_once('classes/downloadFile.class.php');
		$thisDownloadFile = $arrQueryVars["downloadFile"];
		if(!file_exists(DIRECTORY_CREATED_DOCUMENTS_CUSTOMERS . $thisDownloadFile)){
			$thisDownloadFile = searchDownloadFile(DIRECTORY_CREATED_DOCUMENTS_CUSTOMERS . $thisDownloadFile, $arrQueryVars["thisDocumentType"]);
		}
		$thisDownload = new downloadFile(DIRECTORY_CREATED_DOCUMENTS_CUSTOMERS, $thisDownloadFile);
		$errorMessage .= $thisDownload->startDownload();
	}

	// BOF DEFINE TABLES
		if($_REQUEST["documentType"] == 'RE'){
			DEFINE('TABLE_ORDER_THIS_PAYMENTS', constant("TABLE_ORDER_INVOICE_PAYMENTS"));
		}
		else if($_REQUEST["documentType"] == 'AB'){
			DEFINE('TABLE_ORDER_THIS_PAYMENTS', constant("TABLE_ORDER_CONFIRMATION_PAYMENTS"));
		}
		else{
			DEFINE('TABLE_ORDER_THIS_PAYMENTS', '');
		}
		DEFINE('TABLE_ORDER_THIS', constant('TABLE_ORDER_' . $_REQUEST["documentType"]));
		DEFINE('TABLE_ORDER_THIS_DETAILS', constant('TABLE_ORDER_' . $_REQUEST["documentType"].'_DETAILS'));

		$thisDocumentTypeRelated = '';
		$userDatas = getUserDatas();
		if($_REQUEST["statusType"] != 'MA' && $_REQUEST["statusType"] != 'M1' && $_REQUEST["statusType"] != 'M2'){
			if($_REQUEST["documentType"] == 'RE'){
				$thisDocumentTypeRelated = 'AB';
			}
			else if($_REQUEST["documentType"] == 'AB'){
				$thisDocumentTypeRelated = 'RE';
			}
			else if($_REQUEST["documentType"] == 'GU'){
				$thisDocumentTypeRelated = 'RE';
			}

			if($thisDocumentTypeRelated != ''){
				DEFINE('RELATED_TABLE_ORDER_THIS', constant('TABLE_ORDER_' . $thisDocumentTypeRelated));
				DEFINE('RELATED_TABLE_ORDER_THIS_DETAILS', constant('TABLE_ORDER_' . $thisDocumentTypeRelated.'_DETAILS'));
				DEFINE('RELATED_TABLE_ORDER_THIS_PAYMENTS', constant('TABLE_ORDER_' . $thisDocumentTypeRelated.'_PAYMENTS'));
			}
		}
	// EOF DEFINE TABLES

	// BOF SEND MAIL WITH ATTACHED DOCUMENT
		if($_POST["sendAttachedDocument"] == '1') {
			$thisCreatedPdfName = $_POST["mailDocumentFilename"];
			$_POST["selectSubject"] = $arrDocumentTypeDatas[$_REQUEST["documentType"]]["createdDocumentsTypesName"] . ' Nr.: ' . '{###DOCUMENT_NUMBER###}';
			$generatedDocumentNumber = substr($_POST["mailDocumentFilename"], 0, 13);

			require_once(DIRECTORY_HTML_MIME_MAIL . 'htmlMimeMail.php');
			require_once(DIRECTORY_MAIL_TEMPLATES . 'template_BASIC.inc.php');
			require_once(DIRECTORY_MAIL_TEMPLATES . 'template_TEXT_DATAS.inc.php');
			##require_once("inc/mail.inc.php");

			// BOF SEND MAIL
			require_once("classes/createMail.class.php");

			// BOF CREATE SUBJECT
			$thisSubject = $_POST["selectSubject"];
			if($_POST["sendAttachedMailSubject"] != '') {
				$thisSubject = $_POST["sendAttachedMailSubject"] . ', ' . $thisSubject;
			}
			// EOF CREATE SUBJECT

			// BOF GET ATTACHED FILE
			if($_REQUEST["documentType"] == 'KA') {
				$pathDocumentFolder = DIRECTORY_UPLOAD_FILES;
				$thisCreatedPdfName = utf8_decode($thisCreatedPdfName);
			}
			else if($_REQUEST["documentType"] == 'EX') {
				$pathDocumentFolder = DIRECTORY_EXPORT_FILES;
				$thisCreatedPdfName = utf8_decode($thisCreatedPdfName);
			}
			else if($_REQUEST["documentType"] == 'SX') {
				$pathDocumentFolder = DIRECTORY_CREATED_DOCUMENTS_SALESMEN;
				$thisCreatedPdfName = utf8_decode($thisCreatedPdfName);
			}
			else {
				$pathDocumentFolder = DIRECTORY_CREATED_DOCUMENTS_CUSTOMERS;
			}

			$arrAttachmentFiles = array (
				rawurldecode($pathDocumentFolder . $thisCreatedPdfName) => str_replace(" ", "_", rawurldecode(myUnHtmlEntities($thisCreatedPdfName)))
			);
			// EOF GET ATTACHED FILE

			$createMail = new createMail(
								$thisSubject,													// TEXT:	MAIL_SUBJECT
								$_POST["documentType"],											// STRING:	DOCUMENT TYPE
								$generatedDocumentNumber,										// STRING:	DOCUMENT NUMBER
								$arrAttachmentFiles,											// ARRAY:	ATTACHMENT FILES
								$_POST["selectCustomersRecipientMail"],	 										// STRING:	RECIPIENTS
								$arrMailContentDatas[$_POST["selectMailtextTemplates"]],		// MAIL_TEXT_TEMPLATE
								$mailTemplate["html"],											// MAIL_BODY_TEMPLATE
								'',																// STRING:	ADDITIONAL TEXT
								$_POST["sendAttachedMailText"],									// STRING:	SEND ATTACHED TEXT
								true,															// BOOLEAN:	SEND ATTACHED DOCUMENT
								DIRECTORY_MAIL_IMAGES,											// DIRECTORY_MAIL_IMAGES
								$_POST["selectMailtextSender"]									// STRING:	SENDER
							);

			$createMailResult = $createMail->returnResult();
			$thisErr = $createMail->returnErrors();

			$sendMailToClient = $createMailResult;

			#if(!$sendMailToUs) { $errorMessage .= ' Die Mail-Kopie konnte nicht an ' . implode(', ', $arrOrderMail) . ' versendet werden! '.'<br />'; }
			#else { $successMessage .= ' Die Mail-Kopie wurde an ' . implode(', ', $arrOrderMail) . ' versendet! '.'<br />'; }

			if(!$sendMailToClient) { $errorMessage .= ' Die Mail konnte nicht an ' . $_POST["selectCustomersRecipientName"] . ' (' . $_POST["selectCustomersRecipientMail"] . ') versendet werden! '.'<br />'; }
			else {
				$successMessage .= ' Die Mail wurde an ' . $_POST["selectCustomersRecipientName"] . ' (' . $_POST["selectCustomersRecipientMail"] . ') versendet! '.'<br />';
			}
		}
	// EOF SEND MAIL WITH ATTACHED DOCUMENT



	// BOF GET RELATED DOCUMENTS
		if(trim($_REQUEST["searchDocumentNumber"]) != '' && $_REQUEST["documentType"] == "RE"){
			if(preg_match("/[AB|MA|M1|M2|M3]-[0-9]{10}/", $_REQUEST["searchDocumentNumber"])){
				$arrRelatedDocuments = getRelatedDocuments(array($_REQUEST["searchDocumentNumber"]), '');
				if(!empty($arrRelatedDocuments)){
					$_REQUEST["searchDocumentNumber"] = $arrRelatedDocuments["RE"];
				}
			}
		}
	// EOF GET RELATED DOCUMENTS

	// BOF GET INVOICES STATUS DATAS DEPENDING ON STATUS
		$sql = "SELECT
					`paymentStatusTypesID`
					FROM
					`" . TABLE_PAYMENT_STATUS_TYPES . "`
					WHERE 1
						AND `" . TABLE_PAYMENT_STATUS_TYPES . "`.`paymentStatusTypesShortName` = '" . $_REQUEST["statusType"] . "'
		";
		$rs = $dbConnection->db_query($sql);
		list($thisPaymentStatusID) = mysqli_fetch_array($rs);
	// EOF GET INVOICES STATUS DATAS DEPENDING ON STATUS

	// BOF DELETE PAYMENT ENTRY
		if($arrQueryVars["deleteOrderPaymentID"] != "" && $arrQueryVars["searchDocumentNumber"] != "" && $arrQueryVars["deleteDocumentType"] != ""){
			$sql_deletePayment = "
				DELETE
					FROM `" . constant("TABLE_ORDER_" . $arrQueryVars["deleteDocumentType"] . "_PAYMENTS") . "`
					WHERE 1
						AND `orderPaymentID` = '" . $arrQueryVars["deleteOrderPaymentID"] . "'
						AND `orderPaymentOrderNumber` = '" . $arrQueryVars["searchDocumentNumber"] . "'
			";
			$rs_deletePayment = $dbConnection->db_query($sql_deletePayment);

			if($rs_deletePayment) {
				$successMessage .= ' Der Zahlungseingang (&quot;' . $arrQueryVars["searchDocumentNumber"] . '&quot;) wurde entfernt.' .'<br />';
			}
			else {
				$errorMessage .= 'Der Zahlungseingang (&quot;' . $arrQueryVars["searchDocumentNumber"] . '&quot;) konnte nicht entfernt werden.' . '<br />';
			}

			$warningMessage .= 'Durch Entfernen des Zahlungseingangs wird nicht der Zahlstatus (&quot;' . $arrQueryVars["searchDocumentNumber"] . '&quot;) sowie der zugeh&ouml;rigen Dokumente ge&auml;ndert!' . '<br />';
		}
	// EOF DELETE PAYMENT ENTRY

	// BOF SET INVOICE STATUS AND INSERT PAYMENT VALUE
		if($_POST["submitSetInvoicePaymentStatus"] != '' && $_POST["setInvoicePaymentStatus"] != '') {
			$sql = "UPDATE
						`" . TABLE_ORDER_THIS . "`
						SET
							`" . TABLE_ORDER_THIS . "`.`orderDocumentsStatus` = '" . $_POST["selectInvoicePaymentStatus"] . "',
							`" . TABLE_ORDER_THIS . "`.`orderDocumentsStatusChangeDate` = NOW()
						WHERE `" . TABLE_ORDER_THIS . "`.`orderDocumentsID` = '" . $_POST["documentID"] . "'
							AND `" . TABLE_ORDER_THIS . "`.`orderDocumentsNumber` = '" . $_POST["documentNumber"] . "'

			";

			$rs = $dbConnection->db_query($sql);
			if($rs) {
				$successMessage .= ' Der Status (&quot;' . $_POST["documentNumber"] . '&quot;) wurde gespeichert.' .'<br />';
			}
			else {
				$errorMessage .= ' Der Status (&quot;' . $_POST["documentNumber"] . '&quot;) konnte nicht gespeichert werden.' . '<br />';
			}

			// BOF SET STATUS OF RELATED DOCUMENT
			// if($_REQUEST["documentType"] == 'RE' || $_REQUEST["documentType"] == 'MA' || $_REQUEST["documentType"] == 'M1' || $_REQUEST["documentType"] == 'M2' || $_REQUEST["documentType"] == 'M3' || $_REQUEST["documentType"] == 'IK') {
			if(in_array($_REQUEST["documentType"], array('AB', 'LS', 'RE', 'MA', 'M1', 'M2', 'M3', 'IK'))) {
				$arrRelatedDocuments = getRelatedDocuments(array($_POST["documentNumber"]));
				if(!empty($arrRelatedDocuments)){
					foreach($arrRelatedDocuments as $thisRelatedDocument){
						$thisRelatedDocumentType = substr($thisRelatedDocument, 0, 2);
						// if($thisRelatedDocumentType != $_REQUEST["documentType"] && ($thisRelatedDocumentType == 'RE' || $thisRelatedDocumentType == 'MA' || $thisRelatedDocumentType == 'M1' || $thisRelatedDocumentType == 'M2' || $thisRelatedDocumentType == 'M3' || $thisRelatedDocumentType == 'IK')){
						// if($thisRelatedDocumentType != $_REQUEST["documentType"] && (in_array($thisRelatedDocumentType, array('RE', 'MA', 'M1', 'M2', 'M3', 'IK')))){
						// NEU mit AB:
						if($thisRelatedDocumentType != $_REQUEST["documentType"] && (in_array($thisRelatedDocumentType, array('AB', 'LS', 'RE', 'MA', 'M1', 'M2', 'M3', 'IK')))){
							$sql = "UPDATE
										`" . constant('TABLE_ORDER_' . $thisRelatedDocumentType) . "`
										SET
											`" . constant('TABLE_ORDER_' . $thisRelatedDocumentType) . "`.`orderDocumentsStatus` = '" . $_POST["selectInvoicePaymentStatus"] . "',
											`" . constant('TABLE_ORDER_' . $thisRelatedDocumentType) . "`.`orderDocumentsStatusChangeDate` = NOW()
										WHERE `" . constant('TABLE_ORDER_' . $thisRelatedDocumentType) . "`.`orderDocumentsNumber` = '" . $thisRelatedDocument . "'

								";

							// BOF HANDLE MULTIPLE LS
							if($thisRelatedDocumentType == 'LS'){
								$arrRelatedDocumentsDelivery = explode(';', $thisRelatedDocument);

								if(!empty($arrRelatedDocumentsDelivery)){
									$whereADD = "";
									foreach($arrRelatedDocumentsDelivery as $thisRelatedDocumentsDelivery){
										$whereADD .= "
												OR `" . constant('TABLE_ORDER_' . $thisRelatedDocumentType) . "`.`orderDocumentsNumber` = '" . $thisRelatedDocumentsDelivery . "'
											";
									}
									$sql .= $whereADD;
								}

							}
							// EOF HANDLE MULTIPLE LS

							$rs = $dbConnection->db_query($sql);
							if($rs) {
								$successMessage .= ' Der Status (&quot;' . $thisRelatedDocument . '&quot;) wurde gespeichert.' .'<br />';
							}
							else {
								$errorMessage .= ' Der Status (&quot;' . $thisRelatedDocument . '&quot;) konnte nicht gespeichert werden.' . '<br />';
							}
						}
					}
					// BOF HANDLE relatedDocuments_collectiveABs
						if($arrRelatedDocuments["collectiveABs"] != ""){
							$thisRelatedDocumentType = 'AB';
							$arrRelatedCollectiveABs = explode(';', $arrRelatedDocuments["collectiveABs"]);
							$sql = "UPDATE
										`" . constant('TABLE_ORDER_AB') . "`
										SET
											`" . constant('TABLE_ORDER_AB') . "`.`orderDocumentsStatus` = '" . $_POST["selectInvoicePaymentStatus"] . "',
											`" . constant('TABLE_ORDER_AB') . "`.`orderDocumentsStatusChangeDate` = NOW()
										WHERE 1
											AND (
												`orderDocumentsNumber` = '" . implode("' OR `orderDocumentsNumber` = '", $arrRelatedCollectiveABs) . "'
											)

								";
							$rs = $dbConnection->db_query($sql);
							if($rs) {
								$successMessage .= ' Der Status (&quot;' . $arrRelatedDocuments["collectiveABs"] . '&quot;) wurde gespeichert.' .'<br />';
							}
							else {
								$errorMessage .= ' Der Status (&quot;' . $arrRelatedDocuments["collectiveABs"] . '&quot;) konnte nicht gespeichert werden.' . '<br />';
							}
						}
					// EOF HANDLE relatedDocuments_collectiveABs
				}
			}
		// EOF SET STATUS OF RELATED DOCUMENT

		if($_REQUEST["documentType"] == 'GU') {
			// BOF GET RE-NUMBER
				#$sql = "SELECT `documentsToDocumentsOriginDocumentNumber` AS `documentNumberRE` FROM `bctr_documentstodocuments` WHERE `documentsToDocumentsCreatedDocumentNumber` = '" . $_POST["documentNumber"]. "'";
				#$rs = $dbConnection->db_query($sql);
				#list($thisDocumentNumberRE) = mysqli_fetch_array($rs);
				/*
				$sql = "UPDATE
						`" . TABLE_ORDER_INVOICES . "`
						SET
							`" . TABLE_ORDER_INVOICES . "`.`orderDocumentsStatus` = '" . $_POST["selectInvoicePaymentStatus"] . "',
							`" . TABLE_ORDER_INVOICES . "`.`orderDocumentsStatusChangeDate` = NOW()
						WHERE `" . TABLE_ORDER_INVOICES . "`.`orderDocumentsNumber` = '" . $thisDocumentNumberRE . "'
					";
				$rs = $dbConnection->db_query($sql);
				*/
			// EOF GET RE-NUMBER

			// BOF GET AB-NUMBER
				#$sql = "SELECT * FROM `bctr_documentstodocuments` WHERE `documentsToDocumentsCreatedDocumentNumber` = '" . $_POST["documentNumber2"]. "'";
			// EOF GET AB-NUMBER
		}
		if($_REQUEST["documentType"] == 'AB') {
			// BOF GET RE-NUMBER
				if($_POST["selectInvoicePaymentStatus"] == '2'){
					$arrRelatedDocuments = getRelatedDocuments(array($_POST["documentNumber"]), '');
					if($arrRelatedDocuments["RE"] != "" && $arrRelatedDocuments["RE"] != null){
						$sql = "UPDATE
									`" . TABLE_ORDER_INVOICES . "`
									SET
										`" . TABLE_ORDER_INVOICES . "`.`orderDocumentsStatus` = '" . $_POST["selectInvoicePaymentStatus"] . "',
										`" . TABLE_ORDER_INVOICES . "`.`orderDocumentsStatusChangeDate` = NOW()
									WHERE
										`" . TABLE_ORDER_INVOICES . "`.`orderDocumentsNumber` = '" . $arrRelatedDocuments["RE"] . "'

							";

						$rs = $dbConnection->db_query($sql);
						if($rs) {
							$successMessage .= ' Der Status wurde auch f&uuml;r die Rechnung &quot;' . $arrRelatedDocuments["RE"] . '&quot; gespeichert.' .'<br />';
						}
						else {
							$errorMessage .= ' Der Status konnte nicht f&uuml;r die Rechnung &quot;' . $arrRelatedDocuments["RE"] . '&quot; gespeichert werden.' . '<br />';
						}
					}
				}
				else if($_POST["selectInvoicePaymentStatus"] == '3') {

				}
				#$sql = "SELECT * FROM `bctr_documentstodocuments` WHERE `documentsToDocumentsOriginDocumentNumber` = '" . $_POST["documentNumber"]. "'";
			// EOF GET RE-NUMBER

			// BOF GET GU-NUMBER
				#$sql = "SELECT * FROM `bctr_documentstodocuments` WHERE `documentsToDocumentsOriginDocumentNumber` = '" . $_POST["documentNumber2"]. "'";
			// EOF GET GU-NUMBER
		}


		// BOF SET PRODUCTION TO STORNO IF DOCUMENT STATUS AB HAS CHANGED TO STORNO
		if($_REQUEST["documentType"] == 'AB' && $_POST["setInvoicePaymentStatus"] == '1' && $_POST["selectInvoicePaymentStatus"] == '3'){
			$sql = "SELECT
						`orderDocumentsOrdersIDs`

						FROM `" . TABLE_ORDER_THIS . "`

						WHERE 1
							AND `" . TABLE_ORDER_THIS . "`.`orderDocumentsID` = '" . $_POST["documentID"] . "'
							AND `" . TABLE_ORDER_THIS . "`.`orderDocumentsNumber` = '" . $_POST["documentNumber"] . "'
				";
			$rs = $dbConnection->db_query($sql);

			list($thisOrderDocumentsOrdersIDs) = mysqli_fetch_array($rs);

			if($thisOrderDocumentsOrdersIDs != ''){
				$arrOrderDocumentsOrdersIDs = explode(';', $thisOrderDocumentsOrdersIDs);

				$arrSql = array();
				foreach($arrOrderDocumentsOrdersIDs as $thisValue){
					if(trim($thisValue) != '') {
						$arrSql[] = " `ordersID` = '" . trim($thisValue) . "'";
					}
				}
				if(!empty($arrSql)){
					$sql = "UPDATE
								`" . TABLE_ORDERS . "`

								SET
									`ordersStatus` = 6,
									`ordersStornoDatum` = NOW()

								WHERE (" . implode(' OR ', $arrSql). ")
						";
					$rs = $dbConnection->db_query($sql);
					if($rs) {
						$successMessage .= ' Die zugeh&ouml;rigen Produktionen &quot;IDs:' . $thisOrderDocumentsOrdersIDs . '&quot; wurden storniert. ' .'<br />';
					}
					else {
						$errorMessage .= ' Die zugeh&ouml;rigen Produktionen &quot;IDs:' . $thisOrderDocumentsOrdersIDs . '&quot; konnte nicht entfernt storniert.' . '<br />';
					}
				}

			}
		}
		// EOF SET PRODUCTION TO STORNO IF DOCUMENT STATUS AB HAS CHANGED TO STORNO


		if(TABLE_ORDER_THIS_PAYMENTS != '') {
			if($_POST["selectInvoicePaymentStatus"] == '1') {
				// BOF DELETE PAYMENTS WHEN PAYMENT STATUS = 1 / OPEN - NOT PAID
					/*
					$sql = "DELETE
								FROM `" . TABLE_ORDER_THIS_PAYMENTS . "`

								WHERE `orderPaymentOrderID` = '" . $_POST["documentID"] . "'
									AND `orderPaymentOrderNumber` = '" . $_POST["documentNumber"] . "'

					";

					#$rs = $dbConnection->db_query($sql);
					if($rs) {
						$successMessage .= ' Der gezahlte Betrag wurde entfernt.' .'<br />';
					}
					else {
						$errorMessage .= ' Der gezahlte Betrag konnte nicht entfernt werden.' . '<br />';
					}
					*/
				// EOF DELETE PAYMENTS WHEN PAYMENT STATUS = 1 / OPEN - NOT PAID
			}
			// // else if(trim($_POST["setInvoicePaymentPaidValue"] > 0)) {
			// else {
			#if($_POST["selectInvoicePaymentStatus"] != '1') {
			if(1){
				if(trim($_POST["setInvoicePaymentPaidValue"] != 0)) {
					$thisNullPaymentNotiz = $_POST["setInvoicePaymentNotice"];
				}
				else if(trim($_POST["setInvoicePaymentPaidValue"] == 0)) {
					// BOF HANDLE NULL PAYMENTS

						// BOF GET NULL PAYMENTS
							$sql_getNullPayment = "
									SELECT
										`orderPaymentID`,
										`orderPaymentOrderID`,
										`orderPaymentOrderNumber`,
										`orderPaymentDate`,
										`orderPaymentBankAccountID`,
										`orderPaymentValue`,
										`orderPaymentSkontoValue`,
										`orderPaymentNotiz`,

										`orderPaymentEntryDate`,
										`orderPaymentUserID`

										FROM `" . TABLE_ORDER_THIS_PAYMENTS . "`

										WHERE 1
											AND `orderPaymentOrderID` = '" . $_POST["documentID"] . "'
											AND `orderPaymentOrderNumber` = '" . $_POST["documentNumber"] . "'
											AND `orderPaymentValue` = 0
								";
							$rs_getNullPayment = $dbConnection->db_query($sql_getNullPayment);

							$arrThisNullPaymentNotice = array();

							$thisNullPaymentNotiz = '';
							while($ds_getNullPayment = mysqli_fetch_assoc($rs_getNullPayment)){
								$arrThisNullPaymentNotice[] = $ds_getNullPayment["orderPaymentNotiz"];
								$arrThisNullPaymentOrderID = $ds_getNullPayment["orderPaymentOrderID"];
								$arrThisNullPaymentOrderNumber = $ds_getNullPayment["orderPaymentOrderNumber"];
							}

							$arrThisNullPaymentNotice[] = $_POST["setInvoicePaymentNotice"];

							if(!empty($arrThisNullPaymentNotice)){
								$thisNullPaymentNotiz = implode("\n", $arrThisNullPaymentNotice);
							}
						// EOF GET NULL PAYMENTS

						// BOF DELETE EXISTING NULL PAYMENTS
							if($rs_getNullPayment){
								$sql_deleteNullPayment = "
										DELETE
											FROM `" . TABLE_ORDER_THIS_PAYMENTS . "`

										WHERE 1
											AND `orderPaymentOrderID` = '" . $_POST["documentID"] . "'
											AND `orderPaymentOrderNumber` = '" . $_POST["documentNumber"] . "'
											AND `orderPaymentValue` = 0
									";
							}
							$rs_deleteNullPayment = $dbConnection->db_query($sql_deleteNullPayment);
						// EOF DELETE EXISTING NULL PAYMENTS

					// EOF HANDLE NULL PAYMENTS
				}

				// BOF STORE PAYMENT
					$thisNullPaymentNotiz = addslashes($thisNullPaymentNotiz);
					$sql = "INSERT
									INTO `" . TABLE_ORDER_THIS_PAYMENTS . "` (
										`orderPaymentID`,
										`orderPaymentOrderID`,
										`orderPaymentOrderNumber`,
										`orderPaymentDate`,
										`orderPaymentBankAccountID`,
										`orderPaymentValue`,
										`orderPaymentSkontoValue`,
										`orderPaymentNotiz`,

										`orderPaymentEntryDate`,
										`orderPaymentUserID`
									)
									VALUES (
										'%',
										'" . $_POST["documentID"] . "',
										'" . $_POST["documentNumber"] . "',
										'" . formatDate($_POST["setInvoicePaymentDate"], 'store') . "',
										'" . $_POST["setInvoicePaymentBankAccount"] . "',
										'" . preg_replace("/,/", ".", $_POST["setInvoicePaymentPaidValue"]) . "',
										'" . preg_replace("/,/", ".", $_POST["setInvoicePaymentSkontoValue"]) . "',
										'" . ($thisNullPaymentNotiz) . "',

										NOW(),
										'" . $_SESSION["usersID"] . "'
									)

						";
					$rs = $dbConnection->db_query($sql);
					if($rs) {
						$successMessage .= ' Der Zahlbetrag wurde gespeichert.' .'<br />';
					}
					else {
						$errorMessage .= ' Der Zahlbetrag konnte nicht gespeichert werden.' . '<br />';
					}
				// BOF STORE PAYMENT
			}
		}
	}
	// EOF SET INVOICE STATUS AND INSERT PAYMENT VALUE

	// BOF GET INVOICES DEPENDING ON STATUS
	$where = "";
	$having = "";

	if(trim($_REQUEST["searchCustomerNumber"]) != '') {
		#$where = " AND `" . TABLE_ORDER_THIS . "`.`orderDocumentsCustomerNumber` = '" . trim($_REQUEST["searchCustomerNumber"]) . "'";

		// BOF GET CUSTOMER-ID
			$sql_getCustomerID = "
					SELECT
						`" . TABLE_CUSTOMERS . "`.`customersKundennummer`,
						`" . TABLE_CUSTOMERS . "`.`customersID`
						FROM `" . TABLE_CUSTOMERS . "`
						WHERE 1
							AND `" . TABLE_CUSTOMERS . "`.`customersKundennummer` = '" . trim($_REQUEST["searchCustomerNumber"]) . "'
						LIMIT 1
				";
			$rs_getCustomerID = $dbConnection->db_query($sql_getCustomerID);
			list($searchCustomerNumber, $searchCustomerID) = mysqli_fetch_array($rs_getCustomerID);
		// EOF GET CUSTOMER-ID
		$where = " AND (
						`" . TABLE_ORDER_THIS . "`.`orderDocumentsCustomerNumber` = '" . trim($_REQUEST["searchCustomerNumber"]) . "'
						OR
						`" . TABLE_ORDER_THIS . "`.`orderDocumentsSalesman` = '" . trim($searchCustomerID) . "'
					)
			";
	}
	else if(trim($_REQUEST["searchCustomerName"]) != '') {
		$where = " AND (
						`" . TABLE_ORDER_THIS . "`.`orderDocumentsAddressCompany` LIKE '%" . trim(urldecode($_REQUEST["searchCustomerName"])) . "%'
						OR
						`" . TABLE_ORDER_THIS . "`.`orderDocumentsAddressName` LIKE '%" . trim(urldecode($_REQUEST["searchCustomerName"])) . "%'
					)
			";
	}
	else if(trim($_REQUEST["searchDocumentNumber"]) != '') {
		#$where = " AND `orderDocumentsNumber` LIKE '%" . trim($_REQUEST["searchDocumentNumber"]) . "'";
		$where = " AND `" . TABLE_ORDER_THIS . "`.`orderDocumentsNumber` LIKE '%" . trim($_REQUEST["searchDocumentNumber"]) . "'";
	}
	else if(trim($_REQUEST["searchCustomersTaxAccountID"]) != '') {
		$where = " AND (
						`" . TABLE_CUSTOMERS . "`.`customersTaxAccountID` = '" . trim($_REQUEST["searchCustomersTaxAccountID"]) . "'
						OR
						`common_salesmen`.`customersTaxAccountID` = '" . trim($_REQUEST["searchCustomersTaxAccountID"]) . "'
						OR
						`common_recipient`.`customersTaxAccountID` = '" . trim($_REQUEST["searchCustomersTaxAccountID"]) . "'
					)
			";

	}
	else if(trim($_REQUEST["searchWord"]) != '') {
		$where = " AND (
					`" . TABLE_ORDER_THIS . "`.`orderDocumentsCustomerNumber` = '" . trim($_REQUEST["searchWord"]) . "'
					OR
					`" . TABLE_ORDER_THIS . "`.`orderDocumentsAddressCompany` LIKE '%" . trim(urldecode($_REQUEST["searchWord"])) . "%'
					OR
					`" . TABLE_ORDER_THIS . "`.`orderDocumentsAddressName` LIKE '%" . trim(urldecode($_REQUEST["searchWord"])) . "%'
					OR
					`" . TABLE_ORDER_THIS . "`.`orderDocumentsNumber` LIKE '%" . trim($_REQUEST["searchWord"]) . "'
			)";
	}
	/*
	else if(($_REQUEST["searchMonth"]) != '') {
		// $where = " AND SUBSTRING(`" . TABLE_ORDER_THIS . "`.`orderDocumentsDocumentDate`, 1, 7) = '" . ($_REQUEST["searchMonth"]) . "'";
		#$where = " AND SUBSTRING(`" . TABLE_ORDER_THIS . "`.`orderDocumentsDocumentDate`, 1, " . strlen($_REQUEST["searchMonth"]) . ") = '" . ($_REQUEST["searchMonth"]) . "'";
	}
	*/
	else if($_REQUEST["searchTotalPrice"] != ''){
		$thisSearchTotalPrice = trim($_REQUEST["searchTotalPrice"]);
		$thisSearchTotalPrice = urldecode($thisSearchTotalPrice);
		$thisSearchTotalPrice = preg_replace("/,/", ".", $thisSearchTotalPrice);
		#$where = " AND `" . TABLE_ORDER_THIS . "`.`orderDocumentsTotalPrice` = '" . ($thisSearchTotalPrice) . "'";

		$whereAdd = '';

		if($thisDocumentTypeRelated != "" || in_array($_REQUEST["statusType"], array('MA', 'M1', 'M2', 'M3')) || in_array($_REQUEST["documentType"], array('RE'))){
			$whereAdd .= " OR (`" . TABLE_ORDER_M1 . "`.`orderDocumentsTotalPrice` + `" . TABLE_ORDER_M1 . "`.`orderDocumentsInterestPrice` + `" . TABLE_ORDER_M1 . "`.`orderDocumentsChargesPrice` = '" . ($thisSearchTotalPrice) . "') ";
			$whereAdd .= " OR (`" . TABLE_ORDER_M2 . "`.`orderDocumentsTotalPrice` + `" . TABLE_ORDER_M2 . "`.`orderDocumentsInterestPrice` + `" . TABLE_ORDER_M2 . "`.`orderDocumentsChargesPrice` = '" . ($thisSearchTotalPrice) . "') ";

			#$whereAdd .= " OR (`" . TABLE_ORDER_MA . "`.`orderDocumentsTotalPrice` + `" . TABLE_ORDER_MA . "`.`orderDocumentsInterestPrice` + `" . TABLE_ORDER_MA . "`.`orderDocumentsChargesPrice` = '" . ($thisSearchTotalPrice) . "') ";
			#$whereAdd .= " OR (`" . TABLE_ORDER_M3 . "`.`orderDocumentsTotalPrice` + `" . TABLE_ORDER_M3 . "`.`orderDocumentsInterestPrice` + `" . TABLE_ORDER_M3 . "`.`orderDocumentsChargesPrice` = '" . ($thisSearchTotalPrice) . "') ";
			#$whereAdd .= " OR (`" . TABLE_ORDER_AB . "`.`orderDocumentsTotalPrice` + `" . TABLE_ORDER_AB . "`.`orderDocumentsInterestPrice` + `" . TABLE_ORDER_AB . "`.`orderDocumentsChargesPrice` = '" . ($thisSearchTotalPrice) . "') ";
			#$whereAdd .= " OR (`" . TABLE_ORDER_GU . "`.`orderDocumentsTotalPrice` + `" . TABLE_ORDER_GU . "`.`orderDocumentsInterestPrice` + `" . TABLE_ORDER_GU . "`.`orderDocumentsChargesPrice` = '" . ($thisSearchTotalPrice) . "') ";
		}

		$where = " AND (
						`" . TABLE_ORDER_THIS . "`.`orderDocumentsTotalPrice` = '" . ($thisSearchTotalPrice) . "'
						" . $whereAdd . "
					)
			";
	}
	else if($_REQUEST["searchCustomersOrderNumber"] != ''){
		$where = " AND `" . TABLE_ORDER_THIS . "`.`orderDocumentsCustomersOrderNumber` = '" . ($_REQUEST["searchCustomersOrderNumber"]) . "'";
	}
	else if($_REQUEST["searchPaymentType"] != ''){
		$where = " AND `" . TABLE_ORDER_THIS . "`.`orderDocumentsPaymentType` = '" . ($_REQUEST["searchPaymentType"]) . "'";
	}
	else if($_REQUEST["searchBankAccount"] != ''){
		$where = " AND `" . TABLE_ORDER_THIS . "`.`orderDocumentsBankAccount` = '" . ($_REQUEST["searchBankAccount"]) . "'";
	}

	if(($_REQUEST["searchMonth"]) != '') {
		// $where = " AND SUBSTRING(`" . TABLE_ORDER_THIS . "`.`orderDocumentsDocumentDate`, 1, 7) = '" . ($_REQUEST["searchMonth"]) . "'";
		$where .= " AND SUBSTRING(`" . TABLE_ORDER_THIS . "`.`orderDocumentsDocumentDate`, 1, " . strlen($_REQUEST["searchMonth"]) . ") = '" . ($_REQUEST["searchMonth"]) . "'";
	}


	if($_REQUEST["statusType"] != 'ALL' && $_REQUEST["statusType"] != 'OF_MA') {
		if($_REQUEST["statusType"] == 'OF') {
			$where .= " AND (
							`" . TABLE_PAYMENT_STATUS_TYPES . "`.`paymentStatusTypesShortName` = 'OF'
							OR `" . TABLE_PAYMENT_STATUS_TYPES . "`.`paymentStatusTypesShortName` = 'TB'
							OR `" . TABLE_PAYMENT_STATUS_TYPES . "`.`paymentStatusTypesShortName` = 'MA'
							OR `" . TABLE_PAYMENT_STATUS_TYPES . "`.`paymentStatusTypesShortName` = 'M1'
							OR `" . TABLE_PAYMENT_STATUS_TYPES . "`.`paymentStatusTypesShortName` = 'M2'
							OR `" . TABLE_PAYMENT_STATUS_TYPES . "`.`paymentStatusTypesShortName` = 'M3'
							OR `" . TABLE_PAYMENT_STATUS_TYPES . "`.`paymentStatusTypesShortName` = 'IK'
							OR `" . TABLE_PAYMENT_STATUS_TYPES . "`.`paymentStatusTypesShortName` = 'MB'
						)
					";
		}
		else if($_REQUEST["statusType"] != 'OF') {
			$where .= " AND `" . TABLE_PAYMENT_STATUS_TYPES . "`.`paymentStatusTypesShortName` = '" . $_REQUEST["statusType"] . "' ";
		}
	}
	else if($_REQUEST["statusType"] == 'OF_MA') {
		#$where .= " AND `" . TABLE_PAYMENT_STATUS_TYPES . "`.`paymentStatusTypesShortName` = 'OF' ";
		$where .= " AND (
						`" . TABLE_PAYMENT_STATUS_TYPES . "`.`paymentStatusTypesShortName` != 'BZ'
						AND
						`" . TABLE_PAYMENT_STATUS_TYPES . "`.`paymentStatusTypesShortName` != 'ST'
						AND
						`" . TABLE_PAYMENT_STATUS_TYPES . "`.`paymentStatusTypesShortName` != 'GS'
					)
			";
		$having = " HAVING ( '" . date("Y-m-d"). "' > `orderDocumentsDeadline` ) ";
	}

	$sql = "SELECT
				/* SQL_CALC_FOUND_ROWS */

				`" . TABLE_ORDER_THIS . "`.`orderDocumentsNumber`,
				`" . TABLE_ORDER_THIS . "`.`orderDocumentsID`,
				`" . TABLE_ORDER_THIS . "`.`orderDocumentsType`,
				`" . TABLE_ORDER_THIS . "`.`orderDocumentsOrdersIDs`,
				`" . TABLE_ORDER_THIS . "`.`orderDocumentsCustomerNumber`,
				`" . TABLE_ORDER_THIS . "`.`orderDocumentsCustomersOrderNumber`,
				`" . TABLE_ORDER_THIS . "`.`orderDocumentsProcessingDate`,
				`" . TABLE_ORDER_THIS . "`.`orderDocumentsDocumentDate`,
				`" . TABLE_ORDER_THIS . "`.`orderDocumentsOrderDate`,
				`" . TABLE_ORDER_THIS . "`.`orderDocumentsInvoiceDate`,
				`" . TABLE_ORDER_THIS . "`.`orderDocumentsCreditDate`,
				`" . TABLE_ORDER_THIS . "`.`orderDocumentsDeliveryDate`,
				`" . TABLE_ORDER_THIS . "`.`orderDocumentsBindingDate`,
				`" . TABLE_ORDER_THIS . "`.`orderDocumentsSalesman`,
				`" . TABLE_ORDER_THIS . "`.`orderDocumentsKommission`,
				`" . TABLE_ORDER_THIS . "`.`orderDocumentsAddressCompany`,
				`" . TABLE_ORDER_THIS . "`.`orderDocumentsAddressName`,
				`" . TABLE_ORDER_THIS . "`.`orderDocumentsAddressStreet`,
				`" . TABLE_ORDER_THIS . "`.`orderDocumentsAddressStreetNumber`,
				`" . TABLE_ORDER_THIS . "`.`orderDocumentsAddressZipcode`,
				`" . TABLE_ORDER_THIS . "`.`orderDocumentsAddressCity`,
				`" . TABLE_ORDER_THIS . "`.`orderDocumentsAddressCountry`,
				`" . TABLE_ORDER_THIS . "`.`orderDocumentsAddressMail`,
				`" . TABLE_ORDER_THIS . "`.`orderDocumentsSumPrice`,
				`" . TABLE_ORDER_THIS . "`.`orderDocumentsDiscount`,
				`" . TABLE_ORDER_THIS . "`.`orderDocumentsDiscountType`,
				`" . TABLE_ORDER_THIS . "`.`orderDocumentsDiscountPercent`,
				`" . TABLE_ORDER_THIS . "`.`orderDocumentsMwst`,
				`" . TABLE_ORDER_THIS . "`.`orderDocumentsMwstPrice`,
				`" . TABLE_ORDER_THIS . "`.`orderDocumentsShippingCosts`,
				`" . TABLE_ORDER_THIS . "`.`orderDocumentsPackagingCosts`,
				`" . TABLE_ORDER_THIS . "`.`orderDocumentsCashOnDelivery`,
				`" . TABLE_ORDER_THIS . "`.`orderDocumentsBankAccount`,
				`" . TABLE_ORDER_THIS . "`.`orderDocumentsTotalPrice`,
				`" . TABLE_ORDER_THIS . "`.`orderDocumentsPaymentCondition`,
				`" . TABLE_ORDER_THIS . "`.`orderDocumentsPaymentType`,
				`" . TABLE_ORDER_THIS . "`.`orderDocumentsSkonto`,
				`" . TABLE_ORDER_THIS . "`.`orderDocumentsDeliveryType`,
				`" . TABLE_ORDER_THIS . "`.`orderDocumentsDeliveryTypeNumber`,
				`" . TABLE_ORDER_THIS . "`.`orderDocumentsSubject`,
				`" . TABLE_ORDER_THIS . "`.`orderDocumentsContent`,
				`" . TABLE_ORDER_THIS . "`.`orderDocumentsDocumentPath`,
				`" . TABLE_ORDER_THIS . "`.`orderDocumentsTimestamp`,
				`" . TABLE_ORDER_THIS . "`.`orderDocumentsStatus`,
				`" . TABLE_ORDER_THIS . "`.`orderDocumentsSendMail`,

				`" . TABLE_ORDER_THIS . "`.`orderDocumentsAddressCompanyCustomerNumber`,

				`" . TABLE_ORDER_THIS . "`.`orderDocumentsIsCollectiveInvoice`,

				`" . TABLE_PAYMENT_CONDITIONS . "`.`paymentConditionsShortName`,
				`" . TABLE_PAYMENT_CONDITIONS . "`.`paymentConditionsName`,

				`" . TABLE_PAYMENT_TYPES . "`.`paymentTypesShortName`,
				`" . TABLE_PAYMENT_TYPES . "`.`paymentTypesName`,
		";

	$sql .= "
			`" . TABLE_CUSTOMERS . "`.`customersKundennummer`,
			`" . TABLE_CUSTOMERS . "`.`customersFirmenname`,
			`" . TABLE_CUSTOMERS . "`.`customersCompanyPLZ`,
			`" . TABLE_CUSTOMERS . "`.`customersCompanyOrt`,

			`" . TABLE_CUSTOMERS . "`.`customersUseSalesmanDeliveryAdress`,
			`" . TABLE_CUSTOMERS . "`.`customersUseSalesmanInvoiceAdress`,

			`" . TABLE_CUSTOMERS . "`.`customersMail1`,
			`" . TABLE_CUSTOMERS . "`.`customersMail2`,

			`" . TABLE_CUSTOMERS . "`.`customersTaxAccountID`,

			IF(`common_salesmen`.`customersKundennummer` IS NULL, '', `common_salesmen`.`customersKundennummer`) AS `salesmenKundennummer`,
			IF(`common_salesmen`.`customersTaxAccountID` IS NULL, '', `common_salesmen`.`customersTaxAccountID`) AS `salesmenTaxAccountID`,

			`common_recipient`.`customersKundennummer` AS `recipientKundennummer`,
			`common_recipient`.`customersTaxAccountID` AS `recipientTaxAccountID`,

			`common_salesmen`.`customersMail1` AS `salesmenMail1`,
			`common_salesmen`.`customersMail2` AS `salesmenMail2`,

			IF(`" . TABLE_CUSTOMERS . "`.`customersUseSalesmanInvoiceAdress` = '1',
				CONCAT(`common_salesmen`.`customersMail1`, ';', `common_salesmen`.`customersMail2`),
				CONCAT(`" . TABLE_CUSTOMERS . "`.`customersMail1`, ';', `" . TABLE_CUSTOMERS . "`.`customersMail2`)
			) AS `useMailAdress`,
		";

	if(TABLE_ORDER_THIS_PAYMENTS != '') {
		$sql .= "
					`" . TABLE_ORDER_THIS_PAYMENTS . "`.`orderPaymentID`,
					`" . TABLE_ORDER_THIS_PAYMENTS . "`.`orderPaymentOrderID`,
					`" . TABLE_ORDER_THIS_PAYMENTS . "`.`orderPaymentOrderNumber`,
					GROUP_CONCAT(
						`" . TABLE_ORDER_THIS_PAYMENTS . "`.`orderPaymentDate`,
						':::',
						`" . TABLE_ORDER_THIS_PAYMENTS . "`.`orderPaymentValue`,
						':::',
						`" . TABLE_ORDER_THIS_PAYMENTS . "`.`orderPaymentSkontoValue`,
						':::',
						`" . TABLE_ORDER_THIS_PAYMENTS . "`.`orderPaymentBankAccountID`,
						':::',
						`" . TABLE_ORDER_THIS_PAYMENTS . "`.`orderPaymentNotiz`
						SEPARATOR '###'
					) AS `paymentDatas`,
					SUM(DISTINCT `" . TABLE_ORDER_THIS_PAYMENTS . "`.`orderPaymentValue`) AS `paymentPaidSum`,
					SUM(`" . TABLE_ORDER_THIS_PAYMENTS . "`.`orderPaymentValue`) AS `paymentPaidSum2`,
					SUM(DISTINCT `" . TABLE_ORDER_THIS_PAYMENTS . "`.`orderPaymentSkontoValue`) AS `paymentSkontoSum`,
					SUM(`" . TABLE_ORDER_THIS_PAYMENTS . "`.`orderPaymentSkontoValue`) AS `paymentSkontoSum2`,
					COUNT(DISTINCT `" . TABLE_ORDER_THIS_PAYMENTS . "`.`orderPaymentID`) AS `paymentsCount`,
					GROUP_CONCAT(DISTINCT`" . TABLE_ORDER_THIS_PAYMENTS . "`.`orderPaymentID`, ':', `" . TABLE_ORDER_THIS_PAYMENTS . "`.`orderPaymentValue`, ':', `" . TABLE_ORDER_THIS_PAYMENTS . "`.`orderPaymentSkontoValue` SEPARATOR ';') AS `paymentsAll`,
			";
		}

	if($thisDocumentTypeRelated != ""){
		$sql .= "
					`" . RELATED_TABLE_ORDER_THIS_PAYMENTS . "`.`orderPaymentID` AS `relatedOrderPaymentID`,
					`" . RELATED_TABLE_ORDER_THIS_PAYMENTS . "`.`orderPaymentOrderID` AS `relatedOrderPaymentOrderID`,
					`" . RELATED_TABLE_ORDER_THIS_PAYMENTS . "`.`orderPaymentOrderNumber` AS `relatedOrderPaymentOrderNumber`,
					GROUP_CONCAT(
						`" . RELATED_TABLE_ORDER_THIS_PAYMENTS . "`.`orderPaymentDate`,
						':::',
						`" . RELATED_TABLE_ORDER_THIS_PAYMENTS . "`.`orderPaymentValue`,
						':::',
						`" . RELATED_TABLE_ORDER_THIS_PAYMENTS . "`.`orderPaymentSkontoValue`,
						':::',
						`" . RELATED_TABLE_ORDER_THIS_PAYMENTS . "`.`orderPaymentBankAccountID`,
						':::',
						`" . RELATED_TABLE_ORDER_THIS_PAYMENTS . "`.`orderPaymentNotiz`
						SEPARATOR '###'
					) AS `relatedPaymentDatas`,
					SUM(DISTINCT `" . RELATED_TABLE_ORDER_THIS_PAYMENTS . "`.`orderPaymentValue`) AS `relatedPaymentPaidSum`,
					SUM(`" . RELATED_TABLE_ORDER_THIS_PAYMENTS . "`.`orderPaymentValue`) AS `relatedPaymentPaidSum2`,
					SUM(DISTINCT `" . RELATED_TABLE_ORDER_THIS_PAYMENTS . "`.`orderPaymentSkontoValue`) AS `relatedPaymentSkontoSum`,
					SUM(`" . RELATED_TABLE_ORDER_THIS_PAYMENTS . "`.`orderPaymentSkontoValue`) AS `relatedPaymentSkontoSum2`,
					COUNT(DISTINCT `" . RELATED_TABLE_ORDER_THIS_PAYMENTS . "`.`orderPaymentID`) AS `relatedPaymentsCount`,
					GROUP_CONCAT(DISTINCT`" . RELATED_TABLE_ORDER_THIS_PAYMENTS . "`.`orderPaymentID`, ':', `" . RELATED_TABLE_ORDER_THIS_PAYMENTS . "`.`orderPaymentValue`, ':', `" . RELATED_TABLE_ORDER_THIS_PAYMENTS . "`.`orderPaymentSkontoValue` SEPARATOR ';') AS `relatedPaymentsAll`,
		";
	}

	$sql .= "
				COUNT(`" . TABLE_ORDER_THIS . "`.`orderDocumentsID`) `countEntries`,

				/*
				IF(`" . TABLE_ORDER_THIS . "`.`orderDocumentsPaymentCondition` = '1', DATE_ADD(`" . TABLE_ORDER_THIS . "`.`orderDocumentsDocumentDate`, INTERVAL " . PAYMENT_DEADLINE_RE_10TOA . " DAY),
					IF(`" . TABLE_ORDER_THIS . "`.`orderDocumentsPaymentCondition` = '5', DATE_ADD(`" . TABLE_ORDER_THIS . "`.`orderDocumentsDocumentDate`, INTERVAL " . PAYMENT_DEADLINE_RE_14T . " DAY),
						IF(`" . TABLE_ORDER_THIS . "`.`orderDocumentsPaymentCondition` = '6', DATE_ADD(`" . TABLE_ORDER_THIS . "`.`orderDocumentsDocumentDate`, INTERVAL " . PAYMENT_DEADLINE_RE_2PCSK10T30TN . " DAY),
							DATE_ADD(`" . TABLE_ORDER_THIS . "`.`orderDocumentsDocumentDate`, INTERVAL " . PAYMENT_DEADLINE_RE_DEFAULT . " DAY)
						)
					)
				) AS `orderDocumentsDeadline`,
				*/

				IF(`" . TABLE_PAYMENT_CONDITIONS . "`.`paymentConditionsDaysLimit` = 0, DATE_ADD(`" . TABLE_ORDER_THIS . "`.`orderDocumentsDocumentDate`, INTERVAL " . PAYMENT_DEADLINE_RE_DEFAULT . " DAY),
					DATE_ADD(`" . TABLE_ORDER_THIS . "`.`orderDocumentsDocumentDate`, INTERVAL `" . TABLE_PAYMENT_CONDITIONS . "`.`paymentConditionsDaysLimit` DAY)
				) AS `orderDocumentsDeadline`,

				IF(`" . TABLE_ORDER_THIS . "`.`orderDocumentsPaymentCondition` = '6', `" . TABLE_ORDER_THIS . "`.`orderDocumentsTotalPrice` * ((100 - " . PAYMENT_SKONTO . ") / 100),
					0
				) AS `orderDocumentsSkonto2`
		";

	if(in_array($_REQUEST["statusType"], array('MA', 'M1', 'M2', 'M3'))) {
		$sql .= "
				,
				`" . constant("TABLE_ORDER_" . $_REQUEST["statusType"]) . "`.`orderDocumentsInterestPercent`,
				`" . constant("TABLE_ORDER_" . $_REQUEST["statusType"]) . "`.`orderDocumentsInterestPrice`,
				`" . constant("TABLE_ORDER_" . $_REQUEST["statusType"]) . "`.`orderDocumentsChargesPrice`,
				`" . constant("TABLE_ORDER_" . $_REQUEST["statusType"]) . "`.`orderDocumentsNumber` AS `orderDocumentsNumberDemand`,
				`" . constant("TABLE_ORDER_" . $_REQUEST["statusType"]) . "`.`orderDocumentsDocumentDate` AS `orderDocumentsDocumentDateDemand`,
				DATE_ADD(`" . constant("TABLE_ORDER_" . $_REQUEST["statusType"]) . "`.`orderDocumentsDocumentDate`, INTERVAL " . constant("PAYMENT_DEADLINE_" . $_REQUEST["statusType"]) . " DAY) AS `orderDocumentDeadlineDemand`
			";
	}

########## INSERT

	if($thisDocumentTypeRelated != "" || in_array($_REQUEST["statusType"], array('MA', 'M1', 'M2', 'M3')) || in_array($_REQUEST["documentType"], array('RE'))){

		$sql .= "
			,

			IF(`" . TABLE_ORDER_M2 . "`.`orderDocumentsInterestPercent` > 0 || `" . TABLE_ORDER_M2 . "`.`orderDocumentsInterestPrice` > 0 || `" . TABLE_ORDER_M2 . "`.`orderDocumentsChargesPrice` > 0,
				`" . TABLE_ORDER_M2 . "`.`orderDocumentsInterestPercent`,
				IF(`" . TABLE_ORDER_M1 . "`.`orderDocumentsInterestPercent` > 0 || `" . TABLE_ORDER_M1 . "`.`orderDocumentsInterestPrice` > 0 || `" . TABLE_ORDER_M1 . "`.`orderDocumentsChargesPrice` > 0,
					`" . TABLE_ORDER_M1 . "`.`orderDocumentsInterestPercent`,
					0
				)
			) AS `relatedDocumentsInterestPercent`,

			IF(`" . TABLE_ORDER_M2 . "`.`orderDocumentsInterestPrice` > 0 || `" . TABLE_ORDER_M2 . "`.`orderDocumentsInterestPrice` > 0 || `" . TABLE_ORDER_M2 . "`.`orderDocumentsChargesPrice` > 0,
				`" . TABLE_ORDER_M2 . "`.`orderDocumentsInterestPrice`,
				IF(`" . TABLE_ORDER_M1 . "`.`orderDocumentsInterestPrice` > 0 || `" . TABLE_ORDER_M1 . "`.`orderDocumentsInterestPrice` > 0 || `" . TABLE_ORDER_M1 . "`.`orderDocumentsChargesPrice` > 0,
					`" . TABLE_ORDER_M1 . "`.`orderDocumentsInterestPrice`,
					0
				)
			) AS `relatedDocumentsInterestPrice`,

			IF(`" . TABLE_ORDER_M2 . "`.`orderDocumentsChargesPrice` > 0 || `" . TABLE_ORDER_M2 . "`.`orderDocumentsChargesPrice` > 0 || `" . TABLE_ORDER_M2 . "`.`orderDocumentsChargesPrice` > 0,
				`" . TABLE_ORDER_M2 . "`.`orderDocumentsChargesPrice`,
				IF(`" . TABLE_ORDER_M1 . "`.`orderDocumentsChargesPrice` > 0 || `" . TABLE_ORDER_M1 . "`.`orderDocumentsChargesPrice` > 0 || `" . TABLE_ORDER_M1 . "`.`orderDocumentsChargesPrice` > 0,
					`" . TABLE_ORDER_M1 . "`.`orderDocumentsChargesPrice`,
					0
				)
			) AS `relatedDocumentsChargesPrice`

		";

		$sql .= "
				,
				`" . TABLE_RELATED_DOCUMENTS . "`.*
			";
	}

	$sql .= "
				FROM `" . TABLE_PAYMENT_STATUS_TYPES . "`

				INNER JOIN `" . TABLE_ORDER_THIS . "`
				ON(`" . TABLE_PAYMENT_STATUS_TYPES . "`.`paymentStatusTypesID` = `" . TABLE_ORDER_THIS . "`.`orderDocumentsStatus`)
		";

	$sql .= "
				LEFT JOIN `" . TABLE_PAYMENT_TYPES . "`
				ON(`" . TABLE_ORDER_THIS . "`.`orderDocumentsPaymentType` = `" . TABLE_PAYMENT_TYPES . "`.`paymentTypesID`)

				LEFT JOIN `" . TABLE_PAYMENT_CONDITIONS . "`
				ON(`" . TABLE_ORDER_THIS . "`.`orderDocumentsPaymentCondition` = `" . TABLE_PAYMENT_CONDITIONS . "`.`paymentConditionsID`)
		";

	if(TABLE_ORDER_THIS_PAYMENTS != '') {
		$sql .= "
				LEFT JOIN `" . TABLE_ORDER_THIS_PAYMENTS . "`
				/* ON(`" . TABLE_ORDER_THIS . "`.`orderDocumentsID` = `" . TABLE_ORDER_THIS_PAYMENTS . "`.`orderPaymentOrderID`) */
				ON(`" . TABLE_ORDER_THIS . "`.`orderDocumentsNumber` = `" . TABLE_ORDER_THIS_PAYMENTS . "`.`orderPaymentOrderNumber`)
			";
	}
#dd('thisDocumentTypeRelated');
	#if($thisDocumentTypeRelated != ""){
	if($thisDocumentTypeRelated != ""){
		$sql .= "
				LEFT JOIN `" . TABLE_RELATED_DOCUMENTS . "`
				ON(`" . TABLE_ORDER_THIS . "`.`orderDocumentsNumber` = `" . TABLE_RELATED_DOCUMENTS . "`.`relatedDocuments_" . $_REQUEST["documentType"]. "`)
			";


		$sql .= "
				LEFT JOIN `" . RELATED_TABLE_ORDER_THIS_PAYMENTS . "`
				ON(`" . TABLE_RELATED_DOCUMENTS . "`.`relatedDocuments_" . $thisDocumentTypeRelated. "` = `" . RELATED_TABLE_ORDER_THIS_PAYMENTS . "`.`orderPaymentOrderNumber`)
			";
	}

	else if(in_array($_REQUEST["statusType"], array('MA', 'M1', 'M2', 'M3')) || in_array($_REQUEST["documentType"], array('RE'))) {
		$sql .= "
			LEFT JOIN `" . TABLE_RELATED_DOCUMENTS . "`
			ON(`" . constant("TABLE_ORDER_" . $_REQUEST["documentType"]) . "`.`orderDocumentsNumber` = `" . TABLE_RELATED_DOCUMENTS . "`.`relatedDocuments_" . $_REQUEST["documentType"] . "`)

			LEFT JOIN `" . constant("TABLE_ORDER_" . $_REQUEST["statusType"]) . "`
			ON(`" . TABLE_RELATED_DOCUMENTS . "`.`relatedDocuments_" . $_REQUEST["statusType"] . "` = `" . constant("TABLE_ORDER_" . $_REQUEST["statusType"]) . "`.`orderDocumentsNumber`)
		";
	}

	// BOF GET RELATED DEMANDS DATA
	if($thisDocumentTypeRelated != "" || in_array($_REQUEST["statusType"], array('MA', 'M1', 'M2', 'M3')) || in_array($_REQUEST["documentType"], array('RE'))){
		if($_REQUEST["statusType"] != 'M1'){
			$sql .= "
					LEFT JOIN `" . TABLE_ORDER_M1 . "`
					ON(`" . TABLE_RELATED_DOCUMENTS . "`.`relatedDocuments_M1` = `" . TABLE_ORDER_M1 . "`.`orderDocumentsNumber`)
				";
		}
		if($_REQUEST["statusType"] != 'M2'){
			$sql .= "
					LEFT JOIN `" . TABLE_ORDER_M2 . "`
					ON(`" . TABLE_RELATED_DOCUMENTS . "`.`relatedDocuments_M2` = `" . TABLE_ORDER_M2 . "`.`orderDocumentsNumber`)
				";
		}
	}
	// EOF GET RELATED DEMANDS DATA

	$sql .= "
		LEFT JOIN `" . TABLE_CUSTOMERS . "` AS `" . TABLE_CUSTOMERS . "`
		ON(`" . TABLE_ORDER_THIS . "`.`orderDocumentsCustomerNumber` = `" . TABLE_CUSTOMERS . "`.`customersKundennummer`)

		LEFT JOIN `" . TABLE_CUSTOMERS . "` AS `common_salesmen`
		ON(`" . TABLE_ORDER_THIS . "`.`orderDocumentsSalesman` = `common_salesmen`.`customersID`)

		LEFT JOIN `" . TABLE_CUSTOMERS . "` AS `common_recipient`
		ON(`" . TABLE_ORDER_THIS . "`.`orderDocumentsAddressCompanyCustomerNumber` = `common_recipient`.`customersKundennummer`)
	";

	$sql .= "
				WHERE 1
				" . $where . "

				GROUP BY `" . TABLE_ORDER_THIS . "`.`orderDocumentsNumber`

				" . $having . "
		";

		if($_REQUEST["statusType"] == 'OF_MA') {
			$sql .= "
					ORDER BY `orderDocumentsDeadline` ASC
				";
		}
		else {
			$sql .= "
					ORDER BY `" . TABLE_ORDER_THIS . "`.`orderDocumentsNumber` DESC
				";
		}
	// EOF GET INVOICES DEPENDING ON STATUS

	// BOF GET COUNT ALL ROWS
		$sql_getAllRows = "
				SELECT

					COUNT(`" . TABLE_ORDER_THIS . "`.`orderDocumentsNumber`) AS `countAllRows`

				FROM `" . TABLE_PAYMENT_STATUS_TYPES . "`

				INNER JOIN `" . TABLE_ORDER_THIS . "`
				ON(`" . TABLE_PAYMENT_STATUS_TYPES . "`.`paymentStatusTypesID` = `" . TABLE_ORDER_THIS . "`.`orderDocumentsStatus`)


			";
		if($_REQUEST["searchTotalPrice"] != "") {
			$sql_getAllRows .= "
					LEFT JOIN `" . TABLE_RELATED_DOCUMENTS . "`
					ON(`" . constant("TABLE_ORDER_" . $_REQUEST["documentType"]) . "`.`orderDocumentsNumber` = `" . TABLE_RELATED_DOCUMENTS . "`.`relatedDocuments_" . $_REQUEST["documentType"] . "`)
				";

			$sql_getAllRows .= "
					LEFT JOIN `" . TABLE_ORDER_M1 . "`
					ON(`" . TABLE_RELATED_DOCUMENTS . "`.`relatedDocuments_M1` = `" . TABLE_ORDER_M1 . "`.`orderDocumentsNumber`)
				";

			$sql_getAllRows .= "
					LEFT JOIN `" . TABLE_ORDER_M2 . "`
					ON(`" . TABLE_RELATED_DOCUMENTS . "`.`relatedDocuments_M2` = `" . TABLE_ORDER_M2 . "`.`orderDocumentsNumber`)
				";
		}

		if($_REQUEST["searchCustomersTaxAccountID"] != "") {
			$sql_getAllRows .= "
				LEFT JOIN `" . TABLE_CUSTOMERS . "` AS `" . TABLE_CUSTOMERS . "`
				ON(`" . TABLE_ORDER_THIS . "`.`orderDocumentsCustomerNumber` = `" . TABLE_CUSTOMERS . "`.`customersKundennummer`)

				LEFT JOIN `" . TABLE_CUSTOMERS . "` AS `common_salesmen`
				ON(`" . TABLE_ORDER_THIS . "`.`orderDocumentsSalesman` = `common_salesmen`.`customersID`)

				LEFT JOIN `" . TABLE_CUSTOMERS . "` AS `common_recipient`
				ON(`" . TABLE_ORDER_THIS . "`.`orderDocumentsAddressCompanyCustomerNumber` = `common_recipient`.`customersKundennummer`)
			";
		}

		$sql_getAllRows .= "

				LEFT JOIN `" . TABLE_PAYMENT_TYPES . "`
				ON(`" . TABLE_ORDER_THIS . "`.`orderDocumentsPaymentType` = `" . TABLE_PAYMENT_TYPES . "`.`paymentTypesID`)

				WHERE 1
					" . $where . "
			";

		$rs_getAllRows = $dbConnection->db_query($sql_getAllRows);
		list(
			$countAllRows
		) = mysqli_fetch_array($rs_getAllRows);
	// EOF GET COUNT ALL ROWS

	// BOF OLD!!! GET SUMS OF INVOICES DEPENDING ON STATUS
		if(!1){
			#if($_COOKIE["isAdmin"]){

			$sql_total = "SELECT
							SUM(`tempTable`.`allTotal`) AS `allTotal`,
							SUM(DISTINCT `tempTable`.`allTotal`) AS `allTotal2`,

							SUM(`thisPaidValue`) AS `thisPaidTotal`,
							SUM(`thisPaidValue2`) AS `thisPaidTotal2`,
							(`thisPaidValue3`) AS `thisPaidTotal3`
				";
			if($thisDocumentTypeRelated != ""){
				$sql_total .= "
							,
							SUM(`relatedPaidValue`) AS `relatedPaidTotal`,
							SUM(`relatedPaidValue2`) AS `relatedPaidTotal2`,
							(`relatedPaidValue3`) AS `relatedPaidTotal3`
				";
			}

		// orderDocumentsIsCollectiveInvoice

			$sql_total .= "
							FROM (
								SELECT
									`" . TABLE_ORDER_THIS . "`.`orderDocumentsTotalPrice` AS `allTotal`,

									IF(`" . TABLE_ORDER_THIS_PAYMENTS . "`.`orderPaymentValue` IS NULL, '0', `" . TABLE_ORDER_THIS_PAYMENTS . "`.`orderPaymentValue`) AS `thisPaidValue`,
									SUM(`" . TABLE_ORDER_THIS_PAYMENTS . "`.`orderPaymentValue`) AS `thisPaidValue2`,
									GROUP_CONCAT(IF(`" . TABLE_ORDER_THIS_PAYMENTS . "`.`orderPaymentValue` IS NULL, '0', `" . TABLE_ORDER_THIS_PAYMENTS . "`.`orderPaymentValue`)) AS `thisPaidValue3`,
									`" . TABLE_RELATED_DOCUMENTS . "`.`relatedDocuments_" . $_REQUEST["documentType"]. "` AS `thisDocumentNumber`,

				";
			if($thisDocumentTypeRelated != ""){
				$sql_total .= "
									`" . TABLE_ORDER_THIS_PAYMENTS . "`.`orderPaymentID` AS `thisPaymentID`,

									`" . RELATED_TABLE_ORDER_THIS_PAYMENTS . "`.`orderPaymentID` AS `relatedPaymentID`,
									IF(`" . RELATED_TABLE_ORDER_THIS_PAYMENTS . "`.`orderPaymentValue` IS NULL, '0', `" . RELATED_TABLE_ORDER_THIS_PAYMENTS . "`.`orderPaymentValue`) AS `relatedPaidValue`,
									SUM(`" . RELATED_TABLE_ORDER_THIS_PAYMENTS . "`.`orderPaymentValue`) AS `relatedPaidValue2`,
									GROUP_CONCAT(IF(`" . RELATED_TABLE_ORDER_THIS_PAYMENTS . "`.`orderPaymentValue` IS NULL, '0', `" . RELATED_TABLE_ORDER_THIS_PAYMENTS . "`.`orderPaymentValue`)) AS `relatedPaidValue3`,
									`" . TABLE_RELATED_DOCUMENTS . "`.`relatedDocuments_" . $thisDocumentTypeRelated. "` AS `relatedDocumentNumber`,
				";
			}

			$sql_total .= "
									/*
									IF(`" . TABLE_ORDER_THIS . "`.`orderDocumentsPaymentCondition` = '1', DATE_ADD(`" . TABLE_ORDER_THIS . "`.`orderDocumentsDocumentDate`, INTERVAL " . PAYMENT_DEADLINE_RE_10TOA . " DAY),
										IF(`" . TABLE_ORDER_THIS . "`.`orderDocumentsPaymentCondition` = '5', DATE_ADD(`" . TABLE_ORDER_THIS . "`.`orderDocumentsDocumentDate`, INTERVAL " . PAYMENT_DEADLINE_RE_14T . " DAY),
											IF(`" . TABLE_ORDER_THIS . "`.`orderDocumentsPaymentCondition` = '6', DATE_ADD(`" . TABLE_ORDER_THIS . "`.`orderDocumentsDocumentDate`, INTERVAL " . PAYMENT_DEADLINE_RE_2PCSK10T30TN . " DAY),
												DATE_ADD(`" . TABLE_ORDER_THIS . "`.`orderDocumentsDocumentDate`, INTERVAL " . PAYMENT_DEADLINE_RE_DEFAULT . " DAY)
											)
										)
									) AS `orderDocumentsDeadline`
									*/
									IF(`" . TABLE_PAYMENT_CONDITIONS . "`.`paymentConditionsDaysLimit` = 0, DATE_ADD(`" . TABLE_ORDER_INVOICES . "`.`orderDocumentsDocumentDate`, INTERVAL " . PAYMENT_DEADLINE_RE_DEFAULT . " DAY),
										DATE_ADD(`" . TABLE_ORDER_INVOICES . "`.`orderDocumentsDocumentDate`, INTERVAL `" . TABLE_PAYMENT_CONDITIONS . "`.`paymentConditionsDaysLimit` DAY)
									) AS `orderDocumentsDeadline`

								FROM `" . TABLE_ORDER_THIS . "`
					";

			// BOF GET RELATED DEMANDS DATA
				if($thisDocumentTypeRelated != ""){
					$sql_total .= "

							LEFT JOIN `" . TABLE_RELATED_DOCUMENTS . "`
							ON(`" . TABLE_ORDER_THIS . "`.`orderDocumentsNumber` = `" . TABLE_RELATED_DOCUMENTS . "`.`relatedDocuments_" . $_REQUEST["documentType"]. "`)

							/*
							LEFT JOIN (
								SELECT
									*
									FROM `" . TABLE_RELATED_DOCUMENTS . "` AS `tempRelatedDocuments`

									WHERE 1
										GROUP BY `relatedDocuments_" . $_REQUEST["documentType"]. "`


							) AS `" . TABLE_RELATED_DOCUMENTS . "`
							ON(`" . TABLE_ORDER_THIS . "`.`orderDocumentsNumber` = `" . TABLE_RELATED_DOCUMENTS . "`.`relatedDocuments_" . $_REQUEST["documentType"]. "`)
							*/
						";

					$sql_total .= "
							LEFT JOIN `" . RELATED_TABLE_ORDER_THIS_PAYMENTS . "`
							ON(`" . TABLE_RELATED_DOCUMENTS . "`.`relatedDocuments_" . $thisDocumentTypeRelated. "` = `" . RELATED_TABLE_ORDER_THIS_PAYMENTS . "`.`orderPaymentOrderNumber`)
						";
				}

				else if(in_array($_REQUEST["statusType"], array('MA', 'M1', 'M2', 'M3')) || in_array($_REQUEST["documentType"], array('RE'))) {
					$sql_total .= "

						LEFT JOIN `" . TABLE_RELATED_DOCUMENTS . "`
						ON(`" . constant("TABLE_ORDER_" . $_REQUEST["documentType"]) . "`.`orderDocumentsNumber` = `" . TABLE_RELATED_DOCUMENTS . "`.`relatedDocuments_" . $_REQUEST["documentType"] . "`)

						/*
						LEFT JOIN (
							SELECT
									*
									FROM `" . TABLE_RELATED_DOCUMENTS . "` AS `tempRelatedDocuments`

									WHERE 1
										GROUP BY `relatedDocuments_" . $_REQUEST["documentType"]. "`

						) AS `" . TABLE_RELATED_DOCUMENTS . "`
						ON(`" . constant("TABLE_ORDER_" . $_REQUEST["documentType"]) . "`.`orderDocumentsNumber` = `" . TABLE_RELATED_DOCUMENTS . "`.`relatedDocuments_" . $_REQUEST["documentType"] . "`)
						*/

						LEFT JOIN `" . constant("TABLE_ORDER_" . $_REQUEST["statusType"]) . "`
						ON(`" . TABLE_RELATED_DOCUMENTS . "`.`relatedDocuments_" . $_REQUEST["statusType"] . "` = `" . constant("TABLE_ORDER_" . $_REQUEST["statusType"]) . "`.`orderDocumentsNumber`)
					";
				}


				$sql_total .= "
						LEFT JOIN `" . TABLE_ORDER_THIS_PAYMENTS . "`
						ON(`" . TABLE_RELATED_DOCUMENTS . "`.`relatedDocuments_" . $_REQUEST["documentType"] . "` = `" . TABLE_ORDER_THIS_PAYMENTS . "`.`orderPaymentOrderNumber`)

						LEFT JOIN `" . TABLE_PAYMENT_CONDITIONS . "`
						ON(`" . TABLE_ORDER_THIS . "`.`orderDocumentsPaymentCondition` = `" . TABLE_PAYMENT_CONDITIONS . "`.`paymentConditionsID`)
					";

				if($thisDocumentTypeRelated != "" || in_array($_REQUEST["statusType"], array('MA', 'M1', 'M2', 'M3')) || in_array($_REQUEST["documentType"], array('RE'))){
					if($_REQUEST["statusType"] != 'M1'){
						$sql_total .= "
								LEFT JOIN `" . TABLE_ORDER_M1 . "`
								ON(`" . TABLE_RELATED_DOCUMENTS . "`.`relatedDocuments_M1` = `" . TABLE_ORDER_M1 . "`.`orderDocumentsNumber`)
							";
					}
					if($_REQUEST["statusType"] != 'M2'){
						$sql_total .= "
								LEFT JOIN `" . TABLE_ORDER_M2 . "`
								ON(`" . TABLE_RELATED_DOCUMENTS . "`.`relatedDocuments_M2` = `" . TABLE_ORDER_M2 . "`.`orderDocumentsNumber`)
							";
					}
				}
			// EOF GET RELATED DEMANDS DATA

			$sql_total .= "
							LEFT JOIN `" . TABLE_PAYMENT_STATUS_TYPES . "`
							ON(`" . TABLE_ORDER_THIS . "`.`orderDocumentsStatus` = `" . TABLE_PAYMENT_STATUS_TYPES . "`.`paymentStatusTypesID`)

							WHERE 1
							" . $where . "

							GROUP BY `" . TABLE_ORDER_THIS . "`.`orderDocumentsNumber`
							/* GROUP BY CONCAT(`" . TABLE_ORDER_THIS . "`.`orderDocumentsNumber`, `" . TABLE_ORDER_THIS_PAYMENTS . "`.`orderPaymentID`) */

							" . $having . "
						) AS `tempTable`

					/*GROUP BY `thisDocumentNumber`*/
				";

			$rs_total = $dbConnection->db_query($sql_total);
			list(
				$allTotal,
				$allTotal2,

				$thisPaidTotal,
				$thisPaidTotal2,
				$thisPaidTotal3,

				$relatedPaidTotal,
				$relatedPaidTotal2,
				$relatedPaidTotal3
			) = mysqli_fetch_array($rs_total);

			$allTotal = round($allTotal, 2);

			$allTotal2 = round($allTotal2, 2);
			#$allTotal = $allTotal2;
			$thisPaidTotal = round($thisPaidTotal, 2);
			$relatedPaidTotal = round($relatedPaidTotal, 2);

			$thisPaidTotal2 = round($thisPaidTotal2, 2);
			$relatedPaidTotal2 = round($relatedPaidTotal2, 2);

			$thisPaidTotal3 = round($thisPaidTotal3, 2);
			$relatedPaidTotal3 = round($relatedPaidTotal3, 2);

			$thisPaidTotal = $thisPaidTotal2;

			$sumPaidTotal = round(($thisPaidTotal + $relatedPaidTotal), 2);
			$sumUnpaidTotal = round(($allTotal - $sumPaidTotal), 2);
		}
	// EOF OLD !!! GET SUMS OF INVOICES DEPENDING ON STATUS

	// BOF NEW GET SUMS OF INVOICES DEPENDING ON STATUS
		$sql_totalNew = "
				SELECT
					SUM(`" . TABLE_ORDER_THIS . "`.`orderDocumentsTotalPrice`) AS `allTotalNew`,

					/*
					IF(`" . TABLE_ORDER_THIS . "`.`orderDocumentsPaymentCondition` = '1', DATE_ADD(`" . TABLE_ORDER_THIS . "`.`orderDocumentsDocumentDate`, INTERVAL " . PAYMENT_DEADLINE_RE_10TOA . " DAY),
						IF(`" . TABLE_ORDER_THIS . "`.`orderDocumentsPaymentCondition` = '5', DATE_ADD(`" . TABLE_ORDER_THIS . "`.`orderDocumentsDocumentDate`, INTERVAL " . PAYMENT_DEADLINE_RE_14T . " DAY),
							IF(`" . TABLE_ORDER_THIS . "`.`orderDocumentsPaymentCondition` = '6', DATE_ADD(`" . TABLE_ORDER_THIS . "`.`orderDocumentsDocumentDate`, INTERVAL " . PAYMENT_DEADLINE_RE_2PCSK10T30TN . " DAY),
								DATE_ADD(`" . TABLE_ORDER_THIS . "`.`orderDocumentsDocumentDate`, INTERVAL " . PAYMENT_DEADLINE_RE_DEFAULT . " DAY)
							)
						)
					) AS `orderDocumentsDeadline`
					*/
					IF(`" . TABLE_PAYMENT_CONDITIONS . "`.`paymentConditionsDaysLimit` = 0, DATE_ADD(`" . TABLE_ORDER_THIS . "`.`orderDocumentsDocumentDate`, INTERVAL " . PAYMENT_DEADLINE_RE_DEFAULT . " DAY),
						DATE_ADD(`" . TABLE_ORDER_THIS . "`.`orderDocumentsDocumentDate`, INTERVAL `" . TABLE_PAYMENT_CONDITIONS . "`.`paymentConditionsDaysLimit` DAY)
					) AS `orderDocumentsDeadline`

					FROM `" . TABLE_ORDER_THIS . "`

					LEFT JOIN `" . TABLE_PAYMENT_STATUS_TYPES . "`
					ON(`" . TABLE_ORDER_THIS . "`.`orderDocumentsStatus` = `" . TABLE_PAYMENT_STATUS_TYPES . "`.`paymentStatusTypesID`)

					LEFT JOIN `" . TABLE_PAYMENT_CONDITIONS . "`
					ON(`" . TABLE_ORDER_THIS . "`.`orderDocumentsPaymentCondition` = `" . TABLE_PAYMENT_CONDITIONS . "`.`paymentConditionsID`)
			";
		if($_REQUEST["searchTotalPrice"] != "") {
			$sql_totalNew .= "
					LEFT JOIN `" . TABLE_RELATED_DOCUMENTS . "`
					ON(`" . constant("TABLE_ORDER_" . $_REQUEST["documentType"]) . "`.`orderDocumentsNumber` = `" . TABLE_RELATED_DOCUMENTS . "`.`relatedDocuments_" . $_REQUEST["documentType"] . "`)
				";

			$sql_totalNew .= "
					LEFT JOIN `" . TABLE_ORDER_M1 . "`
					ON(`" . TABLE_RELATED_DOCUMENTS . "`.`relatedDocuments_M1` = `" . TABLE_ORDER_M1 . "`.`orderDocumentsNumber`)
				";

			$sql_totalNew .= "
					LEFT JOIN `" . TABLE_ORDER_M2 . "`
					ON(`" . TABLE_RELATED_DOCUMENTS . "`.`relatedDocuments_M2` = `" . TABLE_ORDER_M2 . "`.`orderDocumentsNumber`)
				";

			/*
			LEFT JOIN `" . constant("TABLE_ORDER_" . $_REQUEST["documentType"]) . "`
			ON(`" . TABLE_RELATED_DOCUMENTS . "`.`relatedDocuments_" . $_REQUEST["documentType"] . "` = `" . constant("TABLE_ORDER_" . $_REQUEST["documentType"]) . "`.`orderDocumentsNumber`)
			*/
		}

		if($_REQUEST["searchCustomersTaxAccountID"] != "") {
			$sql_totalNew .= "
				LEFT JOIN `" . TABLE_CUSTOMERS . "` AS `" . TABLE_CUSTOMERS . "`
				ON(`" . TABLE_ORDER_THIS . "`.`orderDocumentsCustomerNumber` = `" . TABLE_CUSTOMERS . "`.`customersKundennummer`)

				LEFT JOIN `" . TABLE_CUSTOMERS . "` AS `common_salesmen`
				ON(`" . TABLE_ORDER_THIS . "`.`orderDocumentsSalesman` = `common_salesmen`.`customersID`)

				LEFT JOIN `" . TABLE_CUSTOMERS . "` AS `common_recipient`
				ON(`" . TABLE_ORDER_THIS . "`.`orderDocumentsAddressCompanyCustomerNumber` = `common_recipient`.`customersKundennummer`)
			";
		}

		$sql_totalNew .= "
					WHERE 1
						" . $where . "
						" . $having . "
			";

		$rs_totalNew = $dbConnection->db_query($sql_totalNew);

		list(
			$allTotalNew
		) = mysqli_fetch_array($rs_totalNew);

		$allTotalNew = round($allTotalNew, 2);
		$allTotal = $allTotalNew;

		// BOF SUMMEN EINZELN ADDIEREN
			if($_COOKIE["isAdmin"] == '-1'){
				$sql_totalNew2 = "
					SELECT
						`" . TABLE_ORDER_THIS . "`.`orderDocumentsTotalPrice` AS `allTotalNew`,

						/*
						IF(`" . TABLE_ORDER_THIS . "`.`orderDocumentsPaymentCondition` = '1', DATE_ADD(`" . TABLE_ORDER_THIS . "`.`orderDocumentsDocumentDate`, INTERVAL " . PAYMENT_DEADLINE_RE_10TOA . " DAY),
							IF(`" . TABLE_ORDER_THIS . "`.`orderDocumentsPaymentCondition` = '5', DATE_ADD(`" . TABLE_ORDER_THIS . "`.`orderDocumentsDocumentDate`, INTERVAL " . PAYMENT_DEADLINE_RE_14T . " DAY),
								IF(`" . TABLE_ORDER_THIS . "`.`orderDocumentsPaymentCondition` = '6', DATE_ADD(`" . TABLE_ORDER_THIS . "`.`orderDocumentsDocumentDate`, INTERVAL " . PAYMENT_DEADLINE_RE_2PCSK10T30TN . " DAY),
									DATE_ADD(`" . TABLE_ORDER_THIS . "`.`orderDocumentsDocumentDate`, INTERVAL " . PAYMENT_DEADLINE_RE_DEFAULT . " DAY)
								)
							)
						) AS `orderDocumentsDeadline`
						*/
						IF(`" . TABLE_PAYMENT_CONDITIONS . "`.`paymentConditionsDaysLimit` = 0, DATE_ADD(`" . TABLE_ORDER_INVOICES . "`.`orderDocumentsDocumentDate`, INTERVAL " . PAYMENT_DEADLINE_RE_DEFAULT . " DAY),
							DATE_ADD(`" . TABLE_ORDER_INVOICES . "`.`orderDocumentsDocumentDate`, INTERVAL `" . TABLE_PAYMENT_CONDITIONS . "`.`paymentConditionsDaysLimit` DAY)
						) AS `orderDocumentsDeadline`

						FROM `" . TABLE_ORDER_THIS . "`

						LEFT JOIN `" . TABLE_PAYMENT_STATUS_TYPES . "`
						ON(`" . TABLE_ORDER_THIS . "`.`orderDocumentsStatus` = `" . TABLE_PAYMENT_STATUS_TYPES . "`.`paymentStatusTypesID`)

						LEFT JOIN `" . TABLE_PAYMENT_CONDITIONS . "`
						ON(`" . TABLE_ORDER_THIS . "`.`orderDocumentsPaymentCondition` = `" . TABLE_PAYMENT_CONDITIONS . "`.`paymentConditionsID`)
				";
				if($_REQUEST["searchTotalPrice"] != "") {
					$sql_totalNew2 .= "
							LEFT JOIN `" . TABLE_RELATED_DOCUMENTS . "`
							ON(`" . constant("TABLE_ORDER_" . $_REQUEST["documentType"]) . "`.`orderDocumentsNumber` = `" . TABLE_RELATED_DOCUMENTS . "`.`relatedDocuments_" . $_REQUEST["documentType"] . "`)
						";

					$sql_totalNew2 .= "
							LEFT JOIN `" . TABLE_ORDER_M1 . "`
							ON(`" . TABLE_RELATED_DOCUMENTS . "`.`relatedDocuments_M1` = `" . TABLE_ORDER_M1 . "`.`orderDocumentsNumber`)
						";

					$sql_totalNew2 .= "
							LEFT JOIN `" . TABLE_ORDER_M2 . "`
							ON(`" . TABLE_RELATED_DOCUMENTS . "`.`relatedDocuments_M2` = `" . TABLE_ORDER_M2 . "`.`orderDocumentsNumber`)
						";

					/*
					LEFT JOIN `" . constant("TABLE_ORDER_" . $_REQUEST["documentType"]) . "`
					ON(`" . TABLE_RELATED_DOCUMENTS . "`.`relatedDocuments_" . $_REQUEST["documentType"] . "` = `" . constant("TABLE_ORDER_" . $_REQUEST["documentType"]) . "`.`orderDocumentsNumber`)
					*/
				}
				$sql_totalNew2 .= "
							WHERE 1
								" . $where . "
								" . $having . "
					";
				$rs_totalNew2 = $dbConnection->db_query($sql_totalNew2);
				$count = 1;
				$tempSum = 0;
				while($ds_totalNew2 = mysqli_fetch_assoc($rs_totalNew2)){
					#dd('count');
					#dd('ds_totalNew2');
					$tempSum += $ds_totalNew2["allTotalNew"];
					$count++;
				}
			}
		// EOF SUMMEN EINZELN ADDIEREN

	#dd('sql_totalNew');
	// EOF NEW GET SUMS OF INVOICES DEPENDING ON STATUS

	// BOF NEW GET SUMS OF PAYMENTS DEPENDING ON STATUS
		$sqlTemplate_totalPayments = "
			SELECT
				SUM(`totalPayments`) AS `totalPayments`

				FROM (
					SELECT

						IF(`{###TABLE_ORDER_PAYMENTS###}`.`orderPaymentValue` IS NULL, 0, `{###TABLE_ORDER_PAYMENTS###}`.`orderPaymentValue`) AS `totalPayments`,

						/*
						IF(`" . TABLE_ORDER_THIS . "`.`orderDocumentsPaymentCondition` = '1', DATE_ADD(`" . TABLE_ORDER_THIS . "`.`orderDocumentsDocumentDate`, INTERVAL " . PAYMENT_DEADLINE_RE_10TOA . " DAY),
							IF(`" . TABLE_ORDER_THIS . "`.`orderDocumentsPaymentCondition` = '5', DATE_ADD(`" . TABLE_ORDER_THIS . "`.`orderDocumentsDocumentDate`, INTERVAL " . PAYMENT_DEADLINE_RE_14T . " DAY),
								IF(`" . TABLE_ORDER_THIS . "`.`orderDocumentsPaymentCondition` = '6', DATE_ADD(`" . TABLE_ORDER_THIS . "`.`orderDocumentsDocumentDate`, INTERVAL " . PAYMENT_DEADLINE_RE_2PCSK10T30TN . " DAY),
									DATE_ADD(`" . TABLE_ORDER_THIS . "`.`orderDocumentsDocumentDate`, INTERVAL " . PAYMENT_DEADLINE_RE_DEFAULT . " DAY)
								)
							)
						) AS `orderDocumentsDeadline`
						*/
						IF(`" . TABLE_PAYMENT_CONDITIONS . "`.`paymentConditionsDaysLimit` = 0, DATE_ADD(`" . TABLE_ORDER_THIS . "`.`orderDocumentsDocumentDate`, INTERVAL " . PAYMENT_DEADLINE_RE_DEFAULT . " DAY),
							DATE_ADD(`" . TABLE_ORDER_THIS . "`.`orderDocumentsDocumentDate`, INTERVAL `" . TABLE_PAYMENT_CONDITIONS . "`.`paymentConditionsDaysLimit` DAY)
						) AS `orderDocumentsDeadline`

						FROM `" . TABLE_ORDER_THIS . "`

						LEFT JOIN `" . TABLE_PAYMENT_STATUS_TYPES . "`
						ON(`" . TABLE_ORDER_THIS . "`.`orderDocumentsStatus` = `" . TABLE_PAYMENT_STATUS_TYPES . "`.`paymentStatusTypesID`)

						LEFT JOIN `" . TABLE_PAYMENT_CONDITIONS . "`
						ON(`" . TABLE_ORDER_THIS . "`.`orderDocumentsPaymentCondition` = `" . TABLE_PAYMENT_CONDITIONS . "`.`paymentConditionsID`)

						LEFT JOIN `" . TABLE_RELATED_DOCUMENTS . "`
						ON(`" . constant("TABLE_ORDER_" . $_REQUEST["documentType"]) . "`.`orderDocumentsNumber` = `" . TABLE_RELATED_DOCUMENTS . "`.`relatedDocuments_" . $_REQUEST["documentType"] . "`)


						LEFT JOIN `{###TABLE_ORDER_PAYMENTS###}`
						ON(`" . TABLE_RELATED_DOCUMENTS . "`.`{###FIELD_ORDER_PAYMENTS###}` = `{###TABLE_ORDER_PAYMENTS###}`.`orderPaymentOrderNumber`)

			";
		if($_REQUEST["searchTotalPrice"] != "") {
			$sqlTemplate_totalPayments .= "
					LEFT JOIN `" . TABLE_ORDER_M1 . "`
					ON(`" . TABLE_RELATED_DOCUMENTS . "`.`relatedDocuments_M1` = `" . TABLE_ORDER_M1 . "`.`orderDocumentsNumber`)
				";

			$sqlTemplate_totalPayments .= "
					LEFT JOIN `" . TABLE_ORDER_M2 . "`
					ON(`" . TABLE_RELATED_DOCUMENTS . "`.`relatedDocuments_M2` = `" . TABLE_ORDER_M2 . "`.`orderDocumentsNumber`)
				";
		}

		if($_REQUEST["searchCustomersTaxAccountID"] != "") {
			$sqlTemplate_totalPayments .= "
				LEFT JOIN `" . TABLE_CUSTOMERS . "` AS `" . TABLE_CUSTOMERS . "`
				ON(`" . TABLE_ORDER_THIS . "`.`orderDocumentsCustomerNumber` = `" . TABLE_CUSTOMERS . "`.`customersKundennummer`)

				LEFT JOIN `" . TABLE_CUSTOMERS . "` AS `common_salesmen`
				ON(`" . TABLE_ORDER_THIS . "`.`orderDocumentsSalesman` = `common_salesmen`.`customersID`)

				LEFT JOIN `" . TABLE_CUSTOMERS . "` AS `common_recipient`
				ON(`" . TABLE_ORDER_THIS . "`.`orderDocumentsAddressCompanyCustomerNumber` = `common_recipient`.`customersKundennummer`)
			";
		}

		$sqlTemplate_totalPayments .= "
					WHERE 1
						" . $where . "

						GROUP BY CONCAT(`" . TABLE_RELATED_DOCUMENTS . "`.`{###FIELD_ORDER_PAYMENTS###}`, `orderPaymentID`)

						" . $having . "
				) AS `tempTable`
			";
	#dd('sqlTemplate_totalPayments')	;
		$sql_totalPayments_AB = $sqlTemplate_totalPayments;
		$sql_totalPayments_AB = preg_replace("/{###TABLE_ORDER_PAYMENTS###}/", constant('TABLE_ORDER_AB_PAYMENTS'), $sql_totalPayments_AB);
		$sql_totalPayments_AB = preg_replace("/{###FIELD_ORDER_PAYMENTS###}/", "relatedDocuments_AB", $sql_totalPayments_AB);

		$rs_totalPayments_AB = $dbConnection->db_query($sql_totalPayments_AB);
		list( $totalPayments_AB	) = mysqli_fetch_array($rs_totalPayments_AB);

		$totalPayments_AB = round($totalPayments_AB, 2);

		$sql_totalPayments_RE = $sqlTemplate_totalPayments;
		$sql_totalPayments_RE = preg_replace("/{###TABLE_ORDER_PAYMENTS###}/", constant('TABLE_ORDER_RE_PAYMENTS'), $sql_totalPayments_RE);
		$sql_totalPayments_RE = preg_replace("/{###FIELD_ORDER_PAYMENTS###}/", "relatedDocuments_RE", $sql_totalPayments_RE);

		$rs_totalPayments_RE = $dbConnection->db_query($sql_totalPayments_RE);
		list( $totalPayments_RE	) = mysqli_fetch_array($rs_totalPayments_RE);

		$totalPayments = $totalPayments_AB + $totalPayments_RE;
		$totalPayments_AB = round($totalPayments_AB, 2);
		$totalPayments_RE = round($totalPayments_RE, 2);
		$totalPayments = round($totalPayments, 2);

		$totalUnpaid = $allTotal - $totalPayments;
		$totalUnpaid = round($totalUnpaid, 2);

	// EOF NEW GET SUMS OF PAYMENTS DEPENDING ON STATUS
	?>

	<?php
		require_once('inc/headerHTML.inc.php');
		if($_REQUEST["documentType"] == 'RE') {
			$thisTitle = "Rechnungen: ";
		}
		else if($_REQUEST["documentType"] == 'AB') {
			$thisTitle = "Auftragsbest&auml;tigungen: ";
		}
		else if($_REQUEST["documentType"] == 'GU') {
			$thisTitle = "Gutschriften: ";
		}
		$thisTitle .= "" . $arrPaymentStatusTypeDatas[$thisPaymentStatusID]["paymentStatusTypesName"];
		if($_REQUEST["statusType"] == 'ALL') {
			$thisTitle .= "Alle";
		}
		else if($_REQUEST["statusType"] == 'OF_MA') {
			$thisTitle .= "&Uuml;berf&auml;llig";
		}

		if($_REQUEST["searchCustomerNumber"] != ""){
			$thisTitle .= ' - <span class="headerSelectedEntry">Kundennr.: ' . $_REQUEST["searchCustomerNumber"] . '</span>';
		}
		else if($_REQUEST["searchCustomerName"] != ""){
			$thisTitle .= ' - <span class="headerSelectedEntry">Kundenname: ' . $_REQUEST["searchCustomerName"] . '</span>';
		}
		else if($_REQUEST["searchCustomersTaxAccountID"] != ""){
			$thisTitle .= ' - <span class="headerSelectedEntry">Fibu-Nr.: ' . $_REQUEST["searchCustomersTaxAccountID"] . '</span>';
		}
		else if($_REQUEST["searchDocumentNumber"] != ""){
			$thisTitle .= ' - <span class="headerSelectedEntry">Dokument-Nr.: ' . $_REQUEST["searchDocumentNumber"] . '</span>';
		}
		else if($_REQUEST["searchCustomersOrderNumber"] != ""){
			$thisTitle .= ' - <span class="headerSelectedEntry">Kunden-Auftrags-Nr.: ' . $_REQUEST["searchCustomersOrderNumber"] . '</span>';
		}
		else if($_REQUEST["searchMonth"] != ""){
			$thisTitle .= ' - <span class="headerSelectedEntry">Datum: ' . $_REQUEST["searchMonth"] . '</span>';
		}
		else if($_REQUEST["searchWord"] != ""){
			$thisTitle .= ' - <span class="headerSelectedEntry">Suchbegriff: ' . $_REQUEST["searchWord"] . '</span>';
		}
		else if($_REQUEST["searchPaymentType"] != ""){
			$thisTitle .= ' - <span class="headerSelectedEntry">Zahlart: ' . $arrPaymentTypeDatas[$_REQUEST["searchPaymentType"]]["paymentTypesName"] . ' [' . $arrPaymentTypeDatas[$_REQUEST["searchPaymentType"]]["paymentTypesShortName"] . ']</span>';
		}
		else if($_REQUEST["searchBankAccount"] != ""){
			$thisTitle .= ' - <span class="headerSelectedEntry">Konto: ' . $arrBankAccountTypeDatas[$_REQUEST["searchBankAccount"]]["bankAccountTypesName"] . ' ' . $arrBankAccountTypeDatas[$_REQUEST["searchBankAccount"]]["bankAccountTypesAccountNumber"] . ' [' . $arrBankAccountTypeDatas[$_REQUEST["searchBankAccount"]]["bankAccountTypesShortName"] . ']</span>';
		}
		else if($_REQUEST["searchTotalPrice"] != ""){
			$thisTitle .= ' - <span class="headerSelectedEntry">Betrag: ' . urldecode($_REQUEST["searchTotalPrice"]) . ' &euro;</span>';
		}

		$headerHTML = preg_replace("/{###TITLE###}/", strip_tags($thisTitle), $headerHTML);
		echo $headerHTML;

		require_once(FILE_MENUE_TOP);
	?>

	<div id="menueSidebarToggleArea">
		<img src="layout/icons/iconToggle.png" class="buttonToggleSidebarMenue" width="10" height="10" alt="" title="Seitennavigation ein-/ausblenden" />
		<div id="menueSidebarToggleContent">
		<?php require_once(FILE_MENUE_SIDEBAR); ?>
		<div class="clear"></div>
		</div>
	</div>
	<div id="mainArea">
		<div id="mainContent">
			<div id="contentArea2">
				<div id="contentAreaElements">
					<h1><?php if(SHOW_PAGE_TITLE_ICONS) { echo '<img src="' . PATH_ICONS_MENUE_TITLES . 'documents4.png' . '" alt="" />'; } ?> <?php echo $thisTitle; ?></h1>

					<?php displayMessages(); ?>

					<div id="searchFilterArea">
						<form name="formFilterSearch" method="get" action="<?php echo $_SERVER["REDIRECT_URL"]; ?>">
							<table border="0" cellpadding="0" cellspacing="0" class="searchFilterContent">
								<tr>
									<td>
										<label for="searchCustomerNumber">Kundennr:</label>
										<input type="text" name="searchCustomerNumber" id="searchCustomerNumber" class="inputField_70" value="" />
									</td>
									<td>
										<label for="searchCustomerName">Kundenname:</label>
										<input type="text" name="searchCustomerName" id="searchCustomerName" class="inputField_100" value="" />
									</td>
									<td>
										<label for="searchDocumentNumber">Dokument-Nr:</label>
										<input type="text" name="searchDocumentNumber" id="searchDocumentNumber" class="inputField_100" value="" />
									</td>
									<td>
										<label for="searchCustomersOrderNumber">Kundenauftrags-Nr:</label>
										<input type="text" name="searchCustomersOrderNumber" id="searchCustomersOrderNumber" class="inputField_100" value="" />
									</td>
									<td>
										<label for="searchCustomersTaxAccountID">FiBu-Nr:</label>
										<input type="text" name="searchCustomersTaxAccountID" id="searchCustomersTaxAccountID" class="inputField_70" value="" />
									</td>
									<!--
									<td>
										<label for="searchPLZ">PLZ:</label>
										<input type="text" name="searchPLZ" id="searchPLZ" class="inputField_40" />
									</td>
									-->
									<td>
										<label for="searchWord">Suchbegriff:</label>
										<input type="text" name="searchWord" id="searchWord" class="inputField_70" />
									</td>
									<td>
										<label for="searchPaymentType">Zahlart:</label>
										<select name="searchPaymentType" id="searchPaymentType" class="inputSelect_120">
											<option value=""></option>
											<?php
												if(!empty($arrPaymentTypeDatas)){
													foreach($arrPaymentTypeDatas as $thisPaymentTypeKey => $thisPaymentTypeValue){
														$selected = '';
														if($thisPaymentTypeKey == $_REQUEST["searchPaymentType"]){
															#$selected = ' selected="selected" ';
														}
														echo '<option value="' . $thisPaymentTypeKey . '" ' . $selected . '>' . $thisPaymentTypeValue["paymentTypesName"] . ' (' . $thisPaymentTypeValue["paymentTypesShortName"] . ')</option>';
													}
												}
											?>
										</select>
									</td>
									<td>
										<label for="searchBankAccount">Konto:</label>
										<select name="searchBankAccount" id="searchBankAccount" class="inputSelect_120">
											<option value=""></option>
											<?php
												if(!empty($arrBankAccountTypeDatas)){
													foreach($arrBankAccountTypeDatas as $thisBankAccountTypeKey => $thisBankAccountTypeValue){
														$selected = '';
														if($thisBankAccountTypeKey == $_REQUEST["searchBankAccount"]){
															#$selected = ' selected="selected" ';
														}
														echo '<option value="' . $thisBankAccountTypeKey . '" ' . $selected . '>' . $thisBankAccountTypeValue["bankAccountTypesShortName"] . ' [' . $thisBankAccountTypeValue["bankAccountTypesName"] . ' ' . $thisBankAccountTypeValue["bankAccountTypesAccountNumber"] . ']</option>';
													}
												}
											?>
										</select>
									</td>
									<td>
										<label for="searchMonth">Datum:</label>
										<select id="searchMonth" name="searchMonth" class="inputSelect_70">
											<option value=""></option>
											<?php
												$todayYear = intval(date("Y"));
												$todayMonth = intval(date("m"));
											?>

											<?php
												// BOF ADD YEAR
												for($k = $todayYear ; $k > 2010 ; $k = $k - 1){
													$thisYear = $k;
													$thisOptionClass = 'row0';
													if($thisYear == $todayYear){ $thisOptionClass = 'row3'; }
													$thisValue = $thisYear;
													$selected = '';
													if($thisValue == $_REQUEST["searchMonth"]){
														$selected = ' selected="selected" ';
													}
													echo '<option class="' . $thisOptionClass . '" value="' . $thisValue . '" ' . $selected . '>' . $thisYear . '</option>';
												}
												// EOF ADD YEAR
											?>

											<?php
												for($k = $todayYear ; $k > 2010 ; $k = $k - 1){
													$thisYear = $k;
													$thisOptionClass = 'row0';
													if($thisYear == $todayYear){ $thisOptionClass = 'row3'; }
													else if($thisYear%2 == 0){ $thisOptionClass = 'row1'; }
													for($i = 12 ; $i > 0 ; $i = $i - 1){
														$thisMonth = $i;
														if($thisMonth < 10){ $thisMonth = "0" . $thisMonth; }
														if(($k == $todayYear && $i <= $todayMonth) || ($k < $todayYear)){
															$thisValue = $thisYear . '-' . $thisMonth;
															$selected = '';
															if($thisValue == $_REQUEST["searchMonth"]){
																$selected = ' selected="selected" ';
															}
															echo '<option class="' . $thisOptionClass . '" value="' . $thisValue . '" ' . $selected . '>' . $thisMonth . '.' . $thisYear . '</option>';
														}
													}
												}
											?>
										</select>
									</td>
									<td>
										<label for="searchTotalPrice">Betrag:</label>
										<input type="text" name="searchTotalPrice" id="searchTotalPrice" class="inputField_40" />
									</td>
									<?php
										if($userDatas["usersLogin"] == 'thorsten' || $arrGetUserRights["diplayAllResultsOnOnePage"]){
											$checked = '';
											if($_REQUEST["showAllResults"] == "1"){
												#$checked = ' checked="checked" ';
											}
											echo '
												<td>
													<input type="checkbox" name="showAllResults" value="1" title="Alle auf einer Seite anzeigen" ' . $checked . ' /> Alle anzeigen
												</td>
											';
										}
									?>
									<td>
										<input type="submit" name="submitSearch" class="inputButton0" value="Suchen" />
									</td>
								</tr>
							</table>
						</form>
					</div>

					<?php
						// BOF GET INVOICES DEPENDING ON STATUS

						/*
						if($pagesCount > 1) {
							if($_REQUEST["page"] == "" || $_REQUEST["page"] == 0) {
								$_REQUEST["page"] = 1;
							}
						}
						else {
							$_REQUEST["page"] = 1;
						}
						*/
						if($_REQUEST["page"] == "" || $_REQUEST["page"] == 0) {
							$_REQUEST["page"] = 1;
						}

						if(MAX_DOCUMENTS_PER_PAGE > 0 && $_REQUEST["showAllResults"] != "1" && ($_REQUEST["searchMonth"] == '' || strlen($_REQUEST["searchMonth"]) == 4)) {
							$showMenuePages = true;
							$sql .= " LIMIT " . (($_REQUEST["page"] - 1) * MAX_DOCUMENTS_PER_PAGE) . ", " . MAX_DOCUMENTS_PER_PAGE." ";
						}

						$rs = $dbConnection->db_query($sql);
#dd('sql');

						// OLD BOF GET ALL ROWS
							// $sql_totalRows = "SELECT FOUND_ROWS() AS `countRows`";
							// $rs_totalRows = $dbConnection->db_query($sql_totalRows);
							// list($countRows) = mysqli_fetch_array($rs_totalRows);
						// OLD EOF GET ALL ROWS

						$countRows = $countAllRows;

						$pagesCount = ceil($countRows / MAX_DOCUMENTS_PER_PAGE);

						echo '<p style="text-align:left; font-size:13px;">';
						echo '
								<span style="padding: 0 60px 0 0;"><b>Gesamt-Betrag der ' . $countRows . ' ' . preg_replace('/: (.*)/', ' (<span style="font-style:italic;">$1</span>', $thisTitle) . '):</b>
								<span style="font-weight:bold;color:#FF0000;">' . number_format($allTotal, 2, ",", ".") . ' &euro;</span></span>
							';
						echo '
								| <span style="padding: 0 60px 0 0;"><b>Summe Zahlungen</b>: <span style="font-weight:bold;color:#009900;">' . number_format($totalPayments, 2, ',', '.') . ' &euro;</span></span>
								| <span><b>Differenz</b>: <span style="font-weight:bold;color:#FF0000;">' . number_format($totalUnpaid, 2, ',', '.') . ' &euro;</span></span>
							';
						echo '</p>';
						echo '<p class="infoArea">Im GESAMT-Betrag werden Mahnbetr&auml;ge und bisher geleistete Teilzahlungen NICHT ber&uuml;cksichtigt!!!!<br />Es werden nur die Betr&auml;ge der Rechnungen summiert!!!</p>';
						if($_COOKIE["isAdmin"] == '1'){
							#echo '<p>Vergleichswert SUMME einzeln aufaddiert: <b>' . number_format($tempSum, 2, ",", ".") . '</b></p>';
						}

						$countResult = $dbConnection->db_getMysqlNumRows($rs);

						if($countResult > 0) {
							if($pagesCount > 1 && $showMenuePages) {
								include(FILE_MENUE_PAGES);
							}

							echo '<table border="0" cellpadding="0" cellspacing="0" width="100%" class="displayOrders">';

							echo '<colgroup>';
							echo '<col />';
							echo '<col />';
							echo '<col />';
							echo '<col />';
							echo '<col />';

							if(in_array($_REQUEST["statusType"], array('MA', 'M1', 'M2', 'M3', 'IK'))) {
								echo '<col />';
								echo '<col />';
							}

							echo '<col />';
							echo '<col />';
							echo '<col />';
							echo '<col />';
							#echo '<col />';
							#echo '<col />';
							#echo '<col />';
							#echo '<col />';
							echo '<col />';
							echo '<col />';
							echo '<col />';

							echo '<col />';

							#if(in_array($_REQUEST["statusType"], array('MA', 'M1', 'M2', 'M3'))) {
							if(in_array($_REQUEST["statusType"], array('MA', 'M1', 'M2', 'M3')) || in_array($_REQUEST["documentType"], array('RE'))) {
								if($arrGetUserRights["displayRemindersCosts"] == '1'){
									echo '<col />';
									echo '<col />';
								}
							}

							echo '<col />';
							echo '<col />';
							echo '</colgroup>';

							echo '<thead>';
							echo '<tr>';
							echo '<th style="width:45px;">#</th>';
							echo '<th>Dokument-Nr.</th>';
							echo '<th>Status</th>';
							echo '<th>Erstellt</th>';
							echo '<th>Frist</th>';

							if(in_array($_REQUEST["statusType"], array('MA', 'M1', 'M2', 'M3', 'IK'))) {
								echo '<th>Mahn-Datum</th>';
								echo '<th>Mahn-Frist</th>';
							}

							echo '<th>Zahldatum</th>'; // echo '<th>Zahlungsdatum</th>';
							echo '<th>K-Nr.</th>';
							echo '<th>FIBU</th>';
							echo '<th>Firma</th>';
							#echo '<th>Inhaber</th>';
							#echo '<th>Stra&szlig;e</th>';
							#echo '<th>Ort</th>';
							#echo '<th>Land</th>';
							echo '<th>Zahlungsart</th>';
							echo '<th>Konto</th>';
							echo '<th>';
							if(in_array($_REQUEST["statusType"], array('MA', 'M1', 'M2', 'M3'))) {
								echo 'RE-';
							}
							echo 'Betrag';
							echo '</th>';

							echo '<th>Skonto</th>';

							#if(in_array($_REQUEST["statusType"], array('MA', 'M1', 'M2', 'M3'))) {
							if(in_array($_REQUEST["statusType"], array('MA', 'M1', 'M2', 'M3')) || in_array($_REQUEST["documentType"], array('RE'))) {
								if($arrGetUserRights["displayRemindersCosts"] == '1'){
									echo '<th>Mahnkosten</th>';
									echo '<th>Gesamt</th>';
								}
							}

							echo '<th>Bezahlt</th>';
							echo '<th style="width:100px !important;">Sperrbetrag</th>';
							echo '<th>Info</th>';
							echo '</tr>';
							echo '</thead>';

							echo '<tbody>';
							$countRow = 0;

							$arrThisPath = parse_url($_SERVER["REQUEST_URI"]);
							$linkPath = $arrThisPath["path"];
#dd('sql');
							while($ds = mysqli_fetch_assoc($rs)) {
								#if($countRow == 0){ dd('ds'); }

								if($countRow%2 == 0){ $rowClass = 'row1'; }
								else { $rowClass = 'row4'; }

								if($ds["orderDocumentsStatus"] == "1") {
									#$rowClass = 'row5';
								}
								else if($ds["orderDocumentsStatus"] == "2") {
									$rowClass = 'row3';
								}
								else if($ds["orderDocumentsStatus"] == "4") {
									$rowClass = 'row2';
								}
								else if($ds["orderDocumentsStatus"] == "6") {
									$rowClass = '';
								}
								else if($ds["orderDocumentsStatus"] == "3") {
									$rowClass = 'row6';
								}
								else if($ds["orderDocumentsStatus"] == "10") {
									$rowClass = 'row3';
								}
								else if($ds["orderDocumentsStatus"] == "5") {
									// $rowClass = 'row10';
									$rowClass = 'row3';
								}

								echo '<tr class="' . $rowClass . '">';
								echo '<td style="text-align:right;">';
								echo '<b title="' . $ds["orderDocumentsID"] . '">' . ($countRow + 1) . '.</b>';
								echo '</td>';
								echo '<td style="background-color:#FEFFAF;">';

								if($ds["orderDocumentsIsCollectiveInvoice"] == '1'){
									echo '<span style="color:#FF0000;font-weight:bold;font-size:10px;">';
									#if($ds["orderDocumentsType"] == 'AB'){ echo 'Die AB ist Teil einer SAMMELRECHNUNG'; }
									if($ds["orderDocumentsType"] == 'AB'){ echo ' in SAMMELRECHNUNG'; }
									else { echo 'SAMMELRECHNUNG'; }
									echo '</span><br />';
								}

								echo '<b><span style="white-space:nowrap;" title="">';
								echo '<a href="' . $arrDocumentUrl[$_REQUEST["documentType"]] . '&amp;searchDocumentNumber=' . $ds["orderDocumentsNumber"] . '">' . $ds["orderDocumentsNumber"] . '</a>';
								#echo '<img src="layout/icons/iconSlideDown.png" width="13" height="7" class="buttonOpenDocumentDetails" alt="x" title="y" />';
								echo '</span></b>';

								echo '</td>';

								echo '<td style="white-space:nowrap;">';
								echo '<span class="toolItem">';
								echo '<b>' . $arrPaymentStatusTypeDatas[$ds["orderDocumentsStatus"]]["paymentStatusTypesName"] . '</b>';

								$thisDemandPrice = 0;
								$thisDemandTotalPrice = 0;
								if(in_array($_REQUEST["statusType"], array('MA', 'M1', 'M2', 'M3')) || in_array($_REQUEST["documentType"], array('RE'))) {
									if($_REQUEST["documentType"] == 'RE'){
										$thisDemandPrice = ($ds["relatedDocumentsInterestPrice"] + $ds["relatedDocumentsChargesPrice"]);
										$thisDemandTotalPrice = ($ds["orderDocumentsTotalPrice"] + $ds["relatedDocumentsInterestPrice"] + $ds["relatedDocumentsChargesPrice"]);
									}
									else{
										$thisDemandPrice = ($ds["orderDocumentsInterestPrice"] + $ds["orderDocumentsChargesPrice"]);
										$thisDemandTotalPrice = ($ds["orderDocumentsTotalPrice"] + $ds["orderDocumentsInterestPrice"] + $ds["orderDocumentsChargesPrice"]);
									}
								}

								$paymentPaidSum = 0;
								$paymentSkontoSum = 0;
								$arrAllPaymentsSum = array();
								$arrPaymentsAll = array();
								$arrRelatedPaymentsAll = array();
								if($ds["paymentsAll"] != ''){
									$arrPaymentsAll = explode(";", $ds["paymentsAll"]);
								}
								if($ds["relatedPaymentsAll"] != ''){
									$arrRelatedPaymentsAll = explode(";", $ds["relatedPaymentsAll"]);
								}

								if(!empty($arrPaymentsAll) && !empty($arrRelatedPaymentsAll)){
									$arrPaymentsAll = array_merge($arrPaymentsAll, $arrRelatedPaymentsAll);
								}
								else if(!empty($arrPaymentsAll)){
									$arrPaymentsAll = $arrPaymentsAll;
								}
								else if(!empty($arrRelatedPaymentsAll)){
									$arrPaymentsAll = $arrRelatedPaymentsAll;
								}

								if(!empty($arrPaymentsAll)){
									#dd('arrPaymentsAll');
									foreach($arrPaymentsAll as $thisPaymentsAllData){
										$arrPaymentsAllData = explode(":", $thisPaymentsAllData);
										$paymentPaidSum += $arrPaymentsAllData[1];
										$paymentSkontoSum += $arrPaymentsAllData[2];
									}
								}
								// if($ds["orderDocumentsStatus"] != '2' && $ds["orderDocumentsStatus"] != '3') {
								if($arrGetUserRights["setInvoicePaymentStatus"]){
									$thisRel = '';
									$thisRel .= $ds["orderDocumentsNumber"];
									$thisRel .= '#';
									$thisRel .= $ds["orderDocumentsID"];
									$thisRel .= '#';
									$thisRel .= $ds["orderDocumentsStatus"];
									$thisRel .= '#';
									if($thisDemandTotalPrice > 0){
										$thisRel .= number_format($thisDemandTotalPrice, 2, ',', '');
									}
									else {
										$thisRel .= number_format($ds["orderDocumentsTotalPrice"], 2, ',', '');
									}
									$thisRel .= '#';
									#$thisRel .= number_format($ds["paymentPaidSum"], 2, ',', '');

									#$thisPaymentSum = $ds["paymentPaidSum"];
									$thisPaymentSum = $paymentPaidSum;

									if($thisPaymentSum > 0){
										$thisRel .= $thisPaymentSum;
									}
									else if($ds["relatedPaymentPaidSum"] > 0){
										$thisRel .= $ds["relatedPaymentPaidSum"];
									}
									#echo '<img src="layout/icons/iconEdit.gif" class="buttonChangeInvoiceStatus" rel="' . $ds["orderDocumentsNumber"] . '#' . $ds["orderDocumentsID"] . '#' . $ds["orderDocumentsStatus"] . '#' . number_format($ds["orderDocumentsTotalPrice"], 2, ',', '') . '#' . number_format($ds["paymentPaidSum"], 2, ',', '') . '" width="16" height="16" alt="Status der Rechnung &auml;ndern" title="Status der Rechnung &auml;ndern" />';
									if(($ds["orderDocumentsIsCollectiveInvoice"] == "1" && $ds["orderDocumentsType"] != "AB") || $ds["orderDocumentsIsCollectiveInvoice"] != "1"){
										echo '<img src="layout/icons/iconEdit.gif" class="buttonChangeInvoiceStatus" rel="' . $thisRel . '" width="16" height="16" alt="Status der Rechnung &auml;ndern" title="Status der Rechnung &auml;ndern" />';
									}
								}
								echo '</span>';
								echo '</td>';
								echo '<td>';
								#echo formatDate($ds["orderDocumentsProcessingDate"], 'display');
								echo formatDate($ds["orderDocumentsDocumentDate"], 'display');
								echo '</td>';
								echo '<td>';
								echo formatDate($ds["orderDocumentsDeadline"], 'display');
								echo '</td>';

								if(in_array($_REQUEST["statusType"], array('MA', 'M1', 'M2', 'M3', 'IK'))) {
									echo '<td>';
									echo formatDate($ds["orderDocumentsDocumentDateDemand"], 'display');
									echo '</td>';
									echo '<td>';
									echo formatDate($ds["orderDocumentDeadlineDemand"], 'display');
									echo '</td>';
								}

								echo '<td style="text-align:right; white-space:nowrap;">';
									#echo formatDate($ds["orderDocumentsPaymentDate"], 'display');
									$arrTemp = explode('###', $ds["paymentDatas"]);
									$arrPaymentDates = array();
									$arrPaymentDatas = array();
									if(!empty($arrTemp)) {
										foreach($arrTemp as $thisKey => $thisValue) {
											$arrTemp2 = explode(':::', $thisValue);
											if(trim($thisValue) != ''){
												$arrPaymentDates[$thisKey] = $arrTemp2[0];
												$arrPaymentDatas[$arrTemp2[0]] = $arrTemp2[1];
												$arrPaymentSkontoDatas[$arrTemp2[0]] = $arrTemp2[2];
												$arrPaymentBankAccountID[$arrTemp2[0]] = $arrTemp2[3];
												$arrPaymentNotices[$arrTemp2[0]] = $arrTemp2[4];
											}
										}
										 rsort($arrPaymentDates);
									}

									$arrTemp = explode('###', $ds["relatedPaymentDatas"]);
									$arrRelatedPaymentDates = array();
									$arrRelatedPaymentDatas = array();
									if(!empty($arrTemp)) {
										foreach($arrTemp as $thisKey => $thisValue) {
											$arrTemp2 = explode(':::', $thisValue);
											if(trim($thisValue) != ''){
												$arrRelatedPaymentDates[$thisKey] = $arrTemp2[0];
												$arrRelatedPaymentDatas[$arrTemp2[0]] = $arrTemp2[1];
												$arrRelatedPaymentSkontoDatas[$arrTemp2[0]] = $arrTemp2[2];
												$arrPaymentBankAccountID[$arrTemp2[0]] = $arrTemp2[3];
												$arrPaymentNotices[$arrTemp2[0]] = $arrTemp2[4];
											}
										}
										 rsort($arrRelatedPaymentDates);
									}

									$thisPaymentDate = $arrPaymentDates[0];
									if($thisPaymentDate == ''){
										$thisPaymentDate = $arrRelatedPaymentDates[0];
									}

									echo formatDate($thisPaymentDate, 'display');
									echo ' <img src="layout/icons/iconInfo.png" class="buttonPaymentInfo" rel="' . $ds["orderDocumentsNumber"] . '" title="Zahlungsdetails" alt="" width="10" height="10" />';
									if(!empty($arrPaymentDates)){
										// echo ' <img src="layout/icons/iconInfo.png" class="buttonPaymentInfo" rel="' . $ds["orderDocumentsNumber"] . '" title="Zahlungsdetails" alt="" width="10" height="10" />';
										/*
										echo '<div class="orderDetailsContent" style="display:none; visibility:hidden;">';
										echo '<table cellpadding="0" cellspacing="0" width="100%" class="border">';
										echo '<tr>';
										echo '<thead>';
										echo '<th style="width:45px;">#</th>';
										echo '<th style="width:100px;">Dokument-Nummer</th>';
										echo '<th style="width:100px;">Zahl-Datum</th>';
										echo '<th style="width:100px;">Betrag</th>';
										echo '<th style="width:100px;">Bankverbindung</th>';
										echo '<th>Notiz</th>';
										echo '</thead>';
										echo '</tr>';
										$count = 0;
										foreach($arrPaymentDates as $thisKey => $thisValue){
											echo '<tr>';
											echo '<td><b>' . ($count + 1) . '.</b> </td>';
											echo '<td>';
											echo '...';
											echo '</td>';
											echo '<td>';
											echo formatDate($arrPaymentDates[$thisKey], 'display');
											echo '</td>';
											echo '<td>';
											echo number_format($arrPaymentDatas[$thisValue], 2, ',', '') . ' &euro;';
											echo '</td>';
											echo '<td>';
											echo $arrBankAccountTypeDatas[$arrPaymentBankAccountID[$thisValue]]["bankAccountTypesName"] . ' (' . $arrBankAccountTypeDatas[$arrPaymentBankAccountID[$thisValue]]["bankAccountTypesAccountNumber"] . ')';
											echo '</td>';
											echo '<td>';
											echo $arrPaymentNotices[$thisValue];
											echo '</td>';
											echo '</tr>';
											$count++;
										}
										echo '</table>';
										echo '</div>';
									*/
									}
								echo '</td>';

								echo '<td>';
								//$thisSalesmenKundennummer = $ds["salesmenKundennummer"];
								$thisSalesmenKundennummer = $ds["orderDocumentsAddressCompanyCustomerNumber"];

								if(in_array($thisSalesmenKundennummer, $arrMandatoriesCustomerNumbers) || $thisSalesmenKundennummer == ''){
									$thisSalesmenKundennummer = $ds["orderDocumentsCustomerNumber"];
								}
								echo '<span title="Rechnungs-Empf&auml;nger Kundendaten ansehen"><b><a href="' . PAGE_EDIT_CUSTOMER . '?editCustomerNumber=' . $thisSalesmenKundennummer . '">' . $thisSalesmenKundennummer . '</a></b></span>';
								echo '<div style="font-size:10px;border-top:1px dotted #333;">';
								echo '<span title="Lieferungs-Empf&auml;nger Kundendaten ansehen" style="padding-left:10px;"><a href="' . PAGE_EDIT_CUSTOMER . '?editCustomerNumber=' . $ds["orderDocumentsCustomerNumber"] . '">' . $ds["orderDocumentsCustomerNumber"] . '</a></span>';
								#echo '<span title="Kundendaten ansehen"><a href="' . getCustomersSourceTableFromCustomerNumber($ds["orderDocumentsCustomerNumber"]) . '?editCustomerNumber=' . $ds["orderDocumentsCustomerNumber"] . '">' . $ds["orderDocumentsCustomerNumber"] . '</span></a>';
								echo '</div>';
								echo '</td>';

								echo '<td>';
								$thisRecipientTaxAccountID = $ds["recipientTaxAccountID"];

								echo '<span style="font-size:10px;">' . $thisRecipientTaxAccountID . '</span>';
								echo '<div style="font-size:10px;border-top:1px dotted #333;">';
								echo '<span style="padding-left:10px;">' . $ds["customersTaxAccountID"] . '</span>';
								echo '</div>';

								echo '</td>';

								#echo '<td>';
								#echo $ds["orderDocumentsSalesman"];
								#echo '</td>';

								echo '<td style="white-space:nowrap;">';
								echo '<b>' . htmlentities(($ds["orderDocumentsAddressCompany"])) . '</b>';

								echo '<div style="font-size:10px;border-top:1px dotted #333;padding-left:10px;">';
								echo htmlentities(($ds["customersFirmenname"]));
								echo ' &bull; ';
								echo $ds["customersCompanyPLZ"] . ' ' . htmlentities(($ds["customersCompanyOrt"]));
								echo '</div>';

								echo '</td>';

								#echo '<td style="white-space:nowrap;">';
								#echo $ds["orderDocumentsAddressStreet"] . ' ' . $ds["orderDocumentsAddressStreetNumber"];
								#echo '</td>';
								#echo '<td style="white-space:nowrap;">';
								#echo $ds["orderDocumentsAddressZipcode"] . ' ' . $ds["orderDocumentsAddressCity"];
								#echo '</td>';
								#echo '<td>';
								#echo $arrCountryTypeDatas[$ds["orderDocumentsAddressCountry"]]["countries_iso_code_3"];
								#echo '</td>';

								echo '<td style="white-space:nowrap;font-size:10px;">';
								echo preg_replace("/,/", "<br />", $ds["paymentConditionsName"]) . ', ' . $ds["paymentTypesShortName"];
								echo '</td>';

								echo '<td style="white-space:nowrap;font-size:10px;font-weight:bold;">';
								echo $arrBankAccountTypeDatas[$ds["orderDocumentsBankAccount"]]["bankAccountTypesShortName"] . ': ' . $arrBankAccountTypeDatas[$ds["orderDocumentsBankAccount"]]["bankAccountTypesAccountNumber"];
								echo '</td>';

								echo '<td style="white-space:nowrap; text-align:right;background-color:#FEFFAF;">';
								echo '<b>' . number_format($ds["orderDocumentsTotalPrice"], 2, ',', '') . ' &euro; </b>';
								echo '</td>';

								echo '<td style="white-space:nowrap; text-align:right;">';
								#dd('arrPaymentSkontoDatas');
								#$thisPaymentSkontoSum = $ds["paymentSkontoSum"];
								#if($thisPaymentSkontoSum == 0 && $ds["relatedPaymentSkontoSum"] != 0){
									#$thisPaymentSkontoSum = $ds["relatedPaymentSkontoSum"];
								#}
								#echo '<span style="font-style:italic;">' . number_format($ds["orderDocumentsSkonto2"], 2, ',', '') . ' &euro;' . '</span>';
								#$thisPaymentSkontoSum = $ds["paymentSkontoSum"];
								$thisPaymentSkontoSum = $paymentSkontoSum;
								if($paymentSkontoSum == 0){
									if($ds["relatedPaymentSkontoSum"] != ''){
										$thisPaymentSkontoSum = $ds["relatedPaymentSkontoSum"];
									}
								}
								echo '<span style="font-style:italic;">' . number_format($thisPaymentSkontoSum, 2, ',', '') . ' &euro;' . '</span>';
								echo '</td>';

								#if(in_array($_REQUEST["statusType"], array('MA', 'M1', 'M2', 'M3'))) {
								if(in_array($_REQUEST["statusType"], array('MA', 'M1', 'M2', 'M3')) || in_array($_REQUEST["documentType"], array('RE'))) {
									if($arrGetUserRights["displayRemindersCosts"] == '1'){
										$thisCellStyle = '';
										if($thisDemandPrice > 0){
											$thisCellStyle = 'background-color:#FFDFDF;font-weight:bold;';
										}

										echo '<td style="text-align:right;white-space:nowrap;' . $thisCellStyle . '">';
										echo convertDecimal($thisDemandPrice, 'display') . ' &euro;';
										echo '</td>';

										echo '<td style="text-align:right;white-space:nowrap;' . $thisCellStyle . '">';
										echo convertDecimal($thisDemandTotalPrice, 'display') . ' &euro;';
										echo '</td>';
									}
								}

								echo '<td style="white-space:nowrap; text-align:right;background-color:#FEFFAF;">';

								#$thisPaymentSum = $ds["paymentPaidSum"];
								$thisPaymentSum = $paymentPaidSum;
								if($thisPaymentSum == 0){
									if($ds["relatedPaymentPaidSum"] != ''){
										$thisPaymentSum = $ds["relatedPaymentPaidSum"];
									}
								}
								echo number_format($thisPaymentSum, 2, ',', '') . ' &euro;';

								if((number_format($thisPaymentSum, 2) > number_format($thisDemandTotalPrice, 2)) && ($thisDemandTotalPrice > 0)){
									echo ' <img src="layout/icons/iconAttention.png" class="signAttention" width="14" height="14" alt="Achtung: Zuviel bezahlt?" title="Zuviel bezahlt?">';
								}
								echo '</td>';


								echo '<td style="white-space:nowrap;">';

								if($arrGetUserRights["setInvoicePaymentStatus"]){
									if($arrBankAccountTypeDatas[$ds["orderDocumentsBankAccount"]]["bankAccountTypesShortName"] == "FAKT"){
										echo '<form name="formUpdateSperrbetrag_' . $countRow . '" action="" method="">';
										$checked = '';
										if($ds["relatedDocuments_SperrbetragFAKT"] != '1'){
											$thisSperrbetragText = "";
										}
										else{
											$checked = ' checked="checked" ';
											$thisSperrbetragText = '<img src="layout/icons/iconAttention.png" alt="" width="16" height="16">' . '<b>' . FAKT_BLOCKED_AMOUNT . ' %</b>';
										}
										echo $thisSperrbetragText;

										echo '<input type="checkbox" name="updateSperrbetragValue" id="updateSperrbetragValue" value="1" ' . $checked . ' />';
										echo '<input type="hidden" name="updateSperrbetragDocumentNumber" id="updateSperrbetragDocumentNumber" value="' . $ds["orderDocumentsNumber"] . '" />';

										/*
										if($_REQUEST["searchDocumentNumber"] != ""){
											echo '<input type="hidden" name="searchSperrbetragDocumentNumber" id="searchSperrbetragDocumentNumber" value="' . $_REQUEST["searchDocumentNumber"] . '" />';
										}
										if($_REQUEST["searchCustomerNumber"] != ""){
											echo '<input type="hidden" name="searchSperrbetragCustomerNumber" id="searchSperrbetragCustomerNumber" value="' . $_REQUEST["searchCustomerNumber"] . '" />';
										}
										*/
										echo '<input class="buttonSaveStatus" src="layout/icons/iconSave.png" title="Sperrbetrag speichern" alt="Sperrbetrag speichern" width="16" type="image" height="16">';
										echo '</form>';
									}
								}
								else {
									if($arrBankAccountTypeDatas[$ds["orderDocumentsBankAccount"]]["bankAccountTypesShortName"] == "FAKT"){
										if($ds["relatedDocuments_SperrbetragFAKT"] != '1'){
											$thisSperrbetragText = "";
										}
										else{
											$checked = ' checked="checked" ';
											$thisSperrbetragText = '<img src="layout/icons/iconAttention.png" alt="" width="16" height="16">' . '<b>' . FAKT_BLOCKED_AMOUNT . ' %</b>';
										}
										echo $thisSperrbetragText;
									}
								}

								echo '</td>';

								echo '<td style="white-space:nowrap;">';
							
								echo '<span class="toolItem">';
								echo '<img src="layout/icons/mailAttachement.png" class="buttonSendDocument" rel="' . urlencode(basename($ds["orderDocumentsDocumentPath"])) . '#' . $_REQUEST["documentType"] . '" width="16" height="16" title="Dokument &quot;' . $ds["orderDocumentsNumber"] . '&quot; direkt per Mail versenden" alt="Dokument versenden" />';
								echo '</span>';

								echo '<span class="toolItem">';
								echo '<span class="mailcontact">';
								if($_COOKIE["isAdmin"] == '1'){
									echo '<a href="mailto:' . $ds["useMailAdress"] . '?body=&subject=' . basename($ds["orderDocumentsDocumentPath"]) . '"><img src="layout/icons/iconMail.gif" width="16" height="14" alt="Mail an Empf&auml;nger" title="Mail an Empf&auml;nger schreiben" /></a>';
								}
								else {
									echo '<a href="mailto:' . $ds["useMailAdress"] . '"><img src="layout/icons/iconMail.gif" width="16" height="14" alt="Mail an Empf&auml;nger" title="Mail an Empf&auml;nger schreiben" /></a>';
								}
								echo '</span>';
								echo '</span>';

								echo '<span class="toolItem">';
								echo '<a href="' . $linkPath . '?downloadFile=' . basename($ds["orderDocumentsDocumentPath"]) . '&amp;thisDocumentType=' . $_REQUEST["documentType"] . '"><img src="layout/icons/iconPDF.gif" width="16" height="16" alt="' . $arrDocumentTypeDatas[$_REQUEST["documentType"]]["createdDocumentsTypesName"]. ' Dokument herunterladen" title="Dokument herunterladen" /></a>';
								echo '</span>';
								
//								echo '<span class="toolItem">';
//echo '<a href="?downloadFile=' . basename($ds["exportFactoringData_filepath"]).'&amp;downloadType=EXPORT"><img src="layout/icons/iconEXCEL.gif" height="16" width="16" alt="Download" title="' . $ds["exportFactoringData_filepath"] . ' &ouml;ffnen"/></a>';
//								echo '</span>';		

								echo '<div class="toolItem">';
								echo '<img src="layout/icons/linkedDocuments.png" class="buttonShowLinkedDocuments" width="16" height="16" alt="Verkn&uuml;pfte Dokumente" title="Verkn&uuml;pfte Dokumente anzeigen" />';
								echo '<div class="displayNoticeArea">';

								/*
								if(!empty($arrDocumentTypeDatas)){
									echo '<ul>';
									foreach($arrDocumentTypeDatas as $thisDocumentTypeDatasKey => $thisDocumentTypeDatasValue){
										if($ds["relatedDocuments_" . $thisDocumentTypeDatasKey] != ""){
											echo '<li>';
											echo '<a href="' . $linkPath . '?downloadFile=' . basename($ds["relatedDocuments_" . $thisDocumentTypeDatasKey]) . '_KNR-' . $ds["orderDocumentsCustomerNumber"] . '.pdf' . '&amp;thisDocumentType=' . $thisDocumentTypeDatasKey . '"><img src="layout/icons/iconPDF.gif" width="16" height="16" alt="Dokument herunterladen" title="' . $arrDocumentTypeDatas[$thisDocumentTypeDatasKey]["createdDocumentsTypesName"]. ' Dokument herunterladen" /></a>';
											echo ' ';
											echo $ds["relatedDocuments_" . $thisDocumentTypeDatasKey] . '';
											echo '</li>';
										}
									}
									echo '</ul>';
								}
								*/

								$arrThisRelatedDocuments = array();

								if(!empty($arrDocumentTypeDatas)){
									foreach($arrDocumentTypeDatas as $thisDocumentTypeDatasKey => $thisDocumentTypeDatasValue){
										if(preg_match("/;/", $ds["relatedDocuments_" . $thisDocumentTypeDatasKey])){
											#relatedDocuments_LS] => LS-1511000226;LS-1511000227
											$arrThisRelatedDocuments_Temp = explode(";", $ds["relatedDocuments_" . $thisDocumentTypeDatasKey]);
											if(!empty($arrThisRelatedDocuments)){
												$arrThisRelatedDocuments = array_merge($arrThisRelatedDocuments, $arrThisRelatedDocuments_Temp);
											}
											else if(!empty($arrThisRelatedDocuments_Temp)){
												$arrThisRelatedDocuments = $arrThisRelatedDocuments_Temp;
											}
										}
										else if($ds["relatedDocuments_" . $thisDocumentTypeDatasKey] != ""){
											$arrThisRelatedDocuments[] = $ds["relatedDocuments_" . $thisDocumentTypeDatasKey];
										}
									}
								}
								#dd('ds');
								if($ds["relatedDocuments_collectiveABs"] != ""){
									$arrThisRelatedDocuments_collectiveABs = explode(";", $ds["relatedDocuments_collectiveABs"]);
									if(!empty($arrThisRelatedDocuments)){
										$arrThisRelatedDocuments = array_merge($arrThisRelatedDocuments, $arrThisRelatedDocuments_collectiveABs);
									}
									else if(!empty($arrThisRelatedDocuments_collectiveABs)){
										$arrThisRelatedDocuments = $arrThisRelatedDocuments_collectiveABs;
									}
								}

								$arrThisRelatedDocuments = array_unique($arrThisRelatedDocuments);
								sort($arrThisRelatedDocuments);
#dd('arrThisRelatedDocuments');
								if(!empty($arrThisRelatedDocuments)){
									$arrTempThisRelatedDocuments = $arrThisRelatedDocuments;
									$arrThisRelatedDocuments = array();
									foreach($arrTempThisRelatedDocuments as $thisTempThisRelatedDocument){
										$arrThisRelatedDocuments[substr($thisTempThisRelatedDocument, 0, 2)][] = $thisTempThisRelatedDocument;
									}
								}

								if(!empty($arrThisRelatedDocuments)){
									if($ds["orderDocumentsIsCollectiveInvoice"] == '1'){
										echo '<span style="color:#FF0000;font-weight:bold;font-size:10px;">';
										if($ds["orderDocumentsType"] == 'AB'){ echo 'Die AB ist Teil einer SAMMELRECHNUNG'; }
										else { echo 'SAMMELRECHNUNG'; }
										echo '</span><br />';
									}

									foreach($arrDocumentTypeDatas as $thisRelatedDocumentsKey => $thisRelatedDocumentsValue){
										if(!empty($arrThisRelatedDocuments[$thisRelatedDocumentsKey])){
											$countItem = 0;
											$thisItemStyle = 'padding-top: 4px;padding-bottom: 4px;border-top: 1px dotted #999;';
											echo '<ul style="' . $thisItemStyle . '">';
											foreach($arrThisRelatedDocuments[$thisRelatedDocumentsKey] as $thisRelatedDocumentsKey => $thisRelatedDocumentsValue){
												echo '<li>';
												echo '<a href="' . $linkPath . '?downloadFile=' . basename($thisRelatedDocumentsValue) . '_KNR-' . $ds["orderDocumentsCustomerNumber"] . '.pdf' . '&amp;thisDocumentType=' . substr($thisRelatedDocumentsValue, 0, 2) . '">';
												echo '<img src="layout/icons/iconPDF.gif" width="16" height="16" alt="Dokument herunterladen" title="' . $thisRelatedDocumentsValue. ' Dokument herunterladen" />';
												echo '</a>';

												echo ' ';
												$thisRelatedDocumentType = substr($thisRelatedDocumentsValue, 0, 2);
												echo '<a href="' . $arrDocumentUrl[$thisRelatedDocumentType] . '?searchDocumentNumber=' . $thisRelatedDocumentsValue . '">';
												echo $thisRelatedDocumentsValue . '';
												echo '</a>';

												echo '</li>';
												$countItem++;
											}
											echo '</ul>';
										}
									}
								}

								echo '</div>';
								echo '</div>';

								echo '</td>';
								echo '</tr>';

								$countRow++;
							}
							echo '</tbody>';
							echo '</table>';

							if($pagesCount > 1 && $showMenuePages) {
								include(FILE_MENUE_PAGES);
							}
						}
						else {
							echo '<p class="warningArea">Es wurden keine Daten gefunden.</p>';
						}
						// EOF GET INVOICES DEPENDING ON STATUS
					?>
				</div>
			</div>
			<div class="clear"></div>
		</div>
		<div class="clear"></div>
	</div>

<script language="javascript" type="text/javascript">
	$(document).ready(function() {
		$(function() {
			$( "#datepicker" ).datepicker();
			$('.buttonPaymentInfo').css('cursor', 'pointer');
			$('.buttonPaymentInfo').click(function () {
				loadPaymentDetails($(this), $(this).attr('rel'), '<?php echo $linkPath; ?>');
				//loadOrderDetails($(this), '<?php echo BASEPATH; ?>', 'Details der Zahlung');
			});
		});
		colorRowMouseOver('.displayOrders tbody tr');
		$('#searchCustomerNumber').keyup(function () {
			// loadSuggestions('searchCustomerNumber', [{'triggerElement': '#searchCustomerNumber', 'fieldNumber': '#searchCustomerNumber', 'fieldName': '#searchCustomerName', 'fieldCity': '', 'fieldZipCode': ''}], 1);
		});
		$('#searchCustomerName').keyup(function () {
			// loadSuggestions('searchCustomerName', [{'triggerElement': '#searchCustomerName', 'fieldNumber': '#searchCustomerNumber', 'fieldName': '#searchCustomerName', 'fieldCity': '', 'fieldZipCode': ''}], 1);
		});

		$('.buttonChangeInvoiceStatus').click(function() {
			var documentData = $(this).attr('rel');
			setInvoicePaymentStatus($(this), documentData, '<?php echo htmlentities($_SERVER["REQUEST_URI"]); ?>', <?php echo json_encode($arrPaymentStatusTypeDatas); ?>, <?php echo json_encode($arrBankAccountTypeDatasActive); ?>);
		});

		$('.buttonShowLinkedDocuments').click(function () {
			loadNotice($(this));
		});

		$('.buttonOpenDocumentDetails').live('click', function(){
			loadNotice($(this).parent().parent().find('.buttonShowLinkedDocuments'));
		});

		$('.buttonSendDocument').click(function() {
			var mailDocumentFilename = $(this).attr('rel');
			var mailDocumentCustomerNumber = '';
			var mailDocumentRecipient = $(this).parent().parent().find('.mailcontact a').attr('href');
			mailDocumentRecipient = mailDocumentRecipient.replace('mailto:', '');
			mailDocumentRecipient = removeAllParamsFromUrl('', mailDocumentRecipient);
			sendAttachedDocument($(this), mailDocumentFilename, mailDocumentCustomerNumber, '<?php echo $_REQUEST["editID"]; ?>', '', '<?php echo htmlentities($_SERVER["REQUEST_URI"]); ?>', mailDocumentRecipient);
		});
		$('.buttonToggleSidebarMenue').click(function() {
			$('#menueSidebarToggleContent').toggle();
		});

		$('.signAttention').attr('title', 'Achtung!!!');
	});
</script>
<?php require_once('inc/footerHTML.inc.php'); ?>