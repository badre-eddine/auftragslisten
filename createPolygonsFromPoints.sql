SELECT

`geodb_coordinates`.`lat` AS `latitude`,
`geodb_coordinates`.`lon` AS `longitude`,
`geodb_textdata`.`text_val`,
`geodb_textdata2`.`text_val` AS `text_val2`,
`common_zipcodesCities`.`place_name`,
`common_zipcodesCities`.`postal_code`,
`common_zipcodesCities`.`firstOrderSubdivision_stateName` AS `region_1`,
`common_zipcodesCities`.`secondOrderSubdivision_stateName` AS `region_2`,
`common_zipcodesCities`.`thirdOrderSubdivision_stateName` AS `region_3`

FROM `geodb_textdata`

LEFT JOIN `geodb_textdata` AS `geodb_textdata2`
ON(`geodb_textdata`.`loc_id` = `geodb_textdata2`.`loc_id`)

LEFT JOIN `geodb_locations`
ON (`geodb_textdata`.`loc_id` = `geodb_locations`.`loc_id`)

LEFT JOIN `geodb_coordinates`
ON(`geodb_textdata`.`loc_id` = `geodb_coordinates`.`loc_id`)

LEFT JOIN `common_zipcodesCities`
ON(`geodb_textdata`.`text_val` = `common_zipcodesCities`.`postal_code`)

WHERE
	`geodb_textdata`.`text_type` = '500300000'
		AND `geodb_textdata2`.`text_type` = '500100000'
		AND (
			`geodb_textdata`.`text_val` LIKE '48%'
		)
GROUP BY CONCAT(`latitude`, '-', `longitude`) ;

-- --------------------------------------------------------------

SELECT

MIN(`geodb_coordinates`.`lat`) AS `latitude`,
MIN(`geodb_coordinates`.`lon`) AS `longitude`

FROM `geodb_textdata`

LEFT JOIN `geodb_textdata` AS `geodb_textdata2`
ON(`geodb_textdata`.`loc_id` = `geodb_textdata2`.`loc_id`)

LEFT JOIN `geodb_locations`
ON (`geodb_textdata`.`loc_id` = `geodb_locations`.`loc_id`)

LEFT JOIN `geodb_coordinates`
ON(`geodb_textdata`.`loc_id` = `geodb_coordinates`.`loc_id`)

LEFT JOIN `common_zipcodesCities`
ON(`geodb_textdata`.`text_val` = `common_zipcodesCities`.`postal_code`)

WHERE
	`geodb_textdata`.`text_type` = '500300000'
		AND `geodb_textdata2`.`text_type` = '500100000'
		AND (
			`geodb_textdata`.`text_val` LIKE '48%'
		);

-- --------------------------------------------------------------

SELECT

MAX(`geodb_coordinates`.`lat`) AS `latitude`,
MAX(`geodb_coordinates`.`lon`) AS `longitude`

FROM `geodb_textdata`

LEFT JOIN `geodb_textdata` AS `geodb_textdata2`
ON(`geodb_textdata`.`loc_id` = `geodb_textdata2`.`loc_id`)

LEFT JOIN `geodb_locations`
ON (`geodb_textdata`.`loc_id` = `geodb_locations`.`loc_id`)

LEFT JOIN `geodb_coordinates`
ON(`geodb_textdata`.`loc_id` = `geodb_coordinates`.`loc_id`)

LEFT JOIN `common_zipcodesCities`
ON(`geodb_textdata`.`text_val` = `common_zipcodesCities`.`postal_code`)

WHERE
	`geodb_textdata`.`text_type` = '500300000'
		AND `geodb_textdata2`.`text_type` = '500100000'
		AND (
			`geodb_textdata`.`text_val` LIKE '48%'
		);

-- --------------------------------------------------------------
MIN
latitude 	longitude
51.76249 	6.69081611633305

MAX
latitude 	longitude
52.50628 	8.15801999999996

-- --------------------------------------------------------------

SELECT

`geodb_coordinates`.`lat` AS `latitude`,
`geodb_coordinates`.`lon` AS `longitude`

FROM `geodb_textdata`

LEFT JOIN `geodb_textdata` AS `geodb_textdata2`
ON(`geodb_textdata`.`loc_id` = `geodb_textdata2`.`loc_id`)

LEFT JOIN `geodb_locations`
ON (`geodb_textdata`.`loc_id` = `geodb_locations`.`loc_id`)

LEFT JOIN `geodb_coordinates`
ON(`geodb_textdata`.`loc_id` = `geodb_coordinates`.`loc_id`)

LEFT JOIN `common_zipcodesCities`
ON(`geodb_textdata`.`text_val` = `common_zipcodesCities`.`postal_code`)

WHERE
	`geodb_textdata`.`text_type` = '500300000'
		AND `geodb_textdata2`.`text_type` = '500100000'
		AND (
			`geodb_textdata`.`text_val` LIKE '48%'
		)

GROUP BY CONCAT(`latitude`, '-', `longitude`)

ORDER BY `latitude` ;

-- --------------------------------------------------------------
0. Schrittgr��e definieren_ LAT_STEP: zb. 0.1
	n =^ schritt
1. ���ersten linkspunkt LAT_MIN w�hlen (latidude min)
2. hier nach min und max suchen
	=> P_MIN[n=0](lat, lon); P_MAX[n=0](lat, lon)
3. Innerhalb LAT_MIN und (LAT_MIN + LAT_STEP) nach min und max suchen
	=> P_MIN[n=n+1](lat, lon); P_MAX[n=n+1](lat, lon)
4. 3. wiederholen
	Innerhalb (LAT_MIN + ((n-1) * LAT_STEP)) und (LAT_MIN + (n * LAT_STEP)) nach min und max suchen
	=> P_MIN[n=n+1](lat, lon); P_MAX[n=n+1](lat, lon)
	bis (LAT_MIN + (n * LAT_STEP)) <= LAT_MAX  ���ersten rechtsspunkt

5. Punkte geeignet zu polygonen verkn�pfen

-- --------------------------------------------------------------
-- --------------------------------------------------------------
-- --------------------------------------------------------------
-- --------------------------------------------------------------
-- --------------------------------------------------------------
-- --------------------------------------------------------------
-- --------------------------------------------------------------
-- --------------------------------------------------------------
-- --------------------------------------------------------------
-- --------------------------------------------------------------
-- --------------------------------------------------------------
-- --------------------------------------------------------------
-- --------------------------------------------------------------
-- --------------------------------------------------------------