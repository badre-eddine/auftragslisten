<?php
	require_once('inc/requires.inc.php');
	require_once('inc/convert.inc.php');

	if(!$arrGetUserRights["reviseDocumentDatas"] && !$arrGetUserRights["insertDeliSprintDatas"] && !$arrGetUserRights["changeDocumentCustomerNumber"]) {
		header('location: ' . PAGE_EXIT_LOCATION);
		exit;
	}

	$jswindowMessage = '';
	$warningMessage = '';
	$errorMessage = '';
	$successMessage = '';
	$infoMessage = '';

	if($_REQUEST["downloadFile"] != "") {
		require_once('classes/downloadFile.class.php');
		$thisDownloadFile = $_REQUEST["downloadFile"];

		if($_REQUEST["documentType"] == 'DPD'){
			// downloadFile=' . urlencode(basename(PATH_DPD_INTERFACE_TRACKING_FILES . $thisDpdLocalTrackingFile)) . '&documentType=DPD
			$fileDirectory = PATH_DPD_INTERFACE_TRACKING_FILES;
			$setHeaderContentType = 'text/csv; charset=utf-8';
		}
		else {
			$fileDirectory = DIRECTORY_CREATED_DOCUMENTS_CUSTOMERS;
			if(!file_exists($fileDirectory . $thisDownloadFile)){
				$thisDownloadFile = searchDownloadFile($fileDirectory . $thisDownloadFile, $arrQueryVars["thisDocumentType"]);
			}
		}

		$thisDownload = new downloadFile($fileDirectory, $thisDownloadFile);

		if($setHeaderContentType != '') {
			$thisDownload->setHeaderContentType($setHeaderContentType);
		}

		$errorMessage .= $thisDownload->startDownload();
	}

	$dbConnection = new DB_Connection();
	$db_open = $dbConnection->db_connect();

	// BOF DEFINE ACTIONS
		$arrActions = array();
		
		if($arrGetUserRights["reviseDocumentDatas"]) {
			$arrActions = array(
					// "ACTION_DELETE_DOCUMENT"																	=> array("text" => "DOKUMENTE: Dokument l&ouml;schen", "class" => "actionDocument"),
					"ACTION_CHANGE_DOCUMENT_CUSTOMER_NUMBER"													=> array("text" => "DOKUMENTE: Dokument-Kundennummer &auml;ndern", "class" => "actionDocument"),
					"ACTION_CHECK_RELATED_DOCUMENTS_PAYMENT_STATUS"												=> array("text" => "DOKUMENTE: Dokument-Status verknüpfter Dokumente prüfen", "class" => "actionDocument"),
					// "ACTION_CHECK_GU_HAVING_RE_WITHOUT_PAYMENTS_AND_DIFFERENT_TOTAL_SUM"						=> array("text" => "DOKUMENTE: Dokument-Status GUs &amp; REs  mit unterschiedlichem Gesamtbetrag ohne AB-/RE-Zahlungen", "class" => "actionDocument"),
					"ACTION_FIND_DOCUMENTS_WITHOUT_PAYMENT_STATUS"												=> array("text" => "DOKUMENTE: Dokumente ohne Zahlstatus finden", "class" => "actionDocument"),
					// "ACTION_RELATED_DOCUMENTS"																	=> array("text" => "DOKUMENTE: Dokument-Verkn&uuml;pfungen erstellen", "class" => "actionDocument"),
					// "ACTION_CHANGE_CUSTOMER_NUMBER"																=> array("text" => "KUNDEN: Kundennummer eines Kunden &auml;ndern", "class" => "actionCustomers"),

					"ACTION_CONVERT_AMAZON_ORDER_DATAS"															=> array("text" => "AMAZON: Bestelldaten f&uuml;r DB konvertieren", "class" => "actionAmazon"),
					"ACTION_AMAZON_INFORM_ORDER_CUSTOMERS_RATING"												=> array("text" => "AMAZON: Kunden Bewertungsaufforderung", "class" => "actionAmazon"),
					"ACTION_CONVERT_AMAZON_TRANSACTION_DATAS"													=> array("text" => "AMAZON: Transaktionen f&uuml;r DB konvertieren", "class" => "actionAmazon"),

					// "ACTION_GET_EMAIL_FROM_TEXT"																=> array("text" => "MAILS: Mail-Adressen aus Text filtern", "class" => "actionMail"),

					"ACTION_LAYOUT_FILE_DATAS"																	=> array("text" => "GRAFIK: Korrektur-Abzug-Daten konvertieren", "class" => "actionLayout"),

					"ACTION_CONVERT_DPD_DELISPRINT_DELIVERY_DATAS"												=> array("text" => "DPD: DELISPRINT-PAKETHISTORIE konvertieren", "class" => "actionDpdDelisprint"),
					"ACTION_IMPORT_DPD_FTP_INTERFACE_STATUS_DATAS"												=> array("text" => "DPD: PAKETSTATUS &uuml;ber DPD-Schnittstelle importieren", "class" => "actionDpdInterface"),
					"ACTION_FIND_IMPORTED_DPD_STATUS_WITHOUT_DELISPRINT_DELIVERY_DATAS"							=> array("text" => "DPD: importierter PAKETSTATUS ohne DELISPRINT-PAKETNUMMER finden", "class" => "actionDpd"),
					// "ACTION_FIND_FIND_CUSTOMER_NUMBERS_FOR_DELISPRINT_DELIVERY_DATAS_WITHOUT_CUSTOMER_NUMBERS"	=> array("text" => "DPD: Kundennummern zu DELISPRINT-PAKETNUMMER ohne Kundennummerfinden", "class" => "actionDpd"),
					// "ACTION_IMPORT_DPD_PARCEL_INVOICES"															=> array("text" => "DPD: PAKETNUMMERN-ABRECHNUNG IMPORTIEREN", "class" => "actionDpd"),

					"ACTION_CONVERT_CONTAINER_LISTS"															=> array("text" => "VERSAND: TUERKEI-PAKET-LISTEN konvertieren", "class" => "actionContainerLists"),

					// "ACTION_FIND_RES_WITH_NO_PRODUCTION_STATUS_SEND"											=> array("text" => "PRODUKTIONEN: Produktionen suchen mit REs und unverschicktem Status", "class" => "actionProductions"),
					// "ACTION_FIND_ORDERS_IN_DOCUMENTS_OF_ALL_MANDATORIES"										=> array("text" => "PRODUKTIONEN: Produktionen suchen die in Dokumenten aller Mandanten verwendet werden", "class" => "actionProductions"),
				);
		}
		else {			
			if($arrGetUserRights["insertDeliSprintDatas"]) {
				$arrActions["ACTION_CONVERT_DPD_DELISPRINT_DELIVERY_DATAS"]										= array("text" => "DPD: DELISPRINT-PAKETHISTORIE konvertieren", "class" => "actionDpdDelisprint");				
			}
			if($arrGetUserRights["changeDocumentCustomerNumber"]) {
				$arrActions["ACTION_CHANGE_DOCUMENT_CUSTOMER_NUMBER"]											= array("text" => "DOKUMENTE: Dokument-Kundennummer &auml;ndern", "class" => "actionDocument");			
			}
		}
	// EOF DEFINE ACTIONS

	/*
	function getDpdLocalTrackingFileDateTime($string){
		$arrTemp = explode("_", $string);
		// D20150807T123040
		$thisDateTime = preg_replace("/D([0-9]{4})([0-9]{2})([0-9]{2})T([0-9]{2})([0-9]{2})([0-9]{2})/", "$1-$2-$3 $4:$5:$6", $arrTemp[2]);
		return($thisDateTime);
	}

	// BOF SQL ACTION BUTTONS
	function createSqlExecButtons(){
		$content = '';
		$content .= '<div style="margin:2px 0 2px 0;padding:0;">';
		$content .= '<span class="buttonExecuteSqlOrders"><img src="layout/icons/iconExecuteSelected.png" width="16" height="16" alt="" title="" style="margin-bottom:-4px;" /> Markierte ausf&uuml;hren (<span class="displayCountSelectedItemsSqlOrders">0</span>)</span>';
		$content .= '<span class="buttonCheckAllCheckboxesSqlOrders"><img src="layout/icons/iconSelectAll.png" width="16" height="" alt="" title="" style="margin-bottom:-4px;" /> Alle SQL-Befehle ausw&auml;hlen</span>';
		$content .= '<span class="buttonUncheckAllCheckboxesSqlOrders"><img src="layout/icons/iconDeselectAll.png" width="16" height="" alt="" title="" style="margin-bottom:-4px;" /> Alle SQL-Befehle abw&auml;hlen</span>';
		$content .= '</div>';
		return $content;
	}
	// BOF SQL ACTION BUTTONS
	*/
	
	// BOF CANCEL
	if($_POST["cancelFormReviseDatas"] != "") {
		unset($_POST["selectAction"]);
		unset($_POST["handleThisDocumentNumber"]);
		unset($_POST["handleThisDocumentCustomerNumber"]);
		unset($_POST["newDocumentCustomerNumber"]);
	}
	// EOF CANCEL

	/*
	function getNotInformedCustomersOrderIDs() {
		global $dbConnection;
		$arrGetNotInformedCustomersOrderIDs = array();
		$sql = "
				SELECT
					`common_ordersamazonratings`.`ordersAmazonRatingsTransaktionsID`,
					`common_ordersamazon`.`AMAZON-BESTELLNUMMER`
				FROM `" . TABLE_AMAZON_ORDERS . "`
				LEFT JOIN `" . TABLE_AMAZON_ORDERS_RATINGS . "`
				ON(`common_ordersamazonratings`.`ordersAmazonRatingsTransaktionsID` = `common_ordersamazon`.`AMAZON-BESTELLNUMMER`)

				WHERE 1

				HAVING `ordersAmazonRatingsTransaktionsID` IS NULL
			";
		$rs = $dbConnection->db_query($sql);

		while($ds = mysqli_fetch_assoc($rs)){
			$arrGetNotInformedCustomersOrderIDs[] = $ds["AMAZON-BESTELLNUMMER"];
		}
		return $arrGetNotInformedCustomersOrderIDs;
	}

	function getNotInformedCustomersOrderIdLinks($string){
		$arrayData = explode("\n", $string);
		$arrGetNotInformedCustomersOrderDatas = array();
		$count = 0;
		if(!empty($arrayData)){
			foreach($arrayData as $thisDataValue){
				$strTemp = $thisDataValue;
				$strTemp = trim($strTemp);

				$arrGetNotInformedCustomersOrderDatas[$count]["ID"] = $strTemp;

				$pattern = '^(.*)$';
				$replace = '$1">$1';
				$strTemp = preg_replace("/" . $pattern . "/ism", $replace, $strTemp);

				$pattern = '^';
				$replace = '<a href="http://sellercentral.amazon.de/gp/orders-v2/details/ref=ag_orddet_cont_myo?ie=UTF8&orderID=';
				$strTemp = preg_replace("/" . $pattern . "/", $replace, $strTemp);

				$pattern = '$';
				$replace = '</a>';
				$strTemp = preg_replace("/" . $pattern . "/", $replace, $strTemp);

				$arrGetNotInformedCustomersOrderDatas[$count]["LINK"] = $strTemp;

				$count++;
			}
		}
		return $arrGetNotInformedCustomersOrderDatas;
	}

	function cleanLineSeparators($string){
		$pattern = "\\r";
		$replace = "\n";
		$string = preg_replace("/" . $pattern . "/", $replace, $string);

		$pattern = "\\n\\n";
		$replace = "\n";
		while(preg_match("/" . $pattern . "/", $string)) {
			$string = preg_replace("/" . $pattern . "/", $replace, $string);
		}
		return $string;
	}

	function cleanDate($string){
		$pattern = "([0-9]{2})\.([0-9]{2})\.([0-9]{4})";
		$replace = "$3-$2-$1";
		$string = preg_replace("/" . $pattern . "/", $replace, $string);
		return $string;
	}

	function cleanDecimals($string){
		$pattern = "([0-9]{1,}),([0-9]{1,})";
		$replace = "$1.$2";
		$string = preg_replace("/" . $pattern . "/", $replace, $string);
		return $string;
	}

	function createQueries($string, $tableName, $tableFields, $ignore, $delayed){
		$pattern = "'";
		$replace = "\'";
		$string = preg_replace("/" . $pattern . "/", $replace, $string);

		$pattern = ";";
		$replace = "', '";
		$string = preg_replace("/" . $pattern . "/", $replace, $string);

		$pattern = "\n|^";
		$replace = "\nINSERT " . ' ' . $delayed . ' ' . $ignore . " INTO `" . $tableName . "` VALUES ('";
		// "INSERT INTO ON DUPLICATE KEY UPDATE id=id."
		$string = preg_replace("/" . $pattern . "/", $replace, $string);

		$pattern = "\n|$";
		$replace = "');\n";
		$string = preg_replace("/" . $pattern . "/", $replace, $string);

		$pattern = "^'\);\\n";
		$replace = "";
		$string = preg_replace("/" . $pattern . "/", $replace, $string);

		return $string;
	}

	function convertMailData($string) {
		$arrOutput = array();

		#$string = html_entity_decode($string);

		$string = preg_replace("/\</", "\n", $string);
		$string = preg_replace("/\>/", "\n", $string);
		$string = preg_replace("/:/", "\n", $string);

		$string = cleanLineSeparators($string);

		$string = preg_replace('/(.*)@/', '{###MAIL###}$1@', $string);
		$string = preg_replace('/(.*) (.*)@/', '$2@', $string);
		$string = preg_replace('/@(.*) (.*)/', '@$1', $string);
		$string = preg_replace('/@(.*)\t(.*)/', '@$1', $string);

		$string = preg_replace("/\\t/", "", $string);

		while(preg_match("/ /", $string)) {
			$string = preg_replace("/ /", " ", $string);
		}
		$string = preg_replace("/ /", "\n", $string);
		$string = preg_replace("/\\n /", "\n", $string);

		$arrTempGetMails = explode("\n", $string);
		$arrTempGetMails = array_unique($arrTempGetMails);
		// print_r($arrTempGetMails);

		$arrGetMails = array();
		foreach($arrTempGetMails as $thisValue) {
			if(preg_match("/@/", $thisValue)) {
				$arrGetMails[] = $thisValue;
			}
		}
		sort($arrGetMails);
		$arrGetMails = array_unique($arrGetMails);
		// print_r($arrGetMails);

		$string = implode("\n", $arrGetMails);

		$string = preg_replace("/(.*)@www.burhan-ctr.de/", "", $string);
		$string = preg_replace("/(.*)@burhan-ctr.de/", "", $string);
		$string = preg_replace("/(.*)@www.b3-werbepartner.de/", "", $string);
		$string = preg_replace("/(.*)@b3-werbepartner.de/", "", $string);
		
		$string = preg_replace("/{###MAIL###}/", "", $string);
		while(preg_match("/\\n\\n/", $string)) {
			$string = preg_replace("/\\n\\n/", "\n", $string);
		}
		$string = preg_replace('/@(.*)\t(.*)/', '@$1', $string);
		$string = preg_replace('/@(.*)&(.*)/', '@$1', $string);
		$string = preg_replace('/(.*)=(.*)/', '$2', $string);
		$string = preg_replace('/\(/', '', $string);
		$string = preg_replace('/\)/', '', $string);
		$string = preg_replace('/\'/', '', $string);
		$string = preg_replace('/"/', '', $string);
		$string = preg_replace('/;/', '', $string);
		$string = preg_replace('/#/', '', $string);
		$string = preg_replace('/,/', '', $string);
		$string = trim($string);

		$arrOutput["mail"] = $string;

		return $arrOutput;
	}

	function convertProductName($string){
		$pattern = "/(;|\t)([A-Z]{1,2})(;|\t)/";
		$replace = "$1$2 52$3";
		$string = preg_replace($pattern, $replace, $string);
		return $string;
	}
		
	function convertTrListsDataAuftragslisten($string) {
		// FIELDS:
		//	containerListsFilename,containerListsProductionsDateStart,containerListsProductionsDateEnd,containerListsDepartureKW,containerListsArrivalKW,containerListsDeliveryKW,containerListsCustomerNumber,containerListsCustomerName,containerListsCustomerKommission,containerListsProduct,containerListsQuantity,containerListsTirArkasi

		$rs_match = preg_match("/LISTE:(.*)/i", $string, $arrFound);
		$containerListsFilename = trim($arrFound[1]);

		$rs_match = preg_match("/PRODUKTION:.*([0-9]{2}\.[0-9]{2}\.[0-9]{4}).*([0-9]{2}\.[0-9]{2}\.[0-9]{4})/i", $string, $arrFound);
		$containerListsProductionsDateStart = trim($arrFound[1]);
		$containerListsProductionsDateEnd = trim($arrFound[2]);
		$containerListsProductionsDateStart = formatDate($containerListsProductionsDateStart, 'store');
		$containerListsProductionsDateEnd = formatDate($containerListsProductionsDateEnd, 'store');

		$rs_match = preg_match("/SCHIFFS-ABFAHRT: ca.(.*)/i", $string, $arrFound);
		$containerListsDepartureKW = $arrFound[1];
		$containerListsDepartureKW = preg_replace("/KW/", "", $containerListsDepartureKW);
		$containerListsDepartureKW = trim($containerListsDepartureKW);

		$rs_match = preg_match("/SCHIFFS-ANKUNFT: ca.(.*)/i", $string, $arrFound);
		$containerListsArrivalKW = $arrFound[1];
		$containerListsArrivalKW = preg_replace("/KW/", "", $containerListsArrivalKW);
		$containerListsArrivalKW = trim($containerListsArrivalKW);

		$rs_match = preg_match("/VERSAND: ca.(.*)/i", $string, $arrFound);
		$containerListsDeliveryKW = $arrFound[1];
		$containerListsDeliveryKW = preg_replace("/KW/", "", $containerListsDeliveryKW);
		$containerListsDeliveryKW = trim($containerListsDeliveryKW);

		$pattern = "/(LISTE|PRODUKTION|SCHIFFS-ABFAHRT|SCHIFFS-ANKUNFT|VERSAND):(.*)/";
		$replace = "";
		$string = preg_replace($pattern, $replace, $string);

		$pattern = "/\t/";
		$replace = ";";
		$string = preg_replace($pattern, $replace, $string);

		$pattern = "/( ;|; )/";
		$replace = ";";
		$string = preg_replace($pattern, $replace, $string);

		$pattern = "/^\n{1,}/";
		$replace = "\n";
		$string = preg_replace($pattern, $replace, $string);

		$addFields = "";
		$addFields .= $containerListsFilename . ';';
		$addFields .= $containerListsProductionsDateStart . ';';
		$addFields .= $containerListsProductionsDateEnd . ';';
		$addFields .= $containerListsDepartureKW . ';';
		$addFields .= $containerListsArrivalKW . ';';
		$addFields .= $containerListsDeliveryKW . ';';

		$pattern = "/\n/";
		$replace = "\n" . $addFields;
		$string = preg_replace($pattern, $replace, $string);

		return $string;
	}

	function convertTrListsDataPrint($string) {
		global $_POST, $_GET, $_REQUEST;
		$arrOutput = array();
		$string = cleanLineSeparators($string);

		#echo "<pre>";
		#echo $string;
		#echo "</pre>";

		// BOF REPLACE DATE FIELD
		$pattern = "/^[0-9]{2}\.[0-9]{2}\.[0-9]{4}[\t; ]/";
		$replace = "";
		$string = preg_replace($pattern, $replace, $string);

		$pattern = "/^[0-9]{2}\.[0-9]{2}\.[0-9]{2}[\t; ]/";
		$replace = "";
		$string = preg_replace($pattern, $replace, $string);

		$pattern = "/\n[0-9]{2}\.[0-9]{2}\.[0-9]{4}[\t; ]/";
		$replace = "\n";
		$string = preg_replace($pattern, $replace, $string);

		$pattern = "/\n[0-9]{2}\.[0-9]{2}\.[0-9]{2}[\t; ]/";
		$replace = "\n";
		$string = preg_replace($pattern, $replace, $string);
		// EOF REPLACE DATE FIELD


		// BOF REPLACE B3 WITH B3-CUSTOMER NUMBER
		$pattern = "/(\n)B3/";
		$replace = "\n48271";
		$string = preg_replace($pattern, $replace, $string);
		// EOF REPLACE B3 WITH B3-CUSTOMER NUMBER

		// BOF CONVERT PRODUCT NAME
		$string = convertProductName($string);
		// EOF CONVERT PRODUCT NAME

		// BOF Tır Arkası
		$pattern = "/Tır Arkası/";
		$replace = "TIR ARKASI";
		$string = preg_replace($pattern, $replace, $string);
		// EOF Tır Arkası

		// BOF plakalık
		$pattern = "/\tplakalık\t/";
		$replace = "\tKennzeichenhalter\t";
		$string = preg_replace($pattern, $replace, $string);
		// EOF Tır Arkası

		// BOF ps-plate
		$pattern = "/\tps\t/";
		$replace = "\tMiniletter\t";
		$string = preg_replace($pattern, $replace, $string);
		// EOF Tır Arkası

		// BOF paspas
		$pattern = "/\tpaspas\t/";
		$replace = "\tPapierfußmatten\t";
		$string = preg_replace($pattern, $replace, $string);
		// EOF Tır Arkası


		#$pattern = "\n.*(Sevk Tarihi|Sevh Tarihi|SevkTarihi|SEVK TARİHİ).*";
		$pattern = "\n.*(Sevk Tar|Sevh Tar|SevkTar|SEVK TAR).*";
		$replace = "";
		$string = preg_replace("/" . $pattern . "/i", $replace, $string);

		$pattern = "\n;.*";
		$replace = "";
		$string = preg_replace("/" . $pattern . "/", $replace, $string);

		$pattern = "\n([0-9]{1,4};)";
		$replace = "\n0$1";
		$string = preg_replace("/" . $pattern . "/", $replace, $string);

		$pattern = "\n([0-9]{1,4}\t)";
		$replace = "\n0$1";
		$string = preg_replace("/" . $pattern . "/", $replace, $string);

		$pattern = ";TIRIN";
		$replace = " TIRIN";
		$string = preg_replace("/" . $pattern . "/i", $replace, $string);

		$pattern = "\tTIRIN";
		$replace = " TIRIN";
		$string = preg_replace("/" . $pattern . "/i", $replace, $string);

		$pattern = ";Ç(.*){1}TA";
		$replace = " ÇİTA";
		#$string = preg_replace("/" . $pattern . "/i", $replace, $string);

		$pattern = "\tÇ(.*){1}TA";
		$replace = " ÇİTA";
		#$string = preg_replace("/" . $pattern . "/i", $replace, $string);


		$pattern = "ÇİTA";
		$replace = "LEISTEN";
		$string = preg_replace("/" . $pattern . "/i", $replace, $string);

		$pattern = "^M.*\n";
		$replace = "";
		$string = preg_replace("/" . $pattern . "/", $replace, $string);

		$pattern = ";{2}\n";
		$replace = ";\n";
		$string = preg_replace("/" . $pattern . "/", $replace, $string);

		$pattern = ";";
		$replace = "\t";
		$string = preg_replace("/" . $pattern . "/", $replace, $string);

		$pattern = "[ ]{1,}";
		$replace = " ";
		$string = preg_replace("/" . $pattern . "/", $replace, $string);

		#$pattern = "[ ]{1,}\t";
		#$replace = " ";
		#$string = preg_replace("/" . $pattern . "/", $replace, $string);

		#$pattern = "\t[ ]{1,}";
		#$replace = " ";
		##$replace = "\t";
		#$string = preg_replace("/" . $pattern . "/", $replace, $string);

		$arrString = explode("\n", $string);

		sort($arrString);

		$arrOutput["csv"] = '';

		$thisListFileName = $_REQUEST["handleThisDeliveryDate"];
		$arrListDates = convertTrFileNameToDate($thisListFileName);

		$thisDateSheepingWeek = 1 * getWeekFromDate($arrListDates[1]);
		$arrOutput["csv"] .= 'LISTE: ' . $thisListFileName . "\n";
		$arrOutput["csv"] .= 'PRODUKTION: ' . implode(" & ", $arrListDates) . "\n";
		$arrOutput["csv"] .= 'SCHIFFS-ABFAHRT: ' . 'ca. KW ' . $thisDateSheepingWeek . "\n";
		$arrOutput["csv"] .= 'SCHIFFS-ANKUNFT: ' . 'ca. KW ' . ($thisDateSheepingWeek + 2) . "\n";
		$arrOutput["csv"] .= 'VERSAND: ' . 'ca. KW ' . ($thisDateSheepingWeek + 3) . "\n";
		$arrOutput["csv"] .= implode("\n", $arrString);
		$arrOutput["sql"] = '';
		return $arrOutput;
	}
	function getWeekFromDate($date){
		$thisDate = formatDate($date, "store");
		$thisDateWeek = date("W", strtotime($thisDate));
		return $thisDateWeek;
	}
	function convertTrFileNameToDate($string){
		$arrDates = array();
		$arrThisListFileInfo = pathinfo("/" . $string);
		$string = trim($arrThisListFileInfo["filename"]);
		$string = preg_replace("/ /", "", $string);
		if(preg_match("/[0-9]{1,2}_[0-9]{1,2}_[0-9]{4}&[0-9]{1,2}_[0-9]{1,2}_[0-9]{4}/", $string)){
			$arrTempDate = explode("&", $string);
			if(!empty($arrTempDate)){
				$countDate = 0;
				foreach($arrTempDate as $thisTempData){
					$arrThisTempDateData = explode("_", $thisTempData);
					$arrDates[$countDate] = '';

					if(!empty($arrThisTempDateData)){
						foreach($arrThisTempDateData as $thisTempDateDataKey => $thisTempDateData){
							#$thisTempDateData = $thisTempDateData * 1;
							$thisTempDateData = intval($thisTempDateData);
							if($thisTempDateData < 10){
								$thisTempDateData = "0" . $thisTempDateData;
							}
							$arrDates[$countDate] .= $thisTempDateData;
							if($thisTempDateDataKey < 2){
								$arrDates[$countDate] .= ".";
							}
						}
					}
					$countDate++;
				}
			}
		}
		else if(preg_match("/[0-9]{1,2}\.[0-9]{1,2}\.[0-9]{4}-[0-9]{1,2}\.[0-9]{1,2}\.[0-9]{4}/", $string)){
			$arrTempDate = explode("-", $string);
			$arrDates = $arrTempDate;
		}
		else if(preg_match("/[0-9]{1,2}\.[0-9]{1,2}\.[0-9]{4}&[0-9]{1,2}\.[0-9]{1,2}\.[0-9]{4}/", $string)){
			$arrTempDate = explode("&", $string);
			$arrDates = $arrTempDate;
		}

		return $arrDates;
	}

	function convertDeliSprintDeliveryData($string, $tableName) {
		$arrOutput = array();
		$string = trim($string);
		$string = cleanLineSeparators($string);
		$string = cleanDate($string);
		$string = cleanDecimals($string);
		if(mb_detect_encoding($string) == 'UTF-8'){
			#$string = utf8_encode($string);
		}

		$arrOutput["csv"] = $string;
		$string = createQueries($string, $tableName, '', 'IGNORE', 'DELAYED');

		// BOF ADD EMPTY FIELD FOR `isInfoMailSended`
			$string = preg_replace("/(\);)/", ",'0'$1", $string);
		// EOF ADD EMPTY FIELD FOR `isInfoMailSended`
		$arrOutput["sql"] = $string;
		return $arrOutput;
	}

	function convertDpdParcelInvoiceData($string, $tableName) {
		$arrOutput = array();
		$string = trim($string);
		$string = cleanLineSeparators($string);
		$string = cleanDate($string);
		$string = cleanDecimals($string);
		if(mb_detect_encoding($string) == 'UTF-8'){
			#$string = utf8_encode($string);
		}

		$arrOutput["csv"] = $string;
		$string = createQueries($string, $tableName, '', 'IGNORE', 'DELAYED');
		$arrOutput["sql"] = $string;
		return $arrOutput;
	}

	function convertDpdInterfaceDeliveryData($string, $tableName) {
		$arrOutput = array();
		$string = trim($string);
		$string = cleanLineSeparators($string);
		$string = cleanDate($string);
		$string = cleanDecimals($string);
		if(mb_detect_encoding($string) == 'UTF-8'){
			#$string = utf8_encode($string);
		}

		$arrOutput["csv"] = $string;
		$string = createQueries($string, $tableName, '', 'IGNORE', ' DELAYED');
		$arrOutput["sql"] = $string;
		return $arrOutput;
	}

	function convertLayoutData($string) {
		$arrOutput = array();

		$string = addslashes($string);

		$string = cleanLineSeparators($string);

		$pattern = "(.*)<DIR>(.*)";
		$replace = "";
		$string = preg_replace("/" . $pattern . "/", $replace, $string);

		#$pattern = "?!(.*[0-9]{2}\.[0-9]{2}\.[0-9]{4}.*)";
		#$replace = "";
		#$string = preg_replace("/" . $pattern . "/ism", $replace, $string);

		$string = cleanLineSeparators($string);



		$string = cleanDate($string);

		$pattern = "(\.[0-9]{1,3}) ";
		$replace = "$1;";
		$string = preg_replace("/" . $pattern . "/", $replace, $string);

		$pattern = "(:[0-9]{2})[ ]{1,}";
		$replace = "$1;";
		$string = preg_replace("/" . $pattern . "/", $replace, $string);

		$arrOutput["csv"] = $string;

		$string = createQueries($string, TABLE_CREATED_LAYOUT_FILES, '', 'IGNORE', ' DELAYED');

		$string = preg_replace("/\\\\\\\'/", "\'", $string);

		$arrOutput["sql"] = $string;
		return $arrOutput;
	}

	function convertAmazonOrdersData($string) {
		$arrOutput = array();
		#$string = utf8_decode($string);

		$tempFieldSeparator = "{###FIELD_SEPARATOR###}";
		$tempLineSeparator = "{###LINE_SEPARATOR###}";

		if(preg_match("/Bestellung stornieren/", $string)){
			$pattern = "(Bestellung stornieren)";
			$replace = "$1" . $tempLineSeparator;
		}
		else if(preg_match("/Bestellung erstatten/", $string)){
			$pattern = "(Bestellung erstatten)";
			$replace = "$1" . $tempLineSeparator;
		}
		$string = preg_replace("/" . $pattern . "/", $replace, $string);

		$pattern = "\\r";
		$replace = "";
		$string = preg_replace("/" . $pattern . "/", $replace, $string);

		$pattern = "\\n[\t ]{1,}";
		$replace = $tempFieldSeparator;
		$string = preg_replace("/" . $pattern . "/", $replace, $string);

		$pattern = "\\n";
		$replace = $tempFieldSeparator;
		$string = preg_replace("/" . $pattern . "/", $replace, $string);

		$pattern = "\\t";
		$replace = $tempFieldSeparator;
		$string = preg_replace("/" . $pattern . "/", $replace, $string);

		$pattern = " " . $tempFieldSeparator;
		$replace = $tempFieldSeparator;
		$string = preg_replace("/" . $pattern . "/", $replace, $string);

		$pattern = $tempFieldSeparator . " ";
		$replace = $tempFieldSeparator;
		$string = preg_replace("/" . $pattern . "/", $replace, $string);

		$pattern = " bis" . $tempFieldSeparator;
		$replace = " bis ";
		$string = preg_replace("/" . $pattern . "/", $replace, $string);

		$arrOrders = explode($tempLineSeparator, $string);

		if(!empty($arrOrders)){
			foreach($arrOrders as $thisOrderKey => $thisOrderValue){
				if($thisOrderValue != ""){
					$arrData = explode($tempFieldSeparator, $thisOrderValue);
					if($arrData[0] == "")	{
						array_shift($arrData);
					}
					$string = "";

					$COUNT = '%';
					$BESTELLDATUM = $arrData[0] . " " . $arrData[1];

					$arrTemp = explode(":", $arrData[8]);
					$VERTRIEBSKANAL = trim($arrTemp[1]);

					$arrTemp = explode(":", $arrData[9]);
					$VERSAND_DURCH = trim($arrTemp[1]);

					$AMAZON_BESTELLNUMMER = $arrData[2];

					$PRODUKTNAME = $arrData[3];

					$arrTemp = explode(":", $arrData[4]);
					$MENGE = trim($arrTemp[1]);

					$arrTemp = explode(":", $arrData[5]);
					$ASIN = trim($arrTemp[1]);

					$arrTemp = explode(":", $arrData[6]);
					$SKU = trim($arrTemp[1]);

					$arrTemp = explode(":", $arrData[7]);
					$KAEUFER = trim($arrTemp[1]);

					$VERSANDART = $arrData[10];
					$STATUS = $arrData[17];

					$arrTemp = explode(" bis ", $arrData[15]);
					$LIEFERDATUM_VON = trim($arrTemp[1]);
					$LIEFERDATUM_BIS = trim($arrTemp[2]);
					$KUNDENNUMMER = '';

					#$string .= $COUNT;
					$string .= $BESTELLDATUM;
					$string .= ';';
					$string .= $VERTRIEBSKANAL;
					$string .= ';';
					$string .= $VERSAND_DURCH;
					$string .= ';';
					$string .= $AMAZON_BESTELLNUMMER;
					$string .= ';';
					$string .= $PRODUKTNAME;
					$string .= ';';
					$string .= $MENGE;
					$string .= ';';
					$string .= $ASIN;
					$string .= ';';
					$string .= $SKU;
					$string .= ';';
					$string .= $KAEUFER;
					$string .= ';';
					$string .= $VERSANDART;
					$string .= ';';
					$string .= $STATUS;
					$string .= ';';
					$string .= $LIEFERDATUM_VON;
					$string .= ';';
					$string .= $LIEFERDATUM_BIS;
					$string .= ';';
					$string .= $KUNDENNUMMER;

					$pattern = "([0-9]{2}\.[0-9]{2}\.[0-9]{4};[0-9]{2}\.[0-9]{2}\.[0-9]{4})";
					$replace = "$1";
					$string = preg_replace("/" . $pattern . "/", $replace, $string);

					$pattern = "^";
					$replace = "%;";
					$string = preg_replace("/" . $pattern . "/", $replace, $string);

					$arrOrders[$thisOrderKey] = $string;
				}
			}
		}

		$string = implode("\n", $arrOrders) . "\n";
		#$string = cleanLineSeparators($string);

		$string = cleanDate($string);

		$arrOutput["csv"] = $string;

		$pattern = ";";
		$replace = "', '";
		$string = preg_replace("/" . $pattern . "/", $replace, $string);

		$pattern = "(%')";
		$replace = "INSERT INTO `" . TABLE_AMAZON_ORDERS . "` (`COUNT`, `BESTELLDATUM`, `VERTRIEBSKANAL`, `VERSAND_DURCH`, `AMAZON-BESTELLNUMMER`, `PRODUKTNAME`, `MENGE`, `ASIN`, `SKU`, `KAEUFER`, `VERSANDART`, `STATUS`, `LIEFERDATUM_VON`, `LIEFERDATUM_BIS`, `KUNDENNUMMER`) VALUES ('$1";
		$string = preg_replace("/" . $pattern . "/", $replace, $string);

		$pattern = "(')\n";
		$replace = "$1');\n";
		$string = preg_replace("/" . $pattern . "/", $replace, $string);

		$arrOutput["sql"] = $string;

		return $arrOutput;
	}

	function _old_convertAmazonOrdersData($string) {
		$arrOutput = array();
		#$string = utf8_decode($string);

		$string = cleanLineSeparators($string);

		$pattern = "\\nbis\\n";
		$replace = ";";
		$string = preg_replace("/" . $pattern . "/", $replace, $string);

		$pattern = "([0-9]{2}:[0-9]{2}:[0-9]{2} GMT)";
		$replace = " $1";
		$string = preg_replace("/" . $pattern . "/", $replace, $string);

		$pattern = "([0-9]{3}-[0-9]{7}-[0-9]{7})";
		$replace = "$1;";
		$string = preg_replace("/" . $pattern . "/", $replace, $string);

		$pattern = "\\n";
		$replace = "";
		$string = preg_replace("/" . $pattern . "/", $replace, $string);

		$pattern = "([0-9]{2}\.[0-9]{2}\.[0-9]{4};[0-9]{2}\.[0-9]{2}\.[0-9]{4})";
		$replace = "$1;\n";
		$string = preg_replace("/" . $pattern . "/", $replace, $string);

		$pattern = "^[A-Za-zäöüß \t]{1,}";
		$replace = "";
		$string = preg_replace("/" . $pattern . "/", $replace, $string);

		$pattern = "\n[A-Za-zäöüß \t]{1,}";
		$replace = "\n";
		$string = preg_replace("/" . $pattern . "/", $replace, $string);

		$pattern = "^([0-9]{2}\.[0-9]{2}\.[0-9]{4})";
		$replace = "$1 ";
		#$string = preg_replace("/" . $pattern . "/", $replace, $string);

		$pattern = " \\t";
		$replace = ";";
		$string = preg_replace("/" . $pattern . "/", $replace, $string);

		$pattern = "\\n([0-9])";
		$replace = "\n%;$1";
		$string = preg_replace("/" . $pattern . "/", $replace, $string);

		$pattern = "^";
		$replace = "%;";
		$string = preg_replace("/" . $pattern . "/", $replace, $string);

		$pattern = "\\t";
		$replace = ";";
		$string = preg_replace("/" . $pattern . "/", $replace, $string);

		$string = cleanDate($string);

		$pattern = "Menge";
		$replace = ";Menge";
		$string = preg_replace("/" . $pattern . "/", $replace, $string);

		$pattern = "Menge: |ASIN: |SKU: ";
		$replace = "";
		$string = preg_replace("/" . $pattern . "/", $replace, $string);

		$pattern = " ";
		$replace = " ";
		$string = preg_replace("/" . $pattern . "/", $replace, $string);

		$arrOutput["csv"] = $string;

		$pattern = ";";
		$replace = "', '";
		$string = preg_replace("/" . $pattern . "/", $replace, $string);

		$pattern = "(%')";
		$replace = "INSERT INTO `" . TABLE_AMAZON_ORDERS . "` VALUES ('$1";
		$string = preg_replace("/" . $pattern . "/", $replace, $string);

		$pattern = "(, ')\\n";
		$replace = "$1');\n";
		$string = preg_replace("/" . $pattern . "/", $replace, $string);

		$arrOutput["sql"] = $string;
		return $arrOutput;
	}

	function convertAmazonTransactionsData($string) {
		$arrOutput = array();
		#$string = utf8_decode($string);

		$string = cleanLineSeparators($string);

		$arrString = explode("\n", $string);
		$string = '';

		if(!empty($arrString)){
			foreach($arrString as $thisString){
				$thisString = cleanDecimals($thisString);

				$pattern = "€";
				$replace = "";
				$thisString = preg_replace("/" . $pattern . "/", $replace, $thisString);

				#if(preg_match("/(echte Galvanik|An Konto mit der Endung)/", $thisString)){ echo $thisString . '<hr />'; }
				#if(preg_match("/(An Konto mit der Endung)/", $thisString)){ echo $thisString . '<hr />'; }
				#if(preg_match("/(echte Galvanik)/", $thisString)){ echo $thisString . '<hr />'; }

				$pattern = '","","';
				$replace = ";;";
				$thisString = preg_replace("/" . $pattern . "/", $replace, $thisString);

				$pattern = '""';
				$replace = '######';
				$thisString = preg_replace("/" . $pattern . "/", $replace, $thisString);

				$pattern = '","';
				$replace = ";";
				$thisString = preg_replace("/" . $pattern . "/", $replace, $thisString);

				$pattern = '",';
				$replace = ";";
				$thisString = preg_replace("/" . $pattern . "/", $replace, $thisString);

				$pattern = ',"';
				$replace = ";";
				$thisString = preg_replace("/" . $pattern . "/", $replace, $thisString);

				$pattern = '^"';
				$replace = '';
				$thisString = preg_replace("/" . $pattern . "/", $replace, $thisString);

				$pattern = '"$';
				$replace = '';
				$thisString = preg_replace("/" . $pattern . "/", $replace, $thisString);

				$arrFields = explode(";", $thisString);

				$thisDate = $arrFields[0];
				$pattern = "([0-9]{2})\.([0-9]{2})\.([0-9]{4})";
				$replace = "$3-$2-$1";
				$thisDate = preg_replace("/" . $pattern . "/", $replace, $thisDate);

				$pattern = "( GMT[+-][0-9]{2}:[0-9]{2})";
				$replace = "";
				$thisDate = preg_replace("/" . $pattern . "/", $replace, $thisDate);
				$arrFields[0] = $thisDate;

				$thisMD5 = md5($arrFields[1] . $arrFields[2] . $arrFields[3] . $arrFields[4]);
				$arrFields[count($arrFields)] = $thisMD5;

				$thisString = implode("\t", $arrFields);

				$thisString = createQueries($thisString, TABLE_AMAZON_ORDERS_TRANSACTIONS, '', 'IGNORE', '');

				$pattern = "\\t";
				$replace = "', '";
				$thisString = preg_replace("/" . $pattern . "/", $replace, $thisString);

				$pattern = '######';
				$replace = '"';
				$thisString = preg_replace("/" . $pattern . "/", $replace, $thisString);

				$string .= $thisString;
			}
		}
		$arrOutput["csv"] = '';

		$arrOutput["sql"] = $string;
		return $arrOutput;
	}

	function _old_convertAmazonTransactionsData($string) {

		$arrOutput = array();
		#$string = utf8_decode($string);

		$string = cleanLineSeparators($string);

		$arrString = explode("\n", $string);
		$string = '';

		if(!empty($arrString)){
			foreach($arrString as $thisString){
				$thisString = cleanDecimals($thisString);

				$pattern = "€";
				$replace = "";
				$thisString = preg_replace("/" . $pattern . "/", $replace, $thisString);

				$arrFields = explode("\t", $thisString);

				$thisDate = $arrFields[0];
				$pattern = "([0-9]{2})\.([0-9]{2})\.([0-9]{4})";
				$replace = "$3-$2-$1";
				$thisDate = preg_replace("/" . $pattern . "/", $replace, $thisDate);
				$arrFields[0] = $thisDate;

				$thisMD5 = md5($arrFields[1] . $arrFields[2] . $arrFields[3] . $arrFields[4] . $arrFields[5]);
				$arrFields[count($arrFields)] = $thisMD5;

				$thisString = implode("\t", $arrFields);

				$thisString = createQueries($thisString, TABLE_AMAZON_ORDERS_TRANSACTIONS, '', 'IGNORE', '');

				$pattern = "\\t";
				$replace = "', '";
				$thisString = preg_replace("/" . $pattern . "/", $replace, $thisString);

				$string .= $thisString;
			}
		}

		$arrOutput["csv"] = '';

		$arrOutput["sql"] = $string;
		return $arrOutput;
	}
	*/
?>
<?php
	require_once('inc/headerHTML.inc.php');
	$thisTitle = "Daten-Korrekturen";
	if($_POST["selectAction"] != ""){
		$thisTitle .= ' - ' . $arrActions[$_POST["selectAction"]]["text"];
	}
	$headerHTML = preg_replace("/{###TITLE###}/", strip_tags($thisTitle), $headerHTML);
	echo $headerHTML;

	require_once(FILE_MENUE_TOP);
?>
<div id="mainArea">
	<div id="mainContent">
		<?php require_once(FILE_MENUE_SIDEBAR); ?>
		<div id="contentArea">
			<div id="contentAreaElements">
				<h1><?php if(SHOW_PAGE_TITLE_ICONS) { echo '<img src="' . PATH_ICONS_MENUE_TITLES . 'revise.png" alt="" />'; } ?> <?php echo $thisTitle; ?></h1>

				<div class="adminEditArea">
				<form name="formSelectActionDatas" method="post" action="<?php echo $_SERVER["REQUEST_URI"]; ?>" enctype="multipart/form-data">
					<fieldset>
					<table border="0" cellpadding="0" cellspacing="0" width="100%" class="">
						<tr>
							<td style="width:200px;"><b>Aktion:</b></td>
							<td>
								<select class="inputSelect_510" style="font-size:11px;" name="selectAction" id="selectAction" <?php if($_POST["selectAction"] != "") { echo ' disabled="disabled" '; }?>>
									<option value=""> - Bitte w&auml;hlen - </option>
									<?php
										if(!empty($arrActions)){
											$thisMarker = '';
											foreach($arrActions as $thisActionKey => $thisActionValue){
												$tmpMarker = preg_replace("/:(.*)/", "", $thisActionValue["text"]);
												if($tmpMarker != $thisMarker){
													echo '<option value="" class="tableRowTitle1">' . $tmpMarker . '</option>';
													$thisMarker = $tmpMarker;
												}
												$selected = '';
												if($_POST["selectAction"] == $thisActionKey){
													$selected = ' selected="selected" ';
												}
												echo '<option value="' . $thisActionKey . '" ' . $selected . ' style="height:20px;" class="' . $thisActionValue["class"] . '">' . $thisActionValue["text"] . '</option>';
											}
										}
									?>
									</select>
							</td>
							<td>
							<?php
								if($_POST["selectAction"] != "") {
							?>
								<input type="hidden" name="selectAction" class="inputButton1" value="<?php echo $_POST["selectAction"]; ?>" />
							<?php
								}
								if($_POST["selectAction"] == "") {
							?>
								<input type="submit" name="goFormReviseDatas" class="inputButton1" value="Weiter" />
							<?php
								}
							?>
							</td>
						</tr>
					</table>
					</fieldset>
				<?php
					// BOF FORM HANDLE DATAS
					if($_POST["selectAction"] != "" && in_array($_POST["selectAction"], array_keys($arrActions))) {
				?>
					<fieldset>
					<table border="0" cellpadding="0" cellspacing="0" width="100%" class="">
					<?php
						if($_POST["selectAction"] == "ACTION_RELATED_DOCUMENTS"){

						}
						else if(
								$_POST["selectAction"] != "ACTION_CONVERT_AMAZON_TRANSACTION_DATAS" &&
								$_POST["selectAction"] != "ACTION_CONVERT_AMAZON_ORDER_DATAS" &&
								$_POST["selectAction"] != "ACTION_AMAZON_INFORM_ORDER_CUSTOMERS_RATING" &&
								$_POST["selectAction"] != "ACTION_GET_EMAIL_FROM_TEXT" &&
								$_POST["selectAction"] != "ACTION_LAYOUT_FILE_DATAS" &&
								$_POST["selectAction"] != "ACTION_CONVERT_DPD_DELISPRINT_DELIVERY_DATAS" &&
								$_POST["selectAction"] != "ACTION_IMPORT_DPD_PARCEL_INVOICES" &&
								$_POST["selectAction"] != "ACTION_IMPORT_DPD_FTP_INTERFACE_STATUS_DATAS" &&
								$_POST["selectAction"] != "ACTION_FIND_IMPORTED_DPD_STATUS_WITHOUT_DELISPRINT_DELIVERY_DATAS" &&
								$_POST["selectAction"] != "ACTION_FIND_RES_WITH_NO_PRODUCTION_STATUS_SEND" &&
								$_POST["selectAction"] != "ACTION_FIND_ORDERS_IN_DOCUMENTS_OF_ALL_MANDATORIES" &&
								$_POST["selectAction"] != "ACTION_CHECK_RELATED_DOCUMENTS_PAYMENT_STATUS" &&
								$_POST["selectAction"] != "ACTION_FIND_DOCUMENTS_WITHOUT_PAYMENT_STATUS" &&
								$_POST["selectAction"] != "ACTION_CONVERT_CONTAINER_LISTS" &&
								$_POST["selectAction"] != "ACTION_CHECK_GU_HAVING_RE_WITHOUT_PAYMENTS_AND_DIFFERENT_TOTAL_SUM" &&
								$_POST["selectAction"] != "ACTION_FIND_FIND_CUSTOMER_NUMBERS_FOR_DELISPRINT_DELIVERY_DATAS_WITHOUT_CUSTOMER_NUMBERS"
							) {
					?>
						<?php if($_POST["selectAction"] != "ACTION_CHANGE_CUSTOMER_NUMBER"){ ?>
						<tr>
							<td style="width:200px;"><b>Dokumentnummer:</b></td>
							<td><input type="text" name="handleThisDocumentNumber" class="inputField_510" value="<?php echo $_POST["handleThisDocumentNumber"]; ?>" /></td>
						</tr>
						<?php } ?>
						<tr>
							<td style="width:200px;"><b>Aktuelle Kundennummer:</b></td>
							<td>
								<input type="text" name="handleThisDocumentCustomerNumber" class="inputField_510" value="<?php echo $_POST["handleThisDocumentCustomerNumber"]; ?>" />
								<?php
									if($_POST["handleThisDocumentCustomerNumber"] != ""){
										echo '<a href="' . PAGE_EDIT_CUSTOMER . '?searchCustomerNumber=' . $_POST["handleThisDocumentCustomerNumber"] . '" target="_blank" >';
										echo '<img src="layout/menueIcons/menueQuicklinks/distribution.png" width="16" height="16" style="margin: 0 0 -4px 4px;" title="Die Kundendaten anzeigen" alt="Kunden anzeigen" /> ';
										echo '</a>';
									}
								?>
							</td>
						</tr>
						<?php
							if($_POST["selectAction"] != 'ACTION_DELETE_DOCUMENT'){
						?>
						<tr>
							<td style="width:200px;"><b>Neue Kundennummer:</b></td>
							<td>
								<input type="text" name="newDocumentCustomerNumber" class="inputField_510" value="<?php echo $_POST["newDocumentCustomerNumber"]; ?>" />
								<?php
									if($_POST["newDocumentCustomerNumber"] != ""){
										echo '<a href="' . PAGE_EDIT_CUSTOMER . '?searchCustomerNumber=' . $_POST["newDocumentCustomerNumber"] . '" target="_blank" >';
										echo '<img src="layout/menueIcons/menueQuicklinks/distribution.png" width="16" height="16" style="margin: 0 0 -4px 4px;" title="Die Kundendaten anzeigen" alt="Kunden anzeigen" /> ';
										echo '</a>';
									}
								?>
							</td>
						</tr>
						<?php
								}
							}
							else if($_POST["selectAction"] == "ACTION_CONVERT_AMAZON_ORDER_DATAS" || $_POST["selectAction"] == "ACTION_CONVERT_AMAZON_TRANSACTION_DATAS") {
						?>
								<tr>
									<td style="width:200px;"><b>Daten:</b></td>
									<td><textarea name="handleThisAmazonDatas" class="inputTextarea_510x140" rows="40" cols="20"><?php echo $_POST["handleThisAmazonDatas"]; ?></textarea></td>
								</tr>
						<?php
							}
							else if($_POST["selectAction"] == "ACTION_AMAZON_INFORM_ORDER_CUSTOMERS_RATING") {
								$arrGetNotInformedCustomersOrderIDs = getNotInformedCustomersOrderIDs();
								if(!empty($arrGetNotInformedCustomersOrderIDs)){
									$strGetNotInformedCustomersOrderIDs = implode("\n", $arrGetNotInformedCustomersOrderIDs);
								}
						?>
								<tr>
									<td style="width:200px;"><b>Daten:</b></td>
									<td><textarea name="handleThisAmazonDatas" class="inputTextarea_510x140" rows="40" cols="20"><?php echo $strGetNotInformedCustomersOrderIDs; ?></textarea></td>
								</tr>
						<?php
							}
							else if($_POST["selectAction"] == "ACTION_GET_EMAIL_FROM_TEXT") {
								$handleThisMailDatas = $_POST["handleThisMailDatas"];
								$handleThisMailDatas = html_entity_decode($handleThisMailDatas);
								$handleThisMailDatas = preg_replace("/<script.*<\/script>/ismU", "", $handleThisMailDatas);
								$handleThisMailDatas = preg_replace("/<style.*<\/style>/ismU", "", $handleThisMailDatas);
								$handleThisMailDatas = stripslashes(($handleThisMailDatas));
								$handleThisMailDatas = strip_tags(($handleThisMailDatas));
								$handleThisMailDatas = utf8_encode(($handleThisMailDatas));
								$handleThisMailDatas = trim($handleThisMailDatas);
						?>
								<tr>
									<td style="width:200px;"><b>Daten:</b></td>
									<td><textarea name="handleThisMailDatas" class="inputTextarea_510x140" rows="40" cols="20"><?php echo $handleThisMailDatas; ?></textarea></td>
								</tr>
						<?php
							}
							else if($_POST["selectAction"] == "ACTION_LAYOUT_FILE_DATAS") {
								$handleThisLayoutDatas = $_POST["handleThisLayoutDatas"];
						?>
								<tr>
									<td style="width:200px;"><b>COMMAND:</b></td>
									<td>
										<ol>
											<li>chcp 65001</li>
											<li>dir Z:\Vorlagen\Kunden_Mails\ *.pdf /O:D > C:\Users\Thorsten\Desktop\korrekturAbzuege.txt</li>
										</ol>
									</td>
								</tr>
								<tr>
									<td style="width:200px;"><b>ab Datum:</b></td>
									<td><input name="handleThisLayoutDate" id="handleThisLayoutDate" class="inputField_70" value="<?php echo $_POST["handleThisLayoutDate"]; ?>" /></td>
								</tr>
								<tr>
									<td style="width:200px;"><b>Daten:</b></td>
									<td><textarea name="handleThisLayoutDatas" class="inputTextarea_510x140" rows="40" cols="20"><?php echo $handleThisLayoutDatas; ?></textarea></td>
								</tr>
						<?php
							}
							else if($_POST["selectAction"] == "ACTION_CONVERT_DPD_DELISPRINT_DELIVERY_DATAS") {
								$handleThisDeliveryDatas = $_POST["handleThisDeliveryDatas"];
						?>
								<tr>
									<td style="width:200px;"><b>Daten:</b></td>
									<td><textarea name="handleThisDeliveryDatas" class="inputTextarea_510x140" rows="40" cols="20"><?php echo $handleThisDeliveryDatas; ?></textarea></td>
								</tr>
						<?php
							}
							else if($_POST["selectAction"] == "ACTION_IMPORT_DPD_PARCEL_INVOICES") {
								$handleThisDeliveryDatas = $_POST["handleThisDeliveryDatas"];
						?>
								<tr>
									<td style="width:200px;"><b>Daten:</b></td>
									<td><textarea name="handleThisDeliveryDatas" class="inputTextarea_510x140" rows="40" cols="20"><?php echo $handleThisDeliveryDatas; ?></textarea></td>
								</tr>
						<?php
							}
							else if($_POST["selectAction"] == "ACTION_IMPORT_DPD_FTP_INTERFACE_STATUS_DATAS") {
								$handleThisDeliveryDatas = $_POST["handleThisDeliveryDatas"];

								// BOF READ TRACKING FILES
									if($handleThisDeliveryDatas == '' && $_REQUEST["submitFormReviseDatas"] == ""){
										echo '<tr>';
										echo '<td colspan="2">';

										// BOF READ LOCAL FILES
											$arrTempDpdLocalTrackingFiles = read_dir(PATH_DPD_INTERFACE_TRACKING_FILES);
											$arrDpdLocalTrackingFiles = array();
											if(!empty($arrTempDpdLocalTrackingFiles)){
												foreach($arrTempDpdLocalTrackingFiles as $thisDpdLocalTrackingFilesKey => $thisDpdLocalTrackingFilesValue ){
													if(!is_dir($thisDpdLocalTrackingFilesValue) && !preg_match("/" . preg_replace("/\//", "\/", PATH_DPD_INTERFACE_TRACKING_FILES_ZIPPED_FILES) . "/", $thisDpdLocalTrackingFilesValue)){
														$arrDpdLocalTrackingFiles[$thisDpdLocalTrackingFilesKey] = basename($thisDpdLocalTrackingFilesValue);
													}

												}
											}
											else {
												echo '<p class="infoArea">Es liegen keine Dateien vor.</p>';
											}
											echo '<div class="detailsArea">';
												echo '<h3>Anzahl aller lokalen Dateien: ' . count($arrDpdLocalTrackingFiles) . '</h3>';
												echo '<div class="displayDetails">';
												if(!empty($arrDpdLocalTrackingFiles)){
													echo '<ol>';
													foreach($arrDpdLocalTrackingFiles as $thisDpdLocalTrackingFiles){
														echo '<li>' . $thisDpdLocalTrackingFiles . '</li>';
													}
													echo '</ol>';
												}
												else {
													echo '<p class="infoArea">Es liegen keine Dateien vor.</p>';
												}
												echo '</div>';
											echo '</div>';
										// BOF READ LOCAL FILES

										echo '<hr />';

										$dpdServer_connect = ftp_connect(DPD_FTP_INTERFACE_SERVER, DPD_FTP_INTERFACE_PORT);
										if($dpdServer_connect){
											echo '<p class="successArea">FTP-Verbindung zum DPD-SERVER hergestellt!</p>';
											$dpdServer_login = ftp_login($dpdServer_connect, DPD_FTP_INTERFACE_LOGIN, DPD_FTP_INTERFACE_PASSWORD);
											if($dpdServer_login){
												echo '<p class="successArea">Login zum DPD-SERVER hergestellt!</p>';
												echo '<hr />';

												$arrDpdExternalTrackingFiles = ftp_nlist($dpdServer_connect, ".");
												if(!empty($arrDpdExternalTrackingFiles)){

													// BOF EXTERNAL FILES
														echo '<div class="detailsArea">';
															echo '<h3>Anzahl aller externer Dateien: ' . count($arrDpdExternalTrackingFiles) . '</h3>';
															echo '<div class="displayDetails">';
															if(!empty($arrDpdExternalTrackingFiles)){
																echo '<ol>';
																foreach($arrDpdExternalTrackingFiles as $thisDpdExternalTrackingFiles){
																	echo '<li>' . $thisDpdExternalTrackingFiles . '</li>';
																}
																echo '</ol>';
															}
															else {
																echo '<p class="infoArea">Es liegen keine Dateien vor.</p>';
															}
															echo '</div>';
														echo '</div>';
													// EOF EXTERNAL FILES

													// BOF REMOVED EXTERNAL FILES
														echo '<hr />';
														echo '<div class="detailsArea">';
															$arrDpdExternalRemovedTrackingFiles = array_diff($arrDpdLocalTrackingFiles, $arrDpdExternalTrackingFiles);
															echo '<h3>Anzahl entfernter externer Dateien: ' . count($arrDpdExternalRemovedTrackingFiles) . '</h3>';
															echo '<div class="displayDetails">';
															if(!empty($arrDpdExternalRemovedTrackingFiles)){
																echo '<ol>';
																foreach($arrDpdExternalRemovedTrackingFiles as $thisDpdExternalRemovedTrackingFiles){
																	echo '<li>' . $thisDpdExternalRemovedTrackingFiles . '</li>';
																}
																echo '</ol>';
															}
															else {
																echo '<p class="infoArea">Es liegen keine Dateien vor.</p>';
															}
															echo '</div>';
														echo '</div>';

														if(!empty($arrDpdExternalRemovedTrackingFiles)){
															/*
															// BOF ZIP REMOVED EXTERNAL FILES
																$arrFilesToZip = array();
																if($arrDpdExternalRemovedTrackingFiles){
																	foreach($arrDpdExternalRemovedTrackingFiles as $thisDpdExternalRemovedTrackingFiles){
																		$tmpArrFileInfo = pathinfo($thisDpdExternalRemovedTrackingFiles);
																		$arrFilesToZip[$tmpArrFileInfo["filename"]][] = PATH_DPD_INTERFACE_TRACKING_FILES . $tmpArrFileInfo["basename"];
																	}
																}
																if((count($arrFilesToZip) * 2) > 0){
																	echo '<hr />';
																	echo '<div class="detailsArea">';
																		echo '<h3>Anzahl Online entfernter Dateien / zu zippender Dateien: ' . (count($arrFilesToZip) * 2) . '</h3>';
																		echo '<div class="displayDetails">';
																		if(!empty($arrFilesToZip)){
																			echo '<ol>';
																			foreach($arrDpdExternalRemovedTrackingFiles as $thisFilesToZip){
																				echo '<li>' . $thisFilesToZip . '</li>';
																			}
																			echo '</ol>';
																		}
																		echo '</div>';
																	echo '</div>';
																}
																*/

																// BOF DO ZIP
																/*
																if(!empty($arrFilesToZip)){
																	foreach($arrFilesToZip as $thisFilesToZipKey => $thisArrFilesToZip) {
																		foreach($thisArrFilesToZip as $thisFileToZip) {
																			$createCommand = ZIP_PATH.' a -tzip '.PATH_DPD_INTERFACE_TRACKING_FILES_ZIPPED_FILES . $thisFilesToZipKey.'.zip '.$thisFileToZip.' ';

																			$arrExecResults[] = $createCommand;
																			$arrExecResults[] = exec($createCommand, $error);
																			if(!empty($error)) {
																				$arrExecResults[] = array('1. BACKUP: mysqldump error', $error);
																			}
																		}
																	}
																	if(!empty($arrExecResults)){
																		#dd('arrExecResults');
																	}
																}
																*/
																// BOF DO ZIP
															// EOF ZIP REMOVED EXTERNAL FILES
														}
													// EOF REMOVED EXTERNAL FILES

													// BOF NEW EXTERNAL FILES
														echo '<hr />';
														echo '<div class="detailsArea">';
															$arrDpdNewExternalTrackingFiles = array_diff($arrDpdExternalTrackingFiles, $arrDpdLocalTrackingFiles);
															echo '<h3>Anzahl neue externe Dateien: ' . count($arrDpdNewExternalTrackingFiles) . '</h3>';
															echo '<div class="displayDetails">';
															if(!empty($arrDpdNewExternalTrackingFiles)){
																sort($arrDpdNewExternalTrackingFiles);
																echo '<ol>';
																foreach($arrDpdNewExternalTrackingFiles as $thisDpdNewExternalTrackingFiles){
																	echo '<li>' . $thisDpdNewExternalTrackingFiles . '</li>';
																}
																echo '</ol>';
															}
															else {
																echo '<p class="infoArea">Es liegen keine Dateien vor.</p>';
															}
															echo '</div>';
														echo '</div>';
													// EOF NEW EXTERNAL FILES

													// BOF RE-READ EXISTING FILES
														echo '<hr />';

														// BOF GET INSERTED FILES DATA
															$sql_getFilesInserted = "
																SELECT
																		`SOURCE_FILE`,
																		COUNT(`PARCELNO`) AS `countData`
																	FROM `" . TABLE_DELIVERY_DATAS_STATUS_IMPORT . "`

																	WHERE 1
																	GROUP BY `SOURCE_FILE`
																";
															$arrInsertedFilesData = array();
															$rs_getFilesInserted = $dbConnection->db_query($sql_getFilesInserted);
															while($ds_getFilesInserted = mysqli_fetch_assoc($rs_getFilesInserted)){
																$arrInsertedFilesData[$ds_getFilesInserted["SOURCE_FILE"]] = $ds_getFilesInserted["countData"];
															}
														// EOF GET INSERTED FILES DATA

														echo '<h3>Lokale Dateien erneut einlesen?</h3>';
														echo '<div style="margin:2px 0 2px 0;padding:0;">';
														echo '<span class="displayCountSelectedItems"><img src="layout/icons/iconExecuteSelected.png" width="16" height="16" alt="" title="" style="margin-bottom:-4px;" /> <span class="displayCountSelectedItemsFiles">0</span> Dateien ausgew&auml;hlt</span>';
														echo '<span class="buttonCheckAllCheckboxesFiles"><img src="layout/icons/iconSelectAll.png" width="16" height="" alt="" title="" style="margin-bottom:-4px;" /> Alle Dateien ausw&auml;hlen</span>';
														echo '<span class="buttonUncheckAllCheckboxesFiles"><img src="layout/icons/iconDeselectAll.png" width="16" height="" alt="" title="" style="margin-bottom:-4px;" /> Alle Dateien abw&auml;hlen</span>';
														echo '</div>';

														echo '<div style="max-height:200px;overflow:auto;" class="displayDetails2">';
														if(!empty($arrDpdLocalTrackingFiles)) {
															echo '<table cellpadding="0" cellspacing="0" class="displayOrders" style="width:96%">';
															echo '<colgroup>';
															echo '<col />';
															echo '<col />';
															echo '<col />';
															echo '<col />';
															echo '<col />';
															echo '<col />';
															echo '<col />';
															echo '<col />';
															echo '<col />';
															echo '<col />';
															echo '<col />';
															echo '<col />';
															echo '<col />';
															echo '</colgroup>';
															echo '<thead>';
															echo '<tr>';
															echo '<th style="width:15px;">#</th>';
															echo '<th style="width:15px;">x</th>';
															echo '<th>Datei</th>';
															echo '<th class="spacer"></th>';
															echo '<th style="font-size:10px;">DB-Zeilen</th>';
															echo '<th style="font-size:10px;">Datei-Zeilen</th>';
															echo '<th>-</th>';
															echo '<th class="spacer"></th>';
															echo '<th style="font-size:10px;">Datei-Timestamp</th>';
															echo '<th style="font-size:10px;">Download-Timestamp</th>';
															echo '<th>Gr&ouml;&szlig;e</th>';
															echo '<th class="spacer"></th>';
															echo '<th style="width:50px;">Info</th>';
															echo '</tr>';
															echo '</thead>';

															echo '<tbody>';

															$countLocalFile = 0;
															rsort ($arrDpdLocalTrackingFiles);

															$countDbLinesTotal = 0;
															$countFileLinesTotal = 0;
															$trackingFilesizeTotal = 0;

															$marker = '';

															foreach($arrDpdLocalTrackingFiles as $thisDpdLocalTrackingFile){
																$arrPathinfo = pathinfo($thisDpdLocalTrackingFile);
																if($arrPathinfo["extension"] != "sem"){

																	$thisDpdLocalTrackingFileDateTime = getDpdLocalTrackingFileDateTime($thisDpdLocalTrackingFile);
																	$thisDpdLocalTrackingFileDate = substr($thisDpdLocalTrackingFileDateTime, 0, 10);

																	if($countLocalFile%2 == 0){ $rowClass = 'row0'; }
																	else { $rowClass = 'row1'; }

																	$countDbLines = $arrInsertedFilesData[$thisDpdLocalTrackingFile];
																	$arrFileLines = file(PATH_DPD_INTERFACE_TRACKING_FILES . $thisDpdLocalTrackingFile);
																	$countFileLines = (count($arrFileLines) - 1); // -1: INCLUDING FIELD NAMES

																	$countDbLinesTotal += $countDbLines;
																	$countFileLinesTotal += $countFileLines;

																	if($countFileLines - $countDbLines == 0){
																		$rowClass = 'row3';
																	}

																	$addStyle = "";
																	if($marker != $thisDpdLocalTrackingFileDate){
																		$addStyle = "border-top:2px solid #000;";
																		$marker = $thisDpdLocalTrackingFileDate;
																	}

																	echo '<tr class="'.$rowClass.'" style="padding:0;line-height:auto;' . $addStyle . '">';

																	echo '<td style="text-align:right;">';
																	echo '<b>' . ($countLocalFile + 1) . '.</b> ';
																	echo '</td>';

																	echo '<td style="text-align:center;">';
																	echo '<input type="checkbox" class="checkboxReReadLocalFiles" name="reReadLocalFile[' . $countLocalFile . ']" value="' . $thisDpdLocalTrackingFile . '"/>';
																	echo '</td>';

																	echo '<td style="font-size:10px;">';
																	echo $thisDpdLocalTrackingFile . '';
																	echo '</td>';

																	echo '<td class="spacer"></td>';

																	echo '<td style="text-align:right;">';
																	if($countDbLines == ''){
																		$countDbLines = 0;
																	}
																	echo $countDbLines;
																	echo '</td>';

																	echo '<td style="text-align:right;">';
																	if($countFileLines == ''){
																		$countFileLines = 0;
																	}
																	echo $countFileLines;
																	echo '</td>';

																	echo '<td>';
																	if($countFileLines - $countDbLines == 0){
																		$thisStatusImage = "iconOk.png";
																		$thisStatusImageTitle = 'OK';
																	}
																	else {
																		$thisStatusImage = "iconNotOk.png";
																		$thisStatusImageTitle = ($countDbLines - $countFileLines);
																	}
																	echo '<img src="layout/icons/' . $thisStatusImage . '" width="16" height="16" alt="' . $thisStatusImageTitle . '" title="' . $thisStatusImageTitle . '" />';
																	echo '</td>';

																	echo '<td class="spacer"></td>';

																	echo '<td style="font-size:10px;white-space:nowrap;">';
																	echo $thisDpdLocalTrackingFileDateTime;
																	echo '</td>';

																	echo '<td style="font-size:10px;white-space:nowrap;">';
																	echo date("Y-m-d H:i:s", filemtime(PATH_DPD_INTERFACE_TRACKING_FILES . $thisDpdLocalTrackingFile));
																	echo '</td>';

																	echo '<td style="font-size:10px;text-align:right;white-space:nowrap;">';
																	$thisDpdLocalTrackingFilesize = filesize(PATH_DPD_INTERFACE_TRACKING_FILES . $thisDpdLocalTrackingFile);
																	$trackingFilesizeTotal += $thisDpdLocalTrackingFilesize;

																	$thisDpdLocalTrackingFilesize = formatFilesize($thisDpdLocalTrackingFilesize, "B", "KB");
																	echo $thisDpdLocalTrackingFilesize;
																	echo '</td>';

																	echo '<td class="spacer"></td>';

																	echo '<td style="text-align:right;">';
																	echo '<span class="toolItem">';
																	echo '<a href="' . $_SERVER["PHP_SELF"] . '?downloadFile=' . urlencode(basename(PATH_DPD_INTERFACE_TRACKING_FILES . $thisDpdLocalTrackingFile)) . '&documentType=DPD">';
																	echo '<img src="layout/icons/iconRtf.gif" width="16" height="16" alt="" title="" />';
																	echo '</a>';
																	echo '</span>';

																	echo '</td>';

																	echo '</tr>';
																	$countLocalFile++;
																}
															}

															echo '<tr class="row2" style="font-weight: bold;">';
															echo '<td colspan="2">Insg.</td>';
															echo '<td></td>';

															echo '<td class="spacer"></td>';

															echo '<td style="text-align:right;">';
															echo $countDbLinesTotal;
															echo '</td>';

															echo '<td style="text-align:right;">';
															echo $countFileLinesTotal;
															echo '</td>';

															echo '<td>';
															if($countFileLinesTotal - $countDbLinesTotal == 0){
																$thisStatusImage = "iconOk.png";
																$thisStatusImageTitle = 'OK';
																$successMessage .= 'Alle bisherigen Dateien wurden korrekt eingelesen!' . '<br />';
															}
															else {
																$thisStatusImage = "iconNotOk.png";
																$thisStatusImageTitle = ($countDbLinesTotal - $countFileLinesTotal);
																$errorMessage .= 'Nicht alle Dateien wurden eingelesen!' . '<br />';
																$jswindowMessage .= 'Nicht alle bisherigen Dateien wurden korrekt eingelesen!' . '<br />';
															}
															echo '<img src="layout/icons/' . $thisStatusImage . '" width="16" height="16" alt="' . $thisStatusImageTitle . '" title="' . $thisStatusImageTitle . '" />';
															echo '</td>';

															echo '<td class="spacer"></td>';

															echo '<td></td>';
															echo '<td></td>';

															echo '<td style="text-align:right;white-space:nowrap;">';
															$trackingFilesizeTotal = formatFilesize($trackingFilesizeTotal, "B", "KB");
															echo $trackingFilesizeTotal;
															echo '</td>';

															echo '<td class="spacer"></td>';

															echo '<td></td>';
															echo '</tr>';

															echo '</tbody>';
															echo '</table>';
														}
														echo '</div>';
													// EOF RE-READ EXISTING FILES

													echo '<br />';
													displayMessages();

													echo '<hr />';

													// BOF GET AND READ NEW EXTERNAL FILES
														foreach($arrDpdNewExternalTrackingFiles as $thisDpdNewExternalTrackingFile){
															$dpdServer_rsGetFile = ftp_get($dpdServer_connect, PATH_DPD_INTERFACE_TRACKING_FILES . basename($thisDpdNewExternalTrackingFile), $thisDpdNewExternalTrackingFile, FTP_BINARY);
															if($dpdServer_rsGetFile){
																echo '<p class="successArea">Datei &quot;' . $thisDpdNewExternalTrackingFile. '&quot; vom DPD-SERVER heruntergeladen!</p>';
																$arrThisPathinfo = pathinfo(PATH_DPD_INTERFACE_TRACKING_FILES . basename($thisDpdNewExternalTrackingFile));
																if(strtolower($arrThisPathinfo["extension"]) != "sem"){
																	$fp_dpdTrackingFile = fopen(PATH_DPD_INTERFACE_TRACKING_FILES . basename($thisDpdNewExternalTrackingFile), 'r');
																	$dpdTrackingFileContent = fread($fp_dpdTrackingFile, filesize(PATH_DPD_INTERFACE_TRACKING_FILES . basename($thisDpdNewExternalTrackingFile)));
																	fclose($fp_dpdTrackingFile);
																	if($dpdTrackingFileContent != ''){
																		$dpdTrackingFileContent = mb_convert_encoding($dpdTrackingFileContent, 'UTF-8');
																		// BOF ADD SOURCE FILENAME TO DATA LINE
																		$dpdTrackingFileContent = preg_replace("/^/", basename($thisDpdNewExternalTrackingFile) . ";", $dpdTrackingFileContent);
																		$dpdTrackingFileContent = preg_replace("/(\n)([0-9])/", "$1" . basename($thisDpdNewExternalTrackingFile).";$2", $dpdTrackingFileContent);
																		// EOF ADD SOURCE FILENAME TO DATA LINE
																		$handleThisDeliveryDatas .= $dpdTrackingFileContent;
																	}
																}
															}
															else {
																echo '<p class="errorArea">Datei &quot;' . $thisDpdNewExternalTrackingFile. '&quot; NICHT vom DPD-SERVER heruntergeladen!</p>';
															}
														}
													// BOF GET AND READ NEW EXTERNAL FILES
												}
											}
											else {
												echo '<p class="errorArea">Kein Login zum DPD-SERVER!</p>';
											}
											$dpdServer_close = ftp_close($dpdServer_connect);
										}
										else {
											echo '<p class="errorArea">Keine FTP-Verbindung zum DPD-SERVER!</p>';
										}
										echo '</td>';
										echo '</tr>';
									}
								// EOF READ TRACKING FILES

						?>
								<tr>
									<td style="width:200px;"><b>Neue Daten:</b></td>
									<td>
										<?php
											if($handleThisDeliveryDatas != ''){
												$countNewDataRows = substr_count($handleThisDeliveryDatas, "\n");
												echo '<p class="infoArea">Anzahl neuer Zeilen: ' . $countNewDataRows . '</p>';
											}
										?>
										<textarea name="handleThisDeliveryDatas" class="inputTextarea_510x140" rows="40" cols="20"><?php echo $handleThisDeliveryDatas; ?></textarea>
									</td>
								</tr>
								<?php
									if($_POST["handleThisDeliveryDatas"] == '') { $handleThisDeliveryDatas = ''; }
								?>
						<?php
							}
							else if($_POST["selectAction"] == "ACTION_FIND_IMPORTED_DPD_STATUS_WITHOUT_DELISPRINT_DELIVERY_DATAS") {
								$sql_findImportedDpdStatusWithoutDelisprintDeliveryDatas = "
									SELECT
											`SOURCE_FILE`,
											`PARCELNO`,
											GROUP_CONCAT(DISTINCT IF(`SCAN_CODE` IS NULL, '', `SCAN_CODE`) ORDER BY `SCAN_CODE` DESC) AS `SCAN_CODES`,
											`DEPOT_CODE`,
											`DEPOTNAME`,
											`EVENT_DATE_TIME`,
											CONCAT(
												SUBSTRING(`" . TABLE_DELIVERY_DATAS_STATUS_IMPORT . "`.`EVENT_DATE_TIME`, 1, 4),
												'-',
												SUBSTRING(`" . TABLE_DELIVERY_DATAS_STATUS_IMPORT . "`.`EVENT_DATE_TIME`, 5, 2),
												'-',
												SUBSTRING(`" . TABLE_DELIVERY_DATAS_STATUS_IMPORT . "`.`EVENT_DATE_TIME`, 7, 2),
												' ',
												SUBSTRING(`" . TABLE_DELIVERY_DATAS_STATUS_IMPORT . "`.`EVENT_DATE_TIME`, 9, 2),
												':',
												SUBSTRING(`" . TABLE_DELIVERY_DATAS_STATUS_IMPORT . "`.`EVENT_DATE_TIME`, 11, 2),
												':',
												SUBSTRING(`" . TABLE_DELIVERY_DATAS_STATUS_IMPORT . "`.`EVENT_DATE_TIME`, 13, 2)
											) AS `EVENT_DATE_TIME_FORMATTED`

										FROM `" . TABLE_DELIVERY_DATAS_STATUS_IMPORT . "`

										LEFT JOIN `" . TABLE_DELIVERY_DATAS . "`
										ON(`" . TABLE_DELIVERY_DATAS_STATUS_IMPORT . "`.`PARCELNO` = `" . TABLE_DELIVERY_DATAS . "`.`Paketnummer`)

										WHERE 1

										AND
											`" . TABLE_DELIVERY_DATAS . "`.`Paketnummer` IS NULL

										GROUP BY `" . TABLE_DELIVERY_DATAS_STATUS_IMPORT . "`.`PARCELNO`
										ORDER  BY `" . TABLE_DELIVERY_DATAS_STATUS_IMPORT . "`.`PARCELNO`
									";

								$rs_findImportedDpdStatusWithoutDelisprintDeliveryDatas = $dbConnection->db_query($sql_findImportedDpdStatusWithoutDelisprintDeliveryDatas);

								echo '<h2>Paketnummern, die gar nicht im DeliSprint-Export auftauchen, aber ein DPD-Status vorliegt:</h2>';
								if($dbConnection->db_getMysqlNumRows($rs_findImportedDpdStatusWithoutDelisprintDeliveryDatas) > 0){
									$countRow = 0;

									echo '<table border="0" cellpadding="0" cellspacing="0" style="width:99%;" class="displayOrders">';

									while($ds_findImportedDpdStatusWithoutDelisprintDeliveryDatas = mysqli_fetch_assoc($rs_findImportedDpdStatusWithoutDelisprintDeliveryDatas)){
										if($countRow == 0){
											echo '<tr>';
											echo '<th style="width:45px;">#</th>';
											echo '<th style="width:300px;">Status-Datei</th>';
											echo '<th style="width:200px;">Paket-Nr.</th>';
											echo '<th>Scan-Code</th>';
											echo '<th>Depot</th>';
											echo '<th>Status-Datum</th>';
											echo '<th>Info</th>';
											echo '</tr>';
										}

										if($countRow%2 == 0){ $rowClass = 'row1'; }
										else { $rowClass = 'row4'; }

										echo '<tr class="' . $rowClass . '">';

										echo '<td style="text-align:right;"><b>';
										echo ($countRow + 1);
										echo '.</b></td>';

										echo '<td>';
										echo $ds_findImportedDpdStatusWithoutDelisprintDeliveryDatas["SOURCE_FILE"];
										echo '</td>';

										echo '<td style="white-space:nowrap;">';
										echo '<a href="' . PAGE_DELIVERY_HISTORY . '?searchDeliveryNumber=' . $ds_findImportedDpdStatusWithoutDelisprintDeliveryDatas["PARCELNO"] . '" target="_blank">' . $ds_findImportedDpdStatusWithoutDelisprintDeliveryDatas["PARCELNO"] . '</a>';
										echo getPrintProductionPlace($ds_findImportedDpdStatusWithoutDelisprintDeliveryDatas["PARCELNO"], "flag");
										echo '</td>';

										echo '<td style="white-space:nowrap;">';
										if($ds_findImportedDpdStatusWithoutDelisprintDeliveryDatas["SCAN_CODES"] != ''){
											$arrScanCodes = explode(",", $ds_findImportedDpdStatusWithoutDelisprintDeliveryDatas["SCAN_CODES"]);
											foreach($arrScanCodes as $thisScanCode){
												echo ' &bull; ';
												echo $arrDpdScanCodes[$thisScanCode] . ' [' . $thisScanCode . ']';
												echo $arrDpdScanCodes[$ds_findImportedDpdStatusWithoutDelisprintDeliveryDatas["SCAN_CODE"]];
											}
										}
										echo '</td>';

										echo '<td style="white-space:nowrap;">';
										echo $ds_findImportedDpdStatusWithoutDelisprintDeliveryDatas["DEPOT_CODE"];
										echo '</td>';

										echo '<td style="white-space:nowrap;">';
										echo formatDate($ds_findImportedDpdStatusWithoutDelisprintDeliveryDatas["EVENT_DATE_TIME_FORMATTED"], 'display');
										echo '</td>';

										echo '<td style="white-space:nowrap;">';
										echo '<a href="' . PAGE_SEARCH_TRACKING . '?searchTrackingNumber=' . $ds_findImportedDpdStatusWithoutDelisprintDeliveryDatas["PARCELNO"] . '" target="_blank">';
										echo '<img src="layout/menueIcons/menueSidebar/dpd.png" width="16" height="16" title="DPD-Tracking" alt="DPD" />';
										echo '</a>';
										echo '</td>';

										echo '</tr>';

										$countRow++;
									}
									echo '</table>';
								}
								else {
									echo '<p class="infoArea">Es wurden keine Daten gefunden.</p>';
								}

								echo '<hr />';

								echo '<h2>Paketnummern abh&auml;ngig vom Zeitraum, die gar nicht im DeliSprint-Export auftauchen, aber ein DPD-Status vorliegt (unter Vorbehalt):</h2>';

								echo '<div id="searchFilterArea"><div id="filterScanCodesArea"></div></div>';

								$sql_findImportedDpdStatusWithoutDelisprintDeliveryDatas = "
										SELECT
											`SOURCE_FILE`,
											`PARCELNO`,
											GROUP_CONCAT(DISTINCT `SCAN_CODES`) AS `SCAN_CODES`,
											`DEPOT_CODE`,
											`DEPOTNAME`,
											/* `CONSIGNEE_ZIP`, */
											GROUP_CONCAT(DISTINCT `CONSIGNEE_ZIP`) AS `CONSIGNEE_ZIP`,
											GROUP_CONCAT(DISTINCT `CONSIGNEE_ZIP_ALL`) AS `CONSIGNEE_ZIP_ALL2`,
											GROUP_CONCAT(IF(LOCATE(',', `CONSIGNEE_ZIP_ALL`) = LENGTH(`CONSIGNEE_ZIP_ALL`), SUBSTRING(`CONSIGNEE_ZIP_ALL`, 1, (LENGTH(`CONSIGNEE_ZIP_ALL`)-1)), `CONSIGNEE_ZIP_ALL`)) AS `CONSIGNEE_ZIP_ALL`,

											`EVENT_DATE_FORMATTED`,
											IF(`" . TABLE_DELIVERY_DATAS . "`.`Datum` IS NULL, '', DATE_FORMAT(`" . TABLE_DELIVERY_DATAS . "`.`Datum`, '%Y-%m-%d')) AS `Datum_Formatted`,
											IF(`" . TABLE_DELIVERY_DATAS . "`.`Datum` IS NULL, '', DATE_ADD(DATE_FORMAT(`" . TABLE_DELIVERY_DATAS . "`.`Datum`, '%Y-%m-%d'), INTERVAL 3 MONTH)) AS `Datum_Interval`,

											`EVENT_DATE_TIME`,
											`EVENT_DATE_TIME_FORMATTED`,
											`Paketnummer`,
											`Datum`

											FROM (
													SELECT
														`" . TABLE_DELIVERY_DATAS_STATUS_IMPORT . "`.`SOURCE_FILE`,
														`" . TABLE_DELIVERY_DATAS_STATUS_IMPORT . "`.`PARCELNO`,
														GROUP_CONCAT(DISTINCT IF(`SCAN_CODE` IS NULL, '', `SCAN_CODE`) ORDER BY `SCAN_CODE` DESC) AS `SCAN_CODES`,
														`" . TABLE_DELIVERY_DATAS_STATUS_IMPORT . "`.`DEPOT_CODE`,
														`" . TABLE_DELIVERY_DATAS_STATUS_IMPORT . "`.`DEPOTNAME`,
														`" . TABLE_DELIVERY_DATAS_STATUS_IMPORT . "`.`EVENT_DATE_TIME`,

														`" . TABLE_DELIVERY_DATAS_STATUS_IMPORT . "`.`CONSIGNEE_ZIP`,
														GROUP_CONCAT(DISTINCT `" . TABLE_DELIVERY_DATAS_STATUS_IMPORT . "`.`CONSIGNEE_ZIP`) AS `CONSIGNEE_ZIP_ALL`,

														CONCAT(
															SUBSTRING(`" . TABLE_DELIVERY_DATAS_STATUS_IMPORT . "`.`EVENT_DATE_TIME`, 1, 4),
															'-',
															SUBSTRING(`" . TABLE_DELIVERY_DATAS_STATUS_IMPORT . "`.`EVENT_DATE_TIME`, 5, 2),
															'-',
															SUBSTRING(`" . TABLE_DELIVERY_DATAS_STATUS_IMPORT . "`.`EVENT_DATE_TIME`, 7, 2),
															' ',
															SUBSTRING(`" . TABLE_DELIVERY_DATAS_STATUS_IMPORT . "`.`EVENT_DATE_TIME`, 9, 2),
															':',
															SUBSTRING(`" . TABLE_DELIVERY_DATAS_STATUS_IMPORT . "`.`EVENT_DATE_TIME`, 11, 2),
															':',
															SUBSTRING(`" . TABLE_DELIVERY_DATAS_STATUS_IMPORT . "`.`EVENT_DATE_TIME`, 13, 2)
														) AS `EVENT_DATE_TIME_FORMATTED`,

														CONCAT(
															SUBSTRING(`" . TABLE_DELIVERY_DATAS_STATUS_IMPORT . "`.`EVENT_DATE_TIME`, 1, 4),
															'-',
															SUBSTRING(`" . TABLE_DELIVERY_DATAS_STATUS_IMPORT . "`.`EVENT_DATE_TIME`, 5, 2),
															'-',
															SUBSTRING(`" . TABLE_DELIVERY_DATAS_STATUS_IMPORT . "`.`EVENT_DATE_TIME`, 7, 2)
														) AS `EVENT_DATE_FORMATTED`

														FROM `" . TABLE_DELIVERY_DATAS_STATUS_IMPORT . "`


														WHERE 1
															/*
															AND `" . TABLE_DELIVERY_DATAS_STATUS_IMPORT . "`.`PARCELNO` = '01485004280002'
															*/


														GROUP BY CONCAT(`" . TABLE_DELIVERY_DATAS_STATUS_IMPORT . "`.`PARCELNO`, `EVENT_DATE_FORMATTED`)
											) AS `tempTable`

											LEFT JOIN `" . TABLE_DELIVERY_DATAS . "`
											ON(
												`tempTable`.`PARCELNO` = `" . TABLE_DELIVERY_DATAS . "`.`Paketnummer`
												AND (
													`EVENT_DATE_FORMATTED`
														BETWEEN
															DATE_SUB(DATE_FORMAT(`" . TABLE_DELIVERY_DATAS . "`.`Datum`, '%Y-%m-%d'), INTERVAL 3 DAY)
															AND
															DATE_ADD(DATE_FORMAT(`" . TABLE_DELIVERY_DATAS . "`.`Datum`, '%Y-%m-%d'), INTERVAL 4 MONTH)
													OR
														`tempTable`.`CONSIGNEE_ZIP` = `" . TABLE_DELIVERY_DATAS . "`.`PLZ_1`
												)
											)

											WHERE 1

											GROUP BY CONCAT(`PARCELNO`, IF(`Datum` IS NULL, '', `Datum`))

											HAVING
												`" . TABLE_DELIVERY_DATAS . "`.`Datum` IS NULL

											ORDER BY
												`EVENT_DATE_TIME` DESC,
												`PARCELNO` ASC
									";

								$rs_findImportedDpdStatusWithoutDelisprintDeliveryDatas = $dbConnection->db_query($sql_findImportedDpdStatusWithoutDelisprintDeliveryDatas);

								$scanCodeItemsCount = $dbConnection->db_getMysqlNumRows($rs_findImportedDpdStatusWithoutDelisprintDeliveryDatas);
								echo '<p class="infoArea">Anzahl Datens&auml;tze: <span id="scanCodeItemsCount">' . $scanCodeItemsCount . '</span></p>';

								if($dbConnection->db_getMysqlNumRows($rs_findImportedDpdStatusWithoutDelisprintDeliveryDatas) > 0){

									$countRow = 0;

									$arrAllScanCodesInDatas = array();

									echo '<table border="0" cellpadding="0" cellspacing="0" style="width:99%;" class="displayOrders">';

									while($ds_findImportedDpdStatusWithoutDelisprintDeliveryDatas = mysqli_fetch_assoc($rs_findImportedDpdStatusWithoutDelisprintDeliveryDatas)){

										if($countRow == 0){
											echo '<tr>';
											echo '<th style="width:45px;">#</th>';
											echo '<th style="width:300px;">Status-Datei</th>';
											echo '<th style="width:200px;">Paket-Nr.</th>';
											echo '<th>Scan-Code</th>';
											echo '<th>Depot</th>';
											echo '<th>Status-Datum</th>';
											echo '<th>Info</th>';
											echo '</tr>';
										}

										if($ds_findImportedDpdStatusWithoutDelisprintDeliveryDatas["SCAN_CODES"] != ''){
											$arrScanCodes = explode(",", $ds_findImportedDpdStatusWithoutDelisprintDeliveryDatas["SCAN_CODES"]);
											foreach($arrScanCodes as $thisScanCode){
												$arrAllScanCodesInDatas[] = $thisScanCode;
											}
										}

										if($countRow%2 == 0){ $rowClass = 'row1'; }
										else { $rowClass = 'row4'; }

										if(!empty($arrScanCodes)){
											$thisClassScanCode = 'scanCode_' . implode(" scanCode_", $arrScanCodes) . ' ';
										}
										echo '<tr class="' . $rowClass . ' ' . $thisClassScanCode . '">';

										echo '<td style="text-align:right;"><b>';
										echo ($countRow + 1);
										echo '.</b></td>';

										echo '<td>';
										echo $ds_findImportedDpdStatusWithoutDelisprintDeliveryDatas["SOURCE_FILE"];
										echo '</td>';

										echo '<td style="white-space:nowrap;">';
										echo '<a href="' . PAGE_DELIVERY_HISTORY . '?searchDeliveryNumber=' . $ds_findImportedDpdStatusWithoutDelisprintDeliveryDatas["PARCELNO"] . '" target="_blank">' . $ds_findImportedDpdStatusWithoutDelisprintDeliveryDatas["PARCELNO"] . '</a>';
										echo getPrintProductionPlace($ds_findImportedDpdStatusWithoutDelisprintDeliveryDatas["PARCELNO"], "flag");
										echo '</td>';

										echo '<td style="white-space:nowrap;">';
										if(!empty($arrScanCodes)){
											sort($arrScanCodes);
											foreach($arrScanCodes as $thisScanCode){
												echo ' &bull; ';
												echo '<span style="font-size:10px;">' . $arrDpdScanCodes[$thisScanCode] . '</span>';
												echo ' <b>[' . $thisScanCode . ']</b>';
												echo $arrDpdScanCodes[$ds_findImportedDpdStatusWithoutDelisprintDeliveryDatas["SCAN_CODE"]];
											}
										}
										echo '</td>';

										echo '<td style="white-space:nowrap;">';
										echo $ds_findImportedDpdStatusWithoutDelisprintDeliveryDatas["DEPOT_CODE"];
										echo '</td>';

										echo '<td style="white-space:nowrap;cursor:pointer;" title="' . formatDate($ds_findImportedDpdStatusWithoutDelisprintDeliveryDatas["EVENT_DATE_TIME_FORMATTED"], 'display') . '">';
										echo formatDate($ds_findImportedDpdStatusWithoutDelisprintDeliveryDatas["EVENT_DATE_TIME_FORMATTED"], 'display');
										#echo formatDate($ds_findImportedDpdStatusWithoutDelisprintDeliveryDatas["EVENT_DATE_FORMATTED"], 'display');
										echo '</td>';

										echo '<td style="white-space:nowrap;">';
										echo '<a href="' . PAGE_SEARCH_TRACKING . '?searchTrackingNumber=' . $ds_findImportedDpdStatusWithoutDelisprintDeliveryDatas["PARCELNO"] . '" target="_blank">';
										echo '<img src="layout/menueIcons/menueSidebar/dpd.png" width="16" height="16" title="DPD-Tracking" alt="DPD" />';
										echo '</a>';
										echo '</td>';

										echo '</tr>';

										$countRow++;
									}
									echo '</table>';

									// BOF GET SCAN CODES IN DATA
										if(!empty($arrAllScanCodesInDatas)){
											$arrAllScanCodesInDatas = array_unique($arrAllScanCodesInDatas);
											sort($arrAllScanCodesInDatas);

											$scanCodeFilterSelections = '';
											$scanCodeFilterSelections .= '<table>';
											$scanCodeFilterSelections .= '<tr>';
											$scanCodeFilterSelections .= '<td>';
											$scanCodeFilterSelections .= '<b>Scan-Code-Filter: </b><br />';
											$scanCodeFilterSelections .= '</td>';
											$scanCodeFilterSelections .= '<td>';
											$scanCodeFilterSelections .= '<span style="padding:0 20px 0 0;" class="buttonScanCodeFilter" id="scanCode_ALL">Alle</span>';
											foreach($arrAllScanCodesInDatas as $thisScanCodesInDatas){
												$scanCodeFilterSelections .= '<span style="padding:0 20px 0 0; color:#00F" class="buttonScanCodeFilter" id="scanCode_' . $thisScanCodesInDatas . '">' . $thisScanCodesInDatas . ': ' . $arrDpdScanCodes[$thisScanCodesInDatas] . '</span>';
											}
											$scanCodeFilterSelections .= '</td>';
											$scanCodeFilterSelections .= '</tr>';
											$scanCodeFilterSelections .= '</table>';
											$scanCodeFilterSelectionsForJS = $scanCodeFilterSelections;
											$scanCodeFilterSelectionsForJS = preg_replace("/\//", "\/", $scanCodeFilterSelectionsForJS);
											$scanCodeFilterSelectionsForJS = preg_replace('/\"/', '\"', $scanCodeFilterSelectionsForJS);
											echo '
												<script language="javascript" type="text/javascript">
													<!--
													/* <![CDATA[ */
													$(document).ready(function() {
														$("#filterScanCodesArea").html("' . $scanCodeFilterSelectionsForJS . '");
														$(".buttonScanCodeFilter").css("cursor", "pointer");
														$(".buttonScanCodeFilter").live("click", function(){
															var selectedScanFilterID = $(this).attr("id");
															if(selectedScanFilterID == "scanCode_ALL"){
																$(".displayOrders tr[class*=\'scanCode_\']").show();
															}
															else {
																$(".displayOrders tr[class*=\'scanCode_\']").hide();
																$(".displayOrders tr[class*=\'" + selectedScanFilterID + "\']").show();
															}
															var scanCodeItemsCount = $(".displayOrders tr[class*=\'scanCode_\']:visible").length;
															$("#scanCodeItemsCount").html(scanCodeItemsCount);
														});
													});
													/* ]]> */
													// -->
												</script>
											';
										}
									// EOF GET SCAN CODES IN DATA
								}
								else {
									echo '<p class="infoArea">Es wurden keine Daten gefunden.</p>';
								}
							}
							else if($_POST["selectAction"] == "ACTION_CONVERT_CONTAINER_LISTS") {
								$handleThisDeliveryDatas = $_POST["handleThisDeliveryDatas"];
								$handleThisDeliveryDate = $_POST["handleThisDeliveryDate"];
						?>
								<tr>
									<td style="width:200px;"><b>Datei/Datum:</b></td>
									<td><input name="handleThisDeliveryDate" class="inputField_510" value="<?php echo $handleThisDeliveryDate; ?>" /></td>
								</tr>
								<tr>
									<td style="width:200px;"><b>Daten:</b></td>
									<td><textarea name="handleThisDeliveryDatas" class="inputTextarea_510x140" rows="40" cols="20"><?php echo $handleThisDeliveryDatas; ?></textarea></td>
								</tr>
						<?php
							}
							else if($_POST["selectAction"] == "ACTION_FIND_RES_WITH_NO_PRODUCTION_STATUS_SEND") {
								$handleThisFindProductionStatusDatas = $_POST["handleThisFindProductionStatusDatas"];
								$arrMandatoriesData = getMandatories();
								$arrMandatories = array();
								if(!empty($arrMandatoriesData)) {
									foreach($arrMandatoriesData as $thisMandatoryKey => $thisMandatoryValue){
										$arrMandatories[] = $thisMandatoryValue["mandatoriesShortName"];
									}
								}
						?>
								<tr>
									<td style="width:200px;"><b>Mandant:</b></td>
									<td>
										<select name="handleThisFindProductionStatusDatas" class="inputSelect_510">
											<?php
												if(!empty($arrMandatories)) {
													echo '<option value="' . implode(";", $arrMandatories) . '">' . implode(" & ", $arrMandatories) . '</option>';
													foreach($arrMandatories as $thisMandatoryKey => $thisMandatoryValue){
														$selected = '';
														if(MANDATOR == $thisMandatoryValue){
															$selected = ' selected="selected" ';
														}
														echo '<option value="' . $thisMandatoryValue . '" ' . $selected . '>' . $thisMandatoryValue . '</option>';
													}
												}
											?>
										</select>
									</td>
								</tr>
						<?php
							}
							else if($_POST["selectAction"] == "ACTION_FIND_ORDERS_IN_DOCUMENTS_OF_ALL_MANDATORIES") {
								$handleThisFindOrdersInDocumentsOfAllMandatories = $_POST["handleThisFindOrdersInDocumentsOfAllMandatories"];
								$arrMandatoriesData = getMandatories();
								$arrMandatories = array();
								if(!empty($arrMandatoriesData)) {
									foreach($arrMandatoriesData as $thisMandatoryKey => $thisMandatoryValue){
										$arrMandatories[] = $thisMandatoryValue["mandatoriesShortName"];
									}
								}
						?>
								<tr>
									<td style="width:200px;"><b>Mandant:</b></td>
									<td>
										<select name="handleThisFindOrdersInDocumentsOfAllMandatories" class="inputSelect_510">
											<?php
												if(!empty($arrMandatories)) {
													echo '<option value="' . implode(';', $arrMandatories) . '" ' . $selected . '>' . implode(' & ', $arrMandatories) . '</option>';
												}
											?>
										</select>
									</td>
								</tr>
						<?php
							}
							else if($_POST["selectAction"] == "ACTION_FIND_DOCUMENTS_WITHOUT_PAYMENT_STATUS"){
								$handleThisFindDocumentsWithoutPaymentStatus = $_POST["handleThisFindDocumentsWithoutPaymentStatus"];
								$arrMandatoriesData = getMandatories();
								$arrMandatories = array();
								if(!empty($arrMandatoriesData)) {
									foreach($arrMandatoriesData as $thisMandatoryKey => $thisMandatoryValue){
										$arrMandatories[] = $thisMandatoryValue["mandatoriesShortName"];
									}
								}
						?>
								<tr>
									<td style="width:200px;"><b>Mandant:</b></td>
									<td>
										<select name="handleThisFindDocumentsWithoutPaymentStatus" class="inputSelect_510">
											<?php
												if(!empty($arrMandatories)) {
													foreach($arrMandatories as $thisMandatory){
														if($thisMandatory == MANDATOR){
															echo '<option value="' . $thisMandatory . '" ' . $selected . '>' . $thisMandatory . '</option>';
														}
													}
												}
											?>
										</select>
									</td>
								</tr>
						<?php
							}
							else if(
								($_POST["selectAction"] == "ACTION_CHECK_RELATED_DOCUMENTS_PAYMENT_STATUS")
								|| ($_POST["selectAction"] == "ACTION_CHECK_GU_HAVING_RE_WITHOUT_PAYMENTS_AND_DIFFERENT_TOTAL_SUM")
								){
								$handleThisCheckRelatedDocumentPaymentStatus = $_POST["handleThisCheckRelatedDocumentPaymentStatus"];
								$arrMandatoriesData = getMandatories();
								$arrMandatories = array();
								if(!empty($arrMandatoriesData)) {
									foreach($arrMandatoriesData as $thisMandatoryKey => $thisMandatoryValue){
										$arrMandatories[] = $thisMandatoryValue["mandatoriesShortName"];
									}
								}
						?>
								<tr>
									<td style="width:200px;"><b>Mandant:</b></td>
									<td>
										<select name="handleThisCheckRelatedDocumentPaymentStatus" class="inputSelect_510">
											<?php
												if(!empty($arrMandatories)) {
													foreach($arrMandatories as $thisMandatory){
														if($thisMandatory == MANDATOR){
															echo '<option value="' . $thisMandatory . '" ' . $selected . '>' . $thisMandatory . '</option>';
														}
													}
												}
											?>
										</select>
									</td>
								</tr>
						<?php
							}
							else if($_POST["selectAction"] == "ACTION_FIND_FIND_CUSTOMER_NUMBERS_FOR_DELISPRINT_DELIVERY_DATAS_WITHOUT_CUSTOMER_NUMBERS"){
								$sql_getCustomerNumbersForDeliverydatasWithoutCustomerNumber_1 = "
										SELECT
												*
											FROM `" . TABLE_DELIVERY_DATAS . "`
											WHERE 1
												AND `" . TABLE_DELIVERY_DATAS . "`.`Ref_Adresse_1` = ''
											ORDER BY
												`" . TABLE_DELIVERY_DATAS . "`.`Datum` DESC,
												`" . TABLE_DELIVERY_DATAS . "`.`Firma_Empfaenger` ASC,
												`" . TABLE_DELIVERY_DATAS . "`.`Paketnummer` ASC
									";

								$sql_getCustomerNumbersForDeliverydatasWithoutCustomerNumber_2 = "
										SELECT
												`" . TABLE_CUSTOMERS . "`.`customersKundennummer` AS `Auftragslisten-KNR`,

												`" . TABLE_DELIVERY_DATAS . "`.`Paketnummer`,
												`" . TABLE_DELIVERY_DATAS . "`.`Datum`,
												/* `" . TABLE_DELIVERY_DATAS . "`.`Produkt_Service`, */

												`" . TABLE_DELIVERY_DATAS . "`.`Referenznr_1`,
												`" . TABLE_DELIVERY_DATAS . "`.`Versanddatum`,
												/* `" . TABLE_DELIVERY_DATAS . "`.`Zustelldatum`, */
												`" . TABLE_DELIVERY_DATAS . "`.`Firma_Empfaenger`,
												`" . TABLE_CUSTOMERS . "`.`customersFirmenname` AS `Auftragslisten-Firma`,

												`" . TABLE_DELIVERY_DATAS . "`.`Adresse_1`,
												CONCAT(`" . TABLE_CUSTOMERS . "`.`customersCompanyStrasse`, ' ', `" . TABLE_CUSTOMERS . "`.`customersCompanyHausnummer`) AS `Auftragslisten-Adresse`,

												`" . TABLE_DELIVERY_DATAS . "`.`PLZ_1`,
												`" . TABLE_CUSTOMERS . "`.`customersCompanyPLZ` AS `Auftragslisten-PLZ`,

												`" . TABLE_DELIVERY_DATAS . "`.`Stadt_1`,
												`" . TABLE_CUSTOMERS . "`.`customersCompanyOrt` AS `Auftragslisten-Ort`

											FROM `" . TABLE_DELIVERY_DATAS . "`

											INNER JOIN `" . TABLE_CUSTOMERS . "`
											ON(
												REPLACE(`" . TABLE_DELIVERY_DATAS . "`.`Firma_Empfaenger`, ' ', '') = REPLACE(`" . TABLE_CUSTOMERS . "`.`customersFirmenname`, ' ', '')
												AND
												`" . TABLE_DELIVERY_DATAS . "`.`PLZ_1` = `" . TABLE_CUSTOMERS . "`.`customersCompanyPLZ`
											)

											WHERE 1
												AND `" . TABLE_DELIVERY_DATAS . "`.`Ref_Adresse_1` = ''

											GROUP BY `" . TABLE_CUSTOMERS . "`.`customersKundennummer`

											ORDER BY
												`" . TABLE_DELIVERY_DATAS . "`.`Datum` DESC,
												`" . TABLE_CUSTOMERS . "`.`customersFirmenname` ASC,
												`" . TABLE_DELIVERY_DATAS . "`.`Paketnummer` ASC
									";	

								$sql_getCustomerNumbersForDeliverydatasWithoutCustomerNumber_3 = "
										SELECT
												`tempTable`.`Auftragslisten-KNR`,

												`tempTable`.`Paketnummer`,
												`tempTable`.`Datum`,
												/* `tempTable`.`Produkt_Service`, */

												`tempTable`.`Referenznr_1`,
												`tempTable`.`Versanddatum`,
												/* `tempTable`.`Zustelldatum`, */
												`tempTable`.`Firma_Empfaenger`,
												`tempTable`.`Auftragslisten-Firma`,

												`tempTable`.`Adresse_1`,
												`tempTable`.`Auftragslisten-Adresse`,

												`tempTable`.`PLZ_1`,
												`tempTable`.`Auftragslisten-PLZ`,

												`tempTable`.`Stadt_1`,
												`tempTable`.`Auftragslisten-Ort`
											FROM (

												SELECT
													`" . TABLE_CUSTOMERS . "`.`customersKundennummer` AS `Auftragslisten-KNR`,

													`" . TABLE_DELIVERY_DATAS . "`.`Paketnummer`,
													`" . TABLE_DELIVERY_DATAS . "`.`Datum`,
													/* `" . TABLE_DELIVERY_DATAS . "`.`Produkt_Service`, */

													`" . TABLE_DELIVERY_DATAS . "`.`Referenznr_1`,
													`" . TABLE_DELIVERY_DATAS . "`.`Versanddatum`,
													/* `" . TABLE_DELIVERY_DATAS . "`.`Zustelldatum`, */
													`" . TABLE_DELIVERY_DATAS . "`.`Firma_Empfaenger`,
													`" . TABLE_CUSTOMERS . "`.`customersFirmenname` AS `Auftragslisten-Firma`,
													
													`" . TABLE_DELIVERY_DATAS . "`.`Firma_Empfaenger` LIKE CONCAT('%', REPLACE(`" . TABLE_CUSTOMERS . "`.`customersFirmenname`, ' ', '%'), '%') AS `companyMatched_1`,
													`" . TABLE_CUSTOMERS . "`.`customersFirmenname` LIKE CONCAT('%', REPLACE(`" . TABLE_DELIVERY_DATAS . "`.`Firma_Empfaenger`, ' ', '%'), '%') AS `companyMatched_2`,
													
													STRCMP(`" . TABLE_DELIVERY_DATAS . "`.`Firma_Empfaenger`, `" . TABLE_CUSTOMERS . "`.`customersFirmenname`) AS `companyCompare_1`,
													STRCMP(`" . TABLE_CUSTOMERS . "`.`customersFirmenname`, `" . TABLE_DELIVERY_DATAS . "`.`Firma_Empfaenger`) AS `companyCompare_2`,
													
													SOUNDEX(`" . TABLE_DELIVERY_DATAS . "`.`Firma_Empfaenger`) AS `companySound_1`,
													SOUNDEX(`" . TABLE_CUSTOMERS . "`.`customersFirmenname`) AS `companySound_2`,

													`" . TABLE_DELIVERY_DATAS . "`.`Adresse_1`,
													CONCAT(`" . TABLE_CUSTOMERS . "`.`customersCompanyStrasse`, ' ', `" . TABLE_CUSTOMERS . "`.`customersCompanyHausnummer`) AS `Auftragslisten-Adresse`,

													`" . TABLE_DELIVERY_DATAS . "`.`PLZ_1`,
													`" . TABLE_CUSTOMERS . "`.`customersCompanyPLZ` AS `Auftragslisten-PLZ`,

													`" . TABLE_DELIVERY_DATAS . "`.`Stadt_1`,
													`" . TABLE_CUSTOMERS . "`.`customersCompanyOrt` AS `Auftragslisten-Ort`

												FROM `" . TABLE_DELIVERY_DATAS . "`

												INNER JOIN `" . TABLE_CUSTOMERS . "`
												ON(
													/*
													REPLACE(`" . TABLE_DELIVERY_DATAS . "`.`Firma_Empfaenger`, ' ', '') = REPLACE(`" . TABLE_CUSTOMERS . "`.`customersFirmenname`, ' ', '')
													AND
													*/
													`" . TABLE_DELIVERY_DATAS . "`.`PLZ_1` = `" . TABLE_CUSTOMERS . "`.`customersCompanyPLZ`
												)

												WHERE 1
													AND `" . TABLE_DELIVERY_DATAS . "`.`Ref_Adresse_1` = ''		
													
											) AS `tempTable`
											
											WHERE 1
												AND `tempTable`.`Adresse_1` = `tempTable`.`Auftragslisten-Adresse`
												AND (
													`tempTable`.`companyMatched_1` = 1
													OR `tempTable`.`companyMatched_2` = 1
												)
												AND (
													`tempTable`.`companySound_1` LIKE CONCAT('%', `tempTable`.`companySound_2`, '%')
													OR `tempTable`.`companySound_2` LIKE CONCAT('%', `tempTable`.`companySound_1`, '%')
												)
												
											GROUP BY `tempTable`.`Auftragslisten-KNR`

											ORDER BY
												`tempTable`.`Datum` DESC,
												`tempTable`.`Auftragslisten-Firma` ASC,
												`tempTable`.`Paketnummer` ASC
									";
									
								$sql_getCustomerNumbersForDeliverydatasWithoutCustomerNumber_4 = "
									
										SELECT
											`" . TABLE_CUSTOMERS . "`.`customersKundennummer` AS `Auftragslisten-KNR`,

											`" . TABLE_DELIVERY_DATAS . "`.`Paketnummer`,
											`" . TABLE_DELIVERY_DATAS . "`.`Datum`,
											/* `" . TABLE_DELIVERY_DATAS . "`.`Produkt_Service`, */

											`" . TABLE_DELIVERY_DATAS . "`.`Referenznr_1`,
											`" . TABLE_DELIVERY_DATAS . "`.`Versanddatum`,
											/* `" . TABLE_DELIVERY_DATAS . "`.`Zustelldatum`, */
											`" . TABLE_DELIVERY_DATAS . "`.`Firma_Empfaenger`,
											`" . TABLE_CUSTOMERS . "`.`customersFirmenname` AS `Auftragslisten-Firma`,

											`" . TABLE_DELIVERY_DATAS . "`.`Adresse_1`,
											CONCAT(`" . TABLE_CUSTOMERS . "`.`customersCompanyStrasse`, ' ', `" . TABLE_CUSTOMERS . "`.`customersCompanyHausnummer`) AS `Auftragslisten-Adresse`,

											`" . TABLE_DELIVERY_DATAS . "`.`PLZ_1`,
											`" . TABLE_CUSTOMERS . "`.`customersCompanyPLZ` AS `Auftragslisten-PLZ`,

											`" . TABLE_DELIVERY_DATAS . "`.`Stadt_1`,
											`" . TABLE_CUSTOMERS . "`.`customersCompanyOrt` AS `Auftragslisten-Ort`

										FROM `" . TABLE_DELIVERY_DATAS . "`

										INNER JOIN `" . TABLE_CUSTOMERS . "`
										ON(`" . TABLE_DELIVERY_DATAS . "`.`PLZ_1` = `" . TABLE_CUSTOMERS . "`.`customersCompanyPLZ`)

										WHERE 1
											AND `" . TABLE_DELIVERY_DATAS . "`.`Ref_Adresse_1` = ''											
											AND (
												CONCAT(`" . TABLE_CUSTOMERS . "`.`customersCompanyStrasse`, ' ', `" . TABLE_CUSTOMERS . "`.`customersCompanyHausnummer`) = `Adresse_1`
												OR
												CONCAT(`" . TABLE_CUSTOMERS . "`.`customersCompanyStrasse`, ' ', `" . TABLE_CUSTOMERS . "`.`customersCompanyHausnummer`) = REPLACE(`Adresse_1`, 'straße', 'str')
												OR
												CONCAT(`" . TABLE_CUSTOMERS . "`.`customersCompanyStrasse`, ' ', `" . TABLE_CUSTOMERS . "`.`customersCompanyHausnummer`) = REPLACE(`Adresse_1`, 'straße', 'str.')
												OR
												CONCAT(`" . TABLE_CUSTOMERS . "`.`customersCompanyStrasse`, ' ', `" . TABLE_CUSTOMERS . "`.`customersCompanyHausnummer`) = REPLACE(`Adresse_1`, 'strasse', 'str')
												OR
												CONCAT(`" . TABLE_CUSTOMERS . "`.`customersCompanyStrasse`, ' ', `" . TABLE_CUSTOMERS . "`.`customersCompanyHausnummer`) = REPLACE(`Adresse_1`, 'strasse', 'str.')
												OR
												CONCAT(`" . TABLE_CUSTOMERS . "`.`customersCompanyStrasse`, ' ', `" . TABLE_CUSTOMERS . "`.`customersCompanyHausnummer`) = REPLACE(`Adresse_1`, 'str', 'str.')
											)

										GROUP BY
											`" . TABLE_CUSTOMERS . "`.`customersKundennummer`

										ORDER BY
											`" . TABLE_DELIVERY_DATAS . "`.`Datum` DESC,
											`" . TABLE_CUSTOMERS . "`.`customersFirmenname` ASC,
											`" . TABLE_DELIVERY_DATAS . "`.`Paketnummer` ASC
									";
									
								$sql_getCustomerNumbersForDeliverydatasWithoutCustomerNumber = $sql_getCustomerNumbersForDeliverydatasWithoutCustomerNumber_4;

								$rs_getCustomerNumbersForDeliverydatasWithoutCustomerNumber = $dbConnection->db_query($sql_getCustomerNumbersForDeliverydatasWithoutCustomerNumber);

								if($dbConnection->db_getMysqlNumRows($rs_getCustomerNumbersForDeliverydatasWithoutCustomerNumber) > 0){

									echo '<table border="0" cellpadding="0" cellspacing="0" width="100%" class="displayOrders">';

									$countRow = 0;
									while($ds_getCustomerNumbersForDeliverydatasWithoutCustomerNumber = mysqli_fetch_assoc($rs_getCustomerNumbersForDeliverydatasWithoutCustomerNumber)){
										if($countRow == 0){
											echo '<tr>';
											echo '<th>#</th>';
											echo '<th>' . implode('</th><th>', array_keys($ds_getCustomerNumbersForDeliverydatasWithoutCustomerNumber)). '</th>';
											echo '</tr>';
										}

										if($countRow%2 == 0){ $rowClass = 'row0'; }
										else { $rowClass = 'row1'; }

										echo '<tr class="' . $rowClass . '">';
										echo '<td style="text-align:right;"><b>' . ($countRow + 1). '</b></td>';
										echo '<td style="white-space:nowrap;">' . implode('</td><td style="white-space:nowrap;">', array_values($ds_getCustomerNumbersForDeliverydatasWithoutCustomerNumber)). '</td>';
										echo '</tr>';
										$countRow++;
									}
									echo '</table>';
								}
								else {
									echo '<p class="infoArea">Keine Daten gefunden!</p>';
								}
								echo '<hr />';
						?>

						<tr>
							<td style="width:200px;"><b>SQL:</b></td>
							<td>
								<?php
									dd('sql_getCustomerNumbersForDeliverydatasWithoutCustomerNumber_1');
									echo '<hr />';
									dd('sql_getCustomerNumbersForDeliverydatasWithoutCustomerNumber_2');
									echo '<hr />';
									dd('sql_getCustomerNumbersForDeliverydatasWithoutCustomerNumber_3');
									echo '<hr />';
									dd('sql_getCustomerNumbersForDeliverydatasWithoutCustomerNumber_4');
								?>
							</td>
						</tr>

						<?php
							}
						?>
					</table>
				</fieldset>
				<div class="actionButtonsArea">
				<?php if($_POST["goFormReviseDatas"] != "" && $_POST["selectAction"] != "ACTION_RELATED_DOCUMENTS") { ?>
					<input type="submit" name="submitFormReviseDatas" class="inputButton1 inputButtonGreen" value="Senden" onclick="return showWarning('Wollen Sie diese Aktion wirklich ausführen?');"/>
					<a href="<?php echo PAGE_REVISE_DOCUMENT_DATAS; ?>" class="linkButton inputButtonOrange" style="width:160px;" >Zur&uuml;ck</a>
				<?php } else { ?>
					<input type="submit" name="cancelFormReviseDatas" class="inputButton1 inputButtonRed" value="Abbrechen" />
				<?php } ?>
				</div>
				<?php
					}
					// EOF FORM HANDLE DATAS

					// BOF EXECUTE
					if($_POST["submitFormReviseDatas"] != "") {
						if($_POST["selectAction"] != "" && $_POST["handleThisDocumentNumber"] != "" && $_POST["handleThisDocumentCustomerNumber"] != "") {
							// $filenameExport = $_POST["selectExportTable"] . '_' . date("Y-m-d_H-i-s") . '.csv';

							if($_POST["selectAction"] == "ACTION_DELETE_DOCUMENT") {
								$thisDocumentType = strtoupper(substr($_POST["handleThisDocumentNumber"], 0, 2));

								echo '<hr />';

								displayDebug('thisDocumentType');
								$thisHandleTable = "TABLE_ORDER_" . $thisDocumentType;

								echo '<p class="infoArea">';
								echo 'thisHandleTable : ' . $thisHandleTable . ' &gt; ' . constant($thisHandleTable);
								echo '</p>';

								if($thisHandleTable != ''){
									$arrDataOutput = array();

									$arrDataOutput['sql'][] = "
											DELETE
												`" . constant($thisHandleTable) . "`,
												`" . constant($thisHandleTable . "_DETAILS") . "`,
												`" . TABLE_CREATED_DOCUMENTS . "`


												FROM `" . constant($thisHandleTable) . "`
												LEFT JOIN `" . constant($thisHandleTable . "_DETAILS") . "`
												ON(`" . constant($thisHandleTable) . "`.`orderDocumentsID` = `" . constant($thisHandleTable . "_DETAILS") . "`.`orderDocumentDetailDocumentID`)
												LEFT JOIN `" . TABLE_CREATED_DOCUMENTS . "`
												ON(`" . constant($thisHandleTable) . "`.`orderDocumentsNumber` = `" . TABLE_CREATED_DOCUMENTS . "`.`createdDocumentsNumber`)

												WHERE 1
													AND `" . constant($thisHandleTable) . "`.`orderDocumentsNumber` = '" . $_POST["handleThisDocumentNumber"] . "'
													AND (
														`" . constant($thisHandleTable) . "`.`orderDocumentsCustomerNumber` = '" . $_POST["handleThisDocumentCustomerNumber"] . "'
														OR
														`" . constant($thisHandleTable) . "`.`orderDocumentsAddressCompanyCustomerNumber` = '" . $_POST["handleThisDocumentCustomerNumber"] . "'
													);
										";

									$arrDataOutput['sql'][] = "
											DELETE
												FROM `" . TABLE_DOCUMENTS_TO_DOCUMENTS . "`
												WHERE `documentsToDocumentsCreatedDocumentNumber` = '" . $_POST["handleThisDocumentNumber"] . "';
										";

									$arrDataOutput['sql'][] = "
											UPDATE
												`" . TABLE_RELATED_DOCUMENTS . "`
												SET `relatedDocuments_" . $thisDocumentType . "` = ''
												WHERE `relatedDocuments_" . $thisDocumentType . "` = '" . $_POST["handleThisDocumentNumber"] . "';
										";


									$arrDataOutput['sql'][] = "
											UPDATE `" . TABLE_RELATED_DOCUMENTS . "`
												SET `relatedDocuments_" . $thisDocumentType . "` = REPLACE(`relatedDocuments_" . $thisDocumentType . "`, ';" . $_POST["handleThisDocumentNumber"] . "', '')
												WHERE `relatedDocuments_" . $thisDocumentType . "` LIKE '%;" . $_POST["handleThisDocumentNumber"] . "%';
										";

									/*
									$arrDataOutput['sql'][] = "
											SELECT
												*
												FROM `" . TABLE_RELATED_DOCUMENTS . "`

												WHERE

													`relatedDocuments_AN` = '' OR
													`relatedDocuments_AB` = '' OR
													`relatedDocuments_RE` = '' OR
													`relatedDocuments_LS` = '' OR
													`relatedDocuments_GU` = '' OR
													`relatedDocuments_MA` = '' OR
													`relatedDocuments_M1` = '' OR
													`relatedDocuments_M2` = '' OR
													`relatedDocuments_M3` = '' OR
													`relatedDocuments_IK` = '' OR
													`relatedDocuments_collectiveABs` = ''
										";
									*/

									$arrDataOutput['sql'][] = "UPDATE `" . TABLE_RELATED_DOCUMENTS . "` SET `relatedDocuments_AN` = NULL WHERE `relatedDocuments_AN` = '';";
									$arrDataOutput['sql'][] = "UPDATE `" . TABLE_RELATED_DOCUMENTS . "` SET `relatedDocuments_AB` = NULL WHERE `relatedDocuments_AB` = '';";
									$arrDataOutput['sql'][] = "UPDATE `" . TABLE_RELATED_DOCUMENTS . "` SET `relatedDocuments_RE` = NULL WHERE `relatedDocuments_RE` = '';";
									$arrDataOutput['sql'][] = "UPDATE `" . TABLE_RELATED_DOCUMENTS . "` SET `relatedDocuments_LS` = NULL WHERE `relatedDocuments_LS` = '';";
									$arrDataOutput['sql'][] = "UPDATE `" . TABLE_RELATED_DOCUMENTS . "` SET `relatedDocuments_GU` = NULL WHERE `relatedDocuments_GU` = '';";
									$arrDataOutput['sql'][] = "UPDATE `" . TABLE_RELATED_DOCUMENTS . "` SET `relatedDocuments_MA` = NULL WHERE `relatedDocuments_MA` = '';";
									$arrDataOutput['sql'][] = "UPDATE `" . TABLE_RELATED_DOCUMENTS . "` SET `relatedDocuments_M1` = NULL WHERE `relatedDocuments_M1` = '';";
									$arrDataOutput['sql'][] = "UPDATE `" . TABLE_RELATED_DOCUMENTS . "` SET `relatedDocuments_M2` = NULL WHERE `relatedDocuments_M2` = '';";
									$arrDataOutput['sql'][] = "UPDATE `" . TABLE_RELATED_DOCUMENTS . "` SET `relatedDocuments_M3` = NULL WHERE `relatedDocuments_M3` = '';";
									$arrDataOutput['sql'][] = "UPDATE `" . TABLE_RELATED_DOCUMENTS . "` SET `relatedDocuments_IK` = NULL WHERE `relatedDocuments_IK` = '';";
									$arrDataOutput['sql'][] = "UPDATE `" . TABLE_RELATED_DOCUMENTS . "` SET `relatedDocuments_collectiveABs` = NULL WHERE `relatedDocuments_collectiveABs` = '';";


									$arrDataOutput['sql'][] = "
										DELETE FROM `" . TABLE_RELATED_DOCUMENTS . "`
											WHERE 1
												AND (`relatedDocuments_AN` IS NULL OR `relatedDocuments_AN` = '')
												AND (`relatedDocuments_AB` IS NULL OR `relatedDocuments_AB` = '')
												AND (`relatedDocuments_RE` IS NULL OR `relatedDocuments_RE` = '')
												AND (`relatedDocuments_LS` IS NULL OR `relatedDocuments_LS` = '')
												AND (`relatedDocuments_GU` IS NULL OR `relatedDocuments_GU` = '')
												AND (`relatedDocuments_MA` IS NULL OR `relatedDocuments_MA` = '')
												AND (`relatedDocuments_M1` IS NULL OR `relatedDocuments_M1` = '')
												AND (`relatedDocuments_M2` IS NULL OR `relatedDocuments_M2` = '')
												AND (`relatedDocuments_M3` IS NULL OR `relatedDocuments_M3` = '');
										";


									#$arrDataOutput['sql'][] = "SELECT * FROM `" . TABLE_DOCUMENTS_TO_DOCUMENTS . "` WHERE `documentsToDocumentsOriginDocumentNumber` = '" . $_POST["handleThisDocumentNumber"] . "' AND `documentsToDocumentsRelationType` = 'convert'";

									echo '<hr />';

									echo createSqlExecButtons();

									echo '<div class="debugVarsArea" style="border:2px solid #FF0000; background-color:#FFFFFF;">';
									echo '<p>';
									echo "<b>dataOutput - sql:</b><br />";
									$arrTemp = $arrDataOutput['sql'];

									$countItem = 0;
									if(!empty($arrTemp)){
										foreach($arrTemp as $thisDataSQL){
											echo '<div class="sqlArea" style="font-size:11px; border-bottom:1px solid #333;">';
												echo '<input type="checkbox" class="executeSqlOrders" name="executeSql_' . $countItem . '" value="1" /> ';
													echo '<span class="sqlOrder">';
														echo trim($thisDataSQL) . "<br />";
													echo '</span>';
											echo '</div>';
											$countItem++;
										}
									}
									echo '</p>';
									echo '</div>';

									echo createSqlExecButtons();
									#######
								}
							}
							else if($_POST["selectAction"] == "ACTION_CHANGE_DOCUMENT_CUSTOMER_NUMBER" && $_POST["newDocumentCustomerNumber"] != "") {
								$arrHandleDocumentNumbers = getRelatedDocuments(array($_POST["handleThisDocumentNumber"]), '');
#dd('arrHandleDocumentNumbers');
								echo '<hr />';

								displayDebug('arrHandleDocumentNumbers');

								$arrDataOutput['sql'] = array();
								if(!empty($arrHandleDocumentNumbers)){
									foreach($arrHandleDocumentNumbers as $thisKey => $thisValue){
										if($thisValue != '' && strlen($thisKey) == 2){
											$thisHandleTableDocuments = 'TABLE_ORDER_' . $thisKey;
											$thisHandleTableDocumentsDetails = 'TABLE_ORDER_' . $thisKey . '_DETAILS';											
											
											$thisSql = "sql_" . $count;
											#$$thisSql = "UPDATE `" . constant($thisHandleTableDocuments) . "` SET `orderDocumentsCustomerNumber` = '" . $_POST["newDocumentCustomerNumber"] . "' WHERE `orderDocumentsNumber` = '" . $thisValue . "' AND `orderDocumentsCustomerNumber` = '" . $_POST["handleThisDocumentCustomerNumber"] . "';";
											#$$thisSql = "UPDATE `" . TABLE_CREATED_DOCUMENTS . "` SET `createdDocumentsCustomerNumber` = '" . $_POST["newDocumentCustomerNumber"] . "' WHERE `createdDocumentsNumber` = '" . $thisValue . "' AND `createdDocumentsCustomerNumber` = '" . $_POST["handleThisDocumentCustomerNumber"] . "';";

											$arrDataOutput['sql'][] = "
														UPDATE
															`" . constant($thisHandleTableDocuments) . "`
														SET `orderDocumentsCustomerNumber` = '" . $_POST["newDocumentCustomerNumber"] . "'
														WHERE `orderDocumentsNumber` = '" . $thisValue . "'
															AND `orderDocumentsCustomerNumber` = '" . $_POST["handleThisDocumentCustomerNumber"] . "';
												";

											$arrDataOutput['sql'][] = "
														UPDATE
															`" . TABLE_CREATED_DOCUMENTS . "`
														SET `createdDocumentsCustomerNumber` = '" . $_POST["newDocumentCustomerNumber"] . "'
														WHERE `createdDocumentsNumber` = '" . $thisValue . "'
															AND `createdDocumentsCustomerNumber` = '" . $_POST["handleThisDocumentCustomerNumber"] . "';
												";											
												
												
											$arrDataOutput['sql'][] = "
														UPDATE														
															`" . TABLE_ORDERS . "`
														
														LEFT JOIN `" . constant($thisHandleTableDocumentsDetails). "`
														ON(`" . TABLE_ORDERS . "`.`ordersID` = `" . constant($thisHandleTableDocumentsDetails). "`.`orderDocumentDetailOrderID`)	
														
														LEFT JOIN `" . constant($thisHandleTableDocuments) . "`
														ON(`" . constant($thisHandleTableDocumentsDetails). "`.`orderDocumentDetailDocumentID` = `" . constant($thisHandleTableDocuments) . "`.`orderDocumentsID`)
														
														LEFT JOIN `" . TABLE_CUSTOMERS . "`
														ON(`" . TABLE_CUSTOMERS . "`.`customersKundennummer` = '" . $_POST["newDocumentCustomerNumber"] . "')
														
														SET 
															`" . TABLE_ORDERS . "`.`ordersKundennummer` = '" . $_POST["newDocumentCustomerNumber"] . "',
															`" . TABLE_ORDERS . "`.`ordersKundenName` = `" . TABLE_CUSTOMERS . "`.`customersFirmenname`
														
														WHERE `" . constant($thisHandleTableDocuments) . "`.`orderDocumentsNumber` = '" . $thisValue . "' 	
															AND `" . TABLE_ORDERS . "`.`ordersKundennummer` = '" . $_POST["handleThisDocumentCustomerNumber"] . "'
													";

										}
									}
								}
								#dd('arrDataOutput');
								echo '<hr />';

								echo createSqlExecButtons();

								echo '<div class="debugVarsArea" style="border:2px solid #FF0000; background-color:#FFFFFF;">';
								echo '<p>';
								echo "<b>dataOutput - sql:</b><br />";
								$arrTemp = $arrDataOutput['sql'];

								$countItem = 0;
								if(!empty($arrTemp)){
									foreach($arrTemp as $thisDataSQL){
										echo '<div class="sqlArea" style="font-size:11px; border-bottom:1px solid #333;">';
											echo '<input type="checkbox" class="executeSqlOrders" name="executeSql_' . $countItem . '" value="1" /> ';
												echo '<span class="sqlOrder">';
													echo trim($thisDataSQL) . "<br />";
												echo '</span>';
										echo '</div>';
										$countItem++;
									}
								}
								echo '</p>';
								echo '</div>';

								echo createSqlExecButtons();
							}
							else {
								$errorMessage .= 'Die einzugebenden Daten sind nicht komplett.' . '<br />';
							}
						}
						else if($_POST["selectAction"] == "ACTION_FIND_DOCUMENTS_WITHOUT_PAYMENT_STATUS"){
							echo '<hr />';
							if($handleThisFindDocumentsWithoutPaymentStatus != ""){
								$thisTableOrderConfirmations	= preg_replace("/" . MANDATOR . "/", $handleThisFindDocumentsWithoutPaymentStatus, TABLE_ORDER_CONFIRMATIONS);
								$thisTableOrderDeliveries		= preg_replace("/" . MANDATOR . "/", $handleThisFindDocumentsWithoutPaymentStatus, TABLE_ORDER_DELIVERIES);
								$thisTableOrderInvoices			= preg_replace("/" . MANDATOR . "/", $handleThisFindDocumentsWithoutPaymentStatus, TABLE_ORDER_INVOICES);
								$thisTableOrderCredits			= preg_replace("/" . MANDATOR . "/", $handleThisFindDocumentsWithoutPaymentStatus, TABLE_ORDER_CREDITS);
								$thisTableOrderReminders		= preg_replace("/" . MANDATOR . "/", $handleThisFindDocumentsWithoutPaymentStatus, TABLE_ORDER_REMINDERS);
								$thisTableOrderFirstDemands		= preg_replace("/" . MANDATOR . "/", $handleThisFindDocumentsWithoutPaymentStatus, TABLE_ORDER_FIRST_DEMANDS);
								$thisTableOrderSecondDemands	= preg_replace("/" . MANDATOR . "/", $handleThisFindDocumentsWithoutPaymentStatus, TABLE_ORDER_SECOND_DEMANDS);

								$sql_findDocumentsWithoutPaymentsStatus = "
										SELECT * FROM `" . $thisTableOrderConfirmations . "` WHERE `orderDocumentsStatus` = '0'
											UNION
										SELECT * FROM `" . $thisTableOrderInvoices . "` WHERE `orderDocumentsStatus` = '0'
											UNION
										SELECT * FROM `" . $thisTableOrderCredits . "` WHERE `orderDocumentsStatus` = '0'
											UNION
										SELECT * FROM `" . $thisTableOrderDeliveries . "` WHERE `orderDocumentsStatus` = '0'
											UNION
										SELECT * FROM `" . $thisTableOrderReminders . "` WHERE `orderDocumentsStatus` = '0'
											UNION
										SELECT * FROM `" . $thisTableOrderFirstDemands . "` WHERE `orderDocumentsStatus` = '0'
											UNION
										SELECT * FROM `" . $thisTableOrderSecondDemands . "` WHERE `orderDocumentsStatus` = '0'
											ORDER BY `orderDocumentsNumber` DESC
									";
								$rs_findDocumentsWithoutPaymentsStatus = $dbConnection->db_query($sql_findDocumentsWithoutPaymentsStatus);

								if($dbConnection->db_getMysqlNumRows($rs_findDocumentsWithoutPaymentsStatus) > 0){
									$countRow = 0;

									echo '<table border="0" cellpadding="0" cellspacing="0" style="width:99%;" class="displayOrders">';

									while($db_findDocumentsWithoutPaymentsStatus = mysqli_fetch_assoc($rs_findDocumentsWithoutPaymentsStatus)){
										if($countRow == 0){
											echo '<tr>';
											echo '<th style="width:45px;">#</th>';
											echo '<th>' . implode("</th><th>", array_keys($db_findDocumentsWithoutPaymentsStatus)) . '</th>';
											echo '</tr>';
										}

										if($countRow%2 == 0){ $rowClass = 'row1'; }
										else { $rowClass = 'row4'; }

										if($arrCountStatus[$thisAverageStatus] == count($arrThisStatus)){
											$rowClass = 'row3';
										}
										else {
											$rowClass = 'row10';
										}

										echo '<tr class="' . $rowClass . '">';

										echo '<td style="text-align:right;"><b>';
										echo ($countRow + 1);
										echo '.</b></td>';

										foreach($db_findDocumentsWithoutPaymentsStatus as $thisDocumentsWithoutPaymentsStatusKey => $thisDocumentsWithoutPaymentsStatusValue){
											if($thisDocumentsWithoutPaymentsStatusKey == "count"){
												echo '<td style="text-align:center;">';
											}
											else {
												echo '<td>';
											}

											echo $thisDocumentsWithoutPaymentsStatusValue;
											echo '</td>';
										}

										echo '</tr>';

										$countRow++;
									}

									echo '</table>';
								}
								else {
									$infoMessage = 'Keine Dokumente ohne Zahlstatus gefunden.' . '<br />';
								}
							}
						}
						else if($_POST["selectAction"] == "ACTION_CHECK_RELATED_DOCUMENTS_PAYMENT_STATUS"){
							echo '<hr />';
							####################################
							if($handleThisCheckRelatedDocumentPaymentStatus != ""){
								$thisTableOrderConfirmations	= preg_replace("/" . MANDATOR . "/", $handleThisCheckRelatedDocumentPaymentStatus, TABLE_ORDER_CONFIRMATIONS);
								$thisTableOrderCredits			= preg_replace("/" . MANDATOR . "/", $handleThisCheckRelatedDocumentPaymentStatus, TABLE_ORDER_CREDITS);
								$thisTableOrderFirstDemands		= preg_replace("/" . MANDATOR . "/", $handleThisCheckRelatedDocumentPaymentStatus, TABLE_ORDER_FIRST_DEMANDS);
								$thisTableOrderInvoices			= preg_replace("/" . MANDATOR . "/", $handleThisCheckRelatedDocumentPaymentStatus, TABLE_ORDER_INVOICES);
								$thisTableOrderReminders		= preg_replace("/" . MANDATOR . "/", $handleThisCheckRelatedDocumentPaymentStatus, TABLE_ORDER_REMINDERS);
								$thisTableOrderSecondDemands	= preg_replace("/" . MANDATOR . "/", $handleThisCheckRelatedDocumentPaymentStatus, TABLE_ORDER_SECOND_DEMANDS);
								$thisTablePaymentStatusTypes	= preg_replace("/" . MANDATOR . "/", $handleThisCheckRelatedDocumentPaymentStatus, TABLE_PAYMENT_STATUS_TYPES);
								$thisTableRelatedDocuments		= preg_replace("/" . MANDATOR . "/", $handleThisCheckRelatedDocumentPaymentStatus, TABLE_RELATED_DOCUMENTS);

								// BOF OLD GET ALL COMBINATIONS
									$sql_OLD_getRelatedDocumentPaymentStatusAllCombinations = "
										SELECT
											COUNT(`SID_AB`) AS `COUNT`,
											CONCAT(`SID_AB`, '_', `SID_RE`, '_', `SID_LS`, '_', `SID_GU`, '_', `SID_MA`, '_', `SID_M1`, '_', `SID_M2`, '_', `SID_M3`) AS `STATUS_IDS`,
											CONCAT('AB-', `status_AB`, '_', 'RE-', `status_RE`, '_', 'LS-', `status_LS`, '_', 'GU-', `status_GU`, '_', 'MA-', `status_MA`, '_', 'M1-', `status_M1`, '_', 'M2-', `status_M2`, '_', 'M3-', `status_M3`) AS `STATUS_NAMES`

										FROM (

											SELECT

											/* ORDER_CONFIRMATIONS */
												IF(`relatedDocuments_AB` IS NULL, '', `relatedDocuments_AB`) AS `doc_AB`,
												IF(`bctr_orderConfirmations`.`orderDocumentsStatus` IS NULL, '0', `bctr_orderConfirmations`.`orderDocumentsStatus`) AS `SID_AB`,
												IF(`common_paymentStatusTypes_AB`.`paymentStatusTypesName` IS NULL, '', `common_paymentStatusTypes_AB`.`paymentStatusTypesName`) AS `status_AB`,

											/* ORDER_INVOICES */
												IF(`relatedDocuments_RE` IS NULL, '', `relatedDocuments_RE`) AS `doc_RE`,
												IF(`bctr_orderInvoices`.`orderDocumentsStatus` IS NULL, '0', `bctr_orderInvoices`.`orderDocumentsStatus`) AS `SID_RE`,
												IF(`common_paymentStatusTypes_RE`.`paymentStatusTypesName` IS NULL, '', `common_paymentStatusTypes_RE`.`paymentStatusTypesName`) AS `status_RE`,

											/* ORDER_DELIVERIES */
												IF(`relatedDocuments_LS` IS NULL, '', `relatedDocuments_LS`) AS `doc_LS`,
												IF(`bctr_orderDeliveries`.`orderDocumentsStatus` IS NULL, '0', `bctr_orderDeliveries`.`orderDocumentsStatus`) AS `SID_LS`,
												IF(`common_paymentStatusTypes_LS`.`paymentStatusTypesName` IS NULL, '', `common_paymentStatusTypes_LS`.`paymentStatusTypesName`) AS `status_LS`,

											/* ORDER_CREDITS */
												IF(`relatedDocuments_GU` IS NULL, '', `relatedDocuments_GU`) AS `doc_GU`,
												IF(`bctr_orderCredits`.`orderDocumentsStatus` IS NULL, '0', `bctr_orderCredits`.`orderDocumentsStatus`) AS `SID_GU`,
												IF(`common_paymentStatusTypes_GU`.`paymentStatusTypesName` IS NULL, '', `common_paymentStatusTypes_GU`.`paymentStatusTypesName`) AS `status_GU`,

											/* ORDER_REMINDERS */
												IF(`relatedDocuments_MA` IS NULL, '', `relatedDocuments_MA`) AS `doc_MA`,
												IF(`bctr_orderReminders`.`orderDocumentsStatus` IS NULL, '0', `bctr_orderReminders`.`orderDocumentsStatus`) AS `SID_MA`,
												IF(`common_paymentStatusTypes_MA`.`paymentStatusTypesName` IS NULL, '', `common_paymentStatusTypes_MA`.`paymentStatusTypesName`) AS `status_MA`,

											/* ORDER_FIRST DEMANDS */
												IF(`relatedDocuments_M1` IS NULL, '', `relatedDocuments_M1`) AS `doc_M1`,
												IF(`bctr_orderFirstDemands`.`orderDocumentsStatus` IS NULL, '0', `bctr_orderFirstDemands`.`orderDocumentsStatus`) AS `SID_M1`,
												IF(`common_paymentStatusTypes_M1`.`paymentStatusTypesName` IS NULL, '', `common_paymentStatusTypes_M1`.`paymentStatusTypesName`) AS `status_M1`,

											/* ORDER_SECOND DEMANDS */
												IF(`relatedDocuments_M2` IS NULL, '', `relatedDocuments_M2`) AS `doc_M2`,
												IF(`bctr_orderSecondDemands`.`orderDocumentsStatus` IS NULL, '0', `bctr_orderSecondDemands`.`orderDocumentsStatus`) AS `SID_M2`,
												IF(`common_paymentStatusTypes_M2`.`paymentStatusTypesName` IS NULL, '', `common_paymentStatusTypes_M2`.`paymentStatusTypesName`) AS `status_M2`,

											/* ORDER_THIRD DEMANDS */
												IF(`relatedDocuments_M3` IS NULL, '', `relatedDocuments_M3`) AS `doc_M3`,
												IF(`bctr_orderThirdDemands`.`orderDocumentsStatus` IS NULL, '0', `bctr_orderThirdDemands`.`orderDocumentsStatus`) AS `SID_M3`,
												IF(`common_paymentStatusTypes_M3`.`paymentStatusTypesName` IS NULL, '', `common_paymentStatusTypes_M3`.`paymentStatusTypesName`) AS `status_M3`

											/* UPDATE SQL */


											FROM `bctr_relateddocuments`

											/* ORDER_CONFIRMATIONS */
												LEFT JOIN `bctr_orderConfirmations`
												ON(`bctr_relatedDocuments`.`relatedDocuments_AB` = `bctr_orderConfirmations`.`orderDocumentsNumber`)

												LEFT JOIN `common_paymentStatusTypes` AS `common_paymentStatusTypes_AB`
												ON(`bctr_orderConfirmations`.`orderDocumentsStatus` = `common_paymentStatusTypes_AB`.`paymentStatusTypesID`)

											/* ORDER_INVOICES */
												LEFT JOIN `bctr_orderInvoices`
												ON(`bctr_relatedDocuments`.`relatedDocuments_RE` = `bctr_orderInvoices`.`orderDocumentsNumber`)

												LEFT JOIN `common_paymentStatusTypes` AS `common_paymentStatusTypes_RE`
												ON(`bctr_orderInvoices`.`orderDocumentsStatus` = `common_paymentStatusTypes_RE`.`paymentStatusTypesID`)

											/* ORDER_DELIVERIES */
												LEFT JOIN `bctr_orderDeliveries`
												ON(`bctr_relatedDocuments`.`relatedDocuments_LS` = `bctr_orderDeliveries`.`orderDocumentsNumber`)

												LEFT JOIN `common_paymentStatusTypes` AS `common_paymentStatusTypes_LS`
												ON(`bctr_orderDeliveries`.`orderDocumentsStatus` = `common_paymentStatusTypes_LS`.`paymentStatusTypesID`)

											/* ORDER_CREDITS */
												LEFT JOIN `bctr_orderCredits`
												ON(`bctr_relatedDocuments`.`relatedDocuments_GU` = `bctr_orderCredits`.`orderDocumentsNumber`)

												LEFT JOIN `common_paymentStatusTypes` AS `common_paymentStatusTypes_GU`
												ON(`bctr_orderCredits`.`orderDocumentsStatus` = `common_paymentStatusTypes_GU`.`paymentStatusTypesID`)

											/* ORDER_REMINDERS */
												LEFT JOIN `bctr_orderReminders`
												ON(`bctr_relatedDocuments`.`relatedDocuments_MA` = `bctr_orderReminders`.`orderDocumentsNumber`)

												LEFT JOIN `common_paymentStatusTypes` AS `common_paymentStatusTypes_MA`
												ON(`bctr_orderReminders`.`orderDocumentsStatus` = `common_paymentStatusTypes_MA`.`paymentStatusTypesID`)

											/* ORDER_FIRST DEMANDS */
												LEFT JOIN `bctr_orderFirstDemands`
												ON(`bctr_relatedDocuments`.`relatedDocuments_M1` = `bctr_orderFirstDemands`.`orderDocumentsNumber`)

												LEFT JOIN `common_paymentStatusTypes` AS `common_paymentStatusTypes_M1`
												ON(`bctr_orderFirstDemands`.`orderDocumentsStatus` = `common_paymentStatusTypes_M1`.`paymentStatusTypesID`)

											/* ORDER_SECOND DEMANDS */
												LEFT JOIN `bctr_orderSecondDemands`
												ON(`bctr_relatedDocuments`.`relatedDocuments_M2` = `bctr_orderSecondDemands`.`orderDocumentsNumber`)

												LEFT JOIN `common_paymentStatusTypes` AS `common_paymentStatusTypes_M2`
												ON(`bctr_orderSecondDemands`.`orderDocumentsStatus` = `common_paymentStatusTypes_M2`.`paymentStatusTypesID`)

											/* ORDER_THIRD DEMANDS */
												LEFT JOIN `bctr_orderThirdDemands`
												ON(`bctr_relatedDocuments`.`relatedDocuments_M3` = `bctr_orderThirdDemands`.`orderDocumentsNumber`)

												LEFT JOIN `common_paymentStatusTypes` AS `common_paymentStatusTypes_M3`
												ON(`bctr_orderThirdDemands`.`orderDocumentsStatus` = `common_paymentStatusTypes_M3`.`paymentStatusTypesID`)

											WHERE 1

										) AS `tempTable`

										GROUP BY CONCAT(`SID_AB`, '_', `SID_RE`, '_', `SID_LS`, '_', `SID_GU`, '_', `SID_MA`, '_', `SID_M1`, '_', `SID_M2`, '_', `SID_M3`)

									";
								// EOF OLD GET ALL COMBINATIONS


								#$thisCheckStatusCombination = "2-2-1-x-x-x";
								if($_POST["thisCheckStatusCombination"] != ""){
									$thisCheckStatusCombination = $_POST["thisCheckStatusCombination"];
								}

								$sql_getRelatedDocumentPaymentStatusOrder = " ORDER BY `concats` ";

								$sql_getRelatedDocumentPaymentStatusSpecialCombination = "

									SELECT

										CONCAT(
											IF(`" . $thisTableOrderConfirmations . "`.`orderDocumentsAddressCompanyCustomerNumber` IS NULL, '', `" . $thisTableOrderConfirmations . "`.`orderDocumentsAddressCompanyCustomerNumber`),
											'-',
											IF(`" . $thisTableOrderInvoices . "`.`orderDocumentsAddressCompanyCustomerNumber` IS NULL, '', `" . $thisTableOrderInvoices . "`.`orderDocumentsAddressCompanyCustomerNumber`),
											'-',
											IF(`" . $thisTableOrderCredits . "`.`orderDocumentsAddressCompanyCustomerNumber` IS NULL, '', `" . $thisTableOrderCredits . "`.`orderDocumentsAddressCompanyCustomerNumber`),
											'-',
											IF(`" . $thisTableOrderReminders . "`.`orderDocumentsAddressCompanyCustomerNumber` IS NULL, '', `" . $thisTableOrderReminders . "`.`orderDocumentsAddressCompanyCustomerNumber`),
											'-',
											IF(`" . $thisTableOrderFirstDemands . "`.`orderDocumentsAddressCompanyCustomerNumber` IS NULL, '', `" . $thisTableOrderFirstDemands . "`.`orderDocumentsAddressCompanyCustomerNumber`),
											'-',
											IF(`" . $thisTableOrderSecondDemands . "`.`orderDocumentsAddressCompanyCustomerNumber` IS NULL, '', `" . $thisTableOrderSecondDemands . "`.`orderDocumentsAddressCompanyCustomerNumber`)
										)  AS `KNR`,

										`" . $thisTableRelatedDocuments . "`.`relatedDocuments_AB` AS `AB-NR`,
										`" . $thisTableOrderConfirmations . "`.`orderDocumentsStatus` AS `AB-St`,
										`" . $thisTableOrderConfirmations . "`.`orderDocumentsDocumentPath` AS `AB-PATH`,

										`" . $thisTableRelatedDocuments . "`.`relatedDocuments_RE` AS `RE-NR`,
										`" . $thisTableOrderInvoices . "`.`orderDocumentsStatus` AS `RE-St`,
										`" . $thisTableOrderInvoices . "`.`orderDocumentsDocumentPath` AS `RE-PATH`,

										`" . $thisTableRelatedDocuments . "`.`relatedDocuments_GU` AS `GU-NR`,
										`" . $thisTableOrderCredits . "`.`orderDocumentsStatus` AS `GU-St`,
										`" . $thisTableOrderCredits . "`.`orderDocumentsDocumentPath` AS `GU-PATH`,

										`" . $thisTableRelatedDocuments . "`.`relatedDocuments_MA` AS `MA-NR`,
										`" . $thisTableOrderReminders . "`.`orderDocumentsStatus` AS `MA-St`,
										`" . $thisTableOrderReminders . "`.`orderDocumentsDocumentPath` AS `MA-PATH`,

										`" . $thisTableRelatedDocuments . "`.`relatedDocuments_M1` AS `M1-NR`,
										`" . $thisTableOrderFirstDemands . "`.`orderDocumentsStatus` AS `M1-St`,
										`" . $thisTableOrderFirstDemands . "`.`orderDocumentsDocumentPath` AS `M1-PATH`,

										`" . $thisTableRelatedDocuments . "`.`relatedDocuments_M2` AS `M2-NR`,
										`" . $thisTableOrderSecondDemands . "`.`orderDocumentsStatus` AS `M2-St`,
										`" . $thisTableOrderSecondDemands . "`.`orderDocumentsDocumentPath` AS `M2-PATH`,

										CONCAT(
											'AB: ', IF(`status_AB`.`paymentStatusTypesShortName` IS NULL, 'x', `status_AB`.`paymentStatusTypesShortName`),
											' | ',
											'RE: ', IF(`status_RE`.`paymentStatusTypesShortName` IS NULL, 'x', `status_RE`.`paymentStatusTypesShortName`),
											' | ',
											'GU: ', IF(`status_GU`.`paymentStatusTypesShortName` IS NULL, 'x', `status_GU`.`paymentStatusTypesShortName`),
											' | ',
											'MA: ', IF(`status_MA`.`paymentStatusTypesShortName` IS NULL, 'x', `status_MA`.`paymentStatusTypesShortName`),
											' | ',
											'M1: ', IF(`status_M1`.`paymentStatusTypesShortName` IS NULL, 'x', `status_M1`.`paymentStatusTypesShortName`),
											' | ',
											'M2: ', IF(`status_M2`.`paymentStatusTypesShortName` IS NULL, 'x', `status_M2`.`paymentStatusTypesShortName`)

										) AS `status`,

										CONCAT(
											IF(`" . $thisTableOrderConfirmations . "`.`orderDocumentsStatus` IS NULL, 'x', `" . $thisTableOrderConfirmations . "`.`orderDocumentsStatus`),
											'-',
											IF(`" . $thisTableOrderInvoices . "`.`orderDocumentsStatus` IS NULL, 'x', `" . $thisTableOrderInvoices . "`.`orderDocumentsStatus`),
											'-',
											IF(`" . $thisTableOrderCredits . "`.`orderDocumentsStatus` IS NULL, 'x', `" . $thisTableOrderCredits . "`.`orderDocumentsStatus`),
											'-',
											IF(`" . $thisTableOrderReminders . "`.`orderDocumentsStatus` IS NULL, 'x', `" . $thisTableOrderReminders . "`.`orderDocumentsStatus`),
											'-',
											IF(`" . $thisTableOrderFirstDemands . "`.`orderDocumentsStatus` IS NULL, 'x', `" . $thisTableOrderFirstDemands . "`.`orderDocumentsStatus`),
											'-',
											IF(`" . $thisTableOrderSecondDemands . "`.`orderDocumentsStatus` IS NULL, 'x', `" . $thisTableOrderSecondDemands . "`.`orderDocumentsStatus`)
										) AS `concats`


										FROM `" . $thisTableRelatedDocuments . "`

										LEFT JOIN `" . $thisTableOrderConfirmations . "`
										ON(`" . $thisTableRelatedDocuments . "`.`relatedDocuments_AB` = `" . $thisTableOrderConfirmations . "`.`orderDocumentsNumber`)

										LEFT JOIN `" . $thisTablePaymentStatusTypes . "` AS `status_AB`
										ON(`" . $thisTableOrderConfirmations . "`.`orderDocumentsStatus` = `status_AB`.`paymentStatusTypesID`)

										LEFT JOIN `" . $thisTableOrderInvoices . "`
										ON(`" . $thisTableRelatedDocuments . "`.`relatedDocuments_RE` = `" . $thisTableOrderInvoices . "`.`orderDocumentsNumber`)

										LEFT JOIN `" . $thisTablePaymentStatusTypes . "` AS `status_RE`
										ON(`" . $thisTableOrderInvoices . "`.`orderDocumentsStatus` = `status_RE`.`paymentStatusTypesID`)

										LEFT JOIN `" . $thisTableOrderCredits . "`
										ON(`" . $thisTableRelatedDocuments . "`.`relatedDocuments_GU` = `" . $thisTableOrderCredits . "`.`orderDocumentsNumber`)

										LEFT JOIN `" . $thisTablePaymentStatusTypes . "` AS `status_GU`
										ON(`" . $thisTableOrderCredits . "`.`orderDocumentsStatus` = `status_GU`.`paymentStatusTypesID`)

										LEFT JOIN `" . $thisTableOrderReminders . "`
										ON(`" . $thisTableRelatedDocuments . "`.`relatedDocuments_MA` = `" . $thisTableOrderReminders . "`.`orderDocumentsNumber`)

										LEFT JOIN `" . $thisTablePaymentStatusTypes . "` AS `status_MA`
										ON(`" . $thisTableOrderReminders . "`.`orderDocumentsStatus` = `status_MA`.`paymentStatusTypesID`)

										LEFT JOIN `" . $thisTableOrderFirstDemands . "`
										ON(`" . $thisTableRelatedDocuments . "`.`relatedDocuments_M1` = `" . $thisTableOrderFirstDemands . "`.`orderDocumentsNumber`)

										LEFT JOIN `" . $thisTablePaymentStatusTypes . "` AS `status_M1`
										ON(`" . $thisTableOrderFirstDemands . "`.`orderDocumentsStatus` = `status_M1`.`paymentStatusTypesID`)

										LEFT JOIN `" . $thisTableOrderSecondDemands . "`
										ON(`" . $thisTableRelatedDocuments . "`.`relatedDocuments_M2` = `" . $thisTableOrderSecondDemands . "`.`orderDocumentsNumber`)

										LEFT JOIN `" . $thisTablePaymentStatusTypes . "` AS `status_M2`
										ON(`" . $thisTableOrderSecondDemands . "`.`orderDocumentsStatus` = `status_M2`.`paymentStatusTypesID`)

										WHERE 1
									";

								if($thisCheckStatusCombination != ""){
									$sql_getRelatedDocumentPaymentStatusHaving = " HAVING `concats` = '" . $thisCheckStatusCombination . "' ";
									$sql_getRelatedDocumentPaymentStatusSpecialCombination .= $sql_getRelatedDocumentPaymentStatusHaving;
									$sql_getRelatedDocumentPaymentStatusSpecialCombination .= $sql_getRelatedDocumentPaymentStatusOrder;

									$sql_getRelatedDocumentPaymentStatus = $sql_getRelatedDocumentPaymentStatusSpecialCombination;
								}
								else {
									$sql_getRelatedDocumentPaymentStatusAllCombinations = "
										SELECT
											COUNT(`tempTable`.`concats`) AS `count`,
											`tempTable`.`concats`,
											`tempTable`.`status`

											FROM (
												" . $sql_getRelatedDocumentPaymentStatusSpecialCombination . "

											) AS `tempTable`

											GROUP BY `tempTable`.`concats`

											" . $sql_getRelatedDocumentPaymentStatusOrder . "
										";
									$sql_getRelatedDocumentPaymentStatus = $sql_getRelatedDocumentPaymentStatusAllCombinations;
								}

								$rs_getRelatedDocumentPaymentStatus = $dbConnection->db_query($sql_getRelatedDocumentPaymentStatus);

								$countRow = 0;

								$arrPaymentStatusTypeDatas = getPaymentStatusTypes();

								echo '<table border="0" cellpadding="0" cellspacing="0" style="width:99%;" class="displayOrders">';

								while($db_getRelatedDocumentPaymentStatus = mysqli_fetch_assoc($rs_getRelatedDocumentPaymentStatus)){
									if($countRow == 0){
										$arrTempFields = array_keys($db_getRelatedDocumentPaymentStatus);
										if(!empty($arrTempFields)){
											$arrFields = array();
											foreach($arrTempFields as $thisTempField){
												if(!preg_match('/^[A-Z]{2}-PATH$|^[A-Z]{1}[0-9]{1}-PATH$/', $thisTempField)) {
													$arrFields[] = $thisTempField;
												}
											}
										}

										echo '<tr>';
										echo '<th style="width:45px;">#</th>';
										echo '<th>' . implode("</th><th>", ($arrFields)) . '</th>';
										echo '<th>check</th>';
										echo '</tr>';
									}

									// BOF HANDLE STATUS CONCATS
										$arrTemp = explode("-", $db_getRelatedDocumentPaymentStatus["concats"]);
										$arrThisStatus = array();
										$thisArrStatusSum = 0;
										$arrCountStatus = array();
										if(!empty($arrTemp)){
											foreach($arrTemp as $thisTemp){
												if(preg_match("/[0-9]{1,}/", $thisTemp)){
													$arrThisStatus[] = $thisTemp;
													$thisArrStatusSum += $thisTemp;
													$arrCountStatus[$thisTemp]++;
												}
											}
										}
										$thisAverageStatus = $thisArrStatusSum / count($arrThisStatus);
									// EOF HANDLE STATUS CONCATS

									// BOF HANDLE DOCUMENT CUSTOMER NUMBERS
										$arrTemp = explode("-", $db_getRelatedDocumentPaymentStatus["KNR"]);
										if(!empty($arrTemp)){
											foreach($arrTemp as $thisTempKey => $thisTempValue){
												if($thisTempValue == ''){
													unset($arrTemp[$thisTempKey]);
												}
											}
										}
										if(!empty($arrTemp)){
											$arrTemp = array_unique($arrTemp);
											$db_getRelatedDocumentPaymentStatus["KNR"] = implode(" - ", $arrTemp);
										}
									// EOF HANDLE DOCUMENT CUSTOMER NUMBERS

									if($countRow%2 == 0){ $rowClass = 'row1'; }
									else { $rowClass = 'row4'; }

									if($arrCountStatus[$thisAverageStatus] == count($arrThisStatus)){
										$rowClass = 'row3';
									}
									else {
										$rowClass = 'row10';
									}

									echo '<tr class="' . $rowClass . '" >';

									echo '<td style="text-align:right;"><b>';
									echo ($countRow + 1);
									echo '.</b></td>';

									$arrThisPath = parse_url($_SERVER["REQUEST_URI"]);
									$linkPath = $arrThisPath["path"];

									foreach($db_getRelatedDocumentPaymentStatus as $thisRelatedDocumentPaymentStatusKey => $thisRelatedDocumentPaymentStatusValue){
										$thisFormChangeDocumentPaymentStatus = '';
										if(!preg_match('/^[A-Z]{2}-PATH$|^[A-Z]{1}[0-9]{1}-PATH$/', $thisRelatedDocumentPaymentStatusKey)) {
											if($thisRelatedDocumentPaymentStatusKey == "count"){
												echo '<td style="text-align:center;">';
											}
											else if(preg_match("/^[A-Z][A-Z0-9]-St$/", $thisRelatedDocumentPaymentStatusKey)){
												echo '<td style="text-align:center;" title="' . $arrPaymentStatusTypeDatas[$thisRelatedDocumentPaymentStatusValue[$thisRelatedDocumentPaymentStatusKey]]["paymentStatusTypesName"] . '">';
												if($thisRelatedDocumentPaymentStatusValue != ''){
													$thisDocumentPaymentStatusDocumentType = substr($thisRelatedDocumentPaymentStatusKey, 0, 2);
													$thisDocumentPaymentStatusDocumentNumber = $db_getRelatedDocumentPaymentStatus[$thisDocumentPaymentStatusDocumentType . "-NR"];
													$thisDocumentPaymentStatusChangeSQL = "
														UPDATE
															`" . constant("TABLE_ORDER_" . $thisDocumentPaymentStatusDocumentType) . "`
															SET `orderDocumentsStatus` = '{###NEW_DOCUMENT_PAYMENT_STATUS###}'
															WHERE 1
																AND `orderDocumentsNumber` = '" . $thisDocumentPaymentStatusDocumentNumber . "';
													";

													$thisFormChangeDocumentPaymentStatus = ' ' . '<img src="layout/icons/iconEdit.gif" width="14" height="14" class="buttonChangeDocumentPaymentStatus" rel="' . $thisDocumentPaymentStatusDocumentType . '" alt="" title="Zahlstatus &auml;ndern" />';

													$thisFormChangeDocumentPaymentStatus .= '
														<div class="thisFormChangeDocumentPaymentStatusArea" style="width:400px;padding:4px;display:none;border:1px solid #333;text-align:left;position:absolute;left:auto;top:auto;background-color:#FFF;">
															<div class="loadNoticeContent">
																<div class="noticeBoxHeader">Dokument-NR: ' . $thisDocumentPaymentStatusDocumentNumber . '</div>
																<div class="noticeBoxClose">
																	<img class="buttonCloseDocumentPaymentStatusChangeSqlArea" width="14" height="14" title="Liste schließen" alt="" src="layout/icons/iconClose.png">
																</div>

																<select name="newPaymentStatus_' . $thisDocumentPaymentStatusDocumentNumber . '" id="newPaymentStatus_' . $thisDocumentPaymentStatusDocumentNumber . '" class="selectStatus" style="width:120px;">
																	<option value=""></option>
														';
														foreach($arrPaymentStatusTypeDatas as $thisPaymentStatusTypeDatasKey => $thisPaymentStatusTypeDatasValue){
															$thisFormChangeDocumentPaymentStatus .= '<option value="' . $thisPaymentStatusTypeDatasKey . '" title="' . $thisPaymentStatusTypeDatasValue["paymentStatusTypesName"] . '">' . $thisPaymentStatusTypeDatasValue["paymentStatusTypesShortName"] . ' - ' . $thisPaymentStatusTypeDatasKey . '</option>';
														}

													$thisFormChangeDocumentPaymentStatus .= '
																</select>
																<div class="clear"></div>
																<br />
																<p class="thisDocumentPaymentStatusChangeSqlArea" style="font-size:10px;" rel="' . $thisDocumentPaymentStatusDocumentType . '">' . $thisDocumentPaymentStatusChangeSQL . '</p>
															</div>
														</div>
													';
												}
											}
											else {
												echo '<td>';
											}

											if(preg_match('/[A-Z]{2}-NR/', $thisRelatedDocumentPaymentStatusKey)) {
												if($thisRelatedDocumentPaymentStatusValue != ''){
													$thisDocumentType = substr($thisRelatedDocumentPaymentStatusValue, 0, 2);
													echo '<a href="' . $linkPath . '?downloadFile=' . basename($db_getRelatedDocumentPaymentStatus[$thisDocumentType . "-PATH"]) . '&thisDocumentType=' . $thisDocumentType . '"><img src="layout/icons/iconPDF.gif" width="16" height="16" alt="' . $arrDocumentTypeDatas[$thisDocumentType]["createdDocumentsTypesName"]. ' Dokument herunterladen" title="Dokument herunterladen" /></a> ';
												}
											}

											if($thisRelatedDocumentPaymentStatusKey == "status"){
												$thisRelatedDocumentPaymentStatusValue = preg_replace("/(x)/", "&nbsp;$1", $thisRelatedDocumentPaymentStatusValue);
											}

											if($thisRelatedDocumentPaymentStatusKey == "KNR"){
												$thisRelatedDocumentPaymentStatusValue = '<a href="' . PAGE_EDIT_CUSTOMER . '?searchCustomerNumber='. $thisRelatedDocumentPaymentStatusValue . '" target="_blank"><b>' . $thisRelatedDocumentPaymentStatusValue . '</b></a>';
											}

											echo $thisRelatedDocumentPaymentStatusValue;
											echo $thisFormChangeDocumentPaymentStatus;
											echo '</td>';
										}
									}


									echo '<td style="text-align:center;">';
									if($arrCountStatus[$thisAverageStatus] == count($arrThisStatus)){
										echo '<b><span style="color:#009900;">ok</span></b>';
									}
									else {
										echo '<form name="formCheckSpecialPaymentStatusCombination_' . $countRow . '" action="' . $_SERVER["PHP_SELF"] . '" method="post" >';
										#[' . $countRow. ']

										$thisSubmitLabel = "prüfen";
										if($_POST["thisCheckStatusCombination"] != ""){
											$thisSubmitLabel = "Abbrechen";
										}
										else {
											echo '<input type="text" class="inputField_70" name="thisCheckStatusCombination" value="' . ($db_getRelatedDocumentPaymentStatus["concats"]) . '" />';
										}
										echo '<input type="hidden" name="selectAction" value="' . $_POST["selectAction"]. '" />';
										echo '<input type="hidden" name="handleThisCheckRelatedDocumentPaymentStatus" value="' . $_POST["handleThisCheckRelatedDocumentPaymentStatus"]. '" />';
										echo '<input type="hidden" name="submitFormReviseDatas" value="' . $_POST["submitFormReviseDatas"]. '" />';

										echo '<input type="submit" style="font-weight:bold;color:#990000;" name="submitCheckStatusCombination" value="' . $thisSubmitLabel . '" />';
										echo '</form>';
									}
									echo '</td>';


									echo '</tr>';

									// BOF SQL AREA
										if($_REQUEST["submitCheckStatusCombination"] != ''){
											echo '<tr class="sqlArea" style="display:none;border-bottom:2px solid #333;">';
											echo '<td style="text-align:right;"><b>&lfloor;</b></td>';
											echo '<td colspan="' . (count($arrFields) + 1) . '">';
											$arrAllDocumentTypes = array("AB", "RE", "GU", "MA", "M1", "M2");
											foreach($arrAllDocumentTypes as $thisAllDocumentType){
												echo '<div class="sqlArea_' . $thisAllDocumentType . '" style="display:none;line-height:20px;border-bottom:1px dotted #333;">';
												echo $thisAllDocumentType;
												echo '</div>';
											}

											echo '</td>';
											echo '</tr>';
										}
									// BOF SQL AREA

									$countRow++;
								}

								echo '</table>';
							}
							####################################
						}
						else if($_POST["selectAction"] == "ACTION_CHANGE_CUSTOMER_NUMBER"){
							##################
							echo '<div style="background-color:#FFFFFF;margin: 20px 0 20px 0;padding:10px;font-size:11px;border:2px solid #FF0000;">';
							$doExecute = false; // true | false

							## -- ALTE KNR: 13183 ID: 20033
							## -- NEUE KNR: 13074 ID: 9721

							#$customerNumber_OLD = "13183";
							#$customerID_OLD = "20033";
							#$customerNumber_NEW = "13074";
							#$customerID_NEW = "9721";

							$customerNumber_NEW = $_POST["newDocumentCustomerNumber"];
							$customerNumber_OLD = $_POST["handleThisDocumentCustomerNumber"];

							if($customerNumber_NEW != "" && $customerNumber_OLD != ""){
								// BOF GET CUSTOMERS IDs FROM COMMON-CUSTOMERS
									$sql_getCID = "SELECT `customersID`, `customersKundennummer` FROM `common_customers` WHERE `customersKundennummer` = '{###CUSTOMERS_NUMBER_NEW###}' OR `customersKundennummer` = '{###CUSTOMERS_NUMBER_OLD###}';";
									$sql_getCID = preg_replace("/{###CUSTOMERS_NUMBER_NEW###}/", $customerNumber_NEW, $sql_getCID);
									$sql_getCID = preg_replace("/{###CUSTOMERS_NUMBER_OLD###}/", $customerNumber_OLD, $sql_getCID);

									if($doExecute || ($customerID_NEW == "" && $customerID_OLD == "")){
										$rs_getCID = $dbConnection->db_query($sql_getCID);
										while($ds_getCID = mysqli_fetch_assoc($rs_getCID)){
											dd('ds_getCID');
											if($ds_getCID["customersKundennummer"] == $customerNumber_OLD){
												$customerID_OLD = $ds_getCID["customersID"];
											}
											else if($ds_getCID["customersKundennummer"] == $customerNumber_NEW){
												$customerID_NEW = $ds_getCID["customersID"];
											}
										}
									}
									else {
										echo "<b>GET CUSTOMERS IDs:</b>" . "<br />";
										echo $sql_getCID;
										echo "<br /><hr /><hr />";
									}
								// EOF GET CUSTOMERS IDs FROM COMMON-CUSTOMERS
							}

							if($customerNumber_NEW != "" && $customerNumber_OLD != "" && $customerID_NEW != "" && $customerNumber_OLD != ""){
								// BOF GET MANDATORIES
									$arrCustomerTypeMandatorData = getCustomerTypeMandators();
									$arrMandatoriesData = getMandatories();
									#dd('arrCustomerTypeMandatorData');
									#dd('arrMandatoriesData');
								// EOF GET MANDATORIES

								// BOF DEFINE DOCUMENT TABLES
									$arrDocumentTables = array(
											TABLE_ORDER_AB,
											TABLE_ORDER_GU,
											TABLE_ORDER_LS,
											TABLE_ORDER_RE,
											TABLE_ORDER_BR,
											TABLE_ORDER_AN,
											TABLE_ORDER_MA,
											TABLE_ORDER_M1,
											TABLE_ORDER_M2,
											TABLE_ORDER_M3,
											TABLE_ORDER_RK
										);
								// EOF DEFINE DOCUMENT TABLES

								// BOF CHANGE DOCUMENTS PATHS / RENAME FILES
									if(!empty($arrDocumentTables)){
										$arrTemplatesSql_getFiles = array();
										foreach($arrDocumentTables as $thisDocumentTable){
											$arrTemplatesSql_getFiles[] = "
													SELECT
														`orderDocumentsID` AS `dataID`,
														`orderDocumentsNumber` AS `dataDocumentsNumber`,
														`orderDocumentsCustomerNumber` AS `dataCustomerNumber`,
														`orderDocumentsDocumentPath` AS `dataDocumentPath`,
														'`orderDocumentsDocumentPath`' AS `dataDocumentPathField`,
														'{###THIS_TABLE_NAME###}' AS `dataTable`
														FROM `" . $thisDocumentTable . "`
														WHERE 1
															AND `orderDocumentsDocumentPath` LIKE '%_KNR-{###CUSTOMERS_NUMBER_OLD###}%'
															AND (
																`orderDocumentsCustomerNumber` = '{###CUSTOMERS_NUMBER_OLD###}'
																OR
																`orderDocumentsCustomerNumber` = '{###CUSTOMERS_NUMBER_NEW###}'
																OR
																`orderDocumentsSalesman` = '{###CUSTOMERS_ID_OLD###}'
																OR
																`orderDocumentsSalesman` = '{###CUSTOMERS_ID_NEW###}'
															)
												";
										}
									}

									$arrTemplatesSql_getFiles[] = "
											SELECT
												`createdDocumentsID` AS `dataID`,
												`createdDocumentsNumber` AS `dataDocumentsNumber`,
												`createdDocumentsCustomerNumber` AS `dataCustomerNumber`,
												`createdDocumentsFilename` AS `dataDocumentPath`,
												'`createdDocumentsFilename`' AS `dataDocumentPathField`,
												'{###THIS_TABLE_NAME###}' AS `dataTable`
												FROM `" . TABLE_CREATED_DOCUMENTS . "`
												WHERE 1
													AND (
														`createdDocumentsFilename` LIKE '%_KNR-{###CUSTOMERS_NUMBER_OLD###}%'
														OR
														`createdDocumentsFilename` LIKE '%/{###CUSTOMERS_NUMBER_OLD###}_%'
														OR
														`createdDocumentsTitle` LIKE '{###CUSTOMERS_NUMBER_OLD###}_%'
													)
													AND (
														`createdDocumentsCustomerNumber` = '{###CUSTOMERS_NUMBER_OLD###}'
														OR
														`createdDocumentsCustomerNumber` = '{###CUSTOMERS_NUMBER_NEW###}'
													)

										";

									#dd('arrTemplatesSql_getFiles');

									if(!empty($arrTemplatesSql_getFiles)){
										$arrSql_getFiles = array();
										foreach($arrTemplatesSql_getFiles as $thisSqlKey => $thisSqlValue){
											$arrTemplatesSql_getFiles[$thisSqlKey] = preg_replace("/{###CUSTOMERS_NUMBER_NEW###}/", $customerNumber_NEW, $arrTemplatesSql_getFiles[$thisSqlKey]);
											$arrTemplatesSql_getFiles[$thisSqlKey] = preg_replace("/{###CUSTOMERS_NUMBER_OLD###}/", $customerNumber_OLD, $arrTemplatesSql_getFiles[$thisSqlKey]);

											$arrTemplatesSql_getFiles[$thisSqlKey] = preg_replace("/{###CUSTOMERS_ID_NEW###}/", $customerID_NEW, $arrTemplatesSql_getFiles[$thisSqlKey]);
											$arrTemplatesSql_getFiles[$thisSqlKey] = preg_replace("/{###CUSTOMERS_ID_OLD###}/", $customerID_OLD, $arrTemplatesSql_getFiles[$thisSqlKey]);



											if(preg_match("/`common_/", $thisSqlValue)){
												$thisFindTableName = preg_match("/[UPDATE|FROM] (`.*`)/", $arrTemplatesSql_getFiles[$thisSqlKey], $arrFound);
												$thisTableName = "";
												if($thisFindTableName){
													$thisTableName = $arrFound[1];
													#dd('thisTableName');
												}
												$arrTemplatesSql_getFiles[$thisSqlKey] = preg_replace("/{###THIS_TABLE_NAME###}/", $thisTableName, $arrTemplatesSql_getFiles[$thisSqlKey]);
												$arrSql_getFiles[] = $arrTemplatesSql_getFiles[$thisSqlKey];
											}
											else {
												if(!empty($arrMandatoriesData)){
													foreach($arrMandatoriesData as $thisMandatorKey => $thisMandatorValue){

														$thisMandator = $thisMandatorValue["mandatoriesShortName"];
														$arrSql_temp = preg_replace("/`" . MANDATOR. "_/", "`" . $thisMandator . "_", $arrTemplatesSql_getFiles[$thisSqlKey]);
														$thisFindTableName = preg_match("/[UPDATE|FROM] (`.*`)/", $arrSql_temp, $arrFound);
														$thisTableName = "";
														if($thisFindTableName){
															$thisTableName = $arrFound[1];
															#dd('thisTableName');
														}
														$arrSql_temp = preg_replace("/{###THIS_TABLE_NAME###}/", $thisTableName, $arrSql_temp);
														#dd('arrSql_temp');
														$arrSql_getFiles[] = $arrSql_temp;
													}
												}
											}
											#echo '<hr>';
											#echo '<hr>';
											#echo '<hr>';

										}
									}
									#dd('arrSql_getFiles');

									if(!empty($arrSql_getFiles)){
										$sql_getFiles = implode(" UNION ", $arrSql_getFiles);

										$rs_getFiles = $dbConnection->db_query($sql_getFiles);

										$arrSql_updateFiles = array();
										$arrCmd_renameFiles = array();

										while($ds_getFiles = mysqli_fetch_assoc($rs_getFiles)){
											dd('ds_getFiles');
											#dd('customerNumber_OLD');
											#dd('customerNumber_NEW');
											$thisDataDocumentPath = $ds_getFiles["dataDocumentPath"];
											#$thisDataDocumentPath = preg_replace("/\//", "\\/", $thisDataDocumentPath);

											$thisFileNameNew = preg_replace("/_KNR-" . $customerNumber_OLD . "/", "_KNR-" . $customerNumber_NEW, $thisDataDocumentPath);
											$thisFileNameNew = preg_replace("/\/" . $customerNumber_OLD . "_/", "/" . $customerNumber_NEW . "_", $thisFileNameNew);
											$thisFileNameOld = $ds_getFiles["dataDocumentPath"];

											if(file_exists($thisFileNameOld)){
												if($thisFileNameNew != ""){
													if($doExecute){
														$rs_renameFile = rename($thisFileNameOld, $thisFileNameNew);
														if($rs_renameFile){
															$succesMessage .= 'TABELLE: ' . $ds_getFiles["dataTable"]. ' - Die Datei &quot;' . basename($thisFileNameOld) . '&quot; wurde umbenannt zu &quot;' . basename($thisFileNameNew) . '&quot;' . '<br />';
														}
														else {
															$errorMessage .= 'TABELLE: ' . $ds_getFiles["dataTable"]. ' - Die Datei &quot;' . basename($thisFileNameOld) . '&quot; konnte nicht umbenannt werden zu &quot;' . basename($thisFileNameNew) . '&quot;' . '<br />';
														}
													}
													else {
														$thisCommand = 'rename "' . BASEPATH . $thisFileNameOld . '" "' . $thisFileNameNew . '"';
														$thisCommand = preg_replace("/\//", "\\", $thisCommand);
														$arrCmd_renameFiles[] = $thisCommand;
													}
												}
											}
											else {
												$errorMessage .= 'Die Datei &quot;' . basename($thisFileNameOld) . '&quot; konnte nicht gefunden werden.' . '<br />';
											}

											if($thisFileNameNew != "" && $thisFileNameOld != ""){
												$arrSql_updateFiles[] = "
													UPDATE
														" . $ds_getFiles["dataTable"] . "
														SET " . $ds_getFiles["dataDocumentPathField"] . " = '" . $thisFileNameNew . "'
														WHERE " . $ds_getFiles["dataDocumentPathField"] . " = '" . $thisFileNameOld . "'
													;
												";
											}

											#dd('thisFileNameNew');
											#dd('thisFileNameOld');
											#dd('ds_getFiles');
											#echo '<hr /><hr /><hr />';
										}

										if(!empty($arrCmd_renameFiles)){
											/*
											echo "<hr>";
											$testCMD = $arrCmd_renameFiles[0];
											#$testCMD = escapeshellcmd($testCMD);

											$output = shell_exec($testCMD);
											$execResult = exec($testCMD, $output);
											dd('testCMD');
											dd('output');
											echo "<hr>";
											*/

											$arrCmd_renameFiles = array_unique($arrCmd_renameFiles);
											#dd('arrCmd_renameFiles');
											echo "<b>COMMAND CHANGE FILE_NAMES:</b> " . "<br />";
											foreach($arrCmd_renameFiles as $thisCmd_renameFile){
												echo $thisCmd_renameFile . "<br /><br />";
											}
										}
										echo '<hr>';
										if(!empty($arrSql_updateFiles)){
											#dd('arrSql_updateFiles');
											echo "<b>SQL CHANGE FILE_NAMES:</b> " . "<br />";
											foreach($arrSql_updateFiles as $thisSql_updateFile){
												$thisSql_updateFile = removeUnnecessaryChars($thisSql_updateFile);
												if($doExecute){
													$rs_updateFile = $dbConnection->db_query($thisSql_updateFile);
													echo "RS: " . $rs_updateFile . " | " . "<br />";
												}
												echo $thisSql_updateFile . "<br /><br />";
											}
										}
									}
								// EOF CHANGE DOCUMENTS PATHS / RENAME FILES

								echo '<hr /><hr /><hr />';
								// ####################################################################################################################
								// ####################################################################################################################
								// ####################################################################################################################


								// BOF DEFINE SQL TEMPLATES
									$arrTemplatesSql_changeCustomerData = array();
									## -- BOF DOCUMENT TABLES --------------
										if(!empty($arrDocumentTables)){
											foreach($arrDocumentTables as $thisDocumentTable){
												## -- REPLACE CUSTOMER NUMBER
												$arrTemplatesSql_changeCustomerData[] = "UPDATE `" . $thisDocumentTable . "` SET `orderDocumentsCustomerNumber` = '{###CUSTOMERS_NUMBER_NEW###}' WHERE `orderDocumentsCustomerNumber` = '{###CUSTOMERS_NUMBER_OLD###}';";

												## -- REPLACE SALESMAN ID
												$arrTemplatesSql_changeCustomerData[] = "UPDATE `" . $thisDocumentTable . "` SET `orderDocumentsSalesman` = '{###CUSTOMERS_ID_NEW###}' WHERE `orderDocumentsSalesman` = '{###CUSTOMERS_ID_OLD###}';";
											}
										}
										$arrTemplatesSql_changeCustomerData[] = "UPDATE `" . TABLE_CREATED_DOCUMENTS . "` SET `createdDocumentsCustomerNumber` = '{###CUSTOMERS_NUMBER_NEW###}' WHERE `createdDocumentsCustomerNumber` = '{###CUSTOMERS_NUMBER_OLD###}';";
									## -- EOF DOCUMENT TABLES --------------

									## -- BOF COMMON ORDERS
										$arrTemplatesSql_changeCustomerData[] = "UPDATE `" . TABLE_ORDERS . "` SET `ordersKundennummer` = '{###CUSTOMERS_NUMBER_NEW###}' WHERE `ordersKundennummer` = '{###CUSTOMERS_NUMBER_OLD###}';";
										$arrTemplatesSql_changeCustomerData[] = "UPDATE `" . TABLE_ORDERS . "` SET `ordersCustomerID` = '{###CUSTOMERS_ID_NEW###}' WHERE `ordersCustomerID` = '{###CUSTOMERS_ID_OLD###}';";
										$arrTemplatesSql_changeCustomerData[] = "UPDATE `" . TABLE_ORDERS . "` SET `ordersVertreterID` = '{###CUSTOMERS_ID_NEW###}' WHERE `ordersVertreterID` = '{###CUSTOMERS_ID_OLD###}';";
									## -- EOF COMMON ORDERS

									## -- COMMON DELIVERY DATAS
										$arrTemplatesSql_changeCustomerData[] = "UPDATE `" . TABLE_DELIVERY_DATAS . "` SET `Ref_Adresse_1` = '{###CUSTOMERS_NUMBER_NEW###}' WHERE `Ref_Adresse_1` = '{###CUSTOMERS_NUMBER_OLD###}';";

									## -- COMMON CUSTOMERS
										#$arrTemplatesSql_changeCustomerData[] = "UPDATE `" . TABLE_CUSTOMERS . "` SET `customersVertreterID` = '{###CUSTOMERS_ID_NEW###}' WHERE `customersVertreterID` = '{###CUSTOMERS_ID_OLD###}';";
										#$arrTemplatesSql_changeCustomerData[] = "UPDATE `" . TABLE_CUSTOMERS . "` SET `customersID` = '{###CUSTOMERS_ID_NEW###}' WHERE `customersID` = '{###CUSTOMERS_ID_OLD###}';";
										#$arrTemplatesSql_changeCustomerData[] = "UPDATE `" . TABLE_CUSTOMERS . "` SET `customersKundennummer` = '{###CUSTOMERS_NUMBER_NEW###}' WHERE `customersKundennummer` = '{###CUSTOMERS_NUMBER_OLD###}';";
										#$arrTemplatesSql_changeCustomerData[] = "UPDATE `" . TABLE_SHOP_CUSTOMERS . "` SET `customers_cid` = '{###CUSTOMERS_NUMBER_NEW###}' WHERE `customers_cid` = '{###CUSTOMERS_NUMBER_OLD###}';";

									## -- COMMON CUSTOMERS CONTACTS
										$arrTemplatesSql_changeCustomerData[] = "UPDATE `" . TABLE_CUSTOMERS_CONTACTS . "` SET `customersContactsContactID` = '{###CUSTOMERS_ID_NEW###}' WHERE `customersContactsContactID` = '{###CUSTOMERS_ID_OLD###}';";

									## -- COMMON CALENDAR
										$arrTemplatesSql_changeCustomerData[] = "UPDATE `" . TABLE_CUSTOMERS_CALENDAR . "` SET `customersCalendarCustomersID` = '{###CUSTOMERS_ID_NEW###}' WHERE `customersCalendarCustomersID` = '{###CUSTOMERS_ID_OLD###}';";
										$arrTemplatesSql_changeCustomerData[] = "UPDATE `" . TABLE_CUSTOMERS_CALENDAR . "` SET `customersCalendarCustomersNumber` = '{###CUSTOMERS_NUMBER_NEW###}' WHERE `customersCalendarCustomersNumber` = '{###CUSTOMERS_NUMBER_OLD###}';";

									## -- COMMON PRINT PRODUCTION ORDERS
										$arrTemplatesSql_changeCustomerData[] = "UPDATE `" . TABLE_PRINT_PRODUCTION_ORDERS . "` SET `printProductionOrdersCompanyID` = '{###CUSTOMERS_ID_NEW###}' WHERE `printProductionOrdersCompanyID` = '{###CUSTOMERS_ID_OLD###}';";
										$arrTemplatesSql_changeCustomerData[] = "UPDATE `" . TABLE_PRINT_PRODUCTION_ORDERS . "` SET `printProductionOrdersCompanyNumber` = '{###CUSTOMERS_NUMBER_NEW###}' WHERE `printProductionOrdersCompanyNumber` = '{###CUSTOMERS_NUMBER_OLD###}';";

									## -- SALESMEN
										$arrTemplatesSql_changeCustomerData[] = "UPDATE `" . TABLE_SALESMEN . "` SET `salesmenKundennummer` = '{###CUSTOMERS_NUMBER_NEW###}' WHERE `salesmenKundennummer` = '{###CUSTOMERS_NUMBER_OLD###}';";

									## -- SALESMEN STOCKS
										$arrTemplatesSql_changeCustomerData[] = "UPDATE `" . TABLE_SALESMEN_STOCKS . "` SET `salesmenStocksSalesmanID` = '{###CUSTOMERS_ID_NEW###}' WHERE `salesmenStocksSalesmanID` = '{###CUSTOMERS_ID_OLD###}';";
										$arrTemplatesSql_changeCustomerData[] = "UPDATE `" . TABLE_SALESMEN_STOCKS . "` SET `salesmenStocksSalesmanCustomerNumber` = '{###CUSTOMERS_NUMBER_NEW###}' WHERE `salesmenStocksSalesmanCustomerNumber` = '{###CUSTOMERS_NUMBER_OLD###}';";

									## -- CUSTOMERS TO CUSTOMERS
										$arrTemplatesSql_changeCustomerData[] = "UPDATE `" . TABLE_CUSTOMERS_TO_CUSTOMERS . "` SET `customersToCustomersParentID` = '{###CUSTOMERS_ID_NEW###}' WHERE `customersToCustomersParentID` = '{###CUSTOMERS_ID_OLD###}';";
										$arrTemplatesSql_changeCustomerData[] = "UPDATE `" . TABLE_CUSTOMERS_TO_CUSTOMERS . "` SET `customersToCustomersChildID` = '{###CUSTOMERS_ID_NEW###}' WHERE `customersToCustomersChildID` = '{###CUSTOMERS_ID_OLD###}';";

									## -- CUSTOMERS RELATIONS
										$arrTemplatesSql_changeCustomerData[] = "UPDATE `" . TABLE_CUSTOMERS_RELATIONS . "` SET `customersRelationsParentCustomerID` = '{###CUSTOMERS_ID_NEW###}' WHERE `customersRelationsParentCustomerID` = '{###CUSTOMERS_ID_OLD###}';";
										$arrTemplatesSql_changeCustomerData[] = "UPDATE `" . TABLE_CUSTOMERS_RELATIONS . "` SET `customersRelationsChildCustomerID` = '{###CUSTOMERS_ID_NEW###}' WHERE `customersRelationsChildCustomerID` = '{###CUSTOMERS_ID_OLD###}';";

										$arrTemplatesSql_changeCustomerData[] = "UPDATE `" . TABLE_CUSTOMERS_RELATIONS . "` SET `customersRelationsParentCustomerNumber` = '{###CUSTOMERS_NUMBER_NEW###}' WHERE `customersRelationsParentCustomerNumber` = '{###CUSTOMERS_NUMBER_OLD###}';";
										$arrTemplatesSql_changeCustomerData[] = "UPDATE `" . TABLE_CUSTOMERS_RELATIONS . "` SET `customersRelationsChildCustomerNumber` = '{###CUSTOMERS_NUMBER_NEW###}' WHERE `customersRelationsChildCustomerNumber` = '{###CUSTOMERS_NUMBER_OLD###}';";

									## -- SALESMEN PROVISIONS
										$arrTemplatesSql_changeCustomerData[] = "UPDATE `" . TABLE_SALESMEN_PROVISIONS . "` SET `salesmenProvisionsSalesmanID` = '{###CUSTOMERS_ID_NEW###}' WHERE `salesmenProvisionsSalesmanID` = '{###CUSTOMERS_ID_OLD###}';";
										$arrTemplatesSql_changeCustomerData[] = "UPDATE `" . TABLE_SALESMEN_PROVISIONS_DETAILS . "` SET `salesmenProvisionsDetailsProductRecipientCustomerNumber` = '{###CUSTOMERS_NUMBER_NEW###}' WHERE `salesmenProvisionsDetailsProductRecipientCustomerNumber` = '{###CUSTOMERS_NUMBER_OLD###}';";

									## -- GROUP PROVISIONS
										$arrTemplatesSql_changeCustomerData[] = "UPDATE `" . TABLE_GROUPS_PROVISIONS_DETAILS . "` SET `groupsProvisionsDetailsProductRecipientCustomerNumber` = '{###CUSTOMERS_NUMBER_NEW###}' WHERE `groupsProvisionsDetailsProductRecipientCustomerNumber` = '{###CUSTOMERS_NUMBER_OLD###}';";

									## -- ORDERS INTERVALS
										$arrTemplatesSql_changeCustomerData[] = "UPDATE `" . TABLE_CUSTOMERS_ORDERS_INTERVALS . "` SET `customersOrdersIntervalsCustomerNumber` = '{###CUSTOMERS_NUMBER_NEW###}' WHERE `customersOrdersIntervalsCustomerNumber` = '{###CUSTOMERS_NUMBER_OLD###}';";

									## -- ORDERS AMAZON
										$arrTemplatesSql_changeCustomerData[] = "UPDATE `" . TABLE_AMAZON_ORDERS . "` SET `KUNDENNUMMER` = '{###CUSTOMERS_NUMBER_NEW###}' WHERE `KUNDENNUMMER` = '{###CUSTOMERS_NUMBER_OLD###}';";

									#dd('sql_getCID');
									#dd('arrTemplatesSql_changeCustomerData');
								// EOF DEFINE SQL TEMPLATES

								// BOF CREATE SQL ORDERS
									if(!empty($arrTemplatesSql_changeCustomerData)){
										$arrSql_changeCustomerData = array();
										foreach($arrTemplatesSql_changeCustomerData as $thisSqlKey => $thisSqlValue){
											$arrTemplatesSql_changeCustomerData[$thisSqlKey] = preg_replace("/{###CUSTOMERS_NUMBER_NEW###}/", $customerNumber_NEW, $arrTemplatesSql_changeCustomerData[$thisSqlKey]);
											$arrTemplatesSql_changeCustomerData[$thisSqlKey] = preg_replace("/{###CUSTOMERS_NUMBER_OLD###}/", $customerNumber_OLD, $arrTemplatesSql_changeCustomerData[$thisSqlKey]);

											$arrTemplatesSql_changeCustomerData[$thisSqlKey] = preg_replace("/{###CUSTOMERS_ID_NEW###}/", $customerID_NEW, $arrTemplatesSql_changeCustomerData[$thisSqlKey]);
											$arrTemplatesSql_changeCustomerData[$thisSqlKey] = preg_replace("/{###CUSTOMERS_ID_OLD###}/", $customerID_OLD, $arrTemplatesSql_changeCustomerData[$thisSqlKey]);

											if(preg_match("/`common_/", $thisSqlValue)){
												$arrSql_changeCustomerData[] = $arrTemplatesSql_changeCustomerData[$thisSqlKey];
											}
											else {
												if(!empty($arrMandatoriesData)){
													foreach($arrMandatoriesData as $thisMandatorKey => $thisMandatorValue){
														$thisMandator = $thisMandatorValue["mandatoriesShortName"];
														$arrSql_changeCustomerData[] = preg_replace("/`" . MANDATOR. "_/", "`" . $thisMandator . "_", $arrTemplatesSql_changeCustomerData[$thisSqlKey]);
													}
												}
											}
										}
									}
								// EOF CREATE SQL ORDERS

								// BOF SQL ORDERS
									#dd('arrSql_changeCustomerData');
									if(!empty($arrSql_changeCustomerData)){
										echo "<b>SQL CHANGE CUSTOMER_NUMBERS:</b> " . "<br />";
										foreach($arrSql_changeCustomerData as $thisSql_changeCustomerData){
											if($doExecute){
												$rs_changeCustomerData = $dbConnection->db_query($thisSql_changeCustomerData);
												echo "RS: " . $rs_changeCustomerData . " | " . "<br />";
											}
											echo $thisSql_changeCustomerData . "<br /><br />";
										}
									}
								// EOF SQL ORDERS

								echo '<p class="warning">';
								echo '<ul>';
								echo '<li>Illustrator Dateien umbenennen!!!!!</li>';
								echo '<li>Vermerk in Kundennotiz schreiben!!!!!</li>';
								echo '</ul>';
								echo '</p>';
								##################
								echo '</div>';
							}
							else {
								$errorMessage .= 'Zu den Kundennummern konnten keine IDs gefunden werden, Kunden existieren nicht! ' . '<br />';
							}
						}
						else if($_POST["selectAction"] == "ACTION_FIND_FIND_CUSTOMER_NUMBERS_FOR_DELISPRINT_DELIVERY_DATAS_WITHOUT_CUSTOMER_NUMBERS"){
							$sql_updateCustomerNumbersForDeliverydatasWithoutCustomerNumber_x = "
									UPDATE
											`" . TABLE_DELIVERY_DATAS . "`

											INNER JOIN `" . TABLE_CUSTOMERS . "`
											ON(
												REPLACE(`" . TABLE_DELIVERY_DATAS . "`.`Firma_Empfaenger`, ' ', '') = REPLACE(`" . TABLE_CUSTOMERS . "`.`customersFirmenname`, ' ', '')
												AND
												`" . TABLE_DELIVERY_DATAS . "`.`PLZ_1` = `" . TABLE_CUSTOMERS . "`.`customersCompanyPLZ`
												AND
												`" . TABLE_DELIVERY_DATAS . "`.`Adresse_1` = CONCAT(`" . TABLE_CUSTOMERS . "`.`customersCompanyStrasse`, ' ', `" . TABLE_CUSTOMERS . "`.`customersCompanyHausnummer`)
											)

											SET `Ref_Adresse_1` = `" . TABLE_CUSTOMERS . "`.`customersKundennummer`
											WHERE 1
												AND `" . TABLE_DELIVERY_DATAS . "`.`Ref_Adresse_1` = ''
								";
								
							$sql_updateCustomerNumbersForDeliverydatasWithoutCustomerNumber = "
									UPDATE
										`" . TABLE_DELIVERY_DATAS . "`
										
										INNER JOIN `" . TABLE_CUSTOMERS . "`
										ON(`" . TABLE_DELIVERY_DATAS . "`.`PLZ_1` = `" . TABLE_CUSTOMERS . "`.`customersCompanyPLZ`)
										
										SET `Ref_Adresse_1` = `" . TABLE_CUSTOMERS . "`.`customersKundennummer`
										
										WHERE 1
											AND `" . TABLE_DELIVERY_DATAS . "`.`Ref_Adresse_1` = ''											
											AND (
												CONCAT(`" . TABLE_CUSTOMERS . "`.`customersCompanyStrasse`, ' ', `" . TABLE_CUSTOMERS . "`.`customersCompanyHausnummer`) = `Adresse_1`
												OR
												CONCAT(`" . TABLE_CUSTOMERS . "`.`customersCompanyStrasse`, ' ', `" . TABLE_CUSTOMERS . "`.`customersCompanyHausnummer`) = REPLACE(`Adresse_1`, 'straße', 'str')
												OR
												CONCAT(`" . TABLE_CUSTOMERS . "`.`customersCompanyStrasse`, ' ', `" . TABLE_CUSTOMERS . "`.`customersCompanyHausnummer`) = REPLACE(`Adresse_1`, 'straße', 'str.')
												OR
												CONCAT(`" . TABLE_CUSTOMERS . "`.`customersCompanyStrasse`, ' ', `" . TABLE_CUSTOMERS . "`.`customersCompanyHausnummer`) = REPLACE(`Adresse_1`, 'strasse', 'str')
												OR
												CONCAT(`" . TABLE_CUSTOMERS . "`.`customersCompanyStrasse`, ' ', `" . TABLE_CUSTOMERS . "`.`customersCompanyHausnummer`) = REPLACE(`Adresse_1`, 'strasse', 'str.')
												OR
												CONCAT(`" . TABLE_CUSTOMERS . "`.`customersCompanyStrasse`, ' ', `" . TABLE_CUSTOMERS . "`.`customersCompanyHausnummer`) = REPLACE(`Adresse_1`, 'str', 'str.')
											)
								";
								
							echo createSqlExecButtons();

							echo '<div class="debugVarsArea" style="border:2px solid #FF0000; background-color:#FFFFFF;">';

							echo '<p>';
							echo "<b>dataOutput - sql:</b><br />";

							$thisDataSQL = cleanSQL($sql_updateCustomerNumbersForDeliverydatasWithoutCustomerNumber);
							#dd('sql_updateCustomerNumbersForDeliverydatasWithoutCustomerNumber');
							#dd('thisDataSQL');
							if($thisDataSQL != ''){
								$countItem = 0;
								echo '<div class="sqlArea" style="font-size:11px; border-bottom:1px solid #333;">';
									echo '<input type="checkbox" class="executeSqlOrders" name="executeSql_' . $countItem . '" value="1" /> ';
										echo '<span class="sqlOrder">';
											echo $thisDataSQL . "<br />";
										echo '</span>';
								echo '</div>';
							}
							echo '</p>';
							echo '</div>';

							echo createSqlExecButtons();
						}
						else if($_POST["selectAction"] == "ACTION_CHECK_GU_HAVING_RE_WITHOUT_PAYMENTS_AND_DIFFERENT_TOTAL_SUM"){

							echo '<p class="infoArea">';
							echo 'Angezeigt werden hier Gutschriften (GU), deren Betrag von dem Betrag der zugeh&ouml;rigen Rechnung (RE) abweicht und für die weder ein RE-Zahlungseingang noch ein AB-Zahlungseingang vorliegt!';
							echo '</p>';

							$sql = "
									SELECT
											/* `" . TABLE_ORDER_CREDITS . "`.`orderDocumentsID` AS `GU-ID`, */
											`" . TABLE_ORDER_CREDITS . "`.`orderDocumentsNumber` AS `GU-NR`,
											`" . TABLE_ORDER_CREDITS . "`.`orderDocumentsTotalPrice` AS `GU-SUMME`,
											`" . TABLE_ORDER_CREDITS . "`.`orderDocumentsStatus` AS `GU-STATUS`,

											/* `" . TABLE_ORDER_INVOICES . "`.`orderDocumentsID` AS `RE-ID`, */
											`" . TABLE_ORDER_INVOICES . "`.`orderDocumentsNumber` AS `RE-NR`,
											`" . TABLE_ORDER_INVOICES . "`.`orderDocumentsTotalPrice` AS `RE-SUMME`,
											`" . TABLE_ORDER_INVOICES . "`.`orderDocumentsStatus` AS `RE-STATUS`,

											/* `" . TABLE_ORDER_CONFIRMATIONS . "`.`orderDocumentsID` AS `AB-ID`, */
											`" . TABLE_ORDER_CONFIRMATIONS . "`.`orderDocumentsNumber` AS `AB-NR`,
											`" . TABLE_ORDER_CONFIRMATIONS . "`.`orderDocumentsTotalPrice` AS `AB-SUMME`,
											`" . TABLE_ORDER_CONFIRMATIONS . "`.`orderDocumentsStatus` AS `AB-STATUS`,

											`" . TABLE_ORDER_INVOICE_PAYMENTS . "`.`orderPaymentBankAccountID` AS `RE-PAY_BANK`,
											`" . TABLE_ORDER_INVOICE_PAYMENTS . "`.`orderPaymentValue` AS `RE-PAY_SUM`,
											`" . TABLE_ORDER_INVOICE_PAYMENTS . "`.`orderPaymentDate` AS `RE-PAY_DATE`,

											`" . TABLE_ORDER_CONFIRMATION_PAYMENTS . "`.`orderPaymentBankAccountID` AS `AB-PAY_BANK`,
											`" . TABLE_ORDER_CONFIRMATION_PAYMENTS . "`.`orderPaymentValue` AS `AB-PAY_SUM`,
											`" . TABLE_ORDER_CONFIRMATION_PAYMENTS . "`.`orderPaymentDate` AS `AB-PAY_DATE`

										FROM `" . TABLE_ORDER_CREDITS . "`

										LEFT JOIN `" . TABLE_RELATED_DOCUMENTS . "`
										ON(`" . TABLE_ORDER_CREDITS . "`.`orderDocumentsNumber` = `" . TABLE_RELATED_DOCUMENTS . "`.`relatedDocuments_GU`)

										INNER JOIN `" . TABLE_ORDER_INVOICES . "`
										ON(`" . TABLE_RELATED_DOCUMENTS . "`.`relatedDocuments_RE` = `" . TABLE_ORDER_INVOICES . "`.`orderDocumentsNumber`)

										LEFT JOIN `" . TABLE_ORDER_CONFIRMATIONS . "`
										ON(`" . TABLE_RELATED_DOCUMENTS . "`.`relatedDocuments_AB` = `" . TABLE_ORDER_CONFIRMATIONS . "`.`orderDocumentsNumber`)

										LEFT JOIN `" . TABLE_ORDER_INVOICE_PAYMENTS . "`
										ON(`" . TABLE_ORDER_INVOICES . "`.`orderDocumentsNumber` = `" . TABLE_ORDER_INVOICE_PAYMENTS . "`.`orderPaymentOrderNumber`)

										LEFT JOIN `" . TABLE_ORDER_CONFIRMATION_PAYMENTS . "`
										ON(`" . TABLE_ORDER_CONFIRMATIONS . "`.`orderDocumentsNumber` = `" . TABLE_ORDER_CONFIRMATION_PAYMENTS . "`.`orderPaymentOrderNumber`)

										/* GROUP BY `" . TABLE_ORDER_CREDITS . "`.`orderDocumentsNumber` */

										HAVING
											`" . TABLE_ORDER_CREDITS . "`.`orderDocumentsTotalPrice` != `" . TABLE_ORDER_CONFIRMATIONS . "`.`orderDocumentsTotalPrice`
											AND `RE-PAY_DATE` IS NULL
											AND `AB-PAY_DATE` IS NULL

										ORDER BY `" . TABLE_ORDER_CREDITS . "`.`orderDocumentsNumber` DESC
								";

							$rs = $dbConnection->db_query($sql);

							if($dbConnection->db_getMysqlNumRows($rs) > 0){
								$countRow = 0;

								$arrPaymentStatusTypeDatas = getPaymentStatusTypes();

								echo '<table border="0" cellpadding="0" cellspacing="0" style="width:99%;" class="displayOrders">';

								while($ds = mysqli_fetch_assoc($rs)){
									if($countRow == 0){
										echo '<tr>';
										echo '<th style="width:45px;">#</th>';
										echo '<th>' . implode("</th><th>", array_keys($ds)) . '</th>';
										echo '</tr>';
									}

									if($countRow%2 == 0){ $rowClass = 'row1'; }
									else { $rowClass = 'row4'; }

									if($ds["GU-SUMME"] != $ds["RE-SUMME"]){
										$rowClass = 'row3';
									}
									if($ds["GU-STATUS"] != $ds["RE-STATUS"]){
										$rowClass = 'row3';
									}

									echo '<tr class="' . $rowClass . '">';
									echo '<td style="text-align:right;"><b>';
									echo ($countRow + 1);
									echo '.</b></td>';

									foreach($ds as $dsKey => $dsValue){
										$thisStyle = '';
										$thisValue = $dsValue;
										if(preg_match("/-SUMME/", $dsKey)){
											$thisStyle = 'text-align:right;font-weight:bold;background-color:#FEFFAF;';
										}
										echo '<td style="white-space:nowrap;' . $thisStyle . '">';
										if(preg_match("/-STATUS/", $dsKey)){
											$thisValue = $arrPaymentStatusTypeDatas[$dsValue]["paymentStatusTypesName"] . ' [' . $dsValue . ']';
										}
										else if(preg_match("/-SUMME/", $dsKey)){
											$thisValue = number_format($dsValue, 2, ",", ".");
										}
										else {
											$thisValue = $dsValue;
										}
										echo $thisValue;
										echo '</td>';
									}


									echo '</tr>';

									$countRow++;
								}

								echo '</table>';
							}
							else {
								$infoMessage = 'Keine Daten gefunden.' . '<br />';
							}
						}
						else {
							#$errorMessage .= 'Sie haben keine Aktion ausgew&auml;hlt! ' . '<br />';
						}
					}
					else if($_POST["selectAction"] == "ACTION_RELATED_DOCUMENTS"){
						$sql = "
							SELECT
								`" . TABLE_CREATED_DOCUMENTS . "`.`createdDocumentsNumber` AS `docNumber_0`,
								`" . TABLE_CREATED_DOCUMENTS . "`.`createdDocumentsNumber` AS `docNumber_1`,
								`" . TABLE_CREATED_DOCUMENTS . "`.`createdDocumentsNumber` AS `relatedDocs`
								FROM `" . TABLE_CREATED_DOCUMENTS . "`
								WHERE
									SUBSTRING(`" . TABLE_CREATED_DOCUMENTS . "`.`createdDocumentsNumber`, 1, 2) != 'KA'
									AND
									SUBSTRING(`" . TABLE_CREATED_DOCUMENTS . "`.`createdDocumentsNumber`, 1, 2) != 'PR'
									AND
									SUBSTRING(`" . TABLE_CREATED_DOCUMENTS . "`.`createdDocumentsNumber`, 1, 2) != 'BR'

							UNION

							SELECT
								`" . TABLE_DOCUMENTS_TO_DOCUMENTS . "`.`documentsToDocumentsOriginDocumentNumber` AS `docNumber_0`,
								`" . TABLE_DOCUMENTS_TO_DOCUMENTS . "`.`documentsToDocumentsCreatedDocumentNumber` AS `docNumber_1`,
								CONCAT(
									`" . TABLE_DOCUMENTS_TO_DOCUMENTS . "`.`documentsToDocumentsOriginDocumentNumber`,
									';',
									`" . TABLE_DOCUMENTS_TO_DOCUMENTS . "`.`documentsToDocumentsCreatedDocumentNumber`
								) AS `relatedDocs`
								FROM `" . TABLE_DOCUMENTS_TO_DOCUMENTS . "`
								WHERE
									`" . TABLE_DOCUMENTS_TO_DOCUMENTS . "`.`documentsToDocumentsRelationType` = 'convert';
						";
						$rs = $dbConnection->db_query($sql);

						if($rs){
							$arrRelatedDocs = array();
							$count=0;
							while($ds = mysqli_fetch_array($rs)){
								$arrRelatedDocs[$ds["docNumber_0"]][substr($ds["docNumber_0"], 0, 2)] = $ds["docNumber_0"];
								$arrRelatedDocs[$ds["docNumber_0"]][substr($ds["docNumber_1"], 0, 2)] = $ds["docNumber_1"];

								$arrRelatedDocs[$ds["docNumber_1"]][substr($ds["docNumber_0"], 0, 2)] = $ds["docNumber_0"];
								$arrRelatedDocs[$ds["docNumber_1"]][substr($ds["docNumber_1"], 0, 2)] = $ds["docNumber_1"];

								if(!empty($arrRelatedDocs[$ds["docNumber_0"]])){
									foreach($arrRelatedDocs[$ds["docNumber_0"]] as $thisRelatedDocKey => $thisRelatedDocValue){
										$arrRelatedDocs[$thisRelatedDocValue] = array_merge($arrRelatedDocs[$ds["docNumber_0"]], $arrRelatedDocs[$thisRelatedDocValue]);
									}
								}
								if(!empty($arrRelatedDocs[$ds["docNumber_1"]])){
									foreach($arrRelatedDocs[$ds["docNumber_1"]] as $thisRelatedDocKey => $thisRelatedDocValue){
										$arrRelatedDocs[$thisRelatedDocValue] = array_merge($arrRelatedDocs[$ds["docNumber_1"]], $arrRelatedDocs[$thisRelatedDocValue]);
									}
								}

								$arrRelatedDocs[$ds["docNumber_0"]] = array_merge($arrRelatedDocs[$ds["docNumber_0"]], $arrRelatedDocs[$ds["docNumber_1"]]);
								$arrRelatedDocs[$ds["docNumber_1"]] = array_merge($arrRelatedDocs[$ds["docNumber_0"]], $arrRelatedDocs[$ds["docNumber_1"]]);
								$count++;
							}
							#displayDebug('arrRelatedDocs');
							$arrRelatedDocsSQL = array();

							echo '<div class="debugVarsArea" style="border:2px solid #FF0000; background-color:#FFFFFF;">';
							foreach($arrRelatedDocs as $thisRelatedDocsKey => $thisRelatedDocsValue){
								$arrThisSqlFields = array();
								$arrThisSqlValues = array();
								$arrThisSqlUpdates = array();
								foreach($thisRelatedDocsValue as $thisRelatedDocsValueKey => $thisRelatedDocsValueValue){
									$arrThisSqlFields[] = "`relatedDocuments_" . $thisRelatedDocsValueKey . "`";
									$arrThisSqlValues[] = "'" . $thisRelatedDocsValueValue . "'";

									$arrThisSqlUpdates[] = "`relatedDocuments_" . $thisRelatedDocsValueKey . "` = '" . $thisRelatedDocsValueValue . "'";
								}
								// $thisSql = "INSERT IGNORE INTO `" . TABLE_RELATED_DOCUMENTS . "` (" . implode(", ", $arrThisSqlFields) . ") VALUES (" . implode(", ", $arrThisSqlValues) . ");";
								$thisSql = "INSERT INTO `" . TABLE_RELATED_DOCUMENTS . "` (" . implode(", ", $arrThisSqlFields) . ") VALUES (" . implode(", ", $arrThisSqlValues) . ")
												ON DUPLICATE KEY UPDATE " . implode(", ", $arrThisSqlUpdates) . "
									;";
								$arrRelatedDocsSQL[] = $thisSql;
								echo $thisSql . '<br />';
							}
							echo '</div>';
							#displayDebug('arrRelatedDocsSQL');
						}
					}
					else if($_POST["selectAction"] == "ACTION_FIND_RES_WITH_NO_PRODUCTION_STATUS_SEND"){
					}
					else if($_POST["selectAction"] == "ACTION_FIND_ORDERS_IN_DOCUMENTS_OF_ALL_MANDATORIES"){
					}
					// EOF EXECUTE
				?>

				</form>

				<?php
					if($_POST["selectAction"] == "ACTION_CONVERT_AMAZON_ORDER_DATAS" && $_POST["handleThisAmazonDatas"] != '') {
						$arrDataOutput = convertAmazonOrdersData($_POST["handleThisAmazonDatas"]);

						echo '<hr />';

						echo createSqlExecButtons();

						echo '<div class="debugVarsArea" style="border:2px solid #FF0000; background-color:#FFFFFF;">';

						#echo '<p>';
						#echo "<b>dataOutput - sql:</b><br />" . nl2br($arrDataOutput['sql']) . '<hr />';
						#echo '</p>';

						echo '<p>';
						echo "<b>dataOutput - sql:</b><br />";
						$arrTemp = explode("\n", trim($arrDataOutput['sql']));
						$countItem = 0;
						if(!empty($arrTemp)){
							foreach($arrTemp as $thisDataSQL){
								echo '<div class="sqlArea" style="font-size:11px; border-bottom:1px solid #333;">';
									echo '<input type="checkbox" class="executeSqlOrders" name="executeSql_' . $countItem . '" value="1" /> ';
										echo '<span class="sqlOrder">';
											echo $thisDataSQL . "<br />";
										echo '</span>';
								echo '</div>';
								$countItem++;
							}
						}
						echo '</p>';
						echo '</div>';

						echo createSqlExecButtons();
					}


					if($_POST["selectAction"] == "ACTION_AMAZON_INFORM_ORDER_CUSTOMERS_RATING" && $_POST["handleThisAmazonDatas"] != '') {
						$arrGetNotInformedCustomersOrderIDs = getNotInformedCustomersOrderIDs();
						if(!empty($arrGetNotInformedCustomersOrderIDs)){
							$arrGetNotInformedCustomersOrderDatas = getNotInformedCustomersOrderIdLinks($_POST["handleThisAmazonDatas"]);
							if(!empty($arrGetNotInformedCustomersOrderDatas)){

								echo '<p>';
								echo "<b>dataOutput - TEXT:</b>";
								echo '<br /><br />Sehr geehrte Kundin, sehr geehrter Kunde,<br /><br />wir bedanken uns nochmals für Ihre Bestellung.<br />Über eine positive Bewertung würden wir uns freuen.<br /><br />Mit freundlichen Grüßen,<br />Ihr B3-Team';
								echo '</p>';

								echo '<p>';
								echo "<b>dataOutput - LINKS:</b>";
								echo '<ol>';
								foreach($arrGetNotInformedCustomersOrderDatas as $thisDataLink){
									echo '<li>' . $thisDataLink["LINK"] . '</li>';
								}
								echo '</ol>';
								echo '</p>';

								echo '<hr />';

								echo createSqlExecButtons();

								echo '<div class="debugVarsArea" style="border:2px solid #FF0000; background-color:#FFFFFF;">';
								echo '<p>';
								echo "<b>dataOutput - sql:</b><br />";
								$countItem = 0;
								foreach($arrGetNotInformedCustomersOrderDatas as $thisDataID){
									echo '<div class="sqlArea" style="font-size:11px; border-bottom:1px solid #333;">';
										echo '<input type="checkbox" class="executeSqlOrders" name="executeSql_' . $countItem . '" value="1" /> ';
											echo '<span class="sqlOrder">';
												echo "INSERT IGNORE INTO `" . TABLE_AMAZON_ORDERS_RATINGS . "` (`ordersAmazonRatingsTransaktionsID`) VALUES ('" . $thisDataID["ID"] . "');" . "<br />";
											echo '</span>';
									echo '</div>';
									$countItem++;
								}

								echo '</p>';
								echo '</div>';

								echo createSqlExecButtons();
							}
						}
					}

					if($_POST["selectAction"] == "ACTION_CONVERT_AMAZON_TRANSACTION_DATAS"&& $_POST["handleThisAmazonDatas"] != '') {
						$arrDataOutput = convertAmazonTransactionsData($_POST["handleThisAmazonDatas"]);
						echo '<div class="debugVarsArea" style="border:2px solid #FF0000; background-color:#FFFFFF;">';

						#echo '<p>';
						#echo "<b>dataOutput - sql:</b><br />" . nl2br($arrDataOutput['sql']) . '<hr />';
						#echo '</p>';

						echo '<hr />';

						echo createSqlExecButtons();

						echo '<p>';
						echo "<b>dataOutput - sql:</b><br />";
						$arrTemp = explode("\n", trim($arrDataOutput['sql']));
						$countItem = 0;
						if(!empty($arrTemp)){
							foreach($arrTemp as $thisDataSQL){
								echo '<div class="sqlArea" style="font-size:11px; border-bottom:1px solid #333;">';
									echo '<input type="checkbox" class="executeSqlOrders" name="executeSql_' . $countItem . '" value="1" /> ';
										echo '<span class="sqlOrder">';
											echo $thisDataSQL . "<br />";
										echo '</span>';
								echo '</div>';
								$countItem++;
							}
						}
						echo '</p>';
						echo '</div>';

						echo createSqlExecButtons();
					}
					if($_POST["selectAction"] == "ACTION_GET_EMAIL_FROM_TEXT" && $handleThisMailDatas != '') {
						$arrDataOutput = convertMailData($handleThisMailDatas);
						echo '<div class="debugVarsArea" style="border:2px solid #FF0000; background-color:#FFFFFF;">';
						echo '<p>';
						echo "<b>dataOutput:</b><br />" . nl2br($arrDataOutput['mail']) . '<hr />';
						echo '</p>';
						echo '</div>';
					}
					if($_POST["selectAction"] == "ACTION_LAYOUT_FILE_DATAS" && $handleThisLayoutDatas != '') {
						$arrDataOutput = convertLayoutData($handleThisLayoutDatas);

						echo '<hr />';

						echo createSqlExecButtons();

						echo '<div class="debugVarsArea" style="border:2px solid #FF0000; background-color:#FFFFFF;">';

						#echo '<p>';
						#echo "<b>dataOutput - sql:</b><br />" . nl2br($arrDataOutput['sql']) . '<hr />';
						#echo '</p>';

						echo '<p>';
						echo "<b>dataOutput - sql:</b><br />";
						$arrTemp = explode("\n", trim($arrDataOutput['sql']));
						$countItem = 0;
						if(!empty($arrTemp)){
							foreach($arrTemp as $thisDataSQL){
								$thisShowSql = true;
								if($_POST["handleThisLayoutDate"] != ""){
									$rs_match = preg_match("/[0-9]{4}-[0-9]{2}-[0-9]{2}/", $thisDataSQL, $arrFound);
									$thisEntryDate = $arrFound[0];

									$thisShowSql = false;
									if($thisEntryDate >= formatDate($_POST["handleThisLayoutDate"], "store")){
										$thisShowSql = true;
									}
								}
								if($thisShowSql){
									echo '<div class="sqlArea" style="font-size:11px; border-bottom:1px solid #333;">';
									echo '<input type="checkbox" class="executeSqlOrders" name="executeSql_' . $countItem . '" value="1" /> ';
										echo '<span class="sqlOrder">';
											echo $thisDataSQL . "<br />";
										echo '</span>';
									echo '</div>';
									$countItem++;
								}
							}
						}
						echo '</p>';
						echo '</div>';

						echo createSqlExecButtons();
					}
					if($_POST["selectAction"] == "ACTION_CONVERT_DPD_DELISPRINT_DELIVERY_DATAS" && $handleThisDeliveryDatas != '') {
						$arrDataOutput = convertDeliSprintDeliveryData($handleThisDeliveryDatas, TABLE_DELIVERY_DATAS);
						$arrDataOutput['sql'] .= "DELETE FROM `" . TABLE_DELIVERY_DATAS . "` WHERE 1 AND (`Paketnummer` = 'Paketnummer' OR `Paketnummer` = 'Parcel number');" . "\n";

						$arrDataOutput['sql'] .= "UPDATE `" . TABLE_DELIVERY_DATAS . "` INNER JOIN `" . TABLE_CUSTOMERS . "` ON( REPLACE(`" . TABLE_DELIVERY_DATAS . "`.`Firma_Empfaenger`, ' ', '') = REPLACE(`" . TABLE_CUSTOMERS . "`.`customersFirmenname`, ' ', '') AND `" . TABLE_DELIVERY_DATAS . "`.`PLZ_1` = `" . TABLE_CUSTOMERS . "`.`customersCompanyPLZ`) SET `Ref_Adresse_1` = `" . TABLE_CUSTOMERS . "`.`customersKundennummer` WHERE 1 AND `" . TABLE_DELIVERY_DATAS . "`.`Ref_Adresse_1` = '' ;" . "\n";

						$arrDataOutput['sql'] .= "UPDATE `" . TABLE_DELIVERY_DATAS . "` SET `Ref_Adresse_1` = TRIM(`Ref_Adresse_1`);" . "\n";
						$arrDataOutput['sql'] .= "UPDATE `" . TABLE_DELIVERY_DATAS . "` SET `Ref_Adresse_2` = TRIM(`Ref_Adresse_2`);" . "\n";
						
						$arrDataOutput['sql'] .= "UPDATE `" . TABLE_DELIVERY_DATAS . "` SET `PLZ_1` = TRIM(`PLZ_1`);" . "\n";
						$arrDataOutput['sql'] .= "UPDATE `" . TABLE_DELIVERY_DATAS . "` SET `PLZ_2` = TRIM(`PLZ_2`);" . "\n";						
						
						echo '<hr />';

						echo createSqlExecButtons();

						echo '<div class="debugVarsArea" style="border:2px solid #FF0000; background-color:#FFFFFF;">';

						#echo '<p>';
						#echo "<b>dataOutput - sql:</b><br />" . nl2br($arrDataOutput['sql']) . '<hr />';
						#echo '</p>';

						echo '<p>';
						echo "<b>dataOutput - sql:</b><br />";
						$arrTemp = explode("\n", trim($arrDataOutput['sql']));

						$countItem = 0;
						if(!empty($arrTemp)){
							foreach($arrTemp as $thisDataSQL){
								echo '<div class="sqlArea" style="font-size:11px; border-bottom:1px solid #333;">';
									echo '<input type="checkbox" class="executeSqlOrders" name="executeSql_' . $countItem . '" value="1" /> ';
										echo '<span class="sqlOrder">';
											echo $thisDataSQL . "<br />";
										echo '</span>';
								echo '</div>';
								$countItem++;
							}
						}
						echo '</p>';
						echo '</div>';

						echo createSqlExecButtons();
					}

					if($_POST["selectAction"] == "ACTION_IMPORT_DPD_PARCEL_INVOICES" && $handleThisDeliveryDatas != '') {
						$arrDataOutput = convertDpdParcelInvoiceData($handleThisDeliveryDatas, TABLE_PARCEL_INVOICES_DPD);
						$arrDataOutput['sql'] .= "DELETE FROM `" . TABLE_PARCEL_INVOICES_DPD . "` WHERE 1 AND (`Fakturierungsbeleg` = 'Fakturierungsbeleg' OR `Fakturierungsbeleg` = '');" . "\n";

						echo '<hr />';

						echo createSqlExecButtons();

						echo '<div class="debugVarsArea" style="border:2px solid #FF0000; background-color:#FFFFFF;">';

						#echo '<p>';
						#echo "<b>dataOutput - sql:</b><br />" . nl2br($arrDataOutput['sql']) . '<hr />';
						#echo '</p>';

						echo '<p>';
						echo "<b>dataOutput - sql:</b><br />";
						$arrTemp = explode("\n", trim($arrDataOutput['sql']));

						$countItem = 0;
						if(!empty($arrTemp)){
							foreach($arrTemp as $thisDataSQL){
								echo '<div class="sqlArea" style="font-size:11px; border-bottom:1px solid #333;">';
									echo '<input type="checkbox" class="executeSqlOrders" name="executeSql_' . $countItem . '" value="1" /> ';
										echo '<span class="sqlOrder">';
											echo $thisDataSQL . "<br />";
										echo '</span>';
								echo '</div>';
								$countItem++;
							}
						}
						echo '</p>';
						echo '</div>';

						echo createSqlExecButtons();
					}

					if($_POST["selectAction"] == "ACTION_IMPORT_DPD_FTP_INTERFACE_STATUS_DATAS" && ($handleThisDeliveryDatas != '' || !empty($_POST["reReadLocalFile"]))) {
						// BOF CONVERT NEW LOADED DATA
							if($handleThisDeliveryDatas != ''){
								$arrDataOutput = convertDpdInterfaceDeliveryData($handleThisDeliveryDatas, TABLE_DELIVERY_DATAS_STATUS_IMPORT);
							}
							else{
								$arrDataOutput = array();
							}
						// EOF CONVERT NEW LOADED DATA

						// BOF CONVERT RE-READ DATA
							if(!empty($_POST["reReadLocalFile"])){
								$arrDataOutputReRead = array();
								foreach($_POST["reReadLocalFile"] as $thisReReadLocalFile){
									$thisReReadLocalFilePath = PATH_DPD_INTERFACE_TRACKING_FILES . $thisReReadLocalFile;
									if(file_exists($thisReReadLocalFilePath)){
										$successMessage .= 'Die Datei &quot;' . $thisReReadLocalFile . '&quot; existiert lokal!' . '<br />';

										$fp_localDpdFile = fopen($thisReReadLocalFilePath, "r");
										$thisReReadLocalFileContent = fread($fp_localDpdFile, filesize($thisReReadLocalFilePath));
										$thisReReadLocalFileContent = utf8_encode($thisReReadLocalFileContent);

										// BOF ADD SOURCE FILENAME TO DATA LINE
										$thisReReadLocalFileContent = preg_replace("/^/", basename($thisReReadLocalFilePath) . ";", $thisReReadLocalFileContent);
										$thisReReadLocalFileContent = preg_replace("/(\n)([0-9])/", "$1" . basename($thisReReadLocalFilePath).";$2", $thisReReadLocalFileContent);
										// EOF ADD SOURCE FILENAME TO DATA LINE

										// BOF DELETE EXISTING DATA OF RELOADED FILE
										$arrDataOutput['sql'] .= "DELETE FROM `" . TABLE_DELIVERY_DATAS_STATUS_IMPORT . "` WHERE 1 AND `SOURCE_FILE` = '" . $thisReReadLocalFile . "';" . "\n";
										// EOF DELETE EXISTING DATA OF RELOADED FILE

										$arrDataOutputReRead = convertDpdInterfaceDeliveryData($thisReReadLocalFileContent, TABLE_DELIVERY_DATAS_STATUS_IMPORT);

										$arrDataOutput['sql'] .= $arrDataOutputReRead["sql"];
									}
									else{
										$errorMessage .= 'Die Datei &quot;' . $thisReReadLocalFile . '&quot; existiert nicht lokal!' . '<br />';
									}
								}
								displayMessages();
							}
						// EOF CONVERT RE-READ DATA

						$arrDataOutput['sql'] .= "DELETE FROM `" . TABLE_DELIVERY_DATAS_STATUS_IMPORT . "` WHERE 1 AND `PARCELNO` = 'PARCELNO';" . "\n";

						echo '<hr />';

						echo createSqlExecButtons();

						echo '<div class="debugVarsArea" style="border:2px solid #FF0000; background-color:#FFFFFF;">';

						#echo '<p>';
						#echo "<b>dataOutput - sql:</b><br />" . nl2br($arrDataOutput['sql']) . '<hr />';
						#echo '</p>';

						echo '<p>';
						echo "<b>dataOutput - sql:</b><br />";
						$arrTemp = explode("\n", trim($arrDataOutput['sql']));

// BOF STORE SQL INTO FILE
$fileDpdInterfaceSql = "fileDpdInterfaceSql.sql";
if(file_exists($fileDpdInterfaceSql)){
	unlink($fileDpdInterfaceSql);
}
$fp_fileDpdInterfaceSql = fopen($fileDpdInterfaceSql, 'a+');
// BOF STORE SQL INTO FILE

						$countItem = 0;
						if(!empty($arrTemp)){
							foreach($arrTemp as $thisDataSQL){

								// BOF GET VALUES FOR MD5 ON ALL FIELDS
									$thisDataSqlValues = preg_match('/\(.*\)/', $thisDataSQL, $arrFound);
									$thisDataSqlAdd = " MD5(CONCAT" . $arrFound[0]. ") ";
									$thisDataSqlNew = preg_replace("/(\);)$/", ", " . $thisDataSqlAdd . "$1", $thisDataSQL);
									$thisDataSQL = $thisDataSqlNew;
								// EOF GET VALUES FOR MD5 ON ALL FIELDS

// BOF STORE SQL INTO FILE
fwrite($fp_fileDpdInterfaceSql, $thisDataSQL . "\n");
// BOF STORE SQL INTO FILE

								echo '<div class="sqlArea" style="font-size:11px; border-bottom:1px solid #333;">';
									echo '<input type="checkbox" class="executeSqlOrders" name="executeSql_' . $countItem . '" value="1" /> ';
										echo '<span class="sqlOrder">';
											echo $thisDataSQL . "<br />";
										echo '</span>';
								echo '</div>';
								$countItem++;
							}
						}

// BOF STORE SQL INTO FILE
fclose($fp_fileDpdInterfaceSql);
// BOF STORE SQL INTO FILE

						echo '</p>';
						echo '</div>';

						echo createSqlExecButtons();
					}

					if($_POST["selectAction"] == "ACTION_FIND_IMPORTED_DPD_STATUS_WITHOUT_DELISPRINT_DELIVERY_DATAS" && $handleThisDeliveryDatas != '') {
						dd('searchSQL2');
					}

					if($_POST["selectAction"] == "ACTION_CONVERT_CONTAINER_LISTS" && $handleThisDeliveryDatas != '') {
						$arrDataOutput = convertTrListsDataPrint($handleThisDeliveryDatas);

						echo '<hr />';

						#echo createSqlExecButtons();
						echo '<h3>Daten f&uuml;r EXCEL-Ausdruck:</h3>';
						echo '<div class="debugVarsArea" style="border:2px solid #FF0000; background-color:#FFFFFF;">';

						#echo '<p>';
						#echo "<b>dataOutput - csv:</b><br />" . nl2br($arrDataOutput['csv']) . '<hr />';
						#echo '</p>';

						echo '<pre>';
						echo $arrDataOutput['csv'];
						echo '</pre>';

						/*
						echo '<p>';
						echo "<b>dataOutput - sql:</b><br />";
						$arrTemp = explode("\n", trim($arrDataOutput['sql']));

						$countItem = 0;
						if(!empty($arrTemp)){
							foreach($arrTemp as $thisDataSQL){
								echo '<div class="sqlArea" style="font-size:11px; border-bottom:1px solid #333;">';
									echo '<input type="checkbox" class="executeSqlOrders" name="executeSql_' . $countItem . '" value="1" /> ';
										echo '<span class="sqlOrder">';
											echo $thisDataSQL . "<br />";
										echo '</span>';
								echo '</div>';
								$countItem++;
							}
						}
						echo '</p>';
						*/
						echo '</div>';

						echo '<hr />';

						echo '<h3>Daten f&uuml;r Auftragslisten:</h3>';
						$arrFieldsImportContainerLists = array(
							"containerListsFilename",
							"containerListsProductionsDateStart",
							"containerListsProductionsDateEnd",
							"containerListsDepartureKW",
							"containerListsArrivalKW",
							"containerListsDeliveryKW",
							"containerListsCustomerNumber",
							"containerListsCustomerName",
							"containerListsCustomerKommission",
							"containerListsProduct",
							"containerListsQuantity",
							"containerListsTirArkasi"
						);
						echo '<p class="infoArea">SELECT * FROM `' . TABLE_CONTAINER_LISTS . '`; </p>';
						echo '<p class="infoArea">FIELDS:<br />' . implode(",", $arrFieldsImportContainerLists) . '</p>';

						$arrDataOutput = convertTrListsDataAuftragslisten($arrDataOutput['csv']);

						echo '<div class="debugVarsArea" style="border:2px solid #FF0000; background-color:#FFFFFF;">';
						echo '<pre>';
						echo '' . implode(";", $arrFieldsImportContainerLists) . '';
						echo $arrDataOutput;
						echo '</pre>';
						echo '</div>';

						#echo createSqlExecButtons();
					}

					if($_POST["selectAction"] == "ACTION_FIND_RES_WITH_NO_PRODUCTION_STATUS_SEND" && $handleThisFindProductionStatusDatas != ""){
						// BOF VERSION 2
							$sql_template_2 = "
									SELECT
										`" . TABLE_ORDERS . "`.`ordersID`,
										`" . TABLE_ORDERS . "`.`ordersStatus`,
										`" . TABLE_ORDERS . "`.`ordersKundennummer`,
										`" . TABLE_ORDERS . "`.`ordersBestellDatum`,
										`" . TABLE_DELIVERY_DATAS . "`.`Versanddatum`,

										`" . TABLE_ORDERS . "`.`ordersLieferDatum`,
										CONCAT(
											`" . TABLE_ORDERS . "`.`ordersKundenName`,
											' - ',
											`" . TABLE_ORDERS . "`.`ordersKommission`
										) AS `ordersKunde`,

										`" . TABLE_ORDERS . "`.`ordersArtikelMenge`,
										`" . TABLE_ORDER_INVOICES_DETAILS . "`.`orderDocumentDetailProductQuantity`,
										CONCAT(
											`" . TABLE_ORDERS . "`.`ordersArtikelNummer`,
											' - ',
											`" . TABLE_ORDERS . "`.`ordersArtikelBezeichnung`
										) AS `ordersArtikel`,
										/* `" . TABLE_ORDERS . "`.`ordersArtikelKategorieID`, */

										`" . TABLE_ORDER_INVOICES_DETAILS . "`.`orderDocumentDetailProductQuantity`,
										CONCAT(
											`" . TABLE_ORDER_INVOICES_DETAILS . "`.`orderDocumentDetailProductNumber`,
											' - ',
											`" . TABLE_ORDER_INVOICES_DETAILS . "`.`orderDocumentDetailProductName`
										) AS `orderDocumentDetailProduct`,
										/* `" . TABLE_ORDER_INVOICES_DETAILS . "`.`orderDocumentDetailProductKategorieID`, */


										/* `" . TABLE_ORDER_INVOICES . "`.`orderDocumentsID`, */
										`" . TABLE_ORDER_INVOICES . "`.`orderDocumentsNumber`,
										`" . TABLE_ORDER_INVOICES . "`.`orderDocumentsStatus`,

										`" . TABLE_ORDER_INVOICES . "`.`orderDocumentsProcessingDate`,
										`" . TABLE_ORDER_INVOICES . "`.`orderDocumentsDocumentDate`,
										`" . TABLE_ORDER_INVOICES . "`.`orderDocumentsDeliveryDate`,

										`" . TABLE_DELIVERY_DATAS . "`.`Ref_Adresse_1`,
										`" . TABLE_DELIVERY_DATAS . "`.`Paketnummer`,
										`" . TABLE_DELIVERY_DATAS . "`.`Datum`,
										`" . TABLE_DELIVERY_DATAS . "`.`Referenznr_1`,
										`" . TABLE_DELIVERY_DATAS . "`.`Referenznr_2`,

										/* `" . TABLE_DELIVERY_DATAS . "`.`Zustelldatum`, */
										`" . TABLE_DELIVERY_DATAS . "`.`Firma_Empfaenger`,
										`" . TABLE_DELIVERY_DATAS . "`.`Name_1`,
										`" . TABLE_DELIVERY_DATAS . "`.`Zustellung`

									FROM `" . TABLE_ORDERS . "`

									LEFT JOIN `" . TABLE_ORDER_INVOICES_DETAILS . "`
									ON(
										`" . TABLE_ORDERS . "`.`ordersID` = `" . TABLE_ORDER_INVOICES_DETAILS . "`.`orderDocumentDetailOrderID`
										AND `" . TABLE_ORDER_INVOICES_DETAILS . "`.`orderDocumentDetailPosType` = 'product'
									)

									LEFT JOIN `" . TABLE_ORDER_INVOICES . "`
									ON(`" . TABLE_ORDER_INVOICES_DETAILS . "`.`orderDocumentDetailDocumentID` = `" . TABLE_ORDER_INVOICES . "`.`orderDocumentsID`)

									LEFT JOIN `" . TABLE_DELIVERY_DATAS . "`
									ON(
										`" . TABLE_ORDERS . "`.`ordersKundennummer` = `" . TABLE_DELIVERY_DATAS . "`.`Ref_Adresse_1`
										AND `" . TABLE_DELIVERY_DATAS . "`.`Zustellung` != ''
									)

									WHERE 1
										AND `" . TABLE_ORDERS . "`.`ordersBestellDatum` < `" . TABLE_DELIVERY_DATAS . "`.`Versanddatum`
										AND (
											`" . TABLE_ORDERS . "`.`ordersStatus` = '3'
											OR `" . TABLE_ORDERS . "`.`ordersStatus` = '7'
											OR `" . TABLE_ORDERS . "`.`ordersStatus` = '2'
										)
										AND `" . TABLE_ORDER_INVOICES . "`.`orderDocumentsStatus` = '2'

									GROUP BY `" . TABLE_ORDERS . "`.`ordersID`

									ORDER BY `" . TABLE_ORDERS . "`.`ordersBestellDatum`

									LIMIT 20
								";
						// EOF VERSION 2

						// BOF VERSION 1
							$sql_template = "
								SELECT
									*
									FROM (
										SELECT
											'{###MANDATOR###}' AS `MDT`,

											`" . TABLE_ORDER_INVOICES_DETAILS . "`.`orderDocumentDetailDocumentID` AS `DOC_ID`,
											`" . TABLE_ORDER_INVOICES_DETAILS . "`.`orderDocumentDetailOrderID` AS `OID`,

											`" . TABLE_ORDER_INVOICES . "`.`orderDocumentsNumber` AS `DOC_NR`,
											`" . TABLE_ORDER_INVOICES . "`.`orderDocumentsCustomerNumber` AS `KNR`,
											`" . TABLE_ORDER_INVOICES . "`.`orderDocumentsPaymentType` AS `PAY_TYPE`,
											`" . TABLE_PAYMENT_TYPES . "`.`paymentTypesName` AS `PAY_TYPE_NAME`,

											`" . TABLE_ORDERS . "`.`ordersStatus` AS `OSTAT`,
											`" . TABLE_ORDER_STATUS_TYPES . "`.`orderStatusTypesName` AS `OSTAT_NAME`,

											GROUP_CONCAT(`" . TABLE_DELIVERY_DATAS . "`.`Paketnummer`, ':', `" . TABLE_DELIVERY_DATAS . "`.`Datum` ORDER BY `" . TABLE_DELIVERY_DATAS . "`.`Paketnummer` SEPARATOR ';' ) AS `deliveryNumbers`

										FROM `" . TABLE_ORDER_INVOICES_DETAILS . "`

										LEFT JOIN `" . TABLE_ORDERS . "`
										ON(`" . TABLE_ORDER_INVOICES_DETAILS . "`.`orderDocumentDetailOrderID` = `" . TABLE_ORDERS . "`.`ordersID`)

										LEFT JOIN `" . TABLE_ORDER_INVOICES . "`
										ON(`" . TABLE_ORDER_INVOICES_DETAILS . "`.`orderDocumentDetailDocumentID` = `" . TABLE_ORDER_INVOICES . "`.`orderDocumentsID`)

										LEFT JOIN `" . TABLE_ORDER_STATUS_TYPES . "`
										ON(`" . TABLE_ORDERS . "`.`ordersStatus` = `" . TABLE_ORDER_STATUS_TYPES . "`.`orderStatusTypesID`)

										LEFT JOIN `" . TABLE_PAYMENT_TYPES . "`
										ON(`" . TABLE_ORDER_INVOICES . "`.`orderDocumentsPaymentType` = `" . TABLE_PAYMENT_TYPES . "`.`paymentTypesID`)

										LEFT JOIN `" . TABLE_DELIVERY_DATAS . "`
										ON(`" . TABLE_ORDER_INVOICES . "`.`orderDocumentsCustomerNumber` = `" . TABLE_DELIVERY_DATAS . "`.`Ref_Adresse_1`)

										WHERE 1

										GROUP BY `" . TABLE_ORDER_INVOICES_DETAILS . "`.`orderDocumentDetailOrderID`

										HAVING (
											/*
											`" . TABLE_ORDERS . "`.`ordersStatus` != 4
											AND
											`" . TABLE_ORDERS . "`.`ordersStatus` != 6
											*/
											`" . TABLE_ORDERS . "`.`ordersStatus` = 7
										)
									) AS `temp{###MANDATOR###}`
								";
						// EOF VERSION 1

						$arrHandleThisFindProductionStatusDatas = explode(";", $handleThisFindProductionStatusDatas);
						$arrSql = array();

						foreach($arrHandleThisFindProductionStatusDatas as $thisHandleThisFindProductionStatusDatas){
							$tempVar = $sql_template;
							$tempVar = preg_replace("/{###MANDATOR###}/", strToUpper($thisHandleThisFindProductionStatusDatas), $tempVar);
							$tempVar = preg_replace("/(`" . MANDATOR . "_)/", "`" . strToLower($thisHandleThisFindProductionStatusDatas) . "_", $tempVar);
							$arrSql[] = $tempVar;
						}

						$sql = implode(" UNION ", $arrSql) . " ORDER BY `MDT` DESC, `DOC_ID`, `KNR` ASC ";

						// BOF USE VERSION 2
							$sql = $sql_template_2;
						// EOF USE VERSION 2

						echo '<div class="debugVarsArea" style="border:2px solid #FF0000; background-color:#FFFFFF;">';
						echo '<pre>';
						echo $sql;
						echo '</pre>';
						echo '</div>';

						$executeSQL = $sql;
						$executeRS = $dbConnection->db_query($executeSQL);

						echo '<table border="0" cellpadding="0" cellspacing="0" style="width:99%;" class="displayOrders">';

						/*
						echo '<tr>';
						echo '<th>#</th>';
						echo '<th>MDT</th>';
						echo '<th>DOC_ID</th>';
						echo '<th>OID</th>';
						echo '<th>DOC_NR</th>';
						echo '<th>KNR</th>';
						echo '<th>PAY_TYPE</th>';
						echo '<th>PAY_TYPE_NAME</th>';
						echo '<th>OSTAT</th>';
						echo '<th>OSTAT_NAME</th>';
						echo '<th>PAKETNUMMERN</th>';
						echo '</tr>';
						*/

						$countRow = 0;
						while($executeDS = mysqli_fetch_assoc($executeRS)){

							if($countRow == 0){
								echo '<tr>';
								echo '<th>#</th>';
								echo '<th>' . implode("</th><th>", array_keys($executeDS)) . '</th>';
								echo '</tr>';
							}

							$executeDS["deliveryNumbers"] = preg_replace("/;/", '<br />', $executeDS["deliveryNumbers"]);
							if($countRow%2 == 0){ $rowClass = 'row1'; }
							else { $rowClass = 'row4'; }

							echo '<tr class="' . $rowClass . '">';

							echo '<td style="text-align:right;">';
							echo '<b>' . ($countRow + 1) . '.</b>';
							echo '</td>';

							echo '<td>' . implode("</td><td>", $executeDS) . '</td>';
							echo '</tr>';

							$countRow++;
						}
						echo '</table>';
						$executeSQL = "";
					}

					if($_POST["selectAction"] == "ACTION_FIND_ORDERS_IN_DOCUMENTS_OF_ALL_MANDATORIES"){

						// BOF TEST WITH TEMPLATE
						/*
						$sql_template_Fields = "
								`{###MANDATOR###}_DocumentDatas`.`orderID` AS `{###MANDATOR###}_orderID`,
								`{###MANDATOR###}`.`_DocumentDatas` AS `{###MANDATOR###}_orderDocumentsNumber`,
							";

						$arrHandleThisFindOrdersInDocumentsOfAllMandatories = explode(";", $handleThisFindOrdersInDocumentsOfAllMandatories);
						$arrSql = array();
						$arrSqlFields = array();

						foreach($arrHandleThisFindOrdersInDocumentsOfAllMandatories as $arrHandleThisFindOrdersInDocumentsOfAllMandatories){
							$tempVar_Fields = $sql_template_Fields;
							$tempVar_Fields = preg_replace("/{###MANDATOR###}/", strToUpper($arrHandleThisFindOrdersInDocumentsOfAllMandatories), $tempVar_Fields);
							$tempVar_Fields = preg_replace("/(`" . MANDATOR . "_)/", "`" . strToLower($arrHandleThisFindOrdersInDocumentsOfAllMandatories) . "_", $tempVar_Fields);
							$arrSqlFields[] = $tempVar_Fields;
						}

						$sqlFields = implode("", $arrSqlFields) . " `" . TABLE_ORDERS . "`.`ordersKundennummer` ";

						dd('sqlFields');
						exit;
						*/
						// EOF TEST WITH TEMPLATE

						/*
						BCTR:
							AB-1408003844
							LS-1408000753

						OID:editID=23861

						B3:
							LS-1408000153
							AB-1408001455

						Betr. 70,21
						KNR:13082
						*/
						// #####################################################################################################################
						$sql = "
								SELECT
									`b3_documentsOrderIds`.`orderID` AS `B3_orderID`,
									`bctr_documentsOrderIds`.`orderID` AS `BCTR_orderID`,

									`b3_documentsOrderIds`.`orderDocumentsNumber` AS `B3_orderDocumentsNumber`,
									`bctr_documentsOrderIds`.`orderDocumentsNumber` AS `BCTR_orderDocumentsNumber`,

									`" . TABLE_ORDERS . "`.`ordersKundennummer`
									FROM (
										SELECT
											`tempTable`.`orderDocumentDetailOrderID` AS `orderID`,
											GROUP_CONCAT(`tempTable`.`orderDocumentsNumber` ORDER BY `orderDocumentsNumber`) AS `orderDocumentsNumber`
											FROM (
												/* BOF ORDER_CONFIRMATIONS */
													SELECT `orderDocumentDetailOrderID`, `orderDocumentsNumber` FROM `b3_orderConfirmationsDetails`
													LEFT JOIN `b3_orderConfirmations` ON(`b3_orderConfirmationsDetails`.`orderDocumentDetailDocumentID` = `b3_orderConfirmations`.`orderDocumentsID`)
												/* EOF ORDER_CONFIRMATIONS */
												UNION

												/* BOF ORDER_INVOICES */
													SELECT `orderDocumentDetailOrderID`, `orderDocumentsNumber` FROM `b3_orderInvoicesDetails`
													LEFT JOIN `b3_orderInvoices` ON(`b3_orderInvoicesDetails`.`orderDocumentDetailDocumentID` = `b3_orderInvoices`.`orderDocumentsID`)
												/* EOF ORDER_INVOICES */
												UNION

												/* BOF ORDER_DELIVERIES */
													SELECT `orderDocumentDetailOrderID`, `orderDocumentsNumber` FROM `b3_orderDeliveriesDetails`
													LEFT JOIN `b3_orderDeliveries` ON(`b3_orderDeliveriesDetails`.`orderDocumentDetailDocumentID` = `b3_orderDeliveries`.`orderDocumentsID`)
												/* EOF ORDER_DELIVERIES */
												UNION

												/* BOF ORDER_CREDITS */
													SELECT `orderDocumentDetailOrderID`, `orderDocumentsNumber` FROM `b3_orderCreditsDetails`
													LEFT JOIN `b3_orderCredits` ON(`b3_orderCreditsDetails`.`orderDocumentDetailDocumentID` = `b3_orderCredits`.`orderDocumentsID`)
												/* EOF ORDER_CREDITS */
												UNION

												/* BOF ORDER_REMINDERS */
													SELECT `orderDocumentDetailOrderID`, `orderDocumentsNumber` FROM `b3_orderRemindersDetails`
													LEFT JOIN `b3_orderReminders` ON(`b3_orderRemindersDetails`.`orderDocumentDetailDocumentID` = `b3_orderReminders`.`orderDocumentsID`)
												/* EOF ORDER_REMINDERS */
												UNION

												/* BOF ORDER_FIRST_DEMANDS */
													SELECT `orderDocumentDetailOrderID`, `orderDocumentsNumber` FROM `b3_orderFirstDemandsDetails`
													LEFT JOIN `b3_orderFirstDemands` ON(`b3_orderFirstDemandsDetails`.`orderDocumentDetailDocumentID` = `b3_orderFirstDemands`.`orderDocumentsID`)
												/* EOF ORDER_FIRST_DEMANDS */
												UNION

												/* BOF ORDER_SECOND_DEMANDS */
													SELECT `orderDocumentDetailOrderID`, `orderDocumentsNumber` FROM `b3_orderSecondDemandsDetails`
													LEFT JOIN `b3_orderSecondDemands` ON(`b3_orderSecondDemandsDetails`.`orderDocumentDetailDocumentID` = `b3_orderSecondDemands`.`orderDocumentsID`)
												/* EOF ORDER_SECOND_DEMANDS */
												UNION

												/* BOF ORDER_THIRD_DEMANDS */
													SELECT `orderDocumentDetailOrderID`, `orderDocumentsNumber` FROM `b3_orderThirdDemandsDetails`
													LEFT JOIN `b3_orderThirdDemands` ON(`b3_orderThirdDemandsDetails`.`orderDocumentDetailDocumentID` = `b3_orderThirdDemands`.`orderDocumentsID`)
												/* EOF ORDER_THIRD_DEMANDS */
											) AS `tempTable`
										GROUP BY `tempTable`.`orderDocumentDetailOrderID`
									) AS `b3_documentsOrderIds`

									INNER JOIN (
										SELECT
											`tempTable`.`orderDocumentDetailOrderID` AS `orderID`,
											GROUP_CONCAT(`tempTable`.`orderDocumentsNumber` ORDER BY `orderDocumentsNumber`) AS `orderDocumentsNumber`
											FROM (
												/* BOF ORDER_CONFIRMATIONS */
													SELECT `orderDocumentDetailOrderID`, `orderDocumentsNumber` FROM `bctr_orderConfirmationsDetails`
													LEFT JOIN `bctr_orderConfirmations` ON(`bctr_orderConfirmationsDetails`.`orderDocumentDetailDocumentID` = `bctr_orderConfirmations`.`orderDocumentsID`)
												/* EOF ORDER_CONFIRMATIONS */
												UNION

												/* BOF ORDER_INVOICES */
													SELECT `orderDocumentDetailOrderID`, `orderDocumentsNumber` FROM `" . TABLE_ORDER_INVOICES_DETAILS . "`
													LEFT JOIN `" . TABLE_ORDER_INVOICES . "` ON(`" . TABLE_ORDER_INVOICES_DETAILS . "`.`orderDocumentDetailDocumentID` = `" . TABLE_ORDER_INVOICES . "`.`orderDocumentsID`)
												/* EOF ORDER_INVOICES */
												UNION

												/* BOF ORDER_DELIVERIES */
													SELECT `orderDocumentDetailOrderID`, `orderDocumentsNumber` FROM `bctr_orderDeliveriesDetails`
													LEFT JOIN `bctr_orderDeliveries` ON(`bctr_orderDeliveriesDetails`.`orderDocumentDetailDocumentID` = `bctr_orderDeliveries`.`orderDocumentsID`)
												/* EOF ORDER_DELIVERIES */
												UNION

												/* BOF ORDER_CREDITS */
													SELECT `orderDocumentDetailOrderID`, `orderDocumentsNumber` FROM `bctr_orderCreditsDetails`
													LEFT JOIN `bctr_orderCredits` ON(`bctr_orderCreditsDetails`.`orderDocumentDetailDocumentID` = `bctr_orderCredits`.`orderDocumentsID`)
												/* EOF ORDER_CREDITS */
												UNION

												/* BOF ORDER_REMINDERS */
													SELECT `orderDocumentDetailOrderID`, `orderDocumentsNumber` FROM `bctr_orderRemindersDetails`
													LEFT JOIN `bctr_orderReminders` ON(`bctr_orderRemindersDetails`.`orderDocumentDetailDocumentID` = `bctr_orderReminders`.`orderDocumentsID`)
												/* EOF ORDER_REMINDERS */
												UNION

												/* BOF ORDER_FIRST_DEMANDS */
													SELECT `orderDocumentDetailOrderID`, `orderDocumentsNumber` FROM `bctr_orderFirstDemandsDetails`
													LEFT JOIN `bctr_orderFirstDemands` ON(`bctr_orderFirstDemandsDetails`.`orderDocumentDetailDocumentID` = `bctr_orderFirstDemands`.`orderDocumentsID`)
												/* EOF ORDER_FIRST_DEMANDS */
												UNION

												/* BOF ORDER_SECOND_DEMANDS */
													SELECT `orderDocumentDetailOrderID`, `orderDocumentsNumber` FROM `bctr_orderSecondDemandsDetails`
													LEFT JOIN `bctr_orderSecondDemands` ON(`bctr_orderSecondDemandsDetails`.`orderDocumentDetailDocumentID` = `bctr_orderSecondDemands`.`orderDocumentsID`)
												/* EOF ORDER_SECOND_DEMANDS */
												UNION

												/* BOF ORDER_THIRD_DEMANDS */
													SELECT `orderDocumentDetailOrderID`, `orderDocumentsNumber` FROM `bctr_orderThirdDemandsDetails`
													LEFT JOIN `bctr_orderThirdDemands` ON(`bctr_orderThirdDemandsDetails`.`orderDocumentDetailDocumentID` = `bctr_orderThirdDemands`.`orderDocumentsID`)
												/* EOF ORDER_THIRD_DEMANDS */
											) AS `tempTable`
										GROUP BY `tempTable`.`orderDocumentDetailOrderID`
									) AS `bctr_documentsOrderIds`
									ON(`b3_documentsOrderIds`.`orderID` = `bctr_documentsOrderIds`.`orderID`)

									LEFT JOIN `" . TABLE_ORDERS . "`
									ON(`b3_documentsOrderIds`.`orderID` = `" . TABLE_ORDERS . "`.`ordersID`)

									HAVING
										`" . TABLE_ORDERS . "`.`ordersKundennummer` IS NOT NULL
										AND
										`" . TABLE_ORDERS . "`.`ordersKundennummer` != '00000'
							";
						// #################################################################
						echo '<div class="debugVarsArea" style="border:2px solid #FF0000; background-color:#FFFFFF;">';
						echo '<pre>';
						echo $sql;
						echo '</pre>';
						echo '</div>';

						echo '<hr />';

						$executeSQL = $sql;
						$executeRS = $dbConnection->db_query($executeSQL);

						echo '<table border="0" cellpadding="0" cellspacing="0" style="width:99%;" class="displayOrders">';
							echo '<tr>';
								echo '<th style="width:45px;">#</th>';
								echo '<th style="width:70px;">B3 orderID</th>';
								echo '<th style="width:70px;">BCTR orderID</th>';
								echo '<th>B3 DOC-NR</th>';
								echo '<th>BCTR DOC-NR</th>';
								echo '<th style="width:70px;">order KNR</th>';
							echo '</tr>';

						$countRow = 0;
						while($executeDS = mysqli_fetch_assoc($executeRS)){
							if($countRow%2 == 0){ $rowClass = 'row1'; }
							else { $rowClass = 'row4'; }

							echo '<tr class="' . $rowClass . '">';

							echo '<td style="text-align:right;">';
							echo '<b>' . ($countRow + 1) . '.</b>';
							echo '</td>';

							echo '<td>';
							echo $executeDS["B3_orderID"];
							echo '</td>';

							echo '<td>';
							echo $executeDS["BCTR_orderID"];
							echo '</td>';

							echo '<td>';
							echo preg_replace("/,/", ", " ,$executeDS["B3_orderDocumentsNumber"]);
							echo '</td>';

							echo '<td>';
							echo preg_replace("/,/", ", " ,$executeDS["BCTR_orderDocumentsNumber"]);
							echo '</td>';

							echo '<td>';
							echo $executeDS["ordersKundennummer"];
							echo '</td>';

							echo '</tr>';

							$countRow++;
						}
						echo '</table>';
						$executeSQL = "";

						echo '</div>';
					}
				?>

				<?php displayMessages(); ?>
			</div>
		</div>
		<div class="clear"></div>
	</div>
	<div class="clear"></div>
</div>

<script language="javascript" type="text/javascript">
	<!--
	/* <![CDATA[ */
	$(document).ready(function() {
		$(function() {
			$("#datepicker").datepicker();
			$.datepicker.setDefaults($.datepicker.regional[""]);
			$('#handleThisLayoutDate').datepicker($.datepicker.regional["de"]);
			$('#handleThisLayoutDate').datepicker("option", "maxDate", "0" );

			var htmlButtonClearField = '<span class="buttonClearField"><img src="layout/icons/iconDelete.png" width="16" height="16" alt="" title="Eingabefeld leeren" \/><\/span>';
			$('#handleThisLayoutDate').parent().append(htmlButtonClearField);
			$('.buttonClearField').click(function () {
				$(this).parent().find('input').val('');
				$(this).parent().find('input').attr('checked', false);
			});
		});
		colorRowMouseOver('.displayOrders tbody tr');

		// BOF AJAX EXECUTE SQL ORDER
			function executeSqlOrder(sqlString, triggerElement) {
				var returnResult;
				var result;
				var getResult;
				var animateTime = 0;
				$(triggerElement).next('.sqlOrder').parent().append('<span class="resultExecuteArea"><\/span>');
				$(triggerElement).next('.sqlOrder').parent().find('.resultExecuteArea').html('<img class="loader" src="layout/ajax-loader.gif" height="10" width="10" \/>');

				try {
					if(sqlString != '') {
						var loadURL = 'inc/ajaxExecuteSql.inc.php';
						getResult = $.get(loadURL + '?sqlString=' + encodeURIComponent(sqlString), function(result) {
							sqlResult = result;
							// alert( "success:" + result);
						})
						.done(function(result) {
							// alert( "second success:" + result);
							if(triggerElement){
								var thisTextColor = '';
								var thisMessage = '';

								if(result == 1){
									thisTextColor = '#009900';
									thisBackgroundColor = '#CBFFBF';
									thisMessage = ' <b>OK<\/b> ';
								}
								else {
									thisTextColor = '#990000';
									thisBackgroundColor = '#FFEADF';
									thisMessage = ' <b>FEHLER<\/b>';
								}
								$(triggerElement).next('.sqlOrder').css('color', thisTextColor);
								// $(triggerElement).next('.sqlOrder').parent().append(thisMessage);
								$(triggerElement).next('.sqlOrder').parent().find('.resultExecuteArea').html(thisMessage);
								$(triggerElement).next('.sqlOrder').parent().css('background-color', thisBackgroundColor);

								$(triggerElement).parent().attr('id', 'sqlAreaDone');
								$(triggerElement).remove();

								var scroll_y = 30;
								// var getRowHeight = $('#sqlAreaDone').height();
								// var getRowHeight = $('#sqlAreaDone').innerHeight();
								var getRowHeight = $('#sqlAreaDone').outerHeight();

								scroll_y = getRowHeight;
								window.scrollBy(0, scroll_y);
								$('#sqlAreaDone').removeAttr('id');

								startExecuteCheckedSql();
								return returnResult;
							}
						})
						.fail(function(result) {
							// alert( "error:" + result);
						})
						.always(function(result) {
							// alert( "finished" );
						});
					}
				}
				catch(err) { handleJsError(err); return false; }
				finally {}
			}
			$('.executeSqlOrders').live('click', function(){
				var thisSqlChecked = $(this).attr('checked');
				var thisSqlOrder = "";
				var thisExecuteSqlOrderResult = false;
				if(thisSqlChecked == "checked"){
					thisSqlOrder = $(this).next('.sqlOrder').text();
					//alert(thisSqlOrder);
					if(thisSqlOrder != ""){
						// thisExecuteSqlOrderResult = executeSqlOrder(thisSqlOrder, $(this));
					}
				}
				countSelectedItemsSqlOrders();
			});

			function countSelectedItemsSqlOrders(){
				var countExecuteSqlOrdersAll = 0;
				var countExecuteSqlOrdersSelected = 0;
				$('.executeSqlOrders').each(function(index) {
					countExecuteSqlOrdersAll++;
					if($(this).attr('checked') == 'checked'){
						countExecuteSqlOrdersSelected++;
					}

				});
				$('.displayCountSelectedItemsSqlOrders').html(countExecuteSqlOrdersSelected + ' von ' + countExecuteSqlOrdersAll);
			}
			function countSelectedItemsFiles(){
				var countFilesAll = 0;
				var countFilesAllSelected = 0;
				$('.checkboxReReadLocalFiles').each(function(index) {
					countFilesAll++;
					if($(this).attr('checked') == 'checked'){
						countFilesAllSelected++;
					}

				});
				$('.displayCountSelectedItemsFiles').html('<b>' + countFilesAllSelected + '<\/b>' + ' von ' + '<b>' + countFilesAll + '<\/b>');
			}

			function showExecuteSqlStatusWindow(countAllElements, countDoneElements, countExecutesOK, countExecutesERRORS, mode){
				var content;
				var contentHeader = 'SQL-Status';
				// var contentText = (countAllElements - countDoneElements) + ' von ' + countProcessElements + ' SQL-Abfragen abgearbeitet ...';
				var contentText = (countAllElements - countDoneElements) + ' von ' + countAllElements + ' SQL-Abfragen abgearbeitet ...';
				// alert('countAllElements: ' + countAllElements + ' | ' + 'countDoneElements: ' + countDoneElements + ' | ' + (countAllElements - countDoneElements));
				contentText += '<hr \/>';
				contentText += '<b>OK:<\/b> ' + countExecutesOK;
				contentText += '<br \/>';
				contentText += '<b>ERRORS:<\/b> ' + countExecutesERRORS;

				contentText += '<hr \/>';
				if(mode == 1){
					contentText += '<span class="loadingArea"><img class="loader" src="layout/ajax-loader.gif" \/> <b>SQL-Abfragen aktiv<\/b>';
				}
				else {
					if(countExecutesERRORS == 0){
						contentText += '<span class="loadingArea"><img class="loader" src="layout/icons/iconOk.png" \/> <b>SQL-Abfragen beendet<\/b>';
					}
					else{
						contentText += '<span class="loadingArea"><img class="loader" src="layout/icons/iconNotOk.png" \/> <b>SQL-Abfragen beendet<\/b>';
					}
				}

				content = '<div class="noticeBoxHeader">' + contentHeader + '<\/div>';
				content += '<div class="noticeBoxContents">' + contentText + '<\/div>';
				content += '<div class="noticeBoxFooter">' + '' + '<\/div>';
				// content += '<div class="noticeBoxClose">' + '<img src="layout/icons/iconClose.png" class="iconClose" width="14" height="14" alt="" title="Liste schlie&szlig;en" \/>' + '<\/div>';
				var arrWindowButtons = loadWindowButtons();
				content += '<div class="noticeBoxClose">' + arrWindowButtons['small'] + ' ' + arrWindowButtons['move'] + ' ' + arrWindowButtons['close'] + '<\/div>';
				content = '<div class="loadNoticeContent">' + content + '<\/div>';
				$('body').append('<div id="loadNoticeArea" class="loadedWindow"><\/div>');
				$('#loadNoticeArea').html(content);
				// actionWindowButtons();
			}

			var isExecuteSqlError = false;
			function startExecuteCheckedSql(){
				var thisElement = $('.executeSqlOrders:checked').first();
				var countCheckedElements = $('.executeSqlOrders:checked').length;
				var countDoneElements = $('.executeSqlOrders').length;
				var countAllElements = $('.sqlArea').length;
				var countProcessElements = countCheckedElements + countDoneElements;
				var countTodoElements = countCheckedElements + countDoneElements;

				var countExecutesOK = $('span.resultExecuteArea:contains("OK")').length;
				var countExecutesERRORS = $('span.resultExecuteArea:contains("FEHLER")').length;
				// $('#loadNoticeArea').remove();

				if(thisElement && thisElement.next('.sqlOrder').text() != ""){
					$('#loadNoticeArea').remove();
					$(thisElement).css('background-color', '#FF0000');
					var thisSqlOrder = thisElement.next('.sqlOrder').text();
					if(thisSqlOrder != ""){
						thisExecuteSqlOrderResult = executeSqlOrder(thisSqlOrder, $(thisElement));
						showExecuteSqlStatusWindow(countAllElements, countDoneElements, countExecutesOK, countExecutesERRORS, 1);
					}
				}
				else {
					$('#loadNoticeArea').remove();
					showExecuteSqlStatusWindow(countAllElements, countDoneElements, countExecutesOK, countExecutesERRORS, 0);
					alert('Keine auszuführenden SQL-Befehle vorhanden!\nDie Bearbeitung wird beendet.');
				}
				$('.noticeBoxClose .iconClose').click(function(){
					$('#loadNoticeArea').fadeOut(animateTime, function(){
						$('#loadNoticeArea').remove()
					});
				});
			};

			$('.sqlArea').live('mouseenter', function(){
				var getBgColor = $(this).css('background-color');
				$(this).css({
					'background-color': '#FFD86F',
					'cursor': 'pointer'
				});
				$(this).live('mouseleave', function(){
					$(this).css({
						'background-color': getBgColor,
						'cursor': ''
					});
				});
				$(this).live('click', function(){
					$(this).css({
						'background-color': '#9ff8ff',
						'cursor': ''
					});
				});
			});

			countSelectedItemsSqlOrders();
			$('.buttonExecuteSqlOrders').live('click', function(){
				startExecuteCheckedSql();
				countSelectedItemsSqlOrders();
			});
			$('.buttonCheckAllCheckboxesSqlOrders').live('click', function(){
				$('.executeSqlOrders').attr('checked', 'true');
				countSelectedItemsSqlOrders();
			});
			$('.buttonUncheckAllCheckboxesSqlOrders').live('click', function(){
				$('.executeSqlOrders').removeAttr('checked');
				countSelectedItemsSqlOrders();
			});
		// EOF AJAX EXECUTE SQL ORDER

		// BOF CHECK REREAD FILES
			countSelectedItemsFiles();
			$('.buttonCheckAllCheckboxesFiles').live('click', function(){
				$('.checkboxReReadLocalFiles').attr('checked', 'true');
				countSelectedItemsFiles();
			});
			$('.buttonUncheckAllCheckboxesFiles').live('click', function(){
				$('.checkboxReReadLocalFiles').removeAttr('checked');
				countSelectedItemsFiles();
			});

			$('.checkboxReReadLocalFiles').live('click', function(){
				countSelectedItemsFiles();
			});
		// EOF CHECK REREAD FILES

		// BOF ADD TOOGLE BUTTON
			var buttonToggle = '<img src="layout/icons/iconToggle3.png" class="buttonToggleDetails" width="14" height="14" alt="" title="Details ein-/ausblenden" style="float:right;" />';
			$('.detailsArea h3').append(buttonToggle);

			$('.buttonToggleDetails').live('click', function(){
				$(this).parent().next('.displayDetails').toggle();
			});
		// EOF ADD TOOGLE BUTTON

		// BOF SELECTED ACTION CSS
			function setSelectedActionClass(){
				var selectedActionClass = $('#selectAction option:selected').attr('class');
				var selectActionClass = $('#selectAction').attr('class');
				$('#selectAction').attr('class', selectActionClass + ' ' + selectedActionClass);
			}
			setSelectedActionClass();
			$('#selectAction').live('change', function(){
				setSelectedActionClass();
			});
		// EOF SELECTED ACTION CSS

		// BOF CHANGE DOCUMENT PAYMENT STATUS FORM
			$('.buttonChangeDocumentPaymentStatus').live('click', function(){
				$(this).next('.thisFormChangeDocumentPaymentStatusArea').toggle();
			});
			$('.selectStatus').live('change', function(){
				var thisNewDocumentPaymentStatus = $(this).val();
				var thisDocumentPaymentStatusChangeSqlArea = $(this).parent().parent().find('.thisDocumentPaymentStatusChangeSqlArea');
				var thisDocumentPaymentStatusChangeDocumentType = thisDocumentPaymentStatusChangeSqlArea.attr('rel');
				var thisDocumentPaymentStatusChangeSql = thisDocumentPaymentStatusChangeSqlArea.text();
				// thisDocumentPaymentStatusChangeSql = thisDocumentPaymentStatusChangeSql.replace("{###NEW_DOCUMENT_PAYMENT_STATUS###}", thisNewDocumentPaymentStatus);
				thisDocumentPaymentStatusChangeSql = thisDocumentPaymentStatusChangeSql.replace(/`orderDocumentsStatus` = '.*'/, "`orderDocumentsStatus` = '" + thisNewDocumentPaymentStatus + "'");
				thisDocumentPaymentStatusChangeSql = '' + thisDocumentPaymentStatusChangeSql + '';

				thisDocumentPaymentStatusChangeSqlArea.text(thisDocumentPaymentStatusChangeSql);

				var thisSqlArea = $(this).parent().parent().parent().parent().next('.sqlArea');
				$(thisSqlArea).find('.sqlArea_' + thisDocumentPaymentStatusChangeDocumentType).html('<span class="sqlOrder">' + thisDocumentPaymentStatusChangeSql + '<\/span>');
				$(thisSqlArea).show();
				$(thisSqlArea).find('.sqlArea_' + thisDocumentPaymentStatusChangeDocumentType).show();
				$('.thisFormChangeDocumentPaymentStatusArea').hide();
			});
			$('.buttonCloseDocumentPaymentStatusChangeSqlArea').live('click', function(){
				$('.thisFormChangeDocumentPaymentStatusArea').hide();
			});
		// EOF CHANGE DOCUMENT PAYMENT STATUS FORM
	});
	/* ]]> */
	// -->
</script>
</div>

<?php require_once('inc/footerHTML.inc.php'); ?>