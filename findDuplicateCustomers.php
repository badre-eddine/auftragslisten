<?php
	require_once('inc/requires.inc.php');

	if(!$arrGetUserRights["reviseDocumentDatas"]) {
		header('location: ' . PAGE_EXIT_LOCATION);
		exit;
	}

	if(!isset($_REQUEST["searchCustomerStatus"])){
		if($_REQUEST["searchCustomerStatus"] == ''){
			$_REQUEST["searchCustomerStatus"] = '-1';
		}
	}

	if($_REQUEST["searchCustomerLimit"] == '' || $_REQUEST["searchCustomerLimit"] < 1){
		$_REQUEST["searchCustomerLimit"] = 10;
	}

	$jswindowMessage = '';
	$warningMessage = '';
	$errorMessage = '';
	$successMessage = '';
	$infoMessage = '';

	$dbConnection = new DB_Connection();
	$db_open = $dbConnection->db_connect();

	$defaultDateYear = date('Y');
	$defaultDateMonth = '';

	$sql_flush = "FLUSH QUERY CACHE";
	$rs_flush = $dbConnection->db_query($sql_flush);

	if($_REQUEST["storeDatas"] != ""){
		if(!empty($_REQUEST["isDuplicateCustomer"])){
			foreach(array_keys($_REQUEST["isDuplicateCustomer"]) as $thisKey){
				$sql = "
					REPLACE
						INTO `" . TABLE_CUSTOMER_DUPLICATES . "` (
							`customerDuplicatesCustomerIDs`,
							`customerDuplicatesCustomerNumbers`,
							`customerDuplicatesIsDuplicate`,
							`customerDuplicatesPossibleDeleteCustomerNumber`,
							`customerDuplicatesPossibleDeleteCustomerID`,
							`customerDuplicatesPossibleUseCustomerNumber`,
							`customerDuplicatesPossibleUseCustomerID`
						)
						VALUES (
							'" . $_REQUEST["isDuplicateCustomerDataCustomerID"][$thisKey] . "',
							'" . $_REQUEST["isDuplicateCustomerDataCustomerNumber"][$thisKey] . "',
							'" . $_REQUEST["isDuplicateCustomer"][$thisKey] . "',
							'" . $_REQUEST["customerPossibleToDeleteCustomerNumber"][$thisKey] . "',
							'" . $_REQUEST["customerPossibleToDeleteCustomerID"][$thisKey] . "',
							'" . $_REQUEST["customerPossibleToUseCustomerNumber"][$thisKey] . "',
							'" . $_REQUEST["customerPossibleToUseCustomerID"][$thisKey] . "'
						)
					";
				#dd('sql');
				$rs = $dbConnection->db_query($sql);
				if($rs){
					$successMessage .= 'KNR: ' . $_REQUEST["customerPossibleToDeleteCustomerNumber"][$thisKey] . ' (CID ' . $_REQUEST["customerPossibleToDeleteCustomerID"][$thisKey] . ') wurde mit Duplikat-Status ' . $_REQUEST["isDuplicateCustomer"][$thisKey] . ' gespeichert!' . '<br />';
				}
				else {
					$errorMessage .= 'KNR: ' . $_REQUEST["customerPossibleToDeleteCustomerNumber"][$thisKey] . ' (CID ' . $_REQUEST["customerPossibleToDeleteCustomerID"][$thisKey] . ') konnte nicht mit Duplikat-Status ' . $_REQUEST["isDuplicateCustomer"][$thisKey] . ' gespeichert werden!' . '<br />';
				}
			}
		}
	}
?>
<?php
	require_once('inc/headerHTML.inc.php');
	$thisTitle = "Doppelte Kunden finden";

	$headerHTML = preg_replace("/{###TITLE###}/", strip_tags($thisTitle), $headerHTML);
	echo $headerHTML;

	require_once(FILE_MENUE_TOP);
?>

<div id="mainArea">
	<div id="mainContent">
		<?php require_once(FILE_MENUE_SIDEBAR); ?>
		<div id="contentArea">
			<a name="top"></a>
			<div id="contentAreaElements">
				<h1><?php if(SHOW_PAGE_TITLE_ICONS) { echo '<img src="' . PATH_ICONS_MENUE_TITLES . 'duplicates.png" alt="" />'; } ?> <?php echo $thisTitle; ?></h1>

				<div class="contentDisplay">
					<?php displayMessages(); ?>
					<div id="searchFilterArea">
						<form name="formFilterSearch" method="get" action="<?php echo $_SERVER["PHP_SELF"]; ?>">
							<table border="0" cellpadding="0" cellspacing="0" width="" class="searchFilterContent">
								<tr>
									<!--
									<td>
										<label for="searchCustomerNumber">Kundennr:</label>
										<input type="text" name="searchCustomerNumber" id="searchCustomerNumber" class="inputField_40" value="" />
									</td>
									-->
									<td>
										<label for="searchCustomerStatus">Pr&uuml;f-Status:</label>
										<select name="searchCustomerStatus" id="searchCustomerStatus" class="inputField_120">
											<option value="">Alle</option>
											<option value="-1" <?php if($_REQUEST["searchCustomerStatus"] == '-1'){ echo ' selected="selected" '; } ?> >ungepr&uuml;ft</option>
											<option value="0" <?php if($_REQUEST["searchCustomerStatus"] == '0'){ echo ' selected="selected" '; } ?> >Keine Duplikate</option>
											<option value="1" <?php if($_REQUEST["searchCustomerStatus"] == '1'){ echo ' selected="selected" '; } ?> >Duplikate</option>
										</select>
									</td>
									<td>
										<label for="searchCustomerLimit">Limit:</label>
										<input type="text" name="searchCustomerLimit" id="searchCustomerLimit" class="inputField_40" value="<?php echo $_REQUEST["searchCustomerLimit"]; ?>" />
									</td>
									<td>
										<input type="submit" name="submitSearch" class="inputButton0" value="Suchen" />
									</td>
								</tr>
							</table>
						</form>
					</div>

					<?php
						// ############################################################################

						$whereStatusDuplicate = "";
						if($_REQUEST["searchCustomerStatus"] == "0" || $_REQUEST["searchCustomerStatus"] == "1"){
							$whereStatusDuplicate .= " AND `" . TABLE_CUSTOMER_DUPLICATES . "`.`customerDuplicatesIsDuplicate` = '" . $_REQUEST["searchCustomerStatus"] . "' ";
						}
						else if($_REQUEST["searchCustomerStatus"] == "-1"){
							$whereStatusDuplicate .= "
										AND (
											`" . TABLE_CUSTOMER_DUPLICATES . "`.`customerDuplicatesIsDuplicate` = '" . $_REQUEST["searchCustomerStatus"] . "'
											OR
											`" . TABLE_CUSTOMER_DUPLICATES . "`.`customerDuplicatesIsDuplicate` IS NULL
										)
								";
						}

						$sql_2 = "
							SELECT

								`c1_KNR`,
								`c2_KNR`,
								`KNRs`,

								`c1_CID`,
								`c2_CID`,

								/*
								GROUP_CONCAT(DISTINCT `c1_OID`) AS `c1_OID`,
								GROUP_CONCAT(DISTINCT `c2_OID`) AS `c2_OID`,
								*/

								`c1_OKNR`,
								`c2_OKNR`,

								`c1_GP`,
								`c2_GP`,

								`c1_FIRMA`,
								`c2_FIRMA`,
								`c1_ADR`,
								`c2_ADR`,
								`c1_PLZ`,
								`c2_PLZ`,
								`c1_ORT`,
								`c2_ORT`,

								/*
								`c1_NameSound`,
								`c2_NameSound`,
								*/

								/*
								GROUP_CONCAT(DISTINCT `c1_b3_DOCNR`) AS `c1_b3_DOCNR`,
								GROUP_CONCAT(DISTINCT `c2_b3_DOCNR`) AS `c2_b3_DOCNR`,
								GROUP_CONCAT(DISTINCT `c1_bctr_DOCNR`) AS `c1_bctr_DOCNR`,
								GROUP_CONCAT(DISTINCT `c2_bctr_DOCNR`) AS `c2_bctr_DOCNR`,
								*/


								`c1_b3_DOCKNR`,
								`c2_b3_DOCKNR`,
								`c1_bctr_DOCKNR`,
								`c2_bctr_DOCKNR`,

								/*
								GROUP_CONCAT(DISTINCT CONCAT(`c1_b3_DOCKNR`, `c1_bctr_DOCKNR`)) AS `c1_DOCKNR`,
								GROUP_CONCAT(DISTINCT CONCAT(`c2_b3_DOCKNR`, `c2_bctr_DOCKNR`)) AS `c2_DOCKNR`,
								*/

								/*
								IF(`" . TABLE_CUSTOMER_DUPLICATES . "`.`customerDuplicatesIsDuplicate` IS NULL || `" . TABLE_CUSTOMER_DUPLICATES . "`.`customerDuplicatesIsDuplicate` = '', '-1', `" . TABLE_CUSTOMER_DUPLICATES . "`.`customerDuplicatesIsDuplicate`) AS `isDuplicate`
								*/
								`isDuplicate`
								/*
								`" . TABLE_CUSTOMER_DUPLICATES . "`.`customerDuplicatesCustomerIDs`,
								`" . TABLE_CUSTOMER_DUPLICATES . "`.`customerDuplicatesCustomerNumbers`,
								`" . TABLE_CUSTOMER_DUPLICATES . "`.`customerDuplicatesPossibleDeleteCustomerNumber`,
								`" . TABLE_CUSTOMER_DUPLICATES . "`.`customerDuplicatesPossibleDeleteCustomerID`,
								*/

								FROM (

									SELECT
										`c1_KNR`,
										`c2_KNR`,
										`KNRs`,

										`c1_CID`,
										`c2_CID`,

										IF(`orders_1`.`ordersID` IS NOT NULL, `orders_1`.`ordersID`, '') AS `c1_OID`,
										IF(`orders_2`.`ordersID` IS NOT NULL, `orders_2`.`ordersID`, '') AS `c2_OID`,

										IF(`orders_1`.`ordersID` IS NOT NULL AND `orders_1`.`ordersID` != '', `c1_KNR`, '') AS `c1_OKNR`,
										IF(`orders_2`.`ordersID` IS NOT NULL AND `orders_2`.`ordersID` != '', `c2_KNR`, '') AS `c2_OKNR`,

										`c1_GP`,
										`c2_GP`,

										`c1_FIRMA`,
										`c2_FIRMA`,

										`c1_ADR`,
										`c2_ADR`,
										`c1_PLZ`,
										`c2_PLZ`,
										`c1_ORT`,
										`c2_ORT`,


										`c1_NameSound`,
										`c2_NameSound`,


										/*
										IF(`b3_Docs_1`.`createdDocumentsNumber` IS NOT NULL, `b3_Docs_1`.`createdDocumentsNumber`, '') AS `c1_b3_DOCNR`,
										IF(`b3_Docs_2`.`createdDocumentsNumber` IS NOT NULL, `b3_Docs_2`.`createdDocumentsNumber`, '') AS `c2_b3_DOCNR`,

										IF(`bctr_Docs_1`.`createdDocumentsNumber` IS NOT NULL, `bctr_Docs_1`.`createdDocumentsNumber`, '') AS `c1_bctr_DOCNR`,
										IF(`bctr_Docs_2`.`createdDocumentsNumber` IS NOT NULL, `bctr_Docs_2`.`createdDocumentsNumber`, '') AS `c2_bctr_DOCNR`
										*/

										CONCAT(`c1_KNR`, ':', IF(`b3_Docs_1`.`createdDocumentsNumber` IS NOT NULL, `b3_Docs_1`.`createdDocumentsNumber`, '')) AS `c1_b3_DOCNR`,
										CONCAT(`c2_KNR`, ':', IF(`b3_Docs_2`.`createdDocumentsNumber` IS NOT NULL, `b3_Docs_2`.`createdDocumentsNumber`, '')) AS `c2_b3_DOCNR`,

										CONCAT(`c1_KNR`, ':', IF(`bctr_Docs_1`.`createdDocumentsNumber` IS NOT NULL, `bctr_Docs_1`.`createdDocumentsNumber`, '')) AS `c1_bctr_DOCNR`,
										CONCAT(`c2_KNR`, ':', IF(`bctr_Docs_2`.`createdDocumentsNumber` IS NOT NULL, `bctr_Docs_2`.`createdDocumentsNumber`, '')) AS `c2_bctr_DOCNR`,

										/*
										IF(`b3_Docs_1`.`createdDocumentsNumber` IS NOT NULL AND `b3_Docs_1`.`createdDocumentsNumber` != '', CONCAT(`c1_KNR`, ':', `b3_Docs_1`.`createdDocumentsNumber`), '') AS `c1_b3_DOCNR`,
										IF(`b3_Docs_2`.`createdDocumentsNumber` IS NOT NULL AND `b3_Docs_2`.`createdDocumentsNumber` != '', CONCAT(`c2_KNR`, ':', `b3_Docs_2`.`createdDocumentsNumber`), '') AS `c2_b3_DOCNR`,

										IF(`bctr_Docs_1`.`createdDocumentsNumber` IS NOT NULL AND `bctr_Docs_1`.`createdDocumentsNumber` != '', CONCAT(`c1_KNR`, ':', `bctr_Docs_1`.`createdDocumentsNumber`), '') AS `c1_bctr_DOCNR`,
										IF(`bctr_Docs_2`.`createdDocumentsNumber` IS NOT NULL AND `bctr_Docs_2`.`createdDocumentsNumber` != '', CONCAT(`c2_KNR`, ':', `bctr_Docs_2`.`createdDocumentsNumber`), '') AS `c2_bctr_DOCNR`
										*/

										IF(`b3_Docs_1`.`createdDocumentsNumber` IS NOT NULL AND `b3_Docs_1`.`createdDocumentsNumber` != '', `c1_KNR`, '') AS `c1_b3_DOCKNR`,
										IF(`b3_Docs_2`.`createdDocumentsNumber` IS NOT NULL AND `b3_Docs_2`.`createdDocumentsNumber` != '', `c2_KNR`, '') AS `c2_b3_DOCKNR`,

										IF(`bctr_Docs_1`.`createdDocumentsNumber` IS NOT NULL AND `bctr_Docs_1`.`createdDocumentsNumber` != '', `c1_KNR`, '') AS `c1_bctr_DOCKNR`,
										IF(`bctr_Docs_2`.`createdDocumentsNumber` IS NOT NULL AND `bctr_Docs_2`.`createdDocumentsNumber` != '', `c2_KNR`, '') AS `c2_bctr_DOCKNR`,

										`isDuplicate`

										FROM (
											SELECT

												`customers_1`.`customersID` AS `c1_CID`,
												`customers_2`.`customersID` AS `c2_CID`,

												`customers_1`.`customersKundennummer` AS `c1_KNR`,
												`customers_2`.`customersKundennummer` AS `c2_KNR`,

												IF(
													`customers_1`.`customersKundennummer` <= `customers_2`.`customersKundennummer`,
													CONCAT(`customers_1`.`customersKundennummer`, ':', `customers_2`.`customersKundennummer`),
													IF(
														`customers_1`.`customersKundennummer` > `customers_2`.`customersKundennummer`,
														CONCAT(`customers_2`.`customersKundennummer`, ':', `customers_1`.`customersKundennummer`),
														''
													)
												) AS `KNRs`,

												`customers_1`.`customersGruppe` AS `c1_GP`,
												`customers_2`.`customersGruppe` AS `c2_GP`,

												`customers_1`.`customersFirmenname` AS `c1_FIRMA`,
												`customers_2`.`customersFirmenname` AS `c2_FIRMA`,

												CONCAT(
													IF(`customers_1`.`customersCompanyStrasse` IS NOT NULL, `customers_1`.`customersCompanyStrasse`, ''),
													' ',
													IF(`customers_1`.`customersCompanyHausnummer` IS NOT NULL, `customers_1`.`customersCompanyHausnummer`, '')
												) AS `c1_ADR`,
												CONCAT(
													IF(`customers_2`.`customersCompanyStrasse` IS NOT NULL, `customers_2`.`customersCompanyStrasse`, ''),
													' ',
													IF(`customers_2`.`customersCompanyHausnummer` IS NOT NULL, `customers_2`.`customersCompanyHausnummer`, '')
												) AS `c2_ADR`,

												`customers_1`.`customersCompanyPLZ` AS `c1_PLZ`,
												`customers_2`.`customersCompanyPLZ` AS `c2_PLZ`,

												`customers_1`.`customersCompanyOrt` AS `c1_ORT`,
												`customers_2`.`customersCompanyOrt` AS `c2_ORT`,

												SOUNDEX(`customers_1`.`customersFirmenname`) AS `c1_NameSound`,
												SOUNDEX(`customers_2`.`customersFirmenname`) AS `c2_NameSound`,

												IF(
													`" . TABLE_CUSTOMER_DUPLICATES . "`.`customerDuplicatesIsDuplicate` IS NULL
													||
													`" . TABLE_CUSTOMER_DUPLICATES . "`.`customerDuplicatesIsDuplicate` = '',
													'-1',
													`" . TABLE_CUSTOMER_DUPLICATES . "`.`customerDuplicatesIsDuplicate`
												) AS `isDuplicate`

											FROM `" . TABLE_CUSTOMERS . "` AS `customers_1`

											LEFT JOIN `" . TABLE_CUSTOMERS . "` AS `customers_2`
											ON(
												`customers_1`.`customersCompanyPLZ` = `customers_2`.`customersCompanyPLZ`
												AND
												TRIM(REPLACE(REPLACE(REPLACE(CONCAT(`customers_1`.`customersCompanyStrasse`,`customers_1`.`customersCompanyHausnummer`), ' ', ''), '-', ''), '.', ''))
												=
												TRIM(REPLACE(REPLACE(REPLACE(CONCAT(`customers_2`.`customersCompanyStrasse`,`customers_2`.`customersCompanyHausnummer`), ' ', ''), '-', ''), '.', ''))
											)

											LEFT JOIN `" . TABLE_CUSTOMER_DUPLICATES . "`
											ON(
												CONCAT(`customers_1`.`customersID`, ':', `customers_2`.`customersID`) = `" . TABLE_CUSTOMER_DUPLICATES . "`.`customerDuplicatesCustomerIDs`
												OR
												CONCAT(`customers_2`.`customersID`, ':', `customers_1`.`customersID`) = `" . TABLE_CUSTOMER_DUPLICATES . "`.`customerDuplicatesCustomerIDs`
											)

											WHERE 1
												AND `customers_1`.`customersKundennummer` NOT LIKE 'AMZ%'
												AND `customers_2`.`customersKundennummer` NOT LIKE 'AMZ%'
												AND `customers_1`.`customersKundennummer` NOT LIKE 'EBY%'
												AND `customers_2`.`customersKundennummer` NOT LIKE 'EBY%'
												AND `customers_1`.`customersID` != `customers_2`.`customersID`

												" . $whereStatusDuplicate . "

											GROUP BY `KNRs`
											LIMIT " . $_REQUEST["searchCustomerLimit"] . "
										) AS `tempTable_1`

										LEFT JOIN (
											SELECT
											`ordersKundennummer`,
											`ordersID`
											FROM `common_ordersprocess`
										) AS `orders_1`
										ON(`c1_KNR` = `orders_1`.`ordersKundennummer`)

										LEFT JOIN (
											SELECT
											`ordersKundennummer`,
											`ordersID`
											FROM `common_ordersprocess`
										) AS `orders_2`
										ON(`c2_KNR` = `orders_2`.`ordersKundennummer`)

										LEFT JOIN (
											SELECT
											`createdDocumentsCustomerNumber`,
											`createdDocumentsNumber`
											FROM `b3_createddocuments`
										) AS `b3_Docs_1`
										ON(`c1_KNR` = `b3_Docs_1`.`createdDocumentsCustomerNumber`)

										LEFT JOIN (
											SELECT
											`createdDocumentsCustomerNumber`,
											`createdDocumentsNumber`
											FROM `b3_createddocuments`
										) AS `b3_Docs_2`
										ON(`c2_KNR` = `b3_Docs_2`.`createdDocumentsCustomerNumber`)

										LEFT JOIN (
											SELECT
											`createdDocumentsCustomerNumber`,
											`createdDocumentsNumber`
											FROM `bctr_createddocuments`
										) AS `bctr_Docs_1`
										ON(`c1_KNR` = `bctr_Docs_1`.`createdDocumentsCustomerNumber`)

										LEFT JOIN (
											SELECT
											`createdDocumentsCustomerNumber`,
											`createdDocumentsNumber`
											FROM `bctr_createddocuments`
										) AS `bctr_Docs_2`
										ON(`c2_KNR` = `bctr_Docs_2`.`createdDocumentsCustomerNumber`)
								) AS `tempTable_2`

								/*
								LEFT JOIN `" . TABLE_CUSTOMER_DUPLICATES . "`
								ON(CONCAT(`c1_CID`, ':', `c2_CID`) = `" . TABLE_CUSTOMER_DUPLICATES . "`.`customerDuplicatesCustomerIDs`)
								*/

								WHERE 1
								GROUP BY `KNRs`
							";

						/*
						$sql_1 = "SELECT
									`customers_1`.`customersKundennummer` AS `c1_KNR`,
									`customers_2`.`customersKundennummer` AS `c2_KNR`,

									IF(
										`customers_1`.`customersKundennummer` <= `customers_2`.`customersKundennummer`,
										CONCAT(`customers_1`.`customersKundennummer`, ':', `customers_2`.`customersKundennummer`),
										IF(
											`customers_1`.`customersKundennummer` > `customers_2`.`customersKundennummer`,
											CONCAT(`customers_2`.`customersKundennummer`, ':', `customers_1`.`customersKundennummer`),
											''
										)
									) AS `KNRs`,

									`customers_1`.`customersGruppe` AS `c1_GP`,
									`customers_2`.`customersGruppe` AS `c2_GP`,

									`customers_1`.`customersID` AS `c1_ID`,
									`customers_2`.`customersID` AS `c2_ID`,

									`customers_1`.`customersFirmenname` AS `c1_FIRMA`,
									`customers_2`.`customersFirmenname` AS `c2_FIRMA`,

									CONCAT(
										IF(`customers_1`.`customersCompanyStrasse` IS NOT NULL, `customers_1`.`customersCompanyStrasse`, ''),
										' ',
										IF(`customers_1`.`customersCompanyHausnummer` IS NOT NULL, `customers_1`.`customersCompanyHausnummer`, '')
									) AS `c1_ADR`,
									CONCAT(
										IF(`customers_2`.`customersCompanyStrasse` IS NOT NULL, `customers_2`.`customersCompanyStrasse`, ''),
										' ',
										IF(`customers_2`.`customersCompanyHausnummer` IS NOT NULL, `customers_2`.`customersCompanyHausnummer`, '')
									) AS `c2_ADR`,

									`customers_1`.`customersCompanyPLZ` AS `c1_PLZ`,
									`customers_2`.`customersCompanyPLZ` AS `c2_PLZ`,

									`customers_1`.`customersCompanyOrt` AS `c1_ORT`,
									`customers_2`.`customersCompanyOrt` AS `c2_ORT`,

									SOUNDEX(`customers_1`.`customersFirmenname`) AS `c1_NameSound`,
									SOUNDEX(`customers_2`.`customersFirmenname`) AS `c2_NameSound`

								FROM `" . TABLE_CUSTOMERS . "` AS `customers_1`

								LEFT JOIN `" . TABLE_CUSTOMERS . "` AS `customers_2`
								ON(
									`customers_1`.`customersCompanyPLZ` = `customers_2`.`customersCompanyPLZ`
									AND
									TRIM(REPLACE(REPLACE(REPLACE(CONCAT(`customers_1`.`customersCompanyStrasse`,`customers_1`.`customersCompanyHausnummer`), ' ', ''), '-', ''), '.', ''))
									=
									TRIM(REPLACE(REPLACE(REPLACE(CONCAT(`customers_2`.`customersCompanyStrasse`,`customers_2`.`customersCompanyHausnummer`), ' ', ''), '-', ''), '.', ''))
								)

								WHERE 1

									AND `customers-_1`.`customersKundennummer` NOT LIKE 'AMZ%'
									AND `customers_2`.`customersKundennummer` NOT LIKE 'AMZ%'
									AND `customers_1`.`customersKundennummer` NOT LIKE 'EBY%'
									AND `customers_2`.`customersKundennummer` NOT LIKE 'EBY%'
									AND `customers_1`.`customersID` != `customers_2`.`customersID`

								GROUP BY `KNRs`
							";
							*/
						// ############################################################################

						$sql = $sql_2;
dd('sql');
exit;
						$rs = $dbConnection->db_query($sql);

						$countTotalRows = $dbConnection->db_getMysqlNumRows($rs);

						if($countTotalRows > 0) {
							echo '<p class="infoArea">' . $countTotalRows . ' m&ouml;gliche Adress-Duplikate gefunden</p>';

							echo '<form name="formStoreDuplicates" method="post" action="' . $_SERVER["PHP_SELF"]. '">';
							echo '<table width="100%" cellpadding="0" cellspacing="0" class="displayOrders">';

							echo '<tr>';
							echo '<th style="width:45px">#</th>';
							echo '<th>Kunde entfernbar?</th>';
							echo '<th>Kunde behaltbar?</th>';

							echo '<th>STATUS</th>';

							echo '<th> </th>';

							echo '<th>KNR</th>';
							echo '<th>CID</th>';

							echo '<th>KNR in Prod.</th>';
							echo '<th>GP</th>';
							echo '<th>FIRMA</th>';
							echo '<th>ADR</th>';
							echo '<th>PLZ</th>';
							echo '<th>ORT</th>';
							echo '<th>KNR in b3 DOKs</th>';
							echo '<th>KNR in bctr DOKs</th>';
							echo '<th>Ist Duplikat</th>';

							echo '<th style="width:90px">Info</th>';
							echo '</tr>';

							$countRow = 0;
							while($ds = mysqli_fetch_assoc($rs)) {

								$rowClass = 'row0';
								if($countRow%2 == 0) {
									$rowClass = 'row1';
								}

								$arrFoundCustomerNumbersInOrders = array();
								$arrKNRs = explode(':', $ds["KNRs"]);
								$customerPossibleToDeleteCustomerNumber = '';
								$customerPossibleToUseCustomerNumber = '';

								// BOF IN CUSTOMER NUMBERS IN ORDERS
								if($ds["c1_OKNR"] != "") { $arrFoundCustomerNumbersInOrders[] = $ds["c1_OKNR"]; }
								if($ds["c2_OKNR"] != "") { $arrFoundCustomerNumbersInOrders[] = $ds["c2_OKNR"]; }
								// EOF IN CUSTOMER NUMBERS IN ORDERS

								// BOF IN CUSTOMER NUMBERS IN DOCUMENTS
								if($ds["c1_b3_DOCKNR"] != "") { $arrFoundCustomerNumbersInOrders[] = $ds["c1_b3_DOCKNR"]; }
								if($ds["c2_b3_DOCKNR"] != "") { $arrFoundCustomerNumbersInOrders[] = $ds["c2_b3_DOCKNR"]; }
								if($ds["c1_bctr_DOCKNR"] != "") { $arrFoundCustomerNumbersInOrders[] = $ds["c1_bctr_DOCKNR"]; }
								if($ds["c2_bctr_DOCKNR"] != "") { $arrFoundCustomerNumbersInOrders[] = $ds["c2_bctr_DOCKNR"]; }
								// EOF IN CUSTOMER NUMBERS IN DOCUMENTS


								if(!empty($arrFoundCustomerNumbersInOrders)){
									$arrFoundCustomerNumbersInOrders = array_unique($arrFoundCustomerNumbersInOrders);
									if(!empty($arrKNRs)){
										$arrCustomerNumberInUse = array_intersect($arrFoundCustomerNumbersInOrders, $arrKNRs);
										$arrCustomerNumberNotInUse = array_diff($arrKNRs, $arrFoundCustomerNumbersInOrders);
										$customerPossibleToDeleteCustomerNumber = implode(',', $arrCustomerNumberNotInUse);
										$customerPossibleToUseCustomerNumber = implode(',', $arrCustomerNumberInUse);
									}
								}
								else {
									/*
									if($arrKNRs[0] > $arrKNRs[1]){
										$customerPossibleToDeleteCustomerNumber = $arrKNRs[0];
									}
									else {
										$customerPossibleToDeleteCustomerNumber = $arrKNRs[1];
									}
									*/
									$customerPossibleToDeleteCustomerNumber = max($arrKNRs);
									$customerPossibleToUseCustomerNumber = min($arrKNRs);
								}

								$customerPossibleToDeleteFieldPrefix = '';
								$customerPossibleToUseFieldPrefix = '';
								if($customerPossibleToDeleteCustomerNumber == $ds["c1_KNR"]){
									$customerPossibleToDeleteFieldPrefix = 'c1';
									$customerPossibleToUseFieldPrefix = 'c2';
								}
								else if($customerPossibleToDeleteCustomerNumber == $ds["c2_KNR"]){
									$customerPossibleToDeleteFieldPrefix = 'c2';
									$customerPossibleToUseFieldPrefix = 'c1';
								}

								// BOF DISPLAY C1
								echo '<tr class="'.$rowClass.'" style="border-top:3px solid #333;">';

								echo '<td style="text-align:right;" rowspan="2"><b>';
								echo ($countRow + 1);
								echo '.</b></td>';

								echo '<td rowspan="2" style="background-color:#FEFFAF;">';
								echo '<span style="white-space:nowrap;"><b>KNR: ' . $customerPossibleToDeleteCustomerNumber . '</b></span>';
								echo '<input type="hidden" name="customerPossibleToDeleteCustomerNumber[' . $countRow . ']" class="inputField_50" value="' . $customerPossibleToDeleteCustomerNumber . '" /> ';
								echo '<br />';
								echo '<span style="white-space:nowrap;"><b>ID: ' . $ds[$customerPossibleToDeleteFieldPrefix . "_CID"] . '</b></span>';
								echo '<br />Kunde ' . $customerPossibleToDeleteFieldPrefix . '';
								echo '<input type="hidden" name="customerPossibleToDeleteCustomerID[' . $countRow . ']" class="inputField_50" value="' . $ds[$customerPossibleToDeleteFieldPrefix . "_CID"] . '" /> ';
								echo '<br /><a href="' . PAGE_EDIT_CUSTOMER . '?editID=' . $ds[$customerPossibleToDeleteFieldPrefix . "_CID"] . '"><img src="layout/icons/iconEdit.gif" width="16" height="16" /></a>' ;

								echo '</td>';

								echo '<td rowspan="2">';
								echo '<span style="white-space:nowrap;"><b>KNR: ' . $customerPossibleToUseCustomerNumber . '</b></span>';
								echo '<input type="hidden" name="customerPossibleToUseCustomerNumber[' . $countRow . ']" class="inputField_50" value="' . $customerPossibleToUseCustomerNumber . '" /> ';
								echo '<br />';
								echo '<span style="white-space:nowrap;"><b>ID: ' . $ds[$customerPossibleToUseFieldPrefix . "_CID"] . '</b></span>';
								echo '<br />Kunde ' . $customerPossibleToUseFieldPrefix . '';
								echo '<input type="hidden" name="customerPossibleToUseCustomerID[' . $countRow . ']" class="inputField_50" value="' . $ds[$customerPossibleToUseFieldPrefix . "_CID"] . '" /> ';
								echo '<br /><a href="' . PAGE_EDIT_CUSTOMER . '?editID=' . $ds[$customerPossibleToUseFieldPrefix . "_CID"] . '"><img src="layout/icons/iconEdit.gif" width="16" height="16" /></a>' ;

								echo '</td>';

								echo '</td>';

								echo '<td rowspan="2">';
								echo '<div style="font-size:11px;">';

								echo '<span style="white-space:nowrap;">';
								$thisChecked = '';
								if($ds["isDuplicate"] == '-1'){
									$thisChecked = ' checked="checked" ';
								}
								echo '<input type="radio" name="isDuplicateCustomer[' . $countRow . ']" value="-1" ' . $thisChecked . ' /> ungepr&uuml;ft';
								echo '</span>';
								echo '<br />';

								echo '<span style="white-space:nowrap;">';
								$thisChecked = '';
								if($ds["isDuplicate"] == '0'){
									$thisChecked = ' checked="checked" ';
								}
								echo '<input type="radio" name="isDuplicateCustomer[' . $countRow . ']" value="0" ' . $thisChecked . ' /> Keine Duplikate';
								echo '</span>';
								echo '<br />';

								echo '<span style="white-space:nowrap;">';
								$thisChecked = '';
								if($ds["isDuplicate"] == '1'){
									$thisChecked = ' checked="checked" ';
								}
								echo '<input type="radio" name="isDuplicateCustomer[' . $countRow . ']" value="1" ' . $thisChecked . ' /> Duplikate';
								echo '<br />';

								// BOF KNRs:
								echo '<input type="hidden" name="isDuplicateCustomerDataCustomerNumber[' . $countRow . ']" class="inputField_120" value="' . $ds["KNRs"] . '" /> ';
								#echo '<br />';
								// BOF CIDs:
								echo '<input type="hidden" name="isDuplicateCustomerDataCustomerID[' . $countRow . ']" class="inputField_120" value="' . $ds["c1_CID"] . ':' . $ds["c2_CID"] . '" /> ';
								echo '</span>';
								echo '</div>';
								echo '</td>';

								$cellStyle = 'color:#009900;';
								if($customerPossibleToDeleteFieldPrefix == "c1"){
									$cellStyle = 'color:#990000;background-color:#FEFFAF;';
								}
								$cellStyle .= 'font-weight:bold;';

								echo '<td style="' . $cellStyle . '"><b>c1</b></td>';

								echo '<td style="' . $cellStyle . 'white-space:nowrap;">';
								echo '<b>' . $ds["c1_KNR"] . '</b>';
								echo '</td>';

								echo '<td style="' . $cellStyle . 'white-space:nowrap;">';
								echo '' . '<a href="' . PAGE_EDIT_CUSTOMER . '?editID=' . $ds["c1_CID"] . '"><img src="layout/icons/iconEdit.gif" width="16" height="16" /></a> ' . $ds["c1_CID"] ;
								echo '</td>';

								echo '<td style="' . $cellStyle . 'white-space:nowrap;">';
								echo '' . $ds["c1_OKNR"];
								echo '</td>';

								echo '<td style="white-space:nowrap;">';
								echo '<span style="' . $cellStyle . '">';
								echo '' . $ds["c1_GP"];
								echo '</span>';
								echo '</td>';

								echo '<td style="' . $cellStyle . 'white-space:nowrap;">';
								echo '' . $ds["c1_FIRMA"];
								echo '</td>';

								echo '<td style="' . $cellStyle . 'white-space:nowrap;">';
								echo '' . $ds["c1_ADR"];
								echo '</td>';

								echo '<td style="' . $cellStyle . 'white-space:nowrap;">';
								echo '' . $ds["c1_PLZ"];
								echo '</td>';

								echo '<td style="' . $cellStyle . 'white-space:nowrap;">';
								echo '' . $ds["c1_ORT"];
								echo '</td>';

								echo '<td style="' . $cellStyle . 'white-space:nowrap;">';
								echo '' . $ds["c1_b3_DOCKNR"];
								echo '</td>';

								echo '<td style="' . $cellStyle . 'white-space:nowrap;">';
								echo '' . $ds["c1_bctr_DOCKNR"];
								echo '</td>';

								echo '<td rowspan="2">';
								echo $ds["isDuplicate"];
								echo '</td>';

								echo '<td rowspan="2">';
								echo '<span class="toolItem">';
								echo '<img src="layout/icons/iconInfo.png" class="buttonShopOrderInfo" alt="Details" title="Detailinformationen ansehen">';
								echo '<div class="orderDetailsContent" style="display:none; visibility:hidden;">';
								echo '</div>';
								echo '</span>';
								echo '</td>';

								echo '</tr>';
								// EOF DISPLAY C1

								// BOF DISPLAY C2
								echo '<tr class="'.$rowClass.'">';

								$cellStyle = 'color:#009900;';
								if($customerPossibleToDeleteFieldPrefix == "c2"){
									$cellStyle = 'color:#990000;';
								}
								$cellStyle .= 'font-weight:bold;';

								echo '<td style=""' . $cellStyle . '><b>c2</b></td>';

								echo '<td style="' . $cellStyle . 'white-space:nowrap;">';
								echo '' . $ds["c2_KNR"];
								echo '</td>';

								echo '<td style="' . $cellStyle . 'white-space:nowrap;">';
								echo '' . '<a href="' . PAGE_EDIT_CUSTOMER . '?editID=' . $ds["c2_CID"] . '"><img src="layout/icons/iconEdit.gif" width="16" height="16" /></a> ' . $ds["c2_CID"] ;
								echo '</td>';

								echo '<td style="' . $cellStyle . 'white-space:nowrap;">';
								echo '' . $ds["c2_OKNR"];
								echo '</td>';

								echo '<td style="' . $cellStyle . 'white-space:nowrap;">';
								echo '' . $ds["c2_GP"];
								echo '</td>';

								echo '<td style="' . $cellStyle . 'white-space:nowrap;">';
								echo '' . $ds["c2_FIRMA"];
								echo '</td>';

								echo '<td style="' . $cellStyle . 'white-space:nowrap;">';
								echo '' . $ds["c2_ADR"];
								echo '</td>';

								echo '<td style="' . $cellStyle . 'white-space:nowrap;">';
								echo '' . $ds["c2_PLZ"];
								echo '</td>';

								echo '<td style="' . $cellStyle . 'white-space:nowrap;">';
								echo '' . $ds["c2_ORT"];
								echo '</td>';

								echo '<td style="' . $cellStyle . 'white-space:nowrap;">';
								echo '' . $ds["c2_b3_DOCKNR"];
								echo '</td>';

								echo '<td style="' . $cellStyle . 'white-space:nowrap;">';
								echo '' . $ds["c2_bctr_DOCKNR"];
								echo '</td>';

								echo '</tr>';
								// EOF DISPLAY C2

								$countRow++;
							}
							echo '</table>';

							echo '<div class="actionButtonsArea">';
							echo '<input type="submit" class="inputButton1 inputButtonGreen" name="storeDatas" value="Speichern" />';
							echo '</div>';


							echo '</form>';
						}
						else {
							$warningMessage .= 'Es wurden keine Daten gefunden.';
						}
						// EOF GET DATAS
					?>
					<?php displayMessages(); ?>
				</div>

			</div>
		</div>
		<div class="clear"></div>
	</div>
</div>
<script language="javascript" type="text/javascript">
	$(document).ready(function() {
		$(function() {
			colorRowMouseOver('.displayOrders tbody tr');
			$('.buttonShopOrderInfo').css('cursor', 'pointer');
			$('.buttonShopOrderInfo').click(function () {
				loadOrderDetails($(this), '<?php echo BASEPATH; ?>', 'Details der Bestellung');
			});
		});
	});
</script>
<?php require_once('inc/footerHTML.inc.php'); ?>