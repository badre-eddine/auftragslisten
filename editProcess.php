<?php
	require_once('inc/requires.inc.php');

	if(!$arrGetUserRights["editOrders"]) {
		header('location: ' . PAGE_EXIT_LOCATION);
		exit;
	}

	$jswindowMessage = '';
	$warningMessage = '';
	$errorMessage = '';
	$successMessage = '';
	$infoMessage = '';

	$dbConnection = new DB_Connection();
	$db_open = $dbConnection->db_connect();

// sachbearbeiter teile von code wichtig

		$userDatas = getUserDatas(); 

		if($userDatas["usersLogin"] == 'thorsten'){
			DEFINE('USE_DEBITING_DATE', false);
		}
		else {
			DEFINE('USE_DEBITING_DATE', false);
		}

	 
	// BOF READ PRINTERS
		$arrPrinterDatas = getPrinters();
	// EOF READ PRINTERS

	// BOF READ PRINTTYPES
		$arrPrintTypeDatas = getPrintTypes();
	// EOF READ PRINTTYPES

	// BOF READ VERTRETER
		$arrAgentDatas = getAgents();
		$arrAgentDatas2 = getAgents2();
	// EOF READ VERTRETER

	// BOF READ PAYMENT TYPES
		$arrPaymentTypeDatas = getPaymentTypes();
	// EOF READ PAYMENT TYPES

	// BOF READ PAYMENT STATUS TYPES
		$arrPaymentStatusTypeDatas = getPaymentStatusTypes();
	// EOF READ PAYMENT STATUS TYPES

	// BOF READ BESTELL ARTEN
		$arrOrderTypeDatas = getOrderTypes();
	// EOF READ BESTELL ARTEN

	// BOF READ BESTELL STATUS TYPEN
		$arrOrderStatusTypeDatas = getOrderStatusTypes();
	// EOF READ BESTELL STATUS TYPEN

	// BOF READ ADDITIONAL COSTS
		$arrAdditionalCostsDatas = getAdditionalCosts();
	// EOF READ ADDITIONAL COSTS

	// BOF READ ORDER SOURCE TYPES
		$arrOrdersSourceTypeDatas = getOrdersSourceTypes();
	// EOF READ ORDER SOURCE TYPES

	// BOF READ BANK TYPES
		$arrBankAccountTypeDatas = getBankAccountTypes();
	// EOF READ BANK TYPES

	// BOF READ ORDER CATEGORIES
		// $arrOrderCategoriesTypeDatas = getOrderCategories();
	// EOF READ ORDER CATEGORIES

	// BOF READ PRINT COLORS
		$arrPrintColors = getPrintColors();
	// EOF READ PRINT COLORS

	// BOF READ PRINT COLORS
		$arrSupplierDatas = getSuppliers();
	// EOF READ PRINT COLORS

	// BOF READ COUNTRIES
		$arrCountryTypeDatas = getCountryTypes();
	// EOF READ COUNTRIES


	if($_GET["downloadFile"] != "") {
		require_once('classes/downloadFile.class.php');
		$thisDownload = new downloadFile(DIRECTORY_CREATED_DOCUMENTS_CUSTOMERS, $_GET["downloadFile"]);
		$errorMessage .= $thisDownload->startDownload();
	}

	// BOF DEFINE DUTY FIELDS
		$arrFormDutyFields = array(
			// "editOrderPrintPlateNumber" => "Druck-Auftrag",
			"editOrdersBestellDatum" => "Bestelldatum",
			"editOrdersKundennummer" => "Kundennummer",
			"editOrdersKundenName" => "Kundenname",
			// "editOrdersPLZ" => "PLZ",
			// "editOrdersOrt" => "Ort",
			// "editOrdersLand" => "Land",
			"editOrdersArtikelNummer" => "Artikel-Nummer",
			"editOrdersArtikelKategorieID" => "Artikel-Kategorie",
			"editOrdersArtikelBezeichnung" => "Artikel-Bezeichnung",
			"editOrdersArtikelMenge" => "Artikel-Menge",
			// "editOrdersArtikelPrintColorsCount" => "Anzahl Druckfarben",
			//"editOrdersSinglePreis" => "Artikel-Einzelpreis",
			//"editOrdersTotalPreis" => "Artikel-Gesamtpreis",
			// "editOrdersDruckFarbe" => "Artikel-Druckfarbe",
			"editOrdersAdditionalCostsID" => "Artikel-Druckart",
			"editOrdersStatus" => "Bestell-Status",
			"editOrdersSourceType" => "Bestell-Quelle",
			"editOrdersArtikelMenge" => "Bestell-Quelle",
			"druckAuftrag" => "Druck-Auftrag",
			"editOrdersOrderType" => "Bestell-Art"
		);
		// CREATE JSON DUTY FIELD
		$jsonFormDutyFields = createJson($arrFormDutyFields);
	// BOF DEFINE DUTY FIELDS



	if($_REQUEST["editID"] == "") {
		// $_REQUEST["editID"] = "NEW";
		// header("location: displayOrders.php");
		// exit;
	}

	// BOF STORE DATAS
	if($_POST["storeDatas"] != "" || $_POST["copyDatas"] != "" || $_POST["storeDatasAndGotToCustomer"] != "") {
		$doAction = checkFormDutyFields($arrFormDutyFields);
		$checkCustomerEmail = true;
		if(trim($_POST["editOrdersMailAdress"] != '')) {
			#$checkCustomerEmail = checkEmailStructure($_POST["editOrdersMailAdress"]);
		}

		// BOF SET DELIVERY KW ON STATUS CHANGE
			if($_POST["storeDatas"] != ""){
				if($_POST["editOrdersID"] > 0){
					if($_POST["editOrdersStatus"] != $_POST["storedStatus"] && $_POST["editOrdersStatus"] == '2' && $_POST["editOrdersFreigabeDatum"] != '') {
						$newOrdersLieferwoche = date('W', strtotime(formatDate($_POST["editOrdersFreigabeDatum"], 'store'))) + MIN_SHIPPING_TIME;
						// BOF GET DELIVERY TIME DEPENDING ON FreigabeDatum AND PRINT-TYPE
							$thisDeliveryTimeKW = getDeliveryTimeKW(formatDate($_POST["editOrdersFreigabeDatum"], 'store'), $_POST["editOrdersAdditionalCostsID"]);
						// EOF GET DELIVERY TIME DEPENDING ON FreigabeDatum AND PRINT-TYPE
						// $_POST["editOrdersLieferwoche"] = $newOrdersLieferwoche;
						$_POST["editOrdersLieferwoche"]	= $thisDeliveryTimeKW;
					}
				}
				else if($_POST["editOrdersStatus"] == '2' && $_POST["editOrdersBestellDatum"] != ''){
					$thisDeliveryTimeKW = getDeliveryTimeKW(formatDate($_POST["editOrdersBestellDatum"], 'store'), $_POST["editOrdersAdditionalCostsID"]);
					$_POST["editOrdersLieferwoche"]	= $thisDeliveryTimeKW;
				}

			}
		// EOF SET DELIVERY KW ON STATUS CHANGE

		if($doAction && $checkCustomerEmail) {
			if($_POST["editOrdersID"] != "") {
				$thisOrdersID = $_POST["editOrdersID"];
				//$_REQUEST["editID"] = $_POST["editOrdersID"];
			}
			else {
				/*
				$sql = "SELECT (MAX(`ordersID`) + 1) AS `newOrdersID` FROM `" . TABLE_ORDERS . "` WHERE 1";
				$rs = $dbConnection->db_query($sql);
				list($thisOrdersID) = mysqli_fetch_array($rs);
				if($thisOrdersID == null || $thisOrdersID == 0){
					$thisOrdersID = 1;
				}
				*/
				//$_REQUEST["editID"] = 'NEW';
				$thisOrdersID = '%';
			}
			if($_POST["editOrdersVertreterID"] != "") {
				// $thisEditOrdersVertreter = $arrAgentDatas[$_POST["editOrdersVertreterID"]]["customersFirmenname"] . " (K-NR: " . $arrAgentDatas[$_POST["editOrdersVertreterID"]]["customersKundennummer"] . ")";
				$thisEditOrdersVertreter = $arrAgentDatas2[$_POST["editOrdersVertreterID"]]["customersFirmenname"];
			}
			else {
				$thisEditOrdersVertreter = "";
			}

			if(!empty($_POST["printColorID"])) {
				$arrOrderColors = array();
				foreach($_POST["printColorID"] as $thisKey => $thisValue) {
					if($_POST["printColorID"][$thisKey] == '1') {
						$thisTempColor = $arrPrintColors[$thisKey]["printColorValue"];
						if($_POST["printColorType"][$thisKey] != "") {
							$thisTempColor .= ' (' . $_POST["printColorType"][$thisKey] . ')';
						}
						if($_POST["printColorValue"][$thisKey] != "") {
							$thisTempColor .= ' [' . $_POST["printColorValue"][$thisKey] . ']';
						}
						$arrOrderColors[] = $thisTempColor;
					}
				}
				$strOrderColors = implode(" / ", $arrOrderColors);
			}
			else if(!empty($_POST["editOrdersDruckFarbe"])){
				$strOrderColors = $_POST["editOrdersDruckFarbe"];
			}
// echo $thisOrdersID,"<hr>";
// print_r($_POST);
// exit();
			$sql = "
				REPLACE INTO `" . TABLE_ORDERS . "`(";
			if($thisOrdersID!='%'){
				$sql.="`ordersID`,";
			}		
			$editOrdersArtikelWithBorder=isset($_POST["editOrdersArtikelWithBorder"])?$_POST["editOrdersArtikelWithBorder"]:0;
 			$editOrdersArtikelGesamtGewichtK=isset($_POST["editOrdersArtikelGesamtGewichtK"])?$_POST["editOrdersArtikelGesamtGewichtK"]:0;
 			$editOrdersArtikelGewicht=isset($_POST["editOrdersArtikelGewicht"])?$_POST["editOrdersArtikelGewicht"]:0;
 			$editOrdersArtikelMaterial=isset($_POST["editOrdersArtikelMaterial"])?$_POST["editOrdersArtikelMaterial"]:0;
			$editOrdersVerpackungseinheiten=isset($_POST["editOrdersVerpackungseinheiten"])?$_POST["editOrdersVerpackungseinheiten"]:0;


			$editOrdersArtikelWithClearPaint=isset($_POST["editOrdersArtikelWithClearPaint"])?$_POST["editOrdersArtikelWithClearPaint"]:0;	
			$ordersSinglePreis=isset($_POST["ordersSinglePreis"])?$_POST["ordersSinglePreis"]:0;	
			$ordersTotalPreis=isset($_POST["ordersTotalPreis"])?$_POST["ordersTotalPreis"]:0;	
			$ordersAdditionalArtikelMenge=isset($_POST["ordersAdditionalArtikelMenge"])?$_POST["ordersAdditionalArtikelMenge"]:0;	
			$ordersAdditionalCosts=isset($_POST["editOrdersAdditionalCostsPrice"][$_POST["editOrdersAdditionalCostsID"]])?$_POST["editOrdersAdditionalCostsPrice"][$_POST["editOrdersAdditionalCostsID"]]:0;	
			$ordersDruckerName=isset($_POST["ordersDruckerName"])?$_POST["ordersDruckerName"]:0;	
			$ordersPerExpress=isset($_POST["ordersPerExpress"])?$_POST["ordersPerExpress"]:0;	
			$ordersBankAccountType=isset($_POST["ordersBankAccountType"])?$_POST["ordersBankAccountType"]:0;	
			$ordersPaymentType=isset($_POST["ordersPaymentType"])?$_POST["ordersPaymentType"]:0;	
			$ordersPaymentStatusType=isset($_POST["ordersPaymentStatusType"])?$_POST["ordersPaymentStatusType"]:0;	
			$ordersArchive=isset($_POST["ordersArchive"])?$_POST["ordersArchive"]:0;	
			$ordersDirectSale=isset($_POST["ordersDirectSale"])?$_POST["ordersDirectSale"]:0;	
			$ordersVertreterID=isset($_POST["ordersVertreterID"])?$_POST["ordersVertreterID"]:0;	
			$ordersLieferDatum=isset($_POST["ordersLieferDatum"])?formatDate($_POST["ordersLieferDatum"], 'store'):date("Y-m-d");	
			$druckAuftrag=isset($_POST["druckAuftrag"])?$_POST["druckAuftrag"]:0;	

			/* Upload File PDF*/
			if(isset($_FILES["druckAuftrag"]["name"]) && $_FILES["druckAuftrag"]["name"]!=""){

				$target_dir = "documents_common/aufdruckPDF/";
				$file_name = "DruckAuftrag"/*(date('yymmddhms'))*/;
				$file_db_name = $file_name.'-'.basename($_FILES["druckAuftrag"]["name"]); ;
				$target_file =  $target_dir.$file_name.'-'.basename($_FILES["druckAuftrag"]["name"]);
				
				$moving_file = move_uploaded_file($_FILES["druckAuftrag"]["tmp_name"], $target_file);
			}
		 

			/*  End Upload */
			$sql.="					
					`ordersBestellDatum`,
					`ordersStornoDatum`,
					`ordersFreigabeDatum`,
					`ordersFreigabeArt`,
					`ordersBelichtungsDatum`,
					`ordersProduktionsDatum`,
					`ordersAuslieferungsDatum`,
					`ordersKundennummer`,
					`ordersKundenName`,
					`ordersKommission`,
					`ordersInfo`,
					`ordersPLZ`,
					`ordersOrt`,
					`ordersLand`,
					`ordersArtikelMenge`,
					`ordersArtikelPrintColorsCount`,
					`ordersArtikelWithBorder`,
					`ordersArtikelWithClearPaint`,
					`ordersAufdruck`,
					`ordersSinglePreis`,
					`ordersTotalPreis`,
					`ordersArtikelBezeichnung`,
					`ordersArtikelID`,
					`ordersArtikelNummer`,
					`ordersArtikelKategorieID`,

					`ordersAdditionalArtikelKategorieID`,
					`ordersAdditionalArtikelMenge`,

					`ordersDruckFarbe`,
					`ordersAdditionalCosts`,
					`ordersAdditionalCostsID`,
					`ordersVertreter`,
					`ordersVertreterID`,
					`ordersMailAdress`,
					`ordersContactOthers`,
					`ordersMandant`,
					`ordersLieferwoche`,
					`ordersLieferDatum`,
					`ordersDruckerName`,
					`ordersNotizen`,
					`ordersStatus`,
					`ordersOrderType`,
					`ordersPerExpress`,
					`ordersBankAccountType`,
					`ordersPaymentType`,
					`ordersPaymentStatusType`,
					`ordersSourceType`,
					`ordersSourceName`,
					`ordersUserID`,
					`ordersTimeCreated`,
					`ordersArchive`,
					`ordersDirectSale`,
					`ordersReklamationsgrund`,
					`ordersProductionPrintingPlant`,
					`ordersAdditionalArtikelNummer`,
					`ordersAdditionalArtikelBezeichnung`,
					`ordersAdditionalArtikelID`,
					`editOrdersArtikelGewicht`,
					`editOrdersArtikelMaterial`,
					`editOrdersArtikelGesamtGewichtK`,
					`editOrdersVerpackungseinheiten`,
					`ordersPrintPlateNumber`,
					`sachbearbeiter`";
					if(isset($file_db_name))
						$sql.=",`aufdruckPDF`";   
					$sql.="
				)
				VALUES (";
				if($thisOrdersID!='%'){
					$sql.="'".$thisOrdersID."',";
				}
					$sql.="
					'".formatDate($_POST["editOrdersBestellDatum"], 'store')."',
					'".formatDate($_POST["editOrdersStornoDatum"], 'store')."',
					'".formatDate($_POST["editOrdersFreigabeDatum"], 'store')."',
					'".($_POST["editOrdersFreigabeArt"])."',
					'".formatDate($_POST["editOrdersBelichtungsDatum"], 'store')."',
					'".formatDate($_POST["editOrdersProduktionsDatum"], 'store')."',
					'".formatDate($_POST["editOrdersAuslieferungsDatum"], 'store')."',
					'".$_POST["editOrdersKundennummer"]."',
					'".addslashes($_POST["editOrdersKundenName"])."',
					'".addslashes($_POST["editOrdersKommission"])."',
					'".addslashes($_POST["editOrdersInfo"])."',
					'".$_POST["editOrdersPLZ"]."',
					'".$_POST["editOrdersOrt"]."',
					'".$_POST["editOrdersLand"]."',
					'".str_replace(",", ".", $_POST["editOrdersArtikelMenge"])."',
					'".str_replace(",", ".", $_POST["editOrdersArtikelPrintColorsCount"])."',
					'".$editOrdersArtikelWithBorder."',
					'".$editOrdersArtikelWithClearPaint."',
					'".addslashes($_POST["editOrdersAufdruck"])."',
					'".$ordersSinglePreis."',
					'".$ordersTotalPreis."',

					'".addslashes($_POST["editOrdersArtikelBezeichnung"])."',
					'".$_POST["editOrdersArtikelID"]."',
					'".$_POST["editOrdersArtikelNummer"]."',
					'".$_POST["editOrdersArtikelKategorieID"]."',

					'".$_POST["editOrdersAdditionalArtikelKategorieID"]."',
					'".$ordersAdditionalArtikelMenge."',

					'".$strOrderColors."',
					'".str_replace(",", ".", $ordersAdditionalCosts)."',
					'".$_POST["editOrdersAdditionalCostsID"]."',
					'".addslashes($thisEditOrdersVertreter)."',
					'".$ordersVertreterID."',
					'".$_POST["editOrdersMailAdress"]."',
					'".$_POST["editOrdersContactOthers"]."',
					'".$_POST["editOrdersMandant"]."',
					'".$_POST["editOrdersLieferwoche"]."',
					'".$ordersLieferDatum."',
					'".$ordersDruckerName."',
					'".addslashes($_POST["editOrdersNotizen"])."',
					'".$_POST["editOrdersStatus"]."',
					'".$_POST["editOrdersOrderType"]."',
					'".$_POST['editOrdersPerExpress']."',
					'".$ordersBankAccountType."',
					'".$ordersPaymentType."',
					'".$ordersPaymentStatusType."',
					'".$_POST["editOrdersSourceType"]."',
					'".$_POST["editOrdersSourceName"]."',
					'".$_SESSION["usersID"]."',
					NOW(),
					'".$ordersArchive."',
					'".$ordersDirectSale."',
					'".addslashes($_POST["editOrdersReklamationsgrund"])."',
					'".$_POST["editOrdersProductionPrintingPlant"]."',
					'',
					'',
					'0',
					'".$editOrdersArtikelGewicht."',
					'".$editOrdersArtikelMaterial."',
					'".$editOrdersArtikelGesamtGewichtK."',
					'".$editOrdersVerpackungseinheiten."',
					'".$_POST["editOrderPrintPlateNumber"]."',
					'" .$userDatas['usersFirstName'].' '.$userDatas['usersLastName'] ."'";
					if(isset($file_db_name))
						$sql.=",'".$file_db_name."'";   
					$sql.="
					
				)
			";
			if($thisOrdersID != '') {
				$rs = $dbConnection->db_query($sql);
				$thisStoreID = $dbConnection->db_getInsertID();
// echo $sql;exit();
			}
			if($rs) {
				$successMessage .= ' Die Produktion wurde gespeichert.' .'<br />';

				// BOF SEND INFORMATION STATUS CHANGE
					if($_POST["editOrdersID"] > 0 && $_POST["editOrdersStatus"] != $_POST["storedStatus"]) {
						/*	$resultOrderStatusChangeInformAgent = sendMailStatusChanges(
							$_POST["editOrdersKundennummer"],
							$_POST["editOrdersStatus"],
							$_POST["storedStatus"],
							$_POST["editOrdersID"]
						); */
					}
				// EOF SEND INFORMATION STATUS CHANGE

				// BOF STORNO DOCUMENTS
				if($_POST["editOrdersStatus"] == '6') {
					$sql_getAssociatedDocuments = "
							SELECT
									`" . TABLE_ORDER_CONFIRMATIONS . "`.`orderDocumentsNumber`,
									`" . TABLE_ORDER_CONFIRMATIONS . "`.`orderDocumentsOrdersIDs`,

									'AB' AS `orderDocumentsType`,

									`" . TABLE_RELATED_DOCUMENTS . "`.`relatedDocuments_AN`,
									`" . TABLE_RELATED_DOCUMENTS . "`.`relatedDocuments_AB`,
									`" . TABLE_RELATED_DOCUMENTS . "`.`relatedDocuments_RE`,
									`" . TABLE_RELATED_DOCUMENTS . "`.`relatedDocuments_LS`,
									`" . TABLE_RELATED_DOCUMENTS . "`.`relatedDocuments_GU`,
									`" . TABLE_RELATED_DOCUMENTS . "`.`relatedDocuments_MA`,
									`" . TABLE_RELATED_DOCUMENTS . "`.`relatedDocuments_M1`,
									`" . TABLE_RELATED_DOCUMENTS . "`.`relatedDocuments_M2`,
									`" . TABLE_RELATED_DOCUMENTS . "`.`relatedDocuments_M3`,
									`" . TABLE_RELATED_DOCUMENTS . "`.`relatedDocuments_IK`,
									`" . TABLE_RELATED_DOCUMENTS . "`.`relatedDocuments_collectiveABs`

								FROM `" . TABLE_ORDER_CONFIRMATIONS . "`

								LEFT JOIN `" . TABLE_ORDER_CONFIRMATIONS_DETAILS . "`
								ON(`" . TABLE_ORDER_CONFIRMATIONS_DETAILS . "`.`orderDocumentDetailDocumentID` = `" . TABLE_ORDER_CONFIRMATIONS . "`.`orderDocumentsID`)

								LEFT JOIN `" . TABLE_RELATED_DOCUMENTS. "`
								ON(`" . TABLE_ORDER_CONFIRMATIONS . "`.`orderDocumentsNumber` = `" . TABLE_RELATED_DOCUMENTS . "`.`relatedDocuments_AB`)

								WHERE 1
									AND `" . TABLE_ORDER_CONFIRMATIONS_DETAILS . "`.`orderDocumentDetailOrderID` = '".$thisOrdersID."'

						";
					$rs_getAssociatedDocuments = $dbConnection->db_query($sql_getAssociatedDocuments);
					$countDocuments = 0;
					while ($ds = mysqli_fetch_assoc($rs_getAssociatedDocuments)){
						$countDocuments++;
						$arrAssociatedDocumentNumbers[$ds["orderDocumentsType"]] = $ds["orderDocumentsNumber"];
						$arrAssociatedDocumentNumbers["relatedDocuments_LS"] = $ds["relatedDocuments_LS"];
						$arrAssociatedDocumentDatas[$ds["orderDocumentsNumber"]]["orderPosition"] = $ds["orderPosition"];
						$arrAssociatedDocumentDatas[$ds["orderDocumentsNumber"]]["orderIDs"] = explode(";", $ds["orderDocumentsOrdersIDs"]);
						$arrAssociatedDocumentDatas[$ds["orderDocumentsNumber"]]["orderIDsCount"] = count(explode(";", $ds["orderDocumentsOrdersIDs"]));
					}

					if($countDocuments > 0) {
						// BOF PRÜFEN OB RE VORLIEGT?
						// DANN NICHT STORNIEREN
						#if($arrAssociatedDocumentNumbers["relatedDocuments_RE"] != ''){

						#}

						$sql_updateAssociatedDocuments = "
								UPDATE
									`" . TABLE_ORDER_CONFIRMATIONS_DETAILS . "`
									LEFT JOIN `" . TABLE_ORDER_CONFIRMATIONS . "`
									ON(`" . TABLE_ORDER_CONFIRMATIONS_DETAILS . "`.`orderDocumentDetailDocumentID` = `" . TABLE_ORDER_CONFIRMATIONS . "`.`orderDocumentsID`)
									SET `" . TABLE_ORDER_CONFIRMATIONS . "`.`orderDocumentsStatus` = '3'

								WHERE 1
									AND `" . TABLE_ORDER_CONFIRMATIONS_DETAILS . "`.`orderDocumentDetailOrderID` = '".$thisOrdersID."'
							";
						$rs_updateAssociatedDocuments = $dbConnection->db_query($sql_updateAssociatedDocuments);

						if($rs_updateAssociatedDocuments) {
							$successMessage .= ' Die zur Produktion geh&ouml;rigen Auftragsbest&auml;tigung (&quot;' . $arrAssociatedDocumentNumbers["AB"] . '&quot;) wurden auf <b>STORNIERT</b> gesetzt.' .'<br />';
						}
						else {
							$errorMessage .= ' Die zur Produktion geh&ouml;rigen Auftragsbest&auml;tigung (&quot;' . $arrAssociatedDocumentNumbers["AB"] . '&quot;) konnten nicht auf <b>STORNIERT</b> gesetzt werden.' . '<br />';
						}

						$sql_updateAssociatedDocuments = "
								UPDATE
									`" . TABLE_ORDER_DELIVERIES_DETAILS . "`
									LEFT JOIN `" . TABLE_ORDER_DELIVERIES . "`
									ON(`" . TABLE_ORDER_DELIVERIES_DETAILS . "`.`orderDocumentDetailDocumentID` = `" . TABLE_ORDER_DELIVERIES . "`.`orderDocumentsID`)
									SET `" . TABLE_ORDER_DELIVERIES . "`.`orderDocumentsStatus` = '3'

								WHERE 1
									AND `" . TABLE_ORDER_DELIVERIES_DETAILS . "`.`orderDocumentDetailOrderID` = '".$thisOrdersID."'
							";
						$rs_updateAssociatedDocuments = $dbConnection->db_query($sql_updateAssociatedDocuments);

						if($rs_updateAssociatedDocuments) {
							$successMessage .= ' Der zur Produktion geh&ouml;rigen Lieferschein (&quot;' . $arrAssociatedDocumentNumbers["relatedDocuments_LS"] . '&quot;) wurden auf <b>STORNIERT</b> gesetzt.' .'<br />';
						}
						else {
							$errorMessage .= ' Der zur Produktion geh&ouml;rigen Lieferschein (&quot;' . $arrAssociatedDocumentNumbers["relatedDocuments_LS"] . '&quot;) konnten nicht auf <b>STORNIERT</b> gesetzt werden.' . '<br />';
						}
					}
					else {
						$infoMessage .= ' Es wurden keine zur Produktion geh&ouml;rigen Auftragsbest&auml;tigungen gefunden. ';
					}
				}
				// EOF STORNO DOCUMENTS
			}
			else {
				$errorMessage .= ' Die Produktion konnte nicht gespeichert werden.' . '<br />';
			}
			// BOF STORE MATERIAL ORDERS
			if($_POST["materialOrderedBySupplierID"] != '' && $_POST["materialOrderdBySupplierAmount"] != '' && $_POST["materialOrderdBySupplierOrderDate"] != ''){
				$sql = "DELETE FROM `" . TABLE_SUPPLIER_ORDERS . "` WHERE `supplierOrdersOrderProcessID` = '" . $thisStoreID . "'";
				$rs = $dbConnection->db_query($sql);

				$sql = "
					INSERT INTO `" . TABLE_SUPPLIER_ORDERS . "`(
							`supplierOrdersID`,
							`supplierOrdersSupplierID`,
							`supplierOrdersOrderProcessID`,
							`supplierOrdersAmount`,
							`supplierOrdersProductNumber`,
							`supplierOrdersProductName`,
							`supplierOrdersProductID`,
							`supplierOrdersProductKategorieID`,
							`supplierOrdersProductSinglePrice`,
							`supplierOrdersOrderDate`,
							`supplierOrdersDeliveryDate`,
							`supplierOrdersDeliveryDirectToCustomer`
							)
							VALUES (
								'%',
								'" . $_POST["materialOrderedBySupplierID"] . "',
								'" . $thisStoreID . "',
								'" . $_POST["materialOrderdBySupplierAmount"] . "',
								'" . $_POST["editOrdersArtikelNummer"] . "',
								'" . addslashes($_POST["editOrdersArtikelBezeichnung"]) . "',
								'" . $_POST["editOrdersArtikelID"]."',
								'" . $_POST["editOrdersArtikelKategorieID"]."',
								'',
								'" . formatDate($_POST["materialOrderdBySupplierOrderDate"], 'store') . "',
								'" . formatDate($_POST["materialOrderdBySupplierDeliveryDate"], 'store') . "',
								'" . $_POST["materialOrderdBySupplierDeliveryDirectToCustomer"]."'
							)
					";
				$rs = $dbConnection->db_query($sql);
				if($rs) {
					$successMessage .= ' Die Warenbestellung wurde gespeichert.' .'<br />';
				}
				else {
					$errorMessage .= ' Die Warenbestellung konnte nicht gespeichert werden.' . '<br />';
				}
			}
			// BOF STORE MATERIAL ORDERS

		}
		else {
			if($_POST["editOrdersID"] != "") {
				$_REQUEST["editID"] = $_POST["editOrdersID"];
			}
			else {
				$_REQUEST["editID"] = 'NEW';
			}
			if(!$doAction) {
				$errorMessage .= ' Der Datensatz konnte nicht gespeichert werden. Bitte füllen Sie alle Pflichtfelder aus.' . '<br />';
			}
			if(!$checkCustomerEmail) {
				$errorMessage .= ' Die eingegebene Mail-Adresse ist nicht korrekt.' . '<br />';
			}
		}
	}
	// EOF STORE DATAS

	// BOF DELETE DATAS
		if($_POST["deleteDatas"] != "") {
			// BOF DELETE LOKAL FROM TABLE ORDERS
				$sql = "DELETE FROM `" . TABLE_ORDERS . "` WHERE `ordersID` = '".$_POST["editOrdersID"]."'";
				$rs = $dbConnection->db_query($sql);

				if($rs) {
					$successMessage .= ' Die Produktion wurde entfernt. ' . '<br />';
					
					// BOF DELETE LOKAL FROM TABLE TRANSFER ORDERS
						$sql = "
							DELETE
									FROM `" . TABLE_PRODUCTIONS_TRANSFER . "`
								WHERE 1
									AND `" . TABLE_PRODUCTIONS_TRANSFER . "`.`ordersProductionsTransferID` > 0
									AND `" . TABLE_PRODUCTIONS_TRANSFER . "`.`ordersProductionsTransferOrdersID` = '" . $_REQUEST["editOrdersID"] . "'
							";
						$rs = $dbConnection->db_query($sql);
		
						if($rs) {
							$successMessage .= ' Die TR-Produktion wurde lokal entfernt. ' . '<br />';
							
							// BOF DELETE EXTERN FROM TABLE TRANSFER ORDERS
								if(constant("DB_HOST_EXTERN_PRODUCTION") != '' && constant("DB_NAME_EXTERN_PRODUCTION") != '' && constant("DB_USER_EXTERN_PRODUCTION") != '' && constant("DB_PASSWORD_EXTERN_PRODUCTION")) {
									$dbConnection_ExternProduction = new DB_Connection(DB_HOST_EXTERN_PRODUCTION, '', DB_NAME_EXTERN_PRODUCTION, DB_USER_EXTERN_PRODUCTION, DB_PASSWORD_EXTERN_PRODUCTION);
									
									$sql = "
											DELETE
													FROM `" . TABLE_PRODUCTIONS_TRANSFER . "`
												WHERE 1
													AND `" . TABLE_PRODUCTIONS_TRANSFER . "`.`ordersProductionsTransferID` > 0
													AND `" . TABLE_PRODUCTIONS_TRANSFER . "`.`ordersProductionsTransferOrdersID` = '" . $_REQUEST["editOrdersID"] . "'
										";
									
									$rs = $dbConnection_ExternProduction->db_query($sql);
				
									if($rs) {
										$successMessage .= ' Die TR-Produktion wurde extern entfernt. ' . '<br />';
									}
									else {
										$errorMessage .= ' Die TR-Produktion konnte nicht extern entfernt werden. ' . '<br />';
									}
								}
							// EOF DELETE EXTERN FROM TABLE TRANSFER ORDERS
						}
						else {
							$errorMessage .= ' Die TR-Produktion konnte nicht lokal entfernt werden. ' . '<br />';
						}
					// EOF DELETE LOKAL FROM TABLE TRANSFER ORDERS					
					
				}
				else {
					$errorMessage .= ' Die Produktion konnte nicht entfernt werden. ' . '<br />';
				}
			// EOF DELETE LOKAL FROM TABLE ORDERS
		}
	// EOF DELETE DATAS

	// BOF READ DATAS
	if($_REQUEST["editID"] != "NEW" && $_REQUEST["editID"] != '') {
		$sql = "SELECT
						`ordersToDocumentsOrderID`,

						`ordersID`,
						`ordersBestellDatum`,
						`ordersStornoDatum`,
						`ordersFreigabeDatum`,
						`ordersFreigabeArt`,
						`ordersBelichtungsDatum`,
						`ordersProduktionsDatum`,
						`ordersAuslieferungsDatum`,
						`ordersKundennummer`,
						`ordersKundenName`,
						`ordersKommission`,
						`ordersInfo`,
						`ordersPLZ`,
						`ordersOrt`,
						`ordersLand`,
						`ordersArtikelMenge`,
						`ordersArtikelPrintColorsCount`,
						`ordersArtikelWithBorder`,
						`ordersArtikelWithClearPaint`,
						`ordersAufdruck`,
						`ordersSinglePreis`,
						`ordersTotalPreis`,
						`ordersArtikelBezeichnung`,
						`ordersArtikelID`,
						`ordersArtikelNummer`,
						`ordersArtikelKategorieID`,

						`ordersAdditionalArtikelKategorieID`,
						`ordersAdditionalArtikelMenge`,

						`ordersDruckFarbe`,
						`ordersAdditionalCosts`,
						`ordersAdditionalCostsID`,
						`ordersVertreter`,
						`ordersVertreterID`,
						`ordersMailAdress`,
						`ordersContactOthers`,
						`ordersMandant`,
						`ordersLieferwoche`,
						`ordersLieferDatum`,
						`ordersDruckerName`,
						`ordersNotizen`,
						`ordersStatus`,
						`ordersOrderType`,
						`ordersPerExpress`,
						`ordersBankAccountType`,
						`ordersPaymentType`,
						`ordersPaymentStatusType`,
						`ordersSourceType`,
						`ordersSourceName`,
						`editOrdersArtikelGewicht`,
						`editOrdersArtikelMaterial`,
						`editOrdersArtikelGesamtGewichtK`,
						`editOrdersVerpackungseinheiten`,
						`ordersUserID`,
						`ordersTimeCreated`,
						`ordersArchive`,
						`ordersDirectSale`,
						`ordersReklamationsgrund`,
						`ordersProductionPrintingPlant`,

						GROUP_CONCAT(
							`createdDocumentsFilename`,
							':',
							IF(`ordersToDocumentsStatus` = 'created', 'Datei erstellt (nicht gesendet)',
								if(`ordersToDocumentsStatus` = 'send', 'Datei erstellt und gesendet',
									'unbekannt'
								)
							)
							SEPARATOR '#'
						) AS `createdDocumentsFilename`,					

						`" . TABLE_CUSTOMERS . "`.`customersGruppe` AS `ordersKundengruppe`,
						
						`ordersPrintPlateNumber`

					FROM `" . TABLE_ORDERS . "`

					LEFT JOIN `" . TABLE_CUSTOMERS . "`
					ON(`" . TABLE_ORDERS . "`.`ordersKundennummer` = `" . TABLE_CUSTOMERS . "`.`customersKundennummer`)

					LEFT JOIN `" . TABLE_ORDERS_TO_DOCUMENTS . "`
					ON(`" . TABLE_ORDERS . "`.`ordersID` = `" . TABLE_ORDERS_TO_DOCUMENTS . "`.`ordersToDocumentsOrderID`)

					LEFT JOIN `" . TABLE_CREATED_DOCUMENTS . "`
					ON( `" . TABLE_ORDERS_TO_DOCUMENTS . "`.`ordersToDocumentsDocumentID` = `" . TABLE_CREATED_DOCUMENTS . "`.`createdDocumentsID`)

					WHERE `ordersID` = '".$_REQUEST["editID"]."'

					GROUP BY `ordersToDocumentsOrderID`
			";

			$rs = $dbConnection->db_query($sql);

			if($dbConnection->db_getMysqlNumRows($rs) > 0) {
				$orderDatas = array();
				list(
					$orderDatas["ordersToDocumentsOrderID"],

					$orderDatas["ordersID"],
					$orderDatas["ordersBestellDatum"],
					$orderDatas["ordersStornoDatum"],
					$orderDatas["ordersFreigabeDatum"],
					$orderDatas["ordersFreigabeArt"],
					$orderDatas["ordersBelichtungsDatum"],
					$orderDatas["ordersProduktionsDatum"],
					$orderDatas["ordersAuslieferungsDatum"],
					$orderDatas["ordersKundennummer"],
					$orderDatas["ordersKundenName"],
					$orderDatas["ordersKommission"],
					$orderDatas["ordersInfo"],
					$orderDatas["ordersPLZ"],
					$orderDatas["ordersOrt"],
					$orderDatas["ordersLand"],
					$orderDatas["ordersArtikelMenge"],
					$orderDatas["ordersArtikelPrintColorsCount"],
					$orderDatas["ordersArtikelWithBorder"],
					$orderDatas["ordersArtikelWithClearPaint"],
					$orderDatas["ordersAufdruck"],
					$orderDatas["ordersSinglePreis"],
					$orderDatas["ordersTotalPreis"],
					$orderDatas["ordersArtikelBezeichnung"],
					$orderDatas["ordersArtikelID"],
					$orderDatas["ordersArtikelNummer"],
					$orderDatas["ordersArtikelKategorieID"],

					$orderDatas["ordersAdditionalArtikelKategorieID"],
					$orderDatas["ordersAdditionalArtikelMenge"],

					$orderDatas["ordersDruckFarbe"],
					$orderDatas["ordersAdditionalCosts"],
					$orderDatas["ordersAdditionalCostsID"],
					$orderDatas["ordersVertreter"],
					$orderDatas["ordersVertreterID"],
					$orderDatas["ordersMailAdress"],
					$orderDatas["ordersContactOthers"],
					$orderDatas["ordersMandant"],
					$orderDatas["ordersLieferwoche"],
					$orderDatas["ordersLieferDatum"],
					$orderDatas["ordersDruckerName"],
					$orderDatas["ordersNotizen"],
					$orderDatas["ordersStatus"],
					$orderDatas["ordersOrderType"],
					$orderDatas["ordersPerExpress"],
					$orderDatas["ordersBankAccountType"],
					$orderDatas["ordersPaymentType"],
					$orderDatas["ordersPaymentStatusType"],
					$orderDatas["ordersSourceType"],
					$orderDatas["ordersSourceName"],
					$orderDatas["editOrdersArtikelGewicht"],
					$orderDatas["editOrdersArtikelMaterial"],
					$orderDatas["editOrdersArtikelGesamtGewichtK"],
					$orderDatas["editOrdersVerpackungseinheiten"],
					$orderDatas["ordersUserID"],
					$orderDatas["ordersTimeCreated"],
					$orderDatas["ordersArchive"],
					$orderDatas["ordersDirectSale"],
					$orderDatas["ordersReklamationsgrund"],
					$orderDatas["ordersProductionPrintingPlant"],
					$orderDatas["createdDocumentsFilename"],

					$orderDatas["ordersKundengruppe"],
					
					$orderDatas["ordersPrintPlateNumber"]

				) = mysqli_fetch_array($rs);

				$sql = "SELECT
							`supplierOrdersID`,
							`supplierOrdersSupplierID`,
							`supplierOrdersOrderProcessID`,
							`supplierOrdersAmount`,
							`supplierOrdersProductNumber`,
							`supplierOrdersProductName`,
							`supplierOrdersProductID`,
							`supplierOrdersProductKategorieID`,
							`supplierOrdersProductSinglePrice`,
							`supplierOrdersOrderDate`,
							`supplierOrdersDeliveryDate`,
							`supplierOrdersDeliveryDirectToCustomer`

							FROM `" . TABLE_SUPPLIER_ORDERS . "`
							WHERE `supplierOrdersOrderProcessID` = '".$_REQUEST["editID"]."'
					";

				$rs = $dbConnection->db_query($sql);
				if($dbConnection->db_getMysqlNumRows($rs) > 0) {
					$supplierOrderDatas = array();
					list(
						$supplierOrderDatas["supplierOrdersID"],
						$supplierOrderDatas["supplierOrdersSupplierID"],
						$supplierOrderDatas["supplierOrdersOrderProcessID"],
						$supplierOrderDatas["supplierOrdersAmount"],
						$supplierOrderDatas["supplierOrdersProductNumber"],
						$supplierOrderDatas["supplierOrdersProductName"],
						$supplierOrderDatas["supplierOrdersProductID"],
						$supplierOrderDatas["supplierOrdersProductKategorieID"],
						$supplierOrderDatas["supplierOrdersProductSinglePrice"],
						$supplierOrderDatas["supplierOrdersOrderDate"],
						$supplierOrderDatas["supplierOrdersDeliveryDate"],
						$supplierOrderDatas["supplierOrdersDeliveryDirectToCustomer"]
					) = mysqli_fetch_array($rs);
				}
			}
			else {
				$errorMessage .= 'Der gew&uuml;nschte Datensatz konnte nicht gefunden werden.' . '<br />';
			}
		}
		else if($_REQUEST["editID"] == "NEW" && ($_REQUEST["editCustomersID"] != "" || $_REQUEST["editCustomersNumber"] != "")) {

			$where = "";

			if($_REQUEST["editCustomersID"] != ""){
				$where = " AND `" . TABLE_CUSTOMERS . "`.`customersID` = '" . $_REQUEST["editCustomersID"] . "' ";
			}
			else if($_REQUEST["editCustomersNumber"] != ""){
				$where = " AND `" . TABLE_CUSTOMERS . "`.`customersKundennummer` = '" . $_REQUEST["editCustomersNumber"] . "' ";
			}

			$sql = "SELECT
						`" . TABLE_CUSTOMERS . "`.`customersKundennummer`,
						`" . TABLE_CUSTOMERS . "`.`customersFirmenname`,
						`" . TABLE_CUSTOMERS . "`.`customersCompanyPLZ`,
						`" . TABLE_CUSTOMERS . "`.`customersCompanyOrt`,
						`" . TABLE_CUSTOMERS . "`.`customersCompanyCountry`,
						`" . TABLE_CUSTOMERS . "`.`customersVertreterName`,
						`" . TABLE_CUSTOMERS . "`.`customersVertreterID`,
						`" . TABLE_CUSTOMERS . "`.`customersMail1`,
						`" . TABLE_CUSTOMERS . "`.`customersFax1`,
						'',
						'',
						'',
						`" . TABLE_CUSTOMERS . "`.`customersGruppe`

						FROM `" . TABLE_CUSTOMERS . "`

						WHERE 1
							" . $where . "

						LIMIT 1
				";


			$rs = $dbConnection->db_query($sql);
			$thisNumRows = $dbConnection->db_getMysqlNumRows($rs);

			if($thisNumRows == 1) {
				list(
					$orderDatas["ordersKundennummer"],
					$orderDatas["ordersKundenName"],
					$orderDatas["ordersPLZ"],
					$orderDatas["ordersOrt"],
					$orderDatas["ordersLand"],
					$orderDatas["ordersVertreter"],
					$orderDatas["ordersVertreterID"],
					$orderDatas["ordersMailAdress"],
					$orderDatas["ordersContactOthers"],
					$orderDatas["ordersMandant"],
					$orderDatas["ordersBankAccountType"],
					$orderDatas["ordersPaymentType"],
					$orderDatas["ordersKundengruppe"]
				) = mysqli_fetch_array($rs);
			}
			##########################
		}


/*
		else if($_REQUEST["editID"] != "NEW" || ($_REQUEST["editID"] == "NEW" && $_REQUEST["addOrdersID"] != ""))
		{
			$sql = "SELECT
					`" . TABLE_ORDERS . "`.`ordersKundennummer`,
					`" . TABLE_ORDERS . "`.`ordersKundenName`,
					`" . TABLE_ORDERS . "`.`ordersKommission`,
					`" . TABLE_ORDERS . "`.`ordersInfo`,
					`" . TABLE_ORDERS . "`.`ordersPLZ`,
					`" . TABLE_ORDERS . "`.`ordersOrt`,
					`" . TABLE_ORDERS . "`.`ordersVertreter`,
					`" . TABLE_ORDERS . "`.`ordersVertreterID`,
					`" . TABLE_ORDERS . "`.`ordersMailAdress`,
					`" . TABLE_ORDERS . "`.`ordersContactOthers`,
					`" . TABLE_ORDERS . "`.`ordersMandant`,
					`" . TABLE_ORDERS . "`.`ordersBankAccountType`,
					`" . TABLE_ORDERS . "`.`ordersPaymentType`

					FROM `" . TABLE_ORDERS . "`

					WHERE `" . TABLE_ORDERS . "`.`ordersID` = '".$_REQUEST["addOrdersID"]."'
			";

			$rs = $dbConnection->db_query($sql);

			$orderDatas = array();
			list(
				$orderDatas["ordersKundennummer"],
				$orderDatas["ordersKundenName"],
				$orderDatas["ordersKommission"],
				$orderDatas["ordersInfo"],
				$orderDatas["ordersPLZ"],
				$orderDatas["ordersOrt"],
				$orderDatas["ordersVertreter"],
				$orderDatas["ordersVertreterID"],
				$orderDatas["ordersMailAdress"],
				$orderDatas["ordersContactOthers"],
				$orderDatas["ordersMandant"],
				$orderDatas["ordersBankAccountType"],
				$orderDatas["ordersPaymentType"],
				$orderDatas["ordersKundengruppe"]
			) = mysqli_fetch_array($rs);
		}
*/
	else if($_POST["copyDatas"] != "" || $_GET["copyID"] != '') {
			if($_POST["copyDatas"] != '') {
				$thisCopyID = $thisStoreID;
			}
			else if($_GET["copyID"] != '') {
				$thisCopyID = $_GET["copyID"];
			}
			$_REQUEST["editID"] = "NEW";

			$sql = "SELECT
								`ordersKundennummer`,
								`ordersStatus`,
								`ordersSourceType`,
								`ordersSourceName`,
								`editOrdersArtikelGewicht`,
								`editOrdersArtikelMaterial`,
								`editOrdersArtikelGesamtGewichtK`,
								`editOrdersVerpackungseinheiten`,
								`ordersBestellDatum`,
								`ordersKundenName`,
								`ordersPLZ`,
								`ordersOrt`,
								`ordersVertreter`,
								`ordersVertreterID`,
								`ordersMailAdress`,
								`ordersContactOthers`,
								`ordersMandant`,
								`ordersOrderType`,
								`ordersBankAccountType`,
								`ordersPaymentType`

								FROM `" . TABLE_ORDERS . "`

								WHERE `ordersID` = '".$thisCopyID."'
						";

						$rs = $dbConnection->db_query($sql);

						$orderDatas = array();
						list(
							$orderDatas["ordersKundennummer"],
							$orderDatas["ordersStatus"],
							$orderDatas["ordersSourceType"],
							$orderDatas["ordersSourceName"],
							$orderDatas["editOrdersArtikelGewicht"],
							$orderDatas["editOrdersArtikelMaterial"],
							$orderDatas["editOrdersArtikelGesamtGewichtK"],
							$orderDatas["editOrdersVerpackungseinheiten"],
							$orderDatas["ordersBestellDatum"],
							$orderDatas["ordersKundenName"],
							$orderDatas["ordersPLZ"],
							$orderDatas["ordersOrt"],
							$orderDatas["ordersVertreter"],
							$orderDatas["ordersVertreterID"],
							$orderDatas["ordersMailAdress"],
							$orderDatas["ordersContactOthers"],
							$orderDatas["ordersMandant"],
							$orderDatas["ordersOrderType"],
							$orderDatas["ordersBankAccountType"],
							$orderDatas["ordersPaymentType"]
						) = mysqli_fetch_array($rs);
		}
	// EOF READ DATAS

	// BOF DUPLICATE DATAS
	else if($_POST["duplicateDatas"] != "" && $_POST["duplicateID"]) {
			$thisCopyID = $_POST["duplicateID"];
			$_REQUEST["editID"] = "NEW";

			$sql = "SELECT
								`" . TABLE_ORDERS . "`.`ordersKundennummer`,
								'2' AS `ordersStatus`,
								'' AS `ordersSourceType`,
								`" . TABLE_ORDERS . "`.`ordersSourceName`,
								`" . TABLE_ORDERS . "`.`editOrdersArtikelGewicht`,
								`" . TABLE_ORDERS . "`.`editOrdersArtikelMaterial`,
								`" . TABLE_ORDERS . "`.`editOrdersArtikelGesamtGewichtK`,
								`" . TABLE_ORDERS . "`.`editOrdersVerpackungseinheiten`,
								'" . date("d.m.Y") . "' AS `ordersBestellDatum`,
								`" . TABLE_ORDERS . "`.`ordersKundenName`,
								`" . TABLE_ORDERS . "`.`ordersPLZ`,
								`" . TABLE_ORDERS . "`.`ordersOrt`,
								`" . TABLE_ORDERS . "`.`ordersVertreter`,
								`" . TABLE_ORDERS . "`.`ordersVertreterID`,
								`" . TABLE_ORDERS . "`.`ordersMailAdress`,
								`" . TABLE_ORDERS . "`.`ordersContactOthers`,
								'" . strtoupper(MANDATOR). "' AS `ordersMandant`,
								`" . TABLE_ORDERS . "`.`ordersMandant` AS `ordersMandantOriginal`,
								'2' AS `ordersOrderType`,
								`" . TABLE_ORDERS . "`.`ordersBankAccountType`,
								`" . TABLE_ORDERS . "`.`ordersPaymentType`,

								'' AS `ordersArtikelMenge`,
								`" . TABLE_ORDERS . "`.`ordersArtikelPrintColorsCount`,
								`" . TABLE_ORDERS . "`.`ordersArtikelWithBorder`,
								`" . TABLE_ORDERS . "`.`ordersArtikelWithClearPaint`,
								`" . TABLE_ORDERS . "`.`ordersAufdruck`,
								`" . TABLE_ORDERS . "`.`ordersArtikelBezeichnung`,
								`" . TABLE_ORDERS . "`.`ordersArtikelID`,
								`" . TABLE_ORDERS . "`.`ordersArtikelNummer`,
								`" . TABLE_ORDERS . "`.`ordersArtikelKategorieID`,

								`" . TABLE_ORDERS . "`.`ordersDruckFarbe`,
								`" . TABLE_ORDERS . "`.`ordersAdditionalCostsID`,

								`" . TABLE_ORDERS . "`.`ordersKommission`,
								'' AS `ordersInfo`,

								`" . TABLE_CUSTOMERS . "`.`customersFirmenname`,

								`" . TABLE_CUSTOMERS . "`.`customersGruppe`

								FROM `" . TABLE_ORDERS . "`

								LEFT JOIN `" . TABLE_CUSTOMERS ."`
								ON(`" . TABLE_ORDERS . "`.`ordersKundennummer` = `" . TABLE_CUSTOMERS . "`.`customersKundennummer`)

								WHERE `ordersID` = '".$thisCopyID."'
							";

						$rs = $dbConnection->db_query($sql);

						$orderDatas = array();
						list(
							$orderDatas["ordersKundennummer"],
							$orderDatas["ordersStatus"],
							$orderDatas["ordersSourceType"],
							$orderDatas["ordersSourceName"],
							$orderDatas["editOrdersArtikelGewicht"],
							$orderDatas["editOrdersArtikelMaterial"],
							$orderDatas["editOrdersArtikelGesamtGewichtK"],
							$orderDatas["editOrdersVerpackungseinheiten"],
							$orderDatas["ordersBestellDatum"],
							$orderDatas["ordersKundenNameSource"],
							$orderDatas["ordersPLZ"],
							$orderDatas["ordersOrt"],
							$orderDatas["ordersVertreter"],
							$orderDatas["ordersVertreterID"],
							$orderDatas["ordersMailAdress"],
							$orderDatas["ordersContactOthers"],
							$orderDatas["ordersMandant"],
							$orderDatas["ordersMandantOriginal"],
							$orderDatas["ordersOrderType"],
							$orderDatas["ordersBankAccountType"],
							$orderDatas["ordersPaymentType"],

							$orderDatas["ordersArtikelMenge"],
							$orderDatas["ordersArtikelPrintColorsCount"],
							$orderDatas["ordersArtikelWithBorder"],
							$orderDatas["ordersArtikelWithClearPaint"],
							$orderDatas["ordersAufdruck"],
							$orderDatas["ordersArtikelBezeichnung"],
							$orderDatas["ordersArtikelID"],
							$orderDatas["ordersArtikelNummer"],
							$orderDatas["ordersArtikelKategorieID"],

							$orderDatas["ordersDruckFarbe"],
							$orderDatas["ordersAdditionalCostsID"],

							$orderDatas["ordersKommission"],
							$orderDatas["ordersInfo"],

							$orderDatas["ordersKundenName"],

							$orderDatas["ordersKundengruppe"]
						) = mysqli_fetch_array($rs);

			// BOF CHECK PRODUCT_NUMBER AND PRODUCT_NAME IF ORIGIN ORDER WAS B3 AND NOE BCTR
				if(USE_ONLY_PRODUCTS_FROM_BCTR){
					if($orderDatas["ordersMandant"] != $orderDatas["ordersMandantOriginal"] && $orderDatas["ordersMandantOriginal"] == 'B3'){
						$sql_getProductData_BCTR = "
								SELECT
									`" . TABLE_ORDER_CATEGORIES . "`.`orderCategoriesName_bctr`,
									`" . TABLE_ORDER_CATEGORIES . "`.`orderCategoriesPartNumber_bctr`

								FROM `" . TABLE_ORDER_CATEGORIES . "`
								WHERE 1
									AND `" . TABLE_ORDER_CATEGORIES . "`.`orderCategoriesPartNumber_b3` = '" . $orderDatas["ordersArtikelNummer"] . "'
									AND `" . TABLE_ORDER_CATEGORIES . "`.`orderCategoriesID` = '" . $orderDatas["ordersArtikelKategorieID"] . "'
								LIMIT 1
							";

						$rs_getProductData_BCTR = $dbConnection->db_query($sql_getProductData_BCTR);
						$$orderDatasProductNew = array();
						list(
							$orderDatasProductNew["orderCategoriesName_bctr"],
							$orderDatasProductNew["orderCategoriesPartNumber_bctr"]
						) = mysqli_fetch_array($rs_getProductData_BCTR);


						if($orderDatasProductNew["orderCategoriesName_bctr"] != '' && $orderDatasProductNew["orderCategoriesPartNumber_bctr"] != ''){
							$orderDatas["ordersArtikelBezeichnung"] = $orderDatasProductNew["orderCategoriesName_bctr"];
							$orderDatas["ordersArtikelNummer"] = $orderDatasProductNew["orderCategoriesPartNumber_bctr"];
						}
					}
				}
			// EOF CHECK PRODUCT_NUMBER AND PRODUCT_NAME IF ORIGIN ORDER WAS B3 AND NOE BCTR
		}
	// EOF DUPLICATE DATAS

	if($_REQUEST["editID"] != "" && $_REQUEST["editID"] != "NEW") {
		// BOF CHECK IF PRODUCTION IS USED
		$sql = "
			SELECT
				SUM(`tempTable`.`checkProductionID`) AS `checkProductionIsUsed`

				FROM (

					SELECT
						COUNT(`orderDocumentDetailOrderID`) AS `checkProductionID`
						FROM `" . TABLE_ORDER_OFFERS_DETAILS . "`
						WHERE `orderDocumentDetailOrderID` = '" . $_REQUEST["editID"] . "'

					UNION

					SELECT
						COUNT(`orderDocumentDetailOrderID`) AS `checkProductionID`
						FROM `" . TABLE_ORDER_CONFIRMATIONS_DETAILS . "`
						WHERE `orderDocumentDetailOrderID` = '" . $_REQUEST["editID"] . "'

					UNION

					SELECT
						COUNT(`orderDocumentDetailOrderID`) AS `checkProductionID`
						FROM `" . TABLE_ORDER_DELIVERIES_DETAILS . "`
						WHERE `orderDocumentDetailOrderID` = '" . $_REQUEST["editID"] . "'

					UNION

					SELECT
						COUNT(`orderDocumentDetailOrderID`) AS `checkProductionID`
						FROM `" . TABLE_ORDER_INVOICES_DETAILS . "`
						WHERE `orderDocumentDetailOrderID` = '" . $_REQUEST["editID"] . "'

					UNION

					SELECT
						COUNT(`orderDocumentDetailOrderID`) AS `checkProductionID`
						FROM `" . TABLE_ORDER_CREDITS_DETAILS . "`
						WHERE `orderDocumentDetailOrderID` = '" . $_REQUEST["editID"] . "'

					UNION

					SELECT
						COUNT(`orderDocumentDetailOrderID`) AS `checkProductionID`
						FROM `" . TABLE_ORDER_REMINDERS_DETAILS . "`
						WHERE `orderDocumentDetailOrderID` = '" . $_REQUEST["editID"] . "'

					UNION

					SELECT
						COUNT(`orderDocumentDetailOrderID`) AS `checkProductionID`
						FROM `" . TABLE_ORDER_LETTERS_DETAILS . "`
						WHERE `orderDocumentDetailOrderID` = '" . $_REQUEST["editID"] . "'


				) AS `tempTable`
		";
#dd('sql');
		$rs = $dbConnection->db_query($sql);
		list($checkProductionIsUsed) = mysqli_fetch_array($rs);
#dd('checkProductionIsUsed');
	// EOF OF CHECK IF PRODUCTION IS USED
	}

	// BOF GET CUSTOMER GROUPS
		$thisCusomerGroupID = 0;
		if($orderDatas["ordersKundengruppe"] != ''){
			$arrCustomerGroups = explode(";", $orderDatas["ordersKundengruppe"]);
			
			// BOF OTLG HANDLING // GROUP-ID:8
				if(in_array("8", $arrCustomerGroups)){
					$thisCusomerGroupID = 8;
				}
			// EOF OTLG HANDLING // GROUP-ID:8
		}
	// EOF GET CUSTOMER GROUPS
	
	// BOF GET ORDER PRODUCTS / CATEGORIES
		if($_COOKIE["isAdmin"] == '1'){
			// $arrLoadedProducts = getOrderProducts('all', MANDATOR, $thisCusomerGroupID);
		}
		else {
			// $arrOrderCategoriesTypeDatas = getOrderCategories();
		}
	// EOF GET ORDER PRODUCTS / CATEGORIES

	if($_REQUEST["storeDatasAndGotToCustomer"] != ""){
		header('location:' . PAGE_EDIT_CUSTOMER . '?editCustomerNumber=' . $_REQUEST["editOrdersKundennummer"]);
	}
?>
<?php
	require_once('inc/headerHTML.inc.php');
	// print_r($orderDatas);exit();
	if($_REQUEST["editID"] == "NEW") {
		$thisTitle = "Neuen Produktions-Vorgang anlegen";
		if($orderDatas["ordersKundennummer"] != '') {
			$thisTitle .= ': <span  class="headerSelectedEntry">'.$orderDatas["ordersKundennummer"].' &bull; '.$orderDatas["ordersKundenName"].'</span>';
		}
		$thisIcon = "addProcess.png";
	}
	else {
		$thisTitle = "Vorhandenen Produktions-Vorgang bearbeiten";
		if($orderDatas["ordersKundennummer"] != '') {
			$thisTitle .= ': <span  class="headerSelectedEntry">'.$orderDatas["ordersKundennummer"].' &bull; '.$orderDatas["ordersKundenName"].'</span>';
		}
		$thisIcon = "create.png";
	}
	$headerHTML = preg_replace("/{###TITLE###}/", strip_tags($thisTitle), $headerHTML);
	echo $headerHTML;

	require_once(FILE_MENUE_TOP);

	if($dbConnection->db_displayErrors() != "") {
		$errorMessage .= $dbConnection->db_displayErrors(). '<br />';
	}
?>

<div id="mainArea">
	<div id="mainContent">
		<?php require_once(FILE_MENUE_SIDEBAR); ?>
<div id="contentArea">
	<div id="contentAreaElements">
		<h1><?php if(SHOW_PAGE_TITLE_ICONS) { echo '<img src="' . PATH_ICONS_MENUE_TITLES . $thisIcon . '" alt="" />'; } ?> <?php echo $thisTitle; ?></h1>

		<?php if($_REQUEST["editID"] != "NEW" && isset($_GET["editID"]) && !empty($orderDatas)) { ?>
			<div class="menueActionsArea">
				<?php if($arrGetUserRights["editCustomers"]) { ?><a href="<?php echo PAGE_EDIT_CUSTOMER; ?>?editID=NEW" class="linkButton" title="Neuen Kunden anlegen" >Neukunden anlegen</a><?php } ?>

				<?php if($arrGetUserRights["editCustomers"] && $orderDatas["ordersKundennummer"] != '') { ?><a href="<?php echo PAGE_EDIT_CUSTOMER; ?>?editCustomerNumber=<?php echo $orderDatas["ordersKundennummer"]; ?>" class="linkButton" title="Neuen Kunden anlegen" >Kunden bearbeiten</a><?php } ?>

				<?php if($arrGetUserRights["editOffers"]) { ?><a href="<?php echo PAGE_CREATE_OFFER; ?>&amp;editID=<?php echo $orderDatas["ordersKundennummer"]; ?>" class="linkButton" title="Angebote schreiben" >Angebot</a><?php } ?>
				<?php if($arrGetUserRights["editConfirmations"]) { ?><a href="<?php echo PAGE_CREATE_CONFIRMATION; ?>&amp;editID=<?php echo $orderDatas["ordersKundennummer"]; ?>" class="linkButton" title="Auftragsbest&auml;tigung schreiben" >Auftragsbest&auml;tigung</a><?php } ?>
				<?php if($arrGetUserRights["editInvoices"]) { ?><a href="<?php echo PAGE_CREATE_INVOICE; ?>&amp;editID=<?php echo $orderDatas["ordersKundennummer"]; ?>" class="linkButton" title="Rechnung schreiben" >Rechnung</a><?php } ?>
				<?php if($arrGetUserRights["editDeliveries"]) { ?><a href="<?php echo PAGE_CREATE_DELIVERY; ?>&amp;editID=<?php echo $orderDatas["ordersKundennummer"]; ?>" class="linkButton" title="Lieferschein schreiben" >Lieferschein</a><?php } ?>
				<?php if($arrGetUserRights["editReminders"]) { ?><a href="<?php echo PAGE_CREATE_REMINDER; ?>&amp;editID=<?php echo $orderDatas["ordersKundennummer"]; ?>" class="linkButton" title="Mahnung schreiben" >Mahnung</a><?php } ?>
				<?php if($arrGetUserRights["editCredits"]) { ?><a href="<?php echo PAGE_CREATE_CREDIT; ?>&amp;editID=<?php echo $orderDatas["ordersKundennummer"]; ?>" class="linkButton" title="Gutschrift schreiben" >Gutschrift</a><?php } ?>
				<div class="clear"></div>
			</div>
		<?php } ?>

		<?php if($_REQUEST["editCustomersID"] != ""){ ?>
			<p><a href="<?php echo PAGE_EDIT_CUSTOMER; ?>?editID=<?php echo $_REQUEST["editCustomersID"]; ?>" class="linkButton">Zur&uuml;ck zu den Kundendaten</a></p>
			<div class="clear"></div>
		<?php } ?>

		<div class="contentDisplay">

			<?php displayMessages(); ?>

			<?php if($_REQUEST["editID"] == "") { ?>
			<?php if($thisOrdersID != "") { ?>
			<p><a href="<?php echo PAGE_EDIT_PROCESS; ?>?editID=<?php echo $thisStoreID; ?>" class="linkButton">Gespeicherten Produktions-Vorgang bearbeiten</a></p>
			<?php } ?>
			<p><a href="<?php echo PAGE_EDIT_PROCESS; ?>?editID=NEW" class="linkButton">Neuen Produktions-Vorgang anlegen</a></p>

			<?php if($thisOrdersID != "") { ?>
			<p><a href="<?php echo PAGE_EDIT_PROCESS; ?>?copyID=<?php echo $thisStoreID; ?>" class="linkButton">Neuen Produktions-Vorgang zum Kunden hinzuf&uuml;gen</a></p>
			<?php } ?>

			<?php if($_POST["editOrdersKundennummer"] != "") { ?>
			<p><a href="<?php echo PAGE_EDIT_CUSTOMER; ?>?editCustomerNumber=<?php echo $_POST["editOrdersKundennummer"]; ?>" class="linkButton">Zu den Kundendaten</a></p>
			<?php } ?>
			<?php if($_REQUEST["jsonReturnParams"] != ''){ ?>
			<p><a href="<?php echo PAGE_DISPLAY_ORDERS_OVERVIEW; ?>?jsonReturnParams=<?php echo rawurlencode($_REQUEST["jsonReturnParams"]); ?>" class="linkButton">Zur Produktions-Vorgangs-&Uuml;bersicht</a></p>
			<?php } else { ?>
			<p><a href="<?php echo PAGE_DISPLAY_ORDERS_OVERVIEW; ?>?searchCustomerNumber=<?php echo $_REQUEST["editOrdersKundennummer"]; ?>" class="linkButton">Zur Produktionsliste wechseln</a></p>
			<?php } ?>
			<?php
				}
			?>
			<?php
				if($_REQUEST["editID"] != "")
				{
			?>
			<div class="adminInfo">Datensatz-ID: <?php echo $orderDatas["ordersID"]; ?></div>
			<div class="adminEditArea">

			<p class="warningArea">Pflichtfelder <span class="dutyField">(*)</span> m&uuml;ssen ausgef&uuml;llt werden!</p>

		<form name="editOrderDatas" method="post" action="<?php echo $_SERVER["PHP_SELF"]; ?>" enctype="multipart/form-data">
			<input type="hidden" name="editOrdersID" value="<?php echo $orderDatas["ordersID"]; ?>" />

			<input type="hidden" name="editOrdersIsOTLG" id="editOrdersIsOTLG" value="<?php echo $isOTLG; ?>" />

			<fieldset>
				<legend>Produktions-Vorgangs-Daten</legend>
				<table border="0" width="100%" cellspacing="0" cellpadding="0">
					<tr>
						<td width="200"><b>Mandant:</b></td>
						<td>
							<!--<select style="font-weight:bold;" name="editOrdersMandant" id="editOrdersMandant" class="inputSelect_510" onchange="getOrderProductCategoriesNeu('<?php echo $thisCusomerGroupID; ?>', 'all', '#editOrdersMandant', '#editOrdersArtikelKategorieID', '<?php echo $orderDatas["ordersArtikelKategorieID"]; ?>');getOrderProductCategoriesNeu('<?php echo $thisCusomerGroupID; ?>', '002', '#editOrdersMandant', '#editOrdersAdditionalArtikelKategorieID', '<?php echo $orderDatas["ordersAdditionalArtikelKategorieID"]; ?>');">-->
							<select style="font-weight:bold;" name="editOrdersMandant" id="editOrdersMandant" class="inputSelect_510" onchange="getOrderProductCategoriesNeu('<?php echo $thisCusomerGroupID; ?>', 'all', '#editOrdersMandant', '#editOrdersArtikelKategorieID', '<?php echo $orderDatas["ordersArtikelKategorieID"]; ?>');">
								<option value="BCTR" <?php if(strtolower($orderDatas["ordersMandant"]) == 'bctr' || ($orderDatas["ordersMandant"] == '' && strtolower(MANDATOR) == 'bctr')) { echo ' selected="selected" '; }?> >Burhan CTR</option>
								<option value="B3" <?php if(strtolower($orderDatas["ordersMandant"]) == 'b3' || ($orderDatas["ordersMandant"] == '' && strtolower(MANDATOR) == 'b3')) { echo ' selected="selected" '; }?> >B3</option>
							</select>
						</td>
					</tr>
					<tr>
						<td width="200"><b>Eingangsdatum:</b></td>
						<td>
							<?php
								$thisDate = formatDate($orderDatas["ordersBestellDatum"], "split");
							?>
							<input type="text" name="editOrdersBestellDatum" id="editOrdersBestellDatum" maxlength="2" class="inputField_100" readonly="readonly" value="<?php if($orderDatas["ordersBestellDatum"] == "" || $orderDatas["ordersBestellDatum"] == "0000-00-00") { echo date("d.m.Y"); } else { echo formatDate($orderDatas["ordersBestellDatum"], "display"); } ?>" />
						</td>
					</tr>
					<tr>
						<td width="200"><b>Stornodatum:</b></td>
						<td>
							<?php
								$thisDate = formatDate($orderDatas["ordersStornoDatum"], "split");
							?>
							<input type="text" name="editOrdersStornoDatum" id="editOrdersStornoDatum" maxlength="2" class="inputField_100" readonly="readonly" value="<?php if($orderDatas["ordersStornoDatum"] == "" || $orderDatas["ordersStornoDatum"] == "0000-00-00") { } else { echo formatDate($orderDatas["ordersStornoDatum"], "display"); } ?>" />
						</td>
					</tr>
					<tr>
						<td><b>Bestell-Art:</b></td>
						<td>
							<select style="font-weight:bold;" name="editOrdersOrderType" class="inputSelect_510">
								<option value=""> - </option>
								<?php
									if(!empty($arrOrderTypeDatas)) {
										foreach($arrOrderTypeDatas as $thisKey => $thisValue) {
											$selected = '';
											if($thisKey == $orderDatas["ordersOrderType"]) {
												$selected = ' selected="selected" ';
											}
											echo '
												<option value="' . $thisKey . '" '.$selected.' >' . htmlentities($arrOrderTypeDatas[$thisKey]["orderTypesName"]) . '</option>
											';
										}
									}
								?>
							</select>
							<br />
							<?php
								$checked = '';
								if($orderDatas["ordersPerExpress"] == '1') {
									$checked = ' checked="checked" ';
								}
							?>
							<input type="checkbox" name="editOrdersPerExpress" <?php echo $checked ; ?> value="1" /> <b>als Express-Bestellung</b>
							<br />
							<input type="checkbox" name="editOrdersDirectSale" value="1" <?php if($orderDatas["ordersDirectSale"] == '1'){ echo ' checked="checked" '; } ?> />	 <b>Direktverkauf durch unseren Vertreter</b>
						</td>
					</tr>
					<tr>
						<td><b>Freigabedatum:</b></td>
						<td>
							<?php
								$thisDate = formatDate($orderDatas["ordersFreigabeDatum"], "display");
							?>
							<input type="text" name="editOrdersFreigabeDatum" id="editOrdersFreigabeDatum" maxlength="2" class="inputField_100" readonly="readonly" value="<?php echo formatDate($orderDatas["ordersFreigabeDatum"], "display"); ?>" />
						</td>
					</tr>
					<tr>
						<td><b>Freigabeart:</b></td>
						<td>
							<select name="editOrdersFreigabeArt" id="editOrdersFreigabeArt" class="inputSelect_100">
								<option value="" ></option>
							<?php
								$arrReleaseTypes = array('Fax', 'Mail');
								foreach($arrReleaseTypes as $thisReleaseType) {
									$selected = '';
									if($orderDatas["ordersFreigabeArt"] == $thisReleaseType) {
										$selected = ' selected="selected" ';
									}
									echo '<option value="' . $thisReleaseType . '"'.$selected.' >' . htmlentities($thisReleaseType) . '</option>';
								}
							?>
							</select>
						</td>
					</tr>
					<tr>
						<td><b>Belichtungsdatum:</b></td>
						<td>
							<?php
								$thisDate = formatDate($orderDatas["ordersBelichtungsDatum"], "split");
							?>
							<input type="text" name="editOrdersBelichtungsDatum" id="editOrdersBelichtungsDatum" maxlength="2" class="inputField_100" readonly="readonly" value="<?php echo formatDate($orderDatas["ordersBelichtungsDatum"], "display"); ?>" />
						</td>
					</tr>
					<tr>
						<td><b>Produktionsdatum:</b></td>
						<td>
							<?php
								$thisDate = formatDate($orderDatas["ordersProduktionsDatum"], "split");
							?>
							<input type="text" name="editOrdersProduktionsDatum" id="editOrdersProduktionsDatum" maxlength="2" class="inputField_100" readonly="readonly" value="<?php echo formatDate($orderDatas["ordersProduktionsDatum"], "display"); ?>" />
							<span>
								<?php
									if($orderDatas["ordersProductionPrintingPlant"] == ''){
										$orderDatas["ordersProductionPrintingPlant"] = 'DE';
									}
									$thisOrdersProductionPrintingPlant = $orderDatas["ordersProductionPrintingPlant"];
								?>
																<input type="text" name="editOrdersProductionPrintingPlant" id="editOrdersProductionPrintingPlant" class="inputField_30" value="<?php echo $thisOrdersProductionPrintingPlant; ?>" <?php if(!$arrGetUserRights["adminArea"]){ echo ' readonly="readonly" '; } ?> />

								<!--
								<input type="hidden" name="editOrdersProductionPrintingPlant" id="editOrdersProductionPrintingPlant" value="<?php echo $thisOrdersProductionPrintingPlant; ?>" readonly="readonly" />
								-->
							</span>
						</td>
					</tr>
					<tr>
						<td><b>Auslieferungsdatum:</b></td>
						<td>
							<?php
								$thisDate = formatDate($orderDatas["ordersAuslieferungsDatum"], "split");
							?>
							<input type="text" name="editOrdersAuslieferungsDatum" id="editOrdersAuslieferungsDatum" class="inputField_100" readonly="readonly" value="<?php echo formatDate($orderDatas["ordersAuslieferungsDatum"], "display"); ?>" />
						</td>
					</tr>
					<tr>
						<td><b>Bestell-Status:</b></td>
						<td>
							<input type="hidden" name="storedStatus" id="storedStatus" value="<?php echo $orderDatas["ordersStatus"]; ?>" />
							<select style="font-weight:bold;" name="editOrdersStatus" class="inputSelect_510">
								<option value=""> - </option>
								<?php
									if(!empty($arrOrderStatusTypeDatas)) {
										foreach($arrOrderStatusTypeDatas as $thisKey => $thisValue) {
											$selected = '';
											if($thisKey == $orderDatas["ordersStatus"]) {
												$selected = ' selected="selected" ';
											}
											echo '
												<option value="' . $thisKey . '" '.$selected.' >' . htmlentities($arrOrderStatusTypeDatas[$thisKey]["orderStatusTypesName"]) . '</option>
											';
										}
									}
								?>
							</select>
						</td>
					</tr>
					<tr>
						<td width="200"><b>Bestell-Quelle:</b></td>
						<td>
							<select name="editOrdersSourceType" class="inputSelect_510">
								<option value=""> - </option>
								<?php
									if(!empty($arrOrdersSourceTypeDatas)) {
										foreach($arrOrdersSourceTypeDatas as $thisKey => $thisValue) {
											$selected = '';
											if($orderDatas["ordersSourceType"] != "") {
												if($thisKey == $orderDatas["ordersSourceType"]) {
													$selected = ' selected="selected" ';
												}
											}
											else {
												if($thisKey == 2) {
													$selected = ' selected="selected" ';
												}
											}
											echo '
												<option value="' . $thisKey . '" '.$selected.' >' . htmlentities($arrOrdersSourceTypeDatas[$thisKey]["ordersSourceTypesName"]).'</option>';
										}
									}
								?>
							</select>
						</td>
					</tr>
					<tr>
						<td width="200"><b>Bestell-Person:</b></td>
						<td>
							<input type="text" name="editOrdersSourceName" class="inputField_510" value="<?php echo $orderDatas["ordersSourceName"]; ?>" />
						</td>
					</tr>
					<?php
						echo '<tr>';
						#echo '<td><b>Vertreter dieses Kunden:</b></td>';
						echo '<td style="color:#FF0000;"><b>Vertreter dieses Auftrages:</b></td>';
						echo '<td>';
						echo '<input type="text" name="editOrdersVertreterName" id="editOrdersVertreterName" class="inputField_510" style="border:1px solid #FF0000; font-weight:bold;color:#FF0000;" value="' . htmlentities($arrAgentDatas2[$orderDatas["ordersVertreterID"]]["customersFirmenname"]) . '" />';
						echo '<input type="hidden" name="editOrdersVertreterID" id="editOrdersVertreterID" value="' . ($orderDatas["ordersVertreterID"]) . '" />';
						echo ' <span class="buttonRemoveSalesman"><img src="layout/icons/iconDelete.png" width="16" height="16" alt="" title="Vertreter entfernen" /></span>';
						echo '<p class="infoArea" style="width:502px;">Wenn dieser Auftrag vom Vertreter eingereicht wurde, bitte hier den Vertreter eintragen.</p>';
						echo '</td>';
						echo '</tr>';
					?>
					<tr>
						<td><b>Lieferwoche (KW):</b></td>
						<td>
							<select name="editOrdersLieferwoche" class="inputSelect_100">
								<option value=""> --- Bitte w&auml;hlen --- </option>
								<?php
									$thisDefaultLieferwoche = (date("W") + MIN_SHIPPING_TIME);
									if($thisDefaultLieferwoche > 52) {
										$thisDefaultLieferwoche = $thisDefaultLieferwoche - 52;
									}
									for($i = 1 ; $i < 54 ; $i++) {
										$w = $i;
										if($w < 10) { $w = "0".$w; };
										$thisSelected = '';
										if($orderDatas["ordersLieferwoche"] != "") {
											if($w == $orderDatas["ordersLieferwoche"]) {
												$thisSelected = ' selected="selected" ';
											}
										}
										else if($w == $thisDefaultLieferwoche) {
											$thisSelected = ' selected="selected" ';
										}
										echo '<option value="'.$w.'" '.$thisSelected.' >' . htmlentities($w) . '</option>';
									}
								?>
							</select>
						</td>
					</tr>
					<tr>
						<td><b>Kundennummer:</b></td>
						<td>
							<input type="text" name="editOrdersKundennummer" id="editOrdersKundennummer" class="inputField_100" style="font-weight:bold;" value="<?php echo $orderDatas["ordersKundennummer"]; ?>" onclick="findExistingCustomer(); " />
							<?php
								if($orderDatas["ordersKundennummer"] != ''){
									echo '<a href="' . PAGE_EDIT_CUSTOMER . '?searchCustomerNumber=' . $orderDatas["ordersKundennummer"] . '">';
									echo '<img src="layout/menueIcons/menueQuicklinks/customers.png" width="16" height="16" style="margin: 0 0 -4px 4px;" title="Die Kundendaten dieses Kunden anzeigen" alt="Kunden anzeigen" /> ';
									echo '</a>';
								}
							?>
						</td>
					</tr>
					<tr>
						<td><b>Kundenname:</b></td>
						<td><input type="text" name="editOrdersKundenName" id="editOrdersKundenName" class="inputField_510" style="font-weight:bold;" value="<?php echo ($orderDatas["ordersKundenName"]); ?>" /></td>
					</tr>
					<tr>
						<td><b>Kommission:</b></td>
						<td><input type="text" name="editOrdersKommission" id="editOrdersKommission" class="inputField_510" value="<?php echo ($orderDatas["ordersKommission"]); ?>" /></td>
					</tr>
					<tr>
						<td><b>Info / Notiz:</b></td>
						<td><input type="text" name="editOrdersInfo" id="editOrdersInfo" class="inputField_510" value="<?php echo ($orderDatas["ordersInfo"]); ?>" /></td>
					</tr>
					<tr>
						<td width="200"><b>Aufdruck:</b></td>
						<td><input type="text" name="editOrdersAufdruck" id="editOrdersAufdruck" class="inputField_510" value="<?php echo $orderDatas["ordersAufdruck"]; ?>" /></td>
					</tr>
					
					<tr>
						<td><b>PLZ / Ort / Land:</b></td>
						<td>
							<input type="text" name="editOrdersPLZ" id="editOrdersPLZ" class="inputField_70" value="<?php echo $orderDatas["ordersPLZ"]; ?>" readonly="readonly" /> /
							<input type="text" name="editOrdersOrt" id="editOrdersOrt" class="inputField_200" value="<?php echo ($orderDatas["ordersOrt"]); ?>" readonly="readonly" />

							<select name="editOrdersLand" id="editOrdersLand" class="inputSelect_200" >
								<option value=""> - </option>
								<?php
									if(!empty($arrCountryTypeDatas)) {
										foreach($arrCountryTypeDatas as $thisKey => $thisValue) {
											$selected = '';
											if($_REQUEST["editID"] == "NEW" && $_REQUEST["editCustomersID"] == '' && $thisKey == 81) {
												$selected = ' selected="selected" ';
											}
											else if($_REQUEST["editCustomersID"] != '' && $thisKey == $orderDatas["ordersLand"]){
												$selected = ' selected="selected" ';
											}
											else if($orderDatas["ordersLand"] != '' && $thisKey == $orderDatas["ordersLand"]) {
												$selected = ' selected="selected" ';
											}
											echo '<option value="' . $thisKey . '" '.$selected.' >' . htmlentities(($arrCountryTypeDatas[$thisKey]["countries_name"])). ' [' . $arrCountryTypeDatas[$thisKey]["countries_iso_code_2"] . ']</option>';
										}
									}
								?>
							</select>
						</td>
					</tr>
					<!--
					<tr>
						<td><b>Vertreter:</b></td>
						<td>
							<select name="editOrdersVertreterID" class="inputSelect_510">
								<option value=""> - </option>
								<?php
									if(!empty($arrAgentDatas2)) {
										foreach($arrAgentDatas2 as $thisKey => $thisValue) {
											$selected = '';
											if($thisKey == $orderDatas["ordersVertreterID"]) {
												$selected = ' selected="selected" ';
											}
											echo '
												<option value="' . $thisKey . '" '.$selected.' >' . htmlentities($arrAgentDatas2[$thisKey]["customersFirmenname"]) . ' | K-NR: '.$arrAgentDatas2[$thisKey]["customersKundennummer"].'</option>';
										}
									}
								?>
							</select>
						</td>
					</tr>
					-->

					<tr>
						<td><b>Mail-Adresse:</b></td>
						<td><input type="text" name="editOrdersMailAdress" id="editOrdersMailAdress" class="inputField_510" value="<?php echo $orderDatas["ordersMailAdress"]; ?>" /></td>
					</tr>
					<tr>
						<td><b>Sonstiger Kontakt:</b></td>
						<td><input type="text" name="editOrdersContactOthers" id="editOrdersContactOthers" class="inputField_510" value="<?php echo $orderDatas["ordersContactOthers"]; ?>" /></td>
					</tr>
				</table>
			</fieldset>
<?php
	#dd('arrLoadedProducts');
?>
			<fieldset>
				<legend>Artikel-Informationen</legend>
				<table border="0" width="100%" cellspacing="0" cellpadding="0">				
					
					<tr>
						<td><b>Artikel-Kategorie:</b></td>
						<td>
							<select name="editOrdersArtikelKategorieID" id="editOrdersArtikelKategorieID" class="inputSelect_510" onchange="insertProductDatas(this.form.name, this.id, '[editOrdersArtikelNummer,editOrdersArtikelBezeichnung,editOrdersArtikelID]');">
								<option value="0">  --- Bitte w&auml;hlen --- </option>
								<?php							  
									if(!empty($arrLoadedProducts)){										
										foreach($arrLoadedProducts as $thisCategoryKey => $thisCategoryData){
										print_r($thisCategoryData);exit();								
											echo '<option class="level_1" value="' . $thisCategoryKey . '">' . $thisCategoryKey . '|' . htmlentities($thisCategoryData["categoriesName"]) . '</option>';
											
											if(!empty($thisCategoryData["products"])){
												foreach($thisCategoryData["products"] as $thisProductKey => $thisProductValue){
													$selected = '';
													if(
														$thisProductKey == $orderDatas["ordersArtikelID"]
														|| $thisProductKey == $orderDatas["ordersArtikelKategorieID"]
													){
														$selected = ' selected="selected" ';
													}
													echo '<option class="level_2" value="' . $thisProductKey . '" ' . $selected . ' >' . htmlentities($thisProductValue["productsName"]) . ' / ' . htmlentities($thisCategoryData["categoriesName"]) . ' / ' . $thisProductValue["productsProductNumberUse"] . ' / ' . $thisProductValue["productsID"] . '</option>';
												}
											}									
										}
									}
								?>
							</select>
						</td>
					</tr>
					<tr>
						<td width="200"><b>Artikel-Nummer:</b></td>
						<td>
							<?php
							
							?>
							<input type="text" name="editOrdersArtikelNummer" id="editOrdersArtikelNummer" class="inputField_510" value="<?php echo $orderDatas["ordersArtikelNummer"]; ?>" />
							<input type="hidden" name="editOrdersArtikelID" id="editOrdersArtikelID" value="<?php echo $orderDatas["ordersArtikelID"] ?>" />
						</td>
					</tr>
					<tr>
						<td><b>Artikel-Bezeichnung:</b></td>
						<td><input type="text" name="editOrdersArtikelBezeichnung" id="editOrdersArtikelBezeichnung" class="inputField_510" value="<?php echo htmlentities(($orderDatas["ordersArtikelBezeichnung"])); ?>" /></td>
					</tr>

						<tr>
						<td width="200"><b>Material:</b></td>
						<td>
							<?php
						if(!empty($arrPrintTypeDatas)) {
									echo '<select name="editOrdersArtikelMaterial" id="editOrdersArtikelMaterial" class="inputSelect_510">';
									echo '<option value=""> -- Bitte w&auml;hlen -- </option>';
									echo '<option value="PP"';
									if($orderDatas["editOrdersArtikelMaterial"]=="PP") echo 'selected';
									echo'>  Leistens PP  </option>';
									echo '<option value="ABS"';
									if($orderDatas["editOrdersArtikelMaterial"]=="ABS") echo 'selected';
									echo '> Leistens ABS </option>';
										/*$selected = '';
										if($_REQUEST["editID"] == "NEW" || $orderDatas["ordersAdditionalCostsID"] == 0) {
											$selected = ' selected="selected" ';
										}
										foreach($arrPrintTypeDatas as $thisKey => $thisValue) {
											$selected = '';
											if($thisKey == 7 && $orderDatas["ordersAdditionalCostsID"] == '') {
												$selected = ' selected="selected" ';
											}
											else if($thisKey == $orderDatas["ordersAdditionalCostsID"]) {
												$selected = ' selected="selected" ';
											}
											echo '<option value="' . $thisKey . '" '.$selected . ' >' . htmlentities(($arrPrintTypeDatas[$thisKey]["printTypesName"])) . '</option>';
										}*/
									echo '</select>';
								}
								?>
						</td>
					</tr>
					<tr>
						<td><b>Artikel-Gewicht (Gr):</b></td>
						<td><input  type="text" name="editOrdersArtikelGewicht" id="editOrdersArtikelGewicht" class="inputField_70" value="<?php echo $orderDatas["editOrdersArtikelGewicht"]; ?>" /></td>
					</tr>
					<tr>
						<td><b>Artikel-Menge:</b></td>
						<td><input type="text" name="editOrdersArtikelMenge" id="editOrdersArtikelMenge" class="inputField_70" value="<?php echo $orderDatas["ordersArtikelMenge"]; ?>" /></td>
					</tr>
						
					<!--<tr>
						<td><b>Gesamt-Gewicht (Gr):</b></td>
						<td><input type="text" name="editOrdersArtikelGesamtGewicht" id="editOrdersArtikelGesamtGewicht" class="inputField_70" value="<?php echo $orderDatas["ordersArtikelGesamtGewicht"]; ?>" /></td>
					</tr>-->
					<tr>
						<td><b>Gesamt-Gewicht (Kg):</b></td>
						<td><input type="text" name="editOrdersArtikelGesamtGewichtK" id="editOrdersArtikelGesamtGewichtK" class="inputField_70" value="<?php echo $orderDatas["editOrdersArtikelGesamtGewichtK"]; ?>" /></td>
					</tr>
					<tr>
						<td><b>Verpackungseinheiten:</b></td>
						<td><input type="text" name="editOrdersVerpackungseinheiten" id="editOrdersVerpackungseinheiten" class="inputField_70" value="<?php echo $orderDatas["editOrdersVerpackungseinheiten"]; ?>" /></td>
					</tr>
					<!--<tr>
						<td><b>Verpackungs Karton 10kg: </b></td>
						<td><input type="text" name="editOrdersVerpackungsrest" id="editOrdersVerpackungsrest" class="inputField_70" value="<?php echo $orderDatas["ordersVerpackungsrest"]; ?>" /></td>
					</tr>
					<tr>
						<td><b>GesamtKarton: </b></td>
						<td><input type="text" name="editOrdersgesamtkarton" id="editOrdersgesamtkarton" class="inputField_70" value="<?php echo $orderDatas["editOrdersgesamtkarton"]; ?>" /></td>
					</tr>-->


					<!--
					<tr>
						<td><b>Zusatzleisten:</b></td>
						<td>
							<table border="0" cellpadding="0" cellspacing="0">
								<tr>
									<td><b>Menge:</b></td>
									<td><input type="text" name="editOrdersAdditionalArtikelMenge" id="editOrdersAdditionalArtikelMenge" class="inputField_70" value="<?php echo $orderDatas["ordersAdditionalArtikelMenge"]; ?>" /></td>
									<td style="border-right:1px dotted #333333;"></td>
									<td><b>Typ:</b></td>
									<td>
										<select name="editOrdersAdditionalArtikelKategorieID" id="editOrdersAdditionalArtikelKategorieID" class="inputSelect_200">
											<option value="0"></option>
										</select>
									</td>
								</tr>
							</table>
						</td>
					</tr>
					-->
					<!--
					<tr>
						<td><b>Artikel-Einzelpreis:</b></td>
						<td><input type="text" name="editOrdersSinglePreis" id="editOrdersSinglePreis" class="inputField_70" value="<?php echo $orderDatas["ordersSinglePreis"]; ?>" /></td>
					</tr>
					<tr>
						<td><b>Artikel-Gesamtpreis:</b></td>
						<td><input type="text" name="editOrdersTotalPreis" id="editOrdersTotalPreis" class="inputField_70" value="<?php echo $orderDatas["ordersTotalPreis"]; ?>" /></td>
					</tr>
					-->
				</table>
			</fieldset>

			<fieldset>
				<legend>Material-Bestellung bei Lieferanten</legend>
				<p class="infoArea">Bitte ausf&uuml;llen, wenn die Ware erst bei einem Lieferanten bestellt werden muss.</p>
				<table border="0" width="100%" cellspacing="0" cellpadding="0">
					<tr>
						<td width="200"><b>Ware bestellt bei:</b></td>
						<td>
							<select name="materialOrderedBySupplierID" class="inputSelect_510" >
								<option value="" > --- Bitte w&auml;hlen --- </option>
								<?php
									if(!empty($arrSupplierDatas)){
										foreach($arrSupplierDatas as $thisKey => $thisValue){
											$selected = '';
											if($thisKey == $supplierOrderDatas["supplierOrdersSupplierID"]) {
												$selected = ' selected="selected" ';
											}
											echo '<option value="' . $thisKey . '" ' . $selected . '>' . htmlentities(($thisValue["suppliersFirmenname"])) . ' (' . htmlentities(($thisValue["supplierTypesName"])) . ')</option>';
										}
									}
								?>
							</select>
						</td>
					</tr>
					<tr>
						<td><b>Bestellte Menge:</b></td>
						<td>
							<input type="text" name="materialOrderdBySupplierAmount" id="materialOrderdBySupplierAmount" class="inputField_100" value="<?php if($supplierOrderDatas["supplierOrdersAmount"] > 0) { echo preg_replace("/\./", ",", $supplierOrderDatas["supplierOrdersAmount"]); } ?>" />
						</td>
					</tr>
					<tr>
						<td><b>Ware bestellt am:</b></td>
						<td>
							<input type="text" name="materialOrderdBySupplierOrderDate" id="materialOrderdBySupplierOrderDate" class="inputField_100" value="<?php if($supplierOrderDatas["supplierOrdersOrderDate"] > 0) { echo formatDate($supplierOrderDatas["supplierOrdersOrderDate"], 'display'); } ?>" readonly="readonly" />
						</td>
					</tr>
					<tr>
						<td><b>Ware lierfern an:</b></td>
						<td>
							<input type="radio" name="materialOrderdBySupplierDeliveryDirectToCustomer" id="materialOrderdBySupplierDeliveryDirectToUs" class="inputRadio" <?php if($supplierOrderDatas["supplierOrdersDeliveryDirectToCustomer"] != '1'){ echo ' checked="checked" '; } ?> value="0" />Ware wird an uns geliefert
							&nbsp;
							<input type="radio" name="materialOrderdBySupplierDeliveryDirectToCustomer" id="materialOrderdBySupplierDeliveryDirectToCustomer" class="inputRadio" <?php if($supplierOrderDatas["supplierOrdersDeliveryDirectToCustomer"] == '1'){ echo ' checked="checked" '; } ?> value="1" />Ware wird direkt an Kunden geliefert
						</td>
					</tr>
					<tr>
						<td><b>Ware geliefert am:</b></td>
						<td>
							<input type="text" name="materialOrderdBySupplierDeliveryDate" id="materialOrderdBySupplierDeliveryDate" class="inputField_100" value="<?php if($supplierOrderDatas["supplierOrdersDeliveryDate"] > 0) { echo formatDate($supplierOrderDatas["supplierOrdersDeliveryDate"], 'display'); } ?>" readonly="readonly" />
						</td>
					</tr>
				</table>
			</fieldset>

			<fieldset>
				<legend>Druck-Informationen</legend>
				<table border="0" width="100%" cellspacing="0" cellpadding="0">
					<tr>
						<td width="200"><b>Klischee-Nr:</b></td>
						<td><input type="text" name="editOrderPrintPlateNumber" id="editOrderPrintPlateNumber" class="inputField_510" value="<?php echo $orderDatas["ordersPrintPlateNumber"]; ?>" /></td>
					</tr>
					<tr>
						<td width="200"><b>Druck-Auftrag:</b></td>
						<td>
							<input type="file" size="50" id="druckAuftrag" name="druckAuftrag" class="inputField_510" />
						</td>
					</tr>
					<tr>
						<td width="200"><b>Anzahl Druckfarbe(n):</b></td>
						<td>
							<select name="editOrdersArtikelPrintColorsCount" id="editOrdersArtikelPrintColorsCount" class="inputSelect_120">
							<?php
								for($i = 0 ; $i < 10 ; $i++) {
									$selected = '';
									if($orderDatas["ordersArtikelPrintColorsCount"] == '' && $i == 1) {
										$selected = ' selected="selected" ';
									}
									else if($i == $orderDatas["ordersArtikelPrintColorsCount"]) {
										$selected = ' selected="selected" ';
									}
									if($i == 0){ $thisText = '0 / unbedruckt'; }
									else { $thisText = $i; }
									echo '
										<option value="' . $i . '" '.$selected.' >' . htmlentities($thisText) . '</option>
									';
								}
							?>
							</select>
						</td>
					</tr>
					<tr>
						<td><b>Umrandung:</b></td>
						<td>
							<?php
								$checked = '';
								if($orderDatas["ordersArtikelWithBorder"] == '1') {
									$checked = ' checked="checked" ';
								}
							?>
							<input type="checkbox" name="editOrdersArtikelWithBorder" value="1" <?php echo $checked; ?> /> mit Umrandung
						</td>
					</tr>
					<tr>
						<td><b>Klarlack:</b></td>
						<td>
							<?php
								$checked = '';
								if($orderDatas["ordersArtikelWithClearPaint"] == '1') {
									$checked = ' checked="checked" ';
								}
							?>
							<input type="checkbox" name="editOrdersArtikelWithClearPaint" value="1" <?php echo $checked; ?> /> mit Klarlack-Extradruck
						</td>
					</tr>
					<tr>
						<td><b>Artikel-Druckfarbe(n):</b></td>
						<td>
							<input type="text" style="font-size:10px;" name="editOrdersDruckFarbe" id="editOrdersDruckFarbe" class="inputField_510" readonly="readonly" value="<?php echo ($orderDatas["ordersDruckFarbe"]); ?>" />
							<div class="inputArea1">
								<?php
									if(!empty($arrPrintColors)) {
										$arrOrderDatasOrdersDruckFarben = explode(" / ", $orderDatas["ordersDruckFarbe"]);

										$arrThisOrderDruckFarben = array();
										if(!empty($arrOrderDatasOrdersDruckFarben)) {
											foreach($arrOrderDatasOrdersDruckFarben as $thisKey => $thisValue) {
												$patternValue = "/ \[(.*)\]$/";
												$thisColorValue = "";
												if(preg_match($patternValue, $thisValue, $thisMatches)) {
													$thisColorValue =  $thisMatches[1];
													$thisValue = preg_replace($patternValue, '', $thisValue);
												}

												$patternType = "/ \((.*)\)$/";
												$thisColorType = "";
												if(preg_match($patternType, $thisValue, $thisMatches)) {
													$thisColorType =  $thisMatches[1];
													$thisValue = preg_replace($patternType, '', $thisValue);
												}

												$thisColorName = $thisValue;
												#$thisColorName = strtolower($thisColorName);

												$arrThisColorNames[$thisKey]	= ($thisColorName);
												$arrThisColorValues[$thisKey]	= $thisColorValue;
												$arrThisColorTypes[$thisKey]	= $thisColorType;

												if($arrGetUserRights["adminArea"]){
													#echo (($thisColorName)) . ' | ' . (mb_detect_encoding($thisColorName)) . '<br />';
												}
											}
										}
								?>
								<table border="0" width="100%" cellspacing="0" cellpadding="0">
									<tr>
										<th>Farbe</th>
										<th>Art</th>
										<th>Farbcode</th>
									</tr>
									<?php
										foreach($arrPrintColors as $thisKey => $thisValue) {
											$thisColorKey = array_search($thisValue["printColorValue"], $arrThisColorNames);
											$checked = '';
											$thisRowStyleBackground = '';
											$thisRowStyleFontStyle	= '';

											if(is_int($thisColorKey) && strtolower($arrThisColorNames[$thisColorKey]) == strtolower(($thisValue["printColorValue"]))) {
												$checked = ' checked="checked" ';
												$thisRowStyleBackground = 'background-color: #CBFFBF;';
												$thisRowStyleFontStyle	= 'font-weight:bold;';
											}

											echo '<tr style="' . $thisRowStyleBackground . '' . $thisRowStyleFontStyle . '">';
											echo '<td>';



											echo '<input type="checkbox" id="printColorID_' . $thisKey . '" name="printColorID[' . $thisKey . ']" class="checkPrintColors" value="1" ' . $checked . ' /> ' . $thisValue["printColorName"];
											echo '</td>';

											echo '<td>';
											$selectedPositiv = '';
											if(is_int($thisColorKey) != "" && strtolower($arrThisColorTypes[$thisColorKey]) == 'positiv') {
												$selectedPositiv = ' selected="selected" ';
											}

											$selectedNegativ = '';
											if(is_int($thisColorKey) != "" && strtolower($arrThisColorTypes[$thisColorKey]) == 'negativ') {
												$selectedNegativ = ' selected="selected" ';
											}
											echo '<select style="' . $thisRowStyleFontStyle . '" name="printColorType[' . $thisKey . ']" class="inputSelect_70">';
											echo '<option value="" selected="selected"></option>';
											echo '<option value="positiv"' . $selectedPositiv . '>positiv</option>';
											echo '<option value="negativ"' . $selectedNegativ . '>negativ</option>';
											echo '</select>';
											echo '</td>';

											echo '<td>';
											$thisColorValue = $thisValue["printColorCode"];
											if(is_int($thisColorKey) && $arrThisColorValues[$thisColorKey] != '') {
												$thisColorValue = $arrThisColorValues[$thisColorKey];
											}
											echo '<input style="' . $thisRowStyleFontStyle . '" type="text" id="printColorValue_' . $thisKey . '"  name="printColorValue[' . $thisKey . ']" class="inputField_200" value="' . $thisColorValue . '" />';
											echo '</td>';
											echo '</tr>';
										}
									?>
								</table>
								<?php } ?>
							</div>
						</td>
					</tr>
					<tr>
						<td><b>Artikel-Druckart:</b></td>
						<td><!-- <p class="infoArea">Nur bei Neubestellungen.</p> -->
							<?php
								/*
								if(!empty($arrAdditionalCostsDatas)) {
									echo '<table border="1" width="516" cellpadding="0" cellspacing="0">';
										echo '
											<tr>
												<th>Hinzuf&uuml;gen</th>
												<th>Beschreibung</th>
												<th>Preis pro Farbe</th>
											</tr>
										';
										$checked = '';
										if($_REQUEST["editID"] == "NEW" || $orderDatas["ordersAdditionalCostsID"] == 0) {
											$checked = ' checked="checked" ';
										}

										echo '</tr>';
										foreach($arrAdditionalCostsDatas as $thisKey => $thisValue) {
											echo '<tr>';
											$checked = '';
											if($thisKey == $orderDatas["ordersAdditionalCostsID"]) {
												$checked = ' checked="checked" ';
											}
											$readonly = '';
											if($thisKey == 1) {
												$readonly = ' readonly="readonly" ';
											}
											echo '<td>
												<input type="radio" name="editOrdersAdditionalCostsID" class="inputRadio1" value="' . $thisKey . '" '.$checked . ' />
											</td>';
											echo '<td>' . ($arrAdditionalCostsDatas[$thisKey]["additionalCostsName"]).'
											</td>';
											echo '<td style="text-align:center;">
												<input type="text" style="text-align:right;" name="editOrdersAdditionalCostsPrice['.$thisKey.']" class="inputField_40" value="'.number_format($arrAdditionalCostsDatas[$thisKey]["additionalCostsPrice"], 2, ',', '').'" '.$checked . $readonly . ' /> &euro;
											</td>';
											echo '</tr>';
										}
									echo '</table>';
								}
								*/

								if(!empty($arrPrintTypeDatas)) {
									echo '<select name="editOrdersAdditionalCostsID" id="editOrdersAdditionalCostsID" class="inputSelect_510">';
									echo '<option value=""> -- Bitte w&auml;hlen -- </option>';
										$selected = '';
										if($_REQUEST["editID"] == "NEW" || $orderDatas["ordersAdditionalCostsID"] == 0) {
											$selected = ' selected="selected" ';
										}
										foreach($arrPrintTypeDatas as $thisKey => $thisValue) {
											$selected = '';
											if($thisKey == 7 && $orderDatas["ordersAdditionalCostsID"] == '') {
												$selected = ' selected="selected" ';
											}
											else if($thisKey == $orderDatas["ordersAdditionalCostsID"]) {
												$selected = ' selected="selected" ';
											}
											echo '<option value="' . $thisKey . '" '.$selected . ' >' . htmlentities(($arrPrintTypeDatas[$thisKey]["printTypesName"])) . '</option>';
										}
									echo '</select>';
								}
							?>
						</td>
					</tr>
					<tr>
						<td><b>Name des Druckers:</b></td>
						<td>
							<select name="editOrdersDruckerName" class="inputSelect_510">
								<option value=""> - </option>
								<?php
									if(!empty($arrPrinterDatas)) {
										foreach($arrPrinterDatas as $thisKey => $thisValue) {
											$selected = '';
											if($thisKey == $orderDatas["ordersDruckerName"]) {
												$selected = ' selected ';
											}
											echo '
												<option value="' . $thisKey . '" '.$selected.' >' . htmlentities($arrPrinterDatas[$thisKey]["printersLastName"]).', '.$arrPrinterDatas[$thisKey]["printersFirstName"] . '</option>
											';
										}
									}
								?>
							</select>
						</td>
					</tr>
				</table>
			</fieldset>
			<?php if(!1){ ?>
			<!--
			<fieldset>
				<legend>Zahlungsdaten</legend>
				<table border="0" width="100%" cellspacing="0" cellpadding="0">
					<tr>
						<td width="200"><b>Dokumente:</b></td>
						</td>
						<td>
							<?php
								/*
								if($orderDatas["createdDocumentsFilename"] != "") {
									$thisDocuments = explode('#', $orderDatas["createdDocumentsFilename"]);
									if(!empty($thisDocuments)) {
										foreach($thisDocuments as $thisKey => $thisValue) {
											$arrThisValue = explode(":", $thisValue);

											if(pathinfo($arrThisValue[0], PATHINFO_EXTENSION) == "doc") {
												$thisImageName = "iconDOC.gif";
											}
											else if(pathinfo($arrThisValue[0], PATHINFO_EXTENSION) == "zip") {
												$thisImageName = "iconZIP.gif";
											}
											else if(pathinfo($arrThisValue[0], PATHINFO_EXTENSION) == "pdf") {
												$thisImageName = "iconPDF.gif";
											}
											else if(pathinfo($arrThisValue[0], PATHINFO_EXTENSION) == "tiff") {
												$thisImageName = "iconTIFF.gif";
											}
											else if(pathinfo($arrThisValue[0], PATHINFO_EXTENSION) == "eps") {
												$thisImageName = "iconEPS.gif";
											}
											else if(pathinfo($arrThisValue[0], PATHINFO_EXTENSION) == "gif") {
												$thisImageName = "iconGIF.gif";
											}
											else {
												$thisImageName = "iconALL.gif";
											}

											echo '<a href="?downloadFile=' . basename($arrThisValue[0]) . '"><img src="layout/icons/'.$thisImageName.'" alt="" title="'.basename($arrThisValue[0]).'"/> ' . pathinfo($arrThisValue[0], PATHINFO_FILENAME). '</a> - ' .$arrThisValue[1]. '<br />';
										}
									}
								}
								else {
									echo 'Keine Dokumente vorhanden. (Rechnungen etc.)';
								}
								*/
							?>
						</td>
					</tr>
					-->
					<!--
					<tr>
						<td><b>Lieferdatum:</b></td>
						<td>
							<?php
								$thisDate = formatDate($orderDatas["ordersLieferDatum"], "split");
							?>
							<input type="text" name="editOrdersLieferDatum" id="editOrdersLieferDatum" maxlength="2" class="inputField_100" readonly value="<?php if($orderDatas["ordersLieferDatum"] == "") { echo "0000-00-00"; } else { echo formatDate($orderDatas["ordersLieferDatum"], "display"); } ?>" />
						</td>
					</tr>
					-->

					<!--
					<tr>
						<td><b>Bank-Verbindung:</b></td>
						<td>
							<select name="editOrdersBankAccountType" id="editOrdersBankAccountType" class="inputSelect_510">
								<?php
									/*
									$selected = '';
									if($_REQUEST["editID"] == "NEW") {
										$selected = ' selected ';
									}
								?>
								<option value="" <?php echo $selected; ?>> - </option>
								<?php
									if(!empty($arrBankAccountTypeDatas)) {
										foreach($arrBankAccountTypeDatas as $thisKey => $thisValue) {
											$selected = '';
											if($thisKey == $orderDatas["ordersBankAccountType"]) {
												$selected = ' selected ';
											}
											echo '
												<option value="' . $thisKey . '" '.$selected.' >' . htmlentities($arrBankAccountTypeDatas[$thisKey]["bankAccountTypesName"]) . '</option>
											';
										}
									}
									*/
								?>
							</select>
						</td>
					</tr>

					<tr>
						<td><b>Bezahl-Status:</b></td>
						<td>
							<select name="editOrdersPaymentStatusType" class="inputSelect_510">
								<?php
									/*
									$selected = '';
									if($_REQUEST["editID"] == "NEW") {
										$selected = ' selected ';
									}
								?>
								<option value="" <?php echo $selected; ?>> - </option>
								<?php
									if(!empty($arrPaymentStatusTypeDatas)) {
										foreach($arrPaymentStatusTypeDatas as $thisKey => $thisValue) {
											$selected = '';
											if($thisKey == $orderDatas["ordersPaymentStatusType"]) {
												$selected = ' selected ';
											}
											echo '
												<option value="' . $thisKey . '" '.$selected.' >' . htmlentities($arrPaymentStatusTypeDatas[$thisKey]["paymentStatusTypesName"]) . '</option>
											';
										}
									}
									*/
								?>
							</select>
						</td>
					</tr>
				</table>
			</fieldset>
			-->
			<?php } ?>
			<fieldset>
				<legend>Sonstige Daten</legend>
				<table border="0" width="100%" cellspacing="0" cellpadding="0">
					<tr>
						<td width="200"><b>Bezahl-Art:</b></td>
						<td>
							<select name="editOrdersPaymentType" id="editOrdersPaymentType" class="inputSelect_510">
								<?php
									$selected = '';
									if($_REQUEST["editID"] == "NEW") {
										$selected = ' selected="selected" ';
									}
								?>
								<option value="" <?php echo $selected; ?>> - </option>
								<?php
									if(!empty($arrPaymentTypeDatas)) {
										foreach($arrPaymentTypeDatas as $thisKey => $thisValue) {
											$selected = '';
											if($thisKey == $orderDatas["ordersPaymentType"]) {
												$selected = ' selected="selected" ';
											}
											echo '
												<option value="' . $thisKey . '" '.$selected.' >' . htmlentities($arrPaymentTypeDatas[$thisKey]["paymentTypesName"]) . '</option>
											';
										}
									}
								?>
							</select>
						</td>
					</tr>
					<tr>
						<td width="200"><b>Bemerkungen / Notizen:</b></td>
						<td><textarea name="editOrdersNotizen" rows="10" cols="20" class="inputTextarea_510x140" ><?php echo ($orderDatas["ordersNotizen"]); ?></textarea></td>
					</tr>
					<tr>
						<td width="200"><b>Reklamationsgrund:</b></td>
						<td><textarea name="editOrdersReklamationsgrund" rows="10" cols="20" class="inputTextarea_510x140" ><?php echo ($orderDatas["ordersReklamationsgrund"]); ?></textarea></td>
					</tr>
					<tr>
						<td width="200"><b>Archiv:</b></td>
						<td>
							<?php
								$checked = '';
								if($orderDatas["ordersArchive"] == '1') {
									$checked = ' checked="checked" ';
								}
							?>
							<input type="checkbox" name="editOrdersArchive" <?php echo $checked ; ?> value="1" /> <b>ins Archiv verschieben</b>
						</td>
					</tr>
				</table>
			</fieldset>
			<div class="actionButtonsArea">
				<input type="hidden" name="jsonReturnParams" value="<?php echo htmlentities($_REQUEST["jsonReturnParams"]); ?>" />

				<?php
					// if($orderDatas["ordersAuslieferungsDatum"] > 0) {
					if(1){

					}
					#else {
				?>
				<?php if($_COOKIE["isAdmin"] == '1' || $orderDatas["ordersStatus"] != '4' || $orderDatas["ordersAuslieferungsDatum"] == date('Y-m-d')) { ?>
				<input type="submit" class="inputButton1 inputButtonGreen" name="storeDatas" value="Speichern" onclick="return checkFormDutyFields(this.form.name, <?php echo $jsonFormDutyFields; ?>);" />
				&nbsp;
				<?php } ?>
				<?php if($_REQUEST["duplicateDatas"] == '' && $_REQUEST["editID"] != "NEW") { ?>
				<input type="submit" class="inputButton1 inputButtonOrange" name="duplicateDatas" value="Kopieren f. Nachbestellung" onclick="return showWarning('Soll eine Kopie fuer eine Nachbestellung bearbeitet werden?');" />
				<input type="hidden" name="duplicateID" value="<?php echo $_REQUEST["editID"]; ?>" />
				&nbsp;
				<?php } ?>
				<?php if($orderDatas["ordersStatus"] != '4') { ?>
				<input type="submit" class="inputButton3 inputButtonGreen" name="copyDatas" value="Speichern und weiteren Vorgang hinzuf&uuml;gen" onclick="return checkFormDutyFields(this.form.name, <?php echo $jsonFormDutyFields; ?>);" />
				&nbsp;
				<input type="submit" class="inputButton3 inputButtonGreen" name="storeDatasAndGotToCustomer" value="Speichern und zu Kundendaten gehen" onclick="return checkFormDutyFields(this.form.name, <?php echo $jsonFormDutyFields; ?>);" />
				<?php } ?>
				<?php
					#}
					if($checkProductionIsUsed < 1 && $_REQUEST["editID"] != "NEW") {
						if(strtolower(MANDATOR) == strtolower($orderDatas["ordersMandant"])) {
				?>
				&nbsp;
				<input type="submit" class="inputButton1 inputButtonRed" name="deleteDatas" value="Entfernen" onclick="return showWarning(' Wollen Sie diesen Eintrag endgültig entfernen??? ');" />
				<?php
						}
						else {
							// PRODUKTION KANN NUR ENTFERNT WERDEN, WENN PRODUKTION NICHT IN DOKUMENT AB etc. VERWENDET WURDE
							// UND WENN PRODUKTIONS-MANDANT MIT DEM EINGELOGGTEN MANDANTEN ÜBEREINSTIMMT!!!
						}
					}
				?>
				&nbsp;
				<input type="submit" class="inputButton1 inputButtonOrange" name="resetDatas" value="Abbrechen" onclick="return showWarning(' Wollen Sie diesen Vorgang wirklich abbrechen??? ');" />
			</div>
			</form>
			</div>
			<?php
				}
			?>
		</div>
	</div>
</div>
</div>
</div>
<script language="javascript" type="text/javascript">	
	<!--
	/* <![CDATA[ */
	$(document).ready(function() {
		$(function() {
			$('#editOrdersKundennummer').keyup(function () {
				// loadSuggestions('searchCustomerNumberDatas', [{'triggerElement': '#editOrdersKundennummer', 'fieldNumber': '#editOrdersKundennummer', 'fieldName': '#editOrdersKundenName', 'fieldCity': '#editOrdersOrt', 'fieldZipCode': '#editOrdersPLZ'}], 1);
				loadSuggestions('searchCustomerNumberDatas', [{'triggerElement': '#editOrdersKundennummer', 'fieldNumber': '#editOrdersKundennummer', 'fieldName': '#editOrdersKundenName', 'fieldCity': '#editOrdersOrt', 'fieldCountry': '#editOrdersLand', 'fieldZipCode': '#editOrdersPLZ', 'fieldSalesmanName': '#editOrdersVertreterName', 'fieldSalesmanID': '#editOrdersVertreterID', 'fieldMail':'#editOrdersMailAdress'}], 1);
			});
			$('#editOrdersKundenName').keyup(function () {
				// loadSuggestions('searchCustomerNumberDatas', [{'triggerElement': '#editOrdersKundenName', 'fieldNumber': '#editOrdersKundennummer', 'fieldName': '#editOrdersKundenName', 'fieldCity': '#editOrdersOrt', 'fieldZipCode': '#editOrdersPLZ'}], 1);
			});
			$('#editOrdersArtikelBezeichnung').keyup(function () {
				loadSuggestions('searchProductName', [{'triggerElement': '#editOrdersArtikelBezeichnung', 'fieldNumber': '#editOrdersArtikelNummer', 'fieldName': '#editOrdersArtikelBezeichnung', 'fieldID': '#editOrdersArtikelID', 'fieldCatID': '#editOrdersArtikelKategorieID'}], 1);
			});
			$('#editOrdersArtikelNummer').keyup(function () {
				loadSuggestions('searchProductNumber', [{'triggerElement': '#editOrdersArtikelNummer', 'fieldNumber': '#editOrdersArtikelNummer', 'fieldName': '#editOrdersArtikelBezeichnung', 'fieldID': '#editOrdersArtikelID', 'fieldCatID': '#editOrdersArtikelKategorieID'}], 1);
			});
			$('#editOrdersPLZ').keyup(function () {
				loadSuggestions('searchPlzCity', [{'triggerElement': '#editOrdersPLZ', 'fieldCity': '#editOrdersOrt', 'fieldZipCode': '#editOrdersPLZ'}], 1);
			});
			$('#editOrdersOrt').keyup(function () {
				loadSuggestions('searchCityPlz', [{'triggerElement': '#editOrdersOrt', 'fieldCity': '#editOrdersOrt', 'fieldZipCode': '#editOrdersPLZ'}], 1);
			});
			$('#editOrdersArtikelMenge').keyup(function () {
				getProductPrices('#editOrdersArtikelMenge', '#editOrdersSinglePreis', '#editOrdersTotalPreis', '#editOrdersArtikelID');
			});
			setFormDutyFields(<?php echo $jsonFormDutyFields; ?>);

			$('#editOrdersPaymentType').change(function() {
				selectOtherOption('#' + $(this).attr('id'), '#editOrdersBankAccountType');
			});
			$('#editOrdersArtikelKategorieID').change(function() {
				gewicht = $('#editOrdersArtikelKategorieID').find(':selected').data("gewicht");
				$('#editOrdersArtikelGewicht').val(gewicht);
			});

			jQuery.fn.gewicht_calculator = function(param) {
				gesamGgewicht = $('#editOrdersArtikelKategorieID').find(':selected').data("gewicht")*$('#editOrdersArtikelMenge').val();
				$('#editOrdersArtikelGesamtGewicht').val(gesamGgewicht);
				gesamGgewichtKg = (gesamGgewicht/1000).toFixed(2);
				Verpackungseinheiten =  Math.ceil((gesamGgewichtKg / 20)) ;
				reste = (gesamGgewichtKg - (Verpackungseinheiten*20)).toFixed(2);
				$('#editOrdersArtikelGesamtGewichtK').val(gesamGgewichtKg);
				$('#editOrdersVerpackungseinheiten').val(Verpackungseinheiten);
			}
			$('#editOrdersArtikelMenge').keyup(function() {
				$(document).gewicht_calculator();
			});
			

			$('#editOrdersVertreterName').keyup(function() {
				loadSuggestions('searchSalesmen', [{'triggerElement': '#editOrdersVertreterName', 'fieldName': '#editOrdersVertreterName', 'fieldID': '#editOrdersVertreterID'}], 1);
			});
			// $('#editOrdersArtikelKategorieID').change(function() {
			$('#editOrdersAdditionalArtikelMenge').keyup(function() {
				selectRelatedArtikel('#editOrdersArtikelKategorieID', '#editOrdersAdditionalArtikelKategorieID', '#editOrdersAdditionalArtikelMenge');
			});
			$('#editOrdersArtikelPrintColorsCount').change(function(){
				var countColors = $(this).val();
				$('.checkPrintColors').removeAttr('checked');
				if(countColors == 0){
					// $('#editOrdersAdditionalCostsID').val(10);
					$('#editOrdersAdditionalCostsID').val(1);
					$('#printColorID_14').attr('checked', 'true');
				}
				else {
					$('#editOrdersAdditionalCostsID').val(7);
					$('#printColorID_12').attr('checked', 'true');
				}
			});

			$('.checkPrintColors').click(function() {
				var thisChecked = $(this).attr('checked');
				if(thisChecked == 'checked'){
					$(this).parent().parent().css('background-color', '#CBFFBF');
					$(this).parent().parent().css('font-weight', 'bold');
					$(this).parent().parent().find('select').css('font-weight', 'bold');
					$(this).parent().parent().find('input').css('font-weight', 'bold');
				}
				else {
					$(this).parent().parent().css('background-color', '');
					$(this).parent().parent().css('font-weight', '');
					$(this).parent().parent().find('select').css('font-weight', '');
					$(this).parent().parent().find('input').css('font-weight', '');
				}
			});

			<?php if($_REQUEST["editID"] != "") { ?>			
			getOrderProductCategoriesNeu('<?php echo $thisCusomerGroupID; ?>', 'all', '#editOrdersMandant', '#editOrdersArtikelKategorieID', '<?php echo $orderDatas["ordersArtikelKategorieID"]; ?>');
			// getOrderProductCategoriesNeu('<?php echo $thisCusomerGroupID; ?>', '002', '#editOrdersMandant', '#editOrdersAdditionalArtikelKategorieID', '<?php echo $orderDatas["ordersAdditionalArtikelKategorieID"]; ?>');

			<?php } ?>

			// $( ".datepicker" ).datepicker({ maxDate: '0' });
			$.datepicker.setDefaults($.datepicker.regional[""]);
			$('#editOrdersBestellDatum').datepicker($.datepicker.regional["de"]);
			$('#editOrdersBestellDatum').datepicker("option", "maxDate", "0" );
			$('#editOrdersStornoDatum').datepicker($.datepicker.regional["de"]);
			$('#editOrdersStornoDatum').datepicker("option", "maxDate",  );
			$('#editOrdersFreigabeDatum').datepicker($.datepicker.regional["de"]);
			$('#editOrdersFreigabeDatum').datepicker("option", "maxDate",  );
			$('#editOrdersBelichtungsDatum').datepicker($.datepicker.regional["de"]);
			$('#editOrdersBelichtungsDatum').datepicker("option", "maxDate",  );
			$('#editOrdersProduktionsDatum').datepicker($.datepicker.regional["de"]);
			$('#editOrdersProduktionsDatum').datepicker("option", "maxDate",  );
			$('#editOrdersAuslieferungsDatum').datepicker($.datepicker.regional["de"]);
			$('#editOrdersAuslieferungsDatum').datepicker("option", "maxDate",  );
			$('#editOrdersLieferDatum').datepicker($.datepicker.regional["de"]);
			$('#editOrdersLieferDatum').datepicker("option", "maxDate",  );
			$('#materialOrderdBySupplierOrderDate').datepicker($.datepicker.regional["de"]);
			$('#materialOrderdBySupplierOrderDate').datepicker("option", "maxDate",  );
			$('#materialOrderdBySupplierDeliveryDate').datepicker($.datepicker.regional["de"]);
			$('#materialOrderdBySupplierDeliveryDate').datepicker("option", "maxDate",  );

			var htmlButtonClearField = '<span class="buttonClearField"><img src="layout/icons/iconDelete.png" width="16" height="16" alt="" title="Eingabefeld leeren" \/><\/span>';
			$('#editOrdersStornoDatum').parent().append(htmlButtonClearField);
			$('#editOrdersFreigabeDatum').parent().append(htmlButtonClearField);
			$('#editOrdersBelichtungsDatum').parent().append(htmlButtonClearField);
			$('#editOrdersProduktionsDatum').parent().append(htmlButtonClearField);
			$('#editOrdersAuslieferungsDatum').parent().append(htmlButtonClearField);
			$('#editOrdersLieferDatum').parent().append(htmlButtonClearField);
			$('#materialOrderdBySupplierOrderDate').parent().append(htmlButtonClearField);
			$('#materialOrderdBySupplierDeliveryDate').parent().append(htmlButtonClearField);
			$('#editOrdersDruckFarbe').parent().parent().append(htmlButtonClearField);
			$('.buttonClearField').click(function () {
				$(this).parent().find('input').val('');
				$(this).parent().find('input').attr('checked', false);
			});

			$('.buttonRemoveSalesman').click(function () {
				$('#editOrdersVertreterName').val('');
				$('#editOrdersVertreterID').val('');
				$('#editOrdersMailAdress').val('');
				$('#editOrdersContactOthers').val('');
			});

			// BOF UNBEDRUCKT
				$('#printColorID_14').click(function(){
					if($(this).attr('checked') == "checked"){
						$('#editOrdersAdditionalCostsID').val('1');
					}
					else {
						$('#editOrdersAdditionalCostsID').val('');
					}
				});
			// EOF UNBEDRUCKT

			// BOF DIGITALDRUCK
				$('#printColorID_13').click(function(){
					if($(this).attr('checked') == "checked"){
						$('#editOrdersAdditionalCostsID').val('6');
					}
					else {
						$('#editOrdersAdditionalCostsID').val('');
					}
				});
			// EOF DIGITALDRUCK

			// BOF DIGITALDRUCK
				$('#printColorID_18').click(function(){
					if($(this).attr('checked') == "checked"){
						$('#editOrdersAdditionalCostsID').val('5');
					}
					else {
						$('#editOrdersAdditionalCostsID').val('');
					}
				});
			// EOF DIGITALDRUCK
		});
	});
	/* ]]> */
	// -->

	/* <![CDATA[ */
	/*
	$('#editOrdersArtikelKategorieID').select2({
		formatResult: function(object) {
			var $el = $(object.element),
				product = $el.text(),
				category = $el.attr('data-category');

			return '<b>' + product + '<\/b>' + ' <i>' + category + '<\/i>';
		}
	});
	*/
	/* ]]> */

</script>

<?php require_once('inc/footerHTML.inc.php'); ?>