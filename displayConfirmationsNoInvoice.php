<?php
	require_once('inc/requires.inc.php');

	if($_REQUEST["documentType"] == 'AB' && !$arrGetUserRights["editConfirmations"]) {
		header('location: ' . PAGE_EXIT_LOCATION);
		exit;
	}

	$jswindowMessage = "";
	$warningMessage = '';
	$errorMessage = '';
	$successMessage = '';
	$infoMessage = '';

	$dbConnection = new DB_Connection();
	$db_open = $dbConnection->db_connect();

	// BOF READ PAYMENT STATUS DATAS
		$arrPaymentStatusTypeDatas = getPaymentStatusTypes();
	// EOF READ PAYMENT STATUS DATAS

	// BOF READ DOCUMENT TYPE DATAS
		$arrDocumentTypeDatas = getDocumentTypes();
	// EOF READ DOCUMENT TYPE DATAS

	// BOF READ ORDER STATUS DATAS
		$arrOrderStatusTypeDatas = getOrderStatusTypes();
	// EOF READ ORDER STATUS DATAS
	
	// BOF READ BANK TYPES
		$arrBankAccountTypeDatas = getBankAccountTypes();
	// EOF READ BANK TYPES	
	
	// BOF READ PAYMENT TYPES
		$arrPaymentTypeDatas = getPaymentTypes();
	// EOF READ PAYMENT TYPES
	
	// BOF READ PAYMENT CONDITIONS
		$arrPaymentConditionDatas = getPaymentConditions();
	// EOF READ PAYMENT CONDITIONS

	// BOF READ ORDER STATUS DATAS
		$arrMandatoriesDatas = getMandatories();
		$currentMandator = MANDATOR;
		$arrOtherMandators = array();
		if(!empty($arrMandatoriesDatas)){
			foreach($arrMandatoriesDatas as $thisMandatorDatas){
				if($thisMandatorDatas["mandatoriesShortName"] != $currentMandator){
					$arrOtherMandators[] = $thisMandatorDatas["mandatoriesShortName"];
				}
			}
		}
	// EOF READ ORDER STATUS DATAS


	// BOF SEND MAIL WITH ATTACHED DOCUMENT
	if($_POST["sendAttachedDocument"] == '1') {
		$thisCreatedPdfName = $_POST["mailDocumentFilename"];
		$_POST["selectSubject"] = $arrDocumentTypeDatas[$_REQUEST["documentType"]]["createdDocumentsTypesName"] . ' Nr.: ' . '{###DOCUMENT_NUMBER###}';
		$generatedDocumentNumber = substr($_POST["mailDocumentFilename"], 0, 13);

		require_once(DIRECTORY_HTML_MIME_MAIL . 'htmlMimeMail.php');
		require_once(DIRECTORY_MAIL_TEMPLATES . 'template_BASIC.inc.php');
		require_once(DIRECTORY_MAIL_TEMPLATES . 'template_TEXT_DATAS.inc.php');
		##require_once("inc/mail.inc.php");

		// BOF SEND MAIL
		require_once("classes/createMail.class.php");

		// BOF CREATE SUBJECT
		$thisSubject = $_POST["selectSubject"];
		if($_POST["sendAttachedMailSubject"] != '') {
			$thisSubject = $_POST["sendAttachedMailSubject"] . ', ' . $thisSubject;
		}
		// EOF CREATE SUBJECT

		// BOF GET ATTACHED FILE
		if($_REQUEST["documentType"] == 'KA') {
			$pathDocumentFolder = DIRECTORY_UPLOAD_FILES;
			$thisCreatedPdfName = utf8_decode($thisCreatedPdfName);
		}
		else if($_REQUEST["documentType"] == 'EX') {
			$pathDocumentFolder = DIRECTORY_EXPORT_FILES;
			$thisCreatedPdfName = utf8_decode($thisCreatedPdfName);
		}
		else if($_REQUEST["documentType"] == 'SX') {
			$pathDocumentFolder = DIRECTORY_CREATED_DOCUMENTS_SALESMEN;
			$thisCreatedPdfName = utf8_decode($thisCreatedPdfName);
		}
		else {
			$pathDocumentFolder = DIRECTORY_CREATED_DOCUMENTS_CUSTOMERS;
		}

		$arrAttachmentFiles = array (
			rawurldecode($pathDocumentFolder . $thisCreatedPdfName) => str_replace(" ", "_", rawurldecode(myUnHtmlEntities($thisCreatedPdfName)))
		);
		// EOF GET ATTACHED FILE

		$createMail = new createMail(
							$thisSubject,													// TEXT:	MAIL_SUBJECT
							$_POST["documentType"],											// STRING:	DOCUMENT TYPE
							$generatedDocumentNumber,										// STRING:	DOCUMENT NUMBER
							$arrAttachmentFiles,											// ARRAY:	ATTACHMENT FILES
							$_POST["selectCustomersRecipientMail"],	 										// STRING:	RECIPIENTS
							$arrMailContentDatas[$_POST["selectMailtextTemplates"]],		// MAIL_TEXT_TEMPLATE
							$mailTemplate["html"],											// MAIL_BODY_TEMPLATE
							'',																// STRING:	ADDITIONAL TEXT
							$_POST["sendAttachedMailText"],									// STRING:	SEND ATTACHED TEXT
							true,															// BOOLEAN:	SEND ATTACHED DOCUMENT
							DIRECTORY_MAIL_IMAGES											// DIRECTORY_MAIL_IMAGES
						);

		$createMailResult = $createMail->returnResult();
		$sendMailToClient = $createMailResult;

		#if(!$sendMailToUs) { $errorMessage .= ' Die Mail-Kopie konnte nicht an ' . implode(', ', $arrOrderMail) . ' versendet werden! '.'<br />'; }
		#else { $successMessage .= ' Die Mail-Kopie wurde an ' . implode(', ', $arrOrderMail) . ' versendet! '.'<br />'; }

		if(!$sendMailToClient) { $errorMessage .= ' Die Mail konnte nicht an ' . $_POST["selectCustomersRecipientName"] . ' (' . $_POST["selectCustomersRecipientMail"] . ') versendet werden! '.'<br />'; }
		else {
			$successMessage .= ' Die Mail wurde an ' . $_POST["selectCustomersRecipientName"] . ' (' . $_POST["selectCustomersRecipientMail"] . ') versendet! '.'<br />';
		}
	}
	// EOF SEND MAIL WITH ATTACHED DOCUMENT


	// BOF DOWNLOAD FILE
	if($_REQUEST["downloadFile"] != "") {
		require_once('classes/downloadFile.class.php');
		$thisDownload = new downloadFile(DIRECTORY_CREATED_DOCUMENTS_CUSTOMERS, $_REQUEST["downloadFile"]);
		$errorMessage .= $thisDownload->startDownload();
	}
	// EOF DOWNLOAD FILE

	// BOF GET SELECTED DOCUMENT DATAS
	$where = "";

	$thisSearchDateEnd = date("Y-m-d", mktime(0, 0, 0, (date("m") - 2),0, date("Y")));
	if(trim($_REQUEST["searchDateEnd"]) != '') {
		$thisSearchDateEnd = formatDate($_REQUEST["searchDateEnd"], "store");
	}

	// BOF SET SQL FOR OTHER MANDATORIES TABLES
		$sqlOtherMandators_Fields = "";
		$sqlOtherMandators_LeftJoin = "";
		if(!empty($arrOtherMandators)){
			foreach($arrOtherMandators as $thisOtherMandators){
				$thisOtherMandatorsTableInvoices = preg_replace("/^" . $currentMandator . "/", $thisOtherMandators, TABLE_ORDER_INVOICES); //  orderDocumentsOrdersIDs
				$thisOtherMandatorsTableInvoicesDetails = preg_replace("/^" . $currentMandator . "/", $thisOtherMandators, TABLE_ORDER_INVOICES_DETAILS); // orderDocumentDetailOrderID

				$sqlOtherMandators_Fields .= "
					,`" . $thisOtherMandatorsTableInvoices . "`.`orderDocumentsNumber` AS `" . $thisOtherMandators . "_" . "orderDocumentsNumber`
				";

				$sqlOtherMandators_LeftJoin .= "
						LEFT JOIN `" . $thisOtherMandatorsTableInvoices . "`
						ON(FIND_IN_SET(`tempTable`.`ordersID`, REPLACE(`" . $thisOtherMandatorsTableInvoices . "`.`orderDocumentsOrdersIDs`, ';', ',')))
					";
			}
		}
	// EOF SET SQL FOR OTHER MANDATORIES TABLES

	$sql = "
			SELECT
				SQL_CALC_FOUND_ROWS

				`tempTable`.`orderDocumentsID`,

				`tempTable`.`orderDocumentsNumber`,
				`tempTable`.`orderDocumentsDocumentDate`,
				`tempTable`.`orderDocumentsCustomerNumber`,
				`tempTable`.`orderDocumentsAddressCompany`,
				`tempTable`.`orderDocumentsTotalPrice`,
				`tempTable`.`orderDocumentsStatus`,
				`tempTable`.`orderDocumentsDocumentPath`,
				
				`tempTable`.`orderDocumentsBankAccount`,
				`tempTable`.`orderDocumentsPaymentCondition`,
				`tempTable`.`orderDocumentsPaymentType`,

				`tempTable`.`relatedDocuments_AB`,
				`tempTable`.`relatedDocuments_RE`,

				`tempTable`.`ordersID`,

				`tempTable`.`ordersStatus`,
				`tempTable`.`ordersBestellDatum`,
				`tempTable`.`ordersLieferDatum`,

				`tempTable`.`orderDocumentDetailOrderID`,
				`tempTable`.`orderDocumentDetailProductQuantity`,
				`tempTable`.`orderDocumentDetailProductNumber`,
				`tempTable`.`orderDocumentDetailProductName`,
				`tempTable`.`orderDocumentDetailPosType`,

				`tempTable`.`customersKundennummer`,
				`tempTable`.`customersFirmenname`,
				`tempTable`.`customersCompanyPLZ`,
				`tempTable`.`customersCompanyOrt`

				" . $sqlOtherMandators_Fields . "

				FROM (
					SELECT
						`" . TABLE_ORDER_CONFIRMATIONS . "`.`orderDocumentsID`,
						`" . TABLE_ORDER_CONFIRMATIONS . "`.`orderDocumentsNumber`,
						`" . TABLE_ORDER_CONFIRMATIONS . "`.`orderDocumentsDocumentDate`,
						`" . TABLE_ORDER_CONFIRMATIONS . "`.`orderDocumentsCustomerNumber`,
						`" . TABLE_ORDER_CONFIRMATIONS . "`.`orderDocumentsAddressCompany`,
						`" . TABLE_ORDER_CONFIRMATIONS . "`.`orderDocumentsTotalPrice`,
						`" . TABLE_ORDER_CONFIRMATIONS . "`.`orderDocumentsStatus`,
						`" . TABLE_ORDER_CONFIRMATIONS . "`.`orderDocumentsDocumentPath`,
						
						`" . TABLE_ORDER_CONFIRMATIONS . "`.`orderDocumentsBankAccount`,
						`" . TABLE_ORDER_CONFIRMATIONS . "`.`orderDocumentsPaymentCondition`,
						`" . TABLE_ORDER_CONFIRMATIONS . "`.`orderDocumentsPaymentType`,

						`" . TABLE_RELATED_DOCUMENTS . "`.`relatedDocuments_AB`,
						`" . TABLE_RELATED_DOCUMENTS . "`.`relatedDocuments_RE`,

						`" . TABLE_ORDERS . "`.`ordersID`,

						GROUP_CONCAT(`" . TABLE_ORDERS . "`.`ordersStatus`) AS `ordersStatus`,
						`" . TABLE_ORDERS . "`.`ordersBestellDatum`,
						`" . TABLE_ORDERS . "`.`ordersLieferDatum`,


						`" . TABLE_ORDER_CONFIRMATIONS_DETAILS . "`.`orderDocumentDetailOrderID`,
						`" . TABLE_ORDER_CONFIRMATIONS_DETAILS . "`.`orderDocumentDetailProductQuantity`,
						`" . TABLE_ORDER_CONFIRMATIONS_DETAILS . "`.`orderDocumentDetailProductNumber`,
						`" . TABLE_ORDER_CONFIRMATIONS_DETAILS . "`.`orderDocumentDetailProductName`,
						`" . TABLE_ORDER_CONFIRMATIONS_DETAILS . "`.`orderDocumentDetailPosType`,

						`" . TABLE_CUSTOMERS . "`.`customersKundennummer`,
						`" . TABLE_CUSTOMERS . "`.`customersFirmenname`,
						`" . TABLE_CUSTOMERS . "`.`customersCompanyPLZ`,
						`" . TABLE_CUSTOMERS . "`.`customersCompanyOrt`

						FROM `" . TABLE_ORDER_CONFIRMATIONS . "`

						LEFT JOIN `" . TABLE_RELATED_DOCUMENTS . "`
						ON(`" . TABLE_ORDER_CONFIRMATIONS . "`.`orderDocumentsNumber` = `" . TABLE_RELATED_DOCUMENTS . "`.`relatedDocuments_AB`)

						LEFT JOIN `" . TABLE_ORDER_CONFIRMATIONS_DETAILS . "`
						ON(`" . TABLE_ORDER_CONFIRMATIONS . "`.`orderDocumentsID` = `" . TABLE_ORDER_CONFIRMATIONS_DETAILS . "`.`orderDocumentDetailDocumentID`)

						LEFT JOIN `" . TABLE_ORDERS . "`
						ON(`" . TABLE_ORDER_CONFIRMATIONS_DETAILS . "`.`orderDocumentDetailOrderID` = `" . TABLE_ORDERS . "`.`ordersID`)

						LEFT JOIN `" . TABLE_CUSTOMERS . "`
						ON(`" . TABLE_ORDER_CONFIRMATIONS . "`.`orderDocumentsCustomerNumber` = `" . TABLE_CUSTOMERS . "`.`customersKundennummer`)

						WHERE 1
							AND `" . TABLE_ORDER_CONFIRMATIONS . "`.`orderDocumentsStatus` != 3
							AND `" . TABLE_ORDER_CONFIRMATIONS . "`.`orderDocumentsDocumentDate` <= '" . $thisSearchDateEnd . "'

						GROUP BY `" . TABLE_ORDER_CONFIRMATIONS . "`.`orderDocumentsID`

						HAVING
							`" . TABLE_RELATED_DOCUMENTS . "`.`relatedDocuments_RE` IS NULL
							AND
							`" . TABLE_ORDER_CONFIRMATIONS_DETAILS . "`.`orderDocumentDetailPosType` = 'product'

						ORDER BY
							`" . TABLE_ORDER_CONFIRMATIONS . "`.`orderDocumentsDocumentDate` ASC,
							`" . TABLE_ORDER_CONFIRMATIONS . "`.`orderDocumentsStatus` DESC
					) AS `tempTable`

					" . $sqlOtherMandators_LeftJoin . "

					WHERE 1
		";

	if($_POST["searchStatusDelievered"] == '1'){
		$sql .= " AND (
					`tempTable`.`ordersStatus` = '4'
					OR
					`tempTable`.`ordersStatus` LIKE '%,4'
					OR
					`tempTable`.`ordersStatus` LIKE '4,%'
				)
			";
	}
	if($_POST["searchStatusPayed"] == '1'){
		$sql .= " AND (
					`tempTable`.`orderDocumentsStatus` = '2'
				)
			";
	}

	if($_REQUEST["page"] == "" || $_REQUEST["page"] == 0) {
		$_REQUEST["page"] = 1;
	}
	if(MAX_ORDERS_PER_PAGE > 0) {
		// $sql .= " LIMIT " . (($_REQUEST["page"] - 1) * MAX_ORDERS_PER_PAGE) . ", " . MAX_ORDERS_PER_PAGE." ";
	}

	$rs = $dbConnection->db_query($sql);

	$sql_totalRows = "SELECT FOUND_ROWS() AS `countRows`";
	$rs_totalRows = $dbConnection->db_query($sql_totalRows);
	list($countRows) = mysqli_fetch_array($rs_totalRows);

	$pagesCount = ceil($countRows / MAX_ORDERS_PER_PAGE);




	while($ds = mysqli_fetch_assoc($rs)) {
		foreach(array_keys($ds) as $field){
			$arrDocumentDatas[$ds["orderDocumentsID"]][$field] = $ds[$field];
		}
	}
	#dd('sql');
	#dd('arrDocumentDatas');
	// EOF GET SELECTED DOCUMENT DATAS
?>
<?php
	require_once('inc/headerHTML.inc.php');
	$thisTitle = "Auftragsbest&auml;tigungen ohne Rechnungen - bis " . formatDate($thisSearchDateEnd, "display") . "";
	$headerHTML = preg_replace("/{###TITLE###}/", ($thisTitle), $headerHTML);
	$headerHTML = preg_replace("/{###TITLE###}/", strip_tags($thisTitle), $headerHTML);
	echo $headerHTML;

	require_once(FILE_MENUE_TOP);
?>
<div id="mainArea">
	<div id="mainContent">
		<?php require_once(FILE_MENUE_SIDEBAR); ?>
		<div id="contentArea">
			<div id="contentAreaElements">
				<h1><?php if(SHOW_PAGE_TITLE_ICONS) { echo '<img src="' . PATH_ICONS_MENUE_TITLES . 'noInvoices.png' . '" alt="" />'; } ?> <?php echo $thisTitle; ?></h1>

				<div id="searchFilterArea">
					<form name="formSearchDatas" method="post" action="<?php echo $_SERVER["PHP_SELF"]; ?>">
						<table border="0" cellpadding="0" cellspacing="0" width="" class="searchFilterContent">
							<tr>
								<!--
								<td>
									<label for="searchDateStart">von:</label>
									<input type="text" name="searchDateStart" id="searchDateStart" maxlength="2" class="inputField_70" readonly value="<?php if($_POST["searchDateStart"] != '') { echo $_POST["searchDateStart"]; } else { echo date ('d.m.Y', mktime(0, 0, 0,(date("m") - 3), 1, date("Y"))); } ?>" />
								</td>
								-->
								<td>
									<label for="searchDateEnd">AB-DATUM bis:</label>
									<input type="text" name="searchDateEnd" id="searchDateEnd" maxlength="2" class="inputField_70" readonly value="<?php echo formatDate($thisSearchDateEnd, "display"); ?>" />
								</td>
								<td>
									<input type="checkbox" name="searchStatusDelievered" id="searchStatusDelievered" value="1" <?php if($_POST["searchStatusDelievered"] == '1'){ echo ' checked="checked" '; } ?>> nur verschickte Auftr&auml;ge filtern
								</td>
								<td>
									<input type="checkbox" name="searchStatusPayed" id="searchStatusPayed" value="1" <?php if($_POST["searchStatusPayed"] == '1'){ echo ' checked="checked" '; } ?>> nur bezahlte Auftr&auml;ge filtern
								</td>
								<td>
									<input type="hidden" name="editID" id="editID" value="" />
									<input type="submit" name="submitExport" class="inputButton0" value="Daten anzeigen" />
								</td>
							</tr>
						</table>
						<input type="hidden" name="submitSearchForm" id="submitSearchForm" value="1" />
					</form>
				</div>

				<?php
					if($warningMessage != "") {	echo '<p class="warningArea">'.$warningMessage.'</p>'; }
					if($errorMessage != "") {	echo '<p class="errorArea">'.$errorMessage.'</p>'; }
					if($successMessage != "") {	echo '<p class="successArea">'.$successMessage.'</p>'; }
					if($infoMessage != "") {	echo '<p class="infoArea">'.$infoMessage.'</p>'; }
				?>

				<?php
					if($pagesCount > 1) {
						include(FILE_MENUE_PAGES);
					}
#dd('arrDocumentDatas');
					if(!empty($arrDocumentDatas)) {
						echo '<table border="0" cellpadding="0" cellspacing="0" width="100%" class="displayOrders">';
						echo '<thead>';
						echo '<tr>';
						echo '<th style="width:45px;text-align:right;">#</th>';
						echo '<th>AB-Nr</th>';

						echo '<th>Kunden-Nr</th>';
						echo '<th>Kunde</th>';

						echo '<th>AB-Datum</th>';
						echo '<th>Bestelldatum</th>';
						echo '<th>Lieferdatum</th>';

						echo '<th>AB-Zahlstatus</th>';
						echo '<th>Konto</th>';
						echo '<th>Zahlart</th>';
						
						echo '<th>Produktionsstatus</th>';

						echo '<th>AB-Summe</th>';

						if(!empty($arrOtherMandators)){
							foreach($arrOtherMandators as $thisOtherMandators){
								echo '<th>' . strtoupper($thisOtherMandators) . '-Rechnung</th>';
							}
						}

						echo '<th>Info</th>';
						echo '</tr>';
						echo '</thead>';

						$count = 0;

						$arrThisPath = parse_url($_SERVER["REQUEST_URI"]);
						$linkPath = $arrThisPath["path"];

						echo '<tbody>';
						foreach($arrDocumentDatas as $thisKey => $thisValue){
							if($count%2 == 0){ $rowClass = 'row0'; }
							else { $rowClass = 'row1'; }

							$thisStyle = '';
							if($arrDocumentDatas[$thisKey]["orderDocumentsStatus"] == '3'){
								$thisStyle = ' style="font-style:italic; color:#FF0000;" ';
							}
							else if($arrDocumentDatas[$thisKey]["orderDocumentsStatus"] == '2'){
								$thisStyle = '';
								$rowClass = 'row3';
							}

							echo '<tr class="'.$rowClass.'" ' . $thisStyle . '>';
							echo '<td style="text-align:right;" title="' . $arrDocumentDatas[$thisKey]["orderDocumentsStatus"] . '|' . $arrPaymentStatusTypeDatas[$arrDocumentDatas[$thisKey]["orderDocumentsStatus"]]["paymentStatusTypesName"] . '"><b>' . ($count + 1). '.</b></td>';

							echo '<td>';
							echo $arrDocumentDatas[$thisKey]["orderDocumentsNumber"];
							echo '</td>';

							echo '<td><a href="' . PAGE_EDIT_CUSTOMER . '?editCustomerNumber=' . $arrDocumentDatas[$thisKey]["orderDocumentsCustomerNumber"] . '" title="Kunden anzeigen">' . $arrDocumentDatas[$thisKey]["orderDocumentsCustomerNumber"] . '</a></td>';
							echo '<td style="white-space:nowrap;">';
							echo $arrDocumentDatas[$thisKey]["orderDocumentsAddressCompany"];

							echo '<div style="font-size:10px;border-top:1px dotted #333;">';
							echo $arrDocumentDatas[$thisKey]["customersFirmenname"];
							echo ' &bull; ';
							echo $arrDocumentDatas[$thisKey]["customersCompanyPLZ"] . ' ' . $arrDocumentDatas[$thisKey]["customersCompanyOrt"];
							echo '</div>';

							echo '</td>';

							echo '<td>' . formatDate($arrDocumentDatas[$thisKey]["orderDocumentsDocumentDate"], 'display') . '</td>';
							echo '<td style="white-space:nowrap;">' . formatDate($arrDocumentDatas[$thisKey]["ordersBestellDatum"], 'display') . '</td>';
							echo '<td style="white-space:nowrap;">';
							echo formatDate($arrDocumentDatas[$thisKey]["ordersLieferDatum"], 'display');
							echo ' <span class="toolItem">';
							echo '<a href="' . PAGE_DELIVERY_HISTORY . '?searchCustomerNumber=' . $arrDocumentDatas[$thisKey]["orderDocumentsCustomerNumber"] . '"><img src="layout/icons/truck1.png" width="12" height="12" title="DPD-Tracking f&uuml;r diese Kundennummer ansehen" alt="" /></a>';
							echo '</span>';
							echo '</td>';

							$thisStyle = '';
							$cellClass = '';
							if($arrDocumentDatas[$thisKey]["orderDocumentsStatus"] == '3'){
								$thisStyle = ' style="font-style:italic; color:#FF0000;" ';
								$cellClass = '';
							}
							else if($arrDocumentDatas[$thisKey]["orderDocumentsStatus"] == '2'){
								$thisStyle = '';
								$cellClass = 'row3';
							}
							echo '<td class="' . $cellClass . '" style="white-space:nowrap; ' . $thisStyle . '">';

							echo '<div style="width:120px;">';

							echo '<span style="width:40px; float:right;">';
							echo ' <span class="toolItem">';
							echo '<a href="' . PAGE_ALL_CONFIRMATIONS . '?searchDocumentNumber=' . $arrDocumentDatas[$thisKey]["orderDocumentsNumber"] . '"><img src="layout/icons/iconEdit.gif" width="12" height="12" title="AB-Status &auml;ndern" alt="" /></a>';
							echo '</span>';
							echo ' <span class="toolItem">';
							echo '<a href="' . PAGE_DISPLAY_CONFIRMATION . '?searchDocumentNumber=' . $arrDocumentDatas[$thisKey]["orderDocumentsNumber"] . '"><img src="layout/icons/convertDocument.png" width="12" height="12" title="AB in Rechnung umwandeln" alt="" /></a>';
							echo '</span>';
							echo '</span>';

							echo '</div>';

							echo '<span style="">';
							echo $arrPaymentStatusTypeDatas[$arrDocumentDatas[$thisKey]["orderDocumentsStatus"]]["paymentStatusTypesName"];
							echo '</span>';
							echo '</td>';
							
							echo '<td>';
							echo $arrDocumentDatas[$thisKey]["orderDocumentsBankAccount"];
							echo '</td>';
							
							echo '<td>';						
							echo $arrPaymentTypeDatas[$arrDocumentDatas[$thisKey]["orderDocumentsPaymentType"]]["paymentTypesName"];
							#echo '<br />' . $arrPaymentConditionDatas[$arrDocumentDatas[$thisKey]["orderDocumentsPaymentType"]]["paymentConditionsName"];
							echo '</td>';

							$thisStyle = '';
							$cellClass = 'row0';
							$statusAdd = '';

							$arrTemp = explode(',', $arrDocumentDatas[$thisKey]["ordersStatus"]);
							$arrTemp = array_unique($arrTemp);
							if(!empty($arrTemp)){
								if(count($arrTemp) == 1){
									$arrDocumentDatas[$thisKey]["ordersStatus"] = $arrTemp[0];
									if($arrDocumentDatas[$thisKey]["ordersStatus"] == '6'){
										$thisStyle = ' font-style:italic; color:#FF0000; ';
										$cellClass = '';
									}
									else if($arrDocumentDatas[$thisKey]["ordersStatus"] == '4'){
										$thisStyle = '';
										$cellClass = 'row3';
									}
									else if($arrDocumentDatas[$thisKey]["ordersStatus"] == '3'){
										$thisStyle = '';
										$cellClass = 'row2';
									}
								}

								else if(in_array('4', $arrTemp)){
									$thisStyle = '';
									$cellClass = 'row3';
									$statusAdd = 'Teil-';
									$arrDocumentDatas[$thisKey]["ordersStatus"] = '4';
								}
							}
							else {

							}
							echo '<td class="' . $cellClass . '" style="white-space:nowrap; ' . $thisStyle . '">';
							echo $statusAdd . $arrOrderStatusTypeDatas[$arrDocumentDatas[$thisKey]["ordersStatus"]]["orderStatusTypesName"];

							echo '<td style="text-align:right;white-space:nowrap;">' . convertDecimal($arrDocumentDatas[$thisKey]["orderDocumentsTotalPrice"], 'display') . ' &euro;</td>';

							if(!empty($arrOtherMandators)){
								foreach($arrOtherMandators as $thisOtherMandators){
									echo '<td>';
									echo $arrDocumentDatas[$thisKey][$thisOtherMandators . "_" . "orderDocumentsNumber"];
									echo '</td>';
								}
							}

							$thisParsedUrl = parse_url($_SERVER["REQUEST_URI"]);
							echo '<td style="white-space:nowrap;">';

							echo '<span class="toolItem">';
							echo ' <img src="layout/icons/iconInfo.png" class="buttonPaymentInfo" rel="' . $arrDocumentDatas[$thisKey]["orderDocumentsNumber"] . '" title="Zahlungsdetails" width="12" height="12" />';
							echo '</span>';

							echo '<span class="toolItem">';
							echo '<a href="' . $thisParsedUrl["path"] . '?downloadFile=' . basename($arrDocumentDatas[$thisKey]["orderDocumentsDocumentPath"]) . '&thisDocumentType=AB">' . '<img src="layout/icons/icon' . getFileType(basename($arrDocumentDatas[$thisKey]["orderDocumentsDocumentPath"])) . '.gif" width="16" height="16" title="Dokument &quot;' . $arrDocumentDatas[$thisKey]["orderDocumentsNumber"] . '&quot; herunterladen" alt="Download" /></a>';
							echo '</span>';

							if($arrDocumentDatas[$thisKey]["orderDocumentsStatus"] != '3'){
								echo '<span class="toolItem">';
								echo '<img src="layout/icons/mailAttachement.png" class="buttonSendDocument" rel="' . urlencode(basename($arrDocumentDatas[$thisKey]["orderDocumentsDocumentPath"])) . '#AB" width="16" height="16" title="Dokument &quot;' . $arrDocumentDatas[$thisKey]["orderDocumentsNumber"] . '&quot; direkt per Mail versenden" alt="Dokument versenden" />';
								echo '</span>';
							}
							echo '</td>';
							echo '</tr>';
							$count++;
						}
						echo '</tbody>';

						echo '</table>';

						if($pagesCount > 1) {
							include(FILE_MENUE_PAGES);
						}
					}
					else {
						echo '<p class="infoArea">F&uuml;r Ihre gew&auml;hlte Suchoptionen liegen momentan keine ABs ohne Rechnungen vor.</p>';
					}
				?>
			</div>
		</div>
		<div class="clear"></div>
	</div>
	<div class="clear"></div>
</div>

<script language="javascript" type="text/javascript">
	$(document).ready(function() {
		$(function() {
			$.datepicker.setDefaults($.datepicker.regional[""]);
			$('#searchDateEnd').datepicker($.datepicker.regional["de"]);
			$('#searchDateEnd').datepicker("option", "maxDate", "0" );
		});
		$('.buttonClearField').live('click', function () {
			$(this).parent().find('input').val('');
		});
		colorRowMouseOver('.displayOrders tbody tr');

		$('.buttonLoadOrderDetails').click(function() {
			loadOrderDocumentDetails($(this).attr('alt'), '<?php echo $arrDocumentTypeDatas[$_REQUEST["documentType"]]["createdDocumentsTypesName"]; ?>', '<?php echo $_REQUEST["documentType"]; ?>');
		});

		$('.buttonSendDocument').click(function() {
			var mailDocumentFilename = $(this).attr('rel');
			var mailDocumentCustomerNumber = '';
			var mailDocumentRecipient = '';
			sendAttachedDocument($(this), mailDocumentFilename, mailDocumentCustomerNumber, '<?php echo $_REQUEST["editID"]; ?>', '', '<?php echo $_SERVER["REQUEST_URI"]; ?>', mailDocumentRecipient);
		});

		$('.buttonPaymentInfo').css('cursor', 'pointer');
		$('.buttonPaymentInfo').click(function () {
			loadPaymentDetails($(this), $(this).attr('rel'), '<?php echo $linkPath; ?>');
			//loadOrderDetails($(this), '<?php echo BASEPATH; ?>', 'Details der Zahlung');
		});
	});
</script>

<?php require_once('inc/footerHTML.inc.php'); ?>