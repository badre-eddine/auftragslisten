<?php
	require_once('inc/requires.inc.php');

	if(!$arrGetUserRights["displayOrdersOverview"] && !$arrGetUserRights["displayOrders"] && !$arrGetUserRights["editOrders"]) {
		header('location: ' . PAGE_EXIT_LOCATION);
		exit;
	}

	$jswindowMessage = "";
	$warningMessage = '';
	$errorMessage = '';
	$successMessage = '';
	$infoMessage = '';

	$dbConnection = new DB_Connection();
	$db_open = $dbConnection->db_connect();

	// BOF READ BESTELL STATUS TYPEN
		$arrOrderStatusTypeDatas = getOrderStatusTypes();
		$arrOrderStatusTypeDatasExtern = getOrderStatusTypesExtern();
	// EOF READ BESTELL STATUS TYPEN

	// EOF GET TRANSFER TRANSPORT TYPES
		$arrTransferTransportTypes = getTransferTransportTypes();
	// BOF GETTRANSFER TRANSPORT TYPES

	// BOF GET PRINT TYPES
		$arrOrderTypes = getOrderTypes();
	// EOF GET PRINT TYPES

	// BOF READ PRINTTYPES
		$arrPrintTypeDatas = getPrintTypes();
	// EOF READ PRINTTYPES

	if($_REQUEST["searchTransferDateSort"] == ''){
		$_REQUEST["searchTransferDateSort"] = 'DESC';
	}

	// BOF DOWNLOAD FILE
		if($_REQUEST["downloadFile"] != "") {
			require_once('classes/downloadFile.class.php');
			$arrTemp = pathinfo($_REQUEST["downloadFile"]);
			$thisDownloadFile = $arrTemp["basename"];
			$thisDir = $arrTemp["dirname"] . "/";
			// $thisDir = "http://productions.burhan-ctr.de/production_sheets/";

			$thisDownload = new downloadFile($thisDir, $thisDownloadFile);
			$errorMessage .= $thisDownload->startDownload();
		}
	// EOF DOWNLOAD FILE

	// BOF SEND MAIL WITH ATTACHED DOCUMENT
		if($_POST["sendAttachedDocument"] == '1') {
			$thisCreatedFileName = rawurldecode($_POST["mailDocumentFilename"]);
			$generatedDocumentNumber = basename($thisCreatedFileName);

			require_once(DIRECTORY_HTML_MIME_MAIL . 'htmlMimeMail.php');
			require_once(DIRECTORY_MAIL_TEMPLATES . 'template_BASIC.inc.php');
			require_once(DIRECTORY_MAIL_TEMPLATES . 'template_TEXT_DATAS.inc.php');
			#require_once("inc/mail.inc.php");

			// BOF SEND MAIL
			require_once("classes/createMail.class.php");

			// BOF CREATE SUBJECT
			$thisSubject = $_POST["selectSubject"];
			if($_POST["sendAttachedMailSubject"] != '') {
				$thisSubject = $_POST["sendAttachedMailSubject"] . ', ' . $thisSubject;
			}
			// EOF CREATE SUBJECT

			// BOF GET ATTACHED FILE
			$arrPathInfo = pathinfo($thisCreatedFileName);

			$pathDocumentFolder = $arrPathInfo["dirname"] . "/";
			$thisCreatedFileName = $arrPathInfo["basename"];

			$arrAttachmentFiles = array (
				rawurldecode($pathDocumentFolder . $thisCreatedFileName) => str_replace(" ", "_", rawurldecode(myUnHtmlEntities($thisCreatedFileName)))
			);
			// EOF GET ATTACHED FILE

			$thisSubject .= " - " . $arrPathInfo["filename"];
			$thisDocumentType = "PA";
			$generatedDocumentNumber = "";
			$arrThisMailContentDatas = $arrMailContentDatas["DEFAULT"];

			$createMail = new createMail(
								$thisSubject,													// TEXT:	MAIL_SUBJECT
								$thisDocumentType,												// STRING:	DOCUMENT TYPE
								$generatedDocumentNumber,										// STRING:	DOCUMENT NUMBER
								$arrAttachmentFiles,											// ARRAY:	ATTACHMENT FILES
								$_POST["selectCustomersRecipientMail"],	 						// STRING:	RECIPIENTS
								$arrThisMailContentDatas,										// MAIL_TEXT_TEMPLATE
								$mailTemplate["html"],											// MAIL_BODY_TEMPLATE
								'',																// STRING:	ADDITIONAL TEXT
								$_POST["sendAttachedMailText"],									// STRING:	SEND ATTACHED TEXT
								true,															// BOOLEAN:	SEND ATTACHED DOCUMENT
								DIRECTORY_MAIL_IMAGES,											// DIRECTORY_MAIL_IMAGES
								$_POST["selectMailtextSender"]									// STRING: SENDER
							);

			$createMailResult = $createMail->returnResult();
			$sendMailToClient = $createMailResult;
			// EOF SEND MAIL

			#if(!$sendMailToUs) { $errorMessage .= ' Die Mail-Kopie konnte nicht an ' . implode(', ', $arrOrderMail) . ' versendet werden! '.'<br />'; }
			#else { $successMessage .= ' Die Mail-Kopie wurde an ' . implode(', ', $arrOrderMail) . ' versendet! '.'<br />'; }

			if(!$sendMailToClient) { $errorMessage .= ' Der Produktionsauftrag (ID: ' . $_POST["orderID"] . ') &quot;KNR-' . $_POST["editCustomerNumber"] . '&quot; konnte nicht an ' . $_POST["selectCustomersRecipientName"] . ' (' . $_POST["selectCustomersRecipientMail"] . ') versendet werden! '.'<br />'; }
			else {
				$successMessage .= ' Der Produktionsauftrag (ID: ' . $_POST["orderID"] . ') &quot;KNR-' . $_POST["editCustomerNumber"] . '&quot; wurde an ' . $_POST["selectCustomersRecipientName"] . ' (' . $_POST["selectCustomersRecipientMail"] . ') versendet! '.'<br />';
			}
			unset($_POST["exportOrdersVertreterID"]);
		}
	// EOF SEND MAIL WITH ATTACHED DOCUMENT



	// BOF COUNT STATUS TYPE
		$sql = "SELECT
					`" . TABLE_ORDER_STATUS_TYPES_EXTERN . "`.`orderStatusTypesID`,
					count(`" . TABLE_PRODUCTIONS_TRANSFER . "`.`ordersProductionsTransferID`) AS `ordersProductionsTransferProductionStatusCount`,
					`orderStatusTypesShortName`,
					`orderStatusTypesName`

					FROM `" . TABLE_ORDER_STATUS_TYPES_EXTERN . "`

					LEFT JOIN `" . TABLE_PRODUCTIONS_TRANSFER . "`
					ON(`" . TABLE_ORDER_STATUS_TYPES_EXTERN . "`.`orderStatusTypesID` = `" . TABLE_PRODUCTIONS_TRANSFER . "`.`ordersProductionsTransferProductionStatus`)

					WHERE 1
						AND `" . TABLE_ORDER_STATUS_TYPES_EXTERN . "`.`orderStatusTypesActive` = '1'
						AND (
							`" . TABLE_ORDER_STATUS_TYPES_EXTERN . "`.`orderStatusTypesID` = '1'
							OR
							`" . TABLE_ORDER_STATUS_TYPES_EXTERN . "`.`orderStatusTypesID` = '4'
							OR
							`" . TABLE_ORDER_STATUS_TYPES_EXTERN . "`.`orderStatusTypesID` = '6'
							OR
							`" . TABLE_ORDER_STATUS_TYPES_EXTERN . "`.`orderStatusTypesID` = '7'
							OR
							`" . TABLE_ORDER_STATUS_TYPES_EXTERN . "`.`orderStatusTypesID` = '8'
						)

					GROUP BY `" . TABLE_ORDER_STATUS_TYPES_EXTERN . "`.`orderStatusTypesID`

					ORDER BY `" . TABLE_ORDER_STATUS_TYPES_EXTERN . "`.`orderStatusTypesID`
			";

		$rs = $dbConnection->db_query($sql);

		$countStatusTypesTotal = 0;
		while($ds = mysqli_fetch_assoc($rs)) {
			$countStatusTypesTotal += $ds["ordersProductionsTransferProductionStatusCount"];
			foreach(array_keys($ds) as $field) {
				$arrCountStatusTypes[$ds["orderStatusTypesID"]][$field] = $ds[$field];
			}
		}
	// EOF COUNT STATUS TYPE

	// BOF GET TRANSFER PRINT TYPES
		$sql_printTypes = "
				SELECT
						`" . TABLE_PRINT_TYPES . "`.`printTypesID`,
						`" . TABLE_PRINT_TYPES . "`.`printTypesShortName`,
						`" . TABLE_PRINT_TYPES . "`.`printTypesName`,
						`" . TABLE_PRODUCTIONS_TRANSFER . "`.`ordersProductionsTransferOrdersPrintType`
					FROM `" . TABLE_PRODUCTIONS_TRANSFER . "`

					INNER JOIN `" . TABLE_PRINT_TYPES . "`
					ON(`" . TABLE_PRODUCTIONS_TRANSFER . "`.`ordersProductionsTransferOrdersPrintType` = `" . TABLE_PRINT_TYPES . "`.`printTypesID`)

					WHERE 1
						AND `" . TABLE_PRODUCTIONS_TRANSFER . "`.`ordersProductionsTransferOrdersPrintType` > 0

					GROUP BY
						`" . TABLE_PRODUCTIONS_TRANSFER . "`.`ordersProductionsTransferOrdersPrintType`

					ORDER BY
						`" . TABLE_PRINT_TYPES . "`.`printTypesName`
			";
		$rs_printTypes = $dbConnection->db_query($sql_printTypes);

		$arrTransferPrintTypes = array();
		while($ds_printTypes = mysqli_fetch_assoc($rs_printTypes)) {
			foreach(array_keys($ds_printTypes) as $field) {
				$arrTransferPrintTypes[$ds_printTypes["printTypesID"]][$field] = $ds_printTypes[$field];
			}
		}
	// EOF GET TRANSFER PRINT TYPES

	// BOF GET TRANSFER PRODUCT CATEGORIES
		$sql_productCategories = "
				SELECT
					SUBSTRING(`ordersProductionsTransferOrdersProductCategory`, 1, 3) AS `productCategory`
				FROM `" . TABLE_PRODUCTIONS_TRANSFER . "`

				GROUP BY
					SUBSTRING(`ordersProductionsTransferOrdersProductCategory`, 1, 3)
			";
		$rs_productCategories = $dbConnection->db_query($sql_productCategories);

		$arrTransferProductCategories = array();

		while($ds_productCategories = mysqli_fetch_assoc($rs_productCategories)) {
			foreach(array_keys($ds_productCategories) as $field) {
				$arrTransferProductCategories[$ds_productCategories["productCategory"]][$field] = $ds_productCategories[$field];
			}
		}
	// EOF GET TRANSFER PRODUCT CATEGORIES

?>
<?php
	require_once('inc/headerHTML.inc.php');
	$thisTitle = "TR-Produktionstransfers";

	if($_POST["searchCustomerNumber"] != "") {
		$thisTitle .= " - Kundennummer ".$_POST["searchCustomerNumber"];
	}
	if($_POST["searchCustomerName"] != "") {
		$thisTitle .= " - Kunde ".$_POST["searchCustomerName"];
	}
	if($_POST["searchPLZ"] != "") {
		$thisTitle .= " - PLZ ".$_POST["searchPLZ"];
	}

	if($_POST["searchWord"] != "") {
		$thisTitle .= " - Suchbegriff ".$_POST["searchWord"];
	}
	if($_REQUEST["displayOrderStatus"] != "") {
		$thisTitle .= " - Status ".$arrOrderStatusTypeDatasExtern[$_REQUEST["displayOrderStatus"]]["orderStatusTypesName"];
	}

	if($_REQUEST["searchTransferPrintType"] != "") {
		$thisTitle .= " - Druckart ".$arrOrderStatusTypeDatasExtern[$_REQUEST["searchTransferPrintType"]]["orderStatusTypesName"];
	}
	if($_REQUEST["searchTransferDate"] != "") {
		$thisTitle .= " - Transfer-Datum " . formatDate($_REQUEST["searchTransferDate"], 'display');
	}
	if($_REQUEST["searchTransferWeek"] != "") {
		$thisTitle .= " - Transfer-KW " . $_REQUEST["searchTransferWeek"];
	}
	if($_REQUEST["searchTransferStatus"] != ""){
		$thisTitle .= " - TR-Status " . $arrOrderStatusTypeDatasExtern[$_REQUEST["searchTransferStatus"]]["orderStatusTypesName"];
	}
	if($_REQUEST["searchTransferPrintType"] != "") {
		$thisTitle .= " - Druckart &quot;" . $arrTransferPrintTypes[$_REQUEST["searchTransferPrintType"]]["printTypesName"] . "&quot;";
	}
	if($_REQUEST["searchTransferProductCategory"] != "") {
		#$thisTitle .= " - Kategorie &quot;" . $_REQUEST["searchTransferProductCategory"] . "&quot;";
	}

	$headerHTML = preg_replace("/{###TITLE###}/", strip_tags($thisTitle), $headerHTML);
	echo $headerHTML;

	require_once(FILE_MENUE_TOP);


	// BOF GET PRODUCTION DATA
		#$sqlSortField = " ORDER BY `" . TABLE_PRODUCTIONS_TRANSFER . "`.`ordersProductionsTransferMailSubmitDateTime` DESC ";
		$sqlSortField = " ORDER BY `" . TABLE_PRODUCTIONS_TRANSFER . "`.`ordersProductionsTransferMailSubmitDateTime` " . $_REQUEST["searchTransferDateSort"] . " ";
		$sqlSortDirection = "";

		$sqlWhere = "";

		// BOF CREATE SQL FOR DISPLAY ORDERS
			if($_REQUEST["searchPLZ"] != "") {
				$sqlWhere = " AND (
									`" . TABLE_PRODUCTIONS_TRANSFER . "`.`ordersProductionsTransferOrderRecipientAddress` LIKE '%" . $_REQUEST["searchPLZ"] . "%'
									OR
									`" . TABLE_PRODUCTIONS_TRANSFER . "`.`ordersProductionsTransferOrderSenderAddress` LIKE '%" . $_REQUEST["searchPLZ"] . "%'
					) ";
			}
			else if($_REQUEST["searchCustomerNumber"] != "") {
				$sqlWhere = "
					AND (
						`" . TABLE_PRODUCTIONS_TRANSFER . "`.`ordersProductionsTransferOrdersCustomerNumber` = '" . $_REQUEST["searchCustomerNumber"] . "'
						OR
						`" . TABLE_PRODUCTIONS_TRANSFER . "`.`ordersProductionsTransferOrderRecipientCustomerNumber` = '" . $_REQUEST["searchCustomerNumber"] . "'
						OR
						`" . TABLE_PRODUCTIONS_TRANSFER . "`.`ordersProductionsTransferOrderSenderCustomerNumber` = '" . $_REQUEST["searchCustomerNumber"] . "'
					) ";
			}
			else if($_REQUEST["searchCustomerName"] != "") {
				$sqlWhere = " AND (
									`" . TABLE_PRODUCTIONS_TRANSFER . "`.`ordersProductionsTransferOrdersCustomerName` LIKE '%" . $_REQUEST["searchCustomerName"] . "%'
									OR
									`" . TABLE_PRODUCTIONS_TRANSFER . "`.`ordersProductionsTransferOrdersKomission` LIKE '%" . $_REQUEST["searchCustomerName"] . "%'
					) ";

			}

			else if($_REQUEST["searchWord"] != "") {
				$sqlWhere = " AND (
								`" . TABLE_PRODUCTIONS_TRANSFER . "`.`ordersProductionsTransferOrdersCustomerNumber` = '" . $_REQUEST["searchWord"] . "'
								OR
								`" . TABLE_PRODUCTIONS_TRANSFER . "`.`ordersProductionsTransferOrderRecipientCustomerNumber` = '" . $_REQUEST["searchWord"] . "'
								OR
								`" . TABLE_PRODUCTIONS_TRANSFER . "`.`ordersProductionsTransferOrderSenderCustomerNumber` = '" . $_REQUEST["searchWord"] . "'
								OR
								`" . TABLE_PRODUCTIONS_TRANSFER . "`.`ordersProductionsTransferOrdersCustomerName` LIKE '%" . $_REQUEST["searchWord"] . "%'
								OR
								`" . TABLE_PRODUCTIONS_TRANSFER . "`.`ordersProductionsTransferOrdersKomission` LIKE '%" . $_REQUEST["searchWord"] . "%'
								OR
								`" . TABLE_PRODUCTIONS_TRANSFER . "`.`ordersProductionsTransferOrderRecipientAddress` LIKE '%" . $_REQUEST["searchWord"] . "%'
								OR
								`" . TABLE_PRODUCTIONS_TRANSFER . "`.`ordersProductionsTransferOrderSenderAddress` LIKE '%" . $_REQUEST["searchWord"] . "%'

							) ";
							// MATCH(`ordersVertreter`) AGAINST ('" . $_REQUEST["searchWord"] . "')
							// MATCH(`ordersKundenName`) AGAINST ('" . $_REQUEST["searchWord"] . "')
			}
			else if($_GET["searchBoxProduction"] != ""){
				$sqlWhere = " AND (
						`ordersPLZ` LIKE '" . addslashes($_GET["searchBoxProduction"]) . "%'
						OR
						`ordersKundennummer` LIKE '" . addslashes($_GET["searchBoxProduction"]) . "%'
						OR
						`ordersKundenName` LIKE '%" . addslashes($_GET["searchBoxProduction"]) . "%'
						OR
						`ordersKommission` LIKE '%" . addslashes($_GET["searchBoxProduction"]) . "%'
						OR
						`ordersOrt` LIKE '%" . addslashes($_GET["searchBoxProduction"]) . "%'
						OR
						`ordersVertreter` LIKE '%" . addslashes($_GET["searchBoxProduction"]) . "%'
						OR
						`ordersArtikelBezeichnung` LIKE '%" . addslashes($_GET["searchBoxProduction"]) . "%'
						OR
						`ordersArtikelNummer` LIKE '%" . addslashes($_GET["searchBoxProduction"]) . "%'
						OR
						`ordersDruckFarbe` LIKE '%" . addslashes($_GET["searchBoxProduction"]) . "%'
					) ";
					// MATCH(`ordersVertreter`) AGAINST ('" . $_GET["searchBoxProduction"] . "')
					// MATCH(`ordersKundenName`) AGAINST ('" . $_GET["searchBoxProduction"] . "')
			}

			if($_REQUEST["searchTransferPrintType"] != "" || $_REQUEST["searchTransferProductCategory"] != "") {
				$sqlWhere_PrintType = "";
				$sqlWhere_ProductCategory = "";

				if($_REQUEST["searchTransferPrintType"] != "") {
					$sqlWhere_PrintType = " AND `ordersProductionsTransferOrdersPrintType` = '" . $_REQUEST["searchTransferPrintType"] . "' ";
				}
				if($_REQUEST["searchTransferProductCategory"] != "") {
					$arrSearchProductCategories = array(
						"KZH" => array("001", "002", "005"),
						"MIN" => array("003"),
						"AWT" => array("006")
					);
					$arrMergedSearchProductCategories = array_unique(call_user_func_array('array_merge', $arrSearchProductCategories));

					if($_REQUEST["searchTransferProductCategory"] != "OTHERS"){
						$sqlWhere_ProductCategory = " AND SUBSTRING(`ordersProductionsTransferOrdersProductCategory`, 1, 3) IN (" . implode(", ", $arrSearchProductCategories[$_REQUEST["searchTransferProductCategory"]]) . ") ";
					}
					else if($_REQUEST["searchTransferProductCategory"] == "OTHERS"){
						$sqlWhere_ProductCategory = "
							AND SUBSTRING(`ordersProductionsTransferOrdersProductCategory`, 1, 3) NOT IN (" . implode(", ", $arrMergedSearchProductCategories) . ")
						";
					}
				}
				$sqlWhere .= $sqlWhere_PrintType  . $sqlWhere_ProductCategory;
			}
			if($_REQUEST["searchTransferDate"] != "") {
				$sqlWhere .= " AND DATE_FORMAT(`ordersProductionsTransferMailSubmitDateTime`, '%Y-%m-%d') = '" . formatDate($_REQUEST["searchTransferDate"], 'store') . "' ";
			}
			else if($_REQUEST["searchTransferWeek"] != "") {
				$sqlWhere .= " AND DATE_FORMAT(`ordersProductionsTransferMailSubmitDateTime`, '%Y-%v') = '" . $_REQUEST["searchTransferWeek"] . "' ";
			}

			if($_REQUEST["displayOrderStatus"] != "") {
				$sqlWhere .= " AND `ordersProductionsTransferProductionStatus` = '" . $_REQUEST["displayOrderStatus"] . "' ";
			}

			if($_REQUEST["searchTransferStatus"] != ""){
				$sqlWhere .= " AND `" . TABLE_PRODUCTIONS_TRANSFER . "`.`ordersProductionsTransferProductionStatus` = '" . $_REQUEST["searchTransferStatus"] . "' ";
			}

			if(!empty($_REQUEST["arrSearchTransferStatus"])){
				$sqlWhere .= " AND `" . TABLE_PRODUCTIONS_TRANSFER . "`.`ordersProductionsTransferProductionStatus` IN(" . implode(", ", array_keys($_REQUEST["arrSearchTransferStatus"])) . ") ";
			}

		$sql = "
			SELECT

				/* SQL_CALC_FOUND_ROWS */

				`" . TABLE_PRODUCTIONS_TRANSFER . "`.`ordersProductionsTransferID`,

				`" . TABLE_PRODUCTIONS_TRANSFER . "`.`ordersProductionsTransferOrdersID`,

				`" . TABLE_PRODUCTIONS_TRANSFER . "`.`ordersProductionsTransferDeliveryRedDot`,
				`" . TABLE_PRODUCTIONS_TRANSFER . "`.`ordersProductionsTransferDeliveryNoDPD`,
				`" . TABLE_PRODUCTIONS_TRANSFER . "`.`ordersProductionsTransferDeliveryContainerDate`,

				`" . TABLE_PRODUCTIONS_TRANSFER . "`.`ordersProductionsTransferMailSubmitDateTime`,
				DATE_FORMAT(`" . TABLE_PRODUCTIONS_TRANSFER . "`.`ordersProductionsTransferMailSubmitDateTime`, '%Y-%v') AS `ordersProductionsTransferMailSubmitDateKW`,

				`" . TABLE_PRODUCTIONS_TRANSFER . "`.`ordersProductionsTransferDeliveryPrintDate`,
				`" . TABLE_PRODUCTIONS_TRANSFER . "`.`ordersProductionsTransferMailLanguage`,
				`" . TABLE_PRODUCTIONS_TRANSFER . "`.`ordersProductionsTransferMailSubject`,
				`" . TABLE_PRODUCTIONS_TRANSFER . "`.`ordersProductionsTransferMailRecipient`,

				`" . TABLE_PRODUCTIONS_TRANSFER . "`.`ordersProductionsTransferLayoutFileName`,
				`" . TABLE_PRODUCTIONS_TRANSFER . "`.`ordersProductionsTransferLayoutFileSize`,
				`" . TABLE_PRODUCTIONS_TRANSFER . "`.`ordersProductionsTransferLayoutFileType`,

				`" . TABLE_PRODUCTIONS_TRANSFER . "`.`ordersProductionsTransferProductionFileName`,
				`" . TABLE_PRODUCTIONS_TRANSFER . "`.`ordersProductionsTransferProductionFileSize`,

				`" . TABLE_PRODUCTIONS_TRANSFER . "`.`ordersProductionsTransferOrdersCustomerNumber`,
				`" . TABLE_PRODUCTIONS_TRANSFER . "`.`ordersProductionsTransferOrdersCustomerName`,

				`" . TABLE_PRODUCTIONS_TRANSFER . "`.`ordersProductionsTransferOrdersPrintText`,
				`" . TABLE_PRODUCTIONS_TRANSFER . "`.`ordersProductionsTransferOrdersKomission`,

				`" . TABLE_PRODUCTIONS_TRANSFER . "`.`ordersProductionsTransferOrdersProductNumber`,
				`" . TABLE_PRODUCTIONS_TRANSFER . "`.`ordersProductionsTransferOrdersProductCategory`,
				`" . TABLE_PRODUCTIONS_TRANSFER . "`.`ordersProductionsTransferOrdersProductName`,
				`" . TABLE_PRODUCTIONS_TRANSFER . "`.`ordersProductionsTransferOrdersProductQuantity`,
				`" . TABLE_PRODUCTIONS_TRANSFER . "`.`ordersProductionsTransferOrdersProductOriginalQuantity`,

				`" . TABLE_PRODUCTIONS_TRANSFER . "`.`ordersProductionsTransferOrdersProductTatsaechlicheProduktion`,


			`" . TABLE_PRODUCTIONS_TRANSFER . "`.`ordersProductionsTransferOrdersColorsQuantity`,
				`" . TABLE_PRODUCTIONS_TRANSFER . "`.`ordersProductionsTransferOrdersColorsNames`,
				`" . TABLE_PRODUCTIONS_TRANSFER . "`.`ordersProductionsTransferOrdersWithClearPaint`,

				`" . TABLE_PRODUCTIONS_TRANSFER . "`.`ordersProductionsTransferOrderRecipientCustomerNumber`,
				`" . TABLE_PRODUCTIONS_TRANSFER . "`.`ordersProductionsTransferOrderRecipientAddress`,

				`" . TABLE_PRODUCTIONS_TRANSFER . "`.`ordersProductionsTransferOrderSenderCustomerNumber`,
				`" . TABLE_PRODUCTIONS_TRANSFER . "`.`ordersProductionsTransferOrderSenderAddress`,

				`" . TABLE_PRODUCTIONS_TRANSFER . "`.`ordersProductionsTransferUserID`,

				`" . TABLE_PRODUCTIONS_TRANSFER . "`.`ordersProductionsTransferProductionPrinter`,
				`" . TABLE_PRODUCTIONS_TRANSFER . "`.`ordersProductionsTransferProductionStatus`,
				`" . TABLE_PRODUCTIONS_TRANSFER . "`.`ordersProductionsTransferProductionTrackingNumbers`,
				`" . TABLE_PRODUCTIONS_TRANSFER . "`.`ordersProductionsTransferProductionNotice`,

				`" . TABLE_PRODUCTIONS_TRANSFER . "`.`ordersProductionsTransferProductionModified`,

				`" . TABLE_PRODUCTIONS_TRANSFER . "`.`ordersProductionsTransferSuccess_MYSQL`,
				`" . TABLE_PRODUCTIONS_TRANSFER . "`.`ordersProductionsTransferSuccess_FTP`,

				`" . TABLE_PRODUCTIONS_TRANSFER . "`.`ordersProductionsTransferMandatory`,

				`" . TABLE_PRODUCTIONS_TRANSFER . "`.`ordersProductionsTransferTransportType`,

				`" . TABLE_PRODUCTIONS_TRANSFER . "`.`ordersProductionsTransferOrdersPrintType`,

				`" . TABLE_PRODUCTIONS_TRANSFER . "`.`ordersProductionsTransferUseNoNeutralPacking`,

				`" . TABLE_PRODUCTIONS_TRANSFER . "`.`ordersProductionsTransferPrintPlateNumber`,
				`" . TABLE_PRODUCTIONS_TRANSFER . "`.`ordersProductionsTransferNotice`,
				`" . TABLE_PRODUCTIONS_TRANSFER . "`.`ordersProductionsTransferOrderType`


			FROM `" . TABLE_PRODUCTIONS_TRANSFER . "`
			WHERE 1
			" . $sqlWhere . " ". $sqlSortField . " ". $sqlSortDirection;
	// EOF GET PRODUCTION DATA

	// BOF GET TRANSFER DATES
		$sql_getTransferDates = "
				SELECT
					DATE_FORMAT(`ordersProductionsTransferMailSubmitDateTime`, '%Y-%m-%d') AS `transferDate`,
					DATE_FORMAT(`ordersProductionsTransferMailSubmitDateTime`, '%Y-%v') AS `transferWeek`
					FROM `" . TABLE_PRODUCTIONS_TRANSFER . "`
					GROUP BY `transferDate`
					ORDER BY `transferDate` DESC

			";
		$rs_getTransferDates = $dbConnection->db_query($sql_getTransferDates);
		$arrTransferDates = array();
		$arrTransferWeeks = array();
		while($ds_getTransferDates = mysqli_fetch_assoc($rs_getTransferDates)){
			$arrTransferDates[] = $ds_getTransferDates["transferDate"];
			$arrTransferWeeks[] = $ds_getTransferDates["transferWeek"];
		}
		if(!empty($arrTransferWeeks)){
			$arrTransferWeeks = array_unique($arrTransferWeeks);
		}
	// EOF GET TRANSFER DATES

?>
<div id="menueSidebarToggleArea">
	<img src="layout/icons/iconToggle.png" class="buttonToggleSidebarMenue" width="10" height="10" alt="" title="Seitennavigation ein-/ausblenden" />
	<div id="menueSidebarToggleContent">
	<?php require_once(FILE_MENUE_SIDEBAR); ?>
	<div class="clear"></div>
	</div>
</div>
<div id="contentArea2">
	<a name="top"></a>
	<div id="contentAreaElements">
		<h1><?php if(SHOW_PAGE_TITLE_ICONS) { echo '<img src="' . PATH_ICONS_MENUE_TITLES . 'iconTR.png" alt="" />'; } ?> <?php echo $thisTitle; ?></h1>
		<div align="right">
			<a target="_blank" href="<?php echo PATH_ONLINE_PRODUCTIONS; ?>">Zur Online-Produktionserfassung</a>
		</div>
		<div id="displayStatusCountArea">
			<span class="displayStatusCount"><b><a href="<?php echo PAGE_DISPLAY_EXTERNAL_PRODUCTIONS; ?>" title="Alle Vorg&auml;nge anzeigen">Insgesamt</a>:</b> <?php echo $countStatusTypesTotal; ?></span>
			<?php
				if(!empty($arrCountStatusTypes)) {
					foreach($arrCountStatusTypes as $thisKey => $thisValue) {
						$thisStyle = "";
						if($thisValue["orderStatusTypesID"] == $_REQUEST["displayOrderStatus"]) {
							$thisStyle = "text-decoration:underline";
						}
						echo ' &bull; <span class="displayStatusCount"><b><a href="' . PAGE_DISPLAY_EXTERNAL_PRODUCTIONS . '?displayOrderStatus='.$thisValue["orderStatusTypesID"].'" style="' . $thisStyle . '" title="Als &quot;'.$thisValue["orderStatusTypesName"].'&quot; markierte Vorg&auml;nge anzeigen">'.$thisValue["orderStatusTypesName"].'</a>:</b> '.$thisValue["ordersProductionsTransferProductionStatusCount"].'</span>';
					}
				}
			?>
		</div>
		<div id="searchFilterArea">
			<form name="formFilterSearch" method="post" action="<?php echo $_SERVER["PHP_SELF"]; ?>">
			<table border="0" cellpadding="0" cellspacing="0" class="searchFilterContent">
				<tr>
					<td>
						<label for="searchCustomerNumber">Kundennr:</label>
						<input type="text" name="searchCustomerNumber" id="searchCustomerNumber" class="inputField_70" value="<?php echo $_REQUEST["searchCustomerNumber"]; ?>" />
					</td>
					<td>
						<label for="searchCustomerName">Kundenname:</label>
						<input type="text" name="searchCustomerName" id="searchCustomerName" class="inputField_100" value="<?php echo $_REQUEST["searchCustomerName"]; ?>" />
					</td>
					<!--
					<td>
						<label for="searchPLZ">PLZ:</label>
						<input type="text" name="searchPLZ" id="searchPLZ" class="inputField_40" value="<?php echo $_REQUEST["searchPLZ"]; ?>" />
					</td>
					-->

					<td>
						<label for="searchWord">Suchbegriff:</label>
						<input type="text" name="searchWord" id="searchWord" class="inputField_70" value="<?php echo $_REQUEST["searchWord"]; ?>" />
					</td>

					<td class="legendSeperator"></td>

					<td>
						<label for="searchTransferDate">Transfer-Datum:</label>
						<select name="searchTransferDate" id="searchTransferDate" class="inputField_100">
							<option value=""></option>
							<?php
								if(!empty($arrTransferDates)){
									foreach($arrTransferDates as $thisTransferDate){
										$selected = '';
										if(formatDate($thisTransferDate, 'display') == $_REQUEST["searchTransferDate"]){
											$selected = ' selected="selected" ';
										}
										echo '<option value="' . formatDate($thisTransferDate, 'display') . '" ' . $selected . ' >' . formatDate($thisTransferDate, 'display') . '</option>';
									}
								}
							?>
						</select>
					</td>

					<td>
						<label for="searchTransferWeek">Transfer-Woche:</label>
						<select name="searchTransferWeek" id="searchTransferWeek" class="inputField_100">
							<option value=""></option>
							<?php
								if(!empty($arrTransferWeeks)){
									foreach($arrTransferWeeks as $thisTransferWeek){
										$selected = '';
										if($thisTransferWeek == $_REQUEST["searchTransferWeek"]){
											$selected = ' selected="selected" ';
										}
										$arrTransferWeek = explode("-", $thisTransferWeek);
										echo '<option value="' . $thisTransferWeek . '" ' . $selected . ' >KW ' . $arrTransferWeek[1] . ' |' . $arrTransferWeek[0] . '</option>';
									}
								}
							?>
						</select>
					</td>

					<td>
						<label for="searchTransferPrintType">Druckart:</label>
						<select name="searchTransferPrintType" id="searchTransferPrintType" class="inputField_100">
							<option value=""></option>
							<?php
								if(!empty($arrTransferPrintTypes)) {
									foreach($arrTransferPrintTypes as $thisKey => $thisValue) {
										$selected = '';
										if($thisKey == $_REQUEST["searchTransferPrintType"]){
											$selected = ' selected="selected" ';
										}
										echo '<option value="' . $thisKey . '" ' . $selected . ' > ' . $thisValue["printTypesName"] . '</option>';
									}
								}
							?>
						</select>
					</td>

					<!--
					<td>
						<label for="searchTransferStatus">TR-Status:</label>
						<select name="searchTransferStatus" id="searchTransferStatus" class="inputField_100">
							<option value=""></option>
							<?php
								if(!empty($arrOrderStatusTypeDatasExtern)) {
									foreach($arrOrderStatusTypeDatasExtern as $thisKey => $thisValue) {
										$selected = '';
										if($thisKey == $_REQUEST["searchTransferStatus"]){
											$selected = ' selected="selected" ';
										}
										echo '<option value="' . $thisKey . '" ' . $selected . ' > ' . $thisKey . ' x ' . $thisValue["orderStatusTypesName"] . '</option>';
									}
								}
							?>
						</select>
					</td>
					-->
					<!--
					<td>
						<label for="arrSearchTransferStatus">TR-Status:</label>
						<div class="inputArea2">
							<?php
								if(!empty($arrOrderStatusTypeDatasExtern)) {
									foreach($arrOrderStatusTypeDatasExtern as $thisKey => $thisValue) {
										if(in_array($thisKey, array(1, 4, 7, 8))){
											$checked = '';
											if(!empty($_REQUEST["arrSearchTransferStatus"])){
												if(in_array($thisKey, array_keys($_REQUEST["arrSearchTransferStatus"]))){
													$checked = ' checked="checked" ';
												}
											}
											$thisClass = '';
											if($thisKey == '4'){
												$thisClass = 'row3';
											}
											else if($thisKey == '7'){
												$thisClass = 'row2';
											}
											else if($thisKey == '8'){
												$thisClass = 'row9';
											}
											echo '<span class="' . $thisClass . '" style="display:block;float:left;padding:0 2px 0 0;margin: 0 0 0 0;border-right:1px solid #333;">';
											echo '<input type="checkbox" name="arrSearchTransferStatus[' . $thisKey . ']" value="1" ' . $checked . ' /> ' . $thisValue["orderStatusTypesName"];
											echo '</span>';
										}
									}
								}
							?>
							<div class="clear"></div>
						</div>
					</td>
					-->

					<td>
						<label for="searchTransferProductCategory">Kategorie:</label>
						<select name="searchTransferProductCategory" id="searchTransferProductCategory" class="inputField_100">
							<option value=""></option>
							<option value="KZH" <?php if($_REQUEST["searchTransferProductCategory"] == 'KZH'){ echo ' selected="selected" '; } ?> >Kennzeichenhalter</option>
							<option value="MIN" <?php if($_REQUEST["searchTransferProductCategory"] == 'MIN'){ echo ' selected="selected" '; } ?> >Miniletter</option>
							<option value="AWT" <?php if($_REQUEST["searchTransferProductCategory"] == 'AWT'){ echo ' selected="selected" '; } ?> >Ausweistaschen & Führerscheintaschen</option>
							<option value="OTHERS" <?php if($_REQUEST["searchTransferProductCategory"] == 'OTHERS'){ echo ' selected="selected" '; } ?> >andere</option>
						</select>
					</td>

					<td>
						<label for="searchTransferDateSort">Datum-Sortierung:</label>
						<select name="searchTransferDateSort" id="searchTransferDateSort" class="inputField_100">
							<option value="DESC" <?php if($_REQUEST["searchTransferDateSort"] == 'DESC'){ echo ' selected="selected" '; } ?> >absteigend</option>
							<option value="ASC" <?php if($_REQUEST["searchTransferDateSort"] == 'ASC'){ echo ' selected="selected" '; } ?> >aufsteigend</option>
						</select>
					</td>

					<td>
						<input type="hidden" name="editID" id="editID" value="" />
						<input type="submit" name="submitSearch" class="inputButton0" value="Suchen" />
					</td>
				</tr>
			</table>
			<input type="hidden" name="sortDirection" id="inputSortDirection" value="<?php echo $_REQUEST["sortDirection"]; ?>" />
			<input type="hidden" name="sortRow" id="inputSortRow" value="<?php echo $_REQUEST["sortRow"]; ?>" />
			<input type="hidden" name="sortField" id="inputSortField" value="<?php echo $_REQUEST["sortField"]; ?>" />
			<input type="hidden" name="displayType" id="inputDisplayType" value="<?php echo $_REQUEST["displayType"]; ?>" />
			<input type="hidden" name="submitSearchForm" id="inputSubmitSearchForm" value="1" />
			<input type="hidden" name="displayOrderStatus" value="<?php echo $_REQUEST["displayOrderStatus"]; ?>" />

			<div class="colorsLegend" style="margin:0 0 0 310px;">
				<div class="legendItem">
					<b>TR-Status / Zeilen-F&auml;rbung:</b>
				</div>

				<?php
					if(!empty($arrOrderStatusTypeDatasExtern)) {
						foreach($arrOrderStatusTypeDatasExtern as $thisKey => $thisValue) {
							if(in_array($thisKey, array(1, 4, 7, 8, 6))){
								$checked = '';
								if(!empty($_REQUEST["arrSearchTransferStatus"])){
									if(in_array($thisKey, array_keys($_REQUEST["arrSearchTransferStatus"]))){
										$checked = ' checked="checked" ';
									}
								}
								$thisClass = '';
								$thisID = '';
								$thisColorfield = '';

								if($thisKey == '1'){
									$thisColorfield = 'colorField0';
								}
								else if($thisKey == '4'){
									$thisColorfield = 'colorField3';
								}
								else if($thisKey == '7'){
									$thisColorfield = 'colorField1';
								}
								else if($thisKey == '8'){
									$thisColorfield = 'colorField8';
								}
								else if($thisKey == '6'){
									$thisColorfield = 'colorField0';
									$thisClass = 'row6';
								}

								echo '<div class="legendItem">';
									echo '<input type="checkbox" name="arrSearchTransferStatus[' . $thisKey . ']" value="1" ' . $checked . ' />';
									echo '<span class="legendColorField ' . $thisColorfield . '"></span> ';
									echo '<span class="legendDescription ' . $thisClass . '"> ' . ($thisValue["orderStatusTypesName"]) . '</span>';
									echo '<div class="clear"></div>';
								echo '</div>';

							}
						}
					}
				?>
				<div class="clear"></div>
			</div>
			</form>
		</div>

		<?php displayMessages(); ?>

		<?php

			// BOF GET COUNT ALL ROWS
				$sql_getAllRows = "
						SELECT

							COUNT(`" . TABLE_PRODUCTIONS_TRANSFER . "`.`ordersProductionsTransferID`) AS `countAllRows`

						FROM `" . TABLE_PRODUCTIONS_TRANSFER . "`

						WHERE 1
							" . $sqlWhere . "
					";

				$rs_getAllRows = $dbConnection->db_query($sql_getAllRows);
				list(
					$countAllRows
				) = mysqli_fetch_array($rs_getAllRows);
			// EOF GET COUNT ALL ROWS

			if($_REQUEST["page"] == "" || $_REQUEST["page"] == 0) {
				$_REQUEST["page"] = 1;
			}
			if(MAX_ORDERS_PER_PAGE > 0) {
				$sql .= " LIMIT " . (($_REQUEST["page"] - 1) * MAX_ORDERS_PER_PAGE) . ", " . MAX_ORDERS_PER_PAGE." ";
			}
			// echo $sql;
			$rs = $dbConnection->db_query($sql);
			// echo "<hr>count : ".mysqli_num_rows($rs);exit();
			// OLD BOF GET ALL ROWS
				#$sql_totalRows = "SELECT FOUND_ROWS() AS `countRows`";
				#$rs_totalRows = $dbConnection->db_query($sql_totalRows);
				#list($countRows) = mysqli_fetch_array($rs_totalRows);
			// OLD EOF GET ALL ROWS

			$countRows = $countAllRows;

			$pagesCount = ceil($countRows / MAX_ORDERS_PER_PAGE);
			if($countRows > 0) {
		?>
		<?php
			if($pagesCount > 1) {
				include(FILE_MENUE_PAGES);
			}
		?>
		<!--
		<div class="colorsLegend">
			<div class="legendItem">
				<b>Zeilen-F&auml;rbung: :</b>
			</div>

			<div class="legendItem">
				<span class="legendColorField" id="colorField1"></span>
				<span class="legendDescription">Produktion</span>
				<div class="clear"></div>
			</div>

			<div class="legendItem">
				<span class="legendColorField" id="colorField8"></span>
				<span class="legendDescription">Versandfertig</span>
				<div class="clear"></div>
			</div>

			<div class="legendItem">
				<span class="legendColorField" id="colorField3"></span>
				<span class="legendDescription">Verschickt</span>
				<div class="clear"></div>
			</div>

			<div class="legendSeperator">&nbsp;</div>

			<div class="clear"></div>
		</div>
		-->
		<div class="contentDisplay">
			<?php
				// BOF COUNT PRINTS AND COUNT PRODUCTS
					if($_REQUEST["searchTransferDate"] != '' || $_REQUEST["searchTransferWeek"] != '') {

						if($_REQUEST["searchTransferDate"] != ''){
							$dateFieldFormat = "%Y-%m-%d";
							$searchTransferDate = formatDate($_REQUEST["searchTransferDate"], "store");
							$displayTransferDate = formatDate($_REQUEST["searchTransferDate"], "display");
							$searchTransferDateText = 'Am';
						}
						else if($_REQUEST["searchTransferWeek"] != ''){
							$dateFieldFormat = "%Y-%v";
							$searchTransferDate = $_REQUEST["searchTransferWeek"];
							$displayTransferDate = $_REQUEST["searchTransferWeek"];
							$searchTransferDateText = 'In der KW';
						}

						// BOF COUNT PRINTS
							$sql_getTotalQuantityPerSelectedDate = "
									SELECT
										DATE_FORMAT(`ordersProductionsTransferMailSubmitDateTime`, '" . $dateFieldFormat . "') AS `ordersProductionsTransferMailSubmitDate`,
										SUM(`ordersProductionsTransferOrdersProductQuantity`) AS `ordersProductionsTransferOrdersProductQuantityTotalPerDate`,
										SUM((`ordersProductionsTransferOrdersColorsQuantity` * `ordersProductionsTransferOrdersProductQuantity`)) AS `ordersProductionsTransferOrdersProductPrintsTotalPerDate`,
										SUM(IF(`ordersProductionsTransferOrdersWithClearPaint` = '1', (1 * `ordersProductionsTransferOrdersProductQuantity`), 0)) AS `ordersProductionsTransferOrdersProductClearPaintPrintsTotalPerDate`

										FROM `" . TABLE_PRODUCTIONS_TRANSFER . "`

										WHERE 1
											AND DATE_FORMAT(`ordersProductionsTransferMailSubmitDateTime`, '" . $dateFieldFormat . "')  = '" .$searchTransferDate . "'

										GROUP BY DATE_FORMAT(`ordersProductionsTransferMailSubmitDateTime`, '" . $dateFieldFormat . "')
								";
							$rs_getTotalQuantityPerSelectedDate = $dbConnection->db_query($sql_getTotalQuantityPerSelectedDate);

							list($ordersProductionsTransferDate, $ordersProductionsTransferTotalQuantity, $ordersProductionsTransferTotalPrints, $ordersProductionsTransferTotalClearPaintPrints) = mysqli_fetch_array($rs_getTotalQuantityPerSelectedDate);
							if($ordersProductionsTransferTotalQuantity == ''){
								$ordersProductionsTransferTotalQuantity = 0;
							}
							if($$ordersProductionsTransferTotalPrints == ''){
								$$ordersProductionsTransferTotalPrints = 0;
							}
							if($$ordersProductionsTransferTotalClearPaintPrints == ''){
								$$ordersProductionsTransferTotalClearPaintPrints = 0;
							}

							echo '<p class="infoArea">';
							echo '<span style="padding-right:30px;">' . $searchTransferDateText . ' ' . $displayTransferDate . ' gesendete Gesamt-St&uuml;ckzahl: ' . number_format($ordersProductionsTransferTotalQuantity, 0, ',', '.') . '</span>';
							echo '<span style="padding-right:30px;"> &bull; Anzahl Farb-Drucke: ' . number_format($ordersProductionsTransferTotalPrints, 0, ',', '.') . '</span>';
							echo '<span style="padding-right:30px;"> &bull; Anzahl Klarlack-Drucke: ' . number_format($ordersProductionsTransferTotalClearPaintPrints, 0, ',', '.') . '</span>';
							echo '<span style="padding-right:30px;"> &bull; Anzahl Gesamtdrucke: ' . number_format(($ordersProductionsTransferTotalPrints + $ordersProductionsTransferTotalClearPaintPrints), 0, ',', '.') . '</span>';
							echo '</p>';

							echo '<div class="transferStatisticsArea">';

							$sql_countPrintsPerSelectedDate = "
									SELECT
										*,
										`tempTable`.`ordersProductionsTransferMailSubmitDate`,
										SUM(`tempTable`.`ordersProductionsTransferOrdersProductQuantity`) AS `productsQuantity`,
										SUM((`tempTable`.`ordersProductionsTransferOrdersProductQuantity` * `tempTable`.`ordersProductionsTransferOrdersColorsQuantity`)) AS `productsPrints`,
										SUM(IF(`ordersProductionsTransferOrdersWithClearPaint` = '' || `ordersProductionsTransferOrdersWithClearPaint` IS NULL, 0, `tempTable`.`ordersProductionsTransferOrdersProductQuantity`)) AS `productsClearPaints`,
										COUNT(`tempTable`.`ordersProductionsTransferID`) AS `countItems`,
										`tempTable`.`parentCategoriesName`,
										`tempTable`.`printTypesName`,
										`tempTable`.`countColors`,
										`tempTable`.`parentCategoriesGroupID`

									FROM (

										SELECT

											`" . TABLE_PRODUCTIONS_TRANSFER . "`.`ordersProductionsTransferID`,
											`" . TABLE_PRODUCTIONS_TRANSFER . "`.`ordersProductionsTransferOrdersID`,

											`" . TABLE_PRODUCTIONS_TRANSFER . "`.`ordersProductionsTransferOrdersColorsQuantity`,
											`" . TABLE_PRODUCTIONS_TRANSFER . "`.`ordersProductionsTransferOrdersWithClearPaint`,
											`" . TABLE_PRODUCTIONS_TRANSFER . "`.`ordersProductionsTransferOrdersProductQuantity`,
											`" . TABLE_PRODUCTIONS_TRANSFER . "`.`ordersProductionsTransferOrdersProductOriginalQuantity`,
											`" . TABLE_PRODUCTIONS_TRANSFER . "`.`ordersProductionsTransferOrdersProductTatsaechlicheProduktion`,
											`" . TABLE_PRODUCTIONS_TRANSFER . "`.`ordersProductionsTransferOrdersProductNumber`,
											`" . TABLE_PRODUCTIONS_TRANSFER . "`.`ordersProductionsTransferOrdersProductName`,
											`" . TABLE_PRODUCTIONS_TRANSFER . "`.`ordersProductionsTransferOrdersProductCategory`,
											`" . TABLE_PRODUCTIONS_TRANSFER . "`.`ordersProductionsTransferMailSubmitDateTime`,
											DATE_FORMAT(`" . TABLE_PRODUCTIONS_TRANSFER . "`.`ordersProductionsTransferMailSubmitDateTime`, '" . $dateFieldFormat . "') AS `ordersProductionsTransferMailSubmitDate`,
											`" . TABLE_PRODUCTIONS_TRANSFER . "`.`ordersProductionsTransferOrdersPrintType`,

											`" . TABLE_ORDERS . "`.`ordersID`,
											`" . TABLE_ORDERS . "`.`ordersArtikelID`,
											`" . TABLE_ORDERS . "`.`ordersArtikelNummer`,
											`" . TABLE_ORDERS . "`.`ordersArtikelBezeichnung`,
											`" . TABLE_ORDERS . "`.`ordersArtikelKategorieID`,

											`productCategories`.`orderCategoriesID` AS `productCategoriesID`,
											`productCategories`.`orderCategoriesParentID` AS `productCategoriesParentID`,
											`productCategories`.`orderCategoriesLevelID` AS `productCategoriesLevelID`,
											`productCategories`.`orderCategoriesRelationID` AS `productCategoriesRelationID`,
											`productCategories`.`orderCategoriesShortName` AS `productCategoriesShortName`,
											`productCategories`.`orderCategoriesName` AS `productCategoriesName`,

											`relationCategories`.`orderCategoriesID` AS `relationCategoriesID`,
											`relationCategories`.`orderCategoriesParentID` AS `relationCategoriesParentID`,
											`relationCategories`.`orderCategoriesLevelID` AS `relationCategoriesLevelID`,
											`relationCategories`.`orderCategoriesRelationID` AS `relationCategoriesRelationID`,
											`relationCategories`.`orderCategoriesShortName` AS `relationCategoriesShortName`,
											`relationCategories`.`orderCategoriesName` AS `relationCategoriesName`,

											`parentCategories`.`orderCategoriesID` AS `parentCategoriesID`,
											`parentCategories`.`orderCategoriesParentID` AS `parentCategoriesParentID`,
											`parentCategories`.`orderCategoriesLevelID` AS `parentCategoriesLevelID`,
											`parentCategories`.`orderCategoriesRelationID` AS `parentCategoriesRelationID`,
											`parentCategories`.`orderCategoriesShortName` AS `parentCategoriesShortName`,
											`parentCategories`.`orderCategoriesName` AS `parentCategoriesName`,

											IF(`productCategories`.`orderCategoriesLevelID` IS NOT NULL, `parentCategories`.`orderCategoriesLevelID`,
												SUBSTRING(`" . TABLE_PRODUCTIONS_TRANSFER . "`.`ordersProductionsTransferOrdersProductCategory`, 1, 3)
											) AS `parentCategoriesGroupID`,

											`" . TABLE_PRINT_TYPES . "`.`printTypesShortName`,
											`" . TABLE_PRINT_TYPES . "`.`printTypesName`,

											IF(`" . TABLE_PRODUCTIONS_TRANSFER . "`.`ordersProductionsTransferOrdersColorsQuantity` = '1', 'uni-color', 'multi-color') AS `countColors`

										FROM `" . TABLE_PRODUCTIONS_TRANSFER . "`

										LEFT JOIN `" . TABLE_ORDERS . "`
										ON(`" . TABLE_PRODUCTIONS_TRANSFER . "`.`ordersProductionsTransferOrdersID` = `" . TABLE_ORDERS . "`.`ordersID`)

										LEFT JOIN `" . TABLE_ORDER_CATEGORIES . "` AS `productCategories`
										ON(`" . TABLE_PRODUCTIONS_TRANSFER . "`.`ordersProductionsTransferOrdersProductCategory` = `productCategories`.`orderCategoriesID`)

										LEFT JOIN `" . TABLE_ORDER_CATEGORIES . "` AS `relationCategories`
										ON(`productCategories`.`orderCategoriesRelationID` = `relationCategories`.`orderCategoriesID`)

										LEFT JOIN `" . TABLE_ORDER_CATEGORIES . "` AS `parentCategories`
										ON(SUBSTRING(`" . TABLE_PRODUCTIONS_TRANSFER . "`.`ordersProductionsTransferOrdersProductCategory`, 1, 3) = `parentCategories`.`orderCategoriesID`)

										LEFT JOIN `" . TABLE_PRINT_TYPES . "`
										ON(`" . TABLE_PRODUCTIONS_TRANSFER . "`.`ordersProductionsTransferOrdersPrintType` = `" . TABLE_PRINT_TYPES . "`.`printTypesID`)

										WHERE 1
											AND DATE_FORMAT(`" . TABLE_PRODUCTIONS_TRANSFER . "`.`ordersProductionsTransferMailSubmitDateTime`, '" . $dateFieldFormat . "')  = '" .$searchTransferDate . "'

									) AS `tempTable`

									GROUP BY
										CONCAT(
											`tempTable`.`parentCategoriesGroupID`,
											`tempTable`.`ordersProductionsTransferOrdersPrintType`,
											`tempTable`.`countColors`
										)

									ORDER BY
										`tempTable`.`parentCategoriesGroupID`,
										`tempTable`.`ordersProductionsTransferOrdersPrintType`
								";
							$rs_countPrintsPerSelectedDate = $dbConnection->db_query($sql_countPrintsPerSelectedDate);

							echo '<div class="transferStatisticsContainer">';
							echo '<h2>Summe Drucke [' . $displayTransferDate . ']:</h2>';
							echo '<div class="transferStatisticsTableArea">';
							echo '<table cellpadding="0" cellspacing="0" border="1" style="width:100%">';
							echo '<tr>';
							echo '<th>#</th>';
							echo '<th>Artikel</th>';
							echo '<th>Farbe</th>';
							echo '<th>Druckart</th>';
							echo '<th>Menge</th>';
							echo '<th>Drucke</th>';
							echo '<th>Klarlack</th>';
							echo '</tr>';

							$countRow = 0;
							$sumPrintsPerSelectedDate = 0;
							$sumClearPrintsPerSelectedDate = 0;
							$sumProductsPerSelectedDate = 0;

							while($ds_countPrintsPerSelectedDate = mysqli_fetch_assoc($rs_countPrintsPerSelectedDate)){

								if($countRow%2 == 0){ $rowClass = 'row0'; }
								else { $rowClass = 'row1'; }

								echo '<tr class="' . $rowClass . '">';
								echo '<td style="text-align:right;">';
								echo '<b>' . ($countRow + 1) . '.</b>';
								echo '</td>';

								if($ds_countPrintsPerSelectedDate["countColors"] == 'uni-color'){
									$thisCountColors = 'einfarbig';
								}
								else if($ds_countPrintsPerSelectedDate["countColors"] == 'multi-color'){
									$thisCountColors = 'mehrfarbig';
								}

								echo '<td>';
								echo htmlentities(stripslashes($ds_countPrintsPerSelectedDate["parentCategoriesName"]));
								echo '</td>';

								echo '<td>';
								echo htmlentities($thisCountColors);
								echo '</td>';

								echo '<td>';
								echo htmlentities(($ds_countPrintsPerSelectedDate["printTypesName"]));
								echo '</td>';

								echo '<td style="text-align:right;">';
								echo number_format($ds_countPrintsPerSelectedDate["productsQuantity"], 0, ',', '.');
								$sumProductsPerSelectedDate += $ds_countPrintsPerSelectedDate["productsQuantity"];
								echo '</td>';

								echo '<td style="text-align:right;">';
								echo number_format($ds_countPrintsPerSelectedDate["productsPrints"], 0, ',', '.');
								$sumPrintsPerSelectedDate += $ds_countPrintsPerSelectedDate["productsPrints"];
								echo '</td>';

								echo '<td style="text-align:right;">';
								echo number_format($ds_countPrintsPerSelectedDate["productsClearPaints"], 0, ',', '.');
								$sumClearPrintsPerSelectedDate += $ds_countPrintsPerSelectedDate["productsClearPaints"];
								echo '</td>';

								echo '</tr>';

								$countRow++;
							}

							echo '<tr class="row2" style="font-weight:bold;">';

							echo '<td colspan="4">';
							echo 'Insgesamt:';
							echo '</td>';

							echo '<td style="text-align:right;">';
							echo number_format($sumProductsPerSelectedDate, 0, ',', '.');
							echo '</td>';

							echo '<td style="text-align:right;">';
							echo number_format($sumPrintsPerSelectedDate, 0, ',', '.');
							echo '</td>';

							echo '<td style="text-align:right;">';
							echo number_format($sumClearPrintsPerSelectedDate, 0, ',', '.');
							echo '</td>';

							echo '</tr>';

							echo '</table>';
							echo '</div>';
							echo '</div>';
						// EOF COUNT PRINTS

						// BOF COUNT PRODUCTS
							$sql_countProductsPerSelectedDate = "
									SELECT
											 `" . TABLE_PRODUCTIONS_TRANSFER . "`.`ordersProductionsTransferOrdersProductID`,
											 SUM(`" . TABLE_PRODUCTIONS_TRANSFER . "`.`ordersProductionsTransferOrdersProductQuantity`) AS `countProductsPerDate`,

											`" . TABLE_STOCK_PRODUCT_CATEGORIES . "`.`stockProductCategoriesProductNumber`,
											`" . TABLE_STOCK_PRODUCT_CATEGORIES . "`.`stockProductCategoriesName`

										FROM `" . TABLE_PRODUCTIONS_TRANSFER . "`

										LEFT JOIN `" . TABLE_STOCK_PRODUCT_CATEGORIES . "`
										ON(`" . TABLE_PRODUCTIONS_TRANSFER . "`.`ordersProductionsTransferOrdersProductID` = `" . TABLE_STOCK_PRODUCT_CATEGORIES . "`.`stockProductCategoriesID`)

										WHERE 1
											AND DATE_FORMAT(`" . TABLE_PRODUCTIONS_TRANSFER . "`.`ordersProductionsTransferMailSubmitDateTime`, '" . $dateFieldFormat . "')  = '" .$searchTransferDate . "'

										GROUP BY `" . TABLE_PRODUCTIONS_TRANSFER . "`.`ordersProductionsTransferOrdersProductID`

										ORDER BY
											REPLACE(REPLACE(`" . TABLE_STOCK_PRODUCT_CATEGORIES . "`.`stockProductCategoriesName`, 'Kennzeichenhalter ', ''), 'Leisten ', '') ASC
								";

							$rs_countProductsPerSelectedDate = $dbConnection->db_query($sql_countProductsPerSelectedDate);

							echo '<div class="transferStatisticsContainer">';
							echo '<h2>Summe Artikel [' . $displayTransferDate . ']:</h2>';
							echo '<div class="transferStatisticsTableArea">';
							echo '<table cellpadding="0" cellspacing="0" border="1" style="width:100%">';
							echo '<tr>';
							echo '<th>#</th>';
							echo '<th>Artikel</th>';
							#echo '<th>Artikel-Nr.</th>';
							echo '<th>Menge</th>';
							echo '</tr>';

							$countRow = 0;
							$sumProductsPerSelectedDate = 0;

							while($ds_countProductsPerSelectedDate = mysqli_fetch_assoc($rs_countProductsPerSelectedDate)){
								if($countRow%2 == 0){ $rowClass = 'row0'; }
								else { $rowClass = 'row1'; }

								echo '<tr class="' . $rowClass . '">';
								echo '<td style="text-align:right;">';
								echo '<b>' . ($countRow + 1) . '.</b>';
								echo '</td>';

								echo '<td>';
								echo htmlentities(stripslashes($ds_countProductsPerSelectedDate["stockProductCategoriesName"]));
								echo '</td>';

								#echo '<td>';
								#echo htmlentities(($ds_countProductsPerSelectedDate["stockProductCategoriesProductNumber"]));
								#echo '</td>';

								echo '<td style="text-align:right;">';
								echo number_format($ds_countProductsPerSelectedDate["countProductsPerDate"], 0, ',', '.');
								$sumProductsPerSelectedDate += $ds_countProductsPerSelectedDate["countProductsPerDate"];
								echo '</td>';

								echo '</tr>';

								$countRow++;
							}

							echo '<tr class="row2" style="font-weight:bold;">';

							echo '<td colspan="2">';
							echo 'Insgesamt:';
							echo '</td>';

							echo '<td style="text-align:right;">';
							echo number_format($sumProductsPerSelectedDate, 0, ',', '.');
							echo '</td>';

							echo '</tr>';

							echo '</table>';
							echo '</div>';
							echo '</div>';
						// EOF COUNT PRODUCTS
						echo '<div class="clear"></div>';
						echo '</div>';
					}
				// EOF COUNT PRINTS AND COUNT PRODUCTS
			?>

			<table border="0" cellpadding="0" cellspacing="0" width="100%" class="displayOrders">
				<thead>
					<tr>
						<th style="width:45px;text-align:right;">#</th>
						<th>Transfer</th>
						<th>Mandant</th>
						<th>Transport</th>
						<th>Verpackung</th>
						<th>KNR</th>
						<th>Klischee-Nr</th>
						<th style="min-width:160px;">Kunde</th>

						<th>Empf&auml;nger</th>
						<th>Absender</th>
						<th>Art-Nr</th>
						<th>Art.-Bezeichnung</th>
						<th>Menge</th>
						<th>tats. produziert</th>
						
						<th>Farbanz.</th>
						<th>Druckfarbe(n)</th>
						<th>Druckart</th>

						<th>Status</th>
						<th>Bestellart</th>
						<th>Drucker</th>
						<th>Produktion</th>
						<th>Verladung</th>
						<th>Bemerkung</th>
						<th>DPD-Nr</th>

						<th>Info</th>
					</tr>
				</thead>

				<tbody>
				<?php
					$count = 0;
					echo mysqli_error();
					while($ds = mysqli_fetch_assoc($rs)) {
						// exit();
						foreach($ds as $thisKey => $thisValue){
							$ds[$thisKey] = ($thisValue);
						}

						if($count%2 == 0){ $rowClass = 'row0'; }
						else { $rowClass = 'row1'; }

						if($ds["ordersProductionsTransferProductionStatus"] == "1") {
							// $rowClass = 'row5';
						}
						else if($ds["ordersProductionsTransferProductionStatus"] == "3") {
							#$rowClass = 'row2';
							$rowClass = 'row9';
						}
						else if($ds["ordersProductionsTransferProductionStatus"] == "4") {
							$rowClass = 'row3';
						}
						else if($ds["ordersProductionsTransferProductionStatus"] == "6") {
							$rowClass = 'row6';
						}
						else if($ds["ordersProductionsTransferProductionStatus"] == "7") {
							#$rowClass = 'row9';
							$rowClass = 'row2';
						}
						else if($ds["ordersProductionsTransferProductionStatus"] == "8") {
							$rowClass = 'row9';
						}

						echo '<tr class="'.$rowClass.'">';
						echo '<td style="text-align:right;"><b>'.($count + 1).'.</b></td>';

						echo '<td style="white-space:nowrap;">';
						echo str_replace(" ", "<br />", formatDate($ds["ordersProductionsTransferMailSubmitDateTime"], "display"));

						echo '<table border="0" cellpadding="0" cellspacing="0" width="100%">';

						echo '<tr>';
						$thisTransferSuccessImage = 'iconNotOk.png';
						$thisTransferSuccessTitle = 'Daten-Transfer fehlerhaft oder noch nicht erledigt ';
						if($ds["ordersProductionsTransferSuccess_MYSQL"] == '1'){
							$thisTransferSuccessImage = 'iconOk.png';
							$thisTransferSuccessTitle = 'Daten-Transfer erfolgreich';
						}
						echo '<td style="font-size:11px;border-width:0;padding:0;">DATEN:</td>';
						echo '<td style="font-size:11px;border-width:0;padding:0;"><img src="layout/icons/' . $thisTransferSuccessImage . '" width="12" height="12" alt="' . $thisTransferSuccessTitle . '" title="' . $thisTransferSuccessTitle . '" /></td>';
						echo '</tr>';

						echo '<tr>';
						$thisTransferSuccessImage = 'iconNotOk.png';
						$thisTransferSuccessTitle = 'Datei-Transfer fehlerhaft oder noch nicht erledigt ';
						if($ds["ordersProductionsTransferSuccess_FTP"] == '1'){
							$thisTransferSuccessImage = 'iconOk.png';
							$thisTransferSuccessTitle = 'Datei-Transfer erfolgreich';
						}
						echo '<td style="font-size:11px;border-width:0;padding:0;">DATEI:</td>';
						echo '<td style="font-size:11px;border-width:0;padding:0;"><img src="layout/icons/' . $thisTransferSuccessImage . '" width="12" height="12" alt="' . $thisTransferSuccessTitle . '" title="' . $thisTransferSuccessTitle . '" /></td>';
						echo '</tr>';

						echo '</table>';

						$thisIconImage = '';
						$thisIconClass = '';
						if($ds["ordersProductionsTransferOrderType"] == 3){
							$thisIconImage = '<img src="layout/icons/iconAttention.png" width="14" height="14" alt="" /> ';
							$thisIconClass = 'class="iconAttention"';
						}
						else if($ds["ordersProductionsTransferOrderType"] == 1){
							$thisIconImage = '<img src="layout/icons/iconNewOrder.png" width="14" height="14" alt="" /> ';
							$thisIconClass = '';
						}
						echo '<span style="font-size:11px;white-space:nowrap;" ' . $thisIconClass . ' >';
						echo $thisIconImage;
						echo htmlentities($arrOrderTypes[$ds["ordersProductionsTransferOrderType"]]["orderTypesName"]);
						echo '</span>';

						echo '</td>';

						echo '<td style="font-size:16px;white-space:nowrap;text-align:center;">';
						if(strtolower($ds["ordersProductionsTransferMandatory"]) == 'b3'){
							echo '<b>' . $ds["ordersProductionsTransferMandatory"] . '</b>';
						}
						echo '</td>';

						echo '<td style="white-space:nowrap;font-size:10px;">';
						if($ds["ordersProductionsTransferDeliveryRedDot"] == '1'){
							echo '<img width="14" height="14" title="Hinten LKW (Vorkasse / Nachnahme)" alt="Hinten LKW (Vorkasse / Nachnahme)" src="layout/icons/redDot.png" />';
						}
						echo '</td>';

						echo '<td style="white-space:nowrap;font-size:10px;">';
						if($ds["ordersProductionsTransferUseNoNeutralPacking"] == '1'){
							echo '<img width="14" height="14" title="Verpackung nicht neutral (BURHAN)" alt="Verpackung nicht neutral (BURHAN)" src="layout/icons/greenDot.png" />';
						}
						echo '</td>';

						echo '<td>';
						echo '<a href="' . PAGE_EDIT_CUSTOMER . '?editCustomerNumber='.$ds["ordersProductionsTransferOrdersCustomerNumber"].'" title="Kunde &quot;'.htmlentities($ds["ordersProductionsTransferOrdersCustomerName"]). '&quot; (KNR: '.$ds["ordersProductionsTransferOrdersCustomerNumber"].') anzeigen">';
						echo '<b>' . $ds["ordersProductionsTransferOrdersCustomerNumber"] . '</b>';
						echo '</a>';
						echo '</td>';

						echo '<td>';
						echo '<b><a href="' . PAGE_DISPLAY_ABS_PLATE_DATA . '?searchPrintingPlateNumber=' . $ds["ordersProductionsTransferPrintPlateNumber"] . '" target="_blank">' . htmlentities($ds["ordersProductionsTransferPrintPlateNumber"]) . '</a></b>';
						echo '</td>';

						echo '<td style="font-weight:bold;">';
						echo htmlentities($ds["ordersProductionsTransferOrdersCustomerName"]);
						echo '</td>';

						echo '<td style="white-space:nowrap;">';
						echo '<b>' . htmlentities($ds["ordersProductionsTransferOrderRecipientCustomerNumber"]) . '</b>';
						echo '<br />';
						echo '<span style="font-size:11px;">';
						echo nl2br(htmlentities($ds["ordersProductionsTransferOrderRecipientAddress"]));
						echo '</span>';
						echo '</td>';

						echo '<td style="white-space:nowrap;">';
						echo '<b>' . $ds["ordersProductionsTransferOrderSenderCustomerNumber"] . '</b>';
						echo '<br />';
						echo '<span style="font-size:11px;">';
						echo nl2br(htmlentities($ds["ordersProductionsTransferOrderSenderAddress"]));
						echo '</span>';
						echo '</td>';

						echo '<td>';
						echo $ds["ordersProductionsTransferOrdersProductNumber"];
						echo '</td>';

						echo '<td>';
						echo htmlentities(stripslashes($ds["ordersProductionsTransferOrdersProductName"]));
						echo '</td>';

						echo '<td>';
						echo '<b>' . $ds["ordersProductionsTransferOrdersProductQuantity"] . '</b> <span style="white-space:nowrap;">( von ' . $ds["ordersProductionsTransferOrdersProductOriginalQuantity"] . ' )</span>';
						echo '</td>';

						echo '<td>';
						echo $ds["ordersProductionsTransferOrdersProductTatsaechlicheProduktion"];
						echo '</td>';

						echo '<td style="white-space:nowrap">';
						echo $ds["ordersProductionsTransferOrdersColorsQuantity"];
						if($ds["ordersProductionsTransferOrdersWithClearPaint"] != '') {
							echo ' +1 [Klarlack]';
						}
						echo '</td>';

						echo '<td>';
						echo '<span style="white-space:nowrap">';
						$thisTransferOrdersColorsNames = $ds["ordersProductionsTransferOrdersColorsNames"];
						$thisTransferOrdersColorsNames = htmlentities($thisTransferOrdersColorsNames);
						$thisTransferOrdersColorsNames = preg_replace("/[\/] (C|M|Y|K) ([0-9])/ismU", " | $1 $2", $thisTransferOrdersColorsNames);
						$thisTransferOrdersColorsNames = preg_replace("/\//", "<br />&bull; ", $thisTransferOrdersColorsNames);
						echo '&bull; ' . $thisTransferOrdersColorsNames;
						echo '</span>';

						if($ds["ordersProductionsTransferOrdersKomission"] != '') {
							echo '<div class="remarksArea">';
							echo '<b>Kommission:</b> ' . htmlentities($ds["ordersProductionsTransferOrdersKomission"]);
							echo '</div>';
						}
						if($ds["ordersProductionsTransferOrdersPrintText"] != '') {
							echo '<div class="remarksArea">';
							echo '<b>Aufdruck:</b> ' . htmlentities(preg_replace("/(\/)/", " / ", $ds["ordersProductionsTransferOrdersPrintText"]));
							echo '</div>';
						}

						if($ds["ordersProductionsTransferOrdersWithClearPaint"] != '') {
							echo '<div class="remarksArea">';
							echo '<b>Aufdruck mit Klarlack</b>';
							echo '</div>';
						}

						echo '</td>';

						echo '<td>';
						echo '' . htmlentities($arrPrintTypeDatas[$ds["ordersProductionsTransferOrdersPrintType"]]["printTypesName"]) . '';
						echo '</td>';

						echo '<td style="white-space:nowrap;">';
						echo '' . $arrOrderStatusTypeDatasExtern[$ds["ordersProductionsTransferProductionStatus"]]["orderStatusTypesName"];
						echo '<br /><br />';
						echo '<span style="font-size:11px">';
						echo 'Status gesetzt am:<br />' . str_replace(" ", "<br />", formatDate($ds["ordersProductionsTransferProductionModified"], "display"));
						#echo '' . formatDate($ds["ordersProductionsTransferProductionModified"], "display");
						echo '</span>';
						echo '</td>';

						echo '<td style="white-space:nowrap;">';
						echo htmlentities($arrOrderTypes[$ds["ordersProductionsTransferOrderType"]]["orderTypesName"]);
						echo '</td>';

						echo '<td style="white-space:nowrap;">';
						echo '' . htmlentities(utf8_decode($ds["ordersProductionsTransferProductionPrinter"]));
						echo '</td>';

						echo '<td style="white-space:nowrap;">';
						echo str_replace(" ", "<br />", formatDate($ds["ordersProductionsTransferDeliveryPrintDate"], "display"));
						#echo '' . formatDate($ds["ordersProductionsTransferDeliveryPrintDate"], 'display');
						echo '</td>';

						echo '<td style="white-space:nowrap;">';
						echo str_replace(" ", "<br />", formatDate($ds["ordersProductionsTransferDeliveryContainerDate"], "display"));
						if($ds["ordersProductionsTransferDeliveryContainerDate"] > 0){
							$thisOrdersProductionsTransferTransportType = $ds["ordersProductionsTransferTransportType"];
							if($thisOrdersProductionsTransferTransportType == ''){
								$thisOrdersProductionsTransferTransportType = "CONTAINER";
							}
							$thisOrdersProductionsTransferTransportTypeDays = $arrTransferTransportTypes[$thisOrdersProductionsTransferTransportType];
							$thisOrdersProductionsTransferDeliveryContainerDateTimestamp = strtotime($ds["ordersProductionsTransferDeliveryContainerDate"]);
							$thisOrdersProductionsTransferArrivalContainerDate = date("Y-m-d", mktime(0, 0, 0, date("m", $thisOrdersProductionsTransferDeliveryContainerDateTimestamp), (date("d", $thisOrdersProductionsTransferDeliveryContainerDateTimestamp) + $thisOrdersProductionsTransferTransportTypeDays), date("Y", $thisOrdersProductionsTransferDeliveryContainerDateTimestamp)));

							echo '<br />Transp. per ' . $thisOrdersProductionsTransferTransportType;
							echo '<br />Ankunft ca. ' . formatDate($thisOrdersProductionsTransferArrivalContainerDate, "display");
						}

						#echo '' . formatDate($ds["ordersProductionsTransferDeliveryContainerDate"], 'display');
						echo '</td>';

						echo '<td style="white-space:nowrap;">';
						echo htmlentities($ds["ordersProductionsTransferNotice"]);
						echo '</td>';

						echo '<td style="white-space:nowrap;">';
						$thisProductionsTransferProductionTrackingNumbers = trim($ds["ordersProductionsTransferProductionTrackingNumbers"]);
						if($thisProductionsTransferProductionTrackingNumbers != ''){
							$arrProductionsTransferProductionTrackingNumbers = explode(";", $thisProductionsTransferProductionTrackingNumbers);
							if(!empty($arrProductionsTransferProductionTrackingNumbers)){
								echo '<ol>';
								foreach($arrProductionsTransferProductionTrackingNumbers as $thisProductionsTransferProductionTrackingNumber){
									echo '<li>';
									echo '<a href="' . PAGE_SEARCH_TRACKING . '?searchTrackingNumber=' . $thisProductionsTransferProductionTrackingNumber . '">';
									echo $thisProductionsTransferProductionTrackingNumber;
									echo '</a>';
									echo '</li>';
								}
								echo '</ol>';
							}
						}
						echo '</td>';

						echo '<td style="white-space:nowrap;width:50px;">';

						echo '<span class="toolItem">';
						echo '<a href="?downloadFile=' . ($ds["ordersProductionsTransferProductionFileName"]) . '&amp;thisDocumentType=PR">' . '<img src="layout/icons/iconZIP.gif" width="16" height="16" title="Produktionsdatei herunterladen" alt="Download" /></a>';
						echo '</span>';

						echo '<span class="toolItem">';
						echo '<img src="layout/icons/mailAttachement.png" class="buttonSendDocument" rel="' . urlencode(($ds["ordersProductionsTransferProductionFileName"])) . '#' . 'PA' . '" width="16" height="16" title="Dokument direkt per Mail versenden" alt="Dokument versenden" />';
						echo '</span>';

						echo '<span class="toolItem">';
						echo '<a href="' . PAGE_EDIT_EXTERNAL_PRODUCTIONS . '?editTransferId=' . ($ds["ordersProductionsTransferID"]) . '">' . '<img src="layout/icons/iconEdit.gif" width="16" height="16" title="Daten bearbeiten" alt="Daten bearbeiten" /></a>';
						echo '</span>';

						#echo '<span class="toolItem">';
						#echo '<img src="layout/icons/iconDpd.gif" class="buttonTrackingNumbers" rel="" width="16" height="16" title="Tracking-Nummern anzeigen" alt="Tracking-Nr" />';
						#echo '</span>';

						#echo '<span class="toolItem">';
						#echo '<a href="' . PAGE_DISPLAY_ORDERS_OVERVIEW . '?searchCustomerNumber=' . $ds["ordersProductionsTransferOrdersCustomerNumber"] . '">' . '<img src="layout/menueIcons/menueSidebar/ordersoverview.png" width="16" height="16" title="In Produktionsansicht anzeigen" alt="Zur Produktionsansicht" /></a>';
						#echo '</span>';

						echo '<span class="toolItem">';
						echo '<a href="' . PAGE_DISPLAY_ORDERS_OVERVIEW . '?searchOrdersID=' . $ds["ordersProductionsTransferOrdersID"] . '">' . '<img src="layout/menueIcons/menueSidebar/ordersoverview.png" width="16" height="16" title="In Produktionsansicht anzeigen" alt="Zur Produktionsansicht" /></a>';
						echo '</span>';

						echo '</td>';

						echo '</tr>';

						$count++;
					}
				?>
				</tbody>
			</table>
		</div>
		<?php
			if($pagesCount > 1) {
				include(FILE_MENUE_PAGES);
			}
		?>
		<div style="text-align:right;" class="menueToTop">
			<a href="#top">nach oben</a>
		</div>
		<?php
			}
			else {
				echo '<p class="warningArea">Es wurden keine Daten gefunden!</p>';
			}
		?>
		<div class="clear"></div>
	</div>
	<div class="clear"></div>
</div>
<script language="javascript" type="text/javascript">
	<!--
	/* <![CDATA[ */
	$(document).ready(function() {
		$('.buttonToggleSidebarMenue').click(function() {
			$('#menueSidebarToggleContent').toggle();
		});

		$('.buttonSendDocument').click(function() {
			var mailDocumentDatas = $(this).attr('rel');
			var mailAdress = '';
			sendAttachedDocument($(this), mailDocumentDatas, '<?php echo $thisDocumentValue[$thisKey]["orderDocumentsCustomerNumber"]; ?>', '<?php echo $_REQUEST["editID"]; ?>', '', '<?php echo $_SERVER["PHP_SELF"]; ?>', mailAdress);
		});

		colorRowMouseOver('.displayOrders tbody tr');

		blink('.iconAttention', 1000, 0.1, 400);
	});
	/* ]]> */
	// -->
</script>

<?php require_once('inc/footerHTML.inc.php'); ?>