<?php
	$arrContentDatas = array();
	$arrDocumentDatas = array();
	$arrWarningBankAccountText = '
						<p class="important">
							<b>ACHTUNG: </b>
							Bitte &uuml;berweisen Sie nur an: 
							&bull; {###BANK_ACCOUNT_NAME###} 
							&bull; Konto-Nr.: {###BANK_ACCOUNT_NUMBER###} 
							&bull; BLZ: {###BANK_ACCOUNT_CODE###}
						</p>
	';

	// BOF AN
	$arrContentDatas['AN'] = array(
		'SALUTATION_TEXT' => '<p>Sehr geehrte Kundin, sehr geehrter Kunde,</p>',
		'INTRO_TEXT' => '<p>hiermit erstellen wir Ihnen folgendes Angebot:</p>',
		'EDITOR_TEXT' => '',
		// 'PAYMENT_CONDITIONS' => '<p><b>Zahlung: {###PAYMENT_CONDITION###}, per {###PAYMENT_TYPE###}{###BANK_DATAS###}</b>.</p>',
		'PAYMENT_CONDITIONS' => '',
		'OUTRO_TEXT' => '<p>Die gelieferten Waren bleiben bis zur vollst&auml;ndigen Bezahlung unser Eigentum. Bei Drucksachen behalten wir uns eine Minder- oder Mehrlieferung bis zu 10% aus technischen Gr&uuml;nden vor.</p><p>Wir danken Ihnen f&uuml;r Ihre Anfrage.</p>',
		'REGARDS_TEXT' => '<p>Mit freundlichen Gr&uuml;&szlig;en,<br><b><i>Ihr Burhan CTR Team</i></b></p>',
		// 'BANK_ACCOUNT_WARNING' => '' . $arrWarningBankAccountText . '',
		'BANK_ACCOUNT_WARNING' => '',
		'FOOTER_NOTICE' => '',
	);

	$arrDocumentDatas['AN'] = '		
		<!-- BOF_DOCUMENT_NUMBER --><tr><td align="left"><b>Angebotssnummer:&nbsp;</b></td><td align="left">{###DOCUMENT_NUMBER###}</td></tr><!-- EOF_DOCUMENT_NUMBER -->
		<!-- BOF_CUSTOMER_NUMBER --><tr><td align="left"><b>Kundennummer:&nbsp;</b></td><td align="left">{###CUSTOMER_NUMBER###}</td></tr><!-- EOF_CUSTOMER_NUMBER -->
		<!-- BOF_CUSTOMER_VAT_ID --><tr><td align="left"><b>Kunden-UstID:&nbsp;</b></td><td align="left">{###CUSTOMER_VAT_ID###}</td></tr><!-- EOF_CUSTOMER_VAT_ID -->
		<!-- BOF_CREATION_DATE --><tr><td align="left"><b>Datum:&nbsp;</b></td><td align="left">{###CREATION_DATE###}</td></tr><!-- EOF_CREATION_DATE -->
		<!-- BOF_BINDING_DATE --><tr><td align="left"><b>Bindefrist:&nbsp;</b></td><td align="left">{###BINDING_DATE###}</td></tr><!-- EOF_BINDING_DATE -->
		<!-- BOF_LINE_DISTANCE --><tr><td colspan="2"></td></tr><!-- EOF_LINE_DISTANCE -->
		<!-- BOF_CUSTOMERS_ORDER_NUMBER --><tr><td align="left"><b>Ihre Auftrags-Nr./AZ:&nbsp;</b></td><td align="left">{###CUSTOMERS_ORDER_NUMBER###}</td></tr><!-- EOF_CUSTOMERS_ORDER_NUMBER -->
		<!-- BOF_CUSTOMERS_SALESMAN --><tr><td align="left"><b>Wiederverk&auml;ufer:&nbsp;</b></td><td align="left">{###CUSTOMERS_SALESMAN###}</td></tr><!-- EOF_CUSTOMERS_SALESMAN -->
		<!-- BOF_SALESMANS_CUSTOMER --><tr><td align="left"><b>Kunde:&nbsp;</b></td><td align="left">{###SALESMANS_CUSTOMER###}</td></tr><!-- EOF_SALESMANS_CUSTOMER -->		
	';
	// EOF AN

	###################################################

	// BOF AB
	$arrContentDatas['AB'] = array(
		'SALUTATION_TEXT' => '<p>Sehr geehrte Kundin, sehr geehrter Kunde,</p>',
		'INTRO_TEXT' => '<p>nachfolgende Posten haben Sie uns in Auftrag gegeben:</p>',
		'EDITOR_TEXT' => '',
		'PAYMENT_CONDITIONS_VK' => '<p><b>Zahlung: {###PAYMENT_CONDITION###}, per {###PAYMENT_TYPE###}. Die Rechnung erhalten Sie bei Lieferung!{###BANK_DATAS###}</b>.</p>',
		// 'PAYMENT_CONDITIONS' => '<p><span class="important"><b>Die Rechnung folgt</b></span>!</p>',
		'PAYMENT_CONDITIONS' => '<p><span class="important"><b>Die Rechnung erhalten Sie bei Lieferung</b></span>!</p>',
		'OUTRO_TEXT' => '<p>Bitte pr&uuml;fen Sie die Auftragsbest&auml;tigung auf Fehler. Setzen Sie uns unverz&uuml;glich &uuml;ber m&ouml;gliche Unstimmigkeiten mit Ihrer Bestellung in Kenntnis. Ansonsten akzeptieren Sie diesen Auftrag wie in der Auftragsbest&auml;tigung beschrieben und eine Reklamation bzgl. Artikel und Farbe wird nicht akzeptiert. Irrt&uuml;mer bzgl. Menge und Preise in dieser Auftragsbest&auml;tigung k&ouml;nnen bei Rechnungsstellung korrigiert werden. Die gelieferten Waren bleiben bis zur vollst&auml;ndigen Bezahlung unser Eigentum. Reklamationen nur schriftlich innerhalb von 14 Tagen. Bei Drucksachen behalten wir uns eine Minder- oder Mehrlieferung bis zu 10% aus technischen Gr&uuml;nden vor.<br><b>&Auml;nderungen bei Lieferzeiten vorbehalten!</b></p><p>Wir danken f&uuml;r Ihren Auftrag.</p>',
		'REGARDS_TEXT' => '<p>Mit freundlichen Gr&uuml;&szlig;en,<br><b><i>Ihr Burhan CTR Team</i></b></p>',
		// 'BANK_ACCOUNT_WARNING' => '' . $arrWarningBankAccountText . '',
		'BANK_ACCOUNT_WARNING' => '',
		'FOOTER_NOTICE' => '',
	);

	$arrDocumentDatas['AB'] = '		
		<!-- BOF_DOCUMENT_NUMBER --><tr><td align="left"><b>Auftragsnummer:&nbsp;</b></td><td align="left">{###DOCUMENT_NUMBER###}</td></tr><!-- EOF_DOCUMENT_NUMBER -->
		<!-- BOF_DOCUMENT_NUMBER_AN --><tr><td align="left"><b>Angebotsnummer:&nbsp;</b></td><td align="left">{###DOCUMENT_NUMBER_AN###}</td></tr><!-- EOF_DOCUMENT_NUMBER_AN -->
		<!-- BOF_CUSTOMER_NUMBER --><tr><td align="left"><b>Kundennummer:&nbsp;</b></td><td align="left">{###CUSTOMER_NUMBER###}</td></tr><!-- EOF_CUSTOMER_NUMBER -->
		<!-- BOF_CUSTOMER_VAT_ID --><tr><td align="left"><b>Kunden-UstID:&nbsp;</b></td><td align="left">{###CUSTOMER_VAT_ID###}</td></tr><!-- EOF_CUSTOMER_VAT_ID -->
		<!-- BOF_CREATION_DATE --><tr><td align="left"><b>Datum:&nbsp;</b></td><td align="left">{###CREATION_DATE###}</td></tr><!-- EOF_CREATION_DATE -->
		<!-- BOF_LINE_DISTANCE --><tr><td colspan="2"></td></tr><!-- EOF_LINE_DISTANCE -->
		<!-- BOF_CUSTOMERS_ORDER_NUMBER --><tr><td align="left"><b>Ihre Auftrags-Nr./AZ:&nbsp;</b></td><td align="left">{###CUSTOMERS_ORDER_NUMBER###}</td></tr><!-- EOF_CUSTOMERS_ORDER_NUMBER -->
		<!-- BOF_ORDER_DATE --><tr><td align="left"><b>Ihr Auftrag vom:&nbsp;</b></td><td align="left">{###ORDER_DATE###}</td></tr><!-- EOF_ORDER_DATE -->
		<!-- BOF_SHIPPING_DATE --><tr><td align="left"><b>Lieferdatum:&nbsp;</b></td><td align="left">{###SHIPPING_DATE###}</td></tr><!-- EOF_SHIPPING_DATE -->
		<!-- BOF_CUSTOMERS_SALESMAN --><tr><td align="left"><b>Wiederverk&auml;ufer:&nbsp;</b></td><td align="left">{###CUSTOMERS_SALESMAN###}</td></tr><!-- EOF_CUSTOMERS_SALESMAN -->
		<!-- BOF_SALESMANS_CUSTOMER --><tr><td align="left"><b>Kunde:&nbsp;</b></td><td align="left">{###SALESMANS_CUSTOMER###}</td></tr><!-- EOF_SALESMANS_CUSTOMER -->
		<!-- BOF_SHIPPING_TYPE --><tr><td align="left"><b>Versandart:&nbsp;</b></td><td align="left">{###SHIPPING_TYPE###}</td></tr><!-- EOF_SHIPPING_TYPE -->		
	';

	// EOF AB

	###################################################

	// BOF LS
	$arrContentDatas['LS'] = array(
		'SALUTATION_TEXT' => '<p>Sehr geehrte Kundin, sehr geehrter Kunde,</p>',
		'INTRO_TEXT' => '<p>wir lieferten gem&auml;&szlig; Ihrer Bestellung.</p>',
		'EDITOR_TEXT' => '',
		// 'PAYMENT_CONDITIONS' => '<p><b>Zahlung: {###PAYMENT_CONDITION###}, per {###PAYMENT_TYPE###}{###BANK_DATAS###}</b>.</p>',
		'PAYMENT_CONDITIONS' => '',
		'OUTRO_TEXT' => '<p>Die gelieferten Waren bleiben bis zur vollst&auml;ndigen Bezahlung unser Eigentum. Reklamationen nur schriftlich innerhalb von 14 Tagen. Bei Drucksachen behalten wir uns eine Minder- oder Mehrlieferung bis zu 10% aus technischen Gr&uuml;nden vor.</p><p>Wir danken f&uuml;r Ihren Auftrag.</p>',
		'REGARDS_TEXT' => '<p>Mit freundlichen Gr&uuml;&szlig;en,<br><b><i>Ihr Burhan CTR Team</i></b></p>',
		// 'BANK_ACCOUNT_WARNING' => '' . $arrWarningBankAccountText . '',
		'BANK_ACCOUNT_WARNING' => '',
		'FOOTER_NOTICE' => '',
	);

	$arrDocumentDatas['LS'] = '
		<!-- BOF_DOCUMENT_NUMBER --><tr><td align="left"><b>Liefernummer:&nbsp;</b></td><td align="left">{###DOCUMENT_NUMBER###}</td></tr><!-- EOF_DOCUMENT_NUMBER -->
		<!-- BOF_DOCUMENT_NUMBER_AB --><tr><td align="left"><b>Auftragssnummer:&nbsp;</b></td><td align="left">{###DOCUMENT_NUMBER_AB###}</td></tr><!-- EOF_DOCUMENT_NUMBER_AB -->		
		<!-- BOF_CUSTOMER_NUMBER --><tr><td align="left"><b>Kundennummer:&nbsp;</b></td><td align="left">{###CUSTOMER_NUMBER###}</td></tr><!-- EOF_CUSTOMER_NUMBER -->
		<!-- BOF_CUSTOMER_VAT_ID --><tr><td align="left"><b>Kunden-UstID:&nbsp;</b></td><td align="left">{###CUSTOMER_VAT_ID###}</td></tr><!-- EOF_CUSTOMER_VAT_ID -->
		<!-- BOF_CREATION_DATE --><tr><td align="left"><b>Datum:&nbsp;</b></td><td align="left">{###CREATION_DATE###}</td></tr><!-- EOF_CREATION_DATE -->
		<!-- BOF_SHIPPING_DATE --><tr><td align="left"><b>Lieferdatum:&nbsp;</b></td><td align="left">{###SHIPPING_DATE###}</td></tr><!-- EOF_SHIPPING_DATE -->
		<!-- BOF_LINE_DISTANCE --><tr><td colspan="2"></td></tr><!-- EOF_LINE_DISTANCE -->
		<!-- BOF_CUSTOMERS_ORDER_NUMBER --><tr><td align="left"><b>Ihre Auftrags-Nr./AZ:&nbsp;</b></td><td align="left">{###CUSTOMERS_ORDER_NUMBER###}</td></tr><!-- EOF_CUSTOMERS_ORDER_NUMBER -->
		<!-- BOF_ORDER_DATE --><tr><td align="left"><b>Ihr Auftrag vom:&nbsp;</b></td><td align="left">{###ORDER_DATE###}</td></tr><!-- EOF_ORDER_DATE -->
		<!-- BOF_CUSTOMERS_SALESMAN --><tr><td align="left"><b>Wiederverk&auml;ufer:&nbsp;</b></td><td align="left">{###CUSTOMERS_SALESMAN###}</td></tr><!-- EOF_CUSTOMERS_SALESMAN -->
		<!-- BOF_SALESMANS_CUSTOMER --><tr><td align="left"><b>Kunde:&nbsp;</b></td><td align="left">{###SALESMANS_CUSTOMER###}</td></tr><!-- EOF_SALESMANS_CUSTOMER -->
		<!-- BOF_SHIPPING_TYPE --><tr><td align="left"><b>Versandart:&nbsp;</b></td><td align="left">{###SHIPPING_TYPE###}</td></tr><!-- EOF_SHIPPING_TYPE -->
		<!-- BOF_SHIPPING_TYPE_NUMBER --><tr><td align="left"><b>Sendungsverfolgung:&nbsp;</b></td><td align="left">{###SHIPPING_TYPE_NUMBER###}</td></tr><!-- EOF_SHIPPING_TYPE_NUMBER -->		
	';
	// EOF LS

	###################################################

	// BOF RE
	$arrContentDatas['RE'] = array(
		'SALUTATION_TEXT' => '<p>Sehr geehrte Kundin, sehr geehrter Kunde,</p>',
		'INTRO_TEXT' => '<p>wir lieferten gem&auml;&szlig; Ihrer Bestellung.</p>',
		'EDITOR_TEXT' => '',
		'PAYMENT_CONDITIONS' => '<p><b>Zahlung: {###PAYMENT_CONDITION###}, per {###PAYMENT_TYPE###}{###BANK_DATAS###}</b>.</p>',
		'OUTRO_TEXT' => '<p>Die gelieferten Waren bleiben bis zur vollst&auml;ndigen Bezahlung unser Eigentum. Reklamationen nur schriftlich innerhalb von 14 Tagen. Bei Drucksachen behalten wir uns eine Minder- oder Mehrlieferung bis zu 10% aus technischen Gr&uuml;nden vor.</p><p>Wir danken f&uuml;r Ihren Auftrag.</p>',
		'REGARDS_TEXT' => '<p>Mit freundlichen Gr&uuml;&szlig;en,<br><b><i>Ihr Burhan CTR Team</i></b></p>',
		'BANK_ACCOUNT_WARNING' => '' . $arrWarningBankAccountText . '',
		'FOOTER_NOTICE' => '',
	);

	$arrDocumentDatas['RE'] = '
		<!-- BOF_DOCUMENT_NUMBER --><tr><td align="left"><b>Rechnungsnummer:&nbsp;</b></td><td align="left">{###DOCUMENT_NUMBER###}</td></tr><!-- EOF_DOCUMENT_NUMBER -->
		<!-- BOF_DOCUMENT_NUMBER_AB --><tr><td align="left"><b>Auftragsnummer:&nbsp;</b></td><td align="left">{###DOCUMENT_NUMBER_AB###}</td></tr><!-- EOF_DOCUMENT_NUMBER_AB -->
		<!-- BOF_CUSTOMER_NUMBER --><tr><td align="left"><b>Kundennummer:&nbsp;</b></td><td align="left">{###CUSTOMER_NUMBER###}</td></tr><!-- EOF_CUSTOMER_NUMBER -->
		<!-- BOF_CUSTOMER_VAT_ID --><tr><td align="left"><b>Kunden-UstID:&nbsp;</b></td><td align="left">{###CUSTOMER_VAT_ID###}</td></tr><!-- EOF_CUSTOMER_VAT_ID -->
		<!-- BOF_CREATION_DATE --><tr><td align="left"><b>Datum:&nbsp;</b></td><td align="left">{###CREATION_DATE###}</td></tr><!-- EOF_CREATION_DATE -->
		<!-- BOF_SHIPPING_DATE --><tr><td align="left"><b>Lieferdatum:&nbsp;</b></td><td align="left">{###SHIPPING_DATE###}</td></tr><!-- EOF_SHIPPING_DATE -->
		<!-- BOF_LINE_DISTANCE --><tr><td colspan="2"></td></tr><!-- EOF_LINE_DISTANCE -->
		<!-- BOF_CUSTOMERS_ORDER_NUMBER --><tr><td align="left"><b>Ihre Auftrags-Nr./AZ:&nbsp;</b></td><td align="left">{###CUSTOMERS_ORDER_NUMBER###}</td></tr><!-- EOF_CUSTOMERS_ORDER_NUMBER -->
		<!-- BOF_ORDER_DATE --><tr><td align="left"><b>Ihr Auftrag vom:&nbsp;</b></td><td align="left">{###ORDER_DATE###}</td></tr><!-- EOF_ORDER_DATE -->
		<!-- BOF_CUSTOMERS_SALESMAN --><tr><td align="left"><b>Wiederverk&auml;ufer:&nbsp;</b></td><td align="left">{###CUSTOMERS_SALESMAN###}</td></tr><!-- EOF_CUSTOMERS_SALESMAN -->
		<!-- BOF_SALESMANS_CUSTOMER --><tr><td align="left"><b>Kunde:&nbsp;</b></td><td align="left">{###SALESMANS_CUSTOMER###}</td></tr><!-- EOF_SALESMANS_CUSTOMER -->
		<!-- BOF_SHIPPING_TYPE --><tr><td align="left"><b>Versandart:&nbsp;</b></td><td align="left">{###SHIPPING_TYPE###}</td></tr><!-- EOF_SHIPPING_TYPE -->		
	';
	// EOF RE

	###################################################

	// BOF MA
	$arrContentDatas['MA'] = array(
		'SALUTATION_TEXT' => '<p>Sehr geehrte Kundin, sehr geehrter Kunde,</p>',
		'INTRO_TEXT' => '<p>leider k&ouml;nnen wir zu der nachfolgenden Rechnung immer noch keinen Zahlungseingang feststellen.</p>',
		'EDITOR_TEXT' => '',
		'PAYMENT_CONDITIONS' => '<p><b>Zahlung: Wir bitten Sie, die Zahlung innerhalb einer Woche vorzunehmen.</b></p>',
		'OUTRO_TEXT' => '<p>Die gelieferten Waren bleiben bis zur vollst&auml;ndigen Bezahlung unser Eigentum.</p><p>Sollten Sie den offenen Betrag bereits ausgeglichen haben, beachten Sie bitte dieses Schreiben als gegenstandslos.</p>',
		'REGARDS_TEXT' => '<p>Mit freundlichen Gr&uuml;&szlig;en,<br><b><i>Ihr Burhan CTR Team</i></b></p>',
		'BANK_ACCOUNT_WARNING' => '' . $arrWarningBankAccountText . '',
		'FOOTER_NOTICE' => '',
	);

	$arrDocumentDatas['MA'] = '		
		<!-- BOF_DOCUMENT_NUMBER --><tr><td align="left"><b>Zahlerinnerungsnummer:&nbsp;</b></td><td align="left">{###DOCUMENT_NUMBER###}</td></tr><!-- EOF_DOCUMENT_NUMBER -->
		<!-- BOF_DOCUMENT_NUMBER_RE--><tr><td align="left"><b>Rechnungssnummer:&nbsp;</b></td><td align="left">{###DOCUMENT_NUMBER_RE###}</td></tr><!-- EOF_DOCUMENT_NUMBER_RE -->
		<!-- BOF_DOCUMENT_NUMBER_AB--><tr><td align="left"><b>Auftragssnummer:&nbsp;</b></td><td align="left">{###DOCUMENT_NUMBER_AB###}</td></tr><!-- EOF_DOCUMENT_NUMBER_AB -->
		<!-- BOF_CUSTOMER_NUMBER --><tr><td align="left"><b>Kundennummer:&nbsp;</b></td><td align="left">{###CUSTOMER_NUMBER###}</td></tr><!-- EOF_CUSTOMER_NUMBER -->
		<!-- BOF_CUSTOMER_VAT_ID --><tr><td align="left"><b>Kunden-UstID:&nbsp;</b></td><td align="left">{###CUSTOMER_VAT_ID###}</td></tr><!-- EOF_CUSTOMER_VAT_ID -->
		<!-- BOF_CREATION_DATE --><tr><td align="left"><b>Datum:&nbsp;</b></td><td align="left">{###CREATION_DATE###}</td></tr><!-- EOF_CREATION_DATE -->
		<!-- BOF_LINE_DISTANCE --><tr><td colspan="2"></td></tr><!-- EOF_LINE_DISTANCE -->
		<!-- BOF_CUSTOMERS_ORDER_NUMBER --><tr><td align="left"><b>Ihre Auftrags-Nr./AZ:&nbsp;</b></td><td align="left">{###CUSTOMERS_ORDER_NUMBER###}</td></tr><!-- EOF_CUSTOMERS_ORDER_NUMBER -->
		<!-- BOF_ORDER_DATE --><tr><td align="left"><b>Ihr Auftrag vom:&nbsp;</b></td><td align="left">{###ORDER_DATE###}</td></tr><!-- EOF_ORDER_DATE -->
		<!-- BOF_INVOICE_DATE --><tr><td align="left"><b>Rechnung vom:&nbsp;</b></td><td align="left">{###INVOICE_DATE###}</td></tr><!-- EOF_SHIPPING_DATE -->
		<!-- BOF_CUSTOMERS_SALESMAN --><tr><td align="left"><b>Wiederverk&auml;ufer:&nbsp;</b></td><td align="left">{###CUSTOMERS_SALESMAN###}</td></tr><!-- EOF_CUSTOMERS_SALESMAN -->
		<!-- BOF_SALESMANS_CUSTOMER --><tr><td align="left"><b>Kunde:&nbsp;</b></td><td align="left">{###SALESMANS_CUSTOMER###}</td></tr><!-- EOF_SALESMANS_CUSTOMER -->
		<!-- BOF_SHIPPING_TYPE --><tr><td align="left"><b>Versandart:&nbsp;</b></td><td align="left">{###SHIPPING_TYPE###}</td></tr><!-- EOF_SHIPPING_TYPE -->		
	';
	// EOF MA

	###################################################

	// BOF M1
	$arrContentDatas['M1'] = array(
		'SALUTATION_TEXT' => '<p>Sehr geehrte Kundin, sehr geehrter Kunde,</p>',
		'INTRO_TEXT' => '<p>leider k&ouml;nnen wir zu der nachfolgenden Rechnung trotz erfolgter Zahlungserinnerung immer noch keinen Zahlungseingang feststellen.</p>',
		'EDITOR_TEXT' => '',
		'PAYMENT_CONDITIONS' => '<p><b>Zahlung: Wir bitten Sie, die Zahlung nunmehr unverz&uuml;glich bis zum {###DEMAND_DEADLINE_DATE###} vorzunehmen, da wir uns gezwungen sehen, rechtliche Hilfe in Anspruch zu nehmen und ggf. das gerichtliche Mahnverfahren gegen Sie einzuleiten.</b></p>',
		'OUTRO_TEXT' => '<p>Die gelieferten Waren bleiben bis zur vollst&auml;ndigen Bezahlung unser Eigentum.</p><p>Sollten Sie den offenen Betrag bereits ausgeglichen haben, beachten Sie bitte dieses Schreiben als gegenstandslos.</p>',
		'REGARDS_TEXT' => '<p>Mit freundlichen Gr&uuml;&szlig;en,<br><b><i>Ihr Burhan CTR Team</i></b></p>',
		'BANK_ACCOUNT_WARNING' => '' . $arrWarningBankAccountText . '',
		'FOOTER_NOTICE' => '',
	);

	$arrDocumentDatas['M1'] = '		
		<!-- BOF_DOCUMENT_NUMBER --><tr><td align="left"><b>Mahnungssnummer:&nbsp;</b></td><td align="left">{###DOCUMENT_NUMBER###}</td></tr><!-- EOF_DOCUMENT_NUMBER -->
		<!-- BOF_DOCUMENT_NUMBER_RE--><tr><td align="left"><b>Rechnungssnummer:&nbsp;</b></td><td align="left">{###DOCUMENT_NUMBER_RE###}</td></tr><!-- EOF_DOCUMENT_NUMBER_RE -->
		<!-- BOF_DOCUMENT_NUMBER_AB--><tr><td align="left"><b>Auftragssnummer:&nbsp;</b></td><td align="left">{###DOCUMENT_NUMBER_AB###}</td></tr><!-- EOF_DOCUMENT_NUMBER_AB -->
		<!-- BOF_CUSTOMER_NUMBER --><tr><td align="left"><b>Kundennummer:&nbsp;</b></td><td align="left">{###CUSTOMER_NUMBER###}</td></tr><!-- EOF_CUSTOMER_NUMBER -->
		<!-- BOF_CUSTOMER_VAT_ID --><tr><td align="left"><b>Kunden-UstID:&nbsp;</b></td><td align="left">{###CUSTOMER_VAT_ID###}</td></tr><!-- EOF_CUSTOMER_VAT_ID -->
		<!-- BOF_CREATION_DATE --><tr><td align="left"><b>Datum:&nbsp;</b></td><td align="left">{###CREATION_DATE###}</td></tr><!-- EOF_CREATION_DATE -->
		<!-- BOF_LINE_DISTANCE --><tr><td colspan="2"></td></tr><!-- EOF_LINE_DISTANCE -->
		<!-- BOF_CUSTOMERS_ORDER_NUMBER --><tr><td align="left"><b>Ihre Auftrags-Nr./AZ:&nbsp;</b></td><td align="left">{###CUSTOMERS_ORDER_NUMBER###}</td></tr><!-- EOF_CUSTOMERS_ORDER_NUMBER -->
		<!-- BOF_ORDER_DATE --><tr><td align="left"><b>Ihr Auftrag vom:&nbsp;</b></td><td align="left">{###ORDER_DATE###}</td></tr><!-- EOF_ORDER_DATE -->
		<!-- BOF_INVOICE_DATE --><tr><td align="left"><b>Rechnung vom:&nbsp;</b></td><td align="left">{###INVOICE_DATE###}</td></tr><!-- EOF_SHIPPING_DATE -->
		<!-- BOF_CUSTOMERS_SALESMAN --><tr><td align="left"><b>Wiederverk&auml;ufer:&nbsp;</b></td><td align="left">{###CUSTOMERS_SALESMAN###}</td></tr><!-- EOF_CUSTOMERS_SALESMAN -->
		<!-- BOF_SALESMANS_CUSTOMER --><tr><td align="left"><b>Kunde:&nbsp;</b></td><td align="left">{###SALESMANS_CUSTOMER###}</td></tr><!-- EOF_SALESMANS_CUSTOMER -->
		<!-- BOF_SHIPPING_TYPE --><tr><td align="left"><b>Versandart:&nbsp;</b></td><td align="left">{###SHIPPING_TYPE###}</td></tr><!-- EOF_SHIPPING_TYPE -->		
	';
	// EOF M1

	###################################################

	// BOF M2
	$arrContentDatas['M2'] = array(
		'SALUTATION_TEXT' => '<p>Sehr geehrte Kundin, sehr geehrter Kunde,</p>',
		'INTRO_TEXT' => '<p>leider k&ouml;nnen wir zu der nachfolgenden Rechnung trotz erfolgter 1. Mahnung immer noch keinen Zahlungseingang feststellen.</p>',
		'EDITOR_TEXT' => '',
		'PAYMENT_CONDITIONS' => '<p><b>Zahlung: Wir bitten Sie, die Zahlung nunmehr unverz&uuml;glich bis zum {###DEMAND_DEADLINE_DATE###} vorzunehmen, da wir uns gezwungen sehen, rechtliche Hilfe in Anspruch zu nehmen und ggf. das gerichtliche Mahnverfahren gegen Sie einzuleiten.</b></p>',
		'OUTRO_TEXT' => '<p>Die gelieferten Waren bleiben bis zur vollst&auml;ndigen Bezahlung unser Eigentum.</p><p>Sollten Sie den offenen Betrag bereits ausgeglichen haben, beachten Sie bitte dieses Schreiben als gegenstandslos.</p>',
		'REGARDS_TEXT' => '<p>Mit freundlichen Gr&uuml;&szlig;en,<br><b><i>Ihr Burhan CTR Team</i></b></p>',
		'BANK_ACCOUNT_WARNING' => '' . $arrWarningBankAccountText . '',
		'FOOTER_NOTICE' => '',
	);

	$arrDocumentDatas['M2'] = '		
		<!-- BOF_DOCUMENT_NUMBER --><tr><td align="left"><b>Mahnungssnummer:&nbsp;</b></td><td align="left">{###DOCUMENT_NUMBER###}</td></tr><!-- EOF_DOCUMENT_NUMBER -->
		<!-- BOF_DOCUMENT_NUMBER_RE--><tr><td align="left"><b>Rechnungssnummer:&nbsp;</b></td><td align="left">{###DOCUMENT_NUMBER_RE###}</td></tr><!-- EOF_DOCUMENT_NUMBER_RE -->
		<!-- BOF_DOCUMENT_NUMBER_AB--><tr><td align="left"><b>Auftragssnummer:&nbsp;</b></td><td align="left">{###DOCUMENT_NUMBER_AB###}</td></tr><!-- EOF_DOCUMENT_NUMBER_AB -->
		<!-- BOF_CUSTOMER_NUMBER --><tr><td align="left"><b>Kundennummer:&nbsp;</b></td><td align="left">{###CUSTOMER_NUMBER###}</td></tr><!-- EOF_CUSTOMER_NUMBER -->
		<!-- BOF_CUSTOMER_VAT_ID --><tr><td align="left"><b>Kunden-UstID:&nbsp;</b></td><td align="left">{###CUSTOMER_VAT_ID###}</td></tr><!-- EOF_CUSTOMER_VAT_ID -->
		<!-- BOF_CREATION_DATE --><tr><td align="left"><b>Datum:&nbsp;</b></td><td align="left">{###CREATION_DATE###}</td></tr><!-- EOF_CREATION_DATE -->
		<!-- BOF_LINE_DISTANCE --><tr><td colspan="2"></td></tr><!-- EOF_LINE_DISTANCE -->
		<!-- BOF_CUSTOMERS_ORDER_NUMBER --><tr><td align="left"><b>Ihre Auftrags-Nr./AZ:&nbsp;</b></td><td align="left">{###CUSTOMERS_ORDER_NUMBER###}</td></tr><!-- EOF_CUSTOMERS_ORDER_NUMBER -->
		<!-- BOF_ORDER_DATE --><tr><td align="left"><b>Ihr Auftrag vom:&nbsp;</b></td><td align="left">{###ORDER_DATE###}</td></tr><!-- EOF_ORDER_DATE -->
		<!-- BOF_INVOICE_DATE --><tr><td align="left"><b>Rechnung vom:&nbsp;</b></td><td align="left">{###INVOICE_DATE###}</td></tr><!-- EOF_SHIPPING_DATE -->
		<!-- BOF_CUSTOMERS_SALESMAN --><tr><td align="left"><b>Wiederverk&auml;ufer:&nbsp;</b></td><td align="left">{###CUSTOMERS_SALESMAN###}</td></tr><!-- EOF_CUSTOMERS_SALESMAN -->
		<!-- BOF_SALESMANS_CUSTOMER --><tr><td align="left"><b>Kunde:&nbsp;</b></td><td align="left">{###SALESMANS_CUSTOMER###}</td></tr><!-- EOF_SALESMANS_CUSTOMER -->
		<!-- BOF_SHIPPING_TYPE --><tr><td align="left"><b>Versandart:&nbsp;</b></td><td align="left">{###SHIPPING_TYPE###}</td></tr><!-- EOF_SHIPPING_TYPE -->		
	';
	// EOF M2

	###################################################

	// BOF M3
	$arrContentDatas['M3'] = array(
		'SALUTATION_TEXT' => '<p>Sehr geehrte Kundin, sehr geehrter Kunde,</p>',
		'INTRO_TEXT' => '<p>leider k&ouml;nnen wir zu der nachfolgenden Rechnung trotz erfolgter 2. Mahnung immer noch keinen Zahlungseingang feststellen.</p>',
		'EDITOR_TEXT' => '',
		'PAYMENT_CONDITIONS' => '<p><b>Zahlung: Wir bitten Sie, die Zahlung nunmehr unverz&uuml;glich vorzunehmen, da wir sonst gerichtliche Schritte gegen Sie einleiten m&uuml;ssten.</b></p>',
		'OUTRO_TEXT' => '<p>Die gelieferten Waren bleiben bis zur vollst&auml;ndigen Bezahlung unser Eigentum.</p><p>Sollten Sie den offenen Betrag bereits ausgeglichen haben, beachten Sie bitte dieses Schreiben als gegenstandslos.</p>',
		'REGARDS_TEXT' => '<p>Mit freundlichen Gr&uuml;&szlig;en,<br><b><i>Ihr Burhan CTR Team</i></b></p>',
		'BANK_ACCOUNT_WARNING' => '' . $arrWarningBankAccountText . '',
		'FOOTER_NOTICE' => '',
	);

	$arrDocumentDatas['M3'] = '		
		<!-- BOF_DOCUMENT_NUMBER --><tr><td align="left"><b>Mahnungssnummer:&nbsp;</b></td><td align="left">{###DOCUMENT_NUMBER###}</td></tr><!-- EOF_DOCUMENT_NUMBER -->
		<!-- BOF_DOCUMENT_NUMBER_RE--><tr><td align="left"><b>Rechnungssnummer:&nbsp;</b></td><td align="left">{###DOCUMENT_NUMBER_RE###}</td></tr><!-- EOF_DOCUMENT_NUMBER_RE -->
		<!-- BOF_DOCUMENT_NUMBER_AB--><tr><td align="left"><b>Auftragssnummer:&nbsp;</b></td><td align="left">{###DOCUMENT_NUMBER_AB###}</td></tr><!-- EOF_DOCUMENT_NUMBER_AB -->
		<!-- BOF_CUSTOMER_NUMBER --><tr><td align="left"><b>Kundennummer:&nbsp;</b></td><td align="left">{###CUSTOMER_NUMBER###}</td></tr><!-- EOF_CUSTOMER_NUMBER -->
		<!-- BOF_CUSTOMER_VAT_ID --><tr><td align="left"><b>Kunden-UstID:&nbsp;</b></td><td align="left">{###CUSTOMER_VAT_ID###}</td></tr><!-- EOF_CUSTOMER_VAT_ID -->
		<!-- BOF_CREATION_DATE --><tr><td align="left"><b>Datum:&nbsp;</b></td><td align="left">{###CREATION_DATE###}</td></tr><!-- EOF_CREATION_DATE -->
		<!-- BOF_LINE_DISTANCE --><tr><td colspan="2"></td></tr><!-- EOF_LINE_DISTANCE -->
		<!-- BOF_CUSTOMERS_ORDER_NUMBER --><tr><td align="left"><b>Ihre Auftrags-Nr./AZ:&nbsp;</b></td><td align="left">{###CUSTOMERS_ORDER_NUMBER###}</td></tr><!-- EOF_CUSTOMERS_ORDER_NUMBER -->
		<!-- BOF_ORDER_DATE --><tr><td align="left"><b>Ihr Auftrag vom:&nbsp;</b></td><td align="left">{###ORDER_DATE###}</td></tr><!-- EOF_ORDER_DATE -->
		<!-- BOF_INVOICE_DATE --><tr><td align="left"><b>Rechnung vom:&nbsp;</b></td><td align="left">{###INVOICE_DATE###}</td></tr><!-- EOF_SHIPPING_DATE -->
		<!-- BOF_CUSTOMERS_SALESMAN --><tr><td align="left"><b>Wiederverk&auml;ufer:&nbsp;</b></td><td align="left">{###CUSTOMERS_SALESMAN###}</td></tr><!-- EOF_CUSTOMERS_SALESMAN -->
		<!-- BOF_SALESMANS_CUSTOMER --><tr><td align="left"><b>Kunde:&nbsp;</b></td><td align="left">{###SALESMANS_CUSTOMER###}</td></tr><!-- EOF_SALESMANS_CUSTOMER -->
		<!-- BOF_SHIPPING_TYPE --><tr><td align="left"><b>Versandart:&nbsp;</b></td><td align="left">{###SHIPPING_TYPE###}</td></tr><!-- EOF_SHIPPING_TYPE -->		
	';
	// EOF M3

	###################################################

	// BOF GU
	$arrContentDatas['GU'] = array(
		'SALUTATION_TEXT' => '<p>Sehr geehrte Kundin, sehr geehrter Kunde,</p>',
		'INTRO_TEXT' => '<p>wir lieferten gem&auml;&szlig; Ihrer Bestellung.</p>',
		'EDITOR_TEXT' => '',
		// 'PAYMENT_CONDITIONS' => '<p><b>Zahlung: {###PAYMENT_CONDITION###}, per {###PAYMENT_TYPE###}{###BANK_DATAS###}</b>.</p>',
		'PAYMENT_CONDITIONS' => '',
		'OUTRO_TEXT' => '<p>Wir danken f&uuml;r Ihren Auftrag.</p>',
		'REGARDS_TEXT' => '<p>Mit freundlichen Gr&uuml;&szlig;en,<br><b><i>Ihr Burhan CTR Team</i></b></p>',
		// 'BANK_ACCOUNT_WARNING' => '' . $arrWarningBankAccountText . '',
		'BANK_ACCOUNT_WARNING' => '',
		'FOOTER_NOTICE' => '',
	);
	$arrDocumentDatas['GU'] = '		
		<!-- BOF_DOCUMENT_NUMBER --><tr><td align="left"><b>Gutschriftsnummer:&nbsp;</b></td><td align="left">{###DOCUMENT_NUMBER###}</td></tr><!-- EOF_DOCUMENT_NUMBER -->
		<!-- BOF_DOCUMENT_NUMBER_RE--><tr><td align="left"><b>Rechnungssnummer:&nbsp;</b></td><td align="left">{###DOCUMENT_NUMBER_RE###}</td></tr><!-- EOF_DOCUMENT_NUMBER_RE -->
		<!-- BOF_DOCUMENT_NUMBER_AB--><tr><td align="left"><b>Auftragssnummer:&nbsp;</b></td><td align="left">{###DOCUMENT_NUMBER_AB###}</td></tr><!-- EOF_DOCUMENT_NUMBER_AB -->
		<!-- BOF_CREATION_DATE --><tr><td align="left"><b>Datum:&nbsp;</b></td><td align="left">{###CREATION_DATE###}</td></tr><!-- EOF_CREATION_DATE -->
		<!-- BOF_CUSTOMERS_ORDER_NUMBER --><tr><td align="left"><b>Ihre Auftrags-Nr./AZ:&nbsp;</b></td><td align="left">{###CUSTOMERS_ORDER_NUMBER###}</td></tr><!-- EOF_CUSTOMERS_ORDER_NUMBER -->
		<!-- BOF_CUSTOMER_NUMBER --><tr><td align="left"><b>Kundennummer:&nbsp;</b></td><td align="left">{###CUSTOMER_NUMBER###}</td></tr><!-- EOF_CUSTOMER_NUMBER -->
		<!-- BOF_CUSTOMER_VAT_ID --><tr><td align="left"><b>Kunden-UstID:&nbsp;</b></td><td align="left">{###CUSTOMER_VAT_ID###}</td></tr><!-- EOF_CUSTOMER_VAT_ID -->
		<!-- BOF_CUSTOMERS_SALESMAN --><tr><td align="left"><b>Wiederverk&auml;ufer:&nbsp;</b></td><td align="left">{###CUSTOMERS_SALESMAN###}</td></tr><!-- EOF_CUSTOMERS_SALESMAN -->
		<!-- BOF_SALESMANS_CUSTOMER --><tr><td align="left"><b>Kunde:&nbsp;</b></td><td align="left">{###SALESMANS_CUSTOMER###}</td></tr><!-- EOF_SALESMANS_CUSTOMER -->
	';
	// EOF GU

	###################################################

	// BOF BR
	$arrContentDatas['BR'] = array(
		'SALUTATION_TEXT' => '<p>Sehr geehrte Kundin, sehr geehrter Kunde,</p>',
		'INTRO_TEXT' => '',
		'EDITOR_TEXT' => '{###EDITOR_TEXT###}',
		'PAYMENT_CONDITIONS' => '',
		'OUTRO_TEXT' => '',
		'REGARDS_TEXT' => '<p>Mit freundlichen Gr&uuml;&szlig;en,<br><b><i>Ihr Burhan CTR Team</i></b></p>',
		'BANK_ACCOUNT_WARNING' => '',
		'FOOTER_NOTICE' => '',
	);

	$arrDocumentDatas['BR'] = '
		<!-- BOF_DOCUMENT_NUMBER --><tr><td align="left"><b>Dokumentnummer:&nbsp;</b></td><td align="left">{###DOCUMENT_NUMBER###}</td></tr><!-- EOF_DOCUMENT_NUMBER -->
		<!-- BOF_CUSTOMER_NUMBER --><tr><td align="left"><b>Kundennummer:&nbsp;</b></td><td align="left">{###CUSTOMER_NUMBER###}</td></tr><!-- EOF_CUSTOMER_NUMBER -->
		<!-- BOF_CUSTOMER_VAT_ID --><tr><td align="left"><b>Kunden-UstID:&nbsp;</b></td><td align="left">{###CUSTOMER_VAT_ID###}</td></tr><!-- EOF_CUSTOMER_VAT_ID -->
		<!-- BOF_CREATION_DATE --><tr><td align="left"><b>Datum:&nbsp;</b></td><td align="left">{###CREATION_DATE###}</td></tr><!-- EOF_CREATION_DATE -->		
		<!-- BOF_CUSTOMERS_SALESMAN --><tr><td align="left"><b>Wiederverk&auml;ufer:&nbsp;</b></td><td align="left">{###CUSTOMERS_SALESMAN###}</td></tr><!-- EOF_CUSTOMERS_SALESMAN -->
		<!-- BOF_SALESMANS_CUSTOMER --><tr><td align="left"><b>Kunde:&nbsp;</b></td><td align="left">{###SALESMANS_CUSTOMER###}</td></tr><!-- EOF_SALESMANS_CUSTOMER -->
	';
	
	if($_REQUEST["customersSource"] == 'suppliers'){
		$arrDocumentDatas['BR'] .= '
			<!-- BOF_OUR_SUPPLIERS_CUSTOMER_NUMBER --><tr><td align="left"><b>Unsere Kundennr.:&nbsp;</b></td><td align="left">{###OUR_SUPPLIERS_CUSTOMER_NUMBER###}</td></tr><!-- EOF_OUR_SUPPLIERS_CUSTOMER_NUMBER -->		
		';
	}
	// EOF BR
	
	###################################################

	// BOF PR
	$arrContentDatas['PR'] = array(
		'SALUTATION_TEXT' => '',
		'INTRO_TEXT' => '',
		'EDITOR_TEXT' => '',
		'PAYMENT_CONDITIONS' => '',
		'OUTRO_TEXT' => '<p><b>Die MwSt. darf nicht geltend gemacht werden!</b></p>',
		'REGARDS_TEXT' => '<p>Mit freundlichen Gr&uuml;&szlig;en,<br><b><i>Ihr Burhan CTR Team</i></b></p>',
		'BANK_ACCOUNT_WARNING' => '',
		'FOOTER_NOTICE' => '',
	);
	$arrDocumentDatas['PR'] = '		
		<!-- BOF_DOCUMENT_NUMBER --><tr><td align="left"><b>Provisionsnummer:&nbsp;</b></td><td align="left">{###DOCUMENT_NUMBER###}</td></tr><!-- EOF_DOCUMENT_NUMBER -->
		<!-- BOF_CREATION_DATE --><tr><td align="left"><b>Datum:&nbsp;</b></td><td align="left">{###CREATION_DATE###}</td></tr><!-- EOF_CREATION_DATE -->
		<!-- BOF_CUSTOMER_NUMBER --><tr><td align="left"><b>Kundennummer:&nbsp;</b></td><td align="left">{###CUSTOMER_NUMBER###}</td></tr><!-- EOF_CUSTOMER_NUMBER -->		
	';
	// EOF PR

	###################################################
	
	// BOF VB
	$arrContentDatas['VB'] = array(
		'SALUTATION_TEXT' => '<p>Sehr geehrte Kundin, sehr geehrter Kunde,</p>',
		'INTRO_TEXT' => '<p>wie vereinbart, geht Ihre Bestellung mit {###SHIPPING_TYPE###} per {###PAYMENT_TYPE###} in den Versand.</p>',
		'EDITOR_TEXT' => '',
		'PAYMENT_CONDITIONS' => '',
		'OUTRO_TEXT' => '<p>Bitte seien Sie <b>morgen und &uuml;bermorgen vor Ort</b> und <b>halten Sie den Betrag &uuml;ber {###CARD_TOTAL_COMPLETE_PRICE_VALUE###} Euro</b> <br>f&uuml;r die Zahlung beim Paketdienst bereit.</p><p>Vielen Dank im Voraus.</p>',
		'REGARDS_TEXT' => '<p>Mit freundlichen Gr&uuml;&szlig;en,<br><b><i>Ihr Burhan CTR Team</i></b></p>',
		'BANK_ACCOUNT_WARNING' => '',
		'FOOTER_NOTICE' => '',
	);

	$arrDocumentDatas['VB'] = '
		<!-- BOF_DOCUMENT_NUMBER --><tr><td align="left"><b>Rechnungsnummer:&nbsp;</b></td><td align="left">{###DOCUMENT_NUMBER###}</td></tr><!-- EOF_DOCUMENT_NUMBER -->
		<!-- BOF_DOCUMENT_NUMBER_AB --><tr><td align="left"><b>Auftragsnummer:&nbsp;</b></td><td align="left">{###DOCUMENT_NUMBER_AB###}</td></tr><!-- EOF_DOCUMENT_NUMBER_AB -->
		<!-- BOF_CUSTOMER_NUMBER --><tr><td align="left"><b>Kundennummer:&nbsp;</b></td><td align="left">{###CUSTOMER_NUMBER###}</td></tr><!-- EOF_CUSTOMER_NUMBER -->
		<!-- BOF_CUSTOMER_VAT_ID --><tr><td align="left"><b>Kunden-UstID:&nbsp;</b></td><td align="left">{###CUSTOMER_VAT_ID###}</td></tr><!-- EOF_CUSTOMER_VAT_ID -->
		<!-- BOF_CREATION_DATE --><tr><td align="left"><b>Datum:&nbsp;</b></td><td align="left">{###CREATION_DATE###}</td></tr><!-- EOF_CREATION_DATE -->			
		<!-- BOF_LINE_DISTANCE --><tr><td colspan="2"></td></tr><!-- EOF_LINE_DISTANCE -->
		<!-- BOF_CUSTOMERS_ORDER_NUMBER --><tr><td align="left"><b>Ihre Auftrags-Nr./AZ:&nbsp;</b></td><td align="left">{###CUSTOMERS_ORDER_NUMBER###}</td></tr><!-- EOF_CUSTOMERS_ORDER_NUMBER -->
		<!-- BOF_ORDER_DATE --><tr><td align="left"><b>Ihr Auftrag vom:&nbsp;</b></td><td align="left">{###ORDER_DATE###}</td></tr><!-- EOF_ORDER_DATE -->
		<!-- BOF_CUSTOMERS_SALESMAN --><tr><td align="left"><b>Wiederverk&auml;ufer:&nbsp;</b></td><td align="left">{###CUSTOMERS_SALESMAN###}</td></tr><!-- EOF_CUSTOMERS_SALESMAN -->
		<!-- BOF_SALESMANS_CUSTOMER --><tr><td align="left"><b>Kunde:&nbsp;</b></td><td align="left">{###SALESMANS_CUSTOMER###}</td></tr><!-- EOF_SALESMANS_CUSTOMER -->
		<!-- BOF_SHIPPING_TYPE --><tr><td align="left"><b>Versandart:&nbsp;</b></td><td align="left">{###SHIPPING_TYPE###}</td></tr><!-- EOF_SHIPPING_TYPE -->		
	';
	// EOF VB
	
	###################################################
?>