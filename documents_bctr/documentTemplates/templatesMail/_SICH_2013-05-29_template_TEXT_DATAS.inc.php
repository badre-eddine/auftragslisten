<?php
	$arrContentDatas = array();	
	
	$noticePDF = '<p>Die Auftragsbest&auml;tigung wird im PDF-Format verschickt. Die Datei k&ouml;nnen Sie mit dem Adobe Reader &ouml;ffnen und ausdrucken. Hier k&ouml;nnen Sie den Adobe Reader kostenlos herunterladen: <a href="http://www.adobe.com/products/acrobat/readstep.html">Adobe Reader</a></p>';
	$imprint = '<div style="font-size:9px;"><hr style="height: 1px; color: #999999; margin: 10px 0px 10px 0px; background-color: #999999; border: 0px solid #999999;" /><b>BURHAN Car Trade Requirement e. K.</b><br /><br /><table border="0" cellpadding="0" cellspacing="0" width="" style="font-size:9px;"><tr style="vertical-align:top;"><td>Rechnungsanschrift:</td><td>Grüner Weg 83<br />D-48268 Greven</td></tr><tr style="vertical-align:top;"><td>Lieferanschrift:</td><td>Grüner Weg 83<br />D-48268 Greven</td></tr><tr><td colspan="2"></td></tr><tr><td>Telefon:</td><td>+49 2571 / 80 00 22</td></tr><tr><td>Fax:</td><td>+49 2571 / 80 00 33</td></tr><tr><td colspan="2"></td></tr><tr style="vertical-align:top;"><td>E-Mail:</td><td><a href="mailto:info@burhan-ctr.de">info@burhan-ctr.de</a><br />für Grafiken und Angebote:<a href="mailto:grafik@burhan-ctr.de">grafik@burhan-ctr.de</a></td></tr><tr><td>Internet:</td><td><a href="http://www.burhan-ctr.de">www.burhan-ctr.de</a></td></tr></table><p>Sitz der Gesellschaft: 48268 Greven<br />Registergericht: Amtsgericht Steinfurt<br />Eintragungsnummer HRA 5629<br />Inhaberin: Fr. Döne Burhan<br />USt-IdNr.: DE 213 816 488<br />StNr.: 327 / 5031 / 1033</p></div>';
	$disclaimer = '<div><font size="1" face="Verdana, Arial, Helvetica, sans-serif"><br /><b>Beanstandungen / Reklamation</b><br /><br />Beanstandungen müssen uns sofort nach Ankunft der Ware schriftlich angezeigt werden. Gleichzeitig ist der beanstandete Artikel frei zurückzusenden.Bei berechtigter und fristgerechter Mängelrüge kann Wandlung, nicht aber Minderung oder Schadenersatz verlangt werden. Qualitäts- und Quantitätsmängel sind grundsätzlich umgehend, spätestens aber bis 5 Tage nach Erhalt schriftlich anzuzeigen.Die Reklamation ist durch jeweils 5 Musterexemplare zu belegen.<br /><br />Maßgebend für die Wirksamkeit der Mängelrüge ist in jedem Fall der Zeitpunkt des Zugangs. Ein Verzicht auf den Einwand der verspäteten Mängelrüge liegt auch dann nicht vor, wenn wir zunächst der Mängelrüge nachgehen und wegen einer Kulanzregelung korrespondieren. Sollte eine Reklamation durch die Mangelhaftigkeit einer Lieferung wirksam werden, so haben wir ein Recht auf Nachbesserung innerhalb einer angemessenen und zumutbaren Frist. Üblicherweise gilt auch hier eine Frist von 90 Tagen ab Feststellung. Sollte die Nachbesserung nach Ablauf der 90 Tage nicht zu einer einwandfreien Lieferung führen, so hat der Kunde das Recht auf Minderung, Wandlung bzw. Rücktritt.<br /><br />Wir behalten uns das Recht vor, eine Nachbesserung abzulehnen und anstatt die Druckkosten teils zu erstatten, sollte eine Nachbesserung mit unverhältnismäßig hohen Kosten im Vergleich zur Verbesserung des Druckresultats verbunden sein. Ein Anspruch auf Schadensersatz, auch wegen positiver Vertragsverletzung, wegen Minderung, Wandlung oder Rücktritt ist nicht möglich. Generell ist bei Werbeartikeln mit einem Ausschuss in Höhe von 3-5% zu rechnen.<br /></font></div>';
	$copyright = '<div style="font-size:10px;color:#666666;"><p>**Diese E-Mail enthält vertrauliche und/oder rechtlich geschützte Informationen. Wenn Sie nicht der richtige Adressat sind oder diese E-Mail irrtümlich erhalten haben, informieren Sie bitte sofort den Absender und vernichten Sie diese Mail. Das unerlaubte Kopieren sowie die unbefugte Weitergabe dieser Mail ist nicht gestattet. Über das Internet versandte E-Mails können leicht unter fremden Namen erstellt oder manipuliert werden. Aus diesem Grunde bitten wir um Verständnis, dass wir zu Ihrem und unserem Schutz die rechtliche Verbindlichkeit der vorstehenden Erklärungen und Äußerungen ausschließen. Deshalb ist der Inhalt der E-Mail nur rechtsverbindlich, wenn er unsererseits durch einen Brief entsprechend bestätigt wird.</p><p>**This e-mail may contain confidential and/or privileged information. If you are not the intended recipient or have received this e-mail in error, please notify the sender immediately and destroy this e-mail. Any unauthorized copying, disclosure or distribution of the material in this e-mail is strictly forbidden. E-mails via Internet can easily be prepared or manipulated by third persons. For this reason we trust you will understand that, for your own and our protection, we rule out the legal validity of the foregoing statements and comments. The content of this e-mail is thus only legally binding if it is confirmed by us in a letter.</p></div>';
	
	// BOF AN
	$arrContentDatas['AN'] = array(
		'MAIL_HEADER_IMAGE' => '<img src="mailHeader.jpg" width="586" height="76" alt="BURHAN Car Trade Requirement e.K." title="BURHAN Car Trade Requirement e.K." />',
		'MAIL_TITLE' => preg_replace('/{###DOCUMENT_NUMBER###}/', $generatedDocumentNumber, $_POST["selectSubject"]).'',
		'MAIL_DATE_VALUE' => '',
		'MAIL_SALUTATION' => 'Sehr geehrte Kundin, sehr geehrter Kunde,',
		'MAIL_CONTENT' => '<p>Wir danken Ihnen f&uuml;r Ihre Anfrage.</p><p>Im Anhang finden Sie unser Angebot <b>{###DOCUMENT_NUMBER###}</b>.</p>' . $noticePDF,
		'MAIL_CONTENT_ADDITIONAL' => '',
		'MAIL_REGARDS' => 'Mit freundlichen Gr&uuml;&szlig;en,<br><b><i>Ihr Burhan CTR Team</i></b>',
		'MAIL_SIGNATURE' => '' . $imprint,	
		'MAIL_COPYRIGHT' => $copyright
	);	
	// EOF AN

	###################################################

	// BOF AB
	$arrContentDatas['AB'] = array(
		'MAIL_HEADER_IMAGE' => '<img src="mailHeader.jpg" width="586" height="76" alt="BURHAN Car Trade Requirement e.K." title="BURHAN Car Trade Requirement e.K." />',
		'MAIL_TITLE' => preg_replace('/{###DOCUMENT_NUMBER###}/', $generatedDocumentNumber, $_POST["selectSubject"]).'',	
		'MAIL_DATE_VALUE' => '',
		'MAIL_SALUTATION' => 'Sehr geehrte Kundin, sehr geehrter Kunde,',
		'MAIL_CONTENT' => '<p>Wir danken Ihnen f&uuml;r Ihre Auftrag.</p><p>Im Anhang finden Sie unsere Auftragsbest&auml;tigung <b>{###DOCUMENT_NUMBER###}</b>.</p>' . $noticePDF,
		'MAIL_CONTENT_ADDITIONAL' => '',
		'MAIL_REGARDS' => 'Mit freundlichen Gr&uuml;&szlig;en,<br><b><i>Ihr Burhan CTR Team</i></b>',
		'MAIL_SIGNATURE' => '' . $imprint,	
		'MAIL_COPYRIGHT' => $copyright
	);
	// EOF AB

	###################################################

	// BOF LS
	$arrContentDatas['LS'] = array(
		'MAIL_HEADER_IMAGE' => '<img src="mailHeader.jpg" width="586" height="76" alt="BURHAN Car Trade Requirement e.K." title="BURHAN Car Trade Requirement e.K." />',
		'MAIL_TITLE' => preg_replace('/{###DOCUMENT_NUMBER###}/', $generatedDocumentNumber, $_POST["selectSubject"]).'',
		'MAIL_DATE_VALUE' => '',
		'MAIL_SALUTATION' => 'Sehr geehrte Kundin, sehr geehrter Kunde,',
		'MAIL_CONTENT' => '<p>Wir danken Ihnen f&uuml;r Ihren Auftrag.</p><p>Im Anhang finden Sie den Lieferschein<b>{###DOCUMENT_NUMBER###}</b>.</p>' . $noticePDF,
		'MAIL_CONTENT_ADDITIONAL' => '',
		'MAIL_REGARDS' => 'Mit freundlichen Gr&uuml;&szlig;en,<br><b><i>Ihr Burhan CTR Team</i></b>',
		'MAIL_SIGNATURE' => '' . $imprint,	
		'MAIL_COPYRIGHT' => $copyright
	);
	// EOF LS

	###################################################

	// BOF RE
	$arrContentDatas['RE'] = array(
		'MAIL_HEADER_IMAGE' => '<img src="mailHeader.jpg" width="586" height="76" alt="BURHAN Car Trade Requirement e.K." title="BURHAN Car Trade Requirement e.K." />',
		'MAIL_TITLE' => preg_replace('/{###DOCUMENT_NUMBER###}/', $generatedDocumentNumber, $_POST["selectSubject"]).'',
		'MAIL_DATE_VALUE' => '',
		'MAIL_SALUTATION' => 'Sehr geehrte Kundin, sehr geehrter Kunde,',
		'MAIL_CONTENT' => '<p>Wir danken Ihnen f&uuml;r Ihren Auftrag.</p><p>Im Anhang finden Sie unsere Rechnung<b>{###DOCUMENT_NUMBER###}</b>.</p>' . $noticePDF,
		'MAIL_CONTENT_ADDITIONAL' => '',
		'MAIL_REGARDS' => 'Mit freundlichen Gr&uuml;&szlig;en,<br><b><i>Ihr Burhan CTR Team</i></b>',
		'MAIL_SIGNATURE' => '' . $imprint,	
		'MAIL_COPYRIGHT' => $copyright
	);
	
	// EOF RE

	###################################################

	// BOF MA
	$arrContentDatas['MA'] = array(
		'MAIL_HEADER_IMAGE' => '<img src="mailHeader.jpg" width="586" height="76" alt="BURHAN Car Trade Requirement e.K." title="BURHAN Car Trade Requirement e.K." />',
		'MAIL_TITLE' => preg_replace('/{###DOCUMENT_NUMBER###}/', $generatedDocumentNumber, $_POST["selectSubject"]).'',
		'MAIL_DATE_VALUE' => '',
		'MAIL_SALUTATION' => 'Sehr geehrte Kundin, sehr geehrter Kunde,',
		'MAIL_CONTENT' => '<p>Wir danken Ihnen f&uuml;r Ihre Auftrag.</p><p>Im Anhang finden Sie die Zahlungserinnerung <b>{###DOCUMENT_NUMBER###}</b>.</p>' . $noticePDF,
		'MAIL_CONTENT_ADDITIONAL' => '',
		'MAIL_REGARDS' => 'Mit freundlichen Gr&uuml;&szlig;en,<br><b><i>Ihr Burhan CTR Team</i></b>',
		'MAIL_SIGNATURE' => '' . $imprint,	
		'MAIL_COPYRIGHT' => $copyright
	);
	// EOF MA

	###################################################

	// BOF M1
	$arrContentDatas['M1'] = array(
		'MAIL_HEADER_IMAGE' => '<img src="mailHeader.jpg" width="586" height="76" alt="BURHAN Car Trade Requirement e.K." title="BURHAN Car Trade Requirement e.K." />',
		'MAIL_TITLE' => preg_replace('/{###DOCUMENT_NUMBER###}/', $generatedDocumentNumber, $_POST["selectSubject"]).'',
		'MAIL_DATE_VALUE' => '',
		'MAIL_SALUTATION' => 'Sehr geehrte Kundin, sehr geehrter Kunde,',
		'MAIL_CONTENT' => '<p>Wir danken Ihnen f&uuml;r Ihre Auftrag.</p><p>Im Anhang finden Sie die 1. Mahnung <b>{###DOCUMENT_NUMBER###}</b>.</p>' . $noticePDF,
		'MAIL_CONTENT_ADDITIONAL' => '',
		'MAIL_REGARDS' => 'Mit freundlichen Gr&uuml;&szlig;en,<br><b><i>Ihr Burhan CTR Team</i></b>',
		'MAIL_SIGNATURE' => '' . $imprint,	
		'MAIL_COPYRIGHT' => $copyright
	);
	// EOF M1

	###################################################

	// BOF M2
	$arrContentDatas['M2'] = array(
		'MAIL_HEADER_IMAGE' => '<img src="mailHeader.jpg" width="586" height="76" alt="BURHAN Car Trade Requirement e.K." title="BURHAN Car Trade Requirement e.K." />',
		'MAIL_TITLE' => preg_replace('/{###DOCUMENT_NUMBER###}/', $generatedDocumentNumber, $_POST["selectSubject"]).'',
		'MAIL_DATE_VALUE' => '',
		'MAIL_SALUTATION' => 'Sehr geehrte Kundin, sehr geehrter Kunde,',
		'MAIL_CONTENT' => '<p>Wir danken Ihnen f&uuml;r Ihre Auftrag.</p><p>Im Anhang finden Sie die 2. Mahnung <b>{###DOCUMENT_NUMBER###}</b>.</p>' . $noticePDF,
		'MAIL_CONTENT_ADDITIONAL' => '',
		'MAIL_REGARDS' => 'Mit freundlichen Gr&uuml;&szlig;en,<br><b><i>Ihr Burhan CTR Team</i></b>',
		'MAIL_SIGNATURE' => '' . $imprint,	
		'MAIL_COPYRIGHT' => $copyright
	);
	// EOF M2

	###################################################

	// BOF GU
	$arrContentDatas['GU'] = array(
		'MAIL_HEADER_IMAGE' => '<img src="mailHeader.jpg" width="586" height="76" alt="BURHAN Car Trade Requirement e.K." title="BURHAN Car Trade Requirement e.K." />',
		'MAIL_TITLE' => preg_replace('/{###DOCUMENT_NUMBER###}/', $generatedDocumentNumber, $_POST["selectSubject"]).'',
		'MAIL_DATE_VALUE' => '',
		'MAIL_SALUTATION' => 'Sehr geehrte Kundin, sehr geehrter Kunde,',
		'MAIL_CONTENT' => '<p>Wir danken Ihnen f&uuml;r Ihre Auftrag.</p><p>Im Anhang finden Sie die Gutschrift <b>{###DOCUMENT_NUMBER###}</b>.</p>' . $noticePDF,
		'MAIL_CONTENT_ADDITIONAL' => '',
		'MAIL_REGARDS' => 'Mit freundlichen Gr&uuml;&szlig;en,<br><b><i>Ihr Burhan CTR Team</i></b>',
		'MAIL_SIGNATURE' => '' . $imprint,	
		'MAIL_COPYRIGHT' => $copyright
	);
	// EOF GU

	###################################################

	// BOF BR
	$arrContentDatas['BR'] = array(
		'MAIL_HEADER_IMAGE' => '<img src="mailHeader.jpg" width="586" height="76" alt="BURHAN Car Trade Requirement e.K." title="BURHAN Car Trade Requirement e.K." />',
		'MAIL_TITLE' => preg_replace('/{###DOCUMENT_NUMBER###}/', $generatedDocumentNumber, $_POST["selectSubject"]).'',
		'MAIL_DATE_VALUE' => '',
		'MAIL_SALUTATION' => 'Sehr geehrte Kundin, sehr geehrter Kunde,',
		'MAIL_CONTENT' => '<p>Wir danken Ihnen f&uuml;r Ihre Anfrage.</p><p>Im Anhang finden Sie unser Dokument<b> {###DOCUMENT_NUMBER###}</b>.</p>' . $noticePDF,
		'MAIL_CONTENT_ADDITIONAL' => '',
		'MAIL_REGARDS' => 'Mit freundlichen Gr&uuml;&szlig;en,<br><b><i>Ihr Burhan CTR Team</i></b>',
		'MAIL_SIGNATURE' => '' . $imprint,	
		'MAIL_COPYRIGHT' => $copyright
	);
	// EOF BR
	
	###################################################

	// BOF KA
	$arrContentDatas['KA'] = array(
		'MAIL_HEADER_IMAGE' => '<img src="mailHeader.jpg" width="586" height="76" alt="BURHAN Car Trade Requirement e.K." title="BURHAN Car Trade Requirement e.K." />',
		'MAIL_TITLE' => 'Korrekturabzug ' . preg_replace('/{###DOCUMENT_NUMBER###}/', $generatedDocumentNumber, $_POST["selectSubject"]).'',
		'MAIL_DATE_VALUE' => '',
		'MAIL_SALUTATION' => 'Sehr geehrte Kundin, sehr geehrter Kunde,',
		'MAIL_CONTENT' => '<p>Wir danken Ihnen f&uuml;r Ihre Anfrage.</p><p>Im Anhang finden Sie unser Dokument<b> {###DOCUMENT_NUMBER###}</b>.</p>' . $noticePDF,
		'MAIL_CONTENT_ADDITIONAL' => '',
		'MAIL_REGARDS' => 'Mit freundlichen Gr&uuml;&szlig;en,<br><b><i>Ihr Burhan CTR Team</i></b>',
		'MAIL_SIGNATURE' => '' . $imprint,	
		'MAIL_COPYRIGHT' => $copyright
	);
	// EOF KA
	
	###################################################
	
	// BOF EX
	$arrContentDatas['EX'] = array(
		'MAIL_HEADER_IMAGE' => '<img src="mailHeader.jpg" width="586" height="76" alt="BURHAN Car Trade Requirement e.K." title="BURHAN Car Trade Requirement e.K." />',
		'MAIL_TITLE' => 'Datenexport',
		'MAIL_DATE_VALUE' => '',
		'MAIL_SALUTATION' => 'Sehr geehrte Damen und Herren,',
		'MAIL_CONTENT' => '',
		'MAIL_CONTENT_ADDITIONAL' => '',
		'MAIL_REGARDS' => 'Mit freundlichen Gr&uuml;&szlig;en,<br><b><i>Ihr Burhan CTR Team</i></b>',
		'MAIL_SIGNATURE' => '' . $imprint,	
		'MAIL_COPYRIGHT' => $copyright
	);
	// EOF EX
	
	###################################################
	
	// BOF SX
	$arrContentDatas['SX'] = array(
		'MAIL_HEADER_IMAGE' => '<img src="mailHeader.jpg" width="586" height="76" alt="BURHAN Car Trade Requirement e.K." title="BURHAN Car Trade Requirement e.K." />',
		'MAIL_TITLE' => 'Datenexport',
		'MAIL_DATE_VALUE' => '',
		'MAIL_SALUTATION' => 'Sehr geehrte Damen und Herren,',
		'MAIL_CONTENT' => '',
		'MAIL_CONTENT_ADDITIONAL' => '',
		'MAIL_REGARDS' => 'Mit freundlichen Gr&uuml;&szlig;en,<br><b><i>Ihr Burhan CTR Team</i></b>',
		'MAIL_SIGNATURE' => '' . $imprint,	
		'MAIL_COPYRIGHT' => $copyright
	);
	// EOF SX
	
	###################################################
?>