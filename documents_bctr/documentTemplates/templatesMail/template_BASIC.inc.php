<?php
	$mailTemplate = array();

	// BOF ADD STYLE SHEETS
		// SET VALUES FOR html_MAIL
			$mailFontSize = "12px";
			$mailFontFamily = "Arial, Helvetica, sans-serif";
			$mailFontColor = "#000000";
			$mailBackgroundColor = "#FFFFFF";
			$mailBorder = "1px solid #CCCCCC";
			$mailBasketBorder = "1px solid #CCCCCC";
			$mailTextDecoration = "text-decoration:none;";


		$styleDatasDiv = 'style="width: 592px; margin: 0 0 10px 0; padding: 0; background-color:'.$mailBackgroundColor.'; font-family:'.$mailFontFamily.'; font-size:'.$mailFontSize.'; color:'.$mailFontColor.';"';
		
		$styleDatasHr = 'style="height: 1px; color: #CCCCCC; margin: 10px 0px 10px 0px; background-color: #CCCCCC; border: 0px solid #CCCCCC;"';
		$styleDatasH1 = 'style="color:#000000; font-weight:bold; font-size:12px; margin:6px 0 15px 0; padding:0px; width:auto; border-bottom:1px #CCCCCC solid; padding-bottom:8px;"';	
		$styleDatasFont = 'style="font-family:'.$mailFontFamily.'; font-size:'.$mailFontSize.'; color:#'.$mailFontColor.';"';
		$styleDatasTable = 'style="border: 1px solid #CCCCCC; border-collapse: collapse; empty-cells: show;";';
	// EOF ADD STYLE SHEETS


	$mailTemplate = array();

	$mailTemplate["html"] = '
			<div '.$styleDatasDiv.'>		
				<div style="border:'.$mailBorder.'; padding: 2px; margin: 0">
					<p style="margin:0; padding:0;">{###MAIL_HEADER_IMAGE###}</p>				
					<div id="content" style="margin:0;padding:0">
						<div style="text-align:right; font-size:'.$mailFontSize.';"><b>Datum</b>: {###MAIL_DATE_VALUE###}</div>
						<h1 '.$styleDatasH1.'>{###MAIL_TITLE###}</h1>						
						<div '.$styleDatasFont.'>
							<p><b>{###MAIL_SALUTATION###}</b></p>
							{###MAIL_CONTENT###}
							<p>{###MAIL_REGARDS###}</p>				   
						</div>
					</div>	
					<br /><br />
					{###MAIL_SIGNATURE###}				
					<hr '.$styleDatasHr.' />
					{###MAIL_COPYRIGHT###}	
				</div>
			</div>
	';
?>