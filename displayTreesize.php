<?php
	require_once('inc/requires.inc.php');

	if(!$arrGetUserRights["serverInfo"]) {
		header('location: ' . PAGE_EXIT_LOCATION);
		exit;
	}

	$jswindowMessage = "";
	$warningMessage = '';
	$errorMessage = '';
	$successMessage = '';
	$infoMessage = '';

	$dbConnection = new DB_Connection();
	$db_open = $dbConnection->db_connect();

	$userDatas = getUserDatas();

?>
<?php
	require_once('inc/headerHTML.inc.php');
	$thisTitle = "Datei-Verzeichnisbaum";
	$headerHTML = preg_replace("/{###TITLE###}/", strip_tags($thisTitle), $headerHTML);
	echo $headerHTML;

	require_once(FILE_MENUE_TOP);
?>
<div id="mainArea">
	<div id="mainContent">
		<?php require_once(FILE_MENUE_SIDEBAR); ?>
		<div id="contentArea">
			<div id="contentAreaElements">
				<h1><?php if(SHOW_PAGE_TITLE_ICONS) { echo '<img src="' . PATH_ICONS_MENUE_TITLES . 'sitemap.png" alt="" />'; } ?> <?php echo $thisTitle; ?></h1>

				<div class="contentDisplay">
					<?php displayMessages(); ?>

					<?php
						// if($_GET['directory'] != '') {$directory = $_SERVER['DOCUMENT_ROOT'] . '/' . DIR_INSTALLATION_NAME . preg_replace("/" . preg_replace("/\//", "", (DIR_INSTALLATION_NAME)) . "/", "", preg_replace("/\//", "", $_GET['directory'])); }
						if($_GET['directory'] != '' && $_GET['directory'] != BASEPATH && $_GET['directory'] != '/') {
							$directory = BASEPATH . $_GET['directory'];							
							$directory = preg_replace("/[\/]{2,}/", "/", $directory);							
							$directory .=  "/";
						}						
						else {							
							$directory = BASEPATH;
						}

						function dirsize($path) {
							$s = 0;
							$result[$path] = 0;
							$handle = @opendir($path);
							if ($handle) {
								while (false !== ($file = readdir($handle))) {
									if ($file != "." && $file != "..") {
										$name = $path . "/" . $file;
										if (is_dir($name)) {
											$ar = dirsize($name);
											while (list($key, $value) = each ($ar)) {$s++; $result[$key] = $value;}
										}
										else {$result[$path] += @filesize($name);}
									}
								}
								closedir($handle);
							}
							return $result;
						}
						$data = dirsize($directory);
						$data1 = $data;

						while (list($key, $value) = each ($data)) {
							if (!preg_match("'\/'",str_replace($directory.'/','',$key))) {
								$verzeichnis[str_replace($directory.'/','',$key)] = $value;
								$dir1 = str_replace($directory.'/','',$key);
								while (list($key1, $value1) = each ($data1)) {
									$key2 = str_replace($directory.'/','',$key1);
									$subdir = explode('/',$key2);
									if (preg_match("'\/'",str_replace($directory.'/','',$key1)) AND $dir1 == $subdir[0]) {
										$verzeichnis[$subdir[0]] += $value1;
									}
								}
								reset($data1);
							}
						}
						@reset($data);
						@arsort($verzeichnis);
						@reset($verzeichnis);
						$handle1 = @opendir($directory);

						if ($handle1) {
							while (false !== ($file = readdir($handle1))) {
								if ($file != "." && $file != "..") {
									$name = $directory . "/" . $file;
									if (!is_dir($name)) {$Dateien[$file] = @filesize($name);}
								}
							}
							closedir($handle1);
						}
						@arsort($Dateien);
						@reset($Dateien);
					?>

					<h2>Aktueller Pfad: <?php echo str_replace('//','/',$directory); ?></h2>

					<?php
						if ($directory != BASEPATH) {
							$arrDirParts = explode("/",  $directory);
							$backDirIndex = (count($arrDirParts) - 3);
							$directory_new = "/" . $arrDirParts[$backDirIndex];
							$directory_new .= "/";							
							$directory_linkText = $directory_new;
							if(preg_match("/" . preg_quote(DIR_INSTALLATION_NAME, "/") . "$/", $directory_new)){
								$directory_new = "/";
							}
							if($backDirIndex > 0) {
								?>					
								<p><a href="<?php echo $_SERVER['PHP_SELF']; ?>?directory=<?php echo $directory_new; ?>">zur&uuml;ck nach: <?php echo $directory_linkText ?></a></p>
								<hr />
								<?php
							}
						}
					?>

					<table border="0" cellpadding="0" cellspacing="0" class="displayOrders">
						<tr>
							<th>Ordner-Name</th>
							<th style="width:200px;">Ordner-Gr&ouml;&szlig;e</th>
						</tr>
						<?php
							$count = 0;
							while (list($key, $value) = @each ($verzeichnis)) {
								$Summe += $value;
								if($count%2 == 0){ $rowClass = 'row0'; }
								else { $rowClass = 'row1'; }
						?>
						<tr class="<?php echo $rowClass; ?>">
							<td>
								<a href="<?php echo $_SERVER['PHP_SELF']; ?>?directory=<?php echo $_GET['directory']; ?>/<?php echo str_replace($_SERVER['DOCUMENT_ROOT'].'/','',$key); ?>">
									<?php echo str_replace($directory,'',$key); ?>
								</a>
							</td>
							<td style="text-align:right;"><?php echo number_format($value / 1024, 3, ",", "."); ?> kB</td>
						</tr>
						<?php
								$count++;
							}
						?>
					</table>
					<hr />
					<table border="0" cellpadding="0" cellspacing="0" class="displayOrders">
						<tr>
							<th>Datei-Name</th>
							<th style="width:200px;">Datei-Gr&ouml;&szlig;e</th>
						</tr>
						<?php
							$count = 0;
							while (list($key, $value) = @each ($Dateien)) {
								$Summe1 += $value;
								if($count%2 == 0){ $rowClass = 'row0'; }
								else { $rowClass = 'row1'; }
						?>
						<tr>
							<td><?php echo htmlentities(utf8_encode($key)); ?></td>
							<td style="text-align:right;"><?php echo number_format($value / 1024, 3, ",", "."); ?> kB</td>
						</tr>
						<?php
								$count++;
							}
						?>
					</table>
					<hr />
					<table border="0" cellpadding="0" cellspacing="0" class="displayOrders">
						<tr>
							<td><b>Summe Dateien: </b></td>
							<td style="text-align:right; width:200px;"><?php echo number_format($Summe1 / 1024, 3, ",", "."); ?> kB</td>
						</tr>
						<tr>
							<td><b>Summe Verzeichnis: </b></td>
							<td style="text-align:right;"><?php echo number_format(($Summe + $Summe1) / 1024 / 1024, 3, ",", "."); ?> MB</td>
						</tr>
					</table>
				</div>
			</div>
		</div>
		<div class="clear"></div>
	</div>
</div>
<?php
	if($arrGetUserRights["editPersonnel"]) {
?>
<script language="javascript" type="text/javascript">
	$(document).ready(function() {
		$(function() {
			$( "#datepicker" ).datepicker();
			$.datepicker.setDefaults($.datepicker.regional[""]);
			$('#editPersonnelVacationsStart').datepicker($.datepicker.regional["de"]);
			$('#editPersonnelVacationsEnd').datepicker($.datepicker.regional["de"]);
			$('#editPersonnelJobStart').datepicker($.datepicker.regional["de"]);
			$('#editPersonnelJobEnd').datepicker($.datepicker.regional["de"]);
			//$('#editPersonnelBirthday').datepicker($.datepicker.regional["de"]);
			$('#editPersonnelDiseaseStart').datepicker($.datepicker.regional["de"]);
			$('#editPersonnelDiseaseEnd').datepicker($.datepicker.regional["de"]);
			$('#editPersonnelOvertimeDate').datepicker($.datepicker.regional["de"]);
		});
		$(function() {
			$('#tabs').tabs();
		});
		setFormDutyFields(<?php echo $jsonFormDutyFields; ?>);
		colorRowMouseOver('.displayOrders tbody tr');

		$('#editPersonnelZipcode').keyup(function () {
			loadSuggestions('searchPlzCity', [{'triggerElement': '#editPersonnelZipcode', 'fieldCity': '#editPersonnelCity', 'fieldZipCode': '#editPersonnelZipcode'}], 1);
		});
		$('#editPersonnelCity').keyup(function () {
			loadSuggestions('searchCityPlz', [{'triggerElement': '#editPersonnelCity', 'fieldCity': '#editPersonnelCity', 'fieldZipCode': '#editPersonnelZipcode'}], 1);
		});

		$('#editPersonnelBankName').keyup(function(){
			loadSuggestions('searchBankName', [{'triggerElement': '#editPersonnelBankName', 'fieldCode': '#editPersonnelBankLeitzahl', 'fieldName': '#editPersonnelBankName', 'fieldBIC': '#editPersonnelBankBIC', 'fieldIBAN': '#editPersonnelBankIBAN'}], 1);
		});
		$('#editPersonnelBankLeitzahl').keyup(function(){
			loadSuggestions('searchBankCode', [{'triggerElement': '#editPersonnelBankLeitzahl', 'fieldCode': '#editPersonnelBankLeitzahl', 'fieldName': '#editPersonnelBankName', 'fieldBIC': '#editPersonnelBankBIC', 'fieldIBAN': '#editPersonnelBankIBAN'}], 1);
		});
	});
</script>
<?php
	}
?>
<?php require_once('inc/footerHTML.inc.php'); ?>