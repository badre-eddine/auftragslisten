<?php
	require_once('inc/requires.inc.php');

	if(!$arrGetUserRights["exportOrders"]) {
		header('location: ' . PAGE_EXIT_LOCATION);
		exit;
	}

	if($_GET["downloadFile"] != "") {
		require_once('classes/downloadFile.class.php');
		$thisDownload = new downloadFile(DIRECTORY_CREATED_DOCUMENTS_SALESMEN, $_GET["downloadFile"]);
		$errorMessage .= $thisDownload->startDownload();
	}

	$jswindowMessage = '';
	$warningMessage = '';
	$errorMessage = '';
	$successMessage = '';
	$infoMessage = '';

	$dbConnection = new DB_Connection();
	$db_open = $dbConnection->db_connect();

	// BOF SEND MAIL WITH ATTACHED DOCUMENT
	if($_POST["sendAttachedDocument"] == '1') {
		$thisCreatedPdfName = $_POST["mailDocumentFilename"];
		// $_POST["selectSubject"] = $arrDocumentTypeDatas[$_REQUEST["documentType"]]["createdDocumentsTypesName"] . ' Nr.: ' . '{###DOCUMENT_NUMBER###}';
		$_POST["selectSubject"] = 'Datenexport: ' . $thisCreatedPdfName;
		$generatedDocumentNumber = $_POST["mailDocumentFilename"];

		require_once(DIRECTORY_HTML_MIME_MAIL . 'htmlMimeMail.php');
		require_once(DIRECTORY_MAIL_TEMPLATES . 'template_BASIC.inc.php');
		require_once(DIRECTORY_MAIL_TEMPLATES . 'template_TEXT_DATAS.inc.php');
		#require_once("inc/mail.inc.php");

		// BOF SEND MAIL
		require_once("classes/createMail.class.php");

		// BOF CREATE SUBJECT
		$thisSubject = $_POST["selectSubject"];
		if($_POST["sendAttachedMailSubject"] != '') {
			$thisSubject = $_POST["sendAttachedMailSubject"] . ', ' . $thisSubject;
		}
		// EOF CREATE SUBJECT

		// BOF GET ATTACHED FILE
		if($_REQUEST["documentType"] == 'KA') {
			$pathDocumentFolder = DIRECTORY_UPLOAD_FILES;
			$thisCreatedPdfName = utf8_decode($thisCreatedPdfName);
		}
		else if($_REQUEST["documentType"] == 'EX') {
			$pathDocumentFolder = DIRECTORY_EXPORT_FILES;
			$thisCreatedPdfName = utf8_decode($thisCreatedPdfName);
		}
		else if($_REQUEST["documentType"] == 'SX') {
			$pathDocumentFolder = DIRECTORY_CREATED_DOCUMENTS_SALESMEN;
			$thisCreatedPdfName = utf8_decode($thisCreatedPdfName);
		}
		else {
			$pathDocumentFolder = DIRECTORY_CREATED_DOCUMENTS_CUSTOMERS;
		}

		$arrAttachmentFiles = array (
			rawurldecode($pathDocumentFolder . $thisCreatedPdfName) => str_replace(" ", "_", rawurldecode(myUnHtmlEntities($thisCreatedPdfName)))
		);
		// EOF GET ATTACHED FILE

		$createMail = new createMail(
							$thisSubject,													// TEXT:	MAIL_SUBJECT
							$_POST["documentType"],											// STRING:	DOCUMENT TYPE
							$generatedDocumentNumber,										// STRING:	DOCUMENT NUMBER
							$arrAttachmentFiles,											// ARRAY:	ATTACHMENT FILES
							$_POST["selectCustomersRecipientMail"],	 										// STRING:	RECIPIENTS
							$arrMailContentDatas[$_POST["selectMailtextTemplates"]],		// MAIL_TEXT_TEMPLATE
							$mailTemplate["html"],											// MAIL_BODY_TEMPLATE
							'',																// STRING:	ADDITIONAL TEXT
							$_POST["sendAttachedMailText"],									// STRING:	SEND ATTACHED TEXT
							true,															// BOOLEAN:	SEND ATTACHED DOCUMENT
							DIRECTORY_MAIL_IMAGES,											// DIRECTORY_MAIL_IMAGES
							$_POST["selectMailtextSender"]									// STRING: SENDER
						);

		$createMailResult = $createMail->returnResult();
		$sendMailToClient = $createMailResult;
		// EOF SEND MAIL

		#if(!$sendMailToUs) { $errorMessage .= ' Die Mail-Kopie konnte nicht an ' . implode(', ', $arrOrderMail) . ' versendet werden! '.'<br />'; }
		#else { $successMessage .= ' Die Mail-Kopie wurde an ' . implode(', ', $arrOrderMail) . ' versendet! '.'<br />'; }

		if(!$sendMailToClient) { $errorMessage .= ' Die Mail konnte nicht an ' . $_POST["selectCustomersRecipientName"] . ' (' . $_POST["selectCustomersRecipientMail"] . ') versendet werden! '.'<br />'; }
		else {
			$successMessage .= ' Die Mail wurde an ' . $_POST["selectCustomersRecipientName"] . ' (' . $_POST["selectCustomersRecipientMail"] . ') versendet! '.'<br />';
		}
		unset($_POST["exportOrdersVertreterID"]);
	}
	// EOF SEND MAIL WITH ATTACHED DOCUMENT

	$defaultORDER = "ordersBestellDatum";
	$defaultSORT = "ASC";

	if($_REQUEST["ordersSort"] != "") {
		$thisORDER = $_REQUEST["ordersSort"];
	}
	else {
		$thisORDER = $defaultORDER;
	}

	if($_REQUEST["sortType"] != "") {
		$thisSORT = $_REQUEST["sortType"];
	}
	else {
		$thisSORT = $defaultSORT;
	}
	if($thisSORT == "DESC") { $thisSORT = "ASC"; }
	else { $thisSORT = "DESC"; }

	// BOF READ VERTRETER
		$arrSalesmenDatas = getSalesmen();
	// EOF READ VERTRETER

	// BOF READ BESTELL ARTEN
		$arrOrderTypeDatas = getOrderTypes();
	// EOF READ BESTELL ARTEN

	// BOF READ BESTELL STATUS TYPEN
		$arrOrderStatusTypeDatas = getOrderStatusTypes();
	// EOF READ BESTELL STATUS TYPEN
?>
<?php
	require_once('inc/headerHTML.inc.php');
	$thisTitle = "Bestellungen f&uuml;r Vertreter exportieren";
	if($_POST["exportOrdersVertreterID"] != "") {
		if($_POST["exportOrdersVertreterID"] != "NO_SALESMAN") {
			$thisTitle .= " - Vertreter ".$arrSalesmenDatas[$_POST["exportOrdersVertreterID"]]["customersFirmenname"];
		}
		else {
			$thisTitle .= " - ohne hinterlegtem Vertreter ";
		}
	}
	$headerHTML = preg_replace("/{###TITLE###}/", strip_tags($thisTitle), $headerHTML);
	echo $headerHTML;

	require_once(FILE_MENUE_TOP);

	$sqlSortField = "";
	$sqlSortDirection = "";
	$sqlWhere = "";

	if($_POST["submitExportForm"] == 1 && $_POST["exportOrdersVertreterID"] != "") {

		// BOF GET SALESMAN DATAS
			$salesmanID = '';
			$salesmanKundennummer = '';
			$salesmanVorname = '';
			$salesmanNachname = '';
			$salesmanMail = '';
			if($_POST["exportOrdersVertreterID"] != "NO_SALESMAN") {
				$sql = "SELECT
						`customersID`,
						`customersKundennummer`,
						`customersFirmenInhaberVorname`,
						`customersFirmenInhaberNachname`,
						`customersMail1`

						FROM `common_customers`

						WHERE `customersID` = " . $_POST["exportOrdersVertreterID"] . "";

				$rs = $dbConnection->db_query($sql);
				list(
						$salesmanID,
						$salesmanKundennummer,
						$salesmanVorname,
						$salesmanNachname,
						$salesmanMail
					) = mysqli_fetch_array($rs);
			}
		// BOF GET SALESMAN DATAS

		// BOF CREATE SQL FOR EXPORT ORDERS
		$sql = "SELECT
					/*
					`ordersID`,
					*/
					`ordersBestellDatum`,
					/*
					`ordersFreigabeDatum`,
					`ordersBelichtungsDatum`,
					*/
					`ordersKundennummer`,
					`ordersKundenName`,
					`ordersKommission`,
					`ordersPLZ`,
					`ordersOrt`,
					`ordersArtikelMenge`,
					/*
					`ordersSinglePreis`,
					`ordersTotalPreis`,
					*/
					`ordersArtikelBezeichnung`,
					/*
					`ordersArtikelID`,
					`ordersArtikelNummer`,
					`ordersAdditionalArtikelKategorieID`,
					*/

					`ordersAdditionalArtikelMenge`,
					`ordersDruckFarbe`,
					/*
					`ordersAdditionalCosts`,
					`ordersVertreter`,
					`ordersVertreterID`,
					*/
					`ordersMailAdress`,
					/*
					`ordersMandant`,
					`ordersLieferwoche`,
					`ordersLieferDatum`,
					`ordersDruckerName`,
					*/
					`ordersNotizen`,
					`ordersStatus`,
					`ordersOrderType`
					/*
					,
					`ordersBankAccountType`,
					`ordersPaymentType`,
					`ordersPaymentStatusType`,
					`ordersUserID`,
					`ordersTimeCreated`
					*/

					FROM `" . TABLE_ORDERS . "`

					WHERE
			";
			if($_POST["exportAllStatusOF"] == 1) {
				$sqlWhere .= " (`ordersStatus` = 1) ";
			}
			if($_POST["exportAllStatusOF"] == 1 && $_POST["exportStatusFG"] == 1) {
				$sqlWhere .= " OR ";
			}
			if($_POST["exportStatusFG"] == 1) {
				$sqlWhere .= " (
					`ordersStatus` = 2
					AND `ordersOrderType` = 1
					AND `ordersBestellDatum`
						BETWEEN '" . formatDate($_POST["exportOrdersFreigabeDatumFrom"], "store") . "' AND '" . formatDate($_POST["exportOrdersFreigabeDatumTo"], "store")."'
				) ";
			}

			if($sqlWhere != "") {
				$sqlWhere = " ( " . $sqlWhere . " ) ";
			}

			if($_POST["exportOrdersVertreterID"] == "NO_SALESMAN") {
				$sqlWhere .= " AND (
								`ordersVertreterID` = ''
								OR
								`ordersVertreterID` = 0
							 )
				";
			}
			else if($_POST["exportOrdersVertreterID"] > 0) {
				$sqlWhere .= " AND (
								`ordersVertreterID` = '" . $_POST["exportOrdersVertreterID"] ."'
								/*
								OR
								`ordersVertreter` LIKE '" . $arrSalesmenDatas[$_POST["exportOrdersVertreterID"]]["customersFirmenname"] . "'
								*/
							 )
				";
			}
			$sql .= $sqlWhere;


			$sql .= " ORDER BY `ordersBestellDatum` ASC ";
			// EOF CREATE SQL FOR EXPORT ORDERS

			$rs = $dbConnection->db_query($sql);

			$countTotalRows = $dbConnection->db_getMysqlNumRows($rs);
			$contentExportPDF = '';
			$contentExportCSV = '';

			if($countTotalRows > 0) {


			$contentExportPDF = '
				<style type="text/css">
				<!--
					table {
						border: 1pt solid #CCCCCC;
						border-collapse:collapse;
					}
					td {
						padding: 1pt;
						border: 0.5pt solid #CCCCCC;
						text-align:left;
						font-size:8pt;
						font-weight:normal;
						color:#000;
						vertical-align:top;
					}
					th {
						text-align:left;
						font-size:7pt;
						font-weight:bold;
						background-color:#DDDDDD;
						color:#000;
						padding: 1pt;
						border: 0.5pt solid #CCCCCC;
					}
					.rowColor {
						background-color:#DDD;
					}
				-->
				</style>
			';


			$contentExportPDF .= '<table width="100%">';

			$countRow = 0;
			while($ds = mysqli_fetch_assoc($rs)) {
				$thisVertreter = $ds["ordersVertreter"];
				if($countRow == 0) {
					$contentExportPDF .= '<tr>';
					$contentExportPDF .= '<th>BESTELLUNG</th>';
					$contentExportPDF .= '<th>K-NR</th>';
					$contentExportPDF .= '<th>KUNDE</th>';
					$contentExportPDF .= '<th>PLZ</th>';
					$contentExportPDF .= '<th>ORT</th>';
					$contentExportPDF .= '<th>MENGE</th>';
					$contentExportPDF .= '<th>ARTIKEL</th>';
					$contentExportPDF .= '<th>DRUCKFARBE</th>';
					// $contentExportPDF .= '<th>VERTRETER</th>';
					$contentExportPDF .= '<th>MAIL</th>';
					$contentExportPDF .= '<th>NOTIZ</th>';
					$contentExportPDF .= '<th>STATUS</th>';
					$contentExportPDF .= '<th>BESTELLART</th>';
					foreach(array_keys($ds) as $field) {
						// $contentExportPDF .= '<th>' . strtoupper(preg_replace('/orders/', '', $field)) . '</th>';
					}
					$contentExportPDF .= '</tr>';
				}

				$thisBackgroundColor = '#FFF';
				if($countRow%2 == 0) {
					$thisBackgroundColor = '#EEE';
				}
				/*
				if($ds["ordersStatus"] == 1) {
					$thisBackgroundColor = '#FEFF7F';
				}
				else if($ds["ordersStatus"] == 2) {
					$thisBackgroundColor = '#AEFC9F';
				}
				*/

				$contentExportPDF .= '<tr style="background-color:'.$thisBackgroundColor.';">';

				#$contentExportPDF .= '<td>';
				#$contentExportPDF .= formatDate($ds["ordersBelichtungsDatum"], 'display');
				#$contentExportPDF .= '</td>';

				$contentExportPDF .= '<td>';
				$contentExportPDF .= formatDate($ds["ordersBestellDatum"], 'display');
				$contentExportPDF .= '</td>';

				$contentExportPDF .= '<td>';
				$contentExportPDF .= ($ds["ordersKundennummer"]);
				$contentExportPDF .= '</td>';

				$contentExportPDF .= '<td>';
				$contentExportPDF .= wordwrap($ds["ordersKundenName"], 20, "<br />", false);
				if($ds["ordersKommission"] != ''){
					$contentExportPDF .= '<br>' . wordwrap("<b>Kommission</b> " . $ds["ordersKommission"], 20, "<br />", false);
				}
				$contentExportPDF .= '</td>';
				$contentExportPDF .= '<td>';
				$contentExportPDF .= nl2br($ds["ordersPLZ"]);
				$contentExportPDF .= '</td>';
				$contentExportPDF .= '<td>';
				$contentExportPDF .= nl2br($ds["ordersOrt"]);
				$contentExportPDF .= '</td>';
				$contentExportPDF .= '<td>';
				$contentExportPDF .= nl2br($ds["ordersArtikelMenge"]);
				$contentExportPDF .= '</td>';

				$contentExportPDF .= '<td>';
				$ds[$ds["ordersArtikelBezeichnung"]] = preg_replace("/\//", "/ ", $ds["ordersArtikelBezeichnung"]);
				$contentExportPDF .= wordwrap($ds["ordersArtikelBezeichnung"], 20, "<br />", false);
				if($ds["ordersAdditionalArtikelMenge"] > 0) {
					$contentExportPDF .= '<br /> (+ '. $ds["ordersAdditionalArtikelMenge"] . ' Leisten)';
				}
				$contentExportPDF .= '</td>';

				$contentExportPDF .= '<td>';
				$contentExportPDF .= preg_replace('/\[(.*)\]/ismU', '', $ds["ordersDruckFarbe"]);
				$contentExportPDF .= '</td>';

				// $contentExportPDF .= '<th>VERTRETER</th>';

				$contentExportPDF .= '<td>';
				if(strlen($ds["ordersMailAdress"]) > 30) {
					if(preg_match("/@/",$ds["ordersMailAdress"])) {
						$contentExportPDF .= str_replace("@", "@<br />", $ds["ordersMailAdress"]);
					}
					else {
						$contentExportPDF .= wordwrap($ds["ordersMailAdress"], 30, "<br />", true);
					}
				}
				else {
					$contentExportPDF .= nl2br($ds["ordersMailAdress"]);
				}
				$contentExportPDF .= '</td>';

				$contentExportPDF .= '<td>';
				$contentExportPDF .= wordwrap($ds["ordersNotizen"], 20, " <br />", false);
				$contentExportPDF .= '</td>';

				$contentExportPDF .= '<td>';
				$contentExportPDF .= $arrOrderStatusTypeDatas[$ds["ordersStatus"]]["orderStatusTypesName"];
				$contentExportPDF .= '</td>';

				$contentExportPDF .= '<td>';
				$contentExportPDF .= $arrOrderTypeDatas[$ds["ordersOrderType"]]["orderTypesName"];
				$contentExportPDF .= '</td>';

				$contentExportPDF .= '</tr>';
				$countRow++;
			}
			$contentExportPDF .= '</table>';

			$pdfContentPage = $contentExportPDF;

			//echo $pdfContentPage;
			//exit;

			$pdfContentPage = removeUnnecessaryChars($pdfContentPage);
			$contentExportCSV = formatCSV($contentExportPDF);

			// BOF STORE CSV
			$thisVertreter = $arrSalesmenDatas[$_POST["exportOrdersVertreterID"]]["customersFirmenname"];
			$thisCreatedCsvName = convertChars($thisVertreter).'.csv';
			if(file_exists(DIRECTORY_CREATED_DOCUMENTS_SALESMEN . $thisCreatedCsvName)) {
				// chmod(DIRECTORY_CREATED_DOCUMENTS_SALESMEN . $thisCreatedCsvName , "0777");
				unlink(DIRECTORY_CREATED_DOCUMENTS_SALESMEN . $thisCreatedCsvName);
			}
			clearstatcache();
			$fp = fopen(DIRECTORY_CREATED_DOCUMENTS_SALESMEN . $thisCreatedCsvName, 'w');
			fwrite($fp, stripslashes($contentExportCSV));
			fclose($fp);
			// EOF STORE CSV

			// BOF WRITE TEMP HTML FILE
			$thisCreatedPdfName = 'Liste_' . convertChars($thisVertreter).'.pdf';
			$tempHtmlFileName = "_temp_".$thisCreatedPdfName.'.html';
			if(file_exists(DIRECTORY_CREATED_DOCUMENTS_SALESMEN . $tempHtmlFileName)) {
				unlink(DIRECTORY_CREATED_DOCUMENTS_SALESMEN . $tempHtmlFileName);
			}
			clearstatcache();
			$fp = fopen(DIRECTORY_CREATED_DOCUMENTS_SALESMEN . $tempHtmlFileName, 'w');
			fwrite($fp, stripslashes($pdfContentPage));
			fclose($fp);
			// EOF WRITE TEMP HTML FILE

			// BOF createPDF
			require_once(DIRECTORY_HTML2PDF . 'html2pdf.class.php');

			/*
			if(file_exists(DIRECTORY_CREATED_DOCUMENTS_SALESMEN . $thisCreatedPdfName)) {
				try {
					chmod(DIRECTORY_CREATED_DOCUMENTS_SALESMEN . $thisCreatedPdfName , "0777");
					unlink(DIRECTORY_CREATED_DOCUMENTS_SALESMEN . $thisCreatedPdfName);
				}
				catch (Exception $e) {}
			}
			clearstatcache();
			*/

			try {
				// $html2pdf = new HTML2PDF('P', 'A4', 'de', true, 'UTF-8', array(0, 0, 0, 0));
				// $html2pdf = new HTML2PDF('L', 'A4', 'de', true, 'UTF-8', array(22, 5, 5, 5));
				$html2pdf = new HTML2PDF('L', 'A4', 'de', true, 'UTF-8', array(10, 10, 10, 10));
				// $html2pdf = new HTML2PDF('P', 'A4', 'de');
				// $html2pdf = new HTML2PDF('P', 'A4', 'de', true, 'UTF-8');
				// $html2pdf->setModeDebug();
				// $html2pdf->setDefaultFont('Arial');
				// $html2pdf->createIndex('Inhaltsverzeichnis', 12, 8, false, true, 2);

				if(file_exists(DIRECTORY_CREATED_DOCUMENTS_SALESMEN . $thisCreatedPdfName)) {
					// chmod(DIRECTORY_CREATED_DOCUMENTS_SALESMEN . $thisCreatedPdfName , "0777");
					unlink(DIRECTORY_CREATED_DOCUMENTS_SALESMEN . $thisCreatedPdfName);
				}
				clearstatcache();

				// BOF READ TEMP HTML FILE
				if(file_exists(DIRECTORY_CREATED_DOCUMENTS_SALESMEN . $tempHtmlFileName)) {
					$pdfContentPage = "";
					$fp = fopen(DIRECTORY_CREATED_DOCUMENTS_SALESMEN . $tempHtmlFileName, 'r');
					$pdfContentPage = fread($fp, filesize(DIRECTORY_CREATED_DOCUMENTS_SALESMEN . $tempHtmlFileName));
					fclose($fp);
					$pdfContentPage = stripslashes($pdfContentPage);
					unlink(DIRECTORY_CREATED_DOCUMENTS_SALESMEN . $tempHtmlFileName);
				}
				clearstatcache();
				// EOF READ TEMP HTML FILE

				ob_start();
				echo $pdfContentPage;
				$contentPDF = ob_get_clean();

				$html2pdf->writeHTML($contentPDF, isset($_GET['vuehtml']));
				$html2pdf->Output($thisCreatedPdfName, 'F', DIRECTORY_CREATED_DOCUMENTS_SALESMEN);

				if(file_exists($thisCreatedPdfName)) {
					unlink($thisCreatedPdfName);

					/*
					$sql_update = array();
					foreach($thisDocumentOrdersIDs as $thisKey => $thisValue) {
						$sql_update[] = " `ordersToDocumentsOrderID` = '" . $thisValue . "' ";
					}

					$sql = "UPDATE `" . TABLE_ORDERS_TO_DOCUMENTS . "`
								SET `ordersToDocumentsStatus` = 'created'
								WHERE ". implode(" OR ", $sql_update) . "
					";

					$rs = $dbConnection->db_query($sql);
					*/
				}
				clearstatcache();
			}
			catch(HTML2PDF_exception $e) {
				echo $e;
				exit;
			}
			$_POST["selectTemplate"] = "";

			if(file_exists(DIRECTORY_CREATED_DOCUMENTS_SALESMEN . $thisCreatedPdfName)) {
				$successMessage .= 'Die PDF-Datei &quot;' . $thisCreatedPdfName . '&quot wurde generiert. ' . '<br />';
				$showPdfDownloadLink = true;
			}
			else {
				$errorMessage .= 'Die PDF-Datei &quot;' . $thisCreatedPdfName . '&quot konnte nicht generiert werden. ' . '<br />';
			}
			clearstatcache();
		// EOF createPDF
		}
		else {
			$errorMessage .= 'Es liegen keine Datens&auml;tze vor, die f&uuml;r den Vertreter ' . $arrSalesmenDatas[$_POST["exportOrdersVertreterID"]]["customersFirmenname"] . ' exportiert werden k&ouml;nnen. ' . '<br />';
		}
	}
?>

<div id="mainArea">
	<div id="mainContent">
		<?php require_once(FILE_MENUE_SIDEBAR); ?>
		<div id="contentArea">
			<a name="top"></a>
			<div id="contentAreaElements">
				<h1><?php if(SHOW_PAGE_TITLE_ICONS) { echo '<img src="' . PATH_ICONS_MENUE_TITLES . 'export.png" alt="" />'; } ?> <?php echo $thisTitle; ?></h1>
<?php
#dd('arrSalesmenDatas');
?>
				<div id="searchFilterArea">
					<form name="formExportDatas" method="post" action="<?php echo $_SERVER["PHP_SELF"]; ?>">
					<table border="0" cellpadding="0" cellspacing="0" width="100%" class="searchFilterContent">
						<tr>
							<td>
								<label for="exportOrdersVertreterID">Vertreter:</label>
								<select id="exportOrdersVertreterID" name="exportOrdersVertreterID" class="inputSelect_120">
									<option value=""> - </option>
									<?php if(MANDATOR == 'bctr'){ ?>
									<option value="NO_SALESMAN">Ohne Vertreter</option>
									<?php } ?>
									<option value="13301">BURHAN-CTR | K-NR: 48464</option>
									<?php if(MANDATOR == 'b3'){ ?>
									<option value="5684">B3 GmbH | K-NR: 48271</option>
									<?php } ?>
									<?php
										if(!empty($arrSalesmenDatas)) {
											foreach($arrSalesmenDatas as $thisKey => $thisValue) {
												if($arrSalesmenDatas[$thisKey]["salesmenActive"] == '1'){
													$selected = '';
													if($thisKey == $orderDatas["ordersVertreterID"]) {
														$selected = ' selected ';
													}
													if($arrSalesmenDatas[$thisKey]["salesmenExportDatas"] == '1'){
														echo '<option value="' . $thisKey . '" '.$selected.' >' . ($arrSalesmenDatas[$thisKey]["customersFirmenname"]). ' | K-NR: '.$arrSalesmenDatas[$thisKey]["customersKundennummer"].'</option>';
													}
												}
											}
										}
									?>
								</select>
							</td>
							<td><input type="checkbox" value="1" name="exportAllStatusOF" id="exportAllStatusOF" checked="checked" /> <b>Alle offenen Bestellungen</b></td>
							<td><input type="checkbox" value="1" name="exportStatusFG" id="exportStatusFG" /> <b>Freigegebene Bestellungen</b> von
								<?php
									$fromDate = formatDate(date("Y-m-d", mktime(1, 1, 1, date("m"), (date("d") - 7), date("Y"))), "display");
								?>
								<input type="text" name="exportOrdersFreigabeDatumFrom" id="exportOrdersFreigabeDatumFrom" maxlength="2" class="inputField_70" readonly="readonly" value="<?php echo $fromDate; ?>" />
								bis
								<?php
									$todayDate = formatDate(date("Y-m-d"), "display");
								?>
								<input type="text" name="exportOrdersFreigabeDatumTo" id="exportOrdersFreigabeDatumTo" maxlength="2" class="inputField_70" readonly="readonly" value="<?php echo $todayDate; ?>" />
							</td>
							<td>
								<input type="hidden" name="editID" id="editID" value="" />
								<input type="submit" name="submitExport" class="inputButton0" value="Bestellungen exportieren" />
							</td>
						</tr>
					</table>
					<input type="hidden" name="submitExportForm" id="submitExportForm" value="1" />
					</form>
				</div>

				<div class="contentDisplay">
					<?php
						// echo $contentExportPDF;
					?>
					<?php displayMessages(); ?>
					<?php
						if($_POST["submitExportForm"] == 1) {
							if($showPdfDownloadLink) {
								// echo '<p class="dataTableContent"><b>DOWNLOAD-LINK: </b><a href="'.DIRECTORY_CREATED_DOCUMENTS_SALESMEN . $thisCreatedPdfName.'" target="_blank">'.$thisCreatedPdfName.'</a></p>';
								echo '<p class="dataTableContent"><b>DOWNLOAD-LINK: </b><img src="layout/icons/iconPDF.gif" height="16" width="16" alt="" /> <a href="?downloadFile=' . $thisCreatedPdfName.'">'.date("Y-m-d").'_'.utf8_decode($thisCreatedPdfName).'</a></p>';
								echo '<p class="dataTableContent"><b>DOWNLOAD-LINK: </b><img src="layout/icons/iconEXCEL.gif" height="16" width="16" alt="" /> <a href="?downloadFile=' . $thisCreatedCsvName.'">'.date("Y-m-d").'_'.utf8_decode($thisCreatedCsvName).'</a></p>';
								echo '<p class="dataTableContent">';
								echo '<b>Datei versenden: </b><img src="layout/icons/mailAttachement.png" class="buttonSendDocument" rel="' . urlencode($thisCreatedPdfName) . '#SX" width="16" height="16" title="Datei direkt per Mail versenden" alt="Dokument versenden" />';
								echo '</p>';
							}
							echo '<hr />';
							// $_POST["editID"] = "";
						}
					?>
				</div>

			</div>
		</div>
		<div class="clear"></div>
	</div>
</div>
<script language="javascript" type="text/javascript">
	$(document).ready(function() {
		$(function() {
			$.datepicker.setDefaults($.datepicker.regional[""]);
			$('#exportOrdersFreigabeDatumFrom').datepicker($.datepicker.regional["de"]);
			$('#exportOrdersFreigabeDatumTo').datepicker($.datepicker.regional["de"]);
		});

		$('.buttonSendDocument').click(function() {
			var mailDocumentFilename = $(this).attr('rel');
			sendAttachedDocument($(this), mailDocumentFilename, '<?php echo $salesmanKundennummer; ?>', '<?php echo $_REQUEST["editID"]; ?>', '', '<?php echo $_SERVER["REQUEST_URI"]; ?>', '<?php echo $salesmanMail; ?>');
		});
	});
</script>
<?php require_once('inc/footerHTML.inc.php'); ?>