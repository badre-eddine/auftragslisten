<?php
	require_once('inc/requires.inc.php');

	if(!$arrGetUserRights["phpInfo"]) {
		header('location: ' . PAGE_EXIT_LOCATION);
		exit;
	}

	$jswindowMessage = '';
	$warningMessage = '';
	$errorMessage = '';
	$successMessage = '';
	$infoMessage = '';

	$dbConnection = new DB_Connection();
	$db_open = $dbConnection->db_connect();

?>
<?php
	require_once('inc/headerHTML.inc.php');
	$thisTitle = "PHP-INFO";
	$headerHTML = preg_replace("/{###TITLE###}/", strip_tags($thisTitle), $headerHTML);
	echo $headerHTML;

	require_once(FILE_MENUE_TOP);
?>
<div id="mainArea">
	<div id="mainContent">
		<?php require_once(FILE_MENUE_SIDEBAR); ?>
		<div id="contentArea">
			<div id="contentAreaElements">
				<h1><?php if(SHOW_PAGE_TITLE_ICONS) { echo '<img src="' . PATH_ICONS_MENUE_TITLES . 'phpinfo.png" alt="" />'; } ?> <?php echo $thisTitle; ?></h1>
					<?php
						ob_start();
						phpinfo();
						$pinfo = ob_get_contents();
						ob_end_clean();
						$pinfo = preg_replace( '%^.*<body>(.*)</body>.*$%ms','$1',$pinfo);
						#$pinfo = preg_replace('/<table/', ' <table width="100%" class="displayOrders" ', $pinfo);
						$pinfo = preg_replace('/<table.*>/', ' <table width="100%" class="displayOrders" cellspacing="0" cellpadding="0">', $pinfo);
						echo $pinfo;

					?>
			</div>
		</div>
		<div class="clear"></div>
	</div>
	<div class="clear"></div>
</div>

<script language="javascript" type="text/javascript">
	$(document).ready(function() {
		$(function() {
			$( "#datepicker" ).datepicker();
		});
		colorRowMouseOver('.displayOrders tbody tr');
	});
</script>

<?php require_once('inc/footerHTML.inc.php'); ?>