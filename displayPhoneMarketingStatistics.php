<?php
	require_once('inc/requires.inc.php');

	if(!$arrGetUserRights["displayDistribution"]) {
		header('location: ' . PAGE_EXIT_LOCATION);
		exit;
	}

	$jswindowMessage = "";
	$warningMessage = '';
	$errorMessage = '';
	$successMessage = '';
	$infoMessage = '';

	// BOF DOWNLOAD FILE
	if($_REQUEST["downloadFile"] != "") {
		$thisDownloadFile = basename($_REQUEST["downloadFile"]);
		$thisDownloadFolder = DIRECTORY_DOWNLOAD_CONTAINER_LISTS;

		require_once('classes/downloadFile.class.php');
		$thisDownload = new downloadFile($thisDownloadFolder, $thisDownloadFile);
		$errorMessage .= $thisDownload->startDownload();
	}
	// BOF DOWNLOAD FILE

	$dbConnection = new DB_Connection();
	$db_open = $dbConnection->db_connect();
	
	// BOF GET USER DATAS
		$arrUserDatas = getUsers();		
	// BOF GET USER DATAS

	// BOF GET ALL MARKETING PHONE DATES
	
		$sqlWhere = "";
		$dateField = "";
		
		if($_POST["searchInterval"] == ""){
			$_POST["searchInterval"] = "DAY";
		}
		
		if($_POST["searchInterval"] == "DAY"){
			$sqlWhere .= "";
			$dateField = "
				DATE_FORMAT(`" . TABLE_CUSTOMERS_PHONE_MARKETING . "`.`customersPhoneMarketingContactDate`, '%Y-%m-%d') AS `interval`
			";
		}		
		else if($_POST["searchInterval"] == "WEEK"){
			$sqlWhere .= "";
			$dateField = "
				CONCAT(
					DATE_FORMAT(`" . TABLE_CUSTOMERS_PHONE_MARKETING . "`.`customersPhoneMarketingContactDate`, '%Y'),
					'#',
					DATE_FORMAT(`" . TABLE_CUSTOMERS_PHONE_MARKETING . "`.`customersPhoneMarketingContactDate`, '%u')
				) AS `interval`
			";
		}
		else if($_POST["searchInterval"] == "MONTH"){
			// DATE_FORMAT(`" . TABLE_ORDER_INVOICES . "`.`customersPhoneMarketingContactDate`, '%M') AS `interval`,
			$sqlWhere .= "";

			$dateField = "
				CONCAT(
					DATE_FORMAT(`" . TABLE_CUSTOMERS_PHONE_MARKETING . "`.`customersPhoneMarketingContactDate`, '%Y'),
					'#',
					DATE_FORMAT(`" . TABLE_CUSTOMERS_PHONE_MARKETING . "`.`customersPhoneMarketingContactDate`, '%m')
				) AS `interval`
			";
		}
		else if($_POST["searchInterval"] == "QUARTER"){
			$sqlWhere .= "";

			$dateField = "
				CONCAT(
					DATE_FORMAT(`" . TABLE_CUSTOMERS_PHONE_MARKETING . "`.`customersPhoneMarketingContactDate`, '%Y'),
					'#',
					IF(DATE_FORMAT(`" . TABLE_CUSTOMERS_PHONE_MARKETING . "`.`customersPhoneMarketingContactDate`, '%m') = '01' OR DATE_FORMAT(`" . TABLE_CUSTOMERS_PHONE_MARKETING . "`.`customersPhoneMarketingContactDate`, '%m') = '02' OR DATE_FORMAT(`" . TABLE_CUSTOMERS_PHONE_MARKETING . "`.`customersPhoneMarketingContactDate`, '%m') = '03', '1. Quartal',
						IF(DATE_FORMAT(`" . TABLE_CUSTOMERS_PHONE_MARKETING . "`.`customersPhoneMarketingContactDate`, '%m') = '04' OR DATE_FORMAT(`" . TABLE_CUSTOMERS_PHONE_MARKETING . "`.`customersPhoneMarketingContactDate`, '%m') = '05' OR DATE_FORMAT(`" . TABLE_CUSTOMERS_PHONE_MARKETING . "`.`customersPhoneMarketingContactDate`, '%m') = '06', '2. Quartal',
							IF(DATE_FORMAT(`" . TABLE_CUSTOMERS_PHONE_MARKETING . "`.`customersPhoneMarketingContactDate`, '%m') = '07' OR DATE_FORMAT(`" . TABLE_CUSTOMERS_PHONE_MARKETING . "`.`customersPhoneMarketingContactDate`, '%m') = '08' OR DATE_FORMAT(`" . TABLE_CUSTOMERS_PHONE_MARKETING . "`.`customersPhoneMarketingContactDate`, '%m') = '09', '3. Quartal',
								IF(DATE_FORMAT(`" . TABLE_CUSTOMERS_PHONE_MARKETING . "`.`customersPhoneMarketingContactDate`, '%m') = '10' OR DATE_FORMAT(`" . TABLE_CUSTOMERS_PHONE_MARKETING . "`.`customersPhoneMarketingContactDate`, '%m') = '11' OR DATE_FORMAT(`" . TABLE_CUSTOMERS_PHONE_MARKETING . "`.`customersPhoneMarketingContactDate`, '%m') = '12', '4. Quartal',
									''
								)
							)
						)
					)
				) AS `interval`
			";

		}
		else if($_POST["searchInterval"] == "YEAR"){
			$sqlWhere .= "";

			$dateField = "
				CONCAT(
					DATE_FORMAT(`" . TABLE_CUSTOMERS_PHONE_MARKETING . "`.`customersPhoneMarketingContactDate`, '%Y'),
					'#',
					DATE_FORMAT(`" . TABLE_CUSTOMERS_PHONE_MARKETING . "`.`customersPhoneMarketingContactDate`, '%Y')
				) AS `interval`
			";

		}
		
	
		$sql = "
			SELECT
					`interval`,
					
					COUNT(`tempTable`.`customersPhoneMarketingID`) AS `countItems`,
					GROUP_CONCAT(DISTINCT `tempTable`.`customersPhoneMarketingCustomerNumber`) AS `customersPhoneMarketingCustomerNumbers`,
					GROUP_CONCAT(`tempTable`.`customersPhoneMarketingCustomerID`) AS `customersPhoneMarketingCustomerCallsCount`,
					COUNT(DISTINCT `tempTable`.`customersPhoneMarketingCustomerNumber`) AS `countCustomers`,
					/*
					GROUP_CONCAT(DISTINCT `tempTable`.`customersPhoneMarketingCustomerID`) AS `customersPhoneMarketingCustomerIDs`,
					GROUP_CONCAT(DISTINCT `tempTable`.`customersPhoneMarketingContactDate`) AS `customersPhoneMarketingContactDates`,			
					GROUP_CONCAT(DISTINCT `tempTable`.`customersPhoneMarketingCampaign`) AS `customersPhoneMarketingCampaigns`,
										
					GROUP_CONCAT(DISTINCT `tempTable`.`customersPhoneMarketingRemindDate`) AS `customersPhoneMarketingRemindDates`,
					GROUP_CONCAT(DISTINCT `tempTable`.`customersPhoneMarketingCallbackDate`) AS `customersPhoneMarketingCallbackDates`,
					GROUP_CONCAT(DISTINCT `tempTable`.`customersPhoneMarketingVisitRequest`) AS `customersPhoneMarketingVisitRequests`,
					GROUP_CONCAT(DISTINCT `tempTable`.`customersPhoneMarketingNotice`) AS `customersPhoneMarketingNotices`,
					*/
					GROUP_CONCAT(`tempTable`.`customersPhoneMarketingUser`) AS `customersPhoneMarketingUsers`,
					
					GROUP_CONCAT(DISTINCT 
						`tempTable`.`customersID`,
						'|',
						`tempTable`.`customersKundennummer`,
						'|',
						`tempTable`.`customersFirmenname`,
						'|',
						`tempTable`.`customersFirmennameZusatz`
						
						ORDER BY `tempTable`.`customersKundennummer` ASC
						
						SEPARATOR '#'
					) AS `customersDatas`
				FROM (
		
					SELECT
					
						" . $dateField . ",
					
						`" . TABLE_CUSTOMERS_PHONE_MARKETING . "`.`customersPhoneMarketingID`,
						`" . TABLE_CUSTOMERS_PHONE_MARKETING . "`.`customersPhoneMarketingCustomerNumber`,
						`" . TABLE_CUSTOMERS_PHONE_MARKETING . "`.`customersPhoneMarketingCustomerID`,
						`" . TABLE_CUSTOMERS_PHONE_MARKETING . "`.`customersPhoneMarketingContactDate`,
						`" . TABLE_CUSTOMERS_PHONE_MARKETING . "`.`customersPhoneMarketingContactTime`,
						`" . TABLE_CUSTOMERS_PHONE_MARKETING . "`.`customersPhoneMarketingCampaign`,
						`" . TABLE_CUSTOMERS_PHONE_MARKETING . "`.`customersPhoneMarketingConversationPerson`,
						`" . TABLE_CUSTOMERS_PHONE_MARKETING . "`.`customersPhoneMarketingConversationContent`,
						`" . TABLE_CUSTOMERS_PHONE_MARKETING . "`.`customersPhoneMarketingRemindDate`,
						`" . TABLE_CUSTOMERS_PHONE_MARKETING . "`.`customersPhoneMarketingCallbackDate`,
						`" . TABLE_CUSTOMERS_PHONE_MARKETING . "`.`customersPhoneMarketingVisitRequest`,
						`" . TABLE_CUSTOMERS_PHONE_MARKETING . "`.`customersPhoneMarketingNotice`,
						`" . TABLE_CUSTOMERS_PHONE_MARKETING . "`.`customersPhoneMarketingUser`,
						
						`" . TABLE_CUSTOMERS . "`.`customersID`,
						`" . TABLE_CUSTOMERS . "`.`customersKundennummer`,
						`" . TABLE_CUSTOMERS . "`.`customersFirmenname`,
						`" . TABLE_CUSTOMERS . "`.`customersFirmennameZusatz`
						
						
					FROM `" . TABLE_CUSTOMERS_PHONE_MARKETING . "`
					
					LEFT JOIN `" . TABLE_CUSTOMERS . "`
					ON(`" . TABLE_CUSTOMERS_PHONE_MARKETING . "`.`customersPhoneMarketingCustomerID` = `" . TABLE_CUSTOMERS . "`.`customersID`)
					
					WHERE 1
						" . $sqlWhere . "
				) AS `tempTable`
						
				GROUP BY `interval`

				ORDER BY `interval` DESC		
		";
	// EOF GET ALL MARKETING PHONE DATES

?>
<?php
	require_once('inc/headerHTML.inc.php');
	$thisTitle = "Telefon-Marketing Statistik";
	
	if($_POST["searchInterval"] == 'DAY') {
		$thisTitle .= ': <span class="headerSelectedEntry">pro Tag</span>';
	}
	else if($_POST["searchInterval"] == 'WEEK') {
		$thisTitle .= ': <span class="headerSelectedEntry">pro Woche</span>';
	}
	else if($_POST["searchInterval"] == 'MONTH') {
		$thisTitle .= ': <span class="headerSelectedEntry">pro Monat</span>';
	}
	else if($_POST["searchInterval"] == 'QUARTER') {
		$thisTitle .= ': <span class="headerSelectedEntry">pro Vierteljahr</span>';
	}
	else if($_POST["searchInterval"] == 'YEAR') {
		$thisTitle .= ': <span class="headerSelectedEntry">pro Jahr</span>';
	}

	$headerHTML = preg_replace("/{###TITLE###}/", strip_tags($thisTitle), $headerHTML);
	echo $headerHTML;

	require_once(FILE_MENUE_TOP);


?>

<div id="mainArea">
	<div id="mainContent">
		<?php require_once(FILE_MENUE_SIDEBAR); ?>
		<div id="contentArea">
			<a name="top"></a>
			<div id="contentAreaElements">
				<h1><?php if(SHOW_PAGE_TITLE_ICONS) { echo '<img src="' . PATH_ICONS_MENUE_TITLES . 'statistics.png" alt="" />'; } ?> <?php echo $thisTitle; ?></h1>

				<div id="searchFilterArea">
					<form name="formFilterSearch" method="post" action="<?php echo $_SERVER["PHP_SELF"]; ?>">
					<table border="0" cellpadding="0" cellspacing="0" class="searchFilterContent">
						<tr>
							<td>
								<label for="searchInterval">Zeitraum:</label>
								<select name="searchInterval" id="searchInterval" class="inputField_130">
									<option value=""></option>
									<option value="DAY" <?php if($_POST["searchInterval"] == 'DAY'){ echo ' selected="selected" '; } ?> >pro Tag</option>
									<option value="WEEK" <?php if($_POST["searchInterval"] == 'WEEK'){ echo ' selected="selected" '; } ?> >pro Woche</option>
									<option value="MONTH" <?php if($_POST["searchInterval"] == 'MONTH'){ echo ' selected="selected" '; } ?> >pro Monat</option>
									<option value="QUARTER" <?php if($_POST["searchInterval"] == 'QUARTER'){ echo ' selected="selected" '; } ?> >pro Vierteljahr</option>
									<option value="YEAR" <?php if($_POST["searchInterval"] == 'YEAR'){ echo ' selected="selected" '; } ?> >pro Jahr</option>
								 </select>
							</td>	
							<td>
								<input type="hidden" name="editID" id="editID" value="" />
								<input type="submit" name="submitSearch" class="inputButton0" value="Suchen" />
							</td>
						</tr>
					</table>					
					</form>
				</div>

				<?php displayMessages(); ?>

				<?php
					
					$rs = $dbConnection->db_query($sql);
					$countRows = $dbConnection->db_getMysqlNumRows($rs);
					
					if($countRows > 0) {
				?>
				
				<!-- BOF GRAPH ELEMENTS -->
					<script type="text/javascript" language="javascript" src="<?php echo PATH_RGRAPH; ?>libraries/RGraph.common.core.js" ></script>
					<script type="text/javascript" language="javascript" src="<?php echo PATH_RGRAPH; ?>libraries/RGraph.bar.js" ></script>
					<script type="text/javascript" language="javascript" src="<?php echo PATH_RGRAPH; ?>libraries/RGraph.line.js" ></script>
					<canvas id="cvs_dataCountItems" width="900" height="250">[No canvas support]</canvas>					
				<!-- EOF GRAPH ELEMENTS -->
				
				<div class="contentDisplay">
					<table border="0" cellpadding="0" cellspacing="0" width="100%" class="displayOrders">
						<thead>
							<tr>
								<th style="width:50px;">#</th>
								<th style="width:90px;">Zeitraum</th>
								<th style="width:90px;">Anzahl Gespr&auml;che</th>
								<th style="width:120px;">Anzahl angerufener Kunden</th>
								<th>angerufene Kunden</th>								
								<th>Anrufe pro Benutzer</th>
							</tr>
						</thead>

						<tbody>
						<?php
							$countRow = 0;
							$thisMarker = "";
													
							while($ds = mysqli_fetch_assoc($rs)){								
								
								$arrLabelsGraph[$ds["interval"]] = $ds["interval"];
								$arrDatasGraph[$ds["interval"]] = $ds["countItems"];								
								
								if($countRow%2 == 0) { $rowClass = 'row0'; }
								else { $rowClass = 'row1'; }
								
								$arrTemp = explode('#', $ds["interval"]);
								if($thisMarker != $arrTemp[0]) {
									$thisMarker = $arrTemp[0];
									echo '<tr><td colspan="8" class="tableRowTitle1">' . $arrTemp[0] . '</td></tr>';
								}							
								
								echo '<tr class="' . $rowClass . '">';
								echo '<td style="text-align:right;">';
								echo '<b>' . ($countRow + 1) . '.</b>';
								echo '</td>';
								
								echo '<td style="text-align:right;">';
								if($_POST["searchInterval"] == "MONTH"){
									$thisMonth = getTimeNames($arrTemp[1], 'month', 'long');
									echo $thisMonth;
								}
								else {
									if($_POST["searchInterval"] == "WEEK"){ echo 'KW '; }
									if($_POST["searchInterval"] == "DAY"){ echo  formatDate($ds["interval"], "display"); }
									else { echo $arrTemp[1]; }
								}
								
								echo '</td>';
								
								echo '<td style="text-align:right;">';
								echo $ds["countItems"];
								echo '</td>';
								
								echo '<td style="text-align:right;">';
								echo $ds["countCustomers"];
								echo '</td>';
								
								echo '<td>';								
								echo '<div style="max-height:50px;overflow:auto;">';
								
								$arrCustomersPhoneMarketingCustomerCallsCount = explode(",", $ds["customersPhoneMarketingCustomerCallsCount"]);								
								if(!empty($arrCustomersPhoneMarketingCustomerCallsCount)){
									$arrCustomerCallsCount = array();
									foreach($arrCustomersPhoneMarketingCustomerCallsCount as $thisCustomersID){
										$arrCustomerCallsCount[$thisCustomersID]++;
									}
								}								
								
								$arrCustomersDatas = explode("#", $ds["customersDatas"]);
								echo '<ul style="margin-bottom:0;padding-bottom:0;">';
								if(!empty($arrCustomersDatas)){
									foreach($arrCustomersDatas as $thisCustomerValue){
										echo '<li>';										
										$arrThisCustomersData = explode("|", $thisCustomerValue);
										echo '<b><a href="' . PAGE_EDIT_CUSTOMER . '?searchBoxCustomer=' . $arrThisCustomersData[1] . '&amp;tab=tabs-14">' . $arrThisCustomersData[1] . '</a></b>';
										echo ' - <b>' . $arrThisCustomersData[2] . '</b>';
										if($arrThisCustomersData[3] != ''){
											echo ' / <b>' . $arrThisCustomersData[3] . '</b>';
										}
										echo ' [' . $arrCustomerCallsCount[$arrThisCustomersData[0]] . 'x]';
										echo '</li>';
									}
								}
								echo '</ul>';	

								echo '</div>';
								echo '</td>';
								
								echo '<td>';
								
								$arrCustomersPhoneMarketingUsers = explode(",", $ds["customersPhoneMarketingUsers"]);								
								if(!empty($arrCustomersPhoneMarketingUsers)){
									$arrPhoneUsers = array();
									foreach($arrCustomersPhoneMarketingUsers as $thisPhoneUserID){
										$arrPhoneUsers[$thisPhoneUserID]++;
									}
									echo '<table border="0" cellpadding="0" cellspacing="0" class="noBorder">';
									foreach($arrPhoneUsers as $thisPhoneUserID => $thisPhoneUserCalls){
										$arrDatasGraph2[$thisPhoneUserID][$ds["interval"]] = $thisPhoneUserCalls;
										echo '<tr>';
										echo '<td>';
										echo htmlentities($arrUserDatas[$thisPhoneUserID]["usersFirstName"]);
										echo ' ';
										echo htmlentities($arrUserDatas[$thisPhoneUserID]["usersLastName"]);
										echo ':';
										echo '</td>';
										echo '<td style="text-align:right;">';										
										echo $thisPhoneUserCalls . 'x';
										echo '</td>';
										echo '</tr>';
									}
									echo '</table>';
								}								
								echo '</td>';
								
								echo '</tr>';
								$countRow++;
							}				
						?>
						</tbody>
					</table>
				</div>
				
				<div style="text-align:right;" class="menueToTop">
					<a href="#top">nach oben</a>
				</div>
				<?php
					}
					else {
						echo '<p class="warningArea">Es wurden keine Daten gefunden!</p>';
					}
				?>
				<div class="clear"></div>
			</div>
			<div class="clear"></div>
		</div>
	</div>
</div>

<script language="javascript" type="text/javascript">
	<!--
	/* <![CDATA[ */
		$(document).ready(function() {
			
			$(function() {
				$('#searchContactDate').datepicker($.datepicker.regional["de"]);
				// $('#searchContactDate').datepicker("option", "maxDate", "0" );

				$('#searchRemindDate').datepicker($.datepicker.regional["de"]);
				// $('#searchRemindDate').datepicker("option", "maxDate", "0" );	
				
				$('#searchCallbackDate').datepicker($.datepicker.regional["de"]);
				// $('#searchCallbackDate').datepicker("option", "maxDate", "0" );	
			});
			
			// BOF GET DATAS FOR GRAPH
				<?php				
					if(!empty($arrDatasGraph)){
						$arrDatasGraph = array_reverse($arrDatasGraph, true);
					}
				?>
				createGraph('cvs_dataCountItems', [<?php echo implode(",", array_values($arrDatasGraph)); ?>], [<?php echo "'" . implode("','", array_keys($arrDatasGraph)) . "'"; ?>], 'Line', [['green','red']]);			
			// EOF GET DATAS FOR GRAPH
			
		});
	/* ]]> */
	// -->
</script>

<?php require_once('inc/footerHTML.inc.php'); ?>

<?php
	echo $graphContent;
?>