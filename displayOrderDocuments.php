<?php
	require_once('inc/requires.inc.php');

	$arrQueryVars = getUrlQueryString($_SERVER["REQUEST_URI"]);
	foreach($arrQueryVars as $thisKey => $thisValue) {
		$_REQUEST[$thisKey] = $thisValue;
	}
	if($_REQUEST["documentType"] == "") {
		header('location: ' . PAGE_EXIT_LOCATION);
		exit;
	}
	else if($_REQUEST["documentType"] == 'RE' && !$arrGetUserRights["editInvoices"]) {
		header('location: ' . PAGE_EXIT_LOCATION);
		exit;
	}
	else if($_REQUEST["documentType"] == 'AB' && !$arrGetUserRights["editConfirmations"]) {
		header('location: ' . PAGE_EXIT_LOCATION);
		exit;
	}
	else if($_REQUEST["documentType"] == 'AN' && !$arrGetUserRights["editOffers"]) {
		header('location: ' . PAGE_EXIT_LOCATION);
		exit;
	}
	else if($_REQUEST["documentType"] == 'BR' && !$arrGetUserRights["editLetters"]) {
		header('location: ' . PAGE_EXIT_LOCATION);
		exit;
	}
	else if($_REQUEST["documentType"] == 'MA' && !$arrGetUserRights["editReminders"]) {
		header('location: ' . PAGE_EXIT_LOCATION);
		exit;
	}
	else if($_REQUEST["documentType"] == 'LS' && !$arrGetUserRights["editDeliveries"]) {
		header('location: ' . PAGE_EXIT_LOCATION);
		exit;
	}
	else if($_REQUEST["documentType"] == 'AS' && !$arrGetUserRights["editInvoices"]) {
		header('location: ' . PAGE_EXIT_LOCATION);
		exit;
	}
	else if($_REQUEST["documentType"] == 'MB' && !$arrGetUserRights["editInvoices"]) {
		header('location: ' . PAGE_EXIT_LOCATION);
		exit;
	}
	else if($_REQUEST["documentType"] == 'RK' && !$arrGetUserRights["editConfirmations"]) {
		header('location: ' . PAGE_EXIT_LOCATION);
		exit;
	}

	$jswindowMessage = "";
	$warningMessage = '';
	$errorMessage = '';
	$successMessage = '';
	$infoMessage = '';

	$dbConnection = new DB_Connection();
	$db_open = $dbConnection->db_connect();

	// BOF READ DOCUMENT TYPE DATAS
		$arrPaymentStatusTypeDatas = getPaymentStatusTypes();
	// EOF READ DOCUMENT TYPE DATAS

	// BOF READ PAYMENT TYPES
		$arrPaymentTypeDatas = getPaymentTypes();
	// EOF READ PAYMENT TYPES

	// BOF DEFINE CONVERT URLS
		$arrDocumentUrl["AN"] = PAGE_DISPLAY_OFFER;
		$arrDocumentUrl["AB"] = PAGE_DISPLAY_CONFIRMATION;
		$arrDocumentUrl["LS"] = PAGE_DISPLAY_DELIVERY;
		$arrDocumentUrl["RE"] = PAGE_DISPLAY_INVOICE;
		$arrDocumentUrl["GU"] = PAGE_DISPLAY_CREDIT;
		$arrDocumentUrl["MA"] = PAGE_DISPLAY_REMINDER;
		$arrDocumentUrl["M1"] = PAGE_DISPLAY_FIRST_DEMAND;
		$arrDocumentUrl["M2"] = PAGE_DISPLAY_SECOND_DEMAND;
		$arrDocumentUrl["BR"] = PAGE_DISPLAY_LETTER;
		$arrDocumentUrl["RK"] = PAGE_DISPLAY_REKLAMATION;
	// EOF DEFINE CONVERT URLS



	// BOF DEFINE DOCUMENT TABLES DEPENDING ON DOCUMENT TYPE
		if($_REQUEST["documentType"] == 'AN') {
			DEFINE('TABLE_ORDER_THIS', TABLE_ORDER_OFFERS);
			DEFINE('TABLE_ORDER_THIS_DETAILS', TABLE_ORDER_OFFERS_DETAILS);
			DEFINE('PAGE_CREATE_THIS', 'createDocumentOffer.php');
		}
		else if($_REQUEST["documentType"] == 'AB') {
			DEFINE('TABLE_ORDER_THIS', TABLE_ORDER_CONFIRMATIONS);
			DEFINE('TABLE_ORDER_THIS_DETAILS', TABLE_ORDER_CONFIRMATIONS_DETAILS);
			DEFINE('PAGE_CREATE_THIS', 'createDocumentConfirmation.php');
		}
		else if($_REQUEST["documentType"] == 'RK') { 
			DEFINE('TABLE_ORDER_THIS', TABLE_ORDER_REKLAMATIONS);
			DEFINE('TABLE_ORDER_THIS_DETAILS', TABLE_ORDER_REKLAMATIONS_DETAILS);
			DEFINE('PAGE_CREATE_THIS', 'createDocumentConfirmation.php');
		}
		else if($_REQUEST["documentType"] == 'LS') {
			DEFINE('TABLE_ORDER_THIS', TABLE_ORDER_DELIVERIES);
			DEFINE('TABLE_ORDER_THIS_DETAILS', TABLE_ORDER_DELIVERIES_DETAILS);
			DEFINE('PAGE_CREATE_THIS', 'createDocumentDelivery.php');
		}
		else if($_REQUEST["documentType"] == 'RE') {
			DEFINE('TABLE_ORDER_THIS', TABLE_ORDER_INVOICES);
			DEFINE('TABLE_ORDER_THIS_DETAILS', TABLE_ORDER_INVOICES_DETAILS);
			DEFINE('PAGE_CREATE_THIS', 'createDocumentInvoice.php');
		}
		else if($_REQUEST["documentType"] == 'GU') {
			DEFINE('TABLE_ORDER_THIS', TABLE_ORDER_CREDITS);
			DEFINE('TABLE_ORDER_THIS_DETAILS', TABLE_ORDER_CREDITS_DETAILS);
			DEFINE('PAGE_CREATE_THIS', 'createDocumentCredit.php');
		}
		else if($_REQUEST["documentType"] == 'MA') {
			DEFINE('TABLE_ORDER_THIS', TABLE_ORDER_REMINDERS);
			DEFINE('TABLE_ORDER_THIS_DETAILS', TABLE_ORDER_REMINDERS_DETAILS);
			DEFINE('PAGE_CREATE_THIS', 'createDocumentReminder.php');
		}
		else if($_REQUEST["documentType"] == 'M1') {
			DEFINE('TABLE_ORDER_THIS', TABLE_ORDER_FIRST_DEMANDS);
			DEFINE('TABLE_ORDER_THIS_DETAILS', TABLE_ORDER_FIRST_DEMANDS_DETAILS);
			DEFINE('PAGE_CREATE_THIS', 'createDocumentReminder.php');
		}
		else if($_REQUEST["documentType"] == 'M2') {
			DEFINE('TABLE_ORDER_THIS', TABLE_ORDER_SECOND_DEMANDS);
			DEFINE('TABLE_ORDER_THIS_DETAILS', TABLE_ORDER_SECOND_DETAILS);
			DEFINE('PAGE_CREATE_THIS', 'createDocumentReminder.php');
		}
		else if($_REQUEST["documentType"] == 'BR') {
			DEFINE('TABLE_ORDER_THIS', TABLE_ORDER_LETTERS);
			DEFINE('TABLE_ORDER_THIS_DETAILS', TABLE_ORDER_LETTERS_DETAILS);
			DEFINE('PAGE_CREATE_THIS', 'createDocumentLetter.php');
		}
		// DEFINE('TABLE_ORDER_THIS', TABLE_ORDER_OFFERS);
		// DEFINE('TABLE_ORDER_THIS_DETAILS', TABLE_ORDER_OFFERS_DETAILS);
	// EOF DEFINE DOCUMENT TABLES DEPENDING ON DOCUMENT TYPE

	// BOF READ DOCUMENT TYPE DATAS
		$arrDocumentTypeDatas = getDocumentTypes();
	// EOF READ DOCUMENT TYPE DATAS

	// BOF SEND MAIL WITH ATTACHED DOCUMENT
	if($_POST["sendAttachedDocument"] == '1') {
		$thisCreatedPdfName = $_POST["mailDocumentFilename"];
		$_POST["selectSubject"] = $arrDocumentTypeDatas[$_REQUEST["documentType"]]["createdDocumentsTypesName"] . ' Nr.: ' . '{###DOCUMENT_NUMBER###}';
		$generatedDocumentNumber = substr($_POST["mailDocumentFilename"], 0, 13);

		require_once(DIRECTORY_HTML_MIME_MAIL . 'htmlMimeMail.php');
		require_once(DIRECTORY_MAIL_TEMPLATES . 'template_BASIC.inc.php');
		require_once(DIRECTORY_MAIL_TEMPLATES . 'template_TEXT_DATAS.inc.php');
		##require_once("inc/mail.inc.php");

		// BOF SEND MAIL
		require_once("classes/createMail.class.php");

		// BOF CREATE SUBJECT
		$thisSubject = $_POST["selectSubject"];
		if($_POST["sendAttachedMailSubject"] != '') {
			$thisSubject = $_POST["sendAttachedMailSubject"] . ', ' . $thisSubject;
		}
		// EOF CREATE SUBJECT

		// BOF GET ATTACHED FILE
		if($_REQUEST["documentType"] == 'KA') {
			$pathDocumentFolder = DIRECTORY_UPLOAD_FILES;
			$thisCreatedPdfName = utf8_decode($thisCreatedPdfName);
		}
		else if($_REQUEST["documentType"] == 'EX') {
			$pathDocumentFolder = DIRECTORY_EXPORT_FILES;
			$thisCreatedPdfName = utf8_decode($thisCreatedPdfName);
		}
		else if($_REQUEST["documentType"] == 'SX') {
			$pathDocumentFolder = DIRECTORY_CREATED_DOCUMENTS_SALESMEN;
			$thisCreatedPdfName = utf8_decode($thisCreatedPdfName);
		}
		else {
			$pathDocumentFolder = DIRECTORY_CREATED_DOCUMENTS_CUSTOMERS;
		}

		$arrAttachmentFiles = array (
			rawurldecode($pathDocumentFolder . $thisCreatedPdfName) => str_replace(" ", "_", rawurldecode(myUnHtmlEntities($thisCreatedPdfName)))
		);
		// EOF GET ATTACHED FILE

		$createMail = new createMail(
							$thisSubject,													// TEXT:	MAIL_SUBJECT
							$_POST["documentType"],											// STRING:	DOCUMENT TYPE
							$generatedDocumentNumber,										// STRING:	DOCUMENT NUMBER
							$arrAttachmentFiles,											// ARRAY:	ATTACHMENT FILES
							$_POST["selectCustomersRecipientMail"],	 										// STRING:	RECIPIENTS
							$arrMailContentDatas[$_POST["selectMailtextTemplates"]],		// MAIL_TEXT_TEMPLATE
							$mailTemplate["html"],											// MAIL_BODY_TEMPLATE
							'',																// STRING:	ADDITIONAL TEXT
							$_POST["sendAttachedMailText"],									// STRING:	SEND ATTACHED TEXT
							true,															// BOOLEAN:	SEND ATTACHED DOCUMENT
							DIRECTORY_MAIL_IMAGES,											// DIRECTORY_MAIL_IMAGES
							$_POST["selectMailtextSender"]									// STRING:	SENDER
						);

		$createMailResult = $createMail->returnResult();
		$thisErr = $createMail->returnErrors();

		$sendMailToClient = $createMailResult;

		#if(!$sendMailToUs) { $errorMessage .= ' Die Mail-Kopie konnte nicht an ' . implode(', ', $arrOrderMail) . ' versendet werden! '.'<br />'; }
		#else { $successMessage .= ' Die Mail-Kopie wurde an ' . implode(', ', $arrOrderMail) . ' versendet! '.'<br />'; }

		if(!$sendMailToClient) { $errorMessage .= ' Die Mail konnte nicht an ' . $_POST["selectCustomersRecipientName"] . ' (' . $_POST["selectCustomersRecipientMail"] . ') versendet werden! '.'<br />'; }
		else {
			$successMessage .= ' Die Mail wurde an ' . $_POST["selectCustomersRecipientName"] . ' (' . $_POST["selectCustomersRecipientMail"] . ') versendet! '.'<br />';
		}
	}
	// EOF SEND MAIL WITH ATTACHED DOCUMENT

	if(!empty($arrQueryVars["searchWord"]) && $arrQueryVars["searchWord"] != "") {
		$arrQueryVars["searchBoxFile"] = trim($arrQueryVars["searchWord"]);
	}

	// BOF DOWNLOAD FILE
	if($arrQueryVars["downloadFile"] != "") {
		require_once('classes/downloadFile.class.php');
		$thisDownloadFile = $arrQueryVars["downloadFile"];
		if(!file_exists(DIRECTORY_CREATED_DOCUMENTS_CUSTOMERS . $thisDownloadFile)){
			$thisDownloadFile = searchDownloadFile(DIRECTORY_CREATED_DOCUMENTS_CUSTOMERS . $thisDownloadFile, $arrQueryVars["thisDocumentType"]);
		}
		$thisDownload = new downloadFile(DIRECTORY_CREATED_DOCUMENTS_CUSTOMERS, $thisDownloadFile);
		$errorMessage .= $thisDownload->startDownload();
	}
	// EOF DOWNLOAD FILE

	// BOF DELETE FILE
	if($arrQueryVars["deleteFile"] != "" && $arrQueryVars["thisDocumentType"] != "" && $arrQueryVars["thisDocumentNumber"]) {
		deleteFile($arrQueryVars["deleteFile"], $arrQueryVars["thisDocumentType"], $arrQueryVars["thisDocumentNumber"], TABLE_ORDER_THIS, TABLE_ORDER_THIS_DETAILS);
	}
	// EOF DELETE FILE

	// BOF CHECK IF DOCUMENT WAS ALREAD CONVERTED TO THIS DOCUMENT-TYPE
	if($_REQUEST["error"] == 'true' && $_REQUEST["errorOriginDocumentNumber"] != ''){
		$errorMessage .= 'Eine &quot;' . $arrDocumentTypeDatas[$_REQUEST["errorDocumentType"]]["createdDocumentsTypesName"] . '&quot; existiert schon:  &quot;' . $_REQUEST["errorConvertDocumentNumber"] . '&quot;.<br />Daher kann die &quot;' . $_REQUEST["errorOriginDocumentNumber"] . '&quot; nicht in eine &quot;' . $_REQUEST["errorDocumentType"] . '&quot; umgewandelt werden.';
		$_REQUEST["searchDocumentNumber"] = $_REQUEST["errorOriginDocumentNumber"];
	}
	// EOF CHECK IF DOCUMENT WAS ALREAD CONVERTED TO THIS DOCUMENT-TYPE

	// BOF GET SELECTED DOCUMENT DATAS
	$where = "";

	if(trim($_REQUEST["searchCustomerNumber"]) != '') {
		$where = " AND `orderDocumentsCustomerNumber` = '" . trim($_REQUEST["searchCustomerNumber"]) . "'";

		// BOF GET CUSTOMER-ID
			$sql_getCustomerID = "
					SELECT
						`" . TABLE_CUSTOMERS . "`.`customersKundennummer`,
						`" . TABLE_CUSTOMERS . "`.`customersID`
						FROM `" . TABLE_CUSTOMERS . "`
						WHERE 1
							AND `" . TABLE_CUSTOMERS . "`.`customersKundennummer` = '" . trim($_REQUEST["searchCustomerNumber"]) . "'
						LIMIT 1
				";
			$rs_getCustomerID = $dbConnection->db_query($sql_getCustomerID);
			list($searchCustomerNumber, $searchCustomerID) = mysqli_fetch_array($rs_getCustomerID);
		// EOF GET CUSTOMER-ID

		$where = " AND (
						`orderDocumentsCustomerNumber` = '" . trim($_REQUEST["searchCustomerNumber"]) . "'
						OR
						`orderDocumentsSalesman` = '" . trim($searchCustomerID) . "'
					)
			";
	}
	else if(trim($_REQUEST["searchCustomerName"]) != '') {
		$where = " AND `orderDocumentsAddressName` LIKE '%" . trim($_REQUEST["searchCustomerName"]) . "%'";
	}
	else if(trim($_REQUEST["searchDocumentNumber"]) != '') {
		$where = " AND `orderDocumentsNumber` LIKE '%" . trim($_REQUEST["searchDocumentNumber"]) . "'";
	}
	else if(trim($_REQUEST["searchWord"]) != '') {
		$where = " AND (
					`orderDocumentsCustomerNumber` LIKE '%" . trim($_REQUEST["searchCustomerNumber"]) . "'
					OR
					`orderDocumentsAddressName` LIKE '%" . trim($_REQUEST["searchCustomerName"]) . "%'
					OR
					`orderDocumentsNumber` = '" . trim($_REQUEST["searchDocumentNumber"]) . "'
		)";
	}
	else if($_REQUEST["searchPaymentType"] != ''){
		$where = " AND `orderDocumentsPaymentType` = '" . ($_REQUEST["searchPaymentType"]) . "'";
	}
	else if($_REQUEST["searchCustomersOrderNumber"] != ''){
		$where = " AND `orderDocumentsCustomersOrderNumber` = '" . ($_REQUEST["searchCustomersOrderNumber"]) . "'";
	}
	else if(($_REQUEST["searchMonth"]) != '') {
		// $where = " AND SUBSTRING(`" . TABLE_ORDER_THIS . "`.`orderDocumentsDocumentDate`, 1, 7) = '" . ($_REQUEST["searchMonth"]) . "'";
		$where = " AND SUBSTRING(`" . TABLE_ORDER_THIS . "`.`orderDocumentsDocumentDate`, 1, " . strlen($_REQUEST["searchMonth"]) . ") = '" . ($_REQUEST["searchMonth"]) . "'";
	}

	$sql = "SELECT

				`" . TABLE_ORDER_THIS . "`.`orderDocumentsID`,
				`" . TABLE_ORDER_THIS . "`.`orderDocumentsNumber`,
				`" . TABLE_ORDER_THIS . "`.`orderDocumentsType`,
				`" . TABLE_ORDER_THIS . "`.`orderDocumentsOrdersIDs`,
				`" . TABLE_ORDER_THIS . "`.`orderDocumentsCustomerNumber`,
				`" . TABLE_ORDER_THIS . "`.`orderDocumentsCustomersOrderNumber`,
				`" . TABLE_ORDER_THIS . "`.`orderDocumentsProcessingDate`,
				`" . TABLE_ORDER_THIS . "`.`orderDocumentsDocumentDate`,
				`" . TABLE_ORDER_THIS . "`.`orderDocumentsDeliveryDate`,
				`" . TABLE_ORDER_THIS . "`.`orderDocumentsBindingDate`,
				`" . TABLE_ORDER_THIS . "`.`orderDocumentsSalesman`,
				`" . TABLE_ORDER_THIS . "`.`orderDocumentsAddressCompany`,
				`" . TABLE_ORDER_THIS . "`.`orderDocumentsAddressCompanyCustomerNumber`,
				`" . TABLE_ORDER_THIS . "`.`orderDocumentsAddressName`,
				`" . TABLE_ORDER_THIS . "`.`orderDocumentsAddressStreet`,
				`" . TABLE_ORDER_THIS . "`.`orderDocumentsAddressStreetNumber`,
				`" . TABLE_ORDER_THIS . "`.`orderDocumentsAddressZipcode`,
				`" . TABLE_ORDER_THIS . "`.`orderDocumentsAddressCity`,
				`" . TABLE_ORDER_THIS . "`.`orderDocumentsAddressCountry`,
				`" . TABLE_ORDER_THIS . "`.`orderDocumentsAddressMail`,
				`" . TABLE_ORDER_THIS . "`.`orderDocumentsSumPrice`,
				`" . TABLE_ORDER_THIS . "`.`orderDocumentsDiscount`,
				`" . TABLE_ORDER_THIS . "`.`orderDocumentsDiscountType`,
				`" . TABLE_ORDER_THIS . "`.`orderDocumentsDiscountPercent`,
				`" . TABLE_ORDER_THIS . "`.`orderDocumentsMwst`,
				`" . TABLE_ORDER_THIS . "`.`orderDocumentsMwstPrice`,
				`" . TABLE_ORDER_THIS . "`.`orderDocumentsShippingCosts`,
				`" . TABLE_ORDER_THIS . "`.`orderDocumentsPackagingCosts`,
				`" . TABLE_ORDER_THIS . "`.`orderDocumentsCashOnDelivery`,
				`" . TABLE_ORDER_THIS . "`.`orderDocumentsTotalPrice`,
				`" . TABLE_ORDER_THIS . "`.`orderDocumentsBankAccount`,
				`" . TABLE_ORDER_THIS . "`.`orderDocumentsPaymentCondition`,
				`" . TABLE_ORDER_THIS . "`.`orderDocumentsPaymentType`,
				`" . TABLE_ORDER_THIS . "`.`orderDocumentsDeliveryType`,
				`" . TABLE_ORDER_THIS . "`.`orderDocumentsDeliveryTypeNumber`,
				`" . TABLE_ORDER_THIS . "`.`orderDocumentsSubject`,
				`" . TABLE_ORDER_THIS . "`.`orderDocumentsContent`,
				`" . TABLE_ORDER_THIS . "`.`orderDocumentsDocumentPath`,
				`" . TABLE_ORDER_THIS . "`.`orderDocumentsTimestamp`,
				`" . TABLE_ORDER_THIS . "`.`orderDocumentsStatus`,
				`" . TABLE_ORDER_THIS . "`.`orderDocumentsSendMail`,

				`" . TABLE_ORDER_THIS . "`.`orderDocumentsIsCollectiveInvoice`,

			";
		if($_REQUEST["documentType"] != 'BR'){
			$sql .= "
				`" . TABLE_RELATED_DOCUMENTS . "`.`relatedDocuments_AN`,
				`" . TABLE_RELATED_DOCUMENTS . "`.`relatedDocuments_AB`,
				`" . TABLE_RELATED_DOCUMENTS . "`.`relatedDocuments_RE`,
				`" . TABLE_RELATED_DOCUMENTS . "`.`relatedDocuments_LS`,
				`" . TABLE_RELATED_DOCUMENTS . "`.`relatedDocuments_GU`,
				`" . TABLE_RELATED_DOCUMENTS . "`.`relatedDocuments_MA`,
				`" . TABLE_RELATED_DOCUMENTS . "`.`relatedDocuments_M1`,
				`" . TABLE_RELATED_DOCUMENTS . "`.`relatedDocuments_M2`,
				`" . TABLE_RELATED_DOCUMENTS . "`.`relatedDocuments_M3`,
				`" . TABLE_RELATED_DOCUMENTS . "`.`relatedDocuments_collectiveABs`,

				IF(
					LENGTH(
						CONCAT(
							IF(`" . TABLE_RELATED_DOCUMENTS . "`.`relatedDocuments_AN` IS NULL, '', `relatedDocuments_AN`),
							IF(`" . TABLE_RELATED_DOCUMENTS . "`.`relatedDocuments_AB` IS NULL, '', `relatedDocuments_AB`),
							IF(`" . TABLE_RELATED_DOCUMENTS . "`.`relatedDocuments_RE` IS NULL, '', `relatedDocuments_RE`),
							IF(`" . TABLE_RELATED_DOCUMENTS . "`.`relatedDocuments_LS` IS NULL, '', `relatedDocuments_LS`),
							IF(`" . TABLE_RELATED_DOCUMENTS . "`.`relatedDocuments_GU` IS NULL, '', `relatedDocuments_GU`),
							IF(`" . TABLE_RELATED_DOCUMENTS . "`.`relatedDocuments_MA` IS NULL, '', `relatedDocuments_MA`),
							IF(`" . TABLE_RELATED_DOCUMENTS . "`.`relatedDocuments_M1` IS NULL, '', `relatedDocuments_M1`),
							IF(`" . TABLE_RELATED_DOCUMENTS . "`.`relatedDocuments_M2` IS NULL, '', `relatedDocuments_M2`),
							IF(`" . TABLE_RELATED_DOCUMENTS . "`.`relatedDocuments_M3` IS NULL, '', `relatedDocuments_M3`),
							IF(`" . TABLE_RELATED_DOCUMENTS . "`.`relatedDocuments_collectiveABs` IS NULL, '', `relatedDocuments_collectiveABs`)
						)
					)
					>
					LENGTH(
						IF(
							`" . TABLE_RELATED_DOCUMENTS . "`.`relatedDocuments_" . $_REQUEST["documentType"]. "` IS NULL, '',
							`relatedDocuments_" . $_REQUEST["documentType"]. "`
						)
					),
					1,
					0
				) AS `documentWasConverted`,
			";
		}

		if($_REQUEST["documentType"] != 'BR'){
			$sql .= "
					`" . TABLE_CUSTOMERS . "`.`customersKundennummer`,
					`" . TABLE_CUSTOMERS . "`.`customersFirmenname`,
					`" . TABLE_CUSTOMERS . "`.`customersCompanyPLZ`,
					`" . TABLE_CUSTOMERS . "`.`customersCompanyOrt`,
				";
		}
		if(in_array($_REQUEST["documentType"], array('MA', 'M1', 'M2', 'M3'))) {
			$sql .= "
					`" . TABLE_ORDER_THIS . "`.`orderDocumentsInterestPercent`,
					`" . TABLE_ORDER_THIS . "`.`orderDocumentsInterestPrice`,
					`" . TABLE_ORDER_THIS . "`.`orderDocumentsChargesPrice`,
					DATE_ADD(`" . TABLE_ORDER_THIS . "`.`orderDocumentsDocumentDate`, INTERVAL " . constant("PAYMENT_DEADLINE_" . $_REQUEST["documentType"]) . " DAY) AS `orderDocumentsDeadline`,
				";
		}
		else if($_REQUEST["documentType"] == 'RE') {
			$sql .= "
					IF(`orderDocumentsPaymentCondition` = '1', DATE_ADD(`orderDocumentsDocumentDate`, INTERVAL 10 DAY),
						IF(`orderDocumentsPaymentCondition` = '5', DATE_ADD(`orderDocumentsDocumentDate`, INTERVAL 14 DAY),
							IF(`orderDocumentsPaymentCondition` = '6', DATE_ADD(`orderDocumentsDocumentDate`, INTERVAL 30 DAY),
								DATE_ADD(`orderDocumentsDocumentDate`, INTERVAL 7 DAY)
							)
						)
					) AS `orderDocumentsDeadline`,
				";
		}
		$sql .= "
					'0' AS `documentWasConverted`

				FROM `" . TABLE_ORDER_THIS . "`
			";
		if($_REQUEST["documentType"] != 'BR'){
			$sql .= "
				LEFT JOIN `" . TABLE_RELATED_DOCUMENTS . "`
				ON(`" . TABLE_ORDER_THIS . "`.`orderDocumentsNumber` = `" . TABLE_RELATED_DOCUMENTS . "`.`relatedDocuments_" . $_REQUEST["documentType"]. "`)
			";
		}

		if($_REQUEST["documentType"] != 'BR'){
			$sql .= "
				LEFT JOIN `" . TABLE_CUSTOMERS . "`
				ON(`" . TABLE_ORDER_THIS . "`.`orderDocumentsCustomerNumber` = `" . TABLE_CUSTOMERS . "`.`customersKundennummer`)
			";
		}

		$sql .= "
			WHERE 1 " . $where . "

			GROUP BY `orderDocumentsNumber`

			ORDER BY `orderDocumentsNumber` DESC
	";
	if($_REQUEST["documentType"] != 'BR'){
		$sql .= ", `relatedDocuments_collectiveABs` ASC ";
	}

	// BOF GET COUNT ALL ROWS
		$sql_getAllRows = "
				SELECT

					COUNT(`" . TABLE_ORDER_THIS . "`.`orderDocumentsType`) AS `countAllRows`

				FROM `" . TABLE_ORDER_THIS . "`

				WHERE 1
					" . $where . "
			";

		$rs_getAllRows = $dbConnection->db_query($sql_getAllRows);
		list(
			$countAllRows
		) = mysqli_fetch_array($rs_getAllRows);
	// EOF GET COUNT ALL ROWS



	if($_REQUEST["page"] == "" || $_REQUEST["page"] == 0) {
		$_REQUEST["page"] = 1;
	}

	if(MAX_DOCUMENTS_PER_PAGE > 0) {
		$sql .= " LIMIT " . (($_REQUEST["page"] - 1) * MAX_DOCUMENTS_PER_PAGE) . ", " . MAX_DOCUMENTS_PER_PAGE." ";
	}

	$rs = $dbConnection->db_query($sql);

	// OLD BOF GET ALL ROWS
		// $sql_totalRows = "SELECT FOUND_ROWS() AS `countRows`";
		// $rs_totalRows = $dbConnection->db_query($sql_totalRows);
		// list($countRows) = mysqli_fetch_array($rs_totalRows);
	// OLD EOF GET ALL ROWS

	$countRows = $countAllRows;

	$pagesCount = ceil($countRows / MAX_DOCUMENTS_PER_PAGE);

	while($ds = mysqli_fetch_assoc($rs)) {
		foreach(array_keys($ds) as $field){
			$arrDocumentDatas[$ds["orderDocumentsID"]][$field] = $ds[$field];
		}
	}

	// EOF GET SELECTED DOCUMENT DATAS
?>
<?php
	require_once('inc/headerHTML.inc.php');
	$thisTitle = $arrDocumentTypeDatas[$_REQUEST["documentType"]]["createdDocumentsTypesName2"] . " ansehen";
	if($_REQUEST["searchMonth"] != ""){
		$thisTitle .= ' -  <span class="headerSelectedEntry">Datum: ' . $_REQUEST["searchMonth"] . '</span>';
	}
	else if($_REQUEST["searchPaymentType"] != ""){
		$thisTitle .= ' - <span class="headerSelectedEntry">Zahlart: ' . $arrPaymentTypeDatas[$_REQUEST["searchPaymentType"]]["paymentTypesName"] . ' (' . $arrPaymentTypeDatas[$_REQUEST["searchPaymentType"]]["paymentTypesShortName"] . ')</span>';
	}
	$headerHTML = preg_replace("/{###TITLE###}/", ($thisTitle), $headerHTML);
	$headerHTML = preg_replace("/{###TITLE###}/", strip_tags($thisTitle), $headerHTML);
	echo $headerHTML;

	require_once(FILE_MENUE_TOP);
?>
<div id="mainArea">
	<div id="mainContent">
		<?php require_once(FILE_MENUE_SIDEBAR); ?>
		<div id="contentArea">
			<div id="contentAreaElements">
				<h1><?php if(SHOW_PAGE_TITLE_ICONS) { echo '<img src="' . PATH_ICONS_MENUE_TITLES . 'documents4.png' . '" alt="" />'; } ?> <?php echo $thisTitle; ?></h1>

				<div id="searchFilterArea">
					<form name="formFilterSearch" method="get" action="<?php echo $_SERVER["REDIRECT_URL"]; ?>">
						<table border="0" cellpadding="0" cellspacing="0" class="searchFilterContent">
							<tr>
								<td>
									<label for="searchCustomerNumber">Kunden-Nr:</label>
									<input type="text" name="searchCustomerNumber" id="searchCustomerNumber" class="inputField_70" value="" />
								</td>
								<td>
									<label for="searchCustomerName">Kundenname:</label>
									<input type="text" name="searchCustomerName" id="searchCustomerName" class="inputField_100" value="" />
								</td>
								<td>
									<label for="searchDocumentNumber">Dokument-Nr:</label>
									<input type="text" name="searchDocumentNumber" id="searchDocumentNumber" class="inputField_100" value="" />
								</td>
								<td>
									<label for="searchCustomersOrderNumber">Kundenauftrags-Nr:</label>
									<input type="text" name="searchCustomersOrderNumber" id="searchCustomersOrderNumber" class="inputField_100" value="" />
								</td>
								<td>
									<label for="searchMonth">Datum:</label>
									<select id="searchMonth" name="searchMonth" class="inputSelect_70">
										<option value=""></option>
										<?php
											$todayYear = intval(date("Y"));
											$todayMonth = intval(date("m"));
										?>

										<?php
											// BOF ADD YEAR
											for($k = $todayYear ; $k > 2010 ; $k = $k - 1){
												$thisYear = $k;
												$thisOptionClass = 'row0';
												if($thisYear == $todayYear){ $thisOptionClass = 'row3'; }
												$thisValue = $thisYear;
												$selected = '';
												if($thisValue == $_REQUEST["searchMonth"]){
													$selected = ' selected="selected" ';
												}
												echo '<option class="' . $thisOptionClass . '" value="' . $thisValue . '" ' . $selected . '>' . $thisYear . '</option>';
											}
											// EOF ADD YEAR
										?>

										<?php
											for($k = $todayYear ; $k > 2010 ; $k = $k - 1){
												$thisYear = $k;
												$thisOptionClass = 'row0';
												if($thisYear == $todayYear){ $thisOptionClass = 'row3'; }
												else if($thisYear%2 == 0){ $thisOptionClass = 'row1'; }
												for($i = 12 ; $i > 0 ; $i = $i - 1){
													$thisMonth = $i;
													if($thisMonth < 10){ $thisMonth = "0" . $thisMonth; }
													if(($k == $todayYear && $i <= $todayMonth) || ($k < $todayYear)){
														$thisValue = $thisYear . '-' . $thisMonth;
														$selected = '';
														if($thisValue == $_REQUEST["searchMonth"]){
															$selected = ' selected="selected" ';
														}
														echo '<option class="' . $thisOptionClass . '" value="' . $thisValue . '" ' . $selected . '>' . $thisMonth . '.' . $thisYear . '</option>';
													}
												}
											}
										?>
									</select>
								</td>
								<td>
									<label for="searchPaymentType">Zahlart:</label>
									<select name="searchPaymentType" id="searchPaymentType" class="inputSelect_120">
										<option value=""></option>
										<?php
											if(!empty($arrPaymentTypeDatas)){
												foreach($arrPaymentTypeDatas as $thisPaymentTypeKey => $thisPaymentTypeValue){
													$selected = '';
													if($thisPaymentTypeKey == $_REQUEST["searchPaymentType"]){
														$selected = ' selected="selected" ';
													}
													echo '<option value="' . $thisPaymentTypeKey . '" ' . $selected . '>' . $thisPaymentTypeValue["paymentTypesName"] . ' (' . $thisPaymentTypeValue["paymentTypesShortName"] . ')</option>';
												}
											}
										?>
									</select>
								</td>
								<!--
								<td>
									<label for="searchPLZ">PLZ:</label>
									<input type="text" name="searchPLZ" id="searchPLZ" class="inputField_40" />
								</td>
								-->
								<td>
									<label for="searchWord">Suchbegriff:</label>
									<input type="text" name="searchWord" id="searchWord" class="inputField_100" />
								</td>
								<td>
									<input type="submit" name="submitSearch" class="inputButton0" value="Suchen" />
								</td>
							</tr>
						</table>
					</form>
				</div>

				<?php
					if($warningMessage != "") {	echo '<p class="warningArea">'.$warningMessage.'</p>'; }
					if($errorMessage != "") {	echo '<p class="errorArea">'.$errorMessage.'</p>'; }
					if($successMessage != "") {	echo '<p class="successArea">'.$successMessage.'</p>'; }
					if($infoMessage != "") {	echo '<p class="infoArea">'.$infoMessage.'</p>'; }
				?>

				<?php
					echo '<p style="text-align:left; font-size:13px;">';
					echo '<span style="padding: 0 60px 0 0;"><b>' . $countRows . '</b> Dokumente' . '</span>';
					echo '</p>';
				?>

				<?php
					if($pagesCount > 1) {
						include(FILE_MENUE_PAGES);
					}

					if(!empty($arrDocumentDatas)) {
						echo '<table border="0" cellpadding="0" cellspacing="0" width="100%" class="displayOrders">';

						echo '<colgroup>';
						echo '<col />';
						echo '<col />';
						if($_REQUEST["documentType"] == 'RE' || $_REQUEST["documentType"] == 'MA' || $_REQUEST["documentType"] == 'M1' || $_REQUEST["documentType"] == 'M2'){
							echo '<col />';
						}
						echo '<col />';
						echo '<col />';
						echo '<col />';

						if($_REQUEST["documentType"] != 'BR') {
								echo '<col />';
							if(in_array($_REQUEST["documentType"], array('MA', 'M1', 'M2', 'M3'))) {
								echo '<col />';
								echo '<col />';
							}
							echo '<col />';
							// BOF COPY RE
							#if($arrGetUserRights["copyInvoices"] == '1' &&  $_REQUEST["documentType"] == 'RE'){
							if($_REQUEST["documentType"] == 'RE'){
								echo '<col />';
							}
							// EOF COPY RE
						}
						echo '<col />';
						echo '</colgroup>';

						echo '<thead>';
						echo '<tr>';
						echo '<th style="width:45px;text-align:right;">#</th>';
						echo '<th>Erstell-Datum</th>';
						if($_REQUEST["documentType"] == 'RE' || $_REQUEST["documentType"] == 'MA' || $_REQUEST["documentType"] == 'M1' || $_REQUEST["documentType"] == 'M2'){
							echo '<th>Frist-Datum</th>';
						}
						echo '<th>Kundennr</th>';
						echo '<th>Empf&auml;ngername</th>';
						echo '<th>Dokument-Nr</th>';

						if($_REQUEST["documentType"] != 'BR') {
							echo '<th>Zahlart</th>';
							echo '<th>RE-Betrag</th>';
							if(in_array($_REQUEST["documentType"], array('MA', 'M1', 'M2', 'M3'))) {
								echo '<th>Mahnkosten</th>';
								echo '<th>Gesamt</th>';
							}
							echo '<th>Umwandeln in:</th>';
							// BOF COPY RE
								#if($arrGetUserRights["copyInvoices"] == '1' &&  $_REQUEST["documentType"] == 'RE'){
								if($_REQUEST["documentType"] == 'RE'){
									echo '<th>RE Kopieren in:</th>';
								}
							// EOF COPY RE
						}

						echo '<th>Info</th>';
						echo '</tr>';
						echo '</thead>';

						$count = 0;

						$arrThisPath = parse_url($_SERVER["REQUEST_URI"]);
						$linkPath = $arrThisPath["path"];

						echo '<tbody>';
						foreach($arrDocumentDatas as $thisKey => $thisValue){
							if($count%2 == 0){ $rowClass = 'row0'; }
							else { $rowClass = 'row1'; }

							$thisStyle = '';
							if($arrDocumentDatas[$thisKey]["orderDocumentsStatus"] == '3'){
								$thisStyle = ' style="font-style:italic; color:#FF0000;" ';
							}
							else if($arrDocumentDatas[$thisKey]["orderDocumentsStatus"] == '2'){
								$thisStyle = '';
								$rowClass = 'row3';
							}
							else if($arrDocumentDatas[$thisKey]["orderDocumentsStatus"] == "10") {
								$rowClass = 'row3';
							}
							else if($arrDocumentDatas[$thisKey]["orderDocumentsStatus"] == "5") {
								$rowClass = 'row3';
							}

							echo '<tr class="'.$rowClass.'" ' . $thisStyle . '>';

							echo '<td style="text-align:right;" title="' . $arrDocumentDatas[$thisKey]["orderDocumentsStatus"] . '|' . $arrPaymentStatusTypeDatas[$arrDocumentDatas[$thisKey]["orderDocumentsStatus"]]["paymentStatusTypesName"] . '"><b>' . ($count + 1). '.</b></td>';

							echo '<td>' . formatDate($arrDocumentDatas[$thisKey]["orderDocumentsDocumentDate"], 'display') . '</td>';

							if($_REQUEST["documentType"] == 'RE' || $_REQUEST["documentType"] == 'MA' || $_REQUEST["documentType"] == 'M1' || $_REQUEST["documentType"] == 'M2'){
								echo '<td>' . formatDate($arrDocumentDatas[$thisKey]["orderDocumentsDeadline"], 'display') . '</td>';
							}

							#echo '<td><a href="' . PAGE_EDIT_CUSTOMER . '?editCustomerNumber=' . $arrDocumentDatas[$thisKey]["orderDocumentsCustomerNumber"] . '" title="Kunden anzeigen">' . $arrDocumentDatas[$thisKey]["orderDocumentsCustomerNumber"] . '</a></td>';
							echo '<td>';

							$thisSalesmenKundennummer = $arrDocumentDatas[$thisKey]["orderDocumentsAddressCompanyCustomerNumber"];

							if(in_array($thisSalesmenKundennummer, $arrMandatoriesCustomerNumbers) || $thisSalesmenKundennummer == ''){
								$thisSalesmenKundennummer = $ds["orderDocumentsCustomerNumber"];
							}

							echo '<a href="' . getCustomersSourceTableFromCustomerNumber($arrDocumentDatas[$thisKey]["orderDocumentsCustomerNumber"]) . '?editCustomerNumber=' . $thisSalesmenKundennummer . '" title="Kunden anzeigen">';
							echo '<b>' . $thisSalesmenKundennummer . '</b>';
							echo '</a>';
							echo '<div style="font-size:10px;border-top:1px dotted #333;">';
							echo '<span title="Lieferungs-Empf&auml;nger Kundendaten ansehen" style="padding-left:10px;">';
							echo '<a href="' . getCustomersSourceTableFromCustomerNumber($thisSalesmenKundennummer) . '?editCustomerNumber=' . $arrDocumentDatas[$thisKey]["orderDocumentsCustomerNumber"] . '" title="Kunden anzeigen">';
							echo $arrDocumentDatas[$thisKey]["orderDocumentsCustomerNumber"];
							echo '</a>';
							echo '</span>';
							echo '</div>';
							echo '</td>';

							echo '<td style="white-space:nowrap;">';
							echo '<b>' . htmlentities(($arrDocumentDatas[$thisKey]["orderDocumentsAddressCompany"])) . '</b>';
							echo '<div style="font-size:10px;border-top:1px dotted #333;">';
							echo '<span title="Lieferungs-Empf&auml;nger Kundendaten ansehen" style="padding-left:10px;">';
							echo htmlentities(($arrDocumentDatas[$thisKey]["customersFirmenname"]));
							echo ' &bull; ';
							echo $arrDocumentDatas[$thisKey]["customersCompanyPLZ"] . ' ' . htmlentities(($arrDocumentDatas[$thisKey]["customersCompanyOrt"]));
							echo '</span>';
							echo '</div>';
							echo '</td>';

							echo '<td>';
								if($arrDocumentDatas[$thisKey]["orderDocumentsIsCollectiveInvoice"] == '1'){
									echo '<div style="margin:0 0 -3px 0;padding:0;color:#FF0000;font-weight:bold;font-size:10px;">';
									#if($arrDocumentDatas[$thisKey]["orderDocumentsType"] == 'AB'){ echo 'Die AB ist Teil einer SAMMELRECHNUNG'; }
									if($arrDocumentDatas[$thisKey]["orderDocumentsType"] == 'AB'){ echo 'in SAMMELRECHNUNG'; }
									else { echo 'SAMMELRECHNUNG'; }
									echo '</div>';
								}
								if($arrDocumentDatas[$thisKey]["orderDocumentsStatus"] == '3' || $arrDocumentDatas[$thisKey]["orderDocumentsStatus"] == '2'){
									echo '<b>' . $arrDocumentDatas[$thisKey]["orderDocumentsNumber"] . '</b>';
								}
								else {
									echo '<form name="formSubmitEditDocument" method="post" action="' . PAGE_CREATE_DOCUMENT . '">';
										echo '<table width="150" cellpadding="0" cellspacing="0" class="noBorder">';
										echo '<tr>';
											echo '<td>';
												$thisDocumentLink = '';
												$thisDocumentLinkDocType = substr($arrDocumentDatas[$thisKey]["orderDocumentsNumber"], 0, 2);
												if($thisDocumentLinkDocType != 'AN'){
													$thisDocumentsPage = PAGE_ALL_INVOICES;
													if($thisDocumentLinkDocType == 'LS'){
														$thisDocumentsPage = PAGE_DISPLAY_DELIVERY;
													}
													else if($thisDocumentLinkDocType == 'RE'){
														$thisDocumentsPage = PAGE_ALL_INVOICES;
													}
													else if($thisDocumentLinkDocType == 'AB'){
														$thisDocumentsPage = PAGE_ALL_CONFIRMATIONS;
													}
													else if($thisDocumentLinkDocType == 'GU'){
														$thisDocumentsPage = PAGE_ALL_CREDITS;
													}

													$thisDocumentLink .= $thisDocumentsPage;
													$thisDocumentLink .= '?searchDocumentNumber=' . $arrDocumentDatas[$thisKey]["orderDocumentsNumber"];
													$thisDocumentLink .= '&amp;documentType=' . $thisDocumentLinkDocType;
												}
												if($thisDocumentLink != '') { echo '<a href="' . $thisDocumentLink . '">'; }
												echo '<b>' . $arrDocumentDatas[$thisKey]["orderDocumentsNumber"] . '</b>';
												if($thisDocumentLink != '') { echo '</a>'; }
											echo '</td>';
											echo '<td>';
												if($arrDocumentDatas[$thisKey]["documentWasConverted"] != '1') {
													$isDocEditable = checkIfDocumentIsEditable($_REQUEST["documentType"], $arrDocumentDatas[$thisKey]["orderDocumentsNumber"]);

													if($isDocEditable){
														echo '<input type="hidden" name="editID" value="' . $arrDocumentDatas[$thisKey]["orderDocumentsCustomerNumber"] . '" />';
														echo '<input type="hidden" name="originDocumentType" value="' . $_REQUEST["documentType"] . '" />';
														echo '<input type="hidden" name="originDocumentID" value="' . $thisKey . '" />';
														echo '<input type="hidden" name="originDocumentNumber" value="' . $arrDocumentDatas[$thisKey]["orderDocumentsNumber"] . '" />';
														echo '<input type="hidden" name="editDocType" value="' . $_REQUEST["documentType"] . '" />';
														echo '<input type="image" src="layout/icons/iconEdit.gif" title="' . $arrDocumentTypeDatas[$_REQUEST["documentType"]]["createdDocumentsTypesName"] . ' ' . $arrDocumentDatas[$thisKey]["orderDocumentsDocumentNumber"] . ' &auml;ndern" />';
													}
													else {
														echo '<img src="layout/icons/iconNotOk.png" width="16" height="16" alt="&Auml;nderung nicht mehr m&ouml;glich!" title="&Auml;nderung nicht mehr m&ouml;glich!" />';
													}
												}
											echo '</td>';
										echo '</tr>';
										echo '</table>';
										echo '</form>';
								}
							echo '</td>';

							if($_REQUEST["documentType"] != 'BR') {
								echo '<td style="font-size:11px;">';
								echo $arrPaymentTypeDatas[$arrDocumentDatas[$thisKey]["orderDocumentsPaymentType"]]["paymentTypesName"];
								echo '</td>';

								echo '<td style="text-align:right;white-space:nowrap;background-color:#FEFFAF;font-weight:bold;">';
								echo convertDecimal($arrDocumentDatas[$thisKey]["orderDocumentsTotalPrice"], 'display') . ' &euro;';
								echo '</td>';

								if(in_array($_REQUEST["documentType"], array('MA', 'M1', 'M2', 'M3'))) {
									echo '<td style="text-align:right;white-space:nowrap;background-color:#FFDFDF;font-weight:bold;">';
									echo convertDecimal(($arrDocumentDatas[$thisKey]["orderDocumentsInterestPrice"] + $arrDocumentDatas[$thisKey]["orderDocumentsChargesPrice"]), 'display') . ' &euro;';
									echo '</td>';

									echo '<td style="text-align:right;white-space:nowrap;background-color:#FFDFDF;font-weight:bold;">';
									echo convertDecimal(($arrDocumentDatas[$thisKey]["orderDocumentsTotalPrice"] + $arrDocumentDatas[$thisKey]["orderDocumentsInterestPrice"] + $arrDocumentDatas[$thisKey]["orderDocumentsChargesPrice"]), 'display') . ' &euro;';
									echo '</td>';
								}

								echo '<td>';
								if($arrDocumentDatas[$thisKey]["orderDocumentsStatus"] != '3'){
									$arrConvertedDocTypes =	"";
									if($arrDocumentDatas[$thisKey]["documentWasConverted"] == '1') {
										echo '<table width="160" cellpadding="0" cellspacing="0" class="noBorder">';
										echo '<tr>';
											echo '<td>';
											echo '<img src="layout/icons/isConverted.png" width="8" height="12" alt="" title="" /> ';
											echo $arrDocumentDatas[$thisKey]["documentsToDocumentsCreatedDocumentNumber"];
											$thisConvertDocumentType = substr($arrDocumentDatas[$thisKey]["documentsToDocumentsCreatedDocumentNumber"], 0, 2);
											#echo '<a href="' . PAGE_DOWNLOAD_DOCUMENTS . '?searchBoxFile=' . $arrDocumentDatas[$thisKey]["documentsToDocumentsCreatedDocumentNumber"] . '">' . $arrDocumentDatas[$thisKey]["documentsToDocumentsCreatedDocumentNumber"] . '</a>';
											echo '</td>';
											echo '<td>';
											#echo '<img src="layout/icons/iconOk.png" width="16" height="16" title="Zu ' . $arrDocumentDatas[$thisKey]["documentsToDocumentsCreatedDocumentNumber"] . ' umgewandelt" alt="" style="cursor:pointer;" />';
											echo '<a href="' . $arrDocumentUrl[$thisConvertDocumentType] . '?searchDocumentNumber=' . $arrDocumentDatas[$thisKey]["documentsToDocumentsCreatedDocumentNumber"] . '">';
											echo '<img src="layout/icons/convertDocument.png" width="16" height="16" title="Dokument umwandeln" alt="Dokument umwandeln" />';
											echo '</a>';
											echo '</td>';
										echo '</tr>';
										echo '</table>';
										#echo '<div class="hr" style="height:1px; margin:-10px 0 0 0; padding:0;border-bottom: 1px dotted #333333;"></div>';
									}
									#if(1) {
									else {
										$arrConvertedDocTypes =	array(
												substr($arrDocumentDatas[$thisKey]["relatedDocuments_AN"], 0, 2),
												substr($arrDocumentDatas[$thisKey]["relatedDocuments_AB"], 0, 2),
												substr($arrDocumentDatas[$thisKey]["relatedDocuments_RE"], 0, 2),
												substr($arrDocumentDatas[$thisKey]["relatedDocuments_LS"], 0, 2),
												substr($arrDocumentDatas[$thisKey]["relatedDocuments_GU"], 0, 2),
												substr($arrDocumentDatas[$thisKey]["relatedDocuments_MA"], 0, 2),
												substr($arrDocumentDatas[$thisKey]["relatedDocuments_M1"], 0, 2),
												substr($arrDocumentDatas[$thisKey]["relatedDocuments_M2"], 0, 2),
												substr($arrDocumentDatas[$thisKey]["relatedDocuments_M3"], 0, 2)
											);										$arrConvertedDocTypes = array_unique($arrConvertedDocTypes);
										#dd('arrConvertedDocTypes');

										if(
											$arrDocumentDatas[$thisKey]["orderDocumentsIsCollectiveInvoice"] != '1'
											||
											($arrDocumentDatas[$thisKey]["orderDocumentsIsCollectiveInvoice"] == '1' && $_REQUEST["documentType"] != 'AB')
										) {
											echo '<form name="formSubmitConvertDocument" method="post" action="' . PAGE_CREATE_DOCUMENT . '">';
											echo '<table width="160" cellpadding="0" cellspacing="0" class="noBorder">';
											echo '<tr>';
											echo '<td>';
											echo '<input type="hidden" name="editID" value="' . $arrDocumentDatas[$thisKey]["orderDocumentsCustomerNumber"] . '" />';
											echo '<input type="hidden" name="originDocumentType" value="' . $_REQUEST["documentType"] . '" />';
											echo '<input type="hidden" name="originDocumentID" value="' . $thisKey . '" />';
											echo '<input type="hidden" name="originDocumentNumber" value="' . $arrDocumentDatas[$thisKey]["orderDocumentsNumber"] . '" />';
											$options = createSelectDocTypes($_REQUEST["documentType"], 'convert', $arrConvertedDocTypes, $arrDocumentDatas[$thisKey]["orderDocumentsStatus"]);

											if($options != "") {
												echo '<img src="layout/icons/doConvert.png" width="8" height="12" alt="" title="" />';
												echo '<select name="convertDocType" class="convertDocType">';
												echo $options;
												echo '</select>';
											}
											else {
												echo '<img src="layout/icons/noConvert.png" width="8" height="12" alt="" title="" />';
												echo ' nicht m&ouml;glich';
											}
											echo '</td>';
											echo '<td>';
											// echo '<a href="' . PAGE_CREATE_THIS . '?editID=' . $arrDocumentDatas[$thisKey]["orderDocumentsCustomerNumber"] . '&originDocumentID=' . $thisKey . '&originDocumentType=' . $_REQUEST["documentType"] . '"><img src="layout/icons/convertDocument.png" width="16" height="16" title="Dokument umwandeln in" alt="Dokument umwandeln" /></a>';
											if($options != "") {
												echo '<input type="image" src="layout/icons/convertDocument.png" title="' . $arrDocumentTypeDatas[$_REQUEST["documentType"]]["createdDocumentsTypesName"] . ' umwandeln" />';
											}
											echo '</td>';
											echo '</tr>';
											echo '</table>';
											echo '</form>';
										}
									}
								}
								echo '</td>';

								// BOF GET RELATED DOCUMENTS
									if($_REQUEST["documentType"] != 'BR' && $_REQUEST["documentType"] != 'AN') {
										$arrThisRelatedDocuments = array();
										if(!empty($arrDocumentTypeDatas)){
											foreach($arrDocumentTypeDatas as $thisDocumentTypeDatasKey => $thisDocumentTypeDatasValue){
												if(preg_match("/;/", $arrDocumentDatas[$thisKey]["relatedDocuments_" . $thisDocumentTypeDatasKey])){
													#relatedDocuments_LS] => LS-1511000226;LS-1511000227
													$arrThisRelatedDocuments_Temp = explode(";", $arrDocumentDatas[$thisKey]["relatedDocuments_" . $thisDocumentTypeDatasKey]);
													if(!empty($arrThisRelatedDocuments)){
														$arrThisRelatedDocuments = array_merge($arrThisRelatedDocuments, $arrThisRelatedDocuments_Temp);
													}
													else if(!empty($arrThisRelatedDocuments_Temp)){
														$arrThisRelatedDocuments = $arrThisRelatedDocuments_Temp;
													}
												}
												else if($arrDocumentDatas[$thisKey]["relatedDocuments_" . $thisDocumentTypeDatasKey] != ""){
													$arrThisRelatedDocuments[] = $arrDocumentDatas[$thisKey]["relatedDocuments_" . $thisDocumentTypeDatasKey];
												}
											}
										}

										if($arrDocumentDatas[$thisKey]["relatedDocuments_collectiveABs"] != ""){
											$arrThisRelatedDocuments_collectiveABs = explode(";", $arrDocumentDatas[$thisKey]["relatedDocuments_collectiveABs"]);
											if(!empty($arrThisRelatedDocuments)){
												$arrThisRelatedDocuments = array_merge($arrThisRelatedDocuments, $arrThisRelatedDocuments_collectiveABs);
											}
											else if(!empty($arrThisRelatedDocuments_collectiveABs)){
												$arrThisRelatedDocuments = $arrThisRelatedDocuments_collectiveABs;
											}
										}
										$arrThisRelatedDocuments = array_unique($arrThisRelatedDocuments);
										sort($arrThisRelatedDocuments);

										if(!empty($arrThisRelatedDocuments)){
											$arrTempThisRelatedDocuments = $arrThisRelatedDocuments;
											$arrThisRelatedDocuments = array();
											foreach($arrTempThisRelatedDocuments as $thisTempThisRelatedDocument){
												$arrThisRelatedDocuments[substr($thisTempThisRelatedDocument, 0, 2)][] = $thisTempThisRelatedDocument;
											}
										}
									}
								// EOF GET RELATED DOCUMENTS

								// BOF TEST COPY RE
									#if($arrGetUserRights["copyInvoices"] == '1' &&  $_REQUEST["documentType"] == 'RE'){
									if($_REQUEST["documentType"] == 'RE'){
										echo '<td>';
										#dd('arrThisRelatedDocuments');
										// BOF COPY RE ONLY WHEN GU EXISTS
											if(!empty($arrThisRelatedDocuments["GU"])){
												echo '<p class="infoArea" style="font-size:10px;">GU vorhanden!</p>';
												echo '<form name="formSubmitConvertDocument" method="post" action="' . PAGE_CREATE_DOCUMENT . '">';
												echo '<table width="150" cellpadding="0" cellspacing="0" class="noBorder">';
												echo '<tr>';
												echo '<td>';
												echo '<input type="hidden" name="editID" value="' . $arrDocumentDatas[$thisKey]["orderDocumentsCustomerNumber"] . '" />';
												echo '<input type="hidden" name="originDocumentType" value="' . $_REQUEST["documentType"] . '" />';
												echo '<input type="hidden" name="originDocumentID" value="' . $thisKey . '" />';
												echo '<select name="copyDocType" class="copyDocType" />';
												##echo createSelectDocTypes($_REQUEST["documentType"], 'copy');
												echo '<option value="RE">Rechnung (RE)</option>';
												echo '</select>';
												echo '</td>';
												echo '<td>';
												// echo '<a href="' . PAGE_CREATE_THIS . '?editID=' . $arrDocumentDatas[$thisKey]["orderDocumentsCustomerNumber"] . '&originDocumentID=' . $thisKey . '&originDocumentType=' . $_REQUEST["documentType"] . '"><img src="layout/icons/copyDocument.png" width="16" height="16" title="Dokument kopieren" alt="Dokument kopieren" /></a>';
												echo '<input type="image" src="layout/icons/copyDocument.png" title="' . $arrDocumentTypeDatas[$_REQUEST["documentType"]]["createdDocumentsTypesName"] . ' kopieren" onclick="return showWarning(\'Sind Sie sicher, dass Sie dieses Dokument kopieren und nicht umwandeln wollen?\');" />';
												echo '</td>';
												echo '</tr>';
												echo '</table>';
												echo '</form>';
											}
											else {
												echo '<p class="infoArea" style="font-size:10px;">RE-Kopie nur m&ouml;glich, wenn GU existiert!</p>';
											}
										// EOF COPY RE ONLY WHEN GU EXISTS
										echo '</td>';
									}
								// EOF TEST COPY RE
							}
							$thisParsedUrl = parse_url($_SERVER["REQUEST_URI"]);
							echo '<td style="white-space:nowrap;">';
							if($_REQUEST["documentType"] == 'BR') {
								echo '<span class="toolItem">';
								echo '<a href="' . $thisParsedUrl["path"] . '?deleteFile=' . basename($arrDocumentDatas[$thisKey]["orderDocumentsDocumentPath"]) . '&amp;thisDocumentType=' . $_REQUEST["documentType"] . '&amp;thisDocumentNumber=' . $arrDocumentDatas[$thisKey]["orderDocumentsNumber"] . '" onclick="return showWarning(\'Möchten Sie die Datei &quot;' . basename($arrDocumentDatas[$thisKey]["orderDocumentsDocumentPath"]) . '&quot; wirklich entfernen?\');">' . '<img src="layout/icons/iconDelete.png" width="16" height="16" title="Dokument unwideruflich entfernen" alt="Entfernen" /></a>';
								echo '</span>';
							}

							echo '<span class="toolItem">';
							echo ' <img src="layout/icons/iconInfo.png" class="buttonPaymentInfo" rel="' . $arrDocumentDatas[$thisKey]["orderDocumentsNumber"] . '" title="Zahlungsdetails" alt="" width="12" height="12" />';
							echo '</span>';

							echo '<span class="toolItem">';
							echo '<a href="' . $thisParsedUrl["path"] . '?downloadFile=' . basename($arrDocumentDatas[$thisKey]["orderDocumentsDocumentPath"]) . '&amp;thisDocumentType=' . $_REQUEST["documentType"] . '">' . '<img src="layout/icons/icon' . getFileType(basename($arrDocumentDatas[$thisKey]["orderDocumentsDocumentPath"])) . '.gif" width="16" height="16" title="Dokument &quot;' . $arrDocumentDatas[$thisKey]["orderDocumentsNumber"] . '&quot; herunterladen" alt="Download" /></a>';
							echo '</span>';

							echo '<span class="toolItem">';
							echo '<img src="layout/icons/iconView.png" class="buttonLoadOrderDetails" width="16" height="16" title="Details ansehen" alt="' . $thisKey . '" />';
							echo '</span>';

							if($_REQUEST["documentType"] != 'BR' && $_REQUEST["documentType"] != 'AN') {
								echo '<div class="toolItem">';
								echo '<img src="layout/icons/linkedDocuments.png" class="buttonShowLinkedDocuments" width="16" height="16" alt="Verkn&uuml;pfte Dokumente" title="Verkn&uuml;pfte Dokumente anzeigen" />';
								echo '<div class="displayNoticeArea">';

								// #### REMOVED GET RELATED DOCUMENTS, MOVED TO TOP

								if(!empty($arrThisRelatedDocuments)){
									if($arrDocumentDatas[$thisKey]["orderDocumentsIsCollectiveInvoice"] == '1'){
										echo '<span style="color:#FF0000;font-weight:bold;font-size:10px;">';
										if($arrDocumentDatas[$thisKey]["orderDocumentsType"] == 'AB'){ echo 'Die AB ist Teil einer SAMMELRECHNUNG'; }
										else { echo 'SAMMELRECHNUNG'; }
										echo '</span><br />';
									}

									foreach($arrDocumentTypeDatas as $thisRelatedDocumentsKey => $thisRelatedDocumentsValue){
										if(!empty($arrThisRelatedDocuments[$thisRelatedDocumentsKey])){
											$countItem = 0;
											$thisItemStyle = 'padding-top: 4px;padding-bottom: 4px;border-top: 1px dotted #999;';
											echo '<ul style="' . $thisItemStyle . '">';
											foreach($arrThisRelatedDocuments[$thisRelatedDocumentsKey] as $thisRelatedDocumentsKey => $thisRelatedDocumentsValue){
												echo '<li>';
												echo '<a href="' . $linkPath . '?downloadFile=' . basename($thisRelatedDocumentsValue) . '_KNR-' . $arrDocumentDatas[$thisKey]["orderDocumentsCustomerNumber"] . '.pdf' . '&amp;thisDocumentType=' . substr($thisRelatedDocumentsValue, 0, 2) . '">';
												echo '<img src="layout/icons/iconPDF.gif" width="16" height="16" alt="Dokument herunterladen" title="' . $thisRelatedDocumentsValue. ' Dokument herunterladen" />';
												echo '</a>';
												echo ' ';
												echo $thisRelatedDocumentsValue . '';
												echo '</li>';
												$countItem++;
											}
											echo '</ul>';
										}
									}
								}

								echo '</div>';
								echo '</div>';
							}

							if($arrDocumentDatas[$thisKey]["orderDocumentsStatus"] != '3'){
								echo '<span class="toolItem">';
								echo '<img src="layout/icons/mailAttachement.png" class="buttonSendDocument" rel="' . urlencode(basename($arrDocumentDatas[$thisKey]["orderDocumentsDocumentPath"])) . '#' . $_REQUEST["documentType"] . '" width="16" height="16" title="Dokument &quot;' . $arrDocumentDatas[$thisKey]["orderDocumentsNumber"] . '&quot; direkt per Mail versenden" alt="Dokument versenden" />';
								echo '</span>';

								echo '<span class="toolItem">';
								echo '<span class="mailcontact">';
								echo '<a href="mailto:' . $arrDocumentDatas[$thisKey]["orderDocumentsAddressMail"] . '"><img src="layout/icons/iconMail.gif" title="Kunden per Mail kontaktieren" alt="Mailkontakt" /></a>';
								echo '</span>';
								echo '</span>';
							}

							echo '</td>';
							echo '</tr>';
							$count++;
						}
						echo '</tbody>';

						echo '</table>';

						if($pagesCount > 1) {
							include(FILE_MENUE_PAGES);
						}
					}
					else {
						echo '<p class="infoArea">Momentan liegen keine ' . $arrDocumentTypeDatas[$_REQUEST["documentType"]]["createdDocumentsTypesName2"] . ' vor.</p>';
					}
				?>
			</div>
		</div>
		<div class="clear"></div>
	</div>
	<div class="clear"></div>
</div>

<script language="javascript" type="text/javascript">
	$(document).ready(function() {
		$(function() {
			$( "#datepicker" ).datepicker();
		});
		$('.buttonClearField').live('click', function () {
			$(this).parent().find('input').val('');
		});
		colorRowMouseOver('.displayOrders tbody tr');
		$('#searchCustomerNumber').keyup(function () {
			loadSuggestions('searchCustomerNumber', [{'triggerElement': '#searchCustomerNumber', 'fieldNumber': '#searchCustomerNumber', 'fieldName': '#searchCustomerName', 'fieldCity': '', 'fieldZipCode': ''}], 1);
		});
		$('#searchCustomerName').keyup(function () {
			loadSuggestions('searchCustomerName', [{'triggerElement': '#searchCustomerName', 'fieldNumber': '#searchCustomerNumber', 'fieldName': '#searchCustomerName', 'fieldCity': '', 'fieldZipCode': ''}], 1);
		});
		$('.buttonLoadOrderDetails').click(function() {
			loadOrderDocumentDetails($(this).attr('alt'), '<?php echo $arrDocumentTypeDatas[$_REQUEST["documentType"]]["createdDocumentsTypesName"]; ?>', '<?php echo $_REQUEST["documentType"]; ?>');
		});

		$('.buttonSendDocument').click(function() {
			var mailDocumentFilename = $(this).attr('rel');
			var mailDocumentCustomerNumber = '';
			var mailDocumentRecipient = $(this).parent().parent().find('.mailcontact a').attr('href');
			mailDocumentRecipient = mailDocumentRecipient.replace('mailto:', '');
			sendAttachedDocument($(this), mailDocumentFilename, mailDocumentCustomerNumber, '<?php echo $_REQUEST["editID"]; ?>', '', '<?php echo htmlentities($_SERVER["REQUEST_URI"]); ?>', mailDocumentRecipient);
		});

		$('.buttonPaymentInfo').css('cursor', 'pointer');
		$('.buttonPaymentInfo').click(function () {
			loadPaymentDetails($(this), $(this).attr('rel'), '<?php echo $linkPath; ?>');
			//loadOrderDetails($(this), '<?php echo BASEPATH; ?>', 'Details der Zahlung');
		});

		$('.buttonShowLinkedDocuments').click(function () {
			loadNotice($(this));
		});
	});
</script>

<?php require_once('inc/footerHTML.inc.php'); ?>
