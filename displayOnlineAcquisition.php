<?php
	require_once('inc/requires.inc.php');

	$maxUsersPerPage = MAX_USERS_PER_PAGE;
	$maxUsersPerPage = 20;

	if(!$arrGetUserRights["displayOnlineAcquisition"] && !$arrGetUserRights["importOnlineAcquisition"]) {
		header('location: ' . PAGE_EXIT_LOCATION);
		exit;
	}

	// BOF DEFINE DUTY FIELDS
		/*
		$arrFormDutyFields = array(
			"editUserFirstName" => "Vorname",
			"editUserLastName" => "Nachname",
			"editUserLogin" => "Login"
		);
		// CREATE JSON DUTY FIELD
		$jsonFormDutyFields = createJson($arrFormDutyFields);
		*/
	// BOF DEFINE DUTY FIELDS

	$jswindowMessage = "";
	$warningMessage = '';
	$errorMessage = '';
	$successMessage = '';
	$infoMessage = '';

	$dbConnection = new DB_Connection();
	$db_open = $dbConnection->db_connect();

	if(DB_HOST_EXTERN_ACQUISITION != '' && DB_NAME_EXTERN_ACQUISITION != '' && DB_USER_EXTERN_ACQUISITION != '' && DB_PASSWORD_EXTERN_ACQUISITION) {
		$existsOnlineAquisition = true;
	}
	else {
		$existsOnlineAquisition = false;
		$errorMessage .= 'Es sind keine Zugansdaten f&uuml;r die Datenbankverbindung zur ' . strtoupper(MANDATOR) . '-Kundenerfassung eingetragen. ' . '<br />';
	}

	if($existsOnlineAquisition) {

	$dbConnection_ExternAcqisition = new DB_Connection(DB_HOST_EXTERN_ACQUISITION, '', DB_NAME_EXTERN_ACQUISITION, DB_USER_EXTERN_ACQUISITION, DB_PASSWORD_EXTERN_ACQUISITION);
	$db_openExternAcqisition = $dbConnection_ExternAcqisition->db_connect();
	if(!$db_openExternAcqisition){
		echo "error cnx";
		exit();
	}

	// BOF READ VERTRETER
		$arrSalesmenDatas = getSalesmen2();
		$arrAquisitionUsers = getAquisitionUsers();
	// EOF READ VERTRETER

	$where = "";

	$defaultORDER = "customersTimeCreated";
	$defaultSORT = "ASC";

	if($_REQUEST["sortField"] != "") {
		$thisORDER = $_REQUEST["sortField"];
	}
	else {
		$thisORDER = $defaultORDER;
	}

	if($_REQUEST["sortDirection"] != "") {
		$thisSORT = $_REQUEST["sortDirection"];
	}
	else {
		$thisSORT = $defaultSORT;
	}

	if($thisSORT == "DESC") { $thisSORT = "ASC"; }
	else { $thisSORT = "DESC"; }

	if(!(int)$_REQUEST["importID"] > 0) {

		if($_REQUEST["searchSalesmanOnlineID"] != "") {
			$where = " AND `customersUserID` = '" . $_REQUEST["searchSalesmanOnlineID"] . "'";
		}
		else if($_REQUEST["searchSalesman"] != "") {
			$where = " AND `customersVertreterName` LIKE '%" . $_REQUEST["searchSalesman"] . "%'";
		}
		else if($_REQUEST["searchWord"] != "") {

			$where = " AND (
							`customersCompanyPLZ` LIKE '" . addslashes($_REQUEST["searchWord"]) . "%'
							OR
							`customersCompanyOrt` LIKE '%" . addslashes($_REQUEST["searchWord"]) . "%'
							OR
							`customersKundennummer` = '" . addslashes($_REQUEST["searchWord"]) . "'
							OR
							`customersFirmenInhaberVorname` LIKE '%" . addslashes($_REQUEST["searchWord"]) . "%'
							OR
							`customersFirmenInhaberNachname` LIKE '%" . addslashes($_REQUEST["searchWord"]) . "%'
							OR
							`customersFirmenname` LIKE '%" . addslashes($_REQUEST["searchWord"]) . "%'
							OR
							`customersFirmennameZusatz` LIKE '%" . addslashes($_REQUEST["searchWord"]) . "%'
							OR
							`customersVertreterName` LIKE '%" . addslashes($_REQUEST["searchWord"]) . "%'
						) ";
		}
		else {
			$where = "";
		}

		if($_REQUEST["searchStatus"] !="") {
			if($_REQUEST["searchStatus"] == "0") {
				$where .= " AND (`customersKundennummer` = '' OR `customersKundennummer` IS NULL ) ";
			}
			else if($_REQUEST["searchStatus"] == "1") {
				$where .= " AND `customersKundennummer` != '' ";
			}
		}
	}
	if((int)$_REQUEST["importID"] > 0) {
		$where = " AND `customersID` = '" . (int)$_REQUEST["importID"] . "' ";
	}
	if((int)$_REQUEST["sendID"] > 0) {
		$where = " AND `customersID` = '" . (int)$_REQUEST["sendID"] . "' ";
	}


		$sql = "SELECT
						SQL_CALC_FOUND_ROWS

						`" . TABLE_ACQUISATION_CUSTOMERS . "`.`customersID`,
						`" . TABLE_ACQUISATION_CUSTOMERS . "`.`customersKundennummer`,
						`" . TABLE_ACQUISATION_CUSTOMERS . "`.`customersFirmenname`,
						`" . TABLE_ACQUISATION_CUSTOMERS . "`.`customersFirmennameZusatz`,
						`" . TABLE_ACQUISATION_CUSTOMERS . "`.`customersFirmenInhaberVorname`,
						`" . TABLE_ACQUISATION_CUSTOMERS . "`.`customersFirmenInhaberNachname`,
						`" . TABLE_ACQUISATION_CUSTOMERS . "`.`customersFirmenInhaberAnrede`,
						`" . TABLE_ACQUISATION_CUSTOMERS . "`.`customersFirmenInhaber2Vorname`,
						`" . TABLE_ACQUISATION_CUSTOMERS . "`.`customersFirmenInhaber2Nachname`,
						`" . TABLE_ACQUISATION_CUSTOMERS . "`.`customersFirmenInhaber2Anrede`,
						`" . TABLE_ACQUISATION_CUSTOMERS . "`.`customersUstID`,
						`" . TABLE_ACQUISATION_CUSTOMERS . "`.`customersAnsprechpartner1Vorname`,
						`" . TABLE_ACQUISATION_CUSTOMERS . "`.`customersAnsprechpartner1Nachname`,
						`" . TABLE_ACQUISATION_CUSTOMERS . "`.`customersAnsprechpartner1Anrede`,
						`" . TABLE_ACQUISATION_CUSTOMERS . "`.`customersAnsprechpartner2Vorname`,
						`" . TABLE_ACQUISATION_CUSTOMERS . "`.`customersAnsprechpartner2Nachname`,
						`" . TABLE_ACQUISATION_CUSTOMERS . "`.`customersAnsprechpartner2Anrede`,
						`" . TABLE_ACQUISATION_CUSTOMERS . "`.`customersTelefon1`,
						`" . TABLE_ACQUISATION_CUSTOMERS . "`.`customersTelefon2`,
						`" . TABLE_ACQUISATION_CUSTOMERS . "`.`customersMobil1`,
						`" . TABLE_ACQUISATION_CUSTOMERS . "`.`customersMobil2`,
						`" . TABLE_ACQUISATION_CUSTOMERS . "`.`customersFax1`,
						`" . TABLE_ACQUISATION_CUSTOMERS . "`.`customersFax2`,
						`" . TABLE_ACQUISATION_CUSTOMERS . "`.`customersMail1`,
						`" . TABLE_ACQUISATION_CUSTOMERS . "`.`customersMail2`,
						`" . TABLE_ACQUISATION_CUSTOMERS . "`.`customersHomepage`,
						`" . TABLE_ACQUISATION_CUSTOMERS . "`.`customersCompanyStrasse`,
						`" . TABLE_ACQUISATION_CUSTOMERS . "`.`customersCompanyHausnummer`,
						`" . TABLE_ACQUISATION_CUSTOMERS . "`.`customersCompanyCountry`,
						`" . TABLE_ACQUISATION_CUSTOMERS . "`.`customersCompanyPLZ`,
						`" . TABLE_ACQUISATION_CUSTOMERS . "`.`customersCompanyOrt`,
						`" . TABLE_ACQUISATION_CUSTOMERS . "`.`customersLieferadresseFirmenname`,
						`" . TABLE_ACQUISATION_CUSTOMERS . "`.`customersLieferadresseFirmennameZusatz`,
						`" . TABLE_ACQUISATION_CUSTOMERS . "`.`customersLieferadresseStrasse`,
						`" . TABLE_ACQUISATION_CUSTOMERS . "`.`customersLieferadresseHausnummer`,
						`" . TABLE_ACQUISATION_CUSTOMERS . "`.`customersLieferadressePLZ`,
						`" . TABLE_ACQUISATION_CUSTOMERS . "`.`customersLieferadresseOrt`,
						`" . TABLE_ACQUISATION_CUSTOMERS . "`.`customersLieferadresseLand`,
						`" . TABLE_ACQUISATION_CUSTOMERS . "`.`customersRechnungsadresseFirmenname`,
						`" . TABLE_ACQUISATION_CUSTOMERS . "`.`customersRechnungsadresseFirmennameZusatz`,
						`" . TABLE_ACQUISATION_CUSTOMERS . "`.`customersRechnungsadresseStrasse`,
						`" . TABLE_ACQUISATION_CUSTOMERS . "`.`customersRechnungsadresseHausnummer`,
						`" . TABLE_ACQUISATION_CUSTOMERS . "`.`customersRechnungsadressePLZ`,
						`" . TABLE_ACQUISATION_CUSTOMERS . "`.`customersRechnungsadresseOrt`,
						`" . TABLE_ACQUISATION_CUSTOMERS . "`.`customersRechnungsadresseLand`,
						`" . TABLE_ACQUISATION_CUSTOMERS . "`.`customersKontoinhaber`,
						`" . TABLE_ACQUISATION_CUSTOMERS . "`.`customersBankName`,
						`" . TABLE_ACQUISATION_CUSTOMERS . "`.`customersBankKontonummer`,
						`" . TABLE_ACQUISATION_CUSTOMERS . "`.`customersBankLeitzahl`,
						`" . TABLE_ACQUISATION_CUSTOMERS . "`.`customersBankIBAN`,
						`" . TABLE_ACQUISATION_CUSTOMERS . "`.`customersBankBIC`,
						`" . TABLE_ACQUISATION_CUSTOMERS . "`.`customersBezahlart`,
						`" . TABLE_ACQUISATION_CUSTOMERS . "`.`customersZahlungskondition`,
						`" . TABLE_ACQUISATION_CUSTOMERS . "`.`customersTyp`,
						`" . TABLE_ACQUISATION_CUSTOMERS . "`.`customersVertreterID`,
						`" . TABLE_ACQUISATION_CUSTOMERS . "`.`customersVertreterName`,
						`" . TABLE_ACQUISATION_CUSTOMERS . "`.`customersNotiz`,
						`" . TABLE_ACQUISATION_CUSTOMERS . "`.`customersUserID`,
						`" . TABLE_ACQUISATION_CUSTOMERS . "`.`customersTimeCreated`,
						`" . TABLE_ACQUISATION_CUSTOMERS . "`.`customersEntryDate`,
						`" . TABLE_ACQUISATION_CUSTOMERS . "`.`customersLastVisit`,
						`" . TABLE_ACQUISATION_CUSTOMERS . "`.`customersNextVisit`,

						GROUP_CONCAT(
							`" . TABLE_ACQUISATION_CUSTOMERS_HISTORY . "`.`customersAcquisitionHistoryID`,
							'+++',
							`" . TABLE_ACQUISATION_CUSTOMERS_HISTORY . "`.`customersAcquisitionHistoryDatetime`,
							'+++',
							`" . TABLE_ACQUISATION_CUSTOMERS_HISTORY . "`.`customersAcquisitionHistoryNotice`,
							'+++',
							`" . TABLE_ACQUISATION_CUSTOMERS_HISTORY . "`.`customersAcquisitionHistoryUserID`,
							'+++',
							`" . TABLE_ACQUISATION_CUSTOMERS_HISTORY . "`.`customersAcquisitionHistoryCustomerID`,
							'+++',
							`" . TABLE_ACQUISATION_CUSTOMERS_HISTORY . "`.`customersAcquisitionHistoryDateVisit`,
							'+++',
							`" . TABLE_ACQUISATION_CUSTOMERS_HISTORY . "`.`customersAcquisitionHistoryDateNextVisit`
							SEPARATOR '###'
						) AS `customersAcquisitionHistory`

					FROM `" . TABLE_ACQUISATION_CUSTOMERS . "`

					LEFT JOIN `" . TABLE_ACQUISATION_CUSTOMERS_HISTORY . "`
					ON(`" . TABLE_ACQUISATION_CUSTOMERS . "`.`customersID` = `" . TABLE_ACQUISATION_CUSTOMERS_HISTORY . "`.`customersAcquisitionHistoryCustomerID`)

					WHERE 1
						AND (
							`" . TABLE_ACQUISATION_CUSTOMERS . "`.`customersKundennummer` = ''
							OR
							`" . TABLE_ACQUISATION_CUSTOMERS . "`.`customersKundennummer` IS NULL
						)
			";
// INCLUDE HISTORY !!!!!!!!
		if($where != "") {
			$sql .= $where;
		}

		$sql .= " GROUP BY `" . TABLE_ACQUISATION_CUSTOMERS . "`.`customersID` ";
		$sql .= " ORDER BY
						`" . TABLE_ACQUISATION_CUSTOMERS . "`.`".$thisORDER."` ". $thisSORT ."
					";

		$sql_files = "SELECT
						`rowid`,
						`usersID`,
						`customerRowID`,
						`path`,
						`filename`,
						`filesize`,
						`uploadtime`

						FROM `" . TABLE_ACQUISATION_SALESMAN_DOCUMENTS . "`" . "

						WHERE 1
		";


		$rs_files = $dbConnection_ExternAcqisition->db_query($sql_files);

		$arrSelectedCustomerFiles = array();
		while ($ds_files = mysqli_fetch_assoc($rs_files)) {
			foreach(array_keys($ds_files) as $field) {
				$arrSelectedCustomerFiles[$ds_files["customerRowID"]][$ds_files["customersID"]][$field] = $ds_files[$field];
			}
		}
	}
	// EOF CREATE SQL FOR DISPLAY ORDERS
?>
<?php
	require_once('inc/headerHTML.inc.php');
	$thisTitle = "Vertreter-Kundenerfassung";
	$headerHTML = preg_replace("/{###TITLE###}/", strip_tags($thisTitle), $headerHTML);
	echo $headerHTML;

	require_once(FILE_MENUE_TOP);
?>
<div id="mainArea">
	<div id="mainContent">
		<?php require_once(FILE_MENUE_SIDEBAR); ?>
		<div id="contentArea">
			<div id="contentAreaElements">
				<h1><?php if(SHOW_PAGE_TITLE_ICONS) { echo '<img src="' . PATH_ICONS_MENUE_TITLES . 'acquisition.png" alt="" />'; } ?> <?php echo $thisTitle; ?></h1>
				<?php
					if($_REQUEST["importID"] == '') {
						echo '<h2 class="legend">Vom Verteter eingetragene Daten</h2>';
						if($existsOnlineAquisition) {
						echo '<div align="right"><a href="' . PATH_ONLINE_ACQUISITION . '" target="_blank">Zur Online-Kundenerfassung</a></div>';
						}
					}
				?>
				<div class="contentDisplay">
					<?php
						if($existsOnlineAquisition) {
						if(!(int)$_REQUEST["importID"] > 0 && $_REQUEST["sendID"] == '' ) {
					?>
					<p>Anzeige der von den Vertretern in der Online-Kundenerfassung  eingegebenen Daten.</p>
					<div id="searchFilterArea">
						<form name="formFilterSearch" method="post" action="<?php echo $_SERVER["PHP_SELF"]; ?>">
							<table border="0" cellpadding="0" cellspacing="0" class="searchFilterContent">
								<tr>
									<?php if ($arrGetUserRights["importOnlineAcquisition"]) { ?>
									<td>
										<label for="searchStatus">Status:</label>
										<select name="searchStatus" id="searchStatus" class="inputSelect_170">
											<option value="" style="color:#000000;" <?php if($_REQUEST["searchStatus"] == ''){echo ' selected="selected" ';} ?> >Alle Eintr&auml;ge</option>
											<option value="0" style="color:#FF0000;" <?php if($_REQUEST["searchStatus"] == '0'){echo ' selected="selected" ';} ?> >Unimportierte Eintr&auml;ge</option>
											<option value="1" style="color:#009900;" <?php if($_REQUEST["searchStatus"] == '1'){echo ' selected="selected" ';} ?> >Importierte Eintr&auml;ge</option>
										</select>
									</td>
									<?php } ?>
									<td>
										<label for="searchSalesman">Vertretername:</label>
										<!--
										<input type="text" name="searchSalesman" id="searchSalesman" class="inputField_100" value="<?php echo $_REQUEST["searchSalesman"]; ?>" />
										-->
										<select name="searchSalesmanOnlineID"  class="inputSelect_120">
											<option value=""> --- Bitte w&auml;hlen --- </option>
											<?php
												if(!empty($arrSalesmenDatas)){
													foreach($arrSalesmenDatas as $thisKey => $thisValue){
														if(!empty($arrAquisitionUsers[$thisValue["customersID"]])){
															$selected = '';
															if($_REQUEST["searchSalesmanOnlineID"] == $arrAquisitionUsers[$thisValue["customersID"]]["usersID"]){
																$selected = ' selected="selected" ';
															}
															echo '<option value="' . $arrAquisitionUsers[$thisValue["customersID"]]["usersID"] . '" ' . $selected . ' >' . utf8_encode($arrAquisitionUsers[$thisValue["customersID"]]["usersLastName"] . ', ' . $arrAquisitionUsers[$thisValue["customersID"]]["usersFirstName"]) . '</option>';
														}
													}
												}
											?>
										</select>
									</td>
									<td>
										<label for="searchWord">Suchbegriff:</label>
										<input type="text" name="searchWord" id="searchWord" class="inputField_100" value="<?php echo $_REQUEST["searchWord"]; ?>" />
									</td>
									<td>
										<input type="hidden" name="editID" id="editID" value="" />
										<input type="submit" name="submitSearch" class="inputButton1" value="Suchen" />
									</td>
								</tr>
							</table>
							<input type="hidden" name="sortDirection" id="inputSortDirection" value="<?php echo $_REQUEST["sortDirection"]; ?>" />
							<input type="hidden" name="sortField" id="inputSortField" value="<?php echo $_REQUEST["sortField"]; ?>" />
							<input type="hidden" name="displayType" id="inputDisplayType" value="<?php echo $_REQUEST["displayType"]; ?>" />
							<input type="hidden" name="submitSearchForm" id="inputSubmitSearchForm" value="1" />
						</form>
					</div>
					<?php
						}
					?>
					<?php

						if($_REQUEST["page"] == "" || $_REQUEST["page"] == 0) {
							$_REQUEST["page"] = 1;
						}

						if($maxUsersPerPage > 0) {
							$sql .= " LIMIT " . (($_REQUEST["page"] - 1) * $maxUsersPerPage) . ", " . $maxUsersPerPage." ";
						}

						$rs = $dbConnection_ExternAcqisition->db_query($sql);

						$sql_totalRows = "SELECT FOUND_ROWS() AS `countRows`";
						$rs_totalRows = $dbConnection_ExternAcqisition->db_query($sql_totalRows);
						list($countRows) = mysqli_fetch_array($rs_totalRows);

						$pagesCount = ceil($countRows / $maxUsersPerPage);

						if($countRows > 0) {

							if((int)$_REQUEST["sendID"] > 0 && $arrGetUserRights["sendMails"]) {
								$ds = mysqli_fetch_array($rs);
								$mailToUs_HTML = '
										<div style="font-size:11px; font-family:Arial; width:600px;">
											<table border="1" cellpadding="2" cellspacing="0" width="600" style="border:1px solid #CCCCCC; font-size:11px; font-family:Arial;" >
												<tr style="vertical-align:top;">
													<td style="width:80px;"><b>Vertreter:</b></td>
													<td> ' . $ds["customersVertreterName"] . ' </td>
												</tr>
												<tr style="vertical-align:top;">
													<td><b>Kunde:</b></td>
													<td>
														<b>Firma:</b> ' . preg_replace("/\//", " / ", $ds["customersFirmenname"]) . '<br />
														<b>Name:</b> ' . $ds["firstname"] . ' ' .  $ds["lastname"] . '<br />
														<b>Ansprech:</b> ' . $ds["customersAnsprechpartner1Vorname"] . ' ' .  $ds["customersAnsprechpartner1Nachname"] . '<br />
													</td>
												</tr>
												<tr style="vertical-align:top;">
													<td><b>Datum:</b></td>
													<td>
														<b>Eingetragen am:</b><br />' . formatDate($ds["customersEntryDate"], "display", "date") . '<br />
														<b>Ge&auml;ndert am:</b><br />' . formatDate($ds["customersTimeCreated"], "display", "date") . '<br />
													</td>
												</tr>
												<tr style="vertical-align:top;">
													<td><b>Adresse:</b></td>
													<td>
														<b>Firmenadresse:</b><br/>' . $ds["customersCompanyStrasse"] . ' ' . $ds["customersCompanyHausnummer"] . '<br />' . $ds["customersCompanyPLZ"] . ' ' . $ds["customersCompanyOrt"] . '<hr />
														<b>Rechnungsadresse:</b><br/>' . $ds["customersRechnungsadresseStrasse"] . ' ' . $ds["customersRechnungsadresseHausnummer"] . '<br />' . $ds["customersRechnungsadressePLZ"] . ' ' . $ds["customersRechnungsadresseOrt"] . '<hr />
														<b>Lieferadresse:</b><br/>' . $ds["customersLieferadresseStrasse"] . ' ' . $ds["customersLieferadresseHausnummer"] . '<br />' . $ds["customersLieferadressePLZ"] . ' ' . $ds["customersLieferadresseOrt"] . '<br />
													</td>
												</tr>
												<tr style="vertical-align:top;">
													<td><b>Telefon:</b></td>
													<td>
														<b>Tel 1:</b> ' . $ds["customersTelefon1"] . '<br />
														<b>Tel 2:</b> ' . $ds["customersTelefon2"] . '<hr />

														<b>Mobil 1:</b> ' . $ds["customersMobil1"] . '<br />
														<b>Mobil 2:</b> ' . $ds["customersMobil2"] . '<hr />

														<b>Fax 1:</b> ' . $ds["customersFax1"]. '<br />
														<b>Fax 2:</b> ' . $ds["customersFax2"]. '<br />
													</td>
												</tr>
												<tr style="vertical-align:top;">
													<td><b>Mail / Homepage:</b></td>
													<td>
														<b>Mail 1:</b> ' . $ds["customersMail1"] . '<br />
														<b>Mail 2:</b> ' . $ds["customersMail2"] . '<hr />
														<b>Homepage:</b> ' . $ds["customersHomepage"] . '<br />
													</td>
												</tr>
												<tr style="vertical-align:top;">
													<td><b>Bankdaten:</b></td>
													<td>
														<b><u>Bankverbindung 1</u></b><br />
														&bull; ' . '<b>1. Bank: </b>' . $ds["customersBankName"] . '<br />
														&bull; ' . '<b>1. BLZ: </b>' . $ds["customersBankLeitzahl"] . '<br />
														&bull; ' . '<b>1. Konto-Nr: </b>' . $ds["customersBankKontonummer"] . '<br />
														&bull; ' . '<b>1. IBAN: </b>' . '' . $ds["customersBankIBAN"] . '<br />
														&bull; ' . '<b>1. BIC: </b>' . $ds["customersBankBIC"] . '<br />
														&bull; ' . '<b>1. Kontoinh.: </b>' . $ds["customersKontoinhaber"] . '<hr />

														<b><u>Bankverbindung 2</u></b><br />
														&bull; ' . '<b>2. Bank: </b>' . $ds["customersBankName"] . '<br />
														&bull; ' . '<b>2. BLZ: </b>' . $ds["customersBankLeitzahl"] . '<br />
														&bull; ' . '<b>2. Konto-Nr: </b>' . $ds["customersBankKontonummer"] . '<br />
														&bull; ' . '<b>2. IBAN: </b>' . '' . $ds["customersBankIBAN"] . '<br />
														&bull; ' . '<b>2. BIC: </b>' . $ds["customersBankBIC"] . '<br />
														&bull; ' . '<b>2. Kontoinh.: </b>' . $ds["customersKontoinhaber"] . '<hr />
													</td>
												</tr>
												<tr style="vertical-align:top;">
													<td><b>Letzter Besuchsbericht:</b></td>
													<td>' . nl2br(wordwrap($ds["customersNotiz"], WORD_WRAP_WIDTH, "\n")) . '</td>
												</tr>
												<tr style="vertical-align:top;">
													<td><b>Dateien:</b></td>
													<td>
									';

								##################
								$arrMailFiles = array();
								$mailFileName = '';
								if(!empty($arrSelectedCustomerFiles[$ds["customersID"]])) {

									$mailToUs_HTML .= '
										<table border="1" cellpadding="2" cellspacing="0" width="600" style="style="border:1px solid #CCCCCC; font-size:11px; font-family:Arial;" >
											<tbody>
												<tr>
													<td><b>Datei</b></td>
													<td><b>Upload-Datum</b></td>
													<td><b>Dateigr&ouml;&szlig;e</b></td>
												</tr>
										';

									$countRow = 0;
									foreach ($arrSelectedCustomerFiles[$ds["customersID"]] as $thisKey => $thisValue) {
										if($countRow%2 == 0){ $rowClass = 'row1'; }
										else { $rowClass = 'row0'; }
										$mailToUs_HTML .= '<tr class="' . $rowClass . '">';
										$thisFilename = $thisValue["filename"];
										if($thisValue["filename"] == '') {
											$thisFilename = getFilename($thisValue["path"]);
										}
										$mailToUs_HTML .= '
											<td><a href="' . PATH_ONLINE_ACQUISITION . $thisValue["path"] . '" title="' . basename($thisValue["path"]) . '" target="_blank" >' . $thisFilename . '</a></td>
											<td>' . formatDate($thisValue["uploadtime"], 'display', 'date') . '</td>
											<td>' . $thisValue["filesize"] . ' KB</td>
										';

										$mailToUs_HTML .= '</tr>';

										// $arrMailFiles[] = $_SERVER["DOCUMENT_ROOT"] . '/dolibarr/htdocs/acquisition/' .$thisValue["path"];
										$arrMailFiles[] = PATH_ONLINE_ACQUISITION . $thisValue["path"];
										$mailFileName = basename($thisValue["path"]);
										$countRow++;
									}

									$mailToUs_HTML .= '
											</tbody>
										</table>
									';
								}

								$mailToUs_HTML .= '
												</td>
											</tr>
										</table>
									</div>
								';
								// echo htmlentities($mailToUs_HTML);
								// exit;
								$mailToUs_TEXT = $mailToUs_HTML;
								$mailToUs_TEXT = "BURHAN.DE<hr />".$mailToUs_TEXT;
								$mailToUs_TEXT = "".convertHtmlToText($mailToUs_TEXT);

								require_once(DIRECTORY_HTML_MIME_MAIL . 'htmlMimeMail.php');
								$mailToUs_Subject = 'Neuer Kunden-Eintrag:  ' . formatDate($ds["customersEntryDate"], "display", "date") . ' von ' . $ds["customersVertreterName"] . ' (' . $ds["customersFirmenname"] . ') in der Online-Kundenerfassung';

								$mailToUs = new htmlMimeMail();
								$mailToUs->setTextCharset('utf-8');
								$mailToUs->setHtmlCharset('utf-8');
								$mailToUs->setTextEncoding('7bit');
								$mailToUs->setHtmlEncoding('quoted-printable');
								$mailToUs->setHeadCharset('utf-8');
								// $mailToUs->setFrom($headersToUs);
								if(MAIL_ADDRESS_BCC != '') { $mailToUs->setBcc(MAIL_ADDRESS_BCC); }
								$mailToUs->setHeader('From', '"' . MAIL_ADDRESS_FROM_NAME . '" <' . MAIL_ADDRESS_FROM . '>');
								$mailToUs->setHeader('Reply-To', MAIL_ADDRESS_REPLY_TO);
								$mailToUs->setHeader('Errors-To', MAIL_ERRORS_TO);
								$mailToUs->setHeader('Subject', $mailToUs_Subject);
								$mailToUs->setHeader('Date', date('D, d M y H:i:s O'));
								// $mailToUs->setHeader($name, $value);
								// $mailToUs->setSubject($mailToUs_Subject);

								if(DEBUG) {
									$tempDisplayMailToUsHTML = $mailToUs_HTML;
									$tempDisplayMailToUsTEXT = $mailToUs_TEXT;
								}

								$mailToUs->setHtml($mailToUs_HTML, $mailToUs_TEXT, DIRECTORY_MAIL_IMAGES);

								if(!empty($arrAttachmentFiles))
								{
									foreach($arrAttachmentFiles as $thisFile => $thisFileName)
									{
										$mailToUs->addAttachment($mailToUs->getFile($thisFile), $thisFileName, 'application/pdf');
									}
								}

								$arrOrderMail = explode(";", MAIL_ADDRESS_FROM); // HAS TO BE AN ARRAY

								if(DEBUG) { $arrOrderMail = array(MAIL_ADDRESS_BCC, DEBUG_MAIL_TO_ADDRESS); }

								if(MAIL_SEND_TYPE == 'mail') {
									$sendMailToUs = $mailToUs->send($arrOrderMail, 'mail');
								}
								else if(MAIL_SEND_TYPE == 'smtp') {
									$mailToUs->setSMTPParams(MAIL_ACCOUNT_SMTP, MAIL_ACCOUNT_PORT, MAIL_ACCOUNT_HELO, MAIL_ACCOUNT_AUTH, MAIL_ACCOUNT_USER, MAIL_ACCOUNT_PASSWORD);
									// function setSMTPParams($host = null, $port = null, $helo = null, $auth = null, $user = null, $pass = null)
									$sendMailToUs = $mailToUs->send($arrOrderMail, 'smtp');
									if(!$sendMailToUs) {
										/*
										echo '<pre>';
										print_r($mailToUs->errors);
										echo '</pre>';

										echo '<pre>';
										print_r($arrOrderMail);
										echo '</pre>';
										*/
									}
								}
								else {
									$sendMailToUs = false;
								}
								if(!$sendMailToUs) { $errorMessage .= ' Die Mail konnte nicht an ' . implode(', ', $arrOrderMail) . ' versendet werden! '.'<br />'; }
								else { $successMessage .= ' Die Mail-Kopie wurde an ' . implode(', ', $arrOrderMail) . ' versendet! '.'<br />'; }

								displayMessages();

								echo '<p><a href="' . $_SERVER["PHP_SELF"] . '?' . '" class="linkButton" >Zur&uuml;ck zur &Uuml;bersicht</a></p>';
							}

							if($_REQUEST["importID"] > 0){
								echo '<table border="0" cellpadding="0" cellspacing="0" width="100%" class="displayOrders">
											<thead>
												<tr>
													<!--<th>ID</th>-->
													<th>Vertreter</th>
													<!--<th>K-Nr.</th>-->
													<th>Kunde</th>
													<th>Datum</th>
													<th>Adresse</th>
													<th>Telefon</th>
													<th>Mail / Homepage</th>
													<th style="width:60px;">Infos</th>
												</tr>
											</thead>
											<tbody>';
								$count = 0;

								while ($ds = mysqli_fetch_assoc($rs)) {
									foreach(array_keys($ds) as $field){
										$arrAcquisitionCustomerDatas[$field] = $ds[$field];
									}
								}
								if(!empty($arrAcquisitionCustomerDatas)) {
									//if(empty($importObj) && (int)$_REQUEST["importID"] > 0) {
										//$importObj = $objOnline;
									//}

									if($count%2 == 0){ $rowClass = 'row0'; }
									else { $rowClass = 'row1'; }

									echo '<tr class="'.$rowClass.'" style="vertical-align:top; font-size:11px;">';

									// echo '<td style="width:45px;">'.($arrAcquisitionCustomerDatas["customersID"]).'</td>';

									echo '<td style="white-space:nowrap;" title="' . $arrAcquisitionCustomerDatas["customersVertreterName"] . ' (Vertreter-ID: ' . $arrAcquisitionCustomerDatas["customersVertreterID"] . ')">';
									echo ($arrAcquisitionCustomerDatas["customersVertreterName"]) . '';
									echo '</td>';

									// echo '<td>' . ($arrAcquisitionCustomerDatas["customersKundennummer"]) . '</td>';

									echo '<td>';
									echo '<b>Firma:</b><br />' . preg_replace("/\//", " / ", $arrAcquisitionCustomerDatas["customersFirmenname"]) . '<br />';
									echo '<hr />';
									echo '<b>Name:</b><br />' . $arrAcquisitionCustomerDatas["firstname"] . ' ' .  $arrAcquisitionCustomerDatas["lastname"] . '<br />';
									echo '<hr />';
									echo '<b>Ansprechpartner:</b><br />' . $arrAcquisitionCustomerDatas["customersAnsprechpartner1Vorname"] . ' ' .  $arrAcquisitionCustomerDatas["customersAnsprechpartner1Nachname"] . '<br />';
									echo '</td>';

									echo '<td>';
									echo '<b>Eingetragen am:</b><br />' . formatDate($arrAcquisitionCustomerDatas["customersEntryDate"] , "display", "date").'<br />';
									echo '<hr />';
									echo '<b>Ge&auml;ndert am:</b><br />' . formatDate($arrAcquisitionCustomerDatas["customersTimeCreated"] , "display", "date");
									echo '</td>';

									echo '<td>';

									echo '<b>Firmenadresse:</b><br/>' . $arrAcquisitionCustomerDatas["customersCompanyStrasse"] . ' ' . $arrAcquisitionCustomerDatas["customersCompanyHausnummer"] . '<br />' . $arrAcquisitionCustomerDatas["customersCompanyPLZ"] . ' ' . $arrAcquisitionCustomerDatas["customersCompanyOrt"] . '<hr />';

									echo '<b>Rechnungsadresse:</b><br/>' . $arrAcquisitionCustomerDatas["customersRechnungsadresseStrasse"] . ' ' . $arrAcquisitionCustomerDatas["customersRechnungsadresseHausnummer"] . '<br />' . $arrAcquisitionCustomerDatas["customersRechnungsadressePLZ"] . ' ' . $arrAcquisitionCustomerDatas["customersRechnungsadresseOrt"] . '<hr />';
									// echo '<td>' . $arrAcquisitionCustomerDatas["fk_departement"] . '</td>';
									// echo '<td>' . $arrAcquisitionCustomerDatas["fk_pays"] . '</td>';

									// echo '<td>' . $arrAcquisitionCustomerDatas["addressDelivery"] . '</td>';
									echo '<b>Lieferadresse:</b><br/>' . $arrAcquisitionCustomerDatas["customersLieferadresseStrasse"] . ' ' . $arrAcquisitionCustomerDatas["customersLieferadresseHausnummer"] . '<br />' . $arrAcquisitionCustomerDatas["customersLieferadressePLZ"] . ' ' . $arrAcquisitionCustomerDatas["customersLieferadresseOrt"] ;
									echo '</td>';
									$compareMail_1 = $arrAcquisitionCustomerDatas["customersMail1"];
									$compareMail_2 = $arrAcquisitionCustomerDatas["customersMail2"];
									echo '<td>';
									echo '<b>Tel 1:</b> ' . formatPhoneNumber($arrAcquisitionCustomerDatas["customersTelefon1"]) . '<br />';
									echo '<b>Tel 2:</b> ' . formatPhoneNumber($arrAcquisitionCustomerDatas["customersTelefon2"]) . '<hr />';

									echo '<b>Mobil 1:</b> ' . formatPhoneNumber($arrAcquisitionCustomerDatas["customersMobil1"]) . '<br />';
									echo '<b>Mobil 2:</b> ' . formatPhoneNumber($arrAcquisitionCustomerDatas["customersMobil2"]) . '<hr />';

									echo '<b>Fax 1:</b> ' . formatPhoneNumber($arrAcquisitionCustomerDatas["customersFax1"]). '<br />';
									echo '<b>Fax 2:</b> ' . formatPhoneNumber($arrAcquisitionCustomerDatas["customersFax2"]). '</td>';

									echo '<td><b>Mail 1:</b> ' . $arrAcquisitionCustomerDatas["customersMail1"] . '<br />';
									echo '<b>Mail 2:</b> ' . $arrAcquisitionCustomerDatas["customersMail2"] . '<hr />';
									echo '<b>Homepage:</b> ' . $arrAcquisitionCustomerDatas["customersHomepage"] ;
									echo '</td>';

									echo '<td style="white-space:nowrap;">';
									if($arrAcquisitionCustomerDatas["customersNotiz"] != "") {
										echo '<span class="toolItem">
													<img src="layout/icons/iconRTF.gif" class="buttonNotice" width="16" height="16" alt="Letzter Besuchsbericht" title="Letzten Besuchsbericht anzeigen" />
										';
										echo '<div class="displayNoticeArea">';
										echo nl2br(wordwrap($arrAcquisitionCustomerDatas["customersNotiz"], WORD_WRAP_WIDTH, "\n"));
										echo '</div>';

										echo '</span>';
									}
									else {
										echo '<span class="toolItem"><img src="layout/icons/spacer.gif" width="16" height="16" alt="" /></span>';
									}

									if($arrAcquisitionCustomerDatas["customersBankName"] != "" || $arrAcquisitionCustomerDatas["customersBankLeitzahl"] != "" || $arrAcquisitionCustomerDatas["customersBankKontonummer"] != "" || $arrAcquisitionCustomerDatas["customersBankIBAN"] != "" || $arrAcquisitionCustomerDatas["customersBankBIC"] != "" || $arrAcquisitionCustomerDatas["customersKontoinhaber"] != "" || $arrAcquisitionCustomerDatas["customersBankName"] != "" || $arrAcquisitionCustomerDatas["customersBankLeitzahl"] != "" || $arrAcquisitionCustomerDatas["customersBankKontonummer"] != "" || $arrAcquisitionCustomerDatas["customersBankIBAN"] != "" || $arrAcquisitionCustomerDatas["customersBankBIC"] != "" || $arrAcquisitionCustomerDatas["customersKontoinhaber"] != "") {
										echo '
											<span class="toolItem">
												<img src="layout/icons/iconBankDatas.gif" width="16" height="16" class="buttonNotice" title="Bankdaten anzeigen" alt="Bankdaten" />';
												echo '<div class="displayNoticeArea">';
												if(1) { echo '<div class="displayNoticeElement"><h3>Bankverbindung 1</h3></div>'; }
												if(1) { echo '<div class="displayNoticeElement"><h3>Bankverbindung 2</h3></div>'; }

												if(1) { echo '<div class="displayNoticeElement"> &bull; ' . '<b>1. Bank: </b>' . $arrAcquisitionCustomerDatas["customersBankName"] . '</div>'; }
												if(1) { echo '<div class="displayNoticeElement"> &bull; ' . '<b>2. Bank: </b>' . $arrAcquisitionCustomerDatas["customersBankName"] . '</div>'; }

												if(1) { echo '<div class="displayNoticeElement"> &bull; ' . '<b>1. BLZ: </b>' . $arrAcquisitionCustomerDatas["customersBankLeitzahl"] . '</div>'; }
												if(1) { echo '<div class="displayNoticeElement"> &bull; ' . '<b>2. BLZ: </b>' . $arrAcquisitionCustomerDatas["customersBankLeitzahl"] . '</div>'; }

												if(1) { echo '<div class="displayNoticeElement"> &bull; ' . '<b>1. Konto-Nr: </b>' . $arrAcquisitionCustomerDatas["customersBankKontonummer"] . '</div>'; }
												if(1) { echo '<div class="displayNoticeElement"> &bull; ' . '<b>2. Konto-Nr: </b>' . $arrAcquisitionCustomerDatas["customersBankKontonummer"] . '</div>'; }

												if(1) { echo '<div class="displayNoticeElement"> &bull; ' . '<b>1. IBAN: </b>' . '' . $arrAcquisitionCustomerDatas["customersBankIBAN"] . '</div>'; }
												if(1) { echo '<div class="displayNoticeElement"> &bull; ' . '<b>2. IBAN: </b>' . $arrAcquisitionCustomerDatas["customersBankIBAN"] . '</div>'; }

												if(1) { echo '<div class="displayNoticeElement"> &bull; ' . '<b>1. BIC: </b>' . $arrAcquisitionCustomerDatas["customersBankBIC"] . '</div>'; }
												if(1) { echo '<div class="displayNoticeElement"> &bull; ' . '<b>2. BIC: </b>' . $arrAcquisitionCustomerDatas["customersBankBIC"] . '</div>'; }

												if(1) { echo '<div class="displayNoticeElement"> &bull; ' . '<b>1. Kontoinh.: </b>' . $arrAcquisitionCustomerDatas["customersKontoinhaber"] . '</div>'; }
												if(1) { echo '<div class="displayNoticeElement"> &bull; ' . '<b>2. Kontoin.: </b>' . $arrAcquisitionCustomerDatas["customersKontoinhaber"] . '</div>'; }

										echo '</div>';
										echo '</span>';
									}
									else {
										echo '<span class="toolItem"><img src="layout/icons/spacer.gif" width="16" height="16" alt="" /></span>';
									}

									if(!empty($arrSelectedCustomerFiles[$arrAcquisitionCustomerDatas["customersID"] ])) {
										echo '
											<span class="toolItem">
												<img src="layout/icons/iconDocuments.gif" width="16" height="16" class="buttonNotice" title="Angeh&auml;ngte Dokumente anzeigen" alt="Dokumente" />';
												echo '<div class="displayNoticeArea">';

												echo '<table class="border" cellpadding="0" cellspacing="0" width="100%">';
												echo '<tbody>';

												echo '<tr>';
												echo '<th>Dateiname</th>';
												echo '<th>Upload-Datum</th>';
												echo '<th>Dateigr&ouml;&szlig;e</th>';
												echo '<th>Datei &ouml;ffnen</th>';
												echo '</tr>';

												$countRow = 0;
												foreach ($arrSelectedCustomerFiles[$arrAcquisitionCustomerDatas["customersID"] ] as $thisKey => $thisValue) {
													if($countRow%2 == 0){ $rowClass = 'row1'; }
													else { $rowClass = 'row0'; }
													echo '<tr class="' . $rowClass . '">';
													$thisFilename = $thisValue["filename"];
													if($thisValue["filename"] == '') {
														$thisFilename = getFilename($thisValue["path"]);
													}
													echo '<td><a href="' . PATH_ONLINE_ACQUISITION . $thisValue["path"] . '" title="' . basename($thisValue["path"]) . '" target="_blank" >' . $thisFilename . '</a></td>';
													echo '<td>' . formatDate($thisValue["uploadtime"], 'display', 'date') . '</td>';
													echo '<td>' . $thisValue["filesize"] . ' KB</td>';
													echo '<td>' . '<a href="' . PATH_ONLINE_ACQUISITION . $thisValue["path"] . '" target="_blank" >' . '<img src="layout/icons/icon' . strtoupper(getFileType(basename($thisValue["path"]))) . '.gif" alt="' . getFileType(basename($thisValue["path"])) . '-Datei anzeigen" title="' . getFileType(basename($thisValue["path"])) . '-Datei anzeigen" /></a></td>';
													$countRow++;
													echo '</tr>';
												}

												echo '</tbody>';
												echo '</table>';

										echo '</div>';
										echo '</span>';
									}
									else {
										echo '<span class="toolItem"><img src="layout/icons/spacer.gif" width="16" height="16" alt="" /></span>';
									}

									echo '</td>';

									echo '</tr>';

									$count++;
									$i++;
								}

								echo '</tbody>';
								echo '</table>';
							}
							else if($_REQUEST["sendID"] == '') {
								if ($countRows > 0) {
									if($pagesCount > 1 && $_REQUEST["displayOrderStatus"] == "") {
										include(FILE_MENUE_PAGES);
									}

									$i = 0;

									echo '<table border="0" cellpadding="0" cellspacing="0" width="100%" class="displayOrders border2" style="font-size:11px;">
											<thead>
												<tr>
													<th style="width:45px;text-align:right;">#</th>
													<th class="sortColumn" id="customersVertreterName">Vertreter</th>
													<th class="sortColumn" id="customersKundennummer">K-Nr</th>
													<th class="sortColumn" id="customersFirmenname">Kunde</th>
													<th class="sortColumn" id="customersEntryDate">Eingetragen am</th>
													<th class="sortColumn" id="customersTimeCreated">Ge&auml;ndert am</th>
													<!--
													<th class="sortColumn" id="customersLastVisit">Letzter Besuch am</th>
													<th class="sortColumn" id="customersNextVisit">N&auml;chster Besuch am</th>
													-->
													<th class="sortColumn" id="cp"">PLZ</th>
													<th class="sortColumn" id="cpDelivery">Ort</th>
													<th class="sortColumn" id="cpDelivery">Anschrift</th>
													<th style="width:100px;">Infos</th>
													<th>Import</th>
												</tr>
											</thead>
											<tbody>';
									$count = 0;

									while ($ds = mysqli_fetch_assoc($rs)) {
										if($count%2 == 0){ $rowClass = 'row0'; }
										else { $rowClass = 'row1'; }

										echo '<tr class="'.$rowClass.'">';

										echo '<td><span style="font-weight:bold;cursor:pointer;" title="Datensatz-ID: '.($ds["customersID"] ).'">'.($count + 1).'.</span></td>';

										echo '<td style="white-space:nowrap;" title="' . $ds["customersVertreterName"]  . ' (Vertreter-ID: ' . $ds["customersVertreterID"]  . ')">';
										echo ($ds["customersVertreterName"] ) . '';
										echo '</td>';

										echo '<td>';
										if($ds["customersKundennummer"] != '') {
											echo '<a href="' . PAGE_EDIT_CUSTOMER . '?searchCustomerNumber=' . $ds["customersKundennummer"] . '">';
											echo ($ds["customersKundennummer"]) . '';
											echo '</a>';
										}
										else {
											echo '<a href="' . PAGE_EDIT_CUSTOMER . '?searchWord=' . $ds["customersFirmenname"] . '">';
											echo 'suchen';
											echo '</a>';
										}
										echo '</td>';
										echo '<td style="white-space:nowrap">'.(preg_replace("/\//", " / ", stripslashes($ds["customersFirmenname"]) )).'</td>';
										echo '<td>' . substr(formatDate($ds["customersEntryDate"] , "display"), 0, 10).'</td>';
										echo '<td>' . substr(formatDate($ds["customersTimeCreated"] , "display"), 0, 10).'</td>';

										#echo '<td>' . substr(formatDate($ds["customersLastVisit"] , "display"), 0, 10).'</td>';
										#echo '<td>' . substr(formatDate($ds["customersNextVisit"] , "display"), 0, 10).'</td>';


										echo '<td>';
										echo '<span class="nowrap">' . ($ds["customersCompanyPLZ"] ) . '</span>';
										echo '</td>';
										echo '<td>';
										echo '<span class="nowrap">' . ($ds["customersCompanyOrt"] ) . '</span>';
										echo '</td>';
										echo '<td>';
										echo '<span class="nowrap">' . ($ds["customersCompanyStrasse"] . ' ' . $ds["customersCompanyHausnummer"] ) . '</span>';
										echo '</td>';

										echo '<td style="white-space:nowrap; width:100px;" >';
										if($ds["customersNotiz"]  != "") {
											echo '<span class="toolItem">
														<img src="layout/icons/iconRTF.gif" class="buttonNotice" width="16" height="16" alt="Letzter Besuchsbericht" title="Letzten Besuchsbericht anzeigen" />
											';
											echo '<div class="displayNoticeArea">';
											echo nl2br(wordwrap($ds["customersNotiz"] , WORD_WRAP_WIDTH, "\n"));
											echo '</div>';

											echo '</span>';
										}
										else {
											echo '<span class="toolItem"><img src="layout/icons/spacer.gif" width="16" height="16" alt="" /></span>';
										}
										/*
										if($ds["customersAcquisitionHistory"]  != ""){

											echo '<span class="toolItem">
														<img src="layout/icons/iconReport2.png" class="buttonNotice" width="16" height="16" alt="Alle Besuchsberichte" title="Alle Besuchsberichte anzeigen" />
											';
											echo '<div class="displayNoticeArea">';

											echo '<table cellpadding="0" cellspacing="0" width="500">';
											echo '<tr>';
											echo '<th style="width:20px;" rowspan="2">#</th>';
											echo '<th style="width:120px;" colspan="2">Besuch</th>';
											echo '<th style="" rowspan="2">Bericht</th>';

											echo '</tr>';

											echo '<tr>';
											echo '<th style="width:60px;">Letzter</th>';
											echo '<th style="width:60px;">N&auml;chster</th>';
											echo '</tr>';

											$arrAcquisitionHistoryReportDatas = explode('###', $ds["customersAcquisitionHistory"]);

											$arrAcquisitionHistoryReportDatasSorted = array();
											foreach($arrAcquisitionHistoryReportDatas as $thisAcquisitionHistoryReportDataKey => $thisAcquisitionHistoryReportDataValue){
												$arrTemp = explode('+++', $thisAcquisitionHistoryReportDataValue);
												$arrAcquisitionHistoryReportDatasSorted[$arrTemp[0]] = array(
														"historyID" => $arrTemp[0],
														"historyDatetime" => $arrTemp[1],
														"historyReport" => $arrTemp[2],
														"historySalesmanID" => $arrTemp[3],
														"historyCustomerID" => $arrTemp[4],
														"historyLastVisit" => $arrTemp[5],
														"historyNextVisit" => $arrTemp[6]
													);
											}

											krsort($arrAcquisitionHistoryReportDatasSorted);

											$countHistory = 0;
											foreach($arrAcquisitionHistoryReportDatasSorted as $thisAcquisitionHistoryReportDataKey => $thisAcquisitionHistoryReportDataValue){
												if($countHistory%2 == 0){ $rowClassNoticeArea = 'row0'; }
												else { $rowClassNoticeArea = 'row1'; }

												echo '<tr class="'.$rowClassNoticeArea.'">';

												echo '<td style="text-align:right;">';
												echo '<b>' . ($countHistory + 1) . '.</b>';
												echo '</td>';

												echo '<td>';
												echo formatDate($thisAcquisitionHistoryReportDataValue["historyLastVisit"], "display");
												echo '</td>';

												echo '<td>';
												echo formatDate($thisAcquisitionHistoryReportDataValue["historyNextVisit"], "display");
												echo '</td>';

												echo '<td>';
												echo nl2br(wordwrap($thisAcquisitionHistoryReportDataValue["historyReport"] , (WORD_WRAP_WIDTH - 20), "\n"));
												echo '</td>';

												echo '</tr>';

												$countHistory++;
											}

											echo '</table>';

											echo '</div>';

											echo '</span>';
										}
										else {
											echo '<span class="toolItem"><img src="layout/icons/spacer.gif" width="16" height="16" alt="" /></span>';
										}
										*/
										if($ds["customersMail1"]  != "" || $ds["customersMail2"]  != "" || $ds["customersTelefon1"]  != "" || $ds["customersTelefon2"]  != "" || $ds["customersFax1"] != "" || $ds["customersFax2"] != "" || $ds["customersMobil1"]  != "" || $ds["customersMobil2"]  != "" || $ds["customersHomepage"]  != "") {
											echo '
												<span class="toolItem">
													<img src="layout/icons/iconContact.gif" width="16" height="16" class="buttonNotice" title="Kontaktdaten anzeigen" alt="Kontaktdaten" />';
													echo '<div class="displayNoticeArea">';
													if(1) { echo '<div class="displayNoticeElement"> &bull; ' . '<b> 1.Tel.: </b>' . formatPhoneNumber($ds["customersTelefon1"])  . '</div>'; }
													if(1) { echo '<div class="displayNoticeElement"> &bull; ' . '<b>2. Tel.: </b>' .  formatPhoneNumber($ds["customersTelefon2"])  . '</div>'; }


													if(1) { echo '<div class="displayNoticeElement"> &bull; ' . '<b>1. Mobil: </b>' .  formatPhoneNumber($ds["customersMobil1"])  . '</div>'; }
													if(1) { echo '<div class="displayNoticeElement"> &bull; ' . '<b>2. Mobil: </b>' .  formatPhoneNumber($ds["customersMobil2"])  . '</div>'; }

													if(1) { echo '<div class="displayNoticeElement"> &bull; ' . '<b>1. Fax: </b>' .  formatPhoneNumber($ds["customersFax1"]) . '</div>'; }
													if(1) { echo '<div class="displayNoticeElement"> &bull; ' . '<b>2. Fax: </b>' .  formatPhoneNumber($ds["customersFax2"]) . '</div>'; }

													if(1) { echo '<div class="displayNoticeElement"> &bull; ' . '<b>1. Mail: </b>' . '<a href="mailto:' . $ds["customersMail1"]  .'">' . $ds["customersMail1"]  . '</a>' . '</div>'; }
													if(1) { echo '<div class="displayNoticeElement"> &bull; ' . '<b>2. Mail: </b>' . '<a href="mailto:' . $ds["customersMail2"]  .'">' . $ds["customersMail2"]  . '</a>' . '</div>'; }

													if(1) { echo '<div class="displayNoticeElement"> &bull; ' . '<b>Homepage: </b>' . '<a href="http://' . $ds["customersHomepage"]  . '" target="_blank">' . $ds["customersHomepage"]  . '</a></div>'; }
											echo '</div>';
											echo '</span>';
										}
										else {
											echo '<span class="toolItem"><img src="layout/icons/spacer.gif" width="16" height="16" alt="" /></span>';
										}

										if($ds["customersBankName"]  != "" || $ds["customersBankLeitzahl"]  != "" || $ds["customersBankKontonummer"]  != "" || $ds["customersBankIBAN"]  != "" || $ds["customersBankBIC"]  != "" || $ds["customersKontoinhaber"]  != "" || $ds["customersBankName"]  != "" || $ds["customersBankLeitzahl"]  != "" || $ds["customersBankKontonummer"]  != "" || $ds["customersBankIBAN"]  != "" || $ds["customersBankBIC"]  != "" || $ds["customersKontoinhaber"]  != "") {
											echo '
												<span class="toolItem">
													<img src="layout/icons/iconBankDatas.gif" width="16" height="16" class="buttonNotice" title="Bankdaten anzeigen" alt="Bankdaten" />';
													echo '<div class="displayNoticeArea">';
													if(1) { echo '<div class="displayNoticeElement"><h3>Bankverbindung 1</h3></div>'; }
													if(1) { echo '<div class="displayNoticeElement"><h3>Bankverbindung 2</h3></div>'; }

													if(1) { echo '<div class="displayNoticeElement"> &bull; ' . '<b>1. Bank: </b>' . $ds["customersBankName"]  . '</div>'; }
													if(1) { echo '<div class="displayNoticeElement"> &bull; ' . '<b>2. Bank: </b>' . $ds["customersBankName"]  . '</div>'; }

													if(1) { echo '<div class="displayNoticeElement"> &bull; ' . '<b>1. BLZ: </b>' . $ds["customersBankLeitzahl"]  . '</div>'; }
													if(1) { echo '<div class="displayNoticeElement"> &bull; ' . '<b>2. BLZ: </b>' . $ds["customersBankLeitzahl"]  . '</div>'; }

													if(1) { echo '<div class="displayNoticeElement"> &bull; ' . '<b>1. Konto-Nr: </b>' . $ds["customersBankKontonummer"]  . '</div>'; }
													if(1) { echo '<div class="displayNoticeElement"> &bull; ' . '<b>2. Konto-Nr: </b>' . $ds["customersBankKontonummer"]  . '</div>'; }

													if(1) { echo '<div class="displayNoticeElement"> &bull; ' . '<b>1. IBAN: </b>' . '' . $ds["customersBankIBAN"]  . '</div>'; }
													if(1) { echo '<div class="displayNoticeElement"> &bull; ' . '<b>2. IBAN: </b>' . $ds["customersBankIBAN"]  . '</div>'; }

													if(1) { echo '<div class="displayNoticeElement"> &bull; ' . '<b>1. BIC: </b>' . $ds["customersBankBIC"]  . '</div>'; }
													if(1) { echo '<div class="displayNoticeElement"> &bull; ' . '<b>2. BIC: </b>' . $ds["customersBankBIC"]  . '</div>'; }

													if(1) { echo '<div class="displayNoticeElement"> &bull; ' . '<b>1. Kontoinh.: </b>' . $ds["customersKontoinhaber"]  . '</div>'; }
													if(1) { echo '<div class="displayNoticeElement"> &bull; ' . '<b>2. Kontoin.: </b>' . $ds["customersKontoinhaber"]  . '</div>'; }

											echo '</div>';
											echo '</span>';
										}
										else {
											echo '<span class="toolItem"><img src="layout/icons/spacer.gif" width="16" height="16" alt="" /></span>';
										}

										if(!empty($arrSelectedCustomerFiles[$ds["customersID"] ])) {
											echo '
												<span class="toolItem">
													<img src="layout/icons/iconDocuments.gif" width="16" height="16" class="buttonNotice" title="Angeh&auml;ngte Dokumente anzeigen" alt="Dokumente" />';
													echo '<div class="displayNoticeArea">';

													echo '<table class="border" cellpadding="0" cellspacing="0" width="100%">';
													echo '<tbody>';

													echo '<tr>';
													echo '<th>Dateiname</th>';
													echo '<th>Upload-Datum</th>';
													echo '<th>Dateigr&ouml;&szlig;e</th>';
													echo '<th>Datei &ouml;ffnen</th>';
													echo '</tr>';

													$countRow = 0;
													foreach ($arrSelectedCustomerFiles[$ds["customersID"] ] as $thisKey => $thisValue) {
														if($countRow%2 == 0){ $rowClass = 'row1'; }
														else { $rowClass = 'row0'; }
														echo '<tr class="' . $rowClass . '">';
														$thisFilename = $thisValue["filename"];
														if($thisValue["filename"] == '') {
															$thisFilename = getFilename($thisValue["path"]);
														}
														echo '<td><a href="' . PATH_ONLINE_ACQUISITION . $thisValue["path"] . '" title="' . basename($thisValue["path"]) . '" target="_blank" >' . $thisFilename . '</a></td>';
														echo '<td>' . formatDate($thisValue["uploadtime"], 'display', 'date') . '</td>';
														echo '<td>' . $thisValue["filesize"] . ' KB</td>';
														echo '<td>' . '<a href="' . PATH_ONLINE_ACQUISITION . $thisValue["path"] . '" target="_blank" >' . '<img src="layout/icons/icon' . strtoupper(getFileType(basename($thisValue["path"]))) . '.gif" alt="' . getFileType(basename($thisValue["path"])) . '-Datei anzeigen" title="' . getFileType(basename($thisValue["path"])) . '-Datei anzeigen" /></a></td>';
														$countRow++;
														echo '</tr>';
													}

													echo '</tbody>';
													echo '</table>';

											echo '</div>';
											echo '</span>';
										}
										else {
											echo '<span class="toolItem"><img src="layout/icons/spacer.gif" width="16" height="16" alt="" /></span>';
										}

										if($ds["catmere"]  != '') {
											echo '
												<span class="toolItem">
													<img src="layout/icons/iconCategories2.png" width="16" height="16" class="buttonNotice" title="Zugeh&ouml;rige Kategorien anzeigen" alt="Kategorien" />
												';
													echo '<div class="displayNoticeArea">';
													$arrCatmere = unserialize($ds["catmere"] );
													if(is_array($arrCatmere)) {
														$arrCategories = getCategories();
														foreach($arrCatmere as $thisKey => $thisValue) {
															echo '<div>&bull; ' . $arrCategories[$thisValue] . '</div>';
														}
													}
													echo '</div>';
											echo '</span>';
										}
										else {
											echo '<span class="toolItem"><img src="layout/icons/iconCategories.png" width="16" height="16" alt="" /></span>';
										}

										echo '</td>';

										echo '<td style="white-space:nowrap;">';
										#if ($arrGetUserRights["importOnlineAcquisition"]) {
										if (1) {
											echo '<span class="toolItem">';

											$arrOnlineCustomerDatas = $ds;
											$arrAcquisitionUserDatas = getAquisitionUsers('usersID');
											$arrOnlineCustomerDatas["customersVertreterID"] = $arrAcquisitionUserDatas[$arrOnlineCustomerDatas["customersVertreterID"]]["usersAuftragslistenCustomerID"];
											unset($arrOnlineCustomerDatas["customersID"]);
											unset($arrOnlineCustomerDatas["customersUserID"]);

											if(!empty($arrOnlineCustomerDatas)){
												foreach($arrOnlineCustomerDatas as $thisOnlineCustomerDatasKey => $thisOnlineCustomerDatasValue){
													$arrOnlineCustomerDatas[$thisOnlineCustomerDatasKey] = stripslashes($thisOnlineCustomerDatasValue);
												}
											}

											#dd('arrOnlineCustomerDatas');
											$importURL = '';
											#$importURL .= PAGE_EDIT_CUSTOMER;
											$importURL .= PAGE_IMPORT_CUSTOMER;
											$importURL .= '?editID=NEW';
											$importURL .= '&source=OKE';
											$importURL .= '&importCustomerDatas=' . urlencode(serialize($arrOnlineCustomerDatas));
											#echo $importURL . '<br />';
											echo '<a href="' . (($importURL)) . '" ><img src="layout/icons/iconCompareAcquisitionCustomers.png" width="16" height="16" title="Kundendaten importieren (Datensatz-ID '.$ds["customers_id"] .')" alt="Bearbeiten" /></a>';

											echo '</span>';
											/*
											echo '<span class="toolItem">
													<a href="' . $_SERVER["PHP_SELF"] . '?' . $_SERVER["QUERY_STRING"] . '&importID='.$ds["customersID"] .'">
														<img src="layout/icons/iconCompareAcquisitionCustomers.png" width="16" height="16" title="Kundendaten abgleichen oder importieren (Datensatz-ID '.$ds["customersID"] .')" alt="Bearbeiten" />
													</a>
												</span>';
											*/
										}
										else {
											echo '<span class="toolItem"><img src="layout/icons/spacer.gif" width="16" height="16" alt="" /></span>';
										}

										/*
										if ($arrGetUserRights["sendMails"]) {
											echo '<span class="toolItem">
													<a href="' . $_SERVER["PHP_SELF"] . '?' . $_SERVER["QUERY_STRING"] . '&sendID='.$ds["customersID"] .'">
														<img src="layout/icons/iconMail.gif" width="16" height="14" title="Diese Daten per Mail senden an '.COMPANY_MAIL.'" alt="Per Mail senden" />
													</a>
												</span>';
										}
										else {
											echo '<span class="toolItem"><img src="layout/icons/spacer.gif" width="16" height="16" alt="" /></span>';
										}
										*/

										echo '</td>';
										echo '</tr>';
										$count++;
										$i++;
									}
									echo '</tbody>';
									echo '</table>';
									if($pagesCount > 1 && $_REQUEST["displayOrderStatus"] == "") {
										include(FILE_MENUE_PAGES);
									}
								}
								else {
									$infoMessage .= 'Es liegen keine Eintr&auml;ge vor' . '<br />';
								}
							}
							displayMessages();
						}

						/*
						// BOFTHIS PART MOVED INTO SEPERATE FILE
						if((int)$_REQUEST["importID"] > 0) {
							echo '<br />';
							echo '<h2 class="legend">Abgleich oder Import dieser Daten mit den Auftragslisten-Kunden</h2>';

							// BOF FIND EXISTING CUSTOMERS DEPENDING ON MAIL-ADSRESS
								if((int)$_REQUEST["importID"] > 0) {
									$sqlCompareWHERE = array();
									if($compareMail_1 != '') {
										$sqlCompareWHERE[] = " `customersMail1` = '" . $compareMail_1 . "' ";
										$sqlCompareWHERE[] = " `customersMail2` = '" . $compareMail_1 . "' ";
									}
									if($compareMail_2 != '') {
										$sqlCompareWHERE[] = " `customersMail1` = '" . $compareMail_2 . "' ";
										$sqlCompareWHERE[] = " `customersMail2` = '" . $compareMail_2 . "' ";
									}

									$sqlCompare = "SELECT
													`customersID`,
													`customersKundennummer`,
													`customersFirmenname`,
													`customersFirmenInhaberVorname`,
													`customersFirmenInhaberNachname`,
													`customersFirmenInhaberAnrede`,
													`customersAnsprechpartner1Vorname`,
													`customersAnsprechpartner1Nachname`,
													`customersAnsprechpartner1Anrede`,
													`customersAnsprechpartner2Vorname`,
													`customersAnsprechpartner2Nachname`,
													`customersAnsprechpartner2Anrede`,
													`customersTelefon1`,
													`customersTelefon2`,
													`customersMobil1`,
													`customersMobil2`,
													`customersFax1`,
													`customersFax2`,
													`customersMail1`,
													`customersMail2`,
													`customersHomepage`,
													`customersCompanyStrasse`,
													`customersCompanyHausnummer`,
													`customersCompanyCountry`,
													`customersCompanyPLZ`,
													`customersCompanyOrt`,
													`customersLieferadresseStrasse`,
													`customersLieferadresseHausnummer`,
													`customersLieferadressePLZ`,
													`customersLieferadresseOrt`,
													`customersLieferadresseLand`,
													`customersRechnungsadresseStrasse`,
													`customersRechnungsadresseHausnummer`,
													`customersRechnungsadressePLZ`,
													`customersRechnungsadresseOrt`,
													`customersRechnungsadresseLand`,
													`customersBankName`,
													`customersKontoinhaber`,
													`customersBankKontonummer`,
													`customersBankLeitzahl`,
													`customersBankIBAN`,
													`customersBankBIC`,
													`customersBezahlart`,
													`customersZahlungskondition`,
													`customersRabatt`,
													`customersUseProductMwst`,
													`customersUseProductDiscount`,
													`customersUstID`,
													`customersVertreterID`,
													`customersVertreterName`,
													`customersUseSalesmanDeliveryAdress`,
													`customersUseSalesmanInvoiceAdress`,
													`customersTyp`,
													`customersGruppe`,
													`customersNotiz`,
													`customersUserID`,
													`customersTimeCreated`,
													`customersActive`

													FROM `" . TABLE_CUSTOMERS . "`" . "

													WHERE 1
									";
									if(!empty($sqlCompareWHERE)){
										$sqlCompareWHERE = ' AND ' .implode(' OR ', $sqlCompareWHERE);
										$sqlCompare .= $sqlCompareWHERE;
									}
									else {
										$infoMessage .= 'Der Vergleich per Mailadresse kann nicht durchgef&uuml;hrt werden, da keine Mailadresse eingetragen ist.' . '<br />';
										$sqlCompare = '';
									}

									if($sqlCompare != ''){
										$rs = $dbConnection->db_query($sqlCompare);
										if(!$rs) {
											$errorMessage .= ' Beim Abgleich der Daten ist ein Fehler aufgetreten. ' . '<br />';
										}
										else {
											$countRows = $dbConnection->db_getMysqlNumRows($rs);

											if($countRows > 0) {
												while($ds = mysqli_fetch_array($rs)){
													foreach(array_keys($ds) as $field){
														$arrFoundDatas[$ds["customersID"]][$field] = $ds[$field];
													}
												}
												$infoMessage .= ' Beim Vergleich der Akquise-Daten mit den vorhandenen Auftragslisten-Kunden-Daten aufgrund der Mail-Adresse wurden m&ouml;gliche &uuml;bereinstimmende Daten gefunden. ' . '<br />';
											}
											else {
												$infoMessage .= ' Der Vergleich der Akquise-Daten mit den vorhandenen Auftragslisten-Kunden-Daten aufgrund der Mail-Adresse lieferte kein Ergebnis. ' . '<br />';
											}
											displayMessages();
										}
									}
								}
							// EOF FIND EXISTING CUSTOMERS DEPENDING ON MAIL-ADSRESS

							displayMessages();

							if(!empty($arrFoundDatas)){
								echo '<table cellpadding="0" cellspacing="0" width="100%" class="displayOrders">';
								echo '<tr>';
								echo '<th style="width:45px;text-align:right;">#</th>';
								echo '<th>Kundennummer</th>';
								echo '<th>Kundenname</th>';
								echo '<th>PLZ</th>';
								echo '<th>Ort</th>';
								echo '<th>Stra&szlig;e / Hausnummer</th>';
								echo '</tr>';
								$count = 0;
								foreach($arrFoundDatas as $thisKey => $thisValue){
									if($count%2 == 0){ $rowClass = 'row0'; }
									else { $rowClass = 'row1'; }

									echo '<tr class="'.$rowClass.'">';
									echo '<td style="text-align:right;">' . ($count + 1) . '</td>';
									echo '<td><a href="' . PAGE_EDIT_CUSTOMER . '?editCustomerNumber=' . ($thisValue["customersKundennummer"]) . '">' . ($thisValue["customersKundennummer"]) . '</a></td>';
									echo '<td>' . ($thisValue["customersFirmenname"]) . '</td>';
									echo '<td>' . ($thisValue["customersCompanyPLZ"]) . '</td>';
									echo '<td>' . ($thisValue["customersCompanyOrt"]) . '</td>';
									echo '<td>' . ($thisValue["customersLieferadresseStrasse"]) . ' ' . ($thisValue["customersLieferadresseHausnummer"]) . '</td>';
									echo '</tr>';
								}
								echo '</table>';
							}
							else {
								?>
								<hr />
								<h2>Kunden suchen</h2>
								<div id="searchFilterArea">
									<form name="formFilterSearch" method="post" action="<?php echo PAGE_EDIT_CUSTOMER; ?>">
									<table border="0" cellpadding="0" cellspacing="0" class="searchFilterContent">
										<tr>
											<td>
												<label for="searchPLZ">PLZ:</label>
												<input type="text" name="searchPLZ" id="searchPLZ" class="inputField_40" value="<?php if( $_REQUEST["searchPLZ"] != '') { echo $_REQUEST["searchPLZ"]; } else { echo $arrAcquisitionCustomerDatas["customersCompanyPLZ"]; }  ?>" />
											</td>
											<td>
												<label for="searchWord">Suchbegriff:</label>
												<input type="text" name="searchWord" id="searchWord" class="inputField_70" value="<?php echo $_REQUEST["searchWord"]; ?>" />
											</td>
											<td>
												<input type="hidden" name="editID" id="editID" value="" />
												<input type="submit" name="submitSearch" class="inputButton0" value="Suchen" />
											</td>
										</tr>
									</table>
									</form>
								</div>

								<?php
									if($arrGetUserRights["importOnlineAcquisition"]){
										echo '<hr />';
										echo '<h2>Kunden importieren</h2>';

										$arrAcquisitionUserDatas = getAquisitionUsers('usersID');
										$arrAcquisitionCustomerDatas["customersVertreterID"] = $arrAcquisitionUserDatas[$arrAcquisitionCustomerDatas["customersVertreterID"]]["usersAuftragslistenCustomerID"];
										unset($arrAcquisitionCustomerDatas["customersID"]);
										unset($arrAcquisitionCustomerDatas["customersUserID"]);

										$importURL = '';
										#$importURL .= PAGE_EDIT_CUSTOMER;
										$importURL .= PAGE_IMPORT_CUSTOMER;
										$importURL .= '?editID=NEW';
										$importURL .= '&source=OKE';
										$importURL .= '&importCustomerDatas=' . urlencode(serialize($arrAcquisitionCustomerDatas));
										#echo $importURL . '<br />';
										echo '<a href="' . (($importURL)) . '" >Diesen Kunden in der Kundenverwaltung anlegen</a>';
									}
								}
							}
							*/
						}
						displayMessages();
					?>
				</div>
			</div>
		</div>
		<div class="clear"></div>
	</div>
</div>

<script language="javascript" type="text/javascript">
	$(document).ready(function() {
		$('#searchSalesman').keyup(function () {
			// loadSearchSuggestions('#searchSalesman', '<?php echo $_SESSION["usersID"]; ?>');
		});
		$('#searchSalesman').keyup(function () {
			loadSuggestions('searchSalesmen', [{'triggerElement': '#searchSalesman', 'fieldName': '#searchSalesman', 'fieldNumber': '', 'fieldID': ''}], 1);
		});

		$('#searchSalesman').click(function () {
			$('#searchWord').val('');
		});
		$('#searchWord').click(function () {
			$('#searchSalesman').val('');
		});

		$('.buttonNotice').click(function () {
			loadNotice($(this));
		});
		setFocus('formFilterSearch', 'searchWord');
		colorRowMouseOver('.displayOrders tbody tr');
		$('.buttonShopOrderInfo').css('cursor', 'pointer');
		$('.buttonShopOrderInfo').click(function () {
			loadOrderDetails($(this), '<?php echo BASEPATH; ?>');
		});

		// toggleAreas();
	});
</script>

<?php require_once('inc/footerHTML.inc.php'); ?>