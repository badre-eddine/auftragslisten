<?php
	require_once('inc/requires.inc.php');

	if(!$arrGetUserRights["cleanAuftragslisten"]) {
		header('location: ' . PAGE_EXIT_LOCATION);
		exit;
	}

	$jswindowMessage = '';
	$warningMessage = '';
	$errorMessage = '';
	$successMessage = '';
	$infoMessage = '';

	$dbConnection = new DB_Connection();
	$db_open = $dbConnection->db_connect();

	if($_POST["submitFormCleanDBCache"] != "") {
		if($_POST["cleanTableCashe"] == "flush") {
			$sql = "FLUSH QUERY CACHE";
		}
		else if($_POST["cleanTableCashe"] == "reset") {
			$sql = "RESET QUERY CACHE";
		}
		else if($_POST["cleanTableCashe"] == "setGlobalCache" && (int)trim($_POST["setGlobalCacheValue"]) != "") {
			$sql = "SET GLOBAL `query_cache_size` = " . trim($_POST["setGlobalCacheValue"]) . "";
		}
		else if($_POST["cleanTableCashe"] == "repair") {
			// BOF GET ALL TABLES
				$sql = "SHOW TABLES";
				$rs = $dbConnection->db_query($sql);
				$arrTables = array();
				while ($ds = mysqli_fetch_assoc($rs)){
					foreach($ds as $thisKey => $thisValue){
						if(!preg_match("/^_/", $thisValue)){
							$arrTables[] = "`" . $thisValue . "`";
						}
					}
				}
				$sql = "";
			// EOF GET ALL TABLES

			if(!empty($arrTables)){
				foreach($arrTables as $thisTable){
					$sql_repair = "REPAIR TABLE " . $thisTable . ";";
					#$infoMessage .= $sql_repair . '<br />';
					$rs_repair = $dbConnection->db_query($sql_repair);
					if($rs_repair) {
						$successMessage .= ' Der Befehl "' . $sql_repair . '" wurde erfolgreich bearbeitet. ' . '<br />';
						#echo '<p class="successArea" style="font-size:11px;line-height:12px;padding:4px;border:2px solid #090;background-color:#0F0;font-weight:bold;color:#333;max-height:100px;overflow:auto">Tabelle &quot;' . $thisTable . '&quot; erfolgreich optimiert!</p>';
					}
					else {
						$errorMessage .= ' Der Befehl "' . $sql_repair . '" konnte nicht erfolgreich bearbeitet werden. ' . '<br />';
						#echo '<p class="errorArea" style="font-size:11px;line-height:12px;padding:4px;border:2px solid #F00;background-color:#FCC7CB;font-weight:bold;color:#333;max-height:100px;overflow:auto">Tabelle &quot;' . $thisTable . '&quot; NICHT erfolgreich optimiert!</p>';
					}
					flush();
				}
				$sql = "";
			}
		}
		else if($_POST["cleanTableCashe"] == "optimize") {
			$optimizeModus = $_POST["optimizeModus"]; // complete | single
			if($optimizeModus == ''){
				$optimizeModus = 'complete'; // complete | single
			}

			// BOF GET ALL TABLES
				$sql = "SHOW TABLES";
				$rs = $dbConnection->db_query($sql);
				$arrTables = array();
				while ($ds = mysqli_fetch_assoc($rs)){
					foreach($ds as $thisKey => $thisValue){
						if(!preg_match("/^_/", $thisValue)){
							$arrTables[] = "`" . $thisValue . "`";
						}
					}
				}
				$sql = "";
			// EOF GET ALL TABLES

			if(!empty($arrTables)){

				// BOF REMOVE SPECIAL TABLES
					$arrExcludeTables = array(
						"`geodb_areadatas`",
						"`geodb_changelog`",
						"`geodb_coordinates`",
						"`geodb_floatdata`",
						"`geodb_hierarchies`",
						"`geodb_intdata`",
						"`geodb_locations`",
						"`geodb_streetnames`",
						"`geodb_textdata`",
						"`geodb_type_names`",
						"`geodb_zipcodeareadatas`",
						"`geodb_zipcodedoubledigitareadatas`"
					);
					foreach($arrExcludeTables as $thisExcludeTable){
						if(in_array($thisExcludeTable, $arrTables)){
							if(($key = array_search($thisExcludeTable, $arrTables)) !== false) {
								unset($arrTables[$key]);
							}
						}
					}
				// EOF REMOVE SPECIAL TABLES

				if($optimizeModus == 'complete'){
					$stringTables = implode(",\n", $arrTables);
					$sql = "OPTIMIZE TABLE " . $stringTables;
				}
				else if($optimizeModus == 'single'){
					foreach($arrTables as $thisTable){
						$sql_optimize = "OPTIMIZE TABLE " . $thisTable;
						$rs_optimize = $dbConnection->db_query($sql_optimize);
						if($rs_optimize) {
							$successMessage .= ' Der Befehl "' . $sql_optimize . '" wurde erfolgreich bearbeitet. ' . '<br />';
							#echo '<p class="successArea" style="font-size:11px;line-height:12px;padding:4px;border:2px solid #090;background-color:#0F0;font-weight:bold;color:#333;max-height:100px;overflow:auto">Tabelle &quot;' . $thisTable . '&quot; erfolgreich optimiert!</p>';
						}
						else {
							$errorMessage .= ' Der Befehl "' . $sql_optimize . '" konnte nicht erfolgreich bearbeitet werden. ' . '<br />';
							#echo '<p class="errorArea" style="font-size:11px;line-height:12px;padding:4px;border:2px solid #F00;background-color:#FCC7CB;font-weight:bold;color:#333;max-height:100px;overflow:auto">Tabelle &quot;' . $thisTable . '&quot; NICHT erfolgreich optimiert!</p>';
						}
						flush();
					}
					$sql = "";
				}
			}
		}
		if($sql != "") {
			$rs = $dbConnection->db_query($sql);
			if($rs) {
				$successMessage .= ' Der Befehl "' . $sql . '" wurde erfolgreich bearbeitet. ' . '<br />';
			}
			else {
				$errorMessage .= ' Der Befehl "' . $sql . '" konnte nicht erfolgreich bearbeitet werden. ' . '<br />';
			}
		}
	}
	/*
	if($_POST["submitFormCleanDBTables"] != "") {
		if(!empty($_POST["cleanTableContent"])) {
			foreach($_POST["cleanTableContent"] as $thisKey => $thisValue) {
				if($thisValue == '1')  {
					$sql = "TRUNCATE TABLE `" . constant($thisKey) . "`";
					if($sql != "") {
						$rs = $dbConnection->db_query($sql);
						if($rs) {
							$successMessage .= ' Der Befehl "' . $sql . '" wurde erfolgreich bearbeitet. ' . '<br />';
						}
						else {
							$errorMessage .= ' Der Befehl "' . $sql . '" konnte nicht erfolgreich bearbeitet werden. ' . '<br />';
						}
					}
				}
			}
		}
		else {
			$errorMessage .= ' Sie haben keine Tabelle ausgewählt. ' . '<br />';
		}
	}
	*/
	/*
	if($_POST["submitFormCleanFolders"] != "") {
		if(!empty($_POST["cleanFolderContent"])) {
			foreach($_POST["cleanFolderContent"] as $thisKey => $thisValue) {
				if($thisValue == '1')  {
					$arrFilesToDelete = read_dir(constant($thisKey));

					if(!empty($arrFilesToDelete)) {
						foreach($arrFilesToDelete as $thisFilesToDelete) {
							$rs = unlink($thisFilesToDelete);
							if($rs) {
								$successMessage .= ' Die Datei "' . $thisFilesToDelete . '" wurde erfolgreich entfernt. ' . '<br />';
							}
							else {
								$errorMessage .= ' Die Datei "' . $thisFilesToDelete . '" konnte nicht erfolgreich entfernt werden. ' . '<br />';
							}
						}
					}
					else {
						$infoMessage .= ' Es sind keine Dateien im Ordner "' . constant($thisKey) . '" vorhanden, die entfernt werden können. ' . '<br />';
					}
				}
			}
		}
		else {
			$errorMessage .= ' Sie haben keinen Ordner ausgewählt. ' . '<br />';
		}
	}
	*/
	#$sql = 'SHOW TABLE STATUS';
	#$rs = $dbConnection->db_query($sql);
?>
<?php
	require_once('inc/headerHTML.inc.php');
	$thisTitle = "Auftragslisten bereinigen und optimieren";
	$headerHTML = preg_replace("/{###TITLE###}/", strip_tags($thisTitle), $headerHTML);
	echo $headerHTML;

	require_once(FILE_MENUE_TOP);
?>
<div id="mainArea">
	<div id="mainContent">
		<?php require_once(FILE_MENUE_SIDEBAR); ?>
		<div id="contentArea">
			<div id="contentAreaElements">
				<h1><?php if(SHOW_PAGE_TITLE_ICONS) { echo '<img src="' . PATH_ICONS_MENUE_TITLES . 'clean.png" alt="" />'; } ?> <?php echo $thisTitle; ?></h1>

				<?php displayMessages(); ?>

				<h2>Optimierung der DB (Tabellen-Cashes leeren)</h2>
				<form name="formCleanDBCache" method="post" action="<?php echo $_SERVER["PHP_SELF"]; ?>">
					<table border="0" cellpadding="0" cellspacing="0" width="100%" class="displayOrders">
						<tr>
							<th style="width:50px;">Auswahl</th>
							<th style="width:280px;">Befehl</th>
							<th>Beschreibung</th>
						</tr>
						<tr>
							<td><input type="radio" name="cleanTableCashe" class="cleanTableCashe" value="flush" checked="checked" /></td>
							<td><b>FLUSH QUERY CACHE</b></td>
							<td>Initialisiert den Cache auf die aktuellen Daten.<br />Der Befehl bewirkt eine Defragmentation des Caches und kann jederzeit zur Optimierung ausgeführt werden. </td>
						</tr>
						<tr>
							<td><input type="radio" name="cleanTableCashe" class="cleanTableCashe" value="setGlobalCache" /></td>
							<td><b>SET GLOBAL `query_cache_size`</b></td>
							<td>Cache in der Laufzeit einschalten (die Größe ist hier in Byte angegeben).
								<br /><b>query_cache_size:</b> <input type="text" class="inputField_70" name="setGlobalCacheValue" value="524288" style="text-align:right;font-weight:bold;" />
							</td>
						</tr>
						<tr>
							<td><input type="radio" name="cleanTableCashe" class="cleanTableCashe" value="reset" /></td>
							<td><b>RESET QUERY CACHE</b></td>
							<td>Löscht alle gespeicherten Cache Ergebnisse aus den Query-Cache.</td>
						</tr>
						<tr>
							<td><input type="radio" name="cleanTableCashe" class="cleanTableCashe" value="repair" /></td>
							<td><b>REPAIR ALL TABLES</b></td>
							<td>Alle Tabellen optimieren.<br />
								<input type="radio" name="repairModus" id="repairModus" value="complete" checked="checked" /><b>Alle Tabellen</b>
							</td>
						</tr>
						<tr>
							<td><input type="radio" name="cleanTableCashe" class="cleanTableCashe" value="optimize" /></td>
							<td><b>OPTIMIZE ALL TABLES</b></td>
							<td>Alle Tabellen optimieren.<br />
								<input type="radio" name="optimizeModus" id="optimizeModus_complete" value="complete" checked="checked" /><b>Alle Tabellen komplett</b>
								<input type="radio" name="optimizeModus" id="optimizeModus_single" value="single" /> <b>Alle Tabellen einzeln</b>
							</td>
						</tr>

					</table>
					<br />
					<div class="actionButtonsArea2">
						<input type="submit" name="submitFormCleanDBCache" class="inputButton1" value="Optimierung ausf&uuml;hren"  onclick="return showWarning(' Soll der Befehl [' + $('.cleanTableCashe:checked').val().toUpperCase() + '] wirklich ausgeführt werden? ');" />
					</div>
				</form>

				<hr />

				<h3>Optimierung in der my.ini</h3>
				<ul>
					<li>query_cache_size = 128M</li>
					<li>query_cache_limit = 16M</li>
					<li>query_cache_type = 1</li>
					<li>max_allowed_packet = 16M</li>
				</ul>

				<hr />

				<h2>Datenbank-Tabellen leeren</h2>
				<?php
					echo '<div class="infoArea">deaktiviert</div>';
					/*
					$arrTableToClean = array(
						"TABLE_CREATED_DOCUMENTS",
						// "TABLE_CUSTOMERS",
						"TABLE_ORDER_CONFIRMATIONS",
						"TABLE_ORDER_CONFIRMATIONS_DETAILS",
						"TABLE_ORDER_CREDITS",
						"TABLE_ORDER_CREDITS_DETAILS",
						"TABLE_ORDER_DELIVERIES",
						"TABLE_ORDER_DELIVERIES_DETAILS",
						"TABLE_ORDER_INVOICES",
						"TABLE_ORDER_INVOICES_DETAILS",
						"TABLE_ORDER_LETTERS",
						"TABLE_ORDER_LETTERS_DETAILS",
						"TABLE_ORDER_OFFERS",
						"TABLE_ORDER_OFFERS_DETAILS",
						"TABLE_ORDER_REMINDERS",
						"TABLE_ORDER_REMINDERS_DETAILS",
						// "TABLE_ORDERS",
						"TABLE_ORDERS_TO_DOCUMENTS",
						// "TABLE_PERSONNEL",
						// "TABLE_PERSONNEL_CALENDAR",

						// "TABLE_STOCK_ITEMS",

						// "TABLE_USERS",
						// "TABLE_USERS_ONLINE",
					);

					if(!empty($arrTableToClean)) {
						echo '<form name="formCleanDBTables" method="post" action="' . $_SERVER["PHP_SELF"] . '">';
						echo '<table border="0" cellpadding="0" cellspacing="0" width="100%" class="displayOrders">';
						echo '<tr>';
						echo '<th style="width:50px;">Auswahl</th>';
						echo '<th>Tabellenname</th>';
						echo '</tr>';

						$count = 0;
						foreach($arrTableToClean as $thisTable) {
							if($count%2 == 0){ $rowClass = 'row0'; }
							else { $rowClass = 'row1'; }
							echo '<tr class="' . $rowClass . '">';
							echo '<td><input type="checkbox" name="cleanTableContent[' . $thisTable . ']" class="cleanTableContent" value="1" /></td>';
							echo '<td>' . constant($thisTable) . ' </td>';
							echo '</tr>';
						}
						echo '</table>';
						?>
						<br />
						<div class="actionButtonsArea2">
							<!-- <input type="reset" name="resetForm" class="inputButton1" value="Zur&uuml;cksetzen" />-->
							<input type="submit" name="submitFormCleanDBTables" class="inputButton1" value="Tabellen leeren" onclick="return showWarning(' Sollen die ausgewählten Tabellen wirklich geleert werden? ');" />
						</div>
						<?php
						echo '</form>';
					}
					*/
					echo '<hr />';
				?>

				<h2>Dateien (erzeugte PDFs und Uploads) entfernen</h2>
				<?php
					echo '<div class="infoArea">deaktiviert</div>';
					/*
					$arrFoldersToClean = array(
						"DIRECTORY_CREATED_DOCUMENTS_SALESMEN",
						"DIRECTORY_CREATED_DOCUMENTS_CUSTOMERS",
						"DIRECTORY_UPLOAD_FILES",
						"DIRECTORY_BACKUP_FILES",
						"DIRECTORY_IMPORT_FILES",
						"DIRECTORY_EXPORT_FILES",
					);

					if(!empty($arrFoldersToClean)) {
						echo '<form name="formCleanFolders" method="post" action="' . $_SERVER["PHP_SELF"] . '">';
						echo '<table border="0" cellpadding="0" cellspacing="0" width="100%" class="displayOrders">';
						echo '<tr>';
						echo '<th style="width:50px;">Auswahl</th>';
						echo '<th>Ordnername</th>';
						echo '</tr>';

						$count = 0;
						foreach($arrFoldersToClean as $thisFolder) {
							if($count%2 == 0){ $rowClass = 'row0'; }
							else { $rowClass = 'row1'; }
							echo '<tr class="' . $rowClass . '">';
							echo '<td><input type="checkbox" name="cleanFolderContent[' . $thisFolder . ']" class="cleanTableContent" value="1" /></td>';
							echo '<td>' . constant($thisFolder) . ' </td>';
							echo '</tr>';
						}
						echo '</table>';
						?>
						<br />
						<div class="actionButtonsArea2">
							<!-- <input type="reset" name="resetForm" class="inputButton1" value="Zur&uuml;cksetzen" />-->
							<input type="submit" name="submitFormCleanFolders" class="inputButton1" value="Ordner leeren" onclick="return showWarning(' Sollen die ausgewählten Ordner wirklich geleert werden? ');" />
						</div>
						<?php
						echo '</form>';
					}
					*/
					echo '<hr />';
				?>
			</div>
		</div>
		<div class="clear"></div>
	</div>
	<div class="clear"></div>
</div>

<script language="javascript" type="text/javascript">
	<!--
	/* <![CDATA[ */
	$(document).ready(function() {
		colorRowMouseOver('.displayOrders tbody tr');
	});
	/* ]]> */
	// -->
</script>
<?php require_once('inc/footerHTML.inc.php'); ?>