<?php
	require_once('inc/requires.inc.php');

	if(!$arrGetUserRights["displaySales"]) {
		header('location: ' . PAGE_EXIT_LOCATION);
		exit;
	}

	$jswindowMessage = '';
	$warningMessage = '';
	$errorMessage = '';
	$successMessage = '';
	$infoMessage = '';


	if($_GET["downloadFile"] != "") {
		require_once('classes/downloadFile.class.php');
		$thisDownload = new downloadFile(DIRECTORY_EXPORT_FILES, $_GET["downloadFile"]);
		$errorMessage .= $thisDownload->startDownload();
	}

	$dbConnection = new DB_Connection();
	$db_open = $dbConnection->db_connect();

	$defaultDateYear = date('Y');
	$defaultDateMonth = '';

	if($_POST["searchDateYear"] == ''){ $_POST["searchDateYear"] = $defaultDateYear; }
	if($_POST["searchDateMonth"] == ''){ $_POST["searchDateMonth"] = $defaultDateMonth; }

	$defaultORDER = "ordersBestellDatum";
	$defaultSORT = "ASC";

	// BOF READ VERTRETER
		$arrAgentDatas = getAgents();
		$arrAgentDatas2 = getAgents2();
	// EOF READ VERTRETER

	// BOF READ BESTELL ARTEN
		$arrOrderTypeDatas = getOrderTypes();
	// EOF READ BESTELL ARTEN

	// BOF READ BESTELL STATUS TYPEN
		$arrOrderStatusTypeDatas = getOrderStatusTypes();
	// EOF READ BESTELL STATUS TYPEN

	// BOF READ DOCUMENT TYPE DATAS
		$arrPaymentStatusTypeDatas = getPaymentStatusTypes();
	// EOF READ DOCUMENT TYPE DATAS
?>
<?php
	require_once('inc/headerHTML.inc.php');
	$thisTitle = "Umsatz";

	$headerHTML = preg_replace("/{###TITLE###}/", strip_tags($thisTitle), $headerHTML);
	echo $headerHTML;

	require_once(FILE_MENUE_TOP);

	$sqlSortField = "";
	$sqlSortDirection = "";
	$sqlWhere = "";

?>

<div id="mainArea">
	<div id="mainContent">
		<?php require_once(FILE_MENUE_SIDEBAR); ?>
		<div id="contentArea">
			<a name="top"></a>
			<div id="contentAreaElements">
				<h1><?php if(SHOW_PAGE_TITLE_ICONS) { echo '<img src="' . PATH_ICONS_MENUE_TITLES . 'statistics.png" alt="" />'; } ?> <?php echo $thisTitle; ?></h1>

				<div id="searchFilterArea">
					<form name="formExportDatas" method="post" action="<?php echo $_SERVER["PHP_SELF"]; ?>">
					<table border="0" cellpadding="0" cellspacing="0" class="searchFilterContent">
						<tr>
							<td>
								<label for="searchDateMonth">Monat:</label>
								<select name="searchDateMonth" id="searchDateMonth" class="inputField_120">
									<option value="" <?php if($_POST["searchDateMonth"] == '') { echo ' selected="selected" '; } ?> >Gesamtes Jahr</option>
									<?php
										for($i = 1 ; $i < 13 ; $i++){
											$m = $i;
											if($m < 10){ $m = '0'.$m;}
											$selected = '';
											if($_POST["searchDateMonth"] != "") {
												if($_POST["searchDateMonth"] == $m) {
													$selected = ' selected="selected" ';
												}
											}
											else if($m == $defaultDateMonth) {
												$selected = ' selected="selected" ';
											}
											$thisMonth = getTimeNames(strftime('%m', mktime(0, 0, 0, $i, 1, date('Y'))), 'month', 'long');
											echo '<option value="' . $m . '" ' . $selected . '>' . $thisMonth . '</option>';
										}
									?>
								</select>
							</td>
							<td>
								<label for="searchDateYear">Jahr:</label>
								<select name="searchDateYear" id="searchDateYear" class="inputField_70">
									<?php
										$selected = '';
										for($i = 2012 ; $i < (date('Y') + 1) ; $i++){
											$selected = '';
											if($_POST["searchDateYear"] != "") {
												if($_POST["searchDateYear"] == $i) {
													$selected = ' selected="selected" ';
												}
											}
											else if($i == $defaultDateYear) {
												$selected = ' selected="selected" ';
											}
											echo '<option value="' . $i . '" ' . $selected . '>' . $i . '</option>';
										}
									?>
								 </select>
							</td>
							<td>
								<input type="hidden" name="editID" id="editID" value="" />
								<input type="submit" name="submitExport" class="inputButton0" value="Anzeigen" />
							</td>
						</tr>
					</table>
					<input type="hidden" name="submitExportForm" id="submitExportForm" value="1" />
					</form>
				</div>

				<div class="contentDisplay">
					<?php

						//if($_POST["submitExportForm"] == 1) {
						if(1) {
							// BOF GET DATAS

								// BOF GET PAYMENTS

									$searchIntervalPayments = substr($_POST["searchDateYear"], 2, 2);
									if($_POST["searchDateMonth"] != ""){
										$searchIntervalPayments .= '' . $_POST["searchDateMonth"];
									}
									$searchIntervalPayments = 'RE-' . $searchIntervalPayments;

									$sql_getPayments = "
											SELECT
												SUM(`" . TABLE_ORDER_INVOICE_PAYMENTS . "`.`orderPaymentValue`) as `documentPayments`,
												'RE' AS `paymentDocumentType`,
												SUBSTRING(`" . TABLE_ORDER_INVOICE_PAYMENTS . "`.`orderPaymentOrderNumber`, 1, 7) AS `documentNumberPart`

												FROM `" . TABLE_ORDER_INVOICE_PAYMENTS . "`

												WHERE 1
													AND SUBSTRING(`" . TABLE_ORDER_INVOICE_PAYMENTS . "`.`orderPaymentOrderNumber`, 1, " . strlen($searchIntervalPayments) . ") = '" . $searchIntervalPayments . "'

												GROUP BY SUBSTRING(`" . TABLE_ORDER_INVOICE_PAYMENTS . "`.`orderPaymentOrderNumber`, 1, 7)

											UNION

											SELECT
												SUM(`" . TABLE_ORDER_CONFIRMATION_PAYMENTS . "`.`orderPaymentValue`) as `documentPayments`,
												'AB' AS `paymentDocumentType`,
												SUBSTRING(`" . TABLE_RELATED_DOCUMENTS . "`.`relatedDocuments_RE`, 1, 7) AS `documentNumberPart`

												FROM `" . TABLE_RELATED_DOCUMENTS . "`

												LEFT JOIN `" . TABLE_ORDER_CONFIRMATION_PAYMENTS . "`
												ON(`" . TABLE_RELATED_DOCUMENTS . "`.`relatedDocuments_AB` = `" . TABLE_ORDER_CONFIRMATION_PAYMENTS . "`.`orderPaymentOrderNumber`)

												WHERE 1
													AND SUBSTRING(`" . TABLE_RELATED_DOCUMENTS . "`.`relatedDocuments_RE`, 1, " . strlen($searchIntervalPayments) . ") = '" . $searchIntervalPayments . "'

												GROUP BY SUBSTRING(`" . TABLE_RELATED_DOCUMENTS . "`.`relatedDocuments_RE`, 1, 7)
										";

									$rs_getPayments = $dbConnection->db_query($sql_getPayments);
									$arrPaymentsData = array();
									while($ds_getPayments = mysqli_fetch_assoc($rs_getPayments)){
										$thisDocumentNumberPart =  $ds_getPayments["documentNumberPart"];
										$thisInterval = '20' . substr($thisDocumentNumberPart, 3, 2) . '-' . substr($thisDocumentNumberPart, 5, 2);
										foreach(array_keys($ds_getPayments) as $field){
											$arrPaymentsData[$thisInterval][$ds_getPayments["paymentDocumentType"]][$field] = $ds_getPayments[$field];
										}
									}
								// EOF GET PAYMENTS

								$whereInvoices = "";
								$whereCredits = "";

								/*
								if($_POST["searchDateYear"] != "" && $_POST["searchDateMonth"] != ""){
									$whereInvoices .= " AND `orderDocumentsDocumentDate` LIKE '" . $_POST["searchDateYear"] . '-' . $_POST["searchDateMonth"]. "%'";
								}
								else if($_POST["searchDateYear"] != "" && $_POST["searchDateMonth"] == ""){
									$whereInvoices .= " AND `orderDocumentsDocumentDate` LIKE '" . $_POST["searchDateYear"] . "%'";
								}
								*/

								$searchIntervalDocuments = $_POST["searchDateYear"];
								if($_POST["searchDateMonth"] != ""){
									$searchIntervalDocuments .= '-' . $_POST["searchDateMonth"];
								}

								#$whereInvoices .= " AND `orderDocumentsDocumentDate` LIKE '" . $searchIntervalDocuments . "%'";
								$whereInvoices .= " AND SUBSTRING(`orderDocumentsDocumentDate`, 1, " . strlen($searchIntervalDocuments) . ") = '" . $searchIntervalDocuments . "'";

								$whereCredits = $whereInvoices;

								$sql_group_concat_max_len = "SET SESSION group_concat_max_len = 1000000;";
								$rs_group_concat_max_len = $dbConnection->db_query($sql_group_concat_max_len);

								$sql = "

									SELECT
										*
										FROM  (
											SELECT
												SUBSTRING(`" . TABLE_ORDER_INVOICES . "`.`orderDocumentsDocumentDate`, 1, 7) AS `dataInvoicesTime`,

												GROUP_CONCAT(`" . TABLE_ORDER_INVOICES . "`.`orderDocumentsNumber`, ';', `" . TABLE_ORDER_INVOICES . "`.`orderDocumentsTotalPrice` ORDER BY `orderDocumentsNumber` SEPARATOR '#') AS `documentsInvoicesData`,

												SUM(`" . TABLE_ORDER_INVOICES . "`.`orderDocumentsSumPrice`) AS `dataInvoicesSumPrice`,
												SUM(`" . TABLE_ORDER_INVOICES . "`.`orderDocumentsMwst`) AS `dataInvoicesMwst`,
												SUM(`" . TABLE_ORDER_INVOICES . "`.`orderDocumentsMwstPrice`) AS `dataInvoicesMwstPrice`,
												SUM(`" . TABLE_ORDER_INVOICES . "`.`orderDocumentsTotalPrice`) AS `dataInvoicesTotalPrice`,
												SUM(`" . TABLE_ORDER_INVOICES . "`.`orderDocumentsSkonto`) AS `dataInvoicesSkonto`,

												/* SUM(`" . TABLE_ORDER_INVOICE_PAYMENTS . "`.`orderPaymentValue`) AS `dataInvoicesPaymentValue`, */

												'INVOICES' AS `dataInvoicesType`

												FROM `" . TABLE_ORDER_INVOICES . "`

												/*
												LEFT JOIN `" . TABLE_ORDER_INVOICE_PAYMENTS . "`
												ON(`" . TABLE_ORDER_INVOICES . "`.`orderDocumentsNumber` = `" . TABLE_ORDER_INVOICE_PAYMENTS . "`.`orderPaymentOrderNumber` )
												*/

												WHERE 1 " . $whereInvoices . "

												GROUP BY SUBSTRING(`" . TABLE_ORDER_INVOICES . "`.`orderDocumentsDocumentDate`, 1, 7)

										) AS `tempTableInvoices`

									LEFT JOIN (
										SELECT
											SUBSTRING(`" . TABLE_ORDER_CREDITS . "`.`orderDocumentsDocumentDate`, 1, 7) AS `dataCreditsTime`,

											GROUP_CONCAT(`" . TABLE_ORDER_CREDITS . "`.`orderDocumentsNumber`, ';', `" . TABLE_ORDER_CREDITS . "`.`orderDocumentsTotalPrice` SEPARATOR '#') AS `documentsCreditsData`,

											SUM(`" . TABLE_ORDER_CREDITS . "`.`orderDocumentsSumPrice`) AS `dataCreditsSumPrice`,
											SUM(`" . TABLE_ORDER_CREDITS . "`.`orderDocumentsMwst`) AS `dataCreditsMwst`,
											SUM(`" . TABLE_ORDER_CREDITS . "`.`orderDocumentsMwstPrice`) AS `dataCreditsMwstPrice`,
											SUM(`" . TABLE_ORDER_CREDITS . "`.`orderDocumentsTotalPrice`) AS `dataCreditsTotalPrice`,
											SUM(`" . TABLE_ORDER_CREDITS . "`.`orderDocumentsSkonto`) AS `dataCreditsSkonto`,

											SUM(0) AS `dataCreditsPaymentValue`,

											'CREDITS' AS `dataCreditsType`

											FROM `" . TABLE_ORDER_CREDITS . "`

											WHERE 1 " . $whereCredits . "

											GROUP BY SUBSTRING(`" . TABLE_ORDER_CREDITS . "`.`orderDocumentsDocumentDate`, 1, 7)

										) AS `tempTableCredits`

									ON (`tempTableInvoices`. `dataInvoicesTime` = `tempTableCredits`. `dataCreditsTime`)

									ORDER BY `tempTableInvoices`. `dataInvoicesTime` DESC
								";

							$rs = $dbConnection->db_query($sql);
#dd('sql');
							$countTotalRows = $dbConnection->db_getMysqlNumRows($rs);

							if($countTotalRows > 0) {

								#echo '<h1>Erstellte ' . strtoupper(MANDATOR) . '-Rechnungen von ' . $_POST["exportDateStart"] . ' bis ' . $_POST["exportDateEnd"] . '</h1>';

								echo '<table width="100%" cellpadding="0" cellspacing="0" class="displayOrders">';

								echo '<tr>';
								echo '<th style="width:45px" rowspan="2">#</th>';
								echo '<th style="width:150px" rowspan="2">Datum</th>';
								echo '<th class="spacer" rowspan="2"></th>';
								echo '<th style="" colspan="4">SUMME RECHNUNGEN</th>';
								echo '<th class="spacer" rowspan="2"></th>';
								echo '<th style="" colspan="4">SUMME GUTSCHRIFTEN</th>';
								echo '<th class="spacer" rowspan="2"></th>';
								echo '<th style="" colspan="3">SUMME DIFFERENZ (RE - GU)</th>';
								echo '<th class="spacer" rowspan="2"></th>';
								#echo '<th style="" colspan="3">SUMME PROVISIONEN</th>';
								#echo '<th class="spacer" rowspan="2"></th>';
								echo '<th style="" colspan="3">ZUGEH&Ouml;RIGE ZAHLUNGEN</th>';
								echo '<th class="spacer" rowspan="2"></th>';
								echo '<th style="width:90px" rowspan="2">Info</th>';
								echo '</tr>';

								echo '<tr>';
								echo '<th style="width:20px">&sum;</th>';
								echo '<th style="width:90px">netto</th>';
								echo '<th style="width:90px">MwSt</th>';
								echo '<th style="width:90px">brutto</th>';

								echo '<th style="width:20px">&sum;</th>';
								echo '<th style="width:90px">netto</th>';
								echo '<th style="width:90px">MwSt</th>';
								echo '<th style="width:90px">brutto</th>';

								echo '<th style="width:90px">netto</th>';
								echo '<th style="width:90px">MwSt</th>';
								echo '<th style="width:90px">brutto</th>';

								/*
								echo '<th style="width:90px">netto</th>';
								echo '<th style="width:90px">MwSt</th>';
								echo '<th style="width:90px">brutto</th>';
								*/

								echo '<th style="width:90px">RE</th>';
								echo '<th style="width:90px">AB</th>';
								echo '<th style="width:90px">SUMME RE + AB</th>';

								echo '</tr>';

								$countRow = 0;
								$dataInvoicesTotalPrice = 0;
								$dataInvoicesMwstPrice = 0;
								$dataCreditsTotalPrice = 0;
								$dataCreditsMwstPrice = 0;

								$totalCountInvoices = 0;
								$totalCountCredits = 0;

								$totalPaymentsInvoices = 0;
								$totalPaymentsConfirmations = 0;

								while($ds = mysqli_fetch_assoc($rs)) {
									$rowClass = 'row0';
									if($countRow%2 == 0) {
										$rowClass = 'row1';
									}

									echo '<tr class="'.$rowClass.'">';

									$arrTempInvoiceData = array();
									$arrTempCreditsData = array();

									if($ds["documentsInvoicesData"] != ''){
										$arrTempInvoiceData = explode('#', $ds["documentsInvoicesData"]);
									}
									if($ds["documentsCreditsData"] != ''){
										$arrTempCreditsData = explode('#', $ds["documentsCreditsData"]);
									}


									echo '<td style="text-align:right;"><b>';
									echo ($countRow + 1);
									echo '.</b></td>';

									echo '<td style="white-space:nowrap;">';
									$arrTemp = explode('-', $ds["dataInvoicesTime"]);
									$thisMonth = getTimeNames(strftime('%m', mktime(0, 0, 0, $arrTemp[1], 1, date('Y'))), 'month', 'long');
									echo $arrTemp[0] . ' ' . $thisMonth;
									echo '</td>';

									echo '<td class="spacer"></td>';

									echo '<td style="text-align:right;font-size:10px;color:#999;">';
									$thisTotalCountInvoices = count($arrTempInvoiceData);
									$totalCountInvoices += $thisTotalCountInvoices;
									echo $thisTotalCountInvoices;
									echo '</td>';

									$dataInvoicesTotalPrice += $ds["dataInvoicesTotalPrice"];
									$dataInvoicesMwstPrice += $ds["dataInvoicesMwstPrice"];
									$dataCreditsTotalPrice += $ds["dataCreditsTotalPrice"];
									$dataCreditsMwstPrice += $ds["dataCreditsMwstPrice"];

									echo '<td style="text-align:right;background-color:#FEFFAF;">';
									echo number_format(($ds["dataInvoicesTotalPrice"] - $ds["dataInvoicesMwstPrice"]), '2', ',', '.');
									echo '</td>';
									echo '<td style="text-align:right;background-color:#FEFFAF;">';
									echo number_format($ds["dataInvoicesMwstPrice"], '2', ',', '.');
									echo '</td>';
									echo '<td style="text-align:right;background-color:#FEFFAF;">';
									echo number_format($ds["dataInvoicesTotalPrice"], '2', ',', '.');
									echo '</td>';

									echo '<td class="spacer"></td>';

									echo '<td style="text-align:right;font-size:10px;color:#999;">';
									$thisTotalCountCredits = count($arrTempCreditsData);
									$totalCountCredits += $thisTotalCountCredits;
									echo $thisTotalCountCredits;
									echo '</td>';

									echo '<td style="text-align:right;">';
									echo number_format(($ds["dataCreditsTotalPrice"] - $ds["dataCreditsMwstPrice"]), '2', ',', '.');
									echo '</td>';
									echo '<td style="text-align:right;">';
									echo number_format($ds["dataCreditsMwstPrice"], '2', ',', '.');
									echo '</td>';
									echo '<td style="text-align:right;">';
									echo number_format($ds["dataCreditsTotalPrice"], '2', ',', '.');
									echo '</td>';

									echo '<td class="spacer"></td>';

									echo '<td style="text-align:right;background-color:#FEFFAF;">';
									echo number_format((($ds["dataInvoicesTotalPrice"] - $ds["dataInvoicesMwstPrice"]) - ($ds["dataCreditsTotalPrice"] - $ds["dataCreditsMwstPrice"])), '2', ',', '.');
									echo '</td>';
									echo '<td style="text-align:right;background-color:#FEFFAF;">';
									echo number_format($ds["dataInvoicesMwstPrice"] - $ds["dataCreditsMwstPrice"], '2', ',', '.');
									echo '</td>';
									echo '<td style="text-align:right;background-color:#FEFFAF;">';
									echo number_format($ds["dataInvoicesTotalPrice"] - $ds["dataCreditsTotalPrice"], '2', ',', '.');
									echo '</td>';

									echo '<td class="spacer"></td>';

									/*
									echo '<td style="text-align:right;background-color:#FEFFAF;">';
									echo $ds["dataTime"];
									echo '</td>';
									echo '<td style="text-align:right;background-color:#FEFFAF;">';
									echo $ds["dataTime"];
									echo '</td>';
									echo '<td style="text-align:right;background-color:#FEFFAF;">';
									echo $ds["dataTime"];
									echo '</td>';

									echo '<td class="spacer"></td>';
									*/

									echo '<td style="text-align:right;">';
									echo number_format($arrPaymentsData[$ds["dataInvoicesTime"]]["RE"]["documentPayments"], '2', ',', '.');
									echo '</td>';

									echo '<td style="text-align:right;">';
									echo number_format($arrPaymentsData[$ds["dataInvoicesTime"]]["AB"]["documentPayments"], '2', ',', '.');
									echo '</td>';

									$totalPaymentsInvoices += $arrPaymentsData[$ds["dataInvoicesTime"]]["RE"]["documentPayments"];
									$totalPaymentsConfirmations += $arrPaymentsData[$ds["dataInvoicesTime"]]["AB"]["documentPayments"];

									echo '<td style="text-align:right;">';
									echo number_format(($arrPaymentsData[$ds["dataInvoicesTime"]]["RE"]["documentPayments"] + $arrPaymentsData[$ds["dataInvoicesTime"]]["AB"]["documentPayments"]), '2', ',', '.');
									echo '</td>';

									echo '<td class="spacer"></td>';

									echo '<td>';
									echo '<span class="toolItem">';
									echo '<img src="layout/icons/iconInfo.png" class="buttonShopOrderInfo" alt="Details" title="Detailinformationen ansehen">';
									echo '<div class="orderDetailsContent" style="display:none; visibility:hidden;">';
										echo '<h3>RE-DATEN (' . count($arrTempInvoiceData) . ' RECHNUNGEN)</h3>';
										$thisTempTotalSumInvoiceData = 0;
										if(!empty($arrTempInvoiceData)){
											echo '<table border="1" cellpadding="2" cellspacing="0" class="border">';
											echo '<tr><th>#</th><th>DOC-NR</th><th>SUMME</th></tr>';
											foreach($arrTempInvoiceData as $thisTempInvoiceDataKey => $thisTempInvoiceDataValue){
												$arrThisTempInvoiceData = explode(';', $thisTempInvoiceDataValue);
												$thisTempTotalSumInvoiceData += $arrThisTempInvoiceData[1];

												$dataRowClass = 'row0';
												if($thisTempInvoiceDataKey%2 == 0) {
													$dataRowClass = 'row1';
												}

												echo '<tr class="'.$dataRowClass.'">';
												echo '<td style="text-align:right;font-size:10px;">' . ($thisTempInvoiceDataKey + 1) . '</td>';
												echo '<td><b>' . $arrThisTempInvoiceData[0] . '</b></td>';
												echo '<td style="text-align:right;background-color:#FEFFAF">' . number_format($arrThisTempInvoiceData[1], 2, ',', '.') . '</td>';
												echo '</tr>';
											}
											echo '</table>';
										}
										echo '<b>SUMME: ' . number_format($thisTempTotalSumInvoiceData, 2, ',', '.') . '</b>';
										echo '<hr>';

										echo '<h3>GU-DATEN (' . count($arrTempCreditsData) . ' GUTSCHRIFTEN)</h3>';
										$thisTempTotalSumCreditsData = 0;
										if(!empty($arrTempCreditsData)){
											echo '<table border="1" cellpadding="2" cellspacing="0" class="border">';
											echo '<tr><th>#</th><th>DOC-NR</th><th>SUMME</th></tr>';
											foreach($arrTempCreditsData as $thisTempCreditsDataKey => $thisTempCreditsDataValue){
												$arrThisTempCreditsData = explode(';', $thisTempCreditsDataValue);
												$thisTempTotalSumCreditsData += $arrThisTempCreditsData[1];

												$dataRowClass = 'row0';
												if($thisTempCreditsDataKey%2 == 0) {
													$dataRowClass = 'row1';
												}

												echo '<tr class="'.$dataRowClass.'">';
												echo '<td style="text-align:right;font-size:10px;">' . ($thisTempCreditsDataKey + 1) . '</td>';
												echo '<td><b>' . $arrThisTempCreditsData[0] . '</b></td>';
												echo '<td style="text-align:right;background-color:#FEFFAF">' . number_format($arrThisTempCreditsData[1], 2, ',', '.') . '</td>';
												echo '</tr>';
											}
											echo '</table>';
										}
										echo '<b>SUMME: ' . number_format($thisTempTotalSumCreditsData, 2, ',', '.') . '</b>';
										echo '<hr>';
										echo '<b>DIFFERENZ RE-GU: ' . number_format(($thisTempTotalSumInvoiceData - $thisTempTotalSumCreditsData), 2, ',', '.') . '</b>';
										echo '<hr>';

									echo '</div>';
									echo '</span>';
									echo '</td>';

									echo '</tr>';
									$countRow++;
								}

								echo '<tr>';
								echo '<td colspan="22"><hr /></td>';
								echo '</tr>';

								echo '<tr class="row2">';
								echo '<td><b>Insg.</b></td>';
								#$dataInvoicesTotalPrice += $ds["dataInvoicesTotalPrice"];
								#$dataInvoicesMwstPrice += $ds["dataInvoicesMwstPrice"];
								#$dataCreditsTotalPrice += $ds["dataCreditsTotalPrice"];
								#$dataCreditsMwstPrice += $ds["dataCreditsMwstPrice"];
								echo '<td style="text-align:left;">';

								if($_POST["searchDateYear"] != "" && $_POST["searchDateMonth"] == ""){
									echo $_POST["searchDateYear"];
								}
								echo '</td>';

								echo '<td class="spacer"></td>';

								echo '<td style="text-align:right;font-size:10px;color:#999;">';
								echo $totalCountInvoices;
								echo '</td>';

								echo '<td style="text-align:right;">';
								echo number_format(($dataInvoicesTotalPrice - $dataInvoicesMwstPrice), '2', ',', '.');
								echo '</td>';

								echo '<td style="text-align:right;">';
								echo number_format($dataInvoicesMwstPrice, '2', ',', '.');
								echo '</td>';

								echo '<td style="text-align:right;">';
								echo number_format($dataInvoicesTotalPrice, '2', ',', '.');
								echo '</td>';

								echo '<td class="spacer"></td>';

								echo '<td style="text-align:right;font-size:10px;color:#999;">';
								echo $totalCountCredits;
								echo '</td>';

								echo '<td style="text-align:right;">';
								echo number_format(($dataCreditsTotalPrice - $dataCreditsMwstPrice), '2', ',', '.');
								echo '</td>';

								echo '<td style="text-align:right;">';
								echo number_format($dataCreditsMwstPrice, '2', ',', '.');
								echo '</td>';

								echo '<td style="text-align:right;">';
								echo number_format($dataCreditsTotalPrice, '2', ',', '.');
								echo '</td>';

								echo '<td class="spacer"></td>';

								echo '<td style="text-align:right;">';
								echo number_format((($dataInvoicesTotalPrice - $dataInvoicesMwstPrice) - ($dataCreditsTotalPrice - $dataCreditsMwstPrice)), '2', ',', '.');
								echo '</td>';

								echo '<td style="text-align:right;">';
								echo number_format(($dataInvoicesMwstPrice - $dataCreditsMwstPrice), '2', ',', '.');
								echo '</td>';

								echo '<td style="text-align:right;">';
								echo number_format(($dataInvoicesTotalPrice - $dataCreditsTotalPrice), '2', ',', '.');
								echo '</td>';

								echo '<td class="spacer"></td>';

								echo '<td style="text-align:right;">';
								echo number_format($totalPaymentsInvoices, '2', ',', '.');
								echo '</td>';

								echo '<td style="text-align:right;">';
								echo number_format($totalPaymentsConfirmations, '2', ',', '.');
								echo '</td>';

								echo '<td style="text-align:right;">';
								echo number_format(($totalPaymentsInvoices + $totalPaymentsConfirmations), '2', ',', '.');
								echo '</td>';

								echo '<td class="spacer"></td>';

								echo '<td>';
								echo '</td>';

								echo '</tr>';
								echo '</table>';
							}
							else {
								$warningMessage .= 'F&uuml;r den gew&auml;hlten Zeitraum wurden keine Daten gefunden.';
							}
							// EOF GET DATAS
						}
					?>
					<?php displayMessages(); ?>
				</div>

			</div>
		</div>
		<div class="clear"></div>
	</div>
</div>
<script language="javascript" type="text/javascript">
	$(document).ready(function() {
		$(function() {
			colorRowMouseOver('.displayOrders tbody tr');
			$('.buttonShopOrderInfo').css('cursor', 'pointer');
			$('.buttonShopOrderInfo').click(function () {
				loadOrderDetails($(this), '<?php echo BASEPATH; ?>', 'Details der Bestellung');
			});
		});
	});
</script>
<?php require_once('inc/footerHTML.inc.php'); ?>