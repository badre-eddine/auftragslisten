<?php
	require_once('inc/requires.inc.php');

	if(!$arrGetUserRights["displayOrderReceipts"]) {
		header('location: ' . PAGE_EXIT_LOCATION);
		exit;
	}

	$jswindowMessage = "";
	$warningMessage = '';
	$errorMessage = '';
	$successMessage = '';
	$infoMessage = '';

	$dbConnection = new DB_Connection();
	$db_open = $dbConnection->db_connect();

	$sql = "SET lc_time_names = 'de_DE'";
	$rs = $dbConnection->db_query($sql);

	if($_REQUEST["submitSearch"] == ''){
		$_REQUEST["searchDisplayYears"] = '0';
	}

	if($_REQUEST["searchYearsStart"] == ''){
		$_REQUEST["searchYearsStart"] = SOFTWARE_START_YEAR;
	}

	$defaultDisplayType = 'WEEK';
	if($_GET["displayType"] == "" && $_REQUEST["searchInterval"] == "") {
		$displayType = $defaultDisplayType;
	}
	else {
		if($_GET["displayType"] != "") {
			$displayType = $_GET["displayType"];
		}
		else if($_REQUEST["searchInterval"] != "") {
			$displayType = $_REQUEST["searchInterval"];
		}
	}

	// BOF DATE TYPE SELECTION
	if($_COOKIE["isAdmin"] == '1'){
		$defaultDateType = 'DATE_ENTRY';
		if($_REQUEST["searchDateType"] == '') {
			$searchDateType = $defaultDateType;
		}
		else {
			$searchDateType = $_REQUEST["searchDateType"];
		}
	}
	else {
		$searchDateType = $defaultDateType;
	}
	// EOF DATE TYPE SELECTION

	if($searchDateType != "DATE_RELEASE"){

		if($displayType == 'WEEK') {
			#$dateField = " CONCAT(DATE_FORMAT(`" . TABLE_ORDERS . "`.`ordersBestellDatum`, '%Y'), '-', DATE_FORMAT(`" . TABLE_ORDERS . "`.`ordersBestellDatum`, '%v'), '_', 'KW') ";
			$dateField = " CONCAT(IF(DATE_FORMAT(`" . TABLE_ORDERS . "`.`ordersBestellDatum`, '%m') = '12' AND DATE_FORMAT(`" . TABLE_ORDERS . "`.`ordersBestellDatum`, '%v') = '01', (DATE_FORMAT(`" . TABLE_ORDERS . "`.`ordersBestellDatum`, '%Y') + 1),  DATE_FORMAT(`" . TABLE_ORDERS . "`.`ordersBestellDatum`, '%Y')), '-', DATE_FORMAT(`" . TABLE_ORDERS . "`.`ordersBestellDatum`, '%v'), '_', 'KW') ";
			$groupField = "";
		}
		else if($displayType == 'YEAR') {
			$dateField = " CONCAT(DATE_FORMAT(`" . TABLE_ORDERS . "`.`ordersBestellDatum`, '%Y')) ";
			$groupField = "";
		}
		else if($displayType == 'MONTH') {
			$dateField = " CONCAT(DATE_FORMAT(`" . TABLE_ORDERS . "`.`ordersBestellDatum`, '%Y'), '-', DATE_FORMAT(`" . TABLE_ORDERS . "`.`ordersBestellDatum`, '%m')) ";
			$groupField = "";
		}
		else if($displayType == 'DAY') {
			$dateField = " `" . TABLE_ORDERS . "`.`ordersBestellDatum` ";
			$groupField = "";
		}
	}

	if($searchDateType == "DATE_RELEASE"){

		$listDateField = "
				IF(`" . TABLE_ORDERS . "`.`ordersFreigabeDatum` IS NOT NULL AND `" . TABLE_ORDERS . "`.`ordersFreigabeDatum` != '0000-00-00',
					`" . TABLE_ORDERS . "`.`ordersFreigabeDatum`,
					`" . TABLE_ORDERS . "`.`ordersBestellDatum`
				) AS `ordersListDatum`,
			";

		if($displayType == 'WEEK') {
			$dateField = "
				IF(`" . TABLE_ORDERS . "`.`ordersFreigabeDatum` IS NOT NULL AND `" . TABLE_ORDERS . "`.`ordersFreigabeDatum` != '0000-00-00',
					CONCAT(IF(DATE_FORMAT(`" . TABLE_ORDERS . "`.`ordersFreigabeDatum`, '%m') = '12' AND DATE_FORMAT(`" . TABLE_ORDERS . "`.`ordersFreigabeDatum`, '%v') = '01', (DATE_FORMAT(`" . TABLE_ORDERS . "`.`ordersFreigabeDatum`, '%Y') + 1),  DATE_FORMAT(`" . TABLE_ORDERS . "`.`ordersFreigabeDatum`, '%Y')), '-', DATE_FORMAT(`" . TABLE_ORDERS . "`.`ordersFreigabeDatum`, '%v'), '_', 'KW'),
					CONCAT(IF(DATE_FORMAT(`" . TABLE_ORDERS . "`.`ordersBestellDatum`, '%m') = '12' AND DATE_FORMAT(`" . TABLE_ORDERS . "`.`ordersBestellDatum`, '%v') = '01', (DATE_FORMAT(`" . TABLE_ORDERS . "`.`ordersBestellDatum`, '%Y') + 1),  DATE_FORMAT(`" . TABLE_ORDERS . "`.`ordersBestellDatum`, '%Y')), '-', DATE_FORMAT(`" . TABLE_ORDERS . "`.`ordersBestellDatum`, '%v'), '_', 'KW')
				) ";
			$groupField = "";
		}
		else if($displayType == 'YEAR') {
			$dateField = "
				IF(`" . TABLE_ORDERS . "`.`ordersFreigabeDatum` IS NOT NULL AND `" . TABLE_ORDERS . "`.`ordersFreigabeDatum` != '0000-00-00',
					CONCAT(DATE_FORMAT(`" . TABLE_ORDERS . "`.`ordersFreigabeDatum`, '%Y')),
					CONCAT(DATE_FORMAT(`" . TABLE_ORDERS . "`.`ordersBestellDatum`, '%Y'))
				) ";
			$groupField = "";
		}
		else if($displayType == 'MONTH') {
			$dateField = "
				IF(`" . TABLE_ORDERS . "`.`ordersFreigabeDatum` IS NOT NULL AND `" . TABLE_ORDERS . "`.`ordersFreigabeDatum` != '0000-00-00',
					CONCAT(DATE_FORMAT(`" . TABLE_ORDERS . "`.`ordersFreigabeDatum`, '%Y'), '-', DATE_FORMAT(`" . TABLE_ORDERS . "`.`ordersFreigabeDatum`, '%M')),
					CONCAT(DATE_FORMAT(`" . TABLE_ORDERS . "`.`ordersBestellDatum`, '%Y'), '-', DATE_FORMAT(`" . TABLE_ORDERS . "`.`ordersBestellDatum`, '%M'))
				) ";
			$groupField = "";
		}
		else if($displayType == 'DAY') {
			$dateField = "
				IF(`" . TABLE_ORDERS . "`.`ordersFreigabeDatum` IS NOT NULL AND `" . TABLE_ORDERS . "`.`ordersFreigabeDatum` != '0000-00-00',
					`" . TABLE_ORDERS . "`.`ordersFreigabeDatum`,
					`" . TABLE_ORDERS . "`.`ordersBestellDatum`
				) ";
			$groupField = "";
		}

	}

	// BOF GET RECEIPTS
		$arrReceiptQuantity = array();
		$arrReceiptPrints = array();
		
		// BOF KENNZEICHENHALTER
			$sql = "
					SELECT
						" . $dateField . " AS `orderReceiptDate`,

						SUM(`" . TABLE_ORDERS . "`.`ordersArtikelMenge`) AS `quantityOrders`,
						SUM((`" . TABLE_ORDERS . "`.`ordersArtikelMenge` * `" . TABLE_ORDERS . "`.`ordersArtikelPrintColorsCount`)) AS `quantityColors`,

						`" . TABLE_STOCK_PRODUCT_CATEGORIES . "`.`stockProductCategoriesName`,
						`" . TABLE_STOCK_PRODUCT_CATEGORIES . "`.`stockProductCategoriesParentID`

					FROM `" . TABLE_ORDERS . "`
					LEFT JOIN `" . TABLE_STOCK_PRODUCT_CATEGORIES . "`
					ON(`" . TABLE_ORDERS . "`.`ordersArtikelKategorieID` = `" . TABLE_STOCK_PRODUCT_CATEGORIES . "`.`stockProductCategoriesLevelID`)

					WHERE `" . TABLE_STOCK_PRODUCT_CATEGORIES . "`.`stockProductCategoriesParentID` = '001'
						AND `" . TABLE_ORDERS . "`.`ordersBestellDatum` != '0000-00-00'
				";
			if($_REQUEST["searchIncludeOpenOrders"] != '1'){
				$sql .= " AND `" . TABLE_ORDERS . "`.`ordersStatus` != 1 ";
			}


			$sql .= "
						AND `" . TABLE_ORDERS . "`.`ordersStatus` != 6

					GROUP BY " . $dateField . "
				";

			if($_REQUEST["searchYearsStart"] != ''){
				$sql .= " HAVING SUBSTRING(`orderReceiptDate`, 1, 4) >= '" . $_REQUEST["searchYearsStart"] . "' ";
			}
			else {
				$sql .= " HAVING SUBSTRING(`orderReceiptDate`, 1, 4) >= '" . SOFTWARE_START_YEAR . "' ";
			}

			$sql .= "
					ORDER BY `" . TABLE_ORDERS . "`.`ordersBestellDatum` DESC
			";
	
			$rs = $dbConnection->db_query($sql);
			while($ds = mysqli_fetch_assoc($rs)) {
				$arrReceiptQuantity[$ds["orderReceiptDate"]]["KENNZEICHENHALTER"] = $ds["quantityOrders"];
				$arrReceiptPrints[$ds["orderReceiptDate"]]["KENNZEICHENHALTER"] = $ds["quantityColors"];
			}
			#dd('arrReceiptQuantity');
		// EOF KENNZEICHENHALTER

		// BOF LEISTEN*/
			$sql = "

					SELECT
						`temp`.`orderReceiptDate` AS `orderReceiptDate`,
						SUM(`temp`.`quantityOrders`) AS `quantityOrders`,
						SUM(`temp`.`quantityOrders`) AS `quantityColors`,
						`temp`.`stockProductCategoriesName` AS `stockProductCategoriesName`,
						`temp`.`stockProductCategoriesParentID` AS `stockProductCategoriesParentID`

					FROM (

							SELECT
								" . $dateField . " AS `orderReceiptDate`,

								SUM(`" . TABLE_ORDERS . "`.`ordersAdditionalArtikelMenge`) AS `quantityOrders`,
								SUM((`" . TABLE_ORDERS . "`.`ordersAdditionalArtikelMenge` * `" . TABLE_ORDERS . "`.`ordersArtikelPrintColorsCount`)) AS `quantityColors`,

								`" . TABLE_STOCK_PRODUCT_CATEGORIES . "`.`stockProductCategoriesName`,
								`" . TABLE_STOCK_PRODUCT_CATEGORIES . "`.`stockProductCategoriesParentID`

							FROM `" . TABLE_ORDERS . "`
							LEFT JOIN `" . TABLE_STOCK_PRODUCT_CATEGORIES . "`
							ON(`" . TABLE_ORDERS . "`.`ordersAdditionalArtikelKategorieID` = `" . TABLE_STOCK_PRODUCT_CATEGORIES . "`.`stockProductCategoriesLevelID`)

							WHERE `" . TABLE_STOCK_PRODUCT_CATEGORIES . "`.`stockProductCategoriesParentID` = '002'
								AND `" . TABLE_ORDERS . "`.`ordersBestellDatum` != '0000-00-00'
				";
			if($_REQUEST["searchIncludeOpenOrders"] != '1'){
				$sql .= " AND `" . TABLE_ORDERS . "`.`ordersStatus` != 1 ";
			}
			$sql .= "
								AND `" . TABLE_ORDERS . "`.`ordersStatus` != 6

							GROUP BY " . $dateField . "

							UNION

							SELECT
								" . $dateField . " AS `orderReceiptDate`,

								SUM(`" . TABLE_ORDERS . "`.`ordersArtikelMenge`) AS `quantityOrders`,
								SUM((`" . TABLE_ORDERS . "`.`ordersArtikelMenge` * `" . TABLE_ORDERS . "`.`ordersArtikelPrintColorsCount`)) AS `quantityColors`,

								`" . TABLE_STOCK_PRODUCT_CATEGORIES . "`.`stockProductCategoriesName`,
								`" . TABLE_STOCK_PRODUCT_CATEGORIES . "`.`stockProductCategoriesParentID`

							FROM `" . TABLE_ORDERS . "`
							LEFT JOIN `" . TABLE_STOCK_PRODUCT_CATEGORIES . "`
							ON(`" . TABLE_ORDERS . "`.`ordersArtikelKategorieID` = `" . TABLE_STOCK_PRODUCT_CATEGORIES . "`.`stockProductCategoriesLevelID`)

							WHERE `" . TABLE_STOCK_PRODUCT_CATEGORIES . "`.`stockProductCategoriesParentID` = '002'
								AND `" . TABLE_ORDERS . "`.`ordersBestellDatum` != '0000-00-00'
				";
			if($_REQUEST["searchIncludeOpenOrders"] != '1'){
				$sql .= " AND `" . TABLE_ORDERS . "`.`ordersStatus` != 1 ";
			}
			$sql .= "
								AND `" . TABLE_ORDERS . "`.`ordersStatus` != 6

							GROUP BY " . $dateField . "

							ORDER BY `orderReceiptDate` DESC
					) AS `temp`

					GROUP BY `temp`.`orderReceiptDate`
			";

			if($_REQUEST["searchYearsStart"] != ''){
				$sql .= " HAVING SUBSTRING(`orderReceiptDate`, 1, 4) >= '" . $_REQUEST["searchYearsStart"] . "' ";
			}
			else {
				$sql .= " HAVING SUBSTRING(`orderReceiptDate`, 1, 4) >= '" . SOFTWARE_START_YEAR . "' ";
			}

			$rs = $dbConnection->db_query($sql);
			while($ds = mysqli_fetch_assoc($rs)) {
				if($arrReceiptQuantity[$ds["orderReceiptDate"]]["LEISTEN"] == '') {$arrReceiptQuantity[$ds["orderReceiptDate"]]["LEISTEN"] = 0;}
				$arrReceiptQuantity[$ds["orderReceiptDate"]]["LEISTEN"] = $ds["quantityOrders"];
				$arrReceiptPrints[$ds["orderReceiptDate"]]["LEISTEN"] = $ds["quantityColors"];
			}
		// EOF LEISTEN*/

		// BOF GRUNDPLATTEN*/
			$sql = "
					SELECT
						" . $dateField . " AS `orderReceiptDate`,

						SUM(`" . TABLE_ORDERS . "`.`ordersArtikelMenge`) AS `quantityOrders`,
						'0' AS `quantityColors`,
						
						`" . TABLE_STOCK_PRODUCT_CATEGORIES . "`.`stockProductCategoriesName`,
						`" . TABLE_STOCK_PRODUCT_CATEGORIES . "`.`stockProductCategoriesParentID`

					FROM `" . TABLE_ORDERS . "`
					LEFT JOIN `" . TABLE_STOCK_PRODUCT_CATEGORIES . "`
					ON(`" . TABLE_ORDERS . "`.`ordersArtikelKategorieID` = `" . TABLE_STOCK_PRODUCT_CATEGORIES . "`.`stockProductCategoriesLevelID`)

					WHERE `" . TABLE_STOCK_PRODUCT_CATEGORIES . "`.`stockProductCategoriesParentID` = '005'
						AND `" . TABLE_ORDERS . "`.`ordersBestellDatum` != '0000-00-00'
				";
			if($_REQUEST["searchIncludeOpenOrders"] != '1'){
				$sql .= " AND `" . TABLE_ORDERS . "`.`ordersStatus` != 1 ";
			}
			$sql .= "
						AND `" . TABLE_ORDERS . "`.`ordersStatus` != 6

					GROUP BY " . $dateField . "
				";

			if($_REQUEST["searchYearsStart"] != ''){
				$sql .= " HAVING SUBSTRING(`orderReceiptDate`, 1, 4) >= '" . $_REQUEST["searchYearsStart"] . "' ";
			}
			else {
				$sql .= " HAVING SUBSTRING(`orderReceiptDate`, 1, 4) >= '" . SOFTWARE_START_YEAR . "' ";
			}

			$sql .= "
					ORDER BY `" . TABLE_ORDERS . "`.`ordersBestellDatum` DESC
				";
			$rs = $dbConnection->db_query($sql);
			while($ds = mysqli_fetch_assoc($rs)) {
				$arrReceiptQuantity[$ds["orderReceiptDate"]]["GRUNDPLATTEN"] = $ds["quantityOrders"];
				$arrReceiptPrints[$ds["orderReceiptDate"]]["GRUNDPLATTEN"] = $ds["quantityColors"];
			}
		// EOF GRUNDPLATTEN*/
	// EOF GET RECEIPTS
?>
<?php
	require_once('inc/headerHTML.inc.php');
	$thisTitle = "Bestellungs-Eing&auml;nge";
	if($searchDateType != 'DATE_RELEASE'){
		$thisTitle .= " nach Eingangsdatum";
	}
	else {
		$thisTitle .= " nach Freigabedatum";
	}

	if($displayType == 'YEAR') { $thisTitle .= ': Jahres-Ansicht'; }
	if($displayType == 'QUARTER') { $thisTitle .= ': Quartals-Ansicht'; }
	if($displayType == 'MONTH') { $thisTitle .= ': Monats-Ansicht'; }
	if($displayType == 'WEEK') { $thisTitle .= ': Wochen-Ansicht'; }
	if($displayType == 'DAY') { $thisTitle .= ': Tages-Ansicht'; }
	$headerHTML = preg_replace("/{###TITLE###}/", strip_tags($thisTitle), $headerHTML);
	echo $headerHTML;

	require_once(FILE_MENUE_TOP);
?>
<div id="mainArea">
	<div id="mainContent">
		<?php require_once(FILE_MENUE_SIDEBAR); ?>
<div id="contentArea">
	<div id="contentAreaElements">
		<h1><?php if(SHOW_PAGE_TITLE_ICONS) { echo '<img src="' . PATH_ICONS_MENUE_TITLES . 'statistics.png" alt="" />'; } ?> <?php echo $thisTitle; ?></h1>
		<p class="infoArea">Die Daten basieren auf den Eintr&auml;gen in der Produktion (keine offenen und stornierten Auftr&auml;ge!).</p>
		<?php displayMessages(); ?>

		<div id="searchFilterArea">
			<form name="formSearchDatas" method="post" action="<?php echo $_SERVER["PHP_SELF"]; ?>">
				<table border="0" cellpadding="0" cellspacing="0" class="searchFilterContent">
					<tr>
						<!--
						<td><a href="<?php echo $_SERVER["PHP_SELF"]; ?>?displayType=DAY" <?php if($displayType == "DAY"){ echo ' class="active" '; } ?> >Tages-Ansicht</a></td>
						<td><a href="<?php echo $_SERVER["PHP_SELF"]; ?>?displayType=WEEK" <?php if($displayType == "WEEK"){ echo ' class="active" '; } ?> >KW-Ansicht</a></td>
						<td><a href="<?php echo $_SERVER["PHP_SELF"]; ?>?displayType=MONTH" <?php if($displayType == "MONTH"){ echo ' class="active" '; } ?> >Monats-Ansicht</a></td>
						<td><a href="<?php echo $_SERVER["PHP_SELF"]; ?>?displayType=YEAR" <?php if($displayType == "YEAR"){ echo ' class="active" '; } ?> >Jahres-Ansicht</a></td>
						-->
						<td>
							<label for="searchInterval">Zeitraum:</label>
							<select name="searchInterval" id="searchInterval" class="inputField_130">
								<!-- <option value="DAY" <?php if($displayType == 'DAY'){ echo ' selected="selected" '; } ?> >pro Tag</option> -->
								<option value="WEEK" <?php if($displayType == 'WEEK'){ echo ' selected="selected" '; } ?> >pro Woche</option>
								<option value="MONTH" <?php if($displayType == 'MONTH'){ echo ' selected="selected" '; } ?> >pro Monat</option>
								<!-- <option value="QUARTER" <?php if($displayType == 'QUARTER'){ echo ' selected="selected" '; } ?> >pro Vierteljahr</option> -->
								<option value="YEAR" <?php if($displayType == 'YEAR'){ echo ' selected="selected" '; } ?> >pro Jahr</option>
							 </select>
						</td>

						<td>
							<label for="searchYearsStart">ab Jahr:</label>
							<select name="searchYearsStart" id="searchYearsStart" class="inputField_130">
								<!--<option value=""></option>-->
								<?php
									for($i = date('Y'); $i > 2012 ; $i--){
										$selected = '';
										if($i == $_REQUEST["searchYearsStart"]){
											$selected = ' selected="selected" ';
										}
										echo '<option value="' . $i . '" ' . $selected . '>' . $i . '</option>';
									}
								?>
							</select>
						</td>

						<td>
							<label for="searchDateType">Datumsart:</label>
							<select name="searchDateType" id="searchDateType" class="inputField_200">
								<option value="DATE_ENTRY" <?php if($searchDateType == 'DATE_ENTRY'){ echo ' selected="selected" '; } ?> >nach Eingangsdatum</option>
								<option value="DATE_RELEASE" <?php if($searchDateType == 'DATE_RELEASE'){ echo ' selected="selected" '; } ?> >nach Freigabedatum</option>
							</select>
						</td>

						<td>
							<?php
								$checked = '';
								if($_REQUEST["searchIncludeOpenOrders"] == '1'){
									$checked = ' checked="checked" ';
								}
							?>
							<input type="checkbox" name="searchIncludeOpenOrders" value="1" <?php echo $checked; ?> /> Auch offene Bestellungen anzeigen?
						</td>

						<td>
							<?php
								$checked = '';
								if($_REQUEST["searchDisplayYears"] == '1'){
									$checked = ' checked="checked" ';
								}
							?>
							<input type="checkbox" name="searchDisplayYears" value="1" <?php echo $checked; ?> /> Jahre nebeneinander anzeigen?
						</td>

						<td>
							<input type="hidden" name="editID" id="editID" value="" />
							<input type="submit" name="submitSearch" class="inputButton0" value="Anzeigen" />
						</td>
					</tr>
				</table>
				<input type="hidden" name="submitSearchForm" id="submitSearchForm" value="1" />
			</form>
		</div>

		<!-- BOF GRAPH ELEMENTS -->
		<script type="text/javascript" language="javascript" src="<?php echo PATH_RGRAPH; ?>libraries/RGraph.common.core.js" ></script>
		<script type="text/javascript" language="javascript" src="<?php echo PATH_RGRAPH; ?>libraries/RGraph.bar.js" ></script>
		<script type="text/javascript" language="javascript" src="<?php echo PATH_RGRAPH; ?>libraries/RGraph.line.js" ></script>
		<canvas id="cvs" width="900" height="250">[No canvas support]</canvas>
		<!-- EOF GRAPH ELEMENTS -->

		<div class="contentDisplay">
			<?php

				if(!empty($arrReceiptQuantity)) {
					#dd('arrReceiptQuantity');
					if($searchDateType == "DATE_RELEASE"){
						krsort($arrReceiptQuantity);
						krsort($arrReceiptPrints);
					}
					$count = 0;
					$thisMarker = "";

					if($_REQUEST["searchDisplayYears"] == '1'){

						// BOF TODAY INTERVAL
							if($displayType == 'WEEK') {
								$currentInterval = date('W');
							}
							else if($displayType == 'YEAR') {
								$currentInterval = date('Y');
							}
							else if($displayType == 'MONTH') {
								$currentInterval = date('m');
							}
							else if($displayType == 'DAY') {
								$currentInterval = date('d');
							}
						// EOF TODAY INTERVAL

						// BOF GET YEARS AND INTERVAL
							$arrReceiptQuantityYears = array();
							$arrReceiptQuantityInterval = array();

							#$arrReceiptPrintsYears = array();
							#$arrReceiptPrintsInterval = array();
							foreach(array_keys($arrReceiptQuantity) as $thisReceiptQuantityKey){
								$arrReceiptQuantityYears[] = substr($thisReceiptQuantityKey, 0, 4);
								$arrReceiptQuantityInterval[] = substr($thisReceiptQuantityKey, 5, strlen($thisReceiptQuantityKey));

								#$arrReceiptPrintsYears[] = substr($thisReceiptQuantityKey, 0, 4);
								#$arrReceiptPrintsInterval[] = substr($thisReceiptQuantityKey, 5, strlen($thisReceiptQuantityKey));
							}

							if(!empty($arrReceiptQuantityYears)) {
								$arrReceiptQuantityYears = array_unique($arrReceiptQuantityYears);
								rsort($arrReceiptQuantityYears);

								#$arrReceiptPrintsYears = array_unique($arrReceiptPrintsYears);
								#rsort($arrReceiptPrintsYears);
							}
							if(!empty($arrReceiptQuantityInterval)) {
								$arrReceiptQuantityInterval = array_unique($arrReceiptQuantityInterval);
								rsort($arrReceiptQuantityInterval);

								#$arrReceiptPrintsInterval = array_unique($arrReceiptPrintsInterval);
								#rsort($arrReceiptPrintsInterval);
							}
							#dd('arrReceiptQuantityYears');
							#dd('arrReceiptQuantityInterval');
						// EOF GET YEARS AND INTERVAL


						echo '<table border="0" cellpadding="0" cellspacing="0" width="100%" class="displayOrders">';

						echo '<thead>';

						echo '<tr>';
						echo '<td colspan="2" class="tableRowTitle1"></td>';
						echo '<td class="spacer"></td>';
						foreach($arrReceiptQuantityYears as $thisReceiptQuantityYear){
							echo '<td colspan="5" class="tableRowTitle1">' . $thisReceiptQuantityYear . '</td>';
							echo '<td class="spacer"></td>';
						}
						echo '</tr>';

						echo '<tr>';
						echo '<th width="3%">#</th>';
						echo '<th width="15%">Zeitraum</th>';
						echo '<th class="spacer"></th>';

						$thisCellWidth = round(((80 / 5) / count($arrReceiptQuantityYears)) + 2);
						
						foreach($arrReceiptQuantityYears as $thisReceiptQuantityYear){
							
							echo '<th width="' . $thisCellWidth . '%">KZH</th>';
							echo '<th width="' . $thisCellWidth . '%">LEI</th>';
							echo '<th width="' . $thisCellWidth . '%">GP</th>';
							echo '<th width="' . $thisCellWidth . '%">&sum; <b>KZH + LEI</b></th>';
							echo '<th width="' . $thisCellWidth . '%">&sum; <b>Drucke</b></th>';
							echo '<th class="spacer"></th>';
						}
						echo '</tr>';

						echo '</thead>';

						echo '<tbody>';

						$count = 0;

						foreach($arrReceiptQuantityInterval as $thisReceiptQuantityInterval){
							if($count%2 == 0){ $rowClass = 'row0'; }
							else { $rowClass = 'row1'; }
							echo '<tr class="' . $rowClass . '">';

							echo '<td style="text-align:right;"><b>' .  ($count+1) . '.</b></td>';

							if($displayType == 'WEEK') {
								$thisDateDisplay = str_replace('_', '. ', $thisReceiptQuantityInterval);
							}
							else if($displayType == 'YEAR') {
								$thisDateDisplay = $thisReceiptQuantityYear;
							}
							else if($displayType == 'MONTH') {
								$thisDateDisplay = getTimeNames($thisReceiptQuantityInterval, 'month', 'long');
							}
							else if($displayType == 'DAY') {
								$arrDateDisplay = explode("-", $thisReceiptQuantityInterval);
								$thisDateDisplay = $arrDateDisplay[1] . '.' . $arrDateDisplay[0];
							}

							echo '<td style="font-weight:bold;">' .  $thisDateDisplay . '</td>';
							echo '<td class="spacer"></td>';

							foreach($arrReceiptQuantityYears as $thisReceiptQuantityYear){

								$thisIntervalIndex = $thisReceiptQuantityYear;
								if($thisReceiptQuantityInterval != ""){
									$thisIntervalIndex .= '-' . $thisReceiptQuantityInterval;
								}

								echo '<td style="text-align:right;">';
								echo number_format($arrReceiptQuantity[$thisIntervalIndex]["KENNZEICHENHALTER"], 0, ',', '.');
								echo '</td>';

								echo '<td style="text-align:right;">';
								echo number_format($arrReceiptQuantity[$thisIntervalIndex]["LEISTEN"], 0, ',', '.');
								echo '</td>';

								echo '<td style="text-align:right;">';
								echo number_format($arrReceiptQuantity[$thisIntervalIndex]["GRUNDPLATTEN"], 0, ',', '.');
								echo '</td>';

								/*
								echo '<td>';
								echo ($arrReceiptQuantity[$thisIntervalIndex]["KENNZEICHENHALTER"] + $arrReceiptQuantity[$thisIntervalIndex]["LEISTEN"] + $arrReceiptQuantity[$thisIntervalIndex]["GRUNDPLATTEN"]);
								echo '</td>';
								*/

								echo '<td style="background-color:#FEFFAF;text-align:right;font-weight:bold;">';
								echo number_format(($arrReceiptQuantity[$thisIntervalIndex]["KENNZEICHENHALTER"] + $arrReceiptQuantity[$thisIntervalIndex]["LEISTEN"]), 0, ',', '.');
								echo '</td>';

								echo '<td style="background-color:#FEFFAF;text-align:right;font-weight:bold;">';
								echo number_format(($arrReceiptPrints[$thisIntervalIndex]["KENNZEICHENHALTER"] + $arrReceiptQuantity[$thisIntervalIndex]["LEISTEN"]), 0, ',', '.');
								echo '</td>';

								echo '<td class="spacer"></td>';
								$arrGraphDatas[$thisIntervalIndex] = $arrReceiptQuantity[$thisIntervalIndex]["KENNZEICHENHALTER"] + $arrReceiptQuantity[$thisIntervalIndex]["LEISTEN"];
							}

							echo '</tr>';

							$count++;
						}



						echo '</tbody>';

						echo '</table>';

					}
					else {
						echo '<table border="0" cellpadding="0" cellspacing="0" width="100%" class="displayOrders">';
						echo '<thead>';
						echo '<tr>';
						echo '<th width="3%">#</th>';
						echo '<th width="17%">Zeitraum</th>';
						echo '<th width="16%">Kennzeichenhalter</th>';
						echo '<th width="16%">Leisten</th>';
						echo '<th width="16%">Grundplatten</th>';
						echo '<th width="16%">&sum; Kennzeichenhalter + Leisten</th>';
						echo '<th width="16%">&sum; Drucke</th>';
						// echo '<th>Insgesamt</th>';
						echo '</tr>';
						echo '</thead>';

						echo '<tbody>';
						#dd('arrReceiptQuantity');
						foreach($arrReceiptQuantity as $thisKey => $thisValue) {

							if($displayType == 'WEEK') {
								$arrDateDisplay = explode("-", $thisKey);
								$thisDateDisplay = str_replace('_', '. ', $arrDateDisplay[1]);
								if($thisMarker != $arrDateDisplay[0]) {
									$thisMarker = $arrDateDisplay[0];
									echo '<tr><td colspan="7" class="tableRowTitle1">' . $thisMarker . '</td></tr>';
								}
							}
							else if($displayType == 'YEAR') {
								$thisDateDisplay = $thisKey;
							}
							else if($displayType == 'MONTH') {
								$arrDateDisplay = explode("-", $thisKey);
								#$thisDateDisplay = $arrDateDisplay[1] . ' ' . $arrDateDisplay[0];								
								$thisDateDisplay = getTimeNames($arrDateDisplay[1], 'month', 'long');
								
								if($thisMarker != $arrDateDisplay[0]) {
									$thisMarker = $arrDateDisplay[0];
									echo '<tr><td colspan="7" class="tableRowTitle1">' . $thisMarker . '</td></tr>';
								}
							}
							else if($displayType == 'DAY') {
								$arrDateDisplay = explode("-", $thisKey);
								$thisDateDisplay = formatDate($thisKey, 'display');
								if($thisMarker != $arrDateDisplay[0]) {
									$thisMarker = $arrDateDisplay[0];
									echo '<tr><td colspan="7" class="tableRowTitle1">' . $thisMarker . '</td></tr>';
								}
							}

							if($count%2 == 0){ $rowClass = 'row0'; }
							else { $rowClass = 'row1'; }
							echo '<tr class="' . $rowClass . '">';

							echo '<td style="text-align:right;"><b>' .  ($count+1) . '.</b></td>';
							echo '<td style="text-align:right;">';
							echo $thisDateDisplay;
							echo '</td>';

							echo '<td style="text-align:right;">' .  number_format($thisValue["KENNZEICHENHALTER"], 0, ',', '.') . '</td>';
							echo '<td style="text-align:right;">' .  number_format($thisValue["LEISTEN"], 0, ',', '.') . '</td>';

							echo '<td style="text-align:right;">' . number_format($thisValue["GRUNDPLATTEN"], 0, ',', '.') . '</td>';
							// echo '<td>' .  ($thisValue["KENNZEICHENHALTER"] + $thisValue["LEISTEN"] + $thisValue["GRUNDPLATTEN"]) . '</td>';
							echo '<td style="background-color:#FEFFAF;text-align:right;font-weight:bold;">' .  number_format(($thisValue["KENNZEICHENHALTER"] + $thisValue["LEISTEN"]), 0, ',', '.') . '</td>';
							echo '<td style="background-color:#FEFFAF;text-align:right;">' .  number_format(($arrReceiptPrints[$thisKey]["KENNZEICHENHALTER"] + $arrReceiptPrints[$thisKey]["LEISTEN"]), 0, ',', '.') . '</td>';
							echo '</tr>';
							$count++;
							$arrGraphDatas[$thisDateDisplay] = $thisValue["KENNZEICHENHALTER"] + $thisValue["LEISTEN"];
						}
						echo '</tbody>';
						echo '</table>';
					}
				}
			?>
		</div>
	</div>
</div>
</div>
</div>
<script language="javascript" type="text/javascript">
	$(document).ready(function() {
		colorRowMouseOver('.displayOrders tr');

		// BOF GET DATAS FOR GRAPH
			<?php
				if(!empty($arrGraphDatas)){
				$arrGraphDatas = array_reverse($arrGraphDatas, true);
			?>
			var data	= [<?php echo implode(",", array_values($arrGraphDatas)); ?>];
			var labels	= ['<?php echo implode("','", array_keys($arrGraphDatas)); ?>'];
			labels = '';

			/*
			var bar = new RGraph.Bar('cvs', data);
			bar.Set('labels', labels);
			bar.Set('labels.above', true);
			bar.Draw();
			*/
			var line = new RGraph.Line('cvs', data);
            line.Set('chart.labels', labels);
			line.Set('labels.above', true);

			line.Set('colors', ['#FFD600']);

            line.Set('shadow', true);
            line.Set('shadow.offsetx', 2);
            line.Set('shadow.offsety', 2);
            line.Set('shadow.blur', 4);
			line.Set('chart.linewidth', 1);
            line.Set('chart.colors.alternate', true);
            line.Set('chart.colors', [['green','red']]);
            line.Draw();

            line.Draw();

			<?php
				}
			?>
		// EOF GET DATAS FOR GRAPH
	});
</script>

<?php require_once('inc/footerHTML.inc.php'); ?>