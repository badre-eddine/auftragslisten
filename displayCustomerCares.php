<?php
	require_once('inc/requires.inc.php');

	if(!$arrGetUserRights["displayDistribution"]) {
		header('location: ' . PAGE_EXIT_LOCATION);
		exit;
	}

	function showActivityStatusData(){
		$content = '';
		echo $content;
	}

	if($_COOKIE["isAdmin"] == 1){
		DEFINE('SHOW_ORDER_POSITIONS', true); // true | false
	}
	else {
		DEFINE('SHOW_ORDER_POSITIONS', true); // true | false
	}


	$jswindowMessage = '';
	$warningMessage = '';
	$errorMessage = '';
	$successMessage = '';
	$infoMessage = '';

	$todayDate = date('Y-m-d');
	$todayWeek = date('W');
	$todayDayNumber = date('W');

	$dbConnection = new DB_Connection();
	$db_open = $dbConnection->db_connect();

	// BOF MIN DATAS
		DEFINE('MIN_QUOTA_NEW_CUSTOMERS', '70'); // %
		DEFINE('MIN_QUOTA_ORDERS', '70'); // %
	// EOF MIN DATAS

	// BOF READ MANDATORY TYPES
		$arrMandatoriesCompanyDatas = getAllMandatoryCompanyDatas();
		DEFINE('EXPORT_COMPANY_NAME', $arrMandatoriesCompanyDatas[MANDATOR]["COMPANY_NAME_LONG"]);
	// EOF READ MANDATORY TYPES

	// BOF READ USER DATAS
		$arrUserDatas = getUserDatas();
	// EOF READ USER DATAS

	// BOF READ PERSONNEL DATAS
		$arrPersonnelDatas = getPersonnelDatas();
		DEFINE('EXPORT_PERSON_NAME', $arrPersonnelDatas[$arrUserDatas["usersUserToPersonnel"]]["personnelFirstName"] . ' ' . $arrPersonnelDatas[$arrUserDatas["usersUserToPersonnel"]]["personnelLastName"]);
	// EOF READ PERSONNEL DATAS

	// EOF READ PERSONNEL DATAS
		$arrPersonnelTypes = getPersonnelTypes();
		DEFINE('EXPORT_PERSON_FUNCTION', $arrPersonnelTypes[$arrPersonnelDatas[$arrUserDatas["usersUserToPersonnel"]]["personnelGroup"]]["personnelTypesName"]);
	// EOF READ PERSONNEL DATAS

	// BOF READ SALESMEN WITH ZIP CODES
		$arrSalesmenWithActiveAreasData = getSalesmenWithActiveAreas();
		#$arrRelationSalesmenZipcodeDatas = getRelationSalesmenZipcode();
		$arrRelationSalesmenZipcodeDatas = getRelationSalesmenZipcode2();
	// EOF READ SALESMEN WITH ZIP CODES

	// BOF OF DEFINE MONTHS
		$arrUseMonths = array(
			"01" => "Januar",
			"02" => "Februar",
			"03" => "März",
			"04" => "April",
			"05" => "Mai",
			"06" => "Juni",
			"07" => "Juli",
			"08" => "August",
			"09" => "September",
			"10" => "Oktober",
			"11" => "November",
			"12" => "Dezember",
		);
	// EOF OF DEFINE MONTHS

	// BOF TEST AREAS
		// $arrTestAreas DEFINED IN CONFIG
		if(!empty($arrTestAreas)){
			foreach($arrTestAreas as $thisTestAreaKey => $thisTestValue){
				$arrSalesmenWithActiveAreasData["testID_" . $thisTestAreaKey] = array(
					"salesmenID" => "testID_" . $thisTestAreaKey,
					"salesmenKundennummer" => "testID_" . $thisTestAreaKey,
					"salesmenFirmenname" => "Testgebiet " . $thisTestAreaKey . ' (' . $thisTestValue["name"] . ')',
					"salesmenAreas" => $thisTestValue["area"],
					"salesmenAreasActive" => 0,
					"salesmenAreaProtection" => 0,
					"customersID" => "testID_" . $thisTestAreaKey,
					"customersKundennummer" => "testID_" . $thisTestAreaKey,
					"customersFirmenname" => "Testgebiet " . $thisTestAreaKey,
					"customersFirmenInhaberVorname" => "",
					"customersFirmenInhaberNachname" => "",
					"customersTyp" => 0,
					"customersVertreterID" => 0,
					"customersActive" => 0
				);
			}
		}
	// EOF TEST AREAS

	// BOF GET ALL DOUBLE AND TRIPLE ZIP CODE AREAS
		$arrAllZipcodes = getAllZipcodes();
		$arrAllDoubleZipCodes = $arrAllZipcodes['double'];
		$arrAllTripleZipCodes = $arrAllZipcodes['triple'];
	// EOF GET ALL DOUBLE AND TRIPLE ZIP CODE AREAS

	// BOF GET NOT USED ZIP CODE AREAS
		$arrNotUsedZipCodeAreas = getNotUsedZipcodeAreas($arrSalesmenWithActiveAreasData);

		$arrNotUsedZipCodeAreasNew = array();
		if(!empty($arrNotUsedZipCodeAreas)){
			foreach($arrNotUsedZipCodeAreas as $thisNotUsedZipCodeAreas){
				$arrNotUsedZipCodeAreasNew[$thisNotUsedZipCodeAreas . str_repeat("0", (3 - strlen($thisNotUsedZipCodeAreas)))] = $thisNotUsedZipCodeAreas;
			}
			ksort($arrNotUsedZipCodeAreasNew);
		}
		$arrNotUsedZipCodeAreas = array();
		if(!empty($arrNotUsedZipCodeAreasNew)){
			foreach($arrNotUsedZipCodeAreasNew as $thisNotUsedZipCodeAreas){
				$arrNotUsedZipCodeAreas[] = $thisNotUsedZipCodeAreas;
			}
		}

		if(!empty($arrNotUsedZipCodeAreas)){
			foreach($arrNotUsedZipCodeAreas as $thisNotUsedZipCodeAreas){
				$arrSalesmenWithActiveAreasData["testID_" . $thisNotUsedZipCodeAreas] = array(
					"salesmenID" => "testID_" . $thisNotUsedZipCodeAreas,
					"salesmenKundennummer" => "testID_" . $thisNotUsedZipCodeAreas,
					"salesmenFirmenname" => "Unbenutzte PLZ " . $thisNotUsedZipCodeAreas . ' | nicht vergeben',
					"salesmenAreas" => $thisNotUsedZipCodeAreas,
					"salesmenAreasActive" => 0,
					"salesmenAreaProtection" => 0,
					"customersID" => "testID_" . $thisNotUsedZipCodeAreas,
					"customersKundennummer" => "testID_" . $thisNotUsedZipCodeAreas,
					"customersFirmenname" => "PLZ " . $thisNotUsedZipCodeAreas . ' (frei)',
					"customersFirmenInhaberVorname" => "",
					"customersFirmenInhaberNachname" => "",
					"customersTyp" => 0,
					"customersVertreterID" => 0,
					"customersActive" => 0
				);
			}
		}
	// EOF GET NOT USED ZIP CODE AREAS

	// BOF SHOW ALL DOUBLE ZIPCODE AREAS
		if($_COOKIE["isAdmin"]){
			if(!empty($arrAllDoubleZipCodes)){
				foreach($arrAllDoubleZipCodes as $thisDoubleZipCodeKey => $thisDoubleZipCodeValue){
					$arrThisAreaSalesmen = getSalesmanInZipcodeArea2($arrRelationSalesmenZipcodeDatas, $thisDoubleZipCodeKey);
					$thisAreaSalesmen = '';
					$arrTempAreaSalesmen = array();
					if(!empty($arrThisAreaSalesmen)){
						foreach($arrThisAreaSalesmen as $thisAreaSalesmenData){
							$arrTempAreaSalesmen[] = $thisAreaSalesmenData["kundenname"];
						}
						$thisAreaSalesmen = "Verteter im Gebiet: " . implode(" &bull; ", $arrTempAreaSalesmen);
					}

					$arrSalesmenWithActiveAreasData["plzID_" . $thisDoubleZipCodeKey] = array(
						"salesmenID" => "plzID_" . $thisDoubleZipCodeKey,
						"salesmenKundennummer" => "plzID_" . $thisDoubleZipCodeKey,
						"salesmenFirmenname" => "PLZ " . $thisDoubleZipCodeKey . ' | ' . $thisAreaSalesmen,
						"salesmenAreas" => $thisDoubleZipCodeKey,
						"salesmenAreasActive" => 0,
						"salesmenAreaProtection" => 0,
						"customersID" => "plzID_" . $thisDoubleZipCodeKey,
						"customersKundennummer" => "plzID_" . $thisDoubleZipCodeKey,
						"customersFirmenname" => "",
						"customersFirmenInhaberVorname" => "",
						"customersFirmenInhaberNachname" => "",
						"customersTyp" => 0,
						"customersVertreterID" => 0,
						"customersActive" => 0
					);
				}
			}
		}
	// EOF SHOW ALL DOUBLE ZIPCODE AREAS

	// BOF SET EXPORT MODUS
		$doExport = false;
		if(
			(int)$_POST["searchSalesman"] > 0 &&
			in_array($_POST["searchInterval"], array("MONTH", "WEEK")) &&
			$_POST["searchDisplay"] == "AREA_TOTAL" &&
			$_POST["exportExecute"] != "" &&
			$_POST["exportType"] != ""
		) {
			$doExport = true;
		}

		if($doExport){
			$downloadFolder = "kundenbetreuung/";
			if(!is_dir(DIRECTORY_EXPORT_FILES . $downloadFolder)){
				mkdir(DIRECTORY_EXPORT_FILES . $downloadFolder);
			}
			$excelBasicFileName = "Kundenbetreuung_{###SALESMAN_NAME###}_KNR-{###SALESMAN_KNR###}_{###YEAR###}_{###DATE_TYPE###}";

			$fileNameCreate = $excelBasicFileName;
			$fileNameCreate = preg_replace("/{###SALESMAN_NAME###}/", convertChars($arrSalesmenWithActiveAreasData[$_REQUEST["searchSalesman"]]["salesmenFirmenname"]), $fileNameCreate);
			$fileNameCreate = preg_replace("/{###YEAR###}/", $_POST["searchYear"], $fileNameCreate);
			$fileNameCreate = preg_replace("/{###SALESMAN_KNR###}/", $arrSalesmenWithActiveAreasData[$_REQUEST["searchSalesman"]]["salesmenKundennummer"], $fileNameCreate);
			if($_POST["searchInterval"] == "WEEK"){
				$fileNameInterval = 'pro-Woche';
			}
			else if($_POST["searchInterval"] == "MONTH"){
				$fileNameInterval = 'pro-Monat';
			}
			$fileNameCreate = preg_replace("/{###DATE_TYPE###}/", $fileNameInterval, $fileNameCreate);

			$thisStoreDir = DIRECTORY_EXPORT_FILES . $downloadFolder . convertChars($arrSalesmenWithActiveAreasData[$_REQUEST["searchSalesman"]]["salesmenFirmenname"]) . '/';
			if(!is_dir($thisStoreDir)){
				mkdir($thisStoreDir);
			}

			$arrReplaceFields = array(
				"A1" => "Gebietsauswertung {###YEAR###}",
				"A3" => "Datum: {###DATE_TO###}",
				"A5" => "Vertreter: {###SALESMAN_NAME###},  KNR: {###SALESMAN_KNR###} ",
				"A7" => "PLZ-Gebiete: {###ZIPCODE_AREAS###}",
			);

			if($_POST["exportType"] == 'xls'){
				require_once (PATH_PHP_EXCEL . 'Classes/PHPExcel.php');
				$fileNameCreate .= ".xls";

				$excelSheetName = "SEITE {###SEITE###}";
				$excelNumberOfSheets = 1;
				$excelRowHeight = 17;
				$excelLastHeaderRow = 5;

				$excelLogoPath = 'C:/Auftragslisten_SERVER/webroot/Auftragslisten/documents_common/documentsDownload/burhan_logo_excel.png';

				$excelTemplatePath = "documents_common/documentsDownload/Kundenbetreuung_template.xls";

				#$excelFileExcelVersion = 'Excel5';
				#$excelFileExcelVersion = 'Excel2007';
				$excelFileExcelVersion = PHPExcel_IOFactory::identify($excelTemplatePath);

				$thisDateStartColumnIndex = 65; // A:65
				$thisDateStartRowIndex = '10';

				$objPHPExcel = new PHPExcel();

				$objReader = PHPExcel_IOFactory::createReader($excelFileExcelVersion);
				$objReader->setIncludeCharts(true);
				$objPHPExcel = $objReader->load($excelTemplatePath);

				$arrSheets = $objPHPExcel->getSheetNames();

				if(!empty($arrSheets)){
					foreach($arrSheets as $thisSheetKey => $thisSheetValue){
						$thisWorksheet = $objPHPExcel->setActiveSheetIndex($thisSheetKey);
					}
					foreach($arrReplaceFields as $thisReplaceFieldKey => $thisReplaceFieldValue){
						$thisReplaceFieldValue = $thisReplaceFieldValue;
						$thisReplaceFieldValue = preg_replace("/{###DATE_FROM###}/", $thisSalesman, $thisReplaceFieldValue);
						$thisReplaceFieldValue = preg_replace("/{###DATE_TO###}/", date('d.m.Y'), $thisReplaceFieldValue);
						$thisReplaceFieldValue = preg_replace("/{###SALESMAN_NAME###}/", $arrSalesmenWithActiveAreasData[$_REQUEST["searchSalesman"]]["salesmenFirmenname"], $thisReplaceFieldValue);
						$thisReplaceFieldValue = preg_replace("/{###SALESMAN_KNR###}/", $arrSalesmenWithActiveAreasData[$_REQUEST["searchSalesman"]]["salesmenKundennummer"], $thisReplaceFieldValue);
						$thisReplaceFieldValue = preg_replace("/{###ZIPCODE_AREAS###}/", preg_replace("/;/", " • ", $arrSalesmenWithActiveAreasData[$_REQUEST["searchSalesman"]]["salesmenAreas"]), $thisReplaceFieldValue);
						$thisReplaceFieldValue = preg_replace("/{###YEAR###}/", $_POST["searchYear"], $thisReplaceFieldValue);

						$thisReplaceFieldValue = preg_replace("/{###COMPANY_NAME###}/", EXPORT_COMPANY_NAME, $thisReplaceFieldValue);
						$thisReplaceFieldValue = preg_replace("/{###PERSON_NAME###}/", EXPORT_PERSON_NAME, $thisReplaceFieldValue);
						$thisReplaceFieldValue = preg_replace("/{###PERSON_FUNCTION###}/", EXPORT_PERSON_FUNCTION, $thisReplaceFieldValue);

						$thisReplaceFieldValue = preg_replace("/{###SEITE###}/", ($thisSheetKey + 1), $thisReplaceFieldValue);

						$thisWorksheet->setCellValue($thisReplaceFieldKey, $thisReplaceFieldValue);
						clearstatcache();
					}
				}
			}
			else if($_POST["exportType"] == 'csv'){
				$lineSeparator = "\r\n";
				$fieldSeparator = ";";

				$fileNameCreate .= ".csv";
				if(file_exists($thisStoreDir . $fileNameCreate)){
					unlink($thisStoreDir . $fileNameCreate);
				}
				$fp_export = fopen($thisStoreDir . $fileNameCreate, 'w');

				if($fp_export){
					$csvResult = true;
					$successMessage .= ' Die CSV-Datei &quot; ' . $fileNameCreate . ' &quot; wurde erstellt.' .'<br />';
				}
				else {
					$csvResult = false;
					$errorMessage .= ' Die CSV-Datei &quot; ' . $fileNameCreate . ' &quot; konnte nicht erstellt werden.' .'<br />';
				}

				foreach($arrReplaceFields as $thisReplaceFieldKey => $thisReplaceFieldValue){
					$thisReplaceFieldValue = $thisReplaceFieldValue;
					$thisReplaceFieldValue = preg_replace("/{###DATE_FROM###}/", $thisSalesman, $thisReplaceFieldValue);
					$thisReplaceFieldValue = preg_replace("/{###DATE_TO###}/", date('d.m.Y'), $thisReplaceFieldValue);
					$thisReplaceFieldValue = preg_replace("/{###SALESMAN_NAME###}/", $arrSalesmenWithActiveAreasData[$_REQUEST["searchSalesman"]]["salesmenFirmenname"], $thisReplaceFieldValue);
					$thisReplaceFieldValue = preg_replace("/{###SALESMAN_KNR###}/", $arrSalesmenWithActiveAreasData[$_REQUEST["searchSalesman"]]["salesmenKundennummer"], $thisReplaceFieldValue);
					$thisReplaceFieldValue = preg_replace("/{###ZIPCODE_AREAS###}/", preg_replace("/;/", " • ", $arrSalesmenWithActiveAreasData[$_REQUEST["searchSalesman"]]["salesmenAreas"]), $thisReplaceFieldValue);
					$thisReplaceFieldValue = preg_replace("/{###YEAR###}/", $_POST["searchYear"], $thisReplaceFieldValue);

					$thisReplaceFieldValue = preg_replace("/{###COMPANY_NAME###}/", EXPORT_COMPANY_NAME, $thisReplaceFieldValue);
					$thisReplaceFieldValue = preg_replace("/{###PERSON_NAME###}/", EXPORT_PERSON_NAME, $thisReplaceFieldValue);
					$thisReplaceFieldValue = preg_replace("/{###PERSON_FUNCTION###}/", EXPORT_PERSON_FUNCTION, $thisReplaceFieldValue);

					$thisReplaceFieldValue = preg_replace("/{###SEITE###}/", ($thisSheetKey + 1), $thisReplaceFieldValue);

					fwrite($fp_export, $thisReplaceFieldValue . $lineSeparator);
					clearstatcache();
				}
				fwrite($fp_export,
					"Datum" . $fieldSeparator .
					"Aufträge gesamt" . $fieldSeparator .
					"Vertreter-Aufträge" . $fieldSeparator .
					"Auftragsquote" . $fieldSeparator .
					"Neukunden gesamt" . $fieldSeparator .
					"Vetreter-Neukunden" . $fieldSeparator .
					"Neukundenquote" . $fieldSeparator .
					"nicht besuchte Kunden" . $lineSeparator
				);


			}
		}
	// EOF SET EXPORT MODUS

	$defaultORDER = "orderDocumentsDocumentDate";
	$defaultSORT = "ASC";

	if($_POST["searchInterval"] == ""){
		$_POST["searchInterval"] = "DAY";
	}
	if($_POST["searchDisplay"] == ""){
		$_POST["searchDisplay"] = "AREA_TOTAL";
	}

	$todayYear = date("Y");
	if($_POST["searchYear"] == ""){
		$_POST["searchYear"] = $todayYear;
	}
?>
<?php
	require_once('inc/headerHTML.inc.php');
	$thisTitle = "Kunden-Betreuung";

	if($_POST["searchSalesman"] == 'SALESMEN_ALL'){
		$thisTitle .= ': <span class="headerSelectedEntry">ALLE VERTRETER</span>';
	}
	else if($_POST["searchSalesman"] == 'PLZ_ALL' && $_POST["searchDisplay"] == 'AREA_TOTAL'){
		$thisTitle .= ': <span class="headerSelectedEntry">' . 'Gesamt' . '</span>';
	}
	else if($_REQUEST["searchSalesman"] != ''){
		$thisTitle .= ': <span class="headerSelectedEntry">' . $arrSalesmenWithActiveAreasData[$_REQUEST["searchSalesman"]]["customersKundennummer"] . ' &bull; ' . $arrSalesmenWithActiveAreasData[$_REQUEST["searchSalesman"]]["customersFirmenname"] . '</span>';
	}

	$headerHTML = preg_replace("/{###TITLE###}/", strip_tags($thisTitle), $headerHTML);
	echo $headerHTML;

	require_once(FILE_MENUE_TOP);

	$sqlSortField = "";
	$sqlSortDirection = "";
	$sqlWhere = "";
?>

		<div id="menueSidebarToggleArea">
			<img src="layout/icons/iconToggle.png" class="buttonToggleSidebarMenue" width="10" height="10" alt="" title="Seitennavigation ein-/ausblenden" />
			<div id="menueSidebarToggleContent">
			<?php require_once(FILE_MENUE_SIDEBAR); ?>
			<div class="clear"></div>
			</div>
		</div>
		<div id="contentArea2">
			<a name="top"></a>
			<div id="contentAreaElements">
				<h1><?php if(SHOW_PAGE_TITLE_ICONS) { echo '<img src="' . PATH_ICONS_MENUE_TITLES . 'acquisition.png" alt="" />'; } ?> <?php echo $thisTitle; ?></h1>
				<?php
					echo '<div align="right"><a href="' . PATH_ONLINE_ACQUISITION . '" target="_blank">Zur Online-Kundenerfassung</a></div>';
				?>
				<p class="infoArea">
					<?php
						$thisInfo = '';
						$thisInfo .= 'Ausgewertet werden nur ';
						if(preg_match("/^" . MANDATOR . "_/", TABLE_ORDER_CONFIRMATIONS)){
							$thisInfo .= strtoupper(MANDATOR) . '-';
						}
						if(preg_match("/_orderConfirmations$/", TABLE_ORDER_CONFIRMATIONS)){
							$thisInfo .= 'Auftragsbest&auml;tigungen';
						}
						else if(preg_match("/_orderInvoices$/", TABLE_ORDER_CONFIRMATIONS)){
							$thisInfo .= 'Rechnungen';
						}
						$thisInfo .= '!';

						if($_REQUEST["searchInterval"] == "DAY"){
							$thisInfo .= '';
						}
						else if($_REQUEST["searchInterval"] == "WEEK"){
							$thisInfo .= '<br />';
							$thisInfo .= 'Stichtag ist Sonntag als letzter tag der Woche.';
						}
						else if($_REQUEST["searchInterval"] == "MONTH"){
							$thisInfo .= '<br />';
							$thisInfo .= 'Stichtag ist der letzte Tag des Monats.';
						}
						else if($_REQUEST["searchInterval"] == "QUARTER"){
							$thisInfo .= '<br />';
							$thisInfo .= 'Stichtag ist der letzte Tag des Quartals.';
						}
						else if($_REQUEST["searchInterval"] == "YEAR"){
							$thisInfo .= '<br />';
							$thisInfo .= 'Stichtag ist der letzte Tag des Jahres.';
						}

						echo $thisInfo;
					?>
				</p>
				<div id="searchFilterArea">
					<form name="formSearchDatas" method="post" action="<?php echo $_SERVER["PHP_SELF"]; ?>">
					<table border="0" cellpadding="0" cellspacing="0" class="searchFilterContent">
						<tr>
							<td>
								<label for="searchInterval">Zeitraum:</label>
								<select name="searchInterval" id="searchInterval" class="inputSelect_130">
									<option value="DAY" <?php if($_POST["searchInterval"] == 'DAY'){ echo ' selected="selected" '; } ?> >pro Tag</option>
									<option value="WEEK" <?php if($_POST["searchInterval"] == 'WEEK'){ echo ' selected="selected" '; } ?> >pro Woche</option>
									<option value="MONTH" <?php if($_POST["searchInterval"] == 'MONTH'){ echo ' selected="selected" '; } ?> >pro Monat</option>
									<option value="QUARTER" <?php if($_POST["searchInterval"] == 'QUARTER'){ echo ' selected="selected" '; } ?> >pro Vierteljahr</option>
									<option value="YEAR" <?php if($_POST["searchInterval"] == 'YEAR'){ echo ' selected="selected" '; } ?> >pro Jahr</option>
								 </select>
							</td>

							<td>
								<label for="searchYear">Jahr:</label>
								<select name="searchYear" id="searchYear" class="inputSelect_130">
									<!-- <option value="ALL" <?php if($_POST["searchYear"] == "ALL"){ echo ' selected="selected" '; } ?>>Alle Jahre</option> -->
									<?php
										for($i = $todayYear ; $i > (SOFTWARE_START_YEAR - 1) ; $i--){
											$selected = '';
											if($i == $_POST["searchYear"]){
												$selected = ' selected="selected" ';
											}
											echo '<option value="' . $i . '" ' . $selected . ' >' . $i . '</option>';
										}
									?>
								</select>
							</td>

							<td>
								<label for="searchSalesman">Vertreter mit PLZ:</label>
								<select name="searchSalesman" id="searchSalesman" class="inputSelect_510">
									<option value=""> -- Bitte w&auml;hlen -- </option>
									<option class="row3" style="font-weight:bold;" value="">Vertreter-Gebiete</option>
									<option value="SALESMEN_ALL" <?php if($_POST["searchSalesman"] == 'SALESMEN_ALL'){ echo ' selected="selected" '; } ?> style="border-top: 1px solid #000;">Alle Vertreter</option>
									<?php if($_COOKIE["isAdmin"] == '1'){ ?>
									<option value="PLZ_ALL" <?php if($_POST["searchSalesman"] == 'PLZ_ALL'){ echo ' selected="selected" '; } ?>> Alle Gebiete </option>
									<?php } ?>
									<?php
										$thisMarker = '';
										$arrFound = array();
										$thisStyle = '';
										if(!empty($arrSalesmenWithActiveAreasData)){
											foreach($arrSalesmenWithActiveAreasData as $thisSalesmanKey => $thisSalesmanValue){
												$thisStyle = '';
												if(preg_match_all("/^(Unbenutzt|Testgebiet|PLZ)/", $thisSalesmanValue["salesmenFirmenname"], $found)){
													if($thisMarker != $found[0][0]){
														$thisMarker = $found[0][0];
														$thisOptionHeader = '';
														if($thisMarker == 'Unbenutzt'){
															$thisOptionHeader = "Nicht vergebene PLZ-Gebiete";
														}
														else if($thisMarker == 'Testgebiet'){
															$thisOptionHeader = "Test-Gebiete";
														}
														else if($thisMarker == 'PLZ'){
															$thisOptionHeader = "Alle PLZ-Gebiete";
														}
														echo '<option class="row3" style="font-weight:bold;" value="">' . $thisOptionHeader . '</option>';
														$thisStyle = 'border-top: 1px solid #000;';
													}
												}
												$selected = '';
												if($_REQUEST["searchSalesman"] == $thisSalesmanKey){
													$selected = 'selected="selected" ';
												}
												echo '<option value="' . $thisSalesmanKey . '"' . $selected . 'style="' . $thisStyle . '">';

												if($thisMarker == 'Unbenutzt'){
													echo $thisSalesmanValue["customersFirmenname"] . '';
												}
												else if($thisMarker == 'Testgebiet'){
													echo $thisSalesmanValue["salesmenFirmenname"] . ' - PLZ: ' . preg_replace("/;/", ", ", $thisSalesmanValue["salesmenAreas"]);
												}
												else if($thisMarker == 'PLZ'){
													echo $thisSalesmanValue["salesmenFirmenname"] . '';
												}
												else {
													echo $thisSalesmanValue["salesmenFirmenname"] . ' - PLZ: ' . preg_replace("/;/", ", ", $thisSalesmanValue["salesmenAreas"]);
												}

												echo '</option>';
											}
										}
									?>
								</select>
							</td>
							<td>
								<label for="searchDisplay">Ansicht:</label>
								<select name="searchDisplay" id="searchDisplay" class="inputSelect_200">
									<?php if($_POST["searchSalesman"] != ''){ ?>
										<?php
											#if($_POST["searchSalesman"] != 'PLZ_ALL'){
											if(1){
										?>
										<option value="AREA_TOTAL" <?php if($_POST["searchDisplay"] == 'AREA_TOTAL'){ echo ' selected="selected" '; } ?> >Gesamt (alle Gebiete)</option>
										<option value="AREA_SALESMAN" <?php if($_POST["searchDisplay"] == 'AREA_SALESMAN'){ echo ' selected="selected" '; } ?> >nach einzelnen Gebieten</option>
										<!--
										<option value="PLZ_3" <?php if($_POST["searchDisplay"] == 'PLZ_3'){ echo ' selected="selected" '; } ?> >nach 3-stelliger PLZ</option>
										-->
										<?php } else  if($_POST["searchSalesman"] == 'PLZ_ALL') { ?>
										<!--
										<option value="PLZ_TOTAL" <?php if($_POST["searchDisplay"] == 'PLZ_TOTAL'){ echo ' selected="selected" '; } ?> >Gesamt</option>
										<option value="PLZ_2" <?php if($_POST["searchDisplay"] == 'PLZ_2'){ echo ' selected="selected" '; } ?> >nach 2-stelliger PLZ</option>
										<option value="PLZ_3" <?php if($_POST["searchDisplay"] == 'PLZ_3'){ echo ' selected="selected" '; } ?> >nach 3-stelliger PLZ</option>
										-->
										<?php } ?>
									<?php } ?>
								</select>
							</td>

							<?php if(
								(int)$_POST["searchSalesman"] > 0 &&
								in_array($_POST["searchInterval"], array("MONTH", "WEEK")) &&
								$_POST["searchDisplay"] == "AREA_TOTAL"
							) { ?>
								<td class="legendSeperator"> </td>

								<td>
									<label for="exportExecute">Exportieren:</label>
									<input type="checkbox" id="exportExecute" name="exportExecute" class="inputButton0" value="1" />
								</td>

								<td>
									<label for="exportType">Format:</label>
									<select name="exportType" id="exportType" class="inputSelect_100">
										<option value="xls">Excel</option>
										<?php if($_COOKIE["isAdmin"] == '1'){ ?>
										<option value="csv">CSV</option>
										<?php } ?>
									</select>
								</td>
							<?php } ?>

							<td>
								<input type="hidden" name="editID" id="editID" value="" />
								<input type="submit" name="submitSearch" class="inputButton0" value="Anzeigen" />
							</td>

						</tr>
					</table>
					<input type="hidden" name="submitSearchForm" id="submitSearchForm" value="1" />
					</form>
				</div>

				<div class="contentDisplay">

					<?php
						$arrCustomerActivityStatus = array();
						if($_REQUEST["searchSalesman"] != ""){

							// BOF DEFINE SALESMEN SEARCH LOOP
								$arrSearchSalesmenIDs = array();
								if($_POST["searchSalesman"] == 'SALESMEN_ALL' || $_POST["searchSalesman"] == 'PLZ_ALL'){
									$arrSearchSalesmenIDs = array_keys($arrSalesmenWithActiveAreasData);
								}
								else {
									$arrSearchSalesmenIDs = array($_REQUEST["searchSalesman"]);
								}
							// EOF DEFINE SALESMEN SEARCH LOOP

							if(!empty($arrSearchSalesmenIDs)){
								foreach($arrSearchSalesmenIDs as $thisSalesmanID){
									// BOF CONSTRUCT WHERE CLAUSE DEPENDING ON SELECTED DATA
										$arrWhere = array();
										$dateField = "";
										$where = "";
										$whereZipCodeDouble = "";
										$whereZipCodeTriple = "";
										$order = "";
										$group = "";
										$areaField = "";

										$where .= "
											AND `" . TABLE_CUSTOMERS_ACTIVITY_STATUS . "`.`customerActivityStatusDate` > DATE_SUB('" . SALEMESMEN_AREA_CONTRACT_START_DATE . "', INTERVAL 2 DAY)
										";

										if($_POST["searchInterval"] == "DAY"){
											$dateField = "
												`" . TABLE_CUSTOMERS_ACTIVITY_STATUS . "`.`customerActivityStatusDate` AS `interval`
											";

											$where .= "
												AND DATE_FORMAT(`" . TABLE_CUSTOMERS_ACTIVITY_STATUS . "`.`customerActivityStatusDate`, '%Y') <= '" . $_POST["searchYear"] . "' ";
										}
										if($_POST["searchInterval"] == "WEEK"){
											$dateField = "
												`" . TABLE_CUSTOMERS_ACTIVITY_STATUS . "`.`customerActivityStatusDate` AS `interval`
											";

											// BOF GET SUNDAY OF EVERY WEEK
												#$where .= " AND DAYOFWEEK(`" . TABLE_CUSTOMERS_ACTIVITY_STATUS . "`.`customerActivityStatusDate`) = 1 ";

												$where .= " AND (";
												$where .= " DAYOFWEEK(`" . TABLE_CUSTOMERS_ACTIVITY_STATUS . "`.`customerActivityStatusDate`) = 1 ";

												if($todayDayNumber != 0){
													$where .= " OR DATE_FORMAT(`" . TABLE_CUSTOMERS_ACTIVITY_STATUS . "`.`customerActivityStatusDate`, '%Y-%m-%d') = '" . date("Y-m-d", mktime(0, 0, 0, date("m"), (date("d") - 1), date("Y"))). "' ";
												}

												$where .= " )";
											// EOF GET SUNDAY OF EVERY WEEK

											$where .= "
												AND DATE_FORMAT(`" . TABLE_CUSTOMERS_ACTIVITY_STATUS . "`.`customerActivityStatusDate`, '%Y') <= '" . $_POST["searchYear"] . "'
											";
										}
										else if($_POST["searchInterval"] == "MONTH"){
											$dateField = "
												`" . TABLE_CUSTOMERS_ACTIVITY_STATUS . "`.`customerActivityStatusDate` AS `interval`
											";

											// BOF GET LAST DAY OF EVERY MONTH
												$where .= " AND ( ";
												for($y = SOFTWARE_START_YEAR ; $y <= $_POST["searchYear"] ; $y++){
													if($y == date("Y")){
														$useMonths = date("m");
													}
													else {
														$useMonths = 12;
													}
													for($i = 1 ; $i < ($useMonths + 1) ; $i++){
														$thisMonth = $i;
														if($thisMonth < 10){
															$thisMonth = "0" . $thisMonth;
														}
														$thisLastDayOfMonth = date("t", mktime(0, 0, 0, $thisMonth, 1, $y));
														if($thisMonth == date("m") && $y == date("Y")){
															$lastDay = (date("d") - 1);
															if($lastDay > 0){
																$thisLastDayOfMonth = $lastDay;
															}
														}

														if(($y == SOFTWARE_START_YEAR && $i > 1) || $y > SOFTWARE_START_YEAR) {
															$where .= " OR ";
														}
														$where .= " `" . TABLE_CUSTOMERS_ACTIVITY_STATUS . "`.`customerActivityStatusDate` = '" . $y . "-" . $thisMonth . "-" . $thisLastDayOfMonth . "' ";
													}
												}
												$where .= " ) ";
											// EOF GET LAST DAY OF EVERY MONTH

											$where .= "
												AND DATE_FORMAT(`" . TABLE_CUSTOMERS_ACTIVITY_STATUS . "`.`customerActivityStatusDate`, '%Y') <= '" . $_POST["searchYear"] . "'
											";
										}
										else if($_POST["searchInterval"] == "QUARTER"){
											$dateField = "
												`" . TABLE_CUSTOMERS_ACTIVITY_STATUS . "`.`customerActivityStatusDate` AS `interval`
											";

											// BOF GET LAST DAY OF EVERY QUARTER
												$where .= " AND ( ";
												for($y = SOFTWARE_START_YEAR ; $y <= $_POST["searchYear"] ; $y++){
													for($i = 3 ; $i < 13 ; $i = $i + 3){
														$thisMonth = $i;
														if($thisMonth < 10){
															$thisMonth = "0" . $thisMonth;
														}
														$thisLastDayOfMonth = date("t", mktime(0, 0, 0, $thisMonth, 1, $y));
														if(($y == SOFTWARE_START_YEAR && $i > 3) || $y > SOFTWARE_START_YEAR) {
															$where .= " OR ";
														}
														$where .= " `" . TABLE_CUSTOMERS_ACTIVITY_STATUS . "`.`customerActivityStatusDate` = '" . $y . "-" . $thisMonth . "-" . $thisLastDayOfMonth . "' ";
													}
												}
												$where .= " ) ";
											// EOF GET LAST DAY OF EVERY QUARTER

											$where .= "
												AND DATE_FORMAT(`" . TABLE_CUSTOMERS_ACTIVITY_STATUS . "`.`customerActivityStatusDate`, '%Y') <= '" . $_POST["searchYear"] . "'
											";
										}
										else if($_POST["searchInterval"] == "YEAR"){
											$dateField = "
												`" . TABLE_CUSTOMERS_ACTIVITY_STATUS . "`.`customerActivityStatusDate` AS `interval`
											";

											// BOF GET LAST DAY OF EVERY YEAR
												$where .= " AND ( ";
												for($y = SOFTWARE_START_YEAR ; $y <= $_POST["searchYear"] ; $y++){
													$thisLastDayOfMonth = date("t", mktime(0, 0, 0, 12, 1, $y));
													if($y > SOFTWARE_START_YEAR) {
														$where .= " OR ";
													}
													$where .= " `" . TABLE_CUSTOMERS_ACTIVITY_STATUS . "`.`customerActivityStatusDate` = '" . $y . "-12-" . $thisLastDayOfMonth . "' ";
												}

												if($todayDate != date("Y") . '12-31'){
													#$where .= " OR DATE_FORMAT(`" . TABLE_CUSTOMERS_ACTIVITY_STATUS . "`.`customerActivityStatusDate`, '%Y-%m-%d') = '" . date("Y-m-d", mktime(0, 0, 0, date("m"), (date("d") - 1), date("Y"))). "' ";
												}

												$where .= " ) ";
											// EOF GET LAST DAY OF EVERY YEAR
										}

										$arrWhereZipCodeDouble = array();
										$arrWhereZipCodeTriple = array();

										$searchAreas = $arrSalesmenWithActiveAreasData[$thisSalesmanID]["salesmenAreas"];

										$arrSearchZipCodes = explode(";", $searchAreas);
										foreach($arrSearchZipCodes as $thisSearchZipCode){
											if(strlen($thisSearchZipCode) == 2){
												$arrWhereZipCodeDouble[] = " `customerActivityStatusZipCodeDouble` = '" . $thisSearchZipCode . "'";
											}
											else if(strlen($thisSearchZipCode) == 3){
												$arrWhereZipCodeTriple[] = " `customerActivityStatusZipCodeTriple` = '" . $thisSearchZipCode . "'";
											}
										}
										if(!empty($arrWhereZipCodeDouble) && !empty($arrWhereZipCodeTriple)){
											$arrWhere = array_merge($arrWhereZipCodeDouble, $arrWhereZipCodeTriple);
										}
										else {
											if(!empty($arrWhereZipCodeDouble)){
												$arrWhere = $arrWhereZipCodeDouble;
											}
											else if(!empty($arrWhereZipCodeTriple)){
												$arrWhere = $arrWhereZipCodeTriple;
											}
										}
									// EOF CONSTRUCT WHERE CLAUSE DEPENDING ON SELECTED DATA

									// BOF GET DATA
										$arrIntervals = array();
										$arrDatas = array();

										$sqlCustomerCareStatus_TEMPLATE = "
													SELECT
														{###DATE_FIELD###},

														`customerActivityStatusDate`,
														`customerActivityStatusZipCodeDouble`,
														`customerActivityStatusZipCodeTriple`,
														{###AREA_FIELD###}

														`customerActivityStatusType` AS `statusType`,
														SUM(`customerActivityStatusCount`) AS `statusCount`

														FROM `" . TABLE_CUSTOMERS_ACTIVITY_STATUS . "`
														WHERE 1
															{###WHERE###}

														{###GROUP###}

														 {###ORDER###}
											";


										if($_POST["searchDisplay"] == 'AREA_TOTAL'){
											$sqlCustomerCareStatusTemp = '';

											$group = " GROUP BY CONCAT(`interval`, `customerActivityStatusType`) ";
											$order = " ORDER BY `interval` DESC, `statusType` DESC";

											$areaField = '';

											if(!empty($arrWhere)){
												$where .= "
													AND ( " . implode(" OR ", $arrWhere). " )
												";
											}
											$sqlCustomerCareStatusTemp = $sqlCustomerCareStatus_TEMPLATE;

											$sqlCustomerCareStatusTemp = preg_replace("/{###DATE_FIELD###}/", $dateField, $sqlCustomerCareStatusTemp);
											$sqlCustomerCareStatusTemp = preg_replace("/{###WHERE###}/", $where, $sqlCustomerCareStatusTemp);
											$sqlCustomerCareStatusTemp = preg_replace("/{###GROUP###}/", $group, $sqlCustomerCareStatusTemp);
											$sqlCustomerCareStatusTemp = preg_replace("/{###ORDER###}/", $order, $sqlCustomerCareStatusTemp);
											$sqlCustomerCareStatusTemp = preg_replace("/{###AREA_FIELD###}/", $areaField, $sqlCustomerCareStatusTemp);
										}
										else if($_POST["searchDisplay"] == 'PLZ_2'){
											$sqlCustomerCareStatusTemp = '';

											$group = " GROUP BY CONCAT(`interval`, `customerActivityStatusZipCodeDouble`, `customerActivityStatusType`) ";
											$order = " ORDER BY `customerActivityStatusZipCodeDouble`, `interval` DESC, `statusType` DESC";
											$areaField = '';

											if(!empty($arrWhere)){
												$where .= "
													AND ( " . implode(" OR ", $arrWhere). " )
												";
											}
											$sqlCustomerCareStatusTemp = $sqlCustomerCareStatus_TEMPLATE;

											$sqlCustomerCareStatusTemp = preg_replace("/{###DATE_FIELD###}/", $dateField, $sqlCustomerCareStatusTemp);
											$sqlCustomerCareStatusTemp = preg_replace("/{###WHERE###}/", $where, $sqlCustomerCareStatusTemp);
											$sqlCustomerCareStatusTemp = preg_replace("/{###GROUP###}/", $group, $sqlCustomerCareStatusTemp);
											$sqlCustomerCareStatusTemp = preg_replace("/{###ORDER###}/", $order, $sqlCustomerCareStatusTemp);
											$sqlCustomerCareStatusTemp = preg_replace("/{###AREA_FIELD###}/", $areaField, $sqlCustomerCareStatusTemp);
										}
										else if($_POST["searchDisplay"] == 'PLZ_3'){
											$sqlCustomerCareStatusTemp = '';

											$group = " GROUP BY CONCAT(`interval`, `customerActivityStatusZipCodeTriple`, `customerActivityStatusType`) ";
											$order = " ORDER BY `customerActivityStatusZipCodeTriple`, `interval` DESC, `statusType` DESC";
											$areaField = '';

											if(!empty($arrWhere)){
												$where .= "
													AND ( " . implode(" OR ", $arrWhere). " )
												";
											}
											$sqlCustomerCareStatusTemp = $sqlCustomerCareStatus_TEMPLATE;

											$sqlCustomerCareStatusTemp = preg_replace("/{###DATE_FIELD###}/", $dateField, $sqlCustomerCareStatusTemp);
											$sqlCustomerCareStatusTemp = preg_replace("/{###WHERE###}/", $where, $sqlCustomerCareStatusTemp);
											$sqlCustomerCareStatusTemp = preg_replace("/{###GROUP###}/", $group, $sqlCustomerCareStatusTemp);
											$sqlCustomerCareStatusTemp = preg_replace("/{###ORDER###}/", $order, $sqlCustomerCareStatusTemp);
											$sqlCustomerCareStatusTemp = preg_replace("/{###AREA_FIELD###}/", $areaField, $sqlCustomerCareStatusTemp);
										}
										else if($_POST["searchDisplay"] == 'AREA_SALESMAN' || $_POST["searchSalesman"] == 'SALESMEN_ALL'){
											$sqlCustomerCareStatusTemp = '';
											$sqlCustomerCareStatusTempDouble = '';
											$sqlCustomerCareStatusTempTriple = '';


											$order = " ORDER BY `customerActivityStatusArea`, `interval` DESC, `statusType` DESC";

											if(!empty($arrWhereZipCodeDouble)){
												$group = " GROUP BY CONCAT(`interval`, `customerActivityStatusZipCodeDouble`, `customerActivityStatusType`) ";
												$areaField = " `customerActivityStatusZipCodeDouble` AS `customerActivityStatusArea`, ";
												$whereZipCodeDouble = $where . "
													AND ( " . implode(" OR ", $arrWhereZipCodeDouble). " )
												";

												$sqlCustomerCareStatusTempDouble = $sqlCustomerCareStatus_TEMPLATE;
												$sqlCustomerCareStatusTempDouble = preg_replace("/{###WHERE###}/", $whereZipCodeDouble, $sqlCustomerCareStatusTempDouble);
												$sqlCustomerCareStatusTempDouble = preg_replace("/{###GROUP###}/", $group, $sqlCustomerCareStatusTempDouble);
												$sqlCustomerCareStatusTempDouble = preg_replace("/{###ORDER###}/", "", $sqlCustomerCareStatusTempDouble);
												$sqlCustomerCareStatusTempDouble = preg_replace("/{###AREA_FIELD###}/", $areaField, $sqlCustomerCareStatusTempDouble);
											}
											if(!empty($arrWhereZipCodeTriple)){
												$group = " GROUP BY CONCAT(`interval`, `customerActivityStatusZipCodeTriple`, `customerActivityStatusType`) ";
												$areaField = " `customerActivityStatusZipCodeTriple` AS `customerActivityStatusArea`, ";
												$whereZipCodeTriple = $where . "
													AND ( " . implode(" OR ", $arrWhereZipCodeTriple). " )
												";

												$sqlCustomerCareStatusTempTriple = $sqlCustomerCareStatus_TEMPLATE;
												$sqlCustomerCareStatusTempTriple = preg_replace("/{###WHERE###}/", $whereZipCodeTriple, $sqlCustomerCareStatusTempTriple);
												$sqlCustomerCareStatusTempTriple = preg_replace("/{###GROUP###}/", $group, $sqlCustomerCareStatusTempTriple);
												$sqlCustomerCareStatusTempTriple = preg_replace("/{###ORDER###}/", "", $sqlCustomerCareStatusTempTriple);
												$sqlCustomerCareStatusTempTriple = preg_replace("/{###AREA_FIELD###}/", $areaField, $sqlCustomerCareStatusTempTriple);
											}
											if($sqlCustomerCareStatusTempDouble != '' && $sqlCustomerCareStatusTempTriple != ''){
												$sqlCustomerCareStatusTemp = $sqlCustomerCareStatusTempDouble . " UNION " . $sqlCustomerCareStatusTempTriple;
											}
											else {
												if($sqlCustomerCareStatusTempDouble != ''){
													$sqlCustomerCareStatusTemp = $sqlCustomerCareStatusTempDouble;
												}
												else if($sqlCustomerCareStatusTempTriple != ''){
													$sqlCustomerCareStatusTemp = $sqlCustomerCareStatusTempTriple;
												}
											}
											$sqlCustomerCareStatusTemp = "
														SELECT
														*
														FROM (
															" . $sqlCustomerCareStatusTemp . "
														) AS `tempTable`
														{###ORDER###}
												";
											$sqlCustomerCareStatusTemp = preg_replace("/{###DATE_FIELD###}/", $dateField, $sqlCustomerCareStatusTemp);
											$sqlCustomerCareStatusTemp = preg_replace("/{###ORDER###}/", $order, $sqlCustomerCareStatusTemp);
										}

										$sqlCustomerCareStatus = $sqlCustomerCareStatusTemp;

										$rsCustomerCareStatus = $dbConnection->db_query($sqlCustomerCareStatus);
#dd('sqlCustomerCareStatus');
										$countItem = 0;

										while($ds = mysqli_fetch_assoc($rsCustomerCareStatus)) {


											foreach(array_keys($ds) as $field){
												if($_POST["searchDisplay"] == 'AREA_TOTAL'){
													$thisIndex = 'AREA_TOTAL';
													$arrCustomerActivityStatus[$thisSalesmanID][$thisIndex][$ds["interval"]][$ds["statusType"]][$field] = $ds[$field];
												}
												else if($_POST["searchDisplay"] == 'PLZ_2'){
													$thisIndex = $ds["customerActivityStatusZipCodeDouble"];
													$arrCustomerActivityStatus[$thisSalesmanID][$thisIndex][$ds["interval"]][$ds["statusType"]][$field] = $ds[$field];
												}
												else if($_POST["searchDisplay"] == 'PLZ_3'){
													$thisIndex = $ds["customerActivityStatusZipCodeTriple"];
													$arrCustomerActivityStatus[$thisSalesmanID][$thisIndex][$ds["interval"]][$ds["statusType"]][$field] = $ds[$field];
												}
												else if($_POST["searchDisplay"] == 'AREA_SALESMAN'){
													$thisIndex = $ds["customerActivityStatusArea"];
													$arrCustomerActivityStatus[$thisSalesmanID][$thisIndex][$ds["interval"]][$ds["statusType"]][$field] = $ds[$field];
												}
											}
											$arrIntervals[] = $ds["interval"];
										}
										if(!empty($arrIntervals)){
											$arrIntervals = array_unique($arrIntervals);
											$arrIntervals = array_values($arrIntervals);
										}
									// EOF GET DATA
								}

								if($_REQUEST["searchSalesman"] != ""){
									echo '<p class="infoArea">PLZ-Gebiete: ';
									if($_REQUEST["searchSalesman"] != "SALESMEN_ALL" && $_REQUEST["searchSalesman"] != "PLZ_ALL") {
										if($_POST["searchDisplay"] == 'AREA_SALESMAN'){
											$arrTempSalesmenAreas = explode(";", $arrSalesmenWithActiveAreasData[$_REQUEST["searchSalesman"]]["salesmenAreas"]);
											if(!empty($arrTempSalesmenAreas)){
												foreach($arrTempSalesmenAreas as $thisTempSalesmenAreas){
													echo ' &bull; <a href="#PLZ_' . $thisTempSalesmenAreas . '">' . $thisTempSalesmenAreas . '</a>';
												}
											}
										}
										else {
											echo '' . preg_replace("/;/", " &bull; ", $arrSalesmenWithActiveAreasData[$_REQUEST["searchSalesman"]]["salesmenAreas"]);
										}
									}
									else {
										#echo preg_replace("/;/", " &bull; ", $searchAreas);
										echo 'Gebiete aller Vertreter';
									}
									echo '</p>';
								}
							}
						}

						if(!empty($arrCustomerActivityStatus)){

							if((int)$_POST["searchSalesman"] > 0 ){
								// BOF GET ABs DEPENDING ON SALESMEN, ZIPCODE AREA, DATE
									$where_getSalesmenOrders = "";
									$where_getAllOrders = "";
									$dateField = "";
									#$where_getSalesmenOrders .= " AND `" . TABLE_ORDER_CONFIRMATIONS . "`.`orderDocumentsDocumentDate` > DATE_SUB('" . SALEMESMEN_AREA_CONTRACT_START_DATE . "', INTERVAL 2 DAY) ";
									$where_getSalesmenOrders .= " AND `" . TABLE_ORDER_CONFIRMATIONS . "`.`orderDocumentsDocumentDate` >= '" . SALEMESMEN_AREA_CONTRACT_START_DATE . "' ";

									$where_getAllOrders .= " AND `" . TABLE_ORDER_CONFIRMATIONS . "`.`orderDocumentsDocumentDate` >= '" . SALEMESMEN_AREA_CONTRACT_START_DATE . "' ";

									if((int)$_POST["searchSalesman"] > 0 ){
										$thisSalesmanCustomerNumber = $arrSalesmenWithActiveAreasData[$_POST["searchSalesman"]]["salesmenKundennummer"];
										$thisSalesmanCustomerID = $arrSalesmenWithActiveAreasData[$_POST["searchSalesman"]]["customersID"];

										#$where_getSalesmenOrders .= " AND `" . TABLE_ORDER_CONFIRMATIONS . "`.`orderDocumentsAddressCompanyCustomerNumber` = '" . $thisSalesmanCustomerNumber . "' ";
										$where_getSalesmenOrders .= "
												AND (
													`" . TABLE_ORDER_CONFIRMATIONS . "`.`orderDocumentsAddressCompanyCustomerNumber` = '" . $thisSalesmanCustomerNumber . "'
													OR
													`common_salesmen`.`customersKundennummer` = '" . $thisSalesmanCustomerNumber . "'
												)
											";


										$arrSalesmenAreas[$_POST["searchSalesman"]] = explode(";", $arrSalesmenWithActiveAreasData[$_POST["searchSalesman"]]["salesmenAreas"]);
									}
									else {
										$thisSalesmanCustomerNumber = $_POST["searchSalesman"];
									}
									$arrWhereAreas = array();
									if(!empty($arrSalesmenAreas[$_POST["searchSalesman"]])){
										foreach($arrSalesmenAreas[$_POST["searchSalesman"]] as $thisSalesmenArea){
											$arrWhereAreas[] = " `" . TABLE_CUSTOMERS . "`.`customersCompanyPLZ` LIKE '" . $thisSalesmenArea . "%' ";
										}
										$where_getSalesmenOrders .= " AND (" . implode(' OR ', $arrWhereAreas) . ") ";
										$where_getAllOrders .= " AND (" . implode(' OR ', $arrWhereAreas) . ") ";
									}

									if($_POST["searchInterval"] == "DAY"){
										$dateField = "
											`" . TABLE_ORDER_CONFIRMATIONS . "`.`orderDocumentsDocumentDate` AS `interval`
										";
									}
									if($_POST["searchInterval"] == "WEEK"){
										$dateField = "
											CONCAT(
												IF(DATE_FORMAT(`" . TABLE_ORDER_CONFIRMATIONS . "`.`orderDocumentsDocumentDate`, '%m') = '12' AND DATE_FORMAT(`" . TABLE_ORDER_CONFIRMATIONS . "`.`orderDocumentsDocumentDate`, '%v') = '01', (DATE_FORMAT(`" . TABLE_ORDER_CONFIRMATIONS . "`.`orderDocumentsDocumentDate`, '%Y') + 1),  DATE_FORMAT(`" . TABLE_ORDER_CONFIRMATIONS . "`.`orderDocumentsDocumentDate`, '%Y')),
												'#',
												DATE_FORMAT(`" . TABLE_ORDER_CONFIRMATIONS . "`.`orderDocumentsDocumentDate`, '%v')
											) AS `interval`
										";
									}
									else if($_POST["searchInterval"] == "MONTH"){
										$dateField = "
											CONCAT(
												DATE_FORMAT(`" . TABLE_ORDER_CONFIRMATIONS . "`.`orderDocumentsDocumentDate`, '%Y'),
												'#',
												DATE_FORMAT(`" . TABLE_ORDER_CONFIRMATIONS . "`.`orderDocumentsDocumentDate`, '%m')
											) AS `interval`
										";

									}
									else if($_POST["searchInterval"] == "QUARTER"){
										$dateField = "
											CONCAT(
												DATE_FORMAT(`" . TABLE_ORDER_CONFIRMATIONS . "`.`orderDocumentsDocumentDate`, '%Y'),
												'#',
												IF(DATE_FORMAT(`" . TABLE_ORDER_CONFIRMATIONS . "`.`orderDocumentsDocumentDate`, '%m') = '01' OR DATE_FORMAT(`" . TABLE_ORDER_CONFIRMATIONS . "`.`orderDocumentsDocumentDate`, '%m') = '02' OR DATE_FORMAT(`" . TABLE_ORDER_CONFIRMATIONS . "`.`orderDocumentsDocumentDate`, '%m') = '03', '1. Quartal',
													IF(DATE_FORMAT(`" . TABLE_ORDER_CONFIRMATIONS . "`.`orderDocumentsDocumentDate`, '%m') = '04' OR DATE_FORMAT(`" . TABLE_ORDER_CONFIRMATIONS . "`.`orderDocumentsDocumentDate`, '%m') = '05' OR DATE_FORMAT(`" . TABLE_ORDER_CONFIRMATIONS . "`.`orderDocumentsDocumentDate`, '%m') = '06', '2. Quartal',
														IF(DATE_FORMAT(`" . TABLE_ORDER_CONFIRMATIONS . "`.`orderDocumentsDocumentDate`, '%m') = '07' OR DATE_FORMAT(`" . TABLE_ORDER_CONFIRMATIONS . "`.`orderDocumentsDocumentDate`, '%m') = '08' OR DATE_FORMAT(`" . TABLE_ORDER_CONFIRMATIONS . "`.`orderDocumentsDocumentDate`, '%m') = '09', '3. Quartal',
															IF(DATE_FORMAT(`" . TABLE_ORDER_CONFIRMATIONS . "`.`orderDocumentsDocumentDate`, '%m') = '10' OR DATE_FORMAT(`" . TABLE_ORDER_CONFIRMATIONS . "`.`orderDocumentsDocumentDate`, '%m') = '11' OR DATE_FORMAT(`" . TABLE_ORDER_CONFIRMATIONS . "`.`orderDocumentsDocumentDate`, '%m') = '12', '4. Quartal',
																''
															)
														)
													)
												)
											) AS `interval`
										";
									}
									else if($_POST["searchInterval"] == "YEAR"){
										$dateField = "
											CONCAT(
												DATE_FORMAT(`" . TABLE_ORDER_CONFIRMATIONS . "`.`orderDocumentsDocumentDate`, '%Y'),
												'#',
												DATE_FORMAT(`" . TABLE_ORDER_CONFIRMATIONS . "`.`orderDocumentsDocumentDate`, '%Y')
											) AS `interval`
										";
									}


									$sql_getSalesmenOrders = "
											SELECT
												" . $dateField . ",

												`" . TABLE_ORDER_CONFIRMATIONS . "`.`orderDocumentsID`,
												`" . TABLE_ORDER_CONFIRMATIONS . "`.`orderDocumentsNumber`,
												`" . TABLE_ORDER_CONFIRMATIONS . "`.`orderDocumentsOrdersIDs`,
												`" . TABLE_ORDER_CONFIRMATIONS . "`.`orderDocumentsCustomerNumber`,
												`" . TABLE_ORDER_CONFIRMATIONS . "`.`orderDocumentsCustomersOrderNumber`,
												`" . TABLE_ORDER_CONFIRMATIONS . "`.`orderDocumentsProcessingDate`,
												`" . TABLE_ORDER_CONFIRMATIONS . "`.`orderDocumentsDocumentDate`,
												`" . TABLE_ORDER_CONFIRMATIONS . "`.`orderDocumentsSalesman`,
												`" . TABLE_ORDER_CONFIRMATIONS . "`.`orderDocumentsKommission`,
												`" . TABLE_ORDER_CONFIRMATIONS . "`.`orderDocumentsAddressCompanyCustomerNumber`,
												`" . TABLE_ORDER_CONFIRMATIONS . "`.`orderDocumentsSumPrice`,
												`" . TABLE_ORDER_CONFIRMATIONS . "`.`orderDocumentsDiscount`,
												`" . TABLE_ORDER_CONFIRMATIONS . "`.`orderDocumentsTotalPrice`,
												`" . TABLE_ORDER_CONFIRMATIONS . "`.`orderDocumentsBankAccount`,
												`" . TABLE_ORDER_CONFIRMATIONS . "`.`orderDocumentsDocumentPath`,
												`" . TABLE_ORDER_CONFIRMATIONS . "`.`orderDocumentsStatus`,

												`common_customers`.`customersKundennummer`,
												`common_customers`.`customersCompanyPLZ`

												FROM `" . TABLE_ORDER_CONFIRMATIONS . "`

												/*
												LEFT JOIN `" . TABLE_CUSTOMERS . "`
												ON(`" . TABLE_ORDER_CONFIRMATIONS . "`.`orderDocumentsCustomerNumber` = `" . TABLE_CUSTOMERS . "`.`customersKundennummer`)
												*/

												LEFT JOIN `" . TABLE_CUSTOMERS . "` AS `common_customers`
												ON(`" . TABLE_ORDER_CONFIRMATIONS . "`.`orderDocumentsCustomerNumber` = `common_customers`.`customersKundennummer`)

												LEFT JOIN `" . TABLE_CUSTOMERS . "` AS `common_salesmen`
												ON(`common_salesmen`.`customersID` = `bctr_orderconfirmations`.`orderDocumentsSalesman`)

												WHERE 1
												" . $where_getSalesmenOrders . "
													AND `common_customers`.`customersCompanyCountry` = 81

										";

									$rs_getSalesmenOrders = $dbConnection->db_query($sql_getSalesmenOrders);
#dd('sql_getSalesmenOrders');
									$arrSalesmenIntervalAreaOrders = array();
									$arrSalesmenIntervalTotalOrders = array();
									$arrSalesmenOrders = array();
									$countOrder = 0;
									while($ds_getSalesmenOrders = mysqli_fetch_assoc($rs_getSalesmenOrders)){
										$thisZipCode = $ds_getSalesmenOrders["customersCompanyPLZ"];
										$thisIntervalIndex = $ds_getSalesmenOrders["interval"];
#dd('ds_getSalesmenOrders');
#dd('countOrder');
										$arrAreas = array();
										if(preg_match("/^testID_/", $_POST["searchSalesman"])){
											$arrAreas[] = preg_replace("/^testID_/", "", $_POST["searchSalesman"]);
											#$thisSalesmanNumber = $_POST["searchSalesman"];
										}
										else {
											$arrAreas = $arrSalesmenAreas[$_POST["searchSalesman"]];
											#$thisSalesmanNumber = $ds_getSalesmenOrders["orderDocumentsAddressCompanyCustomerNumber"];
										}
										$arrSalesmenOrders[$thisIntervalIndex][] = $ds_getSalesmenOrders["orderDocumentsNumber"];
										if(!empty($arrAreas)){
											$arrCountOrderPositions = explode(";", $ds_getSalesmenOrders["orderDocumentsOrdersIDs"]);
											$thisCountOrderPositions = count($arrCountOrderPositions);

											foreach($arrAreas as $thisSalesmanArea){
												if($arrSalesmenIntervalAreaOrders[$thisSalesmanCustomerNumber][$thisIntervalIndex][$thisSalesmanArea] == ''){
													$arrSalesmenIntervalAreaOrders[$thisSalesmanCustomerNumber][$thisIntervalIndex][$thisSalesmanArea] = 0;
													$arrSalesmenIntervalAreaOrdersPositions[$thisSalesmanCustomerNumber][$thisIntervalIndex][$thisSalesmanArea] = 0;
												}
												if($arrSalesmenIntervalTotalOrders[$thisSalesmanCustomerNumber][$thisIntervalIndex] == ''){
													$arrSalesmenIntervalTotalOrdersPositions[$thisSalesmanCustomerNumber][$thisIntervalIndex] = 0;
												}
												if(preg_match("/^" . $thisSalesmanArea . "/", $ds_getSalesmenOrders["customersCompanyPLZ"])){
													$arrSalesmenIntervalAreaOrders[$thisSalesmanCustomerNumber][$thisIntervalIndex][$thisSalesmanArea]++;
													$arrSalesmenIntervalTotalOrders[$thisSalesmanCustomerNumber][$thisIntervalIndex]++;

													$arrSalesmenIntervalAreaOrdersPositions[$thisSalesmanCustomerNumber][$thisIntervalIndex][$thisSalesmanArea] += $thisCountOrderPositions;
													$arrSalesmenIntervalTotalOrdersPositions[$thisSalesmanCustomerNumber][$thisIntervalIndex] += $thisCountOrderPositions;
												}
											}
										}
										$countOrder++;
									}
#dd('arrSalesmenIntervalTotalOrders');
								// EOF GET ABs DEPENDING ON SALESMEN, ZIPCODE AREA, DATE

								// BOF GET ALL ORDERS ON ZIPCODE AREA, DATE
									$sql_getAllOrders = "
											SELECT
												" . $dateField . ",

												`" . TABLE_ORDER_CONFIRMATIONS . "`.`orderDocumentsID`,
												`" . TABLE_ORDER_CONFIRMATIONS . "`.`orderDocumentsNumber`,
												`" . TABLE_ORDER_CONFIRMATIONS . "`.`orderDocumentsOrdersIDs`,
												`" . TABLE_ORDER_CONFIRMATIONS . "`.`orderDocumentsCustomerNumber`,
												`" . TABLE_ORDER_CONFIRMATIONS . "`.`orderDocumentsCustomersOrderNumber`,
												`" . TABLE_ORDER_CONFIRMATIONS . "`.`orderDocumentsProcessingDate`,
												`" . TABLE_ORDER_CONFIRMATIONS . "`.`orderDocumentsDocumentDate`,
												`" . TABLE_ORDER_CONFIRMATIONS . "`.`orderDocumentsSalesman`,
												`" . TABLE_ORDER_CONFIRMATIONS . "`.`orderDocumentsKommission`,
												`" . TABLE_ORDER_CONFIRMATIONS . "`.`orderDocumentsAddressCompanyCustomerNumber`,
												`" . TABLE_ORDER_CONFIRMATIONS . "`.`orderDocumentsSumPrice`,
												`" . TABLE_ORDER_CONFIRMATIONS . "`.`orderDocumentsDiscount`,
												`" . TABLE_ORDER_CONFIRMATIONS . "`.`orderDocumentsTotalPrice`,
												`" . TABLE_ORDER_CONFIRMATIONS . "`.`orderDocumentsBankAccount`,
												`" . TABLE_ORDER_CONFIRMATIONS . "`.`orderDocumentsDocumentPath`,
												`" . TABLE_ORDER_CONFIRMATIONS . "`.`orderDocumentsStatus`,

												`" . TABLE_CUSTOMERS . "`.`customersKundennummer`,
												`" . TABLE_CUSTOMERS . "`.`customersCompanyPLZ`

												FROM `" . TABLE_ORDER_CONFIRMATIONS . "`
												LEFT JOIN `" . TABLE_CUSTOMERS . "`
												ON(`" . TABLE_ORDER_CONFIRMATIONS . "`.`orderDocumentsCustomerNumber` = `" . TABLE_CUSTOMERS . "`.`customersKundennummer`)

												WHERE 1
												" . $where_getAllOrders . "
													AND `" . TABLE_CUSTOMERS . "`.`customersCompanyCountry` = 81

										";

									$rs_getAllOrders = $dbConnection->db_query($sql_getAllOrders);
									echo mysqli_error();

									$arrAllIntervalAreaOrders = array();
									$arrAllIntervalTotalOrders = array();
									$arrAllOrders = array();

									$arrAllIntervalAreaOrdersPositions = array();
									$arrAllIntervalTotalOrdersPositions = array();
									$arrAllOrdersPositions = array();

									$countOrder = 0;
									while($ds_getAllOrders = mysqli_fetch_assoc($rs_getAllOrders)){
										$thisZipCode = $ds_getAllOrders["customersCompanyPLZ"];
										$thisIntervalIndex = $ds_getAllOrders["interval"];

										if(!in_array($ds_getAllOrders["interval"], $arrIntervals)){
											$arrIntervals[] = $ds_getAllOrders["interval"];
										}

										$arrAreas = array();
										if(preg_match("/^testID_/", $_POST["searchSalesman"])){
											$arrAreas[] = preg_replace("/^testID_/", "", $_POST["searchSalesman"]);
											#$thisSalesmanNumber = $_POST["searchSalesman"];
										}
										else {
											$arrAreas = $arrSalesmenAreas[$_POST["searchSalesman"]];
											#$thisSalesmanNumber = $ds_getAllOrders["orderDocumentsAddressCompanyCustomerNumber"];
										}

										$arrAllOrders[$thisIntervalIndex][] = $ds_getAllOrders["orderDocumentsNumber"];

										if(!empty($arrAreas)){
											$arrCountOrderPositions = explode(";", $ds_getAllOrders["orderDocumentsOrdersIDs"]);
											$thisCountOrderPositions = count($arrCountOrderPositions);

											foreach($arrAreas as $thisSalesmanArea){
												if($arrAllIntervalAreaOrders[$thisSalesmanCustomerNumber][$thisIntervalIndex][$thisSalesmanArea] == ''){
													$arrAllIntervalAreaOrders[$thisSalesmanCustomerNumber][$thisIntervalIndex][$thisSalesmanArea] = 0;
													$arrAllIntervalAreaOrdersPositions[$thisSalesmanCustomerNumber][$thisIntervalIndex][$thisSalesmanArea] = 0;
												}
												if($arrAllIntervalTotalOrders[$thisSalesmanCustomerNumber][$thisIntervalIndex] == ''){
													$arrAllIntervalTotalOrders[$thisSalesmanCustomerNumber][$thisIntervalIndex] = 0;
													$arrAllIntervalTotalOrdersPositions[$thisSalesmanCustomerNumber][$thisIntervalIndex] = 0;
												}
												if(preg_match("/^" . $thisSalesmanArea . "/", $ds_getAllOrders["customersCompanyPLZ"])){
													$arrAllIntervalAreaOrders[$thisSalesmanCustomerNumber][$thisIntervalIndex][$thisSalesmanArea]++;
													$arrAllIntervalTotalOrders[$thisSalesmanCustomerNumber][$thisIntervalIndex]++;

													$arrAllIntervalAreaOrdersPositions[$thisSalesmanCustomerNumber][$thisIntervalIndex][$thisSalesmanArea] += $thisCountOrderPositions;
													$arrAllIntervalTotalOrdersPositions[$thisSalesmanCustomerNumber][$thisIntervalIndex] += $thisCountOrderPositions;
												}
											}
										}
									}
									if(!empty($arrIntervals)){
										$arrIntervals = array_unique($arrIntervals);
										$arrIntervals = array_values($arrIntervals);
									}
								// EOF GET ALL ORDERS ON ZIPCODE AREA, DATE

								// BOF GET NEW CUSTOMERS ON ENTRY DATE
									$where_getSalesmenNewCustomers = "";

									$where_getSalesmenNewCustomers .= "
											AND `" . TABLE_CUSTOMERS . "`.`customersEntryDate` >= '" . SALEMESMEN_AREA_CONTRACT_START_DATE . "'
											AND `" . TABLE_CUSTOMERS . "`.`customersVertreterID` = '" . $thisSalesmanCustomerID . "'
										";

									if((int)$_POST["searchSalesman"] > 0 ){
										#$where_getSalesmenNewCustomers .= " AND `" . TABLE_ORDER_CONFIRMATIONS . "`.`orderDocumentsAddressCompanyCustomerNumber` = '" . $thisSalesmanCustomerNumber . "' ";
									}
									else {

									}

									$arrWhereAreas = array();
									if(!empty($arrSalesmenAreas[$_POST["searchSalesman"]])){
										foreach($arrSalesmenAreas[$_POST["searchSalesman"]] as $thisSalesmenArea){
											$arrWhereAreas[] = " `" . TABLE_CUSTOMERS . "`.`customersCompanyPLZ` LIKE '" . $thisSalesmenArea . "%' ";
										}
										$where_getSalesmenNewCustomers .= " AND (" . implode(' OR ', $arrWhereAreas) . ") ";
									}

									if($_POST["searchInterval"] == "DAY"){
										$dateField = "
											`" . TABLE_CUSTOMERS . "`.`customersEntryDate` AS `interval`
										";
									}
									if($_POST["searchInterval"] == "WEEK"){
										$dateField = "
											CONCAT(
												IF(DATE_FORMAT(`" . TABLE_CUSTOMERS . "`.`customersEntryDate`, '%m') = '12' AND DATE_FORMAT(`" . TABLE_CUSTOMERS . "`.`customersEntryDate`, '%v') = '01', (DATE_FORMAT(`" . TABLE_CUSTOMERS . "`.`customersEntryDate`, '%Y') + 1),  DATE_FORMAT(`" . TABLE_CUSTOMERS . "`.`customersEntryDate`, '%Y')),
												'#',
												DATE_FORMAT(`" . TABLE_CUSTOMERS . "`.`customersEntryDate`, '%v')
											) AS `interval`
										";
									}
									else if($_POST["searchInterval"] == "MONTH"){
										$dateField = "
											CONCAT(
												DATE_FORMAT(`" . TABLE_CUSTOMERS . "`.`customersEntryDate`, '%Y'),
												'#',
												DATE_FORMAT(`" . TABLE_CUSTOMERS . "`.`customersEntryDate`, '%m')
											) AS `interval`
										";

									}
									else if($_POST["searchInterval"] == "QUARTER"){
										$dateField = "
											CONCAT(
												DATE_FORMAT(`" . TABLE_CUSTOMERS . "`.`customersEntryDate`, '%Y'),
												'#',
												IF(DATE_FORMAT(`" . TABLE_CUSTOMERS . "`.`customersEntryDate`, '%m') = '01' OR DATE_FORMAT(`" . TABLE_CUSTOMERS . "`.`customersEntryDate`, '%m') = '02' OR DATE_FORMAT(`" . TABLE_CUSTOMERS . "`.`customersEntryDate`, '%m') = '03', '1. Quartal',
													IF(DATE_FORMAT(`" . TABLE_CUSTOMERS . "`.`customersEntryDate`, '%m') = '04' OR DATE_FORMAT(`" . TABLE_CUSTOMERS . "`.`customersEntryDate`, '%m') = '05' OR DATE_FORMAT(`" . TABLE_CUSTOMERS . "`.`customersEntryDate`, '%m') = '06', '2. Quartal',
														IF(DATE_FORMAT(`" . TABLE_CUSTOMERS . "`.`customersEntryDate`, '%m') = '07' OR DATE_FORMAT(`" . TABLE_CUSTOMERS . "`.`customersEntryDate`, '%m') = '08' OR DATE_FORMAT(`" . TABLE_CUSTOMERS . "`.`customersEntryDate`, '%m') = '09', '3. Quartal',
															IF(DATE_FORMAT(`" . TABLE_CUSTOMERS . "`.`customersEntryDate`, '%m') = '10' OR DATE_FORMAT(`" . TABLE_CUSTOMERS . "`.`customersEntryDate`, '%m') = '11' OR DATE_FORMAT(`" . TABLE_CUSTOMERS . "`.`customersEntryDate`, '%m') = '12', '4. Quartal',
																''
															)
														)
													)
												)
											) AS `interval`
										";
									}
									else if($_POST["searchInterval"] == "YEAR"){
										$dateField = "
											CONCAT(
												DATE_FORMAT(`" . TABLE_CUSTOMERS . "`.`customersEntryDate`, '%Y'),
												'#',
												DATE_FORMAT(`" . TABLE_CUSTOMERS . "`.`customersEntryDate`, '%Y')
											) AS `interval`
										";
									}
									$arrNewCustomers = array();
									$sql_getSalesmenNewCustomers = "
											SELECT
												" . $dateField . ",

												`" . TABLE_CUSTOMERS . "`.`customersCompanyPLZ`,
												`" . TABLE_CUSTOMERS . "`.`customersEntryDate`
											FROM `" . TABLE_CUSTOMERS . "`
												WHERE 1
													" . $where_getSalesmenNewCustomers . "
													AND `" . TABLE_CUSTOMERS . "`.`customersCompanyCountry` = 81
													AND (
														`customersTyp` = 1
														OR
														`customersTyp` = 5
														OR
														`customersTyp` = 6
													)
										";
									$rs_getSalesmenNewCustomers = $dbConnection->db_query($sql_getSalesmenNewCustomers);

									while($ds_getSalesmenNewCustomers = mysqli_fetch_assoc($rs_getSalesmenNewCustomers)){

										$thisZipCode = $ds_getSalesmenNewCustomers["customersCompanyPLZ"];
										$thisIntervalIndex = $ds_getSalesmenNewCustomers["interval"];

										$arrAreas = array();
										if(preg_match("/^testID_/", $_POST["searchSalesman"])){
											$arrAreas[] = preg_replace("/^testID_/", "", $_POST["searchSalesman"]);
											#$thisSalesmanNumber = $_POST["searchSalesman"];
										}
										else {
											$arrAreas = $arrSalesmenAreas[$_POST["searchSalesman"]];
											#$thisSalesmanNumber = $ds_getSalesmenNewCustomers["orderDocumentsAddressCompanyCustomerNumber"];
										}

										if(!empty($arrAreas)){
											foreach($arrAreas as $thisSalesmanArea){
												if($arrSalesmenIntervalAreaNewCustomers[$thisSalesmanCustomerNumber][$thisIntervalIndex][$thisSalesmanArea] == ''){
													$arrSalesmenIntervalAreaNewCustomers[$thisSalesmanCustomerNumber][$thisIntervalIndex][$thisSalesmanArea] = 0;
												}
												if($arrSalesmenIntervalTotalNewCustomers[$thisSalesmanCustomerNumber][$thisIntervalIndex] == ''){
													$arrSalesmenIntervalTotalNewCustomers[$thisSalesmanCustomerNumber][$thisIntervalIndex] = 0;
												}
												if(preg_match("/^" . $thisSalesmanArea . "/", $ds_getSalesmenNewCustomers["customersCompanyPLZ"])){
													$arrSalesmenIntervalAreaNewCustomers[$thisSalesmanCustomerNumber][$thisIntervalIndex][$thisSalesmanArea]++;
													$arrSalesmenIntervalTotalNewCustomers[$thisSalesmanCustomerNumber][$thisIntervalIndex]++;
												}
											}
										}
									}
								// EOF GET NEW CUSTOMERS ON ENTRY DATE
							}
							echo '<table width="100%" cellpadding="0" cellspacing="0" class="displayOrders">';

							echo '<thead>';

							echo '<tr>';
							echo '<th style="width:45px">#</th>';
							echo '<th style="width:90px">Datum</th>';

							echo '<th class="spacer"></th>';
							echo '<th style="width:360px;">Status rot (letzte Bestellung &auml;lter als letzte 6 Monate)</th>';
							echo '<th class="spacer"></th>';
							echo '<th style="width:360px;">Status blau (innerhalb 6 Monaten Neukunden)</th>';
							echo '<th class="spacer"></th>';
							echo '<th style="width:360px;">Status gr&uuml;n (letzte Bestellung innerhalb letzter 6 Monate)</th>';
							echo '<th class="spacer"></th>';

							$thisColspan = 11;
							if((int)$_POST["searchSalesman"] > 0 ){
								$thisColspan = 19;
								echo '<th>&sum; ABs</th>';
								echo '<th>Eig. ABs</th>';
								echo '<th colspan="2">AB-Quote</th>';
								echo '<th class="spacer"></th>';

								if(SHOW_ORDER_POSITIONS){
									$thisColspan = 24;
									echo '<th>&sum; POS</th>';
									echo '<th>Eig. POS</th>';
									echo '<th colspan="2">POS-Quote</th>';
									echo '<th class="spacer"></th>';
								}

								echo '<th>Eig. NeuKnd</th>';
								echo '<th colspan="2">Neuknd-Quote</th>';

							}

							echo '<th style="width:150px" colspan="2">&sum; Kunden</th>';

							if((int)$_POST["searchSalesman"] > 0 ){
								// echo '<th>unbesucht</th>';
							}
							echo '</tr>';

							echo '</thead>';

							echo '<tbody>';

							$countRow = 0;
							$thisAreaMarker = "";
							$thisSalesmanMarker = "";

							$arrTendencyStartValues = array('red' => 100, 'blue' => 0, 'green' => 0); // %

							foreach($arrCustomerActivityStatus as $thisSalesmenKey => $thisSalesmenData) {
								$allOrdersTotal = 0;
								$allOrdersTotalPositions = 0;
								$thisAllOrdersTotal = 0;
								$thisSalesmanAllNewCustomersTotal = 0;
								$allNewCustomersTotal = 0;
								$allNoneVisitedCustomersTotal = 0;
								$allCustomersTotal = 0;
#dd('thisSalesmenData');
								foreach($thisSalesmenData as $thisArea => $thisAreaData) {

									if($thisSalesmanMarker != $thisSalesmenKey){
										$thisSalesmanMarker = $thisSalesmenKey;
										echo '<tr class="row8" style="background-color:#B8E6F2">';

										echo '<td colspan="' . $thisColspan . '">';
										echo '<b>' . $arrSalesmenWithActiveAreasData[$thisSalesmenKey]["salesmenFirmenname"] . '</b>';
										echo ' - PLZ-Gebiete: ' . preg_replace("/;/", " &bull; ", $arrSalesmenWithActiveAreasData[$thisSalesmenKey]["salesmenAreas"]);
										echo '</td>';
										echo '</tr>';
									}

									if($thisAreaMarker != $thisArea){
										if($thisArea != "AREA_TOTAL"){
											$thisAreaMarker = $thisArea;
											echo '<tr class="row3" style="font-weight:bold;">';
											echo '<td colspan="' . $thisColspan . '">';
											if($_POST["searchDisplay"] == 'AREA_SALESMAN'){
												echo '<a name="PLZ_' . $thisArea. '"></a>';
											}
											echo 'PLZ: ';
											echo $thisArea;
											echo ' - ' . $arrAllDoubleZipCodes[substr($thisArea, 0, 2)]["zipcodesDoubleDescription"];
											echo '</td>';
											echo '</tr>';
										}
										else{
											/*
											if(!empty($arrSearchZipCodes)){
												echo '<tr class="row3" style="font-weight:bold;">';
												echo '<td colspan="' . $thisColspan . '">';
												foreach($arrSearchZipCodes as $thisSalesmanAreas){
													echo 'PLZ: ' . $thisSalesmanAreas . ' - ' . $arrAllDoubleZipCodes[substr($thisSalesmanAreas, 0, 2)]["zipcodesDoubleDescription"];
													echo '<br />';
												}
												echo '</td>';
												echo '</tr>';
											}
											*/
										}
									}

									foreach($thisAreaData as $thisInterval => $thisCustomerActivityStatusData) {
										if($doExport){
											$exportData = array();
											$countFields = 0;
										}

										$thisArrIntervalsKey = array_search($thisInterval, $arrIntervals);
										$nextArrIntervalsKey = $thisArrIntervalsKey + 1;

										$rowClass = 'row0';
										if($countRow%2 == 0) {
											$rowClass = 'row1';
										}

										$rowStyle = '';
										if($thisInterval < SALEMESMEN_AREA_CONTRACT_START_DATE){
											$rowStyle = 'opacity:1;';
										}

										echo '<tr style="border-bottom: 2px solid #003300;'.$rowStyle.'" class="'.$rowClass.'">';

										echo '<td style="text-align:right;"><b>';
										echo ($countRow + 1);
										echo '.</b></td>';

										echo '<td style="text-align:right;white-space:nowrap;"><b>';
										$thisDate = '';
										if($_POST["searchInterval"] == 'WEEK'){
											$thisDate .= '<span style="padding-right:2px;font-size:8px;font-weight:normal:">KW</span>' . date("W", strtotime($thisInterval)) . ' - ';
										}
										if($_POST["searchInterval"] == 'MONTH'){
											$thisDate .= '<span style="padding-right:2px;font-size:8px;font-weight:normal:">' . $arrUseMonths[date("m", strtotime($thisInterval))] . '</span> - ';
										}

										$thisDate .= formatDate($thisInterval, 'display');

										if($_POST["searchInterval"] == 'MONTH'){
											$thisIntervalIndex = substr($thisInterval, 0, -3);
											$thisIntervalIndex = preg_replace("/\-/", "#", $thisIntervalIndex);
										}
										else if($_POST["searchInterval"] == 'WEEK'){
											$thisIntervalIndex = substr($thisInterval, 0, -6) . '#' . date("W", strtotime($thisInterval));
										}
										else if($_POST["searchInterval"] == 'QUARTER'){
											$thisMonth = date("n", strtotime($thisInterval));
											if($thisMonth < 4){
												$thisQuarter = 1;
											}
											else if($thisMonth < 7){
												$thisQuarter = 2;
											}
											else if($thisMonth < 10){
												$thisQuarter = 3;
											}
											else if($thisMonth < 13){
												$thisQuarter = 4;
											}
											$thisIntervalIndex = substr($thisInterval, 0, 4) . '#' . $thisQuarter . '. Quartal';
										}
										else if($_POST["searchInterval"] == 'YEAR'){
											$thisIntervalIndex = substr($thisInterval, 0, 4) . '#' .substr($thisInterval, 0, 4);
										}
										else {
											$thisIntervalIndex = $thisInterval;
										}

										echo $thisDate;

										echo '</b></td>';

										$thisTotalCount = 0;
										$nextTotalCount = 0;
										$thisOrderTotal = 0;
										$thisNewCustomersTotal = 0;
										$thisNoneVisitedCustomersTotal = 0;

										if(!empty($thisCustomerActivityStatusData)){
											foreach($thisCustomerActivityStatusData as $thisStatusType => $thisStatusTypeData){
												$thisTotalCount += $thisStatusTypeData["statusCount"];
											}

											$nextCustomerActivityStatusData = $thisAreaData[$arrIntervals[$nextArrIntervalsKey]];

											if(!empty($nextCustomerActivityStatusData)){
												foreach($nextCustomerActivityStatusData as $nextStatusType => $nextStatusTypeData){
													$nextTotalCount += $nextStatusTypeData["statusCount"];
												}
											}

											$scaleLenth = 188;
											if(SHOW_ORDER_POSITIONS){
												$scaleLenth = 120;
											}
											$arrOrderStatusTypes = array('red', 'blue', 'green');
											#foreach($thisCustomerActivityStatusData as $thisStatusType => $thisStatusTypeData){
											foreach($arrOrderStatusTypes as $thisStatusType){
												$thisStatusCount = $thisCustomerActivityStatusData[$thisStatusType]["statusCount"];
												$thisStatusCountPercent = round(($thisCustomerActivityStatusData[$thisStatusType]["statusCount"] / $thisTotalCount) * 100, 2);
												$nextStatusCountPercent = round(($thisAreaData[$arrIntervals[$nextArrIntervalsKey]][$thisStatusType]["statusCount"] / $thisTotalCount) * 100, 2);
												$thisStatusCountInterval = $thisCustomerActivityStatusData[$thisStatusType]["statusCount"];
												$nextStatusCountInterval = $thisAreaData[$arrIntervals[$nextArrIntervalsKey]][$thisStatusType]["statusCount"];
												$thisStatusCountIntervalDiff = $thisStatusCountInterval - $nextStatusCountInterval;

												if($arrIntervals[$nextArrIntervalsKey] == ''){
													$nextStatusCountPercent = $arrTendencyStartValues[$thisStatusType];
												}

												$thisTendenyImage = "tendency_zero.png";
												if($nextStatusCountPercent == $thisStatusCountPercent){
													$thisTendenyImage = "tendency_zero.png";
												}
												else if($nextStatusCountPercent < $thisStatusCountPercent){
													$thisTendenyImage = "tendency_up.png";
												}
												else if($nextStatusCountPercent > $thisStatusCountPercent){
													$thisTendenyImage = "tendency_down.png";
												}
												#$arrTendency[$thisStatusType] = $thisStatusCountPercent;

												if($thisStatusType == 'green'){
													$thisBackgroundColor = '#00FF00';
												}
												else if($thisStatusType == 'blue'){
													$thisBackgroundColor = '#0000FF';
												}
												else if($thisStatusType == 'red'){
													$thisBackgroundColor = '#FF0000';
												}

												echo '<td class="spacer"></td>';
												echo '<td style="white-space:nowrap;">';
												echo '<div style="white-space:nowrap;width:' . ($scaleLenth + 160). 'px;margin:0;padding:0">';
												echo '<div style="float:left;background-color:#FFFFFF;width:' . $scaleLenth . 'px;border:1px solid #000000;margin:0;padding:0;text-align:left;">';
												$thisScaleLength = round($scaleLenth * $thisStatusCountPercent / 100);
												if($thisScaleLength < 1){
													$thisScaleLength = 1;
												}
												echo '<div style="height:12px;background-color:' . $thisBackgroundColor . ';width:' . $thisScaleLength . 'px;margin:0;padding:0;">';
												echo '</div>';
												echo '</div>';
												echo '<div style="float:right;width:130px;margin:0;padding:0 20px 0 0;text-align:right;white-space:nowrap;">';
												echo $thisStatusCount . ' (' . number_format($thisStatusCountPercent, 2, ",", ".") . '%)';

												if($thisStatusCountIntervalDiff > 0){
													#$thisStatusCountIntervalDiff = '+' . $thisStatusCountIntervalDiff;
												}
												echo '<span style="padding-left:10px;"><b>' . $thisStatusCountIntervalDiff . '</b></span>';

												if(in_array($thisStatusType, array('blue', 'green'))){
													$thisOrderTotal += $thisStatusCountIntervalDiff;
												}

												if(in_array($thisStatusType, array('blue'))){
													$thisNewCustomersTotal = $thisStatusCountIntervalDiff;
													$thisNewCustomersTotal2 = $thisNewCustomersTotal;
												}
												if(in_array($thisStatusType, array('red'))){
													$thisNoneVisitedCustomersTotal = $thisStatusCount;
												}

												echo '<img src="layout/icons/' . $thisTendenyImage . '" width="14" height="14" alt="Tendenz" title="Tendenz" style="padding-left:10px;margin-bottom:-2px;" />';

												echo '</div>';
												echo '<div class="clear"></div>';
												echo '</div>';
												echo '</td>';
											}
										}

										echo '<td class="spacer"></td>';

										if((int)$_POST["searchSalesman"] > 0 ){
											echo '<td style="background-color:#FCFCC9;text-align:right;font-weight:bold;white-space:nowrap">';
											// echo $thisOrderTotal;

											if($_POST["searchDisplay"] == 'AREA_TOTAL'){
												$thisOrderTotal = $arrAllIntervalTotalOrders[$thisSalesmanCustomerNumber][$thisIntervalIndex];
												$thisOrderTotalPositions = $arrAllIntervalTotalOrdersPositions[$thisSalesmanCustomerNumber][$thisIntervalIndex];
											}
											else if($_POST["searchDisplay"] == 'AREA_SALESMAN'){
												$thisOrderTotal = $arrAllIntervalAreaOrders[$thisSalesmanCustomerNumber][$thisIntervalIndex][$thisArea];
												$thisOrderTotalPositions = $arrAllIntervalAreaOrdersPositions[$thisSalesmanCustomerNumber][$thisIntervalIndex][$thisArea];
											}
											if($thisOrderTotal == ''){ $thisOrderTotal = 0; }
											if($thisOrderTotalPositions == ''){ $thisOrderTotalPositions = 0; }

											$allOrdersTotal += $thisOrderTotal;
											$allOrdersTotalPositions += $thisOrderTotalPositions;

											echo $thisOrderTotal;

											echo '</td>';

											echo '<td style="background-color:#FCFCC9;text-align:right;font-weight:bold;white-space:nowrap">';
											if($_POST["searchDisplay"] == 'AREA_TOTAL'){
												$thisSalesmanOrderTotal = $arrSalesmenIntervalTotalOrders[$thisSalesmanCustomerNumber][$thisIntervalIndex];
												$thisSalesmanOrderTotalPositions = $arrSalesmenIntervalTotalOrdersPositions[$thisSalesmanCustomerNumber][$thisIntervalIndex];
											}
											else if($_POST["searchDisplay"] == 'AREA_SALESMAN'){
												$thisSalesmanOrderTotal = $arrSalesmenIntervalAreaOrders[$thisSalesmanCustomerNumber][$thisIntervalIndex][$thisArea];
												$thisSalesmanOrderTotalPositions = $arrSalesmenIntervalAreaOrdersPositions[$thisSalesmanCustomerNumber][$thisIntervalIndex][$thisArea];
											}
											if($thisSalesmanOrderTotal == ''){ $thisSalesmanOrderTotal = 0; }
											if($thisSalesmanOrderTotalPositions == ''){ $thisSalesmanOrderTotalPositions = 0; }

											$thisAllOrdersTotal += $thisSalesmanOrderTotal;
											$thisAllOrdersTotalPositions += $thisSalesmanOrderTotalPositions;

											echo $thisSalesmanOrderTotal;
											echo '</td>';

											echo '<td style="background-color:#FCFCC9;text-align:right;white-space:nowrap;">';
											$thisSalesmanOrderQuota = ($thisSalesmanOrderTotal / $thisOrderTotal * 100);
											echo number_format($thisSalesmanOrderQuota, 2, ",", ".") . ' %';

											$thisSalesmanOrderQuotaPositions = ($thisSalesmanOrderTotalPositions / $thisOrderTotalPositions * 100);

											echo '</td>';

											echo '<td style="background-color:#FCFCC9;">';
											if($thisSalesmanOrderQuota < MIN_QUOTA_ORDERS){
												$thisQuotaRatingImage = 'iconNotOk.png';
											}
											else {
												$thisQuotaRatingImage = 'iconOk.png';
											}
											echo '<img src="layout/icons/' . $thisQuotaRatingImage . '" width="16" height="16" alt="" title="" />';
											echo '</td>';

											echo '<td class="spacer"></td>';

											if(SHOW_ORDER_POSITIONS){
												echo '<td style="text-align:right;"><b>' . $thisOrderTotalPositions . '</b></td>';
												echo '<td style="text-align:right;"><b>' . $thisSalesmanOrderTotalPositions . '</b></td>';
												echo '<td style="text-align:right;white-space:nowrap;"><b>' . number_format($thisSalesmanOrderQuotaPositions, 2, ",", ".") . ' %' . '</b></td>';
												echo '<td>';
												if($thisSalesmanOrderQuotaPositions < MIN_QUOTA_ORDERS){
													$thisQuotaRatingImage = 'iconNotOk.png';
												}
												else {
													$thisQuotaRatingImage = 'iconOk.png';
												}
												echo '<img src="layout/icons/' . $thisQuotaRatingImage . '" width="16" height="16" alt="" title="" />';
												echo '</td>';
												echo '<td class="spacer"></td>';
											}

											echo '<td style="background-color:#FCFCC9;text-align:right;font-weight:bold;">';

											if($_POST["searchDisplay"] == 'AREA_TOTAL'){
												$thisSalesmanNewCustomersTotal = $arrSalesmenIntervalTotalNewCustomers[$thisSalesmanCustomerNumber][$thisIntervalIndex];
											}
											else if($_POST["searchDisplay"] == 'AREA_SALESMAN'){
												$thisSalesmanNewCustomersTotal = $arrSalesmenIntervalAreaNewCustomers[$thisSalesmanCustomerNumber][$thisIntervalIndex][$thisArea];
											}
											if($thisSalesmanNewCustomersTotal == ''){
												$thisSalesmanNewCustomersTotal = 0;
											}
											echo $thisSalesmanNewCustomersTotal;

											echo '</td>';

											echo '<td style="background-color:#FCFCC9;text-align:right;white-space:nowrap;">';
											$thisSalesmanNewCustomersQuota = ($thisSalesmanNewCustomersTotal / $thisNewCustomersTotal * 100);

											$thisSalesmanAllNewCustomersTotal += $thisSalesmanNewCustomersTotal;

											echo number_format($thisSalesmanNewCustomersQuota, 2, ",", ".") . ' %';
											echo '</td>';

											echo '<td style="background-color:#FCFCC9;">';
											if($thisSalesmanNewCustomersQuota < MIN_QUOTA_NEW_CUSTOMERS){
												$thisQuotaRatingImage = 'iconNotOk.png';
											}
											else {
												$thisQuotaRatingImage = 'iconOk.png';
											}
											echo '<img src="layout/icons/' . $thisQuotaRatingImage . '" width="16" height="16" alt="" title="" />';
											echo '</td>';
										}

										echo '<td style="text-align:right;white-space:nowrap;">';
										if($countRow == 0){
											$allNoneVisitedCustomersTotal = $thisNoneVisitedCustomersTotal;
											$allCustomersTotal = $thisTotalCount;
										}
										echo number_format($thisTotalCount, 0, ',', '.');
										echo '</td>';

										echo '<td style="text-align:right;white-space:nowrap;">';

										$diffTotalCount = $thisTotalCount - $nextTotalCount;

										if($nextTotalCount == 0){
											$diffTotalCount = 0;
											if($thisNewCustomersTotal2 > 0){
												#$diffTotalCount = $thisNewCustomersTotal2;
											}
										}
										if($diffTotalCount > 0){
											$diffTotalCount = '+' . $diffTotalCount;
										}

										$allNewCustomersTotal += $diffTotalCount;
										echo $diffTotalCount;

										echo '</td>';

										if((int)$_POST["searchSalesman"] > 0 ){
											#echo '<td style="text-align:right;">';
											#echo $thisNoneVisitedCustomersTotal;
											#echo '</td>';
										}

										echo '</tr>';

										if($doExport){
											#$thisFieldIndex = chr(($thisDateStartColumnIndex + $countFields)) . ($thisDateStartRowIndex + $countRow);
											$thisFieldIndex = 'A' . ($thisDateStartRowIndex + $countRow);
											$exportData[$thisFieldIndex] = trim(strip_tags(preg_replace("/>/", "> ", $thisDate)));
											$countFields++;

											$thisFieldIndex = 'B' . ($thisDateStartRowIndex + $countRow);
											$exportData[$thisFieldIndex] = trim(strip_tags(preg_replace("/>/", "> ", $thisOrderTotal)));

											$thisFieldIndex = 'C' . ($thisDateStartRowIndex + $countRow);
											$exportData[$thisFieldIndex] = trim(strip_tags(preg_replace("/>/", "> ", $thisSalesmanOrderTotal)));

											$thisFieldIndex = 'D' . ($thisDateStartRowIndex + $countRow);
											$exportData[$thisFieldIndex] = trim(strip_tags(preg_replace("/>/", "> ", number_format($thisSalesmanOrderQuota, 2, ",", ".")))) . ' %';
											if($thisSalesmanOrderQuota < MIN_QUOTA_ORDERS){ $thisQuotaRating = 'NEG'; }
											else { $thisQuotaRating = 'POS'; }
											$exportData[$thisFieldIndex] .= ' [' . $thisQuotaRating . ']';

											$thisFieldIndex = 'E' . ($thisDateStartRowIndex + $countRow);
											$exportData[$thisFieldIndex] = trim(strip_tags(preg_replace("/>/", "> ", $thisNewCustomersTotal)));

											$thisFieldIndex = 'F' . ($thisDateStartRowIndex + $countRow);
											$exportData[$thisFieldIndex] = trim(strip_tags(preg_replace("/>/", "> ", $thisSalesmanNewCustomersTotal)));

											$thisFieldIndex = 'G' . ($thisDateStartRowIndex + $countRow);
											$exportData[$thisFieldIndex] = trim(strip_tags(preg_replace("/>/", "> ", number_format($thisSalesmanNewCustomersQuota, 2, ",", ".")))) . ' %';
											if($thisSalesmanNewCustomersQuota < MIN_QUOTA_NEW_CUSTOMERS){ $thisQuotaRating = 'NEG'; }
											else { $thisQuotaRating = 'POS'; }
											$exportData[$thisFieldIndex] .= ' [' . $thisQuotaRating . ']';

											$thisFieldIndex = 'H' . ($thisDateStartRowIndex + $countRow);
											$exportData[$thisFieldIndex] = trim(strip_tags(preg_replace("/>/", "> ", $thisNoneVisitedCustomersTotal)));

											if($_POST["exportType"] == 'xls'){
												if(!empty($exportData)){
													foreach($exportData as $thisReplaceFieldKey => $thisReplaceFieldValue){
														$thisWorksheet->setCellValue($thisReplaceFieldKey, $thisReplaceFieldValue);
														$thisWorksheet->getStyle($thisReplaceFieldKey)->getBorders()->getAllBorders()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
														$thisWorksheet->getStyle($thisReplaceFieldKey)->getFont()->setSize(9);
													}
												}
											}
											else if($_POST["exportType"] == 'csv'){
												if(!empty($exportData)){
													fwrite($fp_export, implode($fieldSeparator, $exportData) . $lineSeparator);
												}
											}
										}

										$countRow++;
									}
								}

								echo '<tr>';
								echo '<td colspan="' . $thisColspan . '"><hr /></td>';
								echo '</tr>';

								echo '<tr class="row2" style="font-weight:bold;">';

								echo '<td colspan="2">';
								echo 'Insgesamt:';
								echo '</td>';

								echo '<td class="spacer"></td>';

								echo '<td>';
								echo '-';
								echo '</td>';

								echo '<td class="spacer"></td>';

								echo '<td>';
								echo '-';
								echo '</td>';

								echo '<td class="spacer"></td>';

								echo '<td>';
								echo '-';
								echo '</td>';

								echo '<td class="spacer"></td>';

								echo '<td style="text-align:right;white-space:nowrap;">';
								echo $allOrdersTotal;

								echo '</td>';

								echo '<td style="text-align:right;;white-space:nowrap;">';
								echo $thisAllOrdersTotal;

								echo '</td>';

								echo '<td style="text-align:right;;white-space:nowrap;">';
								$allOrdersTotalQuota = (($thisAllOrdersTotal / $allOrdersTotal) * 100);
								echo number_format($allOrdersTotalQuota, 2, ",", ".") . ' %';

								$allOrdersTotalQuotaPositions = (($thisAllOrdersTotalPositions / $allOrdersTotalPositions) * 100);

								echo '</td>';

								echo '<td>';
								if($allOrdersTotalQuota < MIN_QUOTA_ORDERS){
									$thisQuotaRatingImage = 'iconNotOk.png';
								}
								else {
									$thisQuotaRatingImage = 'iconOk.png';
								}
								echo '<img src="layout/icons/' . $thisQuotaRatingImage . '" width="16" height="16" alt="" title="" />';
								echo '</td>';

								echo '<td class="spacer"></td>';

								if(SHOW_ORDER_POSITIONS){
									echo '<td style="text-align:right;">' . $allOrdersTotalPositions . '</td>';
									echo '<td style="text-align:right;">' . $thisAllOrdersTotalPositions . '</td>';
									echo '<td style="text-align:right;white-space:nowrap;">' . number_format($allOrdersTotalQuotaPositions, 2, ",", ".") . ' %' . '</td>';
									echo '<td>';
									if($allOrdersTotalQuotaPositions < MIN_QUOTA_ORDERS){
										$thisQuotaRatingImage = 'iconNotOk.png';
									}
									else {
										$thisQuotaRatingImage = 'iconOk.png';
									}
									echo '<img src="layout/icons/' . $thisQuotaRatingImage . '" width="16" height="16" alt="" title="" />';
									echo '</td>';
									echo '<td class="spacer"></td>';
								}

								echo '<td style="text-align:right;">';
								echo $thisSalesmanAllNewCustomersTotal;
								echo '</td>';

								echo '<td style="text-align:right;">';
								$allNewCustomersTotalQuota = (($thisSalesmanAllNewCustomersTotal / $allNewCustomersTotal) * 100);
								echo number_format($allNewCustomersTotalQuota, 2, ",", ".") . ' %';
								echo '</td>';

								echo '<td style="text-align:right;">';
								if($allNewCustomersTotalQuota < MIN_QUOTA_NEW_CUSTOMERS){
									$thisQuotaRatingImage = 'iconNotOk.png';
								}
								else {
									$thisQuotaRatingImage = 'iconOk.png';
								}
								echo '<img src="layout/icons/' . $thisQuotaRatingImage . '" width="16" height="16" alt="" title="" />';
								echo '</td>';

								echo '<td style="text-align:right;">';
								echo $allCustomersTotal;
								echo '</td>';

								echo '<td style="text-align:right;">';
								if($allNewCustomersTotal > 0){
									$allNewCustomersTotal = '+' . $allNewCustomersTotal;
								}
								echo $allNewCustomersTotal;
								echo '</td>';

								if((int)$_POST["searchSalesman"] > 0 ){
									#echo '<td style="text-align:right;">';
									#echo $allNoneVisitedCustomersTotal;
									#echo '</td>';
								}

								echo '</tr>';

								if($doExport){
									// BOF ADD TOTAL SUM
									$exportData = array();
									$countRow = $countRow + 2;
									#$thisFieldIndex = chr(($thisDateStartColumnIndex + $countFields)) . ($thisDateStartRowIndex + $countRow);
									$thisFieldIndex = 'A' . ($thisDateStartRowIndex + $countRow);
									$exportData[$thisFieldIndex] = 'Insgesamt';
									$countFields++;

									$thisFieldIndex = 'B' . ($thisDateStartRowIndex + $countRow);
									$exportData[$thisFieldIndex] = trim(strip_tags(preg_replace("/>/", "> ", $allOrdersTotal)));

									$thisFieldIndex = 'C' . ($thisDateStartRowIndex + $countRow);
									$exportData[$thisFieldIndex] = trim(strip_tags(preg_replace("/>/", "> ", $thisAllOrdersTotal)));

									$thisFieldIndex = 'D' . ($thisDateStartRowIndex + $countRow);
									$exportData[$thisFieldIndex] = trim(strip_tags(preg_replace("/>/", "> ", number_format($allOrdersTotalQuota, 2, ",", ".")))) . ' %';
									if($allOrdersTotalQuota < MIN_QUOTA_ORDERS){ $thisQuotaRating = 'NEG'; }
									else { $thisQuotaRating = 'POS'; }
									$exportData[$thisFieldIndex] .= ' [' . $thisQuotaRating . ']';

									$thisFieldIndex = 'E' . ($thisDateStartRowIndex + $countRow);
									$exportData[$thisFieldIndex] = trim(strip_tags(preg_replace("/>/", "> ", $allNewCustomersTotal)));

									$thisFieldIndex = 'F' . ($thisDateStartRowIndex + $countRow);
									$exportData[$thisFieldIndex] = trim(strip_tags(preg_replace("/>/", "> ", $thisSalesmanAllNewCustomersTotal)));

									$thisFieldIndex = 'G' . ($thisDateStartRowIndex + $countRow);
									$exportData[$thisFieldIndex] = trim(strip_tags(preg_replace("/>/", "> ", number_format($allNewCustomersTotalQuota, 2, ",", ".")))) . ' %';
									if($allNewCustomersTotalQuota < MIN_QUOTA_NEW_CUSTOMERS){ $thisQuotaRating = 'NEG'; }
									else { $thisQuotaRating = 'POS'; }
									$exportData[$thisFieldIndex] .= ' [' . $thisQuotaRating . ']';

									$thisFieldIndex = 'H' . ($thisDateStartRowIndex + $countRow);
									$exportData[$thisFieldIndex] = trim(strip_tags(preg_replace("/>/", "> ", $allNoneVisitedCustomersTotal)));

									if($_POST["exportType"] == 'xls'){
										if(!empty($exportData)){
											foreach($exportData as $thisReplaceFieldKey => $thisReplaceFieldValue){
												$thisWorksheet->setCellValue($thisReplaceFieldKey, $thisReplaceFieldValue);
												$thisWorksheet->getStyle($thisReplaceFieldKey)->getBorders()->getAllBorders()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
												$thisWorksheet->getStyle($thisReplaceFieldKey)->getFont()->setSize(9);
											}
										}
									}
									else if($_POST["exportType"] == 'csv'){
										if(!empty($exportData)){
											fwrite($fp_export, implode($fieldSeparator, $exportData) . $lineSeparator);
										}
									}
									// EOF ADD TOTAL SUM

									if($_POST["exportType"] == 'xls'){
										$thisReplaceFieldKey = "A" . ($thisDateStartRowIndex + $countRow + 3);
										$thisWorksheet->setCellValue($thisReplaceFieldKey, EXPORT_COMPANY_NAME);
										$thisWorksheet->getStyle($thisReplaceFieldKey)->getFont()->setSize(10);
										$thisWorksheet->getStyle($thisReplaceFieldKey)->getFont()->setBold(true);

										$thisReplaceFieldKey = "A" . ($thisDateStartRowIndex + $countRow + 4);
										$thisWorksheet->setCellValue($thisReplaceFieldKey, EXPORT_PERSON_NAME);
										$thisWorksheet->getStyle($thisReplaceFieldKey)->getFont()->setSize(9);
										$thisWorksheet->getStyle($thisReplaceFieldKey)->getFont()->setBold(true);

										$thisReplaceFieldKey = "A" . ($thisDateStartRowIndex + $countRow + 5);
										$thisWorksheet->setCellValue($thisReplaceFieldKey, EXPORT_PERSON_FUNCTION);
										$thisWorksheet->getStyle($thisReplaceFieldKey)->getFont()->setSize(8);

										$newSheetName = $excelSheetName;
										$newSheetName = preg_replace("/{###SEITE###}/", ($thisSheetKey + 1), $excelSheetName);
										$thisWorksheet->setTitle($newSheetName);
										clearstatcache();
										$countExcelRow = 1;
										foreach($thisWorksheet->getRowDimensions() as $rd) {
											if($countExcelRow > $excelLastHeaderRow){
												$rd->setRowHeight($excelRowHeight);
											}
											$countExcelRow++;
											clearstatcache();
										}

										clearstatcache();
										$objPHPExcel->setActiveSheetIndex(0);
										clearstatcache();

										// BOF INCLUDE LOGO
											if(1){
												#$objDrawing = new PHPExcel_Worksheet_MemoryDrawing();

												$objDrawing = new PHPExcel_Worksheet_Drawing();
												$objDrawing->setName('Logo');
												$objDrawing->setDescription('Logo');

												#$gdImage = imagecreatefromjpeg($excelLogoPath);
												#$objDrawing->setImageResource($gdImage);
												#$objDrawing->setRenderingFunction(PHPExcel_Worksheet_MemoryDrawing::RENDERING_JPEG);
												#$objDrawing->setMimeType(PHPExcel_Worksheet_MemoryDrawing::MIMETYPE_DEFAULT);

												$objDrawing->setPath($excelLogoPath);
												$objDrawing->setCoordinates('F1');
												$objDrawing->setHeight(70);
												$objDrawing->setWorksheet($thisWorksheet);
											}
										// EOF INCLUDE LOGO

										if(file_exists($thisStoreDir . $fileNameCreate)){ unlink($thisStoreDir . $fileNameCreate); }
										$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, $excelFileExcelVersion);
										$excelResult = $objWriter->save($thisStoreDir . $fileNameCreate);

										$excelResult = file_exists($thisStoreDir . $fileNameCreate);

										if($excelResult) {
											$successMessage .= ' Die Excel-Datei &quot; ' . $fileNameCreate . ' &quot; wurde erstellt.' .'<br />';
										}
										else {
											$errorMessage .= ' Die Excel-Datei &quot; ' . $fileNameCreate . ' &quot; konnte nicht erstellt werden.' .'<br />';
										}
										clearstatcache();
										$objPHPExcel->disconnectWorksheets();
										unset($objPHPExcel);
									}
									else if($_POST["exportType"] == 'csv'){
										fwrite($fp_export, EXPORT_COMPANY_NAME . $lineSeparator);
										fwrite($fp_export, EXPORT_PERSON_NAME . $lineSeparator);
										fwrite($fp_export, EXPORT_PERSON_FUNCTION . $lineSeparator);
										fclose($fp_export);
									}
								}
							}

							echo '</tbody>';

							echo '</table>';
							echo '<hr />';
						}
						else {
							$infoMessage .= ' Es liegen keine Daten vor!' . '<br />';
						}
						displayMessages();

						if($excelResult || $csvResult) {
							$downloadPath = str_replace(BASEPATH, "", $thisStoreDir);
							echo '<b>DOWNLOAD:</b> <img src="layout/icons/icon' . strtoupper(getFileType($fileNameCreate)) . '.png" width="14" height="14" alt="Download" title="Download Datei" /> <a href="' . $downloadPath . $fileNameCreate . '">' . $fileNameCreate . '</a>';
						}

					?>
					<?php displayMessages(); ?>
				</div>

			</div>
		</div>

<script language="javascript" type="text/javascript">
	$(document).ready(function() {
		$(function() {
			colorRowMouseOver('.displayOrders tbody tr');
			$('.buttonShopOrderInfo').css('cursor', 'pointer');
			$('.buttonShopOrderInfo').click(function () {
				loadOrderDetails($(this), '<?php echo BASEPATH; ?>', 'Details der Bestellung');
			});
		});

		$('.buttonToggleSidebarMenue').click(function() {
			$('#menueSidebarToggleContent').toggle();
		});

		$('.blink').parent().parent().parent('tr').css('background-color', '#FFBFC8 !important');
		$('.blink').parent().parent().parent().find('td').css('background-color', '#FFBFC8 !important');

		$(function() {
			$('#tabs').tabs();
		});
	});
</script>
<?php require_once('inc/footerHTML.inc.php'); ?>