<?php
	require_once('inc/requires.inc.php');

	if(!$arrGetUserRights["editCustomers"] && !$arrGetUserRights["displayCustomers"]) {
		header('location: ' . PAGE_EXIT_LOCATION);
		exit;
	}

	$jswindowMessage = "";
	$warningMessage = '';
	$errorMessage = '';
	$successMessage = '';
	$infoMessage = '';

	$dbConnection = new DB_Connection();
	$db_open = $dbConnection->db_connect();

	// BOF READ USER DATAS
		$userDatas = getUserDatas();
	// EOF READ USER DATAS

	// BOF EVENT MESSAGES
		$arrTodayMessages = getTodayMessages(date('Y-m-d'), 'all');
	// EOF EVENT MESSAGES


?>
<?php
	require_once('inc/headerHTML.inc.php');
	$thisTitle = "Infos &amp; Benachrichtigungen";
	$headerHTML = preg_replace("/{###TITLE###}/", strip_tags($thisTitle), $headerHTML);
	echo $headerHTML;

	require_once(FILE_MENUE_TOP);
?>
<div id="mainArea">
	<div id="mainContent">
		<?php require_once(FILE_MENUE_SIDEBAR); ?>
		<div id="contentArea">
			<div id="contentAreaElements">
				<h1><?php if(SHOW_PAGE_TITLE_ICONS) { echo '<img src="' . PATH_ICONS_MENUE_TITLES . 'messages.png" alt="" />'; } ?> <?php echo $thisTitle; ?></h1>

				<div class="contentDisplay">
					<?php displayMessages(); ?>
					<?php
						if(!empty($arrTodayMessages)){
							$countRow = 0;
							echo '<table border="0" width="100%" cellpadding="0" cellspacing="0" class="displayOrders">';
							echo '<tr>';
							echo '<th>#</th>';
							echo '<th>Datum</th>';
							echo '<th>Titel</th>';
							echo '<th>Text</th>';
							echo '</tr>';

							foreach($arrTodayMessages as $thisTodayMessageKey => $thisTodayMessageValue){
								if($thisTodayMessageValue["newsType"] != ""){
									if($countRow%2 == 0){ $rowClass = 'row0'; }
									else { $rowClass = 'row1'; }
									echo '<tr class="'.$rowClass.'">';

									echo '<td style="text-align:right;">';
									echo '<b>' . ($countRow + 1) . '.</b>';
									echo '</td>';

									echo '<td>';
									echo formatDate($thisTodayMessageValue["newsDateEntry"], 'display');
									echo '</td>';

									echo '<td style="white-space:nowrap;">';
									echo ($thisTodayMessageValue["newsTitle"]) . '';
									echo '</td>';

									echo '<td>';
									echo nl2br($thisTodayMessageValue["newsText"]) . '';
									echo '</td>';
									echo '</tr>';
									$countRow++;
								}
							}
							echo '</table>';
						}
					?>
				</div>
			</div>
		</div>
	</div>
</div>
<script language="javascript" type="text/javascript">
	$(document).ready(function() {

		setFocus('formFilterSearch', 'searchWord');
		colorRowMouseOver('.displayOrders tbody tr');
		$(function() {
			$.datepicker.setDefaults($.datepicker.regional[""]);
			$('#displayReportsDateFrom').datepicker($.datepicker.regional["de"]);
			$('#displayReportsDateTo').datepicker($.datepicker.regional["de"]);
		});
		$(function() {
			$('#tabs').tabs();
		});

		$('.buttonToggleSidebarMenue').click(function() {
			$('#menueSidebarToggleContent').toggle();
		});
	});
</script>

<?php require_once('inc/footerHTML.inc.php'); ?>