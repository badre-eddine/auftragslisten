<?php
	require_once('inc/requires.inc.php');

	if(!$arrGetUserRights["editCustomers"] && !$arrGetUserRights["displayCustomers"]) {
		header('location: ' . PAGE_EXIT_LOCATION);
		exit;
	}

	$dbConnection = new DB_Connection();
	$db_open = $dbConnection->db_connect();

	$jswindowMessage = '';
	$warningMessage = '';
	$errorMessage = '';
	$successMessage = '';
	$infoMessage = '';

	// BOF GET USER DATAS
		$userDatas = getUserDatas();
	// EOF GET USER DATAS

	// BOF USED TABLES
	$arrTables = array(
			'AN' => TABLE_ORDER_AN,
			'AB' => TABLE_ORDER_AB,
			'RE' => TABLE_ORDER_RE,
			'LS' => TABLE_ORDER_LS,
			'GU' => TABLE_ORDER_GU,
			'MA' => TABLE_ORDER_MA,
			'M1' => TABLE_ORDER_M1,
			'M2' => TABLE_ORDER_M2,
			'BR' => TABLE_ORDER_BR,
			'KR' => TABLE_ORDER_RK
		);
	// EOF USED TABLES

	// BOF DOWNLOAD FILE
	if($_GET["downloadFile"] != "" && $_GET["documentType"] != "") {
		if($_GET["documentType"] == 'KA') {
			$fileDirectory = DIRECTORY_UPLOAD_FILES . '';
		}
		else if($_GET["documentType"] == 'PR') {
			$fileDirectory = DIRECTORY_CREATED_DOCUMENTS_SALESMEN . '';
		}
		else {
			$fileDirectory = DIRECTORY_CREATED_DOCUMENTS_CUSTOMERS;
		}
		$_GET["downloadFile"] = utf8_decode(urldecode($_GET["downloadFile"]));

		require_once('classes/downloadFile.class.php');
		$thisDownload = new downloadFile($fileDirectory, $_GET["downloadFile"]);
		$errorMessage .= $thisDownload->startDownload();
	}
	// EOF DOWNLOAD FILE

	// BOF DELETE FILE
	if($_GET["deleteFile"] != "" && $_GET["deleteFile"] != "") {
		if($_GET["documentType"] == 'KA') {
			$fileDirectory = DIRECTORY_UPLOAD_FILES . '';
		}
		else {
			$fileDirectory = DIRECTORY_CREATED_DOCUMENTS_CUSTOMERS;
		}

		#$thisDeleteFile = utf8_encode($_GET["deleteFile"]);
		$thisDeleteFile = ($_GET["deleteFile"]);
		$sql = "
			DELETE FROM `" . TABLE_CREATED_DOCUMENTS . "`
			WHERE `createdDocumentsType` = '" . $_GET["documentType"] . "'
				AND `createdDocumentsTitle` = '" . $thisDeleteFile . "'
		";

		$rs = $dbConnection->db_query($sql);

		if($rs){
			$successMessage .= 'Die Datei &quot;' . $fileDirectory . $thisDeleteFile . '&quot; wurde aus der Datenbank entfernt.' . '<br />';
		}
		else {
			$errorMessage .= 'Die Datei &quot;' . $fileDirectory . $thisDeleteFile . '&quot; konnte leider nicht aus der Datenbank entfernt werden.' . '<br />';
		}

		$thisDeleteFile = utf8_decode(urldecode($_GET["deleteFile"]));
		if(file_exists($fileDirectory . $thisDeleteFile)) {
			$resultUnlink = unlink($fileDirectory . $thisDeleteFile);
			if($resultUnlink) {
				$successMessage .= 'Die Datei &quot;' . $fileDirectory . utf8_encode($thisDeleteFile) . '&quot; wurde aus dem Verzeichnis entfernt.' . '<br />';
			}
			else {
				$errorMessage .= 'Die Datei &quot;' . $fileDirectory . utf8_encode($thisDeleteFile) . '&quot; konnte leider nicht aus dem Verzeichnis entfernt werden.' . '<br />';
			}
		}
		else {
			$errorMessage .= 'Die Datei &quot;' . $fileDirectory . utf8_encode($thisDeleteFile) . '&quot; existiert nicht und kann daher nicht entfernt werden.' . '<br />';
		}
	}
	// EOF DELETE FILE

	// BOF DEFINE DUTY FIELDS
		$arrFormDutyFields = array(
			// "editCustomersKundennummer" => "Kundennummer",
			"editCustomersFirmenname" => "Firmenname",
			"editCustomersTyp" => "Kunden-Typ",
			"editCustomersFirmenadresseStrasse" => "Firmenanschrift-Straße",
			"editCustomersFirmenadresseHausnummer" => "Firmenanschrift-Hausnummer",
			"editCustomersFirmenadressePLZ" => "Firmenanschrift-PLZ",
			"editCustomersFirmenadresseOrt" => "Firmenanschrift-Ort",
			// "editCustomersGruppe[]" => "Kunden-Gruppe"
			// "editCustomersGruppe" => "Kunden-Gruppe"
		);
		// CREATE JSON DUTY FIELD
		$jsonFormDutyFields = createJson($arrFormDutyFields);
	// BOF DEFINE DUTY FIELDS

	if($_REQUEST["editID"] == "")  {
		// $_REQUEST["editID"] = "NEW";
		// header("location: displayOrders.php");
		// exit;
	}

	if($_REQUEST["tabID"] == "")  {
		$_REQUEST["tabID"] = "tabs-1";
	}

	// BOF READ VERTRETER
		$arrAgentDatas = getAgents();
		$arrAgentDatas2 = getAgents2();
	// EOF READ VERTRETER

	// BOF READ SALUTATIONS
		$arrSalutationTypeDatas = getSalutationTypes();
	// EOF READ SALUTATIONS

	// BOF READ PAYMENT TYPES
		$arrPaymentTypeDatas = getPaymentTypes();
	// EOF READ PAYMENT TYPES

	// BOF READ PAYMENT CONDITIONS
		$arrPaymentConditionDatas = getPaymentConditions();
	// EOF READ PAYMENT CONDITIONS

	// BOF READ BESTELL ARTEN
		$arrOrderTypeDatas = getOrderTypes();
	// EOF READ BESTELL ARTEN

	// BOF READ ADDITIONAL COSTS
		$arrAdditionalCostsDatas = getAdditionalCosts();
	// EOF READ ADDITIONAL COSTS

	// BOF READ COUNTRIES
		$arrCountryTypeDatas = getCountryTypes();
	// EOF READ COUNTRIES

	// BOF READ CUSTOMER TYPES
		$arrCustomerTypeDatas = getCustomerTypes();
	// EOF READ CUSTOMER TYPES

	// BOF READ DOCUMENT TYPE DATAS
		$arrDocumentTypeDatas = getDocumentTypes();
	// EOF READ DOCUMENT TYPE DATAS

	// BOF READ DOCUMENT TYPE DATAS
		$arrPaymentStatusTypeDatas = getPaymentStatusTypes();
	// EOF READ DOCUMENT TYPE DATAS

	// BOF READ CUSTOMER GROUP DATAS
		$arrCustomerGroupDatas = getCustomerGroups();
	// EOF READ CUSTOMER GROUP DATAS

	// BOF READ BESTELL STATUS TYPEN
		$arrOrderStatusTypeDatas = getOrderStatusTypes();
	// EOF READ BESTELL STATUS TYPEN

	// BOF READ ORDER SOURCE TYPES
		$arrOrdersSourceTypeDatas = getOrdersSourceTypes();
	// EOF READ ORDER SOURCE TYPES

	// BOF GET SALESMAN TO ZIPCODE
		$arrRelationSalesmenZipcodeDatas = getRelationSalesmenZipcode();
	// EOF GET SALESMAN TO ZIPCODE

	// BOF READ CONTACT TYPE DATAS
		$arrCustomerContactTypeDatas = getContactTypes();
	// EOF READ CONTACT TYPE DATAS


	// BOF SEND MAIL WITH ATTACHED FILE
	if($_POST["sendAttachedDocument"] == '1') {
		$thisCreatedPdfName = $_POST["mailDocumentFilename"];
		$_POST["selectSubject"] = $arrDocumentTypeDatas[$_REQUEST["documentType"]]["createdDocumentsTypesName"] . ' Nr.: ' . '{###DOCUMENT_NUMBER###}';
		$generatedDocumentNumber = substr($_POST["mailDocumentFilename"], 0, 13);

		require_once(DIRECTORY_HTML_MIME_MAIL . 'htmlMimeMail.php');
		require_once(DIRECTORY_MAIL_TEMPLATES . 'template_BASIC.inc.php');
		require_once(DIRECTORY_MAIL_TEMPLATES . 'template_TEXT_DATAS.inc.php');
		##require_once("inc/mail.inc.php");

		// BOF SEND MAIL
		require_once("classes/createMail.class.php");

		// BOF CREATE SUBJECT
		$thisSubject = $_POST["selectSubject"];
		if($_POST["sendAttachedMailSubject"] != '') {
			$thisSubject = $_POST["sendAttachedMailSubject"] . ', ' . $thisSubject;
		}
		// EOF CREATE SUBJECT

		// BOF GET ATTACHED FILE
		if($_REQUEST["documentType"] == 'KA') {
			$pathDocumentFolder = DIRECTORY_UPLOAD_FILES;
			$thisCreatedPdfName = utf8_decode($thisCreatedPdfName);
		}
		else if($_REQUEST["documentType"] == 'EX') {
			$pathDocumentFolder = DIRECTORY_EXPORT_FILES;
			$thisCreatedPdfName = utf8_decode($thisCreatedPdfName);
		}
		else if($_REQUEST["documentType"] == 'SX') {
			$pathDocumentFolder = DIRECTORY_CREATED_DOCUMENTS_SALESMEN;
			$thisCreatedPdfName = utf8_decode($thisCreatedPdfName);
		}
		else {
			$pathDocumentFolder = DIRECTORY_CREATED_DOCUMENTS_CUSTOMERS;
		}

		$arrAttachmentFiles = array (
			rawurldecode($pathDocumentFolder . $thisCreatedPdfName) => str_replace(" ", "_", rawurldecode(myUnHtmlEntities($thisCreatedPdfName)))
		);
		// EOF GET ATTACHED FILE

		$createMail = new createMail(
							$thisSubject,													// TEXT:	MAIL_SUBJECT
							$_POST["documentType"],											// STRING:	DOCUMENT TYPE
							$generatedDocumentNumber,										// STRING:	DOCUMENT NUMBER
							$arrAttachmentFiles,											// ARRAY:	ATTACHMENT FILES
							$_POST["selectCustomersRecipientMail"],	 										// STRING:	RECIPIENTS
							$arrMailContentDatas[$_POST["selectMailtextTemplates"]],		// MAIL_TEXT_TEMPLATE
							$mailTemplate["html"],											// MAIL_BODY_TEMPLATE
							'',																// STRING:	ADDITIONAL TEXT
							$_POST["sendAttachedMailText"],									// STRING:	SEND ATTACHED TEXT
							true,															// BOOLEAN:	SEND ATTACHED DOCUMENT
							DIRECTORY_MAIL_IMAGES,											// DIRECTORY_MAIL_IMAGES
							$_POST["selectMailtextSender"]									// STRING: SENDER
						);

		$createMailResult = $createMail->returnResult();
		$sendMailToClient = $createMailResult;


		#if(!$sendMailToUs) { $errorMessage .= ' Die Mail-Kopie konnte nicht an ' . implode(', ', $arrOrderMail) . ' versendet werden! '.'<br />'; }
		#else { $successMessage .= ' Die Mail-Kopie wurde an ' . implode(', ', $arrOrderMail) . ' versendet! '.'<br />'; }

		if(!$sendMailToClient) { $errorMessage .= ' Die Mail konnte nicht an ' . $_POST["selectCustomersRecipientName"] . ' (' . $_POST["selectCustomersRecipientMail"] . ') versendet werden! '.'<br />'; }
		else {
			$successMessage .= ' Die Mail wurde an ' . $_POST["selectCustomersRecipientName"] . ' (' . $_POST["selectCustomersRecipientMail"] . ') versendet! '.'<br />';
		}
	}
	// EOF SEND MAIL WITH ATTACHED FILE

	// BOF STORE FILES
	if($_POST["storeFiles"] != "") {
		// BOF STORE FILES
		if(!empty($_FILES)) {

			$sql_insert = array();
			$fileIsEmpty = true;

			$countFiles++;

			for($i = 0 ; $i < count($_FILES["editCustomersPrintFile"]["name"]) ; $i++) {

				if($_FILES["editCustomersPrintFile"]["name"][$i] != "") {
					$thisFileTime = filemtime(stripslashes($_FILES["editCustomersPrintFile"]["tmp_name"][$i]));
					#$thisDocumentNumber = "KA-" . date("ym", $thisFileTime) . str_repeat("0", (6 - strlen($countFiles))) . $countFiles;
					$thisDocumentNumber = "KA-" . date('ymdHis');
					$sql_insert[] = "(
						'%',
						'KA',
						'" . $thisDocumentNumber . "',
						'".$_POST["editCustomersKundennummer2"]."',
						'".basename($_FILES["editCustomersPrintFile"]["name"][$i])."',
						'".DIRECTORY_UPLOAD_FILES . $_FILES["editCustomersPrintFile"]["name"][$i]."',
						'-',
						'".$_SESSION["usersID"]."',
						'".date("Y-m-d H:i:s", filemtime($_FILES["editCustomersPrintFile"]["tmp_name"][$i]))."'
					)";

					$thisDeleteFile = utf8_decode(urldecode($_FILES["editCustomersPrintFile"]["name"][$i]));

					if(file_exists(DIRECTORY_UPLOAD_FILES . ($thisDeleteFile))){
						$infoMessage .= ' Es existiert eine Datei mit diesem Namen. Diese wird &uuml;berschrieben. ' .'<br />';
						$resultUnlink = unlink(DIRECTORY_UPLOAD_FILES . $thisDeleteFile);
						if($resultUnlink) {
							$successMessage .= 'Die existierende Datei &quot;' . $fileDirectory . utf8_encode($thisDeleteFile) . '&quot; wurde entfernt.' . '<br />';
						}
						else {
							$errorMessage .= 'Die existierende Datei &quot;' . $fileDirectory . utf8_encode($thisDeleteFile) . '&quot; konnte leider nicht entfernt werden.' . '<br />';
						}
						$thisDeleteFile = ($_FILES["editCustomersPrintFile"]["name"][$i]);
						$sql = "DELETE
									FROM `" . TABLE_CREATED_DOCUMENTS ."`
									WHERE `createdDocumentsFilename` = '" . DIRECTORY_UPLOAD_FILES . $thisDeleteFile . "'
						";
						$rs = $dbConnection->db_query($sql);
						if($rs){
							$successMessage .= 'Die schon existierende Datei &quot;' . DIRECTORY_UPLOAD_FILES . $thisDeleteFile . '&quot; wurde aus der Datenbank entfernt.' . '<br />';
						}
						else {
							$errorMessage .= 'Die Datei existierende &quot;' . DIRECTORY_UPLOAD_FILES . $thisDeleteFile . '&quot; konnte leider nicht aus der Datenbank entfernt werden.' . '<br />';
						}
					}
					if(copy($_FILES["editCustomersPrintFile"]["tmp_name"][$i], DIRECTORY_UPLOAD_FILES . utf8_decode($_FILES["editCustomersPrintFile"]["name"][$i]))) {
						$successMessage .= ' Die hochgeladenene Datei &quot;' . DIRECTORY_UPLOAD_FILES . utf8_encode($thisDeleteFile) . '&quot; wurde ins Auftragslisten-Verzeichnis kopiert. ' .'<br />';
					}
					else {
						$errorMessage .= ' Die hochgeladenene Datei &quot;' . DIRECTORY_UPLOAD_FILES . utf8_encode($thisDeleteFile) . '&quot; konnte nicht ins Auftragslisten-Verzeichnis kopiert werden. ' .'<br />';
					}
					$countFiles++;
					$fileIsEmpty = false;
				}
				else {

				}
			}
			if(!empty($sql_insert)) {
				$sql = "INSERT INTO `" . TABLE_CREATED_DOCUMENTS ."` (
									`createdDocumentsID`,
									`createdDocumentsType`,
									`createdDocumentsNumber`,
									`createdDocumentsCustomerNumber`,
									`createdDocumentsTitle`,
									`createdDocumentsFilename`,
									`createdDocumentsContent`,
									`createdDocumentsUserID`,
									`createdDocumentsTimeCreated`
								)
								VALUES " .	implode(',', $sql_insert);
				$rs = $dbConnection->db_query($sql);

				if($rs) {
					$successMessage .= ' Die Datei wurde gespeichert. ' .'<br />';
				}
				else {
					$errorMessage .= ' Die Datei konnte nicht gespeichert werden. ' . '<br />';
				}
			}
		}
		if($fileIsEmpty) {
			$errorMessage .= ' Sie haben keine Dateien ausgew&auml;hlt. ' . '<br />';
		}
		// EOF STORE FILES
		#$_REQUEST["editID"] = $_POST["editCustomersKundennummer2"];
		$_REQUEST["editID"] = $_POST["editID"];
	}
	// BOF STORE FILES

	// BOF STORE DATAS
	if($_POST["storeDatas"] != "") {

		$doAction = checkFormDutyFields($arrFormDutyFields);

		// BOF CHECK NAME FIELDS
		$arrCheckSum = array();
		// if($_POST["editCustomersFirmenInhaberVorname"] != "" || $_POST["editCustomersFirmenInhaberNachname"] != "" || $_POST["editCustomersFirmenInhaberAnrede"] != "") {
		if($_POST["editCustomersFirmenInhaberVorname"] != "" || $_POST["editCustomersFirmenInhaberNachname"] != "" || $_POST["editCustomersFirmenInhaberAnrede"] != "") {
			#$arrCheckSum["FirmenInhaber"][] = preg_replace("/^[ A-Za-zÄÖÜäüößáàéè'\.\-]{1,}$/", 1, $_POST["editCustomersFirmenInhaberVorname"]);
			$arrCheckSum["FirmenInhaber"][] = preg_replace("/^[ A-Za-zÄÖÜäüößáàéè'\.\-]{1,}$/", 1, $_POST["editCustomersFirmenInhaberNachname"]);
			$arrCheckSum["FirmenInhaber"][] = preg_replace("/[0-9]{1,}/", 1, $_POST["editCustomersFirmenInhaberAnrede"]);
		}

		if($_POST["editCustomersFirmenInhaber2Vorname"] != "" || $_POST["editCustomersFirmenInhaber2Nachname"] != "" || $_POST["editCustomersFirmenInhaber2Anrede"] != "") {
			#$arrCheckSum["FirmenInhaber2"][] = preg_replace("/^[ A-Za-zÄÖÜäüößáàéè'\.\-]{1,}$/", 1, $_POST["editCustomersFirmenInhaber2Vorname"]);
			$arrCheckSum["FirmenInhaber2"][] = preg_replace("/^[ A-Za-zÄÖÜäüößáàéè'\.\-]{1,}$/", 1, $_POST["editCustomersFirmenInhaber2Nachname"]);
			$arrCheckSum["FirmenInhaber2"][] = preg_replace("/[0-9]{1,}/", 1, $_POST["editCustomersFirmenInhaber2Anrede"]);
		}

		if($_POST["editCustomersAnsprechpartner1Vorname"] != "" || $_POST["editCustomersAnsprechpartner1Nachname"] != "" || $_POST["editCustomersAnsprechpartner1Anrede"] != "") {
			#$arrCheckSum["Ansprechpartner1"][] = preg_replace("/^[ A-Za-zÄÖÜäüößáàéè'\.\-]{1,}$/", 1, $_POST["editCustomersAnsprechpartner1Vorname"]);
			$arrCheckSum["Ansprechpartner1"][] = preg_replace("/^[ A-Za-zÄÖÜäüößáàéè'\.\-]{1,}$/", 1, $_POST["editCustomersAnsprechpartner1Nachname"]);
			$arrCheckSum["Ansprechpartner1"][] = preg_replace("/[0-9]{1,}/", 1, $_POST["editCustomersAnsprechpartner1Anrede"]);
		}

		if($_POST["editCustomersAnsprechpartner2Vorname"] != "" || $_POST["editCustomersAnsprechpartner2Nachname"] != "" || $_POST["editCustomersAnsprechpartner2Anrede"] != "") {
			#$arrCheckSum["Ansprechpartner2"][] = preg_replace("/^[ A-Za-zÄÖÜäüößáàéè'\.\-]{1,}$/", 1, $_POST["editCustomersAnsprechpartner2Vorname"]);
			$arrCheckSum["Ansprechpartner2"][] = preg_replace("/^[ A-Za-zÄÖÜäüößáàéè'\.\-]{1,}$/", 1, $_POST["editCustomersAnsprechpartner2Nachname"]);
			$arrCheckSum["Ansprechpartner2"][] = preg_replace("/[0-9]{1,}/", 1, $_POST["editCustomersAnsprechpartner2Anrede"]);
		}

		$isCheckSumOk = true;
		if(!empty($arrCheckSum)) {
			foreach($arrCheckSum as $thisKey => $thisValue) {
				if(array_sum($thisValue)/2 != 1) {
					$isCheckSumOk = false;
				}
				// echo 'check_sum: ' . array_sum($thisValue)%3 . '<br />';
			}
		}
		// EOF CHECK NAME FIELDS


		// BOF CHECK MAIL ADDRESS
		$checkCustomerEmail = true;
		if(trim($_POST["editCustomersMail1"] != '')) {
			$checkCustomerEmail = checkEmailStructure($_POST["editCustomersMail1"]);
		}
		if(trim($_POST["editCustomersMail2"] != '')) {
			$checkCustomerEmail = checkEmailStructure($_POST["editCustomersMail2"]);
		}
		// EOF CHECK MAIL ADDRESS

		if($doAction && $isCheckSumOk && $checkCustomerEmail) {

			if($_POST["Rechnungsadresse_Firmenadresse"] == '1'){
				$_POST["editCustomersRechnungsadresseFirmenname"] = $_POST["editCustomersFirmenname"];
				$_POST["editCustomersRechnungsadresseFirmennameZusatz"] = $_POST["editCustomersFirmennameZusatz"];
				$_POST["editCustomersRechnungsadresseStrasse"] = $_POST["editCustomersCompanyStrasse"];
				$_POST["editCustomersRechnungsadresseHausnummer"] = $_POST["editCustomersCompanyHausnummer"];
				$_POST["editCustomersRechnungsadressePLZ"] = $_POST["editCustomersCompanyPLZ"];
				$_POST["editCustomersRechnungsadresseOrt"] = $_POST["editCustomersCompanyOrt"];
				$_POST["editCustomersRechnungsadresseLand"] = $_POST["editCustomersCompanyCountry"];
			}
			if($_POST["Lieferadresse_Firmenadresse"] == '1'){
				$_POST["editCustomersLieferadresseFirmenname"] = $_POST["editCustomersFirmenname"];
				$_POST["editCustomersLieferadresseFirmennameZusatz"] = $_POST["editCustomersFirmennameZusatz"];
				$_POST["editCustomersLieferadresseStrasse"] = $_POST["editCustomersCompanyStrasse"];
				$_POST["editCustomersLieferadresseHausnummer"] = $_POST["editCustomersCompanyHausnummer"];
				$_POST["editCustomersLieferadressePLZ"] = $_POST["editCustomersCompanyPLZ"];
				$_POST["editCustomersLieferadresseOrt"] = $_POST["editCustomersCompanyOrt"];
				$_POST["editCustomersLieferadresseLand"] = $_POST["editCustomersCompanyCountry"];
			}

			$thisEditCustomersKundennummer = $_POST["editCustomersKundennummer"];
			$generatedCustomerNumber = "";
			if($_POST["editCustomersID"] == "") {
				$_POST["editCustomersID"] = "%";
				// BOF CREATE COSTUMER NUMBER
				if(strlen($_POST["editCustomersKundennummer"]) < LENGTH_CUSTOMER_NUMBER) {
					$generatedCustomerNumber = generateCustomerNumber('customers', $_POST["editCustomersTyp"], $_POST["editCustomersCompanyCountry"], $_POST["editCustomersCompanyPLZ"]);

					$jswindowMessage .= 'Die generierte Kundennummer für ' . $_POST["editCustomersFirmenname"] . ' lautet: ' . $generatedCustomerNumber;

					$thisEditCustomersKundennummer = $generatedCustomerNumber;
				}
				// EOF CREATE COSTUMER NUMBER
				$sqlInsertType = "INSERT";
			}
			else {
				$sqlInsertType = "REPLACE";
			}

			if(trim($_POST["editCustomersVertreterName"]) == "") {
				$_POST["editCustomersVertreterID"] = "";
			}

			// BOF GET CUSTOMER TAX ACCOUNT NUMBER
				if($_POST["customersTaxAccountID"] == '') {
					$customersTaxAccountID = getNewCustomerTaxAccountNumber($_POST["editCustomersCompanyCountry"], $arrCountryTypeDatas[$_POST["editCustomersCompanyCountry"]]["countries_iso_code_2"], $_POST["editCustomersTyp"]);
				}
				else {
					$customersTaxAccountID = $_POST["customersTaxAccountID"];
				}
			// EOF GET CUSTOMER TAX ACCOUNT NUMBER

			if($_POST["editCustomersBadPaymentBehavior"] == '') { $_POST["editCustomersBadPaymentBehavior"] = '0';}

			$thisCustomersHomepage = trim($_POST["editCustomersHomepage"]);
			if($thisCustomersHomepage == ""){
				if(preg_match("/^info@/", $_POST["editCustomersMail1"])){
					$thisCustomersHomepage = str_replace("info@", "www.", $_POST["editCustomersMail1"]);
				}
				else if(preg_match("/^info@/", $_POST["editCustomersMail2"])){
					$thisCustomersHomepage = str_replace("info@", "www.", $_POST["editCustomersMail2"]);
				}
			}

			$sql = "
				" . $sqlInsertType . " INTO `" . TABLE_CUSTOMERS . "`(
					`customersID`,
					`customersKundennummer`,
					`customersFirmenname`,
					`customersFirmennameZusatz`,
					`customersFirmenInhaberVorname`,
					`customersFirmenInhaberNachname`,
					`customersFirmenInhaberAnrede`,
					`customersFirmenInhaber2Vorname`,
					`customersFirmenInhaber2Nachname`,
					`customersFirmenInhaber2Anrede`,
					`customersAnsprechpartner1Vorname`,
					`customersAnsprechpartner1Nachname`,
					`customersAnsprechpartner1Anrede`,
					`customersAnsprechpartner2Vorname`,
					`customersAnsprechpartner2Nachname`,
					`customersAnsprechpartner2Anrede`,
					`customersTelefon1`,
					`customersTelefon2`,
					`customersMobil1`,
					`customersMobil2`,
					`customersFax1`,
					`customersFax2`,
					`customersMail1`,
					`customersMail2`,
					`customersHomepage`,
					`customersCompanyStrasse`,
					`customersCompanyHausnummer`,
					`customersCompanyCountry`,
					`customersCompanyPLZ`,
					`customersCompanyOrt`,
					`customersLieferadresseFirmenname`,
					`customersLieferadresseFirmennameZusatz`,
					`customersLieferadresseStrasse`,
					`customersLieferadresseHausnummer`,
					`customersLieferadressePLZ`,
					`customersLieferadresseOrt`,
					`customersLieferadresseLand`,
					`customersRechnungsadresseFirmenname`,
					`customersRechnungsadresseFirmennameZusatz`,
					`customersRechnungsadresseStrasse`,
					`customersRechnungsadresseHausnummer`,
					`customersRechnungsadressePLZ`,
					`customersRechnungsadresseOrt`,
					`customersRechnungsadresseLand`,
					`customersKontoinhaber`,
					`customersBankName`,
					`customersBankKontonummer`,
					`customersBankLeitzahl`,
					`customersBankIBAN`,
					`customersBankBIC`,
					`customersBezahlart`,
					`customersZahlungskondition`,
					`customersRabatt`,
					`customersRabattType`,
					`customersUseProductMwst`,
					`customersUseProductDiscount`,
					`customersUstID`,
					`customersVertreterID`,
					`customersVertreterName`,
					`customersUseSalesmanDeliveryAdress`,
					`customersUseSalesmanInvoiceAdress`,
					`customersTyp`,
					`customersGruppe`,
					`customersNotiz`,
					`customersActive`,
					`customersUserID`,
					`customersTimeCreated`,
					`customersDatasUpdated`,
					`customersTaxAccountID`,
					`customersInvoicesNotPurchased`,
					`customersBadPaymentBehavior`,
					`customersEntryDate`
				)
				VALUES (
					'".$_POST["editCustomersID"]."',
					'".$thisEditCustomersKundennummer."',
					'".addslashes($_POST["editCustomersFirmenname"])."',
					'".addslashes($_POST["editCustomersFirmennameZusatz"])."',
					'".addslashes($_POST["editCustomersFirmenInhaberVorname"])."',
					'".addslashes($_POST["editCustomersFirmenInhaberNachname"])."',
					'".$_POST["editCustomersFirmenInhaberAnrede"]."',
					'".addslashes($_POST["editCustomersFirmenInhaber2Vorname"])."',
					'".addslashes($_POST["editCustomersFirmenInhaber2Nachname"])."',
					'".$_POST["editCustomersFirmenInhaber2Anrede"]."',
					'".addslashes($_POST["editCustomersAnsprechpartner1Vorname"])."',
					'".addslashes($_POST["editCustomersAnsprechpartner1Nachname"])."',
					'".$_POST["editCustomersAnsprechpartner1Anrede"]."',
					'".addslashes($_POST["editCustomersAnsprechpartner2Vorname"])."',
					'".addslashes($_POST["editCustomersAnsprechpartner2Nachname"])."',
					'".$_POST["editCustomersAnsprechpartner2Anrede"]."',
					'".cleanPhoneNumbers($_POST["editCustomersTelefon1"], $_POST["editCustomersCompanyCountry"])."',
					'".cleanPhoneNumbers($_POST["editCustomersTelefon2"], $_POST["editCustomersCompanyCountry"])."',
					'".cleanPhoneNumbers($_POST["editCustomersMobil1"], $_POST["editCustomersCompanyCountry"])."',
					'".cleanPhoneNumbers($_POST["editCustomersMobil2"], $_POST["editCustomersCompanyCountry"])."',
					'".cleanPhoneNumbers($_POST["editCustomersFax1"], $_POST["editCustomersCompanyCountry"])."',
					'".cleanPhoneNumbers($_POST["editCustomersFax2"], $_POST["editCustomersCompanyCountry"])."',
					'".($_POST["editCustomersMail1"])."',
					'".($_POST["editCustomersMail2"])."',
					'".preg_replace("/http:\/\//", "", $thisCustomersHomepage)."',
					'".addslashes(formatStreet($_POST["editCustomersCompanyStrasse"]))."',
					'".($_POST["editCustomersCompanyHausnummer"])."',
					'".($_POST["editCustomersCompanyCountry"])."',
					'".($_POST["editCustomersCompanyPLZ"])."',
					'".addslashes($_POST["editCustomersCompanyOrt"])."',
					'".addslashes($_POST["editCustomersLieferadresseFirmenname"])."',
					'".addslashes($_POST["editCustomersLieferadresseFirmennameZusatz"])."',
					'".addslashes(formatStreet($_POST["editCustomersLieferadresseStrasse"]))."',
					'".($_POST["editCustomersLieferadresseHausnummer"])."',
					'".($_POST["editCustomersLieferadressePLZ"])."',
					'".addslashes($_POST["editCustomersLieferadresseOrt"])."',
					'".$_POST["editCustomersLieferadresseLand"]."',
					'".addslashes($_POST["editCustomersRechnungsadresseFirmenname"])."',
					'".addslashes($_POST["editCustomersRechnungsadresseFirmennameZusatz"])."',
					'".addslashes(formatStreet($_POST["editCustomersRechnungsadresseStrasse"]))."',
					'".($_POST["editCustomersRechnungsadresseHausnummer"])."',
					'".($_POST["editCustomersRechnungsadressePLZ"])."',
					'".addslashes($_POST["editCustomersRechnungsadresseOrt"])."',
					'".$_POST["editCustomersRechnungsadresseLand"]."',
					'".addslashes($_POST["editCustomersKontoinhaber"])."',
					'".addslashes($_POST["editCustomersBankName"])."',
					'".cleanChars($_POST["editCustomersBankKontonummer"])."',
					'".cleanChars($_POST["editCustomersBankLeitzahl"])."',
					'".cleanChars($_POST["editCustomersBankIBAN"])."',
					'".cleanChars($_POST["editCustomersBankBIC"])."',
					'".$_POST["editCustomersBezahlart"]."',
					'".$_POST["editCustomersZahlungskondition"]."',
					'".str_replace(",", ".", $_POST["editCustomersRabatt"])."',
					'".$_POST["editCustomersRabattType"]."',
					'".$_POST["editCustomersUseProductMwst"]."',
					'".$_POST["editCustomersUseProductDiscount"]."',
					'".preg_replace("/[ \-\.\/_]/", "", $_POST["editCustomersUstID"])."',
					'".$_POST["editCustomersVertreterID"]."',
					'".$_POST["editCustomersVertreterName"]."',
					'".$_POST["editCustomersUseSalesmanDeliveryAdress"]."',
					'".$_POST["editCustomersUseSalesmanInvoiceAdress"]."',
					'".$_POST["editCustomersTyp"]."',
					'".implode(';', $_POST["editCustomersGruppe"])."',
					'".addslashes($_POST["editCustomersNotiz"])."',
					'".$_POST["editCustomersActive"]."',
					'".$_SESSION["usersID"]."',
					NOW(),
					'1',
					'".($customersTaxAccountID)."',
					'".$_POST["editCustomersInvoicesNotPurchased"]."',
					'".$_POST["editCustomersBadPaymentBehavior"]."',
					'".formatDate($_POST["editCustomersEntryDate"], 'store')."'
				)
			";

			$rs = $dbConnection->db_query($sql);
			$getInsertID = $dbConnection->db_getInsertID();

			if($rs) {
				$successMessage .= ' Der Kunden-Datensatz wurde gespeichert. ' .'<br />';

				// BOF STORE COMPANY RELATIONS
					if($_POST["editCustomersParentCompanyID"] != '' && $_POST["editCustomersParentCompanyNumber"] != ''){
						$sql = "
								REPLACE INTO `" . TABLE_CUSTOMERS_RELATIONS . "` (
									`customersRelationsParentCustomerID`,
									`customersRelationsParentCustomerNumber`,
									`customersRelationsChildCustomerID`,
									`customersRelationsChildCustomerNumber`
								)
								VALUES (
									'" . $_POST["editCustomersParentCompanyID"]. "',
									'" . $_POST["editCustomersParentCompanyNumber"]. "',
									'" . $_POST["editCustomersID"]. "',
									'" . $_POST["editCustomersKundennummer"]. "'
								)
							";
						$rs = $dbConnection->db_query($sql);
						if($rs) {
							$successMessage .= ' Die Kunden-Relation wurde gespeichert. ' .'<br />';
						}
						else {
							$errorMessage .= ' Die Kunden-Relation konnte nicht gespeichert werden. ' .'<br />';
						}
					}
					else if(($_POST["editCustomersParentCompanyNumber"]) == ''){
						$sql = "
								DELETE FROM `" . TABLE_CUSTOMERS_RELATIONS . "`
									WHERE 1
										AND `customersRelationsChildCustomerNumber` = '" . $_POST["editCustomersKundennummer"]. "'
							";
						$rs = $dbConnection->db_query($sql);
						if($rs) {
							#$successMessage .= ' Die Kunden-Relation wurde entfernt. ' .'<br />';
						}
						else {
							$errorMessage .= ' Die Kunden-Relation konnte nicht entfernt werden. ' .'<br />';
						}
					}
				// BOF STORE COMPANY RELATIONS
			}
			else {
				$errorMessage .= ' Der Kunden-Datensatz konnte nicht gespeichert werden. ' .'<br />';
			}

			// $_REQUEST["editID"] = $thisEditCustomersKundennummer;
			if($sqlInsertType == "INSERT") { $_REQUEST["editID"] = $getInsertID; }
			else { $_REQUEST["editID"] = $_POST["editCustomersID"]; }
		}
		else {
			$_REQUEST["editID"] = $_POST["editCustomersID"];
			if($_POST["editCustomersID"] == "") {
				$_REQUEST["editID"] = 'NEW';
			}

			if(!$doAction) {
				$errorMessage .= ' Der Datensatz konnte nicht gespeichert werden. Bitte f&uuml;llen Sie alle Pflichtfelder aus. ' .'<br />';
				$jswindowMessage .= ' Der Datensatz konnte nicht gespeichert werden. Bitte f&uuml;llen Sie alle Pflichtfelder aus. ' .'<br />';
			}
			if(!$isCheckSumOk) {
				$errorMessage .= ' Der Datensatz konnte nicht gespeichert werden. Bitte geben Sie die Namen korrekt ein. ' .'<br />';
				$jswindowMessage .= ' Der Datensatz konnte nicht gespeichert werden. Bitte geben Sie die Namen korrekt ein. ' .'<br />';
			}
			if(!$checkCustomerEmail) {
				$errorMessage .= ' Die eingegebene Mail-Adresse ist nicht korrekt.' . '<br />';
			}
		}
	}
	// EOF STORE DATAS

	// BOF DELETE DATAS
		if($_POST["deleteDatas"] != "") {
			$sql = "DELETE FROM `" . TABLE_CUSTOMERS . "` WHERE `customersID` = '".$_POST["editCustomersID"]."'";

			$rs = $dbConnection->db_query($sql);

			if($rs) {
				$successMessage .= ' Der Kunden-Datensatz wurde endgültig gelöscht. '.'<br />';
				$sql = "
						DELETE FROM `" . TABLE_CUSTOMERS_RELATIONS . "`
							WHERE 1
								AND `customersRelationsChildCustomerNumber` = '" . $_POST["editCustomersKundennummer"]. "'
					";
				$rs = $dbConnection->db_query($sql);
				if($rs) {
					#$successMessage .= ' Die Kunden-Relation wurde entfernt. ' .'<br />';
				}
				else {
					$errorMessage .= ' Die Kunden-Relation konnte nicht entfernt werden. ' .'<br />';
				}
			}
			else {
				$errorMessage .= ' Der Kunden-Datensatz konnte nicht gelöscht werden. '.'<br />';
			}
		}
	// EOF DELETE DATAS

	// BOF STORE CALENDAR DATAS
		if($_POST["storeCalendar"] != "" && !empty($_POST["addCustomerCalendarDates"])){
			if($_POST["addCustomerCalendarDates"][0]["DESCRIPTION"] != '' && $_POST["addCustomerCalendarDates"][0]["DATE"] != ''){
				$sql = "
					INSERT INTO `" . TABLE_CUSTOMERS_CALENDAR . "` (
												`customersCalendarID`,
												`customersCalendarCustomersID`,
												`customersCalendarCustomersNumber`,
												`customersCalendarDate`,
												`customersCalendarDateRepeat`,
												`customersCalendarRepeatType`,
												`customersCalendarRepeatValue`,
												`customersCalendarDescription`,
												`customersCalendarReminderMailSended`,
												`customersCalendarUserID`
											)
											VALUES (
												'%',
												'" . $_POST["addCustomerCalendarDates"][0]["CUSTOMERID"] . "',
												'" . $_POST["addCustomerCalendarDates"][0]["CUSTOMERNUMBER"] . "',
												'" . formatDate($_POST["addCustomerCalendarDates"][0]["DATE"], 'store') . "',
												'0',
												'none',
												'0',
												'" . addslashes($_POST["addCustomerCalendarDates"][0]["DESCRIPTION"]) . "',
												'0',
												'".$_SESSION["usersID"]."'
											)
					";

				$rs = $dbConnection->db_query($sql);

				if($rs){
					$successMessage .= 'Die Termin-Daten wurden gespeichert.' . '<br />';
					unset($_POST["addCustomerCalendarDates"]);
				}
				else {
					$errorMessage .= 'Die Termin-Daten konnten nicht gespeichert werden.' . '<br />';
				}
			}
			else {
				$errorMessage .= 'Sie haben die Termin-Daten nicht korrekt eingegeben.' . '<br />';
			}
		}
	// EOF STORE CALENDAR DATAS

	// BOF STORE CONTACT DATAS
	if($_POST["storeContacts"] != ""){
		if(!empty($_POST["editCustomerContacts"])){
			// BOF STORE EDITED CONTACT DATAS
			$arrSql = array();
			foreach($_POST["editCustomerContacts"] as $thisKey => $thisValue){
				$arrSql[] = "
							(
								'" . $_POST["editCustomerContacts"][$thisKey]["ID"] . "',
								'" . $_POST["editCustomerContacts"][$thisKey]["ContactID"] . "',
								'" . $_POST["editCustomerContacts"][$thisKey]["Salutation"] . "',
								'" . $_POST["editCustomerContacts"][$thisKey]["FirstName"] . "',
								'" . $_POST["editCustomerContacts"][$thisKey]["SecondName"] . "',
								'" . $_POST["editCustomerContacts"][$thisKey]["Function"] . "',
								'" . $_POST["editCustomerContacts"][$thisKey]["Phone"] . "',
								'" . $_POST["editCustomerContacts"][$thisKey]["Fax"] . "',
								'" . $_POST["editCustomerContacts"][$thisKey]["Mobile"] . "',
								'" . $_POST["editCustomerContacts"][$thisKey]["Mail"] . "',
								'1'
							)
					";
			}
			if(!empty($arrSql)){
				$sql = "
					REPLACE INTO `" . TABLE_CUSTOMERS_CONTACTS. "`(
										`customersContactsID`,
										`customersContactsContactID`,
										`customersContactsSalutation`,
										`customersContactsFirstName`,
										`customersContactsSecondName`,
										`customersContactsFunction`,
										`customersContactsPhone`,
										`customersContactsMobile`,
										`customersContactsFax`,
										`customersContactsMail`,
										`customersContactsActive`

									)
									VALUES
					";
				$sql .= implode(", ", $arrSql);
				$rs = $dbConnection->db_query($sql);

				if($rs){
					$successMessage .= 'Die Kontaktdaten wurden aktualisiert.' . '<br />';
				}
				else {
					$errorMessage .= 'Die Kontaktdaten konnten nicht aktualisiert werden.' . '<br />';
				}
			}
		}
		// EOF STORE EDITED CONTACT DATAS

		// BOF STORE NEW CONTACT DATAS
			$arrSql = array();
			foreach($_POST["addCustomerContacts"] as $thisKey => $thisValue){
				if($_POST["addCustomerContacts"][$thisKey]["SecondName"] != ""){
					$arrSql[] = "
							(
								'%',
								'" . $_POST["addCustomerContacts"][$thisKey]["ContactID"] . "',
								'" . $_POST["addCustomerContacts"][$thisKey]["Salutation"] . "',
								'" . $_POST["addCustomerContacts"][$thisKey]["FirstName"] . "',
								'" . $_POST["addCustomerContacts"][$thisKey]["SecondName"] . "',
								'" . $_POST["addCustomerContacts"][$thisKey]["Function"] . "',
								'" . $_POST["addCustomerContacts"][$thisKey]["Phone"] . "',
								'" . $_POST["addCustomerContacts"][$thisKey]["Fax"] . "',
								'" . $_POST["addCustomerContacts"][$thisKey]["Mobile"] . "',
								'" . $_POST["addCustomerContacts"][$thisKey]["Mail"] . "',
								'1'
							)
					";
				}
			}
			if(!empty($arrSql)){
				$sql = "
					INSERT INTO `" . TABLE_CUSTOMERS_CONTACTS. "`(
										`customersContactsID`,
										`customersContactsContactID`,
										`customersContactsSalutation`,
										`customersContactsFirstName`,
										`customersContactsSecondName`,
										`customersContactsFunction`,
										`customersContactsPhone`,
										`customersContactsMobile`,
										`customersContactsFax`,
										`customersContactsMail`,
										`customersContactsActive`
									)
									VALUES
					";
				$sql .= implode(", ", $arrSql);
				$rs = $dbConnection->db_query($sql);

				if($rs){
					$successMessage .= 'Die Kontaktdaten wurden gespeichert.' . '<br />';
				}
				else {
					$errorMessage .= 'Die Kontaktdaten konnten nicht gespeichert werden.' . '<br />';
				}
			}

		// EOF STORE NEW CONTACT DATAS
	}
	// EOF STORE CONTACT DATAS

	// BOF DELETE CONTACT DATAS
	if((int)$_REQUEST["deleteContactsID"] > 0){
		$sql = "DELETE FROM `" . TABLE_CUSTOMERS_CONTACTS. "` WHERE `customersContactsID` = '" . $_REQUEST["deleteContactsID"] . "'";

		$rs = $dbConnection->db_query($sql);

		if($rs){
			$successMessage .= 'Der Kontakt wurde entfernt.' . '<br />';
		}
		else {
			$errorMessage .= 'Der Kontakt konnte nicht entfernt werden.' . '<br />';
		}
	}
	// EOF DELETE CONTACT DATAS

	// BOF DELETE CALENDAR DATAS
	if((int)$_REQUEST["deleteCustomerCalendarID"] > 0){
		$sql = "DELETE FROM `" . TABLE_CUSTOMERS_CALENDAR. "` WHERE `customersCalendarID` = '" . $_REQUEST["deleteCustomerCalendarID"] . "'";

		$rs = $dbConnection->db_query($sql);

		if($rs){
			$successMessage .= 'Der Termin wurde entfernt.' . '<br />';
		}
		else {
			$errorMessage .= 'Der Termin konnte nicht entfernt werden.' . '<br />';
		}
	}
	// EOF DELETE CALENDAR DATAS

	// BOF READ ALL DATAS
		$where = "";

		if($_REQUEST["editCustomerNumber"] != "") {
			$where = " AND `customersKundennummer` = '" . $_REQUEST["editCustomerNumber"] . "' ";
			// $where = " AND `customersKundennummer` LIKE '" . $_REQUEST["editCustomerNumber"] . "%' ";
			$_REQUEST["editID"] = "";
		}
		else if($_REQUEST["searchCustomerNumber"] != "") {
			// $where = " AND `customersKundennummer` = '" . $_REQUEST["searchCustomerNumber"] . "' ";
			$where = " AND `customersKundennummer` LIKE '" . $_REQUEST["searchCustomerNumber"] . "%' ";
			$_REQUEST["editID"] = "";
		}
		else if($_REQUEST["searchCustomerName"] != "") {
			$where = " AND `customersFirmenname` LIKE '%" . $_REQUEST["searchCustomerName"] . "%' ";
			$_REQUEST["editID"] = "";
		}

		else if($_REQUEST["searchPLZ"] != "") {
			$where = " AND (
						`customersLieferadressePLZ` LIKE '" . $_REQUEST["searchPLZ"] . "%'
						OR
						`customersRechnungsadressePLZ` LIKE '" . $_REQUEST["searchPLZ"] . "%'
						OR
						`customersCompanyPLZ` LIKE '" . $_REQUEST["searchPLZ"] . "%'
				)
			";
			$_REQUEST["editID"] = "";
		}

		else if($_REQUEST["searchWord"] != "") {
			$where = " AND (
							`customersLieferadressePLZ` LIKE '" . $_REQUEST["searchWord"] . "%'
							OR
							`customersRechnungsadressePLZ` LIKE '" . $_REQUEST["searchWord"] . "%'
							OR
							`customersCompanyPLZ` LIKE '" . $_REQUEST["searchWord"] . "%'
							OR
							`customersLieferadresseOrt` LIKE '%" . $_REQUEST["searchWord"] . "%'
							OR
							`customersRechnungsadresseOrt` LIKE '%" . $_REQUEST["searchWord"] . "%'
							OR
							`customersCompanyOrt` LIKE '%" . $_REQUEST["searchWord"] . "%'
							OR
							`customersKundennummer` LIKE '" . $_REQUEST["searchWord"] . "%'
							OR
							`customersFirmenname` LIKE '%" . $_REQUEST["searchWord"] . "%'

							OR
							`customersFirmennameZusatz` LIKE '%" . $_REQUEST["searchWord"] . "%'

						) ";
			$_REQUEST["editID"] = "";
		}
		else if ($_GET["searchBoxCustomer"] != "") {
			$where = " AND (
							`customersKundennummer` LIKE '" . $_GET["searchBoxCustomer"] . "%'
							OR
							`customersFirmenname` LIKE '%" . $_GET["searchBoxCustomer"] . "%'
							OR
							`customersFirmennameZusatz` LIKE '%" . $_GET["searchBoxCustomer"] . "%'
							OR
							`customersCompanyPLZ` LIKE '" . $_GET["searchBoxCustomer"] . "%'


						) ";

			$_REQUEST["editID"] = "";
		}
		else if($_REQUEST["searchCustomerGroup"] != "") {
			$where = " AND (
					`customersGruppe` = '" . $_REQUEST["searchCustomerGroup"] . "'
					OR
					`customersGruppe` LIKE '" . $_REQUEST["searchCustomerGroup"] . ";%'
					OR
					`customersGruppe` LIKE '%;" . $_REQUEST["searchCustomerGroup"] . "'
					OR
					`customersGruppe` LIKE '%;" . $_REQUEST["searchCustomerGroup"] . ";%'
				)
			";
			if($_REQUEST["searchCustomerCountry"] != "") {
				$where .= " AND `customersCompanyCountry` = '" . $_GET["searchCustomerCountry"] . "'";
			}

			$_REQUEST["editID"] = "";
		}
		else if($_REQUEST["searchCustomerType"] != "") {
			$where = " AND (
					`customersTyp` = '" . $_REQUEST["searchCustomerType"] . "'
					OR
					`customersTyp` LIKE '" . $_REQUEST["searchCustomerType"] . ";%'
					OR
					`customersTyp` LIKE '%;" . $_REQUEST["searchCustomerType"] . "'
					OR
					`customersTyp` LIKE '%;" . $_REQUEST["searchCustomerType"] . ";%'
				)
			";
			$_REQUEST["editID"] = "";
		}

		if($_REQUEST["editID"] == "") {

			$sql = "SELECT
						`customersID`,
						`customersKundennummer`,
						`customersFirmenname`,
						`customersFirmennameZusatz`,
						`customersFirmenInhaberVorname`,
						`customersFirmenInhaberNachname`,
						`customersFirmenInhaberAnrede`,
						`customersFirmenInhaber2Vorname`,
						`customersFirmenInhaber2Nachname`,
						`customersFirmenInhaber2Anrede`,
						`customersAnsprechpartner1Vorname`,
						`customersAnsprechpartner1Nachname`,
						`customersAnsprechpartner1Anrede`,
						`customersAnsprechpartner2Vorname`,
						`customersAnsprechpartner2Nachname`,
						`customersAnsprechpartner2Anrede`,
						`customersTelefon1`,
						`customersTelefon2`,
						`customersMobil1`,
						`customersMobil2`,
						`customersFax1`,
						`customersFax2`,
						`customersMail1`,
						`customersMail2`,
						`customersHomepage`,
						`customersCompanyStrasse`,
						`customersCompanyHausnummer`,
						`customersCompanyCountry`,
						`customersCompanyPLZ`,
						`customersCompanyOrt`,
						`customersLieferadresseFirmenname`,
						`customersLieferadresseFirmennameZusatz`,
						`customersLieferadresseStrasse`,
						`customersLieferadresseHausnummer`,
						`customersLieferadressePLZ`,
						`customersLieferadresseOrt`,
						`customersLieferadresseLand`,
						`customersRechnungsadresseFirmenname`,
						`customersRechnungsadresseFirmennameZusatz`,
						`customersRechnungsadresseStrasse`,
						`customersRechnungsadresseHausnummer`,
						`customersRechnungsadressePLZ`,
						`customersRechnungsadresseOrt`,
						`customersRechnungsadresseLand`,
						`customersKontoinhaber`,
						`customersBankName`,
						`customersBankKontonummer`,
						`customersBankLeitzahl`,
						`customersBankIBAN`,
						`customersBankBIC`,
						`customersBezahlart`,
						`customersZahlungskondition`,
						`customersRabatt`,
						`customersRabattType`,
						`customersUseProductMwst`,
						`customersUseProductDiscount`,
						`customersUstID`,
						`customersVertreterID`,
						`customersVertreterName`,
						`customersUseSalesmanDeliveryAdress`,
						`customersUseSalesmanInvoiceAdress`,
						`customersTyp`,
						`customersGruppe`,
						`customersNotiz`,
						`customersActive`,
						`customersTaxAccountID`

						FROM `" . TABLE_CUSTOMERS . "`

						WHERE 1
					";
			$sql .= $where;

			$sql .= "
						ORDER BY `customersKundennummer`
			";
#dd('sql');

			$rs = $dbConnection->db_query($sql);
			$thisNumRows = $dbConnection->db_getMysqlNumRows($rs);

			if($thisNumRows == 1) {
				while($ds = mysqli_fetch_assoc($rs)){
					$_REQUEST["editID"] = $ds["customersID"];
				}
			}
			else if($thisNumRows > 1) {

				$pagesCount = ceil($thisNumRows / MAX_CUSTOMERS_PER_PAGE);

				if($pagesCount > 1) {
					if($_REQUEST["page"] == "" || $_REQUEST["page"] == 0) {
						$_REQUEST["page"] = 1;
					}
				}
				else {
					$_REQUEST["page"] = 1;
				}

				if(MAX_CUSTOMERS_PER_PAGE > 0) {
					$sql .= " LIMIT " . (($_REQUEST["page"] - 1) * MAX_CUSTOMERS_PER_PAGE) . ", " . MAX_CUSTOMERS_PER_PAGE." ";
				}

				$rs = $dbConnection->db_query($sql);

				$arrCustomerDatas = array();
				while($ds = mysqli_fetch_assoc($rs)) {
					foreach(array_keys($ds) as $field) {
						$arrCustomerDatas[$ds["customersID"]][$field] = $ds[$field];
					}
				}
			}
			else if($thisNumRows < 1) {
				$warningMessage .= ' Es wurden keine Daten gefunden!' . '<br />';
			}
		}
	// EOF READ ALL DATAS

	// BOF READ SELECTED DATAS
		if($_REQUEST["editID"] != "") {
			$sql = "SELECT
						`customersID`,
						`customersKundennummer`,
						`customersFirmenname`,
						`customersFirmennameZusatz`,
						`customersFirmenInhaberVorname`,
						`customersFirmenInhaberNachname`,
						`customersFirmenInhaberAnrede`,
						`customersFirmenInhaber2Vorname`,
						`customersFirmenInhaber2Nachname`,
						`customersFirmenInhaber2Anrede`,
						`customersAnsprechpartner1Vorname`,
						`customersAnsprechpartner1Nachname`,
						`customersAnsprechpartner1Anrede`,
						`customersAnsprechpartner2Vorname`,
						`customersAnsprechpartner2Nachname`,
						`customersAnsprechpartner2Anrede`,
						`customersTelefon1`,
						`customersTelefon2`,
						`customersMobil1`,
						`customersMobil2`,
						`customersFax1`,
						`customersFax2`,
						`customersMail1`,
						`customersMail2`,
						`customersHomepage`,
						`customersCompanyStrasse`,
						`customersCompanyHausnummer`,
						`customersCompanyCountry`,
						`customersCompanyPLZ`,
						`customersCompanyOrt`,
						`customersLieferadresseFirmenname`,
						`customersLieferadresseFirmennameZusatz`,
						`customersLieferadresseStrasse`,
						`customersLieferadresseHausnummer`,
						`customersLieferadressePLZ`,
						`customersLieferadresseOrt`,
						`customersLieferadresseLand`,
						`customersRechnungsadresseFirmenname`,
						`customersRechnungsadresseFirmennameZusatz`,
						`customersRechnungsadresseStrasse`,
						`customersRechnungsadresseHausnummer`,
						`customersRechnungsadressePLZ`,
						`customersRechnungsadresseOrt`,
						`customersRechnungsadresseLand`,
						`customersKontoinhaber`,
						`customersBankName`,
						`customersBankKontonummer`,
						`customersBankLeitzahl`,
						`customersBankIBAN`,
						`customersBankBIC`,
						`customersBezahlart`,
						`customersZahlungskondition`,
						`customersRabatt`,
						`customersRabattType`,
						`customersUseProductMwst`,
						`customersUseProductDiscount`,
						`customersUstID`,
						`customersVertreterID`,
						`customersVertreterName`,
						`customersUseSalesmanDeliveryAdress`,
						`customersUseSalesmanInvoiceAdress`,
						`customersTyp`,
						`customersGruppe`,
						`customersNotiz`,
						`customersActive`,
						`customersDatasUpdated`,
						`customersTaxAccountID`,

						`customersInvoicesNotPurchased`,
						`customersBadPaymentBehavior`,
						`customersEntryDate`

						FROM `" . TABLE_CUSTOMERS . "`

						WHERE `customersID` = '".$_REQUEST["editID"]."'
				";
						// WHERE `customersID` = '".$_REQUEST["editID"]."'
				$rs = $dbConnection->db_query($sql);

				$customerDatas = array();
				list(
					$customerDatas["customersID"],
					$customerDatas["customersKundennummer"],
					$customerDatas["customersFirmenname"],
					$customerDatas["customersFirmennameZusatz"],
					$customerDatas["customersFirmenInhaberVorname"],
					$customerDatas["customersFirmenInhaberNachname"],
					$customerDatas["customersFirmenInhaberAnrede"],
					$customerDatas["customersFirmenInhaber2Vorname"],
					$customerDatas["customersFirmenInhaber2Nachname"],
					$customerDatas["customersFirmenInhaber2Anrede"],
					$customerDatas["customersAnsprechpartner1Vorname"],
					$customerDatas["customersAnsprechpartner1Nachname"],
					$customerDatas["customersAnsprechpartner1Anrede"],
					$customerDatas["customersAnsprechpartner2Vorname"],
					$customerDatas["customersAnsprechpartner2Nachname"],
					$customerDatas["customersAnsprechpartner2Anrede"],
					$customerDatas["customersTelefon1"],
					$customerDatas["customersTelefon2"],
					$customerDatas["customersMobil1"],
					$customerDatas["customersMobil2"],
					$customerDatas["customersFax1"],
					$customerDatas["customersFax2"],
					$customerDatas["customersMail1"],
					$customerDatas["customersMail2"],
					$customerDatas["customersHomepage"],
					$customerDatas["customersCompanyStrasse"],
					$customerDatas["customersCompanyHausnummer"],
					$customerDatas["customersCompanyCountry"],
					$customerDatas["customersCompanyPLZ"],
					$customerDatas["customersCompanyOrt"],
					$customerDatas["customersLieferadresseFirmenname"],
					$customerDatas["customersLieferadresseFirmennameZusatz"],
					$customerDatas["customersLieferadresseStrasse"],
					$customerDatas["customersLieferadresseHausnummer"],
					$customerDatas["customersLieferadressePLZ"],
					$customerDatas["customersLieferadresseOrt"],
					$customerDatas["customersLieferadresseLand"],
					$customerDatas["customersRechnungsadresseFirmenname"],
					$customerDatas["customersRechnungsadresseFirmennameZusatz"],
					$customerDatas["customersRechnungsadresseStrasse"],
					$customerDatas["customersRechnungsadresseHausnummer"],
					$customerDatas["customersRechnungsadressePLZ"],
					$customerDatas["customersRechnungsadresseOrt"],
					$customerDatas["customersRechnungsadresseLand"],
					$customerDatas["customersKontoinhaber"],
					$customerDatas["customersBankName"],
					$customerDatas["customersBankKontonummer"],
					$customerDatas["customersBankLeitzahl"],
					$customerDatas["customersBankIBAN"],
					$customerDatas["customersBankBIC"],
					$customerDatas["customersBezahlart"],
					$customerDatas["customersZahlungskondition"],
					$customerDatas["customersRabatt"],
					$customerDatas["customersRabattType"],
					$customerDatas["customersUseProductMwst"],
					$customerDatas["customersUseProductDiscount"],
					$customerDatas["customersUstID"],
					$customerDatas["customersVertreterID"],
					$customerDatas["customersVertreterName"],
					$customerDatas["customersUseSalesmanDeliveryAdress"],
					$customerDatas["customersUseSalesmanInvoiceAdress"],
					$customerDatas["customersTyp"],
					$customerDatas["customersGruppe"],
					$customerDatas["customersNotiz"],
					$customerDatas["customersActive"],
					$customerDatas["customersDatasUpdated"],
					$customerDatas["customersTaxAccountID"],
					$customerDatas["customersInvoicesNotPurchased"],
					$customerDatas["customersBadPaymentBehavior"],
					$customerDatas["customersEntryDate"]

			) = mysqli_fetch_array($rs);
			if((!$doAction || !$isCheckSumOk || !$checkCustomerEmail) && $_POST["storeDatas"] != "") {
				foreach($_POST as $thisKey => $thisValue) {
					// $customerDatas["customersActive"] = $_POST["editCustomersActive"];
					$thisKeyNew = preg_replace("/editCustomers/", "customers", $thisKey);
					// echo $thisKey . ' | ' . $thisKeyNew . "<br />";
					$customerDatas[$thisKeyNew] = $_POST[$thisKey];
				}
			}

			// BOF CHECK IF CUSTOMER IS USED
				$arrSubSQL = array();
				foreach($arrTables as $thisTableKey => $thisTableValue){
					$arrSubSQL[] = "SELECT
								COUNT(`orderDocumentsCustomerNumber`) AS `checkCustomerNumber`
								FROM `" . $thisTableValue . "`
								WHERE `orderDocumentsCustomerNumber` = '" . $customerDatas["customersKundennummer"] . "'
						";
				}
				$sql = "
					SELECT
						SUM(`tempTable`.`checkCustomerNumber`) AS `checkCustomerNumberIsUsed`

						FROM (
						" . implode(' UNION ALL ', $arrSubSQL) . "
						) AS `tempTable`
					";

				$rs = $dbConnection->db_query($sql);
				list($checkCustomerNumberIsUsed) = mysqli_fetch_array($rs);
			// EOF OF CHECK IF CUSTOMER IS USED

			// BOF GET COMPANY RELATIONS - MOTHER COMPANY
				// 32007 -> 32008 TEST
				$sql = "
						SELECT
							`" . TABLE_CUSTOMERS_RELATIONS . "`.`customersRelationsParentCustomerID`,
							`" . TABLE_CUSTOMERS_RELATIONS . "`.`customersRelationsParentCustomerNumber`,

							`" . TABLE_CUSTOMERS . "`.`customersFirmenname`,
							`" . TABLE_CUSTOMERS . "`.`customersCompanyStrasse`,
							`" . TABLE_CUSTOMERS . "`.`customersCompanyHausnummer`,
							`" . TABLE_CUSTOMERS . "`.`customersCompanyPLZ`,
							`" . TABLE_CUSTOMERS . "`.`customersCompanyOrt`,
							`" . TABLE_CUSTOMERS . "`.`customersCompanyCountry`

							FROM `" . TABLE_CUSTOMERS_RELATIONS . "`
							LEFT JOIN `" . TABLE_CUSTOMERS . "`
							ON(`" . TABLE_CUSTOMERS_RELATIONS . "`.`customersRelationsParentCustomerID` = `" . TABLE_CUSTOMERS . "`.`customersID`)

							WHERE 1
								AND `customersRelationsChildCustomerID` = '" . $customerDatas["customersID"] . "'
								AND `customersRelationsChildCustomerNumber` = '" . $customerDatas["customersKundennummer"] . "'
					";
				$rs = $dbConnection->db_query($sql);
				if($dbConnection->db_getMysqlNumRows($rs) > 0){
					list(
						$arrCustomerParentCompanyDatas["parentCompanyID"],
						$arrCustomerParentCompanyDatas["parentCompanyNumber"],
						$arrCustomerParentCompanyDatas["parentCompanyName"],
						$arrCustomerParentCompanyDatas["parentCompanyStrasse"],
						$arrCustomerParentCompanyDatas["parentCompanyHausnummer"],
						$arrCustomerParentCompanyDatas["parentCompanyPLZ"],
						$arrCustomerParentCompanyDatas["parentCompanyOrt"],
						$arrCustomerParentCompanyDatas["parentCompanyCountry"]
					) = mysqli_fetch_array($rs);
				}

			// EOF GET COMPANY RELATIONS - MOTHER COMPANY


			// BOF GET COMPANY RELATIONS - CHILD COMPANIES
				$sql = "
						SELECT
							`" . TABLE_CUSTOMERS_RELATIONS . "`.`customersRelationsChildCustomerID`,
							`" . TABLE_CUSTOMERS_RELATIONS . "`.`customersRelationsChildCustomerNumber`,

							`" . TABLE_CUSTOMERS . "`.`customersFirmenname`,
							`" . TABLE_CUSTOMERS . "`.`customersCompanyStrasse`,
							`" . TABLE_CUSTOMERS . "`.`customersCompanyHausnummer`,
							`" . TABLE_CUSTOMERS . "`.`customersCompanyPLZ`,
							`" . TABLE_CUSTOMERS . "`.`customersCompanyOrt`,
							`" . TABLE_CUSTOMERS . "`.`customersCompanyCountry`

							FROM `" . TABLE_CUSTOMERS_RELATIONS . "`
							LEFT JOIN `" . TABLE_CUSTOMERS . "`
							ON(`" . TABLE_CUSTOMERS_RELATIONS . "`.`customersRelationsChildCustomerID` = `" . TABLE_CUSTOMERS . "`.`customersID`)

							WHERE 1
								AND `customersRelationsParentCustomerID` = '" . $customerDatas["customersID"] . "'
								AND `customersRelationsParentCustomerNumber` = '" . $customerDatas["customersKundennummer"] . "'
					";
				$rs = $dbConnection->db_query($sql);
				$arrCustomerChildCompanyDatas = array();
				while($ds = mysqli_fetch_assoc($rs)){
					$arrCustomerChildCompanyDatas[$ds["customersRelationsChildCustomerID"]]["childCompanyID"] = $ds["customersRelationsChildCustomerID"];
					$arrCustomerChildCompanyDatas[$ds["customersRelationsChildCustomerID"]]["childCompanyNumber"] = $ds["customersRelationsChildCustomerNumber"];
					$arrCustomerChildCompanyDatas[$ds["customersRelationsChildCustomerID"]]["childCompanyName"] = $ds["customersFirmenname"];
					$arrCustomerChildCompanyDatas[$ds["customersRelationsChildCustomerID"]]["childCompanyStrasse"] = $ds["customersCompanyStrasse"];
					$arrCustomerChildCompanyDatas[$ds["customersRelationsChildCustomerID"]]["childCompanyHausnummer"] = $ds["customersCompanyHausnummer"];
					$arrCustomerChildCompanyDatas[$ds["customersRelationsChildCustomerID"]]["childCompanyPLZ"] = $ds["customersCompanyPLZ"];
					$arrCustomerChildCompanyDatas[$ds["customersRelationsChildCustomerID"]]["childCompanyOrt"] = $ds["customersCompanyOrt"];
					$arrCustomerChildCompanyDatas[$ds["customersRelationsChildCustomerID"]]["childCompanyCountry"] = $ds["customersCompanyCountry"];
				}
			// EOF GET COMPANY RELATIONS - CHILD COMPANIES
		}
	// EOF READ SELECTED DATAS

	// BOF READ PHONE CODES
		if($_REQUEST["editID"] != "" && $_REQUEST["editID"] != "NEW") {
			$arrPhoneCodes = getPhoneCodes(array(
								array("countryID" => $customerDatas["customersCompanyCountry"], "zipCode" => $customerDatas["customersCompanyPLZ"]),
								array("countryID" => $customerDatas["customersLieferadresseLand"], "zipCode" => $customerDatas["customersLieferadressePLZ"]),
								array("countryID" => $customerDatas["customersRechnungsadresseLand"], "zipCode" => $customerDatas["customersRechnungsadressePLZ"])
							)
						);
		}
	// EOF READ PHONE CODES

	// BOF GET PDF-DOCUMENTS
		if($_REQUEST["editID"] != "" && $_REQUEST["editID"] != "NEW") {
			$sql = "
				SELECT
					`" . TABLE_CREATED_DOCUMENTS . "`.`createdDocumentsID`,
					`" . TABLE_CREATED_DOCUMENTS . "`.`createdDocumentsType`,
					`" . TABLE_CREATED_DOCUMENTS . "`.`createdDocumentsNumber`,
					`" . TABLE_CREATED_DOCUMENTS . "`.`createdDocumentsCustomerNumber`,
					`" . TABLE_CREATED_DOCUMENTS . "`.`createdDocumentsFilename`,
					`" . TABLE_CREATED_DOCUMENTS . "`.`createdDocumentsTimeCreated`,

					`" . TABLE_CREATED_DOCUMENTS_TYPES . "`.`createdDocumentsTypesID`,
					`" . TABLE_CREATED_DOCUMENTS_TYPES . "`.`createdDocumentsTypesName`,
					`" . TABLE_CREATED_DOCUMENTS_TYPES . "`.`createdDocumentsTypesShortName`

					FROM `" . TABLE_CREATED_DOCUMENTS . "`

					LEFT JOIN `" . TABLE_CREATED_DOCUMENTS_TYPES . "`
					ON(`" . TABLE_CREATED_DOCUMENTS . "`.`createdDocumentsType` = `" . TABLE_CREATED_DOCUMENTS_TYPES . "`.`createdDocumentsTypesShortName`)

					WHERE `" . TABLE_CREATED_DOCUMENTS . "`.`createdDocumentsCustomerNumber` = '".$customerDatas["customersKundennummer"]."'
						AND `" . TABLE_CREATED_DOCUMENTS . "`.`createdDocumentsType` = 'KA'
					ORDER BY `createdDocumentsNumber` DESC
			";

			$rs = $dbConnection->db_query($sql);

			while($ds = mysqli_fetch_assoc($rs)) {
				foreach(array_keys($ds) as $field) {
					$arrPdfDatas[$ds["createdDocumentsTypesName"]][$ds["createdDocumentsID"]][$field] = $ds[$field];
				}
			}
		}
	// EOF GET PDF-DOCUMENTS

	// BOF GET CREATED DOCUMENTS
	if($_REQUEST["editID"] != "" && $_REQUEST["editID"] != "NEW") {
		$arrSQL = array();

		foreach($arrTables as $thisTableKey => $thisTableValue){

			$arrSQL[] = "SELECT
						`" . $thisTableValue . "`.`orderDocumentsNumber`,

						'" . $thisTableKey . "' AS `tableDocumentType`,

						`" . $thisTableValue . "`.`orderDocumentsID`,
						`" . $thisTableValue . "`.`orderDocumentsType`,
						`" . $thisTableValue . "`.`orderDocumentsOrdersIDs`,
						`" . $thisTableValue . "`.`orderDocumentsCustomerNumber`,
						`" . $thisTableValue . "`.`orderDocumentsCustomersOrderNumber`,
						`" . $thisTableValue . "`.`orderDocumentsProcessingDate`,
						`" . $thisTableValue . "`.`orderDocumentsDeliveryDate`,
						`" . $thisTableValue . "`.`orderDocumentsBindingDate`,
						`" . $thisTableValue . "`.`orderDocumentsSalesman`,
						`" . $thisTableValue . "`.`orderDocumentsAddressCompany`,
						`" . $thisTableValue . "`.`orderDocumentsAddressName`,
						`" . $thisTableValue . "`.`orderDocumentsAddressStreet`,
						`" . $thisTableValue . "`.`orderDocumentsAddressStreetNumber`,
						`" . $thisTableValue . "`.`orderDocumentsAddressZipcode`,
						`" . $thisTableValue . "`.`orderDocumentsAddressCity`,
						`" . $thisTableValue . "`.`orderDocumentsAddressCountry`,
						`" . $thisTableValue . "`.`orderDocumentsAddressMail`,
						`" . $thisTableValue . "`.`orderDocumentsSumPrice`,
						`" . $thisTableValue . "`.`orderDocumentsDiscount`,
						`" . $thisTableValue . "`.`orderDocumentsDiscountType`,
						`" . $thisTableValue . "`.`orderDocumentsDiscountPercent`,
						`" . $thisTableValue . "`.`orderDocumentsMwst`,
						`" . $thisTableValue . "`.`orderDocumentsMwstPrice`,
						`" . $thisTableValue . "`.`orderDocumentsShippingCosts`,
						`" . $thisTableValue . "`.`orderDocumentsPackagingCosts`,
						`" . $thisTableValue . "`.`orderDocumentsCashOnDelivery`,
						`" . $thisTableValue . "`.`orderDocumentsTotalPrice`,
						`" . $thisTableValue . "`.`orderDocumentsBankAccount`,
						`" . $thisTableValue . "`.`orderDocumentsPaymentCondition`,
						`" . $thisTableValue . "`.`orderDocumentsPaymentType`,
						`" . $thisTableValue . "`.`orderDocumentsDeliveryType`,
						`" . $thisTableValue . "`.`orderDocumentsDeliveryTypeNumber`,
						`" . $thisTableValue . "`.`orderDocumentsSubject`,
						`" . $thisTableValue . "`.`orderDocumentsContent`,
						`" . $thisTableValue . "`.`orderDocumentsDocumentPath`,
						`" . $thisTableValue . "`.`orderDocumentsTimestamp`,
						`" . $thisTableValue . "`.`orderDocumentsStatus`,
						`" . $thisTableValue . "`.`orderDocumentsSendMail`
/*
						,`" . TABLE_DOCUMENTS_TO_DOCUMENTS . "`.`documentsToDocumentsOriginDocumentNumber`,
						`" . TABLE_DOCUMENTS_TO_DOCUMENTS . "`.`documentsToDocumentsCreatedDocumentNumber`,
						`" . TABLE_DOCUMENTS_TO_DOCUMENTS . "`.`documentsToDocumentsRelationType`,
						`" . TABLE_DOCUMENTS_TO_DOCUMENTS . "`.`documentsToDocumentsCreatedDocumentType`,

						IF(`" . $thisTableValue . "`.`orderDocumentsNumber` = `" . TABLE_DOCUMENTS_TO_DOCUMENTS . "`.`documentsToDocumentsOriginDocumentNumber`, '1', '0') AS `documentWasConverted`
*/

					FROM `" . $thisTableValue . "`
/*
					LEFT JOIN `" . TABLE_DOCUMENTS_TO_DOCUMENTS . "`
					ON(`" . $thisTableValue . "`.`orderDocumentsNumber` = `" . TABLE_DOCUMENTS_TO_DOCUMENTS . "`.`documentsToDocumentsOriginDocumentNumber`)
*/
					WHERE 1
						AND `" . $thisTableValue . "`.`orderDocumentsCustomerNumber` = '".$customerDatas["customersKundennummer"]."'

						GROUP BY `" . $thisTableValue . "`.`orderDocumentsNumber`
			";

			// BOF GET DOCUMENTS THAT SALESMAN GET FOR THEIR CUSTOMERS
			$arrSQL[] = "
					SELECT
						`" . $thisTableValue . "`.`orderDocumentsNumber`,

						'" . $thisTableKey . "' AS `tableDocumentType`,

						`" . $thisTableValue . "`.`orderDocumentsID`,
						`" . $thisTableValue . "`.`orderDocumentsType`,
						`" . $thisTableValue . "`.`orderDocumentsOrdersIDs`,
						`" . $thisTableValue . "`.`orderDocumentsCustomerNumber`,
						`" . $thisTableValue . "`.`orderDocumentsCustomersOrderNumber`,
						`" . $thisTableValue . "`.`orderDocumentsProcessingDate`,
						`" . $thisTableValue . "`.`orderDocumentsDeliveryDate`,
						`" . $thisTableValue . "`.`orderDocumentsBindingDate`,
						`" . $thisTableValue . "`.`orderDocumentsSalesman`,
						CONCAT(
							`" . $thisTableValue . "`.`orderDocumentsAddressCompany`,
							'<br /><span class=\"remarksArea\"><b>Kunde: </b>',
							`customerCustomers`.`customersFirmenname`,
							`customerCustomers`.`customersFirmennameZusatz`,
							' (',
							`" . $thisTableValue . "`.`orderDocumentsCustomerNumber`,
							')',
							'</span>'
						) AS `orderDocumentsAddressCompany`,
						`" . $thisTableValue . "`.`orderDocumentsAddressName`,
						`" . $thisTableValue . "`.`orderDocumentsAddressStreet`,
						`" . $thisTableValue . "`.`orderDocumentsAddressStreetNumber`,
						`" . $thisTableValue . "`.`orderDocumentsAddressZipcode`,
						`" . $thisTableValue . "`.`orderDocumentsAddressCity`,
						`" . $thisTableValue . "`.`orderDocumentsAddressCountry`,
						`" . $thisTableValue . "`.`orderDocumentsAddressMail`,
						`" . $thisTableValue . "`.`orderDocumentsSumPrice`,
						`" . $thisTableValue . "`.`orderDocumentsDiscount`,
						`" . $thisTableValue . "`.`orderDocumentsDiscountType`,
						`" . $thisTableValue . "`.`orderDocumentsDiscountPercent`,
						`" . $thisTableValue . "`.`orderDocumentsMwst`,
						`" . $thisTableValue . "`.`orderDocumentsMwstPrice`,
						`" . $thisTableValue . "`.`orderDocumentsShippingCosts`,
						`" . $thisTableValue . "`.`orderDocumentsPackagingCosts`,
						`" . $thisTableValue . "`.`orderDocumentsCashOnDelivery`,
						`" . $thisTableValue . "`.`orderDocumentsTotalPrice`,
						`" . $thisTableValue . "`.`orderDocumentsBankAccount`,
						`" . $thisTableValue . "`.`orderDocumentsPaymentCondition`,
						`" . $thisTableValue . "`.`orderDocumentsPaymentType`,
						`" . $thisTableValue . "`.`orderDocumentsDeliveryType`,
						`" . $thisTableValue . "`.`orderDocumentsDeliveryTypeNumber`,
						`" . $thisTableValue . "`.`orderDocumentsSubject`,
						`" . $thisTableValue . "`.`orderDocumentsContent`,
						`" . $thisTableValue . "`.`orderDocumentsDocumentPath`,
						`" . $thisTableValue . "`.`orderDocumentsTimestamp`,
						`" . $thisTableValue . "`.`orderDocumentsStatus`,
						`" . $thisTableValue . "`.`orderDocumentsSendMail`
/*
						,`" . TABLE_DOCUMENTS_TO_DOCUMENTS . "`.`documentsToDocumentsOriginDocumentNumber`,
						`" . TABLE_DOCUMENTS_TO_DOCUMENTS . "`.`documentsToDocumentsCreatedDocumentNumber`,
						`" . TABLE_DOCUMENTS_TO_DOCUMENTS . "`.`documentsToDocumentsRelationType`,
						`" . TABLE_DOCUMENTS_TO_DOCUMENTS . "`.`documentsToDocumentsCreatedDocumentType`,

						IF(`" . $thisTableValue . "`.`orderDocumentsNumber` = `" . TABLE_DOCUMENTS_TO_DOCUMENTS . "`.`documentsToDocumentsOriginDocumentNumber`, '1', '0') AS `documentWasConverted`
*/

					FROM `" . $thisTableValue . "`
/*
					LEFT JOIN `" . TABLE_DOCUMENTS_TO_DOCUMENTS . "`
					ON(`" . $thisTableValue . "`.`orderDocumentsNumber` = `" . TABLE_DOCUMENTS_TO_DOCUMENTS . "`.`documentsToDocumentsOriginDocumentNumber`)
*/
					LEFT JOIN `" . TABLE_CUSTOMERS . "` AS `customerSalesman`
					ON(`" . $thisTableValue . "`.`orderDocumentsSalesman` = `customerSalesman`.`customersID` AND `" . $thisTableValue . "`.`orderDocumentsSalesman` > 0)

					LEFT JOIN `" . TABLE_CUSTOMERS . "` AS `customerCustomers`
					ON(`" . $thisTableValue . "`.`orderDocumentsCustomerNumber` = `customerCustomers`.`customersKundennummer`)

					WHERE 1
						AND `customerSalesman`.`customersKundennummer` = '".$customerDatas["customersKundennummer"]."'

						GROUP BY `" . $thisTableValue . "`.`orderDocumentsNumber`
			";
			// EOF GET DOCUMENTS THAT SALESMAN GET FOR THEIR CUSTOMERS
		}
		if(!empty($arrSQL)) {
			$sql = implode(" UNION ALL ", $arrSQL) . " ORDER BY `tableDocumentType` DESC, `orderDocumentsProcessingDate` DESC, `orderDocumentsNumber` DESC ;";
		}
#dd('sql');
		$rs = $dbConnection->db_query($sql);

		while($ds = mysqli_fetch_assoc($rs)) {
			$arrTemp = array();
			foreach(array_keys($ds) as $field) {
				$arrTemp[$field] = $ds[$field];
			}
			$arrDocumentDatas[$ds["tableDocumentType"]][$ds["orderDocumentsID"]] = $arrTemp;
		}
	}
	// EOF GET CREATED DOCUMENTS

	// BOF GET SALESMAN ADRESS
	if($customerDatas["customersUseSalesmanDeliveryAdress"] == '1' || $customerDatas["customersUseSalesmanInvoiceAdress"] == '1') {
		$sql = "SELECT
						`customersID`,
						`customersKundennummer`,
						`customersFirmenname`,
						`customersFirmenInhaberVorname`,
						`customersFirmenInhaberNachname`,
						`customersFirmenInhaberAnrede`,

						`customersFirmenInhaber2Vorname`,
						`customersFirmenInhaber2Nachname`,
						`customersFirmenInhaber2Anrede`,

						`customersCompanyStrasse`,
						`customersCompanyHausnummer`,
						`customersCompanyCountry`,
						`customersCompanyPLZ`,
						`customersCompanyOrt`,
						`customersLieferadresseFirmenname`,
						`customersLieferadresseFirmennameZusatz`,
						`customersLieferadresseStrasse`,
						`customersLieferadresseHausnummer`,
						`customersLieferadressePLZ`,
						`customersLieferadresseOrt`,
						`customersLieferadresseLand`,
						`customersRechnungsadresseFirmenname`,
						`customersRechnungsadresseFirmennameZusatz`,
						`customersRechnungsadresseStrasse`,
						`customersRechnungsadresseHausnummer`,
						`customersRechnungsadressePLZ`,
						`customersRechnungsadresseOrt`,
						`customersRechnungsadresseLand`


						FROM `" . TABLE_CUSTOMERS . "`

						WHERE `customersID` = '".$customerDatas["customersVertreterID"]."'

						LIMIT 1
		";

		$rs = $dbConnection->db_query($sql);

		while($ds = mysqli_fetch_assoc($rs)) {
			foreach(array_keys($ds) as $field) {
				if($customerDatas["customersUseSalesmanDeliveryAdress"] == '1') {
					$customerDatas["customersLieferadresseFirmenname"] = $ds["customersLieferadresseFirmenname"];
					$customerDatas["customersLieferadresseFirmennameZusatz"] = $ds["customersLieferadresseFirmennameZusatz"];
					$customerDatas["customersLieferadresseStrasse"] = $ds["customersLieferadresseStrasse"];
					$customerDatas["customersLieferadresseHausnummer"] = $ds["customersLieferadresseHausnummer"];
					$customerDatas["customersLieferadressePLZ"] = $ds["customersLieferadressePLZ"];
					$customerDatas["customersLieferadresseOrt"] = $ds["customersLieferadresseOrt"];
					$customerDatas["customersLieferadresseLand"] = $ds["customersLieferadresseLand"];
				}
				if($customerDatas["customersUseSalesmanInvoiceAdress"] == '1') {
					$customerDatas["customersRechnungsadresseFirmenname"] = $ds["customersRechnungsadresseFirmenname"];
					$customerDatas["customersRechnungsadresseFirmennameZusatz"] = $ds["customersRechnungsadresseFirmennameZusatz"];
					$customerDatas["customersRechnungsadresseStrasse"] = $ds["customersRechnungsadresseStrasse"];
					$customerDatas["customersRechnungsadresseHausnummer"] = $ds["customersRechnungsadresseHausnummer"];
					$customerDatas["customersRechnungsadressePLZ"] = $ds["customersRechnungsadressePLZ"];
					$customerDatas["customersRechnungsadresseOrt"] = $ds["customersRechnungsadresseOrt"];
					$customerDatas["customersRechnungsadresseLand"] = $ds["customersRechnungsadresseLand"];
				}
			}
		}
	}
	// BOF GET SALESMAN ADRESS

	// BOF GET CONNECTED DOCUMENTS
	if($_REQUEST["editID"] != "" && $_REQUEST["editID"] != "NEW") {
		$arrSubSQL = array();
		$count = 0;
		foreach($arrTables as $thisTableKey => $thisTableValue){
			$arrSubSQL[$count] = "SELECT

							`" . $thisTableValue . "`.`orderDocumentsID`,
							`" . $thisTableValue . "`.`orderDocumentsCustomerNumber`,
							`" . $thisTableValue . "`.`orderDocumentsNumber`,
							`" . $thisTableValue . "`.`orderDocumentsDocumentDate`,
							`" . $thisTableValue . "`.`orderDocumentsOrderDate`,
							`" . $thisTableValue . "`.`orderDocumentsType`,
							`" . $thisTableValue . "`.`orderDocumentsOrdersIDs`,
							`" . $thisTableValue . "`.`orderDocumentsStatus`,
							'" . $thisTableValue . "' AS `nameTable`,
				";

			if($thisTableKey != 'BR'){
				$arrSubSQL[$count] .= "
							`" . TABLE_RELATED_DOCUMENTS . "`.`relatedDocuments_AN`,
							`" . TABLE_RELATED_DOCUMENTS . "`.`relatedDocuments_AB`,
							`" . TABLE_RELATED_DOCUMENTS . "`.`relatedDocuments_RE`,
							`" . TABLE_RELATED_DOCUMENTS . "`.`relatedDocuments_LS`,
							`" . TABLE_RELATED_DOCUMENTS . "`.`relatedDocuments_GU`,
							`" . TABLE_RELATED_DOCUMENTS . "`.`relatedDocuments_MA`,
							`" . TABLE_RELATED_DOCUMENTS . "`.`relatedDocuments_M1`,
							`" . TABLE_RELATED_DOCUMENTS . "`.`relatedDocuments_M2`,
							`" . TABLE_RELATED_DOCUMENTS . "`.`relatedDocuments_M3`,
				";
			}
			else {
				$arrSubSQL[$count] .= "
							'' AS `relatedDocuments_AN`,
							'' AS `relatedDocuments_AB`,
							'' AS `relatedDocuments_RE`,
							'' AS `relatedDocuments_LS`,
							'' AS `relatedDocuments_GU`,
							'' AS `relatedDocuments_MA`,
							'' AS `relatedDocuments_M1`,
							'' AS `relatedDocuments_M2`,
							'' AS `relatedDocuments_M3`,
				";
			}
			$arrSubSQL[$count] .= "
							CONCAT(
								`" . $thisTableValue . "`.`orderDocumentsAddressCompany`,
								'<br /><span class=\"remarksArea\"><b>Kunde: </b>',
								`customerCustomers`.`customersFirmenname`,
								`customerCustomers`.`customersFirmennameZusatz`,
								' (',
								`" . $thisTableValue . "`.`orderDocumentsCustomerNumber`,
								')',
								'</span>'
							) AS `orderDocumentsAddressCompany`

						FROM `" . $thisTableValue . "`

						LEFT JOIN `" . TABLE_CUSTOMERS. "` AS `customerCustomers`
						ON(`" . $thisTableValue . "`.`orderDocumentsCustomerNumber` = `customerCustomers`.`customersKundennummer`)

			";
			if($thisTableKey != 'BR'){
				$arrSubSQL[$count] .= "
						LEFT JOIN `" . TABLE_RELATED_DOCUMENTS . "`
						ON(`" . $thisTableValue . "`.`orderDocumentsNumber` = `" . TABLE_RELATED_DOCUMENTS . "`.`relatedDocuments_" . $thisTableKey . "`)
					";
			}
			$arrSubSQL[$count] .= "
						WHERE 1
							AND `" . $thisTableValue . "`.`orderDocumentsCustomerNumber` = '" . $customerDatas["customersKundennummer"] . "'
				";
			$count++;
		}

		// BOF GET DOCUMENTS THAT SALESMAN GET FOR THEIR CUSTOMERS
		foreach($arrTables as $thisTableKey => $thisTableValue){
			if($thisTableKey != 'BR'){
				$arrSubSQL[$count] = "

						SELECT

							`" . $thisTableValue . "`.`orderDocumentsID`,
							`" . $thisTableValue . "`.`orderDocumentsCustomerNumber`,
							`" . $thisTableValue . "`.`orderDocumentsNumber`,
							`" . $thisTableValue . "`.`orderDocumentsDocumentDate`,
							`" . $thisTableValue . "`.`orderDocumentsOrderDate`,
							`" . $thisTableValue . "`.`orderDocumentsType`,
							`" . $thisTableValue . "`.`orderDocumentsOrdersIDs`,
							`" . $thisTableValue . "`.`orderDocumentsStatus`,
							'" . $thisTableValue . "' AS `nameTable`,

							`" . TABLE_RELATED_DOCUMENTS . "`.`relatedDocuments_AN`,
							`" . TABLE_RELATED_DOCUMENTS . "`.`relatedDocuments_AB`,
							`" . TABLE_RELATED_DOCUMENTS . "`.`relatedDocuments_RE`,
							`" . TABLE_RELATED_DOCUMENTS . "`.`relatedDocuments_LS`,
							`" . TABLE_RELATED_DOCUMENTS . "`.`relatedDocuments_GU`,
							`" . TABLE_RELATED_DOCUMENTS . "`.`relatedDocuments_MA`,
							`" . TABLE_RELATED_DOCUMENTS . "`.`relatedDocuments_M1`,
							`" . TABLE_RELATED_DOCUMENTS . "`.`relatedDocuments_M2`,
							`" . TABLE_RELATED_DOCUMENTS . "`.`relatedDocuments_M3`,

							CONCAT(
								`" . $thisTableValue . "`.`orderDocumentsAddressCompany`,
								'<br /><span class=\"remarksArea\"><b>Kunde: </b>',
								`customerCustomers`.`customersFirmenname`,
								`customerCustomers`.`customersFirmennameZusatz`,
								' (',
								`" . $thisTableValue . "`.`orderDocumentsCustomerNumber`,
								')',
								'</span>'
							) AS `orderDocumentsAddressCompany`

						FROM `" . TABLE_CUSTOMERS . "` AS `customerSalesman`

						LEFT JOIN `" . $thisTableValue . "`
						ON(`customerSalesman`.`customersID` = `" . $thisTableValue . "`.`orderDocumentsSalesman`)

						LEFT JOIN `" . TABLE_CUSTOMERS . "` AS `customerCustomers`
						ON(`" . $thisTableValue . "`.`orderDocumentsCustomerNumber` = `customerCustomers`.`customersKundennummer`)

						LEFT JOIN `" . TABLE_RELATED_DOCUMENTS . "`
						ON(`" . $thisTableValue . "`.`orderDocumentsNumber` = `" . TABLE_RELATED_DOCUMENTS . "`.`relatedDocuments_" . $thisTableKey . "`)

						WHERE 1
							AND `customerSalesman`.`customersKundennummer` = '".$customerDatas["customersKundennummer"]."'
					";
				$count++;
			}
		}
		// EOF GET DOCUMENTS THAT SALESMAN GET FOR THEIR CUSTOMERS

		$sql = "SELECT

				`tempTable`.`orderDocumentsID`,
				`tempTable`.`orderDocumentsCustomerNumber`,
				`tempTable`.`orderDocumentsNumber`,
				`tempTable`.`orderDocumentsDocumentDate`,
				`tempTable`.`orderDocumentsOrderDate`,
				`tempTable`.`orderDocumentsType`,
				`tempTable`.`orderDocumentsOrdersIDs`,
				`tempTable`.`nameTable`,
				`tempTable`.`orderDocumentsStatus`,
				`tempTable`.`orderDocumentsAddressCompany`,


				`tempTable`.`relatedDocuments_AN`,
				`tempTable`.`relatedDocuments_AB`,
				`tempTable`.`relatedDocuments_RE`,
				`tempTable`.`relatedDocuments_LS`,
				`tempTable`.`relatedDocuments_GU`,
				`tempTable`.`relatedDocuments_MA`,
				`tempTable`.`relatedDocuments_M1`,
				`tempTable`.`relatedDocuments_M2`,
				`tempTable`.`relatedDocuments_M3`,

				`" . TABLE_DOCUMENTS_TO_DOCUMENTS . "`.`documentsToDocumentsOriginDocumentNumber`,
				`" . TABLE_DOCUMENTS_TO_DOCUMENTS . "`.`documentsToDocumentsCreatedDocumentNumber`,
				`" . TABLE_DOCUMENTS_TO_DOCUMENTS . "`.`documentsToDocumentsRelationType`,

				`" . TABLE_CREATED_DOCUMENTS . "`.`createdDocumentsFilename`

				FROM (
				" . implode(" UNION ALL ", $arrSubSQL) . "
				) AS `tempTable`

					LEFT JOIN `" . TABLE_DOCUMENTS_TO_DOCUMENTS . "`
					ON(`tempTable`.`orderDocumentsNumber` = `" . TABLE_DOCUMENTS_TO_DOCUMENTS . "`.`documentsToDocumentsOriginDocumentNumber`)

					LEFT JOIN `" . TABLE_CREATED_DOCUMENTS . "`
					ON(`tempTable`.`orderDocumentsNumber` = `" . TABLE_CREATED_DOCUMENTS . "`.`createdDocumentsNumber`)

				";

				// GROUP BY CONCAT(`tempTable`.`orderDocumentsNumber`, ';', `" . TABLE_DOCUMENTS_TO_DOCUMENTS . "`.`documentsToDocumentsCreatedDocumentNumber`)

		$rs = $dbConnection->db_query($sql);

		$arrConnectedDocumentIds = array();
		$arrDocumentNumbers = array();
		$arrRelatedDocumentNumbers = array();
		while($ds = mysqli_fetch_assoc($rs)) {
			$arrTemp = explode(";", $ds["orderDocumentsOrdersIDs"]);
			$arrTemp = array_unique($arrTemp);
			$arrDocumentNumbers[] = $ds["orderDocumentsNumber"];
			#$arrDocumentRelatedNumbers[] = getRelatedDocuments(array($ds["orderDocumentsNumber"]));

			if($ds["orderDocumentsNumber"] != ''){
				if($ds["orderDocumentsType"] != 'BR'){
					$arrDocumentRelatedNumbers[] = getRelatedDocuments(array($ds["orderDocumentsNumber"]));
				}
			}

			$arrTemp = explode(";", $ds["orderDocumentsOrdersIDs"]);
			$arrTemp = array_unique($arrTemp);
			$thisProductionKey = implode("#", $arrTemp);
			$arrConnectedDocumentIds[$ds["orderDocumentsNumber"]] = array(
						"table" => $ds["nameTable"],
						"documentID" => $ds["orderDocumentsID"],
						"relationType" => $ds["documentsToDocumentsRelationType"],
						"productionIDs" => $ds["orderDocumentsOrdersIDs"],
						"documentsType" => $ds["orderDocumentsType"],
						"documentsStatus" => $ds["orderDocumentsStatus"],
						"documentsDocumentDate" => $ds["orderDocumentsDocumentDate"],
						"documentsOrderDate" => $ds["orderDocumentsOrderDate"],
						"documentsAddressCompany" => $ds["orderDocumentsAddressCompany"],
						"documentsFilename" => $ds["createdDocumentsFilename"]
				);

			foreach(array_keys($ds) as $field){
				$arrResultDocumentNumbers[$ds["nameTable"]][$thisProductionKey][$ds["orderDocumentsID"]][$field] = $ds[$field];
			}
		}

		$arrTemp = $arrDocumentRelatedNumbers;

		$arrDocumentRelatedNumbers = array();
		foreach($arrTemp as $thisKey => $thisValue){
			$strTemp = '';
			foreach($thisValue as $thisValueKey => $thisValueValue){
				$strTemp .= $thisValueValue;
			}
			$arrDocumentRelatedNumbers[md5($strTemp)] = $arrTemp[$thisKey];
		}

		#$arrConnectedDocumentIds = array_unique($arrConnectedDocumentIds);

		#dd('arrConnectedDocumentIds');
	}
	// EOF GET CONNECTED DOCUMENTS
?>
<?php
	require_once('inc/headerHTML.inc.php');
	if($_REQUEST["editID"] == "NEW") {
		$thisTitle = 'Kunden anlegen';
		$thisIcon = 'addCustomer.png';

		if($arrGetUserRights["importOnlineAcquisition"] && $_REQUEST["importCustomerDatas"] != ''){
			$importCustomerDatas = ($_REQUEST["importCustomerDatas"]);
			$arrImportCustomerDatas = unserialize(rawurldecode($importCustomerDatas));
			unset($arrImportCustomerDatas["customersID"]);
			unset($arrImportCustomerDatas["customersUserID"]);
			$customerDatas = $arrImportCustomerDatas;
		}
	}
	else {
		$thisTitle = 'Kundendaten verwalten';
		$thisIcon = 'customers.png';
		if($customerDatas["customersKundennummer"] != '') {
			$thisTitle .= ': <span class="headerSelectedEntry">' . $customerDatas["customersKundennummer"] . ' &bull; ' . $customerDatas["customersFirmenname"] . '</span>';
		}
	}
	$headerHTML = preg_replace("/{###TITLE###}/", strip_tags($thisTitle), $headerHTML);
	echo $headerHTML;

	require_once(FILE_MENUE_TOP);
?>

<div id="mainArea">
	<div id="mainContent">
		<?php require_once(FILE_MENUE_SIDEBAR); ?>
		<div id="contentArea">

			<div id="contentAreaElements">
				<h1><?php if(SHOW_PAGE_TITLE_ICONS) { echo '<img src="' . PATH_ICONS_MENUE_TITLES . $thisIcon . '" alt="" />'; } ?> <?php echo $thisTitle; ?></h1>
<?php
	if($userDatas["usersLogin"] == 'thorsten'){
		echo '<a href="editCustomerNEU.php?searchBoxCustomer=' . $_GET["searchBoxCustomer"] . '">NEUE VERSION</a>';
	}
?>
				<?php
					if($_REQUEST["editID"] != "NEW")
					{
				?>
						<div class="menueActionsArea">
							<?php if($arrGetUserRights["editCustomers"]) { ?><a href="<?php echo PAGE_EDIT_CUSTOMER; ?>?editID=NEW" class="linkButton" title="Neuen Kunden anlegen" >Neukunden anlegen</a><?php } ?>
							<?php if($_REQUEST["editID"] != "") { ?>
							<?php if($arrGetUserRights["editOrders"]) {	?><a href="<?php echo PAGE_EDIT_PROCESS; ?>?editCustomersID=<?php echo $customerDatas["customersID"]; ?>&editID=NEW" class="linkButton" title="Neue Produktion anlegen" >Produktion</a><?php } ?>
							<?php
								$disableLink = '';
								$disableClass = '';
								$disableTitle = '';
								// if($customerDatas["customersDatasUpdated"] != '1') {
								if($customerDatas["customersDatasUpdated"] != '1' || ($_REQUEST["editID"] != "NEW" && $customerDatas["customersActive"] != '1')) {
									$disableLink = ' onclick="return false" ';
									$disableClass = ' disabled ';
									$disableTitle = ' gesperrt ';
								}
							?>
							<?php if($arrGetUserRights["editOffers"]) { ?><a href="<?php echo PAGE_CREATE_OFFER; ?>&editID=<?php echo $customerDatas["customersKundennummer"]; ?>" class="linkButton <?php echo $disableClass; ?>" title="Neues Angebot schreiben <?php echo $disableTitle; ?>" <?php echo $disableLink; ?> >Angebot</a><?php } ?>
							<?php if($arrGetUserRights["editConfirmations"]) { ?><a href="<?php echo PAGE_CREATE_CONFIRMATION; ?>&editID=<?php echo $customerDatas["customersKundennummer"]; ?>" class="linkButton <?php echo $disableClass; ?>" title="Neue Auftragsbest&auml;tigung schreiben <?php echo $disableTitle; ?>" <?php echo $disableLink; ?> >Auftragsbest&auml;tigung</a><?php } ?>
							<?php if($arrGetUserRights["editInvoices"]) { ?><a href="<?php echo PAGE_CREATE_INVOICE; ?>&editID=<?php echo $customerDatas["customersKundennummer"]; ?>" class="linkButton <?php echo $disableClass; ?>" title="Neue Rechnung schreiben <?php echo $disableTitle; ?>" <?php echo $disableLink; ?> >Rechnung</a><?php } ?>
							<!--
							<?php if($arrGetUserRights["editDeliveries"]) { ?><a href="<?php echo PAGE_CREATE_DELIVERY; ?>&editID=<?php echo $customerDatas["customersKundennummer"]; ?>" class="linkButton <?php echo $disableClass; ?>" title="Neuen Lieferschein schreiben <?php echo $disableTitle; ?>" <?php echo $disableLink; ?> >Lieferschein</a><?php } ?>
							-->
							<!--
							<?php if($arrGetUserRights["editReminders"]) { ?><a href="<?php echo PAGE_CREATE_REMINDER; ?>&editID=<?php echo $customerDatas["customersKundennummer"]; ?>" class="linkButton <?php echo $disableClass; ?>" title="Neue Mahnung schreiben <?php echo $disableTitle; ?>" <?php echo $disableLink; ?> >Mahnung</a><?php } ?>
							-->

							<?php if($arrGetUserRights["editCredits"]) { ?><a href="<?php echo PAGE_CREATE_CREDIT; ?>&editID=<?php echo $customerDatas["customersKundennummer"]; ?>" class="linkButton <?php echo $disableClass; ?>" title="Neue Gutschrift schreiben <?php echo $disableTitle; ?>" <?php echo $disableLink; ?> >Gutschrift</a><?php } ?>

							<?php if($arrGetUserRights["editLetters"]) { ?><a href="<?php echo PAGE_CREATE_LETTER; ?>&editID=<?php echo $customerDatas["customersKundennummer"]; ?>" class="linkButton <?php echo $disableClass; ?>" title="Neuen Brief schreiben <?php echo $disableTitle; ?>" <?php echo $disableLink; ?> >Brief</a><?php } ?>

							<?php } ?>
							<div class="clear"></div>
						</div>
				<?php
					}
				?>
				<div class="adminInfo">Datensatz-ID: <?php echo $customerDatas["customersID"]; ?></div>
				<div class="contentDisplay">
				<?php
					if($_REQUEST["editID"] == "")
					{
				?>
				<div id="searchFilterArea">
					<form name="formFilterSearch" method="get" action="<?php echo $_SERVER["PHP_SELF"]; ?>">
						<table border="0" cellpadding="0" cellspacing="0" width="" class="searchFilterContent">
							<tr>
								<td>
									<label for="searchCustomerNumber">Kundennr:</label>
									<input type="text" name="searchCustomerNumber" id="searchCustomerNumber" class="inputField_40" value="" />
								</td>
								<td>
									<label for="searchCustomerName">Kundenname:</label>
									<input type="text" name="searchCustomerName" id="searchCustomerName" class="inputField_100" value="" />
								</td>
								<td>
									<label for="searchPLZ">PLZ:</label>
									<input type="text" name="searchPLZ" id="searchPLZ" class="inputField_40" />
								</td>
								<td>
									<label for="searchCustomerType">Typ:</label>
									<select name="searchCustomerType" id="searchCustomerType" class="inputSelect_100" />
										<option value=""></option>
										<?php
											if(!empty($arrCustomerTypeDatas)){
												foreach($arrCustomerTypeDatas as $thisKey => $thisValue){
													$selected = '';
													if($thisValue["customerTypesID"] == $_REQUEST["searchCustomerType"]){
														$selected = ' selected="selected" ';
													}
													echo '<option value="' . $thisValue["customerTypesID"] . '" ' . $selected . ' >' . $thisValue["customerTypesName"] . '</option>';
												}
											}
										?>
									</select>
								</td>
								<td>
									<label for="searchCustomerGroup">Gruppe:</label>
									<select name="searchCustomerGroup" id="searchCustomerGroup" class="inputSelect_100" />
										<option value=""></option>
										<?php
											if(!empty($arrCustomerGroupDatas)){
												foreach($arrCustomerGroupDatas as $thisKey => $thisValue){
													$selected = '';
													if($thisValue["customerGroupsID"] == $_REQUEST["searchCustomerGroup"]){
														$selected = ' selected="selected" ';
													}
													echo '<option value="' . $thisValue["customerGroupsID"] . '" ' . $selected . ' >' . $thisValue["customerGroupsName"] . '</option>';
												}
											}
										?>
									</select>
								</td>
								<td>
									<label for="searchCustomerCountry">Land:</label>
									<select name="searchCustomerCountry" id="searchCustomerCountry" class="inputSelect_100" />
										<option value=""> </option>
										<?php
											if(!empty($arrCountryTypeDatas)) {
												foreach($arrCountryTypeDatas as $thisKey => $thisValue) {
													$selected = '';
													if($thisKey == $_REQUEST["searchCustomerCountry"]) {
														$selected = ' selected="selected" ';
													}
													echo '<option value="' . $thisKey . '" '.$selected.' >' . ($arrCountryTypeDatas[$thisKey]["countries_name"]). ' [' . $arrCountryTypeDatas[$thisKey]["countries_iso_code_2"] . ']</option>';
												}
											}
										?>
									</select>
								</td>
								<td>
									<label for="searchWord">Suchbegriff:</label>
									<input type="text" name="searchWord" id="searchWord" class="inputField_100" />
								</td>
								<td>
									<input type="hidden" name="xxxeditID" id="xxxeditID" value="" />
									<input type="submit" name="submitSearch" class="inputButton0" value="Suchen" />
								</td>
							</tr>
						</table>
					</form>
				</div>

				<?php
					}
				?>
				<?php displayMessages(); ?>

				<?php
					if($_REQUEST["editID"] == ""){
						if(!empty($arrCustomerDatas)) {
							if($pagesCount > 1 && $_REQUEST["displayOrderStatus"] == "") {
								include(FILE_MENUE_PAGES);
							}

							echo '<table cellpadding="0" cellspacing="0" border="0" width="100%" class="displayOrders">
									<thead>
										<tr>
											<th style="width:45px;text-align:right;">#</th>
											<th>Kund.Nr</th>
											<th>Firmenname</th>
											<th>Inhaber</th>
											<th>Land</th>
											<th>PLZ</th>
											<th>Ort</th>
											<th>Straße</th>
											<th>Telefon</th>
											<th>Typ</th>
											<th>Gruppe</th>
											<th>Vertreter</th>
											<th>Gebiet von</th>
											<th>Details</th>
										</tr>
									</thead>
									<tbody>
							';

							$count = 0;
							foreach($arrCustomerDatas as $thisKey => $thisValue) {
								if($count%2 == 0){ $rowClass = 'row0'; }
								else { $rowClass = 'row1'; }

								$thisStyle = '';
								if($thisValue["customersActive"] != '1') {
									$thisStyle = ' style="color:#FF0000; font-style:italic;" ';
								}

								echo '<tr ' . $thisStyle . ' class="'.$rowClass.'">';
									echo '<td style="text-align:right;"><span style="font-weight:bold;cursor:pointer;" title="Datensatz-ID: '.($thisValue["customersID"]).'">'.($count + 1).'.</span></td>';
									echo '<td style="text-align:center;">';
									if($arrGetUserRights["editCustomers"] || $arrGetUserRights["displayCustomers"]) {
										echo '<a href="' . PAGE_EDIT_CUSTOMER . '?editID=' . $thisValue["customersID"] . '" title="Kunden bearbeiten (Datensatz '.$thisValue["customersKundennummer"].')" >';
									}
									echo $thisValue["customersKundennummer"];
									if($arrGetUserRights["editCustomers"] || $arrGetUserRights["displayCustomers"]) {
										echo '</a>';
									}
									echo '</td>';
									echo '<td style="white-space:nowrap;text-align:left;">';
									echo preg_replace("/\//", " / ", $thisValue["customersFirmenname"]);
									if($thisValue["customersFirmennameZusatz"] != '') {
										echo ' &bull; ' . $thisValue["customersFirmennameZusatz"];
									}
									echo '</td>';

									echo '<td style="white-space:nowrap;text-align:left;">'.trim(preg_replace("/:/ismU", "", preg_replace("/Inh./ismU", "", $thisValue["customersFirmenInhaberVorname"].' '.$thisValue["customersFirmenInhaberNachname"]))).'</td>';

									echo '<td>';
									echo $arrCountryTypeDatas[$thisValue["customersCompanyCountry"]]["countries_iso_code_3"];
									echo '</td>';

									echo '<td style="white-space:nowrap;">';
									echo $thisValue["customersCompanyPLZ"];
									echo '</td>';

									echo '<td>';
									echo $thisValue["customersCompanyOrt"];
									echo '</td>';

									echo '<td style="white-space:nowrap;">';
									echo $thisValue["customersCompanyStrasse"] . ' ' , $thisValue["customersCompanyHausnummer"];
									echo '</td>';

									echo '<td style="white-space:nowrap;">';
									echo formatPhoneNumber($thisValue["customersTelefon1"]);
									echo '</td>';

									echo '<td style="cursor:pointer;">';
									echo '<span title="' . $arrCustomerTypeDatas[$thisValue["customersTyp"]]["customerTypesName"] . '">';
									echo $arrCustomerTypeDatas[$thisValue["customersTyp"]]["customerTypesShortName"];
									echo '</span>';
									echo '</td>';
									echo '<td style="text-align:center;">';
									$thisArrCustomerGroups = explode(";", stripslashes($thisValue["customersGruppe"]));
									if(!empty($thisArrCustomerGroups)) {
										for($i = 0; $i < count($thisArrCustomerGroups) ; $i++) {
											if($arrCustomerGroupDatas[$thisArrCustomerGroups[$i]]["customerGroupsName"] != '') {
												echo '<span class="nowrap"> &bull; ' . $arrCustomerGroupDatas[$thisArrCustomerGroups[$i]]["customerGroupsName"] . '</span>';
											}
										}
									}
									echo '</td>';
									echo '<td style="white-space:nowrap;">';
									if($thisValue["customersVertreterID"] > 0) {

										if($arrGetUserRights["editCustomers"] || $arrGetUserRights["displayCustomers"]) {
											echo '<a href="' . PAGE_EDIT_CUSTOMER . '?editID=' . $thisValue["customersVertreterID"] . '" title="Vertreter bearbeiten (Datensatz '.$arrAgentDatas2[$thisValue["customersVertreterID"]]["customersKundennummer"].')" >';
										}
										echo $arrAgentDatas2[$thisValue["customersVertreterID"]]["customersFirmenname"];
										if($arrGetUserRights["editCustomers"] || $arrGetUserRights["displayCustomers"]) {
											echo '</a>';
										}
									}
									echo '</td>';

									echo '<td style="white-space:nowrap;">';
									echo $arrRelationSalesmenZipcodeDatas[substr($thisValue["customersCompanyPLZ"], 0, 2)]["kundenname"];
									echo '</td>';

									echo '<td style="white-space:nowrap;">';
										if($thisValue["customersNotiz"] != "") {
											echo '<span class="toolItem">';
											echo '<img src="layout/icons/iconRtf.gif" class="buttonNotice" width="16" height="16" alt="Bemerkung" />';
											echo '<div class="displayNoticeArea" style="display:none; visibility:hidden;">';
											echo wordwrap($thisValue["customersNotiz"], 70, '<br />', false);
											echo '</div>';
											echo '</span>';
										}
										else {
											echo '<span class="toolItem"><img src="layout/spacer.gif" width="16" height="16" alt="" /></span>';
										}
										if($arrGetUserRights["editCustomers"] || $arrGetUserRights["displayCustomers"]) {
											echo '<span class="toolItem"><a href="' . PAGE_EDIT_CUSTOMER . '?editID=' . $thisValue["customersID"] . '"><img src="layout/icons/iconEdit.gif" width="16" height="16" title="Kunden bearbeiten (Datensatz '.$thisValue["customersKundennummer"].')" alt="Bearbeiten" /></a></span>';
										}
										else {
											echo '<span class="toolItem"><img src="layout/spacer.gif" width="16" height="16" alt="" /></span>';
										}

										if($arrGetUserRights["displayOrders"] || $arrGetUserRights["editOrders"]) {
											echo '<span class="toolItem"><a href="' . PAGE_EDIT_PROCESS . '?editCustomersID=' . $thisValue["customersID"] . '&editID=NEW"><img src="layout/menueIcons/menueSidebar/addProcess.png" width="16" height="16" title="Neue Produktion f&uuml;r diesen Kunden anlegen)" alt="Neue Produktion anlegen" /></a></span>';
										}

										if($thisValue["customersMail1"] != "") {
											echo '<span class="toolItem"><a href="mailto:'.$thisValue["customersMail1"].'"><img src="layout/icons/iconMail.gif" width="16" height="14" title="Kunde &quot;'.($thisValue["customersFirmenname"]). '&quot; (KNR: '.$thisValue["customersKundennummer"].') per Mail kontaktieren" alt="per Mail kontaktieren" /></a></span>';
										}
										else {
											echo '<span class="toolItem"><img src="layout/spacer.gif" width="16" height="16" alt="" /></span>';
										}
										if($thisValue["customersHomepage"] != "") {
											echo '<span class="toolItem"><a href="http://'.$thisValue["customersHomepage"].'" target="_blank"><img src="layout/icons/iconWeb.gif" width="16" height="16" title="Homepage des Kunden aufrufen" alt="Homepage" /></a></span>';
										}
										else {
											echo '<span class="toolItem"><img src="layout/spacer.gif" width="16" height="16" alt="" /></span>';
										}
									echo '</td>';
								echo '</tr>';
								$count++;
							}
							echo '	</tbody>
								</table>
							';

							if($pagesCount > 1 && $_REQUEST["displayOrderStatus"] == "") {
								include(FILE_MENUE_PAGES);
							}
						}
						else {

						}
					}
				?>

				<?php if($_REQUEST["editID"] != ""){ ?>
					<div id="tabs">
						<div class="adminEditArea">
							<?php if($_REQUEST["editID"] != "NEW") { ?>
							<ul>
								<li><a href="editCustomerNEU.php?editID=<?php echo $_REQUEST["editID"]; ?>&amp;tabID=tabs-1">Kunden-Daten</a></li>

								<?php if($customerDatas["customersNotiz"] != '') { ?>
								<li class="tabImportant"><a href="editCustomerNEU.php?editID=<?php echo $_REQUEST["editID"]; ?>&amp;tabID=tabs-7">Notizen</a></li>
								<?php } ?>

								<li><a href="editCustomerNEU.php?editID=<?php echo $_REQUEST["editID"]; ?>&amp;tabID=tabs-2">Bisherige Produktionen</a></li>
								<li><a href="editCustomerNEU.php?editID=<?php echo $_REQUEST["editID"]; ?>&amp;tabID=tabs-3">Korrektur-Abz&uuml;ge (<?php echo count($arrPdfDatas["Korrektur-Abzug"]); ?>)</a></li>
								<li><a href="editCustomerNEU.php?editID=<?php echo $_REQUEST["editID"]; ?>&amp;tabID=tabs-6">Auslieferungen</a></li>
								<?php if(!empty($arrDocumentDatas)) { ?>
								<li><a href="editCustomerNEU.php?editID=<?php echo $_REQUEST["editID"]; ?>&amp;tabID=tabs-4">Dokumente</a></li>
								<li><a href="editCustomerNEU.php?editID=<?php echo $_REQUEST["editID"]; ?>&amp;tabID=tabs-5">Dokument-Beziehungen</a></li>
								<?php } ?>

								<li class="tabImportant"><a href="editCustomerNEU.php?editID=<?php echo $_REQUEST["editID"]; ?>&amp;tabID=tabs-10">Erinnerungen</a></li>
								<li><a href="editCustomerNEU.php?editID=<?php echo $_REQUEST["editID"]; ?>&amp;tabID=tabs-9">Kontakte</a></li>
								<?php if(in_array($customerDatas["customersTyp"], array('2', '4'))) { ?>
								<li><a href="editCustomerNEU.php?editID=<?php echo $_REQUEST["editID"]; ?>&amp;tabID=tabs-8">Provisionen</a></li>
								<?php } ?>
							</ul>
							<?php } ?>
							<?php if($_REQUEST["tabID"] == "tabs-1") { ?>
							<div id="tabs-1">
								<?php displayMessages(); ?>
								<form name="formEditCustomerDatas" id="formEditCustomerDatas" method="post" action="<?php echo $_SERVER["PHP_SELF"]; ?>#tabs-1" enctype="multipart/form-data" >
									<input type="hidden" name="tabID" value="<?php echo $_REQUEST["tabID"]; ?>" />
									<input type="hidden" name="editCustomersID" value="<?php echo $customerDatas["customersID"]; ?>" />
									<!--
									<input type="hidden" name="editCustomersActive" value="<?php echo $customerDatas["customersActive"]; ?>">
									-->
									<br /><p class="warningArea">Pflichtfelder  m&uuml;ssen ausgef&uuml;llt werden!</p>
									<fieldset>
										<legend>Kundendaten</legend>
										<table border="0" width="100%" cellspacing="0" cellpadding="0">
											<tr>
												<td style="width:200px;"><b>Kundennummer:</b></td>
												<td>
													<?php
														/*
														if($_REQUEST["editID"] == "NEW") {
															echo '<a href="javascript:void(\'0\');" onclick="createCustomerNumber(\'formEditCustomerDatas\', \'editCustomersKundennummer\', \'editCustomersKundennummer\'); "class="linkButton" id="generateCustomerNumberButton">Kundennummer generieren</a>';
															$readonly = '';
														}
														else {
															$readonly = ' readonly="readonly" ';
														}
														*/

														if($_REQUEST["editID"] == "NEW") {
															$readonly = ' readonly="readonly" ';
															#$description = 'Soll eine <b>Kundennummer automatisch generiert</b> werden, geben Sie bitte die <b>ersten <br />beiden Ziffern der Postleitzahl</b> ein. <br />Die neue Kundennummer wird dann <b>automatisch nach dem Speichern</b> angelegt.';
															$description = 'Soll eine <b>Kundennummer automatisch generiert</b> werden, <br />dieses <b>Feld bitte leer</b> lassen. <br />Die neue Kundennummer wird dann <b>automatisch nach dem Speichern</b> angelegt.';
														}
														else if($customerDatas["customersKundennummer"] != ''){
															$readonly = ' readonly="readonly" ';
															$description = '';
														}
													?>
													<input type="text" name="editCustomersKundennummer" id="editCustomersKundennummer" class="inputField_100" <?php if($_REQUEST["editID"] == "NEW") { echo ' minlength="2" '; } ?> value="<?php echo $customerDatas["customersKundennummer"]; ?>" maxlength="<?php echo (LENGTH_CUSTOMER_NUMBER + 3); ?>" <?php echo $readonly; ?> />

													<?php if($description != '') { echo '<p class="infoArea" style="float:right; width:auto;" >' . $description . '</p>'; } ?>
													<br>
													<?php
														$checked = ' checked="checked" ';
														if($_REQUEST["editID"] == "NEW") {
															$checked = ' checked="checked" ';
														}
														else {
															$checked = '';
															if($customerDatas["customersActive"] == '1'){
																$checked = ' checked="checked" ';
															}
														}
													?>
													<input type="checkbox" value="1" name="editCustomersActive" <?php echo $checked; ?> /><b>Kunde aktivieren</b>
													<?php
														if($_REQUEST["editID"] != "NEW") {
															if($customerDatas["customersActive"] == '1'){
																echo ' <span style="color:#009900; font-weight:bold;">[Kunde ist aktiviert]</span>';
															}
															else {
																echo ' <span style="color:#CC0000; font-weight:bold;">[Kunde ist nicht aktiviert]</span>';
															}
														}
													?>
													<?php
														if($_REQUEST["editID"] != "NEW" && $customerDatas["customersCompanyCountry"] != "81" && preg_match("/^[0-9]/", $customerDatas["customersKundennummer"])){
															echo '<p class="errorArea">' . 'ACHTUNG, die Kundennummer entspricht nicht dem Format f&uuml;r Auslandskunden!!!' . '</p>';
														}
													?>
												</td>
											</tr>
											<tr>
												<td><b>Fibu-Konto:</b></td>
												<td>
													<input type="text" name="customersTaxAccountID" id="customersTaxAccountID" class="inputField_100 readonly" readonly="readonly" value="<?php echo $customerDatas["customersTaxAccountID"]; ?>" />
													<span class="infotext">Wird automatisch nach Vorgaben des Steuerberaters erzeugt.</span>
												</td>
											</tr>
											<?php
												if($_COOKIE["mandator"] == 'bctr') {
											?>
											<!--
											<tr>
												<td><b>Kunden kopieren:</b></td>
												<td>
													<input type="checkbox" name="copyCustomerToExternMandatory" name="copyCustomerToExternMandatory" checked="checked" value="b3" /> Kundendaten zu <b>B3</b> &uuml;bernehmen?
												</td>
											</tr>
											-->
											<?php
												}
											?>
											<tr>
												<td><b>Firmenname:</b></td>
												<td><input type="text" name="editCustomersFirmenname"id="editCustomersFirmenname" class="inputField_510" value="<?php echo ($customerDatas["customersFirmenname"]); ?>" /></td>
											</tr>
											<tr>
												<td><b>Firmenname-Zusatz:</b></td>
												<td>
													<input type="text" name="editCustomersFirmennameZusatz"id="editCustomersFirmennameZusatz" class="inputField_510" value="<?php echo ($customerDatas["customersFirmennameZusatz"]); ?>" />
													<br /><span class="infotext">bei Amazon-Kunden hier &quot;Name des K&auml;ufers&quot; eintragen!!</span>
												</td>
											</tr>
											<!--
											<tr>
												<td><b>Vertreter dieses Kunden:</b></td>
												<td>
													<select name="editCustomersVertreter" id="editCustomersVertreter" class="inputSelect_510">
														<option value="0"> - </option>
														<?php
															#if(!empty($arrAgentDatas2)) {
																#foreach($arrAgentDatas2 as $thisKey => $thisValue) {
																	#$selected = '';
																	// if($customerDatas["customersTyp"] > 0) {
																	#if(1) {
																		#if($thisKey == $customerDatas["customersVertreterID"]) {
																			#$selected = ' selected="selected" ';
																		#}
																	#}
																	#echo '
																		#<option value="' . $thisKey . '" '.$selected.' >' . '' . ($arrAgentDatas2[$thisKey]["customersFirmenname"]) . ' | ' . $arrAgentDatas2[$thisKey]["customersKundennummer"] . ' [' . $arrCustomerTypeDatas[$arrAgentDatas2[$thisKey]["customersTyp"]]["customerTypesName"] . ']' . '</option>';
																#}
															#}
														?>
													</select>
												</td>
											</tr>
											-->
											<?php
												echo '<tr>';
												echo '<td><b>Vertreter dieses Kunden:</b></td>';
												echo '<td>';
												if($customerDatas["customersVertreterID"] > 0) {
													if($arrAgentDatas2[$customerDatas["customersVertreterID"]]["customersFirmenname"] !="") {
														$thisSalesmanValues = ($arrAgentDatas2[$customerDatas["customersVertreterID"]]["customersFirmenname"]) . ' | ' . $arrAgentDatas2[$customerDatas["customersVertreterID"]]["customersKundennummer"] . ' [' . $arrCustomerTypeDatas[$arrAgentDatas2[$customerDatas["customersVertreterID"]]["customersTyp"]]["customerTypesName"] . ']';
													}
												}
												else {
													$thisSalesmanValues = "";
												}

												echo '<input type="text" name="editCustomersVertreterName" id="editCustomersVertreterName" class="inputField_510" value="' . $thisSalesmanValues . '" />';
												echo '<input type="hidden" name="editCustomersVertreterID" id="editCustomersVertreterID" value="' . ($customerDatas["customersVertreterID"]) . '" />';

												#echo ' <img src="layout/menueIcons/menueQuicklinks/distribution.png" width="20" height="20" class="buttonOpenZipcodesToSalesman" /> ';

												if($_REQUEST["editID"] != "NEW" && $customerDatas["customersCompanyCountry"] == '81'){
													// BOF GET SALESMAN TO ZIPCODE
														$thisPlzArea = substr($customerDatas["customersCompanyPLZ"], 0, 2);
													// EOF GET SALESMAN TO ZIPCODE
													echo '<br />';
													echo '<p class="infoArea" style="width:502px;" ><b>Unser Au&szlig;endienst-Vertreter im Gebiet ' . $thisPlzArea . ': </b> ' . $arrRelationSalesmenZipcodeDatas[$thisPlzArea]["kundenname"] . ' (K-NR: ' . $arrRelationSalesmenZipcodeDatas[$thisPlzArea]["kundennummer"] . ')</p>';
												}

												echo '</td>';
												echo '</tr>';
											?>
											<tr>
												<td><b>Kunden-Typ:</b></td>
												<td>
													<select name="editCustomersTyp" id="editCustomersTyp" class="inputSelect_510">
														<option value=""> - </option>
														<?php
															if(!empty($arrCustomerTypeDatas)) {
																foreach($arrCustomerTypeDatas as $thisKey => $thisValue) {
																	$selected = '';
																	if($thisKey == $customerDatas["customersTyp"]) {
																		$selected = ' selected ';
																	}
																	echo '
																		<option value="' . $thisKey . '" '.$selected.' >' . ($arrCustomerTypeDatas[$thisKey]["customerTypesName"]). ' (' . $arrCustomerTypeDatas[$thisKey]["customerTypesShortName"] . ')' . '</option>
																	';
																}
															}
														?>
													</select>
												</td>
											</tr>
											<tr>
												<td><b>Mutter-Firma:</b></td>
												<td>
													<input type="text" name="editCustomersParentCompanyNumber" id="editCustomersParentCompanyNumber" class="inputField_70" value="<?php echo $arrCustomerParentCompanyDatas["parentCompanyNumber"]; ?>" />
													<input type="hidden" name="editCustomersParentCompanyID" id="editCustomersParentCompanyID" value="<?php echo $arrCustomerParentCompanyDatas["parentCompanyID"]; ?>" />
													<?php
														if(!empty($arrCustomerParentCompanyDatas)){
															echo ' <span class="inputArea">';
															echo '<a href="' . PAGE_EDIT_CUSTOMER . '?editCustomerNumber=' . $arrCustomerParentCompanyDatas["parentCompanyNumber"] . '" title="Kunden-Daten anzeigen">' . $arrCustomerParentCompanyDatas["parentCompanyName"] . '</a>';
															echo ' &bull; ' . $arrCustomerParentCompanyDatas["parentCompanyStrasse"] . ' ' . $arrCustomerParentCompanyDatas["parentCompanyHausnummer"];
															echo ' &bull; ' . $arrCustomerParentCompanyDatas["parentCompanyPLZ"] . ' ' . $arrCustomerParentCompanyDatas["parentCompanyOrt"];
															echo ' &bull; ' . $arrCountryTypeDatas[$arrCustomerParentCompanyDatas["parentCompanyCountry"]]["countries_name"];
															echo '</span>';
														}
													?>
												</td>
											</tr>
											<tr>
												<td><b>Tochter-Firmen:</b></td>
												<td>
													<?php
														if(!empty($arrCustomerChildCompanyDatas)){
															echo '<ul>';
															foreach($arrCustomerChildCompanyDatas as $thisKey => $thisValue){
																echo '<li>';
																	echo ' <span class="infotext">';
																	echo '<a href="' . PAGE_EDIT_CUSTOMER . '?editCustomerNumber=' . $thisValue["childCompanyNumber"] . '" title="Kunden-Daten anzeigen">' . $thisValue["childCompanyNumber"] . ' &bull; ' . $thisValue["childCompanyName"] . '</a>';
																	echo ' &bull; ' . $thisValue["childCompanyStrasse"] . ' ' . $thisValue["childCompanyHausnummer"];
																	echo ' &bull; ' . $thisValue["childCompanyPLZ"] . ' ' . $thisValue["childCompanyOrt"];
																	echo ' &bull; ' . $arrCountryTypeDatas[$thisValue["childCompanyCountry"]]["countries_name"];
																	echo '</span>';
																echo '</li>';
															}
															echo '</ul>';
														}
													?>
												</td>
											</tr>
											<tr>
												<td><b>Kunde seit:</b></td>
												<td>
													<input type="text" name="editCustomersEntryDate" id="editCustomersEntryDate" maxlength="2" class="inputField_100" readonly="readonly" value="<?php if($customerDatas["customersEntryDate"] == "") { echo date("d.m.Y"); } else { echo formatDate($customerDatas["customersEntryDate"], "display"); } ?>" />
												</td>
											</tr>
											<?php if ($_REQUEST["editID"] != "NEW") { ?>
											<tr>
												<td><b>Zahlungsmoral:</b></td>
												<td>
													<div class="inputArea1">
														<span class="checkBoxElement1" <?php if($customerDatas["customersInvoicesNotPurchased"] == '1' ) { echo 'style="background-color:#FF7F84;" '; } else { echo 'style="background-color:#00FF00;" '; } ?>>
															<input type="checkbox" name="editCustomersInvoicesNotPurchased" id="editCustomersInvoicesNotPurchased" <?php if($customerDatas["customersInvoicesNotPurchased"] == '1' ) { echo 'checked="checked" '; $questionMark = ''; } else { $questionMark = '?'; } ?> value="1" /> Kein Rechnungsankauf von TEBA <?php echo $questionMark; ?>
														</span>
														<span class="checkBoxElementSeparator">&nbsp;</span>
														<span class="checkBoxElement1" <?php if($customerDatas["customersBadPaymentBehavior"] == '1' ) { echo 'style="background-color:#FF7F84;" '; } else { echo 'style="background-color:#00FF00;" '; } ?>>
															<input type="checkbox" name="editCustomersBadPaymentBehavior" id="editCustomersBadPaymentBehavior" <?php if($customerDatas["customersBadPaymentBehavior"] == '1' ) { echo 'checked="checked" '; $questionMark = ''; } else { $questionMark = '?'; } ?> value="1" /> Kunde zahlt unp&uuml;nktlich <?php echo $questionMark; ?>
														</span>
														<div class="clear"></div>
													</div>
												</td>
											</tr>
											<?php } ?>
											<tr>
												<td><b>Firmeninhaber 1:</b></td>
												<td>
													<select name="editCustomersFirmenInhaberAnrede" id="editCustomersFirmenInhaberAnrede" class="inputSelect_70">
														<option value=""> - </option>
														<?php
															if(!empty($arrSalutationTypeDatas)) {
																foreach($arrSalutationTypeDatas as $thisKey => $thisValue) {
																	$selected = '';
																	if($thisKey == $customerDatas["customersFirmenInhaberAnrede"]) {
																		$selected = ' selected ';
																	}
																	echo '
																		<option value="' . $thisKey . '" '.$selected.' >' . ($arrSalutationTypeDatas[$thisKey]["salutationTypesName"]). '</option>
																	';
																}
															}
														?>
													</select> -
													<input type="text" name="editCustomersFirmenInhaberVorname" id="editCustomersFirmenInhaberVorname" class="inputField_205" value="<?php echo ($customerDatas["customersFirmenInhaberVorname"]); ?>" /> &nbsp;
													<input type="text" name="editCustomersFirmenInhaberNachname" id="editCustomersFirmenInhaberNachname" class="inputField_205" value="<?php echo ($customerDatas["customersFirmenInhaberNachname"]); ?>" />
													<br /><span class="infotext">(Anrede / Vorname / Nachname)</span>
												</td>
											</tr>

											<tr>
												<td><b>Firmeninhaber 2:</b></td>
												<td>
													<select name="editCustomersFirmenInhaber2Anrede" id="editCustomersFirmenInhaber2Anrede" class="inputSelect_70">
														<option value=""> - </option>
														<?php
															if(!empty($arrSalutationTypeDatas)) {
																foreach($arrSalutationTypeDatas as $thisKey => $thisValue) {
																	$selected = '';
																	if($thisKey == $customerDatas["customersFirmenInhaber2Anrede"]) {
																		$selected = ' selected ';
																	}
																	echo '
																		<option value="' . $thisKey . '" '.$selected.' >' . ($arrSalutationTypeDatas[$thisKey]["salutationTypesName"]). '</option>
																	';
																}
															}
														?>
													</select> -
													<input type="text" name="editCustomersFirmenInhaber2Vorname" id="editCustomersFirmenInhaber2Vorname" class="inputField_205" value="<?php echo ($customerDatas["customersFirmenInhaber2Vorname"]); ?>" /> &nbsp;
													<input type="text" name="editCustomersFirmenInhaber2Nachname" id="editCustomersFirmenInhaber2Nachname" class="inputField_205" value="<?php echo ($customerDatas["customersFirmenInhaber2Nachname"]); ?>" />
													<br /><span class="infotext">(Anrede / Vorname / Nachname)</span>
												</td>
											</tr>

											<tr>
												<td><b>UmsatzsteuerID (UstID):</b></td>
												<td>
													<input type="text" name="editCustomersUstID" id="editCustomersUstID" class="inputField_160" value="<?php echo $customerDatas["customersUstID"]; ?>" />
													<?php
														if($customerDatas["customersUstID"] != "") {
															#DEFINE('COMPANY_VAT_LIVE_CHECK', true);
															require_once('inc/vat_validation.class.php');

															$vatID = new vat_validation($customerDatas["customersUstID"], $arrCountryTypeDatas[$customerDatas["customersCompanyCountry"]]);
															$customers_vat_id_status = $vatID->vat_info['vat_id_status'];
															$customers_vat_id_message = $vatID->getVatStatusResult($customers_vat_id_status);
															echo $customers_vat_id_message;
														}
													?>
												</td>
											</tr>
											<tr>
												<td><b>Kunden-Gruppe:</b></td>
												<td>
													<div class="inputArea1">
													<?php
														if(!empty($arrCustomerGroupDatas)) {
															$thisArrCustomerGroups = explode(";", stripslashes($customerDatas["customersGruppe"]));
															foreach($arrCustomerGroupDatas as $thisKey => $thisValue) {
																$checked = '';
																if(!empty($thisArrCustomerGroups)) {
																	if(in_array($thisKey, $thisArrCustomerGroups)) {
																		$checked = ' checked ';
																	}
																}
																echo '<span class="checkBoxElement1">';
																echo '<input type="checkbox" name="editCustomersGruppe[]" value="' . $thisKey . '" '.$checked.'/> ';
																echo $arrCustomerGroupDatas[$thisKey]["customerGroupsName"];
																echo '</span>';
															}
														}
													?>
													</div>
												</td>
											</tr>
											<tr>
												<td><b>Firmenansprechpartner 1:</b></td>
												<td>
													<select name="editCustomersAnsprechpartner1Anrede" id="editCustomersAnsprechpartner1Anrede" class="inputSelect_70">
														<option value=""> - </option>
														<?php
															if(!empty($arrSalutationTypeDatas)) {
																foreach($arrSalutationTypeDatas as $thisKey => $thisValue) {
																	$selected = '';
																	if($thisKey == $customerDatas["customersAnsprechpartner1Anrede"]) {
																		$selected = ' selected ';
																	}
																	echo '
																		<option value="' . $thisKey . '" '.$selected.' >' . ($arrSalutationTypeDatas[$thisKey]["salutationTypesName"]). '</option>
																	';
																}
															}
														?>
													</select> -
													<input type="text" name="editCustomersAnsprechpartner1Vorname" id="editCustomersAnsprechpartner1Vorname" class="inputField_205" value="<?php echo ($customerDatas["customersAnsprechpartner1Vorname"]); ?>" /> &nbsp;
													<input type="text" name="editCustomersAnsprechpartner1Nachname" id="editCustomersAnsprechpartner1Nachname" class="inputField_205" value="<?php echo ($customerDatas["customersAnsprechpartner1Nachname"]); ?>" />
													<br /><span class="infotext">(Anrede / Vorname / Nachname)</span>
												</td>
											</tr>
											<tr>
												<td><b>Firmenansprechpartner 2:</b></td>
												<td>
													<select name="editCustomersAnsprechpartner2Anrede" id="editCustomersAnsprechpartner2Anrede" class="inputSelect_70">
														<option value=""> - </option>
														<?php
															if(!empty($arrSalutationTypeDatas)) {
																foreach($arrSalutationTypeDatas as $thisKey => $thisValue) {
																	$selected = '';
																	if($thisKey == $customerDatas["customersAnsprechpartner2Anrede"]) {
																		$selected = ' selected ';
																	}
																	echo '
																		<option value="' . $thisKey . '" '.$selected.' >' . ($arrSalutationTypeDatas[$thisKey]["salutationTypesName"]). '</option>
																	';
																}
															}
														?>
													</select> -
													<input type="text" name="editCustomersAnsprechpartner2Vorname" id="editCustomersAnsprechpartner2Vorname" class="inputField_205" value="<?php echo ($customerDatas["customersAnsprechpartner2Vorname"]); ?>" /> &nbsp;
													<input type="text" name="editCustomersAnsprechpartner2Nachname" id="editCustomersAnsprechpartner2Nachname" class="inputField_205" value="<?php echo ($customerDatas["customersAnsprechpartner2Nachname"]); ?>" />
													<br /><span class="infotext">(Anrede / Vorname / Nachname)</span>
												</td>
											</tr>
											<tr>
												<td><b>Telefon 1:</b></td>
												<td>
													<input type="text" name="editCustomersTelefon1" id="editCustomersTelefon1" class="inputField_510" value="<?php echo formatPhoneNumber($customerDatas["customersTelefon1"], $arrPhoneCodes); ?>" />
													<?php
														if($customerDatas["customersTelefon1"] != "") {
															echo createPhoneCallLink($customerDatas["customersTelefon1"]);
														}
													?>
												</td>
											</tr>
											<tr>
												<td><b>Telefon 2:</b></td>
												<td>
													<input type="text" name="editCustomersTelefon2" id="editCustomersTelefon2" class="inputField_510" value="<?php echo formatPhoneNumber($customerDatas["customersTelefon2"], $arrPhoneCodes); ?>" />
													<?php
														if($customerDatas["customersTelefon2"] != "") {
															echo createPhoneCallLink($customerDatas["customersTelefon2"]);
														}
													?>
												</td>
											</tr>
											<tr>
												<td><b>Mobil 1:</b></td>
												<td><input type="text" name="editCustomersMobil1" id="editCustomersMobil1" class="inputField_510" value="<?php echo formatPhoneNumber($customerDatas["customersMobil1"]); ?>" /></td>
											</tr>
											<tr>
												<td><b>Mobil 2:</b></td>
												<td><input type="text" name="editCustomersMobil2" id="editCustomersMobil2" class="inputField_510" value="<?php echo formatPhoneNumber($customerDatas["customersMobil2"]); ?>" /></td>
											</tr>
											<tr>
												<td><b>Fax 1:</b></td>
												<td><input type="text" name="editCustomersFax1" id="editCustomersFax1" class="inputField_510" value="<?php echo formatPhoneNumber($customerDatas["customersFax1"], $arrPhoneCodes); ?>" /></td>
											</tr>
											<tr>
												<td><b>Fax 2:</b></td>
												<td><input type="text" name="editCustomersFax2" id="editCustomersFax2" class="inputField_510" value="<?php echo formatPhoneNumber($customerDatas["customersFax2"], $arrPhoneCodes); ?>" /></td>
											</tr>
											<tr>
												<td><b>Mail-Adresse 1:</b></td>
												<td>
													<input type="text" name="editCustomersMail1" id="editCustomersMail1" class="inputField_510" value="<?php echo ($customerDatas["customersMail1"]); ?>" />
													<?php
														if($customerDatas["customersMail1"] != "") {
															echo ' <a href="mailto:' . $customerDatas["customersMail1"] . '"><img src="layout/icons/iconMail.gif" width="16" height="14" alt="Mail-Kontakt" title="Dem Kunden eine E-Mail schreiben" /></a>';
														}
													?>
												</td>
											</tr>
											<tr>
												<td><b>Mail-Adresse 2:</b></td>
												<td>
													<input type="text" name="editCustomersMail2" id="editCustomersMail2" class="inputField_510" value="<?php echo ($customerDatas["customersMail2"]); ?>" />
													<?php
														if($customerDatas["customersMail2"] != "") {
															echo ' <a href="mailto:' . $customerDatas["customersMail2"] . '"><img src="layout/icons/iconMail.gif" width="16" height="14" alt="Mail-Kontakt" title="Dem Kunden eine E-Mail schreiben" /></a>';
														}
													?>
												</td>
											</tr>
											<tr>
												<td><b>Homepage:</b></td>
												<td>
													<input type="text" name="editCustomersHomepage" id="editCustomersHomepage" class="inputField_510" value="<?php echo ($customerDatas["customersHomepage"]); ?>" />
													<?php
														if($customerDatas["customersHomepage"] != ''){
															echo '<a href="http://' . $customerDatas["customersHomepage"] . '" target="_blank"><img src="layout/icons/iconWeb.gif" width="16" height="16" alt="Homepage" title="Zur Homepage" /></a>';
														}
													?>
												</td>
											</tr>
										</table>
									</fieldset>

									<fieldset id="Firmenadresse">
										<legend>Firmenanschrift</legend>
										<p>
										<input type="checkbox" name="Rechnungsadresse_Firmenadresse" id="Rechnungsadresse_Firmenadresse" value="1" <?php if($customerDatas["customersRechnungsadresseStrasse"] == $customerDatas["customersCompanyStrasse"] && $customerDatas["customersUseSalesmanInvoiceAdress"] != '1') { echo ' checked="checked" '; } ?> /> <b>Firmenadresse als Rechnungsadresse verwenden?</b>
										<input type="checkbox" name="Lieferadresse_Firmenadresse" id="Lieferadresse_Firmenadresse" value="1" <?php if($customerDatas["customersLieferadresseStrasse"] == $customerDatas["customersCompanyStrasse"] && $customerDatas["editCustomersUseSalesmanDeliveryAdress"] != '1') { echo ' checked="checked" '; } ?> /> <b>Firmenadresse als Lieferadresse verwenden?</b>
										</p>
										<table border="0" width="100%" cellspacing="0" cellpadding="0">
											<tr>
												<td style="width:200px;"><b>Stra&szlig;e / Hausnummer:</b></td>
												<td>
													<input type="text" name="editCustomersCompanyStrasse" id="editCustomersFirmenadresseStrasse" class="inputField_427" value="<?php echo ($customerDatas["customersCompanyStrasse"]); ?>" /> /
													<input type="text" name="editCustomersCompanyHausnummer" id="editCustomersFirmenadresseHausnummer" class="inputField_70" value="<?php echo ($customerDatas["customersCompanyHausnummer"]); ?>" />
												</td>
											</tr>
											<tr>
												<td><b>PLZ / Ort:</b></td>
												<td>
													<input type="text" name="editCustomersCompanyPLZ" id="editCustomersFirmenadressePLZ" class="inputField_70" value="<?php echo ($customerDatas["customersCompanyPLZ"]); ?>" /> /
													<input type="text" name="editCustomersCompanyOrt" id="editCustomersFirmenadresseOrt" class="inputField_427" value="<?php echo ($customerDatas["customersCompanyOrt"]); ?>" /></td>
											</tr>
											<tr>
												<td><b>Land:</b></td>
												<td>
													<select name="editCustomersCompanyCountry" id="editCustomersFirmenadresseLand" class="inputSelect_510" <?php if ($_REQUEST["editID"] == "NEW") {} ?> >
														<option value=""> - </option>
														<?php
															if(!empty($arrCountryTypeDatas)) {
																foreach($arrCountryTypeDatas as $thisKey => $thisValue) {
																	$selected = '';
																	if($_REQUEST["editID"] == "NEW" && $thisKey == 81) {
																		$selected = ' selected="selected" ';
																	}
																	else if($thisKey == $customerDatas["customersCompanyCountry"]) {
																		$selected = ' selected="selected" ';
																	}
																	echo '<option value="' . $thisKey . '" '.$selected.' >' . ($arrCountryTypeDatas[$thisKey]["countries_name"]). ' [' . $arrCountryTypeDatas[$thisKey]["countries_iso_code_2"] . ']</option>';
																}
															}
														?>
													</select>
												</td>
											</tr>
										</table>
									</fieldset>

									<fieldset id="Rechnungsadresse">
										<legend>Rechnungsanschrift</legend>
										<div>
											<?php
												if($customerDatas["customersUseSalesmanInvoiceAdress"] == '1'){
													$checked = ' checked="checked" ';
													$disabled = ' disabled="disabled" ';
												}
												else {
													$checked = '';
													$disabled = '';
												}
											?>
											<p>
											<input type="checkbox" name="editCustomersUseSalesmanInvoiceAdress" id="editCustomersUseSalesmanInvoiceAdress" value="1" <?php echo $checked; ?> /> <label for="editCustomersUseSalesmanInvoiceAdress">Anschrift des Vertreters verwenden?</label>
											</p>
										</div>
										<table border="0" width="100%" cellspacing="0" cellpadding="0">
											<tr>
												<td style="width:200px;"><b>Firma / Name:</b></td>
												<td>
													<input type="text" name="editCustomersRechnungsadresseFirmenname" id="editCustomersRechnungsadresseFirmenname" class="inputField_510" value="<?php echo ($customerDatas["customersRechnungsadresseFirmenname"]); ?>" <?php echo $disabled; ?> />
												</td>
											</tr>
											<tr>
												<td style="width:200px;"><b>Firma - Zusatz:</b></td>
												<td>
													<input type="text" name="editCustomersRechnungsadresseFirmennameZusatz" id="editCustomersRechnungsadresseFirmennameZusatz" class="inputField_510" value="<?php echo ($customerDatas["customersRechnungsadresseFirmennameZusatz"]); ?>" <?php echo $disabled; ?> />
												</td>
											</tr>
											<tr>
												<td><b>Stra&szlig;e / Hausnummer:</b></td>
												<td>
													<input type="text" name="editCustomersRechnungsadresseStrasse" id="editCustomersRechnungsadresseStrasse" class="inputField_427" value="<?php echo ($customerDatas["customersRechnungsadresseStrasse"]); ?>" <?php echo $disabled; ?> /> /
													<input type="text" name="editCustomersRechnungsadresseHausnummer" id="editCustomersRechnungsadresseHausnummer" class="inputField_70" value="<?php echo ($customerDatas["customersRechnungsadresseHausnummer"]); ?>" <?php echo $disabled; ?> />
												</td>
											</tr>
											<tr>
												<td><b>PLZ / Ort:</b></td>
												<td>
													<input type="text" name="editCustomersRechnungsadressePLZ" id="editCustomersRechnungsadressePLZ" class="inputField_70" value="<?php echo ($customerDatas["customersRechnungsadressePLZ"]); ?>" <?php echo $disabled; ?> /> /
													<input type="text" name="editCustomersRechnungsadresseOrt" id="editCustomersRechnungsadresseOrt" class="inputField_427" value="<?php echo ($customerDatas["customersRechnungsadresseOrt"]); ?>" <?php echo $disabled; ?> /></td>
											</tr>
											<tr>
												<td><b>Land:</b></td>
												<td>
													<select name="editCustomersRechnungsadresseLand" id="editCustomersRechnungsadresseLand" class="inputSelect_510" <?php echo $disabled; ?> >
														<option value=""> - </option>
														<?php
															if(!empty($arrCountryTypeDatas)) {
																foreach($arrCountryTypeDatas as $thisKey => $thisValue) {
																	$selected = '';
																	if($_REQUEST["editID"] == "NEW" && $thisKey == 81) {
																		$selected = ' selected ';
																	}
																	else if($thisKey == $customerDatas["customersRechnungsadresseLand"]) {
																		$selected = ' selected ';
																	}
																	echo '<option value="' . $thisKey . '" '.$selected.' >' . ($arrCountryTypeDatas[$thisKey]["countries_name"]). ' [' . $arrCountryTypeDatas[$thisKey]["countries_iso_code_2"] . ']</option>';
																}
															}
														?>
													</select>
												</td>
											</tr>
										</table>
									</fieldset>

									<fieldset id="Lieferadresse">
										<legend>Lieferanschrift</legend>
										<div>
											<?php
												if($customerDatas["customersUseSalesmanDeliveryAdress"] == '1'){
													$checked = ' checked="checked" ';
													$disabled = ' disabled="disabled" ';
												}
												else {
													$checked = '';
													$disabled = '';
												}
											?>
											<p>
											<input type="checkbox" name="editCustomersUseSalesmanDeliveryAdress" id="editCustomersUseSalesmanDeliveryAdress" value="1" <?php echo $checked; ?> /> <label for="editCustomersUseSalesmanDeliveryAdress">Anschrift des Vertreters verwenden?</label>
											</p>
										</div>
										<table border="0" width="100%" cellspacing="0" cellpadding="0">
											<tr>
												<td style="width:200px;"><b>Firma / Name:</b></td>
												<td>
													<input type="text" name="editCustomersLieferadresseFirmenname" id="editCustomersLieferadresseFirmenname" class="inputField_510" value="<?php echo ($customerDatas["customersLieferadresseFirmenname"]); ?>" <?php echo $disabled; ?> />
												</td>
											</tr>
											<tr>
												<td style="width:200px;"><b>Firma - Zusatz:</b></td>
												<td>
													<input type="text" name="editCustomersLieferadresseFirmennameZusatz" id="editCustomersLieferadresseFirmennameZusatz" class="inputField_510" value="<?php echo ($customerDatas["customersLieferadresseFirmennameZusatz"]); ?>" <?php echo $disabled; ?> />
												</td>
											</tr>
											<tr>
												<td><b>Stra&szlig;e / Hausnummer:</b></td>
												<td>
													<input type="text" name="editCustomersLieferadresseStrasse" id="editCustomersLieferadresseStrasse" class="inputField_427" value="<?php echo ($customerDatas["customersLieferadresseStrasse"]); ?>" <?php echo $disabled; ?> /> /
													<input type="text" name="editCustomersLieferadresseHausnummer" id="editCustomersLieferadresseHausnummer" class="inputField_70" value="<?php echo ($customerDatas["customersLieferadresseHausnummer"]); ?>" <?php echo $disabled; ?> />
												</td>
											</tr>
											<tr>
												<td><b>PLZ / Ort:</b></td>
												<td>
													<input type="text" name="editCustomersLieferadressePLZ" id="editCustomersLieferadressePLZ" class="inputField_70" value="<?php echo ($customerDatas["customersLieferadressePLZ"]); ?>" <?php echo $disabled; ?> /> /
													<input type="text" name="editCustomersLieferadresseOrt" id="editCustomersLieferadresseOrt" class="inputField_427" value="<?php echo ($customerDatas["customersLieferadresseOrt"]); ?>" <?php echo $disabled; ?> />
												</td>
											</tr>
											<tr>
												<td><b>Land:</b></td>
												<td>
													<select name="editCustomersLieferadresseLand" id="editCustomersLieferadresseLand" class="inputSelect_510" <?php echo $disabled; ?> >
														<option value=""> - </option>
														<?php
															if(!empty($arrCountryTypeDatas)) {
																foreach($arrCountryTypeDatas as $thisKey => $thisValue) {
																	$selected = '';
																	if($_REQUEST["editID"] == "NEW" && $thisKey == 81) {
																		$selected = ' selected ';
																	}
																	else if($thisKey == $customerDatas["customersLieferadresseLand"]) {
																		$selected = ' selected ';
																	}
																	echo '<option value="' . $thisKey . '" '.$selected.' >' . ($arrCountryTypeDatas[$thisKey]["countries_name"]). ' [' . $arrCountryTypeDatas[$thisKey]["countries_iso_code_2"] . ']</option>';
																}
															}
														?>
													</select>
												</td>
											</tr>
										</table>
									</fieldset>

									<fieldset>
										<legend>Bankdaten</legend>
										<table border="0" width="100%" cellspacing="0" cellpadding="0">
											<tr>
												<td><b>Bezahlart:</b></td>
												<td>
													<select name="editCustomersBezahlart" id="editCustomersBezahlart" class="inputSelect_510">
														<option value=""> - </option>
														<?php
															if(!empty($arrPaymentTypeDatas)) {
																foreach($arrPaymentTypeDatas as $thisKey => $thisValue) {
																	$selected = '';
																	if($_REQUEST["editID"] == "NEW" && $thisKey == 1) {
																		$selected = ' selected ';
																	}
																	else if($thisKey == $customerDatas["customersBezahlart"]) {
																		$selected = ' selected ';
																	}
																	echo '
																		<option value="' . $thisKey . '" '.$selected.' >' . ($arrPaymentTypeDatas[$thisKey]["paymentTypesName"]). '</option>
																	';
																}
															}
														?>
													</select>
												</td>
											</tr>
											<tr>
												<td><b>Zahlungskondition:</b></td>
												<td>
													<select name="editCustomersZahlungskondition" id="editCustomersZahlungskondition" class="inputSelect_510">
														<option value=""> - </option>
														<?php
															if(!empty($arrPaymentConditionDatas)) {
																foreach($arrPaymentConditionDatas as $thisKey => $thisValue) {
																	$selected = '';
																	if($_REQUEST["editID"] == "NEW" && $thisKey == 5) {
																		$selected = ' selected ';
																	}
																	else if($thisKey == $customerDatas["customersZahlungskondition"]) {
																		$selected = ' selected ';
																	}
																	echo '
																		<option value="' . $thisKey . '" '.$selected.' >' . ($arrPaymentConditionDatas[$thisKey]["paymentConditionsName"]). '</option>
																	';
																}
															}
														?>
													</select>
												</td>
											</tr>
											<tr>
												<td style="width:200px;"><b>Kontoinhaber:</b></td>
												<td><input type="text" name="editCustomersKontoinhaber" id="editCustomersKontoinhaber" class="inputField_510" value="<?php echo ($customerDatas["customersKontoinhaber"]); ?>" /></td>
											</tr>
											<tr>
												<td style="width:200px;"><b>Bankname:</b></td>
												<td><input type="text" name="editCustomersBankName" id="editCustomersBankName" class="inputField_510" value="<?php echo ($customerDatas["customersBankName"]); ?>" /></td>
											</tr>
											<tr>
												<td><b>Bankleitzahl:</b></td>
												<td><input type="text" name="editCustomersBankLeitzahl" id="editCustomersBankLeitzahl" class="inputField_510" value="<?php echo ($customerDatas["customersBankLeitzahl"]); ?>" /></td>
											</tr>
											<tr>
												<td><b>Kontonummer:</b></td>
												<td><input type="text" name="editCustomersBankKontonummer" id="editCustomersBankKontonummer" class="inputField_510" value="<?php echo ($customerDatas["customersBankKontonummer"]); ?>" /></td>
											</tr>
											<tr>
												<td><b>IBAN:</b></td>
												<td><input type="text" name="editCustomersBankIBAN" id="editCustomersBankIBAN" class="inputField_510" value="<?php echo ($customerDatas["customersBankIBAN"]); ?>" /></td>
											</tr>
											<tr>
												<td><b>BIC:</b></td>
												<td><input type="text" name="editCustomersBankBIC" id="editCustomersBankBIC" class="inputField_510" value="<?php echo ($customerDatas["customersBankBIC"]); ?>" /></td>
											</tr>
										</table>
									</fieldset>

									<fieldset>
										<legend>Sonstige Daten</legend>
										<table border="0" width="100%" cellspacing="0" cellpadding="0">
											<tr>
												<td style="width:200px;"><b>Standard-Rabatt:</b></td>
												<td>
													<?php
														if($customerDatas["customersRabatt"] == "") {
															$customerDatas["customersRabatt"] = 0;
														}
													?>
													<input type="text" name="editCustomersRabatt" id="editCustomersRabatt" class="inputField_40" value="<?php echo number_format($customerDatas["customersRabatt"], 2, ',', ''); ?>" />

													<select name="editCustomersRabattType" name="editCustomersRabattType" class="inputSelect_40">
														<option value="percent" style="font-weight:bold;" <?php if($customerDatas["customersRabattType"] == 'percent') { echo ' selected="selected" '; } ?> >%</option>
														<option value="fixed" style="font-weight:bold;" <?php if($customerDatas["customersRabattType"] == 'fixed') { echo ' selected="selected" '; } ?>>&euro;</option>
													</select>
													(Rabatt auf gesamten Nettobetrag)
												</td>
											</tr>
											<!--
											<tr>
												<td><b>Artikel-Rabatte verwenden:</b></td>
												<td>
													<?php
														if($customerDatas["customersUseProductDiscount"] == 1){
															$checked = ' checked="checked" ';
														}
														else {
															$checked = '';
														}
													?>
													<input type="checkbox" name="editCustomersUseProductDiscount" id="editCustomersUseProductDiscount" value="1" <?php echo $checked; ?> /> ja
												</td>
											</tr>
											<tr>
												<td><b>Artikel-MwSt. verwenden:</b></td>
												<td>
													<?php
														if($customerDatas["customersUseProductMwst"] == 1){
															$checked = ' checked="checked" ';
														}
														else {
															$checked = '';
														}
													?>
													<input type="checkbox" name="editCustomersUseProductMwst" id="editCustomersUseProductMwst" value="1" <?php echo $checked; ?> /> ja
												</td>
											</tr>
											-->
											<tr>
												<td><b>Bemerkungen / Notizen:</b></td>
												<td><textarea name="editCustomersNotiz" id="editCustomersNotiz" rows="10" cols="20" class="inputTextarea_510x140" ><?php echo ($customerDatas["customersNotiz"]); ?></textarea></td>
											</tr>
										</table>
									</fieldset>
									<div class="actionButtonsArea">
										<?php
											if($arrGetUserRights["editCustomers"]) {
										?>
										<input type="submit" class="inputButton1 inputButtonGreen" name="storeDatas" value="Speichern" onclick="return checkFormDutyFields(this.form.name, <?php echo $jsonFormDutyFields; ?>);" />
										<?php
											if($_REQUEST["editID"] != "NEW")
											{
												if($checkCustomerNumberIsUsed > 0){
													$disableLink = '';
													$disableClass = '';
													$disableTitle = '';
													if($customerDatas["customersDatasUpdated"] != '1') {
														$disableLink = ' onclick="return false" ';
														$disableClass = ' disabled ';
														$disableTitle = ' gesperrt ';
													}
												}
										?>

										<?php
											if($checkCustomerNumberIsUsed < 1){
										?>
										&nbsp;
										<input type="submit" class="inputButton1 inputButtonRed" name="deleteDatas" value="Endg&uuml;ltig l&ouml;schen" onclick="return showWarning('Wollen Sie diesen Eintrag wirklich endgültig entfernen???\nOder soll der Kunde nur deaktiviert werden?');" />
										<?php
											}
										?>
										&nbsp;
										<?php
											}
										?>
										<input type="submit" class="inputButton1 inputButtonOrange" name="resetDatas" value="Abbrechen" onclick="return showWarning('Wollen Sie diesen Vorgang wirklich abbrechen??? ');" />
										<?php
											}
										?>
									</div>
								</form>
								<?php
									// BOF GET SALESMAN TO ZIPCODE
									$arrRelationSalesmenZipcodeDatas = getRelationSalesmenZipcode();
									if(!empty($arrRelationSalesmenZipcodeDatas)){
										echo '<div class="sideInfo">';
											echo '<div class="sideInfoHeader">PLZ | Au&szlig;endienst-Vertreter</div>';
											echo '<div class="sideInfoContent">';
											$count = 0;

											foreach($arrRelationSalesmenZipcodeDatas as $thisKey => $thisValue){
												$testClick = '';

												if($count%2 == 0){ $rowClass = 'row0'; }
												else { $rowClass = 'row1'; }

												echo '<div class="sideInfoItem ' . $rowClass . '" id="' . $thisValue["kundenID"] . '" title="Diesen Au&szlig;endienst-Mitarbeiter per Klick als Vertreter &uuml;bernehmen" >';
													echo '<span class="sideInfoTitle">' . $thisKey . '</span>';
													echo '<span class="sideInfoText" style="white-space:nowrap;">' . $thisValue["kundenname"] . '</span>';
												echo '</div>';
												$count++;
											}
											echo '</div>';
										echo '</div>';
									}
									// EOF GET SALESMAN TO ZIPCODE
								?>
							</div>
							<?php
								}
								if($_REQUEST["editID"] != "NEW")
								{
							?>

							<?php
								if($_REQUEST["tabID"] == "tabs-7") {
								if($customerDatas["customersNotiz"] != '') {
							?>
									<div id="tabs-7">
										<fieldset>
											<legend>Notizen</legend>
											<?php echo nl2br(htmlentities(utf8_decode($customerDatas["customersNotiz"]))); ?>
										</fieldset>
									</div>
							<?php
								}}
								if($_REQUEST["tabID"] == "tabs-4") {
								if(!empty($arrDocumentDatas)) {
							?>

							<div id="tabs-4">
								<fieldset style="width:90% !important;">
									<legend>Dokumente</legend>
									<?php
										if(!empty($arrDocumentDatas)) {
											foreach($arrDocumentDatas as $thisDocumentKey => $thisDocumentValue){

												if($thisDocumentKey == 'BR') { $thisDocumentUrl = PAGE_DISPLAY_LETTER; }
												else if($thisDocumentKey == 'RE') { $thisDocumentUrl = PAGE_DISPLAY_INVOICE; }
												else if($thisDocumentKey == 'AN') { $thisDocumentUrl = PAGE_DISPLAY_OFFER; }
												else if($thisDocumentKey == 'MA') { $thisDocumentUrl = PAGE_DISPLAY_REMINDER; }
												else if($thisDocumentKey == 'AB') { $thisDocumentUrl = PAGE_DISPLAY_CONFIRMATION; }
												else if($thisDocumentKey == 'LS') { $thisDocumentUrl = PAGE_DISPLAY_DELIVERY; }
												else if($thisDocumentKey == 'GU') { $thisDocumentUrl = PAGE_DISPLAY_CREDIT; }

												echo '<h2>' . $arrDocumentTypeDatas[$thisDocumentKey]["createdDocumentsTypesName2"] . ' (' . $thisDocumentKey . ')</h2>';

												echo '<div class="displayDocumentsArea" >';

												echo '<table border="0" cellpadding="0" cellspacing="0" style="width:99%;" class="displayOrders">';
												echo '<tr>';
												echo '<th style="width:30px !important;text-align:right;">#</th>';
												echo '<th style="width:66px !important;">Datum</th>';
												#echo '<th style="width:45px;">Kund.Nr</th>';
												echo '<th>Dokument-Empf&auml;ngername</th>';
												echo '<th style="width:110px !important;">Dokument &auml;ndern</th>';

												if($thisDocumentKey != "BR"){
													echo '<th style="width:50px !important;">Summe</th>';
													#echo '<th style="width:200px !important;">Dokument umwandeln / kopieren</th>';
													echo '<th style="width:110px !important;">Dokument umwandeln</th>';
												}
												else {
													echo '<th style="width:70px !important;">&nbsp;</th>';
													echo '<th style="width:90px !important;">&nbsp;</th>';
												}
												echo '<th style="width:100px !important;">Zahl-Status</th>';
												echo '<th style="width:60px !important;">Info</th>';
												echo '</tr>';

												$count = 0;
												foreach($thisDocumentValue as $thisKey => $thisValue){
													if($count%2 == 0){ $rowClass = 'row0'; }
													else { $rowClass = 'row1'; }

													if($thisDocumentValue[$thisKey]["orderDocumentsStatus"] == "1") {
														$rowClass2 = 'row2';
													}
													else if($thisDocumentValue[$thisKey]["orderDocumentsStatus"] == "2") {
														$rowClass2 = 'row3';
													}
													else if($thisDocumentValue[$thisKey]["orderDocumentsStatus"] == "3") {
														$rowClass2 = 'row6';
														$rowClass = 'row6';
													}
													else if($thisDocumentValue[$thisKey]["orderDocumentsStatus"] == "4") {
														$rowClass2 = 'row2';
													}
													else if($thisDocumentValue[$thisKey]["orderDocumentsStatus"] == "5") {
														$rowClass2 = 'row5';
													}
													else if($thisDocumentValue[$thisKey]["orderDocumentsStatus"] == "6") {
														$rowClass2 = 'row6';
													}
													else if($thisDocumentValue[$thisKey]["orderDocumentsStatus"] == "6") {
														$rowClass2 = 'row7';
													}

													echo '<tr class="'.$rowClass.'">';

													echo '<td style="text-align:right;"><b>' . ($count + 1). '.</b></td>';
													echo '<td>' . formatDate($thisDocumentValue[$thisKey]["orderDocumentsProcessingDate"], 'display') . '</td>';
													#echo '<td><a href="' . PAGE_EDIT_CUSTOMER . '?editCustomerNumber=' . $thisDocumentValue[$thisKey]["orderDocumentsCustomerNumber"] . '" title="Kunden anzeigen">' . $thisDocumentValue[$thisKey]["orderDocumentsCustomerNumber"] . '</a></td>';
													echo '<td>' . $thisDocumentValue[$thisKey]["orderDocumentsAddressCompany"] . '</td>';
													echo '<td>';

													echo '<form name="formSubmitEditDocument" method="post" action="' . PAGE_CREATE_DOCUMENT . '">';
													echo '<table width="100" cellpadding="0" cellspacing="0" class="noBorder">';
													echo '<tr>';
														echo '<td>';
															echo $thisDocumentValue[$thisKey]["orderDocumentsNumber"];
														echo '</td>';
														echo '<td>';
															echo '<input type="hidden" name="editID" value="' . $thisDocumentValue[$thisKey]["orderDocumentsCustomerNumber"] . '">';
															echo '<input type="hidden" name="originDocumentType" value="' . $thisDocumentKey . '">';
															echo '<input type="hidden" name="originDocumentID" value="' . $thisKey . '">';
															echo '<input type="hidden" name="originDocumentNumber" value="' . $thisDocumentValue[$thisKey]["orderDocumentsNumber"] . '">';
															echo '<input type="hidden" name="editDocType" value="' . $thisDocumentKey . '">';
															echo '<input type="image" src="layout/icons/iconEdit.gif" title="' . $arrDocumentTypeDatas[$thisDocumentKey]["createdDocumentsTypesName"] . ' ' . $thisDocumentValue[$thisKey]["orderDocumentsDocumentNumber"] . ' &auml;ndern" />';
														echo '</td>';
													echo '</tr>';
													echo '</table>';
													echo '</form>';

													echo '</td>';

													if($thisDocumentKey != "BR"){
														echo '<td style="text-align:right;white-space:nowrap;">' . convertDecimal($thisDocumentValue[$thisKey]["orderDocumentsTotalPrice"], 'display') . ' &euro;</td>';
														echo '<td>';
														echo $thisDocumentValue[$thisKey]["orderDocumentsNumber"];
														echo '<a href="' . $thisDocumentUrl . '?searchDocumentNumber=' . $thisDocumentValue[$thisKey]["orderDocumentsNumber"] . '">';
														echo '<img src="layout/icons/iconEdit.gif" width="16" height="16" title="Dokument kopieren oder umwandeln" alt="Dokument kopieren oder umwandeln" />';
														echo '</a>';
														echo '</td>';
													}
													else {
														echo '<td>';
														echo '</td>';
														echo '<td>';
														echo '</td>';
													}
													echo '<td class="'.$rowClass2.'">';
													echo $arrPaymentStatusTypeDatas[$thisDocumentValue[$thisKey]["orderDocumentsStatus"]]["paymentStatusTypesName"];
													echo '</td>';
													echo '<td>';
													echo '<span class="toolItem">';
													#echo 'xxx<a href="' . $_SERVER["PHP_SELF"] . '?editCustomerNumber=' . $thisDocumentValue[$thisKey]["orderDocumentsCustomerNumber"] . '&editID=' . $_REQUEST["editID"] . '&downloadFile=' . urlencode(basename($thisDocumentValue[$thisKey]["orderDocumentsDocumentPath"])) . '&documentType=' . $thisDocumentKey . '">' . '<img src="layout/icons/icon' . getFileType(basename($thisDocumentValue[$thisKey]["orderDocumentsDocumentPath"])) . '.gif" width="16" height="16" title="Dokument herunterladen" alt="Download" /></a>';
													echo '<a href="' . $_SERVER["PHP_SELF"] . '?editCustomerNumber=' . $customerDatas["customersKundennummer"] . '&editID=' . $_REQUEST["editID"] . '&downloadFile=' . urlencode(basename($thisDocumentValue[$thisKey]["orderDocumentsDocumentPath"])) . '&documentType=' . $thisDocumentKey . '">' . '<img src="layout/icons/icon' . getFileType(basename($thisDocumentValue[$thisKey]["orderDocumentsDocumentPath"])) . '.gif" width="16" height="16" title="Dokument herunterladen" alt="Download" /></a>';
													echo '</span>';
													echo '<span class="toolItem">';
													echo '<img src="layout/icons/iconEye.gif" class="buttonLoadOrderDetails" width="16" height="16" title="Details ansehen" alt="' . $thisDocumentKey . '#' .  $arrDocumentTypeDatas[$thisDocumentKey]["createdDocumentsTypesName2"] . '#' . $thisKey . '" />';
													echo '</span>';
													echo '<span class="toolItem">';
													echo '<img src="layout/icons/mailAttachement.png" class="buttonSendDocument" rel="' . urlencode(basename($thisDocumentValue[$thisKey]["orderDocumentsDocumentPath"])) . '#' . $thisDocumentKey . '" width="16" height="16" title="Dokument direkt per Mail versenden" alt="Dokument versenden" />';
													echo '</span>';

													echo '</td>';
													echo '</tr>';
													$count++;
												}

												echo '</table>';
												echo '</div>';

												echo '<hr />';
											}
										}
										else {
											$infoMessage .= 'Es sind keine Dokumente vorhanden.' . '<br />';
											displayMessages();
										}
									?>
									</fieldset>
								</div>

								<?php
									}
									}
									if($_REQUEST["tabID"] == "tabs-3") {
								?>
								<div id="tabs-3">
									<fieldset>
									<?php
										if(!empty($arrPdfDatas)) {
											foreach($arrPdfDatas as $thisKey1 => $thisValue1) {

												echo '<h2>Korrektur-Abz&uuml;ge</h2>';
												echo '<div class="displayDocumentsArea">';
												echo '<table border="0" cellpadding="0" cellspacing="0" width="100%" class="border">';
												echo '<tr>';
												#echo '<th style="width:90px;">Nummer</th>';
												echo '<th style="width:80px;">Datum</th>';
												echo '<th>Dateiname</th>';
												echo '<th style="width:60px;">Info</th>';
												#echo '<th style="width:50px;">Umwandeln in</th>';
												#echo '<th style="width:50px;">Kopieren in</th>';
												echo '</tr>';

												$count = 0;
												foreach($thisValue1 as $thisKey2 => $thisValue2) {
													if($count%2 == 0) {
														$thisClass = "row0";
													}
													else {
														$thisClass = "row1";
													}
													$arrTemp = explode("_", $thisValue2["createdDocumentsFilename"]);
													$arrThisFilename = array();
													for($i = 0 ; $i < (count($arrTemp) - 1); $i++) {
														$arrThisFilename[$i] = $arrTemp[$i];
													}
													$thisFilename = implode("_", $arrThisFilename);
													echo '<tr class="'.$thisClass.'">';
													#echo '<td>'.$thisValue2["createdDocumentsNumber"].'</td>';
													echo '<td style="white-space:nowrap;">'.preg_replace("/ /", ", ", formatDate($thisValue2["createdDocumentsTimeCreated"], 'display')).'</td>';
													echo '<td>' . substr(basename($thisFilename), 0, 100) . '</td>';
													echo '<td style="text-align:center;">';
													echo '<span class="toolItem">';
														echo '<a href="?editID=' . $_REQUEST["editID"] . '&documentType=' . $thisValue2["createdDocumentsType"] . '&downloadFile='.urlencode(basename($thisValue2["createdDocumentsFilename"])).'" title="Datei anzeigen">';
														if(pathinfo($thisValue2["createdDocumentsFilename"], PATHINFO_EXTENSION) == "doc") {
															$thisImageName = "iconDOC.gif";
														}
														else if(pathinfo($thisValue2["createdDocumentsFilename"], PATHINFO_EXTENSION) == "zip") {
															$thisImageName = "iconZIP.gif";
														}
														else if(pathinfo($thisValue2["createdDocumentsFilename"], PATHINFO_EXTENSION) == "pdf") {
															$thisImageName = "iconPDF.gif";
														}
														else if(pathinfo($thisValue2["createdDocumentsFilename"], PATHINFO_EXTENSION) == "tiff") {
															$thisImageName = "iconTIFF.gif";
														}
														else if(pathinfo($thisValue2["createdDocumentsFilename"], PATHINFO_EXTENSION) == "eps") {
															$thisImageName = "iconEPS.gif";
														}
														else if(pathinfo($thisValue2["createdDocumentsFilename"], PATHINFO_EXTENSION) == "gif") {
															$thisImageName = "iconGIF.gif";
														}
														else {
															$thisImageName = "iconALL.gif";
														}

														echo '<img src="layout/icons/'.$thisImageName.'" alt="" />';
														echo '</a>';
														echo '</span>';

														echo '<span class="toolItem">';
														echo '<a href="?editID=' . $_REQUEST["editID"] . '&documentType=' . $thisValue2["createdDocumentsType"] . '&deleteFile=' . urlencode(basename($thisValue2["createdDocumentsFilename"])) . '" onclick="return showWarning(\'Wollen Sie diese Datei wirklich entfernen?\')"><img src="layout/icons/iconDelete.png" width="16" height="16" alt="" title="Diese Datei l&ouml;schen" /></a>';
														echo '</span>';

														echo '<span class="toolItem">';
														echo '<img src="layout/icons/mailAttachement.png" class="buttonSendDocument" rel="' . urlencode(basename($thisValue2["createdDocumentsFilename"])) . '#KA" width="16" height="16" title="' . urlencode(basename($thisValue2["createdDocumentsFilename"])) . '&quot; direkt per Mail versenden" alt="Dokument versenden" />';
														echo '</span>';
													echo '</td>';

													/*
														echo '<td>';
														#echo '<form name="formSubmitConvertDocument" method="post" action="' . PAGE_CREATE_DOCUMENT . '">';
														echo '<table width="150" cellpadding="0" cellspacing="0" class="noBorder">';
														echo '<tr>';
														echo '<td>';
														#echo '<input type="hidden" name="editID" value=" ' . $arrDocumentDatas[$thisKey]["orderDocumentsCustomerNumber"] . ' ">';
														#echo '<input type="hidden" name="originDocumentType" value="' . $_REQUEST["documentType"] . '">';
														#echo '<input type="hidden" name="originDocumentID" value=" ' . $thisKey . ' ">';
														#echo '<select name="convertDocType" class="inputSelect_120">';
														#echo createSelectDocTypes($_REQUEST["documentType"], 'convert');
														#echo '</select>';
														echo '</td>';
														echo '<td>';
														// echo '<a href="' . PAGE_CREATE_THIS . '?editID=' . $arrDocumentDatas[$thisKey]["orderDocumentsCustomerNumber"] . '&originDocumentID=' . $thisKey . '&originDocumentType=' . $_REQUEST["documentType"] . '"><img src="layout/icons/convertDocument.png" width="16" height="16" title="Dokument umwandeln in" alt="Dokument umwandeln" /></a>';
														#echo '<input type="image" src="layout/icons/convertDocument.png" title="' . $arrDocumentTypeDatas[$_REQUEST["documentType"]]["createdDocumentsTypesName"] . ' umwandeln">';
														echo '</td>';
														echo '</tr>';
														echo '</table>';
														#echo '</form>';
														echo '</td>';
														echo '<td>';
														#echo '<form name="formSubmitConvertDocument" method="post" action="' . PAGE_CREATE_DOCUMENT . '">';
														echo '<table width="150" cellpadding="0" cellspacing="0" class="noBorder">';
														echo '<tr>';
														echo '<td>';
														#echo '<input type="hidden" name="editID" value=" ' . $arrDocumentDatas[$thisKey]["orderDocumentsCustomerNumber"] . ' ">';
														#echo '<input type="hidden" name="originDocumentType" value=" ' . $_REQUEST["documentType"] . ' ">';
														#echo '<input type="hidden" name="originDocumentID" value=" ' . $thisKey . ' ">';
														#echo '<select name="copyDocType" class="inputSelect_120">';
														#echo createSelectDocTypes($_REQUEST["documentType"], 'copy');
														#echo '</select>';
														echo '</td>';
														echo '<td>';
														// echo '<a href="' . PAGE_CREATE_THIS . '?editID=' . $arrDocumentDatas[$thisKey]["orderDocumentsCustomerNumber"] . '&originDocumentID=' . $thisKey . '&originDocumentType=' . $_REQUEST["documentType"] . '"><img src="layout/icons/copyDocument.png" width="16" height="16" title="Dokument kopieren" alt="Dokument kopieren" /></a>';
														#echo '<input type="image" src="layout/icons/copyDocument.png" title="' . $arrDocumentTypeDatas[$_REQUEST["documentType"]]["createdDocumentsTypesName"] . ' umwandeln">';
														echo '</tr>';
														echo '</table>';
														#echo '</form>';
														echo '</td>';
													*/
													echo '</tr>';
													$count++;
												}

												echo '</table>';
												echo '</div>';
												echo '<hr />';
											}
										}
										else {
											$infoMessage .= 'Es sind keine Korrektur-Abz&uuml;ge vorhanden.' . '<br />';
											displayMessages();
										}
									?>
								</fieldset>

								<?php
									if($arrGetUserRights["editCustomers"]) {
								?>

								<form name="formUploadCustomerFiles" method="post" action="<?php echo $_SERVER["PHP_SELF"]; ?>" enctype="multipart/form-data" >
									<input type="hidden" name="tabID" value="<?php echo $_REQUEST["tabID"]; ?>" />
									<fieldset>
										<legend>Korrektur-Abz&uuml;ge hochladen</legend>
										<table border="0" width="100%" cellspacing="0" cellpadding="0">
											<tr>
												<td style="width:200px;"><b>Lokale Datei(en) ausw&auml;hlen:</b></td>
												<td>
													<?php
														for($i = 0 ; $i < (MAX_FILE_UPLOADS + 1) ; $i++) {
															echo '<p style="border-bottom: 1px dotted #666; padding: 2px 0 2px 0; ">';
															echo '<input type="file" size="50" name="editCustomersPrintFile['.$i.']" class="inputFile_510" />';
															echo '</p>';
														}
													?>
												</td>
											</tr>
										</table>
										<input type="hidden" name="editCustomersKundennummer2" value="<?php echo $customerDatas["customersKundennummer"]; ?>" />
										<input type="hidden" name="editID" value="<?php echo $_REQUEST["editID"]; ?>" />
									</fieldset>

									<div class="actionButtonsArea">
										<input type="submit" class="inputButton1 inputButtonGreen" name="storeFiles" value="Hochladen" onclick="return showWarning(' Wollen Sie diese Datei wirklich hochladen??? ');" />
										&nbsp;
										<input type="submit" class="inputButton1 inputButtonOrange" name="resetFiles" value="Abbrechen" onclick="return showWarning(' Wollen Sie diesen Vorgang wirklich abbrechen??? ');" />
									</div>
								</form>
								<?php
									}
								?>
							</div>
							<?php
									}
									displayMessages();
								}
							?>


							<?php
								if($_REQUEST["editID"] != "NEW") {
									if($_REQUEST["tabID"] == "tabs-2") {
							?>
							<div id="tabs-2">
								<fieldset style="width:90% !important;">
									<legend>Bisherige Produktionen</legend>

									<div class="colorsLegend">
										<div class="legendItem">
											<b>F&auml;rbung:</b>
										</div>

										<div class="legendItem">
											<b>Zeilen:</b>
										</div>
										<div class="legendItem">
											<span class="legendColorField" id="colorField4"></span>
											<span class="legendDescription">Aktuelle Zeile</span>
											<div class="clear"></div>
										</div>
										<div class="legendSeperator">&nbsp;</div>

										<!--
										<div class="legendItem">
											<span class="legendColorField" id="colorField7"></span>
											<span class="legendDescription">Freigegeben</span>
											<div class="clear"></div>
										</div>
										-->

										<div class="legendItem">
											<span class="legendColorField" id="colorField8"></span>
											<span class="legendDescription">Belichtet</span>
											<div class="clear"></div>
										</div>
										<div class="legendItem">
											<span class="legendColorField" id="colorField1"></span>
											<span class="legendDescription">in Produktion</span>
											<div class="clear"></div>
										</div>
										<div class="legendItem">
											<span class="legendColorField" id="colorField3"></span>
											<span class="legendDescription">Versendet</span>
											<div class="clear"></div>
										</div>

										<div class="legendSeperator">&nbsp;</div>

										<div class="clear"></div>
									</div>
									<?php
										$sql = "
											SELECT
													`" . TABLE_ORDERS . "`.`ordersID`,
													`" . TABLE_ORDERS . "`.`ordersKundennummer`,
													`" . TABLE_ORDERS . "`.`ordersCustomerID`,
													`" . TABLE_ORDERS . "`.`ordersStatus`,
													`" . TABLE_ORDERS . "`.`ordersSourceType`,
													`" . TABLE_ORDERS . "`.`ordersBestellDatum`,
													`" . TABLE_ORDERS . "`.`ordersStornoDatum`,
													`" . TABLE_ORDERS . "`.`ordersFreigabeDatum`,
													`" . TABLE_ORDERS . "`.`ordersFreigabeArt`,
													`" . TABLE_ORDERS . "`.`ordersBelichtungsDatum`,
													`" . TABLE_ORDERS . "`.`ordersProduktionsDatum`,
													`" . TABLE_ORDERS . "`.`ordersAuslieferungsDatum`,
													`" . TABLE_ORDERS . "`.`ordersLieferwoche`,
													`" . TABLE_ORDERS . "`.`ordersLieferDatum`,
													`" . TABLE_ORDERS . "`.`ordersKundenName`,
													`" . TABLE_ORDERS . "`.`ordersKommission`,
													`" . TABLE_ORDERS . "`.`ordersPLZ`,
													`" . TABLE_ORDERS . "`.`ordersOrt`,
													`" . TABLE_ORDERS . "`.`ordersArtikelID`,
													`" . TABLE_ORDERS . "`.`ordersArtikelNummer`,
													`" . TABLE_ORDERS . "`.`ordersArtikelBezeichnung`,
													`" . TABLE_ORDERS . "`.`ordersArtikelKategorieID`,
													`" . TABLE_ORDERS . "`.`ordersArtikelMenge`,
													`" . TABLE_ORDERS . "`.`ordersArtikelPrintColorsCount`,
													`" . TABLE_ORDERS . "`.`ordersArtikelWithBorder`,
													`" . TABLE_ORDERS . "`.`ordersAufdruck`,
													`" . TABLE_ORDERS . "`.`ordersAdditionalArtikelID`,
													`" . TABLE_ORDERS . "`.`ordersAdditionalArtikelNummer`,
													`" . TABLE_ORDERS . "`.`ordersAdditionalArtikelBezeichnung`,
													`" . TABLE_ORDERS . "`.`ordersAdditionalArtikelKategorieID`,
													`" . TABLE_ORDERS . "`.`ordersAdditionalArtikelMenge`,
													`" . TABLE_ORDERS . "`.`ordersSinglePreis`,
													`" . TABLE_ORDERS . "`.`ordersTotalPreis`,
													`" . TABLE_ORDERS . "`.`ordersDruckFarbe`,
													`" . TABLE_ORDERS . "`.`ordersAdditionalCosts`,
													`" . TABLE_ORDERS . "`.`ordersAdditionalCostsID`,
													`" . TABLE_ORDERS . "`.`ordersVertreter`,
													`" . TABLE_ORDERS . "`.`ordersVertreterID`,
													`" . TABLE_ORDERS . "`.`ordersMailAdress`,
													`" . TABLE_ORDERS . "`.`ordersContactOthers`,
													`" . TABLE_ORDERS . "`.`ordersMandant`,
													`" . TABLE_ORDERS . "`.`ordersOrderType`,
													`" . TABLE_ORDERS . "`.`ordersPerExpress`,
													`" . TABLE_ORDERS . "`.`ordersBankAccountType`,
													`" . TABLE_ORDERS . "`.`ordersPaymentType`,
													`" . TABLE_ORDERS . "`.`ordersPaymentStatusType`,
													`" . TABLE_ORDERS . "`.`ordersNotizen`,
													`" . TABLE_ORDERS . "`.`ordersInfo`,
													`" . TABLE_ORDERS . "`.`ordersDruckerName`,
													`" . TABLE_ORDERS . "`.`ordersUserID`,
													`" . TABLE_ORDERS . "`.`ordersTimeCreated`,
													`" . TABLE_ORDERS . "`.`ordersArchive`,
													`" . TABLE_ORDERS . "`.`ordersUseNoStock`,
													`" . TABLE_ORDERS . "`.`ordersDirectSale`,
													`" . TABLE_ORDERS . "`.`ordersReklamationsgrund`

												FROM `" . TABLE_ORDERS . "`

												WHERE 1
													AND `" . TABLE_ORDERS . "`.`ordersKundennummer` = '" . $customerDatas["customersKundennummer"] . "'

												/* ORDER BY `" . TABLE_ORDERS . "`.`ordersBestellDatum` DESC */

											UNION

											SELECT
													`" . TABLE_ORDERS . "`.`ordersID`,
													`" . TABLE_ORDERS . "`.`ordersKundennummer`,
													`" . TABLE_ORDERS . "`.`ordersCustomerID`,
													`" . TABLE_ORDERS . "`.`ordersStatus`,
													`" . TABLE_ORDERS . "`.`ordersSourceType`,
													`" . TABLE_ORDERS . "`.`ordersBestellDatum`,
													`" . TABLE_ORDERS . "`.`ordersStornoDatum`,
													`" . TABLE_ORDERS . "`.`ordersFreigabeDatum`,
													`" . TABLE_ORDERS . "`.`ordersFreigabeArt`,
													`" . TABLE_ORDERS . "`.`ordersProduktionsDatum`,
													`" . TABLE_ORDERS . "`.`ordersBelichtungsDatum`,
													`" . TABLE_ORDERS . "`.`ordersAuslieferungsDatum`,
													`" . TABLE_ORDERS . "`.`ordersLieferwoche`,
													`" . TABLE_ORDERS . "`.`ordersLieferDatum`,
													`" . TABLE_ORDERS . "`.`ordersKundenName`,
													`" . TABLE_ORDERS . "`.`ordersKommission`,
													`" . TABLE_ORDERS . "`.`ordersPLZ`,
													`" . TABLE_ORDERS . "`.`ordersOrt`,
													`" . TABLE_ORDERS . "`.`ordersArtikelID`,
													`" . TABLE_ORDERS . "`.`ordersArtikelNummer`,
													`" . TABLE_ORDERS . "`.`ordersArtikelBezeichnung`,
													`" . TABLE_ORDERS . "`.`ordersArtikelKategorieID`,
													`" . TABLE_ORDERS . "`.`ordersArtikelMenge`,
													`" . TABLE_ORDERS . "`.`ordersArtikelPrintColorsCount`,
													`" . TABLE_ORDERS . "`.`ordersArtikelWithBorder`,
													`" . TABLE_ORDERS . "`.`ordersAufdruck`,
													`" . TABLE_ORDERS . "`.`ordersAdditionalArtikelID`,
													`" . TABLE_ORDERS . "`.`ordersAdditionalArtikelNummer`,
													`" . TABLE_ORDERS . "`.`ordersAdditionalArtikelBezeichnung`,
													`" . TABLE_ORDERS . "`.`ordersAdditionalArtikelKategorieID`,
													`" . TABLE_ORDERS . "`.`ordersAdditionalArtikelMenge`,
													`" . TABLE_ORDERS . "`.`ordersSinglePreis`,
													`" . TABLE_ORDERS . "`.`ordersTotalPreis`,
													`" . TABLE_ORDERS . "`.`ordersDruckFarbe`,
													`" . TABLE_ORDERS . "`.`ordersAdditionalCosts`,
													`" . TABLE_ORDERS . "`.`ordersAdditionalCostsID`,
													`" . TABLE_ORDERS . "`.`ordersVertreter`,
													`" . TABLE_ORDERS . "`.`ordersVertreterID`,
													`" . TABLE_ORDERS . "`.`ordersMailAdress`,
													`" . TABLE_ORDERS . "`.`ordersContactOthers`,
													`" . TABLE_ORDERS . "`.`ordersMandant`,
													`" . TABLE_ORDERS . "`.`ordersOrderType`,
													`" . TABLE_ORDERS . "`.`ordersPerExpress`,
													`" . TABLE_ORDERS . "`.`ordersBankAccountType`,
													`" . TABLE_ORDERS . "`.`ordersPaymentType`,
													`" . TABLE_ORDERS . "`.`ordersPaymentStatusType`,
													`" . TABLE_ORDERS . "`.`ordersNotizen`,
													`" . TABLE_ORDERS . "`.`ordersInfo`,
													`" . TABLE_ORDERS . "`.`ordersDruckerName`,
													`" . TABLE_ORDERS . "`.`ordersUserID`,
													`" . TABLE_ORDERS . "`.`ordersTimeCreated`,
													`" . TABLE_ORDERS . "`.`ordersArchive`,
													`" . TABLE_ORDERS . "`.`ordersUseNoStock`,
													`" . TABLE_ORDERS . "`.`ordersDirectSale`,
													`" . TABLE_ORDERS . "`.`ordersReklamationsgrund`

												FROM `" . TABLE_ORDERS . "`

												LEFT JOIN `" . TABLE_CUSTOMERS . "` AS `customerSalesman`
												ON(`" . TABLE_ORDERS . "`.`ordersVertreterID` = `customerSalesman`.`customersID` AND `" . TABLE_ORDERS . "`.`ordersVertreterID` > 0)

												WHERE 1
													AND `customerSalesman`.`customersKundennummer` = '" . $customerDatas["customersKundennummer"] . "'

											ORDER BY `ordersBestellDatum` DESC
										";

										$rs = $dbConnection->db_query($sql);

										echo '<table border="1" cellpadding="0" cellspacing="0" width="99%" style="width:99%" class="displayOrders">';

										echo '<thead>';

										echo '<tr >';

										echo '<th style="width:45px;text-align:right;">#</th>';
										// echo '<th>K-Nr</th>';
										echo '<th>Bestellt</th>';
										echo '<th>Belichtet</th>';
										echo '<th>In Druck</th>';
										echo '<th>Freigegeben</th>';
										echo '<th>Geliefert</th>';
										echo '<th>Name</th>';
										echo '<th>Status</th>';
										echo '<th>Art.-Nr.</th>';
										echo '<th>Art.-Name</th>';
										echo '<th>Menge</th>';
										#echo '<th>Einzelpreis</th>';
										#echo '<th>Gesamtpreis</th>';
										echo '<th>Druckfarbe</th>';
										#echo '<th>Zusatzkosten</th>';
										// echo '<th>Drucker</th>';
										echo '<th>Bestellart</th>';
										echo '<th>Quelle</th>';
										echo '<th>Info</th>';

										echo '</tr>';

										echo '</thead>';
										echo '<tbody>';
										$count = 0;
										$orderPriceSum = 0;
										while($ds = mysqli_fetch_array($rs)) {

												if($count%2 == 0) {
													$classRow = 'row0';
												}
												else {
													$classRow = 'row1';
												}

												if($ds["ordersStatus"] == "1") {
													// $classRow = 'row5';
												}
												else if($ds["ordersStatus"] == "3") {
													#$classRow = 'row2';
													$classRow = 'row9';
												}
												else if($ds["ordersStatus"] == "4") {
													$classRow = 'row3';
												}
												else if($ds["ordersStatus"] == "6") {
													$classRow = 'row6';
												}
												else if($ds["ordersStatus"] == "7") {
													#$classRow = 'row9';
													$classRow = 'row2';
												}

												echo '<tr class="'.$classRow.'">';
												echo '<td style="text-align:right"><b>'.($count + 1).'.</b></td>';
												// echo '<td style="text-align:center;">'.$ds["ordersKundennummer"].'</td>';
												echo '<td style="text-align:center;">'.formatDate($ds["ordersBestellDatum"], "display").'</td>';
												echo '<td style="text-align:center;">'.formatDate($ds["ordersBelichtungsDatum"], "display").'</td>';
												echo '<td style="text-align:center;">'.formatDate($ds["ordersProduktionsDatum"], "display").'</td>';
												echo '<td style="text-align:center;">'.formatDate($ds["ordersFreigabeDatum"], "display").'</td>';
												echo '<td style="text-align:center;">'.formatDate($ds["ordersAuslieferungsDatum"], "display").'</td>';
												echo '<td>';
												echo ($ds["ordersKundenName"]);
												if($ds["ordersKommission"] != '') {
													echo '<div class="remarksArea">';
													echo '<b>Kommission:</b> ' . ($ds["ordersKommission"]);
													echo '</div>';
												}
												if($ds["ordersAufdruck"] != '') {
													echo '<div class="remarksArea">';
													echo '<b>Aufdruck:</b> ' . ($ds["ordersAufdruck"]);
													echo '</div>';
												}
												if($ds["ordersPerExpress"] == '1') {
													echo '<div class="remarksArea">';
													echo '<b style="color:#FF0000;">als Express-Bestellung</b>';
													echo '</div>';
												}
												if($ds["ordersDirectSale"] == '1') {
													echo '<div class="remarksArea">';
													echo '<b style="color:#FF0000;">Direktverkauf durch den Vertreter</b>';
													echo '</div>';
												}
												if($ds["ordersInfo"] != '') {
													echo '<div class="remarksArea">';
													echo '<b>Info:</b> ' . $ds["ordersInfo"];
													echo '</div>';
												}

												if($ds["ordersNotizen"] != '') {
													echo '<div class="remarksArea">';
													echo '<b>Bemerkung:</b><br />' . $ds["ordersNotizen"];
													echo '</div>';
												}
												// BOF COMPLAINT
												if($ds["ordersReklamationsgrund"] != '') {
													echo '<div class="remarksArea">';
													echo '<b style="color:#FF0000;">Reklamiert:</b> ' . $ds["ordersReklamationsgrund"];
													echo '</div>';
												}
												// EOF COMPLAINT
												echo '</td>';
												echo '<td><span title="'.($arrOrderStatusTypeDatas[$ds["ordersStatus"]]["orderStatusTypesName"]).'">'.($arrOrderStatusTypeDatas[$ds["ordersStatus"]]["orderStatusTypesShortName"]).'</span></td>';
												echo '<td style="text-align:center;">'.$ds["ordersArtikelNummer"].'</td>';
												echo '<td style="text-align:left;">';
												echo $ds["ordersArtikelBezeichnung"];
												if($ds["ordersAdditionalArtikelMenge"] > 0) {
													echo '<br /><b>Zusatzleisten:</b> ' . ($ds["ordersAdditionalArtikelMenge"]);
												}

												echo '</td>';
												echo '<td style="text-align:right;">'.$ds["ordersArtikelMenge"].'</td>';
												#echo '<td style="text-align:right;">'.number_format($ds["ordersSinglePreis"], 2).' &euro;</td>';
												#echo '<td style="text-align:right;">'.number_format($ds["ordersTotalPreis"], 2).' &euro;</td>';
												echo '<td style="text-align:left;white-space:nowrap;">';
												echo ' &middot; ' . preg_replace("/\//", "<br /> &middot; ", $ds["ordersDruckFarbe"]);
												if($ds["ordersArtikelWithBorder"] == '1'){
													echo '<br />' . ' &middot; <b style="color:#FF0000;">mit Umrandung</b>';
												}
												echo '</td>';
												/*
												echo '<td style="text-align:right;">';
												if($ds["ordersAdditionalCosts"] > 0) {
													$orderPriceSum += $ds["ordersAdditionalCosts"];
													echo $arrAdditionalCostsDatas[$ds["ordersAdditionalCostsID"]]["additionalCostsShortName"].' <span style="white-space:nowrap;">'.number_format($ds["ordersAdditionalCosts"], 2).' &euro;</span>';
												}
												else {
													echo '-';
												}
												echo '</td>';
												*/

												echo '<td style="text-align:right;">'.$arrOrderTypeDatas[$ds["ordersOrderType"]]["orderTypesShortName"].'</td>';
												echo '<td style="text-align:left;">';
												echo $arrOrdersSourceTypeDatas[$ds["ordersSourceType"]]["ordersSourceTypesName"];
												echo '<br /><span class="remarksArea" style="white-space:nowrap;">' . $ds["ordersVertreter"] . '</span>';

												echo '</td>';
												echo '<td>';
												// if($arrGetUserRights["editOrders"] && $ds["ordersStatus"] < 4) {
												if(1){
													echo '<span class="toolItem"><a href="' . PAGE_EDIT_PROCESS . '?editID='.$ds["ordersID"]. '"><img src="layout/icons/iconEdit.gif" width="16" height="16" title="Produktion bearbeiten (Datensatz '.$ds["ordersID"].')" alt="Bearbeiten" /></a></span>';
												}
												echo '</td>';

												echo '</tr>';

												$orderPriceSum += $ds["ordersTotalPreis"];
												$count++;
										}
										echo '</tbody>';
										echo '</table>';

										echo '<table border="0" cellpadding="0" cellspacing="0" width="">';
										#echo '<tr>';
										#echo '<td><b>Bestellwert insgesamt:</b></td><td>'.number_format($orderPriceSum, 2).' &euro;</td>';
										#echo '</tr>';
										echo '<tr>';
										echo '<td><b>Bestellungen insgesamt:</b></td><td>'.$count.'</td>';
										echo '</tr>';
										echo '</table>';
									?>
								</fieldset>
							</div>
							<?php
									}
								}
							?>

							<?php
								if($_REQUEST["editID"] != "NEW") {
									if($_REQUEST["tabID"] == "tabs-6") {
									$sql = "SELECT
												`Paketnummer`,
												`Datum`,
												`Produkt_Service`,
												`Gewicht`,
												`Referenznr_1`,
												`Referenznr_2`,
												`Referenznr_3`,
												`Referenznr_4`,
												`ID_Sendung`,
												`ID_Versender_Scan`,
												`Versanddatum`,
												`Zustelldatum`,
												`Firma_Empfaenger`,
												`Name_1`,
												`Zu_Haenden`,
												`Adresse_1`,
												`Adresse_2`,
												`Land_1`,
												`Region_1`,
												`PLZ_1`,
												`Stadt_1`,
												`Tel_1`,
												`Email_1`,
												`Ref_Adresse_1`,
												`Anrede_1`,
												`Fax_1`,
												`Bemerkung_Adresse`,
												`Firma_Absender`,
												`Name_2`,
												`Adresse_Strasse_1`,
												`Adresse_Strasse_2`,
												`Land_2`,
												`Region_2`,
												`PLZ_2`,
												`Stadt_2`,
												`Tel_2`,
												`Email_2`,
												`Ref_Adresse_2`,
												`Anrede_2`,
												`Fax_2`,
												`COD_Verwendungszweck`,
												`COD_Betrag`,
												`COD_Waehrung`,
												`COD_Inkasoart`,
												`AI_Laenge_cm`,
												`AI_Breite_cm`,
												`AI_Hoehe_cm`,
												`AI_enthaelt_Begleitdokumente`,
												`AI_Pakentinhalt`,
												`AI_Betrag`,
												`AI_Waehrung`,
												`AI_Steuernummer`,
												`AI_Kommentar`,
												`AI_RE_Firma`,
												`AI_RE_Adresse_1_Strasse`,
												`AI_RE_Land`,
												`AI_RE_Region`,
												`AI_RE_PLZ`,
												`AI_RE_Stadt`,
												`AI_BI_Kommentar_1`,
												`AI_BI_Inhalt`,
												`AI_BI_Begleitdokumente`,
												`Proaktive_Benachrichtigung`,
												`HV_Betrag`,
												`HV_Waehrung`,
												`HV_Wareninhalt`,
												`ID_Check_Name`,
												`ABT_Gebaeude`,
												`ABT_Stockwerk`,
												`ABT_Abteilung`,
												`SSCC_NVE`,
												`Bankleitzahl`,
												`Name_der_Bank`,
												`Kontonummer`,
												`Kontoinhaber`,
												`IBAN`,
												`BIC`,
												`Shop_Kundennummer`,
												`Shop_Firma`,
												`Shop_Name`,
												`Shop_Addresse_1`,
												`Shop_Addresse_2`,
												`Shop_Land`,
												`Shop_Region`,
												`Shop_PLZ`,
												`Shop_Ort`,
												`Shop_Telefon`,
												`Mandant_ID`,
												`Mandant_Name`,
												`Benutzername`,
												`Journal_ID`,
												`Zustellung`

												FROM `" . TABLE_DELIVERY_DATAS . "`

												WHERE `Ref_Adresse_1` = '" . $customerDatas["customersKundennummer"] . "'

												ORDER BY `Versanddatum` DESC, `Paketnummer` DESC
										";

							?>

								<div id="tabs-6">
									<fieldset>
										<legend>Auslieferungen</legend>
										<table border="0" cellpadding="0" cellspacing="0" width="100%" class="displayOrders">
											<thead>
												<tr>
													<th style="width:45px;text-align:right;">#</th>
													<th class="sortColumn">Paketnummer</th>
													<th class="sortColumn">K-Nr</th>
													<!--
													<th class="sortColumn">Typ</th>
													<th class="sortColumn">Gewicht</th>
													-->
													<th class="sortColumn">Referenz</th>
													<th class="sortColumn">Versanddatum</th>
													<th>Empf&auml;nger</th>
													<th>Adresse</th>
													<th>Land</th>
													<!--
													<th>Region</th>
													-->
													<th>PLZ</th>
													<th>Stadt</th>
												</tr>
											</thead>

											<tbody>
											<?php
												$rs = $dbConnection->db_query($sql);

												$count = 0;
												while($ds = mysqli_fetch_assoc($rs)) {

													if($count%2 == 0){ $rowClass = 'row0'; }
													else { $rowClass = 'row1'; }

													echo '<tr class="'.$rowClass.'">';
													echo '<td style="text-align:right;"><b>' . ($count + 1) .'.</b></td>';
														echo '<td>';
														echo '<a href="' . PAGE_SEARCH_TRACKING . '?searchTrackingNumber=' . $ds["Paketnummer"] . '">' . $ds["Paketnummer"] . '</a>';
														echo '</td>';
														echo '<td>';
														echo '<a href="' . PAGE_EDIT_CUSTOMER . '?editCustomerNumber=' . $ds["Ref_Adresse_1"] . '">' . $ds["Ref_Adresse_1"] . '</a>';
														echo '</td>';
														#echo '<td>' . $ds["Produkt_Service"] .'</td>';
														#echo '<td>' . $ds["Gewicht"] .'</td>';
														echo '<td style="white-space:nowrap;">';
														echo $ds["Referenznr_1"];
														echo '</td>';
														echo '<td>' . formatDate($ds["Versanddatum"], 'display') .'</td>';
														echo '<td style="white-space:nowrap;">' . $ds["Firma_Empfaenger"] .' ' . $ds["Name_1"];
														if($ds["Zu_Haenden"] != '') {
															echo 'z. Hd. ' . $ds["Zu_Haenden"];
														}
														echo '</td>';
														echo '<td style="white-space:nowrap;">' . $ds["Adresse_1"] .' ' . $ds["Adresse_2"] .'</td>';
														echo '<td>' . $ds["Land_1"] .'</td>';
														#echo '<td>' . $ds["Region_1"] .'</td>';
														echo '<td style="white-space:nowrap;">' . $ds["PLZ_1"] .'</td>';
														echo '<td style="white-space:nowrap;">' . $ds["Stadt_1"] .'</td>';
													echo '<tr>';
													$count++;
												}
											?>
											</tbody>
										</table>
									</fieldset>
								</div>
							<?php
									}
								}
							?>

							<?php
								if($_REQUEST["editID"] != "NEW") {
									if($_REQUEST["tabID"] == "tabs-5") {
										if(!empty($arrDocumentDatas)) {
							?>

								<div id="tabs-5">
									<fieldset style="width:90% !important;">
										<legend>Dokument-Beziehungen</legend>
										<?php
											if(!empty($arrDocumentRelatedNumbers)){
												foreach($arrDocumentRelatedNumbers as $thisDocumentNumbers){
													$isSplittedDelivery = false;
													if(preg_match("/;/", $thisDocumentNumbers["LS"])) {
														$isSplittedDelivery = true;
														$arrTemp = explode(";", $thisDocumentNumbers["LS"]);
														$thisDocumentNumbers["LS"] = '';
														$countTemp = 1;
														foreach($arrTemp as $thisTempValue){
															$thisDocumentNumbers["LS".$countTemp] = $thisTempValue;
															$countTemp++;
														}
													}

													if($arrConnectedDocumentIds[$thisDocumentNumbers[0]]["documentsType"] != 'BR' && count($thisDocumentNumbers) > 0 ){
														$countRow = 0;
														echo '<div class="displayDocumentsArea" >';

														echo '<table border="0" cellpadding="0" cellspacing="0" style="width:99%;" class="displayOrders">';
														echo '<tr class="' . $rowClass . '">';
														echo '<th style="width:100px;">Dokument-Nr</th>';
														echo '<th>Dokument-Empf&auml;nger</th>';
														echo '<th style="width:100px;">Dokumentdatum</th>';
														echo '<th style="width:100px;">Auftragsdatum</th>';

														#echo '<th>DB-ID</th>';
														#echo '<th>docType</th>';
														echo '<th style="width:120px;">Zahl-Status</th>';
														echo '<th style="width:20px;">PDF</th>';
														echo '<th style="width:160px;">Zugeh&ouml;rige Produktionen</th>';
														foreach($thisDocumentNumbers as $thisDocumentKey => $thisDocumentNumber){
															if($thisDocumentNumber != ''){
																if($countRow%2 == 0){ $rowClass = 'row1'; }
																else { $rowClass = 'row4'; }

																if($arrConnectedDocumentIds[$thisDocumentNumber]["documentsStatus"] == "1") {
																	$rowClass2 = 'row2';
																}
																else if($arrConnectedDocumentIds[$thisDocumentNumber]["documentsStatus"] == "2") {
																	$rowClass2 = 'row3';
																}
																else if($arrConnectedDocumentIds[$thisDocumentNumber]["documentsStatus"] == "3") {
																	$rowClass2 = 'row6';
																	$rowClass = 'row6';
																}
																else if($arrConnectedDocumentIds[$thisDocumentNumber]["documentsStatus"] == "4") {
																	$rowClass2 = 'row2';
																}
																else if($arrConnectedDocumentIds[$thisDocumentNumber]["documentsStatus"] == "5") {
																	$rowClass2 = 'row5';
																}
																else if($arrConnectedDocumentIds[$thisDocumentNumber]["documentsStatus"] == "6") {
																	$rowClass2 = 'row6';
																}
																else if($arrConnectedDocumentIds[$thisDocumentNumber]["documentsStatus"] == "6") {
																	$rowClass2 = 'row7';
																}

																echo '<tr class="' . $rowClass . '">';
																echo '<td>';
																echo '<a href="' . PAGE_ALL_INVOICES . '?searchDocumentNumber=' . $thisDocumentNumber . '">';
																echo $thisDocumentNumber;
																if($isSplittedDelivery && $arrConnectedDocumentIds[$thisDocumentNumber]["documentsType"] == 'LS'){ echo ' (<b>Teillieferung</b>)';}
																echo '</td>';

																echo '<td>';
																echo $arrConnectedDocumentIds[$thisDocumentNumber]["documentsAddressCompany"];
																echo '</td>';

																echo '<td>';
																echo formatDate($arrConnectedDocumentIds[$thisDocumentNumber]["documentsDocumentDate"], 'display');
																echo '</td>';
																echo '<td>';
																echo formatDate($arrConnectedDocumentIds[$thisDocumentNumber]["documentsOrderDate"], 'display');
																echo '</td>';
																#echo '<td>';
																#echo $arrConnectedDocumentIds[$thisDocumentNumber]["documentID"];
																#echo '</td>';
																#echo '<td>';
																#echo $arrConnectedDocumentIds[$thisDocumentNumber]["documentsType"];
																#echo '</td>';
																echo '<td class="' . $rowClass2 . '">';
																if($arrConnectedDocumentIds[$thisDocumentNumber]["documentsType"] == 'AB' || $arrConnectedDocumentIds[$thisDocumentNumber]["documentsType"] == 'RE'){
																	echo $arrPaymentStatusTypeDatas[$arrConnectedDocumentIds[$thisDocumentNumber]["documentsStatus"]]["paymentStatusTypesName"];
																}
																else {
																	echo ' - ';
																}
																echo '</td>';
																echo '<td>';
																echo '<span class="toolItem">';
																#echo '<a href="' . $_SERVER["PHP_SELF"] . '?editCustomerNumber=' . $thisDocumentValue[$thisKey]["orderDocumentsCustomerNumber"] . '&editID=' . $_REQUEST["editID"] . '&downloadFile=' . urlencode(basename($thisDocumentNumber . '_KNR-' . $thisDocumentValue[$thisKey]["orderDocumentsCustomerNumber"] . '.pdf')) . '&documentType=' . $arrConnectedDocumentIds[$thisDocumentNumber]["documentsType"] . '">' . '<img src="layout/icons/icon' . getFileType(basename($thisDocumentValue[$thisKey]["orderDocumentsDocumentPath"])) . '.gif" width="16" height="16" title="Dokument herunterladen" alt="Download" /></a>';
																#echo '<a href="' . $_SERVER["PHP_SELF"] . '?editCustomerNumber=' . $customerDatas["customersKundennummer"] . '&editID=' . $_REQUEST["editID"] . '&downloadFile=' . urlencode(basename($thisDocumentNumber . '_KNR-' . $thisDocumentValue[$thisKey]["orderDocumentsCustomerNumber"] . '.pdf')) . '&documentType=' . $arrConnectedDocumentIds[$thisDocumentNumber]["documentsType"] . '">' . '<img src="layout/icons/icon' . getFileType(basename($thisDocumentValue[$thisKey]["orderDocumentsDocumentPath"])) . '.gif" width="16" height="16" title="Dokument herunterladen" alt="Download" /></a>';
																echo '<a href="' . $_SERVER["PHP_SELF"] . '?editCustomerNumber=' . $customerDatas["customersKundennummer"] . '&editID=' . $_REQUEST["editID"] . '&downloadFile=' . urlencode(basename($arrConnectedDocumentIds[$thisDocumentNumber]["documentsFilename"])) . '&documentType=' . $arrConnectedDocumentIds[$thisDocumentNumber]["documentsType"] . '">' . '<img src="layout/icons/icon' . getFileType(basename($thisDocumentValue[$thisKey]["orderDocumentsDocumentPath"])) . '.gif" width="16" height="16" title="Dokument herunterladen" alt="Download" /></a>';
																echo '</span>';
																echo '</td>';
																echo '<td>';
																if($countRow == 0){
																		echo '<span class="toolItem">';
																		#$arrProductionIDs = explode("#", $thisProductionID);
																		$arrProductionIDs = explode(";", $arrConnectedDocumentIds[$thisDocumentNumber]["productionIDs"] );
																		$arrProductionIDs = array_unique($arrProductionIDs);
																		if(!empty($arrProductionIDs)) {
																			foreach($arrProductionIDs as $thisValue) {
																				if($thisValue > 0) {
																					echo '<span style="white-space:nowrap;"><a href="' . PAGE_EDIT_PROCESS . '?editID=' . $thisValue . '"><img src="layout/icons/iconEdit.gif" width="16" height="16" title="Produktion bearbeiten (Datensatz ' . $thisValue . ')" alt="Bearbeiten" /></a></span>';
																				}
																			}
																		}
																		echo '</span>';
																}
																echo '</td>';

																echo '</tr>';
																$countRow++;
															}
														}
														echo '</table></div><hr />';
													}
												}
											}
											else {
												$infoMessage .= 'Es sind keine Dokumente vorhanden.' . '<br />';
												displayMessages();
											}
										?>
									</fieldset>
								</div>
							<?php
									}}
									if(in_array($customerDatas["customersTyp"], array('2', '4'))) {
										if($_REQUEST["tabID"] == "tabs-8") {

							?>
									<div id="tabs-8">
										<fieldset>
											<legend>Provisionen</legend>
							<?php
										$sql_xxx = "SELECT
													`salesmenProvisionsDocumentNumber`,

													`salesmenProvisionsID`,
													`salesmenProvisionsSalesmanID`,
													`salesmenProvisionsDocumentDate`,
													`salesmenProvisionsSum`,
													`salesmenProvisionsMwst`,
													`salesmenProvisionsMwstValue`,
													`salesmenProvisionsTotal`,
													`salesmenProvisionsStatus`,
													`salesmenProvisionsDocumentPath`,
													`salesmenProvisionsContentDocumentNumber`,
													`createdDocumentsID`,
													`createdDocumentsType`,
													`createdDocumentsNumber`,
													`createdDocumentsCustomerNumber`,
													`createdDocumentsTitle`,
													`createdDocumentsFilename`,
													`createdDocumentsContent`,
													`createdDocumentsUserID`,
													`createdDocumentsTimeCreated`
													FROM `" . TABLE_SALESMEN_PROVISIONS . "`

													LEFT JOIN `" . TABLE_CREATED_DOCUMENTS . "`
													ON(`" . TABLE_SALESMEN_PROVISIONS . "`.`salesmenProvisionsDocumentNumber` = `" . TABLE_CREATED_DOCUMENTS . "`.`createdDocumentsNumber`)

													WHERE `" . TABLE_SALESMEN_PROVISIONS . "`.`salesmenProvisionsSalesmanID` = " . $customerDatas["customersID"] . "

													GROUP BY `" . TABLE_SALESMEN_PROVISIONS . "`.`salesmenProvisionsDocumentNumber`

													HAVING `" . TABLE_CREATED_DOCUMENTS . "`.`createdDocumentsID` IS NOT NULL

													ORDER BY `" . TABLE_SALESMEN_PROVISIONS . "`.`salesmenProvisionsDocumentNumber` DESC, `" . TABLE_SALESMEN_PROVISIONS . "`.`salesmenProvisionsDocumentDate` DESC
											";

										$sql = "SELECT
													`" . TABLE_SALESMEN_PROVISIONS . "`.`salesmenProvisionsID`,
													`" . TABLE_SALESMEN_PROVISIONS . "`.`salesmenProvisionsDocumentNumber`,
													`" . TABLE_SALESMEN_PROVISIONS . "`.`salesmenProvisionsSalesmanID`,
													`" . TABLE_SALESMEN_PROVISIONS . "`.`salesmenProvisionsDocumentDate`,
													`" . TABLE_SALESMEN_PROVISIONS . "`.`salesmenProvisionsSum`,
													`" . TABLE_SALESMEN_PROVISIONS . "`.`salesmenProvisionsMwst`,
													`" . TABLE_SALESMEN_PROVISIONS . "`.`salesmenProvisionsMwstValue`,
													`" . TABLE_SALESMEN_PROVISIONS . "`.`salesmenProvisionsTotal`,
													`" . TABLE_SALESMEN_PROVISIONS . "`.`salesmenProvisionsStatus`,
													`" . TABLE_SALESMEN_PROVISIONS . "`.`salesmenProvisionsDocumentPath`,
													`" . TABLE_SALESMEN_PROVISIONS . "`.`salesmenProvisionsContentDocumentNumber`,

													`" . TABLE_CREATED_DOCUMENTS . "`.`createdDocumentsID`,
													`" . TABLE_CREATED_DOCUMENTS . "`.`createdDocumentsType`,
													`" . TABLE_CREATED_DOCUMENTS . "`.`createdDocumentsNumber`,
													`" . TABLE_CREATED_DOCUMENTS . "`.`createdDocumentsCustomerNumber`,
													`" . TABLE_CREATED_DOCUMENTS . "`.`createdDocumentsTitle`,
													`" . TABLE_CREATED_DOCUMENTS . "`.`createdDocumentsFilename`,
													`" . TABLE_CREATED_DOCUMENTS . "`.`createdDocumentsContent`,
													`" . TABLE_CREATED_DOCUMENTS . "`.`createdDocumentsUserID`,
													`" . TABLE_CREATED_DOCUMENTS . "`.`createdDocumentsTimeCreated`

													FROM `" . TABLE_SALESMEN_PROVISIONS . "`

													LEFT JOIN `" . TABLE_CREATED_DOCUMENTS . "`
													ON(`" . TABLE_SALESMEN_PROVISIONS . "`.`salesmenProvisionsDocumentNumber` = `" . TABLE_CREATED_DOCUMENTS . "`.`createdDocumentsNumber`)

													WHERE `" . TABLE_SALESMEN_PROVISIONS . "`.`salesmenProvisionsSalesmanID` = " . $customerDatas["customersID"] . "

													/* GROUP BY `" . TABLE_SALESMEN_PROVISIONS . "`.`salesmenProvisionsDocumentNumber` */

													HAVING `" . TABLE_CREATED_DOCUMENTS . "`.`createdDocumentsID` IS NOT NULL

													ORDER BY `" . TABLE_SALESMEN_PROVISIONS . "`.`salesmenProvisionsDocumentNumber` DESC, `" . TABLE_CREATED_DOCUMENTS . "`.`createdDocumentsTimeCreated` DESC
											";

										$rs = $dbConnection->db_query($sql);

										$count = 0;

										echo '<table border="1" cellpadding="0" cellspacing="0" width="0" class="displayOrders">';
												echo '<tr>';
												echo '<th style="width:45px;">#</th>';
												echo '<th>Dokument-Nr</th>';
												echo '<th>Datum</th>';
												echo '<th>Summe</th>';
												echo '<th>MwSt.</th>';
												echo '<th>Gesamtbetrag</th>';
												echo '<th style="width:70px;">Info</th>';
												echo '</tr>';

										$thisMarker = '';
										while($ds = mysqli_fetch_assoc($rs)) {
											if($count%2 == 0){ $rowClass = 'row0'; }
											else { $rowClass = 'row1'; }

											if($thisMarker != $ds["salesmenProvisionsDocumentNumber"]) {
												$thisMarker = $ds["salesmenProvisionsDocumentNumber"];
												echo '<tr class="'.$rowClass.'">';
												echo '<td style="text-align:right;"><b>' . ($count + 1) .'.</b></td>';
												echo '<td>';
												echo $ds["salesmenProvisionsDocumentNumber"];
												echo '</td>';
												echo '<td>';
												echo substr(formatDate($ds["createdDocumentsTimeCreated"], 'display'), 0, 10);
												echo '</td>';
												echo '<td style="text-align:right;">';
												echo number_format($ds["salesmenProvisionsSum"], 2, ',', '') . '&euro;';
												echo '</td>';
												echo '<td style="text-align:right;">';
												echo number_format($ds["salesmenProvisionsMwstValue"], 2, ',', '') . '&euro;' . ' (' . number_format($ds["salesmenProvisionsMwst"], 2, ',', '') . '%)';
												echo '</td>';
												echo '<td style="text-align:right;">';
												echo number_format($ds["salesmenProvisionsTotal"], 2, ',', '') . '&euro;';
												echo '</td>';
												echo '<td>';
												echo '<span class="toolItem">';
												echo '<a href="' . $_SERVER["PHP_SELF"] . '?editCustomerNumber=' . $customerDatas["customersKundennummer"] . '&editID=' . $customerDatas["customersID"] . '&downloadFile=' . urlencode(basename($ds["createdDocumentsFilename"])) . '&documentType=PR">' . '<img src="layout/icons/icon' . getFileType(basename($ds["createdDocumentsFilename"])) . '.gif" width="16" height="16" title="Dokument herunterladen" alt="Download" /></a>';
												echo '</span>';
												echo '<span class="toolItem">';
												echo '<img src="layout/icons/mailAttachement.png" class="buttonSendDocument" rel="' . urlencode(basename($ds["createdDocumentsFilename"])) . '#' . 'PR' . '" width="16" height="16" title="Dokument direkt per Mail versenden" alt="Dokument versenden" />';
												echo '</span>';
												echo '</td>';
												echo '</tr>';
												$count++;
											}
										}
										echo '</table>';
							?>
									</fieldset>
								</div>
							<?php
									}
									}
							?>
							<?php
								if($_REQUEST["tabID"] == "tabs-10") {
							?>
							<div id="tabs-10">
								<?php
									$sql = "
										SELECT
											`customersCalendarID`,
											`customersCalendarCustomersID`,
											`customersCalendarCustomersNumber`,
											`customersCalendarDate`,
											`customersCalendarDateRepeat`,
											`customersCalendarRepeatType`,
											`customersCalendarRepeatValue`,
											`customersCalendarDescription`,
											`customersCalendarReminderMailSended`,
											`customersCalendarUserID`

											FROM `" . TABLE_CUSTOMERS_CALENDAR . "`
											WHERE 1
												AND `customersCalendarCustomersID` = '" . $_REQUEST["editID"] . "'

												ORDER BY `customersCalendarDate` DESC
										";

										$rs = $dbConnection->db_query($sql);
										$countItems = $dbConnection->db_getMysqlNumRows($rs);
								?>
								<form name="formAddCustomerCalendarDate" method="post" action="<?php echo $_SERVER["PHP_SELF"]; ?>#tabs-10" enctype="multipart/form-data" >
									<input type="hidden" name="tabID" value="<?php echo $_REQUEST["tabID"]; ?>" />
									<?php if($countItems > 0){ ?>
									<fieldset>
										<legend>Termin-Erinnerungen</legend>
										<table border="0" width="100%" cellspacing="0" cellpadding="0" class="displayOrders">
											<tr>
												<th style="width:45px;">#</th>
												<th style="width:100px;">Termin</th>
												<th>Beschreibung</th>
												<th style="width:45px;">Info</th>
											</tr>
											<?php
												$count = 0;
												while($ds = mysqli_fetch_assoc($rs)){
													if($count%2 == 0){ $rowClass = 'row0'; }
													else { $rowClass = 'row1'; }

													echo '<tr ' . $thisStyle . ' class="'.$rowClass.'">';
													echo '<td style="text-align:right;">';
													echo '<b>' . ($count + 1) . '.</b>';
													echo '</td>';
													echo '<td>';
													echo formatDate($ds["customersCalendarDate"], 'display');
													echo '</td>';
													echo '<td>';
													echo $ds["customersCalendarDescription"];
													echo '</td>';
													echo '<td>';
													echo '<a href="' . $_SERVER["PHP_SELF"]. '?editID=' . $_REQUEST["editID"] . '&deleteCustomerCalendarID=' . $ds["customersCalendarID"] . '#tabs-10" onclick="return showWarning(\'Soll der Termin wirklich entfernt werden?\');" ><img src="layout/icons/iconDelete.png" width="16" height="16" alt="Termin entfernen" title="Diesen Termin entfernen" /></a>';
													echo '</td>';
													echo '</tr>';
												}
											?>
											</table>
										</fieldset>
										<?php } ?>
										<hr />
										<fieldset>
											<legend>Neue Termin-Erinnerung</legend>
											<table border="0" width="100%" cellspacing="0" cellpadding="0" class="displayOrders">
											<tr>
												<th style="width:45px;">#</th>
												<th style="width:100px;">Termin</th>
												<th>Beschreibung</th>
												<th style="width:45px;">Info</th>
											</tr>
											<?php
												for($i = 0 ; $i < 1 ; $i++){
													if($i%2 == 0){ $rowClass = 'row0'; }
													else { $rowClass = 'row1'; }
													echo '<tr ' . $thisStyle . ' class="'.$rowClass.'">';
													echo '<td style="text-align:right;">';
													echo '<b>' . ($i + 1) . '.</b>';
													echo '<input type="hidden" name="addCustomerCalendarDates[' . $i . '][ID]" value="' . $i . '" />';
													echo '<input type="hidden" name="addCustomerCalendarDates[' . $i . '][CUSTOMERID]" value="' . $_REQUEST["editID"] . '" />';
													echo '<input type="hidden" name="addCustomerCalendarDates[' . $i . '][CUSTOMERNUMBER]" value="' . $customerDatas["customersKundennummer"] . '" />';
													echo '</td>';
													echo '<td>';
													echo '<input type="text" name="addCustomerCalendarDates[' . $i . '][DATE]" id="addCustomerCalendarDates" class="inputField_70" value="' . $_POST["addCustomerCalendarDates"][0]["DATE"] . '" />';
													echo '</td>';
													echo '<td>';
													echo '<input type="text" name="addCustomerCalendarDates[' . $i . '][DESCRIPTION]" class="inputField_600" value="' . $_POST["addCustomerCalendarDates"][0]["DESCRIPTION"] . '" />';
													echo '</td>';
													echo '<td><img src="layout/icons/spacer.gif" width="16" height="14" alt="" title="" /></td>';
													echo '</tr>';
												}
											?>
										</table>
										<input type="hidden" name="editID" value="<?php echo $_REQUEST["editID"]; ?>" />
									</fieldset>

									<div class="actionButtonsArea">
										<input type="submit" class="inputButton1 inputButtonGreen" name="storeCalendar" value="Termin speichern" />
										&nbsp;
										<input type="submit" class="inputButton1 inputButtonOrange" name="resetCalendar" value="Abbrechen" onclick="return showWarning(' Wollen Sie diesen Vorgang wirklich abbrechen??? ');" />
									</div>
								</form>
							</div>
							<?php
								}
							?>

							<?php
								if($_REQUEST["tabID"] == "tabs-9") {
							?>

							<div id="tabs-9">
								<?php
									$sql = "
										SELECT
											`customersContactsID`,
											`customersContactsContactID`,
											`customersContactsSalutation`,
											`customersContactsFirstName`,
											`customersContactsSecondName`,
											`customersContactsFunction`,
											`customersContactsPhone`,
											`customersContactsMobile`,
											`customersContactsFax`,
											`customersContactsMail`,
											`customersContactsActive`

											FROM `" . TABLE_CUSTOMERS_CONTACTS. "`

											WHERE 1
												AND `customersContactsContactID` = " . $_REQUEST["editID"] . "

												ORDER BY `customersContactsSecondName`
									";

									$rs = $dbConnection->db_query($sql);
									$countItems = $dbConnection->db_getMysqlNumRows($rs);
								?>
								<form name="formAddContacts" method="post" action="<?php echo $_SERVER["PHP_SELF"]; ?>#tabs-9" enctype="multipart/form-data" >
									<input type="hidden" name="tabID" value="<?php echo $_REQUEST["tabID"]; ?>" />
									<?php if($countItems > 0){ ?>
									<fieldset>
										<legend>Kunden-Kontakte</legend>
										<table border="0" width="100%" cellspacing="0" cellpadding="0" class="displayOrders">
											<tr>
												<th>#</th>
												<th>Anrede</th>
												<th>Vorname</th>
												<th>Nachname</th>
												<th>Funktion</th>
												<th>Telefon</th>
												<th>Fax</th>
												<th>Mobil</th>
												<th>E-Mail</th>
												<th>Info</th>
											</tr>
											<?php
												$count = 0;
												while($ds = mysqli_fetch_assoc($rs)){
													if($count%2 == 0){ $rowClass = 'row0'; }
													else { $rowClass = 'row1'; }

													echo '<tr ' . $thisStyle . ' class="'.$rowClass.'">';
													echo '<td style="text-align:right;">';
													echo '<b>' . ($count + 1) . '.</b>';
													echo '<input type="hidden" name="editCustomerContacts[' . $ds["customersContactsID"] . '][ID]" class="" value="' . $ds["customersContactsID"] . '" />';
													echo '<input type="hidden" name="editCustomerContacts[' . $ds["customersContactsID"] . '][ContactID]" class="" value="' . $ds["customersContactsContactID"] . '" />';
													// $_REQUEST["editID"]
													echo '</td>';
													echo '<td style="white-space:nowrap;">';
													echo '<select name="editCustomerContacts[' . $ds["customersContactsID"] . '][Salutation]" class="inputSelect_70">';
													echo '<option value=""> - </option>';
														if(!empty($arrSalutationTypeDatas)) {
															foreach($arrSalutationTypeDatas as $thisKey => $thisValue) {
																$selected = '';
																if($thisKey == $ds["customersContactsSalutation"]) {
																	$selected = ' selected ';
																}
																echo '<option value="' . $thisKey . '" '.$selected.' >' . ($arrSalutationTypeDatas[$thisKey]["salutationTypesName"]). '</option>';
															}
														}
													echo '</select> - ';
													echo '</td>';
													echo '<td>';
													echo '<input type="text" name="editCustomerContacts[' . $ds["customersContactsID"] . '][FirstName]" class="inputField_160" value="' . $ds["customersContactsFirstName"] . '" />';
													echo '</td>';
													echo '<td>';
													echo '<input type="text" name="editCustomerContacts[' . $ds["customersContactsID"] . '][SecondName]" class="inputField_160" value="' . $ds["customersContactsSecondName"] . '" />';
													echo '</td>';
													echo '<td>';
													echo '<select name="editCustomerContacts[' . $ds["customersContactsID"] . '][Function]" class="inputSelect_100">';
													echo '<option value=""> - </option>';
															if(!empty($arrCustomerContactTypeDatas)) {
																foreach($arrCustomerContactTypeDatas as $thisKey => $thisValue) {
																	$selected = '';
																	if($thisKey == $ds["customersContactsFunction"]) {
																		$selected = ' selected ';
																	}
																	echo '<option value="' . $thisKey . '" '.$selected.' >' . ($arrCustomerContactTypeDatas[$thisKey]["contactTypeName"]). '</option>';
																}
															}
													echo '</select>';
													echo '</td>';
													echo '<td><input type="text" name="editCustomerContacts[' . $ds["customersContactsID"] . '][Phone]" class="inputField_160" value="' . $ds["customersContactsPhone"]. '" /></td>';
													echo '<td><input type="text" name="editCustomerContacts[' . $ds["customersContactsID"] . '][Fax]" class="inputField_160" value="' . $ds["customersContactsFax"]. '" /></td>';
													echo '<td><input type="text" name="editCustomerContacts[' . $ds["customersContactsID"] . '][Mobile]" class="inputField_160" value="' . $ds["customersContactsMobile"]. '" /></td>';
													echo '<td style="white-space:nowrap;">';
													echo '<input type="text" name="editCustomerContacts[' . $ds["customersContactsID"] . '][Mail]" class="inputField_160" value="' . $ds["customersContactsMail"]. '" />';
													if($ds["customersContactsMail"] != ""){
														echo ' <a href="mailto:' . $ds["customersContactsMail"] . '" ><img src="layout/icons/iconMail.gif" width="16" height="14" alt="Mail senden" title="Diesen Kontakt per Mail kontaktieren" /></a>';
													}
													else {
														echo ' <img src="layout/icons/spacer.gif" width="16" height="14" alt="" title="" />';
													}
													echo '</td>';
													echo '<td>';
													echo '<a href="' . $_SERVER["PHP_SELF"]. '?editID=' . $_REQUEST["editID"] . '&deleteContactsID=' . $ds["customersContactsID"] . '#tabs-9" onclick="return showWarning(\'Soll der Kontakt wirklich entfernt werden?\');" ><img src="layout/icons/iconDelete.png" width="16" height="16" alt="Kontakt entfernen" title="Diesen Kontakt entfernen" /></a>';
													echo '</td>';
													echo '</tr>';

													$count++;
												}
											?>
										</table>
									</fieldset>
									<?php } ?>
									<hr />
									<fieldset>
										<legend>Neue Kontakte hinzuf&uuml;gen</legend>
										<table border="0" width="100%" cellspacing="0" cellpadding="0" class="displayOrders">
											<tr>
												<th>#</th>
												<th>Anrede</th>
												<th>Vorname</th>
												<th>Nachname</th>
												<th>T&auml;tigkeit</th>
												<th>Telefon</th>
												<th>Fax</th>
												<th>Mobil</th>
												<th>E-Mail</th>
												<th>Info</th>
											</tr>
											<?php
												$count = 0;
												for($i = 0; $i < 2 ; $i++){
													if($count%2 == 0){ $rowClass = 'row0'; }
													else { $rowClass = 'row1'; }

													echo '<tr ' . $thisStyle . ' class="'.$rowClass.'">';
													echo '<td style="text-align:right;">';
													echo '<b>' . ($count + 1) . '.</b>';
													echo '<input type="hidden" name="addCustomerContacts['.$i.'][ID]" class="" value="' . $i . '" />';
													echo '<input type="hidden" name="addCustomerContacts['.$i.'][ContactID]" class="" value="' . $_REQUEST["editID"] . '" />';
													// $_REQUEST["editID"]
													echo '</td>';
													echo '<td style="white-space:nowrap;">';
													echo '<select name="addCustomerContacts['.$i.'][Salutation]" class="inputSelect_70">';
													echo '<option value=""> - </option>';

														if(!empty($arrSalutationTypeDatas)) {
															foreach($arrSalutationTypeDatas as $thisKey => $thisValue) {
																echo '<option value="' . $thisKey . '" >' . ($arrSalutationTypeDatas[$thisKey]["salutationTypesName"]). '</option>';
															}
														}
													echo '</select> - ';
													echo '</td>';
													echo '<td>';
													echo '<input type="text" name="addCustomerContacts['.$i.'][FirstName]" class="inputField_160" value="" />';
													echo '</td>';
													echo '<td>';
													echo '<input type="text" name="addCustomerContacts['.$i.'][SecondName]" class="inputField_160" value="" />';
													echo '</td>';
													echo '<td>';
													echo '<select name="addCustomerContacts['.$i.'][Function]" class="inputSelect_100">';
													echo '<option value=""> - </option>';

															if(!empty($arrCustomerContactTypeDatas)) {
																foreach($arrCustomerContactTypeDatas as $thisKey => $thisValue) {
																	echo '<option value="' . $thisKey . '" >' . ($arrCustomerContactTypeDatas[$thisKey]["contactTypeName"]). '</option>';
																}
															}

													echo '</select>';
													echo '</td>';
													echo '<td><input type="text" name="addCustomerContacts['.$i.'][Phone]" class="inputField_160" value="" /></td>';
													echo '<td><input type="text" name="addCustomerContacts['.$i.'][Fax]" class="inputField_160" value="" /></td>';
													echo '<td><input type="text" name="addCustomerContacts['.$i.'][Mobile]" class="inputField_160" value="" /></td>';
													echo '<td style="white-space:nowrap;">';
													echo '<input type="text" name="addCustomerContacts['.$i.'][Mail]" class="inputField_160" value="" />';
													echo ' <img src="layout/icons/spacer.gif" width="16" height="14" alt="" title="" />';
													echo '</td>';
													echo '<td>

													</td>';
													echo '</tr>';

													$count++;
												}
											?>
										</table>
										<input type="hidden" name="editCustomersKundennummer3" value="<?php echo $customerDatas["customersKundennummer"]; ?>" />
										<input type="hidden" name="editID" value="<?php echo $_REQUEST["editID"]; ?>" />
									</fieldset>

									<div class="actionButtonsArea">
										<input type="submit" class="inputButton1 inputButtonGreen" name="storeContacts" value="Kontakt speichern" />
										&nbsp;
										<input type="submit" class="inputButton1 inputButtonOrange" name="resetContacts" value="Abbrechen" onclick="return showWarning(' Wollen Sie diesen Vorgang wirklich abbrechen??? ');" />
									</div>
								</form>
							</div>
							<?php
								}
								}
							?>
						</div>
					</div>
					<?php
						}
					?>

				</div>
			</div>
		</div>
		<div class="clear"></div>
	</div>
</div>

<script language="javascript" type="text/javascript">
	$(document).ready(function() {
		$('#searchCustomerNumber').keyup(function () {
			loadSuggestions('searchCustomerNumber', [{'triggerElement': '#searchCustomerNumber', 'fieldNumber': '#searchCustomerNumber', 'fieldName': '#searchCustomerName', 'fieldCity': '', 'fieldZipCode': ''}], 1);
		});
		$('#searchCustomerName').keyup(function () {
			loadSuggestions('searchCustomerName', [{'triggerElement': '#searchCustomerName', 'fieldNumber': '#searchCustomerNumber', 'fieldName': '#searchCustomerName', 'fieldCity': '', 'fieldZipCode': ''}], 1);
		});

		$('#editCustomersFirmenadressePLZ').keyup(function () {
			loadSuggestions('searchPlzCity', [{'triggerElement': '#editCustomersFirmenadressePLZ', 'fieldCity': '#editCustomersFirmenadresseOrt', 'fieldZipCode': '#editCustomersFirmenadressePLZ'}], 1);
		});
		$('#editCustomersFirmenadresseOrt').keyup(function () {
			loadSuggestions('searchCityPlz', [{'triggerElement': '#editCustomersFirmenadresseOrt', 'fieldCity': '#editCustomersFirmenadresseOrt', 'fieldZipCode': '#editCustomersFirmenadressePLZ'}], 1);
		});

		$('#editCustomersParentCompanyNumber').keyup(function () {
			loadSuggestions('searchCustomerNumberAndID', [{'triggerElement': '#editCustomersParentCompanyNumber', 'fieldNumber': '#editCustomersParentCompanyNumber', 'fieldID': '#editCustomersParentCompanyID'}], 1);
		});

		/*
		$('#editCustomersFirmenadresseStrasse').keyup(function () {
			loadSuggestions('searchStreetName', [{'triggerElement': '#editCustomersFirmenadresseStrasse', 'fieldStreet': '#editCustomersFirmenadresseStrasse'}], 0);
		});
		$('#editCustomersLieferadresseStrasse').keyup(function () {
			loadSuggestions('searchStreetName', [{'triggerElement': '#editCustomersLieferadresseStrasse', 'fieldStreet': '#editCustomersLieferadresseStrasse'}], 0);
		});
		$('#editCustomersRechnungsadresseStrasse').keyup(function () {
			loadSuggestions('searchStreetName', [{'triggerElement': '#editCustomersRechnungsadresseStrasse', 'fieldStreet': '#editCustomersRechnungsadresseStrasse'}], 0);
		});
		*/

		$('#editCustomersLieferadressePLZ').keyup(function () {
			loadSuggestions('searchPlzCity', [{'triggerElement': '#editCustomersLieferadressePLZ', 'fieldCity': '#editCustomersLieferadresseOrt', 'fieldZipCode': '#editCustomersLieferadressePLZ'}], 1);
		});
		$('#editCustomersLieferadresseOrt').keyup(function () {
			loadSuggestions('searchCityPlz', [{'triggerElement': '#editCustomersLieferadresseOrt', 'fieldCity': '#editCustomersLieferadresseOrt', 'fieldZipCode': '#editCustomersLieferadressePLZ'}], 1);
		});

		$('#editCustomersRechnungsadressePLZ').keyup(function () {
			loadSuggestions('searchPlzCity', [{'triggerElement': '#editCustomersRechnungsadressePLZ', 'fieldCity': '#editCustomersRechnungsadresseOrt', 'fieldZipCode': '#editCustomersRechnungsadressePLZ'}], 1);
		});
		$('#editCustomersRechnungsadresseOrt').keyup(function () {
			loadSuggestions('searchCityPlz', [{'triggerElement': '#editCustomersRechnungsadresseOrt', 'fieldCity': '#editCustomersRechnungsadresseOrt', 'fieldZipCode': '#editCustomersRechnungsadressePLZ'}], 1);
		});
		$('#searchPLZ').keyup(function () {
			loadSuggestions('searchPLZ', [{'triggerElement': '#searchPLZ', 'fieldZipCode': '#searchPLZ'}], 1);
		});

		$('#Rechnungsadresse_Firmenadresse').live('click', function(){
			if($(this).attr('checked') == 'checked') {
				// copyFields('#formEditCustomerDatas', '#Firmenadresse', '#Rechnungsadresse', '#Rechnungsadresse_Firmenadresse');
			}
			//$('#Rechnungsadresse').toggle();
		});
		$('#Lieferadresse_Firmenadresse').live('click', function(){
			if($(this).attr('checked') == 'checked') {
				// copyFields('#formEditCustomerDatas', '#Firmenadresse', '#Lieferadresse', '#Lieferadresse_Firmenadresse');
			}
			//$('#Lieferadresse').toggle();
		});

		$('#editCustomersBankName').keyup(function(){
			loadSuggestions('searchBankName', [{'triggerElement': '#editCustomersBankName', 'fieldCode': '#editCustomersBankLeitzahl', 'fieldName': '#editCustomersBankName', 'fieldBIC': '#editCustomersBankBIC', 'fieldIBAN': '#editCustomersBankIBAN'}], 1);
		});
		$('#editCustomersBankLeitzahl').keyup(function(){
			loadSuggestions('searchBankCode', [{'triggerElement': '#editCustomersBankLeitzahl', 'fieldCode': '#editCustomersBankLeitzahl', 'fieldName': '#editCustomersBankName', 'fieldBIC': '#editCustomersBankBIC', 'fieldIBAN': '#editCustomersBankIBAN'}], 1);
		});

		$('#editCustomersVertreterName').keyup(function() {
			loadSuggestions('searchSalesmen', [{'triggerElement': '#editCustomersVertreterName', 'fieldName': '#editCustomersVertreterName', 'fieldID': '#editCustomersVertreterID'}], 1);
		});
		$('#editCustomersVertreterName').focus(function() {
			if(($this).val() == '') { $('#editCustomersVertreterID').val(''); }
		});

		$('#editCustomersUseSalesmanDeliveryAdress').click(function() {
			var salesmanID = $('#editCustomersVertreterID').val();
			loadSalesmansAdress(salesmanID, 'delivery', '#editCustomersUseSalesmanDeliveryAdress');
		});
		$('#editCustomersUseSalesmanInvoiceAdress').click(function() {
			var salesmanID = $('#editCustomersVertreterID').val();
			loadSalesmansAdress(salesmanID, 'invoice', '#editCustomersUseSalesmanInvoiceAdress');
		});

		$('.buttonLoadOrderDetails').click(function() {
			var arrThisData = $(this).attr('alt').split('#');
			loadOrderDocumentDetails(arrThisData[2], arrThisData[1], arrThisData[0]);
		});

		$('.buttonConvertDocument').mouseenter(function() {

		});
		$('.buttonCopyDocument').mouseenter(function() {

		});

		$('.buttonSendDocument').click(function() {
			var mailDocumentDatas = $(this).attr('rel');
			sendAttachedDocument($(this), mailDocumentDatas, '<?php echo $thisDocumentValue[$thisKey]["orderDocumentsCustomerNumber"]; ?>', '<?php echo $_REQUEST["editID"]; ?>', '', '<?php echo $_SERVER["PHP_SELF"]; ?>', '');
		});

		<?php if($customerDatas["customersKundennummer"] == "") { ?>
		$('#editCustomersKundennummer').keyup(function() {
			checkIfCustomerNumberAlreadyExists($(this), '<?php echo LENGTH_CUSTOMER_NUMBER; ?>');
		});
		<?php } ?>

		<?php
			if($_REQUEST["editID"] != "NEW")
			{
		?>
		$(function() {
			// $('#tabs').tabs();
			// BOF
			$('#tabs').attr('class', 'ui-tabs ui-widget ui-widget-content ui-corner-all');
			$('#tabs ul:first').attr('class', 'ui-tabs-nav ui-helper-reset ui-helper-clearfix ui-widget-header ui-corner-all').attr('role', 'tablist');
			$('#tabs ul:first li').attr('class', 'ui-state-default ui-corner-top ui-tabs-active ui-state-active').attr('role', 'tab');
			$('#tabs ul:first li a').attr('class', 'ui-tabs-anchor').attr('role', 'presentation');
			// EOF
		});
		<?php
			if($_REQUEST["editID"] != "") {
				if($customerDatas["customersDatasUpdated"] != '1') {
					echo 'window.alert("Bitte kontrollieren Sie diese Daten mit den Daten des alten Auftragslisten-Programms und speichern Sie diese!\nAnsonsten ist dieser Kunde zur weiteren Verwendung \n(Schreiben von Angeboten, Aufträgen, Rechnungen etc. ) \ngesperrt! ");';
				}
			}
		?>
		<?php
			}
		?>

		$('.sideInfoItem').click(function () {
			var plzSalesmanID = $(this).attr('id');
			var plzSalesmanName = $(this).find('.sideInfoText').text();
			$('#editCustomersVertreterName').val(plzSalesmanName);
			$('#editCustomersVertreterID').val(plzSalesmanID);
		});

		colorRowMouseOver('.displayOrders tbody tr');
		$('.buttonNotice').mouseenter(function () {
			loadNotice($(this));
		});
		setFormDutyFields(<?php echo $jsonFormDutyFields; ?>);

		$('#editCustomersInvoicesNotPurchased').click(function() {
			if($(this).attr('checked') == 'checked'){
				$(this).parent().css('background-color', '#FF7F84');
			}
			else {
				$(this).parent().css('background-color', '#00FF00');
			}
		});
		$('#editCustomersBadPaymentBehavior').click(function() {
			if($(this).attr('checked') == 'checked'){
				$(this).parent().css('background-color', '#FF7F84');
			}
			else {
				$(this).parent().css('background-color', '#00FF00');
			}
		});
		$(function() {
			$.datepicker.setDefaults($.datepicker.regional[""]);
			$('#addCustomerCalendarDates').datepicker($.datepicker.regional["de"]);
			$('#editCustomersEntryDate').datepicker($.datepicker.regional["de"]);
			$('#editCustomersEntryDate').parent().append('<span class="buttonClearField"><img src="layout/icons/iconDelete.png" width="16" height="16" alt="" title="Eingabefeld leeren" /></div>');
			$('#editCustomersParentCompanyNumber').parent().append('<span class="buttonClearField"><img src="layout/icons/iconDelete.png" width="16" height="16" alt="" title="Eingabefeld leeren" /></div>');
			$('.buttonClearField').click(function () {
				$(this).parent().find('input').val('');
				$(this).parent().find('input').attr('checked', false);
			});
		});
		/*
		$('.sideInfo').draggable({
            drag: function(event, ui) {
				$('.sideInfo').css('cursor', 'move');
			}
        });
		*/
		$('.sideInfoHeader').dblclick(function(){
			$('.sideInfoContent').toggle();
		});
	});
</script>

<?php require_once('inc/footerHTML.inc.php'); ?>