<?php
	require_once('inc/requires.inc.php');

	if(!$arrGetUserRights["displayDistribution"]) {
		header('location: ' . PAGE_EXIT_LOCATION);
		exit;
	}

	if($_POST["displayOrdersCount"] !="")
	{
		$displayOrdersCount = $_POST["displayOrdersCount"];
	}
	else {
		$displayOrdersCount = DEFAULT_ONLINE_ORDERS_MONTHS;
	}

	$jswindowMessage = "";
	$warningMessage = '';
	$errorMessage = '';
	$successMessage = '';
	$infoMessage = '';

	$dbConnection = new DB_Connection();
	$db_open = $dbConnection->db_connect();

	// BOF READ VERTRETER
		$arrSalesmenDatas = getSalesmen2();
		$arrTemp = array();
		foreach($arrSalesmenDatas as $thisValue){
			$arrTemp[$thisValue["customersID"]] = $thisValue;
		}
		$arrSalesmenDatas = $arrTemp;
		#$arrAquisitionUsers = getAquisitionUsers();
	// EOF READ VERTRETER

	// BOF DOWNLOAD FILE
	if($_GET["downloadFile"] != "" && $_GET["documentType"] != "") {
		$fileDirectory = DIRECTORY_CREATED_DOCUMENTS_CUSTOMERS;
		$_GET["downloadFile"] = utf8_decode(urldecode($_GET["downloadFile"]));

		require_once('classes/downloadFile.class.php');
		$thisDownload = new downloadFile($fileDirectory, $_GET["downloadFile"]);
		$errorMessage .= $thisDownload->startDownload();
	}
	// EOF DOWNLOAD FILE

// BOF STORE STOCK DATAS
if($arrGetUserRights["adminArea"] == '1' && $_POST["storeStockDatas"] != ""){
	if($_POST["arrInsertStockDatasSalesmanCustomerID"] > 0){
		$_REQUEST["searchSalesmanCustomerID"] = $_POST["arrInsertStockDatasSalesmanCustomerID"];
		$sql_insertStockDatas = "
				INSERT INTO `". TABLE_SALESMEN_STOCKS . "` (
							`salesmenStocksID`,
							`salesmenStocksZipcode`,
							`salesmenStocksSalesmanID`,
							`salesmenStocksSalesmanCustomerNumber`,
							`salesmenStocksCustomerNumber`,
							`salesmenStocksOrderDate`,
							`salesmenStocksProductNumber`,
							`salesmenStocksProductName`,
							`salesmenStocksProductCategory`,
							`salesmenStocksProductQuantity`
						)
						VALUES
			";
		if(!empty($_POST["arrInsertStockDatas"])){
			$arrSql = array();
			foreach($_POST["arrInsertStockDatas"] as $thisKey => $thisValue){
				$arrSql[] = "
						(
							'%',
							'',
							'" . $_POST["arrInsertStockDatasSalesmanCustomerID"] . "',
							'" . $_POST["arrInsertStockDatasSalesmanCustomerID"] . "',
							'',
							'" . formatDate($_POST["arrInsertStockDatas"][$thisKey]["date"], 'store') . "',
							'" . $_POST["arrInsertStockDatas"][$thisKey]["productNumber"] . "',
							'" . $_POST["arrInsertStockDatas"][$thisKey]["productNumber"] . "',
							'" . $_POST["arrInsertStockDatas"][$thisKey]["productNumber"] . "',
							'" . $_POST["arrInsertStockDatas"][$thisKey]["productQuantity"] . "'
						)
					";
			}
			if(!empty($arrSql)){
				$sql_insertStockDatas .= implode(",", $arrSql);
				$rs_insertStockDatas = $dbConnection->db_query($sql_insertStockDatas);

				if(!$rs_insertStockDatas){
					$errorMessage .= 'Die Daten konnten nicht gespeichert werden.' . '<br />';
				}
				else {
					$successMessage .= 'Die Daten wurden gespeichert.' . '<br />';
				}
			}
		}
	}
}
// EOF STORE STOCK DATAS
?>

<?php
	require_once('inc/headerHTML.inc.php');
	$thisTitle = "Direkt-Verk&auml;ufe durch Vertreter";
	$headerHTML = preg_replace("/{###TITLE###}/", strip_tags($thisTitle), $headerHTML);
	echo $headerHTML;

	require_once(FILE_MENUE_TOP);
?>
<div id="mainArea">
	<div id="mainContent">
		<?php require_once(FILE_MENUE_SIDEBAR); ?>
		<div id="contentArea">
			<div id="contentAreaElements">
				<h1><?php if(SHOW_PAGE_TITLE_ICONS) { echo '<img src="' . PATH_ICONS_MENUE_TITLES . 'shop.png" alt="" />'; } ?> <?php echo $thisTitle; ?></h1>
				<div class="contentDisplay">

				<div id="searchFilterArea">
					<form name="formDisplayOrders" action="<?php echo $_SERVER["PHP_SELF"]; ?>" method="post">
						<table class="noborder" cellspacing="0" cellpadding="0">
							<tr>
								<td>
									<label for="searchSalesman">Vertreter:</label>
									<!--
									<input type="text" name="searchSalesman" id="searchSalesman" class="inputField_100" value="<?php echo $_REQUEST["searchSalesman"]; ?>" />
									-->
									<select name="searchSalesmanCustomerID"  class="inputSelect_170">
										<option value=""> --- Bitte w&auml;hlen --- </option>
										<?php
											if(!empty($arrSalesmenDatas)){
												foreach($arrSalesmenDatas as $thisKey => $thisValue){
													$selected = '';
													if($_REQUEST["searchSalesmanCustomerID"] == $arrSalesmenDatas[$thisKey]["customersID"]){
														$selected = ' selected="selected" ';
													}
													echo '<option value="' . $arrSalesmenDatas[$thisKey]["customersID"] . '" ' . $selected . ' >' . ($arrSalesmenDatas[$thisKey]["customersFirmenname"]) . '</option>';
												}
											}
										?>
									</select>
								</td>
								<!--
								<td>
									<label for="searchWord">Suchbegriff:</label>
									<input type="text" name="searchWord" id="searchWord" class="inputField_100" value="<?php echo $_REQUEST["searchWord"]; ?>" />
								</td>
								-->
								<td>
									<input type="submit" name="submitDisplayOrders" class="inputButton1" value="Daten anfordern" />
								</td>
							</tr>
						</table>
					</form>
				</div>

				<?php displayMessages(); ?>
				<?php
					// BOF INSERT STOCK DATAS
					if($_REQUEST["searchSalesmanCustomerID"] > 0){
						if($arrGetUserRights["adminArea"] == '1'){
				?>
				<div class="adminEditArea">
				<h2>Eingabe Vertreter-Bestand</h2>
				<form name="formInsertSalesmenStockDatas" method="post" action="">
				<table cellspacing="0" cellpadding="0" width="100%" class="displayOrders">
					<tr>
						<th>#</th>
						<th>Datum</th>
						<th>Artikel-Nummer</th>
						<th>Menge</th>
					</tr>
					<?php
						for($i = 0 ; $i < 10 ; $i++){
							if($i%2 == 0){ $rowClass = 'row1'; }
							else { $rowClass = 'row0'; }
					?>
					<tr class="<?php echo $rowClass; ?>">
						<td style="text-align:right;"><b><?php echo ($i + 1); ?>.</b></td>
						<td><input type="text" name="arrInsertStockDatas[<?php echo $i; ?>][date]" id="arrInsertStockDatas_<?php echo $i; ?>_date" class="newStockDate inputField_120" value="<?php echo date("d.m.Y"); ?>" /></td>
						<td><input type="text" name="arrInsertStockDatas[<?php echo $i; ?>][productNumber]" id="arrInsertStockDatas_<?php echo $i; ?>_productNumber" class="inputField_120" value="" /></td>
						<td><input type="text" name="arrInsertStockDatas[<?php echo $i; ?>][productQuantity]" id="arrInsertStockDatas_<?php echo $i; ?>_productQuantity" class="inputField_120" value="" /></td>
					</tr>
					<?php
						}
					?>
				</table>
				<input type="hidden" name="arrInsertStockDatasSalesmanCustomerID" value="<?php echo $_REQUEST["searchSalesmanCustomerID"]; ?>">
				<div class="actionButtonsArea">
					<input type="submit" name="storeStockDatas" class="inputButton0" value="Speichern" />
				</div>
				</form>
				</div>
				<?php
						}
					}
					// EOF INSERT STOCK DATAS
				?>
				<?php
					// BOF GET DIRECT SALES
					$where = "";
					if($_REQUEST["searchSalesmanCustomerID"] > 0){
						$where .= " AND `orderDocumentsSalesman` = '" . $_REQUEST["searchSalesmanCustomerID"] . "' ";
						$where .= " AND `orderDocumentDetailProductNumber` != '' ";

						// SET [GLOBAL | SESSION] group_concat_max_len = val;
						$sql = "SET SESSION `group_concat_max_len` = 8096;";
						$rs = $dbConnection->db_query($sql);

						$sql = "SELECT
										`". TABLE_ORDER_INVOICES_DETAILS . "`.`orderDocumentDetailOrderID`,

										`". TABLE_ORDER_INVOICES_DETAILS . "`.`orderDocumentDetailID`,
										`". TABLE_ORDER_INVOICES_DETAILS . "`.`orderDocumentDetailDocumentID`,
										`". TABLE_ORDER_INVOICES_DETAILS . "`.`orderDocumentDetailOrderType`,
										`". TABLE_ORDER_INVOICES_DETAILS . "`.`orderDocumentDetailPos`,
										`". TABLE_ORDER_INVOICES_DETAILS . "`.`orderDocumentDetailProductNumber`,
										`". TABLE_ORDER_INVOICES_DETAILS . "`.`orderDocumentDetailProductName`,
										`". TABLE_ORDER_INVOICES_DETAILS . "`.`orderDocumentDetailProductKategorieID`,
										SUM(`". TABLE_ORDER_INVOICES_DETAILS . "`.`orderDocumentDetailProductQuantity`) AS `orderDocumentDetailProductQuantitySum`,
										GROUP_CONCAT(
											`". TABLE_ORDER_INVOICES . "`.`orderDocumentsDocumentDate`,
											'#',
											`". TABLE_ORDER_INVOICES_DETAILS . "`.`orderDocumentDetailProductQuantity`,
											'#',
											`". TABLE_ORDER_INVOICES_DETAILS . "`.`orderDocumentDetailNotiz`,
											'#',
											`". TABLE_ORDER_INVOICES . "`.`orderDocumentsNumber`,
											'#',
											`". TABLE_ORDER_INVOICES . "`.`orderDocumentsDocumentPath`
											ORDER BY `". TABLE_ORDER_INVOICES . "`.`orderDocumentsDocumentDate` DESC
											SEPARATOR ';'
										) AS `orderDocumentDetailProductQuantityToDate`,
										`". TABLE_ORDER_INVOICES_DETAILS . "`.`orderDocumentDetailProductColorsText`,
										`". TABLE_ORDER_INVOICES_DETAILS . "`.`orderDocumentDetailProductColorsCount`,
										`". TABLE_ORDER_INVOICES_DETAILS . "`.`orderDocumentDetailsWithBorder`,
										`". TABLE_ORDER_INVOICES_DETAILS . "`.`orderDocumentDetailKommission`,
										`". TABLE_ORDER_INVOICES_DETAILS . "`.`orderDocumentDetailPrintText`,
										`". TABLE_ORDER_INVOICES_DETAILS . "`.`orderDocumentDetailNotiz`,
										`". TABLE_ORDER_INVOICES_DETAILS . "`.`orderDocumentDetailProductSinglePrice`,
										`". TABLE_ORDER_INVOICES_DETAILS . "`.`orderDocumentDetailProductTotalPrice`,
										`". TABLE_ORDER_INVOICES_DETAILS . "`.`orderDocumentDetailMwst`,
										`". TABLE_ORDER_INVOICES_DETAILS . "`.`orderDocumentDetailDiscount`,
										`". TABLE_ORDER_INVOICES_DETAILS . "`.`orderDocumentDetailDirectSale`,
										`". TABLE_ORDER_INVOICES_DETAILS . "`.`orderDocumentDetailPosType`,
										`". TABLE_ORDER_INVOICES_DETAILS . "`.`orderDocumentDetailGroupingID`,

										`". TABLE_ORDER_INVOICES . "`.`orderDocumentsOrderDate`,
										`". TABLE_ORDER_INVOICES . "`.`orderDocumentsSalesman`,

										'RE' AS `documentType`

									FROM `". TABLE_ORDER_INVOICES_DETAILS . "`

									LEFT JOIN `". TABLE_ORDER_INVOICES . "`
									ON(`". TABLE_ORDER_INVOICES_DETAILS . "`.`orderDocumentDetailDocumentID` = `". TABLE_ORDER_INVOICES . "`.`orderDocumentsID`)

									WHERE 1
										AND `". TABLE_ORDER_INVOICES_DETAILS . "`.`orderDocumentDetailDirectSale` = '1'
										AND `". TABLE_ORDER_INVOICES_DETAILS . "`.`orderDocumentDetailPosType` = 'product'
										". $where . "
							";
						$xxxx_sql .= "
								UNION

								SELECT
										`". TABLE_ORDER_CONFIRMATIONS_DETAILS . "`.`orderDocumentDetailOrderID`,

										`". TABLE_ORDER_CONFIRMATIONS_DETAILS . "`.`orderDocumentDetailID`,
										`". TABLE_ORDER_CONFIRMATIONS_DETAILS . "`.`orderDocumentDetailDocumentID`,
										`". TABLE_ORDER_CONFIRMATIONS_DETAILS . "`.`orderDocumentDetailOrderType`,
										`". TABLE_ORDER_CONFIRMATIONS_DETAILS . "`.`orderDocumentDetailPos`,
										`". TABLE_ORDER_CONFIRMATIONS_DETAILS . "`.`orderDocumentDetailProductNumber`,
										`". TABLE_ORDER_CONFIRMATIONS_DETAILS . "`.`orderDocumentDetailProductName`,
										`". TABLE_ORDER_CONFIRMATIONS_DETAILS . "`.`orderDocumentDetailProductKategorieID`,
										SUM(`". TABLE_ORDER_CONFIRMATIONS_DETAILS . "`.`orderDocumentDetailProductQuantity`) AS `orderDocumentDetailProductQuantitySum`,
										GROUP_CONCAT(
											`". TABLE_ORDER_CONFIRMATIONS . "`.`orderDocumentsDocumentDate`,
											'#',
											`". TABLE_ORDER_CONFIRMATIONS_DETAILS . "`.`orderDocumentDetailProductQuantity`,
											'#',
											`". TABLE_ORDER_CONFIRMATIONS_DETAILS . "`.`orderDocumentDetailNotiz`,
											'#',
											`". TABLE_ORDER_CONFIRMATIONS . "`.`orderDocumentsNumber`,
											'#',
											`". TABLE_ORDER_CONFIRMATIONS . "`.`orderDocumentsDocumentPath`
											ORDER BY `". TABLE_ORDER_CONFIRMATIONS . "`.`orderDocumentsDocumentDate` DESC
											SEPARATOR ';'
										) AS `orderDocumentDetailProductQuantityToDate`,
										`". TABLE_ORDER_CONFIRMATIONS_DETAILS . "`.`orderDocumentDetailProductColorsText`,
										`". TABLE_ORDER_CONFIRMATIONS_DETAILS . "`.`orderDocumentDetailProductColorsCount`,
										`". TABLE_ORDER_CONFIRMATIONS_DETAILS . "`.`orderDocumentDetailsWithBorder`,
										`". TABLE_ORDER_CONFIRMATIONS_DETAILS . "`.`orderDocumentDetailKommission`,
										`". TABLE_ORDER_CONFIRMATIONS_DETAILS . "`.`orderDocumentDetailPrintText`,
										`". TABLE_ORDER_CONFIRMATIONS_DETAILS . "`.`orderDocumentDetailNotiz`,
										`". TABLE_ORDER_CONFIRMATIONS_DETAILS . "`.`orderDocumentDetailProductSinglePrice`,
										`". TABLE_ORDER_CONFIRMATIONS_DETAILS . "`.`orderDocumentDetailProductTotalPrice`,
										`". TABLE_ORDER_CONFIRMATIONS_DETAILS . "`.`orderDocumentDetailMwst`,
										`". TABLE_ORDER_CONFIRMATIONS_DETAILS . "`.`orderDocumentDetailDiscount`,
										`". TABLE_ORDER_CONFIRMATIONS_DETAILS . "`.`orderDocumentDetailDirectSale`,
										`". TABLE_ORDER_CONFIRMATIONS_DETAILS . "`.`orderDocumentDetailPosType`,
										`". TABLE_ORDER_CONFIRMATIONS_DETAILS . "`.`orderDocumentDetailGroupingID`,

										`". TABLE_ORDER_CONFIRMATIONS . "`.`orderDocumentsOrderDate`,
										`". TABLE_ORDER_CONFIRMATIONS . "`.`orderDocumentsSalesman`,

										'AB' AS `documentType`

									FROM `". TABLE_ORDER_CONFIRMATIONS_DETAILS . "`

									LEFT JOIN `". TABLE_ORDER_CONFIRMATIONS . "`
									ON(`". TABLE_ORDER_CONFIRMATIONS_DETAILS . "`.`orderDocumentDetailDocumentID` = `". TABLE_ORDER_CONFIRMATIONS . "`.`orderDocumentsID`)

									WHERE 1
										AND `". TABLE_ORDER_CONFIRMATIONS_DETAILS . "`.`orderDocumentDetailDirectSale` = '1'
										AND `". TABLE_ORDER_CONFIRMATIONS_DETAILS . "`.`orderDocumentDetailPosType` = 'product'
										". $where . "
							";
						$sql .= "
									/* GROUP BY `orderDocumentDetailOrderID` */
									GROUP BY `". TABLE_ORDER_INVOICES_DETAILS . "`.`orderDocumentDetailProductNumber`
							";
					}
					// BOF GET DIRECT SALES

					// BOF GET SALESMAN STOCKS
					if($_REQUEST["searchSalesmanCustomerID"] > 0){
						$sql_stocks = "
								SELECT
									`salesmenStocksProductNumber`,
									`salesmenStocksID`,
									`salesmenStocksZipcode`,
									`salesmenStocksSalesmanID`,
									`salesmenStocksSalesmanCustomerNumber`,
									`salesmenStocksCustomerNumber`,
									GROUP_CONCAT(
										`salesmenStocksOrderDate`,
										'#',
										`salesmenStocksProductQuantity`
										ORDER BY `salesmenStocksOrderDate` DESC
										SEPARATOR ';'
									) AS `salesmenStocksProductQuantityToDate`,


									`salesmenStocksProductName`,
									`salesmenStocksProductCategory`,
									SUM(`salesmenStocksProductQuantity`) AS `salesmenStocksProductQuantitySum`
								FROM `". TABLE_SALESMEN_STOCKS . "`
								WHERE 1
									AND `salesmenStocksSalesmanID` = '" . $_REQUEST["searchSalesmanCustomerID"] . "'

								GROUP BY `salesmenStocksProductNumber`
							";
						$rs_stocks = $dbConnection->db_query($sql_stocks);
						while($ds_stocks = mysqli_fetch_assoc($rs_stocks)){
							foreach(array_keys($ds_stocks) as $field){
								if($ds_stocks["salesmenStocksProductNumber"] != ""){
									$arrStockDatas[$ds_stocks["salesmenStocksProductNumber"]][$field] = trim($ds_stocks[$field]);
								}
							}
						}
					}
					// EOF GET SALESMAN STOCKS

					$rs = $dbConnection->db_query($sql);
					$displayOrdersCount = $dbConnection->db_getMysqlNumRows($rs);

					if($displayOrdersCount > 0) {
						echo '<table cellspacing="0" cellpadding="0" width="100%" class="displayOrders">';
						echo '<thead>';
						echo '<tr>';
						echo '<th style="width:45px;text-align:right;">#</th>';
						echo '<th>Vertreter</th>';
						echo '<th>Art.-Nr</th>';
						echo '<th>Art.-Name</th>';
						echo '<th>Verkaufte Menge</th>';
						echo '<th>&Uuml;bergebene Menge</th>';
						echo '<th>Restbestand</th>';
						echo '<th>Info</th>';
						echo '</tr>';
						echo '</thead>';


						echo '<tbody>';

						$markerYear = '';
						$markerMonth = '';
						$countRow = 0;

						while ($ds = mysqli_fetch_assoc($rs)) {
							if($countRow%2 == 0){ $rowClass = 'row1'; }
							else { $rowClass = 'row0'; }

							echo '<tr class="' . $rowClass . '">';

							echo '<td style="text-align:right;">';
							echo '<b>'.($countRow + 1).'.</b> ';
							echo '</td>';

							echo '<td style="white-space:nowrap;">';
							echo $arrSalesmenDatas[$ds["orderDocumentsSalesman"]]["customersFirmenname"];
							echo '</td>';

							echo '<td>';
							echo $ds["orderDocumentDetailProductNumber"];
							echo '</td>';

							echo '<td>';
							echo $ds["orderDocumentDetailProductName"];
							echo '</td>';

							echo '<td style="text-align:right;">';
							$thisQuantity = $ds["orderDocumentDetailProductQuantitySum"];
							echo number_format($thisQuantity, 0, ',', '.') . '';
							echo '</td>';

							echo '<td style="text-align:right;">';
							$thisStock = $arrStockDatas[$ds["orderDocumentDetailProductNumber"]]["salesmenStocksProductQuantitySum"];
							echo number_format($thisStock, 0, ',', '.') . '';
							echo '</td>';

							echo '<td style="text-align:right;background-color:#FEFFAF; white-space:nowrap;">';
							$thisStockRest = ($thisStock - $thisQuantity);
							if($thisStockRest < 10){
								echo ' <img src="layout/icons/iconAttention.png" width="16" height="16" alt="Achtung" title="Achtung, Bestand gering"> ';
							}
							else {
								echo ' <img src="layout/icons/iconOk.png" width="16" height="16" alt="ok" title="Bestand in Ordnung"> ';
							}
							echo number_format($thisStockRest, 0, ',', '.') . '';
							echo '</td>';

							echo '<td style="text-align:right;">';
								echo '<span class="toolItem">';
								echo '<img src="layout/icons/iconInfo.png" class="buttonShopOrderInfo" alt="Details" title="Detailinformationen zum Bestand dieses Artikels ansehen">';
								echo '<div class="orderDetailsContent" style="display:none; visibility:hidden;">';
								echo '<p><b>Artikel:</b> ' . $ds["orderDocumentDetailProductNumber"] . ' (' . trim($ds["orderDocumentDetailProductName"]) . ')</p>';
								echo '<hr />';
								echo '<h2 style="border-bottom:1px solid #CCCCCC;">Details der Bestand-Aufstockung</h2>';
								$arrTemp = explode(";", $arrStockDatas[$ds["orderDocumentDetailProductNumber"]]["salesmenStocksProductQuantityToDate"]);

								if(!empty($arrTemp)){
									$arrThisProductQuantityToDate = array();
									foreach($arrTemp as $thisTempData){
										if($thisTempData != ""){
											$arrThisProductQuantityToDate[] = explode("#", $thisTempData);
										}
									}
									if(!empty($arrThisProductQuantityToDate)){
										echo '<table border="0" width="100%" cellpadding="0" cellspacing="0" class="border">';
											echo '<tr>';
											echo '<th style="width:45px;">#</th>';
											echo '<th>Datum</th>';
											echo '<th style="width:70px;">Menge</th>';
											echo '</tr>';
											$countDetailRow = 0;
											foreach($arrThisProductQuantityToDate as $thisProductQuantityToDate){
												if($thisProductQuantityToDate[1] != 0){
													if($countDetailRow%2 == 0){ $rowClass = 'row1'; }
													else { $rowClass = 'row0'; }
													echo '<tr class="' . $rowClass . '">';
													echo '<td style="width:45px;text-align:right;"><b>' . ($countDetailRow + 1) . '.</b></td>';
													echo '<td>' . formatDate($thisProductQuantityToDate[0], 'display') . '</td>';
													echo '<td style="width:45px;text-align:right;background-color:#FEFFAF;">' . number_format($thisProductQuantityToDate[1], 0, ',', '.') . '</td>';
													echo '</tr>';
													$countDetailRow++;
												}
											}
											echo '<tr style="background-color:#FEFFAF;">';
											echo '<td colspan="3"></td>';
											echo '</tr>';
											echo '<tr style="background-color:#FEFFAF;">';
											echo '<td colspan="2" style="text-align:right;"><b>Gesamt:</b></td>';
											echo '<td style="text-align:right;"><b>' . number_format($thisStock, 0, ',', '.') . '</b></td>';
											echo '</tr>';
										echo '</table>';
									}
									else {
										echo '<div class="warningArea">Der Bestand dieses Artikels f&uuml;r den Vertreter ist bisher noch nicht aufgestockt worden.</div>';
									}
								}
								else {
									echo '<div class="warningArea">Der Bestand dieses Artikels f&uuml;r den Vertreter ist bisher noch nicht aufgestockt worden.</div>';
								}
								echo '<hr />';

								echo '<h2 style="border-bottom:1px solid #CCCCCC;">Details der Direkt-Verk&auml;ufe</h2>';
								$arrTemp = explode(";", $ds["orderDocumentDetailProductQuantityToDate"]);
								if(!empty($arrTemp)){
									$arrThisProductQuantityToDate = array();
									foreach($arrTemp as $thisTempData){
										if($thisTempData != ""){
											$arrThisProductQuantityToDate[] = explode("#", $thisTempData);
										}
									}
									if(!empty($arrThisProductQuantityToDate)){
										echo '<table border="0" width="100%" cellpadding="0" cellspacing="0" class="border">';
											echo '<tr>';
											echo '<th style="width:45px;">#</th>';
											echo '<th style="width:70px;">Datum</th>';
											echo '<th style="width:70px;">Dokument</th>';
											echo '<th>Notiz</th>';
											echo '<th style="width:70px;">Menge</th>';
											echo '</tr>';
											$countDetailRow = 0;
											foreach($arrThisProductQuantityToDate as $thisProductQuantityToDate){
												if($thisProductQuantityToDate[1] != 0){
													if($countDetailRow%2 == 0){ $rowClass = 'row1'; }
													else { $rowClass = 'row0'; }
													echo '<tr class="' . $rowClass . '">';
													echo '<td style="width:45px;text-align:right;"><b>' . ($countDetailRow + 1) . '.</b></td>';
													echo '<td>' . formatDate($thisProductQuantityToDate[0], 'display') . '</td>';
													echo '<td style="white-space:nowrap;">';
													$thisDocumentKey = substr($thisProductQuantityToDate[3], 0, 2);
													echo '<a href="' . $_SERVER["PHP_SELF"] . '?searchSalesmanCustomerID=' . $_REQUEST["searchSalesmanCustomerID"] . '&downloadFile=' . urlencode(basename($thisProductQuantityToDate[4])) . '&documentType=' . $thisDocumentKey . '">' . '<img src="layout/icons/icon' . getFileType(basename($thisProductQuantityToDate[4])) . '.gif" width="16" height="16" title="Dokument herunterladen" alt="Download" /></a>';
													echo ' ';
													echo $thisProductQuantityToDate[3];
													echo '</td>';
													echo '<td>' . $thisProductQuantityToDate[2] . '</td>';
													echo '<td style="width:45px;text-align:right;background-color:#FEFFAF;">' . number_format($thisProductQuantityToDate[1], 0, ',', '.') . '</td>';
													echo '</tr>';
													$countDetailRow++;
												}
											}
											echo '<tr style="background-color:#FEFFAF;">';
											echo '<td colspan="5"></td>';
											echo '</tr>';
											echo '<tr style="background-color:#FEFFAF;">';
											echo '<td colspan="4" style="text-align:right;"><b>Gesamt:</b></td>';
											echo '<td style="text-align:right;"><b>' . number_format($thisQuantity, 0, ',', '.') . '</b></td>';
											echo '</tr>';
										echo '</table>';
									}
								}
								else {
									echo '<p class="warningArea">Der Vertreter hat diesen Artikel bisher noch nicht direkt verkauft.</p>';
								}

								echo '</div>';
								echo '</span>';
							echo '</td>';

							echo '</tr>';

							$countRow++;
						}
						echo '</tbody>';

						echo '</table>';
					}
					else {
						$infoMessage .= 'Moment liegen keine Daten vor. ' . '</br>';
					}
				?>

				<?php displayMessages(); ?>

				</div>
			</div>
		</div>
		<div class="clear"></div>
	</div>
</div>

<script language="javascript" type="text/javascript">
	$(document).ready(function() {
		$(function() {
			$.datepicker.setDefaults($.datepicker.regional[""]);
			$('.newStockDate').datepicker($.datepicker.regional["de"]);
		});
		$('.buttonShopOrderInfo').css('cursor', 'pointer');
		$('.buttonShopOrderInfo').click(function () {
			loadOrderDetails($(this), '<?php echo BASEPATH; ?>', 'Detailinformationen zu Bestand und Direktverk&auml;ufen dieses Artikels');
		});
		colorRowMouseOver('.displayOrders tbody tr');
		// toggleAreas();
	});
</script>

<?php require_once('inc/footerHTML.inc.php'); ?>