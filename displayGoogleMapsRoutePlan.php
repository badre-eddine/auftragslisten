<?php
	session_start();

	require_once('config/configMandator.inc.php');
	require_once('config/configParams_' . strtolower(MANDATOR) . '.inc.php');
	require_once('config/configBasic.inc.php');
	require_once('config/configFiles.inc.php');
	require_once('config/configTables.inc.php');
	require_once('inc/functions.inc.php');
	##require_once('header.inc.php');

	DEFINE('DISPLAY_STEP_MARKER', 0);
	DEFINE('MAX_GOOGLE_WAYPOINTS', 8);

	$arrGetUserRights = readUserRights();
	if($arrGetUserRights["displayDistribution"] || $arrGetUserRights["adminArea"] ){
		DEFINE('SHOW_ZIPCODE_AREAS', true);
	}
	else {
		DEFINE('SHOW_ZIPCODE_AREAS', true);
	}
	DEFINE('SHOW_COUNTRY_AREAS', true);

	// BOF GOGGLE GET ROUT DATA
	/*
	GET DATA URL:
	http://maps.googleapis.com/maps/api/directions/json?origin=M%C3%BCnster,Germany&destination=Hamburg,Germany&waypoints=Essen,Germany&sensor=false&language=de
	maps.googleapis.com/maps/api/geocode/json?address=15+Johanniter+Strasse,+Muenster,+Germany&sensor=false&language=de
	maps.googleapis.com/maps/api/geocode/json?address=15+Johanniter+Strasse,+48145,+Germany&sensor=false&language=de
	maps.googleapis.com/maps/api/geocode/json?address=48145,Germany&components=postal_code&sensor=false&language=de
	maps.googleapis.com/maps/api/geocode/json?address=48,Germany&components=postal_code_prefix&sensor=false&language=de


	*/
	// EOF GOGGLE GET ROUT DATA

	// BOF DEFINE ROUTE COLORS
	$arrRouteColors = array(
		//"#D62FA9",
		//"#05FC7C",
		//"#29EEF2",
		//"#CE1421",
		//"#DD6B06",
		//"#298FF4",
		"#FF0000"
	);
	// EOF DEFINE ROUTE COLORS

	$warningMessage = '';
	$errorMessage = '';
	$successMessage = '';
	$infoMessage = '';

	$dbConnection = new DB_Connection(DB_HOST, '', DB_NAME, DB_USER, DB_PASSWORD);
	$dbOpen = $dbConnection->db_connect();

	if(DB_HOST_EXTERN_ACQUISITION != '' && DB_NAME_EXTERN_ACQUISITION != '' && DB_USER_EXTERN_ACQUISITION != '' && DB_PASSWORD_EXTERN_ACQUISITION) {
		$existsOnlineAquisition = true;
	}
	else {
		$existsOnlineAquisition = false;
		$errorMessage .= 'Es sind keine Zugansdaten f&uuml;r die Datenbankverbindung zur ' . strtoupper(MANDATOR) . '-Kundenerfassung eingetragen. ' . '<br />';
	}

	if($existsOnlineAquisition) {
		$dbConnection_ExternAcqisition = new DB_Connection(DB_HOST_EXTERN_ACQUISITION, '', DB_NAME_EXTERN_ACQUISITION, DB_USER_EXTERN_ACQUISITION, DB_PASSWORD_EXTERN_ACQUISITION);
		$db_openExternAcqisition = $dbConnection_ExternAcqisition->db_connect();
	}

	// BOF GET GOOGLE MAPS POINTS
	$thisSearchString = $_GET["strZipcodes"];
	$thisMaptype = $_GET["strMaptype"];
	if($thisMaptype == "") {
		$thisMaptype = "routes";
	}

	// BOF READ USER DATAS
		$arrTemp = explode("|", $thisSearchString);
		$userID = $arrTemp[0];
	// EOF READ USER DATAS

		$userLocation = $arrTemp[1];

		$arrAquisitionUsers = getAquisitionUsers('usersID');
		$arrSalesmenDatas = $arrAquisitionUsers;

		$strUserAreas = '';
		$arrUserAreas = array();
		if(!empty($arrSalesmenDatas)){
			foreach($arrSalesmenDatas as $thisKey => $thisValue){
				//if($thisValue["usersAddressCity"] . ',' . $thisValue["usersAddressZipcode"] . ',' . $thisValue["usersAddressStreetNumber"] . ',' . $thisValue["usersAddressStreet"] == $userLocation){
				if($thisKey == $userID){
					$strUserAreas = $thisValue["usersAreas"];
					$arrUserAreas = explode(";", $strUserAreas);
				}
			}
		}
	// EOF READ USER DATAS

	// BOF GET GOOGLE MAPS COUNTRY AREAS
		$arrGoogleMapCountryAreas = array();
		if(SHOW_COUNTRY_AREAS){
			$sql_mapAreas = "SELECT
						`lat` AS `latitude`,
						`lon` AS `longitude`,
						`country`,
						`subcountry`,
						IF(`subcountry` != '' , CONCAT(`country`, '_', `subcountry`), `country`) AS `area`

						FROM `" . TABLE_GEODB_AREAS . "`

						WHERE 1
							AND `subcountry` = ''
							AND `country` = 'DE'
				";
			$rs_mapAreas = $dbConnection->db_query($sql_mapAreas);

			while($ds_mapAreas = mysqli_fetch_assoc($rs_mapAreas)) {
				$arrGoogleMapCountryAreas[$ds_mapAreas["area"]][] = 'new google.maps.LatLng(' . $ds_mapAreas["latitude"] . ', ' . $ds_mapAreas["longitude"] . ')';
			}
		}
	// EOF GET GOOGLE MAPS COUNTRY AREAS

	// BOF GET GOOGLE MAPS ZIP CODE AREAS
		$arrGoogleMapZipCodeAreas = array();
		/*
		if(!empty($arrUserAreas) && SHOW_ZIPCODE_AREAS) {
			$sql_mapZipCodeAreas = "SELECT
								`zipcodeDoubleDigit`,
								`zipcode`,
								`city`,
								`zipcodeCity`,
								`coordinates`,
								GROUP_CONCAT(`coordinates` SEPARATOR ',') AS `coordinates2`,
								`active`

						FROM `" . TABLE_GEODB_ZIPCODE_AREAS . "`

						WHERE 1
							AND `active` = '1'
							AND (
								`zipcodeDoubleDigit` = '" . implode("' OR `zipcodeDoubleDigit` = '", $arrUserAreas). "'
							)
						GROUP BY `zipcode`
				";

			$rs_mapZipCodeAreas = $dbConnection->db_query($sql_mapZipCodeAreas);

			while($ds_mapZipCodeAreas = mysqli_fetch_assoc($rs_mapZipCodeAreas)) {
				$thisCoordinates = $ds_mapZipCodeAreas["coordinates"];
				#dd('thisCoordinates');
				$thisCoordinates = preg_replace("/[\[]{2,}/", "[", $thisCoordinates);
				$thisCoordinates = preg_replace("/[\]]{2,}/", "]", $thisCoordinates);
				$thisCoordinates = preg_replace("/[\}]{1,}$/", "", $thisCoordinates);
				if(!preg_match("/\]$/", $thisCoordinates)){
					$thisCoordinates .= ']';
				}

				#$searchPattern = "/(\[)(.*),(.*)(\])/ismU";
				$searchPattern = "/(\[)([0-9\.\-]*),([0-9\.\-]*)(\])/ismU";
				$replace = "new google.maps.LatLng($3, $2)";

				$thisCoordinates = preg_replace($searchPattern, $replace, $thisCoordinates);
				$arrGoogleMapZipCodeAreas[$ds_mapZipCodeAreas["zipcode"]] = $thisCoordinates;
			}
		}
		*/
	// EOF GET GOOGLE MAPS ZIP CODE AREAS

	// BOF GET GOOGLE MAPS DOUBLEDIGITZIP CODE AREAS
		$arrGoogleMapDoubleDigitZipCodeAreas = array();

		if(!empty($arrUserAreas) && SHOW_ZIPCODE_AREAS) {
			$sql_mapDoubleDigitZipCodeAreas = "SELECT
									`zipcodeDoubleDigit`,
									`coordinates`

							FROM `geodb_zipcodeDoubleDigitAreadatas`

							WHERE 1
								AND (
									`zipcodeDoubleDigit` = '" . implode("' OR `zipcodeDoubleDigit` = '", $arrUserAreas). "'
								)
					";
			$rs_mapDoubleDigitZipCodeAreas = $dbConnection->db_query($sql_mapDoubleDigitZipCodeAreas);

			while($ds_mapDoubleDigitZipCodeAreas = mysqli_fetch_assoc($rs_mapDoubleDigitZipCodeAreas)) {
				$thisCoordinates = $ds_mapDoubleDigitZipCodeAreas["coordinates"];

				$searchPattern = "/(\[)([0-9\.\-]*),([0-9\.\-]*)(\])/ismU";
				$replace = "new google.maps.LatLng($2, $3)";

				$thisCoordinates = preg_replace($searchPattern, $replace, $thisCoordinates);
				$arrGoogleMapDoubleDigitZipCodeAreas[$ds_mapDoubleDigitZipCodeAreas["zipcodeDoubleDigit"]] = $thisCoordinates;
			}
			$arrGoogleMapZipCodeAreas = $arrGoogleMapDoubleDigitZipCodeAreas;
		}
	// EOF GET GOOGLE MAPS DOUBLEDIGITZIP CODE AREAS

	if($thisMaptype == "routes"){
		$$arrTemp = explode("|", $thisSearchString);
		$userLocation = $arrTemp[1];
		$arrSplitData = explode(",", $arrTemp[1]);
		$userLocationPLZ = 	$arrSplitData[1];
		$userLocationStart = $arrTemp[1] . ",Germany";
		$userLocationEnd = $arrTemp[1] . ",Germany";
		$userTargets = $arrTemp[2];

		$arrAreas = explode("_", $userTargets);

		$destinations = "";
		$arrDestinations = array();
		$waypoints = "";
		$arrWaypoints = array();

		if(!empty($arrAreas)){
			foreach($arrAreas as $thisArea){
				if($thisArea != $userLocation){
					$arrDestinations[] = $thisArea . ",Germany";
					$arrWaypoints[] = "{location: '" . $thisArea . ", Germany" . "', stopover:true}";
				}
			}
			$destinations = implode("|", $arrDestinations);
			$waypoints = implode(", ", $arrWaypoints);
		}
		if(count($arrWaypoints) > MAX_GOOGLE_WAYPOINTS){

		}

		// BOF GET DATA
		#$getGoogleDataUrL_Directions = "http://maps.googleapis.com/maps/api/directions/json?origin=M%C3%BCnster,Germany&destination=Hamburg,Germany&waypoints=Essen,Germany&sensor=false&language=de";
		#$getGoogleDataUrL_Directions = 'http://maps.googleapis.com/maps/api/directions/json?origin=M%C3%BCnster,Germany&destination=Gronau,Germany&waypoints=Emden,Germany&sensor=false&language=de';
		#$getGoogleDataUrL_Directions = 'http://maps.googleapis.com/maps/api/directions/json?origin=M%C3%BCnster,Germany&destination=M%C3%BCnster,Germany&waypoints=optimize:true|Emden,Germany|Lingen,Germany|Gronau,Germany|Minden,Germany|Meppen,Germany|Cloppenburg,Germany|Greven,Germany|Telgte,Germany&sensor=false&language=de';
		#$getGoogleDataUrL_Directions = 'http://maps.googleapis.com/maps/api/directions/json?origin=48145,Germany&destination=48145,Germany&waypoints=optimize:true|Emden,Germany|Lingen,Germany|48599,Germany|Minden,Germany|Meppen,Germany|Cloppenburg,Germany|48268,Germany|Telgte,Germany&sensor=false&language=de';
		$getGoogleDataUrL_Directions = 'http://maps.googleapis.com/maps/api/directions/json?origin=' . $userLocationStart . '&destination=' . $userLocationEnd . '&waypoints=optimize:true|' . $destinations . '&sensor=false&language=de';

		$resultGoogleDataJSON_Directions = implode("", file($getGoogleDataUrL_Directions));
		$arrResultGoogleData_Directions = json_decode($resultGoogleDataJSON_Directions, true);
		// EOF GET DATA

		$strGoogleMapPoints = "";
		$strGoogleMapPolylines = "";
		$arrGoogleMapPolylines = array();
		$arrGoogleMapRouteDescription = array();

		$countPoints = 1;
		$countRoutes = 1;

		$thisGoogleMapOverviewPolylinePoints = $arrResultGoogleData_Directions["routes"][0]["overview_polyline"]["points"];

		if($arrResultGoogleData_Directions["status"] == "OK"){
			if(!empty($arrResultGoogleData_Directions["routes"][0]["legs"])){
				foreach($arrResultGoogleData_Directions["routes"][0]["legs"] as $thisRouteDatas){
					$arrGoogleMapRouteDescription[$countRoutes]["start"] = array(
						"name" => ($thisRouteDatas["start_address"])
					);
					$arrGoogleMapRouteDescription[$countRoutes]["end"] = array(
						"name" => ($thisRouteDatas["end_address"])
					);
					$arrGoogleMapRouteDescription[$countRoutes]["duration"] = array(
						"value" => ($thisRouteDatas["duration"]["text"])
					);
					$arrGoogleMapRouteDescription[$countRoutes]["distance"] = array(
						"value" => ($thisRouteDatas["distance"]["text"])
					);

					// BOF START ADRESS
					$strGoogleMapPoints .= 	'myPointDatas[' . $countPoints . '] = new Array();';
					$strGoogleMapPoints .= 	'myPointDatas[' . $countPoints . ']["latitude"] = "' . $thisRouteDatas["start_location"]["lat"] . '";';
					$strGoogleMapPoints .= 	'myPointDatas[' . $countPoints . ']["longitude"] = "' . $thisRouteDatas["start_location"]["lng"] . '";';
					$strGoogleMapPoints .= 	'myPointDatas[' . $countPoints . ']["title"] = "' . ($thisRouteDatas["start_address"]) . '";';
					$strGoogleMapPoints .= 	'myPointDatas[' . $countPoints . ']["header"] = "Von ' . ($thisRouteDatas["start_address"]) . ' nach ' . ($thisRouteDatas["end_address"]) . '";';
					$strGoogleMapPoints .= 	'myPointDatas[' . $countPoints . ']["duration"] = "' . $thisRouteDatas["duration"]["text"] . '";';
					$strGoogleMapPoints .= 	'myPointDatas[' . $countPoints . ']["distance"] = "' . $thisRouteDatas["distance"]["text"] . '";';
					$thisMarkerImage = "googleMapsIconsTarget.png";
					if(preg_match("/" . $userLocation . "/", $thisRouteDatas["start_address"])){
						$thisMarkerImage = "googleMapsIconsBase.png";
					}
					$strGoogleMapPoints .= 	'myPointDatas[' . $countPoints . ']["markerImage"] = "' . $thisMarkerImage . '";';
					$strGoogleMapPoints .= 	'myPointDatas[' . $countPoints . ']["markerType"] = "target";';
					$countPoints ++;
					// EOF START ADRESS

					// BOF END ADRESS
					$strGoogleMapPoints .= 	'myPointDatas[' . $countPoints . '] = new Array();';
					$strGoogleMapPoints .= 	'myPointDatas[' . $countPoints . ']["latitude"] = "' . $thisRouteDatas["end_location"]["lat"] . '";';
					$strGoogleMapPoints .= 	'myPointDatas[' . $countPoints . ']["longitude"] = "' . $thisRouteDatas["end_location"]["lng"] . '";';
					$strGoogleMapPoints .= 	'myPointDatas[' . $countPoints . ']["title"] = "' . ($thisRouteDatas["end_address"]) . '";';
					$strGoogleMapPoints .= 	'myPointDatas[' . $countPoints . ']["header"] = "' . ($thisRouteDatas["end_address"]) . ' von ' . ($thisRouteDatas["start_address"]) . '";';
					$strGoogleMapPoints .= 	'myPointDatas[' . $countPoints . ']["duration"] = "' . $thisRouteDatas["duration"]["text"] . '";';
					$strGoogleMapPoints .= 	'myPointDatas[' . $countPoints . ']["distance"] = "' . $thisRouteDatas["distance"]["text"] . '";';
					$thisMarkerImage = "googleMapsIconsTarget.png";
					if(preg_match("/" . $userLocation . "/", $thisRouteDatas["end_address"])){
						$thisMarkerImage = "googleMapsIconsBase.png";
					}
					$strGoogleMapPoints .= 	'myPointDatas[' . $countPoints . ']["markerImage"] = "' . $thisMarkerImage . '";';
					$strGoogleMapPoints .= 	'myPointDatas[' . $countPoints . ']["markerType"] = "target";';
					$countPoints ++;
					// EOF END ADRESS

					// BOF STEP POINTS
					if(DISPLAY_STEP_MARKER == 1){
						$thisMarkerImage = "googleRouteMarker.png";
					}
					else {
						$thisMarkerImage = "spacer.gif";
					}
					foreach($thisRouteDatas["steps"] as $thisStepData){
						// BOF CREATE POINTS
						$arrGoogleMapRouteDescription[$countRoutes]["points"][$countPoints] = array(
							"instruction" => (strip_tags($thisStepData["html_instructions"])),
							"duration" => (strip_tags($thisStepData["duration"]["text"])),
							"distance" => (strip_tags($thisStepData["distance"]["text"])),
						);

						$strGoogleMapPoints .= 	'myPointDatas[' . $countPoints . '] = new Array();';
						$strGoogleMapPoints .= 	'myPointDatas[' . $countPoints . ']["latitude"] = "' . $thisStepData["end_location"]["lat"] . '";';
						$strGoogleMapPoints .= 	'myPointDatas[' . $countPoints . ']["longitude"] = "' . $thisStepData["end_location"]["lng"] . '";';
						$strGoogleMapPoints .= 	'myPointDatas[' . $countPoints . ']["title"] = "' . (strip_tags($thisStepData["html_instructions"])) . '";';
						$strGoogleMapPoints .= 	'myPointDatas[' . $countPoints . ']["header"] = "' . (strip_tags($thisStepData["html_instructions"])) . '";';
						if($thisStepData["duration"]["text"] != ""){
							$strGoogleMapPoints .= 	'myPointDatas[' . $countPoints . ']["duration"] = "' . $thisStepData["duration"]["text"] . '";';
						}
						$strGoogleMapPoints .= 	'myPointDatas[' . $countPoints . ']["distance"] = "' . $thisStepData["distance"]["text"] . '";';
						$strGoogleMapPoints .= 	'myPointDatas[' . $countPoints . ']["markerImage"] = "' . $thisMarkerImage . '";';
						$strGoogleMapPoints .= 	'myPointDatas[' . $countPoints . ']["markerType"] = "step";';
						// EOF CREATE POINTS

						// BOF CREATE POLYLINES
						# $arrGoogleMapPolylines[$countPoints] = $thisStepData["polyline"]["points"];
						$arrGoogleMapPolylines[$countRoutes][] = $thisStepData["polyline"]["points"];
						// EOF CREATE POLYLINES

						$countPoints ++;
					}
					// EOF STEP POINTS
					$countRoutes++;
				}
			}
		}
		else if($arrResultGoogleData_Directions["status"] != "OK"){
			if($arrResultGoogleData_Directions["status"] == "MAX_WAYPOINTS_EXCEEDED"){
				$errorMessage .= 'Zu viele Streckenpunkte. Google-Limit &uuml;berschritten! Daher werden nur die Ortspunkte angezeigt.' .'<br />';
				displayMessages();
				$thisMaptype = "allpoints";
			}
			else if($arrResultGoogleData_Directions["status"] == "ZERO_RESULTS"){
				$errorMessage .= 'Orte konnten von Google nicht gefunden werden! Es wird auf die Daten von OpenGeoDB zurückgegriffen und nur die nur die Ortspunkte angezeigt.' .'<br />';
				displayMessages();
				$thisMaptype = "allpoints";
			}
		}
	}
	if($thisMaptype == "points"){
		#$getGoogleDataUrL_Points = 'https://maps.googleapis.com/maps/api/geocode/json?address=' . $thisSearchString . '&sensor=false&language=de&key=AIzaSyDxB0jouKhQua7mAkgdTVwE0HOQYMqtbf4';
		$getGoogleDataUrL_Points = 'https://maps.googleapis.com/maps/api/geocode/json?address=' . rawurlencode(utf8_decode($thisSearchString)) . '&sensor=false&language=de';
		#echo $getGoogleDataUrL_Points;

		$resultGoogleDataJSON_Points = implode("", file($getGoogleDataUrL_Points));
		$arrResultGoogleData_Points = json_decode($resultGoogleDataJSON_Points, true);

		$strGoogleMapPoints = "";
		$strGoogleMapPolylines = "";
		$arrGoogleMapPolylines = array();
		$arrGoogleMapRouteDescription = array();

		$countPoints = 1;
		$countRoutes = 1;

		$userLocation = "xxxxx";

		// BOF START ADRESS
		/*
		$strGoogleMapPoints .= 	'myPointDatas[' . $countPoints . '] = new Array();';
		$strGoogleMapPoints .= 	'myPointDatas[' . $countPoints . ']["latitude"] = "' . $thisRouteDatas["start_location"]["lat"] . '";';
		$strGoogleMapPoints .= 	'myPointDatas[' . $countPoints . ']["longitude"] = "' . $thisRouteDatas["start_location"]["lng"] . '";';
		$strGoogleMapPoints .= 	'myPointDatas[' . $countPoints . ']["title"] = "' . utf8_decode($thisRouteDatas["start_address"]) . '";';
		$strGoogleMapPoints .= 	'myPointDatas[' . $countPoints . ']["header"] = "Von ' . utf8_decode($thisRouteDatas["start_address"]) . ' nach ' . utf8_decode($thisRouteDatas["end_address"]) . '";';
		$strGoogleMapPoints .= 	'myPointDatas[' . $countPoints . ']["duration"] = "' . $thisRouteDatas["duration"]["text"] . '";';
		$strGoogleMapPoints .= 	'myPointDatas[' . $countPoints . ']["distance"] = "' . $thisRouteDatas["distance"]["text"] . '";';
		$thisMarkerImage = "googleMapsIconsTarget.png";
		if(preg_match("/" . $userLocation . "/", $thisRouteDatas["start_address"])){
			$thisMarkerImage = "googleMapsIconsBase.png";
		}
		$strGoogleMapPoints .= 	'myPointDatas[' . $countPoints . ']["markerImage"] = "' . $thisMarkerImage . '";';
		$strGoogleMapPoints .= 	'myPointDatas[' . $countPoints . ']["markerType"] = "target";';
		$countPoints ++;
		*/
		// EOF START ADRESS

		foreach($arrResultGoogleData_Points["results"] as $thisPointDatas){
			// BOF END ADRESS
			$strGoogleMapPoints .= 	'myPointDatas[' . $countPoints . '] = new Array();';
			$strGoogleMapPoints .= 	'myPointDatas[' . $countPoints . ']["latitude"] = "' . $thisPointDatas["geometry"]["location"]["lat"] . '";';
			$strGoogleMapPoints .= 	'myPointDatas[' . $countPoints . ']["longitude"] = "' . $thisPointDatas["geometry"]["location"]["lng"] . '";';
			$strGoogleMapPoints .= 	'myPointDatas[' . $countPoints . ']["title"] = "' . utf8_decode($thisPointDatas["formatted_address"]) . '";';
			$strGoogleMapPoints .= 	'myPointDatas[' . $countPoints . ']["header"] = "' . utf8_decode($thisPointDatas["formatted_address"]) . '";';
			$strGoogleMapPoints .= 	'myPointDatas[' . $countPoints . ']["duration"] = "";';
			$strGoogleMapPoints .= 	'myPointDatas[' . $countPoints . ']["distance"] = "";';
			$thisMarkerImage = "googleMapsIconsTarget.png";
			if(preg_match("/" . $userLocation . "/", $thisPointDatas["address_components"][6]["long_name"])){
				$thisMarkerImage = "googleMapsIconsBase.png";
			}
			$strGoogleMapPoints .= 	'myPointDatas[' . $countPoints . ']["markerImage"] = "' . $thisMarkerImage . '";';
			$strGoogleMapPoints .= 	'myPointDatas[' . $countPoints . ']["markerType"] = "target";';
			$countPoints ++;
			// EOF END ADRESS
		}
	}
	if($thisMaptype == "allpoints"){
		$arrTemp = explode("|", $thisSearchString);
		$userLocation = $arrTemp[1];
		$arrSplitData = explode(",", $arrTemp[1]);
		$userLocationPLZ = 	$arrSplitData[1];
		$userLocationStart = $arrTemp[1] . ",Germany";
		$userLocationEnd = $arrTemp[1] . ",Germany";
		$userTargets = $arrTemp[2];

		$arrAreas = explode("_", $userTargets);
		array_push($arrAreas, $userLocationPLZ);


		$destinations = "";
		$arrDestinations = array();
		$waypoints = "";
		$arrWaypoints = array();
		$arrZipcodes = array();


		if(!empty($arrAreas)){
			foreach($arrAreas as $thisArea){
				if($thisArea != $userLocation){
					$arrDestinations[] = $thisArea . " ,Germany";
				}
			}
			$destinations = implode("|", $arrDestinations);
		}

		##$getGoogleDataUrL_Points = 'https://maps.googleapis.com/maps/api/geocode/json?address=' . $thisSearchString . '&sensor=false&language=de&key=AIzaSyDxB0jouKhQua7mAkgdTVwE0HOQYMqtbf4';
		#$getGoogleDataUrL_Points = 'https://maps.googleapis.com/maps/api/geocode/json?address=' . rawurlencode(utf8_decode($destinations)) . '&sensor=false&language=de';
		#echo $getGoogleDataUrL_Points;

		#$resultGoogleDataJSON_Points = implode("", file($getGoogleDataUrL_Points));
		#$arrResultGoogleData_Points = json_decode($resultGoogleDataJSON_Points, true);

		$strGoogleMapPoints = "";
		$strGoogleMapPolylines = "";
		$arrGoogleMapPolylines = array();
		$arrGoogleMapRouteDescription = array();

		$countPoints = 1;
		$countRoutes = 1;

		if(!empty($arrAreas)) {
			$where .= " AND (";

			foreach($arrAreas as $thisZipcode) {
				$arrWhere[] = " `" . TABLE_GEODB_TEXTDATA . "`.`text_val` = '" . $thisZipcode . "' ";
				#$arrWhere2[] = " `" . TABLE_ZIPCODES_CITIES . "`.`postal_code` LIKE '" . $thisZipcode . "%' ";
			}
			$where .= implode (" OR ", $arrWhere);
			$where .= ") ";
		}

		$sql = "
				 SELECT
					`" . TABLE_GEODB_COORDINATES . "`.`lat` AS `latitude`,
					`" . TABLE_GEODB_COORDINATES . "`.`lon` AS `longitude`,
					`" . TABLE_GEODB_TEXTDATA . "`.`text_val`,
					`geodb_textdata2`.`text_val` AS `text_val2`,
					`" . TABLE_ZIPCODES_CITIES . "`.`place_name`,
					`" . TABLE_ZIPCODES_CITIES . "`.`postal_code`,
					`" . TABLE_ZIPCODES_CITIES . "`.`firstOrderSubdivision_stateName` AS `region_1`,
					`" . TABLE_ZIPCODES_CITIES . "`.`secondOrderSubdivision_stateName` AS `region_2`,
					`" . TABLE_ZIPCODES_CITIES . "`.`thirdOrderSubdivision_stateName` AS `region_3`

				FROM `" . TABLE_GEODB_TEXTDATA . "`

				LEFT JOIN `" . TABLE_GEODB_TEXTDATA . "`  AS `geodb_textdata2`
				ON(`" . TABLE_GEODB_TEXTDATA . "`.`loc_id` = `geodb_textdata2`.`loc_id`)

				LEFT JOIN `" . TABLE_GEODB_LOCATIONS . "`
				ON (`" . TABLE_GEODB_TEXTDATA . "`.`loc_id` = `" . TABLE_GEODB_LOCATIONS . "`.`loc_id`)


				LEFT JOIN `" . TABLE_GEODB_COORDINATES . "`
				ON(`" . TABLE_GEODB_TEXTDATA . "`.`loc_id` = `" . TABLE_GEODB_COORDINATES . "`.`loc_id`)

				LEFT JOIN `" . TABLE_ZIPCODES_CITIES . "`
				ON(`" . TABLE_GEODB_TEXTDATA . "`.`text_val` = `" . TABLE_ZIPCODES_CITIES . "`.`postal_code`)

				WHERE `" . TABLE_GEODB_TEXTDATA . "`.`text_type` = '500300000'
					AND `geodb_textdata2`.`text_type` = '500100000'
					" . $where . "


				GROUP BY CONCAT(`latitude`, '-', `longitude`)
			";

		$rs = $dbConnection->db_query($sql);

		$strGoogleMapPoints = "";

		$countPoints = 1;

		while($ds = mysqli_fetch_assoc($rs)) {
			$arrPoints[] = array("latitude" => $ds["latitude"], "longitude" => $ds["longitude"], "text_val" =>  $ds["text_val"], "placeName" =>  $ds["place_name"]);

			$strGoogleMapPoints .= 	'myPointDatas[' . $countPoints . '] = new Array();';
			$strGoogleMapPoints .= 	'myPointDatas[' . $countPoints . ']["latitude"] = "' . $ds["latitude"] . '";';
			$strGoogleMapPoints .= 	'myPointDatas[' . $countPoints . ']["longitude"] = "' . $ds["longitude"] . '";';
			$strGoogleMapPoints .= 	'myPointDatas[' . $countPoints . ']["title"] = "' . utf8_decode($ds["text_val"]) . ' ' . utf8_decode($ds["place_name"]);
			if($ds["text_val2"] != $ds["place_name"]) { $strGoogleMapPoints .= ' / ' . utf8_decode($ds["text_val2"]); }
			$strGoogleMapPoints .= 	'";';
			$strGoogleMapPoints .= 	'myPointDatas[' . $countPoints . ']["header"] = "' . (utf8_decode($ds["text_val"] . ' ' . $ds["place_name"]));
			if($ds["text_val2"] != $ds["place_name"]) { $strGoogleMapPoints .= ' / ' . utf8_decode($ds["text_val2"]); }
			$strGoogleMapPoints .= 	'";';
			$strGoogleMapPoints .= 	'myPointDatas[' . $countPoints . ']["duration"] = "";';
			$strGoogleMapPoints .= 	'myPointDatas[' . $countPoints . ']["distance"] = "";';
			$strGoogleMapPoints .= 	'myPointDatas[' . $countPoints . ']["text"] = "";';
			$strGoogleMapPoints .= 	'myPointDatas[' . $countPoints . ']["city"] = "' . utf8_decode($ds["text_val"]) . '";';
			$strGoogleMapPoints .= 	'myPointDatas[' . $countPoints . ']["region_1"] = "' . utf8_decode($ds["region_1"]) . '";';
			$strGoogleMapPoints .= 	'myPointDatas[' . $countPoints . ']["region_2"] = "' . utf8_decode($ds["region_2"]) . '";';
			$strGoogleMapPoints .= 	'myPointDatas[' . $countPoints . ']["region_3"] = "' . utf8_decode($ds["region_3"]) . '";';
			$strGoogleMapPoints .= 	'myPointDatas[' . $countPoints . ']["imagePath"] = "";';
			$thisMarkerImage = "googleMapsIconsTarget.png";
			if(preg_match("/" . $userLocationPLZ . "/", $ds["text_val"])){
				$thisMarkerImage = "googleMapsIconsBase.png";
			}
			$strGoogleMapPoints .= 	'myPointDatas[' . $countPoints . ']["markerImage"] = "' . $thisMarkerImage . '";';
			$countPoints ++;
		}
	}
?>

<?php
	require_once('inc/headerHTML.inc.php');
	$thisTitle = '';
	$headerHTML = preg_replace("/{###TITLE###}/", strip_tags($thisTitle), $headerHTML);
	$headerHTML = preg_replace('/<div id="headerArea">(.*)<noscript>/ism', '<noscript>', $headerHTML);
	$headerHTML = preg_replace('/<body/ism', '<body id="bodyFrame" onload="initialize();"', $headerHTML);

	echo $headerHTML;
?>
	<div id="contentAreaElements">
		<div id="myGoogleMapsArea">
			<div id="myGoogleMapCanvas">
				<span class="loadingArea"><img class="loader" src="layout/ajax-loader.gif" /> Bitte haben Sie ein wenig Geduld. Die Daten werden geladen...</span>
			</div>
			<?php if($thisMaptype == "routes"){	?>
			<div id="myGoogleMapRouteArea">
				<p style="text-align:right;"><span class="buttonRefreshMap">Karte neu laden</span></p>
				<h2>Routenbeschreibung</h2>
				<div class="colorsLegend">
					<div class="legendItem">
						<img src="layout/icons/googleMapsIconsBase.png" width="18" height="18" alt="Heimatort"/> Heimatort
					</div>
					<div class="legendItem">
						<img src="layout/icons/googleMapsIconsTarget.png" width="18" height="18" alt="Zielort"/> Zielort
					</div>
					<div class="clear"></div>
					<div class="legendItem">
						<span style="background-color:#FF0000;padding-right:16px;font-size:10px;">&nbsp;</span> Berechnete Strecke
					</div>
					<div class="clear"></div>
					<div class="legendItem">
						<span style="background-color:#539BDC;padding-right:16px;font-size:10px;">&nbsp;</span> Individuelle Strecke
					</div>
					<div class="legendItem">
						<img src="layout/icons/googleMapsDrawablePoint.png" width="11" height="11" alt="Beweglicher Punkt"/> Beweglicher Streckenpunkt
					</div>
					<div class="clear"></div>
				</div>
				<hr />
				<div id="myGoogleMapTotalDistance">Gesamtstrecke: <span></span></div>
				<div id="myGoogleMapRouteDescription">
				<?php
					/*
					if(!empty($arrGoogleMapRouteDescription)){
						echo '<ol style="margin-left:0;padding-left:20px;">';
						foreach($arrGoogleMapRouteDescription as $thisRouteKey => $thisRouteValue){
							echo '<li style="margin-left: 0;padding-left:0">';
							echo '<div class="googleMapRouteTitle">';
							echo '<h3>Von ' . $thisRouteValue["start"]["name"] . ' nach ' . $thisRouteValue["end"]["name"] . '</h3>';
							echo '<p style="margin:0; padding:0;font-size:11px;font-style:italic;">Gesamtdauer: ' . $thisRouteValue["duration"]["value"] . ' &bull; Strecke: ' . $thisRouteValue["distance"]["value"] . '</p>';
							echo '</div>';
							echo '<div class="googleMapRouteContent">';
							echo '<ol style="margin-left:0;padding-left:20px">';
							foreach($thisRouteValue["points"] as $thisPointKey => $thisPointValue){
								$thisRowClass = 'row1';
								if($thisPointKey%2 == 0){
									$thisRowClass = 'row0';
								}
								echo '<li class="' . $thisRowClass . '" style="margin-left: 0;padding-left:0;border-bottom:1px dotted #333;">';
								echo nl2br($thisPointValue["instruction"]) . '<br />';
								echo '<span style="font-size:11px;font-style:italic;">';
								echo 'Entfernung: ' . $thisPointValue["distance"] . ' &middot; ';
								echo 'Dauer: ' . $thisPointValue["duration"];
								echo '</span>';
								echo '</li>';
							}
							echo '</ol>';
							echo '</div>';
							echo '</li>';
						}
						echo '</ol>';
					}
					*/
					?>
					</div>
					<?php
				?>
			</div>
			<div class="clear"></div>
			<?php } ?>
		</div>
	</div>
	<script language="javascript" type="text/javascript">
		<?php if($thisMaptype == "routes"){	?>
		$('#myGoogleMapCanvas').css({
			'width': '70%',
			'height': ($(window).height() - 30) + 'px',
			'float': 'left'
		});
		$('#myGoogleMapRouteArea').css({
			'overflow': 'auto',
			'width': '28%',
			'height': ($(window).height() - 30) + 'px',
			'float': 'right'
		});
		<?php } ?>

		<?php if($thisMaptype == "points"){	?>
		$('#myGoogleMapCanvas').css({
			'width': '100%',
			'height': ($(window).height() - 30) + 'px',
			'float': 'left'
		});
		<?php } ?>

		<?php if($thisMaptype == "allpoints"){	?>
		$('#myGoogleMapCanvas').css({
			'width': '100%',
			'height': ($(window).height() - 30) + 'px',
			'float': 'left'
		});
		<?php } ?>
	</script>

	<!-- GOOGLE MAPS START -->
	<script type="text/javascript" src="<?php echo PATH_GOOGLE_MAPS_API; ?>"></script>
	<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?sensor=false&libraries=geometry"></script>
	<script type="text/javascript" language="javascript">
		// try {
		var DISPLAY_STEP_MARKER = <?php echo DISPLAY_STEP_MARKER; ?>;
		function setGmapMarkers(map, i) {
			var infWindowKey = i;
			var thisMarkerImage;
			if(i == 0) {
				thisMarkerImage = "bctr_googleMarkerFlag.png";
				thisMarkerShadowImage = "bctr_googleMarkerFlagShadow.png";
				thisMarkerSize = new Array(81, 84);
				thisMarkerShadowSize = new Array(81, 84);
				thisMarkerZindex = (myPointDatas.length + 10);
				thisMarkerShadowZindex = (myPointDatas.length + 9);
			}
			else {
				if(myPointDatas[i]["markerType"] == "step"){
					thisMarkerImage = myPointDatas[i]["markerImage"];
					thisMarkerShadowImage = "googleMapsIconShadow.png";
					thisMarkerSize = new Array(12, 12);
					thisMarkerShadowSize = new Array(12, 12);
					thisMarkerZindex = (myPointDatas.length - i);
					thisMarkerShadowZindex = (myPointDatas.length - i);
				}
				else {
					thisMarkerImage = myPointDatas[i]["markerImage"];
					thisMarkerShadowImage = "googleMapsIconsTargetShadow.png";
					thisMarkerSize = new Array(39, 43);
					thisMarkerShadowSize = new Array(39, 43);
					thisMarkerZindex = (myPointDatas.length);
					thisMarkerShadowZindex = (myPointDatas.length - 1);
				}
			}

			var image = new google.maps.MarkerImage(
						"layout/icons/" + thisMarkerImage,
						new google.maps.Size(thisMarkerSize[0], thisMarkerSize[1]),
						new google.maps.Point(0,0),
						new google.maps.Point(0, thisMarkerSize[1])
			);

			var shadow = new google.maps.MarkerImage(
						"layout/icons/" + thisMarkerShadowImage,
						new google.maps.Size(thisMarkerShadowSize[0], thisMarkerShadowSize[1]),
						new google.maps.Point(0, 0),
						new google.maps.Point(0, thisMarkerShadowSize[1])
			);

			var shape = {
				// coord: [1, (thisMarkerSize[1] + 1 - 20), 1, thisMarkerSize[1], thisMarkerSize[0], thisMarkerSize[1], thisMarkerSize[0] , (thisMarkerSize[1] + 1 - 20)],
				coord: [1, 1, 1, 28, 20, 28, 20 , 1],
				type: "poly"
			};

			var myLatLng = new google.maps.LatLng(myPointDatas[i]["latitude"], myPointDatas[i]["longitude"]);

			marker[infWindowKey] = new google.maps.Marker({
				position: myLatLng,
				map: map,
				shadow: shadow,
				icon: image,
				shape: shape,
				title: myPointDatas[i]["title"],
				zIndex: thisMarkerZindex
			});

			shadow[infWindowKey] = new google.maps.Marker({
				position: myLatLng,
				map: map,
				icon: shadow,
				shape: shape,
				title: myPointDatas[i]["title"],
				zIndex: thisMarkerShadowZindex
			});

			// DEFINE INFO WINDOW
			var html		= new Array();
			var imageExists	= false;
			html[infWindowKey] = "";
			html[infWindowKey] += "<div class=\"myGmapInfo\">";
			// html[infWindowKey] += "  <p class='headline'>" + myPointDatas[i]["title"] + "</p>";
			html[infWindowKey] += "  <p class='xxxheadline'>" + myPointDatas[i]["header"] + "</p>";
			if(myPointDatas[i]["imagePath"] != "") {
			  imageExists = true;
			  // html[infWindowKey] += '  <img src="' + myPointDatas[i]["imagePath"] + '" ' + myPointDatas[i]["imageDimension"] + ' alt="' + myPointDatas[i]["title"] + '"/>';
			}

			html[infWindowKey] += '<table border="0" width="" cellpadding="0" cellspacing="0">';

			if(myPointDatas[i]["text"] != "") {
			  // html[infWindowKey] += "  <tr><td><b>Text:</b></td><td>" + myPointDatas[i]["text"] + "</td></tr>";
			}

			if(myPointDatas[i]["duration"] != "") {
			  html[infWindowKey] += "  <tr><td><b>Dauer:</b></td><td>" + myPointDatas[i]["duration"] + "</td></tr>";
			}

			if(myPointDatas[i]["distance"] != "") {
			  html[infWindowKey] += "  <tr><td><b>Entfernung:</b></td><td>" + myPointDatas[i]["distance"] + "</td></tr>";
			}

			if(myPointDatas[i]["city"] != "") {
			  // html[infWindowKey] += "  <tr><td><b>PLZ:</b></td><td>" + myPointDatas[i]["city"] + "</td></tr>";
			}

			if(myPointDatas[i]["region_1"] != "") {
			  // html[infWindowKey] += "  <tr><td><b>Bundesland:</b></td><td>" + myPointDatas[i]["region_1"] + "</td></tr>";
			}

			if(myPointDatas[i]["region_2"] != "") {
			  // html[infWindowKey] += "  <tr><td><b>Region:</b></td><td>" + myPointDatas[i]["region_2"] + "</td></tr>";
			}

			if(myPointDatas[i]["region_3"] != "") {
			  // html[infWindowKey] += "  <tr><td><b>Kreis:</b></td><td>" + myPointDatas[i]["region_3"] + "</td></tr>";
			}

			// html[infWindowKey] += "  <tr><td><b>Longitude:</b></td><td>" + myPointDatas[i]["longitude"] + "</td></tr>";
			// html[infWindowKey] += "  <tr><td><b>Latitude: </b></td><td>" + myPointDatas[i]["latitude"] + "</td></tr>";

			html[infWindowKey] += '</table>';

			html[infWindowKey] += "<div class='clear'></div>";
			html[infWindowKey] += "</div>";

			infowindow[infWindowKey] = new Array();
			infowindow[infWindowKey] = new google.maps.InfoWindow({
				content: html[infWindowKey]
			});
			// END DEFINE INFO WINDOW

			// SHOW INFO WINDOW
			google.maps.event.addListener(marker[infWindowKey], "click", function() {
				if(activeMarker != '') {
					infowindow[activeMarker].close();
				}
				try{
					infowindow[infWindowKey].open(map, marker[infWindowKey]);
				}
				catch(e){
					alert("error: " + e + " | i: " + i);
				}
				activeMarker = infWindowKey;
			});
				// createMarkerLinkInGmapSidebar(i, myPointDatas[i]["title"], marker[infWindowKey], imageExists);
			// END SHOW INFO WINDOW
		}

		/* BOF DEFINE DEFAULT VALUES */
		var myPointDatas	= new Array();

		var elementId = "myGoogleMapCanvas";
		<?php if($arrResultGoogleData_Directions["status"] == "OK" || $thisMaptype == "allpoints"){ ?>
		var default_arrayKey	= 1;
		<?php } else { ?>
		var default_arrayKey	= 0;
		<?php } ?>
		var default_lat			= "52.103138";
		var default_lon			= "7.622817";
		<?php if($thisMaptype == "allpoints"){ ?>
		var default_zoom		= 8;
		<?php } else { ?>
		var default_zoom		= 12;
		<?php } ?>
		var sidebarHtml		= "";              			/* HTML-Code für die gesamte Sidebar */
		var sidebarMarkers	= [];              			/* Array für die Marker */
		var sidebarCount	= 0;               			/* Zähler für die Marker (= Zeile in der Sidebar) */
		var marker			= new Array();
		var activeMarker	= '';
		var infowindow		= new Array();
		/* EOF DEFINE DEFAULT VALUES */

		/* BOF DEFINE POINT DATAS */
		myPointDatas[0] = new Array();
		myPointDatas[0]["latitude"] = default_lat;
		myPointDatas[0]["longitude"] = default_lon;
		myPointDatas[0]["title"] = "BURHAN CTR";
		myPointDatas[0]["header"] = "BURHAN CTR";
		myPointDatas[0]["text"] = "";
		myPointDatas[0]["city"] = "";
		myPointDatas[0]["region_1"] = "";
		myPointDatas[0]["region_2"] = "";
		myPointDatas[0]["region_3"] = "";
		myPointDatas[0]["imagePath"] = "";
		/* EOF DEFINE POINT DATAS */

		<?php
		// BOF WRITE GOOGLE POINTS
			echo $strGoogleMapPoints;
		// EOF WRITE GOOGLE POINTS
		?>

		/* BOF CREATE GOOGLE MAP */
			function initialize() {
				if (!document.getElementById(elementId)) {
					alert("Fehler: das Element mit der id "+ elementId+ " konnte nicht auf dieser Webseite gefunden werden!");
					return false;
				}
				else {

				}
			}

			var latlng = new google.maps.LatLng(myPointDatas[default_arrayKey]["latitude"], myPointDatas[default_arrayKey]["longitude"]);

			var myOptions = {
				zoom: default_zoom,
				center: latlng,
				panControl: true,
				zoomControl: true,
				navigationControl: true,
				mapTypeControl: true,
				scaleControl: true,
				overviewMapControl: true,
				streetViewControl: true,
				mapTypeId: google.maps.MapTypeId.TERRAIN
				/* ROADMAP zeigt die normalen Standard-2D-Kacheln von Google Maps an. */
				/* SATELLITE zeigt Fotokacheln an. */
				/* HYBRID zeigt eine Mischung aus Fotokacheln und eine Kachelebene für markante Merkmale an (Straßen, Ortsnamen). */
				/* TERRAIN zeigt Reliefkacheln für Erhebungen und GewÃ¤sser (Berge, Flüsse usw.) an. */
			};
			var map = new google.maps.Map(document.getElementById(elementId), myOptions);

			// BOF COUNTRY BORDERS
			<?php if(SHOW_COUNTRY_AREAS){ ?>
			var arrMyCountryAreaPoints = new Array();
			<?php
			if(!empty($arrGoogleMapCountryAreas)){
				$countItem = 0;
				foreach($arrGoogleMapCountryAreas as $thisKey => $thisValue) {
			?>
			arrMyCountryAreaPoints[<?php echo $countItem; ?>] = [<?php echo implode(", ", $thisValue); ?>];
			<?php
					$countItem++;
				}
			}
			?>
			function showCountryBorders(map){
				if(arrMyCountryAreaPoints.length > 0){
					var arrMyCountryAreaPolygon = new Array();
					for(i = 0 ; i < arrMyCountryAreaPoints.length ; i++){
						arrMyCountryAreaPolygon[i] = new google.maps.Polygon({
								paths: arrMyCountryAreaPoints[i],
								strokeColor: "#00FF3A",
								strokeOpacity: 0.8,
								strokeWeight: 2,
								fillColor: "transparent",
								fillOpacity: 0.0
								});
						arrMyCountryAreaPolygon[i].setMap(map);
					}
				}
			}
			<?php } ?>
			// EOF COUNTRY BORDERS

			// BOF ZIPCODE AREAS
			<?php if(SHOW_ZIPCODE_AREAS){ ?>
			var arrMyZipcodeAreaPoints = new Array();
			<?php
			if(!empty($arrGoogleMapZipCodeAreas)){
				$countItem = 0;
				foreach($arrGoogleMapZipCodeAreas as $thisKey => $thisValue) {
			?>
			arrMyZipcodeAreaPoints[<?php echo $countItem; ?>] = [<?php echo $thisValue; ?>];
			<?php
					$countItem++;
				}
			}
			?>
			function showZipCodeAreas(map){
				if(arrMyZipcodeAreaPoints.length > 0){
					var arrMyZipcodeAreaPolygon = new Array();
					for(i = 0 ; i < arrMyZipcodeAreaPoints.length ; i++){
						arrMyZipcodeAreaPolygon[i] = new google.maps.Polygon({
								paths: arrMyZipcodeAreaPoints[i],
								strokeColor: "#0000FF",
								strokeOpacity: 0.3,
								strokeWeight: 1,
								fillColor: "#0000FF",
								fillOpacity: 0.1,
								zIndex:1
								});
						arrMyZipcodeAreaPolygon[i].setMap(map);
					}
				}
			}
			<?php } ?>
			// EOF ZIPCODE AREAS

			// BOF POLYLINE
			var myPolylineDatas = new Array();
			var myPolygonDatas = new Array();
			var decodedPolylinePath = new Array();
			<?php
				if(!empty($arrGoogleMapPolylines)){
					$colorCount = 0;
					foreach($arrGoogleMapPolylines as $thisGoogleMapRouteKey => $thisGoogleMapRouteValue){
						foreach($thisGoogleMapRouteValue as $thisGoogleMapPolylineKey => $thisGoogleMapPolylineValue){
			?>
			decodedPolylinePath[<?php echo $thisGoogleMapPolylineKey; ?>] = google.maps.geometry.encoding.decodePath("<?php echo addslashes($thisGoogleMapPolylineValue); ?>");
			myPolylineDatas[<?php echo $thisGoogleMapPolylineKey; ?>] = new google.maps.Polyline({
						map: map,
						path: decodedPolylinePath[<?php echo $thisGoogleMapPolylineKey; ?>],
						strokeColor: "#FF0000",
						strokeOpacity: 0.8,
						strokeWeight: 2,
						geodesic: true,
						zIndex:2
						});
			myPolylineDatas[<?php echo $thisGoogleMapPolylineKey; ?>].setMap(map);
			<?php
							$colorCount++;
							if($colorCount > count($arrRouteColors)){
								$colorCount = 0;
							}
						}
					}
				}
			?>
			// EOF POLYLINE

			<?php if($thisMaptype == "routes"){ ?>
			var rendererOptions = {
				map: map,
				draggable: true,
				suppressMarkers: true,
				polylineOptions:{strokeColor:'#539BDC', zIndex:1, strokeOpacity: 1, strokeWeight: 6}
			};
			var directionsDisplay = new google.maps.DirectionsRenderer(rendererOptions);;
			var directionsService = new google.maps.DirectionsService();
			directionsDisplay.setMap(map);
			directionsDisplay.setPanel(document.getElementById('myGoogleMapRouteDescription'));

			google.maps.event.addListener(directionsDisplay, 'directions_changed', function() {
				computeTotalDistance(directionsDisplay.getDirections());
			});
			<?php } ?>

			// BOF ROUTE PLANER
			<?php if($arrResultGoogleData_Directions["status"] == "OK"){ ?>
			calcRoute();
			<?php } ?>
			function calcRoute() {
				var request = {
					origin: '<?php echo $userLocationStart; ?>',
					destination: '<?php echo $userLocationStart; ?>',
					waypoints: [<?php echo $waypoints; ?>],
					// waypoints:[{location: 'Bourke, NSW'}, {location: 'Broken Hill, NSW'}],
					// waypoints: '<?php echo $destinations; ?>',
					optimizeWaypoints: true,
					travelMode: google.maps.TravelMode.DRIVING
					/*
					google.maps.TravelMode.DRIVING (Standardeinstellung) gibt die Standardroute unter Verwendung des Straßennetzes an.
					google.maps.TravelMode.BICYCLING dient zum Anfordern von Fahrradrouten unter Verwendung von Fahrradwegen und bevorzugten Straßen.
					google.maps.TravelMode.TRANSIT dient zum Anfordern von Routen unter Nutzung öffentlicher Verkehrsmittel.
					google.maps.TravelMode.WALKING dient zum Anfordern von Routen für Fußgänger, die über Fußgängerwege und Bürgersteige führen.
					*/

				};
				directionsService.route(request, function(response, status) {
					if (status == google.maps.DirectionsStatus.OK) {
						directionsDisplay.setDirections(response);
					}
				});
			}

			function computeTotalDistance(result) {
				var total = 0;
				var myroute = result.routes[0];
				for (var i = 0; i < myroute.legs.length; i++) {
					total += myroute.legs[i].distance.value;
				}
				total = total / 1000.0;
				$('#myGoogleMapTotalDistance span').html(total + ' km');
			}
			// EOF ROUTE PLANER

			// BOF POLYGON
			<?php
				/*
				if(!empty($arrGoogleMapPolylines)){
					foreach($arrGoogleMapPolylines as $thisGoogleMapRouteKey => $thisGoogleMapRouteValue){
						foreach($thisGoogleMapRouteValue as $thisGoogleMapPolylineKey => $thisGoogleMapPolylineValue){
			?>
			myPolygonDatas[<?php echo $thisGoogleMapPolylineKey; ?>] = new google.maps.Polygon({
						paths: decodedPolylinePath[<?php echo $thisGoogleMapPolylineKey; ?>],
						strokeColor: "#0000FF",
						strokeOpacity: 0.8,
						strokeWeight: 2,
						fillColor: "#FFFF00",
						fillOpacity: 0.1
						});
			// myPolygonDatas[<?php echo $thisGoogleMapPolylineKey; ?>].setMap(map);
			<?php
						}
					}
				}
				*/
			?>
			var decodedPolygonPath = '';
			<?php if($thisGoogleMapOverviewPolylinePoints != ""){ ?>
			decodedPolygonPath = google.maps.geometry.encoding.decodePath("<?php echo addslashes($thisGoogleMapOverviewPolylinePoints); ?>");

			myPolygonDatas = new google.maps.Polygon({
						paths: decodedPolygonPath,
						strokeColor: "#0000FF",
						strokeOpacity: 0.8,
						strokeWeight: 0,
						fillColor: "#FFFF00",
						fillOpacity: 0.1
						});
			myPolygonDatas.setMap(map);
			<?php } ?>
			// BOF POLYGON


			try{
				for (i = 0 ; i < myPointDatas.length ; i++) {
					if(myPointDatas[i]){
						setGmapMarkers(map, i);
					}
				}
				<?php if(SHOW_COUNTRY_AREAS){ ?>
				showCountryBorders(map);
				<?php } ?>

				<?php if(SHOW_ZIPCODE_AREAS){ ?>
				showZipCodeAreas(map);
				<?php } ?>
			}
			catch(e){
				alert("error: " + e + " | i: " + i);
			}
		/* EOF CREATE GOOGLE MAP */

	// } catch(err) { alert(err); } finally {}
	</script>
	<!-- GOOGLE MAPS END -->

<script language="javascript" type="text/javascript">
	$(document).ready(function() {
		// $('body').attr('onload', 'initialize();');
		$('.googleMapRouteContent').toggle();
		$('.googleMapRouteTitle').css('cursor', 'pointer');
		$('.googleMapRouteTitle').live('click', function(){
			$('.googleMapRouteContent').hide();
			$(this).next('.googleMapRouteContent').toggle();
		});
		$('.googleMapRouteContent').live('click', function(){
			$('.googleMapRouteContent').hide();
		});
		$('.buttonRefreshMap').click(function(){
			calcRoute();
		});
	});
</script>
<?php $noMandatorySwitch = true; ?>
<?php $noUserSwitch = true; ?>
<?php require_once('inc/footerHTML.inc.php'); ?>