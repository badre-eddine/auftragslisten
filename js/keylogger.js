function submitKeyLogEvent(eventType, eventData){
	var sendUrl = 'inc/keylogger.inc.php';
	sendUrl += '?eventType=' + eventType + '&eventData=' + eventData;
	$.get(sendUrl, function(result) {
		// alert(result);
	});
}

// ----------------------------------------------------------------
function getKeyLogEvent(){
	// BOF KEY EVENT
		/*
		$('html').keydown = function(key){
			var eventType = 'keydown';
			var eventData = key.keyCode;
			submitKeyLogEvent(eventType, eventData);
		}
		*/

		$('html').keyup(function(key){
			var eventType = 'keyup';
			var eventData = key.keyCode;
			submitKeyLogEvent(eventType, eventData);
		});
		/*
		$('html').keypress(function(key){
			var eventType = 'keypress';
			var eventData = key.keyCode;
			submitKeyLogEvent(eventType, eventData);
		});
		*/
	// EOF KEY EVENT

	// BOF MOUSE EVENT
		/*
		$('html').mousemove(function(event){
			var eventType = 'mousemove';
			var eventData = 'X:' + event.pageX + ' Y:' + event.pageY;
			submitKeyLogEvent(eventType, eventData);
		});
		*/

		$('html').click(function(event){
			var eventType = 'mouseclick';
			var eventData = 'X:' + event.pageX + ' Y:' + event.pageY;
			submitKeyLogEvent(eventType, eventData);
		});
		$('html').contextmenu(function(event){
			var eventType = 'contextmenu';
			// var eventData = 'clickRight';
			var eventData = 'X:' + event.pageX + ' Y:' + event.pageY;
			submitKeyLogEvent(eventType, eventData);
		});
		$('html').dblclick(function(event){
			var eventType = 'dblclick';
			var eventData = 'X:' + event.pageX + ' Y:' + event.pageY;
			submitKeyLogEvent(eventType, eventData);
		});
	// EOF MOUSE EVENT
}

// ----------------------------------------------------------------

function showCoordinatesGrid(){
	var visibility = $('#gridArea').css('display');
	if(visibility == 'block'){
		$('#gridArea').remove();
	}
	else {
		var myGutterUnit = 20;

		var myWindowInnerWidth = $(window).innerWidth();
		var myWindowWidth = $(window).width();
		var myDocumentWidth = $(document).width();

		var myWindowInnerHeight = $(window).innerHeight();
		var myWindowHeight = $(window).height();
		var myDocumentHeight = $(document).height();

		var useWindowWidth = myWindowWidth;
		var useWindowHeight = myDocumentHeight;
		$('body').append('<div id="gridArea" style="margin:0;padding:0;"></div>');
		$('#gridArea').hide();
		for(iW = 0 ; iW < useWindowWidth ; iW = iW + (myGutterUnit * 2)){
			for(iH = 0 ; iH < useWindowHeight ; iH = iH + (myGutterUnit * 2)){
				$('#gridArea').append('<div title="X:' + iW + ' - Y:' + iH + ' | E:' + myGutterUnit + 'px" style="position:absolute;z-index:999;top:' + iH + 'px;left:' + iW + 'px;height:' + myGutterUnit + 'px;width:' + myGutterUnit + 'px;background-color:#FF0000;color:#FF00FF;opacity:0.2;">.</div>');
			}
		}
		$('#gridArea').show();
	}
}

// $('#buttonToggleDebugContent').append('<img src="layout/icons/iconGrid.png" class="buttonshowGrid" height="16" width="16" alt="Gitternetz" title="Gitternetz anzeigen" style="cursor:pointer;" />');
// $('.buttonshowGrid').live('click', function(){
	// showCoordinatesGrid();
// });