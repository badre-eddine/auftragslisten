var colorWarningBackground = '#FFBFC2';
var colorWarningBorder = '#FF0000';
var animationTime = 'fast';

function submitForm(formName, formElement) {
	try {
		var form = window.document.forms[formName];
		var formElement = form[formElement];
		var url = "";

		form.submit();
	}
	catch(err) { handleJsError(err); return false; }
	finally {}
}

// ------------------------------------------------------------

function showWarning(message) {
	try {
		var thisReturn = false;
		thisReturn = window.confirm(message);
		return thisReturn;
	}
	catch(err) { handleJsError(err); return false; }
	finally {}
}

// ------------------------------------------------------------

function checkFormFields(formName) {
	try {
		alert(formName);
	}
	catch(err) { handleJsError(err); return false; }
}

// ------------------------------------------------------------

function setFocus(formName, formElement) {
	try {
		var form = window.document.forms[formName];
		var formElement = form[formElement];
		formElement.focus();
	}
	catch(err) { handleJsError(err); return false; }
	finally {}
}

// ------------------------------------------------------------

function findExistingCustomer() {
	try {

	}
	catch(err) { handleJsError(err); return false; }
	finally {}
}
// ------------------------------------------------------------

function colorRowMouseOver(element) {
	try {
		/*
		$(element).mouseenter(function(){
			$(this).css('background-color', '#FFD86F');
			$(this).mouseleave(function(){
				$(this).css('background-color', '');
			});
			$(element).click(function(){
				$(element).css('background-color', '');
				$(this).css('background-color', '#9FF8FF');
				$(this).click(function(){
					// $(this).css('background-color', '');
				});
			});
		});
		*/

		$(element).bind({
			mouseenter: function() {
				$(this).css('background-color', '#FFD86F');
				// $(this).find('td').css('background-color', '#FFD86F');
				$(this).mouseleave(function(){
					$(this).css('background-color', '');
					// $(this).find('td').css('background-color', '');
				});
			},
			dblclick: function() {
				$(element).css('background-color', '');
				$(this).css('background-color', '#9FF8FF');
				// $(this).find('td').css('background-color', '#9FF8FF');
				$(this).unbind('mouseleave');
				$(this).click(function(){
					$(this).css('background-color', '');
					// $(this).find('td').css('background-color', '');
				});
			},
		});
	}
	catch(err) { handleJsError(err); return false; }
	finally {}
}

// ------------------------------------------------------------

function loadSuggestions(searchMode, jsonFieldDatas, cleanMode) {
	triggerElement = jsonFieldDatas[0].triggerElement;
	var animateTime = 0;
	try {
		$('#searchResultArea').remove();
		var searchString = $(triggerElement).attr('value');

		if(triggerElement != '' && searchString != '') {
			$(triggerElement).parent().css('position', 'relative');
			$(triggerElement).parent().append('<div id="searchResultArea"></div>');
			$('#searchResultArea').slideDown(animateTime);
			var loadURL = 'inc/loadSuggestions.inc.php';

			loadURL += '?type=' + searchMode;

			var content = '';
			var contentHeader = 'Gefundene Vorschl&auml;ge';
			var contentText = '';
			var contentFooter = '';
			content = '<div class="noticeBoxHeader">' + contentHeader + '</div>';
			content += '<div class="noticeBoxContents">' + contentText + '</div>';
			content += '<div class="noticeBoxFooter">' + contentFooter + '</div>';
			content += '<div class="noticeBoxClose">' + '<img src="layout/icons/iconClose.png" class="iconClose" width="14" height="14" alt="" title="Liste schlie&szlig;en" />' + '</div>';
			content = '<div class="loadNoticeContent">' + content + '</div>';

			$('#searchResultArea').html(content);
			$('.noticeBoxContents').empty().html('<span class="loadingArea"><img class="loader" src="layout/ajax-loader.gif" /> Bitte haben Sie ein wenig Geduld. Die Daten werden geladen...</span>');

			loadURL += '&searchString=' + searchString;
			// loadURL = escape(loadURL);
			// loadURL = encodeURIComponent(loadURL);
			// loadURL = encodeURI(loadURL);
			// loadURL = encodeURI(loadURL);
			loadURL = loadURL.replace(/ /, 'xxxxxx');

			$('.noticeBoxContents').load(loadURL, function(response, status, xhr){
				if (status == 'error') {
					// var msg = 'Es ist ein Fehler aufgetreten. Fehler-Code: ';
					var msg = 'Die Daten konnten nicht geladen werden. Fehler-Code: ';
					$('.noticeBoxContents').html(msg + xhr.status + ' ' + xhr.statusText);
					$('#searchResultArea').remove();
				}
				else {
					if($('.noticeBoxContents').html() != '') {
						$('.searchResultItem').click(function(){
							if(searchMode == 'searchCustomerNumberDatas') {
								var searchValueCustomerNumber = $(this).find('.customerNumber').text();
								var searchValueCustomerName = $(this).find('.customerName').text();
								var searchValueCustomerID = $(this).find('.customerNumber').text();
								var searchValueCustomerZipCode = $(this).find('.customerZipCode').text();
								var searchValueCustomerCity = $(this).find('.customerCity').text();
								var searchValueCustomerCountry = $(this).find('.customerCountry').text();

								var searchValueSalesmanName = $(this).find('.salesmanName').text();
								var searchValueSalesmanID = $(this).find('.salesmanID').text();
								var searchValueMail = $(this).find('.customerMail').text();

								$(jsonFieldDatas[0].fieldNumber).attr('value', searchValueCustomerNumber);
								$(jsonFieldDatas[0].fieldName).attr('value', searchValueCustomerName);
								$(jsonFieldDatas[0].fieldZipCode).attr('value', searchValueCustomerZipCode);
								$(jsonFieldDatas[0].fieldCity).attr('value', searchValueCustomerCity);
								$(jsonFieldDatas[0].fieldCountry).attr('value', searchValueCustomerCountry);

								$(jsonFieldDatas[0].fieldSalesmanName).attr('value', searchValueSalesmanName);
								$(jsonFieldDatas[0].fieldSalesmanID).attr('value', searchValueSalesmanID);
								$(jsonFieldDatas[0].fieldMail).attr('value', searchValueMail);
								// $('#editID').attr('value', searchValueCustomerID);
							}
							else if(searchMode == 'searchBankCode' || searchMode == 'searchBankName' || searchMode == 'searchBankBIC' || searchMode == 'searchBankIBAN') {
								var searchValuesBankName = $(this).find('.searchBankName').text();
								var searchValuesBankCode = $(this).find('.searchBankCode').text();
								var searchValuesBankBIC = $(this).find('.searchBankBIC').text();

								$(jsonFieldDatas[0].fieldName).attr('value', searchValuesBankName);
								$(jsonFieldDatas[0].fieldCode).attr('value', searchValuesBankCode);
								$(jsonFieldDatas[0].fieldBIC).attr('value', searchValuesBankBIC);
							}
							else if(searchMode == 'searchCityPlz' || searchMode == 'searchPlzCity') {
								var searchValueZipCode = $(this).find('.cityZipCode').text();
								var searchValueCityName = $(this).find('.cityName').text();

								$(jsonFieldDatas[0].fieldCity).attr('value', searchValueCityName);
								$(jsonFieldDatas[0].fieldZipCode).attr('value', searchValueZipCode);
							}
							else if(searchMode == 'searchCustomerName' || searchMode == 'searchCustomerNumber') {
								var searchValueCustomerNumber = $(this).find('.customerNumber').text();
								var searchValueCustomerName = $(this).find('.customerName').text();
								var searchValueCustomerID = $(this).find('.customerNumber').text();								

								$(jsonFieldDatas[0].fieldNumber).attr('value', searchValueCustomerNumber);
								$(jsonFieldDatas[0].fieldName).attr('value', searchValueCustomerName);
								// $('#editID').attr('value', searchValueCustomerID);
							}
							else if(searchMode == 'searchCustomerNumberAndID') {
								var searchValueCustomerNumber = $(this).find('.customerNumber').text();
								var searchValueCustomerName = $(this).find('.customerName').text();
								var searchValueCustomerID = $(this).find('.customerID').text();

								$(jsonFieldDatas[0].fieldNumber).attr('value', searchValueCustomerNumber);
								$(jsonFieldDatas[0].fieldID).attr('value', searchValueCustomerID);
								// $('#editID').attr('value', searchValueCustomerID);
							}
							else if(searchMode == 'searchSupplierName' || searchMode == 'searchSupplierNumber') {
								var searchValueSupplierNumber = $(this).find('.supplierNumber').text();
								var searchValueSupplierName = $(this).find('.supplierName').text();
								var searchValueSupplierID = $(this).find('.supplierNumber').text();

								$(jsonFieldDatas[0].fieldNumber).attr('value', searchValueSupplierNumber);
								$(jsonFieldDatas[0].fieldName).attr('value', searchValueSupplierName);
								// $('#editID').attr('value', searchValueSupplierID);
							}
							else if(searchMode == 'searchPLZ') {
								var searchValueZipCode = $(this).find('.cityZipCode').text();
								$(jsonFieldDatas[0].fieldZipCode).attr('value', searchValueZipCode);
							}
							else if(searchMode == 'searchProduct') {
								var searchValueText = $(this).text();
								$(jsonFieldDatas[0].fieldText).attr('value', searchValueText);
							}
							else if(searchMode == 'searchProductName' || searchMode == 'searchProductNumber') {
								var searchValueProductNumber = $(this).find('.productNumber').text();
								var searchValueProductName = $(this).find('.productName').text();
								var searchValueProductID = $(this).find('.productID').text();
								var searchValueProductCategoryID = $(this).find('.productCategoryID').text();

								$(jsonFieldDatas[0].fieldNumber).attr('value', searchValueProductNumber);
								$(jsonFieldDatas[0].fieldName).attr('value', searchValueProductName);
								$(jsonFieldDatas[0].fieldID).attr('value', searchValueProductID);
								$(jsonFieldDatas[0].fieldCatID).attr('value', searchValueProductCategoryID);
							}
							else if(searchMode == 'searchSalesmanName') {
								var searchValue = $(this).text();
								$(jsonFieldDatas[0].fieldName).attr('value', searchValue);
							}
							else if(searchMode == 'searchSalesmen') {
								var searchValueCustomerNumber = $(this).find('.customerNumber').text();
								var searchValueCustomerName = $(this).find('.customerName').text();
								var searchValueCustomerID = $(this).find('.customerID').text();

								$(jsonFieldDatas[0].fieldID).attr('value', searchValueCustomerID);
								$(jsonFieldDatas[0].fieldNumber).attr('value', searchValueCustomerNumber);
								$(jsonFieldDatas[0].fieldName).attr('value', searchValueCustomerName);
							}
							else if(searchMode == 'searchStreetName') {
								var searchValue = $(this).text();
								$(jsonFieldDatas[0].fieldStreet).attr('value', searchValue);
							}
							else if(searchMode == 'searchFinanceOffice') {
								var searchValue = $(this).text();
								$(jsonFieldDatas[0].fieldName).attr('value', searchValue);
							}
							$('#searchResultArea').slideUp(animateTime, function(){
								$('#searchResultArea').remove();
							});
						});
					}
					else {
						$('#searchResultArea').remove();
					}
				}
			});

			$('.noticeBoxClose .iconClose').click(function(){
				if(cleanMode == '1'){ $(triggerElement).attr('value', ''); }
				$('#searchResultArea').fadeOut(animateTime, function(){
					$('#searchResultArea').remove()
				});
			});
		}

		$('.inputButton0').enter(function() {
			$('#searchResultArea').remove();
		});

		/*
		var code = (e.keyCode ? e.keyCode : e.which);
		if(code == 13) { //Enter keycode
			$('#searchResultArea').remove();
		}
		$('form').keyup(function (e) {
			if (e.keyCode == 13) {
				$('#searchResultArea').remove();
			}
		});
		*/
	}
	catch(err) { handleJsError(err); return false; }
	finally {}
}

// ------------------------------------------------------------

function createCustomerNumber(formName, formElementGet, formElementSet) {
	try {
		var form = window.document.forms[formName];
		var formElementGet = form.elements[formElementGet];
		var formElementSet = form.elements[formElementSet];
		var getElementValue = formElementGet.value;
		if(getElementValue == "") {
			alert('Bitte geben Sie im Feld "Kundennummer" die ersten beiden Ziffern der Kunden-Postleitzahl an.');
		}
		else {
			$.get('inc/generateCustomerNumber.inc.php?searchString=' + getElementValue, function(result) {
				$(formElementSet).attr('value', result);
			});
		}
	}
	catch(err) { handleJsError(err); return false; }
	finally {}
}
// ------------------------------------------------------------

function getProductPrices(elementMenge, formElementSinglePreis, elementArtikelID) {
	try {
		$('.loadingArea').remove();
		// $(formElementSinglePreis).parent().append('<div style="position:absolute;width:40px;height:20px;top:auto;left:auto;background-color:#FFFFFF;text-align:center;"><img src="layout/ajax-loader.gif" /></div>');
		$(formElementSinglePreis).parent().append('<span class="loadingArea"><img class="loader" src="layout/ajax-loader.gif" /> Bitte haben Sie ein wenig Geduld. Die Daten werden geladen...</span>');
		// $(formElementTotalPreis).parent().append('<span class="loadingArea"><img class="loader" src="layout/ajax-loader.gif" /> Bitte haben Sie ein wenig Geduld. Die Daten werden geladen...</span>');

		$.get('inc/getProductPrices.inc.php?elementMenge=' + elementMenge + '&elementArtikelID=' + elementArtikelID, function(result) {
			var searchValueSinglePreis = result;

			if(isNaN(searchValueSinglePreis) || searchValueSinglePreis == '') {
				searchValueSinglePreis = 0;
			}
			else {
				searchValueSinglePreis = myToFixed(searchValueSinglePreis, 2);
			}
			if(searchValueSinglePreis == 0) {
				// window.alert('Es konnte kein Preis gefunden werden ...');
			}
			//var searchValueTotalPreis = searchValueSinglePreis * thisMenge;
			//searchValueTotalPreis = myToFixed(searchValueTotalPreis, 2);
			$(formElementSinglePreis).attr('value', searchValueSinglePreis);
			// $(formElementTotalPreis).attr('value', searchValueTotalPreis);
			$('.loadingArea').remove();
		});
	}
	catch(err) { handleJsError(err); return false; }
	finally {}
}

// ------------------------------------------------------------

function myToFixed(value, decimal) {
	try {
		value = (value.toFixed) ? value.toFixed(decimal) : Math.floor(value) + "," + (100 + Math.round((value-Math.floor(value)) * 100) + "").substr(1,decimal);
		return value;
	}
	catch(err) { handleJsError(err); return false; }
	finally {}
}

// ------------------------------------------------------------

function loadNotice(element) {
	try {
		var animateTime = 0;
		$('#loadNoticeArea').remove();
		// var thisNotice = element.attr('alt');
		var content;
		var contentHeader = element.attr('alt');
		var contentText = element.parent().find('.displayNoticeArea').html();

		content = '<div class="noticeBoxHeader">' + contentHeader + '</div>';
		content += '<div class="noticeBoxContents">' + contentText + '</div>';
		content += '<div class="noticeBoxFooter">' + '' + '</div>';
		// content += '<div class="noticeBoxClose">' + '<img src="layout/icons/iconClose.png" class="iconClose" width="14" height="14" alt="" title="Liste schlie&szlig;en" />' + '</div>';
		var arrWindowButtons = loadWindowButtons();
		content += '<div class="noticeBoxClose">' + arrWindowButtons['small'] + ' ' + arrWindowButtons['move'] + ' ' + arrWindowButtons['close'] + '</div>';
		content = '<div class="loadNoticeContent">' + content + '</div>';


		element.parent().append('<div id="loadNoticeArea" class="loadedWindow"></div>');
		$('#loadNoticeArea').hide();
		$('#loadNoticeArea').fadeIn(animateTime);
		$('#loadNoticeArea').empty().html('<span class="loadingArea"><img class="loader" src="layout/ajax-loader.gif" /> Bitte haben Sie ein wenig Geduld. Die Daten werden geladen...</span>');
		$('#loadNoticeArea').html(content);
		actionWindowButtons();
	}
	catch(err) { handleJsError(err); return false; }
	finally {}
}

// ------------------------------------------------------------

function checkFormDutyFields(formName, jsonData) {
	try {
		var returnValue = true;

		var arrEmptyFields = new Array();
		var arrEmptyFieldLabels = new Array();
		var thisElementType = '';
		var thisElementLength = 1;
		var control = 0;
		var form = window.document.forms[formName];
		var firstEmptyField = null;
		var alertText = 'Bitte füllen Sie alle Pflichtfelder korrekt aus!';

		for (var index in jsonData){
			// alert(jsonData[index].fieldName);
			// alert(jsonData[index].fieldLabel);
			thisElementType = form.elements[jsonData[index].fieldName].type;
			// alert(thisElementType);
			if(thisElementType == undefined) {
				thisElementLength = form.elements[jsonData[index].fieldName].length;
				control = 0;
				for(j = 0 ; j < thisElementLength ; j++) {
					thisElementType = form.elements[jsonData[index].fieldName][j].type;

					if(thisElementType == 'radio') {
						if(form.elements[jsonData[index].fieldName][j].checked) {
							control = 1;
						}
					}
					else if(thisElementType == 'checkbox') {
						if(form.elements[jsonData[index].fieldName][j].checked) {
							control = 1;
						}
					}
				}
				if(control != 1) {
					form.elements[jsonData[index].fieldName][0].parentNode.parentNode.style.borderColor = colorWarningBorder;
					// form.elements[jsonData[index].fieldName][0].parentNode.parentNode.style.backgroundColor = colorWarningBackground;
					form.elements[jsonData[index].fieldName][0].style.backgroundColor = colorWarningBackground;
					arrEmptyFields.push(jsonData[index].fieldName);
					arrEmptyFieldLabels.push(jsonData[index].fieldLabel);
					returnValue = false;
				}
				else {
					form.elements[jsonData[index].fieldName][0].parentNode.parentNode.style.borderColor = '';
					form.elements[jsonData[index].fieldName][0].parentNode.parentNode.style.backgroundColor = '';
				}
			}
			else if(thisElementType == 'text' || thisElementType == 'password') {
				if(form.elements[jsonData[index].fieldName].value == '') {
					form.elements[jsonData[index].fieldName].style.borderColor = colorWarningBorder;
					form.elements[jsonData[index].fieldName].parentNode.style.backgroundColor = colorWarningBackground;
					arrEmptyFields.push(jsonData[index].fieldName);
					arrEmptyFieldLabels.push(jsonData[index].fieldLabel);
					returnValue = false;
				}
				else {
					form.elements[jsonData[index].fieldName].style.borderColor = '';
					form.elements[jsonData[index].fieldName].parentNode.style.backgroundColor = '';
				}
			}
			else if(thisElementType == 'select-one') {
				if(form.elements[jsonData[index].fieldName].selectedIndex == 0) {
					form.elements[jsonData[index].fieldName].style.borderColor = colorWarningBorder;
					form.elements[jsonData[index].fieldName].parentNode.style.backgroundColor = colorWarningBackground;
					arrEmptyFields.push(jsonData[index].fieldName);
					arrEmptyFieldLabels.push(jsonData[index].fieldLabel);
					returnValue = false;
				}
				else {
					form.elements[jsonData[index].fieldName].style.borderColor = '';
					form.elements[jsonData[index].fieldName].parentNode.style.backgroundColor = '';
				}
			}
			else if(thisElementType == 'select-multiple') {
				if(form.elements[jsonData[index].fieldName].selectedIndex == -1) {
					form.elements[jsonData[index].fieldName].style.borderColor = colorWarningBorder;
					form.elements[jsonData[index].fieldName].parentNode.style.backgroundColor = colorWarningBackground;
					arrEmptyFields.push(jsonData[index].fieldName);
					arrEmptyFieldLabels.push(jsonData[index].fieldLabel);
					returnValue = false;
				}
				else {
					form.elements[jsonData[index].fieldName].style.borderColor = '';
					form.elements[jsonData[index].fieldName].parentNode.style.backgroundColor = '';
				}
			}
			if(firstEmptyField == null && returnValue == false) {
				firstEmptyField = index;
			}
		}

		if(!returnValue) {

			thisElementType = form.elements[jsonData[firstEmptyField].fieldName].type;
			if(thisElementType != undefined) {
				form.elements[jsonData[firstEmptyField].fieldName].focus();
			}
			else {
				form.elements[jsonData[firstEmptyField].fieldName][0].focus();
			}
			alert(alertText + '\n\n' + arrEmptyFieldLabels.join(',\n') );
		}
		else {
			returnValue = true;
		}
		return returnValue;
	}
	catch(err) { handleJsError(err); return false; }
	finally {}
}

// ------------------------------------------------------------

function xxxxcopyFields(formID, getElementID, setElementID) {

	try {
		var form = $(formID);
		$(getElementID + ' input').keyup(function(){
			// var getFormElementName = $(this).attr('name');
			var getFormElementId = $(this).attr('id');
			// var getFormElementNamePart = getElementID.replace('#', '');
			var getFormElementIdPart = getElementID.replace('#', '');
			// var setFormElementNamePart = setElementID.replace('#', '');
			var setFormElementIdPart = setElementID.replace('#', '');
			// setFormElementName = getFormElementName.replace(getFormElementNamePart, setFormElementNamePart);
			setFormElementId = '#' + getFormElementId.replace(getFormElementIdPart, setFormElementIdPart);
			// $('input[name=' + setFormElementName + ']').attr('value', $(this).attr('value'));
			$(setFormElementId).attr('value', $(this).attr('value'));
		});

		$(getElementID + ' select').change(function(){
			// var getFormElementName = $(this).attr('name');
			var getFormElementId = $(this).attr('id');
			// var getFormElementNamePart = getElementID.replace('#', '');
			var getFormElementIdPart = getElementID.replace('#', '');
			// var setFormElementNamePart = setElementID.replace('#', '');
			var setFormElementIdPart = setElementID.replace('#', '');
			// setFormElementName = getFormElementName.replace(getFormElementNamePart, setFormElementNamePart);
			setFormElementId = '#' + getFormElementId.replace(getFormElementIdPart, setFormElementIdPart);
			// $('select[name=' + setFormElementName + ']').attr('value', $(this).attr('value'));
			$(setFormElementId).attr('value', $(this).attr('value'));
		});
	}
	catch(err) { handleJsError(err); return false; }
	finally {}
}

function copyFieldValues(formID, getElementID, setElementID, getFormElementId, triggerID) {
	try {
		if($(triggerID).attr('checked') == 'checked') {
			$(setElementID).find('input, select').attr('readonly', true);
			$(setElementID).find('input, select').css('background-image', 'none');
			$(setElementID).find('input, select').css('background-color', '#DDDDDD');
			$(setElementID).find('input, select').css('color', '#666');
			var getFormElementIdPart = getElementID.replace('#', '');
			var setFormElementIdPart = setElementID.replace('#', '');
			setFormElementId = '#' + getFormElementId.replace(getFormElementIdPart, setFormElementIdPart);
			$(setFormElementId).attr('value', $('#' + getFormElementId).attr('value'));
		}
		else {
			$(setElementID).find('input, select').attr('readonly', false);
			$(setElementID).find('input, select').css('background-image', '');
			$(setElementID).find('input, select').css('background-color', '');
			$(setElementID).find('input, select').css('color', '');
		}
	}
	catch(err) { handleJsError(err); return false; }
	finally {}
}
function copyFields(formID, getElementID, setElementID, triggerID) {
	try {
		var form = $(formID);
		$(getElementID + ' input').live('keyup', function(){
			copyFieldValues(formID, getElementID, setElementID, $(this).attr('id'), triggerID);
		});

		$(getElementID + ' select').live('change', function(){
			copyFieldValues(formID, getElementID, setElementID, $(this).attr('id'), triggerID);
		});

	}
	catch(err) { handleJsError(err); return false; }
	finally {}
}


// ------------------------------------------------------------
function toggleColums(elementTable, setCookieName) {
	var iconPath = "layout/icons/";
	var buttonClose = 'iconSlideIn.png';
	var buttonOpen = 'iconSlideOut.png';
	/* zelleninhalte in containder packen, damit der container ein- und ausgeblendet werden kann */

	jQuery(jQuery(elementTable + ' tr')).each( function(index,value){
		jQuery(jQuery(this).find('td')).each(function(cindex,value){
			var container = '<span id="zelle_'+index+'_'+cindex+'">'+jQuery(this).html()+'</span>';
			jQuery(this).html(container);
		});
	});

	/* alle spalten header mit link zum auf- und zuklappen versehen */
	jQuery(elementTable + ' tr th').each( function(index,value){
		// var container = '<span class="tableheader">'+jQuery(this).html()+'</span>';
		//jQuery(this).html(container);
		var id='spalte'+index;
		var title='Spalte \'' + jQuery(this).first().text()+'\' auf- und zuklappen';
		// jQuery(this).append('<a href="" id="'+id+'" title="'+title+'"><span class="close">[-]</span><span class="open">[+]</span></a>');
		jQuery(this).append('<span class="buttonToggleColumns" id="'+id+'" title="'+title+'"><span class="close"><img src="' + iconPath + buttonClose + '" /></span><span class="open"><img src="' + iconPath + buttonOpen + '" /></span></span>');
		var linkOpen=jQuery(this).children('span#'+id+'');
		jQuery(linkOpen).children('.open').hide();
		jQuery(linkOpen).children('.open').css('display', 'none');
		jQuery(linkOpen).bind('click',function() {
			var pindex = index;
			jQuery(elementTable + ' tr').each(function(tindex,value){
				jQuery(jQuery(this).find('td')).each(function(cindex,value){
					if(cindex == pindex){
						jQuery(this).children().first().toggle();
					}
				});
			});
			jQuery(elementTable + ' tr th').each(function(thindex,value){
				if(thindex == pindex){
					jQuery(this).children().first().toggle();
					jQuery(this).find('.sortDirectionButton').toggle();
					jQuery(linkOpen).children('.close').toggle();
					jQuery(linkOpen).children('.open').toggle();
				}
			});

			// jQuery(elementTable + ' tfoot tr td').children().first().css('display', 'inline');
			jQuery(elementTable + ' tfoot tr td').children().first().fadeIn();

			$('.buttonShowAllColumns').css('color', '');
			$('.buttonHideAllColumns').css('color', '');

			setToggleCookie(elementTable, setCookieName);
			jQuery(linkOpen).blur();
			return false;
		});
	});

	jQuery('.buttonShowAllColumns').bind('click', function(){
		jQuery(elementTable + ' tr th').each(function(thindex,value){
			jQuery(this).children().first().show();
			// jQuery(this).find('.sortDirectionButton').toggle();
			jQuery(this).find('.sortDirectionButton').show();
		});
		jQuery(elementTable + ' tr td').each(function(thindex,value){
			jQuery(this).children().first().show();
		});
		jQuery('.close').show();
		jQuery('.open').hide();
		$(this).css('color', '#999999');
		$('.buttonHideAllColumns').css('color', '');
		setToggleCookie(elementTable, setCookieName);
	});
	jQuery('.buttonHideAllColumns').bind('click', function(){
		jQuery(elementTable + ' tr th').each(function(thindex,value){
			jQuery(this).children().first().hide();
			// jQuery(this).find('.sortDirectionButton').toggle();
			jQuery(this).find('.sortDirectionButton').hide();
		});
		jQuery(elementTable + ' tr td').each(function(thindex,value){
			jQuery(this).children().first().hide();
		});
		jQuery('.close').hide();
		jQuery('.open').show();
		$(this).css('color', '#999999');
		$('.buttonShowAllColumns').css('color', '');
		jQuery(elementTable + ' tfoot tr td').children().show();
		setToggleCookie(elementTable, setCookieName);
	});
}
// ------------------------------------------------------------
function setToggleCookie(elementSelector, cookieName) {
	var elementMode;
	var elementJSON = '';
	var  cookieExpires = new Date();
	cookieExpires = new Date(cookieExpires.getTime() + 1000 * 60 * 60 * 24 * 365);

	var elementType = $(elementSelector).get(0).tagName;
	if(elementType == 'TABLE') {

		jQuery(elementSelector + ' th').each(function(index, value) {
			elementMode = jQuery(this).children().first().css('display');
			if(elementMode == 'none') {
				elementJSON += '{' + '"elementID": "' + index + '", ' + '"elementMode": "' + elementMode + '"' + '}, ';
			}
		});
		if(elementJSON != '') {
			elementJSON = elementJSON.substr(0, (elementJSON.length - 2));
			elementJSON = '[' + elementJSON + ']';
			document.cookie = cookieName + '=' + elementJSON + '; expires=' + cookieExpires + '; path=/; ';
		}
		else {
			elementJSON = '';
			cookieExpires = new Date(cookieExpires.getTime() - 1000 * 60 * 60 * 24 * 365);
			document.cookie = cookieName + '=' + elementJSON + '; expires=' + cookieExpires + '; path=/; ';
		}
	}
}

// ------------------------------------------------------------

function getToggleCookie(elementSelector, cookieName) {
	if(document.cookie) {

		a = document.cookie;

		var arrCookieDatas = a.split("; ");
		var arrThisCookieData;
		for(i = 0 ; i < arrCookieDatas.length ; i++) {

			var arrThisCookieData =  arrCookieDatas[i].split("=");
			if( arrThisCookieData[0] == cookieName) {
				var cookieName = arrThisCookieData[0];
				var cookieValue = arrThisCookieData[1];
			}
		}

		if(cookieValue) {
			if($(elementSelector).size() > 0){	
			// if($(elementSelector).length > 0){			
				
				var elementType = $(elementSelector).get(0).tagName;
				var objJSON = jQuery.parseJSON(cookieValue);

				jQuery(objJSON).each(function(indexJSON) {
					if(elementType == 'TABLE') {
						$(elementSelector + ' th').each(function(indexElement) {
							if(objJSON[indexJSON].elementID == indexElement) {
								// jQuery(this).children().first().css('display', objJSON[indexJSON].elementMode);
								jQuery(this).find('.close').toggle();
								jQuery(this).find('.open').toggle();
								jQuery(this).children().first().toggle();
								jQuery(this).find('.sortDirectionButton').toggle();
							}
						});
						jQuery(elementSelector + ' tr').each(function(tindex,value){
							$(jQuery(this).find('td')).each(function(indexElement) {
								if(objJSON[indexJSON].elementID == indexElement) {
									// jQuery(this).children().first().css('display', objJSON[indexJSON].elementMode);
									jQuery(this).children().first().toggle();
								}
							});
						});
						// jQuery(elementTable + ' tfoot tr td').children().first().css('display', 'inline');
						jQuery(elementSelector + ' tfoot tr td').children().first().show();
					}
				});
			}
		}

	}
}
// ------------------------------------------------------------
function toggleDisplayDebugArea(elementSelector, cookieName){
	cookieName = 'displayDebugArea';
	$(elementSelector).toggle();
	var  cookieExpires = new Date();
	cookieExpires = new Date(cookieExpires.getTime() + 1000 * 60 * 60 * 24 * 365);
	document.cookie = cookieName + '=' + $(elementSelector).css('display') + '; expires=' + cookieExpires + '; path=/; ';
}
function getCookieDisplayDebugArea(elementSelector, cookieName){
	cookieName = 'displayDebugArea';
	a = document.cookie;
	var arrCookieDatas = a.split("; ");
	var arrThisCookieData;
	for(i = 0 ; i < arrCookieDatas.length ; i++) {
		var arrThisCookieData =  arrCookieDatas[i].split("=");
		if( arrThisCookieData[0] == cookieName) {
			var cookieName = arrThisCookieData[0];
			var cookieValue = arrThisCookieData[1];
		}
	}
	if(cookieValue) {
		$(elementSelector).css('display', cookieValue);
	}
}
// ------------------------------------------------------------
function setFormDutyFields(jsonData) {
	for (var index in jsonData){
		thisElement = jsonData[index].fieldName;
		$('#' + thisElement).parent().parent().find('td:first b, td:first label').append(' <span class="dutyField">(*)</span>');
	}
}
// ------------------------------------------------------------

function selectOtherOption(triggerID, changeID) {
	// alert(triggerID + " - " + changeID);
	var triggerVal = $(triggerID).val();
	if(triggerVal == 4 || triggerVal == 3 || triggerVal == 2){
		$(changeID).val(1);
	}
	else {
		$(changeID).val(0);
	}
}

// ------------------------------------------------------------

function loadPaymentDetails(triggerElement, searchString, linkPath) {
	// loadOrderDetails(triggerElement, documentRoot, title);
	var loadURL = 'inc/loadPaymentDetails.inc.php';
	var animateTime = 'fast';
	$('#loadOrderDetailsArea').remove();
	$(triggerElement).parent().append('<div class="loadedWindow" id="loadOrderDetailsArea"></div>');
	$('#loadOrderDetailsArea').slideDown(animateTime);
	var content = '';
	var contentHeader = 'Details der Zahlung';
	var contentText = '';
	var contentFooter = '';
	content = '<div class="noticeBoxHeader">' + contentHeader + '</div>';
	content += '<div class="noticeBoxContents">' + contentText + '</div>';
	content += '<div class="noticeBoxFooter">' + contentFooter + '</div>';
	// content += '<div class="noticeBoxClose">' + '<img src="layout/icons/iconClose.png" class="iconClose" width="14" height="14" alt="" title="Liste schlie&szlig;en" />' + '</div>';
	var arrWindowButtons = loadWindowButtons();

	content += '<div class="noticeBoxClose">' + arrWindowButtons['small'] + ' ' + arrWindowButtons['move'] + ' ' + arrWindowButtons['close'] + '</div>';
	content = '<div class="loadNoticeContent">' + content + '</div>'

	$('#loadOrderDetailsArea').html(content);

	actionWindowButtons();

	$('.noticeBoxContents').empty().html('<span class="loadingArea"><img class="loader" src="layout/ajax-loader.gif" /> Bitte haben Sie ein wenig Geduld. Die Daten werden geladen...</span>');

	loadURL += '?searchString=' + searchString;
	loadURL += '&linkPath=' + linkPath;

	$('.noticeBoxContents').load(loadURL, function(response, status, xhr){
		if (status == 'error') {
			// var msg = 'Es ist ein Fehler aufgetreten. Fehler-Code: ';
			var msg = 'Die Daten konnten nicht geladen werden. Fehler-Code: ';
			$('.noticeBoxContents').html(msg + xhr.status + ' ' + xhr.statusText);
			//$('#loadOrderDetailsArea').remove();
		}
		else {
			if($('.noticeBoxContents').html() != '') {
				$('#loadOrderDetailsArea').fadeIn(animateTime);
			}
			else {
				//$('#loadOrderDetailsArea').remove();
			}
		}
	});
}

// ------------------------------------------------------------

function loadReportDetails(triggerElement, documentRoot, title) {
	try {
		var status = $('#loadReportDetailsArea').css('visibility');
		var animateTime = 0;
		var screenFactor = 2;
		var screenAddX = -300;
		// var screenAddY = 132;
		var screenAddY = 20;
		// var posX = ($(window).width() / screenFactor)  + screenAddX;
		var posX = 30;
		var posY = $('html').scrollTop() + screenAddY;

		var content;
		var contentHeader = title;
		var contentText = '';

		content = '<div class="noticeBoxHeader">' + contentHeader + '</div>';
		content += '<div class="noticeBoxContents">' + contentText + '</div>';
		content += '<div class="noticeBoxFooter">' + '' + '</div>';
		// content += '<div class="noticeBoxClose">' + '<img src="layout/icons/iconClose.png" class="iconClose" width="14" height="14" alt="" title="Liste schlie&szlig;en" />' + '</div>';
		var arrWindowButtons = loadWindowButtons();

		content += '<div class="noticeBoxClose">' + arrWindowButtons['small'] + ' ' + arrWindowButtons['move'] + ' ' + arrWindowButtons['close'] + '</div>';
		content = '<div class="loadNoticeContent">' + content + '</div>'

		$('#loadReportDetailsArea').fadeOut(animateTime).remove();

		element = $('body');
		// element = element.parent();
		element.append('<div class="loadedWindow" id="loadReportDetailsArea"></div>');
		$('#loadReportDetailsArea').hide();
		// $('#loadReportDetailsArea').css('height', '90%');
		$('.loadNoticeContent').css('height', '90%');
		$('#loadReportDetailsArea').css('top', posY + 'px');
		$('#loadReportDetailsArea').css('left', posX + 'px');
		$('#loadReportDetailsArea').fadeIn(animateTime);
		$('#loadReportDetailsArea').html(content);

		actionWindowButtons();

		$('.noticeBoxContents').empty().html('<span class="loadingArea"><img class="loader" src="layout/ajax-loader.gif" /> Bitte haben Sie ein wenig Geduld. Die Daten werden geladen...</span>');
		contentText = triggerElement.parent().find('.orderDetailsContent').html();
		$('.noticeBoxContents').html(contentText);

		colorRowMouseOver('.displayOrders tbody tr');
	}
	catch(err) { }
	finally {}
}

// ------------------------------------------------------------

function loadOrderDetails(triggerElement, documentRoot, title) {
	try {
		var status = $('#loadOrderDetailsArea').css('visibility');
		var animateTime = 0;
		var screenFactor = 2;
		var screenAddX = -300;
		var screenAddY = 132;
		// var posX = ($(window).width() / screenFactor)  + screenAddX;
		var posX = 184;
		var posY = $('html').scrollTop() + screenAddY;

		var content;
		var contentHeader = title;
		var contentText = '';

		content = '<div class="noticeBoxHeader">' + contentHeader + '</div>';
		content += '<div class="noticeBoxContents">' + contentText + '</div>';
		content += '<div class="noticeBoxFooter">' + '' + '</div>';
		// content += '<div class="noticeBoxClose">' + '<img src="layout/icons/iconClose.png" class="iconClose" width="14" height="14" alt="" title="Liste schlie&szlig;en" />' + '</div>';
		var arrWindowButtons = loadWindowButtons();

		content += '<div class="noticeBoxClose">' + arrWindowButtons['small'] + ' ' + arrWindowButtons['move'] + ' ' + arrWindowButtons['close'] + '</div>';
		content = '<div class="loadNoticeContent">' + content + '</div>'

		$('#loadOrderDetailsArea').fadeOut(animateTime).remove();

		element = $('body');
		// element = element.parent();
		element.append('<div class="loadedWindow" id="loadOrderDetailsArea"></div>');
		$('#loadOrderDetailsArea').hide();
		// $('#loadOrderDetailsArea').css('top', posY + 'px');
		$('#loadOrderDetailsArea').css('left', posX + 'px');
		$('#loadOrderDetailsArea').fadeIn(animateTime);
		$('#loadOrderDetailsArea').html(content);

		actionWindowButtons();

		$('.noticeBoxContents').empty().html('<span class="loadingArea"><img class="loader" src="layout/ajax-loader.gif" /> Bitte haben Sie ein wenig Geduld. Die Daten werden geladen...</span>');
		contentText = triggerElement.parent().find('.orderDetailsContent').html();
		$('.noticeBoxContents').html(contentText);

		colorRowMouseOver('.displayOrders tbody tr');
	}
	catch(err) { }
	finally {}
}

// ------------------------------------------------------------

function insertProductDatas(formName, getFieldId, setFields) {
	var form = window.document.forms[formName];
	var getDatasValue = $('#' + getFieldId).val();
	var getDatasText = $('#' + getFieldId + ' option:selected').text();
	setFields = setFields.substring(1, (setFields.length - 1));
	var arrSetFieldIDs = setFields.split(',');

	var arrGetDatasText = getDatasText.split(' / ');
	var productName = $.trim(arrGetDatasText[0].replace(/•/, ''));
	var productCat = $.trim(arrGetDatasText[1]);
	var productModel = $.trim(arrGetDatasText[2]);
	var productID = $.trim(arrGetDatasText[3]);
	$('#' + arrSetFieldIDs[0]).val(productModel);
	$('#' + arrSetFieldIDs[1]).val(productName);
	$('#' + arrSetFieldIDs[2]).val(productID);
	
}
function insertProductDatas2(formName, getFieldName, setFieldNames) {
	var form = window.document.forms[formName];
	var getDatasValue = $(getFieldName).val();
	var getDatasText = $(getFieldName).find('option:selected').text();
	setFieldNames = setFieldNames.substring(1, (setFieldNames.length - 1));
	var arrSetFieldIDs = setFieldNames.split(',');

	var arrGetDatasText = getDatasText.split(' / ');
	var productName = $.trim(arrGetDatasText[0].replace(/•/, ''));
	var productCat = $.trim(arrGetDatasText[1]);
	var productModel = $.trim(arrGetDatasText[2]);
	var productID = $.trim(arrGetDatasText[3]);
	$(getFieldName).parent().parent().parent().find('input[name*="' + arrSetFieldIDs[0] + '"]').val(productModel);
	$(getFieldName).parent().parent().parent().find('input[name*="' + arrSetFieldIDs[1] + '"]').val(productName);
	$(getFieldName).parent().parent().parent().find('input[name*="' + arrSetFieldIDs[2] + '"]').val(productID);
}
function loadProductDatas(formName, getFieldName, setFieldNames) {
	var animateTime = 0;
	try {
		$('#searchResultArea').remove();
		var searchString = $(getFieldName).attr('value');
		var form = window.document.forms[formName];
		var getDatasValue = $(getFieldName).val();
		setFieldNames = setFieldNames.substring(1, (setFieldNames.length - 1));
		var arrSetFieldIDs = setFieldNames.split(',');

		if(searchString != '') {
			$(getFieldName).parent().css('position', 'relative');
			$(getFieldName).parent().append('<div class="loadedWindow" id="searchResultArea"></div>');
			$('#searchResultArea').slideDown(animateTime);
			var loadURL = 'inc/loadSuggestions.inc.php';

			var content = '';
			var contentHeader = 'Gefundene Vorschl&auml;ge';
			var contentText = '';
			var contentFooter = '';
			content = '<div class="noticeBoxHeader">' + contentHeader + '</div>';
			content += '<div class="noticeBoxContents">' + contentText + '</div>';
			content += '<div class="noticeBoxFooter">' + contentFooter + '</div>';
			// content += '<div class="noticeBoxClose">' + '<img src="layout/icons/iconClose.png" class="iconClose" width="14" height="14" alt="" title="Liste schlie&szlig;en" />' + '</div>';
			var arrWindowButtons = loadWindowButtons();

			content += '<div class="noticeBoxClose">' + arrWindowButtons['small'] + ' ' + arrWindowButtons['move'] + ' ' + arrWindowButtons['close'] + '</div>';
			content = '<div class="loadNoticeContent">' + content + '</div>';

			$('#searchResultArea').html(content);

			actionWindowButtons();

			$('.noticeBoxContents').empty().html('<span class="loadingArea"><img class="loader" src="layout/ajax-loader.gif" /> Bitte haben Sie ein wenig Geduld. Die Daten werden geladen...</span>');

			if($(getFieldName).attr('name') == 'addProductDetails[ordersArtikelNummer][]') {
				loadURL += '?type=searchProductNumber';
			}
			else if($(getFieldName).attr('name') == 'addProductDetails[ordersArtikelBezeichnung][]') {
				loadURL += '?type=searchProductName';
			}

			loadURL += '&searchString=' + searchString;

			$('.noticeBoxContents').load(loadURL, function(response, status, xhr){
				if (status == 'error') {
					// var msg = 'Es ist ein Fehler aufgetreten. Fehler-Code: ';
					var msg = 'Die Daten konnten nicht geladen werden. Fehler-Code: ';
					$('.noticeBoxContents').html(msg + xhr.status + ' ' + xhr.statusText);
					$('#searchResultArea').remove();
				}
				else {
					if($('.noticeBoxContents').html() != '') {
						$('.searchResultItem').click(function(){
							if($(getFieldName).attr('name') == 'addProductDetails[ordersArtikelNummer][]') {
								var searchValueProductNumber = $(this).find('.productNumber').text();
								// $('#editOrdersArtikelNummer').attr('value', searchValueProductNumber);
								var searchValueProductName = $(this).find('.productName').text();
								// $('#editOrdersArtikelBezeichnung').attr('value', searchValueProductName);
								var searchValueProductID = $(this).find('.productID').text();
								// $('#editOrdersArtikelID').attr('value', searchValueProductID);
								var searchValueProductCategoryID = $(this).find('.productCategoryID').text();
								// $('#editOrdersArtikelID').attr('value', searchValueProductCategoryID);
							}
							else if($(getFieldName).attr('name') == 'addProductDetails[ordersArtikelBezeichnung][]') {
								var searchValueProductNumber = $(this).find('.productNumber').text();
								// $('#editOrdersArtikelNummer').attr('value', searchValueProductNumber);
								var searchValueProductName = $(this).find('.productName').text();
								// $('#editOrdersArtikelBezeichnung').attr('value', searchValueProductName);
								var searchValueProductID = $(this).find('.productID').text();
								// $('#editOrdersArtikelID').attr('value', searchValueProductID);
								var searchValueProductCategoryID = $(this).find('.productCategoryID').text();
								// $('#editOrdersArtikelID').attr('value', searchValueProductID);
							}
							$(getFieldName).parent().parent().parent().find('input[name*="' + arrSetFieldIDs[0] + '"]').val(searchValueProductNumber);
							$(getFieldName).parent().parent().parent().find('input[name*="' + arrSetFieldIDs[1] + '"]').val(searchValueProductName);
							$(getFieldName).parent().parent().parent().find('input[name*="' + arrSetFieldIDs[2] + '"]').val(searchValueProductID);
							$(getFieldName).parent().parent().parent().find('select[name*="' + arrSetFieldIDs[3] + '"]').val(searchValueProductCategoryID);
							
							$('#searchResultArea').slideUp(animateTime, function(){
								$('#searchResultArea').remove();
							});
						});
					}
				}
			});
		}
	}
	catch(err) { }
	finally {}

	/*
	var form = window.document.forms[formName];
	var getDatasValue = $(getFieldName).val();
	var getDatasText = $(getFieldName).find('option:selected').text();
	setFieldNames = setFieldNames.substring(1, (setFieldNames.length - 1));
	var arrSetFieldIDs = setFieldNames.split(',');

	var arrGetDatasText = getDatasText.split(' / ');
	var productName = $.trim(arrGetDatasText[0].replace(/•/, ''));
	var productCat = $.trim(arrGetDatasText[1]);
	var productModel = $.trim(arrGetDatasText[2]);
	$(getFieldName).parent().parent().parent().find('input[name*="' + arrSetFieldIDs[0] + '"]').val(productModel);
	$(getFieldName).parent().parent().parent().find('input[name*="' + arrSetFieldIDs[1] + '"]').val(productName);
	*/
}

// ------------------------------------------------------------
function insertAdditionalCosts(formName, getFieldName, setFields) {
	var form = window.document.forms[formName];
	// var getDatasValue = $('#' + getFieldId).val();
	var getDatasValue = $(getFieldName).val();
	var getDatasText = $(getFieldName).find('option:selected').text();
	setFields = setFields.substring(1, (setFields.length - 1));
	var arrSetFieldIDs = setFields.split(',');

	var arrGetDatasText = getDatasText.split(' - ');
	var additionalCosts = $.trim(arrGetDatasText[1]);
	$('#' + arrSetFieldIDs[0]).val(additionalCosts);
}
function insertAdditionalCosts2(formName, getFieldName, setFieldNames) {
	var form = window.document.forms[formName];
	var getDatasValue = $(getFieldName).val();
	var getDatasText = $(getFieldName).find('option:selected').text();
	setFieldNames = setFieldNames.substring(1, (setFieldNames.length - 1));
	var arrSetFieldIDs = setFieldNames.split(',');

	var arrGetDatasText = getDatasText.split(' - ');
	var additionalCosts = $.trim(arrGetDatasText[1]);
	$(getFieldName).parent().parent().parent().find('input[name*="' + arrSetFieldIDs[0] + '"]').val(additionalCosts);
}
// ------------------------------------------------------------
function openPDF(file, print, windowName) {
	windowPDF = window.open(file, windowName);
	$(windowPDF).ready(function() {
		windowPDF.focus();
		if(print) {
			setTimeout('printPDF(windowPDF)', 2000);
		}
	});
}
function printPDF(windowPDF) {
	windowPDF.focus();
	windowPDF.print();
}
// ------------------------------------------------------------
function loadAddFormProduct(triggerElement, form) {
	// var content = $('.addProductForm').html();
	var content = form;

	$(triggerElement).parent().parent().append('<span class="loadingArea"><img class="loader" src="layout/ajax-loader.gif" /> Bitte haben Sie ein wenig Geduld. Die Daten werden geladen...</span>');

	$('#addProductFormArea').append(content);
	$('.loadingArea').remove();
}
function deleteFormProductItem(triggerElement) {
	var countFormItems = $('.addProductFormItem').length;
	if(countFormItems > 1) {
		$(triggerElement).parent().parent().remove();
	}
}
// ------------------------------------------------------------
var modeSound;
function toggleSound(element, cookieModeSound) {
	var cookieExpires = new Date();
	var cookieExpiresTime = 1000 * 60 * 60 * 24 * 365;
	var cookieName = 'modeSound';

	var pathIcon = 'layout/menueIcons/menueSidebar/';
	var thisTitleStop = 'Alarm abschalten';
	var thisTitlePlay = 'Alarm einschalten';
	var thisSrcPlay = 'notifications.png';
	var thisSrcStop = 'noNotifications.png';

	if(modeSound == undefined) {
		modeSound = cookieModeSound;
	}
	if(modeSound == 'stop') {
		modeSound = 'play';
		title = thisTitleStop;
		src = thisSrcPlay;
		cookieExpires = 0 + cookieExpiresTime;
	}
	else {
		modeSound = 'stop';
		title = thisTitlePlay;
		src = thisSrcStop;
		cookieExpires = 0 - cookieExpiresTime;
	}
	$(element).attr('title', title);
	$(element).attr('src', pathIcon + src);
	document.cookie = cookieName + '=' + modeSound + '; expires=' + cookieExpires + '; path=/; ';
}

function loadEventWindow(timeIntervallLoad, timeIntervallDisplay){
	var loadURL = 'inc/loadEvents.inc.php';
	var animateTime = 'fast';

	var content = '';
	var contentHeader = 'Wichtige Ereignisse';
	var contentText = '';
	var contentFooter = '';
	content = '<div class="eventWindowHeader">' + contentHeader + '</div>';
	content += '<div class="eventWindowContents">' + contentText + '</div>';
	content += '<div class="eventWindowFooter">' + contentFooter + '</div>';
	content += '<div class="eventWindowClose">' + '<img src="layout/icons/iconClose.png" class="iconClose" width="14" height="14" alt="" title="Liste schlie&szlig;en" />' + '</div>';
	content = '<div class="eventWindowContent">' + content + '</div>';
	$('body').append('<div id="eventWindow"></div>');
	$('#eventWindow').html(content);
	$('#eventWindow').hide();
	$('.eventWindowContents').empty().html('<span class="loadingArea"><img class="loader" src="layout/ajax-loader.gif" /> Bitte haben Sie ein wenig Geduld. Die Daten werden geladen...</span>');

	$('.eventWindowContents').load(loadURL, function(response, status, xhr){
		if (status == 'error') {
			// var msg = 'Es ist ein Fehler aufgetreten. Fehler-Code: ';
			var msg = 'Die Daten konnten nicht geladen werden. Fehler-Code: ';
			$('.eventWindowContents').html(msg + xhr.status + ' ' + xhr.statusText);
			$('#eventWindow').remove();
		}
		else {
			if($('.eventWindowContents').html() != '') {
				$('#eventWindow').fadeIn(animateTime);
			}
			else {
				$('#eventWindow').remove();
			}
		}
	});
	$('.eventWindowClose .iconClose').click(function(){
		$('#eventWindow').fadeOut(animateTime, function(){
			$('#eventWindow').remove()
		});
	});
	setTimeout('unloadEventWindow(' + timeIntervallLoad + ', ' + timeIntervallDisplay + ')', timeIntervallDisplay);
}
// ------------------------------------------------------------
function unloadEventWindow(timeIntervallLoad, timeIntervallDisplay){
	var animateTime = 500;
	$('#eventWindow').fadeOut(animateTime, function(){
		$('#eventWindow').remove();
	});
	setTimeout('loadEventWindow(' + timeIntervallLoad + ', ' + timeIntervallDisplay + ')', timeIntervallLoad);
}
// ------------------------------------------------------------
function handleJsError(err) {
	// window.alert(err);
}
// ------------------------------------------------------------
function getOrderProductCategories(catID, triggerID, elementID, selectedKey) {
	// alert(elementID);
	var elementFormName = $(triggerID).closest("form").attr('name');

	var isOTLG = $('form[name=' + elementFormName + ']').find('#editOrdersIsOTLG').val();	
	
	var mandator = $(triggerID).val().toLowerCase();
	var loadURL = 'inc/loadOrderCategories.inc.php';
	loadURL += '?mandator=' + mandator + '&catID=' + catID + '&selectedKey=' + selectedKey;
	if(isOTLG > 0){
		loadURL += '&isOTLG=' + isOTLG;		
	}
	$(elementID + ' option').remove();
	$(elementID).parent().append('<span class="loadingArea"><img class="loader" src="layout/ajax-loader.gif" /> Bitte haben Sie ein wenig Geduld. Die Daten werden geladen...</span>');
	$(elementID).load(loadURL, function(response, status, xhr){
		if (status == 'error') {

		}
		else {
			if($(elementID).html() != '') {
				$(elementID).parent().find('.loadingArea').remove();
				if(catID == 'all'){
					if($(triggerID).closest("form")){
						var elementFormName = $(triggerID).closest("form").attr('name');
						// insertProductDatas(elementFormName, 'editOrdersArtikelKategorieID', '[editOrdersArtikelNummer,editOrdersArtikelBezeichnung]')
					}
				}
			}
			else {
				// $('#eventWindow').remove();
			}
		}
	});
}

function getOrderProductCategoriesNeu(customerGroupID, catID, triggerID, elementID, selectedKey) {	
	var elementFormName = $(triggerID).closest("form").attr('name');	
	
	var mandator = $(triggerID).val().toLowerCase();
	var loadURL = 'inc/loadOrderCategories.inc.php';
	loadURL += '?mandator=' + mandator + '&catID=' + catID + '&selectedKey=' + selectedKey;
	if(customerGroupID > 0){
		loadURL += '&customerGroupID=' + customerGroupID;		
	}
	
	$(elementID + ' option').remove();
	$(elementID).parent().append('<span class="loadingArea"><img class="loader" src="layout/ajax-loader.gif" /> Bitte haben Sie ein wenig Geduld. Die Daten werden geladen...</span>');
	$(elementID).load(loadURL, function(response, status, xhr){
		if (status == 'error') {

		}
		else {
			if($(elementID).html() != '') {
				$(elementID).parent().find('.loadingArea').remove();
				if(catID == 'all'){
					if($(triggerID).closest("form")){
						var elementFormName = $(triggerID).closest("form").attr('name');
						// insertProductDatas(elementFormName, 'editOrdersArtikelKategorieID', '[editOrdersArtikelNummer,editOrdersArtikelBezeichnung]')
					}
				}
			}
			else {
				// $('#eventWindow').remove();
			}
		}
	});
}
// ------------------------------------------------------------

function styleTitleAttribute() {
	$('body').append('<div id="replaceTitleAttribute"></div>');
	$('*[title!=""]').each(function() {
		var a = $(this);
		if(a.attr('title') != undefined && a.attr('title') != '') {
			a.data('title', a.attr('title'))
			.removeAttr('title')
			.hover(
				function() { showAnchorTitle(a, '<img src="layout/icons/iconInfo2.png" width="12" class="titleInfo" height="12" alt="Info"> ' + a.data('title')); },
				function() { hideAnchorTitle(); }
			);
		}
	});
}
function showAnchorTitle(element, text) {
	var windowWidth = $(window).width();
	var offset = element.offset();
	var posLeft = offset.left + 60;
	if(offset.left > (windowWidth - 20 - parseInt($('#replaceTitleAttribute').css('width')))) { posLeft = (offset.left - parseInt($('#replaceTitleAttribute').css('width'))); }

	if(posLeft < 20)  { posLeft = 20;}

	$('#replaceTitleAttribute')
	.css({
		'top'  : (offset.top + element.outerHeight() + 4) + 'px',
		'left' : (posLeft) + 'px',
		'z-index' : 99999
	})
	.html(text)
	.show();
}
function hideAnchorTitle() {
	$('#replaceTitleAttribute').hide();
}

// ------------------------------------------------------------
function loadSalesmansAdress(salesmanID, typeAdress, triggerID) {
	if($(triggerID).attr('checked') == 'checked'){
		if(salesmanID < 1) {
			window.alert('Bitte wählen Sie erst einen Vertreter aus!');
			$(triggerID).attr('checked', false);
		}
		else {
			var loadURL = 'inc/loadAdressData.inc.php';
			loadURL += '?typeAdress=' + typeAdress + '&salesmanID=' + salesmanID;
			var salesmanAdress = '';
			$(triggerID).parent().append('<span class="loadingArea"><img class="loader" src="layout/ajax-loader.gif" /> Bitte haben Sie ein wenig Geduld. Die Daten werden geladen...</span>');
			$.get(loadURL, function(jsonData) {
				if(jsonData == '') {
					$(triggerID).attr('checked', false);
					$('.loadingArea').remove();
					window.alert('Es konnten keine Daten gefunden werden!');
				}
				else {
					var objJSON = jQuery.parseJSON(jsonData);

					if(typeAdress == 'invoice') {
						$('#editCustomersRechnungsadresseKundennummer').val(objJSON['invoice']['customerNumber']).attr('disabled', false);
						
						$('#editCustomersRechnungsadresseFirmenname').val(objJSON['invoice']['company']).attr('disabled', true);
						$('#editCustomersRechnungsadresseFirmennameZusatz').val(objJSON['invoice']['companyAdd']).attr('disabled', true);
						$('#editCustomersRechnungsadresseStrasse').val(objJSON['invoice']['street']).attr('disabled', true);
						$('#editCustomersRechnungsadresseHausnummer').val(objJSON['invoice']['number']).attr('disabled', true);
						$('#editCustomersRechnungsadresseLand').val(objJSON['invoice']['country']).attr('disabled', true);
						$('#editCustomersRechnungsadressePLZ').val(objJSON['invoice']['zipcode']).attr('disabled', true);
						$('#editCustomersRechnungsadresseOrt').val(objJSON['invoice']['city']).attr('disabled', true);
					}
					else if(typeAdress == 'delivery') {
						$('#editCustomersLieferadresseKundennummer').val(objJSON['delivery']['customerNumber']).attr('disabled', false);
						
						$('#editCustomersLieferadresseFirmenname').val(objJSON['delivery']['company']).attr('disabled', true);
						$('#editCustomersLieferadresseFirmennameZusatz').val(objJSON['delivery']['companyAdd']).attr('disabled', true);
						$('#editCustomersLieferadresseStrasse').val(objJSON['delivery']['street']).attr('disabled', true);
						$('#editCustomersLieferadresseHausnummer').val(objJSON['delivery']['number']).attr('disabled', true);
						$('#editCustomersLieferadresseLand').val(objJSON['delivery']['country']).attr('disabled', true);
						$('#editCustomersLieferadressePLZ').val(objJSON['delivery']['zipcode']).attr('disabled', true);
						$('#editCustomersLieferadresseOrt').val(objJSON['delivery']['city']).attr('disabled', true);
					}
					/*
					$('#').val(objJSON['customer']['ID']);
					$('#').val(objJSON['customer']['Number']);
					$('#').val(objJSON['customer']['Name']);

					$('#').val(objJSON['company']['street']);
					$('#').val(objJSON['company']['number']);
					$('#').val(objJSON['company']['country']);
					$('#').val(objJSON['company']['zipcode']);
					$('#').val(objJSON['company']['city']);
					*/
				}
				$(triggerID).parent().find('.loadingArea').remove();
			});
		}
	}
	else {
		if(typeAdress == 'invoice') {
			$('#editCustomersRechnungsadresseFirmenname').val('').attr('disabled', false);
			$('#editCustomersRechnungsadresseFirmennameZusatz').val('').attr('disabled', false);
			$('#editCustomersRechnungsadresseStrasse').val('').attr('disabled', false);
			$('#editCustomersRechnungsadresseHausnummer').val('').attr('disabled', false);
			$('#editCustomersRechnungsadresseLand').val('').attr('disabled', false);
			$('#editCustomersRechnungsadressePLZ').val('').attr('disabled', false);
			$('#editCustomersRechnungsadresseOrt').val('').attr('disabled', false);
		}
		else if(typeAdress == 'delivery') {
			$('#editCustomersLieferadresseFirmenname').val('').attr('disabled', false);
			$('#editCustomersLieferadresseFirmennameZusatz').val('').attr('disabled', false);
			$('#editCustomersLieferadresseStrasse').val('').attr('disabled', false);
			$('#editCustomersLieferadresseHausnummer').val('').attr('disabled', false);
			$('#editCustomersLieferadresseLand').val('').attr('disabled', false);
			$('#editCustomersLieferadressePLZ').val('').attr('disabled', false);
			$('#editCustomersLieferadresseOrt').val('').attr('disabled', false);
		}
	}
}


// ------------------------------------------------------------
function loadProductDetails(productID) {
	try {
		var animateTime = 0;
		// var screenFactor = 2;
		// var screenAddX = -300;
		// var screenAddY = 132;
		// var posX = ($(window).width() / screenFactor)  + screenAddX;
		// var posX = 184;
		// var posY = $('html').scrollTop() + screenAddY;
		// var posY = screenAddY;

		$('#loadOrderDetailsArea').remove();

		var content;
		var contentHeader = 'Produkt-Details';
		var contentText = '';

		content = '<div class="noticeBoxHeader">' + contentHeader + '</div>';
		content += '<div class="noticeBoxContents"> </div>';
		content += '<div class="noticeBoxFooter">' + '' + '</div>';
		// content += '<div class="noticeBoxClose">' + '<img src="layout/icons/iconClose.png" class="iconClose" width="14" height="14" alt="" title="Liste schlie&szlig;en" />' + '</div>';
		var arrWindowButtons = loadWindowButtons();

		content += '<div class="noticeBoxClose">' + arrWindowButtons['small'] + ' ' + arrWindowButtons['move'] + ' ' + arrWindowButtons['close'] + '</div>';
		content = '<div class="loadNoticeContent">' + content + '</div>'

		element = $('body');
		element.append('<div class="loadedWindow" id="loadOrderDetailsArea"></div>');
		$('#loadOrderDetailsArea').html(content);

		actionWindowButtons();

		$('#loadOrderDetailsArea').hide();
		// $('#loadOrderDetailsArea').css('top', posY + 'px');
		// $('#loadOrderDetailsArea').css('left', posX + 'px');
		$('#loadOrderDetailsArea').fadeIn(animateTime);
		$('.noticeBoxContents').empty().html('<span class="loadingArea"><img class="loader" src="layout/ajax-loader.gif" /> Bitte haben Sie ein wenig Geduld. Die Daten werden geladen...</span>');

		var loadURL = 'inc/loadProductDetails.inc.php';
		loadURL += '?productID=' + productID;

		$('.noticeBoxContents').load(loadURL, function(response, status, xhr){
			if (status == 'error') {
				// var msg = 'Es ist ein Fehler aufgetreten. Fehler-Code: ';
				var msg = 'Die Daten konnten nicht geladen werden. Fehler-Code: ';
				$('.noticeBoxContents').html(msg + xhr.status + ' ' + xhr.statusText);
				$('#loadOrderDetailsArea').remove();
			}
			else {

			}
		});
	}
	catch(err) { }
	finally {}
}

// ------------------------------------------------------------

function loadOrderDocumentDetails(documentID, documentName, documentType) {
	try {
		var animateTime = 0;
		var screenFactor = 2;
		// var screenAddX = -300;
		// var screenAddY = 132;
		// var posX = ($(window).width() / screenFactor)  + screenAddX;
		// var posX = 184;
		// var posY = $('html').scrollTop() + screenAddY;

		$('#loadOrderDetailsArea').remove();

		var content;
		var contentHeader = documentName + '-Details';
		var contentText = '';

		content = '<div class="noticeBoxHeader">' + contentHeader + '</div>';
		content += '<div class="noticeBoxContents"> </div>';
		content += '<div class="noticeBoxFooter">' + '' + '</div>';
		// content += '<div class="noticeBoxClose">' + '<img src="layout/icons/iconClose.png" class="iconClose" width="14" height="14" alt="" title="Liste schlie&szlig;en" />' + '</div>';
		var arrWindowButtons = loadWindowButtons();

		content += '<div class="noticeBoxClose">' + arrWindowButtons['small'] + ' ' + arrWindowButtons['move'] + ' ' + arrWindowButtons['close'] + '</div>';
		content = '<div class="loadNoticeContent">' + content + '</div>'

		element = $('body');
		element.append('<div class="loadedWindow" id="loadOrderDetailsArea"></div>');
		$('#loadOrderDetailsArea').html(content);

		actionWindowButtons();

		$('#loadOrderDetailsArea').hide();
		// $('#loadOrderDetailsArea').css('top', posY + 'px');
		// $('#loadOrderDetailsArea').css('left', posX + 'px');
		$('#loadOrderDetailsArea').fadeIn(animateTime);
		$('.noticeBoxContents').empty().html('<span class="loadingArea"><img class="loader" src="layout/ajax-loader.gif" /> Bitte haben Sie ein wenig Geduld. Die Daten werden geladen...</span>');

		var loadURL = 'inc/loadOrderDocumentDetails.inc.php';
		loadURL += '?documentID=' + documentID + '&documentType=' + documentType;

		$('.noticeBoxContents').load(loadURL, function(response, status, xhr){
			if (status == 'error') {
				// var msg = 'Es ist ein Fehler aufgetreten. Fehler-Code: ';
				var msg = 'Die Daten konnten nicht geladen werden. Fehler-Code: ';
				$('.noticeBoxContents').html(msg + xhr.status + ' ' + xhr.statusText);
				$('#loadOrderDetailsArea').remove();
			}
			else {

			}
		});
	}
	catch(err) { }
	finally {}
}

// ------------------------------------------------------------
function translate(element, x, y) {
	var translation = "translate(" + x + "px," + y + "px)";
	element.css({
		"transform": translation,
		"-ms-transform": translation,
		"-webkit-transform": translation,
		"-o-transform": translation,
		"-moz-transform": translation
	});
}
// ------------------------------------------------------------

function fixTableHeader(table) {
	if($(table + " thead").offset()){
		var delta = $(window).scrollTop() - $(table + " thead").offset().top;
		if(delta > 0){ delta = delta + 1; }
		else { delta = 0; }
		translate($(table + " > thead th"),0,delta);
		// $(table + " th").css('border', '1px solid #FF0000');
	}
}
// ------------------------------------------------------------
function addslashes(str) {
	str=str.replace(/\\/g,'\\\\');
	str=str.replace(/\'/g,'\\\'');
	str=str.replace(/\"/g,'\\"');
	str=str.replace(/\0/g,'\\0');
	return str;
}
function stripslashes(str) {
	str=str.replace(/\\'/g,'\'');
	str=str.replace(/\\"/g,'"');
	str=str.replace(/\\0/g,'\0');
	str=str.replace(/\\\\/g,'\\');
	return str;
}
// ------------------------------------------------------------
function loadGoogleMapAddresses(addressType) {
	try {
		var animateTime = 0;
		var screenFactor = 2;
		var screenAddX = -300;
		var screenAddY = 132;
		// var posX = ($(window).width() / screenFactor)  + screenAddX;
		var posX = 10;
		// var posY = $('html').scrollTop() + screenAddY;
		var posY = 10;

		$('#loadGoogleMapsArea').remove();

		var content;
		var contentHeader = 'Adressen-Karte';
		var contentText = '';

		content = '<div class="noticeBoxHeader">' + contentHeader + '</div>';
		content += '<div class="noticeBoxContents"> </div>';
		content += '<div class="noticeBoxFooter">' + '' + '</div>';
		// content += '<div class="noticeBoxClose">' + '<img src="layout/icons/iconClose.png" class="iconClose" width="14" height="14" alt="" title="Liste schlie&szlig;en" />' + '</div>';
		var arrWindowButtons = loadWindowButtons();

		content += '<div class="noticeBoxClose">' + arrWindowButtons['small'] + ' ' + arrWindowButtons['move'] + ' ' + arrWindowButtons['close'] + '</div>';
		content = '<div class="loadNoticeContent">' + content + '</div>'

		element = $('body');
		element.append('<div class="loadedWindow" id="loadGoogleMapsArea"></div>');
		$('#loadGoogleMapsArea').html(content);

		actionWindowButtons();

		$('#loadGoogleMapsArea').hide();
		$('#loadGoogleMapsArea').css('top', posY + 'px');
		$('#loadGoogleMapsArea').css('left', posX + 'px');
		$('#loadGoogleMapsArea').css('width', ($(window).width() - 40) + 'px');
		$('#loadGoogleMapsArea').css('height', ($(window).height() - 60) + 'px');

		$('#loadGoogleMapsArea').fadeIn(animateTime);

		var loadURL = 'displayGoogleMapsAddresses.php?addressType=' + addressType;

		$('.noticeBoxContents').html('<iframe id="frameGoogleMap" src="' + loadURL + '" style="border: 0px solid #EEEEEE;width:100%; height:' + ($(window).height() - 110) + 'px;"></iframe>');

		$('iframe#frameGoogleMap').ready(function() {
			$('body', $('iframe').contents()).html('<span class="loadingArea"><img class="loader" src="layout/ajax-loader.gif" /> Bitte haben Sie ein wenig Geduld. Die Daten werden geladen...</span>');
		});
	}
	catch(err) { }
	finally {}
}

// ------------------------------------------------------------
function loadGoogleMapSalesmen(salesmanID) {
	try {
		var animateTime = 0;
		var screenFactor = 2;
		var screenAddX = -300;
		var screenAddY = 132;
		// var posX = ($(window).width() / screenFactor)  + screenAddX;
		var posX = 10;
		// var posY = $('html').scrollTop() + screenAddY;
		var posY = 10;

		$('#loadGoogleMapsArea').remove();

		var content;
		var contentHeader = 'Vertreter-Karte';
		var contentText = '';

		content = '<div class="noticeBoxHeader">' + contentHeader + '</div>';
		content += '<div class="noticeBoxContents"> </div>';
		content += '<div class="noticeBoxFooter">' + '' + '</div>';
		// content += '<div class="noticeBoxClose">' + '<img src="layout/icons/iconClose.png" class="iconClose" width="14" height="14" alt="" title="Liste schlie&szlig;en" />' + '</div>';
		var arrWindowButtons = loadWindowButtons();

		content += '<div class="noticeBoxClose">' + arrWindowButtons['small'] + ' ' + arrWindowButtons['move'] + ' ' + arrWindowButtons['close'] + '</div>';
		content = '<div class="loadNoticeContent">' + content + '</div>'

		element = $('body');
		element.append('<div class="loadedWindow" id="loadGoogleMapsArea"></div>');
		$('#loadGoogleMapsArea').html(content);

		actionWindowButtons();

		$('#loadGoogleMapsArea').hide();
		$('#loadGoogleMapsArea').css('top', posY + 'px');
		$('#loadGoogleMapsArea').css('left', posX + 'px');
		$('#loadGoogleMapsArea').css('width', ($(window).width() - 40) + 'px');
		$('#loadGoogleMapsArea').css('height', ($(window).height() - 60) + 'px');

		$('#loadGoogleMapsArea').fadeIn(animateTime);
		// $('.noticeBoxContents').empty().html('<span class="loadingArea"><img class="loader" src="layout/ajax-loader.gif" /> Bitte haben Sie ein wenig Geduld. Die Daten werden geladen...</span>');
		//$('.noticeBoxContents').html('<span class="loadingArea"><img class="loader" src="layout/ajax-loader.gif" /> Bitte haben Sie ein wenig Geduld. Die Daten werden geladen...</span>');

		var loadURL = 'displayGoogleMapsSalesmen.php';

		$('.noticeBoxContents').html('<iframe id="frameGoogleMap" src="' + loadURL + '" style="border: 0px solid #EEEEEE;width:100%; height:' + ($(window).height() - 110) + 'px;"></iframe>');

		$('iframe#frameGoogleMap').ready(function() {
			$('body', $('iframe').contents()).html('<span class="loadingArea"><img class="loader" src="layout/ajax-loader.gif" /> Bitte haben Sie ein wenig Geduld. Die Daten werden geladen...</span>');
		});

	}
	catch(err) { }
	finally {}
}

// ------------------------------------------------------------
function loadGoogleMapSalesmenReportAreas(strZipcodes) {
	try {
		var animateTime = 0;
		var screenFactor = 2;
		var screenAddX = -300;
		var screenAddY = 132;
		// var posX = ($(window).width() / screenFactor)  + screenAddX;
		var posX = 10;
		// var posY = $('html').scrollTop() + screenAddY;
		var posY = 10;

		$('#loadGoogleMapsArea').remove();

		var content;
		var contentHeader = 'Vertreter-Karte';
		var contentText = '';

		content = '<div class="noticeBoxHeader">' + contentHeader + '</div>';
		content += '<div class="noticeBoxContents"> </div>';
		content += '<div class="noticeBoxFooter">' + '' + '</div>';
		// content += '<div class="noticeBoxClose">' + '<img src="layout/icons/iconClose.png" class="iconClose" width="14" height="14" alt="" title="Liste schlie&szlig;en" />' + '</div>';
		var arrWindowButtons = loadWindowButtons();

		content += '<div class="noticeBoxClose">' + arrWindowButtons['small'] + ' ' + arrWindowButtons['move'] + ' ' + arrWindowButtons['close'] + '</div>';
		content = '<div class="loadNoticeContent">' + content + '</div>'

		element = $('body');
		element.append('<div class="loadedWindow" id="loadGoogleMapsArea"></div>');
		$('#loadGoogleMapsArea').html(content);

		actionWindowButtons();

		$('#loadGoogleMapsArea').hide();
		$('#loadGoogleMapsArea').css('top', posY + 'px');
		$('#loadGoogleMapsArea').css('left', posX + 'px');
		$('#loadGoogleMapsArea').css('width', ($(window).width() - 40) + 'px');
		$('#loadGoogleMapsArea').css('height', ($(window).height() - 60) + 'px');

		$('#loadGoogleMapsArea').fadeIn(animateTime);
		// $('.noticeBoxContents').empty().html('<span class="loadingArea"><img class="loader" src="layout/ajax-loader.gif" /> Bitte haben Sie ein wenig Geduld. Die Daten werden geladen...</span>');
		//$('.noticeBoxContents').html('<span class="loadingArea"><img class="loader" src="layout/ajax-loader.gif" /> Bitte haben Sie ein wenig Geduld. Die Daten werden geladen...</span>');

		// var loadURL = 'displayGoogleMapsSalesmenReportAreas.php?strZipcodes=' + strZipcodes;
		var loadURL = 'displayGoogleMapsRoutePlan.php?strMaptype=routes&strZipcodes=' + strZipcodes;

		$('.noticeBoxContents').html('<iframe id="frameGoogleMap" src="' + loadURL + '" style="border: 0px solid #EEEEEE;width:100%; height:' + ($(window).height() - 110) + 'px;"></iframe>');

		$('iframe#frameGoogleMap').ready(function() {
			$('body', $('iframe').contents()).html('<span class="loadingArea"><img class="loader" src="layout/ajax-loader.gif" /> Bitte haben Sie ein wenig Geduld. Die Daten werden geladen...</span>');
		});

	}
	catch(err) { }
	finally {}
}

// ------------------------------------------------------------

function loadScript(pathScript) {
	var script = document.createElement("script");
	script.type = "text/javascript";
	script.src = pathScript;
	document.body.appendChild(script);
}
// ------------------------------------------------------------
function checkIfCustomerNumberAlreadyExists(element, lengthCustomerNumber) {
	// alert(element.val());
	var checkCustomerNumber = element.val();
	checkCustomerNumber = checkCustomerNumber.replace(/[a-zA-Z\-]/, "");
	$('#checkCustomerNumberWarning').remove();
	if(checkCustomerNumber.length >= lengthCustomerNumber) {

		// $('#loadOrderDetailsArea').fadeIn(animateTime);
		// $('.noticeBoxContents').empty().html('<span class="loadingArea"><img class="loader" src="layout/ajax-loader.gif" /> Bitte haben Sie ein wenig Geduld. Die Daten werden geladen...</span>');

		var loadURL = 'inc/checkCustomerNumber.inc.php';
		loadURL += '?checkCustomerNumber=' + checkCustomerNumber;;

		$.get(loadURL, function(result) {
			if(result != '') {
				element.attr('value', '');
				element.parent().append('<div id="checkCustomerNumberWarning">' + result + '</div>');
			}
		});
	}
}
// ------------------------------------------------------------

function sendAttachedDocument(triggerElement, mailDocumentData, editCustomerNumber, editID, documentType, sendFormURL, mailRecipient) {
	try {
	var arrMailDocumentData = mailDocumentData.split('#');
	var mailDocumentFilename = arrMailDocumentData[0];
	var documentType = arrMailDocumentData[1];

	var animateTime = 0;
	$('#loadMailDetailsArea').remove();
	triggerElement.parent().parent().parent().append('<div id="loadMailDetailsArea" class="loadedWindow"></div>');
	var content = '';
	var contentHeader = '&quot;<span style="color:#990000;">' + decodeURI(mailDocumentFilename).substring(0, 35) + '</span>&quot; per Mail senden an:';
	var contentText = '';
	var contentFooter = '';

	var loadURL = 'inc/loadMailDatas.inc.php';

	$.get(loadURL, function(result) {
		var arrMailTextTemplates = new Array();
		var arrMailUserSettings = new Array();
		var arrMailAddresses = new Array();
		objJson = JSON.parse(result);

		for (var index in objJson){
			if(index == "mailUserMailDatas"){
				arrMailUserSettings[index2] = new Array();
				for (var index2 in objJson[index]){
					arrMailUserSettings[index2] =  objJson[index][index2];
				};
			}
			else if(index == "mailTemplates"){
				for (var index2 in objJson[index]){
					arrMailTextTemplates[index2] = new Array();
					for (var index3 in objJson[index][index2]){
						arrMailTextTemplates[index2][index3] =  objJson[index][index2][index3];
					};
				};
			}
			else if(index == "mailAddresses"){
				for (var index2 in objJson[index]){
					arrMailAddresses[index2] = new Array();
					for (var index3 in objJson[index][index2]){
						arrMailAddresses[index2][index3] =  objJson[index][index2][index3];
					};
				};
			}

		};

		var mailtextTemplateSelection;
		mailtextTemplateSelection = '';
		mailtextTemplateSelection += '<select name="selectMailtextTemplates" id="selectMailtextTemplates" class="inputSelect_510">';
		mailtextTemplateSelection += '<option value=""> </option>';
		for(var key in arrMailTextTemplates){
			mailtextTemplateSelection += '<option value="' + key + '">' + arrMailTextTemplates[key]["OPTION"] + '</option>';
		}
		mailtextTemplateSelection += '</select>';

		if(arrMailAddresses.length > 0){
			var mailtextMailAdressSelection;
			mailtextMailAdressSelection = '';
			mailtextMailAdressSelection += '<select name="selectMailtextSender" id="selectMailtextSender" class="inputSelect_510">';
			mailtextMailAdressSelection += '<option value="">Buchhaltung</option>';
			for(var key in arrMailAddresses){
				mailtextMailAdressSelection += '<option value="' + arrMailAddresses[key]["MAIL"] + '">' + arrMailAddresses[key]["MAIL"] + ' (' + arrMailAddresses[key]["NAME"] + ')</option>';
			}
			mailtextMailAdressSelection += '</select>';
		}

		contentText += '<div class="adminEditArea">';
		contentText += '<form name="formSendAttachmentMail" method="post" action="' + sendFormURL + '" enctype="multipart/form-data">';
		contentText += '<input type="hidden" name="sendAttachedDocument" value="1" />';
		contentText += '<input type="hidden" name="editID" value="' + editID + '" />';
		contentText += '<input type="hidden" name="editCustomerNumber" value="' + editCustomerNumber + '" />';
		contentText += '<input type="hidden" name="mailDocumentFilename" value="' + (mailDocumentFilename) + '" />';
		contentText += '<input type="hidden" name="documentType" value="' + documentType + '" />';
		contentText += '<input type="hidden" name="selectSubject" value="' + documentType + '" />';
		contentText += '<fieldset style="width:600px;">';
		contentText += '<legend>Dokument mailen:</legend>';
		contentText += '<table border="0" class="orderDetails" cellpadding="0" cellspacing="0" width="100%">';

		if(arrMailAddresses.length > 0){
			contentText += '<tr>';
			contentText += '<td style="width:100px;"><b>Absender:</b></td><td>' + mailtextMailAdressSelection + '</td>';
			contentText += '</tr>';
		}

		contentText += '<tr>';
		contentText += '<td style="width:100px;"><b>Textbausteine:</b></td><td>' + mailtextTemplateSelection + '</td>';
		contentText += '</tr>';
		contentText += '<tr>';
		contentText += '<td style="width:100px;"><b>Datei:</b></td><td>' + decodeURI(mailDocumentFilename) + '</td>';
		contentText += '</tr>';
		contentText += '<tr>';
		contentText += '<td><b>Empf&auml;nger:</b></td><td><input type="text" name="selectCustomersRecipientMail" id="selectCustomersRecipientMail" class="inputField_510" value="' + mailRecipient + '" /></td>';
		contentText += '</tr>';

		contentText += '<tr>';
		contentText += '<td><b>Betreff:</b></td><td><input type="text" name="sendAttachedMailSubject" id="sendAttachedMailSubject" class="inputField_510" value="' + arrMailTextTemplates["DEFAULT"]["SUBJECT"] + ' " /></td>';
		contentText += '</tr>';

		contentText += '<tr>';
		contentText += '<td><b>Text:</b></td><td><textarea name="sendAttachedMailText" id="sendAttachedMailText" class="inputTextarea_510x140">' + arrMailTextTemplates["DEFAULT"]["TEXT"] + '</textarea></td>';
		contentText += '</tr>';

		contentText += '</table>';
		contentText += '<div><input type="submit" class="inputButton1" name="submitSendAttachmentMail" value="Dokument versenden" /></div>';
		contentText += '</fieldset>';
		contentText += '</form>';
		contentText += '</div>';
		$('#loadMailDetailsArea').slideDown(animateTime);

		content = '<div class="noticeBoxHeader">' + contentHeader + '</div>';
		content += '<div class="noticeBoxContents" style="height:340px;">' + contentText + '</div>';
		content += '<div class="noticeBoxFooter">' + contentFooter + '</div>';
		// content += '<div class="noticeBoxClose">' + '<img src="layout/icons/iconClose.png" class="iconClose" width="14" height="14" alt="" title="Liste schlie&szlig;en" />' + '</div>';
		var arrWindowButtons = loadWindowButtons();

		content += '<div class="noticeBoxClose">' + arrWindowButtons['small'] + ' ' + arrWindowButtons['move'] + ' ' + arrWindowButtons['close'] + '</div>';

		content = '<div class="loadNoticeContent">' + content + '</div>';
		$('#loadMailDetailsArea').html(content);

		actionWindowButtons();

		$('#selectMailtextTemplates').val(documentType);
		$('#sendAttachedMailText').val(arrMailTextTemplates[documentType]["TEXT"]);
		$('#sendAttachedMailSubject').val(arrMailTextTemplates[documentType]["SUBJECT"]);
		$('#selectMailtextTemplates').change(function(){
			$('#sendAttachedMailText').val(arrMailTextTemplates[$(this).val()]["TEXT"]);
			$('#sendAttachedMailSubject').val(arrMailTextTemplates[$(this).val()]["SUBJECT"]);
		});
	});
	}
	catch(err) { alert(err); }
	finally {}
}

// ------------------------------------------------------------
function setInvoicePaymentStatus(triggerElement, documentData, sendFormURL, jsonStatusTypes, jsonBankAccounts) {

	var arrDocumentData = documentData.split('#');
	var documentNumber = arrDocumentData[0];
	var documentID = arrDocumentData[1];
	var documentStatus = arrDocumentData[2];
	var documentType =  documentNumber.substring(0, 2);
	var documentTypeName = '';
	if(documentType == 'RE') { documentTypeName = 'Rechnung' }
	else if(documentType == 'AB') { documentTypeName = 'Auftragsbest&auml;tigung' }
	else if(documentType == 'GU') { documentTypeName = 'Gutschrift' }
	// else if(documentType == 'AN') { documentTypeName = 'Angebot' }
	var documentTotalPrice = arrDocumentData[3];
	var documentPaidPrice = arrDocumentData[4];
	if(documentPaidPrice == '' || documentPaidPrice == null || documentPaidPrice == 'undefinded'){
		documentPaidPrice = '0';
	}
	var documentPriceToPay = documentTotalPrice;

	var tempTotalPrice = (documentTotalPrice.replace(',', '.'));
	tempTotalPrice = parseFloat(tempTotalPrice);

	var tempPaidPrice = documentPaidPrice.replace(',', '.');
	var tempPaidPrice = parseFloat(tempPaidPrice);

	var tempPriceToPay = tempTotalPrice - tempPaidPrice;
	tempPriceToPay = tempPriceToPay.toFixed(2);
	documentPriceToPay = tempPriceToPay;

	var displayDocumentPriceToPay = documentPriceToPay.replace('.', ',');
	var displayDocumentPaidPrice = documentPaidPrice.replace('.', ',');

	var animateTime = 500;
	$('#loadOrderDetailsArea').remove();
	$('#mainArea').append('<div class="loadedWindow" id="loadOrderDetailsArea"></div>');

	var animateTime = 0;
	var screenFactor = 2;
	var screenAddX = -300;
	var screenAddY = 132;
	// var posX = ($(window).width() / screenFactor)  + screenAddX;
	var posX = 184;
	// var posY = $('html').scrollTop() + screenAddY;
	posY = 100;

	var content = '';
	var contentHeader = 'Status der ' + documentTypeName + ' &quot;<span style="color:#990000;">' + documentNumber + '</span>&quot; bearbeiten:';
	var contentText = '';
	var contentFooter = '';

	var today = new Date();
	var dd = today.getDate();
	var mm = today.getMonth() + 1; //January is 0!

	var yyyy = today.getFullYear();
	if(dd < 10){ dd = '0' + dd; }
	if(mm < 10){ mm = '0' + mm; }
	today = dd + '.' + mm + '.' + yyyy;

	contentText += '<div class="adminEditArea">';
	contentText += '<form name="formSetInvoicePaymentStatus" method="post" action="' + sendFormURL + '" enctype="multipart/form-data">';
	contentText += '<input type="hidden" name="setInvoicePaymentStatus" value="1" />';
	contentText += '<input type="hidden" name="documentID" value="' + documentID + '" />';
	contentText += '<input type="hidden" name="documentNumber" value="' + documentNumber + '" />';
	contentText += '<input type="hidden" name="documentType" value="' + documentType + '" />';
	contentText += '<fieldset>';
	contentText += '<legend>Status der ' + documentTypeName + ' setzen:</legend>';
	contentText += '<table border="0" class="orderDetails" cellpadding="0" cellspacing="0" width="100%">';
	contentText += '<tr>';
	contentText += '<td style="width:100px;"><b>' + documentTypeName + ':</b></td><td>' + documentNumber + '</td>';
	contentText += '</tr>';
	contentText += '<tr>';
	contentText += '<td style="width:100px;"><b>Status:</b></td>';
	contentText += '<td>';
	contentText += '<select name="selectInvoicePaymentStatus" id="selectInvoicePaymentStatus" class="inputSelect_200">';
	var paymentStatusOptions = new Array();
	for (var index in jsonStatusTypes){
		// alert(jsonStatusTypes[index].fieldName);
		// alert(jsonStatusTypes[index].fieldLabel);
		// thisElementType = form.elements[jsonStatusTypes[index].fieldName].type;
		// contentText += '<option value="' + index + '">' + jsonStatusTypes[index].paymentStatusTypesName + '</option>';
		// contentText += '<option value="' + index + '">' + jsonStatusTypes[index].paymentStatusTypesName + '</option>';
		// contentText += '<option value="' + index + '">' + jsonStatusTypes[index].paymentStatusTypesName + '</option>';
		// alert(jsonStatusTypes[index].paymentStatusTypesSort);
		paymentStatusOptions[jsonStatusTypes[index].paymentStatusTypesSort] = '<option value="' + index + '">' + jsonStatusTypes[index].paymentStatusTypesName + '</option>';
	}

	if(paymentStatusOptions.length > 0){
		for (var index in paymentStatusOptions){
			contentText += paymentStatusOptions[index];
		}
	}

	contentText += '</select>';
	contentText += '</td>';
	contentText += '</tr>';
	contentText += '</table>';
	contentText += '</fieldset>';

	contentText += '<fieldset>';
	contentText += '<legend>Bezahlten Teilbetrag eintragen:</legend>';
	contentText += '<p class="infoArea">';
	contentText += 'Bisher wurden <b>' + displayDocumentPaidPrice + ' &euro;</b> von <b>' + documentTotalPrice + ' &euro;</b> bezahlt!';
	if(tempPriceToPay > 0){
		contentText += ' Es sind noch <b>' + displayDocumentPriceToPay + ' &euro;</b> zu zahlen!';
	}
	else if(tempPriceToPay < 0){
		// documentPriceToPay = Math.abs(documentPriceToPay);
		displayDocumentPriceToPay = documentPriceToPay.replace('.', ',');
		contentText += ' Es wurden <b>' + displayDocumentPriceToPay + ' &euro;</b> zuviel bezahlt!';
	}
	else if(tempPriceToPay == 0){
		contentText += ' Die Rechnung ist ausgeglichen!';
	}
	contentText += '</p>';
	contentText += '<table border="0" class="orderDetails" cellpadding="0" cellspacing="0" width="">';
	contentText += '<tr>';
	contentText += '<td style="width:100px;"><b>Zahlungsbetrag:</b></td>';
	contentText += '<td style="white-space:nowrap;">';
	contentText += '<input type="text" name="setInvoicePaymentPaidValue" id="setInvoicePaymentPaidValue" class="inputField_60" value="' + documentPriceToPay + '" />';
	contentText += ' <span class="buttonClearField"><img src="layout/icons/iconDelete.png" width="16" height="16" alt="" title="Eingabefeld leeren" onclick="$(\'#setInvoicePaymentPaidValue\').val(0);" /></div>';
	contentText += '</td>';
	contentText += '<td style="width:10px;"> </td>';
	contentText += '<td><b>Skonto:</b></td>';
	contentText += '<td>';
	contentText += '<input type="text" name="setInvoicePaymentSkontoValue" id="setInvoicePaymentSkontoValue" class="inputField_40" value="0,00" />';
	contentText += '</td>';
	contentText += '<td style="width:10px;"> </td>';
	contentText += '<td style="width:100px;"><b>Eingangsdatum:</b></td>';
	contentText += '<td>';
	contentText += '<input type="text" name="setInvoicePaymentDate" id="setInvoicePaymentDate" class="inputField_70" value="' + today + '" />';
	contentText += '</td>';

	contentText += '<td style="width:20px;"> </td>';
	contentText += '<td style="width:100px;"><b>Empf&auml;nger-Bank:</b></td>';
	contentText += '<td>';
	contentText += '<select name="setInvoicePaymentBankAccount" id="setInvoicePaymentBankAccount" class="inputSelect_205">';
	contentText += '<option value=""> --- Bitte w&auml;hlen --- </option>';
	for (var index in jsonBankAccounts){
		contentText += '<option value="' + index + '">' + jsonBankAccounts[index].bankAccountTypesNotice + ' ' + jsonBankAccounts[index].bankAccountTypesName + ' (' + jsonBankAccounts[index].bankAccountTypesAccountNumber + ')</option>';
	}

	contentText += '</select>';
	contentText += '</td>';

	contentText += '</tr>';
	contentText += '<tr>';
	contentText += '<td style="width:100px;"><b>Notiz:</b></td>';
	contentText += '<td colspan=6"><textarea name="setInvoicePaymentNotice" id="setInvoicePaymentNotice" class="inputTextarea_300x40"></textarea>';
	contentText += '</td>';
	contentText += '</tr>';
	contentText += '</table>';
	contentText += '</fieldset>';
	contentText += '<br /><br /><div><input type="submit" class="inputButton1" name="submitSetInvoicePaymentStatus" value="Daten speichern" /></div><br />';
	contentText += '</form>';
	contentText += '</div>';

	contentText += '<div id="loadPaymentDetailsArea"></div>';

	content = '<div class="noticeBoxHeader">' + contentHeader + '</div>';
	content += '<div class="noticeBoxContents">' + contentText + '</div>';
	content += '<div class="noticeBoxFooter">' + contentFooter + '</div>';
	// content += '<div class="noticeBoxClose">' + '<img src="layout/icons/iconClose.png" class="iconClose" width="14" height="14" alt="" title="Liste schlie&szlig;en" />' + '</div>';
	var arrWindowButtons = loadWindowButtons();

	content += '<div class="noticeBoxClose">' + arrWindowButtons['small'] + ' ' + arrWindowButtons['move'] + ' ' + arrWindowButtons['close'] + '</div>';

	content = '<div class="loadNoticeContent">' + content + '</div>';

	$('#loadOrderDetailsArea').hide();
	$('#loadOrderDetailsArea').css('top', posY + 'px');
	$('#loadOrderDetailsArea').css('left', posX + 'px');
	$('#loadOrderDetailsArea').fadeIn(animateTime);
	$('#loadOrderDetailsArea').html(content);

	actionWindowButtons();

	$('#loadOrderDetailsArea').slideDown(animateTime);

	$('#selectInvoicePaymentStatus').val(documentStatus);
	$.datepicker.setDefaults($.datepicker.regional[""]);
	$('#setInvoicePaymentDate').datepicker($.datepicker.regional["de"]);
	$('#setInvoicePaymentDate').datepicker("option", "maxDate", "0" );

	// BOF LOAD PAYMENT DETAILS
		var linkPath = sendFormURL;
		var searchString = documentNumber;
		if(searchString != ''){
			var loadURL = 'inc/loadPaymentDetails.inc.php';
			loadURL += '?searchString=' + searchString;
			loadURL += '&linkPath=' + linkPath;

			$('#loadPaymentDetailsArea').empty().html('<span class="loadingArea"><img class="loader" src="layout/ajax-loader.gif" /> Bitte haben Sie ein wenig Geduld. Die Daten werden geladen...</span>');
			$('#loadPaymentDetailsArea').load(loadURL, function(response, status, xhr){
				if (status == 'error') {
					var msg = 'Die Daten konnten nicht geladen werden. Fehler-Code: ';
					$('#loadPaymentDetailsArea').html(msg + xhr.status + ' ' + xhr.statusText);
				}
				else {
					if($('#loadPaymentDetailsArea').html() != '') {
						$('#loadPaymentDetailsArea').fadeIn(animateTime);
					}
					else {
						//$('#loadPaymentDetailsArea').remove();
						$('#loadPaymentDetailsArea .loadingArea').remove();
					}
				}
			});
		}
	// EOF LOAD PAYMENT DETAILS
}

// ------------------------------------------------------------

function selectRelatedArtikel(mainProductFieldID, relatedProductFieldID, relatedProductFieldQuantity){
	try {
		var animateTime = 0;
		var mainProductID = $(mainProductFieldID).val();
		var relatedProductQuantity = $(relatedProductFieldQuantity).val();
		if(isNaN(relatedProductQuantity)){relatedProductQuantity = 0;}

		if(mainProductID != '0' && relatedProductQuantity > 0) {
			var searchString = mainProductID;
			if(searchString != '') {
				var loadURL = 'inc/loadRelatedProducts.inc.php';
				loadURL += '?searchString=' + searchString;
				$.get(loadURL, function(result) {
					// $(formElementSet).attr('value', result);
					$(relatedProductFieldID).val(result);
				});
			}
		}
		else {
			$(relatedProductFieldID).val(0);
		}
	}
	catch(err) { }
	finally {}
}
// ------------------------------------------------------------
function searchMailAddress(triggerElement){
	var animateTime = 0;

	try {
		var searchString = $(triggerElement).val();
		var	searchMode = 'searchMailAddress';

		$('#suggestionsMailAddress').remove();
		if(searchString != '' && searchString.length > 1) {
			var loadURL = 'inc/loadSuggestions.inc.php';
			loadURL += '?type=' + searchMode;
			loadURL += '&searchString=' + searchString;
			var content = '';
			content += '<div id="suggestionsMailAddress">';
			content += '<div class="noticeBoxClose">' + '<img src="layout/icons/iconClose.png" class="iconClose" width="14" height="14" alt="" title="Liste schlie&szlig;en" />' + '</div>';
			content += '<div id="suggestionsMailAddressContent">';
			content += '</div>';
			content += '</div>';

			$(triggerElement).parent().append(content);

			$('#suggestionsMailAddressContent').empty().html('<span class="loadingArea"><img class="loader" src="layout/ajax-loader.gif" /> Bitte haben Sie ein wenig Geduld. Die Daten werden geladen...</span>');
			$('#suggestionsMailAddressContent').load(loadURL, function(response, status, xhr){
				if (status == 'error') {
					// var msg = 'Es ist ein Fehler aufgetreten. Fehler-Code: ';
					var msg = 'Die Daten konnten nicht geladen werden. Fehler-Code: ';
					$('#suggestionsMailAddressContent').html(msg + xhr.status + ' ' + xhr.statusText);
					$('#suggestionsMailAddress').remove();
				}
				else {
					if($('#suggestionsMailAddressContent').html() != '') {
						$('.searchResultItem').click(function(){
							var searchValueMailAddress = $(this).text();
							$(triggerElement).val(searchValueMailAddress);
							$('#suggestionsMailAddress').remove();
						});
					}
				}
			});
		}
		$('#suggestionsMailAddress .iconClose').click(function(){
			$('#suggestionsMailAddress').fadeOut(animateTime, function(){
				$('#suggestionsMailAddress').remove()
			});
		});
	}
	catch(err) { }
	finally {}
}

// ------------------------------------------------------------

function loadWindowButtons() {
	var arrWindowButtons = new Array();
	arrWindowButtons['close'] = '<img src="layout/icons/iconClose.png" class="iconClose" width="14" height="14" alt="" title="Liste schlie&szlig;en" />';
	arrWindowButtons['move'] = '<img src="layout/icons/iconMove.png" class="iconMove" width="14" height="14" alt="" title="Fenster verschieben" />';
	arrWindowButtons['small'] = '<img src="layout/icons/iconSmallWindow.png" class="iconSmallWindow" width="14" height="14" alt="" title="Fenster minimieren" />';
	arrWindowButtons['big'] = '<img src="layout/icons/iconBigWindow.png" class="iconBigWindow" width="14" height="14" alt="" title="Fenster maximieren" />';

	return arrWindowButtons;
}

// ------------------------------------------------------------

function actionWindowButtons(){
	var animateTime = 'fast';
	var loadedWindowWidth;
	/*
	$('.loadedWindow').draggable({
            drag: function(event, ui) {
				$('.loadedWindow').css('cursor', 'move');
			}
        });
	*/
	$('.loadedWindow .noticeBoxHeader').live('mousedown', function(){
		// $('.loadedWindow').draggable();
		// $('.loadedWindow').css('cursor', 'move');
	});
	// $('.loadedWindow .iconMove').live('mouseup', function(){
		// $('.loadedWindow').draggable('destroy');
	// });
	$('.loadedWindow .iconClose').live('click', function(){
		$('.loadedWindow').fadeOut(animateTime, function(){
			$('.loadedWindow').remove();
		});
	});
	$('.iconSmallWindow').live('click', function(){
		$(this).attr('src', 'layout/icons/iconBigWindow.png');
		$('.loadedWindow').css('height', '40px');

		loadedWindowWidth = $('.loadedWindow').width();
		$('.loadedWindow').width('300');

		$('.loadedWindow .noticeBoxContents').css('display', 'none');
		$('.loadedWindow .noticeBoxFooter').css('display', 'none');

		$(this).removeClass('iconSmallWindow');
		$(this).addClass('iconBigWindow');

	});
	$('.iconBigWindow').live('click', function(){
		$('.loadedWindow').css('height', 'auto');
		$('.loadedWindow').width(loadedWindowWidth);

		$('.loadedWindow .noticeBoxContents').css('display', '');
		$('.loadedWindow .noticeBoxFooter').css('display', '');

		$(this).removeClass('iconBigWindow');
		$(this).addClass('iconSmallWindow');
		$('.noticeBoxHeader').mousedown(function(){
			$('.loadedWindow').draggable();
			$(this).css('cursor', 'move');
		});
		$('.noticeBoxHeader').mouseup(function(){
			$('.loadedWindow').draggable('destroy');
			$(this).css('cursor', '');
		});
	});
}

// ------------------------------------------------------------

function createGraph(canvasID, graphDatas, graphLabels, graphType, graphColor){
	/*
	var bar = new RGraph.Bar('cvs', dataInvoices);
	bar.Set('labels', labelsCash);
	bar.Set('labels.above', true);
	bar.Draw();
	*/

	if(graphType == '') {graphType = 'Line'; }
	if(graphType == 'Line') {
		var line = new RGraph.Line(canvasID, graphDatas);
		line.Set('chart.labels', graphLabels);
		line.Set('chart.text.size', 5);
		line.Set('labels.above', true);
		line.Set('chart.units.post', '');
		line.Set('colors', ['#FFD600']);
		line.Set('chart.gutter.left', 50);
		line.Set('shadow', true);
		line.Set('shadow.offsetx', 2);
		line.Set('shadow.offsety', 2);
		line.Set('shadow.blur', 4);
		line.Set('chart.linewidth', 1);
		line.Set('chart.colors.alternate', true);
		line.Set('chart.crosshairs', false);
		line.Set('chart.crosshairs.color', '#990000');
		line.Set('chart.crosshairs.hline', false);
		line.Set('chart.crosshairs.vline', false);
		line.Set('chart.colors', graphColor);
		line.Draw();
	}
}



// ------------------------------------------------------------

function number_format(num,dig,dec,sep) {
	x = new Array();
	s = (num<0?"-":"");
	num = Math.abs(num).toFixed(dig).split(".");
	r = num[0].split("").reverse();
	for(var i=1;i<=r.length;i++){
		x.unshift(r[i-1]);
		if(i%3==0&&i!=r.length){
			x.unshift(sep);
		}
	}
	return s+x.join("")+(num[1]?dec+num[1]:"");
}
// ------------------------------------------------------------
function loadFormSubmitPrintProduction(triggerElement, sendFormURL, mailRecipient){
	var thisOrderData = triggerElement.attr('rel');
	var arrTemp = thisOrderData.split("|");
	var thisOrderID = arrTemp[0];
	var editCustomerNumber = arrTemp[1];
	var editStatusIDStored = arrTemp[2];
	var editStatusIDChange = arrTemp[3];
	var thisOrderQuantity = arrTemp[4];
	var thisOrderMandatory = arrTemp[5];
	var thisOrderTransport = arrTemp[6];
	var thisOrderProductName = arrTemp[7];
	var thisOrderPrintPlateNumber = arrTemp[8];
	var editID = '';
	var mailDocumentFilename = '';
	var documentType = 'PA';

	try {

	var animateTime = 0;
	$('#loadMailDetailsArea').remove();
	triggerElement.parent().parent().parent().append('<div id="loadMailDetailsArea" class="loadedWindow"></div>');
	var content = '';
	var contentHeader = '&quot;<span style="color:#990000;">Produktionsauftrag KNR: ' + editCustomerNumber + '</span>&quot; per Mail senden an:';
	var contentText = '';
	var contentFooter = '';

	var loadURL = 'inc/loadMailDatas.inc.php';

	$.get(loadURL, function(result) {
		var arrMailTextTemplates = new Array();
		var arrMailUserSettings = new Array();
		var arrMailAddresses = new Array();
		objJson = JSON.parse(result);

		for (var index in objJson){
			if(index == "mailUserMailDatas"){
				arrMailUserSettings[index2] = new Array();
				for (var index2 in objJson[index]){
					arrMailUserSettings[index2] = objJson[index][index2];
				};
			}
			else if(index == "mailTemplates"){
				for (var index2 in objJson[index]){
					arrMailTextTemplates[index2] = new Array();
					for (var index3 in objJson[index][index2]){
						arrMailTextTemplates[index2][index3] = objJson[index][index2][index3];
					};
				};
			}
			else if(index == "mailAddresses"){
				for (var index2 in objJson[index]){
					arrMailAddresses[index2] = new Array();
					for (var index3 in objJson[index][index2]){
						arrMailAddresses[index2][index3] = objJson[index][index2][index3];
					};
				};
			}

		};

		var mailtextTemplateSelection;
		mailtextTemplateSelection = '';
		mailtextTemplateSelection += '<select name="selectMailtextTemplates" id="selectMailtextTemplates" class="inputSelect_510">';
		mailtextTemplateSelection += '<option value=""> </option>';
		for(var key in arrMailTextTemplates){
			mailtextTemplateSelection += '<option value="' + key + '">' + arrMailTextTemplates[key]["OPTION"] + '</option>';
		}
		mailtextTemplateSelection += '</select>';

		if(arrMailAddresses.length > 0){
			var mailtextMailAdressSelection;
			mailtextMailAdressSelection = '';
			mailtextMailAdressSelection += '<select name="selectMailtextSender" id="selectMailtextSender" class="inputSelect_510">';
			mailtextMailAdressSelection += '<option value="">Buchhaltung</option>';
			for(var key in arrMailAddresses){
				mailtextMailAdressSelection += '<option value="' + arrMailAddresses[key]["MAIL"] + '">' + arrMailAddresses[key]["MAIL"] + ' (' + arrMailAddresses[key]["NAME"] + ')</option>';
			}
			mailtextMailAdressSelection += '</select>';
		}

		// contentText += '<b>Artikel: ' + thisOrderProductName + '</b>';
		contentText += '<div class="adminEditArea">';
		contentText += '<form name="formSendAttachmentMail" method="post" action="' + sendFormURL + '" enctype="multipart/form-data">';
		contentText += '<input type="hidden" name="sendAttachedDocument" value="1" />';
		contentText += '<input type="hidden" name="editID" value="' + editID + '" />';
		contentText += '<input type="hidden" name="orderID" value="' + thisOrderID + '" />';
		contentText += '<input type="hidden" name="editCustomerNumber" value="' + editCustomerNumber + '" />';
		contentText += '<input type="hidden" name="editStatusIDStored" value="' + editStatusIDStored + '" />';
		contentText += '<input type="hidden" name="editStatusIDChange" value="' + editStatusIDChange + '" />';
		contentText += '<input type="hidden" name="documentType" value="' + documentType + '" />';
		contentText += '<input type="hidden" name="selectSubject" value="' + documentType + '" />';
		contentText += '<fieldset style="width:600px;">';
		contentText += '<legend style="margin:2px 0 2px 0px;padding:0 2px 0 2px">Dokument mailen:</legend>';
		contentText += '<table border="0" class="orderDetails" cellpadding="0" cellspacing="0" width="100%">';

		if(arrMailAddresses.length > 0){
			contentText += '<tr>';
			contentText += '<td style="width:100px;"><b>Absender:</b></td><td>' + mailtextMailAdressSelection + '</td>';
			contentText += '</tr>';
		}

		contentText += '<tr>';
		contentText += '<td style="width:100px;"><b>Textbausteine:</b></td><td>' + mailtextTemplateSelection + '</td>';
		contentText += '</tr>';
		contentText += '<tr>';
		contentText += '<td style="width:100px;"><b>AI-Datei:</b></td><td><b style="color:#C00">' + thisOrderProductName + '</b><br /><input type="file" name="sendAttachedMailFile" id="sendAttachedMailFile" class="inputField_510" /></td>';
		contentText += '</tr>';

		contentText += '<tr>';
		contentText += '<td style="width:100px;"><b>Infos:</b></td>';
		contentText += '<td>';
		contentText += '<div class="inputArea1">';
		contentText += '<span class="checkBoxElement1">';
		var checkedTransport = '';
		if(thisOrderTransport == '1'){
			var checkedTransport = ' checked="checked" ';
		}
		contentText += '<input type="checkbox" name="sendAttachedMailRedDot" value="1" ' + checkedTransport + ' /> <img src="layout/icons/redDot.png" width="14" height="14" alt="Hinten LKW (Nachnahme)" title="Hinten LKW (Nachnahme)" /> <b>Hinten LKW (Nachnahme)</b>';
		contentText += '</span>';
		
		contentText += '<span style="padding-left:20px;"><b>Mandant:</b> <input type="text" name="sendAttachedMailMandatory" id="sendAttachedMailMandatory" readonly="readonly" class="inputField_50" style="font-weight:bold;text-align:right;" value="' + thisOrderMandatory + '" /></span>';
		
		contentText += '<span class="checkBoxElement1">';
		var checkedUseNoNeutralPacking = '';
		var thisUseNoNeutralPacking = '';
		if(thisUseNoNeutralPacking == '1'){
			var checkedUseNoNeutralPacking = ' checked="checked" ';
		}
		contentText += '<input type="checkbox" name="sendAttachedMailUseNoNeutralPacking" value="1" ' + checkedUseNoNeutralPacking + ' /> <img src="layout/icons/greenDot.png" width="14" height="14" alt="Mit Werbung (BURHAN)" title="Mit Werbung (BURHAN)" /> <b>Mit Werbung (BURHAN)</b>';
		contentText += '</span>';
		
		contentText += '</div>';		
		contentText += '</td>';
		contentText += '</tr>';		

		contentText += '<tr>';
		contentText += '<td style="width:100px;"><b>Menge:</b></td>';
		contentText += '<td>';
		contentText += '<input type="text" name="sendAttachedMailNewQuantity" value="' + thisOrderQuantity + '" class="inputField_50" style="font-weight:bold;text-align:right;" />';
		contentText += ' Stck. in TR drucken';
		contentText += '</td>';
		contentText += '</tr>';
		
		contentText += '<tr>';
		contentText += '<td style="width:100px;"><b>Klischee-Nr:</b></td>';
		contentText += '<td>';
		contentText += '<input type="text" name="sendAttachedMailPrintingPlateNumber" value="' + thisOrderPrintPlateNumber + '" class="inputField_50" style="font-weight:bold;" />';
		contentText += ' bei erhabenen Kennzeichenhaltern angeben, wenn vorhanden!';
		contentText += '</td>';
		contentText += '</tr>';

		contentText += '<tr>';
		contentText += '<td style="width:100px;"><b>Sprache:</b></td><td>';
		contentText += '<select name="sendAttachedMailLanguage" id="sendAttachedMailLanguage" class="inputSelect_510">';
		contentText += '<option value="DE">deutsch</option>';
		contentText += '<option value="TR" selected="selected">türkisch</option>';
		contentText += '</select>';
		contentText += '</td>';
		contentText += '</tr>';

		contentText += '<tr>';
		contentText += '<td><b>Empf&auml;nger:</b></td><td><input type="text" name="selectCustomersRecipientMail" id="selectCustomersRecipientMail" class="inputField_510" value="' + mailRecipient + '" /></td>';
		contentText += '</tr>';

		contentText += '<tr>';
		contentText += '<td><b>Betreff:</b></td><td><input type="text" name="sendAttachedMailSubject" id="sendAttachedMailSubject" class="inputField_510" value="' + arrMailTextTemplates["DEFAULT"]["SUBJECT"] + ' " /></td>';
		contentText += '</tr>';

		contentText += '<tr>';
		contentText += '<td><b>eMail-Text:</b></td><td><textarea name="sendAttachedMailText" id="sendAttachedMailText" class="inputTextarea_510x60">' + arrMailTextTemplates["DEFAULT"]["TEXT"] + '</textarea></td>';
		contentText += '</tr>';

		contentText += '</table>';
		contentText += '<div><input type="submit" class="inputButton1" name="submitSendAttachmentMail" value="Dokument versenden" /></div>';
		contentText += '</fieldset>';
		contentText += '</form>';
		contentText += '</div>';
		$('#loadMailDetailsArea').slideDown(animateTime);

		content = '<div class="noticeBoxHeader">' + contentHeader + '</div>';
		content += '<div class="noticeBoxContents" style="height:420px !important;">' + contentText + '</div>';
		content += '<div class="noticeBoxFooter">' + contentFooter + '</div>';
		// content += '<div class="noticeBoxClose">' + '<img src="layout/icons/iconClose.png" class="iconClose" width="14" height="14" alt="" title="Liste schlie&szlig;en" />' + '</div>';
		var arrWindowButtons = loadWindowButtons();

		content += '<div class="noticeBoxClose">' + arrWindowButtons['small'] + ' ' + arrWindowButtons['move'] + ' ' + arrWindowButtons['close'] + '</div>';

		content = '<div class="loadNoticeContent">' + content + '</div>';
		$('#loadMailDetailsArea').html(content);

		actionWindowButtons();

		$('#selectMailtextTemplates').val(documentType);
		$('#sendAttachedMailText').val(arrMailTextTemplates[documentType]["TEXT"]);
		$('#sendAttachedMailSubject').val(arrMailTextTemplates[documentType]["SUBJECT"]);
		$('#selectMailtextTemplates').change(function(){
			$('#sendAttachedMailText').val(arrMailTextTemplates[$(this).val()]["TEXT"]);
			$('#sendAttachedMailSubject').val(arrMailTextTemplates[$(this).val()]["SUBJECT"]);
		});
	});
	}
	catch(err) { alert(err); }
	finally {}
}
// ------------------------------------------------------------

function checkCustomerData(formName, jsonData) {
	var returnValue = false;
	var customerAlreadyExists = false;
	var customerCheckFormDutyFields = false;
		
	customerCheckFormDutyFields = checkFormDutyFields(formName, jsonData);	
	if(customerCheckFormDutyFields == true){
		customerAlreadyExists = checkIfCustomerAlreadyExists(formName);		
	}
	return returnValue;
}

function checkIfCustomerAlreadyExists(formName) {	
	var animateTime = 0;
	try {
	$('#searchResultArea').remove();
	var triggerElement = '#formEditCustomerDatas';
	// $(triggerElement).parent().css('position', 'relative');
	$(triggerElement).append('<div id="searchResultArea" style="position:fixed;width:900px;top:100px;left:20%;height:auto;min-height:200px;"></div>');
	$('#searchResultArea').slideDown(animateTime);
	
	var objCompareData = '{' + 	
		'"searchCompanyName": "' + $('#editCustomersFirmenname').val() + '",' +
	
		'"searchCompanyZipcode": "' + $('#editCustomersFirmenadressePLZ').val() + '",' +
		'"searchCompanyCity": "' + $('#editCustomersFirmenadresseOrt').val() + '",' +
		'"searchCompanyCountry": "' + $('#editCustomersFirmenadresseLand').val() + '",' +
		'"searchCompanyStreet": "' + $('#editCustomersFirmenadresseStrasse').val() + '",' +
		'"searchCompanyStreetNumber": "' + $('#editCustomersFirmenadresseHausnummer').val() + '",' +
		
		'"searchCompanyMail1": "' + $('#editCustomersMail1').val() + '",' +
		'"searchCompanyMail2": "' + $('#editCustomersMail2').val() + '",' +
		
		'"searchCompanyPhone1": "' + $('#editCustomersTelefon1').val() + '",' +
		'"searchCompanyPhone2": "' + $('#editCustomersTelefon2').val() + '",' +
		
		'"searchCompanyFax1": "' + $('#editCustomersFax1').val() + '",' +
		'"searchCompanyFax2": "' + $('#editCustomersFax2').val() + '",' +
		
		'"searchCompanyMobil1": "' + $('#editCustomersMobil1').val() + '",' +
		'"searchCompanyMobil2": "' + $('#editCustomersMobil2').val() + '"' + 
	'}';
	
	var loadURL = 'inc/checkIfCustomerAlreadyExists.inc.php';
	loadURL += '?objCompareData=' + encodeURI(objCompareData);
	
	var content = '';
	var contentHeader = 'Gefundene Duplikate';
	var contentText = '';
	var contentFooter = '';
	content = '<div class="noticeBoxHeader">' + contentHeader + '</div>';
	content += '<div class="noticeBoxContents" style="height:auto;overflow:visible;">' + contentText + '</div>';
	content += '<div class="noticeBoxFooter">' + contentFooter + '</div>';
	// content += '<div class="noticeBoxClose">' + '<img src="layout/icons/iconClose.png" class="iconClose" width="14" height="14" alt="" title="Liste schlie&szlig;en" />' + '</div>';
	content = '<div class="loadNoticeContent">' + content + '</div>';

	$('#searchResultArea').html(content);
	$('.noticeBoxContents').empty().html('<span class="loadingArea" style="font-weight:bold;font-size:18px;"><img class="loader" src="layout/ajax-loader.gif" /> Bitte haben Sie ein wenig Geduld. Die Daten werden auf Duplikate geprueft...</span>');
	
	$('.noticeBoxContents').load(loadURL, function(response, status, xhr){		
		if (status == 'error') {
			// var msg = 'Es ist ein Fehler aufgetreten. Fehler-Code: ';
			var msg = 'Die Daten konnten nicht geladen werden. Fehler-Code: ';
			$('.noticeBoxContents').html(msg + xhr.status + ' ' + xhr.statusText);
			$('#searchResultArea').remove();					
		}
		else {
			if($('.noticeBoxContents').html() != '') {
				
			}
			else {
				$('#searchResultArea').remove();
				$(triggerElement).append('<input type="hidden" name="storeDatas" value="1" />');
				document.forms[formName].submit();
			}
		}		
	});
	
	$('.noticeBoxClose .iconClose').click(function(){		
		$('#searchResultArea').fadeOut(animateTime, function(){
			$('#searchResultArea').remove()
		});
	});		
	
	}
	catch(err) { }
	finally {}
}

// ------------------------------------------------------------

function reloadCustomerAdressFields(searchMode, typeAdress, jsonFieldDatas){
	triggerElement = jsonFieldDatas[0].triggerElement;
	$('.loadingArea').remove();
	var animateTime = 0;
	try {
		delete objJSON;
		var searchString = $(triggerElement).attr('value');
		if(searchString != ''){
			$(triggerElement).parent().append('<span class="loadingArea"><img class="loader" src="layout/ajax-loader.gif" \/> Bitte haben Sie ein wenig Geduld. Die Daten werden geladen...<\/span>');

			var loadURL = 'inc/loadAdressData.inc.php';
			loadURL += '?type=' + searchMode;
			loadURL += '&customerNumber=' + searchString;
			loadURL += '&typeAdress=' + typeAdress;

			$.get(loadURL, function(jsonData) {
				if(jsonData == '') {
					$('.loadingArea').remove();
					window.alert('Es konnten keine Daten gefunden werden!');
				}
				else {
					var objJSON = jQuery.parseJSON(jsonData);

					var fieldNumber = jsonFieldDatas[0].fieldNumber;
					var fieldName = jsonFieldDatas[0].fieldName;
					var fieldNameAdd = jsonFieldDatas[0].fieldNameAdd;
					var fieldCity = jsonFieldDatas[0].fieldCity;
					var fieldZipCode = jsonFieldDatas[0].fieldZipCode;
					var fieldCountry = jsonFieldDatas[0].fieldCountry;
					var fieldStreet = jsonFieldDatas[0].fieldStreet;
					var fieldStreetNumber = jsonFieldDatas[0].fieldStreetNumber;
					var fieldManager = jsonFieldDatas[0].fieldManager;
					var fieldContact = jsonFieldDatas[0].fieldContact;

					var fieldMail = jsonFieldDatas[0].fieldMail;
					var fieldMailCopy = jsonFieldDatas[0].fieldMailCopy;
					
					var fieldGroupCompanyNumber = jsonFieldDatas[0].fieldGroupCompanyNumber;

					var thisDisabled = false; // true
					$(fieldName).val(objJSON[typeAdress]['company']).attr('disabled', thisDisabled);
					$(fieldNameAdd).val(objJSON[typeAdress]['companyAdd']).attr('disabled', thisDisabled);
					$(fieldStreet).val(objJSON[typeAdress]['street']).attr('disabled', thisDisabled);
					$(fieldStreetNumber).val(objJSON[typeAdress]['number']).attr('disabled', thisDisabled);
					$(fieldCountry).val(objJSON[typeAdress]['country']).attr('disabled', thisDisabled);
					$(fieldZipCode).val(objJSON[typeAdress]['zipcode']).attr('disabled', thisDisabled);
					$(fieldCity).val(objJSON[typeAdress]['city']).attr('disabled', thisDisabled);

					$(fieldManager).val(objJSON[typeAdress]['manager']).attr('disabled', thisDisabled);
					$(fieldContact).val(objJSON[typeAdress]['contact']).attr('disabled', thisDisabled);

					$(fieldMail).val(objJSON[typeAdress]['mail']).attr('disabled', thisDisabled);
					$(fieldMailCopy).val(objJSON[typeAdress]['mailCopy']).attr('disabled', thisDisabled);
					
					$(fieldGroupCompanyNumber).val(objJSON[typeAdress]['groupCompanyNumber']).attr('disabled', thisDisabled);

					$(triggerElement).parent().find('.loadingArea').remove();
				}
			});
		}
	}
	catch(err) { handleJsError(err); return false; }
	finally {}
}

// ------------------------------------------------------------

function removeSpecialParamFromUrl(key, sourceURL) {
	var rtn = sourceURL.split("?")[0],
		param,
		params_arr = [],
		queryString = (sourceURL.indexOf("?") !== -1) ? sourceURL.split("?")[1] : "";
	if (queryString !== "") {
		params_arr = queryString.split("&");
		for (var i = params_arr.length - 1; i >= 0; i -= 1) {
			param = params_arr[i].split("=")[0];
			if (param === key) {
				params_arr.splice(i, 1);
			}
		}
		rtn = rtn + "?" + params_arr.join("&");
	}
	return rtn;
}

// ------------------------------------------------------------

function removeAllParamsFromUrl(parameter, sourceURL) {
	// var url = document.location.href;
	var url = sourceURL;
	var urlparts = sourceURL.split('?');

	if (urlparts.length >= 2){			
		var urlBase=urlparts.shift(); 
		var queryString=urlparts.join("?"); 
		
		url = urlBase;
		if(parameter != ''){
			var prefix = encodeURIComponent(parameter) + '=';
			var pars = queryString.split(/[&;]/g);
			for (var i= pars.length; i-->0;)               
			if (pars[i].lastIndexOf(prefix, 0)!==-1) {  
				pars.splice(i, 1);
			}
			url = urlBase+'?'+pars.join('&');
			window.history.pushState('',document.title,url); // added this line to push the new url directly to url bar .
		}
	}
	return url;
}

// ------------------------------------------------------------
function blink(element, repeat, opacity, speed){
	var defaultRepeat = 100;
	if(repeat < 1){
		repeat = defaultRepeat;
	}
	if(repeat > 0){
		for(i = 0; i < repeat ; i++){
			$(element).fadeTo(speed, opacity);
			$(element).fadeTo(speed, 1);
		}
	}	
}
// ------------------------------------------------------------
function loadProductionDetails(searchString, rowID) {
	if(rowID != null){
		var loadURL = 'inc/loadProductionDetails.inc.php';
		var animateTime = 'fast';
		$('#loadOrderDetailsArea').remove();
		$('body').append('<div class="loadedWindow" id="loadOrderDetailsArea"></div>');
		$('#loadOrderDetailsArea').slideDown(animateTime);
		var content = '';
		var contentHeader = 'Details des Auftrages';
		var contentText = '';
		var contentFooter = '';
		content = '<div class="noticeBoxHeader">' + contentHeader + '</div>';
		content += '<div class="noticeBoxContents">' + contentText + '</div>';
		content += '<div class="noticeBoxFooter">' + contentFooter + '</div>';
		// content += '<div class="noticeBoxClose">' + '<img src="layout/icons/iconClose.png" class="iconClose" width="14" height="14" alt="" title="Liste schlie&szlig;en" />' + '</div>';
		var arrWindowButtons = loadWindowButtons();

		content += '<div class="noticeBoxClose">' + arrWindowButtons['small'] + ' ' + arrWindowButtons['move'] + ' ' + arrWindowButtons['close'] + '</div>';
		content = '<div class="loadNoticeContent">' + content + '</div>'

		$('#loadOrderDetailsArea').html(content);

		actionWindowButtons();

		$('.noticeBoxContents').empty().html('<span class="loadingArea"><img class="loader" src="layout/ajax-loader.gif" /> Bitte haben Sie ein wenig Geduld. Die Daten werden geladen...</span>');

		loadURL += '?searchString=' + searchString;

		$('.noticeBoxContents').load(loadURL, function(response, status, xhr){
			if (status == 'error') {
				// var msg = 'Es ist ein Fehler aufgetreten. Fehler-Code: ';
				var msg = 'Die Daten konnten nicht geladen werden. Fehler-Code: ';
				$('.noticeBoxContents').html(msg + xhr.status + ' ' + xhr.statusText);
				//$('#loadOrderDetailsArea').remove();
			}
			else {
				if($('.noticeBoxContents').html() != '') {							
					$('#loadOrderDetailsArea').fadeIn(animateTime);

					$('.buttonUseData').live('click', function(){
						var thisOrderTotalColors = $(this).parent().parent().find('.productsTotalColors').text();
						var thisOrderQuantity = $(this).parent().parent().find('.productsQuantity').text();
						var thisOrderPrintTypeID = $(this).parent().parent().find('.productsPrintTypeID').text();
						var thisOrderOrderID = $(this).parent().parent().find('.productsOrderID').text();
						var thisOrderProductID = $(this).parent().parent().find('.productsProductID').text();
						
						if(rowID != null){												
							$('#printingOrdersProductCountColors_' + rowID).val(thisOrderTotalColors);
							$('#printingOrdersProductQuantity_' + rowID).val(thisOrderQuantity);
							$('#printingOrdersPrintTypeID_' + rowID).val(thisOrderPrintTypeID);
							
							$('#printersPrintingOrdersOrderID_' + rowID).val(thisOrderOrderID);
							$('#printersPrintingOrdersProductID_' + rowID).val(thisOrderProductID);

							$('#loadOrderDetailsArea').remove();
							rowID = null;
						}
					});
				}
				else {
					//$('#loadOrderDetailsArea').remove();
				}
			}
		});
	}
}
// ------------------------------------------------------------