<?php
	require_once('inc/requires.inc.php');

	if(!$arrGetUserRights["editOrders"]) {
		header('location: ' . PAGE_EXIT_LOCATION);
		exit;
	}

	$jswindowMessage = '';
	$warningMessage = '';
	$errorMessage = '';
	$successMessage = '';
	$infoMessage = '';

	$dbConnection = new DB_Connection();
	$db_open = $dbConnection->db_connect();

	// BOF READ PRINTERS
		$arrPrinterDatas = getPrinters();
	// EOF READ PRINTERS

	// BOF READ PRINTTYPES
		$arrPrintTypeDatas = getPrintTypes();
	// EOF READ PRINTTYPES

	// BOF DELETE SINGLE ENTRY
		if((int)$_REQUEST["deleteID"] > 0 && (int)$_REQUEST["editPrinterID"] > 0 && $_REQUEST["editPrintDate"] != ''){
			$sql_deleteSingleEntry = "
						DELETE FROM `" . TABLE_PRINTERS_PRINTING_ORDERS . "`

						WHERE 1
							AND `printersPrintingOrdersID` = '" . $_REQUEST["deleteID"] . "'
							AND `printersPrintingOrdersPrinterID` = '" . $_REQUEST["editPrinterID"] . "'
							AND `printersPrintingOrdersPrintDate` = '" . $_REQUEST["editPrintDate"] . "'
				";

			$rs_deleteSingleEntry = $dbConnection->db_query($sql_deleteSingleEntry);
			if($rs_deleteSingleEntry){
				$successMessage .= "Der Eintrag wurde entfernt!" . "\n";
			}
			else {
				$errorMessage .= "Der Eintrag konnte nicht entfernt werden!" . "\n";
			}
		}
	// EOF DELETE SINGLE ENTRY

	// BOF DELETE ALL ENTRY
		if($_REQUEST["deleteDatas"] != "" && (int)$_REQUEST["editPrinterID"] > 0 && $_REQUEST["editPrintDate"] != ''){
			$sql_deleteMultipleEntry = "
						DELETE FROM `" . TABLE_PRINTERS_PRINTING_ORDERS . "`

						WHERE 1
							AND `printersPrintingOrdersPrinterID` = '" . $_REQUEST["editPrinterID"] . "'
							AND `printersPrintingOrdersPrintDate` = '" . $_REQUEST["editPrintDate"] . "'
				";

			$rs_deleteMultipleEntry = $dbConnection->db_query($sql_deleteMultipleEntry);
			if($rs_deleteMultipleEntry){
				$successMessage .= "Die Eintr&auml;ge wurden entfernt!" . "\n";
			}
			else {
				$errorMessage .= "Die Eintr&auml;ge konnten nicht entfernt werden!" . "\n";
			}
			unset($_REQUEST["editPrinterID"]);
			unset($_REQUEST["editPrintDate"]);
		}
	// EOF DELETE ALL ENTRY

	// BOF CANCEL DATA
		if($_REQUEST["cancelDatas"] != ''){
			unset($_REQUEST["editPrinterID"]);
			unset($_REQUEST["editPrintDate"]);
		}
	// EOF CANCEL DATA

	// BOF STORE DATA
		if($_REQUEST["storeDatas"] != '' || $_REQUEST["storeAndAddDatas"] != ''){
			if($_REQUEST["editPrintersPrintingOrdersPrinterID"] > 0){
				if(!empty($_REQUEST["editPrintersPrintingOrdersData"])){
					$rs_all = false;
					foreach($_REQUEST["editPrintersPrintingOrdersData"] as $thisKey => $thisValue){
						if(
							$_REQUEST["editPrintersPrintingOrdersPrinterID"] > 0 &&
							$_REQUEST["editPrintersPrintingOrdersData"][$thisKey]["printingOrdersProductQuantity"] > 0 &&
							$_REQUEST["editPrintersPrintingOrdersData"][$thisKey]["printingOrdersProductCountColors"] > 0
						){
							$_REQUEST["editPrintersPrintingOrdersData"][$thisKey]["printersPrintingOrdersPrintLocation"] = 'DE';

							if($_REQUEST["editPrintersPrintingOrdersData"][$thisKey]["printersPrintingOrdersID"] == ''){
								$_REQUEST["editPrintersPrintingOrdersData"][$thisKey]["printersPrintingOrdersID"] = '%';
							}

							$sql = "
									REPLACE INTO `" . TABLE_PRINTERS_PRINTING_ORDERS . "` (
												`printersPrintingOrdersID`,
												`printersPrintingOrdersDocumentNumber`,
												`printersPrintingOrdersPrintDate`,
												`printersPrintingOrdersPrinterID`,
												`printersPrintingOrdersProductID`,
												`printersPrintingOrdersPrintTypeID`,
												`printersPrintingOrdersProductQuantity`,
												`printersPrintingOrdersProductCountColors`,
												`printersPrintingOrdersProductTotalPrints`,
												`printersPrintingOrdersPrintLocation`,
												`printersPrintingOrdersOrderID`

											)
											VALUES (
												'" . $_REQUEST["editPrintersPrintingOrdersData"][$thisKey]["printersPrintingOrdersID"] . "',
												'" . $_REQUEST["editPrintersPrintingOrdersData"][$thisKey]["printingOrdersDocumentNumber"] . "',
												'" . formatDate($_REQUEST["editPrintersPrintingOrdersPrintDate"], 'store') . "',
												'" . $_REQUEST["editPrintersPrintingOrdersPrinterID"] . "',
												'" . $_REQUEST["editPrintersPrintingOrdersData"][$thisKey]["printingOrdersProductID"] . "',
												'" . $_REQUEST["editPrintersPrintingOrdersData"][$thisKey]["printingOrdersPrintTypeID"] . "',
												'" . $_REQUEST["editPrintersPrintingOrdersData"][$thisKey]["printingOrdersProductQuantity"] . "',
												'" . $_REQUEST["editPrintersPrintingOrdersData"][$thisKey]["printingOrdersProductCountColors"] . "',
												'" . ($_REQUEST["editPrintersPrintingOrdersData"][$thisKey]["printingOrdersProductQuantity"] * $_REQUEST["editPrintersPrintingOrdersData"][$thisKey]["printingOrdersProductCountColors"]) . "',
												'" . $_REQUEST["editPrintersPrintingOrdersData"][$thisKey]["printersPrintingOrdersPrintLocation"] . "',
												'" . $_REQUEST["editPrintersPrintingOrdersData"][$thisKey]["printersPrintingOrdersOrderID"] . "'
											)
								";
							$rs = $dbConnection->db_query($sql);
							if($rs){
								$successMessage .= "Der Eintrag " . ($thisKey + 1) . " wurde gespeichert!" . "<br />";
							}
							else {
								$errorMessage .= "Der Eintrag " . ($thisKey + 1) . " konnte nicht gespeichert werden!" . "<br />";
							}
						}
						#$errorMessage .= "Der Eintrag " . ($thisKey + 1) . " konnte nicht gespeichert werden, weil er nicht vollst&auml;ndige Daten enth&auml;lt!" . "<br />";
					}
				}
				$_REQUEST["editPrinterID"] = $_REQUEST["editPrintersPrintingOrdersPrinterID"];
				$_REQUEST["editPrintDate"] = formatDate($_REQUEST["editPrintersPrintingOrdersPrintDate"], 'store');

				if($_REQUEST["storeAndAddDatas"] != ''){
					$_REQUEST["editPrinterID"] = 'NEW';
					$_REQUEST["editPrintDate"] = 'NEW';
				}
			}
		}

	// EOF STORE DATA

	// BOF GET ALL DATA
		if(!isset($_REQUEST["editPrinterID"]) && !isset($_REQUEST["editPrintDate"])){

			// BOF GET ALL DATES
				$sql_getAllDates = "
							SELECT
									`" . TABLE_PRINTERS_PRINTING_ORDERS . "`.`printersPrintingOrdersPrintDate`

								FROM `" . TABLE_PRINTERS_PRINTING_ORDERS . "`

								WHERE 1

								GROUP BY `" . TABLE_PRINTERS_PRINTING_ORDERS . "`.`printersPrintingOrdersPrintDate`

								ORDER BY `" . TABLE_PRINTERS_PRINTING_ORDERS . "`.`printersPrintingOrdersPrintDate` DESC
					";
				$rs_getAllDates = $dbConnection->db_query($sql_getAllDates);

				$arrAllPrintDates = array();

				while($ds_getAllDates = mysqli_fetch_assoc($rs_getAllDates)){
					$arrAllPrintDates[] = $ds_getAllDates["printersPrintingOrdersPrintDate"];
				}
			// EOF GET ALL DATES

			if($_REQUEST["searchInterval"] == ""){
				$_REQUEST["searchInterval"] = "DAY";
			}
			if($_REQUEST["searchPrinter"] == ""){
				$_REQUEST["searchPrinter"] = "PRINTERS_TOTAL";
			}



			$sqlWhere = "";
			$sqlGroup = " GROUP BY `interval` ";
			$sqlOrder = " ORDER BY `interval` DESC ";
			$dateField = "";

			if($_REQUEST["searchInterval"] == "DAY"){
				$sqlWhere .= "";
				$dateField = "
					DATE_FORMAT(`" . TABLE_PRINTERS_PRINTING_ORDERS . "`.`printersPrintingOrdersPrintDate`, '%Y-%m-%d') AS `interval`
				";
			}
			else if($_REQUEST["searchInterval"] == "WEEK"){
				$sqlWhere .= "";
				$dateField = "
					CONCAT(
						DATE_FORMAT(`" . TABLE_PRINTERS_PRINTING_ORDERS . "`.`printersPrintingOrdersPrintDate`, '%Y'),
						'#',
						DATE_FORMAT(`" . TABLE_PRINTERS_PRINTING_ORDERS . "`.`printersPrintingOrdersPrintDate`, '%u')
					) AS `interval`
				";
			}
			else if($_REQUEST["searchInterval"] == "MONTH"){
				// DATE_FORMAT(`" . TABLE_ORDER_INVOICES . "`.`printersPrintingOrdersPrintDate`, '%M') AS `interval`,
				$sqlWhere .= "";

				$dateField = "
					CONCAT(
						DATE_FORMAT(`" . TABLE_PRINTERS_PRINTING_ORDERS . "`.`printersPrintingOrdersPrintDate`, '%Y'),
						'#',
						DATE_FORMAT(`" . TABLE_PRINTERS_PRINTING_ORDERS . "`.`printersPrintingOrdersPrintDate`, '%m')
					) AS `interval`
				";
			}
			else if($_REQUEST["searchInterval"] == "QUARTER"){
				$sqlWhere .= "";

				$dateField = "
					CONCAT(
						DATE_FORMAT(`" . TABLE_PRINTERS_PRINTING_ORDERS . "`.`printersPrintingOrdersPrintDate`, '%Y'),
						'#',
						IF(DATE_FORMAT(`" . TABLE_PRINTERS_PRINTING_ORDERS . "`.`printersPrintingOrdersPrintDate`, '%m') = '01' OR DATE_FORMAT(`" . TABLE_PRINTERS_PRINTING_ORDERS . "`.`printersPrintingOrdersPrintDate`, '%m') = '02' OR DATE_FORMAT(`" . TABLE_PRINTERS_PRINTING_ORDERS . "`.`printersPrintingOrdersPrintDate`, '%m') = '03', '1. Quartal',
							IF(DATE_FORMAT(`" . TABLE_PRINTERS_PRINTING_ORDERS . "`.`printersPrintingOrdersPrintDate`, '%m') = '04' OR DATE_FORMAT(`" . TABLE_PRINTERS_PRINTING_ORDERS . "`.`printersPrintingOrdersPrintDate`, '%m') = '05' OR DATE_FORMAT(`" . TABLE_PRINTERS_PRINTING_ORDERS . "`.`printersPrintingOrdersPrintDate`, '%m') = '06', '2. Quartal',
								IF(DATE_FORMAT(`" . TABLE_PRINTERS_PRINTING_ORDERS . "`.`printersPrintingOrdersPrintDate`, '%m') = '07' OR DATE_FORMAT(`" . TABLE_PRINTERS_PRINTING_ORDERS . "`.`printersPrintingOrdersPrintDate`, '%m') = '08' OR DATE_FORMAT(`" . TABLE_PRINTERS_PRINTING_ORDERS . "`.`printersPrintingOrdersPrintDate`, '%m') = '09', '3. Quartal',
									IF(DATE_FORMAT(`" . TABLE_PRINTERS_PRINTING_ORDERS . "`.`printersPrintingOrdersPrintDate`, '%m') = '10' OR DATE_FORMAT(`" . TABLE_PRINTERS_PRINTING_ORDERS . "`.`printersPrintingOrdersPrintDate`, '%m') = '11' OR DATE_FORMAT(`" . TABLE_PRINTERS_PRINTING_ORDERS . "`.`printersPrintingOrdersPrintDate`, '%m') = '12', '4. Quartal',
										''
									)
								)
							)
						)
					) AS `interval`
				";
			}
			else if($_REQUEST["searchInterval"] == "YEAR"){
				$sqlWhere .= "";

				$dateField = "
					CONCAT(
						DATE_FORMAT(`" . TABLE_PRINTERS_PRINTING_ORDERS . "`.`printersPrintingOrdersPrintDate`, '%Y'),
						'#',
						DATE_FORMAT(`" . TABLE_PRINTERS_PRINTING_ORDERS . "`.`printersPrintingOrdersPrintDate`, '%Y')
					) AS `interval`
				";

			}

			if($_REQUEST["searchPrinter"] == "PRINTERS_SEPERATE"){
				$sqlGroup = " GROUP BY CONCAT(`interval`, `printersPrintingOrdersPrinterID`) ";
				$sqlOrder = " ORDER BY `interval` DESC ";
			}

			if($_REQUEST["searchPrinter"] != '' && (int)$_REQUEST["searchPrinter"] > 0){
				$sqlWhere .= " AND `printersPrintingOrdersPrinterID` = '" . $_REQUEST["searchPrinter"] . "' ";
			}
			if($_REQUEST["searchDate"] != ''){
				$sqlWhere .= " AND `printersPrintingOrdersPrintDate` = '" . $_REQUEST["searchDate"] . "' ";
			}
			if($_REQUEST["searchPrintType"] != ''){
				$sqlWhere .= " AND `printersPrintingOrdersPrintTypeID` = '" . $_REQUEST["searchPrintType"] . "' ";
			}


			$sql_getAllData = "
					SELECT
							`interval`,

							GROUP_CONCAT(DISTINCT `tempTable`.`printersPrintingOrdersPrinterID`) AS `printersPrintingOrdersPrinterIDs`,
							SUM(`tempTable`.`printersPrintingOrdersProductQuantity`) AS `printersPrintingOrdersProductQuantitiesSum`,
							SUM(`tempTable`.`printersPrintingOrdersProductCountColors`) AS `printersPrintingOrdersProductCountColorsSum`,
							SUM(`tempTable`.`printersPrintingOrdersProductTotalPrints`) AS `printersPrintingOrdersProductTotalPrintsSum`,

							GROUP_CONCAT(
								`tempTable`.`printersPrintingOrdersID`,
								':',
								`tempTable`.`printersPrintingOrdersPrinterID`,
								':',
								`tempTable`.`printersPrintingOrdersDocumentNumber`,
								':',
								`tempTable`.`printersPrintingOrdersPrintDate`,
								':',
								`tempTable`.`printersPrintingOrdersProductQuantity`,
								':',
								`tempTable`.`printersPrintingOrdersProductCountColors`,
								':',
								`tempTable`.`printersPrintingOrdersProductTotalPrints`,
								':',
								`tempTable`.`printersPrintingOrdersPrintLocation`,
								':',
								`tempTable`.`printersPrintingOrdersPrintTypeID`,
								':',
								`tempTable`.`printersPrintingOrdersProductID`,
								':',
								`tempTable`.`printersPrintingOrdersOrderID`

								SEPARATOR '###'
							) AS `printersPrintingOrdersData`


							FROM (
								SELECT

									" . $dateField . ",

									`" . TABLE_PRINTERS_PRINTING_ORDERS . "`.`printersPrintingOrdersID`,
									`" . TABLE_PRINTERS_PRINTING_ORDERS . "`.`printersPrintingOrdersDocumentNumber`,
									`" . TABLE_PRINTERS_PRINTING_ORDERS . "`.`printersPrintingOrdersPrintDate`,
									`" . TABLE_PRINTERS_PRINTING_ORDERS . "`.`printersPrintingOrdersPrinterID`,
									`" . TABLE_PRINTERS_PRINTING_ORDERS . "`.`printersPrintingOrdersProductID`,
									`" . TABLE_PRINTERS_PRINTING_ORDERS . "`.`printersPrintingOrdersPrintTypeID`,
									`" . TABLE_PRINTERS_PRINTING_ORDERS . "`.`printersPrintingOrdersProductQuantity`,
									`" . TABLE_PRINTERS_PRINTING_ORDERS . "`.`printersPrintingOrdersProductCountColors`,
									`" . TABLE_PRINTERS_PRINTING_ORDERS . "`.`printersPrintingOrdersProductTotalPrints`,
									`" . TABLE_PRINTERS_PRINTING_ORDERS . "`.`printersPrintingOrdersPrintLocation`,
									`" . TABLE_PRINTERS_PRINTING_ORDERS . "`.`printersPrintingOrdersOrderID`

								FROM `" . TABLE_PRINTERS_PRINTING_ORDERS . "`

								WHERE 1
									" . $sqlWhere . "
						) AS `tempTable`

						" . $sqlGroup . "

						" . $sqlOrder . "
				";

		}
	// EOF GET ALL DATA

	// BOF GET SELECTED DATA
		if((int)$_REQUEST["editPrinterID"] > 0 && $_REQUEST["editPrintDate"] != '') {
			$sql_getSelectedPrintingOrdersData = "
				SELECT
						`" . TABLE_PRINTERS_PRINTING_ORDERS . "`.`printersPrintingOrdersID`,
						`" . TABLE_PRINTERS_PRINTING_ORDERS . "`.`printersPrintingOrdersDocumentNumber`,
						`" . TABLE_PRINTERS_PRINTING_ORDERS . "`.`printersPrintingOrdersPrintDate`,
						`" . TABLE_PRINTERS_PRINTING_ORDERS . "`.`printersPrintingOrdersPrinterID`,
						`" . TABLE_PRINTERS_PRINTING_ORDERS . "`.`printersPrintingOrdersProductID`,
						`" . TABLE_PRINTERS_PRINTING_ORDERS . "`.`printersPrintingOrdersPrintTypeID`,
						`" . TABLE_PRINTERS_PRINTING_ORDERS . "`.`printersPrintingOrdersProductQuantity`,
						`" . TABLE_PRINTERS_PRINTING_ORDERS . "`.`printersPrintingOrdersProductCountColors`,
						`" . TABLE_PRINTERS_PRINTING_ORDERS . "`.`printersPrintingOrdersProductTotalPrints`,
						`" . TABLE_PRINTERS_PRINTING_ORDERS . "`.`printersPrintingOrdersPrintLocation`,
						`" . TABLE_PRINTERS_PRINTING_ORDERS . "`.`printersPrintingOrdersOrderID`

					FROM `" . TABLE_PRINTERS_PRINTING_ORDERS . "`

					WHERE 1
						AND `" . TABLE_PRINTERS_PRINTING_ORDERS . "`.`printersPrintingOrdersPrinterID` = '" . $_REQUEST["editPrinterID"] . "'
						AND `" . TABLE_PRINTERS_PRINTING_ORDERS . "`.`printersPrintingOrdersPrintDate` = '" . $_REQUEST["editPrintDate"] . "'

					ORDER BY `" . TABLE_PRINTERS_PRINTING_ORDERS . "`.`printersPrintingOrdersID`;
			";
		}


		$rs_getSelectedPrintingOrdersData = $dbConnection->db_query($sql_getSelectedPrintingOrdersData);

		$arrSelectedPrintingOrdersData = array();

		$countItems = 0;
		while($ds_getSelectedPrintingOrdersData = mysqli_fetch_assoc($rs_getSelectedPrintingOrdersData)){
			foreach(array_keys($ds_getSelectedPrintingOrdersData) as $field){
				$arrSelectedPrintingOrdersData["printersPrintingOrdersData"][$countItems][$field] = $ds_getSelectedPrintingOrdersData[$field];
			}
			$arrSelectedPrintingOrdersData["printersPrintingOrdersPrintDate"] = $ds_getSelectedPrintingOrdersData["printersPrintingOrdersPrintDate"];
			$arrSelectedPrintingOrdersData["printersPrintingOrdersPrinterID"] = $ds_getSelectedPrintingOrdersData["printersPrintingOrdersPrinterID"];
			$countItems++;
		}
	// EOF GET SELECTED DATA

?>
<?php
	require_once('inc/headerHTML.inc.php');
	if($_REQUEST["editPrinterID"] == "NEW" && $_REQUEST["editPrintDate"] == "NEW") {
		$thisTitle = "Neuen Drucker-Auftrag anlegen";
		$thisIcon = "addProcess.png";
	}
	else if((int)$_REQUEST["editPrinterID"] > 0 && $_REQUEST["editPrintDate"] != '') {
		$thisTitle = "Drucker-Auftrag bearbeiten";
		$thisTitle .= ': <span class="headerSelectedEntry">'.htmlentities(($arrPrinterDatas[$_REQUEST["editPrinterID"]]["printersFirstName"]) . ' ' . ($arrPrinterDatas[$_REQUEST["editPrinterID"]]["printersLastName"])).' &bull; '.formatDate($_REQUEST["editPrintDate"], 'display').'</span>';
		$thisIcon = "addProcess.gif";

	}
	else {
		$thisTitle = "Auftr&auml;ge der Drucker";
		if($_REQUEST["searchPrinter"] != '') {
			$thisTitle .= ': <span class="headerSelectedEntry">'.htmlentities(($arrPrinterDatas[$_REQUEST["searchPrinter"]]["printersFirstName"]) . ' ' . ($arrPrinterDatas[$_REQUEST["searchPrinter"]]["printersLastName"])).'</span>';
		}
		if($_REQUEST["searchDate"] != '') {
			$thisTitle .= ': <span class="headerSelectedEntry">'.formatDate($_REQUEST["searchDate"], 'display').'</span>';
		}
		if($_REQUEST["searchInterval"] == 'DAY') {
			$thisTitle .= ': <span class="headerSelectedEntry">pro Tag</span>';
		}
		else if($_REQUEST["searchInterval"] == 'WEEK') {
			$thisTitle .= ': <span class="headerSelectedEntry">pro Woche</span>';
		}
		else if($_REQUEST["searchInterval"] == 'MONTH') {
			$thisTitle .= ': <span class="headerSelectedEntry">pro Monat</span>';
		}
		else if($_REQUEST["searchInterval"] == 'QUARTER') {
			$thisTitle .= ': <span class="headerSelectedEntry">pro Vierteljahr</span>';
		}
		else if($_REQUEST["searchInterval"] == 'YEAR') {
			$thisTitle .= ': <span class="headerSelectedEntry">pro Jahr</span>';
		}
		$thisIcon = "datasheet.png";
	}
	$headerHTML = preg_replace("/{###TITLE###}/", strip_tags($thisTitle), $headerHTML);
	echo $headerHTML;

	require_once(FILE_MENUE_TOP);

	if($dbConnection->db_displayErrors() != "") {
		$errorMessage .= $dbConnection->db_displayErrors(). '<br />';
	}
?>
<div id="mainArea">
	<div id="mainContent">
		<?php require_once(FILE_MENUE_SIDEBAR); ?>
		<div id="contentArea">
			<div id="contentAreaElements">
				<h1><?php if(SHOW_PAGE_TITLE_ICONS) { echo '<img src="' . PATH_ICONS_MENUE_TITLES . $thisIcon . '" alt="" />'; } ?> <?php echo $thisTitle; ?></h1>
				<?php displayMessages(); ?>
				<?php if(!isset($_REQUEST["editPrinterID"]) && !isset($_REQUEST["editPrintDate"])){ ?>
				<div id="searchFilterArea">
					<form name="formFilterSearch" method="post" action="<?php echo $_SERVER["PHP_SELF"]; ?>">
						<table border="0" cellpadding="0" cellspacing="0" class="searchFilterContent">
							<tr>
								<td>
									<label for="searchInterval">Zeitraum:</label>
									<select name="searchInterval" id="searchInterval" class="inputSelect_100">
										<option value=""> </option>
										<option value="DAY" <?php if($_REQUEST["searchInterval"] == 'DAY'){ echo ' selected="selected" '; } ?> >pro Tag</option>
										<option value="WEEK" <?php if($_REQUEST["searchInterval"] == 'WEEK'){ echo ' selected="selected" '; } ?> >pro Woche</option>
										<option value="MONTH" <?php if($_REQUEST["searchInterval"] == 'MONTH'){ echo ' selected="selected" '; } ?> >pro Monat</option>
										<option value="QUARTER" <?php if($_REQUEST["searchInterval"] == 'QUARTER'){ echo ' selected="selected" '; } ?> >pro Vierteljahr</option>
										<option value="YEAR" <?php if($_REQUEST["searchInterval"] == 'YEAR'){ echo ' selected="selected" '; } ?> >pro Jahr</option>
									</select>
								</td>
								<td>
									<label for="searchDate">Datum:</label>

									<select name="searchDate" id="searchDate" class="inputSelect_100">
										<option value=""> </option>
										<?php
											if(!empty($arrAllPrintDates)){
												foreach($arrAllPrintDates as $thisKey => $thisValue){
													$selected = '';
													if($thisValue == $_REQUEST["searchDate"]){
														$selected = ' selected="selected" ';
													}
													echo '<option value="' . $thisValue . '" ' . $selected . ' >' . formatDate($thisValue, 'display') . '</option>';
												}
											}
										?>
									</select>

								</td>
								<td>
									<label for="searchPrinter">Drucker:</label>
									<select name="searchPrinter" id="searchPrinter" class="inputSelect_100">
										<option value=""> </option>
										<option value="PRINTERS_TOTAL" <?php if($_REQUEST["searchPrinter"] == 'PRINTERS_TOTAL') { echo ' selected="selected" '; } ?> >Alle Drucker zusammen</option>
										<option value="PRINTERS_SEPERATE" <?php if($_REQUEST["searchPrinter"] == 'PRINTERS_SEPERATE') { echo ' selected="selected" '; } ?> >Drucker einzeln</option>
										<?php
											if(!empty($arrPrinterDatas)){
												foreach($arrPrinterDatas as $thisKey => $thisValue){
													$selected = '';
													if($thisValue["printersID"] == $_REQUEST["searchPrinter"]){
														$selected = ' selected="selected" ';
													}
													echo '<option value="' . $thisValue["printersID"] . '" ' . $selected . ' >' . htmlentities(($thisValue["printersFirstName"]) . ' ' . ($thisValue["printersLastName"])) . '</option>';
												}
											}
										?>
									</select>
								</td>
								<td>
									<label for="searchPrintType">Druckart:</label>
									<select name="searchPrintType" id="searchPrintType" class="inputSelect_100">
										<option value=""> </option>
										<?php
											if(!empty($arrPrintTypeDatas)){
												foreach($arrPrintTypeDatas as $thisKey => $thisValue){
													$selected = '';
													if($thisValue["printTypesID"] == $_REQUEST["searchPrintType"]){
														$selected = ' selected="selected" ';
													}
													echo '<option value="' . $thisValue["printTypesID"] . '" ' . $selected . ' >' . htmlentities(($thisValue["printTypesName"])) . '</option>';
												}
											}
										?>
									</select>
								</td>

								<td>
									<input type="submit" name="submitSearch" class="inputButton0" value="Suchen" />
								</td>

								<td class="legendSeparator"></td>

								<td>
									<a href="<?php echo PAGE_DISPLAY_PRINTERS_ORDERRS; ?>?editPrinterID=NEW&amp;editPrintDate=NEW" class="linkButton"> Neuen Druckauftrag eintragen</a>
								</td>
							</tr>
						</table>
					</form>
				</div>
				<?php } ?>

				<?php if(!isset($_REQUEST["editPrinterID"]) && !isset($_REQUEST["editPrintDate"])){ ?>

				<?php
					$rs_getAllData = $dbConnection->db_query($sql_getAllData);

					$countItems = $dbConnection->db_getMysqlNumRows($rs_getAllData);
					if($countItems > 0){

				?>
				<table cellpadding="0" cellspacing="0" width="100%" class="displayOrders">
					<tr>
						<th style="width:50px;">#</th>
						<th>Zeitraum</th>
						<th>Drucker</th>
						<th>St&uuml;ckzahl</th>
						<th>Anzahl Farben (Druckdurchg&auml;nge)</th>
						<th>Anzahl Drucke</th>
						<th>Info</th>
					</tr>

					<?php

						$rs_getAllData = $dbConnection->db_query($sql_getAllData);

						$countRow = 0;
						$thisMarker = '';

						while($ds_getAllData = mysqli_fetch_assoc($rs_getAllData)) {							

							$arrTemp = explode('#', $ds_getAllData["interval"]);
							if($thisMarker != $arrTemp[0]) {
								$thisMarker = $arrTemp[0];
								echo '<tr><td colspan="8" class="tableRowTitle1">' . $arrTemp[0] . '</td></tr>';
							}
							
							if($countRow%2 == 0){ $rowClass = 'row0'; }
							else { $rowClass = 'row1'; }
							echo '<tr class="' . $rowClass . '">';

							echo '<td style="text-align:right;"><b>' . ($countRow + 1) . '.</b></td>';

							echo '<td style="text-align:right;">';
							if($_REQUEST["searchInterval"] == "MONTH"){
								$thisMonth = getTimeNames($arrTemp[1], 'month', 'long');
								echo $thisMonth;
							}
							else {
								if($_REQUEST["searchInterval"] == "WEEK"){ echo 'KW '; }
								if($_REQUEST["searchInterval"] == "DAY"){ echo formatDate($ds_getAllData["interval"], "display"); }
								else { echo $arrTemp[1]; }
							}

							echo ' <img src="layout/icons/iconInfo.png" class="buttonShowProductionDetails" width="12" height="12" alt="Info" title="Details anzeigen" />';
								echo '<div class="productionDetailsArea" style="display:none;position:absolute;top:auto;right:auto;background-color:#FFF;padding:10px;border:1px solid #333;box-shadow: 6px 6px 8px #333;">';

								echo '<table cellpadding="0" cellspacing="0">';
								echo '<tr>';
								echo '<th>#</th>';
								echo '<th>Datum</th>';
								echo '<th>Drucker</th>';
								echo '<th>AB-Nummer</th>';
								echo '<th>Menge</th>';
								echo '<th>Farben</th>';
								echo '<th>Drucke</th>';
								echo '<th>Druckart</th>';
								echo '<th></th>';
								echo '</tr>';
								$arrThisPrintersPrintingOrdersData = explode("###", $ds_getAllData["printersPrintingOrdersData"]);

								$countThisOrder = 0;
								foreach($arrThisPrintersPrintingOrdersData as $thisPrintersPrintingOrdersValue){
									$thisPrintersPrintingOrdersData = explode(':', $thisPrintersPrintingOrdersValue);

									if($countThisOrder%2 == 0){ $rowClass = 'row0'; }
									else { $rowClass = 'row1'; }
									echo '<tr class="' . $rowClass . '">';

									echo '<td style="text-align:right;">';
									echo '<b>' . ($countThisOrder + 1) . '.</b>';
									echo '</td>';

									echo '<td>';
									echo formatDate($thisPrintersPrintingOrdersData["3"], 'display');
									echo '</td>';

									echo '<td>';
									echo htmlentities($arrPrinterDatas[$thisPrintersPrintingOrdersData["1"]]["printersFirstName"] . ' ' . $arrPrinterDatas[$thisPrintersPrintingOrdersData["1"]]["printersLastName"]);
									echo '</td>';

									echo '<td>';
									echo $thisPrintersPrintingOrdersData["2"];
									echo '</td>';

									echo '<td style="text-align:right;">';
									echo number_format($thisPrintersPrintingOrdersData["4"], 0, ',', '.');
									echo '</td>';

									echo '<td style="text-align:right;">';
									echo number_format($thisPrintersPrintingOrdersData["5"], 0, ',', '.');
									echo '</td>';

									echo '<td style="text-align:right;">';
									echo number_format($thisPrintersPrintingOrdersData["6"], 0, ',', '.');
									echo '</td>';

									echo '<td>';
									echo $arrPrintTypeDatas[$thisPrintersPrintingOrdersData["8"]]["printTypesName"];
									echo '</td>';

									echo '<td>';
										echo '<span class="toolItem">';
										echo '<a href="' . PAGE_DISPLAY_PRINTERS_ORDERRS . '?editPrinterID=' . $thisPrintersPrintingOrdersData["1"] . '&amp;editPrintDate=' . $thisPrintersPrintingOrdersData["3"] . '"><img src="layout/icons/iconEdit.gif" width="16" height="16" alt="" title="Dieses Datum bearbeiten" /></a>';
										echo '</span>';
									echo '</td>';

									echo '</tr>';
									$countThisOrder++;
								}
								echo '</table>';
								echo '</div>';

							echo '</td>';

							echo '<td>';
							if($ds_getAllData["printersPrintingOrdersPrinterIDs"] != ''){
								$arrThisPrintersPrintingOrdersPrinterIDs = explode(",", $ds_getAllData["printersPrintingOrdersPrinterIDs"]);
								foreach($arrThisPrintersPrintingOrdersPrinterIDs as $thisPrintersPrintingOrdersPrinterID){
									echo ' &bull; ' . htmlentities($arrPrinterDatas[$thisPrintersPrintingOrdersPrinterID]["printersFirstName"] . ' ' . $arrPrinterDatas[$thisPrintersPrintingOrdersPrinterID]["printersLastName"]) . '<br />';
								}
							}
							echo '</td>';

							echo '<td style="text-align:right;">';
							echo number_format($ds_getAllData["printersPrintingOrdersProductQuantitiesSum"], 0, ',', '.');
							echo '</td>';

							echo '<td style="text-align:right;">';
							echo number_format($ds_getAllData["printersPrintingOrdersProductCountColorsSum"], 0, ',', '.');
							echo '</td>';

							echo '<td style="text-align:right;">';
							echo number_format($ds_getAllData["printersPrintingOrdersProductTotalPrintsSum"], 0, ',', '.');
							echo '</td>';

							echo '<td>';
							if(($_REQUEST["searchInterval"] == 'DAY' || $_REQUEST["searchDate"] != '') && ($_REQUEST["searchPrinter"] == "PRINTERS_SEPERATE" || (int)$_REQUEST["searchPrinter"] > 0)) {
								echo '<span class="toolItem">';
								echo '<a href="' . PAGE_DISPLAY_PRINTERS_ORDERRS . '?editPrinterID=' . $ds_getAllData["printersPrintingOrdersPrinterIDs"] . '&amp;editPrintDate=' . $ds_getAllData["interval"] . '"><img src="layout/icons/iconEdit.gif" width="16" height="16" alt="" title="Dieses Datum bearbeiten" /></a>';
								echo '</span>';
							}
							echo '</td>';

							echo '</tr>';

							$countRow++;
						}

					?>
				</table>
				<?php } else { ?>
				<p class="infoArea">Es wurden keine Daten gefunden!</p>
				<?php } ?>

				<?php } ?>

				<?php if(isset($_REQUEST["editPrinterID"]) && isset($_REQUEST["editPrintDate"])){ ?>

				<div class="adminInfo">Datensatz-ID: <?php echo $orderDatas["ordersID"]; ?></div>
				<div class="adminEditArea">
					<form name="editOrderDatas" method="post" action="<?php echo $_SERVER["PHP_SELF"]; ?>" enctype="multipart/form-data">
						<input type="hidden" name="editPrinterID" value="<?php echo $_REQUEST["editPrinterID"]; ?>" />
						<input type="hidden" name="editPrintDate" value="<?php echo $_REQUEST["editPrintDate"]; ?>" />

						<fieldset>
							<legend>Druckdaten</legend>
							<table border="0" width="100%" cellspacing="0" cellpadding="0">
								<tr>
									<td width="200"><b>Drucker:</b></td>
									<td>
										<?php if((int)$arrSelectedPrintingOrdersData["printersPrintingOrdersPrinterID"] > 0){ ?>
										<input type="hidden" name="editPrintersPrintingOrdersPrinterID" id="editPrintersPrintingOrdersPrinterID" value="<?php echo $arrSelectedPrintingOrdersData["printersPrintingOrdersPrinterID"]; ?>" readonly="readonly" />
										<?php echo htmlentities(($arrPrinterDatas[$arrSelectedPrintingOrdersData["printersPrintingOrdersPrinterID"]]["printersFirstName"]) . ' ' . ($arrPrinterDatas[$arrSelectedPrintingOrdersData["printersPrintingOrdersPrinterID"]]["printersLastName"])); ?>

										<?php } else { ?>
										<select name="editPrintersPrintingOrdersPrinterID" id="editPrintersPrintingOrdersPrinterID" class="inputSelect_510">
											<option value=""> </option>
											<?php
												if(!empty($arrPrinterDatas)){
													foreach($arrPrinterDatas as $thisKey => $thisValue){
														$selected = '';
														if($thisValue["printersID"] == $arrSelectedPrintingOrdersData["printersPrintingOrdersPrinterID"]){
															$selected = ' selected="selected" ';
														}
														echo '<option value="' . $thisValue["printersID"] . '" ' . $selected . ' >' . htmlentities(($thisValue["printersFirstName"]) . ' ' . ($thisValue["printersLastName"])) . '</option>';
													}
												}
											?>
										</select>
										<?php } ?>
									</td>
								</tr>
								<tr>
									<td width="200"><b>Datum:</b></td>
									<td>
										<?php
											$thisPrintersPrintingOrdersPrintDate = $arrSelectedPrintingOrdersData["printersPrintingOrdersPrintDate"];
											if($arrSelectedPrintingOrdersData["printersPrintingOrdersPrintDate"] == ''){
												$thisPrintersPrintingOrdersPrintDate = date("Y-m-d", mktime(0, 0, 0, date("m"), (date("d") - 1), date("Y")));
											}
										?>
										<input type="text" name="editPrintersPrintingOrdersPrintDate" id="editPrintersPrintingOrdersPrintDate" class="inputField_70" readonly="readonly" value="<?php echo formatDate($thisPrintersPrintingOrdersPrintDate, 'display'); ?>" />
									</td>
								</tr>
							</table>
						</fieldset>

						<fieldset style="display:none;" id="formEditPrintOrders">
							<legend>Druck-Auftr&auml;ge</legend>
							<table border="0" width="100%" cellspacing="0" cellpadding="0" class="displayOrders" id="printOrdersTable">
								<tr>
									<th>#</th>
									<th>AB-/RE-Nummer</th>
									<th>St&uuml;ckzahl</th>
									<th>Anzahl Farben</th>
									<th>Druckart</th>
									<th>Druckort</th>
									<th>Aktion</th>
								</tr>

								<?php
									$insertRows = 20;
									if(!empty($arrSelectedPrintingOrdersData["printersPrintingOrdersData"])){
										$insertRows = (count($arrSelectedPrintingOrdersData["printersPrintingOrdersData"]) + 10);
									}
									for($i = 0 ; $i < $insertRows ; $i++){

										if($i%2 == 0){ $rowClass = 'row0'; }
										else { $rowClass = 'row1'; }

										#if($_REQUEST["editPrinterID"] == 'NEW' && $_REQUEST["editPrintDate"] == 'NEW'){
											$rowID = ' id="row_' . $i . '" ';
										#}

										echo '<tr class="' . $rowClass . '" ' . $rowID . '>';

										echo '<td style="text-align:right;">';
										echo '<b>' . ($i + 1) . '.</b>';
										echo '<input type="hidden" name="editPrintersPrintingOrdersData[' . $i . '][printersPrintingOrdersID]" id="printersPrintingOrdersID_row_' . $i . '" class="inputField_120" value="' . $arrSelectedPrintingOrdersData["printersPrintingOrdersData"][$i]["printersPrintingOrdersID"] . '" />';

										echo '<input type="hidden" name="editPrintersPrintingOrdersData[' . $i . '][printersPrintingOrdersOrderID]" id="printersPrintingOrdersOrderID_row_' . $i . '" class="inputField_120" value="' . $arrSelectedPrintingOrdersData["printersPrintingOrdersData"][$i]["printersPrintingOrdersOrderID"] . '" />';
										echo '<input type="hidden" name="editPrintersPrintingOrdersData[' . $i . '][printersPrintingOrdersProductID]" id="printersPrintingOrdersProductID_row_' . $i . '" class="inputField_120" value="' . $arrSelectedPrintingOrdersData["printersPrintingOrdersData"][$i]["printersPrintingOrdersProductID"] . '" />';

										echo '</td>';

										echo '<td>';
										echo '<input type="text" name="editPrintersPrintingOrdersData[' . $i . '][printingOrdersDocumentNumber]" id="printingOrdersDocumentNumber_row_' . $i . '" class="inputField_120" value="' . $arrSelectedPrintingOrdersData["printersPrintingOrdersData"][$i]["printersPrintingOrdersDocumentNumber"] . '" />';
										echo '</td>';

										echo '<td>';
										echo '<input type="text" name="editPrintersPrintingOrdersData[' . $i . '][printingOrdersProductQuantity]" id="printingOrdersProductQuantity_row_' . $i . '" class="inputField_70" style="text-align:right;" value="' . $arrSelectedPrintingOrdersData["printersPrintingOrdersData"][$i]["printersPrintingOrdersProductQuantity"] . '" />';
										echo '</td>';

										echo '<td>';
										echo '<input type="text" name="editPrintersPrintingOrdersData[' . $i . '][printingOrdersProductCountColors]" id="printingOrdersProductCountColors_row_' . $i . '" class="inputField_70" style="text-align:right;" value="' . $arrSelectedPrintingOrdersData["printersPrintingOrdersData"][$i]["printersPrintingOrdersProductCountColors"] . '" />';
										echo '</td>';


										echo '<td>';
											$thisPrintersPrintingOrdersPrintTypeID = $arrSelectedPrintingOrdersData["printersPrintingOrdersData"][$i]["printersPrintingOrdersPrintTypeID"];
											if($arrSelectedPrintingOrdersData["printersPrintingOrdersPrintTypeID"] == ''){
												#$thisPrintersPrintingOrdersPrintTypeID = 7;
											}
											echo '<select name="editPrintersPrintingOrdersData[' . $i . '][printingOrdersPrintTypeID]" id="printingOrdersPrintTypeID_row_' . $i . '" class="inputSelect_120">';
											echo '<option value=""> </option>';
												if(!empty($arrPrintTypeDatas)){
													foreach($arrPrintTypeDatas as $thisKey => $thisValue){
														$selected = '';
														if($thisValue["printTypesID"] == $thisPrintersPrintingOrdersPrintTypeID){
															$selected = ' selected="selected" ';
														}
														echo '<option value="' . $thisValue["printTypesID"] . '" ' . $selected . ' >' . htmlentities(($thisValue["printTypesName"])) . '</option>';
													}
												}
											echo '</select>';
										echo '</td>';

										echo '<td>';
										$thisPrintersPrintingOrdersPrintLocation = $arrSelectedPrintingOrdersData["printersPrintingOrdersData"][$i]["printersPrintingOrdersPrintLocation"];
										#$thisPrintersPrintingOrdersPrintLocation = 'DE';
										echo '<input type="text" name="editPrintersPrintingOrdersData[' . $i . '][printersPrintingOrdersPrintLocation]" class="inputField_70" readonly="readonly" value="' . $thisPrintersPrintingOrdersPrintLocation . '" />';
										echo '</td>';

										echo '<td>';
										if($_REQUEST["editPrinterID"] != 'NEW' && $_REQUEST["editPrintDate"] != 'NEW' && $arrSelectedPrintingOrdersData["printersPrintingOrdersData"][$i]["printersPrintingOrdersID"] > 0){
											echo '<span class="toolItem">';
											echo '<a href="' . PAGE_DISPLAY_PRINTERS_ORDERRS . '?deleteID=' . $arrSelectedPrintingOrdersData["printersPrintingOrdersData"][$i]["printersPrintingOrdersID"] . '&amp;editPrinterID=' . $arrSelectedPrintingOrdersData["printersPrintingOrdersPrinterID"] . '&editPrintDate=' . $arrSelectedPrintingOrdersData["printersPrintingOrdersPrintDate"] . '" onclick="showWarning(\'Wollen Sie diesen Eintrag wirklich entfernen?\');"><img src="layout/icons/iconDelete.png" width="16" height="16" alt="entfernen" title="Diesen Eintrag entfernen" /></a>';
											echo '</span>';
										}
										else {
											echo '<span class="toolItem">';
											echo '<img src="layout/spacer.gif" width="16" height="16" alt="x" title="" />';
											echo '</span>';
										}
										echo '</td>';

										echo '</tr>';
									}
								?>

							</table>
						</fieldset>

						<div class="actionButtonsArea">
						<input type="submit" class="inputButton1 inputButtonGreen" name="storeDatas" value="Speichern" />
						<input type="submit" class="inputButton3 inputButtonGreen" name="storeAndAddDatas" value="Speichern und neuen Vorgang hinzuf&uuml;gen" title="Speichern und neuen Vorgang hinzuf&uuml;gen" />
						<input type="submit" class="inputButton1 inputButtonRed" name="deleteDatas" value="Alle entfernen" onclick="return showWarning(' Wollen Sie diese Eintr&auml;ge endg&uuml;ltig entfernen??? ');" />
						<input type="submit" class="inputButton1 inputButtonOrange" name="cancelDatas" value="Abbrechen" onclick="return showWarning(' Wollen Sie diesen Vorgang wirklich abbrechen??? ');" />
					</div>

					</form>


				</div>
				<?php } ?>
			</div>
		</div>
	</div>
</div>

<script language="javascript" type="text/javascript">
	<!--
	/* <![CDATA[ */

	$(document).ready(function() {
		$(function() {

			$('#editPrintersPrintingOrdersPrinterID').live('change', function(){
				if($(this).val() > 0){
					$('#formEditPrintOrders').show();
				}
			});
			<?php if($arrSelectedPrintingOrdersData["printersPrintingOrdersPrinterID"] > 0) { ?>
			$('#formEditPrintOrders').show();
			<?php } ?>

			$('#editPrintersPrintingOrdersPrintDate').datepicker($.datepicker.regional["de"]);
			$('#editPrintersPrintingOrdersPrintDate').datepicker("option", "maxDate", "0" );

			var htmlButtonClearField = '<span class="buttonClearField"><img src="layout/icons/iconDelete.png" width="16" height="16" alt="" title="Eingabefeld leeren" \/><\/span>';
			// $('#editPrintersPrintingOrdersPrintDate').parent().append(htmlButtonClearField);

			$('.buttonClearField').click(function () {
				$(this).parent().find('input').val('');
				$(this).parent().find('input').attr('checked', false);
			});

			$('#searchInterval').live('change', function(){
				if($(this).val() != ''){
					$('#searchDate').val('');
				}
			});
			$('#searchDate').live('click', function(){
				$('#searchInterval').val('');
			});

			$('input[name*="printingOrdersDocumentNumber"]').live('click', function(){
				var thisRowID = $(this).parent().parent().attr('id');

				if($('#printingOrdersPrintTypeID_' + thisRowID).val() == ''){
					$('#printingOrdersPrintTypeID_' + thisRowID).val('7');
				}
				if($('#printersPrintingOrdersPrintLocation_' + thisRowID).val() == ''){
					$('#printersPrintingOrdersPrintLocation_' + thisRowID).val('DE');
				}
				if($('#printingOrdersDocumentNumber_' + thisRowID).val() == ''){
					$('#printingOrdersDocumentNumber_' + thisRowID).val('AB-');
				}
			});

			$('input[name*="printingOrdersDocumentNumber"]').live('keyup', function(){
				var thisDocumentNumber = $(this).val();
				var thisRowID = $(this).parent().parent('tr').attr('id');

				thisDocumentNumber = thisDocumentNumber.trim();
				$(this).val(thisDocumentNumber);

				if(thisDocumentNumber.length == 13){
					loadProductionDetails(thisDocumentNumber, thisRowID);
				}
			});

			$('.buttonShowProductionDetails').css('cursor', 'pointer');
			$('.buttonShowProductionDetails').click(function() {
				$('.productionDetailsArea').not($(this).next('.productionDetailsArea')).hide();
				$(this).next('.productionDetailsArea').toggle();
			});
		});
	});

	/* ]]> */
	// -->

</script>
<?php require_once('inc/footerHTML.inc.php'); ?>