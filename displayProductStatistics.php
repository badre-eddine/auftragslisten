<?php
	require_once('inc/requires.inc.php');

	if(!$arrGetUserRights["displayOnlineOrdersStatistics"]) {
		header('location: ' . PAGE_EXIT_LOCATION);
		exit;
	}

	$jswindowMessage = '';
	$warningMessage = '';
	$errorMessage = '';
	$successMessage = '';
	$infoMessage = '';


	$dbConnection = new DB_Connection();
	$db_open = $dbConnection->db_connect();

	// BOF READ VERTRETER
		$arrSalesmenDatas = getSalesmen();
	// EOF READ VERTRETER

	$defaultORDER = "BESTELLDATUM";
	$defaultSORT = "ASC";

	if($_POST["searchInterval"] == ""){
		$_POST["searchInterval"] = "WEEK";
	}

	// BOF READ BESTELL ARTEN
		$arrOrderTypeDatas = getOrderTypes();
	// EOF READ BESTELL ARTEN
?>
<?php
	require_once('inc/headerHTML.inc.php');
	$thisTitle = "Produkt-Statistik";

	if($_POST["searchInterval"] == 'WEEK') {
		$thisTitle .= ': <span class="headerSelectedEntry">pro Woche</span>';
	}
	else if($_POST["searchInterval"] == 'MONTH') {
		$thisTitle .= ': <span class="headerSelectedEntry">pro Monat</span>';
	}
	else if($_POST["searchInterval"] == 'QUARTER') {
		$thisTitle .= ': <span class="headerSelectedEntry">pro Vierteljahr</span>';
	}
	else if($_POST["searchInterval"] == 'YEAR') {
		$thisTitle .= ': <span class="headerSelectedEntry">pro Jahr</span>';
	}

	$headerHTML = preg_replace("/{###TITLE###}/", strip_tags($thisTitle), $headerHTML);
	echo $headerHTML;

	require_once(FILE_MENUE_TOP);

	$sqlSortField = "";
	$sqlSortDirection = "";
	$sqlWhere = "";

?>

<div id="mainArea">
	<div id="mainContent">
		<?php require_once(FILE_MENUE_SIDEBAR); ?>
		<div id="contentArea">
			<a name="top"></a>
			<div id="contentAreaElements">
				<h1><?php if(SHOW_PAGE_TITLE_ICONS) { echo '<img src="' . PATH_ICONS_MENUE_TITLES . 'statistics.png" alt="" />'; } ?> <?php echo $thisTitle; ?></h1>
				<p>Grundlage der Statistik sind bezahlte und offene Rechnungen!</p>
				<div id="searchFilterArea">
					<form name="formSearchDatas" method="post" action="<?php echo $_SERVER["PHP_SELF"]; ?>">
					<table border="0" cellpadding="0" cellspacing="0" class="searchFilterContent">
						<tr>
							<td>
								<label for="searchInterval">Zeitraum:</label>
								<select name="searchInterval" id="searchInterval" class="inputField_130">
									<option value=""></option>
									<option value="WEEK" <?php if($_POST["searchInterval"] == 'WEEK'){ echo ' selected="selected" '; } ?> >pro Woche</option>
									<option value="MONTH" <?php if($_POST["searchInterval"] == 'MONTH'){ echo ' selected="selected" '; } ?> >pro Monat</option>
									<option value="QUARTER" <?php if($_POST["searchInterval"] == 'QUARTER'){ echo ' selected="selected" '; } ?> >pro Vierteljahr</option>
									<option value="YEAR" <?php if($_POST["searchInterval"] == 'YEAR'){ echo ' selected="selected" '; } ?> >pro Jahr</option>
								 </select>
							</td>
							<td>
								<input type="hidden" name="editID" id="editID" value="" />
								<input type="submit" name="submitSearch" class="inputButton0" value="Anzeigen" />
							</td>
						</tr>
					</table>
					<input type="hidden" name="submitSearchForm" id="submitSearchForm" value="1" />
					</form>
				</div>

				<div class="contentDisplay">
					<?php
						//if($_POST["submitExportForm"] == 1) {
						if(1) {
							// BOF GET DATAS
							$where = "";
							$dateField = "";
							$whereAmazon = "";
							$dateFieldAmazon = "";

							if($_POST["searchInterval"] == "WEEK"){
								$where .= "";
								$dateField = "
									CONCAT(
										IF(DATE_FORMAT(`" . TABLE_ORDER_INVOICES . "`.`orderDocumentsDocumentDate`, '%m') = '12' AND DATE_FORMAT(`" . TABLE_ORDER_INVOICES . "`.`orderDocumentsDocumentDate`, '%v') = '01', (DATE_FORMAT(`" . TABLE_ORDER_INVOICES . "`.`orderDocumentsDocumentDate`, '%Y') + 1),  DATE_FORMAT(`" . TABLE_ORDER_INVOICES . "`.`orderDocumentsDocumentDate`, '%Y')),
										'#',
										DATE_FORMAT(`" . TABLE_ORDER_INVOICES . "`.`orderDocumentsDocumentDate`, '%v')
									) AS `interval`
								";
								$whereAmazon .= "";
								$dateFieldAmazon = "
									CONCAT(
										DATE_FORMAT(`" . TABLE_AMAZON_ORDERS . "`.`BESTELLDATUM`, '%Y'),
										'#',
										DATE_FORMAT(`" . TABLE_AMAZON_ORDERS . "`.`BESTELLDATUM`, '%u')
									) AS `interval`
								";
							}
							else if($_POST["searchInterval"] == "MONTH"){
								// DATE_FORMAT(`" . TABLE_ORDER_INVOICES . "`.`orderDocumentsDocumentDate`, '%M') AS `interval`,
								$where .= "";

								$dateField = "
									CONCAT(
										DATE_FORMAT(`" . TABLE_ORDER_INVOICES . "`.`orderDocumentsDocumentDate`, '%Y'),
										'#',
										DATE_FORMAT(`" . TABLE_ORDER_INVOICES . "`.`orderDocumentsDocumentDate`, '%m')
									) AS `interval`
								";

								$whereAmazon .= "";

								$dateFieldAmazon = "
									CONCAT(
										DATE_FORMAT(`" . TABLE_AMAZON_ORDERS . "`.`BESTELLDATUM`, '%Y'),
										'#',
										DATE_FORMAT(`" . TABLE_AMAZON_ORDERS . "`.`BESTELLDATUM`, '%m')
									) AS `interval`
								";
							}
							else if($_POST["searchInterval"] == "QUARTER"){
								$where .= "";

								$dateField = "
									CONCAT(
										DATE_FORMAT(`" . TABLE_ORDER_INVOICES . "`.`orderDocumentsDocumentDate`, '%Y'),
										'#',
										IF(DATE_FORMAT(`" . TABLE_ORDER_INVOICES . "`.`orderDocumentsDocumentDate`, '%m') = '01' OR DATE_FORMAT(`" . TABLE_ORDER_INVOICES . "`.`orderDocumentsDocumentDate`, '%m') = '02' OR DATE_FORMAT(`" . TABLE_ORDER_INVOICES . "`.`orderDocumentsDocumentDate`, '%m') = '03', '1. Quartal',
											IF(DATE_FORMAT(`" . TABLE_ORDER_INVOICES . "`.`orderDocumentsDocumentDate`, '%m') = '04' OR DATE_FORMAT(`" . TABLE_ORDER_INVOICES . "`.`orderDocumentsDocumentDate`, '%m') = '05' OR DATE_FORMAT(`" . TABLE_ORDER_INVOICES . "`.`orderDocumentsDocumentDate`, '%m') = '06', '2. Quartal',
												IF(DATE_FORMAT(`" . TABLE_ORDER_INVOICES . "`.`orderDocumentsDocumentDate`, '%m') = '07' OR DATE_FORMAT(`" . TABLE_ORDER_INVOICES . "`.`orderDocumentsDocumentDate`, '%m') = '08' OR DATE_FORMAT(`" . TABLE_ORDER_INVOICES . "`.`orderDocumentsDocumentDate`, '%m') = '09', '3. Quartal',
													IF(DATE_FORMAT(`" . TABLE_ORDER_INVOICES . "`.`orderDocumentsDocumentDate`, '%m') = '10' OR DATE_FORMAT(`" . TABLE_ORDER_INVOICES . "`.`orderDocumentsDocumentDate`, '%m') = '11' OR DATE_FORMAT(`" . TABLE_ORDER_INVOICES . "`.`orderDocumentsDocumentDate`, '%m') = '12', '4. Quartal',
														''
													)
												)
											)
										)
									) AS `interval`
								";

								$whereAmazon .= "";

								$dateFieldAmazon = "
									CONCAT(
										DATE_FORMAT(`" . TABLE_AMAZON_ORDERS . "`.`BESTELLDATUM`, '%Y'),
										'#',
										IF(DATE_FORMAT(`" . TABLE_AMAZON_ORDERS . "`.`BESTELLDATUM`, '%m') = '01' OR DATE_FORMAT(`" . TABLE_AMAZON_ORDERS . "`.`BESTELLDATUM`, '%m') = '02' OR DATE_FORMAT(`" . TABLE_AMAZON_ORDERS . "`.`BESTELLDATUM`, '%m') = '03', '1. Quartal',
											IF(DATE_FORMAT(`" . TABLE_AMAZON_ORDERS . "`.`BESTELLDATUM`, '%m') = '04' OR DATE_FORMAT(`" . TABLE_AMAZON_ORDERS . "`.`BESTELLDATUM`, '%m') = '05' OR DATE_FORMAT(`" . TABLE_AMAZON_ORDERS . "`.`BESTELLDATUM`, '%m') = '06', '2. Quartal',
												IF(DATE_FORMAT(`" . TABLE_AMAZON_ORDERS . "`.`BESTELLDATUM`, '%m') = '07' OR DATE_FORMAT(`" . TABLE_AMAZON_ORDERS . "`.`BESTELLDATUM`, '%m') = '08' OR DATE_FORMAT(`" . TABLE_AMAZON_ORDERS . "`.`BESTELLDATUM`, '%m') = '09', '3. Quartal',
													IF(DATE_FORMAT(`" . TABLE_AMAZON_ORDERS . "`.`BESTELLDATUM`, '%m') = '10' OR DATE_FORMAT(`" . TABLE_AMAZON_ORDERS . "`.`BESTELLDATUM`, '%m') = '11' OR DATE_FORMAT(`" . TABLE_AMAZON_ORDERS . "`.`BESTELLDATUM`, '%m') = '12', '4. Quartal',
														''
													)
												)
											)
										)
									) AS `interval`
								";

							}
							else if($_POST["searchInterval"] == "YEAR"){
								$where .= "";

								$dateField = "
									CONCAT(
										DATE_FORMAT(`" . TABLE_ORDER_INVOICES . "`.`orderDocumentsDocumentDate`, '%Y'),
										'#',
										DATE_FORMAT(`" . TABLE_ORDER_INVOICES . "`.`orderDocumentsDocumentDate`, '%Y')
									) AS `interval`
								";

								$whereAmazon .= "";

								$dateFieldAmazon = "
									CONCAT(
										DATE_FORMAT(`" . TABLE_AMAZON_ORDERS . "`.`BESTELLDATUM`, '%Y'),
										'#',
										DATE_FORMAT(`" . TABLE_AMAZON_ORDERS . "`.`BESTELLDATUM`, '%Y')
									) AS `interval`
								";

							}

							$arrIntervals = array();
							$arrDatas = array();

							$sql = "
								SELECT
									" . $dateField . ",
									`" . TABLE_ORDER_INVOICES_DETAILS . "`.`orderDocumentDetailProductNumber` AS `productNumber`,

									`" . TABLE_ORDER_INVOICES . "`.`orderDocumentsDocumentDate` AS `date`,
									`" . TABLE_ORDER_INVOICES_DETAILS . "`.`orderDocumentDetailProductName` AS `productName`,
									`" . TABLE_ORDER_INVOICES_DETAILS . "`.`orderDocumentDetailProductKategorieID` AS `productCategoryID`,
									SUM(`" . TABLE_ORDER_INVOICES_DETAILS . "`.`orderDocumentDetailProductQuantity`) AS `countItems`

									FROM `" . TABLE_ORDER_INVOICES . "`

									LEFT JOIN `" . TABLE_ORDER_INVOICES_DETAILS . "`
									ON(`" . TABLE_ORDER_INVOICES . "`.`orderDocumentsID` = `" . TABLE_ORDER_INVOICES_DETAILS . "`.`orderDocumentDetailDocumentID`)

									WHERE 1
										AND `" . TABLE_ORDER_INVOICES . "`.`orderDocumentsStatus` != 3
										AND `" . TABLE_ORDER_INVOICES . "`.`orderDocumentsStatus` != 5
										AND `" . TABLE_ORDER_INVOICES_DETAILS . "`.`orderDocumentDetailPosType` = 'product'
										AND `" . TABLE_ORDER_INVOICES_DETAILS . "`.`orderDocumentDetailProductNumber` != ''
										" . $where . "

									GROUP BY CONCAT(`interval`, ';', `" . TABLE_ORDER_INVOICES_DETAILS . "`.`orderDocumentDetailProductNumber`)

									ORDER BY `interval` DESC, `countItems` DESC
								";

							$rs = $dbConnection->db_query($sql);

							$countTotalRows = $dbConnection->db_getMysqlNumRows($rs);

							while($ds = mysqli_fetch_assoc($rs)) {
								$arrIntervals[] = $ds["interval"];
								$arrProductNumbers[] = $ds["productNumber"];
								foreach(array_keys($ds) as $field){
									$arrDatas[$ds["interval"]][$ds["productNumber"]][$field] = $ds[$field];
								}
							}

							$sqlAmazon = "
									SELECT
										" . $dateFieldAmazon . ",
										`" . TABLE_AMAZON_ORDERS . "`.`SKU` AS `productNumber`,

										`" . TABLE_AMAZON_ORDERS . "`.`BESTELLDATUM` AS `date`,
										`" . TABLE_AMAZON_ORDERS . "`.`PRODUKTNAME` AS `productName`,
										'' AS `productCategoryID`,
										SUM(`" . TABLE_AMAZON_ORDERS . "`.`MENGE`) AS `countItems`

									FROM `" . TABLE_AMAZON_ORDERS . "`

									WHERE 1
										AND `" . TABLE_AMAZON_ORDERS . "`.`SKU` != ''
										" . $whereAmazon . "

									GROUP BY CONCAT(`interval`, ';', `" . TABLE_AMAZON_ORDERS . "`.`SKU`)

									ORDER BY `interval` DESC, `countItems` DESC
								";

							$rsAmazon = $dbConnection->db_query($sqlAmazon);

							$countTotalRowsAmazon = $dbConnection->db_getMysqlNumRows($rsAmazon);

							while($ds = mysqli_fetch_assoc($rsAmazon)) {
								$arrIntervals[] = $ds["interval"];
								$arrProductNumbers[] = $ds["productNumber"];
								foreach(array_keys($ds) as $field){
									$arrDatas[$ds["interval"]][$ds["productNumber"]][$field] = $ds[$field];
								}
							}

							if(!empty($arrIntervals)){
								$arrIntervals = array_unique($arrIntervals);
								#arsort($arrIntervals);
							}
							if(!empty($arrProductNumbers)){
								$arrProductNumbers = array_unique($arrProductNumbers);
								#arsort($arrProductNumbers);
							}

							if($countTotalRows > 0) {
							?>
								<!-- BOF GRAPH ELEMENTS -->
								<!--
								<script type="text/javascript" language="javascript" src="<?php echo PATH_RGRAPH; ?>libraries/RGraph.common.core.js" ></script>
								<script type="text/javascript" language="javascript" src="<?php echo PATH_RGRAPH; ?>libraries/RGraph.bar.js" ></script>
								<script type="text/javascript" language="javascript" src="<?php echo PATH_RGRAPH; ?>libraries/RGraph.line.js" ></script>

								<div class="adminEditArea">
									<h2>Anzahl der bestellten Produkte</h2>
									<canvas id="cvs_dataCountItems" width="900" height="250">[No canvas support]</canvas>
								</div>
								-->
								<!-- EOF GRAPH ELEMENTS -->

							<?php
								echo '<table width="100%" cellpadding="0" cellspacing="0" class="displayOrders">';

								echo '<thead>';
								echo '<tr>';
								echo '<th style="width:45px">#</th>';
								echo '<th style="width:80px">Zeitraum</th>';
								echo '<th>Artikel-Nummer</th>';
								echo '<th>Artikel-Name</th>';
								echo '<th>Anzahl</th>';
								// echo '<th>Betrag</th>';
								echo '<th style="width:50px">Info</th>';
								echo '</tr>';
								echo '</thead>';

								echo '<tbody>';

								$countRow = 0;
								$thisMarker = "";
								$thisMarker2 = "";

								foreach($arrIntervals as $thisIntervalKey => $thisIntervalValue) {
									foreach($arrProductNumbers as $thisProductNumberKey => $thisProductNumberValue) {
										if($arrDatas[$thisIntervalValue][$thisProductNumberValue]['countItems'] > 0){
											if($countRow == 0) {

											}

											$rowClass = 'row0';
											if($countRow%2 == 0) {
												$rowClass = 'row1';
											}
											$arrTemp = explode('#', $thisIntervalValue);
											if($thisMarker != $arrTemp[0]) {
												$thisMarker = $arrTemp[0];
												echo '<tr><td colspan="8" class="tableRowTitle1">' . $arrTemp[0] . '</td></tr>';
											}

											if($_POST["searchInterval"] != "YEAR") {
												if($thisMarker2 != $arrTemp[1]) {
													if($countRow != 0){
														echo '<tr>';
														echo '<td colspan="6"><hr /></td>';
														echo '</tr>';
													}
													$thisMarker2 = $arrTemp[1];
													echo '<tr><td colspan="8" class="tableRowTitle2">';
													if($_POST["searchInterval"] == "WEEK"){ echo 'KW '; }
													if($_POST["searchInterval"] == "MONTH"){
														$thisMonth = getTimeNames($arrTemp[1], 'month', 'long');
														echo $thisMonth . ' ';
													}
													echo $arrTemp[1] . ' ' . $arrTemp[0] ;
													echo '</td></tr>';
												}
											}

											echo '<tr class="'.$rowClass.'">';

											echo '<td style="text-align:right;"><b>';
											echo ($countRow + 1);
											echo '.</b></td>';
											echo '<td>';

											if($_POST["searchInterval"] == "MONTH"){
												$thisMonth = getTimeNames($arrTemp[1], 'month', 'long');
												echo $thisMonth;
											}
											else {
												if($_POST["searchInterval"] == "WEEK"){ echo 'KW '; }
												echo $arrTemp[1];
											}
											echo '</td>';

											echo '<td style="white-space:nowrap;text-align:right;background-color:#FEFFAF;">';
											echo '' . $arrDatas[$thisIntervalValue][$thisProductNumberValue]['productNumber'] . '';
											echo '</td>';

											echo '<td style="text-align:left;background-color:#FEFFAF;">';
											echo '' . $arrDatas[$thisIntervalValue][$thisProductNumberValue]['productName'] . '';
											echo '</td>';
											echo '<td style="text-align:right;background-color:#FEFFAF;">';
											echo '' . number_format($arrDatas[$thisIntervalValue][$thisProductNumberValue]['countItems'], 0, ',', '.') . '';
											echo '</td>';

											// echo '<td style="text-align:right;background-color:#FEFFAF;">';
											// echo '' . number_format($arrDatas[$thisIntervalValue]['invoices']['totalPrice'], 2, ',', '.') . ' &euro;';
											// echo '</td>';

											echo '<td>';
											echo '<span class="toolItem">';
											echo '<img src="layout/icons/iconInfo.png" class="buttonShopOrderInfo" alt="Details" title="Detailinformationen ansehen">';
											echo '<div class="orderDetailsContent" style="display:none; visibility:hidden;">';
											echo '</div>';
											echo '</span>';
											echo '</td>';
											echo '</tr>';
											$countRow++;
										}
									}
								}
								echo '</tbody>';
								echo '</table>';
							}
							else {
								$warningMessage .= 'F&uuml;r den gew&auml;hlten Zeitraum wurden keine Daten gefunden.';
							}
							// EOF GET DATAS
						}
					?>
					<?php displayMessages(); ?>
				</div>

			</div>
		</div>
		<div class="clear"></div>
	</div>
</div>
<script language="javascript" type="text/javascript">
	$(document).ready(function() {
		$(function() {
			colorRowMouseOver('.displayOrders tbody tr');
			$('.buttonShopOrderInfo').css('cursor', 'pointer');
			$('.buttonShopOrderInfo').click(function () {
				loadOrderDetails($(this), '<?php echo BASEPATH; ?>', 'Details der Bestellung');
			});

			// BOF GET DATAS FOR GRAPH CASH
			<?php
				/*
				if(!empty($arrDatasGraph)){
					$arrDatasGraph["countItems"] = array_reverse($arrDatasGraph["countItems"], true);
					$arrDatasGraph["totalPrice"] = array_reverse($arrDatasGraph["totalPrice"], true);
					$arrLabelsGraph = array_reverse($arrLabelsGraph , true);
				}
				*/
			?>

			// createGraph('cvs_dataCountItems', [<?php echo implode(",", array_values($arrDatasGraph["countItems"])); ?>], [<?php echo "'" . implode("','", $arrLabelsGraph) . "'"; ?>], 'Line', [['green','red']]);
			// createGraph('cvs_dataTotalPrice', [<?php echo implode(",", array_values($arrDatasGraph["totalPrice"])); ?>], [<?php echo "'" . implode("','", $arrLabelsGraph) . "'"; ?>], 'Line', [['green','red']]);

		// EOF GET DATAS FOR GRAPH CASH
		});
		$(function() {
			$('#tabs').tabs();
		});
	});
</script>
<?php require_once('inc/footerHTML.inc.php'); ?>