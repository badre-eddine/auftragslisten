<?php
	require_once('inc/requires.inc.php');

	if(!$arrGetUserRights["displayOnlineUsers"]) {
		header('location: ' . PAGE_EXIT_LOCATION);
		exit;
	}

	$jswindowMessage = "";
	$warningMessage = '';
	$errorMessage = '';
	$successMessage = '';
	$infoMessage = '';

	$dbConnection = new DB_Connection();
	$db_open = $dbConnection->db_connect();

		if($_GET["displayMode"] == "datasOfflineUser") {
			// BOF ALL OFFLINE USERS
			$sql ="SELECT
						`temp`.`usersOnlineUserID`,

						`temp`.`usersOnlineID`,
						MAX(`temp`.`usersOnlineDateTime`) AS `usersOnlineDateTime`,
						`temp`.`usersOnlineMandator`,
						`temp`.`usersOnlineType`,
						IF(`temp`.`usersOnlineType` = 'login', 'online', 'offline') AS `usersOnlineMode`,

						`temp`.`usersID`,
						`temp`.`usersFirstName`,
						`temp`.`usersLastName`,
						`temp`.`usersLogin`

						FROM (
							SELECT
								`usersOnlineUserID`,
								`usersOnlineType`,

								`usersOnlineID`,
								MAX(`usersOnlineDateTime`) AS `usersOnlineDateTime`,
								`usersOnlineMandator`,
								IF(`usersOnlineType` = 'login', 'online', 'offline') AS `usersOnlineMode`,

								`usersID`,
								`usersFirstName`,
								`usersLastName`,
								`usersLogin`

							FROM `" . TABLE_USERS_ONLINE . "`
							LEFT JOIN `" . TABLE_USERS . "`
							ON(`" . TABLE_USERS_ONLINE . "`.`usersOnlineUserID` = `" . TABLE_USERS . "`.`usersID`)

							WHERE 1
							GROUP BY CONCAT (`usersOnlineUserID`, '_', `usersOnlineType`)

							ORDER BY `usersOnlineDateTime`  DESC
						) AS `temp`

						WHERE `temp`.`usersOnlineType` = 'logout'

						GROUP BY `usersOnlineUserID`
			";
			// EOF ALL OFFLINE USERS
		}
		else if($_GET["displayMode"] == "datasOnlineUser") {
			// BOF ALL ONLINE USERS
			$sql ="SELECT
						`temp`.`usersOnlineID`,
						`temp`.`usersOnlineUserID`,
						MAX(`temp`.`usersOnlineDateTime`) AS `usersOnlineDateTime`,
						`temp`.`usersOnlineMandator`,
						`temp`.`usersOnlineType`,
						IF(`temp`.`usersOnlineType` = 'login', 'online', 'offline') AS `usersOnlineMode`,

						`temp`.`usersID`,
						`temp`.`usersFirstName`,
						`temp`.`usersLastName`,
						`temp`.`usersLogin`

						FROM (
							SELECT
							`usersOnlineID`,
							`usersOnlineUserID`,
							MAX(`usersOnlineDateTime`) AS `usersOnlineDateTime`,
							`usersOnlineMandator`,
							`usersOnlineType`,
							IF(`usersOnlineType` = 'login', 'online', 'offline') AS `usersOnlineMode`,

							`usersID`,
							`usersFirstName`,
							`usersLastName`,
							`usersLogin`

							FROM `" . TABLE_USERS_ONLINE . "`
							LEFT JOIN `" . TABLE_USERS . "`
							ON(`" . TABLE_USERS_ONLINE . "`.`usersOnlineUserID` = `" . TABLE_USERS . "`.`usersID`)

							WHERE 1
							GROUP BY CONCAT (`usersOnlineUserID`, '_', `usersOnlineType`)

							ORDER BY `usersOnlineDateTime`  DESC
						) AS `temp`

						WHERE `temp`.`usersOnlineType` = 'login'

						GROUP BY `usersOnlineUserID`
			";
			// EOF ALL ONLINE USERS
		}
		else if($_GET["displayMode"] == "datasAllUser") {
			// BOF ALL  USERS
			$sql ="SELECT
						`temp`.`usersOnlineID`,
						`temp`.`usersOnlineUserID`,
						MAX(`temp`.`usersOnlineDateTime`) AS `usersOnlineDateTime`,
						`temp`.`usersOnlineMandator`,
						`temp`.`usersOnlineType`,
						IF(`temp`.`usersOnlineType` = 'login', 'online', 'offline') AS `usersOnlineMode`,

						`temp`.`usersID`,
						`temp`.`usersFirstName`,
						`temp`.`usersLastName`,
						`temp`.`usersLogin`

						FROM (
							SELECT
							`usersOnlineID`,
							`usersOnlineUserID`,
							MAX(`usersOnlineDateTime`) AS `usersOnlineDateTime`,
							`usersOnlineMandator`,
							`usersOnlineType`,
							IF(`usersOnlineType` = 'login', 'online', 'offline') AS `usersOnlineMode`,

							`usersID`,
							`usersFirstName`,
							`usersLastName`,
							`usersLogin`

							FROM `" . TABLE_USERS_ONLINE . "`
							LEFT JOIN `" . TABLE_USERS . "`
							ON(`" . TABLE_USERS_ONLINE . "`.`usersOnlineUserID` = `" . TABLE_USERS . "`.`usersID`)

							WHERE 1
							GROUP BY CONCAT (`usersOnlineUserID`, '_', `usersOnlineType`)

							ORDER BY `usersOnlineDateTime`  DESC
						) AS `temp`

						GROUP BY `usersOnlineUserID`
			";
			// EOF ALL  USERS
		}
		else {
			// BOF ALL DATAS
			$sql = "SELECT

					`usersOnlineID`,
					`usersOnlineUserID`,
					`usersOnlineDateTime`,
					`usersOnlineMandator`,
					`usersOnlineType`,
					if(`usersOnlineType` = 'login', 'online', 'offline') AS `usersOnlineMode`,

					`usersID`,
					`usersFirstName`,
					`usersLastName`,
					`usersLogin`

					FROM `" . TABLE_USERS_ONLINE . "`
					LEFT JOIN `" . TABLE_USERS . "`
					ON(`" . TABLE_USERS_ONLINE . "`.`usersOnlineUserID` = `" . TABLE_USERS . "`.`usersID`)

					WHERE 1

					ORDER BY `usersOnlineDateTime` DESC
				";
			// EOF ALL DATAS
		}

	// BOF READ ALL DATAS
	if(1) {

		$rs = $dbConnection->db_query($sql);

		$pagesCount = ceil($dbConnection->db_getMysqlNumRows($rs) / MAX_USERS_PER_PAGE);

		if($pagesCount > 1) {
			if($_REQUEST["page"] == "" || $_REQUEST["page"] == 0) {
				$_REQUEST["page"] = 1;
			}
		}
		else {
			$_REQUEST["page"] = 1;
		}

		if(MAX_USERS_PER_PAGE > 0) {
			$sql .= " LIMIT " . (($_REQUEST["page"] - 1) * MAX_USERS_PER_PAGE) . ", " . MAX_USERS_PER_PAGE." ";
		}

		$rs = $dbConnection->db_query($sql);

		while($ds = mysqli_fetch_assoc($rs)) {
			foreach(array_keys($ds) as $field) {
				$arrUserOnlineDatas[$ds["usersOnlineID"]][$field] = $ds[$field];
			}
		}
	}
	// EOF READ ALL DATAS
?>
<?php
	require_once('inc/headerHTML.inc.php');
	if($_REQUEST["editID"] == "NEW") {
		$thisTitle = "User anlegen";
	}
	else {
		$thisTitle = "User Online-Log";
	}
	$headerHTML = preg_replace("/{###TITLE###}/", strip_tags($thisTitle), $headerHTML);
	echo $headerHTML;

	require_once(FILE_MENUE_TOP);
?>
<div id="mainArea">
	<div id="mainContent">
		<?php require_once(FILE_MENUE_SIDEBAR); ?>
		<div id="contentArea">
			<div id="contentAreaElements">
				<h1><?php if(SHOW_PAGE_TITLE_ICONS) { echo '<img src="' . PATH_ICONS_MENUE_TITLES . 'online.png" alt="" />'; } ?> <?php echo $thisTitle; ?></h1>

				<table border="0" cellpadding="0" cellspacing="0" width="100%">
					<tr>
						<td>
							<span class="buttonShowAllColumns"><a href="<?php echo $_SERVER["PHP_SELF"]; ?>?displayMode=datasAll">Alle Daten anzeigen</a></span>
							<span class="buttonHideAllColumns"><a href="<?php echo $_SERVER["PHP_SELF"]; ?>?displayMode=datasAllUser">Nur User anzeigen</a></span>
							<span class="buttonHideAllColumns"><a href="<?php echo $_SERVER["PHP_SELF"]; ?>?displayMode=datasOnlineUser">Nur Online-User anzeigen</a></span>
							<span class="buttonHideAllColumns"><a href="<?php echo $_SERVER["PHP_SELF"]; ?>?displayMode=datasOfflineUser">Nur Offline-User anzeigen</a></span>
						</td>
						<td style="text-align:right; font-size:10px;">
							Anzahl Zeilen insgesamt: <?php echo $countRows; ?>
						</td>
					<tr>
				</table>

				<div class="contentDisplay">
					<?php displayMessages(); ?>
					<?php
						if(!empty($arrUserOnlineDatas)) {

							if($pagesCount > 1 && $_REQUEST["displayOrderStatus"] == "") {
								include(FILE_MENUE_PAGES);
							}

							echo '<table cellpadding="0" cellspacing="0" border="0" width="100%" class="displayOrders">';
							echo '<tr>';
							echo '<th style="width:45px;text-align:right;">#</th>';
							echo '<th>Zeit</th>';
							echo '<th>Name</th>';
							echo '<th>Mandator</th>';
							echo '<th>Aktion</th>';
							echo '</tr>';

							$count = 0;
							foreach($arrUserOnlineDatas as $thisKey => $thisValue) {
								if($count%2 == 0){ $rowClass = 'row0'; }
								else { $rowClass = 'row1'; }

								echo '<tr class="'.$rowClass.'">';
								echo '<td style="text-align:right;"><b>' . ($count + 1). '.</b></td>';
								echo '<td>' . $thisValue["usersOnlineDateTime"] . '</td>';
								echo '<td>' . $thisValue["usersFirstName"] . ' ' . $thisValue["usersLastName"] . '</td>';
								echo '<td>' . $thisValue["usersOnlineMandator"] . '</td>';
								echo '<td style="text-align:left;"><img src="layout/icons/' . $thisValue["usersOnlineMode"] . '.png" alt="' . $thisValue["usersOnlineType"] . '" title="' . ucfirst($thisValue["usersOnlineType"]) . '" /> ' . ucfirst($thisValue["usersOnlineType"]) . '</td>';
								echo '</tr>';
								$count++;
							}
						}
						echo '</table>';

						if($pagesCount > 1 && $_REQUEST["displayOrderStatus"] == "") {
							include(FILE_MENUE_PAGES);
						}

					?>

				</div>
			</div>
		</div>
		<div class="clear"></div>
	</div>
</div>

<script language="javascript" type="text/javascript">
	$(document).ready(function() {
		colorRowMouseOver('.displayOrders tbody tr');
	});
</script>

<?php require_once('inc/footerHTML.inc.php'); ?>