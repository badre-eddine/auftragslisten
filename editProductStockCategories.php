<?php
	require_once('inc/requires.inc.php');

	if(!$arrGetUserRights["displayStocks"] && !$arrGetUserRights["editStocks"]) {
		header('location: ' . PAGE_EXIT_LOCATION);
		exit;
	}

	$jswindowMessage = "";
	$warningMessage = '';
	$errorMessage = '';
	$successMessage = '';
	$infoMessage = '';

	$dbConnection = new DB_Connection();
	$db_open = $dbConnection->db_connect();

	// BOF READ VERTRETER
		$arrAgentDatas = getAgents();
		$arrAgentDatas2 = getAgents2();
	// EOF READ VERTRETER

	// BOF READ PAYMENT TYPES
		$arrPaymentTypeDatas = getPaymentTypes();
	// EOF READ PAYMENT TYPES

	// BOF READ CUSTOMER GROUP DATAS
		$arrCustomerGroupDatas = getCustomerGroups();
	// EOF READ CUSTOMER GROUP DATAS

	// BOF READ CUSTOMER TYPES
		$arrCustomerTypeDatas = getCustomerTypes();
	// EOF READ CUSTOMER TYPES

	// BOF STORE PRODUCTS_CAT
		if($_POST["storeDatas"] != ""){
			$thisStoreID = $_POST["editID"];
			// BOF CREATE LEVEL ID
				if($thisStoreID == 'NEW' || $_POST["editProductsCategoryParentID"] != $_POST["originalProductsCategoryParentID"]){
					if($_POST["editProductsCategoryParentID"] == '000'){
						// BOF FIND LEVEL_ID
							// BOF GET GAP LEVEL ID 3 CHARS
								$thisCatLength = 3;
								$where = " AND LENGTH(`stockproductcategories_1`.`stockProductCategoriesLevelID`) = " . $thisCatLength . " ";
							// EOF GET GAP LEVEL ID 3 CHARS
						// EOF FIND LEVEL_ID
					}
					else if($_POST["editProductsCategoryParentID"] != '000'){
						// BOF GET GAP LEVEL ID 6 CHARS
							$thisCatLength = 6;
							$where = "
									AND LENGTH(`stockproductcategories_1`.`stockProductCategoriesLevelID`) = " . $thisCatLength . "
									AND `stockproductcategories_1`.`stockProductCategoriesLevelID` LIKE '" . $_POST["editProductsCategoryParentID"] . "%'
								";
						// EOF GET GAP LEVEL ID 6 CHARS
					}

					$sql_getLevel_ID = "
							SELECT
								/*
								`tempTable`.`stockProductCategoriesLevelID_beforeGap`,
								`tempTable`.`stockProductCategoriesLevelID_fillGap`,
								*/

								CONCAT(REPEAT('0', (" . $thisCatLength . " - LENGTH(`tempTable`.`newStockProductCategoriesLevelID`))), `tempTable`.`newStockProductCategoriesLevelID`)  AS `newStockProductCategoriesLevelID`

								FROM (

									SELECT

										CONVERT(`stockproductcategories_1`.`stockProductCategoriesLevelID`, SIGNED INTEGER) AS `stockProductCategoriesLevelID_beforeGap`,
										(CONVERT(`stockproductcategories_1`.`stockProductCategoriesLevelID`, SIGNED INTEGER) + 1) AS `stockProductCategoriesLevelID_fillGap`,

										(CONVERT(`stockproductcategories_1`.`stockProductCategoriesLevelID`, SIGNED INTEGER) + 1) AS `newStockProductCategoriesLevelID`

									FROM `common_stockproductcategories` AS `stockproductcategories_1`
									WHERE 1
										" . $where . "
										AND (
											SELECT
												CONVERT(`stockproductcategories_2`.`stockProductCategoriesLevelID`, SIGNED INTEGER) AS `stockProductCategoriesLevelID`
												FROM `common_stockproductcategories` AS `stockproductcategories_2`
												WHERE 1
													AND CONVERT(`stockproductcategories_2`.`stockProductCategoriesLevelID`, SIGNED INTEGER) = (CONVERT(`stockproductcategories_1`.`stockProductCategoriesLevelID`, SIGNED INTEGER) + 1)
										) IS NULL
									ORDER BY `stockproductcategories_1`.`stockProductCategoriesLevelID`
									LIMIT 1
								) AS `tempTable`
						";
					$rs_getLevel_ID = $dbConnection->db_query($sql_getLevel_ID);
					list($getNewStockProductCategoriesLevelID) = mysqli_fetch_array($rs_getLevel_ID);

					if($getNewStockProductCategoriesLevelID == ''){
						$getNewStockProductCategoriesLevelID = "1";
						$getNewStockProductCategoriesLevelID = str_repeat("0", (3 - strlen($getNewStockProductCategoriesLevelID))) . $getNewStockProductCategoriesLevelID;

						if($thisCatLength == 3){

						}
						else if($thisCatLength == 6){
							$getNewStockProductCategoriesLevelID = $_POST["editProductsCategoryParentID"] . $getNewStockProductCategoriesLevelID;
						}
					}

					$thisStockProductCategoriesLevelID = $getNewStockProductCategoriesLevelID;
					$thisStockProductCategoriesParentID = $_POST["editProductsCategoryParentID"];
					$thisStockProductCategoriesRelationID = $_POST["editProductCategoriesRelationID"];
				}
				else {
					$thisStockProductCategoriesLevelID = $_POST["editProductsCategoryLevelID"];
					$thisStockProductCategoriesParentID = $_POST["editProductsCategoryParentID"];
					$thisStockProductCategoriesRelationID = $_POST["editProductCategoriesRelationID"];
				}
			// EOF CREATE LEVEL ID

			if($thisStoreID == 'NEW'){
				$thisStoreID = '%';
			}

			$sql_storeCat = "
					REPLACE INTO `" . TABLE_STOCK_PRODUCT_CATEGORIES . "` (
							
							`stockProductCategoriesLevelID`,
							`stockProductCategoriesParentID`,
							`stockProductCategoriesRelationID`,
							`stockProductCategoriesName`,
							`stockProductCategoriesName_TR`,
							`stockProductCategoriesShortName`,
							`stockProductCategoriesProductNumber`,
							`stockProductCategoriesSort`,
							`stockProductCategoriesActive`,
							`stockProductCategoriesContainsStockProducts`
					)
					VALUES (
							
							'" . $thisStockProductCategoriesLevelID . "',
							'" . $thisStockProductCategoriesParentID . "',
							'" . $thisStockProductCategoriesRelationID . "',
							'" . $_POST["editProductsCategoryName"] . "',
							'" . $_POST["editProductsCategoryName_TR"] . "',
							'" . $_POST["editProductsCategoryShortName"] . "',
							'" . $_POST["editProductCategoriesProductNumber"] . "',
							'" . $_POST["editProductsCategorySort"] . "',
							'" . $_POST["editProductsCategoryStatus"] . "',
							'" . $_POST["editProductCategoryContainsStockProducts"] . "'
					)
				";
			$rs_storeCat = $dbConnection->db_query($sql_storeCat);

			if($rs_storeCat){
				$successMessage .= 'Die Kategorie wurde gespeichert.' . '<br />';
			}
			else {
				$errorMessage .= 'Die Kategorie konnte nicht gespeichert werden.' . '<br />';
			}
			unset($_REQUEST["editID"]);
		}
	// EOF STORE PRODUCTS_CAT



	// BOF CANCEL PRODUCTS_CAT
		if($_POST["cancelDatas"] != ""){
			unset($_REQUEST["editID"]);
		}
	// EOF CANCEL PRODUCTS_CAT

	// BOF DELETE PRODUCTS_CAT
		if($_POST["deleteDatas"] != ""){
			$sql_deleteCat = "
					DELETE
						FROM `" . TABLE_STOCK_PRODUCT_CATEGORIES . "`
						WHERE 1
							AND `stockProductCategoriesID` = '" . $_POST["editID"] . "'
				";
			$rs_deleteCat = $dbConnection->db_query($sql_deleteCat);
			if($rs_deleteCat){
				$successMessage .= 'Die Kategorie wurde entfernt.' . '<br />';
			}
			else {
				$errorMessage .= 'Die Kategorie konnte nicht entfernt werden.' . '<br />';
			}
			unset($_REQUEST["editID"]);
		}
	// EOF DELETE PRODUCTS_CAT

	// BOF GET ALL PRODUCTS_CAT	LEVEL 1
		$sql_getCat = "
				SELECT
						`stockProductCategoriesID`,
						`stockProductCategoriesLevelID`,
						`stockProductCategoriesParentID`,
						`stockProductCategoriesRelationID`,						
						`stockProductCategoriesName`,
						`stockProductCategoriesName_TR`,
						`stockProductCategoriesShortName`,
						`stockProductCategoriesProductNumber`,
						`stockProductCategoriesSort`,
						`stockProductCategoriesActive`,
						`stockProductCategoriesContainsStockProducts`
					FROM `" . TABLE_STOCK_PRODUCT_CATEGORIES . "`
					WHERE 1
						AND `stockProductCategoriesParentID` = '000'

					ORDER BY
						`stockProductCategoriesSort`,
						`stockProductCategoriesName`,
						`stockProductCategoriesParentID`,
						`stockProductCategoriesLevelID`				
						
			";
		$rs_getCat = $dbConnection->db_query($sql_getCat);

		$arrProductsCategoryData = array();

		while($ds_getCat = mysqli_fetch_assoc($rs_getCat)){
			foreach(array_keys($ds_getCat) as $field){
				$arrProductsCategoryData[$ds_getCat["stockProductCategoriesID"]][$field] = $ds_getCat[$field];
			}
		}
	// EOF GET ALL PRODUCTS_CAT LEVEL 1

	// BOF GET SELECTED PRODUCTS_CAT
		if($_REQUEST["editID"] != '' && $_REQUEST["editID"] != 'NEW'){
			
			$sql_getCat = "
					SELECT
							`stockProductCategoriesID`,
							`stockProductCategoriesLevelID`,
							`stockProductCategoriesParentID`,
							`stockProductCategoriesRelationID`,
							`stockProductCategoriesName`,
							`stockProductCategoriesName_TR`,
							`stockProductCategoriesShortName`,
							`stockProductCategoriesProductNumber`,
							`stockProductCategoriesSort`,
							`stockProductCategoriesActive`,
							`stockProductCategoriesContainsStockProducts`
							
						FROM `" . TABLE_STOCK_PRODUCT_CATEGORIES . "`

						WHERE 1
							 AND `stockProductCategoriesID` = '" . $_REQUEST["editID"] . "'

						ORDER BY
							`stockProductCategoriesParentID`,
							`stockProductCategoriesLevelID`,
							`stockProductCategoriesSort`,
							`stockProductCategoriesName`
							
						LIMIT 1
				";
			$rs_getCat = $dbConnection->db_query($sql_getCat);			

			$arrSelectedProductsCategory = array();
			while($ds_getCat = mysqli_fetch_assoc($rs_getCat)){
				foreach(array_keys($ds_getCat) as $field){
					$arrSelectedProductsCategory[$field] = $ds_getCat[$field];
				}
			}			
			
			// BOF GET ALL PRODUCTS_CAT	LEVEL 2		
				$sql_getCat = "
						SELECT
								`stockProductCategoriesID`,
								`stockProductCategoriesLevelID`,
								`stockProductCategoriesParentID`,
								`stockProductCategoriesRelationID`,
								`stockProductCategoriesName`,
								`stockProductCategoriesName_TR`,
								`stockProductCategoriesShortName`,
								`stockProductCategoriesProductNumber`,
								`stockProductCategoriesSort`,
								`stockProductCategoriesActive`,
								`stockProductCategoriesContainsStockProducts`
								
							FROM `" . TABLE_STOCK_PRODUCT_CATEGORIES . "`
							WHERE 1
								AND `stockProductCategoriesParentID` != '000'
								AND `stockProductCategoriesParentID` = '" . $arrSelectedProductsCategory["stockProductCategoriesLevelID"] . "'

							ORDER BY
								`stockProductCategoriesSort`,
								`stockProductCategoriesName`,
								`stockProductCategoriesParentID`,
								`stockProductCategoriesLevelID`	
					";
				$rs_getCat = $dbConnection->db_query($sql_getCat);

				$arrSelectedProductsSubCategoryData = array();

				while($ds_getCat = mysqli_fetch_assoc($rs_getCat)){
					foreach(array_keys($ds_getCat) as $field){
						$arrSelectedProductsSubCategoryData[$ds_getCat["stockProductCategoriesLevelID"]][$field] = $ds_getCat[$field];
					}
				}				
			// EOF GET ALL PRODUCTS_CAT LEVEL 2			
			
			$categoryIsDeletable = false;
			
			if(!empty($arrSelectedProductsSubCategoryData)){
				$categoryIsDeletable = false;
			}
			else {
				$categoryIsDeletable = true;
			}
		}
	// EOF GET SELECTED PRODUCTS_CAT
?>
<?php
	require_once('inc/headerHTML.inc.php');
	$thisTitle = "Artikel-Kategorien";

	$headerHTML = preg_replace("/{###TITLE###}/", strip_tags($thisTitle), $headerHTML);
	echo $headerHTML;

	require_once(FILE_MENUE_TOP);

?>

<div id="mainArea">
	<div id="mainContent">
		<?php require_once(FILE_MENUE_SIDEBAR); ?>
		<div id="contentArea">
			<a name="top"></a>
			<div id="contentAreaElements">
				<h1><?php if(SHOW_PAGE_TITLE_ICONS) { echo '<img src="' . PATH_ICONS_MENUE_TITLES . 'shipping.png" alt="" />'; } ?> <?php echo $thisTitle; ?></h1>

				<?php displayMessages(); ?>
				<?php if($_REQUEST["editID"] == "" && $arrGetUserRights["editStocks"] == '1') { ?>
					<div class="menueActionsArea">
						<a href="<?php echo PAGE_PRODUCTS_STOCK_CATEGORIES; ?>?editID=NEW" class="linkButton">Neue Kategorie anlegen</a>
						<div class="clear"></div>
					</div>
				<?php } ?>

				<div class="adminInfo">Datensatz-ID: <?php echo $_REQUEST["editID"]; ?></div>

				<div class="contentDisplay">
					<div class="adminEditArea">
					<?php
						if($_REQUEST["editID"] == ''){
							$countRow = 0;

							if(!empty($arrProductsCategoryData)){
								echo '<table cellpadding="0" cellspacing="0" border="0" width="100%" class="displayOrders">
									<tr>
										<th style="width:45px;text-align:right;">#</th>
										<th>Bezeichnung</th>
										<th>Lagerartikel?</th>
										<th>Kurz</th>										
										<th>Sortierung</th>
										<th>Aktiv?</th>
										<th>LID</th>
										<th>PID</th>
										<th>RID</th>
										<th>Info</th>
									</tr>
								';

								foreach($arrProductsCategoryData as $thisProductsCategoryKey => $thisProductsCategoryValue){
									if($countRow%2 == 0){ $rowClass = 'row0'; }
									else { $rowClass = 'row1'; }
									
									if($thisProductsSubCategoryValue["stockProductCategoriesActive"] != '1'){
										$rowClass = 'row6';
									}

									// BOF CATEGORIES
										echo '<tr style="font-weight:bold;" class="row2">';

										echo '<td style="text-align:right;">';
										echo '<b>' . ($countRow + 1). '.</b>';
										echo '</td>';

										echo '<td>';
										echo '&#9632; ' . htmlentities($thisProductsCategoryValue["stockProductCategoriesName"]);
										echo '</td>';

										echo '<td>';								
										
										if($thisProductsCategoryValue["stockProductCategoriesContainsStockProducts"] == '1'){
											$thisStatusImage = 'stockProduct.png';
											$thisStatusTitle = 'Diese Kategorie enth&auml;lt Lagerartikel!';
										}
										else {
											$thisStatusImage = 'noStockProduct.png';
											$thisStatusTitle = 'Diese Kategorie enth&auml;lt keine Lagerartikel!';
										}

										echo '<img src="layout/icons/' . $thisStatusImage . '" width="16" height="16" alt="' . $thisStatusTitle . '" title="' . $thisStatusTitle . '" />';
										
										echo '</td>';
										
										echo '<td>';
										echo $thisProductsCategoryValue["stockProductCategoriesShortName"];
										echo '</td>';
										
										#echo '<td>';
										#echo $thisProductsCategoryValue["stockProductCategoriesProductNumber"];
										#echo '</td>';

										echo '<td>';
										echo $thisProductsCategoryValue["stockProductCategoriesSort"];
										echo '</td>';

										echo '<td>';
										// echo $thisProductsCategoryValue["stockProductCategoriesActive"];
										if($thisProductsCategoryValue["stockProductCategoriesActive"] == '1'){
											$thisStatusImage = 'iconOk.png';
											$thisStatusTitle = 'Diese Kategorie ist aktiv!';
										}
										else {
											$thisStatusImage = 'iconNotOk.png';
											$thisStatusTitle = 'Diese Kategorie ist nicht aktiv!';
										}

										echo '<img src="layout/icons/' . $thisStatusImage . '" width="16" height="16" alt="' . $thisStatusTitle . '" title="' . $thisStatusTitle . '" />';
										echo '</td>';

										echo '<td>';
										echo $thisProductsCategoryValue["stockProductCategoriesLevelID"];
										echo '</td>';

										echo '<td>';
										echo $thisProductsCategoryValue["stockProductCategoriesParentID"];
										echo '</td>';
										
										echo '<td>';
										echo $thisProductsCategoryValue["stockProductCategoriesRelationID"];
										echo '</td>';

										echo '<td>';
										if($arrGetUserRights["editStocks"]) {
											echo '<span class="toolItem"><a href="' . PAGE_PRODUCTS_STOCK_CATEGORIES . '?editID=' . $thisProductsCategoryValue["stockProductCategoriesID"] . '"><img src="layout/icons/iconEdit.gif" width="16" height="16" title="Kategorie  bearbeiten" alt="Bearbeiten" /></a></span>';
										}
										else {
											echo '<span class="toolItem"><img src="layout/spacer.gif" width="16" height="16" alt="" /></span>';
										}
										echo '</td>';

										echo '</tr>';

										$countRow++;
									// EOF CATEGORIES

									// BOF SUB CATEGORIES
										foreach($arrProductsSubCategoryData[$thisProductsCategoryValue["stockProductCategoriesLevelID"]] as $thisProductsSubCategoryKey => $thisProductsSubCategoryValue){
											if($countRow%2 == 0){ $rowClass = 'row0'; }
											else { $rowClass = 'row1'; }

											echo '<tr ' . $thisStyle . ' class="'.$rowClass.'">';

											echo '<td style="text-align:right;">';
											echo '<b>' . ($countRow + 1). '.</b>';
											echo '</td>';

											echo '<td>';
											echo '<img src="layout/icons/subCat.png" width="19" height="11" alt="" /> ' . $thisProductsSubCategoryValue["stockProductCategoriesName"];
											echo '</td>';

											echo '<td>';
											echo $thisProductsSubCategoryValue["stockProductCategoriesShortName"];
											echo '</td>';
											
											#echo '<td>';
											#echo $thisProductsSubCategoryValue["stockProductCategoriesProductNumber"];
											#echo '</td>';

											echo '<td>';
											echo $thisProductsSubCategoryValue["stockProductCategoriesSort"];
											echo '</td>';

											echo '<td>';
											// echo $thisProductsSubCategoryValue["stockProductCategoriesActive"];
											if($thisProductsSubCategoryValue["stockProductCategoriesActive"] == '1'){
												$thisStatusImage = 'iconOk.png';
												$thisStatusTitle = 'aktiv';
											}
											else {
												$thisStatusImage = 'iconNotOk.png';
												$thisStatusTitle = 'nicht aktiv';
											}

											echo '<img src="layout/icons/' . $thisStatusImage . '" width="16" height="16" alt="' . $thisStatusTitle . '" title="' . $thisStatusTitle . '" />';
											echo '</td>';

											echo '<td>';
											echo $thisProductsSubCategoryValue["stockProductCategoriesLevelID"];
											echo '</td>';

											echo '<td>';
											echo $thisProductsSubCategoryValue["stockProductCategoriesParentID"];
											echo '</td>';
											
											echo '<td>';
											echo $thisProductsSubCategoryValue["stockProductCategoriesRelationID"];
											echo '</td>';

											echo '<td>';
											if($arrGetUserRights["editStocks"]) {
												echo '<span class="toolItem"><a href="' . PAGE_PRODUCTS_STOCK_CATEGORIES . '?editID=' . $thisProductsSubCategoryValue["stockProductCategoriesID"] . '"><img src="layout/icons/iconEdit.gif" width="16" height="16" title="Kategorie bearbeiten" alt="Bearbeiten" /></a></span>';
											}
											else {
												echo '<span class="toolItem"><img src="layout/spacer.gif" width="16" height="16" alt="" /></span>';
											}
											echo '</td>';

											echo '</tr>';

											$countRow++;
										}
									// EOF SUB CATEGORIES

								}

								echo '</table>';
							}
						}
						else {
							?>

							<form name="formEditProductsCategory" id="formEditProductsCategory" method="post" action="<?php echo $_SERVER["PHP_SELF"]; ?>#tabs-1" enctype="multipart/form-data" >
								<input type="hidden" name="editID" value="<?php echo $_REQUEST["editID"]; ?>" />
								<input type="hidden" name="originalProductsCategoryLevelID" class="inputField_70" value="<?php echo $arrSelectedProductsCategory["stockProductCategoriesLevelID"]; ?>" readonly="readonly" />
								<input type="hidden" name="editProductsCategoryLevelID" class="inputField_70" value="<?php echo $arrSelectedProductsCategory["stockProductCategoriesLevelID"]; ?>" readonly="readonly" />
								<input type="hidden" name="originalProductsCategoryParentID" class="inputField_70" value="<?php echo $arrSelectedProductsCategory["stockProductCategoriesParentID"]; ?>" readonly="readonly" />
								<input type="hidden" name="originalProductCategoriesRelationID" class="inputField_70" value="<?php echo $arrSelectedProductsCategory["stockProductCategoriesRelationID"]; ?>" readonly="readonly" />
								<input type="hidden" name="editProductsCategoryParentID" class="inputField_70" value="000" readonly="readonly" />
								
								<fieldset class="colored1">
									<legend>Artikel-Kategorie-Daten</legend>
									<table border="0" width="100%" cellspacing="0" cellpadding="0">										
										<tr>
											<td style="width:200px;"><b>Kategorie-Name [DE]:</b></td>
											<td><input type="text" name="editProductsCategoryName" class="inputField_510" value="<?php echo $arrSelectedProductsCategory["stockProductCategoriesName"]; ?>" /></td>
										</tr>
										<tr>
											<td style="width:200px;"><b>Kategorie-Name [TR]:</b></td>
											<td><input type="text" name="editProductsCategoryName_TR" class="inputField_510" value="<?php echo $arrSelectedProductsCategory["stockProductCategoriesName_TR"]; ?>" /></td>
										</tr>
										<tr>
											<td style="width:200px;"><b>Lager-Produkte?:</b></td>
											<td>
												<?php
													$checked = '';
													if($arrSelectedProductsCategory["stockProductCategoriesContainsStockProducts"] == '1'){
														$checked = ' checked="checked" ';

													}
												?>
												<input type="checkbox" name="editProductCategoryContainsStockProducts" value="1" <?php echo $checked; ?> /> Diese Kategorie enth&auml;lt Lagerprodukte?
											</td>
										</tr>
										<tr>
											<td style="width:200px;"><b>Kategorie-Kurzname:</b></td>
											<td><input type="text" name="editProductsCategoryShortName" class="inputField_70" value="<?php echo $arrSelectedProductsCategory["stockProductCategoriesShortName"]; ?>" /></td>
										</tr>
										<tr>
											<td style="width:200px;"><b>Kategorie-Sortierung:</b></td>
											<td><input type="text" name="editProductsCategorySort" class="inputField_70" value="<?php echo $arrSelectedProductsCategory["stockProductCategoriesSort"]; ?>" /></td>
										</tr>										
										<tr>
											<td style="width:200px;"><b>Kategorie-Status:</b></td>
											<td>
												<?php
													$checked = '';
													if($arrSelectedProductsCategory["stockProductCategoriesActive"] == '1'){
														$checked = ' checked="checked" ';

													}
												?>
												<input type="checkbox" name="editProductsCategoryStatus" value="1" <?php echo $checked; ?> /> Aktiv?
											</td>
										</tr>
										
										<tr>
											<td style="width:200px;"><b>Verkn&uuml;pfte Kategorie: </b></td>
											<td>
												<?php
													#dd('arrProductsCategoryData');
												?>												
												<select name="editProductCategoriesRelationID" class="inputSelect_510">
													<?php
														$selected = '';
														if($arrSelectedProductsCategory["editProductCategoriesRelationID"] == ''){
															$selected = ' selected="selected" ';
														}
													?>
													<option value="" <?php echo $selected; ?>>Keine Verkn&uuml;pfung</option>
													<?php
														if(!empty($arrProductsCategoryData)){
															foreach($arrProductsCategoryData as $thisProductsCategoryKey => $thisProductsCategoryValue){
																if($thisProductsCategoryValue["stockProductCategoriesActive"] == '1'){
																	$selected = '';
																	if($arrSelectedProductsCategory["stockProductCategoriesRelationID"] == $thisProductsCategoryValue["stockProductCategoriesLevelID"]){
																		$selected = ' selected="selected" ';
																	}
																	echo '<option value="' .  $thisProductsCategoryValue["stockProductCategoriesLevelID"] . '" ' . $selected . '>' .  $thisProductsCategoryValue["stockProductCategoriesName"] . ' [LID ' .  $thisProductsCategoryValue["stockProductCategoriesLevelID"] . ']</option>';
																}
															}
														}
													?>
												</select>
											</td>
										</tr>
										
									</table>
								</fieldset>


								<div class="actionButtonsArea">
									<?php if($arrGetUserRights["editStocks"] == '1') { ?>
									<input type="submit" class="inputButton1 inputButtonGreen" name="storeDatas" value="Speichern" />
									&nbsp;
									<input type="submit" class="inputButton1 inputButtonRed" name="deleteDatas" value="Endg&uuml;ltig l&ouml;schen" onclick="return showWarning('Wollen Sie diesen Eintrag wirklich endgültig entfernen???\nOder soll der Eintrag nur deaktiviert werden?');" <?php if($categoryIsDeletable == false){ echo ' disabled="disabled" '; } ?> />
									&nbsp;
									<input type="submit" class="inputButton1 inputButtonOrange" name="cancelDatas" value="Abbrechen" onclick="return showWarning('Wollen Sie diesen Vorgang wirklich abbrechen??? ');" />
									<?php } ?>
								</div>
							</form>
							<?php
						}
					?>
				</div>
				</div>
				</div>
			<div class="clear"></div>
		</div>
	</div>
</div>

<script language="javascript" type="text/javascript">

</script>
<?php require_once('inc/footerHTML.inc.php'); ?>