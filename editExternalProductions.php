<?php
	require_once('inc/requires.inc.php');

	if(isset($_REQUEST["resetDatas"]) && $_REQUEST["resetDatas"] != ""){
		header('location: ' . PAGE_DISPLAY_EXTERNAL_PRODUCTIONS);
	}

	if(!$arrGetUserRights["displayOrdersOverview"] && !$arrGetUserRights["displayOrders"] && !$arrGetUserRights["editOrders"]) {
		header('location: ' . PAGE_EXIT_LOCATION);
		exit;
	}

	$jswindowMessage = "";
	$warningMessage = '';
	$errorMessage = '';
	$successMessage = '';
	$infoMessage = '';

	$dbConnection = new DB_Connection();
	$db_open = $dbConnection->db_connect();

	// BOF READ BESTELL STATUS TYPEN
		$arrOrderStatusTypeDatas = getOrderStatusTypes();
	// EOF READ BESTELL STATUS TYPEN

	// BOF DOWNLOAD FILE
		if($_REQUEST["downloadFile"] != "") {
			require_once('classes/downloadFile.class.php');
			$arrTemp = pathinfo($_REQUEST["downloadFile"]);
			$thisDownloadFile = $arrTemp["basename"];
			$thisDir = $arrTemp["dirname"] . "/";

			$thisDownload = new downloadFile($thisDir, $thisDownloadFile);
			$errorMessage .= $thisDownload->startDownload();
		}
	// EOF DOWNLOAD FILE

	// BOF DELETE DATAS
		if($_POST["deleteDatas"] != ""){
			$sql = "
				DELETE
						FROM `" . TABLE_PRODUCTIONS_TRANSFER . "`
					WHERE 1
						AND `" . TABLE_PRODUCTIONS_TRANSFER . "`.`ordersProductionsTransferID` = '" . $_REQUEST["editTransferId"] . "'
						AND `" . TABLE_PRODUCTIONS_TRANSFER . "`.`ordersProductionsTransferOrdersID` = '" . $_REQUEST["editOrdersId"] . "'
				";
			$rs = $dbConnection->db_query($sql);

			if($rs){
				$successMessage .= 'Die Daten wurden lokal entfernt.' . '<br />';
			}
			else {
				$errorMessage .= 'Die Daten konnten lokal nicht entfernt werden.' . '<br />';
			}

			// BOF UNSET PRODUCTION PRINTING PLANT
				$sql = "UPDATE
							`" . TABLE_ORDERS . "`
							SET
								`ordersProductionPrintingPlant` = '',
								`ordersStatus` = '2'
								/* , `ordersProduktionsDatum` = '' */
							WHERE 1 AND (
								 `ordersID` = '" . $_REQUEST["editOrdersId"] . "'
							)
					";
				$rs = $dbConnection->db_query($sql);
				if(!$rs){
					$errorMessage .= ' Der Produktionsstandort konnte nicht entfernt werden !' . '<br />';
				}
				else {
					$successMessage .= ' Der Produktionsstandort wurde entfernt! ' . '<br />';
				}
			// EOF UNSET PRODUCTION PRINTING PLANT

			if(constant("DB_HOST_EXTERN_PRODUCTION") != '' && constant("DB_NAME_EXTERN_PRODUCTION") != '' && constant("DB_USER_EXTERN_PRODUCTION") != '' && constant("DB_PASSWORD_EXTERN_PRODUCTION")) {

				$dbConnection_ExternProduction = new DB_Connection(DB_HOST_EXTERN_PRODUCTION, '', DB_NAME_EXTERN_PRODUCTION, DB_USER_EXTERN_PRODUCTION, DB_PASSWORD_EXTERN_PRODUCTION);

				$sql = "
						DELETE
								FROM `" . TABLE_PRODUCTIONS_TRANSFER . "`
							WHERE 1
								AND `" . TABLE_PRODUCTIONS_TRANSFER . "`.`ordersProductionsTransferID` = '" . $_REQUEST["editTransferId"] . "'
								AND `" . TABLE_PRODUCTIONS_TRANSFER . "`.`ordersProductionsTransferOrdersID` = '" . $_REQUEST["editOrdersId"] . "'
					";

				$rs = $dbConnection_ExternProduction->db_query($sql);

				if($rs){
					$successMessage .= 'Die Daten wurden extern entfernt.' . '<br />';
				}
				else {
					$errorMessage .= 'Die Daten konnten extern nicht entfernt werden.' . '<br />';
				}
			}
			unset($_REQUEST["editTransferId"]);
		}
	// EOF DELETE DATAS

	// BOF UPDATE DATAS
		if($_POST["storeDatas"] != ""){
			$sql = "
				UPDATE
					`" . TABLE_PRODUCTIONS_TRANSFER . "`

					SET
						/*
						`" . TABLE_PRODUCTIONS_TRANSFER . "`.`ordersProductionsTransferDeliveryContainerDate` = '" . formatDate($_POST["editContainerDate"], "store") . "',
						`" . TABLE_PRODUCTIONS_TRANSFER . "`.`ordersProductionsTransferDeliveryPrintDate` = '" . formatDate($_POST["editProductionsDate"], "store") . "',
						`" . TABLE_PRODUCTIONS_TRANSFER . "`.`ordersProductionsTransferProductionPrinter` = '" . addslashes(($_POST["editPrinterName"])) . "',
						`" . TABLE_PRODUCTIONS_TRANSFER . "`.`ordersProductionsTransferProductionStatus` = '" . $_POST["editProductionsStatus"] . "',
						`" . TABLE_PRODUCTIONS_TRANSFER . "`.`ordersProductionsTransferProductionTrackingNumbers` = '" . preg_replace("/\r\n/", ";", $_POST["editDeliveryTrackingNumbers"]) . "',
						*/

						`" . TABLE_PRODUCTIONS_TRANSFER . "`.`ordersProductionsTransferOrderRecipientCustomerNumber` = '" . addslashes(($_POST["editProductionsRecipientCustomerNumber"])) . "',
						`" . TABLE_PRODUCTIONS_TRANSFER . "`.`ordersProductionsTransferOrderRecipientAddress` = '" . addslashes(($_POST["editProductionsRecipientAddress"])) . "',

						`" . TABLE_PRODUCTIONS_TRANSFER . "`.`ordersProductionsTransferOrderSenderCustomerNumber` = '" . addslashes(($_POST["editProductionsSenderCustomerNumber"])) . "',
						`" . TABLE_PRODUCTIONS_TRANSFER . "`.`ordersProductionsTransferOrderSenderAddress` = '" . addslashes(($_POST["editProductionsSenderAddress"])) . "',

						`" . TABLE_PRODUCTIONS_TRANSFER . "`.`ordersProductionsTransferDeliveryRedDot` = '" . ($_POST["editProductionsTransferDeliveryRedDot"]) . "',

						`" . TABLE_PRODUCTIONS_TRANSFER . "`.`ordersProductionsTransferOrdersProductQuantity` = '" . ($_POST["editProductionsTransferOrdersProductQuantity"]) . "',
						
						`" . TABLE_PRODUCTIONS_TRANSFER . "`.`ordersProductionsTransferOrdersProductTatsaechlicheProduktion` = '" . ($_POST["editProductionsTransferOrdersProductTatsaechlicheProduktion"]) . "',

						/* `" . TABLE_PRODUCTIONS_TRANSFER . "`.`ordersProductionsTransferProductionModified` = NOW(), */

						`" . TABLE_PRODUCTIONS_TRANSFER . "`.`ordersProductionsTransferUseNoNeutralPacking` = '" . ($_POST["editProductionsTransferUseNoNeutralPacking"]) . "',

						`" . TABLE_PRODUCTIONS_TRANSFER . "`.`ordersProductionsTransferPrintPlateNumber` = '" . $_POST["editPrintPlateNumber"] . "',
						`" . TABLE_PRODUCTIONS_TRANSFER . "`.`ordersProductionsTransferNotice` = '" . $_POST["editNotice"] . "'

				WHERE 1
					AND `" . TABLE_PRODUCTIONS_TRANSFER . "`.`ordersProductionsTransferID` = '" . $_POST["editTransferId"] . "'
					AND `" . TABLE_PRODUCTIONS_TRANSFER . "`.`ordersProductionsTransferOrdersID` = '" . $_POST["editOrdersId"] . "'
			";

			$rs = $dbConnection->db_query($sql);
#dd('sql');
			if($rs){
				$successMessage .= 'Die Daten wurden gespeichert.' . '<br />';
			}
			else {
				$errorMessage .= 'Die Daten konnten gespeichert werden.' . '<br />';
			}
		}
	// EOF UPDATE DATAS
?>
<?php
	require_once('inc/headerHTML.inc.php');
	$thisTitle = "TR-Produktionen";

	if($_POST["searchCustomerNumber"] != "") {
		$thisTitle .= " - Kundennummer ".$_POST["searchCustomerNumber"];
	}
	if($_POST["searchCustomerName"] != "") {
		$thisTitle .= " - Kunde ".$_POST["searchCustomerName"];
	}
	if($_POST["searchPLZ"] != "") {
		$thisTitle .= " - PLZ ".$_POST["searchPLZ"];
	}

	if($_POST["searchWord"] != "") {
		$thisTitle .= " - Suchbegriff ".$_POST["searchWord"];
	}
	if($_REQUEST["displayOrderStatus"] != "") {
		$thisTitle .= " - Status ".$arrOrderStatusTypeDatas[$_REQUEST["displayOrderStatus"]]["orderStatusTypesName"];
	}

	$headerHTML = preg_replace("/{###TITLE###}/", strip_tags($thisTitle), $headerHTML);
	echo $headerHTML;

	require_once(FILE_MENUE_TOP);


	// BOF GET PRODUCTION DATA
		if($_REQUEST["editTransferId"] > 0) {
			$sql = "
				SELECT

					`" . TABLE_PRODUCTIONS_TRANSFER . "`.`ordersProductionsTransferID`,

					`" . TABLE_PRODUCTIONS_TRANSFER . "`.`ordersProductionsTransferOrdersID`,

					`" . TABLE_PRODUCTIONS_TRANSFER . "`.`ordersProductionsTransferDeliveryRedDot`,
					`" . TABLE_PRODUCTIONS_TRANSFER . "`.`ordersProductionsTransferDeliveryNoDPD`,
					`" . TABLE_PRODUCTIONS_TRANSFER . "`.`ordersProductionsTransferDeliveryContainerDate`,

					`" . TABLE_PRODUCTIONS_TRANSFER . "`.`ordersProductionsTransferMailSubmitDateTime`,
					`" . TABLE_PRODUCTIONS_TRANSFER . "`.`ordersProductionsTransferDeliveryPrintDate`,
					`" . TABLE_PRODUCTIONS_TRANSFER . "`.`ordersProductionsTransferMailLanguage`,
					`" . TABLE_PRODUCTIONS_TRANSFER . "`.`ordersProductionsTransferMailSubject`,
					`" . TABLE_PRODUCTIONS_TRANSFER . "`.`ordersProductionsTransferMailRecipient`,

					`" . TABLE_PRODUCTIONS_TRANSFER . "`.`ordersProductionsTransferLayoutFileName`,
					`" . TABLE_PRODUCTIONS_TRANSFER . "`.`ordersProductionsTransferLayoutFileSize`,
					`" . TABLE_PRODUCTIONS_TRANSFER . "`.`ordersProductionsTransferLayoutFileType`,

					`" . TABLE_PRODUCTIONS_TRANSFER . "`.`ordersProductionsTransferProductionFileName`,
					`" . TABLE_PRODUCTIONS_TRANSFER . "`.`ordersProductionsTransferProductionFileSize`,

					`" . TABLE_PRODUCTIONS_TRANSFER . "`.`ordersProductionsTransferOrdersCustomerNumber`,
					`" . TABLE_PRODUCTIONS_TRANSFER . "`.`ordersProductionsTransferOrdersCustomerName`,

					`" . TABLE_PRODUCTIONS_TRANSFER . "`.`ordersProductionsTransferOrdersPrintText`,
					`" . TABLE_PRODUCTIONS_TRANSFER . "`.`ordersProductionsTransferOrdersKomission`,

					`" . TABLE_PRODUCTIONS_TRANSFER . "`.`ordersProductionsTransferOrdersProductNumber`,
					`" . TABLE_PRODUCTIONS_TRANSFER . "`.`ordersProductionsTransferOrdersProductCategory`,
					`" . TABLE_PRODUCTIONS_TRANSFER . "`.`ordersProductionsTransferOrdersProductName`,
					`" . TABLE_PRODUCTIONS_TRANSFER . "`.`ordersProductionsTransferOrdersProductQuantity`,
					`" . TABLE_PRODUCTIONS_TRANSFER . "`.`ordersProductionsTransferOrdersProductOriginalQuantity`,
					`" . TABLE_PRODUCTIONS_TRANSFER . "`.`ordersProductionsTransferOrdersProductTatsaechlicheProduktion`,

					`" . TABLE_PRODUCTIONS_TRANSFER . "`.`ordersProductionsTransferOrdersColorsQuantity`,
					`" . TABLE_PRODUCTIONS_TRANSFER . "`.`ordersProductionsTransferOrdersColorsNames`,
					`" . TABLE_PRODUCTIONS_TRANSFER . "`.`ordersProductionsTransferOrdersWithClearPaint`,

					`" . TABLE_PRODUCTIONS_TRANSFER . "`.`ordersProductionsTransferOrderRecipientCustomerNumber`,
					`" . TABLE_PRODUCTIONS_TRANSFER . "`.`ordersProductionsTransferOrderRecipientAddress`,

					`" . TABLE_PRODUCTIONS_TRANSFER . "`.`ordersProductionsTransferOrderSenderCustomerNumber`,
					`" . TABLE_PRODUCTIONS_TRANSFER . "`.`ordersProductionsTransferOrderSenderAddress`,

					`" . TABLE_PRODUCTIONS_TRANSFER . "`.`ordersProductionsTransferUserID`,

					`" . TABLE_PRODUCTIONS_TRANSFER . "`.`ordersProductionsTransferProductionPrinter`,
					`" . TABLE_PRODUCTIONS_TRANSFER . "`.`ordersProductionsTransferProductionStatus`,
					`" . TABLE_PRODUCTIONS_TRANSFER . "`.`ordersProductionsTransferProductionTrackingNumbers`,
					`" . TABLE_PRODUCTIONS_TRANSFER . "`.`ordersProductionsTransferProductionNotice`,

					`" . TABLE_PRODUCTIONS_TRANSFER . "`.`ordersProductionsTransferProductionModified`,

					`" . TABLE_PRODUCTIONS_TRANSFER . "`.`ordersProductionsTransferSuccess_MYSQL`,
					`" . TABLE_PRODUCTIONS_TRANSFER . "`.`ordersProductionsTransferSuccess_FTP`,

					`" . TABLE_PRODUCTIONS_TRANSFER . "`.`ordersProductionsTransferMandatory`,

					`" . TABLE_PRODUCTIONS_TRANSFER . "`.`ordersProductionsTransferTransportType`,
					`" . TABLE_PRODUCTIONS_TRANSFER . "`.`ordersProductionsTransferOrdersPrintType`,

					`" . TABLE_PRODUCTIONS_TRANSFER . "`.`ordersProductionsTransferUseNoNeutralPacking`,

					`" . TABLE_PRODUCTIONS_TRANSFER . "`.`ordersProductionsTransferPrintPlateNumber`,
					`" . TABLE_PRODUCTIONS_TRANSFER . "`.`ordersProductionsTransferNotice`

				FROM `" . TABLE_PRODUCTIONS_TRANSFER . "`
				WHERE 1
					AND `" . TABLE_PRODUCTIONS_TRANSFER . "`.`ordersProductionsTransferID` = '" . $_REQUEST["editTransferId"] . "'
					LIMIT 1
			";
		}
	// EOF GET PRODUCTION DATA
?>
<?php require_once(FILE_MENUE_SIDEBAR); ?>
<div id="contentArea">
	<a name="top"></a>
	<div id="contentAreaElements">
		<h1><?php if(SHOW_PAGE_TITLE_ICONS) { echo '<img src="' . PATH_ICONS_MENUE_TITLES . 'iconTR.png" alt="" />'; } ?> <?php echo $thisTitle; ?></h1>

		<?php displayMessages(); ?>

		<?php
			if($_REQUEST["editTransferId"] > 0) {
				$rs = mysqli_query($sql, $db_open);
				$arrProductionDatas = array();
				while($ds = mysqli_fetch_assoc($rs)) {
					foreach(array_keys($ds) as $field){
						$arrProductionDatas[$field] = ($ds[$field]);
					}
				}
		?>
		<div class="adminInfo">Datensatz-ID: <?php echo 'TID: ' . $arrProductionDatas["ordersProductionsTransferID"] . ' | OID: ' . $arrProductionDatas["ordersProductionsTransferOrdersID"]; ?></div>
		<div class="contentDisplay">
			<div class="adminEditArea">
				<form name="formEditProductionDatas" id="formEditProductionDatas" method="post" action="<?php echo $_SERVER["PHP_SELF"]; ?>" enctype="multipart/form-data" >
					<input type="hidden" name="editTransferId" value="<?php echo $arrProductionDatas["ordersProductionsTransferID"]; ?>" />
					<input type="hidden" name="editOrdersId" value="<?php echo $arrProductionDatas["ordersProductionsTransferOrdersID"]; ?>" />
					<fieldset>
						<legend>Produktionsdaten</legend>
						<table border="0" width="100%" cellspacing="0" cellpadding="0">
							<tr>
								<td style="width:200px;"><b>Transfer-Datum:</b></td>
								<td>
									<?php
										echo substr(formatDate($arrProductionDatas["ordersProductionsTransferMailSubmitDateTime"], "display"), 0, 10);
									?>
								</td>
							</tr>
							<tr>
								<td><b>Transport:</b></td>
								<td>
									<?php
										$checked = '';
										if($arrProductionDatas["ordersProductionsTransferDeliveryRedDot"] == '1'){
											$checked = ' checked="checked" ';
											echo '<img width="14" height="14" title="Hinten LKW" alt="Hinten LKW" src="layout/icons/redDot.png" /> ';
										}
									?>
									<input type="checkbox" id="editProductionsTransferDeliveryRedDot" name="editProductionsTransferDeliveryRedDot" <?php echo $checked; ?> value="1" /> <b>Hinten LKW ?</b>
								</td>
							</tr>

							<tr>
								<td><b>Verpackung:</b></td>
								<td>
									<?php
										$checked = '';
										if($arrProductionDatas["ordersProductionsTransferUseNoNeutralPacking"] == '1'){
											$checked = ' checked="checked" ';
											echo '<img width="14" height="14" title="Verpackung nicht neutral (BURHAN)" alt="Verpackung nicht neutral (BURHAN)" src="layout/icons/greenDot.png" />';
										}
									?>
									<input type="checkbox" id="editProductionsTransferUseNoNeutralPacking" name="editProductionsTransferUseNoNeutralPacking" <?php echo $checked; ?> value="1" /> <b>Verpackung nicht neutral (BURHAN) ?</b>
								</td>
							</tr>

							<tr>
								<td><b>Mandant:</b></td>
								<td>
									<input type="text" name="editProductionsMandatory" id="editProductionsMandatory" class="inputField_70" value="<?php echo $arrProductionDatas["ordersProductionsTransferMandatory"]; ?>" readonly="readonly" />
								</td>
							</tr>

							<tr>
								<td><b>Kunde:</b></td>
								<td><b><?php echo $arrProductionDatas["ordersProductionsTransferOrdersCustomerName"]; ?></b> (<?php echo $arrProductionDatas["ordersProductionsTransferOrdersCustomerNumber"]; ?>)</td>
							</tr>
							<tr>
								<td><b>Produktionsstatus:</b></td>
								<td>
									<select id="editProductionsStatus" name="editProductionsStatus" class="inputField_510" disabled="disabled">
										<option value=""> - </option>
										<?php
											if(!empty($arrOrderStatusTypeDatas)){
												foreach($arrOrderStatusTypeDatas as $thisKey => $thisValue){
													if($thisValue["orderStatusTypesActive"]){
														$selected = '';
														if($thisKey == $arrProductionDatas["ordersProductionsTransferProductionStatus"]){
															$selected = ' selected="selected" ';
														}
														echo '<option value="' . $thisKey . '" ' . $selected . '>' . $thisValue["orderStatusTypesName"] . '</option>';
													}

												}
											}
										?>
										<option value=""> - </option>

									</select>
								</td>
							</tr>
							<tr>
								<td><b>Produktions-Datum:</b></td>
								<td><input type="text" id="editProductionsDate" name="editProductionsDate" class="inputField_70" value="<?php echo formatDate($arrProductionDatas["ordersProductionsTransferDeliveryPrintDate"]); ?>" disabled="disabled" /></td>
							</tr>
							<tr>
								<td><b>Drucker-Name:</b></td>
								<td><input type="text" id="editPrinterName" name="editPrinterName" class="inputField_510" value="<?php echo htmlentities($arrProductionDatas["ordersProductionsTransferProductionPrinter"]); ?>" disabled="disabled" /></td>
							</tr>
							<tr>
								<td><b>Verlade-Datum:</b></td>
								<td><input type="text" id="editContainerDate" name="editContainerDate" class="inputField_70" value="<?php echo formatDate($arrProductionDatas["ordersProductionsTransferDeliveryContainerDate"]); ?>" disabled="disabled"/></td>
							</tr>

							<tr>
								<td><b>Bemerkung:</b></td>
								<td>
									<textarea id="editNotice" name="editNotice" class="inputTextarea_510x80"><?php echo htmlentities($arrProductionDatas["ordersProductionsTransferNotice"]); ?></textarea>
								</td>
							</tr>

							<tr>
								<td><b>DPD-Nummern:</b></td>
								<td>
									<textarea id="editDeliveryTrackingNumbers" name="editDeliveryTrackingNumbers" class="inputTextarea_510x80" disabled="disabled"><?php echo preg_replace("/;/", "\n", htmlentities($arrProductionDatas["ordersProductionsTransferProductionTrackingNumbers"])); ?></textarea>
									<p class="infoArea" style="width:500px;">Eine DPD-Nummer pro Zeile!</p>
								</td>
							</tr>
						</table>
					</fieldset>

					<fieldset>
						<legend>Druckdaten</legend>
						<table border="0" width="100%" cellspacing="0" cellpadding="0">
							<tr>
								<td style="width:200px;"><b>Druck-Datei:</b></td>
								<td><?php echo '<a href="?downloadFile=' . ($arrProductionDatas["ordersProductionsTransferProductionFileName"]) . '&amp;thisDocumentType=PR">' . '<img src="layout/icons/iconZIP.gif" width="16" height="16" title="Produktionsdatei herunterladen" alt="Download" /></a>'; ?></td>
							</tr>
							<tr>
								<td><b>Artikel-Nummer:</b></td>
								<td><?php echo $arrProductionDatas["ordersProductionsTransferOrdersProductNumber"]; ?></td>
							</tr>
							<tr>
								<td><b>Artikel-Name:</b></td>
								<td><?php echo $arrProductionDatas["ordersProductionsTransferOrdersProductName"]; ?></td>
							</tr>
							<tr>
								<td><b>Menge:</b></td>
								<td>
									<input id="editProductionsTransferOrdersProductQuantity" name="editProductionsTransferOrdersProductQuantity" class="inputField_70" value="<?php echo $arrProductionDatas["ordersProductionsTransferOrdersProductQuantity"]; ?>">
								</td>
							</tr>
							
								<td><b>tats&auml;chlich Produziert:</b></td>
								<td>
									<input id="editProductionsTransferOrdersProductTatsaechlicheProduktion" name="editProductionsTransferOrdersProductTatsaechlicheProduktion" class="inputField_70" value="<?php echo $arrProductionDatas["ordersProductionsTransferOrdersProductTatsaechlicheProduktion"]; ?>">
								</td>
							</tr>
							<tr>
								<td><b>Farben:</b></td>
								<td>
									Farbanzahl: <?php echo $arrProductionDatas["ordersProductionsTransferOrdersColorsQuantity"]; ?><br />
									<?php
										echo '&bull; ' . (preg_replace("/\//", "<br />&bull; ", $arrProductionDatas["ordersProductionsTransferOrdersColorsNames"]));

										if($arrProductionDatas["ordersProductionsTransferOrdersWithClearPaint"] != '') {
											echo '<div class="remarksArea">';
											echo '<b>Aufdruck mit Klarlack</b>';
											echo '</div>';
										}
									?>
								</td>
							</tr>

							<?php if($arrProductionDatas["ordersProductionsTransferOrdersKomission"] != '') { ?>
							<tr>
								<td><b>Kommission:</b></td>
								<td><?php echo $arrProductionDatas["ordersProductionsTransferOrdersKomission"]; ?></td>
							</tr>
							<?php } ?>

							<?php if($arrProductionDatas["ordersProductionsTransferOrdersPrintText"] != '') { ?>
							<tr>
								<td><b>Aufdruck:</b></td>
								<td><?php echo $arrProductionDatas["ordersProductionsTransferOrdersPrintText"]; ?></td>
							</tr>
							<?php } ?>

							<tr>
								<td><b>Klischee-Nr:</b></td>
								<td><input type="text" name="editPrintPlateNumber" class="inputField_510" value="<?php echo htmlentities($arrProductionDatas["ordersProductionsTransferPrintPlateNumber"]); ?>" /></td>
							</tr>
						</table>
					</fieldset>

					<fieldset>
						<legend>Adressdaten</legend>
						<table border="0" width="100%" cellspacing="0" cellpadding="0">
							<tr>
								<td style="width:45%;">
									<b>Liefer-Adresse:</b>
									<br />
									K-Nr: <input name="editProductionsRecipientCustomerNumber" class="inputField_70" value="<?php echo $arrProductionDatas["ordersProductionsTransferOrderRecipientCustomerNumber"]; ?>" />
									<div style="padding:10px;background-color:#FFFFFF;">
									<textarea name="editProductionsRecipientAddress" class="inputTextarea_400x120"><?php echo ($arrProductionDatas["ordersProductionsTransferOrderRecipientAddress"]); ?></textarea>
									</div>
								</td>
								<td style="width:10%;"></td>
								<td style="width:45%;">
									<b>Absender-Adresse:</b>
									<br />
									K-Nr: <input name="editProductionsSenderCustomerNumber" class="inputField_70" value="<?php echo $arrProductionDatas["ordersProductionsTransferOrderSenderCustomerNumber"]; ?>" />
									<div style="padding:10px;background-color:#FFFFFF;">
									<textarea name="editProductionsSenderAddress" class="inputTextarea_400x120"><?php echo ($arrProductionDatas["ordersProductionsTransferOrderSenderAddress"]); ?></textarea>
									</div>
								</td>
							</tr>
						</table>
					</fieldset>

					<div class="actionButtonsArea">
						<?php if($arrGetUserRights["editOrders"]) { ?>
						<input type="submit" class="inputButton1 inputButtonGreen" name="storeDatas" value="Speichern" onclick="return checkFormDutyFields(this.form.name, <?php echo $jsonFormDutyFields; ?>);" />
						<input type="submit" class="inputButton1 inputButtonRed" name="deleteDatas" value="Entfernen" onclick="return showWarning('Wollen Sie diesen Vorgang wirklich entfernen??? ');" />
						<?php } ?>
						<input type="submit" class="inputButton1 inputButtonOrange" name="resetDatas" value="Abbrechen" onclick="return showWarning('Wollen Sie diesen Vorgang wirklich abbrechen??? ');" />
					</div>
				</form>
			</div>
		</div>

		<div style="text-align:right;" class="menueToTop">
			<a href="#top">nach oben</a>
		</div>
		<?php
			}
			else {
				echo '<p class="warningArea">Es wurden keine Daten gefunden!</p>';
				echo '<a href="' . PAGE_DISPLAY_EXTERNAL_PRODUCTIONS . '" class="linkButton">Zur&uuml;ck zur Liste</a>';
			}
		?>
		<div class="clear"></div>
	</div>
	<div class="clear"></div>
</div>
<script language="javascript" type="text/javascript">
	<!--
	/* <![CDATA[ */
	$(document).ready(function() {
		$('.buttonToggleSidebarMenue').click(function() {
			$('#menueSidebarToggleContent').toggle();
		});

		$.datepicker.setDefaults($.datepicker.regional[""]);
		$('#editProductionsDate').datepicker($.datepicker.regional["de"]);
		$('#editProductionsDate').datepicker("option", "maxDate", "0" );
		$('#editContainerDate').datepicker($.datepicker.regional["de"]);
		$('#editContainerDate').datepicker("option", "maxDate", "0" );

		var htmlButtonClearField = '<span class="buttonClearField"><img src="layout/icons/iconDelete.png" width="16" height="16" alt="" title="Eingabefeld leeren" /><\/span>';
		$('#editProductionsDate').parent().append(htmlButtonClearField);
		$('#editContainerDate').parent().append(htmlButtonClearField);

		$('.buttonClearField').click(function () {
			$(this).parent().find('input').val('');
			$(this).parent().find('input').attr('checked', false);
		});
	});
	/* ]]> */
	// -->
</script>

<?php require_once('inc/footerHTML.inc.php'); ?>