<?php
	require_once('inc/requires.inc.php');

	if(!$arrGetUserRights["displayDistribution"]) {
		header('location: ' . PAGE_EXIT_LOCATION);
		exit;
	}

	$jswindowMessage = "";
	$warningMessage = '';
	$errorMessage = '';
	$successMessage = '';
	$infoMessage = '';
	
	$todayDate = date("Y-m-d");
	if($_REQUEST["submitSearch"] == ""){
		#$_REQUEST["submitSearch"] = "1";		
		#$_REQUEST["searchCallbackDate"] = formatDate($todayDate, 'display');
		#$_REQUEST["searchRemindDate"] = formatDate($todayDate, 'display');
	}

	// BOF DOWNLOAD FILE
	if($_REQUEST["downloadFile"] != "") {
		$thisDownloadFile = basename($_REQUEST["downloadFile"]);
		$thisDownloadFolder = DIRECTORY_DOWNLOAD_CONTAINER_LISTS;

		require_once('classes/downloadFile.class.php');
		$thisDownload = new downloadFile($thisDownloadFolder, $thisDownloadFile);
		$errorMessage .= $thisDownload->startDownload();
	}
	// BOF DOWNLOAD FILE

	$dbConnection = new DB_Connection();
	$db_open = $dbConnection->db_connect();
	
	// BOF GET USER DATAS
		$arrUserDatas = getUsers();
	// BOF GET USER DATAS

	// BOF GET ALL MARKETING PHONE DATES	
		$sqlWhere = "";
		
		if($_REQUEST["searchCustomerNumber"] != ""){
			$sqlWhere = " AND `" . TABLE_CUSTOMERS_PHONE_MARKETING . "`.`customersPhoneMarketingCustomerNumber` = '" . $_REQUEST["searchCustomerNumber"] . "'";
		}
		else if($_REQUEST["searchCustomerName"] != ""){
			$sqlWhere = " 
					AND (
						`" . TABLE_CUSTOMERS . "`.`customersFirmenname` LIKE '%" . $_REQUEST["searchCustomerName"] . "%'
						OR `" . TABLE_CUSTOMERS . "`.`customersFirmennameZusatz` = '%" . $_REQUEST["searchCustomerName"] . "%'
					)
				";
		}
		else if($_REQUEST["searchContactDate"] != ""){
			$sqlWhere = " AND `" . TABLE_CUSTOMERS_PHONE_MARKETING . "`.`customersPhoneMarketingContactDate` = '" . formatDate($_REQUEST["searchContactDate"], 'store') . "'";
		}
		else if($_REQUEST["searchCallbackDate"] != "" || $_REQUEST["searchRemindDate"] != ""){
			$arrSqlWhere_Dates = array();
			if($_REQUEST["searchCallbackDate"] != ""){
				$arrSqlWhere_Dates[] = " `" . TABLE_CUSTOMERS_PHONE_MARKETING . "`.`customersPhoneMarketingCallbackDate` = '" . formatDate($_REQUEST["searchCallbackDate"], 'store') . "'";
			}
			if($_REQUEST["searchRemindDate"] != ""){
				$arrSqlWhere_Dates[] = " `" . TABLE_CUSTOMERS_PHONE_MARKETING . "`.`customersPhoneMarketingRemindDate` = '" . formatDate($_REQUEST["searchRemindDate"], 'store') . "'";
			}
			$sqlWhere = " AND ( " . implode(" OR ", $arrSqlWhere_Dates) . ")";
		}		
		else if($_REQUEST["searchWord"] != ""){
			$sqlWhere = "
					AND (
						`" . TABLE_CUSTOMERS_PHONE_MARKETING . "`.`customersPhoneMarketingCustomerNumber` LIKE '%" . $_REQUEST["searchWord"] . "%'
						OR `" . TABLE_CUSTOMERS . "`.`customersFirmenname` LIKE '%" . $_REQUEST["searchWord"] . "%'
						OR `" . TABLE_CUSTOMERS . "`.`customersFirmennameZusatz` LIKE '%" . $_REQUEST["searchWord"] . "%'
					)
				";
		}
	
		$sql = "
			SELECT
				`" . TABLE_CUSTOMERS_PHONE_MARKETING . "`.`customersPhoneMarketingID`,
				`" . TABLE_CUSTOMERS_PHONE_MARKETING . "`.`customersPhoneMarketingCustomerNumber`,
				`" . TABLE_CUSTOMERS_PHONE_MARKETING . "`.`customersPhoneMarketingCustomerID`,
				`" . TABLE_CUSTOMERS_PHONE_MARKETING . "`.`customersPhoneMarketingContactDate`,
				`" . TABLE_CUSTOMERS_PHONE_MARKETING . "`.`customersPhoneMarketingContactTime`,
				`" . TABLE_CUSTOMERS_PHONE_MARKETING . "`.`customersPhoneMarketingCampaign`,
				`" . TABLE_CUSTOMERS_PHONE_MARKETING . "`.`customersPhoneMarketingConversationPerson`,
				`" . TABLE_CUSTOMERS_PHONE_MARKETING . "`.`customersPhoneMarketingConversationContent`,
				`" . TABLE_CUSTOMERS_PHONE_MARKETING . "`.`customersPhoneMarketingRemindDate`,
				`" . TABLE_CUSTOMERS_PHONE_MARKETING . "`.`customersPhoneMarketingCallbackDate`,
				`" . TABLE_CUSTOMERS_PHONE_MARKETING . "`.`customersPhoneMarketingVisitRequest`,
				`" . TABLE_CUSTOMERS_PHONE_MARKETING . "`.`customersPhoneMarketingNotice`,
				`" . TABLE_CUSTOMERS_PHONE_MARKETING . "`.`customersPhoneMarketingUser`,
				
				`" . TABLE_CUSTOMERS . "`.`customersID`,
				`" . TABLE_CUSTOMERS . "`.`customersKundennummer`,
				`" . TABLE_CUSTOMERS . "`.`customersFirmenname`,
				`" . TABLE_CUSTOMERS . "`.`customersFirmennameZusatz`,
				`" . TABLE_CUSTOMERS . "`.`customersTelefon1`,
				`" . TABLE_CUSTOMERS . "`.`customersTelefon2`,
				`" . TABLE_CUSTOMERS . "`.`customersMobil1`,
				`" . TABLE_CUSTOMERS . "`.`customersMobil2`,
				`" . TABLE_CUSTOMERS . "`.`customersMail1`,
				`" . TABLE_CUSTOMERS . "`.`customersMail2`
				
				
			FROM `" . TABLE_CUSTOMERS_PHONE_MARKETING . "`
			
			LEFT JOIN `" . TABLE_CUSTOMERS . "`
			ON(`" . TABLE_CUSTOMERS_PHONE_MARKETING . "`.`customersPhoneMarketingCustomerID` = `" . TABLE_CUSTOMERS . "`.`customersID`)
			
			WHERE 1
				" . $sqlWhere . "
				
			ORDER BY
				`customersPhoneMarketingContactDate` DESC,
				`customersPhoneMarketingContactTime` DESC
		";
	// EOF GET ALL MARKETING PHONE DATES

?>
<?php
	require_once('inc/headerHTML.inc.php');
	$thisTitle = "Telefon-Marketing Termine";

	if($_POST["searchCustomerNumber"] != "") {
		$thisTitle .= ': <span class="headerSelectedEntry">Kundennummer ' . $_POST["searchCustomerNumber"] . '</span>';
	}
	if($_POST["searchCustomerName"] != "") {
		$thisTitle .= ': <span class="headerSelectedEntry">Kunde ' . $_POST["searchCustomerName"] . '</span>';
	}	
	if($_POST["searchWord"] != "") {
		$thisTitle .= ': <span class="headerSelectedEntry">Suchbegriff ' . $_POST["searchWord"] . '</span>';
	}
	if($_REQUEST["searchContactDate"] != "") {
		$thisTitle .= ': <span class="headerSelectedEntry">Gespr&auml;chsdatum ' . formatDate($_REQUEST["searchContactDate"], 'display') . '</span>';
	}
	if($_REQUEST["searchRemindDate"] != "") {
		$thisTitle .= ': <span class="headerSelectedEntry">Wiedervorlage-Datum ' . formatDate($_REQUEST["searchRemindDate"], 'display') . '</span>';
	}
	if($_REQUEST["searchCallbackDate"] != "") {
		$thisTitle .= ': <span class="headerSelectedEntry">R&uuml;ckruf-Datum ' . formatDate($_REQUEST["searchCallbackDate"], 'display') . '</span>';
	}	

	$headerHTML = preg_replace("/{###TITLE###}/", strip_tags($thisTitle), $headerHTML);
	echo $headerHTML;

	require_once(FILE_MENUE_TOP);


?>

<div id="xxmainArea">
	<div id="xxmainContent">
		<div id="menueSidebarToggleArea">
			<img src="layout/icons/iconToggle.png" class="buttonToggleSidebarMenue" width="10" height="10" alt="" title="Seitennavigation ein-/ausblenden" />
			<div id="menueSidebarToggleContent">
			<?php require_once(FILE_MENUE_SIDEBAR); ?>
			<div class="clear"></div>
			</div>
		</div>
		<div id="contentArea2">
			<a name="top"></a>
			<div id="contentAreaElements">
				<h1><?php if(SHOW_PAGE_TITLE_ICONS) { echo '<img src="' . PATH_ICONS_MENUE_TITLES . 'iconTime.png" alt="" />'; } ?> <?php echo $thisTitle; ?></h1>

				<div id="searchFilterArea">
					<form name="formFilterSearch" method="post" action="<?php echo $_SERVER["PHP_SELF"]; ?>">
					<table border="0" cellpadding="0" cellspacing="0" class="searchFilterContent">
						<tr>
							<td>
								<label for="searchCustomerNumber">K-NR:</label>
								<input type="text" name="searchCustomerNumber" id="searchCustomerNumber" class="inputField_70" value="" />
							</td>
							<td>
								<label for="searchCustomerName">Kunde:</label>
								<input type="text" name="searchCustomerName" id="searchCustomerName" class="inputField_70" value="" />
							</td>							
							<td>
								<label for="searchContactDate">Gespr&auml;chsdatum:</label>
								<input type="text" name="searchContactDate" id="searchContactDate" class="inputField_70" value="" readonly="readonly" />
							</td>
							<td>
								<label for="searchRemindDate">Wiedervorlage-Datum:</label>
								<input type="text" name="searchRemindDate" id="searchRemindDate" class="inputField_70" value="" readonly="readonly" />
							</td>
							<td>
								<label for="searchCallbackDate">R&uuml;ckruf-Datum:</label>
								<input type="text" name="searchCallbackDate" id="searchCallbackDate" class="inputField_70" value="" readonly="readonly" />
							</td>
							<td>
								<label for="searchWord">Suchbegriff:</label>
								<input type="text" name="searchWord" id="searchWord" class="inputField_70" value="" />
							</td>							
							<td>
								<input type="hidden" name="editID" id="editID" value="" />
								<input type="submit" name="submitSearch" class="inputButton0" value="Suchen" />
							</td>
						</tr>
					</table>					
					</form>
				</div>

				<?php displayMessages(); ?>

				<?php
					// BOF GET COUNT ALL ROWS
						$sql_getAllRows = "
								SELECT

									COUNT(`" . TABLE_CUSTOMERS_PHONE_MARKETING . "`.`customersPhoneMarketingID`) AS `countAllRows`

								FROM `" . TABLE_CUSTOMERS_PHONE_MARKETING . "`
								
								LEFT JOIN `" . TABLE_CUSTOMERS . "`
								ON(`" . TABLE_CUSTOMERS_PHONE_MARKETING . "`.`customersPhoneMarketingCustomerID` = `" . TABLE_CUSTOMERS . "`.`customersID`)

								WHERE 1
									" . $sqlWhere . "
							";

						$rs_getAllRows = $dbConnection->db_query($sql_getAllRows);
						list(
							$countAllRows
						) = mysqli_fetch_array($rs_getAllRows);
					// EOF GET COUNT ALL ROWS
				
					if($_REQUEST["page"] == "" || $_REQUEST["page"] == 0) {
						$_REQUEST["page"] = 1;
					}

					if(MAX_DELIVERIES_PER_PAGE > 0) {
						$sql .= " LIMIT " . (($_REQUEST["page"] - 1) * MAX_DELIVERIES_PER_PAGE) . ", " . MAX_DELIVERIES_PER_PAGE." ";
					}

					$rs = $dbConnection->db_query($sql, $db_open);	

					$countRows = $countAllRows;

					$pagesCount = ceil($countRows / MAX_DELIVERIES_PER_PAGE);

					if($countRows > 0) {
				?>			
				
				<?php
					if($pagesCount > 1 && $_GET["loadAll"] != "true") {
						include(FILE_MENUE_PAGES);
					}
				?>
				<div class="contentDisplay">
					<table border="0" cellpadding="0" cellspacing="0" width="100%" class="displayOrders">
						<colgroup>
							<col />
							<col />
							<col />
							<col />
							<col />
							<col />
							<col />
							<col />
							<col />
							<col />
							<col />
							<col />
							<col />
						</colgroup>
						<thead>
							<tr>
								<th style="width:50px;">#</th>
								<th style="width:100px;">Gespr&auml;chsdatum</th>
								<th>KNR</th>
								<th>Firma</th>
								<th>Gespr&auml;chspartner</th>
								<th>Gespr&auml;chsinhalt</th>
								<th>Kampagne</th>
								<th>Bemerkungen</th>
								<th>Wiedervorlage</th>
								<th>Besuch?</th>
								<th>R&uuml;ckruf?</th>
								<th>Kontakt</th>
								<th>Benutzer</th>
							</tr>
						</thead>

						<tbody>
						<?php
							$countRow = 0;
													
							while($ds_getPhomeMarketingData = mysqli_fetch_assoc($rs)){								
								if($countRow%2 == 0) { $rowClass = 'row0'; }
								else { $rowClass = 'row1'; }
								
								if($todayDate == $ds_getPhomeMarketingData["customersPhoneMarketingCallbackDate"] || $todayDate == $ds_getPhomeMarketingData["customersPhoneMarketingRemindDate"]){
									$rowClass = "row2";
								}
								
								echo '<tr class="' . $rowClass . '">';
								echo '<td style="text-align:right;">';
								echo '<b>' . ($countRow + 1) . '.</b>';
								echo '</td>';
								
								$thisCellStyle = "";
								if($todayDate == $ds_getPhomeMarketingData["customersPhoneMarketingContactDate"]){
									$thisCellStyle = "background-color:#DBFFCF;font-weight:bold;";
								}
								echo '<td style="' . $thisCellStyle . '">';	
								echo formatDate($ds_getPhomeMarketingData["customersPhoneMarketingContactDate"], 'display');
								echo ',<br />' . substr($ds_getPhomeMarketingData["customersPhoneMarketingContactTime"], 0, -3) . ' Uhr';
								echo '</td>';	

								echo '<td>';
								echo '<b><a href="' . PAGE_EDIT_CUSTOMER. '?searchBoxCustomer=' . $ds_getPhomeMarketingData["customersKundennummer"] . '&amp;tab=tabs-14">';
								echo htmlentities($ds_getPhomeMarketingData["customersKundennummer"]);
								echo '</a></b>';
								echo '</td>';
								
								echo '<td><b>';
								echo htmlentities($ds_getPhomeMarketingData["customersFirmenname"]);
								if($ds_getPhomeMarketingData["customersFirmennameZusatz"] != ""){
									echo '<br />' . htmlentities($ds_getPhomeMarketingData["customersFirmennameZusatz"]);
								}
								echo '</b></td>';
								
								echo '<td>';
								echo htmlentities($ds_getPhomeMarketingData["customersPhoneMarketingConversationPerson"]);
								echo '</td>';
								
								echo '<td>';
								echo htmlentities($ds_getPhomeMarketingData["customersPhoneMarketingConversationContent"]);
								echo '</td>';
								
								echo '<td>';
								echo htmlentities($ds_getPhomeMarketingData["customersPhoneMarketingCampaign"]);
								echo '</td>';
								
								echo '<td>';														
								echo htmlentities($ds_getPhomeMarketingData["customersPhoneMarketingNotice"]);
								echo '</td>';

								$thisCellStyle = "background-color:#FEFFAF;";								
								echo '<td style="' . $thisCellStyle . '">';								
								echo formatDate($ds_getPhomeMarketingData["customersPhoneMarketingRemindDate"], "display");
								echo '</td>';
								
								echo '<td style="text-align:center;">';
								if($ds_getPhomeMarketingData["customersPhoneMarketingVisitRequest"] == '1'){
									$thisImage = "iconOk.png";
									$thisAlt = "Besuch erw&uuml;nscht";
								}
								else {
									$thisImage = "iconNotOk.png";
									$thisAlt = "Besuch nicht erw&uuml;nscht";
								}														
								echo '<img src="layout/icons/' . $thisImage . '" alt="' . $thisAlt . '" title="' . $thisAlt . '" />';														
								echo '</td>';
								
								// background-color:#FEFFAF;
								
								$thisCellStyle = "background-color:#FEFFAF;";								
								echo '<td style="' . $thisCellStyle . '">';	
								echo formatDate($ds_getPhomeMarketingData["customersPhoneMarketingCallbackDate"], "display");
								echo '</td>';
								
								echo '<td style="line-height:16px;">';
								if($ds_getPhomeMarketingData["customersTelefon1"] != ""){
									echo '<span class="dataList"> &bull; <b><img src="layout/icons/iconPhone2.png" width="14" height="14" alt="Tel." title="Tel." /></b>: ' . formatPhoneNumber($ds_getPhomeMarketingData["customersTelefon1"]) . '</span>';
								}
								if($ds_getPhomeMarketingData["customersTelefon2"] != ""){
									echo '<span class="dataList"> &bull; <b><img src="layout/icons/iconPhone2.png" width="14" height="14" alt="Tel." title="Tel." /></b>: ' . formatPhoneNumber($ds_getPhomeMarketingData["customersTelefon2"]) . '</span>';
								}
								if($ds_getPhomeMarketingData["customersMobil1"] != ""){
									echo '<span class="dataList"> &bull; <b><img src="layout/icons/iconMobile.png" width="14" height="14" alt="Mobil" title="Mobil" /></b>: ' . formatPhoneNumber($ds_getPhomeMarketingData["customersMobil1"]) . '</span>';
								}
								if($ds_getPhomeMarketingData["customersMobil2"] != ""){
									echo '<span class="dataList"> &bull; <b><img src="layout/icons/iconMobile.png" width="14" height="14" alt="Mobil" title="Mobil" /></b>: ' . formatPhoneNumber($ds_getPhomeMarketingData["customersMobil2"]) . '</span>';
								}
								if($ds_getPhomeMarketingData["customersMail1"] != ''){
									echo '<span class="dataList"> &bull; <b><img src="layout/icons/iconMail2.png" width="14" height="14" alt="Mail" title="Mail" /></b>: <a href="mailto:' . $ds_getPhomeMarketingData["customersMail1"] . '">' . ($ds_getPhomeMarketingData["customersMail1"]) . '</a></span>';
								}
								if($ds_getPhomeMarketingData["customersMail2"] != ''){
									echo '<span class="dataList"> &bull; <b><img src="layout/icons/iconMail2.png" width="14" height="14" alt="Mail" title="Mail" /></b>: <a href="mailto:' . $ds_getPhomeMarketingData["customersMail2"] . '">' . ($ds_getPhomeMarketingData["customersMail2"]) . '</a></span>';
								}
								echo '</td>';
								
								echo '<td>';
								echo htmlentities($arrUserDatas[$ds_getPhomeMarketingData["customersPhoneMarketingUser"]]["usersFirstName"] . ' ' . $arrUserDatas[$ds_getPhomeMarketingData["customersPhoneMarketingUser"]]["usersLastName"]);
								echo '</td>';
										
								echo '</tr>';
								$countRow++;
							}
						?>
						</tbody>
					</table>
				</div>
				<?php
					if($pagesCount > 1 && $_GET["loadAll"] != "true") {
						include(FILE_MENUE_PAGES);
					}
				?>
				<div style="text-align:right;" class="menueToTop">
					<a href="#top">nach oben</a>
				</div>
				<?php
					}
					else {
						echo '<p class="warningArea">Es wurden keine Daten gefunden!</p>';
					}

				?>
				<div class="clear"></div>
			</div>
			<div class="clear"></div>
		</div>
	</div>
</div>

<script language="javascript" type="text/javascript">
	<!--
	/* <![CDATA[ */
		$(document).ready(function() {
			$(function() {
				$('#searchContactDate').datepicker($.datepicker.regional["de"]);
				// $('#searchContactDate').datepicker("option", "maxDate", "0" );

				$('#searchRemindDate').datepicker($.datepicker.regional["de"]);
				// $('#searchRemindDate').datepicker("option", "maxDate", "0" );	
				
				$('#searchCallbackDate').datepicker($.datepicker.regional["de"]);
				// $('#searchCallbackDate').datepicker("option", "maxDate", "0" );	
			});
			
			$('.buttonToggleSidebarMenue').click(function() {
				$('#menueSidebarToggleContent').toggle();
			});
		});
	/* ]]> */
	// -->
</script>

<?php require_once('inc/footerHTML.inc.php'); ?>