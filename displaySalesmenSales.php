<?php
	require_once('inc/requires.inc.php');

	if(!$arrGetUserRights["displayDistribution"]) {
		header('location: ' . PAGE_EXIT_LOCATION);
		exit;
	}

	$jswindowMessage = '';
	$warningMessage = '';
	$errorMessage = '';
	$successMessage = '';
	$infoMessage = '';

	$checkSourceTypesID = 8; // SALESMAN

	$dbConnection = new DB_Connection();
	$db_open = $dbConnection->db_connect();

	// BOF READ VERTRETER
		$arrSalesmenDatas = getSalesmen();
	// EOF READ VERTRETER

	// BOF READ CUSTOMER TYPES
		$arrCustomerTypesDatas = getCustomerTypes();
	// EOF READ CUSTOMER TYPES

	// BOF READ ORDER TYPES
		$arrOrderTypesDatas = getOrderTypes();
	// EOF READ ORDER TYPES

	// BOF READ ORDER TYPES
		$arrOrdersSourceDatas = getOrdersSourceTypes();
	// EOF READ ORDER TYPES

	// BOF READ ORDER TYPES
		$arrMandatoriesDatas = getMandatories();
		if($_POST["searchMandatory"] == ''){
			$_POST["searchMandatory"] = MANDATOR;
		}
	// EOF READ ORDER TYPES

	$defaultORDER = "orderDocumentsDocumentDate";
	$defaultSORT = "ASC";

	if($_POST["searchInterval"] == ""){
		$_POST["searchInterval"] = "MONTH";
	}

	$todayYear = date("Y");
	if($_POST["searchYear"] == ""){
		$_POST["searchYear"] = $todayYear;
	}

	// BOF READ VERTRETER
		$arrAgentDatas = getAgents();
		$arrAgentDatas2 = getAgents2();
		$arrUseAgentDatas = $arrAgentDatas2;

		// ADD OUR COMPANY (MANDANT)
		$arrCustomerTypeMandatorsDatas = getCustomerTypeMandators();
		if(!empty($arrCustomerTypeMandatorsDatas)){
			foreach($arrCustomerTypeMandatorsDatas as $thisKey => $thisValue){
				$arrUseAgentDatas[$thisKey] = $thisValue;
			}
		}
	// EOF READ VERTRETER
?>
<?php
	require_once('inc/headerHTML.inc.php');
	$thisTitle = "Vertreter-Verk&auml;ufe";
	if($_POST["searchSalesman"] != '') {
		$thisTitle .= ': <span class="headerSelectedEntry">' . $arrSalesmenDatas[$_POST["searchSalesman"]]["customersFirmenname"] . '</span>';
	}

	$headerHTML = preg_replace("/{###TITLE###}/", strip_tags($thisTitle), $headerHTML);
	echo $headerHTML;

	require_once(FILE_MENUE_TOP);

	$sqlSortField = "";
	$sqlSortDirection = "";
	$sqlWhere = "";
?>
<div id="xmainArea">
	<div id="xmainContent">
		<div id="menueSidebarToggleArea">
			<img src="layout/icons/iconToggle.png" class="buttonToggleSidebarMenue" width="10" height="10" alt="" title="Seitennavigation ein-/ausblenden" />
			<div id="menueSidebarToggleContent">
			<?php require_once(FILE_MENUE_SIDEBAR); ?>
			<div class="clear"></div>
			</div>
		</div>
		<div id="contentArea2">
			<a name="top"></a>
			<div id="contentAreaElements">
				<h1><?php if(SHOW_PAGE_TITLE_ICONS) { echo '<img src="' . PATH_ICONS_MENUE_TITLES . 'statistics.png" alt="" />'; } ?> <?php echo $thisTitle; ?></h1>

				<p class="infoArea">Angezeigt werden nur KZH-St&uuml;ckzahlen aus Auftragsbest&auml;tigungen!</p>
				<div id="searchFilterArea">
					<form name="formSearchDatas" method="post" action="<?php echo $_SERVER["PHP_SELF"]; ?>">
					<table border="0" cellpadding="0" cellspacing="0" class="searchFilterContent">
						<tr>
							<!--
							<td>
								<label for="searchSalesman">Vertreter:</label>
								<select name="searchSalesman" id="searchSalesman" class="inputField_300">
									<option value=""> - </option>
									<?php
										if(!empty($arrSalesmenDatas)) {
											$thisMarker = '';
											foreach($arrSalesmenDatas as $thisKey => $thisValue) {
												if($arrSalesmenDatas[$thisKey]["customersTyp"] != 2){
													if($thisMarker != $arrSalesmenDatas[$thisKey]["customersTyp"]){
														echo '<option value="" class="row3" style="font-weight:bold;">' . $arrCustomerTypesDatas[$arrSalesmenDatas[$thisKey]["customersTyp"]]["customerTypesName"] . '</option>';
														$thisMarker = $arrSalesmenDatas[$thisKey]["customersTyp"];
													}
													$selected = '';
													if($thisKey == $_POST["searchSalesman"]) {
														$selected = ' selected ';
													}
													echo '<option value="' . $thisKey . '" '.$selected.' >' . ($arrSalesmenDatas[$thisKey]["customersFirmenname"]). ' | K-NR: '.$arrSalesmenDatas[$thisKey]["customersKundennummer"].'</option>';
												}
											}
										}
									?>
								</select>
							</td>
							-->
							<td>
								<label for="searchMandatory">Mandant:</label>
								<select name="searchMandatory" id="searchMandatory" class="inputField_300">
									<!--<option value=""> - </option>-->
									<?php
										if(!empty($arrMandatoriesDatas)) {
											$thisMarker = '';
											foreach($arrMandatoriesDatas as $thisKey => $thisValue) {
												$selected = '';
												if($arrMandatoriesDatas[$thisKey]["mandatoriesShortName"] == $_POST["searchMandatory"]) {
													$selected = ' selected ';
												}
												echo '<option value="' . $arrMandatoriesDatas[$thisKey]["mandatoriesShortName"]. '" '.$selected.' >' . ($arrMandatoriesDatas[$thisKey]["mandatoriesName"]).'</option>';
											}
										}
									?>
								</select>
							</td>
							<td>
								<label for="searchInterval">Zeitraum:</label>
								<select name="searchInterval" id="searchInterval" class="inputField_130">
									<option value=""></option>
									<option value="WEEK" <?php if($_POST["searchInterval"] == 'WEEK'){ echo ' selected="selected" '; } ?> >pro Woche</option>
									<option value="MONTH" <?php if($_POST["searchInterval"] == 'MONTH'){ echo ' selected="selected" '; } ?> >pro Monat</option>
									<option value="QUARTER" <?php if($_POST["searchInterval"] == 'QUARTER'){ echo ' selected="selected" '; } ?> >pro Vierteljahr</option>
									<option value="YEAR" <?php if($_POST["searchInterval"] == 'YEAR'){ echo ' selected="selected" '; } ?> >pro Jahr</option>
								 </select>
							</td>

							<td>
								<label for="searchInterval">Jahr:</label>
								<select name="searchYear" id="searchYear" class="inputField_130">
									<option value="ALL" <?php if($_POST["searchYear"] == "ALL"){ echo ' selected="selected" '; } ?>>Alle Jahre</option>
									<?php
										$todayYear = date("Y");
										for($i = $todayYear ; $i > 2012 ; $i--){
											$selected = '';
											if($i == $_POST["searchYear"]){
												$selected = ' selected="selected" ';
											}
											echo '<option value="' . $i . '" ' . $selected . ' >' . $i . '</option>';
										}
									?>
								</select>
							</td>

							<td>
								<input type="hidden" name="editID" id="editID" value="" />
								<input type="submit" name="submitSearch" class="inputButton0" value="Anzeigen" />
							</td>
						</tr>
					</table>
					<input type="hidden" name="submitSearchForm" id="submitSearchForm" value="1" />
					</form>
				</div>

				<div class="contentDisplay">
					<?php
						$TABLE_ORDER_INVOICES = str_replace(MANDATOR, $_POST["searchMandatory"], TABLE_ORDER_INVOICES);
						$TABLE_ORDER_INVOICES_DETAILS = str_replace(MANDATOR, $_POST["searchMandatory"], TABLE_ORDER_INVOICES_DETAILS);

						$TABLE_ORDER_CONFIRMATIONS = str_replace(MANDATOR, $_POST["searchMandatory"], TABLE_ORDER_CONFIRMATIONS);
						$TABLE_ORDER_CONFIRMATIONS_DETAILS = str_replace(MANDATOR, $_POST["searchMandatory"], TABLE_ORDER_CONFIRMATIONS_DETAILS);

						// EOF CONSTRUCT WHERE CLAUSE DEPENDING ON SELECTED DATA
						$arrWhereInvoices = array();
						$arrWhereConfirmations = array();
						$arrWhereProcess = array();

						/*
						if(!empty($arrSalesmenDatas)) {
							foreach($arrSalesmenDatas as $thisKey => $thisValue) {
								#if($arrSalesmenDatas[$thisKey]["customersTyp"] != 2){
								if($arrSalesmenDatas[$thisKey]["customersID"] != 13301){
									$arrWhereInvoices[]			= " `" . $TABLE_ORDER_INVOICES . "`.`orderDocumentsSalesman` = " . $arrSalesmenDatas[$thisKey]["customersID"] . " ";
									$arrWhereConfirmations[]	= " `" . $TABLE_ORDER_CONFIRMATIONS . "`.`orderDocumentsSalesman` = " . $arrSalesmenDatas[$thisKey]["customersID"] . " ";
								}
							}
						}
						*/

						if(!empty($arrUseAgentDatas)) {
							foreach($arrUseAgentDatas as $thisKey => $thisValue) {
								$arrWhereConfirmations[]	= "
										`" . TABLE_CUSTOMERS . "`.`customersID` = " . $arrUseAgentDatas[$thisKey]["customersID"] . "
									";
							}
						}
						if($_COOKIE["isAdmin"] == 'xxx'){
							$arrWhereConfirmations = array();
							$arrWhereConfirmations[]	= "
											`" . TABLE_CUSTOMERS . "`.`customersID` = 5088
										";
						}

						$dateFieldConfirmations = "";
						$dateFieldProcess = "";

						if($_POST["searchInterval"] == "WEEK"){
							$whereConfirmations .= "";
							$dateFieldConfirmations = "
								CONCAT(
									IF(DATE_FORMAT(`tempTable`.`orderDocumentsDocumentDate`, '%m') = '12' AND DATE_FORMAT(`tempTable`.`orderDocumentsDocumentDate`, '%v') = '01', (DATE_FORMAT(`tempTable`.`orderDocumentsDocumentDate`, '%Y') + 1),  DATE_FORMAT(`tempTable`.`orderDocumentsDocumentDate`, '%Y')),
									'#',
									DATE_FORMAT(`tempTable`.`orderDocumentsDocumentDate`, '%v')
								) AS `interval`
							";

							$dateFieldProcess = "
								CONCAT(
									IF(DATE_FORMAT(`tempTable`.`ordersBestellDatum`, '%m') = '12' AND DATE_FORMAT(`tempTable`.`ordersBestellDatum`, '%v') = '01', (DATE_FORMAT(`tempTable`.`ordersBestellDatum`, '%Y') + 1),  DATE_FORMAT(`tempTable`.`ordersBestellDatum`, '%Y')),
									'#',
									DATE_FORMAT(`tempTable`.`ordersBestellDatum`, '%v')
								) AS `interval`
							";
						}
						else if($_POST["searchInterval"] == "MONTH"){
							$whereConfirmations .= "";
							$dateFieldConfirmations = "
								CONCAT(
									DATE_FORMAT(`tempTable`.`orderDocumentsDocumentDate`, '%Y'),
									'#',
									DATE_FORMAT(`tempTable`.`orderDocumentsDocumentDate`, '%m')
								) AS `interval`
							";
							$dateFieldProcess = "
								CONCAT(
									DATE_FORMAT(`tempTable`.`ordersBestellDatum`, '%Y'),
									'#',
									DATE_FORMAT(`tempTable`.`ordersBestellDatum`, '%m')
								) AS `interval`
							";
						}
						else if($_POST["searchInterval"] == "QUARTER"){
							$whereConfirmations .= "";
							$dateFieldConfirmations = "
								CONCAT(
									DATE_FORMAT(`tempTable`.`orderDocumentsDocumentDate`, '%Y'),
									'#',
									IF(DATE_FORMAT(`tempTable`.`orderDocumentsDocumentDate`, '%m') = '01' OR DATE_FORMAT(`tempTable`.`orderDocumentsDocumentDate`, '%m') = '02' OR DATE_FORMAT(`tempTable`.`orderDocumentsDocumentDate`, '%m') = '03', '1. Quartal',
										IF(DATE_FORMAT(`tempTable`.`orderDocumentsDocumentDate`, '%m') = '04' OR DATE_FORMAT(`tempTable`.`orderDocumentsDocumentDate`, '%m') = '05' OR DATE_FORMAT(`tempTable`.`orderDocumentsDocumentDate`, '%m') = '06', '2. Quartal',
											IF(DATE_FORMAT(`tempTable`.`orderDocumentsDocumentDate`, '%m') = '07' OR DATE_FORMAT(`tempTable`.`orderDocumentsDocumentDate`, '%m') = '08' OR DATE_FORMAT(`tempTable`.`orderDocumentsDocumentDate`, '%m') = '09', '3. Quartal',
												IF(DATE_FORMAT(`tempTable`.`orderDocumentsDocumentDate`, '%m') = '10' OR DATE_FORMAT(`tempTable`.`orderDocumentsDocumentDate`, '%m') = '11' OR DATE_FORMAT(`tempTable`.`orderDocumentsDocumentDate`, '%m') = '12', '4. Quartal',
													''
												)
											)
										)
									)
								) AS `interval`
							";

							$dateFieldProcess = "
								CONCAT(
									DATE_FORMAT(`tempTable`.`ordersBestellDatum`, '%Y'),
									'#',
									IF(DATE_FORMAT(`tempTable`.`ordersBestellDatum`, '%m') = '01' OR DATE_FORMAT(`tempTable`.`ordersBestellDatum`, '%m') = '02' OR DATE_FORMAT(`tempTable`.`ordersBestellDatum`, '%m') = '03', '1. Quartal',
										IF(DATE_FORMAT(`tempTable`.`ordersBestellDatum`, '%m') = '04' OR DATE_FORMAT(`tempTable`.`ordersBestellDatum`, '%m') = '05' OR DATE_FORMAT(`tempTable`.`ordersBestellDatum`, '%m') = '06', '2. Quartal',
											IF(DATE_FORMAT(`tempTable`.`ordersBestellDatum`, '%m') = '07' OR DATE_FORMAT(`tempTable`.`ordersBestellDatum`, '%m') = '08' OR DATE_FORMAT(`tempTable`.`ordersBestellDatum`, '%m') = '09', '3. Quartal',
												IF(DATE_FORMAT(`tempTable`.`ordersBestellDatum`, '%m') = '10' OR DATE_FORMAT(`tempTable`.`ordersBestellDatum`, '%m') = '11' OR DATE_FORMAT(`tempTable`.`ordersBestellDatum`, '%m') = '12', '4. Quartal',
													''
												)
											)
										)
									)
								) AS `interval`
							";
						}
						else if($_POST["searchInterval"] == "YEAR"){
							$whereConfirmations .= "";
							$dateFieldConfirmations = "
								CONCAT(
									DATE_FORMAT(`tempTable`.`orderDocumentsDocumentDate`, '%Y'),
									'#',
									DATE_FORMAT(`tempTable`.`orderDocumentsDocumentDate`, '%Y')
								) AS `interval`
							";

							$dateFieldProcess = "
								CONCAT(
									DATE_FORMAT(`tempTable`.`ordersBestellDatum`, '%Y'),
									'#',
									DATE_FORMAT(`tempTable`.`ordersBestellDatum`, '%Y')
								) AS `interval`
							";
						}

						$whereYearConfirmations = "";
						if($_POST["searchYear"] != "" && $_POST["searchYear"] != "ALL"){
							$whereYearConfirmations = "
								AND DATE_FORMAT(`orderDocumentsDocumentDate`, '%Y') = '" . $_POST["searchYear"] . "'
							";

							$whereYearProcess = "
								AND DATE_FORMAT(`ordersBestellDatum`, '%Y') = '" . $_POST["searchYear"] . "'
							";
						}
					// EOF CONSTRUCT WHERE CLAUSE DEPENDING ON SELECTED DATA

					$arrIntervals = array();
					$arrDatas = array();

					// BOF GET CONFIRMATIONS DATA
						$sqlConfirmations = "
								SELECT
										" . $dateFieldConfirmations . ",
										`tempTable`.`customersID`,
										`tempTable`.`customersKundennummer`,
										`tempTable`.`customersTyp`,
										`tempTable`.`orderDocumentDetailOrderType` AS `orderType`,
										'CONFIRMATIONS' AS `dataType`,
										SUM(`tempTable`.`orderDocumentDetailProductQuantity`) AS `countItemsKZH`,
										GROUP_CONCAT(`tempTable`.`ordersSourceType`, ':', `tempTable`.`orderDocumentDetailProductQuantity` SEPARATOR ';') AS `ordersSourceTypes`,

										GROUP_CONCAT(`tempTable`.`orderDocumentDetailOrderID`) AS `tempOrderIDs`

									FROM (

										SELECT

											`customersID`,
											`customersKundennummer`,
											`customersFirmenname`,
											`customersTyp`,

											`orderDocumentsDocumentDate`,

											`orderDocumentDetailOrderType`,
											`orderDocumentDetailProductKategorieID`,
											`orderDocumentDetailProductQuantity`,
											`orderDocumentDetailOrderID`,

											`ordersSourceType`

											FROM (


										SELECT

											`customersID`,
											`customersKundennummer`,
											`customersFirmenname`,
											`customersTyp`,

											`orderDocumentsDocumentDate`,

											`orderDocumentDetailOrderType`,
											`orderDocumentDetailProductKategorieID`,
											`orderDocumentDetailProductQuantity`,
											`orderDocumentDetailOrderID`,

											`ordersSourceType`

											FROM `" . TABLE_CUSTOMERS . "`

											LEFT JOIN `" . $TABLE_ORDER_CONFIRMATIONS . "`
											ON(`" . TABLE_CUSTOMERS . "`.`customersKundennummer` = `" . $TABLE_ORDER_CONFIRMATIONS . "`.`orderDocumentsCustomerNumber`)

											LEFT JOIN `" . $TABLE_ORDER_CONFIRMATIONS_DETAILS . "`
											ON(`" . $TABLE_ORDER_CONFIRMATIONS . "`.`orderDocumentsID` = `" . $TABLE_ORDER_CONFIRMATIONS_DETAILS . "`.`orderDocumentDetailDocumentID`)

											LEFT JOIN `". TABLE_ORDERS . "`
											ON(`" . $TABLE_ORDER_CONFIRMATIONS_DETAILS . "`.`orderDocumentDetailOrderID` = `". TABLE_ORDERS . "`.`ordersID`)

											WHERE 1
												" . $whereYearConfirmations . "
												AND (" . implode(" OR ", $arrWhereConfirmations). ")
												AND `" . $TABLE_ORDER_CONFIRMATIONS_DETAILS . "`.`orderDocumentDetailPosType` = 'product'
												AND SUBSTRING(`orderDocumentDetailProductKategorieID`, 1, 3) = '001'

										UNION

										SELECT

											`customersID`,
											`customersKundennummer`,
											`customersFirmenname`,
											`customersTyp`,

											`orderDocumentsDocumentDate`,

											`orderDocumentDetailOrderType`,
											`orderDocumentDetailProductKategorieID`,
											`orderDocumentDetailProductQuantity`,
											`orderDocumentDetailOrderID`,

											`ordersSourceType`

											FROM `" . TABLE_CUSTOMERS . "`

											LEFT JOIN `" . $TABLE_ORDER_CONFIRMATIONS . "`
											ON(`" . TABLE_CUSTOMERS . "`.`customersID` = `" . $TABLE_ORDER_CONFIRMATIONS . "`.`orderDocumentsSalesman`)

											LEFT JOIN `" . $TABLE_ORDER_CONFIRMATIONS_DETAILS . "`
											ON(`" . $TABLE_ORDER_CONFIRMATIONS . "`.`orderDocumentsID` = `" . $TABLE_ORDER_CONFIRMATIONS_DETAILS . "`.`orderDocumentDetailDocumentID`)

											LEFT JOIN `". TABLE_ORDERS . "`
											ON(`" . $TABLE_ORDER_CONFIRMATIONS_DETAILS . "`.`orderDocumentDetailOrderID` = `". TABLE_ORDERS . "`.`ordersID`)

											WHERE 1
												" . $whereYearConfirmations . "
												AND (" . implode(" OR ", $arrWhereConfirmations). ")
												AND `" . $TABLE_ORDER_CONFIRMATIONS_DETAILS . "`.`orderDocumentDetailPosType` = 'product'
												AND SUBSTRING(`orderDocumentDetailProductKategorieID`, 1, 3) = '001'

									) AS `tempTable2`

									GROUP BY `orderDocumentDetailOrderID`

									) AS `tempTable`

								GROUP BY CONCAT(`tempTable`.`customersID`, '#', `interval`, '#', `tempTable`.`orderDocumentDetailOrderType`)
								ORDER BY `interval` DESC, `customersTyp` DESC, `customersFirmenname`
							";


						$rsConfirmations = $dbConnection->db_query($sqlConfirmations);

						$countTotalRowsConfirmations = $dbConnection->db_getMysqlNumRows($rsConfirmations);

						while($ds = mysqli_fetch_assoc($rsConfirmations)) {
							$arrIntervals[] = $ds["interval"];
							foreach(array_keys($ds) as $field){
								$arrDatas[$ds["interval"]][$ds["customersTyp"]][$ds["customersID"]][$ds["dataType"]][$ds["orderType"]][$field] = $ds[$field];
							}
						}

						$arrIntervals = array_unique($arrIntervals);
					// EOF GET CONFIRMATIONS DATA

					// BOF GET OPEN ORDERS DATA
						$sqlOrdersOpen = "
								SELECT
									" . $dateFieldProcess . ",
									`tempTable`.`customersID`,
									`tempTable`.`customersKundennummer`,
									`tempTable`.`customersTyp`,
									`tempTable`.`ordersOrderType` AS `orderType`,
									'PROCESS_OPEN' AS `dataType`,
									SUM(`tempTable`.`ordersArtikelMenge`) AS `countItemsKZH`,
									GROUP_CONCAT(`tempTable`.`ordersSourceType`, ':', `tempTable`.`ordersArtikelMenge` SEPARATOR ';') AS `ordersSourceTypes`

									FROM (
										SELECT

											`customersID`,
											`customersKundennummer`,
											`customersFirmenname`,
											`customersTyp`,

											`ordersBestellDatum`,
											`ordersArtikelKategorieID`,
											`ordersArtikelMenge`,
											`ordersID`,

											`ordersOrderType`,

											`ordersSourceType`

											FROM (

											SELECT

											`customersID`,
											`customersKundennummer`,
											`customersFirmenname`,
											`customersTyp`,

											`ordersBestellDatum`,
											`ordersArtikelKategorieID`,
											`ordersArtikelMenge`,
											`ordersID`,

											`ordersOrderType`,

											`ordersSourceType`

											FROM `". TABLE_ORDERS . "`

											LEFT JOIN `" . TABLE_CUSTOMERS . "`
											ON(
												`". TABLE_ORDERS . "`.`ordersCustomerID` = `" . TABLE_CUSTOMERS . "`.`customersID`
												OR `". TABLE_ORDERS . "`.`ordersVertreterID` = `" . TABLE_CUSTOMERS . "`.`customersID`
											)

											WHERE 1
												AND `ordersStatus` = 1
												AND `ordersMandant` = 'bctr'
												" . $whereYearProcess . "
												AND (" . implode(" OR ", $arrWhereConfirmations). ")
												AND SUBSTRING(`ordersArtikelKategorieID`, 1, 3) = '001'
											GROUP BY `ordersID`
									) AS `tempTable2`

									GROUP BY `ordersID`
									) AS `tempTable`

									GROUP BY CONCAT(`tempTable`.`customersID`, '#', `interval`, '#', `tempTable`.`ordersOrderType`)
									ORDER BY `interval` DESC, `customersTyp` DESC, `customersFirmenname`
							";

#dd('sql_ordersOpen');
						$rsOrdersOpen = $dbConnection->db_query($sqlOrdersOpen);

						$countTotalRowsOrdersOpen = $dbConnection->db_getMysqlNumRows($rsOrdersOpen);
#
						while($ds = mysqli_fetch_assoc($rsOrdersOpen)) {
							$arrIntervals[] = $ds["interval"];
							foreach(array_keys($ds) as $field){
								$arrDatas[$ds["interval"]][$ds["customersTyp"]][$ds["customersID"]][$ds["dataType"]][$ds["orderType"]][$field] = $ds[$field];
							}
						}
#dd('arrDatas');
						$arrIntervals = array_unique($arrIntervals);
					// EOF GET CONFIRMATIONS DATA


						if(!empty($arrDatas)){
							echo '<table width="100%" cellpadding="0" cellspacing="0" class="displayOrders">';

							echo '<tr>';
							echo '<th style="width:45px" rowspan="2">#</th>';
							#echo '<th style="width:80px">Zeitraum</th>';
							echo '<th style="width:80px" rowspan="2">Vertreter</th>';
							echo '<th style="width:40px" rowspan="2">KNR</th>';
							echo '<th class="spacer" rowspan="2"></th>';
							#echo '<th style="width:20px">Typ</th>';
							echo '<th style="width:200px" colspan="' . (1 + count($arrOrderTypesDatas)) . '">Anzahl KZH aus ABs</th>';

							echo '<th class="spacer" rowspan="2"></th>';
							echo '<th colspan="' . (1 + count($arrOrderTypesDatas)) . '">Anzahl KZH offene Auftr&auml;ge</th>';
							echo '<th class="spacer"></th>';
							echo '<th rowspan="2">GESAMT (AB + OFFEN)</th>';
							#echo '<th colspan="2">Anzahl KZH aus REs</th>';
							#echo '<th style="width:50px">Info</th>';
							echo '</tr>';

							echo '<tr>';
							echo '<th>AB insg.</th>';
							foreach($arrOrderTypesDatas as $thisOrderTypesKey => $thisOrderTypesValue){
								echo '<th style="width:20px">' . $arrOrderTypesDatas[$thisOrderTypesKey]["orderTypesName"] . '</th>';
							}

							echo '<th>OFFEN insg.</th>';

							foreach($arrOrderTypesDatas as $thisOrderTypesKey => $thisOrderTypesValue){
								echo '<th style="width:20px">' . $arrOrderTypesDatas[$thisOrderTypesKey]["orderTypesName"] . '</th>';
							}
							echo '</tr>';

							$countRow = 0;
							$thisMarker = "";
							$thisMarker2 = "";
							$thisMarker3 = "";

							// BOF GET TOTAL SUM PER INTERVAL
							function getTotalSumsPerInterval($dataType, $arrDatas){
								global $arrOrderTypesDatas, $arrIntervals;
								$arrReturnData = array();

								foreach($arrIntervals as $thisIntervalKey => $thisIntervalValue) {
									foreach($arrDatas[$thisIntervalValue] as $thisCustomerTypeKey => $thisCustomerTypeValue){
										$tempCount = 0;
										foreach($thisCustomerTypeValue as $thisDatasKey => $thisDatasValue){
											foreach($arrOrderTypesDatas as $thisOrderTypesKey => $thisOrderTypesValue){
												$thisOrderValue = $thisDatasValue[$dataType][$thisOrderTypesKey];
												#echo 'thisOrderValue<pre>';
												#print_r($thisOrderValue);
												#echo '</pre>';
												if($thisOrderValue["countItemsKZH"] == '') { $thisOrderValue["countItemsKZH"] = 0; }
												$tempCount += $thisOrderValue["countItemsKZH"];
											}
										}
										$arrTotalCountItemsPerIntervalAndCustomerType[$thisIntervalValue][$thisCustomerTypeKey]["KZH"] = $tempCount;
										$arrTotalCountItemsPerInterval[$thisIntervalValue]["KZH"] += $tempCount;
									}
								}


								$arrReturnData = array(
									"PerInterval" => $arrTotalCountItemsPerInterval,
									"PerIntervalAndCustomerType" => $arrTotalCountItemsPerIntervalAndCustomerType
								);

								return $arrReturnData;
							}

							$arrTotalCountItems = array()							;
							$arrTotalCountItems["CONFIRMATIONS"] = getTotalSumsPerInterval("CONFIRMATIONS", $arrDatas);
							$arrTotalCountItems["PROCESS_OPEN"] = getTotalSumsPerInterval("PROCESS_OPEN", $arrDatas);

							// EOF GET TOTAL SUM PER INTERVAL
							foreach($arrIntervals as $thisIntervalKey => $thisIntervalValue) {
								$countRow = 0;
								$thisMarker3 = "";
								foreach($arrDatas[$thisIntervalValue] as $thisCustomerTypeKey => $thisCustomerTypeValue){
									foreach($thisCustomerTypeValue as $thisDatasKey => $thisDatasValue){
										$arrTemp = explode('#', $thisIntervalValue);
										if($thisMarker != $arrTemp[0]) {
											$thisMarker = $arrTemp[0];
											echo '<tr><td colspan="' . (9 + 2 * count($arrOrderTypesDatas)) . '" class="tableRowTitle1">' . $arrTemp[0] . '</td></tr>';
										}
										$arrTemp = explode('#', $thisIntervalValue);
										if($thisMarker2 != $arrTemp[1]) {
											$thisMarker2 = $arrTemp[1];

											echo '<tr>';
											echo '<td colspan="3" class="tableRowTitle2" style="border-top:4px double #666;">';
											if($_POST["searchInterval"] == "MONTH"){
												$thisMonth = getTimeNames($thisMarker2, 'month', 'long');
												echo $thisMonth . ' ' . $arrTemp[0];
											}
											else {
												if($_POST["searchInterval"] == "WEEK"){ echo 'KW '; }
												echo $thisMarker2;
											}
											echo '</td>';

											echo '<td class="spacer"></td>';

											echo '<td class="tableRowTotal4" style="border-top:4px double #666;text-align:right;">';
											echo number_format($arrTotalCountItems["CONFIRMATIONS"]["PerInterval"][$thisIntervalValue]["KZH"], 0, ",", ".");
											echo '</td>';

											echo '<td colspan="' . (0 + 1 * count($arrOrderTypesDatas)) . '" class="tableRowTitle2" style="border-top:4px double #666;">';

											echo '<td class="spacer"></td>';

											echo '<td class="tableRowTotal4" style="border-top:4px double #666;text-align:right;">';
											echo number_format($arrTotalCountItems["PROCESS_OPEN"]["PerInterval"][$thisIntervalValue]["KZH"], 0, ",", ".");
											echo '</td>';

											echo '<td colspan="' . (0 + 1 * count($arrOrderTypesDatas)) . '" class="tableRowTitle2" style="border-top:4px double #666;">';
											echo '</td>';

											echo '<td class="spacer"></td>';

											echo '<td class="tableRowTotal4" style="border-top:4px double #666;text-align:right;">';
											echo number_format(($arrTotalCountItems["CONFIRMATIONS"]["PerInterval"][$thisIntervalValue]["KZH"] + $arrTotalCountItems["PROCESS_OPEN"]["PerInterval"][$thisIntervalValue]["KZH"]), 0, ",", ".");
											echo '</td>';

											echo '</tr>';
										}
										if($thisMarker3 != $thisCustomerTypeKey) {
											$thisMarker3 = $thisCustomerTypeKey;
											echo '<tr>';
												echo '<td style="text-align:right;font-size:14px;"><b>&dArr;</b></td>';
												echo '<td colspan="2" class="tableRowTitle3" style="border-top:1px dotted #000;">';
												echo $arrCustomerTypesDatas[$thisCustomerTypeKey]["customerTypesName"];
												echo '</td>';

												echo '<td class="spacer"></td>';

												echo '<td class="tableRowTotal3" style="text-align:right;border-top:1px dotted #000;">';
												echo '<b>' . number_format($arrTotalCountItems["CONFIRMATIONS"]["PerIntervalAndCustomerType"][$thisIntervalValue][$thisCustomerTypeKey]["KZH"], 0, ",", ".") . '</b>';
												echo '</td>';

												echo '<td colspan="' . (0 + count($arrOrderTypesDatas)) . '" class="tableRowTitle3" style="border-top:1px dotted #000;"></td>';

												echo '<td class="spacer"></td>';

												echo '<td class="tableRowTotal3" style="text-align:right;border-top:1px dotted #000;">';
												echo '<b>' . number_format($arrTotalCountItems["PROCESS_OPEN"]["PerIntervalAndCustomerType"][$thisIntervalValue][$thisCustomerTypeKey]["KZH"], 0, ",", ".") . '</b>';
												echo '</td>';

												echo '<td colspan="' . (0 + count($arrOrderTypesDatas)) . '" class="tableRowTitle3" style="border-top:1px dotted #000;"></td>';

												echo '<td class="spacer"></td>';

												echo '<td class="tableRowTotal3" style="text-align:right;border-top:1px dotted #000;">';
												echo '<b>' . number_format(($arrTotalCountItems["CONFIRMATIONS"]["PerIntervalAndCustomerType"][$thisIntervalValue][$thisCustomerTypeKey]["KZH"] + $arrTotalCountItems["PROCESS_OPEN"]["PerIntervalAndCustomerType"][$thisIntervalValue][$thisCustomerTypeKey]["KZH"]), 0, ",", ".") . '</b>';
												echo '</td>';

											echo '</tr>';
										}

										$rowClass = 'row0';
										if($countRow%2 == 0) {
											$rowClass = 'row1';
										}

										echo '<tr style="border-bottom: 2px solid #003300;" class="'.$rowClass.'">';

										echo '<td style="text-align:right;"><b>';
										echo ($countRow + 1);
										echo '.</b></td>';

										echo '<td style="white-space:nowrap;padding-left:10px;">';
										#echo $arrSalesmenDatas[$thisDatasKey]["customersFirmenname"];
										echo $arrUseAgentDatas[$thisDatasKey]["customersFirmenname"];
										echo '</td>';

										echo '<td style="white-space:nowrap;">';
										echo '<a href="' . PAGE_EDIT_CUSTOMER. '?editCustomerNumber=' . $arrUseAgentDatas[$thisDatasKey]["customersKundennummer"] . '">';
										echo $arrUseAgentDatas[$thisDatasKey]["customersKundennummer"];
										echo '</a>';
										echo '</td>';

										echo '<td class="spacer"></td>';

										#echo '<td>';
										#echo ' ' . $arrCustomerTypesDatas[$arrUseAgentDatas[$thisDatasKey]["customersTyp"]]["customerTypesShortName"];
										#echo ' ' . $arrUseAgentDatas[$thisDatasKey]["customersTyp"];
										#echo '</td>';


										$arrCountItemsIntervalKZH = array();
										$totalCountItemsIntervalKZH["CONFIRMATIONS"] = 0;
										$arrOrdersSourcesPerOrderType["CONFIRMATIONS"] = array();
										$arrOrdersSourcesPerOrderQuantity["CONFIRMATIONS"] = array();
										foreach($arrOrderTypesDatas as $thisOrderTypesKey => $thisOrderTypesValue){
											$thisOrderKey = $thisOrderTypesKey;
											$thisOrderValue = $thisDatasValue["CONFIRMATIONS"][$thisOrderKey];
											if($thisOrderValue["countItemsKZH"] == '') { $thisOrderValue["countItemsKZH"] = 0; }
											$arrCountItemsIntervalKZH["CONFIRMATIONS"][$thisOrderKey] = number_format($thisOrderValue["countItemsKZH"], 0, ",", ".") . '';
											$totalCountItemsIntervalKZH["CONFIRMATIONS"] += $thisOrderValue["countItemsKZH"];

											if($thisOrderValue["ordersSourceTypes"] != ''){
												$arrTempOrdersSourcesPerOrderTypes = explode(";", $thisOrderValue["ordersSourceTypes"]);
												foreach($arrTempOrdersSourcesPerOrderTypes as $thisTempOrdersSourcesPerOrderTypesData){
													$arrTemp = explode(":", $thisTempOrdersSourcesPerOrderTypesData);
													$thisTempOrdersSourcesPerOrderTypes = $arrTemp[0];
													$thisTempOrdersSourcesPerOrderQuantity = $arrTemp[1];
													if($arrOrdersSourcesPerOrderType["CONFIRMATIONS"][$thisOrderKey][$thisTempOrdersSourcesPerOrderTypes] == ''){
														$arrOrdersSourcesPerOrderType["CONFIRMATIONS"][$thisOrderKey][$thisTempOrdersSourcesPerOrderTypes] = 0;
													}
													if($arrOrdersSourcesPerOrderQuantity["CONFIRMATIONS"][$thisOrderKey][$thisTempOrdersSourcesPerOrderTypes] == ''){
														$arrOrdersSourcesPerOrderQuantity["CONFIRMATIONS"][$thisOrderKey][$thisTempOrdersSourcesPerOrderTypes] = 0;
													}
													$arrOrdersSourcesPerOrderType["CONFIRMATIONS"][$thisOrderKey][$thisTempOrdersSourcesPerOrderTypes]++;
													$arrOrdersSourcesPerOrderQuantity["CONFIRMATIONS"][$thisOrderKey][$thisTempOrdersSourcesPerOrderTypes] += $thisTempOrdersSourcesPerOrderQuantity;
												}
											}
										}

										echo '<td class="tableRowTotal2" style="text-align:right;width:70px;">';
											echo number_format($totalCountItemsIntervalKZH["CONFIRMATIONS"], 0, ",", ".");;
										echo '</td>';

										if(!empty($arrCountItemsIntervalKZH["CONFIRMATIONS"])) {
											$countCell = 0;
											foreach($arrCountItemsIntervalKZH["CONFIRMATIONS"] as $thisOrderTypeKey => $thisOrderTypeValue){
												$thisCellStyle = '';
												if($countCell == 1){
													$thisCellStyle = 'background-color:#FFFFCF;';
												}
												echo '<td style="text-align:right;' . $thisCellStyle . '">';
												if($thisOrderTypeValue > 0){

												}
												else {

												}

												echo '<b>' . $thisOrderTypeValue . '</b><br />';

												if(!empty($arrOrdersSourcesPerOrderType["CONFIRMATIONS"][$thisOrderTypeKey])){
													echo '<table class="detailData" border="0" cellpadding="0" cellspacing="0" align="right">';
													ksort($arrOrdersSourcesPerOrderType["CONFIRMATIONS"][$thisOrderTypeKey]);
													foreach($arrOrdersSourcesPerOrderType["CONFIRMATIONS"][$thisOrderTypeKey] as $thisSourcesPerOrderTypeKey => $thisSourcesPerOrderTypeValue){
														echo '<tr>';
														echo '<td style="font-weight:bold;white-space:nowrap;">';
														echo $arrOrdersSourceDatas[$thisSourcesPerOrderTypeKey]["ordersSourceTypesName"] . ': ';
														echo '</td>';

														echo '<td style="font-weight:normal;text-align:right;white-space:nowrap;">';
														echo $thisSourcesPerOrderTypeValue . 'x ';
														echo '</td>';

														echo '<td style="font-weight:normal;text-align:right;white-space:nowrap;">';
														echo number_format($arrOrdersSourcesPerOrderQuantity["CONFIRMATIONS"][$thisOrderTypeKey][$thisSourcesPerOrderTypeKey], 0, ",", ".");
														echo '</td>';

														echo '</tr>';
													}
													echo '</table>';
												}

												echo '</td>';

												$countCell++;
											}
										}

										echo '<td class="spacer"></td>';

										########
										$arrCountItemsIntervalKZH = array();
										$totalCountItemsIntervalKZH["PROCESS_OPEN"] = 0;
										$arrOrdersSourcesPerOrderType["PROCESS_OPEN"] = array();
										$arrOrdersSourcesPerOrderQuantity["PROCESS_OPEN"] = array();

										foreach($arrOrderTypesDatas as $thisOrderTypesKey => $thisOrderTypesValue){
											$thisOrderKey = $thisOrderTypesKey;
											$thisOrderValue = $thisDatasValue["PROCESS_OPEN"][$thisOrderKey];
											if($thisOrderValue["countItemsKZH"] == '') { $thisOrderValue["countItemsKZH"] = 0; }
											$arrCountItemsIntervalKZH[$thisOrderKey] = number_format($thisOrderValue["countItemsKZH"], 0, ",", ".") . '';
											$totalCountItemsIntervalKZH["PROCESS_OPEN"] += $thisOrderValue["countItemsKZH"];

											if($thisOrderValue["ordersSourceTypes"] != ''){
												$arrTempOrdersSourcesPerOrderTypes = explode(";", $thisOrderValue["ordersSourceTypes"]);
												foreach($arrTempOrdersSourcesPerOrderTypes as $thisTempOrdersSourcesPerOrderTypesData){
													$arrTemp = explode(":", $thisTempOrdersSourcesPerOrderTypesData);
													$thisTempOrdersSourcesPerOrderTypes = $arrTemp[0];
													$thisTempOrdersSourcesPerOrderQuantity = $arrTemp[1];

													if($arrOrdersSourcesPerOrderType["PROCESS_OPEN"][$thisOrderKey][$thisTempOrdersSourcesPerOrderTypes] == ''){
														$arrOrdersSourcesPerOrderType["PROCESS_OPEN"][$thisOrderKey][$thisTempOrdersSourcesPerOrderTypes] = 0;
													}
													if($arrOrdersSourcesPerOrderQuantity["PROCESS_OPEN"][$thisOrderKey][$thisTempOrdersSourcesPerOrderTypes] == ''){
													   $arrOrdersSourcesPerOrderQuantity["PROCESS_OPEN"][$thisOrderKey][$thisTempOrdersSourcesPerOrderTypes] = 0;
													}
													$arrOrdersSourcesPerOrderType["PROCESS_OPEN"][$thisOrderKey][$thisTempOrdersSourcesPerOrderTypes]++;
													$arrOrdersSourcesPerOrderQuantity["PROCESS_OPEN"][$thisOrderKey][$thisTempOrdersSourcesPerOrderTypes] += $thisTempOrdersSourcesPerOrderQuantity;
												}
											}
										}

										echo '<td class="tableRowTotal2" style="text-align:right;width:70px;">';
											echo number_format($totalCountItemsIntervalKZH["PROCESS_OPEN"], 0, ",", ".");;
										echo '</td>';

										if(!empty($arrCountItemsIntervalKZH)) {
											$countCell = 0;
											foreach($arrCountItemsIntervalKZH as $thisOrderTypeKey => $thisOrderTypeValue){
												$thisCellStyle = '';
												if($countCell == 1){
													$thisCellStyle = 'background-color:#FFFFCF;';
												}
												echo '<td style="text-align:right;' . $thisCellStyle . '">';
												if($thisOrderTypeValue > 0){

												}
												else {

												}

												echo '<b>' . $thisOrderTypeValue . '</b><br />';

												if(!empty($arrOrdersSourcesPerOrderType["PROCESS_OPEN"][$thisOrderTypeKey])){
													echo '<table class="detailData" border="0" cellpadding="0" cellspacing="0" align="right">';
													ksort($arrOrdersSourcesPerOrderType["PROCESS_OPEN"][$thisOrderTypeKey]);
													foreach($arrOrdersSourcesPerOrderType["PROCESS_OPEN"][$thisOrderTypeKey] as $thisSourcesPerOrderTypeKey => $thisSourcesPerOrderTypeValue){
														echo '<tr>';

														echo '<td style="font-weight:bold;white-space:nowrap;">';
														echo $arrOrdersSourceDatas[$thisSourcesPerOrderTypeKey]["ordersSourceTypesName"] . ': ';
														echo '</td>';

														echo '<td style="font-weight:normal;text-align:right;white-space:nowrap;">';
														echo $thisSourcesPerOrderTypeValue . 'x ';
														echo '</td>';

														echo '<td style="font-weight:normal;text-align:right;white-space:nowrap;">';
														echo number_format($arrOrdersSourcesPerOrderQuantity["PROCESS_OPEN"][$thisOrderTypeKey][$thisSourcesPerOrderTypeKey], 0, ",", ".");
														echo '</td>';

														echo '</tr>';
													}
													//
													echo '</table>';
												}

												echo '</td>';

												$countCell++;
											}
										}

										echo '</td>';

										echo '<td class="spacer"></td>';

										echo '<td class="tableRowTotal2" style="text-align:right;">';

										if(!empty($arrOrdersSourcesPerOrderType)){
											$arrTotalSourceDatas = array();
											$arrTotalSourceQuantity = array();
											foreach($arrOrdersSourcesPerOrderType as $thisStatusKey => $thisStatusValue){
												foreach($thisStatusValue as $thisOrderTypeKey => $thisOrderTypeValue){
													foreach($thisOrderTypeValue as $thisSourceTypeKey => $thisSourceTypeValue){
														if($thisSourceTypeValue == '') {
															$thisSourceTypeValue = 0;
														}
														$thisSourceTypeQuantity = $arrOrdersSourcesPerOrderQuantity[$thisStatusKey][$thisOrderTypeKey][$thisSourceTypeKey];
														if($thisSourceTypeQuantity == '') {
															$thisSourceTypeQuantity = 0;
														}
														$arrTotalSourceDatas[$thisSourceTypeKey] += $thisSourceTypeValue;
														$arrTotalSourceQuantity[$thisSourceTypeKey] += $thisSourceTypeQuantity;
													}
												}
											}

											echo number_format(($totalCountItemsIntervalKZH["CONFIRMATIONS"] + $totalCountItemsIntervalKZH["PROCESS_OPEN"]), 0, ",", ".");

											if(in_array($arrUseAgentDatas[$thisDatasKey]["customersTyp"], array(4, 9, 10)) && !in_array($checkSourceTypesID, array_keys($arrTotalSourceDatas))){
												echo '<span class="detailData" style="color:#FF0000;padding:0 4px 0 4px;">';
												echo '<img src="layout/icons/iconAttention.png" style="" class="blink" width="16" height="16" alt="Keine Vertreter-Bestellungen" title="Keine Vertreter-Bestellungen" />';
												echo '</span> ';
											}

											echo '<br />';

											echo '<table class="detailData" border="0" cellpadding="0" cellspacing="0" align="right">';
											foreach($arrTotalSourceDatas as $thisSourceTypeKey => $thisSourceTypeValue){
												echo '<tr>';

												echo '<td style="font-weight:bold;white-space:nowrap;">';
												echo $arrOrdersSourceDatas[$thisSourceTypeKey]["ordersSourceTypesName"] . ': ';
												echo '</td>';

												echo '<td style="font-weight:normal;text-align:right;white-space:nowrap;">';
												echo $thisSourceTypeValue . 'x ';
												echo '</td>';

												echo '<td style="font-weight:normal;text-align:right;white-space:nowrap;">';
												echo number_format($arrTotalSourceQuantity[$thisSourceTypeKey], 0, ",", ".");
												echo '</td>';
												echo '</tr>';
											}
											echo '</table>';
										}

										echo '</td>';

										echo '</tr>';
										$countRow++;
									}
								}
							}
						}
						else {
							$infoMessage .= ' Es liegen keine Daten vor!' . '<br />';
						}
						displayMessages();
					?>
					<?php displayMessages(); ?>
				</div>

			</div>
		</div>
		<div class="clear"></div>
	</div>
</div>

<script language="javascript" type="text/javascript">
	$(document).ready(function() {
		$(function() {
			colorRowMouseOver('.displayOrders tbody tr');
			$('.buttonShopOrderInfo').css('cursor', 'pointer');
			$('.buttonShopOrderInfo').click(function () {
				loadOrderDetails($(this), '<?php echo BASEPATH; ?>', 'Details der Bestellung');
			});
		});

		$('.buttonToggleSidebarMenue').click(function() {
			$('#menueSidebarToggleContent').toggle();
		});

		$('.blink').parent().parent().parent('tr').css('background-color', '#FFBFC8 !important');
		$('.blink').parent().parent().parent().find('td').css('background-color', '#FFBFC8 !important');

		$(function() {
			$('#tabs').tabs();
		});
	});
</script>
<?php require_once('inc/footerHTML.inc.php'); ?>