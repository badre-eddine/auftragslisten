<?php
	require_once('inc/requires.inc.php');

	$arrQueryVars = getUrlQueryString($_SERVER["REQUEST_URI"]);
	foreach($arrQueryVars as $thisKey => $thisValue) {
		$_REQUEST[$thisKey] = $thisValue;
	}

	if($_REQUEST["configType"] == '') {
		header('location: ' . PAGE_EXIT_LOCATION);
		exit;
	}
	else {
		if($_REQUEST["configType"] == 'BasicDatas') {
			if(!$arrGetUserRights["displayBasicDatas"] && !$arrGetUserRights["editBasicDatas"]) {
				header('location: ' . PAGE_EXIT_LOCATION);
				exit;
			}
			$thisTitle = "Grund-Einstellungen konfigurieren";
			$thisIcon = 'config.png';
			$sqlWhere = " `configDatasCategorie` = 'BASIC_CONFIG' OR `configDatasCategorie` = 'ADMIN_CONFIG' ";
		}
		else if($_REQUEST["configType"] == 'FtpDatas') {
			if(!$arrGetUserRights["displayFtpDatas"] && !$arrGetUserRights["editFtpDatas"]) {
				header('location: ' . PAGE_EXIT_LOCATION);
				exit;
			}
			$thisTitle = "FTP-Konten konfigurieren";
			$thisIcon = 'ftp.png';
			$sqlWhere = " `configDatasCategorie` = 'FTP_CONFIG' ";
		}
		else if($_REQUEST["configType"] == 'MailDatas') {
			if(!$arrGetUserRights["displayMailDatas"] && !$arrGetUserRights["editMailDatas"]) {
				header('location: ' . PAGE_EXIT_LOCATION);
				exit;
			}
			$thisTitle = "Mail-Konten konfigurieren";
			$thisIcon = 'mail.png';
			$sqlWhere = " `configDatasCategorie` = 'MAIL_CONFIG' ";
		}
		else if($_REQUEST["configType"] == 'DbDatas') {
			if(!$arrGetUserRights["displayDbDatas"] && !$arrGetUserRights["editDbDatas"]) {
				header('location: ' . PAGE_EXIT_LOCATION);
				exit;
			}
			$thisTitle = "Datenbanken konfigurieren";
			$thisIcon = 'database.png';
			$sqlWhere = " `configDatasCategorie` = 'DB_CONFIG' ";
		}
		else if($_REQUEST["configType"] == 'CompanyDatas') {
			if(!$arrGetUserRights["displayCompanyDatas"] && !$arrGetUserRights["editCompanyDatas"]) {
				header('location: ' . PAGE_EXIT_LOCATION);
				exit;
			}
			$thisTitle = "Firmen-Daten konfigurieren";#
			$thisIcon = 'company.png';
			$sqlWhere = " `configDatasCategorie` = 'COMPANY_DATAS' ";
		}
		else if($_REQUEST["configType"] == 'ShippingDatas') {
			if(!$arrGetUserRights["displayShippingDatas"] && !$arrGetUserRights["editShippingDatas"]) {
				header('location: ' . PAGE_EXIT_LOCATION);
				exit;
			}
			$thisTitle = "Versand-Einstellungen konfigurieren";
			$thisIcon = 'shipping.png';
			$sqlWhere = " `configDatasCategorie` = 'CONFIG_ORDERS' ";
		}
		else if($_REQUEST["configType"] == 'BankDatas') {
			if(!$arrGetUserRights["displayBankDatas"] && !$arrGetUserRights["editBankDatas"]) {
				header('location: ' . PAGE_EXIT_LOCATION);
				exit;
			}
			$thisTitle = "Bank-Daten konfigurieren";
			$thisIcon = 'bank.png';
			$sqlWhere = " `configDatasCategorie` = 'BANK_DATAS' ";
		}
	}

	$jswindowMessage = "";
	$warningMessage = '';
	$errorMessage = '';
	$successMessage = '';
	$infoMessage = '';

	$dbConnection = new DB_Connection();
	$db_open = $dbConnection->db_connect();

	// BOF READ PERSONNEL DATAS
		$arrPersonnelDatas = getPersonnelDatas();
	// EOF READ PERSONNEL DATAS

	// BOF STORE DATAS
		if($_POST["storeDatas"] != "") {

			$doAction = checkFormDutyFields($arrFormDutyFields);

			if($doAction) {
				$sql = "";
				if(!empty($_POST["arrEditConfigDatas"])) {
					foreach($_POST["arrEditConfigDatas"] as $thisConfigCategoryKey => $thisConfigCategoryValue){
						if(!empty($thisConfigCategoryValue)) {
							foreach($thisConfigCategoryValue as $thisConfigGroupKey => $thisConfigGroupValue){
								foreach($thisConfigGroupValue as $thisConfigDataKey => $thisConfigDataValue){
									$sql = " UPDATE `" . TABLE_CONFIG_DATAS . "`
													SET
														`configDatasValue` = '" .addslashes($thisConfigDataValue) . "'
													WHERE `configDatasParam` = '" . $thisConfigDataKey . "';";
									$rs = $dbConnection->db_query($sql);
								}
							}
						}
					}
				}
				if($rs) {
					$successMessage .= ' Der Datensatz wurde gespeichert.' .'<br />';
					if($_REQUEST["configType"] == 'CompanyDatas') {
						writeVcard();
					}
				}
				else {
					$errorMessage .= ' Der Datensatz konnte nicht gespeichert werden.' .'<br />';
				}
				writeConfigFile();
			}
			else {
				$errorMessage .= ' Der Datensatz konnte nicht gespeichert werden. Bitte füllen Sie alle Pflichtfelder aus.' .'<br />';
			}
		}
	// EOF STORE DATAS

	// BOF READ ALL DATAS
		$sql = "SELECT

					`configDatasID`,
					`configDatasCategorie`,
					`configDatasGroupID`,
					`configDatasParam`,
					`configDatasName`,
					`configDatasDescription`,
					`configDatasValue`,
					`configDatasSetValueFieldType`,
					`configDatasSetValueFieldValues`

				FROM `" . TABLE_CONFIG_DATAS . "`

				WHERE 1 AND " . $sqlWhere . "

				ORDER BY `configDatasCategorie`, `configDatasGroupID`
		";
		$rs = $dbConnection->db_query($sql);

		while($ds = mysqli_fetch_assoc($rs)) {
			foreach(array_keys($ds) as $field) {
				$arrConfigDatas[$ds["configDatasCategorie"]][$ds["configDatasGroupID"]][$ds["configDatasID"]][$field] = $ds[$field];
			}
		}
	// EOF READ ALL DATAS
?>
<?php
	require_once('inc/headerHTML.inc.php');

	// $thisTitle = "Grund-Einstellungen konfigurieren";

	$headerHTML = preg_replace("/{###TITLE###}/", strip_tags($thisTitle), $headerHTML);
	echo $headerHTML;

	require_once(FILE_MENUE_TOP);
?>
<div id="mainArea">
	<div id="mainContent">
		<?php require_once(FILE_MENUE_SIDEBAR); ?>
		<div id="contentArea">
			<div id="contentAreaElements">
				<h1><?php if(SHOW_PAGE_TITLE_ICONS) { echo '<img src="' . PATH_ICONS_MENUE_TITLES . $thisIcon . '" alt="" />'; } ?> <?php echo $thisTitle; ?></h1>

				<div class="contentDisplay">

					<?php displayMessages(); ?>

					<div class="adminEditArea">

					<form name="editConfigDatas" method="post" action="<?php echo $_SERVER["PHP_SELF"]; ?>">
					<input type="hidden" name="configType" value="<?php echo $_REQUEST["configType"]; ?>" />

					<!--
					<p class="warningArea">Pflichtfelder m&uuml;ssen ausgef&uuml;llt werden!</p>
					-->

					<?php
						if(!empty($arrConfigDatas)) {

							foreach($arrConfigDatas as $thisConfigCategoryKey => $thisConfigCategoryValue){
								if(!empty($thisConfigCategoryValue)) {
									foreach($thisConfigCategoryValue as $thisConfigGroupKey => $thisConfigGroupValue){
										echo '<fieldset>';
										echo '<legend>' . getParamTitels($thisConfigCategoryKey, 'categorie') . getParamTitels($thisConfigGroupKey, 'group') . '</legend>';
										echo '<table border="0" width="100%" cellspacing="0" cellpadding="0">';
										foreach($thisConfigGroupValue as $thisConfigDataKey => $thisConfigDataValue){
											echo '<tr>'	;
											echo '<td width="200"><b>' . $thisConfigDataValue["configDatasName"] . ':</b></td>';
											echo '<td>';
											echo createConfigInputField($thisConfigDataValue, $thisConfigCategoryKey, $thisConfigGroupKey, 'editConfigDatas', 'arrEditConfigDatas');
											echo '</td>';
											echo '</tr>';
										}
										echo '</table>';
										echo '</fieldset>';
									}
								}
							}

							if($arrGetUserRights["edit" . $_REQUEST["configType"]]) {
						?>
								<div class="actionButtonsArea">
									<input type="submit" class="inputButton1" name="storeDatas" value="Speichern" onclick="return showWarning(' Wollen Sie diesen Vorgang wirklich speichern??? '); "/>
									&nbsp;
									<input type="submit" class="inputButton1" name="resetDatas" value="Abbrechen" onclick="return showWarning(' Wollen Sie diesen Vorgang wirklich abbrechen??? '); "/>
								</div>
						<?php
							}
						}
					?>
					</form>
					</div>
				</div>

				<?php if($_REQUEST["configType"] == 'MailDatas'){ ?>
				<div class="adminEditArea">
					<fieldset>
					<legend>Mailkonten</legend>

					<?php
						// BOF STORE MAIL ACCOUNT
							if($_POST["storeMailAccount"] != ""){
								if($_POST["mailAccountMailPrefix"] != "" && $_POST["mailAccountMailSuffix"] != "" && $_POST["mailAccountMailboxName"] != ""){

									if($_POST["mailAccountMailboxForwardToMailbox"] != ""){
										$_POST["mailAccountMailboxName"] = 'ex ' . $_POST["mailAccountMailboxName"];
									}

									$sql = "
										REPLACE INTO `" . TABLE_PERSONNEL_MAIL_ACCOUNTS . "`(
																`personnelMailAccountsID`,
																`personnelMailAccountsPersonnelID`,
																`personnelMailAccountsMailPrefix`,
																`personnelMailAccountsMailSuffix`,
																`personnelMailAccountsMailboxName`,
																`personnelMailAccountsMailboxLogin`,
																`personnelMailAccountsMailboxPassword`,
																`personnelMailAccountsMailAdressActive`,
																`personnelMailAccountsMailboxForwardToMailbox`,
																`personnelMailAccountsSort`
															)
															VALUES (
																'" . $_POST["mailAccountMailAccountsID"] . "',
																'" . implode(";", $_POST["mailAccountMailAccountsPersonnelID"]) . "',
																'" . $_POST["mailAccountMailPrefix"] . "',
																'" . $_POST["mailAccountMailSuffix"] . "',
																'" . $_POST["mailAccountMailboxName"] . "',
																'" . $_POST["mailAccountMailboxLogin"] . "',
																'" . $_POST["mailAccountMailboxPassword"] . "',
																'" . $_POST["mailAccountMailAdressActive"] . "',
																'" . $_POST["mailAccountMailboxForwardToMailbox"] . "',
																'" . $_POST["mailAccountMailAccountsSort"] . "'
															)
										";

									$rs = $dbConnection->db_query($sql);
									if($rs){
										$successMessage .= 'Die Mail-Account-Daten wurden gespeichert.' . '<br />';
										$_REQUEST["editMailAccountID"] = '';

									}
									else {
										$errorMessage .= 'Die Mail-Account-Daten konnten nicht gespeichert werden.' . '<br />';
									}
								}
								else {
									$errorMessage .= 'Sie haben nicht die notwendigen Daten eingetragen.' . '<br />';
								}
							}
						// EOF STORE MAIL ACCOUNT

						// BOF DELETE MAIL ACCOUNT
							if($_POST["deleteMailAccount"] != ""){
								$sql = "DELETE FROM `" . TABLE_PERSONNEL_MAIL_ACCOUNTS . "`
											WHERE 1
												AND `personnelMailAccountsID` = '" . $_REQUEST["editMailAccountID"] . "'
									";
								$rs = $dbConnection->db_query($sql);

								if($rs){
									$successMessage .= 'Die Mail-Account-Daten wurden entfernt.' . '<br />';
									$_REQUEST["editMailAccountID"] = '';

								}
								else {
									$errorMessage .= 'Die Mail-Account-Daten konnten nicht entfernt werden.' . '<br />';
								}
							}
						// EOF DELETE MAIL ACCOUNT

						// BOF GET MAIL ACCOUNTS
						$sql = "SELECT
									`personnelMailAccounts`.`personnelMailAccountsID`,
									`personnelMailAccounts`.`personnelMailAccountsPersonnelID`,
									`personnelMailAccounts`.`personnelMailAccountsMailPrefix`,
									`personnelMailAccounts`.`personnelMailAccountsMailSuffix`,
									`personnelMailAccounts`.`personnelMailAccountsMailboxName`,
									`personnelMailAccounts`.`personnelMailAccountsMailboxLogin`,
									`personnelMailAccounts`.`personnelMailAccountsMailboxPassword`,
									`personnelMailAccounts`.`personnelMailAccountsMailAdressActive`,
									`personnelMailAccounts`.`personnelMailAccountsMailboxForwardToMailbox`,
									`personnelMailAccounts`.`personnelMailAccountsSort`,

									`forwardMailAccounts`.`personnelMailAccountsID` AS `forwardMailAccountsID`,
									`forwardMailAccounts`.`personnelMailAccountsPersonnelID` AS `forwardMailAccountsPersonnelID`,
									`forwardMailAccounts`.`personnelMailAccountsMailPrefix` AS `forwardMailAccountsMailPrefix`,
									`forwardMailAccounts`.`personnelMailAccountsMailSuffix` AS `forwardMailAccountsMailSuffix`,
									`forwardMailAccounts`.`personnelMailAccountsMailboxName` AS `forwardMailAccountsMailboxName`,
									`forwardMailAccounts`.`personnelMailAccountsMailboxLogin` AS `forwardMailAccountsMailboxLogin`,
									`forwardMailAccounts`.`personnelMailAccountsMailboxPassword` AS `forwardMailAccountsMailboxPassword`,
									`forwardMailAccounts`.`personnelMailAccountsMailAdressActive` AS `forwardMailAccountsMailAdressActive`,
									`forwardMailAccounts`.`personnelMailAccountsMailboxForwardToMailbox` AS `forwardMailAccountsMailboxForwardToMailbox`,
									`forwardMailAccounts`.`personnelMailAccountsSort` AS `forwardMailAccountsSort`

								FROM `" . TABLE_PERSONNEL_MAIL_ACCOUNTS . "` AS `personnelMailAccounts`
								LEFT JOIN `" . TABLE_PERSONNEL_MAIL_ACCOUNTS . "` AS `forwardMailAccounts`
								ON(`personnelMailAccounts`.`personnelMailAccountsMailboxForwardToMailbox` = `forwardMailAccounts`.`personnelMailAccountsMailboxName`)

								WHERE 1
									ORDER BY
										`personnelMailAccounts`.`personnelMailAccountsMailSuffix` DESC,
										`personnelMailAccounts`.`personnelMailAccountsMailAdressActive` DESC,
										`personnelMailAccounts`.`personnelMailAccountsMailboxForwardToMailbox` ASC,
										LENGTH(`personnelMailAccounts`.`personnelMailAccountsMailboxLogin`),
										`personnelMailAccounts`.`personnelMailAccountsMailboxLogin` ASC,
										`personnelMailAccounts`.`personnelMailAccountsMailPrefix`
							";
						$rs = $dbConnection->db_query($sql);

						displayMessages();

						if($arrGetUserRights["editMailDatas"]){
							echo '<div class="actionButtonsArea"><p><a href="' . $_SERVER["REDIRECT_URL"] . '?editMailAccountID=NEW" class="linkButton">Neuen Mail-Account eintragen</a><br /></p></div>';
						}

						echo '<table border="0" style="width:800px;" cellspacing="0" cellpadding="0" class="displayOrders">';
						
						echo '<tr>';
						echo '<th style="width:45px;">#</th>';
						echo '<th colspan="2">MAILADRESSE</th>';
						echo '<th>MAILBOX</th>';
						echo '<th>LOGIN</th>';
						echo '<th>PASSWORT</th>';
						echo '<th>BENUTZER</th>';
						echo '</tr>';						

						$countRow = 0;
						$marker = "";

						$arrMailboxesInUse = array();
						$arrMailboxesNotInUse = array();
						$arrMailboxesExistent = array();
						$arrSelectedMailAccount = array();
						while($ds = mysqli_fetch_assoc($rs)){
							if($_REQUEST["editMailAccountID"] != "" && $_REQUEST["editMailAccountID"] == $ds["personnelMailAccountsID"]){
								foreach(array_keys($ds) as $field) {
									$arrSelectedMailAccount[$field] = $ds[$field];
								}
							}
							if(!in_array($ds["personnelMailAccountsMailboxName"], $arrMailboxesInUse) && $ds["personnelMailAccountsMailboxForwardToMailbox"] == ""){
								$arrMailboxesInUse[] = $ds["personnelMailAccountsMailboxName"];
							}
							if(!in_array($ds["personnelMailAccountsMailboxName"], $arrMailboxesNotInUse) && $ds["personnelMailAccountsMailboxForwardToMailbox"] != ""){
								$arrMailboxesNotInUse[] = $ds["personnelMailAccountsMailboxName"];
							}
							$arrMailboxesExistent[] = str_replace("ex ", "", $ds["personnelMailAccountsMailboxName"]);
							#if($countRow == 0) {dd('ds');}

							if($marker != $ds["personnelMailAccountsMailSuffix"]){
								echo '<tr style="background-color:#AFFFC9">';
								echo '<td colspan="7"><b>' . strtoupper($ds["personnelMailAccountsMailSuffix"]) . '</b></td>';
								echo '</tr>';
								$marker = $ds["personnelMailAccountsMailSuffix"];
							}

							if($countRow%2 == 0){ $rowClass = 'row0'; }
							else { $rowClass = 'row1'; }

							$rowStyle = '';
							if($ds["personnelMailAccountsMailboxForwardToMailbox"] != ""){
								$rowStyle = ' style="color:#990000;font-style:italic;" ';
							}

							echo '<tr class="' . $rowClass . '" ' . $rowStyle . '>';
							echo '<td style="text-align:right;">';
							echo '<b>' . ($countRow + 1) . '.</b>';
							echo '</td>';

							echo '<td style="font-size:11px;">';
							echo $ds["personnelMailAccountsMailPrefix"];
							echo '</td>';

							echo '<td style="position:relative">';
							echo '<span style="font-size:10px;">' . $ds["personnelMailAccountsMailSuffix"] . '</span>';
							if($arrGetUserRights["adminArea"]){
								echo '<span style="position:absolute;top:0;right:2px;">';
								echo '<a href="' . $_SERVER["REDIRECT_URL"] . '?editMailAccountID=' . $ds["personnelMailAccountsID"]. '#editFormArea">';
								echo '<img src="layout/icons/iconEdit.gif" width="14" height="14" alt="" title="Mailkonto bearbeiten" />';
								echo '</a>';
								echo '</span>';
							}
							echo '</td>';

							echo '<td style="background-color:#FEFFAF;white-space:nowrap">';
							echo $ds["personnelMailAccountsMailboxName"];
							echo '</td>';


							if($ds["personnelMailAccountsMailboxForwardToMailbox"] != ""){
								echo '<td style="background-color:#D1E0EF;color:#990000;font-size:11px;white-space:nowrap" colspan="2">';
								echo 'Weiterleitung an ' . $ds["forwardMailAccountsMailPrefix"] . $ds["forwardMailAccountsMailSuffix"] . ' [' . $ds["personnelMailAccountsMailboxForwardToMailbox"] . ']';
								echo '</td>';
							}
							else {
								echo '<td style="background-color:#D1E0EF">';
								echo $ds["personnelMailAccountsMailboxLogin"];
								echo '</td>';

								echo '<td style="background-color:#D1E0EF">';
								echo $ds["personnelMailAccountsMailboxPassword"];
								echo '</td>';
							}

							echo '<td>';
								if($ds["personnelMailAccountsPersonnelID"] != ''){
									$arrPersonnelMailAccountsPersonnelID = explode(';', $ds["personnelMailAccountsPersonnelID"]);
									if(!empty($arrPersonnelMailAccountsPersonnelID)){
										foreach($arrPersonnelMailAccountsPersonnelID as $thisPersonnelMailAccountsPersonnelID){
											echo '<span style="font-size:8px;white-space:nowrap;">&bull; ' . $arrPersonnelDatas[$thisPersonnelMailAccountsPersonnelID]["personnelFirstName"] . ' ' . $arrPersonnelDatas[$thisPersonnelMailAccountsPersonnelID]["personnelLastName"] . '</span><br />';
										}
									}
								}
							echo '</td>';

							echo '</tr>';

							$countRow++;
						}
					?>
						<tr>
							<td></td>
							<td></td>
							<td></td>
							<td style="background-color:#FEFFAF"></td>
							<td style="background-color:#D1E0EF"></td>
							<td style="background-color:#D1E0EF"></td>
							<!--
							<td></td>
							<td style="background-color:#FEFFAF"></td>
							<td style="background-color:#FEFFAF"></td>
							-->
							<td></td>
						</tr>
						<tr style="background-color:#AFFFC9">
							<td colspan="7"><b>Freie Postf&auml;cher</b></td>
						</tr>
						<tr>
							<td></td>
							<td></td>
							<td></td>
							<td style="text-align:right;background-color:#FEFFAF">
								<?php
									sort($arrMailboxesNotInUse);
									$arrMailboxesExistent = array_unique($arrMailboxesExistent);
									sort($arrMailboxesInUse);
									sort($arrMailboxesExistent);
									#dd('arrMailboxesNotInUse');
									#dd('arrMailboxesInUse');
									#dd('arrMailboxesExistent');

									$arrMailboxesFree = array_diff($arrMailboxesExistent, $arrMailboxesInUse);
									#dd('arrMailboxesFree');
									if(!empty($arrMailboxesFree)){
										echo implode("<br />", $arrMailboxesFree);
									}
								?>
							</td>
							<td style="background-color:#D1E0EF"></td>
							<td style="background-color:#D1E0EF"></td>
							<!--
							<td></td>
							<td style="background-color:#FEFFAF"></td>
							<td style="background-color:#FEFFAF"></td>
							-->
							<td></td>
						</tr>
						<tr style="background-color:#AFFFC9">
							<td colspan="7"><b>Mailkonten-Konfiguration</b></td>
						</tr>
						<tr>
							<td></td>
							<td><b>SMTP POSTAUSGANG-SERVER</b></td>
							<td></td>
							<td colspan="3" style="background-color:#D1E0EF"><?php echo MAIL_ACCOUNT_SMTP; ?></td>
							<!--
							<td></td>
							<td colspan="2" style="background-color:#FEFFAF"></td>
							-->
							<td></td>
						</tr>
						<tr>
							<td></td>
							<td><b>SMTP POSTAUSGANG-PORT</b></td>
							<td></td>
							<td colspan="3" style="background-color:#D1E0EF"><?php echo MAIL_ACCOUNT_PORT; ?></td>
							<!--
							<td></td>
							<td colspan="2" style="background-color:#FEFFAF"></td>
							-->
							<td></td>
						</tr>
						<tr>
							<td></td>
							<td><b>POP3 POSTEINGANG-SERVER</b></td>
							<td></td>
							<td colspan="3" style="background-color:#D1E0EF"><?php echo MAIL_ACCOUNT_SMTP; ?></td>
							<!--
							<td></td>
							<td colspan="2" style="background-color:#FEFFAF">192.168.2.220</td>
							-->
							<td></td>
						</tr>
						<tr>
							<td></td>
							<td><b>POP3 POSTEINGANG-PORT</b></td>
							<td></td>
							<td colspan="3" style="background-color:#D1E0EF">110</td>
							<!--
							<td></td>
							<td colspan="2" style="background-color:#FEFFAF"></td>
							-->
							<td></td>
						</tr>
						<tr>
							<td></td>
							<td><b>BURHAN-WEBMAIL</b></td>
							<td></td>
							<td colspan="3" style="background-color:#D1E0EF"><a href="<?php echo PAGE_MAIL_ACCOUNT_HOSTER_4; ?>" target="_blank"><?php echo PAGE_MAIL_ACCOUNT_HOSTER_4; ?></a></td>
							<!--
							<td></td>
							<td colspan="2" style="background-color:#FEFFAF">https://192.168.2.58/</td>
							-->
							<td></td>
						</tr>
						<tr>
							<td></td>
							<td><b>ALFAHOSTING-WEBMAIL</b></td>
							<td></td>
							<td colspan="3" style="background-color:#D1E0EF"><a href="<?php echo PAGE_MAIL_ACCOUNT_HOSTER_1; ?>" target="_blank"><?php echo PAGE_MAIL_ACCOUNT_HOSTER_1; ?></a></td>
							<!--
							<td></td>
							<td colspan="2" style="background-color:#FEFFAF">https://192.168.2.58/</td>
							-->
							<td></td>
						</tr>
					</table>

					<?php if($_REQUEST["editMailAccountID"] != ""){ ?>
					<hr />
					<a name="editFormArea"></a>
					<form name="formeditMailAccounts" method="post" action="">

						<?php
							if($arrSelectedMailAccount["personnelMailAccountsID"] == ''){
								$arrSelectedMailAccount["personnelMailAccountsID"] = '%';
							}
						?>
						<?php
							if($arrSelectedMailAccount["personnelMailAccountsPersonnelID"] == ''){
								$arrSelectedMailAccount["personnelMailAccountsPersonnelID"] = '0';
							}
						?>

						<input type="hidden" name="mailAccountMailAccountsID" value="<?php echo $arrSelectedMailAccount["personnelMailAccountsID"]; ?>">
						<!--<input type="hidden" name="mailAccountMailAccountsPersonnelID" value="<?php echo $arrSelectedMailAccount["personnelMailAccountsPersonnelID"]; ?>">-->
						<input type="hidden" name="mailAccountMailAccountsSort" value="<?php echo $arrSelectedMailAccount["personnelMailAccountsSort"]; ?>">

						<table cellpadding="0" cellspacing="0" class="displayOrders">
							<tr>
								<th>Mail-Prefix</th>
								<th>Mail-Suffix</th>
								<th>Mailbox</th>
								<th>Login</th>
								<th>Passwort</th>
								<th>Mail aktiv?</th>
								<th>Weiterleitung an Mailbox</th>
								<th>Mitarbeiter</th>
							</tr>
							<tr>
								<td>
									<input type="text" name="mailAccountMailPrefix" class="inputField_120" value="<?php echo $arrSelectedMailAccount["personnelMailAccountsMailPrefix"]; ?>" />
								</td>
								<td>
									<select name="mailAccountMailSuffix" class="inputField_120">
									<?php
										$arrMandatoryDatas = getMandatories();
										if(!empty($arrMandatoryDatas)){
											foreach($arrMandatoryDatas as $thisMandatoryData){
												$thisDomain = "@" . str_replace("www.", "", parse_url(constant("PATH_ONLINE_SHOP_" . strtoupper($thisMandatoryData["mandatoriesShortName"])), PHP_URL_HOST));
												$selected = '';
												if($thisDomain == $arrSelectedMailAccount["personnelMailAccountsMailSuffix"]){
													$selected = ' selected="selected" ';
												}

												echo '<option value="' . $thisDomain . '"' . $selected . ' >' . $thisDomain . '</option>';
											}
										}
									?>
									</select>
								</td>
								<td>
									<?php if($_REQUEST["editMailAccountID"] == "NEW"){ ?>
									<input type="text" name="mailAccountMailboxName" class="inputField_120" value="<?php echo $arrSelectedMailAccount["personnelMailAccountsMailboxName"]; ?>" />
									<?php } else { ?>
									<select name="mailAccountMailboxName" class="inputField_120">
										<option value=""></option>
										<?php
											if(!empty($arrMailboxesExistent)){
												foreach($arrMailboxesExistent as $thisMailboxesInUse){
													if($arrSelectedMailAccount["personnelMailAccountsMailboxForwardToMailbox"] != ""){
														##$thisMailboxesInUse = 'ex ' .  $thisMailboxesInUse;
													}
													$selected = '';
													if($thisMailboxesInUse == $arrSelectedMailAccount["personnelMailAccountsMailboxName"]){
														$selected = ' selected="selected" ';
													}
													echo '<option value="' . $thisMailboxesInUse . '" ' . $selected . '>' . $thisMailboxesInUse . '</option>';
												}
											}
										?>
									</select>
									<?php } ?>
								</td>
								<td>
									<input type="text" name="mailAccountMailboxLogin" class="inputField_120" value="<?php echo $arrSelectedMailAccount["personnelMailAccountsMailboxLogin"]; ?>" />
								</td>
								<td>
									<input type="text" name="mailAccountMailboxPassword" class="inputField_120" value="<?php echo $arrSelectedMailAccount["personnelMailAccountsMailboxPassword"]; ?>" />
								</td>
								<td>
									<?php
										$checked = '';
										if($arrSelectedMailAccount["personnelMailAccountsMailAdressActive"] == '1'){
											$checked = ' checked="checked" ';
										}
									?>
									<input type="checkbox" name="mailAccountMailAdressActive" class="inputField_70" <?php echo $checked; ?> value="1" />
								</td>
								<td>
									<select name="mailAccountMailboxForwardToMailbox" class="inputField_120">
										<option value=""></option>
										<?php
											if(!empty($arrMailboxesInUse)){
												foreach($arrMailboxesInUse as $thisMailboxesInUse){
													$selected = '';
													if($thisMailboxesInUse == $arrSelectedMailAccount["personnelMailAccountsMailboxForwardToMailbox"]){
														$selected = ' selected="selected" ';
													}
													echo '<option value="' . $thisMailboxesInUse . '" ' . $selected . '>' . $thisMailboxesInUse . '</option>';
												}
											}
										?>
									</select>
								</td>
								<td>
									<select name="mailAccountMailAccountsPersonnelID[]" id="mailAccountMailAccountsPersonnelID" class="inputMultiple_120" multiple="multiple" size="1" />
										<option value="0">kein Mitarbeiter</option>
										<?php
											if(!empty($arrPersonnelDatas)){
												foreach($arrPersonnelDatas as $thisPersonnelDataKey => $thisPersonnelDataValue){
													if($thisPersonnelDataValue["personnelActive"] == '1'){
														$selected = '';
														if(in_array($thisPersonnelDataKey, explode(";", $arrSelectedMailAccount["personnelMailAccountsPersonnelID"]))){
															$selected = ' selected="selected" ';
														}
														echo '<option value="' . $thisPersonnelDataKey . '" ' . $selected . '>' . $thisPersonnelDataValue["personnelFirstName"] . ' ' . $thisPersonnelDataValue["personnelLastName"] . '</option>';
													}
												}
											}
										?>
									</select>
								</td>
							</tr>
						<table>
						<div class="actionButtonsArea">
							<input type="submit" class="inputButton1 inputButtonOrange" name="cancelMailAccount" value="Abbrechen" onclick="return showWarning(' Wollen Sie diesen Vorgang wirklich abbrechen??? '); "/>
							<?php if($_REQUEST["editMailAccountID"] != "NEW") { ?>
							<input type="submit" class="inputButton1 inputButtonRed" name="deleteMailAccount" value="Entfernen" onclick="return showWarning(' Wollen Sie diesen Eintrag wirklich entfernen??? '); "/>
							<?php } ?>
							<input type="submit" class="inputButton1 inputButtonGreen" name="storeMailAccount" value="Speichern" />
						</div>
					</form>
					<?php } ?>

				</fieldset>
				</div>
			<?php } ?>
			<?php displayMessages(); ?>
			</div>
		</div>
		<div class="clear"></div>
	</div>
</div>

<script language="javascript" type="text/javascript">
	$(document).ready(function() {
		$(function() {
			$( "#datepicker" ).datepicker();
		});
		// setFormDutyFields(<?php echo $jsonFormDutyFields; ?>);
		colorRowMouseOver('.displayOrders tbody tr');
		$('#mailAccountMailAccountsPersonnelID').live('mouseenter', function (){
			$(this).attr('size', 5);
		});
	});
</script>

<?php require_once('inc/footerHTML.inc.php'); ?>