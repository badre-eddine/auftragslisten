<?php
	require_once('inc/requires.inc.php');

	if(!$arrGetUserRights["updateAcquisitionCustomers"]) {
		header('location: ' . PAGE_EXIT_LOCATION);
		exit;
	}

	$thisDebug = false;

	$jswindowMessage = "";
	$warningMessage = '';
	$errorMessage = '';
	$successMessage = '';
	$infoMessage = '';

	$dbConnection = new DB_Connection();
	$db_open = $dbConnection->db_connect();

	if(DB_HOST_EXTERN_ACQUISITION != '' && DB_NAME_EXTERN_ACQUISITION != '' && DB_USER_EXTERN_ACQUISITION != '' && DB_PASSWORD_EXTERN_ACQUISITION) {
		$existsOnlineAquisition = true;
	}
	else {
		$existsOnlineAquisition = false;
		$errorMessage .= 'Es sind keine Zugansdaten f&uuml;r die Datenbankverbindung zur ' . strtoupper(MANDATOR) . '-Kundenerfassung eingetragen. ' . '<br />';
	}

	$thisUSE_DB = DB_NAME_EXTERN_ACQUISITION;
	if($thisDebug){
		$thisUSE_DB = "usr_web23_7";
		$warningMessage .= 'ACHTUNG!!!! ES WIRD DIE TEST DB ' . $thisUSE_DB . ' verwendet!!!!!!' . '<br />';
	}

	$dbConnection_ExternAcqisition = new DB_Connection(DB_HOST_EXTERN_ACQUISITION, '', DB_NAME_EXTERN_ACQUISITION, DB_USER_EXTERN_ACQUISITION, DB_PASSWORD_EXTERN_ACQUISITION);
	$db_openExternAcqisition = $dbConnection_ExternAcqisition->db_connect();

	// BOF BACK UP
		$thisTempTableName = "_SICH_" . TABLE_ACQUISATION_CUSTOMERS . "_" . date("Y-m-d_H-i-s");
		$sql_extern = "CREATE TABLE `" . $thisTempTableName . "`  LIKE `" . TABLE_ACQUISATION_CUSTOMERS . "` ";
		$rs_extern = $dbConnection_ExternAcqisition->db_query($sql_extern);

		$sql_extern = "INSERT INTO `" . $thisTempTableName . "` SELECT * FROM `" . TABLE_ACQUISATION_CUSTOMERS . "`";
		$rs_extern = $dbConnection_ExternAcqisition->db_query($sql_extern);
	// BOF BACK UP
?>

<?php
	require_once('inc/headerHTML.inc.php');
	$thisTitle = '&quot;Kundenerfassung&quot;-Kunden die Auftragslisten-Kundennummer zuweisen';
	$headerHTML = preg_replace("/{###TITLE###}/", strip_tags($thisTitle), $headerHTML);
	echo $headerHTML;

	require_once(FILE_MENUE_TOP);
?>
<div id="mainArea">
	<div id="mainContent">
		<?php require_once(FILE_MENUE_SIDEBAR); ?>
		<div id="contentArea">
			<div id="contentAreaElements">
				<h1><?php if(SHOW_PAGE_TITLE_ICONS) { echo '<img src="' . PATH_ICONS_MENUE_TITLES . 'shop.png" alt="" />'; } ?> <?php echo $thisTitle; ?></h1>
				<div class="contentDisplay">

				<?php displayMessages(); ?>
				<form name="formUpdateOnlineCustomerNumbers" method="post" action="<?php echo $_SERVER["PHP_SELF"]; ?>">
					<div class="actionButtonsArea">
						<input type="submit" class="inputButton0" name="submitDatas" value="Aktualisierung starten" />
					</div>
				</form>
				<?php
					if($_POST["submitDatas"] != ''){
						$countHeaders = 0;
						// BOF UPDATE CUSTUMERNUMBERS OKE-CUSTOMERS WITH Auftragslisten-CUSTUMERS
						$countHeaders++;
						echo '<h2>' . $countHeaders . '. Zuordnung der Auftragslisten-Kundennummern den Kunden in der Online-Kundenerfassung (OKE)</h2>';
						$sql_local = "DROP TABLE IF EXISTS `_temp_" . MANDATOR . "_acquisition_customers`";
						$rs_local = $dbConnection->db_query($sql_local);

						$sql_local = "CREATE TABLE IF NOT EXISTS `_temp_" . MANDATOR . "_acquisition_customers` (
										`customersID` int(11) NOT NULL AUTO_INCREMENT,
										`customersKundennummer` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
										`customersFirmenname` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
										`customersFirmennameZusatz` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
										`customersFirmenInhaberVorname` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
										`customersFirmenInhaberNachname` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
										`customersFirmenInhaberAnrede` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
										`customersFirmenInhaber2Vorname` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
										`customersFirmenInhaber2Nachname` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
										`customersFirmenInhaber2Anrede` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
										`customersAnsprechpartner1Vorname` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
										`customersAnsprechpartner1Nachname` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
										`customersAnsprechpartner1Anrede` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
										`customersAnsprechpartner2Vorname` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
										`customersAnsprechpartner2Nachname` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
										`customersAnsprechpartner2Anrede` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
										`customersTelefon1` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
										`customersTelefon2` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
										`customersMobil1` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
										`customersMobil2` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
										`customersFax1` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
										`customersFax2` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
										`customersMail1` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
										`customersMail2` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
										`customersHomepage` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
										`customersCompanyStrasse` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
										`customersCompanyHausnummer` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
										`customersCompanyCountry` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
										`customersCompanyPLZ` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
										`customersCompanyOrt` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
										`customersLieferadresseFirmenname` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
										`customersLieferadresseFirmennameZusatz` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
										`customersLieferadresseStrasse` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
										`customersLieferadresseHausnummer` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
										`customersLieferadressePLZ` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
										`customersLieferadresseOrt` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
										`customersLieferadresseLand` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
										`customersRechnungsadresseFirmenname` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
										`customersRechnungsadresseFirmennameZusatz` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
										`customersRechnungsadresseStrasse` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
										`customersRechnungsadresseHausnummer` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
										`customersRechnungsadressePLZ` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
										`customersRechnungsadresseOrt` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
										`customersRechnungsadresseLand` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
										`customersBankName` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
										`customersKontoinhaber` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
										`customersBankKontonummer` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
										`customersBankLeitzahl` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
										`customersBankIBAN` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
										`customersBankBIC` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
										`customersBezahlart` int(11) NOT NULL DEFAULT '1',
										`customersZahlungskondition` int(11) NOT NULL DEFAULT '5',
										`customersRabatt` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
										`customersUseProductMwst` enum('0','1') COLLATE utf8_unicode_ci NOT NULL DEFAULT '1',
										`customersUseProductDiscount` enum('0','1') COLLATE utf8_unicode_ci NOT NULL DEFAULT '1',
										`customersUstID` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
										`customersVertreterID` int(11) NOT NULL,
										`customersVertreterName` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
										`customersUseSalesmanDeliveryAdress` enum('0','1') COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
										`customersUseSalesmanInvoiceAdress` enum('0','1') COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
										`customersTyp` int(11) NOT NULL,
										`customersGruppe` text COLLATE utf8_unicode_ci NOT NULL,
										`customersNotiz` text COLLATE utf8_unicode_ci NOT NULL,
										`customersUserID` int(11) NOT NULL,
										`customersTimeCreated` datetime NOT NULL,
										`customersActive` enum('0','1') COLLATE utf8_unicode_ci NOT NULL,
										`customersDatasUpdated` enum('0','1') COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
										`customersTaxAccountID` int(11) NOT NULL,
										`customersInvoicesNotPurchased` enum('0','1') COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
										`customersBadPaymentBehavior` enum('0','1') COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
										`customersEntryDate` date DEFAULT NULL,
										PRIMARY KEY (`customersID`),
										KEY `customersCompanyPLZ` (`customersCompanyPLZ`),
										KEY `customersFirmenname` (`customersFirmenname`),
										KEY `customersFirmenInhaberNachname` (`customersFirmenInhaberNachname`),
										KEY `customersAnsprechpartner1Nachname` (`customersAnsprechpartner1Nachname`),
										KEY `customersAnsprechpartner2Nachname` (`customersAnsprechpartner2Nachname`),
										KEY `customersCompanyOrt` (`customersCompanyOrt`),
										KEY `customersLieferadressePLZ` (`customersLieferadressePLZ`),
										KEY `customersLieferadresseOrt` (`customersLieferadresseOrt`),
										KEY `customersRechnungsadressePLZ` (`customersRechnungsadressePLZ`),
										KEY `customersRechnungsadresseOrt` (`customersRechnungsadresseOrt`),
										KEY `customersVertreter` (`customersVertreterID`),
										KEY `customersZahlungskondition` (`customersZahlungskondition`),
										KEY `customersActive` (`customersActive`),
										KEY `customersUserID` (`customersUserID`),
										KEY `customersTyp` (`customersTyp`),
										KEY `customersVertreterName` (`customersVertreterName`),
										KEY `customersDatasUpdated` (`customersDatasUpdated`),
										KEY `customersFirmenInhaber2Nachname` (`customersFirmenInhaber2Nachname`),
										KEY `customersTaxAccountID` (`customersTaxAccountID`),
										KEY `customersFirmennameZusatz` (`customersFirmennameZusatz`),
										KEY `customersLieferadresseFirmennameZusatz` (`customersLieferadresseFirmennameZusatz`),
										KEY `customersRechnungsadresseFirmennameZusatz` (`customersRechnungsadresseFirmennameZusatz`),
										KEY `customersKundennummer` (`customersKundennummer`)
									) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='Kunden-Daten';
						";
						$rs_local = $dbConnection->db_query($sql_local);

						$sql_extern = "SELECT
									`" . TABLE_ACQUISATION_CUSTOMERS . "`.`customersID`,
									`" . TABLE_ACQUISATION_CUSTOMERS . "`.`customersKundennummer`,
									`" . TABLE_ACQUISATION_CUSTOMERS . "`.`customersFirmenname`,
									`" . TABLE_ACQUISATION_CUSTOMERS . "`.`customersFirmennameZusatz`,
									`" . TABLE_ACQUISATION_CUSTOMERS . "`.`customersFirmenInhaberVorname`,
									`" . TABLE_ACQUISATION_CUSTOMERS . "`.`customersFirmenInhaberNachname`,
									`" . TABLE_ACQUISATION_CUSTOMERS . "`.`customersFirmenInhaberAnrede`,
									`" . TABLE_ACQUISATION_CUSTOMERS . "`.`customersFirmenInhaber2Vorname`,
									`" . TABLE_ACQUISATION_CUSTOMERS . "`.`customersFirmenInhaber2Nachname`,
									`" . TABLE_ACQUISATION_CUSTOMERS . "`.`customersFirmenInhaber2Anrede`,
									`" . TABLE_ACQUISATION_CUSTOMERS . "`.`customersAnsprechpartner1Vorname`,
									`" . TABLE_ACQUISATION_CUSTOMERS . "`.`customersAnsprechpartner1Nachname`,
									`" . TABLE_ACQUISATION_CUSTOMERS . "`.`customersAnsprechpartner1Anrede`,
									`" . TABLE_ACQUISATION_CUSTOMERS . "`.`customersAnsprechpartner2Vorname`,
									`" . TABLE_ACQUISATION_CUSTOMERS . "`.`customersAnsprechpartner2Nachname`,
									`" . TABLE_ACQUISATION_CUSTOMERS . "`.`customersAnsprechpartner2Anrede`,
									`" . TABLE_ACQUISATION_CUSTOMERS . "`.`customersTelefon1`,
									`" . TABLE_ACQUISATION_CUSTOMERS . "`.`customersTelefon2`,
									`" . TABLE_ACQUISATION_CUSTOMERS . "`.`customersMobil1`,
									`" . TABLE_ACQUISATION_CUSTOMERS . "`.`customersMobil2`,
									`" . TABLE_ACQUISATION_CUSTOMERS . "`.`customersFax1`,
									`" . TABLE_ACQUISATION_CUSTOMERS . "`.`customersFax2`,
									`" . TABLE_ACQUISATION_CUSTOMERS . "`.`customersMail1`,
									`" . TABLE_ACQUISATION_CUSTOMERS . "`.`customersMail2`,
									`" . TABLE_ACQUISATION_CUSTOMERS . "`.`customersHomepage`,
									`" . TABLE_ACQUISATION_CUSTOMERS . "`.`customersCompanyStrasse`,
									`" . TABLE_ACQUISATION_CUSTOMERS . "`.`customersCompanyHausnummer`,
									`" . TABLE_ACQUISATION_CUSTOMERS . "`.`customersCompanyCountry`,
									`" . TABLE_ACQUISATION_CUSTOMERS . "`.`customersCompanyPLZ`,
									`" . TABLE_ACQUISATION_CUSTOMERS . "`.`customersCompanyOrt`,
									`" . TABLE_ACQUISATION_CUSTOMERS . "`.`customersLieferadresseFirmenname`,
									`" . TABLE_ACQUISATION_CUSTOMERS . "`.`customersLieferadresseFirmennameZusatz`,
									`" . TABLE_ACQUISATION_CUSTOMERS . "`.`customersLieferadresseStrasse`,
									`" . TABLE_ACQUISATION_CUSTOMERS . "`.`customersLieferadresseHausnummer`,
									`" . TABLE_ACQUISATION_CUSTOMERS . "`.`customersLieferadressePLZ`,
									`" . TABLE_ACQUISATION_CUSTOMERS . "`.`customersLieferadresseOrt`,
									`" . TABLE_ACQUISATION_CUSTOMERS . "`.`customersLieferadresseLand`,
									`" . TABLE_ACQUISATION_CUSTOMERS . "`.`customersRechnungsadresseFirmenname`,
									`" . TABLE_ACQUISATION_CUSTOMERS . "`.`customersRechnungsadresseFirmennameZusatz`,
									`" . TABLE_ACQUISATION_CUSTOMERS . "`.`customersRechnungsadresseStrasse`,
									`" . TABLE_ACQUISATION_CUSTOMERS . "`.`customersRechnungsadresseHausnummer`,
									`" . TABLE_ACQUISATION_CUSTOMERS . "`.`customersRechnungsadressePLZ`,
									`" . TABLE_ACQUISATION_CUSTOMERS . "`.`customersRechnungsadresseOrt`,
									`" . TABLE_ACQUISATION_CUSTOMERS . "`.`customersRechnungsadresseLand`,
									`" . TABLE_ACQUISATION_CUSTOMERS . "`.`customersBankName`,
									`" . TABLE_ACQUISATION_CUSTOMERS . "`.`customersKontoinhaber`,
									`" . TABLE_ACQUISATION_CUSTOMERS . "`.`customersBankKontonummer`,
									`" . TABLE_ACQUISATION_CUSTOMERS . "`.`customersBankLeitzahl`,
									`" . TABLE_ACQUISATION_CUSTOMERS . "`.`customersBankIBAN`,
									`" . TABLE_ACQUISATION_CUSTOMERS . "`.`customersBankBIC`,
									`" . TABLE_ACQUISATION_CUSTOMERS . "`.`customersBezahlart`,
									`" . TABLE_ACQUISATION_CUSTOMERS . "`.`customersZahlungskondition`,
									`" . TABLE_ACQUISATION_CUSTOMERS . "`.`customersRabatt`,
									`" . TABLE_ACQUISATION_CUSTOMERS . "`.`customersUseProductMwst`,
									`" . TABLE_ACQUISATION_CUSTOMERS . "`.`customersUseProductDiscount`,
									`" . TABLE_ACQUISATION_CUSTOMERS . "`.`customersUstID`,
									`" . TABLE_ACQUISATION_CUSTOMERS . "`.`customersVertreterID`,
									`" . TABLE_ACQUISATION_CUSTOMERS . "`.`customersVertreterName`,
									`" . TABLE_ACQUISATION_CUSTOMERS . "`.`customersUseSalesmanDeliveryAdress`,
									`" . TABLE_ACQUISATION_CUSTOMERS . "`.`customersUseSalesmanInvoiceAdress`,
									`" . TABLE_ACQUISATION_CUSTOMERS . "`.`customersTyp`,
									`" . TABLE_ACQUISATION_CUSTOMERS . "`.`customersGruppe`,
									`" . TABLE_ACQUISATION_CUSTOMERS . "`.`customersNotiz`,
									`" . TABLE_ACQUISATION_CUSTOMERS . "`.`customersUserID`,
									`" . TABLE_ACQUISATION_CUSTOMERS . "`.`customersTimeCreated`,
									`" . TABLE_ACQUISATION_CUSTOMERS . "`.`customersActive`,
									`" . TABLE_ACQUISATION_CUSTOMERS . "`.`customersDatasUpdated`,
									`" . TABLE_ACQUISATION_CUSTOMERS . "`.`customersTaxAccountID`,
									`" . TABLE_ACQUISATION_CUSTOMERS . "`.`customersInvoicesNotPurchased`,
									`" . TABLE_ACQUISATION_CUSTOMERS . "`.`customersBadPaymentBehavior`,
									`" . TABLE_ACQUISATION_CUSTOMERS . "`.`customersEntryDate`

								FROM `" . TABLE_ACQUISATION_CUSTOMERS . "`

								WHERE 1
									AND (
										`" . TABLE_ACQUISATION_CUSTOMERS . "`.`customersKundennummer` IS NULL
										OR
										`" . TABLE_ACQUISATION_CUSTOMERS . "`.`customersKundennummer` = ''
										OR
										`" . TABLE_ACQUISATION_CUSTOMERS . "`.`customersKundennummer` LIKE '%xx'
									)
						";

						$rs_extern = $dbConnection_ExternAcqisition->db_query($sql_extern);
						$countDatas = $dbConnection_ExternAcqisition->db_getMysqlNumRows($rs_extern);

						$infoMessage .= 'Es wurden ' . $countDatas . ' in den Online-Kunden ohne Kundennummern gefunden.' . '<br />';

						if($countDatas > 0) {
							$sqlTemp_local_fields = "INSERT INTO `_temp_" . MANDATOR . "_acquisition_customers` (
												`customersID`,
												`customersKundennummer`,
												`customersFirmenname`,
												`customersFirmennameZusatz`,
												`customersFirmenInhaberVorname`,
												`customersFirmenInhaberNachname`,
												`customersFirmenInhaberAnrede`,
												`customersFirmenInhaber2Vorname`,
												`customersFirmenInhaber2Nachname`,
												`customersFirmenInhaber2Anrede`,
												`customersAnsprechpartner1Vorname`,
												`customersAnsprechpartner1Nachname`,
												`customersAnsprechpartner1Anrede`,
												`customersAnsprechpartner2Vorname`,
												`customersAnsprechpartner2Nachname`,
												`customersAnsprechpartner2Anrede`,
												`customersTelefon1`,
												`customersTelefon2`,
												`customersMobil1`,
												`customersMobil2`,
												`customersFax1`,
												`customersFax2`,
												`customersMail1`,
												`customersMail2`,
												`customersHomepage`,
												`customersCompanyStrasse`,
												`customersCompanyHausnummer`,
												`customersCompanyCountry`,
												`customersCompanyPLZ`,
												`customersCompanyOrt`,
												`customersLieferadresseFirmenname`,
												`customersLieferadresseFirmennameZusatz`,
												`customersLieferadresseStrasse`,
												`customersLieferadresseHausnummer`,
												`customersLieferadressePLZ`,
												`customersLieferadresseOrt`,
												`customersLieferadresseLand`,
												`customersRechnungsadresseFirmenname`,
												`customersRechnungsadresseFirmennameZusatz`,
												`customersRechnungsadresseStrasse`,
												`customersRechnungsadresseHausnummer`,
												`customersRechnungsadressePLZ`,
												`customersRechnungsadresseOrt`,
												`customersRechnungsadresseLand`,
												`customersBankName`,
												`customersKontoinhaber`,
												`customersBankKontonummer`,
												`customersBankLeitzahl`,
												`customersBankIBAN`,
												`customersBankBIC`,
												`customersBezahlart`,
												`customersZahlungskondition`,
												`customersRabatt`,
												`customersUseProductMwst`,
												`customersUseProductDiscount`,
												`customersUstID`,
												`customersVertreterID`,
												`customersVertreterName`,
												`customersUseSalesmanDeliveryAdress`,
												`customersUseSalesmanInvoiceAdress`,
												`customersTyp`,
												`customersGruppe`,
												`customersNotiz`,
												`customersUserID`,
												`customersTimeCreated`,
												`customersActive`,
												`customersDatasUpdated`,
												`customersTaxAccountID`,
												`customersInvoicesNotPurchased`,
												`customersBadPaymentBehavior`,
												`customersEntryDate`
											)
											VALUES
							";
							$fileName = tempnam("/tmp", "FOO");
							$infoMessage .= 'Die tempor&auml;re Datei &quot;' . $fileName . '&quot; wurde angelegt.' . '<br />';
							$fp = fopen($fileName, 'a+');
							#fwrite($fp, removeUnnecessaryChars($sqlTemp_local) . "\n");
							$count = 0;

							$count = 0;
							$maxQueries = 400;
							$countQueries = 0;

							$sqlTemp_local_data = "";
							while ($ds = mysqli_fetch_assoc($rs_extern)) {
								foreach($ds as $thisKey => $thisValue){
									$ds[$thisKey] = addslashes($thisValue);
								}
								$sqlTemp_local_data = "('" . implode("', '", $ds) . "')";

								if($countQueries%$maxQueries == 0){
									$sqlTemp_local_data .= ";";
									$sqlTemp_local = $sqlTemp_local_fields . $sqlTemp_local_data;
									$rsTemp_local = $dbConnection->db_query($sqlTemp_local);
									#fwrite($fp_log, removeUnnecessaryChars($sqlTemp_local) . "\n");
									if(mysqli_error()) { fwrite($fp_log, date("Y-m-d H:i:s") . ": " . mysqli_error() . "\n\n\n"); }
									$sqlTemp_local_data = '';
								}
								else{
									if($count == ($countDatas - 1)) {
										$sqlTemp_local .= ";";
									}
									else {
										$sqlTemp_local .= ",";
									}
								}
								#fwrite($fp, removeUnnecessaryChars($sqlTemp_local) . "\n");
								$count++;
								$countQueries++;
							}
							fclose($fp);

							#$fp = fopen($fileName, 'r');
							#$sqlTemp_local = fread($fp, filesize($fileName));
							#fclose($fp);
							#if(file_exists($fileName)){
								#unlink($fileName);
							#}

							#$rsTemp_local = $dbConnection->db_query($sqlTemp_local);

							if($sqlTemp_local_data != ''){
								$sqlTemp_local = $sqlTemp_local_fields . $sqlTemp_local_data;
								$rsTemp_local = $dbConnection->db_query($sqlTemp_local);
								fwrite($fp_log, removeUnnecessaryChars($sqlTemp_local) . "\n");
								if(mysqli_error()) { fwrite($fp_log, date("Y-m-d H:i:s") . ": " . mysqli_error() . "\n\n\n"); }
								$sqlTemp_local_data = '';
							}

							$infoMessage . ' Zuweisung der Auftragslisten-Kundennummer im Online-Shop aufgrund der Mail-Adresse. ' . '<br />';
							$sql_local = "
									SELECT
											`_temp_" . MANDATOR . "_acquisition_customers`.`customersMail1`,
											`_temp_" . MANDATOR . "_acquisition_customers`.`customersID` AS `acquisitionCustomerID`,
											CONCAT(
												'UPDATE `" . TABLE_ACQUISATION_CUSTOMERS . "` SET `customersKundennummer` = \'',
												`" . TABLE_CUSTOMERS . "`.`customersKundennummer`,
												'\' WHERE (`customersMail1` = \'',
												`_temp_" . MANDATOR . "_acquisition_customers`.`customersMail1`,
												'\'  OR `customersMail2` = \'',
												`_temp_" . MANDATOR . "_acquisition_customers`.`customersMail1`,
												'\')  AND (`customersKundennummer` IS NULL OR `customersKundennummer` = \'\' OR `customersKundennummer` LIKE \'%xx\');'
											) AS `sqlTemp_extern`

										FROM `_temp_" . MANDATOR . "_acquisition_customers`
										INNER  JOIN `" . TABLE_CUSTOMERS . "`
										ON(
											`_temp_" . MANDATOR . "_acquisition_customers`.`customersMail1` = `" . TABLE_CUSTOMERS . "`.`customersMail1`
											OR
											`_temp_" . MANDATOR . "_acquisition_customers`.`customersMail1` = `" . TABLE_CUSTOMERS . "`.`customersMail2`
										)

										WHERE `_temp_" . MANDATOR . "_acquisition_customers`.`customersMail1` != ''

										GROUP BY `_temp_" . MANDATOR . "_acquisition_customers`.`customersMail1`

									UNION

									SELECT
											`_temp_" . MANDATOR . "_acquisition_customers`.`customersMail2`,
											`_temp_" . MANDATOR . "_acquisition_customers`.`customersID` AS `acquisitionCustomerID`,
											CONCAT(
												'UPDATE `" . TABLE_ACQUISATION_CUSTOMERS . "` SET `customersKundennummer` = \'',
												`" . TABLE_CUSTOMERS . "`.`customersKundennummer`,
												'\' WHERE (`customersMail1` = \'',
												`_temp_" . MANDATOR . "_acquisition_customers`.`customersMail2`,
												'\' OR `customersMail2` = \'',
												`_temp_" . MANDATOR . "_acquisition_customers`.`customersMail2`,
												'\') AND (`customersKundennummer` IS NULL OR `customersKundennummer` = \'\' OR `customersKundennummer` LIKE \'%xx\');'
											) AS `sqlTemp_extern`

										FROM `_temp_" . MANDATOR . "_acquisition_customers`
										INNER  JOIN `" . TABLE_CUSTOMERS . "`
										ON(
											`_temp_" . MANDATOR . "_acquisition_customers`.`customersMail2` = `" . TABLE_CUSTOMERS . "`.`customersMail1`
											OR
											`_temp_" . MANDATOR . "_acquisition_customers`.`customersMail2` = `" . TABLE_CUSTOMERS . "`.`customersMail2`
										)

										WHERE `_temp_" . MANDATOR . "_acquisition_customers`.`customersMail2` != ''

										GROUP BY `_temp_" . MANDATOR . "_acquisition_customers`.`customersMail2`
							";

							$rs_local = $dbConnection->db_query($sql_local);

							$countUpdateRows = 0;
							while ($ds = mysqli_fetch_assoc($rs_local)) {
								$tempVar = $ds["acquisitionCustomerID"] . ' | ' . $ds["sqlTemp_extern"];
								#dd('tempVar');
								$sqlTemp_local = "DELETE FROM `_temp_" . MANDATOR . "_acquisition_customers` WHERE `customersID` = '" . $ds["acquisitionCustomerID"] . "'";
								$rsTemp_local = $dbConnection->db_query($sqlTemp_local);
								$rsTemp_extern = $dbConnection_ExternAcqisition->db_query($ds["sqlTemp_extern"]);
								if($rsTemp_extern) {
									$countUpdateRows++;
								}
							}

							$successMessage .= 'Es wurden ' . $countUpdateRows . ' Kundennummern in den Kundendaten aufgrund der Mail-Adresse aktualisiert.' . '<br />';


							##################################
							/*
							$infoMessage . ' Zuweisung der Auftragslisten-Kundennummer im Online-Shop aufgrund der Anschrift. ' . '<br />';
							$sql_local = "
									SELECT
										`_temp_" . MANDATOR . "_acquisition_customers`.`customersID` AS `acquisitionCustomerID`,
										CONCAT(
											'UPDATE `" . TABLE_ACQUISATION_CUSTOMERS . "` SET `customersKundennummer` = \'',
											`" . TABLE_CUSTOMERS . "`.`customersKundennummer`,
											'\' WHERE `" . TABLE_ACQUISATION_CUSTOMERS . "`.`customersID` = \'',
											`_temp_" . MANDATOR . "_acquisition_customers`.`customersID`,
											'\' AND (`" . TABLE_ACQUISATION_CUSTOMERS . "`.`customersKundennummer` IS NULL OR `" . TABLE_ACQUISATION_CUSTOMERS . "`.`customersKundennummer` = \'\' OR `" . TABLE_ACQUISATION_CUSTOMERS . "`.`customersKundennummer` LIKE \'%xx\');'
										) AS `sqlTemp_extern`

									FROM `_temp_" . MANDATOR . "_acquisition_customers`
									INNER  JOIN `" . TABLE_CUSTOMERS . "`
									ON(
										/* ---
										`_temp_" . MANDATOR . "_acquisition_customers`.`customersCompanyStrasse` = `" . TABLE_CUSTOMERS . "`.`customersCompanyStrasse`
										AND
										`_temp_" . MANDATOR . "_acquisition_customers`.`customersCompanyHausnummer` = `" . TABLE_CUSTOMERS . "`.`customersCompanyHausnummer`
										---*/


										/*
										AND
										TRIM(REPLACE(REPLACE(REPLACE(CONCAT(`_temp_" . MANDATOR . "_acquisition_customers`.`customersCompanyStrasse`.`_temp_" . MANDATOR . "_acquisition_customers`.`customersCompanyHausnummer`), ' ', ''), '-', ''), '.', '')) = TRIM(REPLACE(REPLACE(REPLACE(CONCAT(`" . TABLE_CUSTOMERS . "`.`customersCompanyStrasse`.`" . TABLE_CUSTOMERS . "`.`customersCompanyHausnummer`), ' ', ''), '-', ''), '.', ''))

										AND
										`_temp_" . MANDATOR . "_acquisition_customers`.`customersCompanyPLZ` = `" . TABLE_CUSTOMERS . "`.`customersCompanyPLZ`

									)

									WHERE 1
										AND `_temp_" . MANDATOR . "_acquisition_customers`.`customersCompanyStrasse` != ''
										AND `_temp_" . MANDATOR . "_acquisition_customers`.`customersCompanyHausnummer` != ''
										AND `_temp_" . MANDATOR . "_acquisition_customers`.`customersCompanyPLZ` != ''

									GROUP BY CONCAT(
										`_temp_" . MANDATOR . "_acquisition_customers`.`customersCompanyStrasse`,
										'###',
										`_temp_" . MANDATOR . "_acquisition_customers`.`customersCompanyHausnummer`,
										'###',
										`_temp_" . MANDATOR . "_acquisition_customers`.`customersCompanyPLZ`
									)
							";

							$rs_local = $dbConnection->db_query($sql_local);

							$countUpdateRows = 0;
							while ($ds = mysqli_fetch_assoc($rs_local)) {
								$tempVar = $ds["acquisitionCustomerID"] . ' | ' . $ds["sqlTemp_extern"];
								#dd('tempVar');
								$sqlTemp_local = "DELETE FROM `_temp_" . MANDATOR . "_acquisition_customers` WHERE `customersID` = '" . $ds["acquisitionCustomerID"] . "'";
								$rsTemp_local = $dbConnection->db_query($sqlTemp_local);
								$rsTemp_extern = $dbConnection_ExternAcqisition->db_query($ds["sqlTemp_extern"]);
								if($rsTemp_extern) {
									$countUpdateRows++;
								}
							}

							$infoMessage .= 'Es wurden ' . $countUpdateRows . ' Kundennummern in den Kundendaten aufgrund der Anschrift aktualisiert.' . '<br />';
							*/
							##################################

							$infoMessage . ' Zuweisung der Auftragslisten-Kundennummer im Online-Shop aufgrund der Anschrift und der Telefonnummer. ' . '<br />';

							/*
							UPDATE`bctr_customersAcquisition` SET `customersKundennummer` = NULL WHERE 1;

							SELECT name, SOUNDEX(name) AS X FROM tabelle_namen HAVING X = SOUNDEX("Schmidt");
							SELECT name, SOUNDEX(name) AS X FROM tabelle_namen HAVING X = SOUNDEX("Schmidt");
							SELECT
								`customersKundennummer`,
								`customersFirmenname`,
								`customersCompanyStrasse`,
								SOUNDEX(`customersCompanyStrasse`),
								SOUNDEX('Schondorfer Str.'),
								SOUNDEX('Schorndorfer Str.'),
								SOUNDEX('Schrondorfer Str.'),
								SOUNDEX('Schondorfer Str'),
								SOUNDEX('Schondorferstr'),
								SOUNDEX('Schtondorferstr'),
								SOUNDEX('Schndorferstr'),
								SOUNDEX('Schnodorferstr'),
								SOUNDEX('Schnodorterstr'),
								SOUNDEX('Schondorterstr')
								FROM `common_customers`
								WHERE `customersFirmenname` LIKE '%teker%';


							SELECT
								`customersTelefon1`,
								SUBSTRING(`customersTelefon1`, (LENGTH(`customersTelefon1`) - 3), LENGTH(`customersTelefon1`)) AS 'length',
								REPLACE(`customersTelefon1`,' ', '') AS `replace1`,
								SUBSTRING(REPLACE(`customersTelefon1`,' ', ''), (LENGTH(REPLACE(`customersTelefon1`,' ', '')) - 3), LENGTH(REPLACE(`customersTelefon1`,' ', ''))) AS `replace2`
							FROM `common_customers`
							WHERE `customersTelefon1` != ''
							LIMIT 10;
							*/

							$sql_local = "
									SELECT
											`" . TABLE_CUSTOMERS . "`.`customersKundennummer`,
											`_temp_" . MANDATOR . "_acquisition_customers`.`customersID` AS `acquisitionCustomerID`,
											CONCAT(
												'UPDATE `" . TABLE_ACQUISATION_CUSTOMERS . "` SET `customersKundennummer` = \'',
												`" . TABLE_CUSTOMERS . "`.`customersKundennummer`,
												'\' WHERE `" . TABLE_ACQUISATION_CUSTOMERS . "`.`customersID` = \'',
												`_temp_" . MANDATOR . "_acquisition_customers`.`customersID`,
												'\' AND (`" . TABLE_ACQUISATION_CUSTOMERS . "`.`customersKundennummer` IS NULL OR `" . TABLE_ACQUISATION_CUSTOMERS . "`.`customersKundennummer` = \'\' OR `" . TABLE_ACQUISATION_CUSTOMERS . "`.`customersKundennummer` LIKE \'%xx\');'
											) AS `sqlTemp_extern`

										FROM `_temp_" . MANDATOR . "_acquisition_customers`
										INNER  JOIN `" . TABLE_CUSTOMERS . "`
										ON(
											REPLACE(`_temp_" . MANDATOR . "_acquisition_customers`.`customersCompanyStrasse`, ' ', '') = REPLACE(`" . TABLE_CUSTOMERS . "`.`customersCompanyStrasse`, ' ', '')
											AND
											`_temp_" . MANDATOR . "_acquisition_customers`.`customersCompanyHausnummer` = `" . TABLE_CUSTOMERS . "`.`customersCompanyHausnummer`
											AND
											`_temp_" . MANDATOR . "_acquisition_customers`.`customersCompanyPLZ` = `" . TABLE_CUSTOMERS . "`.`customersCompanyPLZ`
											AND (
												SUBSTRING(REPLACE(`_temp_" . MANDATOR . "_acquisition_customers`.`customersTelefon1`,' ', ''), (LENGTH(REPLACE(`_temp_" . MANDATOR . "_acquisition_customers`.`customersTelefon1`,' ', '')) - 3), LENGTH(REPLACE(`_temp_" . MANDATOR . "_acquisition_customers`.`customersTelefon1`,' ', '')))
												= SUBSTRING(REPLACE(`" . TABLE_CUSTOMERS . "`.`customersTelefon1`,' ', ''), (LENGTH(REPLACE(`" . TABLE_CUSTOMERS . "`.`customersTelefon1`,' ', '')) - 3), LENGTH(REPLACE(`" . TABLE_CUSTOMERS . "`.`customersTelefon1`,' ', '')))
												OR
												SUBSTRING(REPLACE(`_temp_" . MANDATOR . "_acquisition_customers`.`customersTelefon1`,' ', ''), (LENGTH(REPLACE(`_temp_" . MANDATOR . "_acquisition_customers`.`customersTelefon1`,' ', '')) - 3), LENGTH(REPLACE(`_temp_" . MANDATOR . "_acquisition_customers`.`customersTelefon1`,' ', '')))
												= SUBSTRING(REPLACE(`" . TABLE_CUSTOMERS . "`.`customersTelefon2`,' ', ''), (LENGTH(REPLACE(`" . TABLE_CUSTOMERS . "`.`customersTelefon2`,' ', '')) - 3), LENGTH(REPLACE(`" . TABLE_CUSTOMERS . "`.`customersTelefon2`,' ', '')))
											)
										)

										WHERE 1
											AND `_temp_" . MANDATOR . "_acquisition_customers`.`customersCompanyStrasse` != ''
											AND `_temp_" . MANDATOR . "_acquisition_customers`.`customersCompanyHausnummer` != ''
											AND `_temp_" . MANDATOR . "_acquisition_customers`.`customersCompanyPLZ` != ''
											AND `_temp_" . MANDATOR . "_acquisition_customers`.`customersTelefon1` != ''

										GROUP BY `" . TABLE_CUSTOMERS . "`.`customersKundennummer`

									UNION

									SELECT
											`" . TABLE_CUSTOMERS . "`.`customersKundennummer`,
											`_temp_" . MANDATOR . "_acquisition_customers`.`customersID` AS `acquisitionCustomerID`,
											CONCAT(
												'UPDATE `" . TABLE_ACQUISATION_CUSTOMERS . "` SET `customersKundennummer` = \'',
												`" . TABLE_CUSTOMERS . "`.`customersKundennummer`,
												'\' WHERE `" . TABLE_ACQUISATION_CUSTOMERS . "`.`customersID` = \'',
												`_temp_" . MANDATOR . "_acquisition_customers`.`customersID`,
												'\' AND (`" . TABLE_ACQUISATION_CUSTOMERS . "`.`customersKundennummer` IS NULL OR `" . TABLE_ACQUISATION_CUSTOMERS . "`.`customersKundennummer` = \'\' OR `" . TABLE_ACQUISATION_CUSTOMERS . "`.`customersKundennummer` LIKE \'%xx\');'
											) AS `sqlTemp_extern`

										FROM `_temp_" . MANDATOR . "_acquisition_customers`
										INNER  JOIN `" . TABLE_CUSTOMERS . "`
										ON(
											REPLACE(`_temp_" . MANDATOR . "_acquisition_customers`.`customersCompanyStrasse`, ' ', '') = REPLACE(`" . TABLE_CUSTOMERS . "`.`customersCompanyStrasse`, ' ', '')
											AND
											`_temp_" . MANDATOR . "_acquisition_customers`.`customersCompanyHausnummer` = `" . TABLE_CUSTOMERS . "`.`customersCompanyHausnummer`
											AND
											`_temp_" . MANDATOR . "_acquisition_customers`.`customersCompanyPLZ` = `" . TABLE_CUSTOMERS . "`.`customersCompanyPLZ`
											AND (
												SUBSTRING(REPLACE(`_temp_" . MANDATOR . "_acquisition_customers`.`customersTelefon2`,' ', ''), (LENGTH(REPLACE(`_temp_" . MANDATOR . "_acquisition_customers`.`customersTelefon2`,' ', '')) - 3), LENGTH(REPLACE(`_temp_" . MANDATOR . "_acquisition_customers`.`customersTelefon2`,' ', '')))
												= SUBSTRING(REPLACE(`" . TABLE_CUSTOMERS . "`.`customersTelefon1`,' ', ''), (LENGTH(REPLACE(`" . TABLE_CUSTOMERS . "`.`customersTelefon1`,' ', '')) - 3), LENGTH(REPLACE(`" . TABLE_CUSTOMERS . "`.`customersTelefon1`,' ', '')))
												OR
												SUBSTRING(REPLACE(`_temp_" . MANDATOR . "_acquisition_customers`.`customersTelefon2`,' ', ''), (LENGTH(REPLACE(`_temp_" . MANDATOR . "_acquisition_customers`.`customersTelefon2`,' ', '')) - 3), LENGTH(REPLACE(`_temp_" . MANDATOR . "_acquisition_customers`.`customersTelefon2`,' ', '')))
												= SUBSTRING(REPLACE(`" . TABLE_CUSTOMERS . "`.`customersTelefon2`,' ', ''), (LENGTH(REPLACE(`" . TABLE_CUSTOMERS . "`.`customersTelefon2`,' ', '')) - 3), LENGTH(REPLACE(`" . TABLE_CUSTOMERS . "`.`customersTelefon2`,' ', '')))
											)
										)

										WHERE 1
											AND `_temp_" . MANDATOR . "_acquisition_customers`.`customersCompanyStrasse` != ''
											AND `_temp_" . MANDATOR . "_acquisition_customers`.`customersCompanyHausnummer` != ''
											AND `_temp_" . MANDATOR . "_acquisition_customers`.`customersCompanyPLZ` != ''
											AND `_temp_" . MANDATOR . "_acquisition_customers`.`customersTelefon2` != ''

										GROUP BY `" . TABLE_CUSTOMERS . "`.`customersKundennummer`

									UNION

									SELECT
											`" . TABLE_CUSTOMERS . "`.`customersKundennummer`,
											`_temp_" . MANDATOR . "_acquisition_customers`.`customersID` AS `acquisitionCustomerID`,
											CONCAT(
												'UPDATE `" . TABLE_ACQUISATION_CUSTOMERS . "` SET `customersKundennummer` = \'',
												`" . TABLE_CUSTOMERS . "`.`customersKundennummer`,
												'\' WHERE `" . TABLE_ACQUISATION_CUSTOMERS . "`.`customersID` = \'',
												`_temp_" . MANDATOR . "_acquisition_customers`.`customersID`,
												'\' AND (`" . TABLE_ACQUISATION_CUSTOMERS . "`.`customersKundennummer` IS NULL OR `" . TABLE_ACQUISATION_CUSTOMERS . "`.`customersKundennummer` = \'\' OR `" . TABLE_ACQUISATION_CUSTOMERS . "`.`customersKundennummer` LIKE \'%xx\');'
											) AS `sqlTemp_extern`

										FROM `_temp_" . MANDATOR . "_acquisition_customers`
										INNER  JOIN `" . TABLE_CUSTOMERS . "`
										ON(
											REPLACE(`_temp_" . MANDATOR . "_acquisition_customers`.`customersCompanyStrasse`, ' ', '') = REPLACE(`" . TABLE_CUSTOMERS . "`.`customersCompanyStrasse`, ' ', '')
											AND
											`_temp_" . MANDATOR . "_acquisition_customers`.`customersCompanyHausnummer` = `" . TABLE_CUSTOMERS . "`.`customersCompanyHausnummer`
											AND
											`_temp_" . MANDATOR . "_acquisition_customers`.`customersCompanyPLZ` = `" . TABLE_CUSTOMERS . "`.`customersCompanyPLZ`
											AND (
												SUBSTRING(REPLACE(`_temp_" . MANDATOR . "_acquisition_customers`.`customersMobil1`,' ', ''), (LENGTH(REPLACE(`_temp_" . MANDATOR . "_acquisition_customers`.`customersMobil1`,' ', '')) - 3), LENGTH(REPLACE(`_temp_" . MANDATOR . "_acquisition_customers`.`customersMobil1`,' ', '')))
												= SUBSTRING(REPLACE(`" . TABLE_CUSTOMERS . "`.`customersMobil1`,' ', ''), (LENGTH(REPLACE(`" . TABLE_CUSTOMERS . "`.`customersMobil1`,' ', '')) - 3), LENGTH(REPLACE(`" . TABLE_CUSTOMERS . "`.`customersMobil1`,' ', '')))
												OR
												SUBSTRING(REPLACE(`_temp_" . MANDATOR . "_acquisition_customers`.`customersMobil1`,' ', ''), (LENGTH(REPLACE(`_temp_" . MANDATOR . "_acquisition_customers`.`customersMobil1`,' ', '')) - 3), LENGTH(REPLACE(`_temp_" . MANDATOR . "_acquisition_customers`.`customersMobil1`,' ', '')))
												= SUBSTRING(REPLACE(`" . TABLE_CUSTOMERS . "`.`customersMobil2`,' ', ''), (LENGTH(REPLACE(`" . TABLE_CUSTOMERS . "`.`customersMobil2`,' ', '')) - 3), LENGTH(REPLACE(`" . TABLE_CUSTOMERS . "`.`customersMobil2`,' ', '')))
											)
										)

										WHERE 1
											AND `_temp_" . MANDATOR . "_acquisition_customers`.`customersCompanyStrasse` != ''
											AND `_temp_" . MANDATOR . "_acquisition_customers`.`customersCompanyHausnummer` != ''
											AND `_temp_" . MANDATOR . "_acquisition_customers`.`customersCompanyPLZ` != ''
											AND `_temp_" . MANDATOR . "_acquisition_customers`.`customersMobil1` != ''
										GROUP BY `" . TABLE_CUSTOMERS . "`.`customersKundennummer`

									UNION

									SELECT
											`" . TABLE_CUSTOMERS . "`.`customersKundennummer`,
											`_temp_" . MANDATOR . "_acquisition_customers`.`customersID` AS `acquisitionCustomerID`,
											CONCAT(
												'UPDATE `" . TABLE_ACQUISATION_CUSTOMERS . "` SET `customersKundennummer` = \'',
												`" . TABLE_CUSTOMERS . "`.`customersKundennummer`,
												'\' WHERE `" . TABLE_ACQUISATION_CUSTOMERS . "`.`customersID` = \'',
												`_temp_" . MANDATOR . "_acquisition_customers`.`customersID`,
												'\' AND (`" . TABLE_ACQUISATION_CUSTOMERS . "`.`customersKundennummer` IS NULL OR `" . TABLE_ACQUISATION_CUSTOMERS . "`.`customersKundennummer` = \'\' OR `" . TABLE_ACQUISATION_CUSTOMERS . "`.`customersKundennummer` LIKE \'%xx\');'
											) AS `sqlTemp_extern`

										FROM `_temp_" . MANDATOR . "_acquisition_customers`
										INNER  JOIN `" . TABLE_CUSTOMERS . "`
										ON(
											REPLACE(`_temp_" . MANDATOR . "_acquisition_customers`.`customersCompanyStrasse`, ' ', '') = REPLACE(`" . TABLE_CUSTOMERS . "`.`customersCompanyStrasse`, ' ', '')
											AND
											`_temp_" . MANDATOR . "_acquisition_customers`.`customersCompanyHausnummer` = `" . TABLE_CUSTOMERS . "`.`customersCompanyHausnummer`
											AND
											`_temp_" . MANDATOR . "_acquisition_customers`.`customersCompanyPLZ` = `" . TABLE_CUSTOMERS . "`.`customersCompanyPLZ`
											AND (
												SUBSTRING(REPLACE(`_temp_" . MANDATOR . "_acquisition_customers`.`customersMobil2`,' ', ''), (LENGTH(REPLACE(`_temp_" . MANDATOR . "_acquisition_customers`.`customersMobil2`,' ', '')) - 3), LENGTH(REPLACE(`_temp_" . MANDATOR . "_acquisition_customers`.`customersMobil2`,' ', '')))
												= SUBSTRING(REPLACE(`" . TABLE_CUSTOMERS . "`.`customersMobil1`,' ', ''), (LENGTH(REPLACE(`" . TABLE_CUSTOMERS . "`.`customersMobil1`,' ', '')) - 3), LENGTH(REPLACE(`" . TABLE_CUSTOMERS . "`.`customersMobil1`,' ', '')))
												OR
												SUBSTRING(REPLACE(`_temp_" . MANDATOR . "_acquisition_customers`.`customersMobil2`,' ', ''), (LENGTH(REPLACE(`_temp_" . MANDATOR . "_acquisition_customers`.`customersMobil2`,' ', '')) - 3), LENGTH(REPLACE(`_temp_" . MANDATOR . "_acquisition_customers`.`customersMobil2`,' ', '')))
												= SUBSTRING(REPLACE(`" . TABLE_CUSTOMERS . "`.`customersMobil2`,' ', ''), (LENGTH(REPLACE(`" . TABLE_CUSTOMERS . "`.`customersMobil2`,' ', '')) - 3), LENGTH(REPLACE(`" . TABLE_CUSTOMERS . "`.`customersMobil2`,' ', '')))
											)
										)

										WHERE 1
											AND `_temp_" . MANDATOR . "_acquisition_customers`.`customersCompanyStrasse` != ''
											AND `_temp_" . MANDATOR . "_acquisition_customers`.`customersCompanyHausnummer` != ''
											AND `_temp_" . MANDATOR . "_acquisition_customers`.`customersCompanyPLZ` != ''
											AND `_temp_" . MANDATOR . "_acquisition_customers`.`customersMobil2` != ''

										GROUP BY `" . TABLE_CUSTOMERS . "`.`customersKundennummer`

									UNION

									SELECT
											`" . TABLE_CUSTOMERS . "`.`customersKundennummer`,
											`_temp_" . MANDATOR . "_acquisition_customers`.`customersID` AS `acquisitionCustomerID`,
											CONCAT(
												'UPDATE `" . TABLE_ACQUISATION_CUSTOMERS . "` SET `customersKundennummer` = \'',
												`" . TABLE_CUSTOMERS . "`.`customersKundennummer`,
												'\' WHERE `" . TABLE_ACQUISATION_CUSTOMERS . "`.`customersID` = \'',
												`_temp_" . MANDATOR . "_acquisition_customers`.`customersID`,
												'\' AND (`" . TABLE_ACQUISATION_CUSTOMERS . "`.`customersKundennummer` IS NULL OR `" . TABLE_ACQUISATION_CUSTOMERS . "`.`customersKundennummer` = \'\' OR `" . TABLE_ACQUISATION_CUSTOMERS . "`.`customersKundennummer` LIKE \'%xx\');'
											) AS `sqlTemp_extern`

										FROM `_temp_" . MANDATOR . "_acquisition_customers`
										INNER  JOIN `" . TABLE_CUSTOMERS . "`
										ON(
											REPLACE(`_temp_" . MANDATOR . "_acquisition_customers`.`customersCompanyStrasse`, ' ', '') = REPLACE(`" . TABLE_CUSTOMERS . "`.`customersCompanyStrasse`, ' ', '')
											AND
											`_temp_" . MANDATOR . "_acquisition_customers`.`customersCompanyHausnummer` = `" . TABLE_CUSTOMERS . "`.`customersCompanyHausnummer`
											AND
											`_temp_" . MANDATOR . "_acquisition_customers`.`customersCompanyPLZ` = `" . TABLE_CUSTOMERS . "`.`customersCompanyPLZ`
											AND (
												SUBSTRING(REPLACE(`_temp_" . MANDATOR . "_acquisition_customers`.`customersFax1`,' ', ''), (LENGTH(REPLACE(`_temp_" . MANDATOR . "_acquisition_customers`.`customersFax1`,' ', '')) - 3), LENGTH(REPLACE(`_temp_" . MANDATOR . "_acquisition_customers`.`customersFax1`,' ', '')))
												= SUBSTRING(REPLACE(`" . TABLE_CUSTOMERS . "`.`customersFax1`,' ', ''), (LENGTH(REPLACE(`" . TABLE_CUSTOMERS . "`.`customersFax1`,' ', '')) - 3), LENGTH(REPLACE(`" . TABLE_CUSTOMERS . "`.`customersFax1`,' ', '')))
												OR
												SUBSTRING(REPLACE(`_temp_" . MANDATOR . "_acquisition_customers`.`customersFax1`,' ', ''), (LENGTH(REPLACE(`_temp_" . MANDATOR . "_acquisition_customers`.`customersFax1`,' ', '')) - 3), LENGTH(REPLACE(`_temp_" . MANDATOR . "_acquisition_customers`.`customersFax1`,' ', '')))
												= SUBSTRING(REPLACE(`" . TABLE_CUSTOMERS . "`.`customersFax2`,' ', ''), (LENGTH(REPLACE(`" . TABLE_CUSTOMERS . "`.`customersFax2`,' ', '')) - 3), LENGTH(REPLACE(`" . TABLE_CUSTOMERS . "`.`customersFax2`,' ', '')))
											)
										)

										WHERE 1
											AND `_temp_" . MANDATOR . "_acquisition_customers`.`customersCompanyStrasse` != ''
											AND `_temp_" . MANDATOR . "_acquisition_customers`.`customersCompanyHausnummer` != ''
											AND `_temp_" . MANDATOR . "_acquisition_customers`.`customersCompanyPLZ` != ''
											AND `_temp_" . MANDATOR . "_acquisition_customers`.`customersFax1` != ''

										GROUP BY `" . TABLE_CUSTOMERS . "`.`customersKundennummer`

									UNION

									SELECT
											`" . TABLE_CUSTOMERS . "`.`customersKundennummer`,
											`_temp_" . MANDATOR . "_acquisition_customers`.`customersID` AS `acquisitionCustomerID`,
											CONCAT(
												'UPDATE `" . TABLE_ACQUISATION_CUSTOMERS . "` SET `customersKundennummer` = \'',
												`" . TABLE_CUSTOMERS . "`.`customersKundennummer`,
												'\' WHERE `" . TABLE_ACQUISATION_CUSTOMERS . "`.`customersID` = \'',
												`_temp_" . MANDATOR . "_acquisition_customers`.`customersID`,
												'\' AND (`" . TABLE_ACQUISATION_CUSTOMERS . "`.`customersKundennummer` IS NULL OR `" . TABLE_ACQUISATION_CUSTOMERS . "`.`customersKundennummer` = \'\' OR `" . TABLE_ACQUISATION_CUSTOMERS . "`.`customersKundennummer` LIKE \'%xx\');'
											) AS `sqlTemp_extern`

										FROM `_temp_" . MANDATOR . "_acquisition_customers`
										INNER  JOIN `" . TABLE_CUSTOMERS . "`
										ON(
											REPLACE(`_temp_" . MANDATOR . "_acquisition_customers`.`customersCompanyStrasse`, ' ', '') = REPLACE(`" . TABLE_CUSTOMERS . "`.`customersCompanyStrasse`, ' ', '')
											AND
											`_temp_" . MANDATOR . "_acquisition_customers`.`customersCompanyHausnummer` = `" . TABLE_CUSTOMERS . "`.`customersCompanyHausnummer`
											AND
											`_temp_" . MANDATOR . "_acquisition_customers`.`customersCompanyPLZ` = `" . TABLE_CUSTOMERS . "`.`customersCompanyPLZ`
											AND (
												SUBSTRING(REPLACE(`_temp_" . MANDATOR . "_acquisition_customers`.`customersFax2`,' ', ''), (LENGTH(REPLACE(`_temp_" . MANDATOR . "_acquisition_customers`.`customersFax2`,' ', '')) - 3), LENGTH(REPLACE(`_temp_" . MANDATOR . "_acquisition_customers`.`customersFax2`,' ', '')))
												= SUBSTRING(REPLACE(`" . TABLE_CUSTOMERS . "`.`customersFax1`,' ', ''), (LENGTH(REPLACE(`" . TABLE_CUSTOMERS . "`.`customersFax1`,' ', '')) - 3), LENGTH(REPLACE(`" . TABLE_CUSTOMERS . "`.`customersFax1`,' ', '')))
												OR
												SUBSTRING(REPLACE(`_temp_" . MANDATOR . "_acquisition_customers`.`customersFax2`,' ', ''), (LENGTH(REPLACE(`_temp_" . MANDATOR . "_acquisition_customers`.`customersFax2`,' ', '')) - 3), LENGTH(REPLACE(`_temp_" . MANDATOR . "_acquisition_customers`.`customersFax2`,' ', '')))
												= SUBSTRING(REPLACE(`" . TABLE_CUSTOMERS . "`.`customersFax2`,' ', ''), (LENGTH(REPLACE(`" . TABLE_CUSTOMERS . "`.`customersFax2`,' ', '')) - 3), LENGTH(REPLACE(`" . TABLE_CUSTOMERS . "`.`customersFax2`,' ', '')))
											)
										)

										WHERE 1
											AND `_temp_" . MANDATOR . "_acquisition_customers`.`customersCompanyStrasse` != ''
											AND `_temp_" . MANDATOR . "_acquisition_customers`.`customersCompanyHausnummer` != ''
											AND `_temp_" . MANDATOR . "_acquisition_customers`.`customersCompanyPLZ` != ''
											AND `_temp_" . MANDATOR . "_acquisition_customers`.`customersFax2` != ''

										GROUP BY `" . TABLE_CUSTOMERS . "`.`customersKundennummer`
							";

							$rs_local = $dbConnection->db_query($sql_local);

							$countUpdateRows = 0;
							while ($ds = mysqli_fetch_assoc($rs_local)) {
								$tempVar = $ds["acquisitionCustomerID"] . ' | ' . $ds["sqlTemp_extern"];
								#dd('tempVar');
								$sqlTemp_local = "DELETE FROM `_temp_" . MANDATOR . "_acquisition_customers` WHERE `customersID` = '" . $ds["acquisitionCustomerID"] . "'";
								$rsTemp_local = $dbConnection->db_query($sqlTemp_local);
								$rsTemp_extern = $dbConnection_ExternAcqisition->db_query($ds["sqlTemp_extern"]);
								if($rsTemp_extern) {
									$countUpdateRows++;
								}
							}

							$successMessage .= 'Es wurden ' . $countUpdateRows . ' Kundennummern in den Kundendaten aufgrund der Anschrift und der Telefonnummer aktualisiert.' . '<br />';

							##################################

							$sql_local = "DROP TABLE IF EXISTS `_temp_" . MANDATOR . "_acquisition_customers`";
							$rs_local = $dbConnection->db_query($sql_local);
						}
						// EOF UPDATE CUSTUMERNUMBERS OKE-CUSTOMERS WITH Auftragslisten-CUSTUMERS


						displayMessages();

						// BOF COPY AND CREATE CUSTOMERS FROM Auftragslisten FOR OKE UPLOAD
							echo '<hr />';
							$countHeaders++;
							echo '<h2>' . $countHeaders . '. UPLOAD der Kunden aus dem Auftragslisten in die Online-Kundenerfassung (OKE)</h2>';
							$infoMessage .= 'Es werden nur Kunden hochgeladen, die in der OKE nicht erfasst sind.' . '<br />';
						if(1){

							// BOF COPY Auftragslisten CUSTOMERS
							$sql_local = "DROP TABLE IF EXISTS `temp_" . MANDATOR . "_upload_customers`";
							$rs_local = $dbConnection->db_query($sql_local);

							$sql_local = "CREATE TABLE `temp_" . MANDATOR . "_upload_customers` LIKE `common_customers`";
							$rs_local = $dbConnection->db_query($sql_local);

							$sql_local = "INSERT INTO `temp_" . MANDATOR . "_upload_customers` SELECT * FROM `common_customers`";
							$rs_local = $dbConnection->db_query($sql_local);
							// EOF COPY Auftragslisten CUSTOMERS

							// BOF CLEAN Auftragslisten CUSTOMERS: DELETE SALESMEN, AMAZON etc AND NON GERMAN CUSTOMERS
							$sql_local = "DELETE FROM `temp_" . MANDATOR . "_upload_customers`
											WHERE 1
												AND (
													`customersTyp` != 1
													AND
													`customersTyp` != 5
													AND
													`customersTyp` != 6
												)";
							$rs_local = $dbConnection->db_query($sql_local);

							$sql_local = "DELETE FROM `temp_" . MANDATOR . "_upload_customers`
											WHERE 1
												AND `customersCompanyCountry` != 81 ";
							$rs_local = $dbConnection->db_query($sql_local);
							// EOF CLEAN Auftragslisten CUSTOMERS: DELETE SALESMEN, AMAZON etc AND NON GERMAN CUSTOMERS

							// BOF DELETE CUSTOMERS THAT ARE IN OKE (DELETING CUSTOMER NUMBERS)
							$sql_extern = "SELECT `customersKundennummer` FROM `bctr_customersAcquisition` WHERE 1 AND `customersKundennummer` != ''";
							$rs_extern = $dbConnection_ExternAcqisition->db_query($sql_extern);
							while($ds = mysqli_fetch_assoc($rs_extern)){
								$sqlTemp_local = "DELETE FROM `temp_" . MANDATOR . "_upload_customers` WHERE `customersKundennummer` = '" . $ds["customersKundennummer"] .  "'";
								$rsTemp_local = $dbConnection->db_query($sqlTemp_local);
							}
							// EOF DELETE CUSTOMERS THAT ARE IN OKE (DELETING CUSTOMER NUMBERS)

							// BOF UPLOAD CUSTOMERS FROM Auftragslisten TO OKE
								$sql_local = "SELECT * FROM `temp_" . MANDATOR . "_upload_customers` WHERE 1";
								$rs_local = $dbConnection->db_query($sql_local);

								$sqlTemp_externPrefix = "
									INSERT INTO `".$thisUSE_DB."`.`bctr_customersAcquisition` (
											`customersID`,
											`customersKundennummer`,
											`customersFirmenname`,
											`customersFirmennameZusatz`,
											`customersFirmenInhaberVorname`,
											`customersFirmenInhaberNachname`,
											`customersFirmenInhaberAnrede`,
											`customersFirmenInhaber2Vorname`,
											`customersFirmenInhaber2Nachname`,
											`customersFirmenInhaber2Anrede`,
											`customersAnsprechpartner1Vorname`,
											`customersAnsprechpartner1Nachname`,
											`customersAnsprechpartner1Anrede`,
											`customersAnsprechpartner1Typ`,
											`customersAnsprechpartner2Vorname`,
											`customersAnsprechpartner2Nachname`,
											`customersAnsprechpartner2Anrede`,
											`customersAnsprechpartner2Typ`,
											`customersTelefon1`,
											`customersTelefon2`,
											`customersMobil1`,
											`customersMobil2`,
											`customersFax1`,
											`customersFax2`,
											`customersMail1`,
											`customersMail2`,
											`customersHomepage`,
											`customersCompanyStrasse`,
											`customersCompanyHausnummer`,
											`customersCompanyCountry`,
											`customersCompanyPLZ`,
											`customersCompanyOrt`,
											`customersLieferadresseFirmenname`,
											`customersLieferadresseFirmennameZusatz`,
											`customersLieferadresseStrasse`,
											`customersLieferadresseHausnummer`,
											`customersLieferadressePLZ`,
											`customersLieferadresseOrt`,
											`customersLieferadresseLand`,
											`customersRechnungsadresseFirmenname`,
											`customersRechnungsadresseFirmennameZusatz`,
											`customersRechnungsadresseStrasse`,
											`customersRechnungsadresseHausnummer`,
											`customersRechnungsadressePLZ`,
											`customersRechnungsadresseOrt`,
											`customersRechnungsadresseLand`,
											`customersBankName`,
											`customersKontoinhaber`,
											`customersBankKontonummer`,
											`customersBankLeitzahl`,
											`customersBankIBAN`,
											`customersBankBIC`,
											`customersBezahlart`,
											`customersZahlungskondition`,
											`customersRabatt`,
											`customersRabattType`,
											`customersUseProductMwst`,
											`customersUseProductDiscount`,
											`customersUstID`,
											`customersVertreterID`,
											`customersVertreterName`,
											`customersUseSalesmanDeliveryAdress`,
											`customersUseSalesmanInvoiceAdress`,
											`customersTyp`,
											`customersGruppe`,
											`customersNotiz`,
											`customersUserID`,
											`customersTimeCreated`,
											`customersActive`,
											`customersDatasUpdated`,
											`customersTaxAccountID`,
											`customersInvoicesNotPurchased`,
											`customersBadPaymentBehavior`,
											`customersEntryDate`,
											`customersSepaExists`,
											`customersSepaValidUntilDate`,
											`customersSepaRequestedDate`,
											`customersLastVisit`,
											`customersNextVisit`
										) VALUES
									";
								$arrSqlTemp_extern = array();
								$limitSql = 14;
								$countRow = 0;
								while($ds = mysqli_fetch_assoc($rs_local)){
									foreach(array_keys($ds) as $field){
										$ds[$field] = ($ds[$field]);
										$ds[$field] = preg_replace("/'/", "\'", $ds[$field]);
									}

									$arrSqlTemp_extern[] = "
										(
											'%',
											'" . $ds["customersKundennummer"] . "',
											'" . $ds["customersFirmenname"] . "',
											'" . $ds["customersFirmennameZusatz"] . "',
											'" . $ds["customersFirmenInhaberVorname"] . "',
											'" . $ds["customersFirmenInhaberNachname"] . "',
											'" . $ds["customersFirmenInhaberAnrede"] . "',
											'" . $ds["customersFirmenInhaber2Vorname"] . "',
											'" . $ds["customersFirmenInhaber2Nachname"] . "',
											'" . $ds["customersFirmenInhaber2Anrede"] . "',
											'" . $ds["customersAnsprechpartner1Vorname"] . "',
											'" . $ds["customersAnsprechpartner1Nachname"] . "',
											'" . $ds["customersAnsprechpartner1Anrede"] . "',
											'" . $ds["customersAnsprechpartner1Typ"] . "',
											'" . $ds["customersAnsprechpartner2Vorname"] . "',
											'" . $ds["customersAnsprechpartner2Nachname"] . "',
											'" . $ds["customersAnsprechpartner2Anrede"] . "',
											'" . $ds["customersAnsprechpartner2Typ"] . "',
											'" . $ds["customersTelefon1"] . "',
											'" . $ds["customersTelefon2"] . "',
											'" . $ds["customersMobil1"] . "',
											'" . $ds["customersMobil2"] . "',
											'" . $ds["customersFax1"] . "',
											'" . $ds["customersFax2"] . "',
											'" . $ds["customersMail1"] . "',
											'" . $ds["customersMail2"] . "',
											'" . $ds["customersHomepage"] . "',
											'" . $ds["customersCompanyStrasse"] . "',
											'" . $ds["customersCompanyHausnummer"] . "',
											'" . $ds["customersCompanyCountry"] . "',
											'" . $ds["customersCompanyPLZ"] . "',
											'" . $ds["customersCompanyOrt"] . "',
											'" . $ds["customersLieferadresseFirmenname"] . "',
											'" . $ds["customersLieferadresseFirmennameZusatz"] . "',
											'" . $ds["customersLieferadresseStrasse"] . "',
											'" . $ds["customersLieferadresseHausnummer"] . "',
											'" . $ds["customersLieferadressePLZ"] . "',
											'" . $ds["customersLieferadresseOrt"] . "',
											'" . $ds["customersLieferadresseLand"] . "',
											'" . $ds["customersRechnungsadresseFirmenname"] . "',
											'" . $ds["customersRechnungsadresseFirmennameZusatz"] . "',
											'" . $ds["customersRechnungsadresseStrasse"] . "',
											'" . $ds["customersRechnungsadresseHausnummer"] . "',
											'" . $ds["customersRechnungsadressePLZ"] . "',
											'" . $ds["customersRechnungsadresseOrt"] . "',
											'" . $ds["customersRechnungsadresseLand"] . "',
											'" . $ds["customersBankName"] . "',
											'" . $ds["customersKontoinhaber"] . "',
											'" . $ds["customersBankKontonummer"] . "',
											'" . $ds["customersBankLeitzahl"] . "',
											'" . $ds["customersBankIBAN"] . "',
											'" . $ds["customersBankBIC"] . "',
											'" . $ds["customersBezahlart"] . "',
											'" . $ds["customersZahlungskondition"] . "',
											'" . $ds["customersRabatt"] . "',
											'" . $ds["customersRabattType"] . "',
											'" . $ds["customersUseProductMwst"] . "',
											'" . $ds["customersUseProductDiscount"] . "',
											'" . $ds["customersUstID"] . "',
											'0',
											'',
											'" . $ds["customersUseSalesmanDeliveryAdress"] . "',
											'" . $ds["customersUseSalesmanInvoiceAdress"] . "',
											'" . $ds["customersTyp"] . "',
											'" . $ds["customersGruppe"] . "',
											'" . $ds["customersNotiz"] . "',
											'0',
											'" . $ds["customersTimeCreated"] . "',
											'" . $ds["customersActive"] . "',
											'" . $ds["customersDatasUpdated"] . "',
											'" . $ds["customersTaxAccountID"] . "',
											'" . $ds["customersInvoicesNotPurchased"] . "',
											'" . $ds["customersBadPaymentBehavior"] . "',
											'" . $ds["customersEntryDate"] . "',
											'" . $ds["customersSepaExists"] . "',
											'" . $ds["customersSepaValidUntilDate"] . "',
											'" . $ds["customersSepaRequestedDate"] . "',
											'" . $ds["customersLastVisit"] . "',
											'" . $ds["customersNextVisit"] . "'
										)
									";

									if($countRow%$limitSql == 0 && $countRow > 0 && !empty($arrSqlTemp_extern)){
										$sqlTemp_extern = $sqlTemp_externPrefix . implode(", ", $arrSqlTemp_extern);
										$sqlTemp_extern = removeUnnecessaryChars($sqlTemp_extern);
										$rsTemp_extern = $dbConnection_ExternAcqisition->db_query($sqlTemp_extern);
										$arrSqlTemp_extern = array();
										if(!$rsTemp_extern){ echo $sqlTemp_extern . '<hr />'; }
									}
									$countRow++;
								}
								if(!empty($arrSqlTemp_extern)){
									$sqlTemp_extern = $sqlTemp_externPrefix . implode(", ", $arrSqlTemp_extern);
									$sqlTemp_extern = removeUnnecessaryChars($sqlTemp_extern);
									$rsTemp_extern = $dbConnection_ExternAcqisition->db_query($sqlTemp_extern);
									$arrSqlTemp_extern = array();
									if(!$rsTemp_extern){ echo $sqlTemp_extern . '<hr />'; }
								}

								if($rsTemp_extern){
									$successMessage .= ' Es wurden ' . $countRow . ' Kunden eingelesen. ' . '<br />';
								}
								else {
									$errorMessage .= ' FEHLER beim  ' . $countRow . ' Kunden eingelesen. ' . '<br />';
								}
							// EOF UPLOAD CUSTOMERS FROM Auftragslisten TO OKE
						}
						else {
							$warningMessage .= 'ACHTUNG!!!! Der Kundenimport vom Auftragslisten in die OKE findet nicht statt.' . '<br />';
						}
						// EOF COPY AND CREATE CUSTOMERS FROM Auftragslisten FOR OKE UPLOAD

						displayMessages();

						// BOF UPDATE OKE CUSTOMERS WITH SPECIAL Auftragslisten CUSTOMERS DATA
						$todayDate = date("Y-m-d");
						$todayMonth = date("m");
						$timeInterval = 3; // DAYS
						$selectDate = date("Y-m-d", mktime(0, 0, 0, date("m"), (date("d") - $timeInterval),date("Y") ));
						echo '<hr />';
						$countHeaders++;
						echo '<h2>' . $countHeaders . '. UPDATE der OKE-Kunden mit speziellen Daten der Auftragslisten-Kunden</h2>';
						$infoMessage .= 'ACHTUNG!!!! Daten der letzten ' . $timeInterval . ' Tage (ab ' . formatDate($selectDate, "display") . ').' . '<br />';

						$where_local = "";
						if($timeInterval > 0){
							$where_local = " AND `customersTimeCreated` > '" . $selectDate . "' ";
						}

						$sql_local = "SELECT
										*
										FROM `" . TABLE_CUSTOMERS . "`
										WHERE 1
											AND (
												`customersTyp` = 1
												OR
												`customersTyp` = 5
												OR
												`customersTyp` = 6
											)
											AND `customersCompanyCountry` = 81
											AND `customersKundennummer` != ''
											" . $where_local . "

							";

						$rs_local = $dbConnection->db_query($sql_local);
						$countDatas = $dbConnection->db_getMysqlNumRows($rs_local);
						$countRow = 0;

if(file_exists($fileUpdateCustomers)){
	unlink($fileUpdateCustomers);
}
$fileUpdateCustomers = 'updateCustomersSQL.sql';
$fpUpdateCustomers = fopen($fileUpdateCustomers, 'a');

						while($ds = mysqli_fetch_assoc($rs_local)){
							$sqlTemp_extern = "
									UPDATE
										`".$thisUSE_DB."`.`bctr_customersAcquisition`

										SET
											`customersFirmenname` = '" . $ds["customersFirmenname"] . "',
											`customersFirmennameZusatz` = '" . $ds["customersFirmennameZusatz"] . "',

											`customersFirmenInhaberVorname` = '" . $ds["customersFirmenInhaberVorname"] . "',
											`customersFirmenInhaberNachname` = '" . $ds["customersFirmenInhaberNachname"] . "',
											`customersFirmenInhaberAnrede` = '" . $ds["customersFirmenInhaberAnrede"] . "',
											`customersFirmenInhaber2Vorname` = '" . $ds["customersFirmenInhaber2Vorname"] . "',
											`customersFirmenInhaber2Nachname` = '" . $ds["customersFirmenInhaber2Nachname"] . "',
											`customersFirmenInhaber2Anrede` = '" . $ds["customersFirmenInhaber2Anrede"] . "',
											`customersAnsprechpartner1Vorname` = '" . $ds["customersAnsprechpartner1Vorname"] . "',
											`customersAnsprechpartner1Nachname` = '" . $ds["customersAnsprechpartner1Nachname"] . "',
											`customersAnsprechpartner1Anrede` = '" . $ds["customersAnsprechpartner1Anrede"] . "',
											`customersAnsprechpartner1Typ` = '" . $ds["customersAnsprechpartner1Typ"] . "',
											`customersAnsprechpartner2Vorname` = '" . $ds["customersAnsprechpartner2Vorname"] . "',
											`customersAnsprechpartner2Nachname` = '" . $ds["customersAnsprechpartner2Nachname"] . "',
											`customersAnsprechpartner2Anrede` = '" . $ds["customersAnsprechpartner2Anrede"] . "',
											`customersAnsprechpartner2Typ` = '" . $ds["customersAnsprechpartner2Typ"] . "',
											`customersTelefon1` = '" . $ds["customersTelefon1"] . "',
											`customersTelefon2` = '" . $ds["customersTelefon2"] . "',
											`customersMobil1` = '" . $ds["customersMobil1"] . "',
											`customersMobil2` = '" . $ds["customersMobil2"] . "',
											`customersFax1` = '" . $ds["customersFax1"] . "',
											`customersFax2` = '" . $ds["customersFax2"] . "',
											`customersMail1` = '" . $ds["customersMail1"] . "',
											`customersMail2` = '" . $ds["customersMail2"] . "',
											`customersHomepage` = '" . $ds["customersHomepage"] . "',
											`customersCompanyStrasse` = '" . $ds["customersCompanyStrasse"] . "',
											`customersCompanyHausnummer` = '" . $ds["customersCompanyHausnummer"] . "',
											`customersCompanyCountry` = '" . $ds["customersCompanyCountry"] . "',
											`customersCompanyPLZ` = '" . $ds["customersCompanyPLZ"] . "',
											`customersCompanyOrt` = '" . $ds["customersCompanyOrt"] . "',
											`customersLieferadresseFirmenname` = '" . $ds["customersLieferadresseFirmenname"] . "',
											`customersLieferadresseFirmennameZusatz` = '" . $ds["customersLieferadresseFirmennameZusatz"] . "',
											`customersLieferadresseStrasse` = '" . $ds["customersLieferadresseStrasse"] . "',
											`customersLieferadresseHausnummer` = '" . $ds["customersLieferadresseHausnummer"] . "',
											`customersLieferadressePLZ` = '" . $ds["customersLieferadressePLZ"] . "',
											`customersLieferadresseOrt` = '" . $ds["customersLieferadresseOrt"] . "',
											`customersLieferadresseLand` = '" . $ds["customersLieferadresseLand"] . "',
											`customersRechnungsadresseFirmenname` = '" . $ds["customersRechnungsadresseFirmenname"] . "',
											`customersRechnungsadresseFirmennameZusatz` = '" . $ds["customersRechnungsadresseFirmennameZusatz"] . "',
											`customersRechnungsadresseStrasse` = '" . $ds["customersRechnungsadresseStrasse"] . "',
											`customersRechnungsadresseHausnummer` = '" . $ds["customersRechnungsadresseHausnummer"] . "',
											`customersRechnungsadressePLZ` = '" . $ds["customersRechnungsadressePLZ"] . "',
											`customersRechnungsadresseOrt` = '" . $ds["customersRechnungsadresseOrt"] . "',
											`customersRechnungsadresseLand` = '" . $ds["customersRechnungsadresseLand"] . "',
											`customersBankName` = '" . $ds["customersBankName"] . "',
											`customersKontoinhaber` = '" . $ds["customersKontoinhaber"] . "',
											`customersBankKontonummer` = '" . $ds["customersBankKontonummer"] . "',
											`customersBankLeitzahl` = '" . $ds["customersBankLeitzahl"] . "',
											`customersBankIBAN` = '" . $ds["customersBankIBAN"] . "',
											`customersBankBIC` = '" . $ds["customersBankBIC"] . "',
											`customersBezahlart` = '" . $ds["customersBezahlart"] . "',
											`customersZahlungskondition` = '" . $ds["customersZahlungskondition"] . "',
											`customersRabatt` = '" . $ds["customersRabatt"] . "',
											`customersRabattType` = '" . $ds["customersRabattType"] . "',
											`customersUseProductMwst` = '" . $ds["customersUseProductMwst"] . "',
											`customersUseProductDiscount` = '" . $ds["customersUseProductDiscount"] . "',
											`customersUstID` = '" . $ds["customersUstID"] . "',

											`customersUseSalesmanDeliveryAdress` = '" . $ds["customersUseSalesmanDeliveryAdress"] . "',
											`customersUseSalesmanInvoiceAdress` = '" . $ds["customersUseSalesmanInvoiceAdress"] . "',
											`customersTyp` = '" . $ds["customersTyp"] . "',

											`customersNotiz` = '" . $ds["customersNotiz"] . "',
											`customersUserID` = '" . $ds["customersUserID"] . "',
											`customersTimeCreated` = '" . $ds["customersTimeCreated"] . "',

											`customersDatasUpdated` = '" . $ds["customersDatasUpdated"] . "',
											`customersTaxAccountID` = '" . $ds["customersTaxAccountID"] . "',

											`customersLastVisit` = '" . $ds["customersLastVisit"] . "',
											`customersNextVisit` = '" . $ds["customersNextVisit"] . "',
											`customersCompanyNotExists` = '" . $ds["customersCompanyNotExists"] . "',
											`customersCompanyLocation` = '" . $ds["customersCompanyLocation"] . "',
											`customersIsVisibleForUserID` = '" . $ds["customersIsVisibleForUserID"] . "',
											`customersPriceListsID` = '" . $ds["customersPriceListsID"] . "',
											`customersOriginalVertreterID` = '" . $ds["customersOriginalVertreterID"] . "',
											`customersGetNoCollectedInvoice` = '" . $ds["customersGetNoCollectedInvoice"] . "',
											`customersNoDeliveries` = '" . $ds["customersNoDeliveries"] . "',
											`customersSendPaperInvoice` = '" . $ds["customersSendPaperInvoice"] . "',

											`customersGruppe` = '" . $ds["customersGruppe"] . "',
											`customersInvoicesNotPurchased` = '" . $ds["customersInvoicesNotPurchased"] . "',
											`customersBadPaymentBehavior` = '" . $ds["customersBadPaymentBehavior"] . "',
											`customersEntryDate` = '" . $ds["customersEntryDate"] . "',
											`customersSepaExists` = '" . $ds["customersSepaExists"] . "',
											`customersSepaValidUntilDate` = '" . $ds["customersSepaValidUntilDate"] . "',
											`customersSepaRequestedDate` = '" . $ds["customersSepaRequestedDate"] . "',
											`customersActive` = '" . $ds["customersActive"] . "'

											,
											`customersVertreterID` = '" . $ds["customersVertreterID"] . "',
											`customersVertreterName` = '" . addslashes($ds["customersVertreterName"]) . "'
										WHERE `customersKundennummer` = '" . $ds["customersKundennummer"] . "'

								";
							$sqlTemp_extern = removeUnnecessaryChars($sqlTemp_extern);

fwrite($fpUpdateCustomers, $sqlTemp_extern . "\n");

							$rsTemp_extern = $dbConnection_ExternAcqisition->db_query($sqlTemp_extern);

							$countRow++;
						}
fclose($fpUpdateCustomers);
						if($rsTemp_extern){
							$successMessage .= ' Es wurden f&uuml;r ' . $countRow . ' Kunden Daten aktualisiert. ' . '<br />';
						}
						else {
							$errorMessage .= ' FEHLER beim ' . $countRow . ' Kunden Daten aktualisiert. ' . '<br />';
						}
						// EOF UPDATE OKE CUSTOMERS WITH SPECIAL Auftragslisten CUSTOMERS DATA

						displayMessages();

						// BOF INSERT CUSTOMERS LAST ORDER DATAS FROM Auftragslisten FOR OKE UPLOAD
							$todayDate = date("Y-m-d");
							$todayMonth = date("m");
							$timeInterval = 30; // DAYS
							$selectDate = date("Y-m-d", mktime(0, 0, 0, date("m"), (date("d") - $timeInterval),date("Y") ));

							echo '<hr />';
							$countHeaders++;
							echo '<h2>' . $countHeaders . '. UPDATE der Kunden in der Online-Kundenerfassung (OKE) mit den Daten der letzten Bestellung</h2>';

							$infoMessage .= 'ACHTUNG!!!! Daten der letzten ' . $timeInterval . ' Tage (ab ' . formatDate($selectDate, "display") . ').' . '<br />';

							$exportCountryID = 81;
							$sql_local = "
									SELECT
										`common_customers`.`customersFirmenname`,
										`common_customers`.`customersKundennummer`,
										`common_customers`.`customersGruppe`,
										`common_customers`.`customersTyp`,

										`tempTable`.`orderDocumentsNumber`,
										`tempTable`.`orderDocumentsCustomerNumber`,
										`tempTable`.`orderDocumentsDocumentDate`,
										`tempTable`.`orderDocumentsStatus`,
										`tempTable`.`orderDocumentsSalesman`,

										`tempTable`.`orderDocumentDetailProductNumber`,
										`tempTable`.`orderDocumentDetailProductName`,
										`tempTable`.`orderDocumentDetailProductQuantity`,
										`tempTable`.`orderDocumentDetailProductSinglePrice`,
										`tempTable`.`orderDocumentDetailProductKategorieID`,

										`tempTable`.`orderDocumentsMandatory`,

										`tableSalesmen`.`customersFirmenname` AS `salesmenFirmenname`,

										`common_productCategories`.`orderCategoriesName`

										FROM (
												SELECT
													`orderDocumentsDocumentDate`,
													`orderDocumentsNumber`,
													`orderDocumentsCustomerNumber`,
													`orderDocumentsStatus`,
													`orderDocumentsSalesman`,

													`orderDocumentDetailProductNumber`,
													`orderDocumentDetailProductName`,
													`orderDocumentDetailProductQuantity`,
													`orderDocumentDetailProductSinglePrice`,
													`orderDocumentDetailProductKategorieID`,

													'bctr' AS `orderDocumentsMandatory`

												FROM `bctr_orderConfirmations`
												LEFT JOIN `bctr_orderConfirmationsDetails`
												ON( `bctr_orderConfirmations`.`orderDocumentsID` = `bctr_orderConfirmationsDetails`.`orderDocumentDetailDocumentID`)

												WHERE 1
													AND `orderDocumentDetailProductKategorieID` LIKE '001%'
													AND `orderDocumentsDocumentDate` > '" . $selectDate . "'

												UNION

												SELECT
													`orderDocumentsDocumentDate`,
													`orderDocumentsNumber`,
													`orderDocumentsCustomerNumber`,
													`orderDocumentsStatus`,
													`orderDocumentsSalesman`,

													`orderDocumentDetailProductNumber`,
													`orderDocumentDetailProductName`,
													`orderDocumentDetailProductQuantity`,
													`orderDocumentDetailProductSinglePrice`,
													`orderDocumentDetailProductKategorieID`,

													'b3' AS `orderDocumentsMandatory`

												FROM `b3_orderConfirmations`
												LEFT JOIN `b3_orderConfirmationsDetails`
												ON( `b3_orderConfirmations`.`orderDocumentsID` = `b3_orderConfirmationsDetails`.`orderDocumentDetailDocumentID`)

												WHERE 1
													AND `orderDocumentDetailProductKategorieID` LIKE '001%'
													AND `orderDocumentsDocumentDate` > '" . $selectDate . "'

												ORDER BY `orderDocumentsDocumentDate` DESC
											) AS `tempTable`

											LEFT JOIN `common_customers`
											ON(`tempTable`.`orderDocumentsCustomerNumber` = `common_customers`.`customersKundennummer`)

											LEFT JOIN `common_productCategories`
											ON(`tempTable`.`orderDocumentDetailProductKategorieID` COLLATE utf8_unicode_ci = `common_productCategories`.`orderCategoriesID`)

											/*
											LEFT JOIN `common_customerTypes`
											ON(`common_customers`.`customersTyp` = `common_customerTypes`.`customerTypesID`)

											LEFT JOIN `common_customerGroups`
											ON(`common_customers`.`customersGruppe` = `common_customerGroups`.`customerGroupsID`)

											LEFT JOIN `common_customers` AS `tableSalesmen`
											ON(`common_customers`.`customersVertreterID` = `tableSalesmen`.`customersID`)
											*/

											WHERE 1
												AND `common_customers`.`customersCompanyCountry` = 81
												AND `common_customers`.`customersKundennummer` NOT LIKE 'AMZ%'
												AND `common_customers`.`customersKundennummer` NOT LIKE 'EBY%'
												AND (
													`common_customers`.`customersTyp` = 1
													OR
													`common_customers`.`customersTyp` = 5
													OR
													`common_customers`.`customersTyp` = 6
												)

											GROUP BY `tempTable`.`orderDocumentsNumber`
								";
							$rs_local = $dbConnection->db_query($sql_local);

							$arrSqlTemp_extern = array();
							$limitSql = 60;
							$countRow = 0;

							$sqlTemp_externPrefix = "
									REPLACE INTO `common_customersLastOrders` (
													`customersLastOrdersID`,
													`customersLastOrdersCustomerNumber`,
													`customersLastOrdersSalesmanID`,
													`customersLastOrdersDocumentNumber`,
													`customersLastOrdersDocumentDate`,
													`customersLastOrdersDocumentStatus`,
													`customersLastOrdersProductCategoryID`,
													`customersLastOrdersProductCategoryName`,
													`customersLastOrdersProductNumber`,
													`customersLastOrdersProductName`,
													`customersLastOrdersProductQuantity`,
													`customersLastOrdersProductSinglePrice`,
													`customersLastOrdersTimestamp`,

													`customersLastOrdersSalesmanCompanyName`,

													`customersLastOrdersMandatory`
												)
												VALUES
								";

							while($ds = mysqli_fetch_assoc($rs_local)){
								foreach(array_keys($ds) as $field){
									$ds[$field] = ($ds[$field]);
									$ds[$field] = preg_replace("/'/", "\'", $ds[$field]);
								}

								$arrSqlTemp_extern[] = "
									(
													'%',
													'" . $ds["customersKundennummer"] . "',
													'" . $ds["orderDocumentsNumber"] . "',
													'" . $ds["orderDocumentsDocumentDate"] . "',
													'" . $ds["orderDocumentsStatus"] . "',
													'" . $ds["orderDocumentDetailProductKategorieID"] . "',
													'" . $ds["orderCategoriesName"] . "',
													'" . $ds["orderDocumentDetailProductNumber"] . "',
													'" . $ds["orderDocumentDetailProductName"] . "',
													'" . $ds["orderDocumentDetailProductQuantity"] . "',
													'" . $ds["orderDocumentDetailProductSinglePrice"] . "',
													NOW(),

													'" . $ds["salesmenFirmenname"] . "',

													'" . $ds["orderDocumentsMandatory"] . "'
												)
									";

								if($countRow%$limitSql == 0 && $countRow > 0 && !empty($arrSqlTemp_extern)){
									$sqlTemp_extern = $sqlTemp_externPrefix . implode(", ", $arrSqlTemp_extern);
									$sqlTemp_extern = removeUnnecessaryChars($sqlTemp_extern);
									$rsTemp_extern = $dbConnection_ExternAcqisition->db_query($sqlTemp_extern);
									$arrSqlTemp_extern = array();
									if(!$rsTemp_extern){ echo $sqlTemp_extern . '<hr />'; }
								}
								$countRow++;
							}

							if(!empty($arrSqlTemp_extern)){
								$sqlTemp_extern = $sqlTemp_externPrefix . implode(", ", $arrSqlTemp_extern);
								$sqlTemp_extern = removeUnnecessaryChars($sqlTemp_extern);
								$rsTemp_extern = $dbConnection_ExternAcqisition->db_query($sqlTemp_extern);
								$arrSqlTemp_extern = array();
								if(!$rsTemp_extern){ echo $sqlTemp_extern . '<hr />'; }
							}

							if($rsTemp_extern){
								$successMessage .= ' Es wurden f&uuml;r ' . $countRow . ' Kunden Daten eingelesen. ' . '<br />';
							}
							else {
								$errorMessage .= ' FEHLER beim ' . $countRow . ' Kunden Daten eingelesen. ' . '<br />';
							}

							displayMessages();
						// EOF INSERT CUSTOMERS LAST ORDER DATAS FROM Auftragslisten FOR OKE UPLOAD

						// BOF DELETE OLDER BACK_UP_TABLES
							$dateToday = date("Y-m-d");
							$timeInterval = 3; // DAYS
							$dateToDelete = date("Y-m-d", mktime(0, 0, 0, date("m"), (date("d") - $timeInterval), date("Y")));

							echo '<hr />';
							$countHeaders++;
							echo '<h2>' . $countHeaders . '. UPDATE der Kunden in der Online-Kundenerfassung (OKE) mit den Daten der letzten Bestellung</h2>';

							$infoMessage .= 'Es werden die Tabellen-Sicherungen der letzten ' . $timeInterval . ' Tage (ab ' . formatDate($dateToDelete, "display") . ') entfernt.' . '<br />';

							$deleteTableName = "_SICH_" . TABLE_ACQUISATION_CUSTOMERS . "_" . $dateToDelete;
							$sql_extern = "
									SELECT
										CONCAT( 'DROP TABLE ', GROUP_CONCAT('`', `TABLE_NAME`, '`') , ';' ) AS `statement`
										FROM `information_schema`.`TABLES`
										WHERE 1
											AND `table_schema` = '" . DB_NAME_EXTERN_ACQUISITION . "'
											AND `TABLE_NAME` LIKE '" . $deleteTableName . "%'
								";
							$rs_extern = $dbConnection_ExternAcqisition->db_query($sql_extern);
							list($thisSqlStatement_extern) = mysqli_fetch_array($rs_extern);

							$rs_extern = $dbConnection_ExternAcqisition->db_query($thisSqlStatement_extern);

							if($rsTemp_extern){
								$successMessage .= ' Es wurden ' . $dbConnection_ExternAcqisition->db_getMysqlAffectedRows($rs_extern) . ' Sicherungs-Tabellen entfernt. ' . '<br />';
							}
							else {
								$errorMessage .= ' FEHLER beim Entfernen der alten Sicherungs-Tabellen. ' . '<br />';
							}


							$fp = fopen(FILENAME_LOGFILE, "w");
							fwrite($fp, $dateToDelete . "\n" . $deleteTableName . "\n" . $sql_extern);
							fclose($fp);

							displayMessages();
						// EOF DELETE OLDER BACK_UP_TABLES

						// BOF UPDATE CUSTOMER NUMBERS IN CUSTOMER ORDERS
							echo '<hr />';
							$countHeaders++;
							echo '<h2>' . $countHeaders . '. Aktualisieren der Kunden-Bestellungen mit Kundennummern</h2>';

							$sql_extern = "
								UPDATE
									`common_orders`

									LEFT JOIN `bctr_customersAcquisition`
									ON(`common_orders`.`ordersCustomerID` = `bctr_customersAcquisition`.`customersID`)

									SET `common_orders`.`ordersKundennummer` = `bctr_customersAcquisition`.`customersKundennummer`
									WHERE 1
										AND `common_orders`.`ordersKundennummer` = ''
										AND `common_orders`.`ordersCustomerID` > 0
										AND `bctr_customersAcquisition`.`customersKundennummer` > 0
								";
							$rs_extern = $dbConnection_ExternAcqisition->db_query($sql_extern);

							if($rsTemp_extern){
								$successMessage .= ' Es wurden ' . $dbConnection_ExternAcqisition->db_getMysqlAffectedRows($rs_extern) . ' Kunden-Bestellungen mit Kundennummern aktualisiert. ' . '<br />';
							}
							else {
								$errorMessage .= ' FEHLER beim Aktualisieren der Kunden-Bestellungen mit Kundennummern. ' . '<br />';
							}
							displayMessages();
						// EOF UPDATE CUSTOMER NUMBERS IN CUSTOMER ORDERS

						// BOF UPDATE CUSTOMER NUMBERS IN SALESMEN REPORTS
							echo '<hr />';
							$countHeaders++;
							echo '<h2>' . $countHeaders . '. Aktualisieren der ADM-Berichte mit Kundennummern</h2>';

							$sql_extern = "
								UPDATE
									`bctr_usersDriversLogsDetails`

									LEFT JOIN `bctr_customersAcquisition`
									ON(`bctr_usersDriversLogsDetails`.`usersDriversLogsDetailsCustomerID` = `bctr_customersAcquisition`.`customersID`)

									SET `bctr_usersDriversLogsDetails`.`usersDriversLogsDetailsCustomerNumber` = `bctr_customersAcquisition`.`customersKundennummer`
									WHERE 1
										AND `usersDriversLogsDetailsCustomerNumber` = ''
										AND `usersDriversLogsDetailsCustomerID` > 0
										AND `bctr_customersAcquisition`.`customersKundennummer` > 0	;
								";
							$rs_extern = $dbConnection_ExternAcqisition->db_query($sql_extern);

							if($rsTemp_extern){
								$successMessage .= ' Es wurden ' . $dbConnection_ExternAcqisition->db_getMysqlAffectedRows($rs_extern) . ' ADM-Berichte mit Kundennummern aktualisiert. ' . '<br />';
							}
							else {
								$errorMessage .= ' FEHLER beim Aktualisieren der ADM-Berichte mit Kundennummern. ' . '<br />';
							}
							displayMessages();
						// EOF UPDATE CUSTOMER NUMBERS IN SALESMEN REPORTS

						// BOF SET CUSTOMERS VISIBLE FOR HVs IN OKE DEPENDING ON Auftragslisten-ORDERS
							echo '<hr />';
							$countHeaders++;
							echo '<h2>' . $countHeaders . '. Freischalten bestimmter OKE-Kunden zur Ansicht f&uuml;r Handelsvertreter.</h2>';
							// BOF GET HVs FROM OKE
								$sql_extern = "
									SELECT
										*
										FROM `bctr_users`
										WHERE 1
											AND `bctr_users`.`usersGroupID` = 4
											AND `bctr_users`.`usersActive` = '1'
											AND `bctr_users`.`usersAuftragslistenCustomerID` > 0
									";
								$rs_extern = $dbConnection_ExternAcqisition->db_query($sql_extern);
								echo mysqli_error();

								$arrHVsinOKE = array();
								while($ds_extern = mysqli_fetch_assoc($rs_extern)){
									foreach(array_keys($ds_extern) as $field){
										$arrHVsinOKE[$ds_extern["usersID"]][$field] = $ds_extern[$field];
									}
								}
							// EOF GET HVs FROM OKE

							if(!empty($arrHVsinOKE)){
								foreach($arrHVsinOKE as $thisHVsinOKEKey => $thisHVsinOKEValue){
									// BOF GET CUSTOMER NUMBERS FROM INVOICES AND CONFIRMATIONS WITH SALESMAN-ID
										$sql_lokal = "
												SELECT
													`orderDocumentsCustomerNumber`,
													`orderDocumentsSalesman`,
													`orderDocumentsNumber`
													FROM `bctr_orderConfirmations`
													WHERE 1
														AND `orderDocumentsSalesman` = " . $thisHVsinOKEValue["usersAuftragslistenCustomerID"] . "

												UNION

												SELECT
													`orderDocumentsCustomerNumber`,
													`orderDocumentsSalesman`,
													`orderDocumentsNumber`
													FROM `bctr_orderInvoices`
													WHERE 1
														AND `orderDocumentsSalesman` = " . $thisHVsinOKEValue["usersAuftragslistenCustomerID"] . "

												UNION

												SELECT
													`orderDocumentsCustomerNumber`,
													`orderDocumentsSalesman`,
													`orderDocumentsNumber`
													FROM `b3_orderConfirmations`
													WHERE 1
														AND `orderDocumentsSalesman` = " . $thisHVsinOKEValue["usersAuftragslistenCustomerID"] . "

												UNION

												SELECT
													`orderDocumentsCustomerNumber`,
													`orderDocumentsSalesman`,
													`orderDocumentsNumber`
													FROM `b3_orderInvoices`
													WHERE 1
														AND `orderDocumentsSalesman` = " . $thisHVsinOKEValue["usersAuftragslistenCustomerID"] . "

												UNION

												SELECT
													`ordersKundennummer` AS `orderDocumentsCustomerNumber`,
													`ordersVertreterID` AS `orderDocumentsSalesman`,
													'' AS `orderDocumentsNumber`
													FROM `common_ordersprocess`
													WHERE 1
														AND `ordersVertreterID` = " . $thisHVsinOKEValue["usersAuftragslistenCustomerID"] . "

												GROUP BY `orderDocumentsCustomerNumber`
											";

										$rs_lokal = $dbConnection->db_query($sql_lokal);
										echo mysqli_error();

										$arrSqlWhere = array();
										while($ds_lokal = mysqli_fetch_assoc($rs_lokal)){
											$arrSqlWhere[] = " `bctr_customersAcquisition`.`customersKundennummer` = '" . $ds_lokal["orderDocumentsCustomerNumber"]. "' ";
										}
										if(!empty($arrSqlWhere)){
											$sql_externUpdate = "
													UPDATE
														`bctr_customersAcquisition`
														SET `bctr_customersAcquisition`.`customersIsVisibleForUserID` = '" . $thisHVsinOKEValue["usersID"] . "'
														WHERE 1
															AND (
																" . implode(" OR ", $arrSqlWhere). "
															)
												";

											$rs_externUpdate = $dbConnection_ExternAcqisition->db_query($sql_externUpdate);
											echo mysqli_error();

											if($rs_externUpdate){
												$successMessage .= count($arrSqlWhere) . ' OKE-Kunden wurden f&uuml;r ' . $thisHVsinOKEValue["usersFirstName"] . ' ' . $thisHVsinOKEValue["usersLastName"] . ' (Auftragslisten-ID: ' . $thisHVsinOKEValue["usersAuftragslistenCustomerID"] . ') zur Ansicht freigegeben. ' . '<br />';
											}
											else {
												$errorMessage .= count($arrSqlWhere) . ' OKE-Kunden konnten nicht f&uuml;r ' . $thisHVsinOKEValue["usersFirstName"] . ' ' . $thisHVsinOKEValue["usersLastName"] . ' (Auftragslisten-ID: ' . $thisHVsinOKEValue["usersAuftragslistenCustomerID"] . ') zur Ansicht freigegeben werden. ' . '<br />';
											}
										}
									// EOF GET CUSTOMER NUMBERS FROM INVOICES AND CONFIRMATIONS WITH SALESMAN-ID
									displayMessages();
								}
							}
						// EOF SET CUSTOMERS VISIBLE FOR HVs IN OKE DEPENDING ON Auftragslisten-ORDERS
					}
				?>
				<?php displayMessages(); ?>
				</div>
			</div>
		</div>
		<div class="clear"></div>
	</div>
</div>

<script language="javascript" type="text/javascript">
	$(document).ready(function() {
		colorRowMouseOver('.displayOrders tbody tr');
		// toggleAreas();
	});
</script>

<?php require_once('inc/footerHTML.inc.php'); ?>