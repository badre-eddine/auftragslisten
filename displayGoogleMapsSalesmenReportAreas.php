<?php
	session_start();

	require_once('config/configMandator.inc.php');
	require_once('config/configParams_' . strtolower(MANDATOR) . '.inc.php');
	require_once('config/configBasic.inc.php');
	require_once('config/configFiles.inc.php');
	require_once('config/configTables.inc.php');
	require_once('inc/functions.inc.php');
	##require_once('header.inc.php');

	$thisSearchString = (trim($_GET["strZipcodes"]));

	$warningMessage = '';
	$errorMessage = '';
	$successMessage = '';
	$infoMessage = '';

	$useUf8Encode = false;

	$dbConnection = new DB_Connection(DB_HOST, '', DB_NAME, DB_USER, DB_PASSWORD);
	$dbOpen = $dbConnection->db_connect();

	// BOF GET GOOGLE MAPS POINTS
	$arrAreas = explode("_", $thisSearchString);
	$where = "";
	$arrWhere = array();

	if(!empty($arrAreas)) {
		$where .= " AND (";

		foreach($arrAreas as $thisZipcode) {
			$arrWhere[] = " `" . TABLE_GEODB_TEXTDATA . "`.`text_val` = '" . $thisZipcode . "' ";
			#$arrWhere2[] = " `" . TABLE_ZIPCODES_CITIES . "`.`postal_code` LIKE '" . $thisZipcode . "%' ";
		}
		$where .= implode (" OR ", $arrWhere);
		$where .= ") ";
	}
	$sql = "
			 SELECT
				`" . TABLE_GEODB_COORDINATES . "`.`lat` AS `latitude`,
				`" . TABLE_GEODB_COORDINATES . "`.`lon` AS `longitude`,
				`" . TABLE_GEODB_TEXTDATA . "`.`text_val`,
				`geodb_textdata2`.`text_val` AS `text_val2`,
				`" . TABLE_ZIPCODES_CITIES . "`.`place_name`,
				`" . TABLE_ZIPCODES_CITIES . "`.`postal_code`,
				`" . TABLE_ZIPCODES_CITIES . "`.`firstOrderSubdivision_stateName` AS `region_1`,
				`" . TABLE_ZIPCODES_CITIES . "`.`secondOrderSubdivision_stateName` AS `region_2`,
				`" . TABLE_ZIPCODES_CITIES . "`.`thirdOrderSubdivision_stateName` AS `region_3`

			FROM `" . TABLE_GEODB_TEXTDATA . "`

			LEFT JOIN `" . TABLE_GEODB_TEXTDATA . "`  AS `geodb_textdata2`
			ON(`" . TABLE_GEODB_TEXTDATA . "`.`loc_id` = `geodb_textdata2`.`loc_id`)

			LEFT JOIN `" . TABLE_GEODB_LOCATIONS . "`
			ON (`" . TABLE_GEODB_TEXTDATA . "`.`loc_id` = `" . TABLE_GEODB_LOCATIONS . "`.`loc_id`)


			LEFT JOIN `" . TABLE_GEODB_COORDINATES . "`
			ON(`" . TABLE_GEODB_TEXTDATA . "`.`loc_id` = `" . TABLE_GEODB_COORDINATES . "`.`loc_id`)

			LEFT JOIN `" . TABLE_ZIPCODES_CITIES . "`
			ON(`" . TABLE_GEODB_TEXTDATA . "`.`text_val` = `" . TABLE_ZIPCODES_CITIES . "`.`postal_code`)

			WHERE `" . TABLE_GEODB_TEXTDATA . "`.`text_type` = '500300000'
				AND `geodb_textdata2`.`text_type` = '500100000'
				" . $where . "


			GROUP BY CONCAT(`latitude`, '-', `longitude`)
	";

	$rs = $dbConnection->db_query($sql);

	echo mysqli_error();
	$strGoogleMapPoints = "";

	$countPoints = 1;
	while($ds = mysqli_fetch_assoc($rs)) {
		$arrPoints[] = array("latitude" => $ds["latitude"], "longitude" => $ds["longitude"], "text_val" =>  $ds["text_val"], "placeName" =>  $ds["place_name"]);

		#$arrGoogleMapZipcodeAreas[$salesmenDatas["salesmenFirmenname"]][] = 'new google.maps.LatLng(' . $ds["latitude"] . ', ' . $ds["longitude"] . ')';

		$strGoogleMapPoints .= 	'myPointDatas[' . $countPoints . '] = new Array();';
		$strGoogleMapPoints .= 	'myPointDatas[' . $countPoints . ']["latitude"] = "' . $ds["latitude"] . '";';
		$strGoogleMapPoints .= 	'myPointDatas[' . $countPoints . ']["longitude"] = "' . $ds["longitude"] . '";';
		$strGoogleMapPoints .= 	'myPointDatas[' . $countPoints . ']["title"] = "' . utf8_decode($ds["text_val"]) . ' ' . utf8_decode($ds["place_name"]);
		if($ds["text_val2"] != $ds["place_name"]) { $strGoogleMapPoints .= ' / ' . utf8_decode($ds["text_val2"]); }
		$strGoogleMapPoints .= 	'";';
		$strGoogleMapPoints .= 	'myPointDatas[' . $countPoints . ']["header"] = "' . utf8_decode($ds["text_val"]) . ' ' . utf8_decode($ds["place_name"]);
		if($ds["text_val2"] != $ds["place_name"]) { $strGoogleMapPoints .= ' / ' . utf8_decode($ds["text_val2"]); }
		$strGoogleMapPoints .= 	'";';
		#$strGoogleMapPoints .= 	'myPointDatas[' . $countPoints . ']["text"] = "' . $_POST["editSalesmenFirmenname"] . ' (K-Nr: ' . $_POST["editSalesmenKundennummer"] . ')";';
		$strGoogleMapPoints .= 	'myPointDatas[' . $countPoints . ']["text"] = "";';
		$strGoogleMapPoints .= 	'myPointDatas[' . $countPoints . ']["city"] = "' . utf8_decode($ds["text_val"]) . '";';
		$strGoogleMapPoints .= 	'myPointDatas[' . $countPoints . ']["region_1"] = "' . utf8_decode($ds["region_1"]) . '";';
		$strGoogleMapPoints .= 	'myPointDatas[' . $countPoints . ']["region_2"] = "' . utf8_decode($ds["region_2"]) . '";';
		$strGoogleMapPoints .= 	'myPointDatas[' . $countPoints . ']["region_3"] = "' . utf8_decode($ds["region_3"]) . '";';
		$strGoogleMapPoints .= 	'myPointDatas[' . $countPoints . ']["imagePath"] = "";';
		$strGoogleMapPoints .= 	'myPointDatas[' . $countPoints . ']["markerImage"] = "googleMapsIcon_2.png";';
		$countPoints ++;


	}
	// EOF GET GOOGLE MAPS POINTS
?>

<?php
	require_once('inc/headerHTML.inc.php');
	$thisTitle = '';
	$headerHTML = preg_replace("/{###TITLE###}/", strip_tags($thisTitle), $headerHTML);
	$headerHTML = preg_replace('/<div id="headerArea">(.*)<noscript>/ism', '<noscript>', $headerHTML);
	$headerHTML = preg_replace('/<body/ism', '<body id="bodyFrame" onload="initialize();"', $headerHTML);

	echo $headerHTML;
?>
	<div id="contentAreaElements">
		<div id="myGoogleMapsArea">
			<div id="myGoogleMapCanvas">
				<span class="loadingArea"><img class="loader" src="layout/ajax-loader.gif" /> Bitte haben Sie ein wenig Geduld. Die Daten werden geladen...</span>
			</div>
		</div>
	</div>
	<script language="javascript" type="text/javascript">
		$('#myGoogleMapCanvas').css({
			'width': '100%',
			'height': ($(window).height() - 30) + 'px'
		});
	</script>

	<!-- GOOGLE MAPS START -->
	<script type="text/javascript" src="<?php echo PATH_GOOGLE_MAPS_API; ?>"></script>
	<script type="text/javascript" language="javascript">
		// try {
		function setGmapMarkers(map, i) {
			var infWindowKey = i;
			var thisMarkerImage;
			if(i == 0) {
				thisMarkerImage = "bctr_googleMarkerFlag.png";
				thisMarkerShadowImage = "bctr_googleMarkerFlagShadow.png";
				thisMarkerSize = new Array(81, 84);
				thisMarkerShadowSize = new Array(81, 84);
				thisZindex = (myPointDatas.length + 10);
			}
			else {
				thisMarkerImage = myPointDatas[i]["markerImage"];
				thisMarkerShadowImage = "googleMapsIconShadow.png";
				thisMarkerSize = new Array(28, 20);
				thisMarkerShadowSize = new Array(28, 20);
				thisZindex = (myPointDatas.length - i);
			}

			var image = new google.maps.MarkerImage(
						"layout/icons/" + thisMarkerImage,
						new google.maps.Size(thisMarkerSize[0], thisMarkerSize[1]),
						new google.maps.Point(0,0),
						new google.maps.Point(0, thisMarkerSize[1])
			);

			var shadow = new google.maps.MarkerImage(
						"layout/icons/" + thisMarkerShadowImage,
						new google.maps.Size(thisMarkerShadowSize[0], thisMarkerShadowSize[1]),
						new google.maps.Point(0, 0),
						new google.maps.Point(0, thisMarkerShadowSize[1])
			);

			var shape = {
				 // coord: [1, (thisMarkerSize[1] + 1 - 20), 1, thisMarkerSize[1], thisMarkerSize[0], thisMarkerSize[1], thisMarkerSize[0] , (thisMarkerSize[1] + 1 - 20)],
				coord: [1, 1, 1, 28, 20, 28, 20 , 1],
				type: "poly"
			};

			var myLatLng = new google.maps.LatLng(myPointDatas[i]["latitude"], myPointDatas[i]["longitude"]);

			marker[infWindowKey] = new google.maps.Marker({
				position: myLatLng,
				map: map,
				shadow: shadow,
				icon: image,
				shape: shape,
				title: myPointDatas[i]["title"],
				zIndex: thisZindex
			});

			// DEFINE INFO WINDOW
				var html		= new Array();
				var imageExists	= false;
				html[infWindowKey] = "";
				html[infWindowKey] += "<div class=\"myGmapInfo\">";
				// html[infWindowKey] += "  <p class='headline'>" + myPointDatas[i]["title"] + "</p>";
				html[infWindowKey] += "  <p class='headline'>" + myPointDatas[i]["header"] + "</p>";
				if(myPointDatas[i]["imagePath"] != "") {
				  imageExists = true;
				  // html[infWindowKey] += '  <img src="' + myPointDatas[i]["imagePath"] + '" ' + myPointDatas[i]["imageDimension"] + ' alt="' + myPointDatas[i]["title"] + '"/>';
				}

				html[infWindowKey] += '<table border="0" width="" cellpadding="0" cellspacing="0">';

				if(myPointDatas[i]["text"] != "") {
				  // html[infWindowKey] += "  <p class='text'>" + myPointDatas[i]["text"].replace(/, /g, ",<br />") + "</p>";
				  html[infWindowKey] += "  <tr><td><b>Vertreter:</b></td><td>" + myPointDatas[i]["text"] + "</td></tr>";
				}

				if(myPointDatas[i]["city"] != "") {
				  // html[infWindowKey] += "  <tr><td><b>PLZ:</b></td><td>" + myPointDatas[i]["city"] + "</td></tr>";
				}

				if(myPointDatas[i]["region_1"] != "") {
				  html[infWindowKey] += "  <tr><td><b>Bundesland:</b></td><td>" + myPointDatas[i]["region_1"] + "</td></tr>";
				}

				if(myPointDatas[i]["region_2"] != "") {
				  html[infWindowKey] += "  <tr><td><b>Region:</b></td><td>" + myPointDatas[i]["region_2"] + "</td></tr>";
				}

				if(myPointDatas[i]["region_3"] != "") {
				  html[infWindowKey] += "  <tr><td><b>Kreis:</b></td><td>" + myPointDatas[i]["region_3"] + "</td></tr>";
				}

				html[infWindowKey] += "  <tr><td><b>Longitude:</b></td><td>" + myPointDatas[i]["longitude"] + "</td></tr>";
				html[infWindowKey] += "  <tr><td><b>Latitude: </b></td><td>" + myPointDatas[i]["latitude"] + "</td></tr>";

				html[infWindowKey] += '</table>';

				html[infWindowKey] += "<div class='clear'></div>";
				html[infWindowKey] += "</div>";

				infowindow[infWindowKey] = new Array();
				infowindow[infWindowKey] = new google.maps.InfoWindow({
					content: html[infWindowKey]
				});

			// END DEFINE INFO WINDOW

			// SHOW INFO WINDOW
				google.maps.event.addListener(marker[infWindowKey], "click", function() {
					if(activeMarker != '') {
						infowindow[activeMarker].close();
					}
					try{
						infowindow[infWindowKey].open(map, marker[infWindowKey]);
					}
					catch(e){
						alert("error: " + e + " | i: " + i + " | k: " + k);
					}
					activeMarker = infWindowKey;
				});
				// createMarkerLinkInGmapSidebar(i, myPointDatas[i]["title"], marker[infWindowKey], imageExists);
			// END SHOW INFO WINDOW
		}

		/* BOF DEFINE DEFAULT VALUES */
			var myPointDatas	= new Array();

			var elementId = "myGoogleMapCanvas";
			var default_arrayKey	= 1;
			var default_lat			= "52.103138";
			var default_lon			= "7.622817";
			var default_zoom		= 12;

			var sidebarHtml		= "";              			/* HTML-Code für die gesamte Sidebar */
			var sidebarMarkers	= [];              			/* Array für die Marker */
			var sidebarCount	= 0;               			/* Zähler für die Marker (= Zeile in der Sidebar) */
			var marker			= new Array();
			var activeMarker	= '';
			var infowindow		= new Array();
		/* EOF DEFINE DEFAULT VALUES */

		/* BOF DEFINE POINT DATAS */
			myPointDatas[0] = new Array();
			myPointDatas[0]["latitude"] = default_lat;
			myPointDatas[0]["longitude"] = default_lon;
			myPointDatas[0]["title"] = "BURHAN CTR";
			myPointDatas[0]["header"] = "BURHAN CTR";
			myPointDatas[0]["text"] = "";
			myPointDatas[0]["city"] = "";
			myPointDatas[0]["region_1"] = "";
			myPointDatas[0]["region_2"] = "";
			myPointDatas[0]["region_3"] = "";
			myPointDatas[0]["imagePath"] = "";
		/* EOF DEFINE POINT DATAS */

		<?php
		// BOF WRITE GOOGLE POINTS
			echo $strGoogleMapPoints;
		// EOF WRITE GOOGLE POINTS

		?>

		/* BOF CREATE GOOGLE MAP */
			function initialize() {
				if (!document.getElementById(elementId)) {
					alert("Fehler: das Element mit der id "+ elementId+ " konnte nicht auf dieser Webseite gefunden werden!");
					return false;
				}
				else {

				}
			}

			var latlng = new google.maps.LatLng(myPointDatas[default_arrayKey]["latitude"], myPointDatas[default_arrayKey]["longitude"]);

			var myOptions = {
				zoom: default_zoom,
				center: latlng,
				panControl: true,
				zoomControl: true,
				navigationControl: true,
				mapTypeControl: true,
				scaleControl: true,
				overviewMapControl: true,
				streetViewControl: true,
				mapTypeId: google.maps.MapTypeId.TERRAIN
				/* ROADMAP zeigt die normalen Standard-2D-Kacheln von Google Maps an. */
				/* SATELLITE zeigt Fotokacheln an. */
				/* HYBRID zeigt eine Mischung aus Fotokacheln und eine Kachelebene für markante Merkmale an (Straßen, Ortsnamen). */
				/* TERRAIN zeigt Reliefkacheln für Erhebungen und GewÃ¤sser (Berge, Flüsse usw.) an. */
			};
			var map = new google.maps.Map(document.getElementById(elementId), myOptions);


			try{
				for (i = 0 ; i < myPointDatas.length ; i++) {
					if(myPointDatas[i]){
							setGmapMarkers(map, i);
						}
				}
			}
			catch(e){
				alert("error: " + e + " | i: " + i + " | k: " + k);
			}
		/* EOF CREATE GOOGLE MAP */

	// } catch(err) { alert(err); } finally {}
	</script>
	<!-- GOOGLE MAPS END -->

<script language="javascript" type="text/javascript">
	$(document).ready(function() {
		// $('body').attr('onload', 'initialize();');
	});
</script>
<?php $noMandatorySwitch = true; ?>
<?php $noUserSwitch = true; ?>
<?php require_once('inc/footerHTML.inc.php'); ?>