<?php
	require_once('inc/requires.inc.php');

	if(!$arrGetUserRights["exportOrders"]) {
		header('location: ' . PAGE_EXIT_LOCATION);
		exit;
	}

	$jswindowMessage = '';
	$warningMessage = '';
	$errorMessage = '';
	$successMessage = '';
	$infoMessage = '';


	if($_GET["downloadFile"] != "") {
		require_once('classes/downloadFile.class.php');
		$thisDownload = new downloadFile(DIRECTORY_EXPORT_FILES, $_GET["downloadFile"]);
		$errorMessage .= $thisDownload->startDownload();
	}

	$dbConnection = new DB_Connection();
	$db_open = $dbConnection->db_connect();

	$defaultORDER = "ordersBestellDatum";
	$defaultSORT = "ASC";

	// BOF READ VERTRETER
		$arrAgentDatas = getAgents();
		$arrAgentDatas2 = getAgents2();
	// EOF READ VERTRETER

	// BOF READ BESTELL ARTEN
		$arrOrderTypeDatas = getOrderTypes();
	// EOF READ BESTELL ARTEN

	// BOF READ BESTELL STATUS TYPEN
		$arrOrderStatusTypeDatas = getOrderStatusTypes();
	// EOF READ BESTELL STATUS TYPEN

	// BOF READ DOCUMENT TYPE DATAS
		$arrPaymentStatusTypeDatas = getPaymentStatusTypes();
	// EOF READ DOCUMENT TYPE DATAS
?>
<?php
	require_once('inc/headerHTML.inc.php');
	$thisTitle = "Rechnungen exportieren";
	if($_POST["exportOrdersVertreterID"] != "") {
		$thisTitle .= " - Vertreter ".$arrAgentDatas[$_POST["exportOrdersVertreterID"]]["customersFirmenname"];
	}
	$headerHTML = preg_replace("/{###TITLE###}/", strip_tags($thisTitle), $headerHTML);
	echo $headerHTML;

	require_once(FILE_MENUE_TOP);

	$sqlSortField = "";
	$sqlSortDirection = "";
	$sqlWhere = "";

	if($_POST["submitExportForm"] == 1 && (($_POST["exportDateStart"] != "" && $_POST["exportDateEnd"] != "") || $_POST["exportNumberStart"] != "")) {
		// BOF CREATE SQL FOR EXPORT INVOICES

		$where = "";

		if($_POST["exportNumberStart"] != ""){
			$where .= " AND REPLACE(`" . TABLE_ORDER_INVOICES . "`.`orderDocumentsNumber`, 'RE-', '') >= '" . preg_replace("/[A-Za-z\-]/", "", $_POST["exportNumberStart"]) . "' ";
		}
		else {
			$where .= " AND `" . TABLE_ORDER_INVOICES . "`.`orderDocumentsDocumentDate`
						BETWEEN '" . formatDate($_POST["exportDateStart"], "store") . "' AND '" . formatDate($_POST["exportDateEnd"], "store")."' ";
		}

		if($_POST["exportPaymentStatusType"] != "" && $_POST["exportPaymentStatusType"] != "UNPAID"){
			$where .= " AND `" . TABLE_ORDER_INVOICES . "`.`orderDocumentsStatus` = '" . $_POST["exportPaymentStatusType"] . "' ";
		}
		else if($_POST["exportPaymentStatusType"] = "UNPAID"){
			$where .= " AND (
							`" . TABLE_ORDER_INVOICES . "`.`orderDocumentsStatus` = '1'
							OR
							`" . TABLE_ORDER_INVOICES . "`.`orderDocumentsStatus` = '4'
							OR
							`" . TABLE_ORDER_INVOICES . "`.`orderDocumentsStatus` = '6'
							OR
							`" . TABLE_ORDER_INVOICES . "`.`orderDocumentsStatus` = '7'
							OR
							`" . TABLE_ORDER_INVOICES . "`.`orderDocumentsStatus` = '8'
							OR
							`" . TABLE_ORDER_INVOICES . "`.`orderDocumentsStatus` = '11'
							OR
							`" . TABLE_ORDER_INVOICES . "`.`orderDocumentsStatus` = '12'
						)
					";
		}

		$sql = "SELECT
					`" . TABLE_ORDER_INVOICES . "`.`orderDocumentsID`,
					`" . TABLE_ORDER_INVOICES . "`.`orderDocumentsNumber`,
					`" . TABLE_ORDER_INVOICES . "`.`orderDocumentsType`,
					`" . TABLE_ORDER_INVOICES . "`.`orderDocumentsOrdersIDs`,
					`" . TABLE_ORDER_INVOICES . "`.`orderDocumentsCustomerNumber`,
					`" . TABLE_ORDER_INVOICES . "`.`orderDocumentsCustomersOrderNumber`,
					`" . TABLE_ORDER_INVOICES . "`.`orderDocumentsProcessingDate`,
					`" . TABLE_ORDER_INVOICES . "`.`orderDocumentsDocumentDate`,
					`" . TABLE_ORDER_INVOICES . "`.`orderDocumentsOrderDate`,
					`" . TABLE_ORDER_INVOICES . "`.`orderDocumentsInvoiceDate`,
					`" . TABLE_ORDER_INVOICES . "`.`orderDocumentsCreditDate`,
					`" . TABLE_ORDER_INVOICES . "`.`orderDocumentsDeliveryDate`,
					`" . TABLE_ORDER_INVOICES . "`.`orderDocumentsBindingDate`,
					`" . TABLE_ORDER_INVOICES . "`.`orderDocumentsSalesman`,
					`" . TABLE_ORDER_INVOICES . "`.`orderDocumentsKommission`,
					`" . TABLE_ORDER_INVOICES . "`.`orderDocumentsAddressCompany`,
					`" . TABLE_ORDER_INVOICES . "`.`orderDocumentsAddressName`,
					`" . TABLE_ORDER_INVOICES . "`.`orderDocumentsAddressStreet`,
					`" . TABLE_ORDER_INVOICES . "`.`orderDocumentsAddressStreetNumber`,
					`" . TABLE_ORDER_INVOICES . "`.`orderDocumentsAddressZipcode`,
					`" . TABLE_ORDER_INVOICES . "`.`orderDocumentsAddressCity`,
					`" . TABLE_ORDER_INVOICES . "`.`orderDocumentsAddressCountry`,
					`" . TABLE_ORDER_INVOICES . "`.`orderDocumentsAddressMail`,
					`" . TABLE_ORDER_INVOICES . "`.`orderDocumentsSumPrice`,
					`" . TABLE_ORDER_INVOICES . "`.`orderDocumentsDiscount`,
					`" . TABLE_ORDER_INVOICES . "`.`orderDocumentsDiscountType`,
					`" . TABLE_ORDER_INVOICES . "`.`orderDocumentsDiscountPercent`,
					`" . TABLE_ORDER_INVOICES . "`.`orderDocumentsMwst`,
					`" . TABLE_ORDER_INVOICES . "`.`orderDocumentsMwstPrice`,
					`" . TABLE_ORDER_INVOICES . "`.`orderDocumentsShippingCosts`,
					`" . TABLE_ORDER_INVOICES . "`.`orderDocumentsPackagingCosts`,
					`" . TABLE_ORDER_INVOICES . "`.`orderDocumentsCashOnDelivery`,
					`" . TABLE_ORDER_INVOICES . "`.`orderDocumentsTotalPrice`,
					`" . TABLE_ORDER_INVOICES . "`.`orderDocumentsBankAccount`,
					`" . TABLE_ORDER_INVOICES . "`.`orderDocumentsPaymentCondition`,
					`" . TABLE_ORDER_INVOICES . "`.`orderDocumentsPaymentType`,
					`" . TABLE_ORDER_INVOICES . "`.`orderDocumentsSkonto`,
					`" . TABLE_ORDER_INVOICES . "`.`orderDocumentsDeliveryType`,
					`" . TABLE_ORDER_INVOICES . "`.`orderDocumentsDeliveryTypeNumber`,
					`" . TABLE_ORDER_INVOICES . "`.`orderDocumentsSubject`,
					`" . TABLE_ORDER_INVOICES . "`.`orderDocumentsContent`,
					`" . TABLE_ORDER_INVOICES . "`.`orderDocumentsDocumentPath`,
					`" . TABLE_ORDER_INVOICES . "`.`orderDocumentsTimestamp`,
					`" . TABLE_ORDER_INVOICES . "`.`orderDocumentsStatus`,
					`" . TABLE_ORDER_INVOICES . "`.`orderDocumentsSendMail`,


					`" . TABLE_ORDER_INVOICE_PAYMENTS . "`.`orderPaymentID`,
					`" . TABLE_ORDER_INVOICE_PAYMENTS . "`.`orderPaymentOrderID`,
					`" . TABLE_ORDER_INVOICE_PAYMENTS . "`.`orderPaymentOrderNumber`,
					`" . TABLE_ORDER_INVOICE_PAYMENTS . "`.`orderPaymentDate`,
					`" . TABLE_ORDER_INVOICE_PAYMENTS . "`.`orderPaymentValue`

					FROM `" . TABLE_ORDER_INVOICES . "`

					LEFT JOIN `" . TABLE_ORDER_INVOICE_PAYMENTS . "`
					ON(`" . TABLE_ORDER_INVOICES . "`.`orderDocumentsNumber` = `" . TABLE_ORDER_INVOICE_PAYMENTS . "`.`orderPaymentOrderNumber` )

					WHERE 1 " . $where . "

					ORDER BY `" . TABLE_ORDER_INVOICES . "`.`orderDocumentsNumber` ASC, `" . TABLE_ORDER_INVOICES . "`.`orderDocumentsProcessingDate` ASC

			";
			// EOF CREATE SQL FOR EXPORT INVOICES

			$rs = $dbConnection->db_query($sql);

			$countTotalRows = $dbConnection->db_getMysqlNumRows($rs);
			$contentExportPDF = '';
			$contentExportCSV = '';

			if($countTotalRows > 0) {


			$contentExportPDF = '
				<style type="text/css">
				<!--
					table {
						border: 1pt solid #CCCCCC;
						border-collapse:collapse;
					}
					td {
						padding: 2pt;
						border: 0.5pt solid #CCCCCC;
						text-align:left;
						font-size:6pt;
						font-weight:normal;
						color:#000;
						vertical-align:top;
					}
					th {
						text-align:left;
						font-size:7pt;
						font-weight:bold;
						background-color:#DDDDDD;
						color:#000;
						padding:0;
						border: 0.5pt solid #CCCCCC;
					}
					.rowColor {
						background-color:#DDD;
					}

					h1 {
						color:#000;
						font-size:11pt;
					}
				-->
				</style>
			';

			$contentExportPDF .= '<h1>Erstellte ' . strtoupper(MANDATOR) . '-Rechnungen von ' . $_POST["exportDateStart"] . ' bis ' . $_POST["exportDateEnd"] . '</h1>';

			$contentExportPDF .= '<table width="95%">';

			$contentExportPDF .= '<tr>';
			$contentExportPDF .= '<th>#</th>';
			$contentExportPDF .= '<th style="width:10pt">Kunden-Nr.</th>';
			$contentExportPDF .= '<th style="width:10pt">Waehrung</th>';
			$contentExportPDF .= '<th style="width:10pt">Rechnungs-Nr.</th>';
			$contentExportPDF .= '<th style="width:10pt">Betrag</th>';
			$contentExportPDF .= '<th style="width:10pt">Rechnung vom</th>';
			$contentExportPDF .= '<th style="width:10pt">Faelligkeit</th>';			
			$contentExportPDF .= '<th style="width:10pt">Steuersatz</th>';
			$contentExportPDF .= '<th style="width:10pt">SkontoTg</th>';			
			$contentExportPDF .= '<th style="width:10pt">Skonto %</th>';			
			$contentExportPDF .= '<th style="width:10pt">NettoTg</th>';	
			
			$contentExportPDF .= '<th style="width:10pt">Kunde</th>';
			$contentExportPDF .= '<th style="width:10pt">Name</th>';
			$contentExportPDF .= '<th style="width:10pt">Str.</th>';
			$contentExportPDF .= '<th style="width:10pt">Haus-Nr.</th>';
			$contentExportPDF .= '<th style="width:10pt">PLZ</th>';
			$contentExportPDF .= '<th style="width:10pt">Ort</th>';			
			$contentExportPDF .= '<th style="width:10pt">Land</th>';
			$contentExportPDF .= '<th style="width:10pt">Telefon</th>';			


			$contentExportPDF .= '<th>Mailadresse</th>';
			$contentExportPDF .= '<th style="width:10pt">Webseite</th>';
			$contentExportPDF .= '</tr>';

			$countRow = 0;
			while($ds = mysqli_fetch_assoc($rs)) {
				if($countRow == 0) {

				}

				$thisBackgroundColor = '#FFF';
				if($countRow%2 == 0) {
					$thisBackgroundColor = '#EEE';
				}

				$contentExportPDF .= '<tr style="background-color:'.$thisBackgroundColor.';">';
				$contentExportPDF .= '<td style="text-align:right;">';
				$contentExportPDF .= $countRow;
				$contentExportPDF .= '</td>';
				$contentExportPDF .= '<td style="text-align:right;">';
				$contentExportPDF .= $ds["orderDocumentsCustomerNumber"];
				$contentExportPDF .= '</td>';
				$contentExportPDF .= '<td>';
				$contentExportPDF .= 'EUR';
				$contentExportPDF .= '</td>';
				$contentExportPDF .= '<td>';
				$contentExportPDF .= $ds["orderDocumentsNumber"];
				$contentExportPDF .= '</td>';
				$contentExportPDF .= '<td style="text-align:right;">';
				$contentExportPDF .= number_format($ds["orderDocumentsTotalPrice"], 2, ',', '');
				$contentExportPDF .= '</td>';
				$contentExportPDF .= '<td style="text-align:right;">';
				$contentExportPDF .= formatDate($ds["orderDocumentsProcessingDate"], 'display');
				$contentExportPDF .= '</td>';
				$contentExportPDF .= '<td style="text-align:right;">';
				$contentExportPDF .= formatDate($ds["orderDocumentsCreditDate"], 'display');
				$contentExportPDF .= '</td>';
				$contentExportPDF .= '<td>';
				$contentExportPDF .= $ds["orderDocumentsMwst"];
				$contentExportPDF .= '</td>';				
				$contentExportPDF .= '<td>';
				$contentExportPDF .= '-';
				$contentExportPDF .= '</td>';				
				$contentExportPDF .= '<td>';
				$contentExportPDF .= '-';
				$contentExportPDF .= '</td>';
				$contentExportPDF .= '<td>';
				$contentExportPDF .= '-';
				$contentExportPDF .= '</td>';				
				$contentExportPDF .= '<td>';
				$contentExportPDF .= $ds["orderDocumentsAddressCompany"];
				$contentExportPDF .= '</td>';
				$contentExportPDF .= '<td>';
				$contentExportPDF .= $ds["orderDocumentsAddressName"];
				$contentExportPDF .= '</td>';
				$contentExportPDF .= '<td>';
				$contentExportPDF .= $ds["orderDocumentsAddressStreet"];
				$contentExportPDF .= '</td>';
				$contentExportPDF .= '<td>';
				$contentExportPDF .= $ds["orderDocumentsAddressStreetNumber"];
				$contentExportPDF .= '</td>';
				$contentExportPDF .= '<td>';
				$contentExportPDF .= $ds["orderDocumentsAddressZipCode"];
				$contentExportPDF .= '</td>';
				$contentExportPDF .= '<td>';
				$contentExportPDF .= $ds["orderDocumentsAddressCity"];
				$contentExportPDF .= '</td>';
				$contentExportPDF .= '<td>';
				$contentExportPDF .= $ds["orderDocumentsAddressCountry"];
				$contentExportPDF .= '</td>';
				$contentExportPDF .= '<td>';
				$contentExportPDF .= $ds["orderDocumentsAddressPhone"];
				$contentExportPDF .= '</td>';
				$contentExportPDF .= '<td>';
				$contentExportPDF .= $ds["orderDocumentsAddressMail"];
				$contentExportPDF .= '</td>';
				$contentExportPDF .= '<td>';
				$contentExportPDF .= $ds["orderDocumentsAddressWebsite"];
				$contentExportPDF .= '</td>';
				$contentExportPDF .= '</tr>';
				$countRow++;
			}
			$contentExportPDF .= '</table>';

			$pdfContentPage = $contentExportPDF;

			//echo $pdfContentPage;
			//exit;

			$pdfContentPage = removeUnnecessaryChars($pdfContentPage);
			$contentExportCSV = formatCSV($contentExportPDF);

			// BOF STORE CSV
			$thisCreatedCsvName = convertChars('export_Rechnungen') . '_' . $_POST["exportDateStart"] . '_' . $_POST["exportDateEnd"] . '.csv';
			if($_POST["exportNumberStart"] != '') {
				$thisCreatedCsvName = convertChars('export_Rechnungen') . '_ab_' . $_POST["exportNumberStart"] . '.csv';
			}
			if(file_exists(DIRECTORY_EXPORT_FILES . $thisCreatedCsvName)) {
				// chmod(DIRECTORY_EXPORT_FILES . $thisCreatedCsvName , "0777");
				unlink(DIRECTORY_EXPORT_FILES . $thisCreatedCsvName);
			}
			clearstatcache();
			$fp = fopen(DIRECTORY_EXPORT_FILES . $thisCreatedCsvName, 'w');
			fwrite($fp, stripslashes($contentExportCSV));
			fclose($fp);
			// EOF STORE CSV

			// BOF WRITE TEMP HTML FILE
			$thisCreatedPdfName = convertChars('export_Rechnungen') . '_' . $_POST["exportDateStart"] . '_' . $_POST["exportDateEnd"] . '.pdf';
			if($_POST["exportNumberStart"] != '') {
				$thisCreatedPdfName = convertChars('export_Rechnungen') . '_ab_' . $_POST["exportNumberStart"] . '.pdf';
			}
			$tempHtmlFileName = "_temp_".$thisCreatedPdfName.'.html';
			if(file_exists(DIRECTORY_EXPORT_FILES . $tempHtmlFileName)) {
				unlink(DIRECTORY_EXPORT_FILES . $tempHtmlFileName);
			}
			clearstatcache();
			$fp = fopen(DIRECTORY_EXPORT_FILES . $tempHtmlFileName, 'w');
			fwrite($fp, stripslashes($pdfContentPage));
			fclose($fp);
			// EOF WRITE TEMP HTML FILE

			// BOF createPDF
			require_once(DIRECTORY_HTML2PDF . 'html2pdf.class.php');

			/*
			if(file_exists(DIRECTORY_EXPORT_FILES . $thisCreatedPdfName)) {
				try {
					chmod(DIRECTORY_EXPORT_FILES . $thisCreatedPdfName , "0777");
					unlink(DIRECTORY_EXPORT_FILES . $thisCreatedPdfName);
				}
				catch (Exception $e) {}
			}
			clearstatcache();
			*/

			try {
				 $html2pdf = new HTML2PDF('P', 'A4', 'de', true, 'UTF-8', array(0, 0, 0, 0));
				// $html2pdf = new HTML2PDF('L', 'A4', 'de', true, 'UTF-8', array(22, 5, 5, 5));
		//$html2pdf = new HTML2PDF('L', 'A4', 'de', true, 'UTF-8', array(10, 10, 10, 10));
				// $html2pdf = new HTML2PDF('P', 'A4', 'de');
				// $html2pdf = new HTML2PDF('P', 'A4', 'de', true, 'UTF-8');
				// $html2pdf->setModeDebug();
				// $html2pdf->setDefaultFont('Arial');
				// $html2pdf->createIndex('Inhaltsverzeichnis', 12, 8, false, true, 2);

				if(file_exists(DIRECTORY_EXPORT_FILES . $thisCreatedPdfName)) {
					// chmod(DIRECTORY_EXPORT_FILES . $thisCreatedPdfName , "0777");
					unlink(DIRECTORY_EXPORT_FILES . $thisCreatedPdfName);
				}
				clearstatcache();

				// BOF READ TEMP HTML FILE
				if(file_exists(DIRECTORY_EXPORT_FILES . $tempHtmlFileName)) {
					$pdfContentPage = "";
					$fp = fopen(DIRECTORY_EXPORT_FILES . $tempHtmlFileName, 'r');
					$pdfContentPage = fread($fp, filesize(DIRECTORY_EXPORT_FILES . $tempHtmlFileName));
					fclose($fp);
					$pdfContentPage = stripslashes($pdfContentPage);
					unlink(DIRECTORY_EXPORT_FILES . $tempHtmlFileName);
				}
				clearstatcache();
				// EOF READ TEMP HTML FILE

				ob_start();
				echo $pdfContentPage;
				$contentPDF = ob_get_clean();

				$html2pdf->writeHTML($contentPDF, isset($_GET['vuehtml']));
				$html2pdf->Output($thisCreatedPdfName, 'F', DIRECTORY_EXPORT_FILES);

				if(file_exists($thisCreatedPdfName)) {
					unlink($thisCreatedPdfName);

					/*
					$sql_update = array();
					foreach($thisDocumentOrdersIDs as $thisKey => $thisValue) {
						$sql_update[] = " `ordersToDocumentsOrderID` = '" . $thisValue . "' ";
					}

					$sql = "UPDATE `" . TABLE_ORDERS_TO_DOCUMENTS . "`
								SET `ordersToDocumentsStatus` = 'created'
								WHERE ". implode(" OR ", $sql_update) . "
					";

					$rs = $dbConnection->db_query($sql);
					*/
				}
				clearstatcache();
			}
			catch(HTML2PDF_exception $e) {
				echo $e;
				exit;
			}
			$_POST["selectTemplate"] = "";

			if(file_exists(DIRECTORY_EXPORT_FILES . $thisCreatedPdfName)) {
				$successMessage .= 'Die PDF-Datei &quot;' . $thisCreatedPdfName . '&quot wurde generiert. ' . '<br />';
				$showPdfDownloadLink = true;
			}
			else {
				$errorMessage .= 'Die PDF-Datei &quot;' . $thisCreatedPdfName . '&quot konnte nicht generiert werden. ' . '<br />';
			}
			clearstatcache();
		// EOF createPDF
		}
		else {
			$errorMessage .= 'Es liegen keine Datens&auml;tze vor, die exportiert werden k&ouml;nnen. ' . '<br />';
		}
	}
?>

<div id="mainArea">
	<div id="mainContent">
		<?php require_once(FILE_MENUE_SIDEBAR); ?>
		<div id="contentArea">
			<a name="top"></a>
			<div id="contentAreaElements">
				<h1><?php if(SHOW_PAGE_TITLE_ICONS) { echo '<img src="' . PATH_ICONS_MENUE_TITLES . 'export.png" alt="" />'; } ?> <?php echo $thisTitle; ?></h1>

				<div id="searchFilterArea">
					<form name="formExportDatas" method="post" action="<?php echo $_SERVER["PHP_SELF"]; ?>">
					<table border="0" cellpadding="0" cellspacing="0" class="searchFilterContent">
						<tr>
							<td><b>Rechnungs-Datum </b></td>
							<td>
								<label for="exportDateStart">von:</label>
								<input type="text" name="exportDateStart" id="exportDateStart" maxlength="2" class="inputField_70" readonly="readonly" value="<?php if($_POST["exportDateStart"] != '') { echo $_POST["exportDateStart"]; } else { echo date ('d.m.Y'); } ?>" />
							</td>
							<td>
								<label for="exportDateEnd">bis:</label>
								<input type="text" name="exportDateEnd" id="exportDateEnd" maxlength="2" class="inputField_70" readonly="readonly" value="<?php if($_POST["exportDateEnd"] != '') { echo $_POST["exportDateEnd"]; } else { echo date ('d.m.Y'); } ?>" />
							</td>
							<td class="legendSeperator">&nbsp;</td>
							<td><b>oder Rechnungsnummer</b></td>
							<td>
								<label for="exportNumberStart">ab:</label>
								<input type="text" name="exportNumberStart" id="exportNumberStart" class="inputField_100" value="<?php echo $_POST["exportNumberStart"]; ?>" />
							</td>
							<td>
								<label for="exportPaymentStatusType">Status</label>
								<select name="exportPaymentStatusType"id="exportPaymentStatusType" class="inputSelect_120">
									<option value="">ALLE</option>
									<option value="UNPAID">ALLE unbezahlten</option>
								<?php
									if(!empty($arrPaymentStatusTypeDatas)){
										foreach($arrPaymentStatusTypeDatas as $thisKey => $thisValue){
											$selected = '';
											if($thisKey == $_POST["exportPaymentStatusType"]){
												$selected = ' selected="selected" ';
											}
											echo '<option value="' . $thisKey . '" ' . $selected . '>' . $thisValue["paymentStatusTypesName"] . '</option>';
										}
									}
								?>
								</select>
							</td>
							<td>
								<input type="hidden" name="editID" id="editID" value="" />
								<input type="submit" name="submitExport" class="inputButton0" value="Rechnungen exportieren" />
							</td>
						</tr>
					</table>
					<input type="hidden" name="submitExportForm" id="submitExportForm" value="1" />
					</form>
				</div>

				<div class="contentDisplay">
					<?php displayMessages(); ?>
					<?php
						if($_POST["submitExportForm"] == 1) {
							if($showPdfDownloadLink) {
								// echo '<p class="dataTableContent"><b>DOWNLOAD-LINK: </b><a href="'.DIRECTORY_EXPORT_FILES . $thisCreatedPdfName.'" target="_blank">'.$thisCreatedPdfName.'</a></p>';
								echo '<p class="dataTableContent"><b>DOWNLOAD-LINK: </b><img src="layout/icons/iconPDF.gif" height="16" width="16" alt="" /> <a href="?downloadFile=' . $thisCreatedPdfName.'">'.utf8_decode($thisCreatedPdfName).'</a></p>';
								echo '<p class="dataTableContent"><b>DOWNLOAD-LINK: </b><img src="layout/icons/iconEXCEL.gif" height="16" width="16" alt="" /> <a href="?downloadFile=' . $thisCreatedCsvName.'">'.utf8_decode($thisCreatedCsvName).'</a></p>';
							}
							echo '<hr />';
							// $_POST["editID"] = "";
						}
					?>
				</div>

			</div>
		</div>
		<div class="clear"></div>
	</div>
</div>
<script language="javascript" type="text/javascript">
	$(document).ready(function() {
		$(function() {
			$.datepicker.setDefaults($.datepicker.regional[""]);
			$('#exportDateStart').datepicker($.datepicker.regional["de"]);
			$('#exportDateEnd').datepicker($.datepicker.regional["de"]);
		});
	});
</script>
<?php require_once('inc/footerHTML.inc.php'); ?>