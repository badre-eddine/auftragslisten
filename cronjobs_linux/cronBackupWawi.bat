#!/bin/bash
echo off

# REM ---------------------------------------------------------------------------------------------------------------
# REM ---------------------------------------------------------------------------------------------------------------

# REM BOF SET NAME OF DATABASE
	mysqli_DATABASE_DB_NAME=burhanctrAuftragslisten
	mysqli_DATABASE_DB_USER=burhan
	mysqli_DATABASE_DB_PASS=burhan
	mysqli_DATABASE_DB_HOST=localhost
# REM EOF SET NAME OF DATABASE

# REM ---------------------------------------------------------------------------------------------------------------
# REM ---------------------------------------------------------------------------------------------------------------

# REM BOF SET ACTIONS
	ACTION_USE_CLOUD_STORAGE=false
	ACTION_BACKUP_DATABASE_COMPLETE=false
	ACTION_BACKUP_DATABASE_TABLES=true
	ACTION_BACKUP_FILES=true
	# REM MODE_BACKUP_FILES=update|new
	MODE_BACKUP_FILES=new
# REM EOF SET ACTIONS

# REM ---------------------------------------------------------------------------------------------------------------
# REM ---------------------------------------------------------------------------------------------------------------

# REM BOF SET ARCHIVE TYPES 7z | zip
	ARCHIVE_TYPE_DATABASE=7z
	ARCHIVE_TYPE_FILES=7z
# REM EOF SET ARCHIVE TYPES

# REM ---------------------------------------------------------------------------------------------------------------
# REM ---------------------------------------------------------------------------------------------------------------

# REM BOF SET TIMESTAMP AND PARAMS
	CRONJOB_SAVESTAMP_DATETIME=`date +'%Y-%m-%d %H:%M:%S'`

	CRONJOB_SAVESTAMP_HOUR=`date +'%H'`
	CRONJOB_SAVESTAMP_MINUTE=`date +'%M'`
	CRONJOB_SAVESTAMP_SECONDS=`date +'%S'`
	# REM ---------------------------------------------------------------------------------------------------------------
	CRONJOB_SAVESTAMP_YEAR=`date +'%Y'`
	CRONJOB_SAVESTAMP_MONTH=`date +'%m'`
	CRONJOB_SAVESTAMP_DAY=`date +'%d'`
	# REM ---------------------------------------------------------------------------------------------------------------
	# CRONJOB_SAVESTAMP="$CRONJOB_SAVESTAMP_YEAR"-"$CRONJOB_SAVESTAMP_MONTH"-"$CRONJOB_SAVESTAMP_DAY"_"$CRONJOB_SAVESTAMP_HOUR"-"$CRONJOB_SAVESTAMP_MINUTE"-"$CRONJOB_SAVESTAMP_SECONDS"
	CRONJOB_SAVESTAMP="$CRONJOB_SAVESTAMP_YEAR"-"$CRONJOB_SAVESTAMP_MONTH"-"$CRONJOB_SAVESTAMP_DAY"_"$CRONJOB_SAVESTAMP_HOUR"-"$CRONJOB_SAVESTAMP_MINUTE"
# REM EOF SET TIMESTAMP AND PARAMS

# REM ---------------------------------------------------------------------------------------------------------------
# REM ---------------------------------------------------------------------------------------------------------------

# REM BOF SET FILENAMES
	PATH_Auftragslisten=/var/www/Auftragslisten/
	CRONJOB_DIR_EXECUTE_FILES=/var/www/Auftragslisten/cronjobs_linux/
	CRONJOB_DIR_LOG_FILES="$CRONJOB_DIR_EXECUTE_FILES"logs/
	CRONJOB_DIR_TEMP_FILES="$CRONJOB_DIR_EXECUTE_FILES"tmp/
	CRONJOB_BACKUP_PATH=/var/www/_Auftragslisten_backup/

	CRONJOB_FILENAME_BACKUP_FILES=Auftragslisten_backup_"$CRONJOB_SAVESTAMP"_Files."$ARCHIVE_TYPE_FILES"
	CRONJOB_BASE_FILENAME_BACKUP_FILES=Auftragslisten_base_backup_Files."$ARCHIVE_TYPE_FILES"
	CRONJOB_FILENAME_BACKUP_DATABASE=Auftragslisten_backup_"$CRONJOB_SAVESTAMP"_Database."$ARCHIVE_TYPE_DATABASE"
	CRONJOB_FILENAME_BACKUP_TABLES=Auftragslisten_backup_"$CRONJOB_SAVESTAMP"_DatabaseTables."$ARCHIVE_TYPE_DATABASE"

	TEMP_CRONJOB_BACKUP_PATH=E:\Auftragslisten_backups\
	TEMP_CRONJOB_BACKUP_PATH_SQL_FILES="$TEMP_CRONJOB_BACKUP_PATH"tmp\

	PATH_CLOUD_BACKUP=C:\CLOUD_BACKUP\MagentaCLOUD\Auftragslisten\

	# REM -----------------------------------------------------------------------------------------------------------

	PATH_ZIP=7z
	PATH_PHP=php
	PATH_MYSQLDUMP=mysqldump
	PATH_MYSQL=mysql

	# REM -----------------------------------------------------------------------------------------------------------

	KEEP_BACKUP_FILES_DAY_LIMIT_CLOUD=-1
	FILE_TABLE_LIST="$CRONJOB_DIR_LOG_FILES"listBackUpTables.txt
# REM EOF SET FILENAMES

# REM ---------------------------------------------------------------------------------------------------------------
# REM ---------------------------------------------------------------------------------------------------------------

# REM BOF BACK UP MYSQL DATABASE ALL TABLES IN ONE FILE
	if [ "$ACTION_BACKUP_DATABASE_COMPLETE" == "true" ] ; then
		$PATH_MYSQL -u $mysqli_DATABASE_DB_USER -p$mysqli_DATABASE_DB_PASS $mysqli_DATABASE_DB_NAME -e "FLUSH TABLES WITH READ LOCK;"
		$PATH_MYSQLDUMP  --databases burhanctrAuftragslisten -hlocalhost -u $mysqli_DATABASE_DB_USER -p$mysqli_DATABASE_DB_PASS > "$CRONJOB_BACKUP_PATH""$CRONJOB_FILENAME_BACKUP_DATABASE".sql
		$PATH_MYSQL -u $mysqli_DATABASE_DB_USER -p$mysqli_DATABASE_DB_PASS $mysqli_DATABASE_DB_NAME -e "UNLOCK TABLES;"
		$PATH_ZIP a -t$ARCHIVE_TYPE_DATABASE "$CRONJOB_BACKUP_PATH""$CRONJOB_FILENAME_BACKUP_DATABASE" "$CRONJOB_BACKUP_PATH""$CRONJOB_FILENAME_BACKUP_DATABASE".sql -mx7
		rm $CRONJOB_BACKUP_PATH$CRONJOB_FILENAME_BACKUP_DATABASE.sql
	fi
# REM EOF BACK UP MYSQL DATABASE IN ONE FILE IN ONE FILE

# REM ---------------------------------------------------------------------------------------------------------------
# REM ---------------------------------------------------------------------------------------------------------------

# REM BOF BACK UP MYSQL DATABASE EACH TABLES IN ONE FILE
	if [ "$ACTION_BACKUP_DATABASE_TABLES" == "true" ] ; then
		# rm $CRONJOB_DIR_TEMP_FILES*.sql
		$PATH_MYSQL -u $mysqli_DATABASE_DB_USER -p$mysqli_DATABASE_DB_PASS $mysqli_DATABASE_DB_NAME -e "SHOW TABLES;" --skip-column-names > $FILE_TABLE_LIST
		$PATH_MYSQL -u $mysqli_DATABASE_DB_USER -p$mysqli_DATABASE_DB_PASS $mysqli_DATABASE_DB_NAME -e "FLUSH TABLES WITH READ LOCK;"

		# REM BOF LOCK ALL TABLES
		#	for line in `<$FILE_TABLE_LIST` do
		#		$PATH_MYSQL -u $mysqli_DATABASE_DB_USER -p$mysqli_DATABASE_DB_PASS $mysqli_DATABASE_DB_NAME -e "LOCK TABLES $line WRITE"
		#	done;

		# REM EOF LOCK ALL TABLES

		# REM BOF CREATE TABLE DUMPS
			for line in `<$FILE_TABLE_LIST`
			do
				$PATH_MYSQLDUMP -u $mysqli_DATABASE_DB_USER -p$mysqli_DATABASE_DB_PASS $mysqli_DATABASE_DB_NAME $line > "$CRONJOB_DIR_TEMP_FILES""$line".sql
			done;
		# REM BOF CREATE TABLE DUMPS

		# REM BOF UNLOCK ALL TABLES
			$PATH_MYSQL -u $mysqli_DATABASE_DB_USER -p$mysqli_DATABASE_DB_PASS $mysqli_DATABASE_DB_NAME -e "UNLOCK TABLES;"
		# REM EOF UNLOCK ALL TABLES

		$PATH_ZIP a -t$ARCHIVE_TYPE_DATABASE "$CRONJOB_DIR_TEMP_FILES""$CRONJOB_FILENAME_BACKUP_TABLES" "$CRONJOB_DIR_TEMP_FILES"*.sql -mx7
		cp "$CRONJOB_DIR_TEMP_FILES""$CRONJOB_FILENAME_BACKUP_TABLES" "$CRONJOB_BACKUP_PATH""$CRONJOB_FILENAME_BACKUP_TABLES"
		rm "$CRONJOB_DIR_TEMP_FILES""$CRONJOB_FILENAME_BACKUP_TABLES"
		rm "$CRONJOB_DIR_TEMP_FILES"*.sql
		rm "$FILE_TABLE_LIST"
	fi

# REM EOF BACK UP MYSQL DATABASE EACH TABLES IN ONE FILE


# REM ---------------------------------------------------------------------------------------------------------------
# REM ---------------------------------------------------------------------------------------------------------------

# REM BOF BACKUP AND ZIP FILES
	if [ "$ACTION_BACKUP_FILES" == "true" ] && [ "$MODE_BACKUP_FILES" == "new" ] ; then
		# >>"$CRONJOB_DIR_LOG_FILES"cronBackupAuftragslisten2.txt 2>&1
		$PATH_ZIP -t$ARCHIVE_TYPE_FILES a "$CRONJOB_BACKUP_PATH""$CRONJOB_FILENAME_BACKUP_FILES" "$PATH_Auftragslisten" -mx7
	elif [ "$ACTION_BACKUP_FILES" == "true" ] && [ "$MODE_BACKUP_FILES" == "update" ] ; then
		$PATH_ZIP -t$ARCHIVE_TYPE_FILES u "$CRONJOB_BACKUP_PATH""$CRONJOB_BASE_FILENAME_BACKUP_FILES" "$PATH_Auftragslisten" -ms=off -mx7
		cp "$CRONJOB_BACKUP_PATH""$CRONJOB_BASE_FILENAME_BACKUP_FILES" "$CRONJOB_BACKUP_PATH""$CRONJOB_FILENAME_BACKUP_FILES"
	fi
# REM EOF BACKUP AND ZIP FILES

# REM ---------------------------------------------------------------------------------------------------------------
# REM ---------------------------------------------------------------------------------------------------------------

# REM BOF DELETE OLDER BACKUP FILES ON LOCAL SERVER
	# REM KEEP_BACKUP_FILES_DAY_LIMIT_LOCAL=-50
	# REM forfiles /p $CRONJOB_BACKUP_PATH /s /m *.* /c "cmd /c del @path" /d $KEEP_BACKUP_FILES_DAY_LIMIT_LOCAL
# REM EOF DELETE OLDER BACKUP FILES ON LOCAL SERVER

# REM ---------------------------------------------------------------------------------------------------------------
# REM ---------------------------------------------------------------------------------------------------------------

# REM BOF COPY BACKUP FILES TO CLOUD STORAGE

	# REM BOF DELETE OLDER BACKUP FILES IN CLOUD
		if [ "$ACTION_USE_CLOUD_STORAGE" == "true" ] ; then
			find $PATH_CLOUD_BACKUP -mtime +10 -type f -delete
		fi
	# REM EOF DELETE OLDER BACKUP FILES IN CLOUD

	# REM BOF COPY BACKUP FILES TO CLOUD
		if [ "$ACTION_USE_CLOUD_STORAGE" == "true" ] && [ "$ACTION_BACKUP_FILES" == "true" ] ; then
			cp "$CRONJOB_BACKUP_PATH""$CRONJOB_FILENAME_BACKUP_FILES" "$PATH_CLOUD_BACKUP""$CRONJOB_FILENAME_BACKUP_FILES"
		fi
	# REM EOF COPY BACKUP FILES TO CLOUD

	# REM BOF COPY BACKUP DATABASE COMPLETE TO CLOUD
		if [ "$ACTION_USE_CLOUD_STORAGE" == "true" ] && [ "$ACTION_BACKUP_DATABASE_COMPLETE" == "true" ] ; then
			cp "$CRONJOB_BACKUP_PATH""$CRONJOB_FILENAME_BACKUP_DATABASE" "$PATH_CLOUD_BACKUP""$CRONJOB_FILENAME_BACKUP_DATABASE"
		fi
	# REM EOF COPY BACKUP DATABASE COMPLETE TO CLOUD

	# REM BOF COPY BACKUP DATABASE TABLES TO CLOUD
		if [ "$ACTION_USE_CLOUD_STORAGE" == "true" ] && [ "$ACTION_BACKUP_DATABASE_TABLES" == "true" ] ; then
			cp "$CRONJOB_BACKUP_PATH""$CRONJOB_FILENAME_BACKUP_TABLES" "$PATH_CLOUD_BACKUP$CRONJOB_FILENAME_BACKUP_TABLES"
		fi
	# REM EOF COPY BACKUP DATABASE TABLES TO CLOUD

# REM EOF COPY BACKUP FILES TO CLOUD STORAGE

# REM ---------------------------------------------------------------------------------------------------------------
# REM ---------------------------------------------------------------------------------------------------------------

>>$CRONJOB_DIR_LOG_FILEScronBackupAuftragslisten2.txt 2>&1