<?php

	#error_reporting(0);
	error_reporting(E_ALL & ~E_NOTICE);
	set_time_limit(0);

	DEFINE('BASEPATH_Auftragslisten', "/var/www/Auftragslisten/");
	#DEFINE('BASEPATH_CRONJOBS', $_SERVER['DOCUMENT_ROOT'] . "/");
	DEFINE('BASEPATH_CRONJOBS', BASEPATH_Auftragslisten . "cronjobs_linux/");

	require_once(BASEPATH_CRONJOBS . 'cronjobsConfig.inc.php');
	#require_once(BASEPATH_Auftragslisten . 'config/configFiles.inc.php');

	require_once(BASEPATH_Auftragslisten . 'config/configParams_' . strtolower(MANDATOR) . '.inc.php');
	require_once(BASEPATH_Auftragslisten . 'config/configBasic.inc.php');
	require_once(BASEPATH_Auftragslisten . 'config/configTables.inc.php');
	require_once(BASEPATH_Auftragslisten . 'inc/functions.inc.php');
	require_once(BASEPATH_Auftragslisten . 'classes/DB_Connection.class.php');
	require_once(BASEPATH_Auftragslisten . 'scriptPackages/htmlMimeMail/htmlMimeMail.php');
	require_once(BASEPATH_Auftragslisten . 'classes/createMail.class.php');

	require_once(BASEPATH_Auftragslisten . 'documents_' . MANDATOR . '/documentTemplates/templatesMail/template_BASIC.inc.php');
	require_once(BASEPATH_Auftragslisten . 'documents_' . MANDATOR . '/documentTemplates/templatesMail/template_TEXT_DATAS.inc.php');

	// BOF RUN CRONJOB 05:00 MORNING
	$todayDate = date("Y-m-d");
	$todayDateTime = date("Y-m-d H:i:s");
	$yesterdayDate = date("Y-m-d", mktime(0, 0, 0, date("m"), (date("d") - 1), date("Y")));

	DEFINE('FILENAME_LOGFILE', BASEPATH_CRONJOBS . "logs/" . MANDATOR . "_" . basename($_SERVER["PHP_SELF"]) . ".txt");

	if(file_exists(FILENAME_LOGFILE)){
		unlink(FILENAME_LOGFILE);
	}
	$fp_log = fopen(FILENAME_LOGFILE, "w");

	fwrite($fp_log, date("Y-m-d H:i:s") . ": MANDATOR : " . MANDATOR . " | getMandator : " . $getMandator . "\n");

	$dbConnection = new DB_Connection();
	$db_open = $dbConnection->db_connect();

	$thisMessage = '';

	// BOF GET MANDATORIES
		$arrMandatories = getMandatoriesShort();
	// EOF GET MANDATORIES

	// BOF GET OVERDUE PRODUCTIONS
		$sql_getOverdueProductions_TEMPLATE = "
				SELECT
						*,

						((`tempTable`.`ordersArtikelMenge` + `tempTable`.`ordersProductionsTransferOrdersProductOriginalQuantity_2`) - `tempTable`.`ordersProductionsTransferOrdersProductQuantity_2`) AS 'QUANTITY_CHECK'

						FROM (
							SELECT

								IF(
									/* BOF VK-ZAHLUNG EINGANGSDATUM */
										`" . TABLE_PAYMENT_TYPES . "`.`paymentTypesID` = '2', `" . TABLE_ORDER_CONFIRMATION_PAYMENTS . "`.`orderPaymentDate`,
									/* EOF VK-ZAHLUNG EINGANGSDATUM */

									IF(
										/* BOF NACHBESTELLUNG ODER REKLAMATION EINGANGSDATUM */
											`" . TABLE_ORDER_TYPES . "`.`orderTypesID` = '2' || `" . TABLE_ORDER_TYPES . "`.`orderTypesID` = '3', `" . TABLE_ORDERS . "`.`ordersBestellDatum`,
										/* EOF NACHBESTELLUNG ODER REKLAMATION EINGANGSDATUM */

										/* BOF NEUBESTELLUNG FREIGABE-DATUM */
											IF(
												`" . TABLE_ORDERS . "`.`ordersFreigabeDatum` != '0000-00-00', `" . TABLE_ORDERS . "`.`ordersFreigabeDatum`,
												`" . TABLE_ORDERS . "`.`ordersBestellDatum`
											)
										/* EOF NEUBESTELLUNG FREIGABE-DATUM */
									)
								) AS `DATUM_1`,
								
								IF(									
									`" . TABLE_PAYMENT_TYPES . "`.`paymentTypesID` = '2',
									IF(`salesmen`.`customersCompanyCountry` = '81' && `salesmen`.`customersTyp` IN(2, 3, 4),
										IF(
											`" . TABLE_ORDER_TYPES . "`.`orderTypesID` = '2' || `" . TABLE_ORDER_TYPES . "`.`orderTypesID` = '3', `common_ordersProcess`.`ordersBestellDatum`,
											IF(
												`common_ordersProcess`.`ordersFreigabeDatum` != '0000-00-00', `common_ordersProcess`.`ordersFreigabeDatum`,
												`common_ordersProcess`.`ordersBestellDatum`
											)
										),
										`bctr_orderConfirmationPayments`.`orderPaymentDate`
									),
										
									IF(
										/* BOF NACHBESTELLUNG ODER REKLAMATION EINGANGSDATUM */
											`" . TABLE_ORDER_TYPES . "`.`orderTypesID` = '2' || `" . TABLE_ORDER_TYPES . "`.`orderTypesID` = '3', `common_ordersProcess`.`ordersBestellDatum`,
										/* EOF NACHBESTELLUNG ODER REKLAMATION EINGANGSDATUM */

										/* BOF NEUBESTELLUNG FREIGABE-DATUM */
											IF(
												`common_ordersProcess`.`ordersFreigabeDatum` != '0000-00-00', `common_ordersProcess`.`ordersFreigabeDatum`,
												`common_ordersProcess`.`ordersBestellDatum`
											)
										/* EOF NEUBESTELLUNG FREIGABE-DATUM */
									)									
								) AS `DATUM_2`,

								`" . TABLE_ORDERS . "`.`ordersID`,
								`" . TABLE_ORDERS . "`.`ordersKundennummer`,
								`" . TABLE_ORDERS . "`.`ordersKundenName`,
								`" . TABLE_ORDERS . "`.`ordersKommission`,
								`" . TABLE_ORDERS . "`.`ordersInfo`,

								`" . TABLE_ORDERS . "`.`ordersArtikelNummer`,
								`" . TABLE_ORDERS . "`.`ordersArtikelBezeichnung`,
								`" . TABLE_ORDERS . "`.`ordersArtikelKategorieID`,
								SUBSTRING(`" . TABLE_ORDERS . "`.`ordersArtikelKategorieID`, 1, 3) AS `ordersArtikelGroup`,
								`" . TABLE_ORDERS . "`.`ordersArtikelMenge`,
								`" . TABLE_ORDERS . "`.`ordersPerExpress`,

								`" . TABLE_ORDER_CONFIRMATIONS . "`.`orderDocumentsNumber` AS `orderDocumentsNumber_AB`,
								`" . TABLE_ORDER_CONFIRMATIONS . "`.`orderDocumentsBankAccount` AS `orderDocumentsBankAccount_AB`,
								`" . TABLE_ORDER_CONFIRMATIONS . "`.`orderDocumentsPaymentCondition` AS `orderDocumentsPaymentCondition_AB`,
								`" . TABLE_ORDER_CONFIRMATIONS . "`.`orderDocumentsPaymentType` AS `orderDocumentsPaymentType_AB`,
								`" . TABLE_ORDER_CONFIRMATIONS . "`.`orderDocumentsStatus` AS `orderDocumentsStatus_AB`,
								`" . TABLE_ORDER_CONFIRMATIONS . "`.`orderDocumentsCustomerNumber` AS `orderDocumentsCustomerNumber_AB`,
								`" . TABLE_ORDER_CONFIRMATIONS . "`.`orderDocumentsSalesman` AS `orderDocumentsSalesman_AB`,
								`" . TABLE_ORDER_CONFIRMATIONS . "`.`orderDocumentsAddressCompanyCustomerNumber` AS `orderDocumentsAddressCompanyCustomerNumber_AB`,
								
								`" . TABLE_ORDER_INVOICES . "`.`orderDocumentsNumber` AS `orderDocumentsNumber_RE`,
								`" . TABLE_ORDER_INVOICES . "`.`orderDocumentsBankAccount` AS `orderDocumentsBankAccount_RE`,
								`" . TABLE_ORDER_INVOICES . "`.`orderDocumentsPaymentCondition` AS `orderDocumentsPaymentCondition_RE`,
								`" . TABLE_ORDER_INVOICES . "`.`orderDocumentsPaymentType` AS `orderDocumentsPaymentType_RE`,
								`" . TABLE_ORDER_INVOICES . "`.`orderDocumentsStatus` AS `orderDocumentsStatus_RE`,
								`" . TABLE_ORDER_INVOICES . "`.`orderDocumentsCustomerNumber` AS `orderDocumentsCustomerNumber_RE`,
								`" . TABLE_ORDER_INVOICES . "`.`orderDocumentsSalesman` AS `orderDocumentsSalesman_RE`,
								`" . TABLE_ORDER_INVOICES . "`.`orderDocumentsAddressCompanyCustomerNumber` AS `orderDocumentsAddressCompanyCustomerNumber_RE`,

								`" . TABLE_PAYMENT_STATUS_TYPES . "`.`paymentStatusTypesShortName`,
								`" . TABLE_PAYMENT_STATUS_TYPES . "`.`paymentStatusTypesName`,

								`" . TABLE_ORDER_STATUS_TYPES . "`.`orderStatusTypesShortName`,
								`" . TABLE_ORDER_STATUS_TYPES . "`.`orderStatusTypesName`,

								IF(`ordersproductionstransfer_1`.`ordersProductionsTransferOrdersProductQuantity` IS NULL, 0, `ordersproductionstransfer_1`.`ordersProductionsTransferOrdersProductQuantity`) AS `ordersProductionsTransferOrdersProductQuantity_1`,
								IF(`ordersproductionstransfer_1`.`ordersProductionsTransferOrdersProductOriginalQuantity` IS NULL, 0, `ordersproductionstransfer_1`.`ordersProductionsTransferOrdersProductOriginalQuantity`) AS `ordersProductionsTransferOrdersProductOriginalQuantity_1`,

								IF(`ordersproductionstransfer_2`.`ordersProductionsTransferOrdersProductQuantity` IS NULL, 0, `ordersproductionstransfer_2`.`ordersProductionsTransferOrdersProductQuantity`) AS `ordersProductionsTransferOrdersProductQuantity_2`,
								IF(`ordersproductionstransfer_2`.`ordersProductionsTransferOrdersProductOriginalQuantity` IS NULL, 0, `ordersproductionstransfer_2`.`ordersProductionsTransferOrdersProductOriginalQuantity`) AS `ordersProductionsTransferOrdersProductOriginalQuantity_2`,


								/* `" . TABLE_PRINT_TYPES . "`.`printTypesShortName`, */
								`" . TABLE_PRINT_TYPES . "`.`printTypesName`,

								/* `" . TABLE_ORDER_TYPES . "`.`orderTypesShortName`, */
								`" . TABLE_ORDER_TYPES . "`.`orderTypesName`,

								/* `" . TABLE_PAYMENT_TYPES . "`.`paymentTypesShortName`, */
								`" . TABLE_PAYMENT_TYPES . "`.`paymentTypesName`,

								/* `" . TABLE_PAYMENT_CONDITIONS . "`.`paymentConditionsShortName`, */
								`" . TABLE_PAYMENT_CONDITIONS . "`.`paymentConditionsName`,

								`" . TABLE_ORDER_CONFIRMATION_PAYMENTS . "`.`orderPaymentOrderNumber` AS `orderPaymentOrderNumber_AB`,
								`" . TABLE_ORDER_CONFIRMATION_PAYMENTS . "`.`orderPaymentDate` AS `orderPaymentDate_AB`,
								`" . TABLE_ORDER_CONFIRMATION_PAYMENTS . "`.`orderPaymentBankAccountID` AS `orderPaymentBankAccountID_AB`,
								
								`" . TABLE_ORDER_INVOICE_PAYMENTS . "`.`orderPaymentOrderNumber` AS `orderPaymentOrderNumber_RE`,
								`" . TABLE_ORDER_INVOICE_PAYMENTS . "`.`orderPaymentDate` AS `orderPaymentDate_RE`,
								`" . TABLE_ORDER_INVOICE_PAYMENTS . "`.`orderPaymentBankAccountID` AS `orderPaymentBankAccountID_RE`,
								
								`salesmen`.`customersID`,
								`salesmen`.`customersKundennummer`,
								`salesmen`.`customersFirmenname`,
								`salesmen`.`customersTyp`,
								`salesmen`.`customersCompanyCountry`,
								
								`" . TABLE_CUSTOMER_TYPES . "`.`customerTypesID`,
								`" . TABLE_CUSTOMER_TYPES . "`.`customerTypesName`,
								`" . TABLE_CUSTOMER_TYPES . "`.`customerTypesShortName`,
								
								`" . TABLE_COUNTRIES . "`.`countries_name_DE`,
								`" . TABLE_COUNTRIES . "`.`countries_iso_code_2`,
								`" . TABLE_COUNTRIES . "`.`countries_iso_code_3`								
								
							FROM `" . TABLE_ORDERS . "`

							LEFT JOIN `" . TABLE_PRODUCTIONS_TRANSFER . "` AS `ordersproductionstransfer_1`
							ON(`" . TABLE_ORDERS . "`.`ordersID` = `ordersproductionstransfer_1`.`ordersProductionsTransferOrdersID`)

							LEFT JOIN `" . TABLE_ORDER_TYPES . "`
							ON(`" . TABLE_ORDERS . "`.`ordersOrderType` = `" . TABLE_ORDER_TYPES . "`.`orderTypesID`)

							LEFT JOIN `" . TABLE_ORDER_STATUS_TYPES . "`
							ON(`" . TABLE_ORDERS . "`.`ordersStatus` = `" . TABLE_ORDER_STATUS_TYPES . "`.`orderStatusTypesID`)

							LEFT JOIN `" . TABLE_PRINT_TYPES . "`
							ON(`" . TABLE_ORDERS . "`.`ordersAdditionalCostsID` = `" . TABLE_PRINT_TYPES . "`.`printTypesID`)

							LEFT JOIN `" . TABLE_ORDER_CONFIRMATIONS_DETAILS . "` AS `orderconfirmationsdetails_1`
							ON(`" . TABLE_ORDERS . "`.`ordersID` = `orderconfirmationsdetails_1`.`orderDocumentDetailOrderID`)

							LEFT JOIN `" . TABLE_ORDER_CONFIRMATIONS . "`
							ON(`orderconfirmationsdetails_1`.`orderDocumentDetailDocumentID` = `" . TABLE_ORDER_CONFIRMATIONS . "`.`orderDocumentsID`)

							LEFT JOIN `" . TABLE_ORDER_CONFIRMATIONS_DETAILS . "` AS `orderconfirmationsdetails_2`
							ON(
								`orderconfirmationsdetails_1`.`orderDocumentDetailDocumentID` = `orderconfirmationsdetails_2`.`orderDocumentDetailDocumentID`

								AND (
									ABS((`orderconfirmationsdetails_1`.`orderDocumentDetailPos` - `orderconfirmationsdetails_2`.`orderDocumentDetailPos`)) = 1
									OR ((`orderconfirmationsdetails_1`.`orderDocumentDetailProductQuantity`) = (`orderconfirmationsdetails_2`.`orderDocumentDetailProductQuantity` * 10))
									OR ((`orderconfirmationsdetails_1`.`orderDocumentDetailProductQuantity` * 10) = (`orderconfirmationsdetails_2`.`orderDocumentDetailProductQuantity`))
								)
								AND `orderconfirmationsdetails_1`.`orderDocumentDetailPosType` = `orderconfirmationsdetails_2`.`orderDocumentDetailPosType`
							)													
							
							LEFT JOIN `" . TABLE_PRODUCTIONS_TRANSFER . "` AS `ordersproductionstransfer_2`
							ON(`orderconfirmationsdetails_2`.`orderDocumentDetailOrderID` = `ordersproductionstransfer_2`.`ordersProductionsTransferOrdersID`)

							LEFT JOIN `" . TABLE_ORDER_CONFIRMATION_PAYMENTS . "`
							ON(`" . TABLE_ORDER_CONFIRMATIONS . "`.`orderDocumentsNumber` = `" . TABLE_ORDER_CONFIRMATION_PAYMENTS . "`.`orderPaymentOrderNumber`)
							
							LEFT JOIN `" . TABLE_RELATED_DOCUMENTS . "`
							ON(`" . TABLE_ORDER_CONFIRMATIONS . "`.`orderDocumentsNumber` = `" . TABLE_RELATED_DOCUMENTS . "`.`relatedDocuments_AB`)
							
							LEFT JOIN `" . TABLE_CUSTOMERS . "` AS `salesmen`
							/* ON(`bctr_orderConfirmations`.`orderDocumentsSalesman` = `salesmen`.`customersID`) */
							ON(`bctr_orderConfirmations`.`orderDocumentsAddressCompanyCustomerNumber` = `salesmen`.`customersKundennummer`)
							
							LEFT JOIN `" . TABLE_COUNTRIES . "`
							ON(`salesmen`.`customersCompanyCountry` = `" . TABLE_COUNTRIES . "`.`countries_id`)
							
							LEFT JOIN `" . TABLE_CUSTOMER_TYPES . "`							
							ON(`" . TABLE_CUSTOMER_TYPES . "`.`customerTypesID` = `salesmen`.`customersTyp`)
							
							LEFT JOIN `" . TABLE_ORDER_INVOICES . "`
							ON(`" . TABLE_RELATED_DOCUMENTS . "`.`relatedDocuments_RE` = `" . TABLE_ORDER_INVOICES . "`.`orderDocumentsNumber`)
							
							LEFT JOIN `" . TABLE_ORDER_INVOICE_PAYMENTS . "`
							ON(`" . TABLE_ORDER_INVOICES . "`.`orderDocumentsNumber` = `" . TABLE_ORDER_INVOICE_PAYMENTS . "`.`orderPaymentOrderNumber`)

							LEFT JOIN `" . TABLE_PAYMENT_TYPES . "`
							ON(`" . TABLE_ORDER_CONFIRMATIONS . "`.`orderDocumentsPaymentType` = `" . TABLE_PAYMENT_TYPES . "`.`paymentTypesID`)

							LEFT JOIN `" . TABLE_PAYMENT_CONDITIONS . "`
							ON(`" . TABLE_ORDER_CONFIRMATIONS . "`.`orderDocumentsPaymentCondition` = `" . TABLE_PAYMENT_CONDITIONS . "`.`paymentConditionsID`)

							LEFT JOIN `" . TABLE_PAYMENT_STATUS_TYPES . "`
							ON(`" . TABLE_ORDER_CONFIRMATIONS . "`.`orderDocumentsStatus` = `" . TABLE_PAYMENT_STATUS_TYPES . "`.`paymentStatusTypesID`)

							WHERE 1
								/* AND `" . TABLE_ORDERS . "`.`ordersID` = '50840' */

								AND `" . TABLE_ORDERS . "`.`ordersStatus` = '2'
								AND `" . TABLE_PRINT_TYPES . "`.`printTypesID` != '10'
								AND `orderconfirmationsdetails_1`.`orderDocumentDetailPosType` = 'product'
								AND (
									`orderconfirmationsdetails_2`.`orderDocumentDetailPosType` = 'product'
									OR
									`orderconfirmationsdetails_2`.`orderDocumentDetailPosType` IS NULL
								)

							HAVING
								/* `DATUM_1` IS NOT NULL */
								`DATUM_2` IS NOT NULL
						) AS `tempTable`

						GROUP BY
							`tempTable`.`orderDocumentsNumber_AB`

						HAVING
							`QUANTITY_CHECK` > 0
			";

	if(!empty($arrMandatories)){
		$sql_getOverdueProductions = array();
		foreach(array_keys($arrMandatories) as $thisMandatory){
			$sql_getOverdueProductions[$thisMandatory] = $sql_getOverdueProductions_TEMPLATE;
			$sql_getOverdueProductions[$thisMandatory] = preg_replace("/`bctr_/", "`" . $thisMandatory . "_", $sql_getOverdueProductions[$thisMandatory]);
		}

		if(!empty($sql_getOverdueProductions)){
			$sql_getOverdueProductions = implode(" UNION ", $sql_getOverdueProductions);

			$xxx_sql_getOverdueProductions .= "
					ORDER BY 							
						`DATUM_2` ASC,
						`ordersArtikelKategorieID` ASC,
						`printTypesName` ASC
						/*
						`ordersKundennummer` ASC,
						`orderDocumentsNumber_AB` ASC
						*/
				";
				
			$sql_getOverdueProductions .= "
					ORDER BY 		
						`printTypesName` ASC,
						`DATUM_2` ASC,
						`ordersArtikelKategorieID` ASC
				";	
			#fwrite($fp_log, date("Y-m-d H:i:s") . ":  sql_getOverdueProductions : " . $sql_getOverdueProductions . "\n\n\n");
			$rs_getOverdueProductions = $dbConnection->db_query($sql_getOverdueProductions);
			if(mysqli_error($db_open)) {
				fwrite($fp_log, date("Y-m-d H:i:s") . ":  sql_getOverdueProductions : " . $sql_getOverdueProductions . "\n" . mysqli_error($db_open) . "\n\n\n");
			}
			
			$countItems = $dbConnection->db_getMysqlNumRows($rs_getOverdueProductions);

			$content = '';

			$content .= '
					<style type="text/css">
						#orderslist{font-size:11px;font-family:Arial;border:1px solid #333;border-collapse:collapse;}
						#orderslist th, #orderslist td{border:1px solid #333;vertical-align:top;}
						#orderslist th {background-color:#444;color:#EEE;border:1px solid #EEE;}
						#orderslist tr.headerRow {background-color:#666;color:#FFF;font-weight:bold;}
						hr {height:1px;border: 1px dotted #333;background-color:transparent;color:transparent;padding:0;margin:0}
						.ordersInfo {color:#C00;font-weight:bold;}
					</style>
				';
			$content .= '<div style="font-size:12px;font-family:Arial;width:500px;">';

			$content .= '<h1 style="font-size:12px;">&Uuml;berf&auml;llige Produktionen</h1>';
			$content .= '
					<p>
						Angezeigt werden Produktionen, die nicht als &quot;in Druck&quot; markiert sind.<br />
						Als Datum gilt bei Nachbestellungen das Eingangsdatum, bei Neubestellungen das Freigabedatum, bei Vorkasse das Zahldatum.<br /><br />
						Bei Vertretern im Inland gilt bei Nachbestellungen das Eingangsdatum, bei Neubestellungen das Freigabedatum, unabh&auml;ngig ob per Vorkasse oder nicht!<br /><br />
						<b>Stand: ' . formatDate($todayDateTime, 'display') . '
					</p>
				';
				
			if($countItems > 0){
				
				$content .= '<table id="orderslist" border="0" cellpadding="2" cellspacing="0">';

				$content .= '
					<tr>
						<th>#</th>
						<th>DATUM</th>
						<th>KNR / Firma</th>
						<th>Zahlungsdaten</th>
						<th>Artikel</th>
					</tr>
				';

				$countRow = 0;
				$thisMarker = '';
				while($ds_getOverdueProductions = mysqli_fetch_assoc($rs_getOverdueProductions)){

					foreach($ds_getOverdueProductions as $thisKey => $thisValue){
						#$ds_getOverdueProductions[$thisKey] = utf8_decode($thisValue);
					}

					$ds_getOverdueProductions["DATUM"] = $ds_getOverdueProductions["DATUM_2"];
					
					$thisBackgroundColor = '#FFF';
					if($countRow%2 == 0) {
						$thisBackgroundColor = '#EEE';
					}
					
					if($thisMarker != $ds_getOverdueProductions["printTypesName"]){
					#if($thisMarker != substr($ds_getOverdueProductions["DATUM"], 0, 7)){
						#$thisMarker = substr($ds_getOverdueProductions["DATUM"], 0, 7);
						$thisMarker = $ds_getOverdueProductions["printTypesName"];
						
						$content .= '<tr class="headerRow">';					
						$content .= '<td colspan="6">' . htmlentities($thisMarker) . '</td>';
						$content .= '</tr>';
					}

					$content .= '<tr style="background-color:' . $thisBackgroundColor . '">';

					$content .= '<td style="text-align:right;">';
					$content .= '<b>' . ($countRow + 1) . '.</b>';
					$content .= '</td>';

					$content .= '<td>';
					$content .= formatDate($ds_getOverdueProductions["DATUM"], 'display');
					#$content .= 'ID: ' . $ds_getOverdueProductions["ordersID"];
					$content .= '</td>';				

					$content .= '<td>';
					$content .= '<b>' . htmlentities($ds_getOverdueProductions["ordersKundennummer"]) . '</b><br />';
					$content .= htmlentities($ds_getOverdueProductions["ordersKundenName"]);
					if($ds_getOverdueProductions["ordersKommission"] != ''){
						$content .= '<br />Kom.: ' . htmlentities($ds_getOverdueProductions["ordersKommission"]);
					}
					if($ds_getOverdueProductions["ordersInfo"] != ''){
						$content .= '<br /><span class="ordersInfo"> ' . htmlentities($ds_getOverdueProductions["ordersInfo"]) . '</span>';
					}
					$content .= '</td>';

					$content .= '<td>';
					$content .= '<b>' . htmlentities($ds_getOverdueProductions["orderDocumentsNumber_AB"]) . '</b>';
					if($ds_getOverdueProductions["orderDocumentsNumber_RE"] != ''){
						$content .= '<br /><b>' . htmlentities($ds_getOverdueProductions["orderDocumentsNumber_RE"]) . '</b>';
					}
					$content .= '<br /><span style="white-space:nowrap;font-size:11px;">per ' . htmlentities($ds_getOverdueProductions["paymentTypesName"]) . ': ' . htmlentities($ds_getOverdueProductions["paymentStatusTypesName"]) . '</span>';
					
					if($ds_getOverdueProductions["customersKundennummer"] != $ds_getOverdueProductions["ordersKundennummer"]){
						$content .= '<hr />';
						$content .= '<b>' . htmlentities($ds_getOverdueProductions["customersKundennummer"]) . '</b><br />';
						$content .= '<b>' . htmlentities($ds_getOverdueProductions["customersFirmenname"]) . '</b><br />';			
						// $content .= '<b>' . htmlentities($ds_getOverdueProductions["customerTypesName"]) . '</b><br />';
						// $content .= '<b>' . htmlentities($ds_getOverdueProductions["countries_name_DE"]) . '</b>';		
					}				
					$content .= '</td>';

					$content .= '<td>';
					$thisOrdersArtikelBezeichnung = $ds_getOverdueProductions["ordersArtikelBezeichnung"];					
					$thisOrdersArtikelBezeichnung = wordwrap($thisOrdersArtikelBezeichnung, 60);
					$thisOrdersArtikelBezeichnung = explode("\n", $thisOrdersArtikelBezeichnung);
					$content .= htmlentities($ds_getOverdueProductions["ordersArtikelMenge"]) . ' ' . htmlentities($thisOrdersArtikelBezeichnung[0]);
					$content .= '<br />' . htmlentities($ds_getOverdueProductions["ordersArtikelNummer"]);
					$content .= '<br /><span class="ordersInfo">' . htmlentities($ds_getOverdueProductions["printTypesName"]) . '</span>';
					$content .= '</td>';

					$content .= '</tr>';

					$countRow++;
				}

				$content .= '</table>';
			}
			else {
				$content .= '<p>Es liegen keine Daten vor!</p>';
			}
			$content .= '</div>';

			$content = removeUnnecessaryChars($content);
		}		

		// BOF SEND MAIL

			// BOF CREATE SUBJECT
				$thisSubject = 'Produktionen noch nicht in Druck: ' . formatDate($todayDate, 'display');
			// EOF CREATE SUBJECT

			$generatedDocumentNumber = '';
			$arrAttachmentFiles = array();

			$documentType = 'SX';
			$selectMailtextTemplates = 'SX';			
			
			$arrRecipientMails = array();
			$arrRecipientMails[] = 'webmaster@burhan-ctr.de';
			$arrRecipientMails[] = 'grafik@burhan-ctr.de';
			$arrRecipientMails[] = 'info@burhan-ctr.de';
			$arrRecipientMails[] = 'engin.burhan@burhan-ctr.de';
			
			$selectCustomersRecipientMail = implode(";", $arrRecipientMails);
			
			$sendAttachedMailText = $content;
			$selectMailtextSender = 'webmaster@burhan-ctr.de';
			$createMail = new createMail(
								$thisSubject,													// TEXT:	MAIL_SUBJECT
								$documentType,													// STRING:	DOCUMENT TYPE
								$generatedDocumentNumber,										// STRING:	DOCUMENT NUMBER
								$arrAttachmentFiles,											// ARRAY:	ATTACHMENT FILES
								$selectCustomersRecipientMail,	 								// STRING:	RECIPIENTS
								$arrMailContentDatas[$selectMailtextTemplates],					// MAIL_TEXT_TEMPLATE
								$mailTemplate["html"],											// MAIL_BODY_TEMPLATE
								'',																// STRING:	ADDITIONAL TEXT
								$sendAttachedMailText,											// STRING:	SEND ATTACHED TEXT
								true,															// BOOLEAN:	SEND ATTACHED DOCUMENT
								DIRECTORY_MAIL_IMAGES,											// DIRECTORY_MAIL_IMAGES
								$selectMailtextSender											// STRING: SENDER
							);

			$createMailResult = $createMail->returnResult();
			$sendMailToClient = $createMailResult;

			if(!$sendMailToClient) {
				$thisMessage .= ' Die Mail konnte nicht an ' . $selectCustomersRecipientName . ' (' . $selectCustomersRecipientMail . ') versendet werden! ' . "\n";
			}
			else {
				$thisMessage .= ' Die Mail wurde an ' . $selectCustomersRecipientName . ' (' . $selectCustomersRecipientMail . ') versendet! ' . "\n";

				// BOF DELETE CREATED FILES
					if(file_exists(PATH_EXPORTED_FILES . $thisCreatedPdfName)) {
						unlink(PATH_EXPORTED_FILES . $thisCreatedPdfName);
					}
					if(file_exists(PATH_EXPORTED_FILES . $thisCreatedCsvName)) {
						unlink(PATH_EXPORTED_FILES . $thisCreatedCsvName);
					}
				// EOF DELETE CREATED FILES
			}

			fwrite($fp_log, date("Y-m-d H:i:s") . ': ' . $thisMessage . "-----\n");
			$thisMessage = '';
		// EOF SEND MAIL

	}

	// EOF GET OVERDUE PRODUCTIONS

	fwrite($fp_log, "ENDE: " . date("Y-m-d H:i:s") . "\n\n" . str_repeat("#", 100) . "\n");

	if($dbConnection){
		$dbConnection->db_close();
	}
	if($fp_log){
		fclose($fp_log);
	}
?>