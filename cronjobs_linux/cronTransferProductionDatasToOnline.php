<?php

	#error_reporting(0);
	error_reporting(E_ALL & ~E_NOTICE);
	set_time_limit(0);

	DEFINE('BASEPATH_Auftragslisten', "/var/www/Auftragslisten/");
	#DEFINE('BASEPATH_CRONJOBS', $_SERVER['DOCUMENT_ROOT'] . "/");
	DEFINE('BASEPATH_CRONJOBS', BASEPATH_Auftragslisten . "cronjobs_linux/");

	DEFINE('TEMP_TABLE_TRANSFER_EXTERNAL_ORDER_IDS', '_temp_ordersProductionsTransferExternalOrderIDs');

	require_once(BASEPATH_CRONJOBS . 'cronjobsConfig.inc.php');

	require_once(BASEPATH_Auftragslisten . 'config/configParams_' . strtolower(MANDATOR) . '.inc.php');
	require_once(BASEPATH_Auftragslisten . 'config/configBasic.inc.php');
	require_once(BASEPATH_Auftragslisten . 'config/configTables.inc.php');
	require_once(BASEPATH_Auftragslisten . 'inc/functions.inc.php');
	require_once(BASEPATH_Auftragslisten . 'classes/DB_Connection.class.php');

	// BOF DEFINE DB_CONNECTION
		DEFINE("DB_HOST_EXTERN_PRODUCTION", "mysql.b3-werbepartner.de");
		DEFINE("DB_NAME_EXTERN_PRODUCTION", "usr_web23_9");
		DEFINE("DB_USER_EXTERN_PRODUCTION", "web23");
		DEFINE("DB_PASSWORD_EXTERN_PRODUCTION", "ZutXxKSq");
	// EOF DEFINE DB_CONNECTION

	DEFINE ("PRODUCTION_SHEET_DIR", "production_sheets/");
	DEFINE ("PRODUCTION_SHEET_DIR_BASEPATH", "/html/productions.burhan-ctr.de/" . PRODUCTION_SHEET_DIR);
	DEFINE ("PRODUCTION_SHEET_DIR_URL", "productions.burhan-ctr.de/" . PRODUCTION_SHEET_DIR);

	DEFINE('FILENAME_LOGFILE', BASEPATH_CRONJOBS . "logs/" . MANDATOR . "_" . basename($_SERVER["PHP_SELF"]) . ".txt");

	if(file_exists(FILENAME_LOGFILE)){
		#unlink(FILENAME_LOGFILE);
	}
	$fp_log = fopen(FILENAME_LOGFILE, "w");

	fwrite($fp_log, date("Y-m-d H:i:s") . ": MANDATOR : " . MANDATOR . " | getMandator : " . $getMandator . "\n");

	$dbConnection = new DB_Connection();
	$db_open = $dbConnection->db_connect();

	if(constant("DB_HOST_EXTERN_PRODUCTION") != '' && constant("DB_NAME_EXTERN_PRODUCTION") != '' && constant("DB_USER_EXTERN_PRODUCTION") != '' && constant("DB_PASSWORD_EXTERN_PRODUCTION")) {
		$existsOnlineProduction = true;
	}
	else {
		$existsOnlineProduction = false;
	}

	fwrite($fp_log, date("Y-m-d H:i:s") . "\n");

	if($existsOnlineProduction){
		$dbConnection_ExternProduction = new DB_Connection(DB_HOST_EXTERN_PRODUCTION, '', DB_NAME_EXTERN_PRODUCTION, DB_USER_EXTERN_PRODUCTION, DB_PASSWORD_EXTERN_PRODUCTION);
		if(mysqli_error($db_openExternProduction)) { fwrite($fp_log, date("Y-m-d H:i:s") . ": " . mysqli_error($db_openExternProduction) . "\n"); }
		$db_openExternProduction = $dbConnection_ExternProduction->db_connect();
		if(mysqli_error($db_openExternProduction)) { fwrite($fp_log, date("Y-m-d H:i:s") . ": " . mysqli_error($db_openExternProduction) . "\n"); }

		// BOF GET EXPORT TRANSFER DATAS FROM Auftragslisten NOT IN ONLINE PRODUCTION
			$sql_local = "
					DROP TABLE IF EXISTS `" . TEMP_TABLE_TRANSFER_EXTERNAL_ORDER_IDS . "`
				";
			$rs_local = $dbConnection->db_query($sql_local);
			if(mysqli_error($db_open)) { fwrite($fp_log, date("Y-m-d H:i:s") . ": " . mysqli_error($db_open) . "\n"); }

			$sql_local = "
					CREATE TABLE IF NOT EXISTS `" . TEMP_TABLE_TRANSFER_EXTERNAL_ORDER_IDS . "` (
						`ordersProductionsTransferExternalOrderID` varchar(20) NOT NULL
					) ENGINE=MyISAM DEFAULT CHARSET=utf8;
				";
			$rs_local = $dbConnection->db_query($sql_local);
			if(mysqli_error($db_open)) { fwrite($fp_log, date("Y-m-d H:i:s") . ": " . mysqli_error($db_open) . "\n"); }

			$sql_local = "
				ALTER TABLE `" . TEMP_TABLE_TRANSFER_EXTERNAL_ORDER_IDS . "`
					ADD UNIQUE KEY `ordersProductionsTransferExternalOrderID` (`ordersProductionsTransferExternalOrderID`);
				";
			$rs_local = $dbConnection->db_query($sql_local);
			if(mysqli_error($db_open)) { fwrite($fp_log, date("Y-m-d H:i:s") . ": " . mysqli_error($db_open) . "\n"); }

			$sql_extern = "
							SELECT
								`ordersProductionsTransferOrdersID`
								FROM `" . TABLE_PRODUCTIONS_TRANSFER . "`
								WHERE 1
									/* DATE SELECT AND `ordersProductionsTransferProductionStatus` = '1' */
					";
			$rs_extern = $dbConnection_ExternProduction->db_query($sql_extern);
			if(mysqli_error($db_openExternProduction)) { fwrite($fp_log, date("Y-m-d H:i:s") . ": " . mysqli_error($db_openExternProduction) . "\n"); }

			while($ds_extern = mysqli_fetch_assoc($rs_extern)){
				$sql_local_insert = "
						INSERT IGNORE
							INTO `" . TEMP_TABLE_TRANSFER_EXTERNAL_ORDER_IDS . "` (
								`ordersProductionsTransferExternalOrderID`
							)
							VALUES (
								'" . $ds_extern["ordersProductionsTransferOrdersID"] . "'
							)
					";
				$rs_local_insert = $dbConnection->db_query($sql_local_insert);
				if(mysqli_error($db_open)) { fwrite($fp_log, date("Y-m-d H:i:s") . ": " . mysqli_error($db_open) . "\n"); }
			}

			$sql_local_compare = "
					SELECT
						`" . TABLE_PRODUCTIONS_TRANSFER . "`.`ordersProductionsTransferOrdersID`,
						`" . TEMP_TABLE_TRANSFER_EXTERNAL_ORDER_IDS . "`.`ordersProductionsTransferExternalOrderID`


						FROM `" . TABLE_PRODUCTIONS_TRANSFER . "`

						LEFT JOIN `" . TEMP_TABLE_TRANSFER_EXTERNAL_ORDER_IDS . "`
						ON(`" . TABLE_PRODUCTIONS_TRANSFER . "`.`ordersProductionsTransferOrdersID` = `" . TEMP_TABLE_TRANSFER_EXTERNAL_ORDER_IDS . "`.`ordersProductionsTransferExternalOrderID`)

						WHERE 1

						HAVING
							`" . TEMP_TABLE_TRANSFER_EXTERNAL_ORDER_IDS . "`.`ordersProductionsTransferExternalOrderID` IS NULL
				";
			$rs_local_compare = $dbConnection->db_query($sql_local_compare);
			if(mysqli_error($db_open)) { fwrite($fp_log, date("Y-m-d H:i:s") . ": " . mysqli_error($db_open) . "\n"); }

			while($ds_local_compare = mysqli_fetch_assoc($rs_local_compare)){
				if($db_openExternProduction){
					$sql_local_update = "
								UPDATE
									`" . TABLE_PRODUCTIONS_TRANSFER . "`
									SET
										`ordersProductionsTransferSuccess_MYSQL` = '0',
										`ordersProductionsTransferSuccess_FTP` = '0'
									WHERE 1
										AND `" . TABLE_PRODUCTIONS_TRANSFER . "`.`ordersProductionsTransferOrdersID` = '" . $ds_local_compare["ordersProductionsTransferOrdersID"] . "'
										AND `" . TABLE_PRODUCTIONS_TRANSFER . "`.`ordersProductionsTransferSuccess_MYSQL` != '0'
										AND `" . TABLE_PRODUCTIONS_TRANSFER . "`.`ordersProductionsTransferSuccess_FTP` != '0'
						";
					$rs_local_update = $dbConnection->db_query($sql_local_update);
					if(mysqli_error($db_open)) { fwrite($fp_log, date("Y-m-d H:i:s") . ": " . mysqli_error($db_open) . "\n"); }
				}
			}

			$sql_local = "
					DROP TABLE IF EXISTS `" . TEMP_TABLE_TRANSFER_EXTERNAL_ORDER_IDS . "`
				";
			$rs_local = $dbConnection->db_query($sql_local);
			if(mysqli_error($db_open)) { fwrite($fp_log, date("Y-m-d H:i:s") . ": " . mysqli_error($db_open) . "\n"); }
		// EOF GET EXPORT TRANSFER DATAS FROM Auftragslisten NOT IN ONLINE PRODUCTION

		// BOF EXPORT TRANSFER DATAS FROM Auftragslisten WITH STATUS = 'OF' TO ONLINE PRODUCTION
			$sql_local = "
				SELECT

					`" . TABLE_PRODUCTIONS_TRANSFER . "`.`ordersProductionsTransferID`,

					`" . TABLE_PRODUCTIONS_TRANSFER . "`.`ordersProductionsTransferOrdersID`,

					`" . TABLE_PRODUCTIONS_TRANSFER . "`.`ordersProductionsTransferDeliveryRedDot`,
					`" . TABLE_PRODUCTIONS_TRANSFER . "`.`ordersProductionsTransferDeliveryNoDPD`,
					/* `" . TABLE_PRODUCTIONS_TRANSFER . "`.`ordersProductionsTransferDeliveryContainerDate`, */

					`" . TABLE_PRODUCTIONS_TRANSFER . "`.`ordersProductionsTransferMailSubmitDateTime`,
					/*`" . TABLE_PRODUCTIONS_TRANSFER . "`.`ordersProductionsTransferDeliveryPrintDate`, */
					`" . TABLE_PRODUCTIONS_TRANSFER . "`.`ordersProductionsTransferMailLanguage`,
					`" . TABLE_PRODUCTIONS_TRANSFER . "`.`ordersProductionsTransferMailSubject`,
					`" . TABLE_PRODUCTIONS_TRANSFER . "`.`ordersProductionsTransferMailRecipient`,

					`" . TABLE_PRODUCTIONS_TRANSFER . "`.`ordersProductionsTransferLayoutFileName`,
					`" . TABLE_PRODUCTIONS_TRANSFER . "`.`ordersProductionsTransferLayoutFileSize`,
					`" . TABLE_PRODUCTIONS_TRANSFER . "`.`ordersProductionsTransferLayoutFileType`,

					`" . TABLE_PRODUCTIONS_TRANSFER . "`.`ordersProductionsTransferProductionFileName`,
					`" . TABLE_PRODUCTIONS_TRANSFER . "`.`ordersProductionsTransferProductionFileSize`,

					`" . TABLE_PRODUCTIONS_TRANSFER . "`.`ordersProductionsTransferOrdersCustomerNumber`,
					`" . TABLE_PRODUCTIONS_TRANSFER . "`.`ordersProductionsTransferOrdersCustomerName`,

					`" . TABLE_PRODUCTIONS_TRANSFER . "`.`ordersProductionsTransferOrdersPrintText`,
					`" . TABLE_PRODUCTIONS_TRANSFER . "`.`ordersProductionsTransferOrdersKomission`,

					`" . TABLE_PRODUCTIONS_TRANSFER . "`.`ordersProductionsTransferOrdersProductNumber`,
					`" . TABLE_PRODUCTIONS_TRANSFER . "`.`ordersProductionsTransferOrdersProductCategory`,
					`" . TABLE_PRODUCTIONS_TRANSFER . "`.`ordersProductionsTransferOrdersProductID`,
					
					`" . TABLE_PRODUCTIONS_TRANSFER . "`.`ordersProductionsTransferOrdersProductName`,
					`" . TABLE_PRODUCTIONS_TRANSFER . "`.`ordersProductionsTransferOrdersProductQuantity`,
					`" . TABLE_PRODUCTIONS_TRANSFER . "`.`ordersProductionsTransferOrdersProductOriginalQuantity`,

					`" . TABLE_PRODUCTIONS_TRANSFER . "`.`ordersProductionsTransferOrdersColorsQuantity`,
					`" . TABLE_PRODUCTIONS_TRANSFER . "`.`ordersProductionsTransferOrdersColorsNames`,
					`" . TABLE_PRODUCTIONS_TRANSFER . "`.`ordersProductionsTransferOrdersWithClearPaint`,

					`" . TABLE_PRODUCTIONS_TRANSFER . "`.`ordersProductionsTransferOrderRecipientCustomerNumber`,
					`" . TABLE_PRODUCTIONS_TRANSFER . "`.`ordersProductionsTransferOrderRecipientAddress`,

					`" . TABLE_PRODUCTIONS_TRANSFER . "`.`ordersProductionsTransferOrderSenderCustomerNumber`,
					`" . TABLE_PRODUCTIONS_TRANSFER . "`.`ordersProductionsTransferOrderSenderAddress`,

					`" . TABLE_PRODUCTIONS_TRANSFER . "`.`ordersProductionsTransferUserID`,

					/* `" . TABLE_PRODUCTIONS_TRANSFER . "`.`ordersProductionsTransferProductionPrinter`, */
					`" . TABLE_PRODUCTIONS_TRANSFER . "`.`ordersProductionsTransferProductionStatus`,
					/* `" . TABLE_PRODUCTIONS_TRANSFER . "`.`ordersProductionsTransferProductionTrackingNumbers`, */
					/* `" . TABLE_PRODUCTIONS_TRANSFER . "`.`ordersProductionsTransferProductionNotice`, */

					`" . TABLE_PRODUCTIONS_TRANSFER . "`.`ordersProductionsTransferProductionModified`,

					`" . TABLE_PRODUCTIONS_TRANSFER . "`.`ordersProductionsTransferMandatory`,

					`" . TABLE_PRODUCTIONS_TRANSFER . "`.`ordersProductionsTransferOrdersPrintType`,

					`" . TABLE_PRODUCTIONS_TRANSFER . "`.`ordersProductionsTransferUseNoNeutralPacking`,
					
					`" . TABLE_PRODUCTIONS_TRANSFER . "`.`ordersProductionsTransferPrintPlateNumber`,
					`" . TABLE_PRODUCTIONS_TRANSFER . "`.`ordersProductionsTransferNotice`,				
					`" . TABLE_PRODUCTIONS_TRANSFER . "`.`ordersProductionsTransferOrderType`				
					


				FROM `" . TABLE_PRODUCTIONS_TRANSFER . "`

				WHERE 1
					AND `" . TABLE_PRODUCTIONS_TRANSFER . "`.`ordersProductionsTransferProductionStatus` = '1'
					AND (
						`" . TABLE_PRODUCTIONS_TRANSFER . "`.`ordersProductionsTransferSuccess_MYSQL` != '1'
						OR
						`" . TABLE_PRODUCTIONS_TRANSFER . "`.`ordersProductionsTransferSuccess_FTP` != '1'
					)

			";

			$rs_local = $dbConnection->db_query($sql_local);
			if(mysqli_error($db_open)) { fwrite($fp_log, date("Y-m-d H:i:s") . ": " . mysqli_error($db_open) . "\n" . $sql_local . "\n"); }

			$sql_extern = "";
			while($ds_local = mysqli_fetch_assoc($rs_local)){
				$thisProductionSheetFilename = basename($ds_local["ordersProductionsTransferProductionFileName"]);
				$thisProductionSheetLocalPath = $ds_local["ordersProductionsTransferProductionFileName"];
				$thisProductionSheetLocalBasePath = BASEPATH_Auftragslisten . $thisProductionSheetLocalPath;
				$thisProductionSheetExternPath = PRODUCTION_SHEET_DIR . $thisProductionSheetFilename;
				$thisProductionSheetExternBasePath = PRODUCTION_SHEET_DIR_BASEPATH . $thisProductionSheetFilename;

				$ds_local["ordersProductionsTransferProductionFileName"] = $thisProductionSheetExternPath;

				foreach($ds_local as $ds_localKey => $ds_localvalue){
					$ds_local[$ds_localKey] = addslashes($ds_localvalue);
				}

				// BOF CREATE SQL FOR ONLINE PRODUCTION
				$sql_extern = "";
				$_old_sql_extern = "INSERT IGNORE INTO `" . TABLE_PRODUCTIONS_TRANSFER . "` (
									`" . implode("`, `", array_keys($ds_local)) . "`
									)
									VALUES (
										'" . implode("', '", array_values($ds_local)) . "'
									)
									ON DUPLICATE KEY UPDATE
										`ordersProductionsTransferProductionStatus` = '1';
					";

				$sql_extern = "REPLACE INTO `" . TABLE_PRODUCTIONS_TRANSFER . "` (
									`" . implode("`, `", array_keys($ds_local)) . "`
									)
									VALUES (
										'" . implode("', '", array_values($ds_local)) . "'
									);
					";

				$rs_extern = $dbConnection_ExternProduction->db_query($sql_extern);
				if(mysqli_error($db_openExternProduction)) { fwrite($fp_log, date("Y-m-d H:i:s") . ": " . mysqli_error($db_openExternProduction) . "\n"); }
				// EOF CREATE SQL FOR ONLINE PRODUCTION

				// BOF UPLOAD PRODUCTION SHEET
					$ftp_connect = ftp_connect(FTP_SERVER) or die(date("Y-m-d H:i:s") . ": Konnte keine FTP-Verbindung aufbauen" . "\n");
					fwrite($fp_log, date("Y-m-d H:i:s") . ": ftp_connect " . $ftp_connect . "\n");
					$rs_ftp = false;
					if($ftp_connect){
						
						$ftp_login = ftp_login ($ftp_connect, FTP_LOGIN, FTP_PASSWORD);
						
						if($ftp_login){
							
							// BOF TURN PASSIVE MODE ON
								ftp_pasv($ftp_connect, true);
							// EOF TURN PASSIVE MODE ON							
							
							#$fileListExtern = ftp_nlist($ftp_connect, PRODUCTION_SHEET_DIR_BASEPATH);

							// BOF DELETE EXISTING FILE
								if(ftp_size($ftp_connect, $thisProductionSheetExternBasePath) != -1){
									ftp_delete($ftp_connect, $thisProductionSheetExternBasePath);
								}
								fwrite($fp_log, date("Y-m-d H:i:s") . ": FILE " . $thisProductionSheetExternBasePath . " SIZE " . ftp_size($ftp_connect, $thisProductionSheetExternBasePath) . "\n");
							// EOF DELETE EXISTING FILE						

							$rs_ftp = ftp_put($ftp_connect, $thisProductionSheetExternBasePath, $thisProductionSheetLocalBasePath, FTP_BINARY);
							if(!$rs_ftp) {
								fwrite($fp_log, date("Y-m-d H:i:s") . ": rs_ftp " . $rs_ftp . "\n");
								fwrite($fp_log, date("Y-m-d H:i:s") . ": exists thisProductionSheetExternBasePath " . file_exists($thisProductionSheetExternBasePath) . "\n");
								fwrite($fp_log, date("Y-m-d H:i:s") . ": exists thisProductionSheetLocalBasePath " . file_exists($thisProductionSheetLocalBasePath) . "\n");
								fwrite($fp_log, date("Y-m-d H:i:s") . ": Kein Upload " . $thisProductionSheetFilename . "\n");							
							}
							else{
								// BOF DELETE LOCAL FILE AFTER FTP-UPLOAD AND UPDATE LOCAL PATH TO EXTERN PATH
									if(file_exists($thisProductionSheetLocalBasePath)){
										// unlink($thisProductionSheetLocalBasePath);
										fwrite($fp_log, date("Y-m-d H:i:s") . ": unlink " . $thisProductionSheetLocalBasePath . "\n");
									}
									$sql_updateLocalPath = "
											UPDATE `" . TABLE_PRODUCTIONS_TRANSFER . "`
													SET `ordersProductionsTransferProductionFileName` = '" . PRODUCTION_SHEET_DIR_URL . basename($thisProductionSheetLocalPath) . "'
												WHERE 1
													AND `ordersProductionsTransferOrdersID` = '" . $ds_local["ordersProductionsTransferOrdersID"] . "'
													AND `ordersProductionsTransferProductionFileName` = '" . $thisProductionSheetLocalPath . "'
										";
									fwrite($fp_log, date("Y-m-d H:i:s") . ": sql_updateLocalPath " . $sql_updateLocalPath . "\n");
								// EOF DELETE LOCAL FILE AFTER FTP-UPLOAD AND UPDATE LOCAL PATH TO EXTERN PATH
							}

							ftp_close($ftp_connect);
						}
						else {
							fwrite($fp_log, date("Y-m-d H:i:s") . ": ftp_login failed" . "\n");
						}
					}
					else {
						fwrite($fp_log, date("Y-m-d H:i:s") . ": ftp_connect failed" . "\n");
					}
				// EOF UPLOAD PRODUCTION SHEET

				// BOF SET TRANSFER MARKER IF SUCCESFULL
					if($rs_extern){
						$sql_common = "
							UPDATE `" . TABLE_PRODUCTIONS_TRANSFER . "`
								SET `ordersProductionsTransferSuccess_MYSQL` = '1'
							WHERE 1
								AND `ordersProductionsTransferOrdersID` = '" . $ds_local["ordersProductionsTransferOrdersID"] . "'
								AND `ordersProductionsTransferSuccess_MYSQL` != '1'
						";

						$rs_local_success = $dbConnection->db_query($sql_common);
						if(mysqli_error($db_open)) { fwrite($fp_log, date("Y-m-d H:i:s") . ": " . mysqli_error($db_open) . "\n"); }

						$rs_extern_success = $dbConnection_ExternProduction->db_query($sql_common);
						if(mysqli_error($db_openExternProduction)) { fwrite($fp_log, date("Y-m-d H:i:s") . ": " . mysqli_error($db_openExternProduction) . "\n"); }
					}
					if($rs_ftp){
						$sql_common = "
							UPDATE `" . TABLE_PRODUCTIONS_TRANSFER . "`
								SET `ordersProductionsTransferSuccess_FTP` = '1'
							WHERE 1
								AND `ordersProductionsTransferOrdersID` = '" . $ds_local["ordersProductionsTransferOrdersID"] . "'
								AND `ordersProductionsTransferSuccess_FTP` != '1'
						";

						$rs_local_success = $dbConnection->db_query($sql_common);
						if(mysqli_error($db_open)) { fwrite($fp_log, date("Y-m-d H:i:s") . ": " . mysqli_error($db_open) . "\n"); }

						$rs_extern_success = $dbConnection_ExternProduction->db_query($sql_common);
						if(mysqli_error($db_openExternProduction)) { fwrite($fp_log, date("Y-m-d H:i:s") . ": " . mysqli_error($db_openExternProduction) . "\n"); }
					}
				// EOF SET TRANSFER MARKER IF SUCCESFULL
			}
		// EOF EXPORT TRANSFER DATAS FROM Auftragslisten WITH STATUS = 'OF' TO ONLINE PRODUCTION

		// BOF IMPORT TRANSFER DATAS FROM ONLINE PRODUCTION TO Auftragslisten
			$sql_extern = "
					SELECT
						`" . TABLE_PRODUCTIONS_TRANSFER . "`.`ordersProductionsTransferID`,

						`" . TABLE_PRODUCTIONS_TRANSFER . "`.`ordersProductionsTransferOrdersID`,

						`" . TABLE_PRODUCTIONS_TRANSFER . "`.`ordersProductionsTransferDeliveryContainerDate`,
						`" . TABLE_PRODUCTIONS_TRANSFER . "`.`ordersProductionsTransferDeliveryPrintDate`,
						`" . TABLE_PRODUCTIONS_TRANSFER . "`.`ordersProductionsTransferProductionPrinter`,
						`" . TABLE_PRODUCTIONS_TRANSFER . "`.`ordersProductionsTransferProductionStatus`,
						`" . TABLE_PRODUCTIONS_TRANSFER . "`.`ordersProductionsTransferProductionTrackingNumbers`,

						`" . TABLE_PRODUCTIONS_TRANSFER . "`.`ordersProductionsTransferMailSubmitDateTime`,
						`" . TABLE_PRODUCTIONS_TRANSFER . "`.`ordersProductionsTransferProductionModified`,

						`" . TABLE_PRODUCTIONS_TRANSFER . "`.`ordersProductionsTransferTransportType`,
						
						`" . TABLE_PRODUCTIONS_TRANSFER . "`.`ordersProductionsTransferPrintPlateNumber`,
						`" . TABLE_PRODUCTIONS_TRANSFER . "`.`ordersProductionsTransferNotice`

					FROM `" . TABLE_PRODUCTIONS_TRANSFER . "`
					WHERE 1
						AND `" . TABLE_PRODUCTIONS_TRANSFER . "`.`ordersProductionsTransferProductionModified` > `" . TABLE_PRODUCTIONS_TRANSFER . "`.`ordersProductionsTransferMailSubmitDateTime`
						AND DATE_FORMAT(DATE_ADD(`ordersProductionsTransferProductionModified`, INTERVAL 10 DAY), '%Y-%m-%d') > NOW()
				";
			$rs_extern = $dbConnection_ExternProduction->db_query($sql_extern);

			if(mysqli_error($db_openExternProduction)) { fwrite($fp_log, date("Y-m-d H:i:s") . ": " . mysqli_error($db_openExternProduction) . "\n"); }

			while($ds_extern = mysqli_fetch_assoc($rs_extern)){
				$sql_local = "
					UPDATE
					`" . TABLE_PRODUCTIONS_TRANSFER . "`

						SET
							`" . TABLE_PRODUCTIONS_TRANSFER . "`.`ordersProductionsTransferDeliveryContainerDate` = '" . $ds_extern["ordersProductionsTransferDeliveryContainerDate"] . "',
							`" . TABLE_PRODUCTIONS_TRANSFER . "`.`ordersProductionsTransferDeliveryPrintDate` = '" . $ds_extern["ordersProductionsTransferDeliveryPrintDate"] . "',
							`" . TABLE_PRODUCTIONS_TRANSFER . "`.`ordersProductionsTransferProductionPrinter` = '" .  addslashes($ds_extern["ordersProductionsTransferProductionPrinter"]) . "',
							`" . TABLE_PRODUCTIONS_TRANSFER . "`.`ordersProductionsTransferProductionStatus` = '" . $ds_extern["ordersProductionsTransferProductionStatus"] . "',
							`" . TABLE_PRODUCTIONS_TRANSFER . "`.`ordersProductionsTransferProductionTrackingNumbers` = '" . $ds_extern["ordersProductionsTransferProductionTrackingNumbers"] . "',

							`" . TABLE_PRODUCTIONS_TRANSFER . "`.`ordersProductionsTransferProductionModified` = '" . $ds_extern["ordersProductionsTransferProductionModified"] . "',

							`" . TABLE_PRODUCTIONS_TRANSFER . "`.`ordersProductionsTransferTransportType` = '" . $ds_extern["ordersProductionsTransferTransportType"] . "',
														
							`" . TABLE_PRODUCTIONS_TRANSFER . "`.`ordersProductionsTransferNotice` = '" . addslashes($ds_extern["ordersProductionsTransferNotice"]) . "'

					WHERE 1
						AND `" . TABLE_PRODUCTIONS_TRANSFER . "`.`ordersProductionsTransferOrdersID` = '" . $ds_extern["ordersProductionsTransferOrdersID"] . "'
				";

				$rs_local = $dbConnection->db_query($sql_local);

				if(mysqli_error($db_open)) { fwrite($fp_log, date("Y-m-d H:i:s") . ": " . mysqli_error($db_open) . "\n"); }
			}
		// EOF IMPORT TRANSFER DATAS FROM ONLINE PRODUCTION TO Auftragslisten

	}
	
	// BOF READ_FILES_VIA_FTP / BOF DELETE PRINT PRODUCTION FILES OLDER THAN SPECIAL DATE
		$thisAction = '';
		if($thisAction == 'DELETE_OLD_PRINT_PRODUCTION_FILES'){
			
			#DEFINE ("PRODUCTION_SHEET_DIR", "production_sheets/");
			#DEFINE ("PRODUCTION_SHEET_DIR_BASEPATH", "/html/productions.burhan-ctr.de/" . PRODUCTION_SHEET_DIR);

			$fileDeleteInterval = 3; // MONTHS
			$dateFileToDelete = date("Y-m-d", mktime(0, 0, 0, (date('m') - $fileDeleteInterval), date('d'), date('Y')));

			// BOF DELETE LOCAL PRINT PRODUCTION FILES OLDER THAN SPECIAL DATE
				$sql_getFilesToDelete = "
						SELECT 
							`ordersProductionsTransferID`,
							`ordersProductionsTransferOrdersID`,
							`ordersProductionsTransferMailSubmitDateTime`,
							DATE_FORMAT(`ordersProductionsTransferMailSubmitDateTime`, '%Y-%m-%d') AS `ordersProductionsTransferMailSubmitDate`,
							`ordersProductionsTransferLayoutFileName`,
							`ordersProductionsTransferLayoutFileSize`,
							`ordersProductionsTransferLayoutFileType`,
							`ordersProductionsTransferProductionFileName`,
							`ordersProductionsTransferProductionFileSize`,
							`ordersProductionsTransferProductionStatus`
						
						FROM `" . TABLE_PRODUCTIONS_TRANSFER . "`
						
						WHERE 1
							AND DATE_FORMAT(`ordersProductionsTransferMailSubmitDateTime`, '%Y-%m-%d') < '" . $dateFileToDelete . "'
							AND `ordersProductionsTransferProductionStatus` IN('4', '6', '8', '9')
					";		
					
					
				$rs_getFilesToDelete = $dbConnection->db_query($sql_getFilesToDelete);
				
				$countFiles = $dbConnection->db_getMysqlNumRows($rs_getFilesToDelete);										
				
				$ftp_connect = false;
				$ftp_login = false;
				
				$countFileToDeleteLocal = 0;
				$countFileToDeleteExternal = 0;
					
				if($countFiles > 0){
					$ftp_connect = ftp_connect(FTP_SERVER) or die(date("Y-m-d H:i:s") . ": Konnte keine FTP-Verbindung aufbauen" . "\n");
					
					if($ftp_connect){
						$ftp_login = ftp_login ($ftp_connect, FTP_LOGIN, FTP_PASSWORD);
					}												
					
					while($ds_getFilesToDelete = mysqli_fetch_assoc($rs_getFilesToDelete)){												
						$thisFileToDeletePathLocal = $ds_getFilesToDelete["ordersProductionsTransferProductionFileName"];
						$thisFileToDeleteBasename = basename($ds_getFilesToDelete["ordersProductionsTransferProductionFileName"]);
						$thisFileToDeletePathExternal = PRODUCTION_SHEET_DIR_BASEPATH . $thisFileToDeleteBasename;
						
						#dd('thisFileToDeleteBasename');
						#dd('thisFileToDeletePathLocal');												
						#dd('thisFileToDeletePathExternal');
						
						if(file_exists($thisFileToDeletePathLocal)){												
							
							// BOF DELETE LOCAL FILE
								$infoMessage .= ' Die Datei ' . $thisFileToDeletePathLocal . ' existiert<br>';
								/*
								$rs_deleteLocalFile = unlink($thisFileToDeletePathLocal);
								if($rs_deleteLocalFile){
									echo ' Die Datei ' . $thisFileToDeletePathLocal . ' wurde entfernt!' . '<br />';
								}
								else {
									echo ' Die Datei ' . $thisFileToDeletePathLocal . ' konnte nicht entfernt werden!' . '<br />';
								}
								*/
								$countFileToDeleteLocal++;
							// EOF DELETE LOCAL FILE
							
							// BOF DELETE EXTERNAL FILE
								if($ftp_login){
									$rs_getFtpFileDate = ftp_mdtm($ftp_connect, $thisFileToDeletePathExternal);

									$thisFileDateTimeExternal = date("Y-m-d H:i:s", $rs_getFtpFileDate);
									$thisFileDateExternal = date("Y-m-d", $rs_getFtpFileDate);
									
									if($rs_getFtpFileDate != -1) {
										if($thisFileDateExternal < $dateFileToDelete){
											$infoMessage .= ' Die Datei ' . $thisFileToDeletePathExternal . ' wurde zuletzt ge&auml;ndert am: ' . $thisFileDateExternal . '<br />';
											$countFileToDelete++;
											
											/*
											$rs_deleteFtpFile = ftp_delete($ftp_connect, $thisFileToDeletePathExternal);
											if($rs_deleteFtpFile){
												echo ' Die Datei ' . $thisFileToDeletePathExternal . ' wurde entfernt!' . '<br />';
											}
											else {
												echo ' Die Datei ' . $thisFileToDeletePathExternal . ' konnte nicht entfernt werden!' . '<br />';
											}
											*/
											$countFileToDeleteExternal++;
										}	
									}
								}
							// EOF DELETE EXTERNAL FILE
						}
						flush();
						clearstatcache();
					}
					
					if($ftp_connect){
						$ftp_close = ftp_close($ftp_connect);
					}
				}
			// EOF DELETE LOCAL PRINT PRODUCTION FILES OLDER THAN SPECIAL DATE									
		}	
	// EOF READ_FILES_VIA_FTP / BOF DELETE PRINT PRODUCTION FILES OLDER THAN SPECIAL DATE
		
	// BOF CHECK PRODUCTION STATUS DE AND CHECK PRODUCTION STATUS TR
		/*
		// 4: verschickt
		// 6: storniert
		// 1: offen
		$sql_getStatusDifferences = "
				SELECT 		
					`" . TABLE_PRODUCTIONS_TRANSFER . "`.`ordersProductionsTransferID`,
					`" . TABLE_PRODUCTIONS_TRANSFER . "`.`ordersProductionsTransferOrdersID`
			
		
				FROM `" . TABLE_ORDERS . "` 
				
				INNER JOIN `" . TABLE_PRODUCTIONS_TRANSFER . "`
				ON(`" . TABLE_ORDERS . "`.`ordersID` = `" . TABLE_PRODUCTIONS_TRANSFER . "`.`ordersProductionsTransferOrdersID`)
						
				WHERE 1
					AND `" . TABLE_ORDERS . "`.`ordersStatus` = '4'
					AND `" . TABLE_PRODUCTIONS_TRANSFER . "`.`ordersProductionsTransferProductionStatus` != '4'
					AND `" . TABLE_PRODUCTIONS_TRANSFER . "`.`ordersProductionsTransferProductionStatus` != '6'
					AND `" . TABLE_PRODUCTIONS_TRANSFER . "`.`ordersProductionsTransferProductionStatus` != '1'
			";
		$rs_getStatusDifferences = $dbConnection->db_query($sql_getStatusDifferences);
		if(mysqli_error($db_open)) { fwrite($fp_log, date("Y-m-d H:i:s") . ": " . mysqli_error($db_open) . "\n"); }
		
		while($ds_getStatusDifferences = mysqli_fetch_assoc($rs_getStatusDifferences)){
			$sql_updateStatusDifferences_Extern = "
					UPDATE
						`" . TABLE_PRODUCTIONS_TRANSFER . "`
						SET `ordersProductionsTransferProductionStatus` = '4'
						
						WHERE 1
							AND `" . TABLE_PRODUCTIONS_TRANSFER . "`.`ordersProductionsTransferID` = '" . $ds_getStatusDifferences["ordersProductionsTransferID"] . "'
							AND `" . TABLE_PRODUCTIONS_TRANSFER . "`.`ordersProductionsTransferOrdersID` = '" . $ds_getStatusDifferences["ordersProductionsTransferOrdersID"] . "'						
				";
							
			#fwrite($fp_log, date("Y-m-d H:i:s") . ": " . $sql_updateStatusDifferences_Extern . "\n");
			$rs_updateStatusDifferences_Extern = $dbConnection_ExternProduction->db_query($sql_updateStatusDifferences_Extern);
			if(mysqli_error($db_openExternProduction)) { fwrite($fp_log, date("Y-m-d H:i:s") . ": " . mysqli_error($db_openExternProduction) . "\n"); }
		}
		*/
	// EOF CHECK PRODUCTION STATUS DE AND CHECK PRODUCTION STATUS TR	

	#if(mysqli_error($db_open)) { fwrite($fp_log, date("Y-m-d H:i:s") . ": " . mysqli_error($db_open) . "\n"); }
	fwrite($fp_log, date("Y-m-d H:i:s") . "ENDE" . "\n" . str_repeat("#", 10) . "\n\n\n");

	if($dbConnection_ExternProduction){
		$dbConnection_ExternProduction->db_close();
	}
	if($dbConnection){
		$dbConnection->db_close();
	}
	if($fp_log){
		fclose($fp_log);
	}
?>