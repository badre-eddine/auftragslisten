<?php
	#error_reporting(0);
	error_reporting(E_ALL & ~E_NOTICE);
	set_time_limit(0);

	if(!defined('MANDATOR')) {
		DEFINE('MANDATOR', 'bctr');
	}

	$showScanCodes = false; // false | true

	DEFINE('BASEPATH_Auftragslisten', "/var/www/Auftragslisten/");
	#DEFINE('BASEPATH_CRONJOBS', $_SERVER['DOCUMENT_ROOT'] . "/");
	DEFINE('BASEPATH_CRONJOBS', BASEPATH_Auftragslisten . "cronjobs_linux/");

	require_once(BASEPATH_CRONJOBS . 'cronjobsConfig.inc.php');

	#DEFINE('PATH_EXPORTED_FILES', DIRECTORY_CREATED_DOCUMENTS_SALESMEN);
	DEFINE('PATH_EXPORTED_FILES', BASEPATH_CRONJOBS . 'tmp/');
	DEFINE('DIRECTORY_HTML2PDF', BASEPATH_Auftragslisten . 'scriptPackages/html2pdf/');

	require_once(BASEPATH_Auftragslisten . 'config/configParams_' . strtolower(MANDATOR) . '.inc.php');
	require_once(BASEPATH_Auftragslisten . 'config/configBasic.inc.php');
	require_once(BASEPATH_Auftragslisten . 'config/configTables.inc.php');
	require_once(BASEPATH_Auftragslisten . 'inc/functions.inc.php');
	require_once(BASEPATH_Auftragslisten . 'classes/DB_Connection.class.php');
	require_once(BASEPATH_Auftragslisten . 'scriptPackages/htmlMimeMail/htmlMimeMail.php');
	require_once(BASEPATH_Auftragslisten . 'classes/createMail.class.php');

	require_once(BASEPATH_Auftragslisten . 'documents_' . MANDATOR . '/documentTemplates/templatesMail/template_BASIC.inc.php');
	require_once(BASEPATH_Auftragslisten . 'documents_' . MANDATOR . '/documentTemplates/templatesMail/template_TEXT_DATAS.inc.php');

	$todayDate = date("Y-m-d");
	$todayWeekDay = date("w");
	$todayWeek = date("W");
	$intervalDay = 10;
	$thisWeekStart = date("Y-m-d", mktime(0, 0, 0, date("m"), (date("d") - $intervalDay), date("Y")));
	$thisWeekEnd = date("Y-m-d");

	DEFINE('FILENAME_LOGFILE', BASEPATH_CRONJOBS . "logs/" . MANDATOR . "_" . basename($_SERVER["PHP_SELF"]) . ".txt");

	if(file_exists(FILENAME_LOGFILE)){
		#unlink(FILENAME_LOGFILE);
	}
	$fp_log = fopen(FILENAME_LOGFILE, "w");

	fwrite($fp_log, date("Y-m-d H:i:s") . ": MANDATOR : " . MANDATOR . " | getMandator : " . $getMandator . "\n");

	$dbConnection = new DB_Connection();
	$db_open = $dbConnection->db_connect();

	$thisMessage = '';

	
	// UPDATE `common_deliverydatas` SET `InfoMailGesendet` = '1' WHERE 1 AND DATE_FORMAT(`Versanddatum`, '%Y-%m') < '2016-08'
	// DELETE FROM `common_sendedmails` WHERE 1 AND `sendedMailsSubject` = 'DPD-Tracking-Nummern'		
		
	$arrSql_getTrackingNumbers = array(
		"WV" => "", // WIEDERVERKÄUFER
		"NC" => "" // NORMAL CUSTOMERS (WITHOUT WIEDERVERKÄUFER)
	);

	// BOF GET TRACKING NUMBERS FOR HV / WIEDERVERKÄUFER
		$arrSql_getTrackingNumbers["WV"] = "
				SELECT
						GROUP_CONCAT(DISTINCT `" . TABLE_CUSTOMERS . "`.`customersFirmenname`) AS `COMPANY_NAME`,
						GROUP_CONCAT(DISTINCT `" . TABLE_CUSTOMERS . "`.`customersMail1`) AS `MAIL_RECIPENT`,
						GROUP_CONCAT(DISTINCT `" . TABLE_CUSTOMERS . "`.`customersMail2`) AS `MAIL_RECIPENT2`,
						GROUP_CONCAT(DISTINCT `tempTable`.`Ref_Adresse_2`) AS `RECIPIENT_KNR`,
						GROUP_CONCAT(DISTINCT `tempTable`.`Firma_Absender`) AS `RECIPIENT_NAME`,
						GROUP_CONCAT(DISTINCT `tempTable`.`Ref_Adresse_1`) AS `DELIVERY_KNR`,
						`tempTable`.`SCAN_CODES`,
						COUNT(`tempTable`.`Paketnummer`) AS `PAKET_ANZAHL`,
						GROUP_CONCAT(`tempTable`.`Paketnummer` SEPARATOR '|') AS `PAKETNUMMERN`,
						GROUP_CONCAT(DISTINCT `tempTable`.`Referenznr_1` SEPARATOR '|') AS `REFERENZ`,
						GROUP_CONCAT(`tempTable`.`Paketnummer`,'#', `tempTable`.`Referenznr_1`, ' / ', `tempTable`.`Referenznr_2` SEPARATOR '|') AS `PAKETREFERENZ`,
						`tempTable`.`Firma_Empfaenger` AS `EMPFAENGER`,
						`tempTable`.`Adresse_1` AS `ANSCHRIFT`,
						CONCAT(`tempTable`.`PLZ_1`) AS `PLZ_1`,
						CONCAT(`tempTable`.`Stadt_1`) AS `ORT`,
						GROUP_CONCAT(DISTINCT `tempTable`.`EVENT_DATE` SEPARATOR '|') AS `DATUM`

					FROM (
						SELECT
							`" . TABLE_DELIVERY_DATAS . "`.`Paketnummer`,
							`" . TABLE_DELIVERY_DATAS . "`.`Datum`,
							`" . TABLE_DELIVERY_DATAS . "`.`Referenznr_1`,
							`" . TABLE_DELIVERY_DATAS . "`.`Referenznr_2`,
							`" . TABLE_DELIVERY_DATAS . "`.`Referenznr_3`,
							`" . TABLE_DELIVERY_DATAS . "`.`Referenznr_4`,
							`" . TABLE_DELIVERY_DATAS . "`.`Versanddatum`,
							`" . TABLE_DELIVERY_DATAS . "`.`Firma_Empfaenger`,
							`" . TABLE_DELIVERY_DATAS . "`.`Name_1`,
							`" . TABLE_DELIVERY_DATAS . "`.`Zu_Haenden`,
							`" . TABLE_DELIVERY_DATAS . "`.`Adresse_1`,
							`" . TABLE_DELIVERY_DATAS . "`.`Land_1`,
							`" . TABLE_DELIVERY_DATAS . "`.`Region_1`,
							`" . TABLE_DELIVERY_DATAS . "`.`PLZ_1`,
							`" . TABLE_DELIVERY_DATAS . "`.`Stadt_1`,
							`" . TABLE_DELIVERY_DATAS . "`.`Email_1`,
							`" . TABLE_DELIVERY_DATAS . "`.`Ref_Adresse_1`,
							`" . TABLE_DELIVERY_DATAS . "`.`Firma_Absender`,
							`" . TABLE_DELIVERY_DATAS . "`.`Name_2`,
							`" . TABLE_DELIVERY_DATAS . "`.`Adresse_Strasse_1`,
							`" . TABLE_DELIVERY_DATAS . "`.`Adresse_Strasse_2`,
							`" . TABLE_DELIVERY_DATAS . "`.`Land_2`,
							`" . TABLE_DELIVERY_DATAS . "`.`Region_2`,
							`" . TABLE_DELIVERY_DATAS . "`.`PLZ_2`,
							`" . TABLE_DELIVERY_DATAS . "`.`Stadt_2`,
							`" . TABLE_DELIVERY_DATAS . "`.`Email_2`,
							`" . TABLE_DELIVERY_DATAS . "`.`Ref_Adresse_2`,
							`" . TABLE_DELIVERY_DATAS . "`.`COD_Verwendungszweck`,
							`" . TABLE_DELIVERY_DATAS . "`.`COD_Betrag`,
							`" . TABLE_DELIVERY_DATAS . "`.`COD_Waehrung`,
							`" . TABLE_DELIVERY_DATAS . "`.`COD_Inkasoart`,
							`" . TABLE_DELIVERY_DATAS . "`.`Zustellung`,
							`" . TABLE_DELIVERY_DATAS . "`.`InfoMailGesendet`,

							`" . TABLE_DELIVERY_DATAS_STATUS_IMPORT . "`.`SOURCE_FILE`,
							`" . TABLE_DELIVERY_DATAS_STATUS_IMPORT . "`.`PARCELNO`,
							GROUP_CONCAT(DISTINCT `" . TABLE_DELIVERY_DATAS_STATUS_IMPORT . "`.`SCAN_CODE` ORDER BY `" . TABLE_DELIVERY_DATAS_STATUS_IMPORT . "`.`SCAN_CODE`) AS `SCAN_CODES`,
							`" . TABLE_DELIVERY_DATAS_STATUS_IMPORT . "`.`EVENT_DATE_TIME`,
							DATE_FORMAT(`EVENT_DATE_TIME`, '%Y-%m-%d') AS `EVENT_DATE`,
							`" . TABLE_DELIVERY_DATAS_STATUS_IMPORT . "`.`ROUTE`,
							`" . TABLE_DELIVERY_DATAS_STATUS_IMPORT . "`.`TOUR`,
							`" . TABLE_DELIVERY_DATAS_STATUS_IMPORT . "`.`CONSIGNEE_COUNTRY_CODE`,
							`" . TABLE_DELIVERY_DATAS_STATUS_IMPORT . "`.`CONSIGNEE_ZIP`,
							`" . TABLE_DELIVERY_DATAS_STATUS_IMPORT . "`.`CUSTOMER_REFERENCE`,
							`" . TABLE_DELIVERY_DATAS_STATUS_IMPORT . "`.`INFO_TEXT`

						FROM `" . TABLE_DELIVERY_DATAS . "`

						LEFT JOIN `" . TABLE_DELIVERY_DATAS_STATUS_IMPORT . "`
						ON(
							`" . TABLE_DELIVERY_DATAS . "`.`Paketnummer` = `" . TABLE_DELIVERY_DATAS_STATUS_IMPORT . "`.`PARCELNO`
							AND (
								`" . TABLE_DELIVERY_DATAS . "`.`Ref_Adresse_1` = `" . TABLE_DELIVERY_DATAS_STATUS_IMPORT . "`.`CUSTOMER_REFERENCE`
								OR `" . TABLE_DELIVERY_DATAS . "`.`PLZ_1` = `" . TABLE_DELIVERY_DATAS_STATUS_IMPORT . "`.`CONSIGNEE_ZIP`
							)
						)

						WHERE 1
							/* AND `" . TABLE_DELIVERY_DATAS . "`.`Zustellung` = ''	*/
							AND `" . TABLE_DELIVERY_DATAS . "`.`InfoMailGesendet` = '0'
							AND DATE_FORMAT(`" . TABLE_DELIVERY_DATAS_STATUS_IMPORT . "`.`EVENT_DATE_TIME`, '%Y-%m-%d') > DATE_FORMAT(`" . TABLE_DELIVERY_DATAS . "`.`Datum`, '%Y-%m-%d')

							AND `" . TABLE_DELIVERY_DATAS_STATUS_IMPORT . "`.`SCAN_CODE` IN ('02', '03', '05', '13', '14')
							AND `" . TABLE_DELIVERY_DATAS . "`.`Ref_Adresse_2` != ''
							AND `" . TABLE_DELIVERY_DATAS . "`.`Firma_Absender` NOT LIKE 'Burhan%'
							AND `" . TABLE_DELIVERY_DATAS . "`.`Firma_Absender` NOT LIKE 'B3%'

						GROUP BY  `" . TABLE_DELIVERY_DATAS . "`.`Paketnummer`
					) AS `tempTable`

					LEFT JOIN `" . TABLE_CUSTOMERS . "`
					ON(`tempTable`.`Ref_Adresse_2` = `" . TABLE_CUSTOMERS . "`.`customersKundennummer`)

					WHERE 1
						AND `tempTable`.`SCAN_CODES` != ''						

					GROUP BY CONCAT(`tempTable`.`Ref_Adresse_2`,`tempTable`.`Ref_Adresse_1`)

					ORDER BY
						`tempTable`.`Ref_Adresse_2` ASC,
						`tempTable`.`Ref_Adresse_1` ASC,
						`tempTable`.`Paketnummer` ASC
				";				
		// EOF GET TRACKING NUMBERS FOR HV / WIEDERVERKÄUFER
		
		// BOF GET TRACKING NUMBERS FOR NC / NORMAL CUSTOMERS (WITHOUT WIEDERVERKÄUFER)
			$arrSql_getTrackingNumbers["NC"] = "
					SELECT
								GROUP_CONCAT(DISTINCT `" . TABLE_CUSTOMERS . "`.`customersFirmenname`) AS `COMPANY_NAME`,
								GROUP_CONCAT(DISTINCT `" . TABLE_CUSTOMERS . "`.`customersMail1`) AS `MAIL_RECIPENT`,
								GROUP_CONCAT(DISTINCT `" . TABLE_CUSTOMERS . "`.`customersMail2`) AS `MAIL_RECIPENT2`,
								GROUP_CONCAT(DISTINCT `tempTable`.`Ref_Adresse_1`) AS `RECIPIENT_KNR`,
								GROUP_CONCAT(DISTINCT `tempTable`.`Firma_Empfaenger`) AS `RECIPIENT_NAME`,
								GROUP_CONCAT(DISTINCT `tempTable`.`Ref_Adresse_1`) AS `DELIVERY_KNR`,
								`tempTable`.`SCAN_CODES`,
								COUNT(`tempTable`.`Paketnummer`) AS `PAKET_ANZAHL`,
								GROUP_CONCAT(`tempTable`.`Paketnummer` SEPARATOR '|') AS `PAKETNUMMERN`,
								GROUP_CONCAT(DISTINCT `tempTable`.`Referenznr_1` SEPARATOR '|') AS `REFERENZ`,
								GROUP_CONCAT(`tempTable`.`Paketnummer`,'#', `tempTable`.`Referenznr_1`, ' / ', `tempTable`.`Referenznr_2` SEPARATOR '|') AS `PAKETREFERENZ`,
								`tempTable`.`Firma_Empfaenger` AS `EMPFAENGER`,
								`tempTable`.`Adresse_1` AS `ANSCHRIFT`,
								CONCAT(`tempTable`.`PLZ_1`) AS `PLZ_1`,
								CONCAT(`tempTable`.`Stadt_1`) AS `ORT`,
								GROUP_CONCAT(DISTINCT `tempTable`.`EVENT_DATE` SEPARATOR '|') AS `DATUM`

							FROM (
								SELECT
									`" . TABLE_DELIVERY_DATAS . "`.`Paketnummer`,
									`" . TABLE_DELIVERY_DATAS . "`.`Datum`,
									`" . TABLE_DELIVERY_DATAS . "`.`Referenznr_1`,
									`" . TABLE_DELIVERY_DATAS . "`.`Referenznr_2`,
									`" . TABLE_DELIVERY_DATAS . "`.`Referenznr_3`,
									`" . TABLE_DELIVERY_DATAS . "`.`Referenznr_4`,
									`" . TABLE_DELIVERY_DATAS . "`.`Versanddatum`,
									`" . TABLE_DELIVERY_DATAS . "`.`Firma_Empfaenger`,
									`" . TABLE_DELIVERY_DATAS . "`.`Name_1`,
									`" . TABLE_DELIVERY_DATAS . "`.`Zu_Haenden`,
									`" . TABLE_DELIVERY_DATAS . "`.`Adresse_1`,
									`" . TABLE_DELIVERY_DATAS . "`.`Land_1`,
									`" . TABLE_DELIVERY_DATAS . "`.`Region_1`,
									`" . TABLE_DELIVERY_DATAS . "`.`PLZ_1`,
									`" . TABLE_DELIVERY_DATAS . "`.`Stadt_1`,
									`" . TABLE_DELIVERY_DATAS . "`.`Email_1`,
									`" . TABLE_DELIVERY_DATAS . "`.`Ref_Adresse_1`,
									`" . TABLE_DELIVERY_DATAS . "`.`Firma_Absender`,
									`" . TABLE_DELIVERY_DATAS . "`.`Name_2`,
									`" . TABLE_DELIVERY_DATAS . "`.`Adresse_Strasse_1`,
									`" . TABLE_DELIVERY_DATAS . "`.`Adresse_Strasse_2`,
									`" . TABLE_DELIVERY_DATAS . "`.`Land_2`,
									`" . TABLE_DELIVERY_DATAS . "`.`Region_2`,
									`" . TABLE_DELIVERY_DATAS . "`.`PLZ_2`,
									`" . TABLE_DELIVERY_DATAS . "`.`Stadt_2`,
									`" . TABLE_DELIVERY_DATAS . "`.`Email_2`,
									`" . TABLE_DELIVERY_DATAS . "`.`Ref_Adresse_2`,
									`" . TABLE_DELIVERY_DATAS . "`.`COD_Verwendungszweck`,
									`" . TABLE_DELIVERY_DATAS . "`.`COD_Betrag`,
									`" . TABLE_DELIVERY_DATAS . "`.`COD_Waehrung`,
									`" . TABLE_DELIVERY_DATAS . "`.`COD_Inkasoart`,
									`" . TABLE_DELIVERY_DATAS . "`.`Zustellung`,
									`" . TABLE_DELIVERY_DATAS . "`.`InfoMailGesendet`,

									`" . TABLE_DELIVERY_DATAS_STATUS_IMPORT . "`.`SOURCE_FILE`,
									`" . TABLE_DELIVERY_DATAS_STATUS_IMPORT . "`.`PARCELNO`,
									GROUP_CONCAT(DISTINCT `" . TABLE_DELIVERY_DATAS_STATUS_IMPORT . "`.`SCAN_CODE` ORDER BY `" . TABLE_DELIVERY_DATAS_STATUS_IMPORT . "`.`SCAN_CODE`) AS `SCAN_CODES`,
									`" . TABLE_DELIVERY_DATAS_STATUS_IMPORT . "`.`EVENT_DATE_TIME`,
									DATE_FORMAT(`EVENT_DATE_TIME`, '%Y-%m-%d') AS `EVENT_DATE`,
									`" . TABLE_DELIVERY_DATAS_STATUS_IMPORT . "`.`ROUTE`,
									`" . TABLE_DELIVERY_DATAS_STATUS_IMPORT . "`.`TOUR`,
									`" . TABLE_DELIVERY_DATAS_STATUS_IMPORT . "`.`CONSIGNEE_COUNTRY_CODE`,
									`" . TABLE_DELIVERY_DATAS_STATUS_IMPORT . "`.`CONSIGNEE_ZIP`,
									`" . TABLE_DELIVERY_DATAS_STATUS_IMPORT . "`.`CUSTOMER_REFERENCE`,
									`" . TABLE_DELIVERY_DATAS_STATUS_IMPORT . "`.`INFO_TEXT`

								FROM `" . TABLE_DELIVERY_DATAS . "`

								LEFT JOIN `" . TABLE_DELIVERY_DATAS_STATUS_IMPORT . "`
								ON(
									`" . TABLE_DELIVERY_DATAS . "`.`Paketnummer` = `" . TABLE_DELIVERY_DATAS_STATUS_IMPORT . "`.`PARCELNO`
									AND (
										`" . TABLE_DELIVERY_DATAS . "`.`Ref_Adresse_1` = `" . TABLE_DELIVERY_DATAS_STATUS_IMPORT . "`.`CUSTOMER_REFERENCE`
										OR `" . TABLE_DELIVERY_DATAS . "`.`PLZ_1` = `" . TABLE_DELIVERY_DATAS_STATUS_IMPORT . "`.`CONSIGNEE_ZIP`
									)
								)

								WHERE 1
									
									AND `" . TABLE_DELIVERY_DATAS . "`.`InfoMailGesendet` = '0'
									AND DATE_FORMAT(`" . TABLE_DELIVERY_DATAS_STATUS_IMPORT . "`.`EVENT_DATE_TIME`, '%Y-%m-%d') > DATE_FORMAT(`" . TABLE_DELIVERY_DATAS . "`.`Datum`, '%Y-%m-%d')

									AND `" . TABLE_DELIVERY_DATAS_STATUS_IMPORT . "`.`SCAN_CODE` IN ('02', '03', '05', '13', '14')
									AND `" . TABLE_DELIVERY_DATAS . "`.`Ref_Adresse_1` != ''
									AND (
										`" . TABLE_DELIVERY_DATAS . "`.`Firma_Absender` LIKE 'Burhan%'
										OR `" . TABLE_DELIVERY_DATAS . "`.`Firma_Absender` LIKE 'B3%'
										OR `" . TABLE_DELIVERY_DATAS . "`.`Firma_Absender` = ''
									)
									AND `" . TABLE_DELIVERY_DATAS . "`.`Email_1` != ''

								GROUP BY  `" . TABLE_DELIVERY_DATAS . "`.`Paketnummer`
							) AS `tempTable`

							LEFT JOIN `" . TABLE_CUSTOMERS . "`
							ON(`tempTable`.`Ref_Adresse_1` = `" . TABLE_CUSTOMERS . "`.`customersKundennummer`)

							WHERE 1
								AND `tempTable`.`SCAN_CODES` != ''										

							GROUP BY CONCAT(`tempTable`.`Ref_Adresse_1`)

							ORDER BY
								
								`tempTable`.`Ref_Adresse_1` ASC,
								`tempTable`.`Paketnummer` ASC
			
				";
			#$arrSql_getTrackingNumbers["NC"] = "";
		// EOF GET TRACKING NUMBERS FOR NC / NORMAL CUSTOMERS (WITHOUT WIEDERVERKÄUFER)
	
		if(!empty($arrSql_getTrackingNumbers)){
			foreach($arrSql_getTrackingNumbers as $thisGetTrackingNumbersKey => $sql_getTrackingNumbers){
				if($sql_getTrackingNumbers != ''){
			
					fwrite($fp_log, date("Y-m-d H:i:s") . ":  thisGetTrackingNumbersKey : " . $thisGetTrackingNumbersKey . "\n");
				
					$sql_getTrackingNumbers = $arrSql_getTrackingNumbers[$thisGetTrackingNumbersKey];
					
					// BOF GET TRACKING NUMBERS
					
						// BOF SET group_concat_max_len
							// SET [GLOBAL | SESSION] group_concat_max_len = val;
							$sql_setGroupConcatMaxLen = "SET SESSION `group_concat_max_len` = 102400;";
							$rs_setGroupConcatMaxLen = $dbConnection->db_query($sql_setGroupConcatMaxLen);
							if(mysqli_error($db_open)) {
								fwrite($fp_log, date("Y-m-d H:i:s") . ":  sql_setGroupConcatMaxLen : " . $sql_setGroupConcatMaxLen . "\n" . "ERROR: " . mysqli_error($db_open) . "\n\n\n");
							}
						// EOF SET group_concat_max_len
						
						$rs_getTrackingNumbers = $dbConnection->db_query($sql_getTrackingNumbers);
						if(mysqli_error($db_open)) {
							fwrite($fp_log, date("Y-m-d H:i:s") . ":  sql_getTrackingNumbers : " . $sql_getTrackingNumbers . "\n" . "ERROR: " . mysqli_error($db_open) . "\n\n\n");
						}
						$countItems = $dbConnection->db_getMysqlNumRows($rs_getTrackingNumbers);

						#fwrite($fp_log, date("Y-m-d H:i:s") . ":  sql_getTrackingNumbers : " . $sql_getTrackingNumbers . "\n");

						if($countItems > 0){
							$arrMailAdresses = array();
							$arrDeliveryData = array();

							while($ds_getTrackingNumbers = mysqli_fetch_assoc($rs_getTrackingNumbers)){
								$arrMailAdresses[$ds_getTrackingNumbers["RECIPIENT_KNR"]] = array(
																								"MAIL" => trim($ds_getTrackingNumbers["MAIL_RECIPENT"]),
																								"NAME" => $ds_getTrackingNumbers["RECIPIENT_NAME"],
																								"COMPANY_NAME" => $ds_getTrackingNumbers["COMPANY_NAME"]
																							);
								if(trim($ds_getTrackingNumbers["MAIL_RECIPENT2"]) != ''){
									$arrMailAdresses[$ds_getTrackingNumbers["RECIPIENT_KNR"]]["MAIL"] .= ';' . trim($ds_getTrackingNumbers["MAIL_RECIPENT2"]);
								}
								$arrDeliveryData[$ds_getTrackingNumbers["RECIPIENT_KNR"]][$ds_getTrackingNumbers["DELIVERY_KNR"]] = $ds_getTrackingNumbers;
							}
						}
						else {
							fwrite($fp_log, "Keine Tracking-Nummern gefunden" . "\n");
							fwrite($fp_log, str_repeat('-', 50) . "\n");
						}
					// EOF GET TRACKING NUMBERS

					// BOF CREATE MAIL TEXT
						$countRow = 0;
						if(!empty($arrDeliveryData)){
							foreach($arrDeliveryData as $thisDeliveryDataKey => $arrDeliveryDatas){
								$arrSqlUpdateDataWhere = array();
								$countRow = 0;
								$thisRecipientMailAdress = trim($arrMailAdresses[$thisDeliveryDataKey]["MAIL"]);																
								$thisRecipientName = trim($arrMailAdresses[$thisDeliveryDataKey]["COMPANY_NAME"]);
								if(trim($arrMailAdresses[$thisDeliveryDataKey]["NAME"]) != trim($arrMailAdresses[$thisDeliveryDataKey]["COMPANY_NAME"])){
									$thisRecipientName .= ' | ' . trim($arrMailAdresses[$thisDeliveryDataKey]["NAME"]);
								}

								$thisMailTextHTML = '';
								$thisMailTextHTML .= '<div style="font-family:Arial;width:580px;">';

								$thisMailTextHTML .= '<p style="font-size:11px;"><b>' . $thisRecipientName . '</b> &bull; Ihre Kunden-Nr: <b>' . $thisDeliveryDataKey . '</b></p>';

								$thisMailTextHTML .= '<h2 style="font-size:12px;color:#333;">Paketdaten</h2>';

								$thisMailTextHTML .= '<table cellpadding="2" cellspacing="0" width="100%" border="1" style="font-size:12px;border:1px solid #333;border-collapse:collapse">';

								$thisMailTextHTML .= '<tr style="font-size:10px;border:1px solid #333;background-color:#5e5e5e;color:#EEE;">';

								$thisMailTextHTML .= '<th>#</th>';
								$thisMailTextHTML .= '<th>KNR</th>';
								$thisMailTextHTML .= '<th>FIRMA</th>';
								$thisMailTextHTML .= '<th>ANZ</th>';
								$thisMailTextHTML .= '<th colspan="2">PAKET-DATEN</th>';
								if($showScanCodes){
									$thisMailTextHTML .= '<th>SCAN-CODES</th>';
								}

								$thisMailTextHTML .= '</tr>';

								foreach($arrDeliveryDatas as $thisDeliveryDatasKey => $thisDeliveryDatasValue){

									if($countRow%2 == 0){ $rowColor = ''; }
									else { $rowColor = 'background-color:#EEE;'; }
									$thisMailTextHTML .= '<tr style="font-size:11px;border:1px solid #333;vertical-align:top;' . $rowColor . '">';

									$thisMailTextHTML .= '<td style="text-align:right;"><b>' . ($countRow + 1) . '.</b></td>';
									$thisMailTextHTML .= '<td><b>' . $thisDeliveryDatasValue["DELIVERY_KNR"] . '</b></td>';

									$thisMailTextHTML .= '<td>';
									$thisMailTextHTML .= '<b>' . htmlentities(($thisDeliveryDatasValue["EMPFAENGER"])) . '</b>' . '<br />';
									$thisMailTextHTML .= '<span style="white-space:nowrap">' . htmlentities(($thisDeliveryDatasValue["ANSCHRIFT"])) . '</span><br />';
									$thisMailTextHTML .= '<span style="white-space:nowrap">' . htmlentities(($thisDeliveryDatasValue["PLZ_1"])) . ' ' . htmlentities(($thisDeliveryDatasValue["ORT"])) . '</span><br />';
									$thisMailTextHTML .= '</td>';

									$thisMailTextHTML .= '<td style="text-align:right;">' . $thisDeliveryDatasValue["PAKET_ANZAHL"] . '</td>';

									$thisMailTextHTML .= '<td>';
									$arrThisPAKETREFERENZ = explode("|", $thisDeliveryDatasValue["PAKETREFERENZ"]);

									// $thisMailTextHTML .= '<b>Datum:</b> ' . formatDate($thisDeliveryDatasValue["DATUM"], 'display');

									$thisMailTextHTML .= '<ul style="list-style:numeric;margin:0;padding:0 0 0 20px">';

									foreach($arrThisPAKETREFERENZ as $thisPAKETREFERENZ){
										$arrThisPaketData = explode("#", $thisPAKETREFERENZ);

										$thisPaketDataTrackingNumber = $arrThisPaketData[0];
										$thisPaketDataTrackingUrl = getTrackingUrl('dpd', $thisPaketDataTrackingNumber);

										$thisPaketDataContent = $arrThisPaketData[1];
										// BOF TRANSLATE TR TO DE
											$thisPaketDataContent = translateDeliveryData($thisPaketDataContent, "TR", "DE");
											$thisPaketDataContent = transliterateTurkishChars($thisPaketDataContent, false);
										// EOF TRANSLATE TR TO DE
										$thisPaketDataContent = htmlentities(($thisPaketDataContent));

										$thisMailTextHTML .= '<li style="border-bottom:1px dotted #CCC;">';
										$thisMailTextHTML .= '<a href="' . $thisPaketDataTrackingUrl . '" style="text-decoration:none;color:#009;"><b>' . $thisPaketDataTrackingNumber . '</b></a>';
										$thisMailTextHTML .= ': <br />';

										$thisMailTextHTML .= '<i>' . $thisPaketDataContent . '</i>';
										$thisMailTextHTML .= '</li>';

										// BOF CREATE SQL UPDATE TRACKING
											if($thisGetTrackingNumbersKey == "WV"){
												$sql_updateTrackingNumberWhere = "
														(
															`Ref_Adresse_2` = '" . $thisDeliveryDatasValue["RECIPIENT_KNR"] . "'
															AND `Ref_Adresse_1` = '" . $thisDeliveryDatasValue["DELIVERY_KNR"] . "'
															AND `Paketnummer` = '" . $thisPaketDataTrackingNumber . "'
														)
													";
											}
											else if($thisGetTrackingNumbersKey == "NC"){
												$sql_updateTrackingNumberWhere = "
														(
															`Ref_Adresse_1` = '" . $thisDeliveryDatasValue["RECIPIENT_KNR"] . "'
															AND `Ref_Adresse_1` = '" . $thisDeliveryDatasValue["DELIVERY_KNR"] . "'
															AND `Paketnummer` = '" . $thisPaketDataTrackingNumber . "'
														)
													";
											}
											$sql_updateTrackingNumberWhere = cleanSQL($sql_updateTrackingNumberWhere) . "\n";

											$arrSqlUpdateDataWhere[$thisDeliveryDataKey][] = $sql_updateTrackingNumberWhere;
										// EOF CREATE SQL UPDATE TRACKING
									}

									$thisMailTextHTML .= '</ul>';

									$thisMailTextHTML .= '<td style="text-align:center;">';
									if(preg_match("/13/", $thisDeliveryDatasValue["SCAN_CODES"])){
										// IS DELIVERED
										$thisDeliveryStatusImage = 'deliveryStatusDelivered.png';
										$thisDeliveryStatusTitle = 'Paket zugestellt';
									}
									else if(preg_match("/14/", $thisDeliveryDatasValue["SCAN_CODES"])){
										// DELIVERY FAILURE
										$thisDeliveryStatusImage = 'deliveryStatusFailure.png';
										$thisDeliveryStatusTitle = 'Zustellungsproblem';
									}
									else {
										// ON THE WAY
										$thisDeliveryStatusImage = 'deliveryStatusInTransit.png';
										$thisDeliveryStatusTitle = 'Paket unterwegs';
									}
									$thisMailTextHTML .= '<img src="' . $thisDeliveryStatusImage . '" width="24" height="24" alt="' . $thisDeliveryStatusTitle . '" title="' . $thisDeliveryStatusTitle . '" style="cursor:pointer;"/>';
									$thisMailTextHTML .= '</td>';

									if($showScanCodes){
										$thisMailTextHTML .= '<td>';
										#$thisMailTextHTML .= $thisDeliveryDatasValue["SCAN_CODES"];
										$thisMailTextHTML .= preg_replace_callback(
																'/[0-9]{2}/',
																function($found){
																	global $arrDpdScanCodes;
																	return '<span title="' . $arrDpdScanCodes[$found[0]] . '" style="cursor:pointer">' . $found[0] . '</span>';
																},
																$thisDeliveryDatasValue["SCAN_CODES"]
															);
										$thisMailTextHTML .= '</td>';
									}

									$thisMailTextHTML .= '</tr>';

									$countRow++;
								}
								$thisMailTextHTML .= '</table>';


								if($showScanCodes){
									if(!empty($arrDpdScanCodes)){
										$thisMailTextHTML .= '<hr style="margin:10px 0 10px 0;height:1px;background-color:#333;color:#333;border:0px solid #333;" />';
										$thisMailTextHTML .= '<h2 style="font-size:12px;color:#333;">Legende DPD-Scancodes</h2>';

										$thisMailTextHTML .= '<table cellpadding="2" cellspacing="0" width="100%" border="1" style="font-size:12px;border:1px solid #333;border-collapse:collapse">';

										$thisMailTextHTML .= '<tr style="font-size:10px;border:1px solid #333;background-color:#5e5e5e;color:#EEE;">';
										$thisMailTextHTML .= '<th>Code</th>';
										$thisMailTextHTML .= '<th>Beschreibung</th>';
										$thisMailTextHTML .= '</tr>';

										$countRow = 0;
										foreach($arrDpdScanCodes as $thisDpdScanCodeKey => $thisDpdScanCodeValue){
											if($countRow%2 == 0){ $rowColor = ''; }
											else { $rowColor = 'background-color:#EEE;'; }
											$thisMailTextHTML .= '<tr style="font-size:11px;border:1px solid #333;vertical-align:top;' . $rowColor . '">';
											$thisMailTextHTML .= '<td><b>' . $thisDpdScanCodeKey . '</td>';
											$thisMailTextHTML .= '<td>' . $thisDpdScanCodeValue . '</td>';
											$thisMailTextHTML .= '</tr>';

											$countRow++;
										}
										$thisMailTextHTML .= '</table>';

										$documentPathDpdScancodes = 'https://extranet.dpd.de/data/lang_files/REMARKS.de.pdf';
										$thisMailTextHTML .= '<p>Eine genaue Beschreibung der Scan-Codes finden Sie in dieser PDF-Datei: <a href="' . $documentPathDpdScancodes . '" style="text-decoration:none;color:#009;">DPD-Scan-Codes</a></p>';
									}
								}

								$thisMailTextHTML .= '</div>';


								// BOF SEND MAIL
									if($thisRecipientMailAdress != ''){
										$thisMailSender = 'webmaster@burhan-ctr.de';
										
										#$thisMailRecipient = 'webmaster@burhan-ctr.de';
										/*
										if($thisGetTrackingNumbersKey == "NC"){
											$thisMailRecipient = 'webmaster@burhan-ctr.de';
										}
										else if($thisGetTrackingNumbersKey == "WV"){
											$thisMailRecipient = 'webmaster@burhan-ctr.de';
										}
										*/
										$thisMailRecipient = $thisRecipientMailAdress;										
										
										$thisMailSubject = 'DPD-Tracking-Nummern' . ' - ' . formatDate($todayDate, 'display') . ' | ' . $thisDeliveryDataKey;

										#$thisMailBcc = "";
										$thisMailBcc = "webmaster@burhan-ctr.de";

										$sendAttachedMailText = 'Sehr geehrte Damen und Herren,' . "\n\n" . 'anbei die DPD-Trackingnummern zu Ihren offenen Auftr&auml;gen.' . "\n" . "Die Paket-Nummern sind mit dem DPD-Portal verlinkt." . "\n\n";
										$sendAttachedMailText .= $thisMailTextHTML;

										$documentType = 'SX';
										$selectMailtextTemplates = 'SX';

										// BOF GET ATTACHED FILE
											$arrAttachmentFiles = array();
										// EOF GET ATTACHED FILE

										$createMail = new createMail(
															$thisMailSubject,												// TEXT:	MAIL_SUBJECT
															'',																// STRING:	DOCUMENT TYPE
															'',																// STRING:	DOCUMENT NUMBER
															$arrAttachmentFiles,											// ARRAY:	ATTACHMENT FILES
															$thisMailRecipient,	 											// STRING:	RECIPIENTS
															$arrMailContentDatas[$selectMailtextTemplates],					// MAIL_TEXT_TEMPLATE
															$mailTemplate["html"],											// MAIL_BODY_TEMPLATE
															'',																// STRING:	ADDITIONAL TEXT
															$sendAttachedMailText,											// STRING:	SEND ATTACHED TEXT
															true,															// BOOLEAN:	SEND ATTACHED DOCUMENT
															DIRECTORY_MAIL_IMAGES,											// DIRECTORY_MAIL_IMAGES
															$thisMailSender,												// STRING: SENDER
															$thisMailBcc													// STRING: BCC
														);
										$createMailResult = $createMail->returnResult();

										// BOF MARK TRACKING-NUMBER AS SENDED
											if($createMailResult){
												fwrite($fp_log, date("Y-m-d H:i:s") . ": Die Mail wurde an " . $thisMailRecipient . " gesendet." . "\n");

												// BOF UPDATE DELIVERY TRACKING NUMBERS
													if(!empty($arrSqlUpdateDataWhere[$thisDeliveryDataKey])){
														$sql_updateTrackingNumber = "
																UPDATE
																	`" . TABLE_DELIVERY_DATAS . "`
																	SET
																		`InfoMailGesendet` = '1'

																	WHERE 1
																		AND
																		" . implode(" OR ", $arrSqlUpdateDataWhere[$thisDeliveryDataKey]) . "
															";
														$sql_updateTrackingNumber = cleanSQL($sql_updateTrackingNumber);
														$rs_updateTrackingNumber = $dbConnection->db_query($sql_updateTrackingNumber);
														if(mysqli_error($db_open)) {
															fwrite($fp_log, date("Y-m-d H:i:s") . ":  sql_updateTrackingNumber : " . $sql_updateTrackingNumber . "\n" . "ERROR: " . mysqli_error($db_open) . "\n\n\n");
														}
													}
												// EOF UPDATE DELIVERY TRACKING NUMBERS
											}
											else{
												fwrite($fp_log, date("Y-m-d H:i:s") . ": ERROR - Die Mail konnte nicht an " . $thisMailRecipient . " gesendet werden." . "\n");
											}
										// EOF MARK TRACKING-NUMBER AS SENDED
									}
								// EOF SEND MAIL

								#dd('thisRecipientMailAdress');
								#dd('createMailResult');
								#dd('thisMailTextHTML');
								#echo $thisMailTextHTML;
								
								sleep(30);
							}
						}
					// EOF CREATE MAIL TEXT
			}
		}
	}

	fwrite($fp_log, date("Y-m-d H:i:s") . ": END : " . MANDATOR . " | getMandator : " . $getMandator . "\n");
	fwrite($fp_log, str_repeat('#', 50) . "\n");

	if($dbConnection){
		$dbConnection->db_close();
	}
	if($fp_log){
		fclose($fp_log);
	}
?>