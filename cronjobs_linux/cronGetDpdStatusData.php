<?php

	#error_reporting(0);
	error_reporting(E_ALL & ~E_NOTICE);
	set_time_limit(0);

	DEFINE('BASEPATH_Auftragslisten', "/var/www/Auftragslisten/");
	#DEFINE('BASEPATH_CRONJOBS', $_SERVER['DOCUMENT_ROOT'] . "/");
	DEFINE('BASEPATH_CRONJOBS', BASEPATH_Auftragslisten . "cronjobs_linux/");
		
	DEFINE('PATH_DPD_INTERFACE_TRACKING_FILES', BASEPATH_Auftragslisten . "dpdTrackingFilesInterface/");

	require_once(BASEPATH_CRONJOBS . 'cronjobsConfig.inc.php');
	#require_once(BASEPATH_Auftragslisten . 'config/configFiles.inc.php');

	require_once(BASEPATH_Auftragslisten . 'config/configParams_' . strtolower(MANDATOR) . '.inc.php');
	require_once(BASEPATH_Auftragslisten . 'config/configBasic.inc.php');
	require_once(BASEPATH_Auftragslisten . 'config/configTables.inc.php');
	require_once(BASEPATH_Auftragslisten . 'inc/functions.inc.php');
	require_once(BASEPATH_Auftragslisten . 'classes/DB_Connection.class.php');
	
	require_once(BASEPATH_Auftragslisten . 'inc/convert.inc.php');
	
	// BOF RUN CRONJOB 05:00 MORNING
	$todayDate = date("Y-m-d");
	$todayDateTime = date("Y-m-d H:i:s");
	$yesterdayDate = date("Y-m-d", mktime(0, 0, 0, date("m"), (date("d") - 1), date("Y")));

	DEFINE('FILENAME_LOGFILE', BASEPATH_CRONJOBS . "logs/" . MANDATOR . "_" . basename($_SERVER["PHP_SELF"]) . ".txt");

	if(file_exists(FILENAME_LOGFILE)){
		unlink(FILENAME_LOGFILE);
	}
	$fp_log = fopen(FILENAME_LOGFILE, "w");

	fwrite($fp_log, date("Y-m-d H:i:s") . " : MANDATOR : " . MANDATOR . " | getMandator : " . $getMandator . "\n");

	$dbConnection = new DB_Connection();
	$db_open = $dbConnection->db_connect();

	$thisMessage = '';
	
	$handleThisDeliveryDatas = '';

	// ACTION_IMPORT_DPD_FTP_INTERFACE_STATUS_DATAS

	// BOF READ TRACKING FILES
		// BOF READ LOCAL FILES
			$arrTempDpdLocalTrackingFiles = read_dir( PATH_DPD_INTERFACE_TRACKING_FILES);
		
			$arrDpdLocalTrackingFiles = array();
			if(!empty($arrTempDpdLocalTrackingFiles)){
				foreach($arrTempDpdLocalTrackingFiles as $thisDpdLocalTrackingFilesKey => $thisDpdLocalTrackingFilesValue ){
					if(!is_dir($thisDpdLocalTrackingFilesValue) && !preg_match("/" . preg_replace("/\//", "\/", PATH_DPD_INTERFACE_TRACKING_FILES_ZIPPED_FILES) . "/", $thisDpdLocalTrackingFilesValue)){
						$arrDpdLocalTrackingFiles[$thisDpdLocalTrackingFilesKey] = basename($thisDpdLocalTrackingFilesValue);
					}
				}
			}
			else {				
				fwrite($fp_log, date("Y-m-d H:i:s") . " Es liegen keine lokalen Dateien vor" . "\n\n");
			}		
			fwrite($fp_log, date("Y-m-d H:i:s") . " Anzahl aller lokalen Dateien: " . count($arrDpdLocalTrackingFiles) . "\n\n");			
			if(!empty($arrDpdLocalTrackingFiles)){				
				foreach($arrDpdLocalTrackingFiles as $thisDpdLocalTrackingFiles){					
					#fwrite($fp_log, date("Y-m-d H:i:s") . " Lokale Datei: " . $thisDpdLocalTrackingFiles . "\n\n");
				}				
			}
			else {
				fwrite($fp_log, date("Y-m-d H:i:s") . " Es liegen keine lokalen Dateien vor" . "\n\n");
			}
		// BOF READ LOCAL FILES	
		
			$dpdServer_connect = ftp_connect(DPD_FTP_INTERFACE_SERVER, DPD_FTP_INTERFACE_PORT);
			if($dpdServer_connect){				
				fwrite($fp_log, date("Y-m-d H:i:s") . " FTP-Verbindung zum DPD-SERVER hergestellt!" . "\n\n");
				
				$dpdServer_login = ftp_login($dpdServer_connect, DPD_FTP_INTERFACE_LOGIN, DPD_FTP_INTERFACE_PASSWORD);
				if($dpdServer_login){					
					fwrite($fp_log, date("Y-m-d H:i:s") . " Login zum DPD-SERVER hergestellt!" . "\n\n");

					$arrDpdExternalTrackingFiles = ftp_nlist($dpdServer_connect, ".");
					
					if(!empty($arrDpdExternalTrackingFiles)){				
						// BOF EXTERNAL FILES								
							fwrite($fp_log, date("Y-m-d H:i:s") . " Anzahl aller externen Dateien: " . count($arrDpdExternalTrackingFiles) . "\n\n");
								
							if(!empty($arrDpdExternalTrackingFiles)){								
								foreach($arrDpdExternalTrackingFiles as $thisDpdExternalTrackingFiles){									
									#fwrite($fp_log, date("Y-m-d H:i:s") . " Externe Datei: " . $thisDpdExternalTrackingFiles . "\n\n");
								}
							}
							else {
								fwrite($fp_log, date("Y-m-d H:i:s") . " Es liegen keine externen Dateien vor" . "\n\n");
							}
						// EOF EXTERNAL FILES

						// BOF REMOVED EXTERNAL FILES							
							$arrDpdExternalRemovedTrackingFiles = array_diff($arrDpdLocalTrackingFiles, $arrDpdExternalTrackingFiles);								
							fwrite($fp_log, date("Y-m-d H:i:s") . " Anzahl entfernter externer Dateien: " . count($arrDpdExternalRemovedTrackingFiles) . "\n\n");
							
							if(!empty($arrDpdExternalRemovedTrackingFiles)){
								foreach($arrDpdExternalRemovedTrackingFiles as $thisDpdExternalRemovedTrackingFiles){
									#fwrite($fp_log, date("Y-m-d H:i:s") . " Externe entfernte Datei: " . $thisDpdExternalRemovedTrackingFiles . "\n\n");
								}
							}
							else {
								fwrite($fp_log, date("Y-m-d H:i:s") . " Es liegen keine externen entfernten Dateien vor" . "\n\n");
							}	
							
						// EOF REMOVED EXTERNAL FILES

						// BOF NEW EXTERNAL FILES							
							$arrDpdNewExternalTrackingFiles = array_diff($arrDpdExternalTrackingFiles, $arrDpdLocalTrackingFiles);							
							fwrite($fp_log, date("Y-m-d H:i:s") . " Anzahl neuer externer Dateien: " . count($arrDpdNewExternalTrackingFiles) . "\n\n");
							
							if(!empty($arrDpdNewExternalTrackingFiles)){
								sort($arrDpdNewExternalTrackingFiles);								
								foreach($arrDpdNewExternalTrackingFiles as $thisDpdNewExternalTrackingFiles){									
									fwrite($fp_log, date("Y-m-d H:i:s") . " Externe neue Datei: " . $thisDpdNewExternalTrackingFiles . "\n\n");
								}								
							}
							else {
								fwrite($fp_log, date("Y-m-d H:i:s") . " Es liegen keine externen neuen Dateien vor" . "\n\n");
							}								
						// EOF NEW EXTERNAL FILES
			
						// BOF GET AND READ NEW EXTERNAL FILES
							foreach($arrDpdNewExternalTrackingFiles as $thisDpdNewExternalTrackingFile){
								$dpdServer_rsGetFile = ftp_get($dpdServer_connect, PATH_DPD_INTERFACE_TRACKING_FILES . basename($thisDpdNewExternalTrackingFile), $thisDpdNewExternalTrackingFile, FTP_BINARY);
								if($dpdServer_rsGetFile){	
									chmod(PATH_DPD_INTERFACE_TRACKING_FILES . basename($thisDpdNewExternalTrackingFile), 0777);
									fwrite($fp_log, date("Y-m-d H:i:s") . " Datei &quot;" . $thisDpdNewExternalTrackingFile. "&quot; vom DPD-SERVER heruntergeladen!" . "\n\n");
									$arrThisPathinfo = pathinfo(PATH_DPD_INTERFACE_TRACKING_FILES . basename($thisDpdNewExternalTrackingFile));
									if(strtolower($arrThisPathinfo["extension"]) != "sem"){
										$fp_dpdTrackingFile = fopen(PATH_DPD_INTERFACE_TRACKING_FILES . basename($thisDpdNewExternalTrackingFile), 'r');
										$dpdTrackingFileContent = fread($fp_dpdTrackingFile, filesize(PATH_DPD_INTERFACE_TRACKING_FILES . basename($thisDpdNewExternalTrackingFile)));
										fclose($fp_dpdTrackingFile);
										if($dpdTrackingFileContent != ''){
											$dpdTrackingFileContent = mb_convert_encoding($dpdTrackingFileContent, 'UTF-8');
											// BOF ADD SOURCE FILENAME TO DATA LINE
											$dpdTrackingFileContent = preg_replace("/^/", basename($thisDpdNewExternalTrackingFile) . ";", $dpdTrackingFileContent);
											$dpdTrackingFileContent = preg_replace("/(\n)([0-9])/", "$1" . basename($thisDpdNewExternalTrackingFile).";$2", $dpdTrackingFileContent);
											// EOF ADD SOURCE FILENAME TO DATA LINE
											$handleThisDeliveryDatas .= $dpdTrackingFileContent;
										}
									}
								}
								else {								
									fwrite($fp_log, date("Y-m-d H:i:s") . " Datei &quot;" . $thisDpdNewExternalTrackingFile. "&quot; NICHT vom DPD-SERVER heruntergeladen!" . "\n\n");
								}
							}
						// EOF GET AND READ NEW EXTERNAL FILES
					}
				}
				else {					
					fwrite($fp_log, date("Y-m-d H:i:s") . " Kein Login zum DPD-SERVER!" . "\n\n");
				}
				$dpdServer_close = ftp_close($dpdServer_connect);
			}
			else {				
				fwrite($fp_log, date("Y-m-d H:i:s") . " Keine FTP-Verbindung zum DPD-SERVER!" . "\n\n");
			}	
	// EOF READ TRACKING FILES

	
	// BOF INSERT TRACKING DATA
		if($handleThisDeliveryDatas != ''){
			#fwrite($fp_log, date("Y-m-d H:i:s") . "handleThisDeliveryDatas: " . $handleThisDeliveryDatas . "\n\n");
			fwrite($fp_log, date("Y-m-d H:i:s") . "handleThisDeliveryDatas ist nicht leer" . "\n\n");
			$arrDataOutput = convertDpdInterfaceDeliveryData($handleThisDeliveryDatas, TABLE_DELIVERY_DATAS_STATUS_IMPORT);
			$arrDataOutput['sql'] .= "DELETE FROM `" . TABLE_DELIVERY_DATAS_STATUS_IMPORT . "` WHERE 1 AND `PARCELNO` = 'PARCELNO';" . "\n";
					
			$arrTemp = explode("\n", trim($arrDataOutput['sql']));
			
			if(!empty($arrTemp)){
				foreach($arrTemp as $thisDataSQL){
					// BOF GET VALUES FOR MD5 ON ALL FIELDS
						$thisDataSqlValues = preg_match('/\(.*\)/', $thisDataSQL, $arrFound);
						$thisDataSqlAdd = " MD5(CONCAT" . $arrFound[0]. ") ";
						$thisDataSqlNew = preg_replace("/(\);)$/", ", " . $thisDataSqlAdd . "$1", $thisDataSQL);
						$thisDataSQL = $thisDataSqlNew;
					// EOF GET VALUES FOR MD5 ON ALL FIELDS	
					
					$rs_thisDataSQL = $dbConnection->db_query($thisDataSQL);
					
					if(!$rs_thisDataSQL){
						fwrite($fp_log, date("Y-m-d H:i:s") . " thisDataSQL: " . $thisDataSQL . "\n\n");
						fwrite($fp_log, date("Y-m-d H:i:s") . " ERROR : " . mysqli_error() . "\n\n");
					}
					else {
						fwrite($fp_log, date("Y-m-d H:i:s") . " OK : " . $thisDataSQL . "\n\n");
					}
				}	
			}		
		}
		else {
			fwrite($fp_log, date("Y-m-d H:i:s") . " handleThisDeliveryDatas ist leer" . "\n\n");
		}
	// EOF INSERT TRACKING DATA
	
	// BOF UPDATE DELIVERY DATAS WITH MISSING CUSTOMER NUMBER
		$sql_updateMissingCustomerNumbers = "
					 UPDATE
						`" . TABLE_DELIVERY_DATAS . "`
						
						INNER JOIN `" . TABLE_CUSTOMERS . "`
						ON(`" . TABLE_DELIVERY_DATAS . "`.`PLZ_1` = `" . TABLE_CUSTOMERS . "`.`customersCompanyPLZ`)
						
						SET
							`Ref_Adresse_1` = `" . TABLE_CUSTOMERS . "`.`customersKundennummer`
							
						WHERE 1
							AND `" . TABLE_DELIVERY_DATAS . "`.`Ref_Adresse_1` = ''
							AND (
								CONCAT(`" . TABLE_CUSTOMERS . "`.`customersCompanyStrasse`, ' ', `" . TABLE_CUSTOMERS . "`.`customersCompanyHausnummer`) = `Adresse_1` 
								OR CONCAT(`" . TABLE_CUSTOMERS . "`.`customersCompanyStrasse`, ' ', `" . TABLE_CUSTOMERS . "`.`customersCompanyHausnummer`) = REPLACE(`Adresse_1`, 'straße', 'str') 
								OR CONCAT(`" . TABLE_CUSTOMERS . "`.`customersCompanyStrasse`, ' ', `" . TABLE_CUSTOMERS . "`.`customersCompanyHausnummer`) = REPLACE(`Adresse_1`, 'straße', 'str.') 
								OR CONCAT(`" . TABLE_CUSTOMERS . "`.`customersCompanyStrasse`, ' ', `" . TABLE_CUSTOMERS . "`.`customersCompanyHausnummer`) = REPLACE(`Adresse_1`, 'strasse', 'str') 
								OR CONCAT(`" . TABLE_CUSTOMERS . "`.`customersCompanyStrasse`, ' ', `" . TABLE_CUSTOMERS . "`.`customersCompanyHausnummer`) = REPLACE(`Adresse_1`, 'strasse', 'str.') 
								OR CONCAT(`" . TABLE_CUSTOMERS . "`.`customersCompanyStrasse`, ' ', `" . TABLE_CUSTOMERS . "`.`customersCompanyHausnummer`) = REPLACE(`Adresse_1`, 'str', 'str.') 
							) 
			";
		$rs_updateMissingCustomerNumbers = $dbConnection->db_query($sql_updateMissingCustomerNumbers);
		
		if(!$rs_updateMissingCustomerNumbers){
			fwrite($fp_log, date("Y-m-d H:i:s") . " sql_updateMissingCustomerNumbers: " . $sql_updateMissingCustomerNumbers . "\n\n");
			fwrite($fp_log, date("Y-m-d H:i:s") . " ERROR : " . mysqli_error() . "\n\n");
		}
		else {
			fwrite($fp_log, date("Y-m-d H:i:s") . " OK : UPDATE DELIVERY DATAS WITH MISSING CUSTOMER NUMBER" . "\n\n");
		}
	// EOF UPDATE DELIVERY DATAS WITH MISSING CUSTOMER NUMBER
	
	fwrite($fp_log, "ENDE: " . date("Y-m-d H:i:s") . " \n\n" . str_repeat("#", 100) . "\n");

	if($dbConnection){
		$dbConnection->db_close();
	}
	if($fp_log){
		fclose($fp_log);
	}