#!/bin/bash
echo off
# REM /var/www/Auftragslisten/cronjobs_linux/gzipJsAndCssFiles.bat
# REM ---------------------------------------------------------------------------------------------------------------
# REM ---------------------------------------------------------------------------------------------------------------

# REM BOF SET FILENAMES
	PATH_Auftragslisten=/var/www/Auftragslisten/
	CRONJOB_DIR_EXECUTE_FILES=/var/www/Auftragslisten/cronjobs_linux/
	CRONJOB_DIR_LOG_FILES="$CRONJOB_DIR_EXECUTE_FILES"logs/
	CRONJOB_DIR_TEMP_FILES="$CRONJOB_DIR_EXECUTE_FILES"tmp/

	# REM -----------------------------------------------------------------------------------------------------------

	PATH_PHP=php
# REM EOF SET FILENAMES

# REM ---------------------------------------------------------------------------------------------------------------
# REM ---------------------------------------------------------------------------------------------------------------

# REM css/borders.min.css.gzip
# REM css/layout.min.css.gzip
# REM css/print.min.css.gzip
# REM js/functions.min.js.gzip
# REM scriptPackages/clusterize.js-master/clusterize.css
# REM scriptPackages/clusterize.js-master/clusterize.min.js
# REM scriptPackages/googleMaplabel/maplabel.min.js
# REM scriptPackages/javascript-alert-v2.4/alert.css
# REM scriptPackages/javascript-alert-v2.4/alert.js
# REM scriptPackages/jquery-1.8.3.min.js
# REM scriptPackages/jquery-confirm/jquery-confirm.min.css
# REM scriptPackages/jquery-confirm/jquery-confirm.min.js
# REM scriptPackages/jquery-ui-custom/jquery-ui-1.8.18.custom.css
# REM scriptPackages/jquery-ui-custom/jquery-ui-1.9.2.custom.min.js
# REM scriptPackages/jquery-ui-custom/jquery.ui.datepicker-de.js
# REM scriptPackages/jquery-ui-timepicker-0.3.3/jquery.ui.timepicker.css
# REM scriptPackages/jquery-ui-timepicker-0.3.3/jquery.ui.timepicker.js
# REM scriptPackages/jquery.lazyload.min.js	
# REM scriptPackages/jquery.media.js
# REM scriptPackages/jquery_select2-3.5.2/select2.css
# REM scriptPackages/jquery_select2-3.5.2/select2.js
# REM scriptPackages/powertable/jquery-powertable.js

# REM -- ####################
# REM 7z -tgzip a "/var/www/Auftragslisten/xxx" "/var/www/Auftragslisten/xxx"
# REM PATTERN:	(.*)
# REM REPLACE:	7z -tgzip a "$1\.gzip" "$1"
# REM -- ####################

7z -tgzip a "/var/www/Auftragslisten/css/borders.min.css.gzip" "/var/www/Auftragslisten/css/borders.min.css"
7z -tgzip a "/var/www/Auftragslisten/css/layout.min.css.gzip" "/var/www/Auftragslisten/css/layout.min.css"
7z -tgzip a "/var/www/Auftragslisten/css/print.min.css.gzip" "/var/www/Auftragslisten/css/print.min.css"
7z -tgzip a "/var/www/Auftragslisten/js/functions.min.js.gzip" "/var/www/Auftragslisten/js/functions.min.js"
7z -tgzip a "/var/www/Auftragslisten/scriptPackages/clusterize.js-master/clusterize.css.gzip" "/var/www/Auftragslisten/scriptPackages/clusterize.js-master/clusterize.css"
7z -tgzip a "/var/www/Auftragslisten/scriptPackages/clusterize.js-master/clusterize.min.js.gzip" "/var/www/Auftragslisten/scriptPackages/clusterize.js-master/clusterize.min.js"
7z -tgzip a "/var/www/Auftragslisten/scriptPackages/javascript-alert-v2.4/alert.css.gzip" "/var/www/Auftragslisten/scriptPackages/javascript-alert-v2.4/alert.css"
7z -tgzip a "/var/www/Auftragslisten/scriptPackages/javascript-alert-v2.4/alert.js.gzip" "/var/www/Auftragslisten/scriptPackages/javascript-alert-v2.4/alert.js"
7z -tgzip a "/var/www/Auftragslisten/scriptPackages/jquery-1.8.3.min.js.gzip" "/var/www/Auftragslisten/scriptPackages/jquery-1.8.3.min.js"
7z -tgzip a "/var/www/Auftragslisten/scriptPackages/jquery-confirm/jquery-confirm.min.css.gzip" "/var/www/Auftragslisten/scriptPackages/jquery-confirm/jquery-confirm.min.css"
7z -tgzip a "/var/www/Auftragslisten/scriptPackages/jquery-confirm/jquery-confirm.min.js.gzip" "/var/www/Auftragslisten/scriptPackages/jquery-confirm/jquery-confirm.min.js"
7z -tgzip a "/var/www/Auftragslisten/scriptPackages/jquery-ui-custom/jquery-ui-1.8.18.custom.css.gzip" "/var/www/Auftragslisten/scriptPackages/jquery-ui-custom/jquery-ui-1.8.18.custom.css"
7z -tgzip a "/var/www/Auftragslisten/scriptPackages/jquery-ui-custom/jquery-ui-1.9.2.custom.min.js.gzip" "/var/www/Auftragslisten/scriptPackages/jquery-ui-custom/jquery-ui-1.9.2.custom.min.js"
7z -tgzip a "/var/www/Auftragslisten/scriptPackages/jquery-ui-custom/jquery.ui.datepicker-de.js.gzip" "/var/www/Auftragslisten/scriptPackages/jquery-ui-custom/jquery.ui.datepicker-de.js"
7z -tgzip a "/var/www/Auftragslisten/scriptPackages/jquery-ui-timepicker-0.3.3/jquery.ui.timepicker.css.gzip" "/var/www/Auftragslisten/scriptPackages/jquery-ui-timepicker-0.3.3/jquery.ui.timepicker.css"
7z -tgzip a "/var/www/Auftragslisten/scriptPackages/jquery-ui-timepicker-0.3.3/jquery.ui.timepicker.js.gzip" "/var/www/Auftragslisten/scriptPackages/jquery-ui-timepicker-0.3.3/jquery.ui.timepicker.js"
7z -tgzip a "/var/www/Auftragslisten/scriptPackages/jquery.lazyload.min.js.gzip" "/var/www/Auftragslisten/scriptPackages/jquery.lazyload.min.js"
7z -tgzip a "/var/www/Auftragslisten/scriptPackages/jquery.media.js.gzip" "/var/www/Auftragslisten/scriptPackages/jquery.media.js"
7z -tgzip a "/var/www/Auftragslisten/scriptPackages/jquery_select2-3.5.2/select2.css.gzip" "/var/www/Auftragslisten/scriptPackages/jquery_select2-3.5.2/select2.css"
7z -tgzip a "/var/www/Auftragslisten/scriptPackages/jquery_select2-3.5.2/select2.js.gzip" "/var/www/Auftragslisten/scriptPackages/jquery_select2-3.5.2/select2.js"
7z -tgzip a "/var/www/Auftragslisten/scriptPackages/powertable/jquery-powertable.js.gzip" "/var/www/Auftragslisten/scriptPackages/powertable/jquery-powertable.js"
7z -tgzip a "/var/www/Auftragslisten/scriptPackages/googleMaplabel/maplabel.min.js.gzip" "/var/www/Auftragslisten/scriptPackages/googleMaplabel/maplabel.min.js"

>>"$CRONJOB_DIR_LOG_FILES"gzipJsAndCssFiles.txt 2>&1