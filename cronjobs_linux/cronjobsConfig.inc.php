<?php
	if ($_SERVER['DOCUMENT_ROOT'] == ""){
		$_SERVER['DOCUMENT_ROOT'] = dirname(__FILE__);
	}

	// BOF READ BATCH GET VARS
		$arrArgs = array();
		if(!empty($_SERVER['argv'])){
			foreach($_SERVER['argv'] as $thisArgKey => $thisArgValue){
				if(preg_match("/=/", $thisArgValue)){
					$arrTemp = explode("=", $thisArgValue);
					$$arrTemp[0] = $arrTemp[1];
					$arrArgs[$arrTemp[0]] = $arrTemp[1];
				}
			}
			$getMandator = '';
			if(isset($arrArgs["MANDATOR"])){
				$getMandator = $arrArgs["MANDATOR"];
			}
		}
	// EOF READ BATCH GET VARS


	if($getMandator != ''){
		DEFINE('MANDATOR', $getMandator);
	}
	else{
		DEFINE('MANDATOR', 'bctr');
	}

	DEFINE('DIRECTORY_MAIL_IMAGES', BASEPATH_Auftragslisten . 'documents_' . MANDATOR . '/documentTemplates/templatesMail/images/');

	// TIMEZONE / PHP_INI
	if(!defined("SET_DATE_DEFAULT_TIMEZONE")) { DEFINE('SET_DATE_DEFAULT_TIMEZONE', 'Europe/Berlin'); }
	if(!defined("LC_ALL")) { DEFINE('LC_ALL', '"de_DE@euro", "de_DE", "de", "ge"'); }

	date_default_timezone_set(SET_DATE_DEFAULT_TIMEZONE);
	setlocale(LC_ALL, constant("LC_ALL"));
	setlocale(LC_TIME, constant("LC_ALL"));
?>