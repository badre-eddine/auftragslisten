<?php
	#error_reporting(0);
	error_reporting(E_ALL & ~E_NOTICE);
	set_time_limit(0);

	DEFINE('BASEPATH_Auftragslisten', "/var/www/Auftragslisten/");
	#DEFINE('BASEPATH_CRONJOBS', $_SERVER['DOCUMENT_ROOT'] . "/");
	DEFINE('BASEPATH_CRONJOBS', BASEPATH_Auftragslisten . "cronjobs_linux/");

	require_once(BASEPATH_CRONJOBS . 'cronjobsConfig.inc.php');
	#require_once(BASEPATH_Auftragslisten . 'config/configFiles.inc.php');

	require_once(BASEPATH_Auftragslisten . 'config/configParams_' . strtolower(MANDATOR) . '.inc.php');
	require_once(BASEPATH_Auftragslisten . 'config/configBasic.inc.php');
	require_once(BASEPATH_Auftragslisten . 'config/configTables.inc.php');
	require_once(BASEPATH_Auftragslisten . 'inc/functions.inc.php');
	require_once(BASEPATH_Auftragslisten . 'classes/DB_Connection.class.php');


	// BOF RUN CRONJOB 05:00 MORNING
	$todayDate = date("Y-m-d");
	$todayDateTime = date("Y-m-d H:i:s");
	$yesterdayDate = date("Y-m-d", mktime(0, 0, 0, date("m"), (date("d") - 1), date("Y")));

	DEFINE('FILENAME_LOGFILE', BASEPATH_CRONJOBS . "logs/" . MANDATOR . "_" . basename($_SERVER["PHP_SELF"]) . ".txt");

	if(file_exists(FILENAME_LOGFILE)){
		unlink(FILENAME_LOGFILE);
	}
	$fp_log = fopen(FILENAME_LOGFILE, "w");

	fwrite($fp_log, date("Y-m-d H:i:s") . ": MANDATOR : " . MANDATOR . " | getMandator : " . $getMandator . "\n");

	$dbConnection = new DB_Connection();
	$db_open = $dbConnection->db_connect();

	$thisMessage = '';
	
	// BOF SET INVOICES AS PAID IF SUM IS 0
		$sql_updateZeroInvoicesAsPaid = "
			UPDATE
					`" . TABLE_ORDER_INVOICES . "`
				SET `orderDocumentsStatus` = '2'
				WHERE 1
					AND `orderDocumentsTotalPrice` = 0
					AND `orderDocumentsStatus` = '1'			
		";
		$rs_updateZeroInvoicesAsPaid = $dbConnection->db_query($sql_updateZeroInvoicesAsPaid);
	// EOF SET INVOICES AS PAID IF SUM IS 0	
	
	// BOF SET CONFIRMATIONS AS PAID IF SUM IS 0
		$sql_updateZeroConfirmationsAsPaid = "
				UPDATE
					`" . TABLE_ORDER_CONFIRMATIONS . "`
					SET `orderDocumentsStatus` = '2'
					WHERE 1
						AND `orderDocumentsTotalPrice` = 0
						AND `orderDocumentsStatus` = '1'
		";
		$rs_updateZeroConfirmationsAsPaid = $dbConnection->db_query($sql_updateZeroConfirmationsAsPaid);
	// EOF SET CONFIRMATIONS AS PAID IF SUM IS 0

	// BOF GET FINANCIAL DATA
		if(MANDATOR != ''){
			// TABLE_ORDER_RE || TABLE_ORDER_AB
			// TABLE_ORDER_RE_PAYMENTS || TABLE_ORDER_AB_PAYMENTS

				// BOF GET OPEN INVOICES
					$sql_getOpenInvoices = "
							INSERT
								INTO `" . TABLE_FINANCIAL_PAYMENT_STATISTICS . "` (
									`financialPaymentStatisticsID`,
									`financialPaymentStatisticsTableType`,
									`financialPaymentStatisticsStoreDateTime`,
									`financialPaymentStatisticsItemsCount`,
									`financialPaymentStatisticsStatus`,
									`financialPaymentStatisticsPaymentDates`,
									`financialPaymentStatisticsTotalSum`,
									`financialPaymentStatisticsBankAccount`,
									`financialPaymentStatisticsEntryDate`,
									`financialPaymentStatisticsSearchDate`
								)

								SELECT
										'%',
										'unpaid_RE' AS `tableType`,
										'" . $todayDateTime . "' AS `storeDateTime`,
										COUNT(`" . TABLE_ORDER_INVOICES . "`.`orderDocumentsID`) AS `itemsCount`,
										`" . TABLE_PAYMENT_STATUS_TYPES . "`.`paymentStatusTypesShortName` AS `status`,

										NULL AS `paymentDates`,

										SUM(`" . TABLE_ORDER_INVOICES . "`.`orderDocumentsTotalPrice`) AS `totalSum`,
										`" . TABLE_ORDER_INVOICES . "`.`orderDocumentsBankAccount` AS `bankAccount`,
										'' AS `entryDate`,
										'" . $yesterdayDate . "' AS `searchDate`

									FROM `" . TABLE_ORDER_INVOICES . "`

									LEFT JOIN `" . TABLE_PAYMENT_STATUS_TYPES . "`
									ON(`" . TABLE_ORDER_INVOICES . "`.`orderDocumentsStatus` = `" . TABLE_PAYMENT_STATUS_TYPES . "`.`paymentStatusTypesID`)

									LEFT JOIN `" . TABLE_PAYMENT_CONDITIONS . "`
									ON(`" . TABLE_ORDER_INVOICES . "`.`orderDocumentsPaymentCondition` = `" . TABLE_PAYMENT_CONDITIONS . "`.`paymentConditionsID`)

									LEFT JOIN `" . TABLE_BANK_ACCOUNT_TYPES . "`
									ON(`" . TABLE_ORDER_INVOICES . "`.`orderDocumentsBankAccount` = `" . TABLE_BANK_ACCOUNT_TYPES . "`.`bankAccountTypesShortName`)

									WHERE 1
										AND (
											`" . TABLE_PAYMENT_STATUS_TYPES . "`.`paymentStatusTypesShortName` = 'OF'
											OR `" . TABLE_PAYMENT_STATUS_TYPES . "`.`paymentStatusTypesShortName` = 'TB'
											OR `" . TABLE_PAYMENT_STATUS_TYPES . "`.`paymentStatusTypesShortName` = 'MA'
											OR `" . TABLE_PAYMENT_STATUS_TYPES . "`.`paymentStatusTypesShortName` = 'M1'
											OR `" . TABLE_PAYMENT_STATUS_TYPES . "`.`paymentStatusTypesShortName` = 'M2'
											OR `" . TABLE_PAYMENT_STATUS_TYPES . "`.`paymentStatusTypesShortName` = 'M3'
											OR `" . TABLE_PAYMENT_STATUS_TYPES . "`.`paymentStatusTypesShortName` = 'IK'
											OR `" . TABLE_PAYMENT_STATUS_TYPES . "`.`paymentStatusTypesShortName` = 'MB'
										)

									GROUP BY
										CONCAT(
											`" . TABLE_PAYMENT_STATUS_TYPES . "`.`paymentStatusTypesShortName`,
											`" . TABLE_ORDER_INVOICES . "`.`orderDocumentsBankAccount`
										)
						";
					// dd('sql_getOpenInvoices');
					$rs_getOpenInvoices = $dbConnection->db_query($sql_getOpenInvoices);
					if(mysqli_error($db_open)) {
						fwrite($fp_log, date("Y-m-d H:i:s") . ":  sql_getOpenInvoices : " . $sql_getOpenInvoices . "\n" . mysqli_error($db_open) . "\n\n\n");
					}
				// EOF GET OPEN INVOICES

				// BOF GET PAYMENTS AB
					$sql_getPaymentsConfirmations = "
							INSERT
								INTO `" . TABLE_FINANCIAL_PAYMENT_STATISTICS . "` (
									`financialPaymentStatisticsID`,
									`financialPaymentStatisticsTableType`,
									`financialPaymentStatisticsStoreDateTime`,
									`financialPaymentStatisticsItemsCount`,
									`financialPaymentStatisticsStatus`,
									`financialPaymentStatisticsPaymentDates`,
									`financialPaymentStatisticsTotalSum`,
									`financialPaymentStatisticsBankAccount`,
									`financialPaymentStatisticsEntryDate`,
									`financialPaymentStatisticsSearchDate`
								)
								SELECT
										'%',
										'payments_AB' AS `tableType`,
										'" . $todayDateTime . "' AS `storeDateTime`,
										COUNT(`" . TABLE_ORDER_CONFIRMATION_PAYMENTS . "`.`orderPaymentID`) AS `itemsCount`,
										`" . TABLE_PAYMENT_STATUS_TYPES . "`.`paymentStatusTypesShortName` AS `status`,
										GROUP_CONCAT(DISTINCT `" . TABLE_ORDER_CONFIRMATION_PAYMENTS . "`.`orderPaymentDate` ORDER BY `" . TABLE_ORDER_CONFIRMATION_PAYMENTS . "`.`orderPaymentDate` DESC) AS `paymentDates`,
										SUM(`" . TABLE_ORDER_CONFIRMATION_PAYMENTS . "`.`orderPaymentValue`) AS `totalSum`,
										`" . TABLE_ORDER_CONFIRMATION_PAYMENTS . "`.`orderPaymentBankAccountID` AS `bankAccount`,
										`" . TABLE_ORDER_CONFIRMATION_PAYMENTS . "`.`orderPaymentEntryDate` AS `entryDate`,
										'" . $yesterdayDate . "' AS `searchDate`

									FROM `" . TABLE_ORDER_CONFIRMATION_PAYMENTS . "`

									LEFT JOIN `" . TABLE_BANK_ACCOUNT_TYPES . "`
									ON(`" . TABLE_ORDER_CONFIRMATION_PAYMENTS . "`.`orderPaymentBankAccountID` = `" . TABLE_BANK_ACCOUNT_TYPES . "`.`bankAccountTypesShortName`)

									LEFT JOIN `" . TABLE_ORDER_CONFIRMATIONS . "`
									ON(`" . TABLE_ORDER_CONFIRMATION_PAYMENTS . "`.`orderPaymentOrderNumber` = `" . TABLE_ORDER_CONFIRMATIONS . "`.`orderDocumentsNumber`)

									LEFT JOIN `" . TABLE_PAYMENT_STATUS_TYPES . "`
									ON(`" . TABLE_ORDER_CONFIRMATIONS . "`.`orderDocumentsStatus` = `" . TABLE_PAYMENT_STATUS_TYPES . "`.`paymentStatusTypesID`)

									WHERE 1
										AND `" . TABLE_ORDER_CONFIRMATION_PAYMENTS . "`.`orderPaymentEntryDate` = '" . $yesterdayDate . "'

									GROUP BY
										CONCAT(
											`" . TABLE_ORDER_CONFIRMATION_PAYMENTS . "`.`orderPaymentEntryDate`,
											`" . TABLE_ORDER_CONFIRMATION_PAYMENTS . "`.`orderPaymentBankAccountID`,
											`" . TABLE_PAYMENT_STATUS_TYPES . "`.`paymentStatusTypesShortName`
										)
									ORDER BY `" . TABLE_ORDER_CONFIRMATION_PAYMENTS . "`.`orderPaymentEntryDate` DESC
						";
					// dd('sql_getPaymentsConfirmations');
					$rs_getPaymentsConfirmations = $dbConnection->db_query($sql_getPaymentsConfirmations);
					if(mysqli_error($db_open)) {
						fwrite($fp_log, date("Y-m-d H:i:s") . ":  sql_getPaymentsConfirmations : " . $sql_getPaymentsConfirmations . "\n" . mysqli_error($db_open) . "\n\n\n");
					}
				// EOF GET PAYMENTS AB

				// EOF PAYMENTS AB WITH RE
					$sql_getPaymentsConfirmationsWithInvoices = "
							INSERT
								INTO `" . TABLE_FINANCIAL_PAYMENT_STATISTICS . "` (
									`financialPaymentStatisticsID`,
									`financialPaymentStatisticsTableType`,
									`financialPaymentStatisticsStoreDateTime`,
									`financialPaymentStatisticsItemsCount`,
									`financialPaymentStatisticsStatus`,
									`financialPaymentStatisticsPaymentDates`,
									`financialPaymentStatisticsTotalSum`,
									`financialPaymentStatisticsBankAccount`,
									`financialPaymentStatisticsEntryDate`,
									`financialPaymentStatisticsSearchDate`
								)
								SELECT
										'%',
										'payments_AB_with_RE' AS `tableType`,
										'" . $todayDateTime . "' AS `storeDateTime`,
										COUNT(`" . TABLE_ORDER_CONFIRMATION_PAYMENTS . "`.`orderPaymentID`) AS `itemsCount`,
										`" . TABLE_PAYMENT_STATUS_TYPES . "`.`paymentStatusTypesShortName` AS `status`,
										GROUP_CONCAT(DISTINCT `" . TABLE_ORDER_CONFIRMATION_PAYMENTS . "`.`orderPaymentDate` ORDER BY `" . TABLE_ORDER_CONFIRMATION_PAYMENTS . "`.`orderPaymentDate` DESC) AS `paymentDates`,
										SUM(`" . TABLE_ORDER_CONFIRMATION_PAYMENTS . "`.`orderPaymentValue`) AS `totalSum`,
										`" . TABLE_ORDER_CONFIRMATION_PAYMENTS . "`.`orderPaymentBankAccountID` AS `bankAccount`,
										`" . TABLE_ORDER_CONFIRMATION_PAYMENTS . "`.`orderPaymentEntryDate` AS `entryDate`,
										'" . $yesterdayDate . "' AS `searchDate`

									FROM `" . TABLE_ORDER_CONFIRMATION_PAYMENTS . "`

									LEFT JOIN `" . TABLE_BANK_ACCOUNT_TYPES . "`
									ON(`" . TABLE_ORDER_CONFIRMATION_PAYMENTS . "`.`orderPaymentBankAccountID` = `" . TABLE_BANK_ACCOUNT_TYPES . "`.`bankAccountTypesShortName`)

									LEFT JOIN `" . TABLE_RELATED_DOCUMENTS. "`
									ON(`" . TABLE_ORDER_CONFIRMATION_PAYMENTS . "`.`orderPaymentOrderNumber` = `" . TABLE_RELATED_DOCUMENTS. "`.`relatedDocuments_AB`)

									INNER JOIN `" . TABLE_ORDER_INVOICES . "`
									ON(`" . TABLE_RELATED_DOCUMENTS . "`.`relatedDocuments_RE` = `" . TABLE_ORDER_INVOICES . "`.`orderDocumentsNumber`)

									LEFT JOIN `" . TABLE_PAYMENT_STATUS_TYPES . "`
									ON(`" . TABLE_ORDER_INVOICES . "`.`orderDocumentsStatus` = `" . TABLE_PAYMENT_STATUS_TYPES . "`.`paymentStatusTypesID`)

									WHERE 1
										AND `" . TABLE_ORDER_CONFIRMATION_PAYMENTS . "`.`orderPaymentEntryDate` = '" . $yesterdayDate . "'

									GROUP BY
										CONCAT(
											`" . TABLE_ORDER_CONFIRMATION_PAYMENTS . "`.`orderPaymentEntryDate`,
											`" . TABLE_ORDER_CONFIRMATION_PAYMENTS . "`.`orderPaymentBankAccountID`,
											`" . TABLE_PAYMENT_STATUS_TYPES . "`.`paymentStatusTypesShortName`
										)
									ORDER BY `" . TABLE_ORDER_CONFIRMATION_PAYMENTS . "`.`orderPaymentEntryDate` DESC
						";
					// dd('sql_getPaymentsConfirmationsWithInvoices');
					$rs_getPaymentsConfirmationsWithInvoices = $dbConnection->db_query($sql_getPaymentsConfirmationsWithInvoices);
					if(mysqli_error($db_open)) {
						fwrite($fp_log, date("Y-m-d H:i:s") . ":  sql_getPaymentsConfirmations : " . $sql_getPaymentsConfirmationsWithInvoices . "\n" . mysqli_error($db_open) . "\n\n\n");
					}
				// BOF PAYMENTS AB WITH RE


				// BOF GET PAYMENTS RE
					$sql_getPaymentsInvoices = "
							INSERT
								INTO `" . TABLE_FINANCIAL_PAYMENT_STATISTICS . "` (
									`financialPaymentStatisticsID`,
									`financialPaymentStatisticsTableType`,
									`financialPaymentStatisticsStoreDateTime`,
									`financialPaymentStatisticsItemsCount`,
									`financialPaymentStatisticsStatus`,
									`financialPaymentStatisticsPaymentDates`,
									`financialPaymentStatisticsTotalSum`,
									`financialPaymentStatisticsBankAccount`,
									`financialPaymentStatisticsEntryDate`,
									`financialPaymentStatisticsSearchDate`
								)
								SELECT
									'%',
									'payments_RE' AS `tableType`,
									'" . $todayDateTime . "' AS `storeDateTime`,
									COUNT(`" . TABLE_ORDER_INVOICE_PAYMENTS . "`.`orderPaymentID`) AS `itemsCount`,
									`" . TABLE_PAYMENT_STATUS_TYPES . "`.`paymentStatusTypesShortName` AS `status`,
									GROUP_CONCAT(DISTINCT `" . TABLE_ORDER_INVOICE_PAYMENTS . "`.`orderPaymentDate` ORDER BY `" . TABLE_ORDER_INVOICE_PAYMENTS . "`.`orderPaymentDate` DESC) AS `paymentDates`,
									SUM(`" . TABLE_ORDER_INVOICE_PAYMENTS . "`.`orderPaymentValue`) AS `totalSum`,
									`" . TABLE_ORDER_INVOICE_PAYMENTS . "`.`orderPaymentBankAccountID` AS `bankAccount`,
									`" . TABLE_ORDER_INVOICE_PAYMENTS . "`.`orderPaymentEntryDate` AS `entryDate`,
									'" . $yesterdayDate . "' AS `searchDate`

								FROM `" . TABLE_ORDER_INVOICE_PAYMENTS . "`

								LEFT JOIN `" . TABLE_BANK_ACCOUNT_TYPES . "`
								ON(`" . TABLE_ORDER_INVOICE_PAYMENTS . "`.`orderPaymentBankAccountID` = `" . TABLE_BANK_ACCOUNT_TYPES . "`.`bankAccountTypesShortName`)

								LEFT JOIN `" . TABLE_ORDER_INVOICES . "`
								ON(`" . TABLE_ORDER_INVOICE_PAYMENTS . "`.`orderPaymentOrderNumber` = `" . TABLE_ORDER_INVOICES . "`.`orderDocumentsNumber`)

								LEFT JOIN `" . TABLE_PAYMENT_STATUS_TYPES . "`
								ON(`" . TABLE_ORDER_INVOICES . "`.`orderDocumentsStatus` = `" . TABLE_PAYMENT_STATUS_TYPES . "`.`paymentStatusTypesID`)

								WHERE 1
									AND `" . TABLE_ORDER_INVOICE_PAYMENTS . "`.`orderPaymentEntryDate` = '" . $yesterdayDate . "'

								GROUP BY
									CONCAT(
										`" . TABLE_ORDER_INVOICE_PAYMENTS . "`.`orderPaymentEntryDate`,
										`" . TABLE_ORDER_INVOICE_PAYMENTS . "`.`orderPaymentBankAccountID`,
										`" . TABLE_PAYMENT_STATUS_TYPES . "`.`paymentStatusTypesShortName`
									)
								ORDER BY `" . TABLE_ORDER_INVOICE_PAYMENTS . "`.`orderPaymentEntryDate` DESC
						";
					// dd('sql_getPaymentsInvoices');
					$rs_getPaymentsInvoices = $dbConnection->db_query($sql_getPaymentsInvoices);

					if(mysqli_error($db_open)) {
						fwrite($fp_log, date("Y-m-d H:i:s") . ":  sql_getPaymentsInvoices : " . $sql_getPaymentsInvoices . "\n" . mysqli_error($db_open) . "\n\n\n");
					}
				// EOF GET PAYMENTS RE

				// BOF GET WRITTEN REs
					$sql_getWrittenInvoices = "
							INSERT
								INTO `" . TABLE_FINANCIAL_PAYMENT_STATISTICS . "` (
									`financialPaymentStatisticsID`,
									`financialPaymentStatisticsTableType`,
									`financialPaymentStatisticsStoreDateTime`,
									`financialPaymentStatisticsItemsCount`,
									`financialPaymentStatisticsStatus`,
									`financialPaymentStatisticsPaymentDates`,
									`financialPaymentStatisticsTotalSum`,
									`financialPaymentStatisticsBankAccount`,
									`financialPaymentStatisticsEntryDate`,
									`financialPaymentStatisticsSearchDate`
								)
								SELECT
										'%',
										'written_RE' AS `tableType`,
										'" . $todayDateTime . "' AS `storeDateTime`,
										COUNT(`" .TABLE_ORDER_INVOICES . "`.`orderDocumentsID`) AS `itemsCount`,
										`" . TABLE_PAYMENT_STATUS_TYPES . "`.`paymentStatusTypesShortName` AS `status`,
										'' AS `paymentDates`,
										SUM(`" .TABLE_ORDER_INVOICES . "`.`orderDocumentsTotalPrice`) AS `totalSum`,
										`" .TABLE_ORDER_INVOICES . "`.`orderDocumentsBankAccount` AS `bankAccount`,
										'" . $yesterdayDate . "' AS `entryDate`,
										'" . $yesterdayDate . "' AS `searchDate`

									FROM `" .TABLE_ORDER_INVOICES . "`

									LEFT JOIN `" . TABLE_PAYMENT_STATUS_TYPES . "`
									ON(`" .TABLE_ORDER_INVOICES . "`.`orderDocumentsStatus` = `" . TABLE_PAYMENT_STATUS_TYPES . "`.`paymentStatusTypesID`)
										WHERE 1
											AND `" .TABLE_ORDER_INVOICES . "`.`orderDocumentsDocumentDate` = '" . $yesterdayDate . "'
									GROUP BY
										CONCAT(
											`" . TABLE_PAYMENT_STATUS_TYPES . "`.`paymentStatusTypesShortName`,
											`" .TABLE_ORDER_INVOICES . "`.`orderDocumentsBankAccount`
										)
						";
					// dd('sql_getWrittenInvoices');
					$rs_getWrittenInvoices = $dbConnection->db_query($sql_getWrittenInvoices);
					if(mysqli_error($db_open)) {
						fwrite($fp_log, date("Y-m-d H:i:s") . ":  sql_getWrittenInvoices : " . $sql_getWrittenInvoices . "\n" . mysqli_error($db_open) . "\n\n\n");
					}
				// EOF GET WRITTEN REs

				// BOF GET WRITTEN ABs
					$sql_getWrittenConfirmations = "
							INSERT
								INTO `" . TABLE_FINANCIAL_PAYMENT_STATISTICS . "` (
									`financialPaymentStatisticsID`,
									`financialPaymentStatisticsTableType`,
									`financialPaymentStatisticsStoreDateTime`,
									`financialPaymentStatisticsItemsCount`,
									`financialPaymentStatisticsStatus`,
									`financialPaymentStatisticsPaymentDates`,
									`financialPaymentStatisticsTotalSum`,
									`financialPaymentStatisticsBankAccount`,
									`financialPaymentStatisticsEntryDate`,
									`financialPaymentStatisticsSearchDate`
								)
								SELECT
									'%',
									'written_AB' AS `tableType`,
									'" . $todayDateTime . "' AS `storeDateTime`,
									COUNT(`" .TABLE_ORDER_CONFIRMATIONS . "`.`orderDocumentsID`) AS `itemsCount`,
									`" . TABLE_PAYMENT_STATUS_TYPES . "`.`paymentStatusTypesShortName` AS `status`,
									'' AS `paymentDates`,
									SUM(`" .TABLE_ORDER_CONFIRMATIONS . "`.`orderDocumentsTotalPrice`) AS `totalSum`,
									`" .TABLE_ORDER_CONFIRMATIONS . "`.`orderDocumentsBankAccount` AS `bankAccount`,
									'" . $yesterdayDate . "' AS `entryDate`,
									'" . $yesterdayDate . "' AS `searchDate`

								FROM `" .TABLE_ORDER_CONFIRMATIONS . "`

								LEFT JOIN `" . TABLE_PAYMENT_STATUS_TYPES . "`
								ON(`" .TABLE_ORDER_CONFIRMATIONS . "`.`orderDocumentsStatus` = `" . TABLE_PAYMENT_STATUS_TYPES . "`.`paymentStatusTypesID`)
									WHERE 1
										AND `" .TABLE_ORDER_CONFIRMATIONS . "`.`orderDocumentsDocumentDate` = '" . $yesterdayDate . "'
								GROUP BY
									CONCAT(
										`" . TABLE_PAYMENT_STATUS_TYPES . "`.`paymentStatusTypesShortName`,
										`" .TABLE_ORDER_CONFIRMATIONS . "`.`orderDocumentsBankAccount`
									)
						";
					// dd('sql_getWrittenConfirmations');
					$rs_getWrittenConfirmations = $dbConnection->db_query($sql_getWrittenConfirmations);
					if(mysqli_error($db_open)) {
						fwrite($fp_log, date("Y-m-d H:i:s") . ":  sql_getWrittenConfirmations : " . $sql_getWrittenConfirmations . "\n" . mysqli_error($db_open) . "\n\n\n");
					}
				// EOF GET WRITTEN ABs

				// BOF GET WRITTEN GUs
					$sql_getWrittenCredits = "
							INSERT
								INTO `" . TABLE_FINANCIAL_PAYMENT_STATISTICS . "` (
									`financialPaymentStatisticsID`,
									`financialPaymentStatisticsTableType`,
									`financialPaymentStatisticsStoreDateTime`,
									`financialPaymentStatisticsItemsCount`,
									`financialPaymentStatisticsStatus`,
									`financialPaymentStatisticsPaymentDates`,
									`financialPaymentStatisticsTotalSum`,
									`financialPaymentStatisticsBankAccount`,
									`financialPaymentStatisticsEntryDate`,
									`financialPaymentStatisticsSearchDate`
								)
								SELECT
									'%',
									'written_GS' AS `tableType`,
									'" . $todayDateTime . "' AS `storeDateTime`,
									COUNT(`" .TABLE_ORDER_CREDITS . "`.`orderDocumentsID`) AS `itemsCount`,
									`" . TABLE_PAYMENT_STATUS_TYPES . "`.`paymentStatusTypesShortName` AS `status`,
									'' AS `paymentDates`,
									SUM(`" .TABLE_ORDER_CREDITS . "`.`orderDocumentsTotalPrice`) AS `totalSum`,
									`" .TABLE_ORDER_CREDITS . "`.`orderDocumentsBankAccount` AS `bankAccount`,
									'" . $yesterdayDate . "' AS `entryDate`,
									'" . $yesterdayDate . "' AS `searchDate`

								FROM `" .TABLE_ORDER_CREDITS . "`

								LEFT JOIN `" . TABLE_PAYMENT_STATUS_TYPES . "`
								ON(`" .TABLE_ORDER_CREDITS . "`.`orderDocumentsStatus` = `" . TABLE_PAYMENT_STATUS_TYPES . "`.`paymentStatusTypesID`)
									WHERE 1
										AND `" .TABLE_ORDER_CREDITS . "`.`orderDocumentsDocumentDate` = '" . $yesterdayDate . "'
								GROUP BY
									CONCAT(
										`" . TABLE_PAYMENT_STATUS_TYPES . "`.`paymentStatusTypesShortName`,
										`" .TABLE_ORDER_CREDITS . "`.`orderDocumentsBankAccount`
									)
						";
					// dd('sql_getWrittenCredits');
					$rs_getWrittenCredits = $dbConnection->db_query($sql_getWrittenCredits);
					if(mysqli_error($db_open)) {
						fwrite($fp_log, date("Y-m-d H:i:s") . ":  sql_getWrittenCredits : " . $sql_getWrittenCredits . "\n" . mysqli_error($db_open) . "\n\n\n");
					}
				// EOF GET WRITTEN GUs
		}
		else{
			fwrite($fp_log, "'MANDATOR' ist leer!" . "\n");
		}
	// EOF GET FINANCIAL DATA

	fwrite($fp_log, "ENDE: " . date("Y-m-d H:i:s") . "\n\n" . str_repeat("#", 100) . "\n");

	if($dbConnection){
		$dbConnection->db_close();
	}
	if($fp_log){
		fclose($fp_log);
	}
?>