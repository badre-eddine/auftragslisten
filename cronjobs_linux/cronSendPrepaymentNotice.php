<?php

	#error_reporting(0);
	error_reporting(E_ALL & ~E_NOTICE);
	set_time_limit(0);

	// BOF EXCLUDE CUSTOMER NUMBERS
		$arrExcludeCustomerNumbers = array(
			"78084"
		);
	// EOF EXCLUDE CUSTOMER NUMBERS

	DEFINE('BASEPATH_Auftragslisten', "/var/www/Auftragslisten/");
	#DEFINE('BASEPATH_CRONJOBS', $_SERVER['DOCUMENT_ROOT'] . "/");
	DEFINE('BASEPATH_CRONJOBS', BASEPATH_Auftragslisten . "cronjobs_linux/");

	require_once(BASEPATH_CRONJOBS . 'cronjobsConfig.inc.php');
	#require_once(BASEPATH_Auftragslisten . 'config/configFiles.inc.php');

	DEFINE('DIRECTORY_DOCUMENTS', BASEPATH_Auftragslisten . 'documents_' . MANDATOR . '/');
	DEFINE('DIRECTORY_ATTACHMENT_DOCUMENTS', DIRECTORY_DOCUMENTS . 'attachmentFiles/');
	DEFINE('PATH_AGB_PDF', DIRECTORY_ATTACHMENT_DOCUMENTS . 'AGB_' . strtoupper(MANDATOR) . '.pdf');
	DEFINE('PATH_VCARD', DIRECTORY_ATTACHMENT_DOCUMENTS . 'contact.vcf');
	DEFINE('DIRECTORY_CREATED_DOCUMENTS_CUSTOMERS', DIRECTORY_DOCUMENTS . 'documentsCreated/customers/');

	DEFINE('DIRECTORY_HTML2PDF', BASEPATH_Auftragslisten . 'scriptPackages/html2pdf/');

	require_once(BASEPATH_Auftragslisten . 'config/configParams_' . strtolower(MANDATOR) . '.inc.php');
	require_once(BASEPATH_Auftragslisten . 'config/configBasic.inc.php');
	require_once(BASEPATH_Auftragslisten . 'config/configTables.inc.php');
	require_once(BASEPATH_Auftragslisten . 'inc/functions.inc.php');
	require_once(BASEPATH_Auftragslisten . 'classes/DB_Connection.class.php');
	require_once(BASEPATH_Auftragslisten . 'scriptPackages/htmlMimeMail/htmlMimeMail.php');
	require_once(BASEPATH_Auftragslisten . 'classes/createMail.class.php');

	require_once(BASEPATH_Auftragslisten . 'documents_' . MANDATOR . '/documentTemplates/templatesMail/template_BASIC.inc.php');
	require_once(BASEPATH_Auftragslisten . 'documents_' . MANDATOR . '/documentTemplates/templatesMail/template_TEXT_DATAS.inc.php');

	// BOF DEFINE USE DOCUMENT TABLES
		$useDocType = "AB"; // RE || AB
		DEFINE(THIS_TABLE_DOCUMENTS, constant("TABLE_ORDER_" . $useDocType)); 							// TABLE_ORDER_RE || TABLE_ORDER_AB
		DEFINE(THIS_TABLE_DOCUMENTS_PAYMENTS, constant("TABLE_ORDER_" . $useDocType . "_PAYMENTS")); 	// TABLE_ORDER_RE_PAYMENTS || TABLE_ORDER_AB_PAYMENTS
	// EOF DEFINE USE DOCUMENT TABLES

	$todayDate = date("Y-m-d");
	$todayWeekDay = date("w");
	$todayWeek = date("W");
	#$intervalDay = 10;
	$intervalDay = 8;

	$sleepSeconds = 30; // seconds

	DEFINE('FILENAME_LOGFILE', BASEPATH_CRONJOBS . "logs/" . MANDATOR . "_" . basename($_SERVER["PHP_SELF"]) . ".txt");

	if(file_exists(FILENAME_LOGFILE)){
		unlink(FILENAME_LOGFILE);
	}
	$fp_log = fopen(FILENAME_LOGFILE, "w");

	fwrite($fp_log, date("Y-m-d H:i:s") . ": MANDATOR : " . MANDATOR . " | getMandator : " . $getMandator . "\n");

	DEFINE('MAIL_ADDRESS_FINANCIAL_BCTR', 'finanzen@burhan-ctr.de');
	DEFINE('MAIL_ADDRESS_FINANCIAL_B3', 'finanzen@b3-werbepartner.de');
	DEFINE('MAIL_ADDRESS_FROM_FINANCIAL', constant('MAIL_ADDRESS_FINANCIAL_' . strtoupper(MANDATOR)));

	fwrite($fp_log, date("Y-m-d H:i:s") . ": MAIL_ADDRESS_FROM_FINANCIAL : " . MAIL_ADDRESS_FROM_FINANCIAL . "\n");

	$dbConnection = new DB_Connection();
	$db_open = $dbConnection->db_connect();

	$thisMessage = '';

	// BOF GET UNPAID PREPAYMENT INVOICES AND SEND MAIL INFO
		if(MANDATOR != ''){

			// BOF EXCLUDE CUSTOMER NUMBERS
				if(!empty($arrExcludeCustomerNumbers)){
					$arrSql_excludeCustomerNumbers = array();
					$sql_excludeCustomerNumbers = "";
					foreach($arrExcludeCustomerNumbers as $thisExcludeCustomerNumber){
						$arrSql_excludeCustomerNumbers[] = " AND `" . THIS_TABLE_DOCUMENTS . "`.`orderDocumentsAddressCompanyCustomerNumber` != '" . $thisExcludeCustomerNumber . "' ";
					}

					if(!empty($arrSql_excludeCustomerNumbers)){
						$sql_excludeCustomerNumbers = implode(" ", $arrSql_excludeCustomerNumbers);
					}
				}
			// EOF EXCLUDE CUSTOMER NUMBERS

			$sql = "
				SELECT
					'" . MANDATOR . "' AS `mandator`,
					DATE_FORMAT(NOW(), '%Y-%m-%d') AS `dateToday`,
					DATE_FORMAT(DATE_SUB(NOW(), INTERVAL " . $intervalDay . " DAY), '%Y-%m-%d') AS `dateTodayInterval`,

					DATE_FORMAT(`" . THIS_TABLE_DOCUMENTS . "`.`orderDocumentsDocumentDate`, '%Y-%m-%d') AS `dateDocument`,
					DATE_FORMAT(DATE_ADD(`" . THIS_TABLE_DOCUMENTS . "`.`orderDocumentsDocumentDate`, INTERVAL " . $intervalDay . " DAY), '%Y-%m-%d') AS `dateDocumentInterval`,

					`" . THIS_TABLE_DOCUMENTS . "`.`orderDocumentsID`,
					`" . THIS_TABLE_DOCUMENTS . "`.`orderDocumentsNumber`,
					`" . THIS_TABLE_DOCUMENTS . "`.`orderDocumentsAddressCompanyCustomerNumber`,
					`" . THIS_TABLE_DOCUMENTS . "`.`orderDocumentsProcessingDate`,
					`" . THIS_TABLE_DOCUMENTS . "`.`orderDocumentsDocumentDate`,
					`" . THIS_TABLE_DOCUMENTS . "`.`orderDocumentsStatus`,
					`" . THIS_TABLE_DOCUMENTS . "`.`orderDocumentsTotalPrice`,
					`" . THIS_TABLE_DOCUMENTS . "`.`orderDocumentsDocumentPath`,

					`" . TABLE_PAYMENT_CONDITIONS . "`.`paymentConditionsID`,
					`" . TABLE_PAYMENT_CONDITIONS . "`.`paymentConditionsShortName`,
					`" . TABLE_PAYMENT_CONDITIONS . "`.`paymentConditionsName`,

					`" . TABLE_PAYMENT_TYPES . "`.`paymentTypesID`,
					`" . TABLE_PAYMENT_TYPES . "`.`paymentTypesShortName`,
					`" . TABLE_PAYMENT_TYPES . "`.`paymentTypesName`,

					`" . THIS_TABLE_DOCUMENTS_PAYMENTS . "`.`orderPaymentID`,
					`" . THIS_TABLE_DOCUMENTS_PAYMENTS . "`.`orderPaymentOrderID`,
					`" . THIS_TABLE_DOCUMENTS_PAYMENTS . "`.`orderPaymentOrderNumber`,
					`" . THIS_TABLE_DOCUMENTS_PAYMENTS . "`.`orderPaymentDate`,

					`" . TABLE_PAYMENT_STATUS_TYPES . "`.`paymentStatusTypesID`,
					`" . TABLE_PAYMENT_STATUS_TYPES . "`.`paymentStatusTypesShortName`,
					`" . TABLE_PAYMENT_STATUS_TYPES . "`.`paymentStatusTypesName`,

					`" . TABLE_CUSTOMERS . "`.`customersID`,
					`" . TABLE_CUSTOMERS . "`.`customersKundennummer`,

					`" . TABLE_CUSTOMERS . "`.`customersFirmenname`,
					`" . TABLE_CUSTOMERS . "`.`customersFirmennameZusatz`,

					CONCAT(
						`" . TABLE_CUSTOMERS . "`.`customersMail1`,
						IF(`" . TABLE_CUSTOMERS . "`.`customersMail1` != '' AND `" . TABLE_CUSTOMERS . "`.`customersMail2` != '', ';', ''),
						`" . TABLE_CUSTOMERS . "`.`customersMail2`
					) AS `customersMail`

					/*
					,CONCAT(
						`" . TABLE_CUSTOMERS . "`.`customersMail1`,

						IF(`" . TABLE_CUSTOMERS . "`.`customersMail2` != `" . TABLE_CUSTOMERS . "`.`customersMail1`,
							CONCAT(
								IF(`" . TABLE_CUSTOMERS . "`.`customersMail1` != '' AND `" . TABLE_CUSTOMERS . "`.`customersMail2` != '', ';', ''),
								`" . TABLE_CUSTOMERS . "`.`customersMail2`
							),
							''
						)
					) AS `customersMails`
					*/

					FROM `" . THIS_TABLE_DOCUMENTS . "`

					LEFT JOIN `" . TABLE_PAYMENT_TYPES . "`
					ON(`" . THIS_TABLE_DOCUMENTS . "`.`orderDocumentsPaymentType` = `" . TABLE_PAYMENT_TYPES . "`.`paymentTypesID`)

					LEFT JOIN `" . TABLE_PAYMENT_CONDITIONS . "`
					ON(`" . THIS_TABLE_DOCUMENTS . "`.`orderDocumentsPaymentCondition` = `" . TABLE_PAYMENT_CONDITIONS . "`.`paymentConditionsID`)

					LEFT JOIN `" . TABLE_PAYMENT_STATUS_TYPES . "`
					ON(`" . THIS_TABLE_DOCUMENTS . "`.`orderDocumentsStatus` = `" . TABLE_PAYMENT_STATUS_TYPES . "`.`paymentStatusTypesID`)

					LEFT JOIN `" . THIS_TABLE_DOCUMENTS_PAYMENTS . "`
					ON(`" . THIS_TABLE_DOCUMENTS . "`.`orderDocumentsNumber` = `" . THIS_TABLE_DOCUMENTS_PAYMENTS . "`.`orderPaymentOrderNumber`)

					LEFT JOIN `" . TABLE_CUSTOMERS . "`
					ON(`" . THIS_TABLE_DOCUMENTS . "`.`orderDocumentsAddressCompanyCustomerNumber` = `" . TABLE_CUSTOMERS . "`.`customersKundennummer`)

					WHERE 1
						AND `" . THIS_TABLE_DOCUMENTS . "`.`orderDocumentsPaymentCondition` = 2
						AND `" . THIS_TABLE_DOCUMENTS . "`.`orderDocumentsPaymentType` = 2
						AND `" . THIS_TABLE_DOCUMENTS . "`.`orderDocumentsStatus` = 1
						AND `" . THIS_TABLE_DOCUMENTS . "`.`orderDocumentsTotalPrice` > 0

						" . $sql_excludeCustomerNumbers . "

					HAVING
						`dateToday` > `dateDocumentInterval`
				";

			$sql .= " ORDER BY  `mandator`, `dateDocument` ";
			#$sql .= " LIMIT 3 ";

			#fwrite($fp_log, date("Y-m-d H:i:s") . ": sql : " . $sql . "\n");

			$rs = $dbConnection->db_query($sql);
			if(mysqli_error($db_open)) { fwrite($fp_log, date("Y-m-d H:i:s") . ": " . mysqli_error($db_open) . "\n\n\n"); }

			$countTotalRows = $dbConnection->db_getMysqlNumRows($rs);

			if($countTotalRows > 0) {
				while($ds = mysqli_fetch_assoc($rs)){
					// BOF CREATE MAIL
						$arrTempMails = explode(";", $ds["customersMail"]);
						$sendAttachedDocument = '';
						if(!empty($arrTempMails)){
							$arrTempMails = array_unique($arrTempMails);
							$sendAttachedDocument = '1';
						}

						if($sendAttachedDocument == '1') {
							$selectSubject = 'Zahlungserinnerung für Vorkasse-Rechnung ' . $ds["orderDocumentsNumber"];
							$generatedDocumentNumber = $ds["orderDocumentsNumber"];

							// BOF CREATE SUBJECT
								$thisSubject = $selectSubject;
							// EOF CREATE SUBJECT

							// BOF GET ATTACHED FILE
								$arrAttachmentFiles = array (
									rawurldecode(BASEPATH_Auftragslisten . $ds["orderDocumentsDocumentPath"]) => str_replace(" ", "_", rawurldecode(myUnHtmlEntities(basename($ds["orderDocumentsDocumentPath"]))))
								);
							// EOF GET ATTACHED FILE

							$documentType = 'VK';
							$selectCustomersRecipientName = $ds["customersFirmenname"];
							$selectMailtextTemplates = 'VK';

							$debugCustomersRecipientMail = 'webmaster@burhan-ctr.de';
							$selectCustomersRecipientMail = implode(";", $arrTempMails);
							#$selectCustomersRecipientMail .= ";" . $debugCustomersRecipientMail;
							
							#$selectCustomersRecipientMail = 'webmaster@burhan-ctr.de';

							// 'Beachten Sie bitte, dass bei nicht geleisteter Bezahlung der Vorkasse-Rechnung<br />die kostenlose 10% Mehr-Lieferung für Kennzeichenhalter bzw. Miniletter entfällt und diese Ware extra berechnet wird.' . "\n\n" .

							$sendAttachedMailText =
									'Sehr geehrte Kundin, sehr geehrter Kunde,' . "\n\n" .
									'Sie haben am {###RE_DATUM###} die <b>Vorkasse-Rechnung {###RE_NUMMER###}</b> <br />mit einem Betrag in Höhe von <b>{###RE_SUMME###} EUR</b> erhalten.' . "\n\n" .
									'<b>Diese ist bisher noch nicht gezahlt worden</b>.' . "\n\n" .

									'Beachten Sie bitte, dass bei nicht geleisteter Bezahlung der Vorkasse-Rechnung<br />evtl. angebotene Vorkasse-Aktionen nicht berücksichtigt werden können!' . "\n\n" .

									'Falls Sie die Rechnung zwischenzeitlich bezahlt haben, betrachten Sie diese Mitteilung als hinfällig.' . "\n\n" .'
								';
							$sendAttachedMailText = preg_replace("/{###RE_DATUM###}/", formatDate($ds["orderDocumentsDocumentDate"], "display"), $sendAttachedMailText);
							$sendAttachedMailText = preg_replace("/{###RE_NUMMER###}/", $ds["orderDocumentsNumber"], $sendAttachedMailText);
							$sendAttachedMailText = preg_replace("/{###RE_SUMME###}/", number_format($ds["orderDocumentsTotalPrice"], 2, ",", "."), $sendAttachedMailText);
							$sendAttachedMailText = preg_replace("/{###MANDATOR###}/", strtoupper($ds["mandator"]), $sendAttachedMailText);

							$selectMailtextSender = MAIL_ADDRESS_FROM_FINANCIAL;

							// BOF SEND MAIL
								$createMail = new createMail(
														$thisSubject,													// TEXT:	MAIL_SUBJECT
														$documentType,													// STRING:	DOCUMENT TYPE
														$generatedDocumentNumber,										// STRING:	DOCUMENT NUMBER
														$arrAttachmentFiles,											// ARRAY:	ATTACHMENT FILES
														$selectCustomersRecipientMail,	 								// STRING:	RECIPIENTS
														$arrMailContentDatas[$selectMailtextTemplates],					// MAIL_TEXT_TEMPLATE
														$mailTemplate["html"],											// MAIL_BODY_TEMPLATE
														'',																// STRING:	ADDITIONAL TEXT
														$sendAttachedMailText,											// STRING:	SEND ATTACHED TEXT
														true,															// BOOLEAN:	SEND ATTACHED DOCUMENT
														DIRECTORY_MAIL_IMAGES,											// DIRECTORY_MAIL_IMAGES
														$selectMailtextSender											// STRING: SENDER
													);
								#$createMail->setAttachmentPathPrefix('../');
								$createMailResult = $createMail->returnResult();
								$sendMailToClient = $createMailResult;
								// EOF SEND MAIL

								$thisMessage .= $ds["orderDocumentsNumber"] . ' - KNR ' . $ds["orderDocumentsAddressCompanyCustomerNumber"] . ' | ';
								if(!$sendMailToClient) {
									$thisMessage .= ' Die Mail konnte nicht an ' . $selectCustomersRecipientName . ' (' . $selectCustomersRecipientMail . ') versendet werden! ' . "\n";
								}
								else {
									$thisMessage .= ' Die Mail wurde an ' . $selectCustomersRecipientName . ' (' . $selectCustomersRecipientMail . ') versendet! ' . "\n";
								}

								fwrite($fp_log, date("Y-m-d H:i:s") . ': ' . $thisMessage . "-----\n");
								$thisMessage = '';
							// EOF SEND MAIL
							#exit;
							sleep($sleepSeconds);
						}
					// EOF CREAT MAIL
				}
			}

		}
		else{
			fwrite($fp_log, "'MANDATOR' ist leer!" . "\n");
		}
	// EOF GET UNPAID PREPAYMENT INVOICES AND SEND MAIL INFO

	fwrite($fp_log, "ENDE: " . date("Y-m-d H:i:s") . "\n\n" . str_repeat("#", 100) . "\n");

	if($dbConnection){
		$dbConnection->db_close();
	}
	if($fp_log){
		fclose($fp_log);
	}
?>