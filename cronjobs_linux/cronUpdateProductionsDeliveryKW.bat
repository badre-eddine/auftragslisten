#!/bin/bash
echo off

# REM ---------------------------------------------------------------------------------------------------------------
# REM ---------------------------------------------------------------------------------------------------------------

# REM BOF SET TIMESTAMP AND PARAMS
	CRONJOB_SAVESTAMP_DATETIME=`date +'%Y-%m-%d %H:%M:%S'`

	CRONJOB_SAVESTAMP_HOUR=`date +'%H'`
	CRONJOB_SAVESTAMP_MINUTE=`date +'%M'`
	CRONJOB_SAVESTAMP_SECONDS=`date +'%S'`
	# REM ---------------------------------------------------------------------------------------------------------------
	CRONJOB_SAVESTAMP_YEAR=`date +'%Y'`
	CRONJOB_SAVESTAMP_MONTH=`date +'%m'`
	CRONJOB_SAVESTAMP_DAY=`date +'%d'`
	# REM ---------------------------------------------------------------------------------------------------------------
	CRONJOB_SAVESTAMP="$CRONJOB_SAVESTAMP_YEAR"-"$CRONJOB_SAVESTAMP_MONTH"-"$CRONJOB_SAVESTAMP_DAY"_"$CRONJOB_SAVESTAMP_HOUR"-"$CRONJOB_SAVESTAMP_MINUTE"-"$CRONJOB_SAVESTAMP_SECONDS"
# REM EOF SET TIMESTAMP AND PARAMS

# REM ---------------------------------------------------------------------------------------------------------------
# REM ---------------------------------------------------------------------------------------------------------------

# REM BOF SET FILENAMES
	PATH_Auftragslisten=/var/www/Auftragslisten/
	CRONJOB_DIR_EXECUTE_FILES=/var/www/Auftragslisten/cronjobs_linux/
	CRONJOB_DIR_LOG_FILES="$CRONJOB_DIR_EXECUTE_FILES"logs/
	CRONJOB_DIR_TEMP_FILES="$CRONJOB_DIR_EXECUTE_FILES"tmp/

	# REM -----------------------------------------------------------------------------------------------------------

	PATH_PHP=php
# REM EOF SET FILENAMES

# REM ---------------------------------------------------------------------------------------------------------------
# REM ---------------------------------------------------------------------------------------------------------------

# REM BOF RUN PHP SCRIPT
$PATH_PHP "$CRONJOB_DIR_EXECUTE_FILES"cronUpdateProductionsDeliveryKW.php MANDATOR=bctr >>"$CRONJOB_DIR_LOG_FILES"bctr_cronUpdateProductionsDeliveryKW2.txt 2>&1
$PATH_PHP "$CRONJOB_DIR_EXECUTE_FILES"cronUpdateProductionsDeliveryKW.php MANDATOR=b3 >>"$CRONJOB_DIR_LOG_FILES"b3_cronUpdateProductionsDeliveryKW2.txt 2>&1
# REM EOF RUN PHP SCRIPT

# REM ---------------------------------------------------------------------------------------------------------------

# REM nslookup www.burhan-ctr.de >>C:\Auftragslisten_SERVER\webroot\Auftragslisten\cronjobs\cronUpdateProductionsDeliveryKW.txt
>>"$CRONJOB_DIR_LOG_FILES"cronUpdateProductionsDeliveryKW2.txt 2>&1