<?php
	exit;
	DEFINE('DEBUG_MODE', true); // true | false

	DEFINE('LIMIT_ITEMS', 14);
	DEFINE('ADD_DAYS_TO_INTERVAL', 10);
	DEFINE('ADD_DAYS_TO_INTERVAL_FACTORING', 120);
	#error_reporting(0);
	error_reporting(E_ALL & ~E_NOTICE);
	set_time_limit(0);

	DEFINE('BASEPATH_Auftragslisten', "/var/www/Auftragslisten/");
	#DEFINE('BASEPATH_CRONJOBS', $_SERVER['DOCUMENT_ROOT'] . "/");
	DEFINE('BASEPATH_CRONJOBS', BASEPATH_Auftragslisten . "cronjobs_linux/");

	require_once(BASEPATH_CRONJOBS . 'cronjobsConfig.inc.php');

	DEFINE('FILENAME_LOGFILE', BASEPATH_CRONJOBS . "logs/" . MANDATOR . "_" . basename($_SERVER["PHP_SELF"]) . ".txt");

	if(file_exists(FILENAME_LOGFILE)){
		if(DEBUG_MODE){
			unlink(FILENAME_LOGFILE);
		}
	}
	$fp_log = fopen(FILENAME_LOGFILE, "a+");

	fwrite($fp_log, date("Y-m-d H:i:s") . ": MANDATOR : " . MANDATOR . " | getMandator : " . $getMandator . "\n");

	if(MANDATOR != ''){
		DEFINE('DIRECTORY_DOCUMENTS', BASEPATH_Auftragslisten . 'documents_' . MANDATOR . '/');
		DEFINE('DIRECTORY_ATTACHMENT_DOCUMENTS', DIRECTORY_DOCUMENTS . 'attachmentFiles/');
		DEFINE('PATH_AGB_PDF', DIRECTORY_ATTACHMENT_DOCUMENTS . 'AGB_' . strtoupper(MANDATOR) . '.pdf');
		DEFINE('PATH_VCARD', DIRECTORY_ATTACHMENT_DOCUMENTS . 'contact.vcf');
		DEFINE('DIRECTORY_CREATED_DOCUMENTS_CUSTOMERS', DIRECTORY_DOCUMENTS . 'documentsCreated/customers/_testGeneratedDocument/');

		DEFINE('DIRECTORY_HTML2PDF', BASEPATH_Auftragslisten . 'scriptPackages/html2pdf/');
		DEFINE('DIRECTORY_MAIL_TEMPLATES', BASEPATH_Auftragslisten . 'documents_' . MANDATOR . '/documentTemplates/templatesMail/');
		DEFINE('DIRECTORY_PDF_TEMPLATES', BASEPATH_Auftragslisten . 'documents_' . MANDATOR . '/documentTemplates/templatesPDF/');
		DEFINE('DIRECTORY_PDF_IMAGES', 'documents_' . MANDATOR . '/documentTemplates/templatesPDF/images/');


		DEFINE('USE_DEBITING_DATE', false);

		require_once(BASEPATH_Auftragslisten . 'config/configParams_' . strtolower(MANDATOR) . '.inc.php');
		require_once(BASEPATH_Auftragslisten . 'config/configBasic.inc.php');
		require_once(BASEPATH_Auftragslisten . 'config/configTables.inc.php');
		require_once(BASEPATH_Auftragslisten . 'inc/functions.inc.php');
		require_once(BASEPATH_Auftragslisten . 'classes/DB_Connection.class.php');
		require_once(BASEPATH_Auftragslisten . 'scriptPackages/htmlMimeMail/htmlMimeMail.php');
		require_once(BASEPATH_Auftragslisten . "classes/createMail.class.php");

		require_once(DIRECTORY_PDF_TEMPLATES . '/template_TEXT_DATAS.inc.php');
		require_once(DIRECTORY_MAIL_TEMPLATES . 'template_BASIC.inc.php');
		require_once(DIRECTORY_MAIL_TEMPLATES . 'template_TEXT_DATAS.inc.php');

		require_once(DIRECTORY_HTML2PDF . 'html2pdf.class.php');

		require_once(BASEPATH_Auftragslisten . 'inc/loadCardDatas.inc.php');

		// BOF RUN CRONJOB 05:00 MORNING
		$todayDate = date("Y-m-d");
		$todayDateTime = date("Y-m-d H:i:s");
		$yesterdayDate = date("Y-m-d", mktime(0, 0, 0, date("m"), (date("d") - 1), date("Y")));

		// BOF DEFINE DOCUMENT TYPES TO HANDLE AND SET CONVERSION TYPES
			$arrDocumentsToHandle = array();
			$arrDocumentsToHandle['bctr'] = array(
				"RE" => 'MA',
				"MA" => 'M1',
				"M1" => 'M2'
			);
			$arrDocumentsToHandle['b3'] = array(
				#"RE" => 'MA',
				#"MA" => 'M1',
				#"M1" => 'M2'
			);
		// EOF DEFINE DOCUMENT TYPES TO HANDLE AND SET CONVERSION TYPES

		DEFINE('MAIL_ADDRESS_FINANCIAL_BCTR', 'finanzen@burhan-ctr.de');
		DEFINE('MAIL_ADDRESS_FINANCIAL_B3', 'finanzen@b3-werbepartner.de');
		DEFINE('MAIL_ADDRESS_FROM_FINANCIAL', constant('MAIL_ADDRESS_FINANCIAL_' . strtoupper(MANDATOR)));


		if(DEBUG_MODE){
			fwrite($fp_log, "\n\n\n" . str_repeat("=", 50) .  str_repeat("=", 50) . "\n");
			fwrite($fp_log, date("Y-m-d H:i:s") . ": DEBUG MODUS " . "\n" . str_repeat("=", 50) . "\n");
		}

		fwrite($fp_log, date("Y-m-d H:i:s") . ": MANDATOR : " . MANDATOR . " | getMandator : " . $getMandator . "\n");
		fwrite($fp_log, date("Y-m-d H:i:s") . ": MAIL_ADDRESS_FROM_FINANCIAL : " . MAIL_ADDRESS_FROM_FINANCIAL . "\n");
		fwrite($fp_log, "\n" . str_repeat("-", 50) . "\n");

		$dbConnection = new DB_Connection();
		$db_open = $dbConnection->db_connect();

		$thisMessage = '';

		// BOF READ VERTRETER
			$arrAgentDatas = getAgents();
			$arrAgentDatas2 = getAgents2();
		// EOF READ VERTRETER

		// BOF READ PAYMENT TYPES
			$arrPaymentTypeDatas = getPaymentTypes();
		// EOF READ PAYMENT TYPES

		// BOF READ DOCUMENT TYPE DATAS
			$arrPaymentStatusTypeDatas = getPaymentStatusTypes();
			$arrPaymentStatusTypeDatasToShortName = array();
			if(!empty($arrPaymentStatusTypeDatas)){
				foreach($arrPaymentStatusTypeDatas as $thisKey => $thisValue){
					$arrPaymentStatusTypeDatasToShortName[$thisValue["paymentStatusTypesShortName"]] = $thisKey;
				}
			}
		// EOF READ DOCUMENT TYPE DATAS

		// BOF READ PAYMENT CONDITIONS
			$arrPaymentConditionDatas = getPaymentConditions();
		// EOF READ PAYMENT CONDITIONS

		// BOF READ BESTELL ARTEN
			$arrOrderTypeDatas = getOrderTypes();
		// EOF READ BESTELL ARTEN

		// BOF READ ADDITIONAL COSTS
			$arrAdditionalCostsDatas = getAdditionalCosts();
		// EOF READ ADDITIONAL COSTS

		// BOF READ BANK TYPES
			$arrBankAccountTypeDatas = getBankAccountTypes();
		// EOF READ BANK TYPES

		// BOF READ ORDER CATEGORIES
			$arrOrderCategoriesTypeDatas = getOrderCategories();
		// EOF READ ORDER CATEGORIES

		// BOF READ COUNTRIES
			$arrCountryTypeDatas = getCountryTypes();
		// EOF READ COUNTRIES

		// BOF READ DOCUMENT TYPE DATAS
			$arrDocumentTypeDatas = getDocumentTypes();
		// EOF READ DOCUMENT TYPE DATAS

		// BOF READ CUSTOMER GROUP DATAS
			$arrCustomerGroupDatas = getCustomerGroups();
		// EOF READ CUSTOMER GROUP DATAS

		// BOF READ CUSTOMER TYPES
			$arrCustomerTypeDatas = getCustomerTypes();
		// EOF READ CUSTOMER TYPES


		// BOF GET DOCUMENTS TO HANDLE
			if(!empty($arrDocumentsToHandle[MANDATOR])){

				$arrSqlDocumentsToHandle = array();

				// BOF GET OPEN INVOICES RE FOR CREATING MA
					if(in_array('RE', array_keys($arrDocumentsToHandle[MANDATOR]))){
						$sql_getOpenInvoices = "
								SELECT
									`" . TABLE_ORDER_INVOICES . "`.`orderDocumentsID`,
									`" . TABLE_ORDER_INVOICES . "`.`orderDocumentsNumber`,
									`" . TABLE_RELATED_DOCUMENTS . "`.`relatedDocuments_RE`,
									`" . TABLE_ORDER_INVOICES . "`.`orderDocumentsDocumentDate` AS `orderDocumentsInvoiceDate`,

									`" . TABLE_PAYMENT_STATUS_TYPES . "`.`paymentStatusTypesID`,
									`" . TABLE_PAYMENT_STATUS_TYPES . "`.`paymentStatusTypesShortName`,
									`" . TABLE_PAYMENT_STATUS_TYPES . "`.`paymentStatusTypesName`,

									`" . TABLE_PAYMENT_TYPES . "`.`paymentTypesID`,
									`" . TABLE_PAYMENT_TYPES . "`.`paymentTypesShortName`,
									`" . TABLE_PAYMENT_TYPES . "`.`paymentTypesName`,

									`" . TABLE_PAYMENT_CONDITIONS . "`.`paymentConditionsID`,
									`" . TABLE_PAYMENT_CONDITIONS . "`.`paymentConditionsShortName`,
									`" . TABLE_PAYMENT_CONDITIONS . "`.`paymentConditionsName`,

									IF(`" . TABLE_PAYMENT_CONDITIONS . "`.`paymentConditionsDaysLimit` = 0, DATE_ADD(`" . TABLE_ORDER_INVOICES . "`.`orderDocumentsDocumentDate`, INTERVAL " . (PAYMENT_DEADLINE_RE_DEFAULT + ADD_DAYS_TO_INTERVAL) . " DAY),
									/* IF(`" . TABLE_PAYMENT_CONDITIONS . "`.`paymentConditionsDaysLimit` = 0, DATE_ADD(`" . TABLE_ORDER_INVOICES . "`.`orderDocumentsDocumentDate`, INTERVAL (7 + " . ADD_DAYS_TO_INTERVAL . ") DAY), */
										DATE_ADD(`" . TABLE_ORDER_INVOICES . "`.`orderDocumentsDocumentDate`, INTERVAL (`" . TABLE_PAYMENT_CONDITIONS . "`.`paymentConditionsDaysLimit` + " . ADD_DAYS_TO_INTERVAL . ") DAY)
									) AS `orderDocumentsDeadline`,

									`" . TABLE_ORDER_INVOICES . "`.`orderDocumentsProcessingDate`,
									`" . TABLE_ORDER_INVOICES . "`.`orderDocumentsDocumentDate`,
									`" . TABLE_ORDER_INVOICES . "`.`orderDocumentsOrderDate`,

									IF(`" . TABLE_ORDER_INVOICES . "`.`orderDocumentsType` IS NULL || `" . TABLE_ORDER_INVOICES . "`.`orderDocumentsType` = '', 'RE', `" . TABLE_ORDER_INVOICES . "`.`orderDocumentsType`) AS `orderDocumentsType`,
									`" . TABLE_ORDER_INVOICES . "`.`orderDocumentsOrdersIDs`,
									`" . TABLE_ORDER_INVOICES . "`.`orderDocumentsCustomerNumber`,
									`" . TABLE_ORDER_INVOICES . "`.`orderDocumentsCustomersOrderNumber`,

									`" . TABLE_ORDER_INVOICES . "`.`orderDocumentsSalesman`,
									`" . TABLE_ORDER_INVOICES . "`.`orderDocumentsKommission`,
									`" . TABLE_ORDER_INVOICES . "`.`orderDocumentsAddressCompanyCustomerNumber`,
									`" . TABLE_ORDER_INVOICES . "`.`orderDocumentsAddressCompany`,
									`" . TABLE_ORDER_INVOICES . "`.`orderDocumentsAddressName`,
									`" . TABLE_ORDER_INVOICES . "`.`orderDocumentsAddressStreet`,
									`" . TABLE_ORDER_INVOICES . "`.`orderDocumentsAddressStreetNumber`,
									`" . TABLE_ORDER_INVOICES . "`.`orderDocumentsAddressZipcode`,
									`" . TABLE_ORDER_INVOICES . "`.`orderDocumentsAddressCity`,
									`" . TABLE_ORDER_INVOICES . "`.`orderDocumentsAddressCountry`,
									`" . TABLE_ORDER_INVOICES . "`.`orderDocumentsAddressMail`,
									`" . TABLE_ORDER_INVOICES . "`.`orderDocumentsSumPrice`,
									`" . TABLE_ORDER_INVOICES . "`.`orderDocumentsDiscount`,
									`" . TABLE_ORDER_INVOICES . "`.`orderDocumentsDiscountType`,
									`" . TABLE_ORDER_INVOICES . "`.`orderDocumentsDiscountPercent`,
									`" . TABLE_ORDER_INVOICES . "`.`orderDocumentsMwst`,
									`" . TABLE_ORDER_INVOICES . "`.`orderDocumentsMwstPrice`,
									`" . TABLE_ORDER_INVOICES . "`.`orderDocumentsMwstType`,
									`" . TABLE_ORDER_INVOICES . "`.`orderDocumentsShippingCosts`,
									`" . TABLE_ORDER_INVOICES . "`.`orderDocumentsShippingCostsPackages`,
									`" . TABLE_ORDER_INVOICES . "`.`orderDocumentsShippingCostsPerPackage`,
									`" . TABLE_ORDER_INVOICES . "`.`orderDocumentsPackagingCosts`,
									`" . TABLE_ORDER_INVOICES . "`.`orderDocumentsCashOnDelivery`,
									`" . TABLE_ORDER_INVOICES . "`.`orderDocumentsTotalPrice`,
									`" . TABLE_ORDER_INVOICES . "`.`orderDocumentsBankAccount`,
									`" . TABLE_ORDER_INVOICES . "`.`orderDocumentsPaymentCondition`,
									`" . TABLE_ORDER_INVOICES . "`.`orderDocumentsPaymentType`,
									`" . TABLE_ORDER_INVOICES . "`.`orderDocumentsSkonto`,
									`" . TABLE_ORDER_INVOICES . "`.`orderDocumentsDeliveryType`,
									`" . TABLE_ORDER_INVOICES . "`.`orderDocumentsDeliveryTypeNumber`,
									`" . TABLE_ORDER_INVOICES . "`.`orderDocumentsSubject`,
									`" . TABLE_ORDER_INVOICES . "`.`orderDocumentsContent`,
									`" . TABLE_ORDER_INVOICES . "`.`orderDocumentsDocumentPath`,
									`" . TABLE_ORDER_INVOICES . "`.`orderDocumentsTimestamp`,
									`" . TABLE_ORDER_INVOICES . "`.`orderDocumentsStatus`,
									`" . TABLE_ORDER_INVOICES . "`.`orderDocumentsSendMail`,
									`" . TABLE_ORDER_INVOICES . "`.`orderDocumentsInterestPercent`,
									`" . TABLE_ORDER_INVOICES . "`.`orderDocumentsInterestPrice`,
									`" . TABLE_ORDER_INVOICES . "`.`orderDocumentsChargesPrice`,
									`" . TABLE_ORDER_INVOICES . "`.`orderDocumentsDeadlineDate`,
									`" . TABLE_ORDER_INVOICES . "`.`orderDocumentsCustomerGroupID`,
									`" . TABLE_ORDER_INVOICES . "`.`orderDocumentsIsCollectiveInvoice`,
									`" . TABLE_ORDER_INVOICES . "`.`orderDocumentsStatusChangeDate`,
									`" . TABLE_ORDER_INVOICES . "`.`orderDocumentsInvoiceRecipientData`,
									`" . TABLE_ORDER_INVOICES . "`.`orderDocumentsDeliveryRecipientData`

								FROM `" . TABLE_ORDER_INVOICES . "`

								LEFT JOIN `" . TABLE_RELATED_DOCUMENTS . "`
								ON(`" . TABLE_ORDER_INVOICES . "`.`orderDocumentsNumber` = `" . TABLE_RELATED_DOCUMENTS . "`.`relatedDocuments_RE`)

								LEFT JOIN `" . TABLE_PAYMENT_TYPES . "`
								ON(`" . TABLE_ORDER_INVOICES . "`.`orderDocumentsPaymentType` = `" . TABLE_PAYMENT_TYPES . "`.`paymentTypesID`)

								LEFT JOIN `" . TABLE_PAYMENT_STATUS_TYPES . "`
								ON(`" . TABLE_ORDER_INVOICES . "`.`orderDocumentsStatus` = `" . TABLE_PAYMENT_STATUS_TYPES . "`.`paymentStatusTypesID`)

								LEFT JOIN `" . TABLE_PAYMENT_CONDITIONS . "`
								ON(`" . TABLE_ORDER_INVOICES . "`.`orderDocumentsPaymentCondition` = `" . TABLE_PAYMENT_CONDITIONS . "`.`paymentConditionsID`)

								WHERE 1
									AND `" . TABLE_ORDER_INVOICES . "`.`orderDocumentsStatus` = '1'

									/* BOF EXCLUDE NACHNAHME */
										AND `" . TABLE_ORDER_INVOICES . "`.`orderDocumentsPaymentType` != '3'
									/* EOF EXCLUDE NACHNAHME */

									/* BOF EXCLUDE BANK-ACCOUNT FAKTORING */
										AND `" . TABLE_ORDER_INVOICES . "`.`orderDocumentsBankAccount` != 'FAKT'
									/* EOF EXCLUDE BANK-ACCOUNT FAKTORING */

									AND `" . TABLE_RELATED_DOCUMENTS . "`.`relatedDocuments_GU` IS NULL
									AND `" . TABLE_RELATED_DOCUMENTS . "`.`relatedDocuments_MA` IS NULL
									AND `" . TABLE_RELATED_DOCUMENTS . "`.`relatedDocuments_M1` IS NULL
									AND `" . TABLE_RELATED_DOCUMENTS . "`.`relatedDocuments_M2` IS NULL

								HAVING
									DATE_FORMAT(NOW(), '%Y-%m-%d') > `orderDocumentsDeadline`
							";
						IF(DEBUG_MODE){
							$sql_getOpenInvoices .= " AND `orderDocumentsAddressCompanyCustomerNumber` = '59027' ";
						}
						$arrSqlDocumentsToHandle[] = $sql_getOpenInvoices;
					}
				// EOF GET OPEN INVOICES RE FOR CREATING MA

				// BOF GET OPEN REMINDERS MA FOR CREATING M1
					if(in_array('MA', array_keys($arrDocumentsToHandle[MANDATOR]))){
						$sql_getOpenReminders = "
								SELECT
										`" . TABLE_ORDER_REMINDERS . "`.`orderDocumentsID`,
										`" . TABLE_ORDER_REMINDERS . "`.`orderDocumentsNumber`,
										`" . TABLE_RELATED_DOCUMENTS . "`.`relatedDocuments_RE`,
										`" . TABLE_ORDER_INVOICES . "`.`orderDocumentsDocumentDate` AS `orderDocumentsInvoiceDate`,

										`" . TABLE_PAYMENT_STATUS_TYPES . "`.`paymentStatusTypesID`,
										`" . TABLE_PAYMENT_STATUS_TYPES . "`.`paymentStatusTypesShortName`,
										`" . TABLE_PAYMENT_STATUS_TYPES . "`.`paymentStatusTypesName`,

										`" . TABLE_PAYMENT_TYPES . "`.`paymentTypesID`,
										`" . TABLE_PAYMENT_TYPES . "`.`paymentTypesShortName`,
										`" . TABLE_PAYMENT_TYPES . "`.`paymentTypesName`,

										`" . TABLE_PAYMENT_CONDITIONS . "`.`paymentConditionsID`,
										`" . TABLE_PAYMENT_CONDITIONS . "`.`paymentConditionsShortName`,
										`" . TABLE_PAYMENT_CONDITIONS . "`.`paymentConditionsName`,

										DATE_ADD(`" . TABLE_ORDER_REMINDERS . "`.`orderDocumentsDocumentDate`, INTERVAL (7 + " . ADD_DAYS_TO_INTERVAL . ") DAY) AS `orderDocumentsDeadline`,

										`" . TABLE_ORDER_REMINDERS . "`.`orderDocumentsProcessingDate`,
										`" . TABLE_ORDER_REMINDERS . "`.`orderDocumentsDocumentDate`,
										`" . TABLE_ORDER_REMINDERS . "`.`orderDocumentsOrderDate`,

										IF(`" . TABLE_ORDER_REMINDERS . "`.`orderDocumentsType` IS NULL || `" . TABLE_ORDER_REMINDERS . "`.`orderDocumentsType` = '', 'MA', `" . TABLE_ORDER_REMINDERS . "`.`orderDocumentsType`) AS `orderDocumentsType`,
										`" . TABLE_ORDER_REMINDERS . "`.`orderDocumentsOrdersIDs`,
										`" . TABLE_ORDER_REMINDERS . "`.`orderDocumentsCustomerNumber`,
										`" . TABLE_ORDER_REMINDERS . "`.`orderDocumentsCustomersOrderNumber`,

										`" . TABLE_ORDER_REMINDERS . "`.`orderDocumentsSalesman`,
										`" . TABLE_ORDER_REMINDERS . "`.`orderDocumentsKommission`,
										`" . TABLE_ORDER_REMINDERS . "`.`orderDocumentsAddressCompanyCustomerNumber`,
										`" . TABLE_ORDER_REMINDERS . "`.`orderDocumentsAddressCompany`,
										`" . TABLE_ORDER_REMINDERS . "`.`orderDocumentsAddressName`,
										`" . TABLE_ORDER_REMINDERS . "`.`orderDocumentsAddressStreet`,
										`" . TABLE_ORDER_REMINDERS . "`.`orderDocumentsAddressStreetNumber`,
										`" . TABLE_ORDER_REMINDERS . "`.`orderDocumentsAddressZipcode`,
										`" . TABLE_ORDER_REMINDERS . "`.`orderDocumentsAddressCity`,
										`" . TABLE_ORDER_REMINDERS . "`.`orderDocumentsAddressCountry`,
										`" . TABLE_ORDER_REMINDERS . "`.`orderDocumentsAddressMail`,
										`" . TABLE_ORDER_REMINDERS . "`.`orderDocumentsSumPrice`,
										`" . TABLE_ORDER_REMINDERS . "`.`orderDocumentsDiscount`,
										`" . TABLE_ORDER_REMINDERS . "`.`orderDocumentsDiscountType`,
										`" . TABLE_ORDER_REMINDERS . "`.`orderDocumentsDiscountPercent`,
										`" . TABLE_ORDER_REMINDERS . "`.`orderDocumentsMwst`,
										`" . TABLE_ORDER_REMINDERS . "`.`orderDocumentsMwstPrice`,
										`" . TABLE_ORDER_REMINDERS . "`.`orderDocumentsMwstType`,
										`" . TABLE_ORDER_REMINDERS . "`.`orderDocumentsShippingCosts`,
										`" . TABLE_ORDER_REMINDERS . "`.`orderDocumentsShippingCostsPackages`,
										`" . TABLE_ORDER_REMINDERS . "`.`orderDocumentsShippingCostsPerPackage`,
										`" . TABLE_ORDER_REMINDERS . "`.`orderDocumentsPackagingCosts`,
										`" . TABLE_ORDER_REMINDERS . "`.`orderDocumentsCashOnDelivery`,
										`" . TABLE_ORDER_REMINDERS . "`.`orderDocumentsTotalPrice`,
										`" . TABLE_ORDER_REMINDERS . "`.`orderDocumentsBankAccount`,
										`" . TABLE_ORDER_REMINDERS . "`.`orderDocumentsPaymentCondition`,
										`" . TABLE_ORDER_REMINDERS . "`.`orderDocumentsPaymentType`,
										`" . TABLE_ORDER_REMINDERS . "`.`orderDocumentsSkonto`,
										`" . TABLE_ORDER_REMINDERS . "`.`orderDocumentsDeliveryType`,
										`" . TABLE_ORDER_REMINDERS . "`.`orderDocumentsDeliveryTypeNumber`,
										`" . TABLE_ORDER_REMINDERS . "`.`orderDocumentsSubject`,
										`" . TABLE_ORDER_REMINDERS . "`.`orderDocumentsContent`,
										`" . TABLE_ORDER_REMINDERS . "`.`orderDocumentsDocumentPath`,
										`" . TABLE_ORDER_REMINDERS . "`.`orderDocumentsTimestamp`,
										`" . TABLE_ORDER_REMINDERS . "`.`orderDocumentsStatus`,
										`" . TABLE_ORDER_REMINDERS . "`.`orderDocumentsSendMail`,
										`" . TABLE_ORDER_REMINDERS . "`.`orderDocumentsInterestPercent`,
										`" . TABLE_ORDER_REMINDERS . "`.`orderDocumentsInterestPrice`,
										`" . TABLE_ORDER_REMINDERS . "`.`orderDocumentsChargesPrice`,
										`" . TABLE_ORDER_REMINDERS . "`.`orderDocumentsDeadlineDate`,
										`" . TABLE_ORDER_REMINDERS . "`.`orderDocumentsCustomerGroupID`,
										`" . TABLE_ORDER_REMINDERS . "`.`orderDocumentsIsCollectiveInvoice`,
										`" . TABLE_ORDER_REMINDERS . "`.`orderDocumentsStatusChangeDate`,
										`" . TABLE_ORDER_REMINDERS . "`.`orderDocumentsInvoiceRecipientData`,
										`" . TABLE_ORDER_REMINDERS . "`.`orderDocumentsDeliveryRecipientData`

									FROM `" . TABLE_ORDER_REMINDERS . "`

									LEFT JOIN `" . TABLE_RELATED_DOCUMENTS . "`
									ON(`" . TABLE_ORDER_REMINDERS . "`.`orderDocumentsNumber` = `" . TABLE_RELATED_DOCUMENTS . "`.`relatedDocuments_MA`)

									LEFT JOIN `" . TABLE_ORDER_INVOICES . "`
									ON(`" . TABLE_RELATED_DOCUMENTS . "`.`relatedDocuments_RE` = `" . TABLE_ORDER_INVOICES . "`.`orderDocumentsNumber`)

									LEFT JOIN `" . TABLE_PAYMENT_TYPES . "`
									ON(`" . TABLE_ORDER_REMINDERS . "`.`orderDocumentsPaymentType` = `" . TABLE_PAYMENT_TYPES . "`.`paymentTypesID`)

									LEFT JOIN `" . TABLE_PAYMENT_STATUS_TYPES . "`
									ON(`" . TABLE_ORDER_REMINDERS . "`.`orderDocumentsStatus` = `" . TABLE_PAYMENT_STATUS_TYPES . "`.`paymentStatusTypesID`)

									LEFT JOIN `" . TABLE_PAYMENT_CONDITIONS . "`
									ON(`" . TABLE_ORDER_REMINDERS . "`.`orderDocumentsPaymentCondition` = `" . TABLE_PAYMENT_CONDITIONS . "`.`paymentConditionsID`)

									WHERE 1
										/* BOF EXCLUDE BANK-ACCOUNT FAKTORING */
											AND `" . TABLE_ORDER_REMINDERS . "`.`orderDocumentsBankAccount` != 'FAKT'
										/* EOF EXCLUDE BANK-ACCOUNT FAKTORING */

										AND `" . TABLE_ORDER_REMINDERS . "`.`orderDocumentsStatus` != '2'
										AND `" . TABLE_ORDER_REMINDERS . "`.`orderDocumentsStatus` != '3'
										AND `" . TABLE_ORDER_REMINDERS . "`.`orderDocumentsStatus` != '4'
										AND `" . TABLE_ORDER_REMINDERS . "`.`orderDocumentsStatus` != '10'
										AND `" . TABLE_ORDER_REMINDERS . "`.`orderDocumentsStatus` != '12'
										AND `" . TABLE_RELATED_DOCUMENTS . "`.`relatedDocuments_GU` IS NULL
										AND `" . TABLE_RELATED_DOCUMENTS . "`.`relatedDocuments_MA` IS NOT NULL
										AND `" . TABLE_RELATED_DOCUMENTS . "`.`relatedDocuments_M1` IS NULL
										AND `" . TABLE_RELATED_DOCUMENTS . "`.`relatedDocuments_M2` IS NULL

									HAVING
										DATE_FORMAT(NOW(), '%Y-%m-%d') > `orderDocumentsDeadline`

							";
						IF(DEBUG_MODE){
							$sql_getOpenReminders .= " AND `orderDocumentsAddressCompanyCustomerNumber` = '59027' ";
						}
						$arrSqlDocumentsToHandle[] = $sql_getOpenReminders;
					}
				// EOF GET OPEN REMINDERS MA FOR CREATING M1

				// BOF GET OPEN FIRST DEMANDS M1 FOR CREATING M2
					if(in_array('M1', array_keys($arrDocumentsToHandle[MANDATOR]))){
						$sql_getFirstDemands = "
								SELECT
									`" . TABLE_ORDER_FIRST_DEMANDS . "`.`orderDocumentsID`,
									`" . TABLE_ORDER_FIRST_DEMANDS . "`.`orderDocumentsNumber`,
									`" . TABLE_RELATED_DOCUMENTS . "`.`relatedDocuments_RE`,
									`" . TABLE_ORDER_INVOICES . "`.`orderDocumentsDocumentDate` AS `orderDocumentsInvoiceDate`,

									`" . TABLE_PAYMENT_STATUS_TYPES . "`.`paymentStatusTypesID`,
									`" . TABLE_PAYMENT_STATUS_TYPES . "`.`paymentStatusTypesShortName`,
									`" . TABLE_PAYMENT_STATUS_TYPES . "`.`paymentStatusTypesName`,

									`" . TABLE_PAYMENT_TYPES . "`.`paymentTypesID`,
									`" . TABLE_PAYMENT_TYPES . "`.`paymentTypesShortName`,
									`" . TABLE_PAYMENT_TYPES . "`.`paymentTypesName`,

									`" . TABLE_PAYMENT_CONDITIONS . "`.`paymentConditionsID`,
									`" . TABLE_PAYMENT_CONDITIONS . "`.`paymentConditionsShortName`,
									`" . TABLE_PAYMENT_CONDITIONS . "`.`paymentConditionsName`,

									DATE_ADD(`" . TABLE_ORDER_FIRST_DEMANDS . "`.`orderDocumentsDocumentDate`, INTERVAL (7 + " . ADD_DAYS_TO_INTERVAL . ") DAY) AS `orderDocumentsDeadline`,

									`" . TABLE_ORDER_FIRST_DEMANDS . "`.`orderDocumentsProcessingDate`,
									`" . TABLE_ORDER_FIRST_DEMANDS . "`.`orderDocumentsDocumentDate`,
									`" . TABLE_ORDER_FIRST_DEMANDS . "`.`orderDocumentsOrderDate`,

									IF(`" . TABLE_ORDER_FIRST_DEMANDS . "`.`orderDocumentsType` IS NULL || `" . TABLE_ORDER_FIRST_DEMANDS . "`.`orderDocumentsType` = '', 'M1', `" . TABLE_ORDER_FIRST_DEMANDS . "`.`orderDocumentsType`) AS `orderDocumentsType`,
									`" . TABLE_ORDER_FIRST_DEMANDS . "`.`orderDocumentsOrdersIDs`,
									`" . TABLE_ORDER_FIRST_DEMANDS . "`.`orderDocumentsCustomerNumber`,
									`" . TABLE_ORDER_FIRST_DEMANDS . "`.`orderDocumentsCustomersOrderNumber`,

									`" . TABLE_ORDER_FIRST_DEMANDS . "`.`orderDocumentsSalesman`,
									`" . TABLE_ORDER_FIRST_DEMANDS . "`.`orderDocumentsKommission`,
									`" . TABLE_ORDER_FIRST_DEMANDS . "`.`orderDocumentsAddressCompanyCustomerNumber`,
									`" . TABLE_ORDER_FIRST_DEMANDS . "`.`orderDocumentsAddressCompany`,
									`" . TABLE_ORDER_FIRST_DEMANDS . "`.`orderDocumentsAddressName`,
									`" . TABLE_ORDER_FIRST_DEMANDS . "`.`orderDocumentsAddressStreet`,
									`" . TABLE_ORDER_FIRST_DEMANDS . "`.`orderDocumentsAddressStreetNumber`,
									`" . TABLE_ORDER_FIRST_DEMANDS . "`.`orderDocumentsAddressZipcode`,
									`" . TABLE_ORDER_FIRST_DEMANDS . "`.`orderDocumentsAddressCity`,
									`" . TABLE_ORDER_FIRST_DEMANDS . "`.`orderDocumentsAddressCountry`,
									`" . TABLE_ORDER_FIRST_DEMANDS . "`.`orderDocumentsAddressMail`,
									`" . TABLE_ORDER_FIRST_DEMANDS . "`.`orderDocumentsSumPrice`,
									`" . TABLE_ORDER_FIRST_DEMANDS . "`.`orderDocumentsDiscount`,
									`" . TABLE_ORDER_FIRST_DEMANDS . "`.`orderDocumentsDiscountType`,
									`" . TABLE_ORDER_FIRST_DEMANDS . "`.`orderDocumentsDiscountPercent`,
									`" . TABLE_ORDER_FIRST_DEMANDS . "`.`orderDocumentsMwst`,
									`" . TABLE_ORDER_FIRST_DEMANDS . "`.`orderDocumentsMwstPrice`,
									`" . TABLE_ORDER_FIRST_DEMANDS . "`.`orderDocumentsMwstType`,
									`" . TABLE_ORDER_FIRST_DEMANDS . "`.`orderDocumentsShippingCosts`,
									`" . TABLE_ORDER_FIRST_DEMANDS . "`.`orderDocumentsShippingCostsPackages`,
									`" . TABLE_ORDER_FIRST_DEMANDS . "`.`orderDocumentsShippingCostsPerPackage`,
									`" . TABLE_ORDER_FIRST_DEMANDS . "`.`orderDocumentsPackagingCosts`,
									`" . TABLE_ORDER_FIRST_DEMANDS . "`.`orderDocumentsCashOnDelivery`,
									`" . TABLE_ORDER_FIRST_DEMANDS . "`.`orderDocumentsTotalPrice`,
									`" . TABLE_ORDER_FIRST_DEMANDS . "`.`orderDocumentsBankAccount`,
									`" . TABLE_ORDER_FIRST_DEMANDS . "`.`orderDocumentsPaymentCondition`,
									`" . TABLE_ORDER_FIRST_DEMANDS . "`.`orderDocumentsPaymentType`,
									`" . TABLE_ORDER_FIRST_DEMANDS . "`.`orderDocumentsSkonto`,
									`" . TABLE_ORDER_FIRST_DEMANDS . "`.`orderDocumentsDeliveryType`,
									`" . TABLE_ORDER_FIRST_DEMANDS . "`.`orderDocumentsDeliveryTypeNumber`,
									`" . TABLE_ORDER_FIRST_DEMANDS . "`.`orderDocumentsSubject`,
									`" . TABLE_ORDER_FIRST_DEMANDS . "`.`orderDocumentsContent`,
									`" . TABLE_ORDER_FIRST_DEMANDS . "`.`orderDocumentsDocumentPath`,
									`" . TABLE_ORDER_FIRST_DEMANDS . "`.`orderDocumentsTimestamp`,
									`" . TABLE_ORDER_FIRST_DEMANDS . "`.`orderDocumentsStatus`,
									`" . TABLE_ORDER_FIRST_DEMANDS . "`.`orderDocumentsSendMail`,
									`" . TABLE_ORDER_FIRST_DEMANDS . "`.`orderDocumentsInterestPercent`,
									`" . TABLE_ORDER_FIRST_DEMANDS . "`.`orderDocumentsInterestPrice`,
									`" . TABLE_ORDER_FIRST_DEMANDS . "`.`orderDocumentsChargesPrice`,
									`" . TABLE_ORDER_FIRST_DEMANDS . "`.`orderDocumentsDeadlineDate`,
									`" . TABLE_ORDER_FIRST_DEMANDS . "`.`orderDocumentsCustomerGroupID`,
									`" . TABLE_ORDER_FIRST_DEMANDS . "`.`orderDocumentsIsCollectiveInvoice`,
									`" . TABLE_ORDER_FIRST_DEMANDS . "`.`orderDocumentsStatusChangeDate`,
									`" . TABLE_ORDER_FIRST_DEMANDS . "`.`orderDocumentsInvoiceRecipientData`,
									`" . TABLE_ORDER_FIRST_DEMANDS . "`.`orderDocumentsDeliveryRecipientData`

								FROM `" . TABLE_ORDER_FIRST_DEMANDS . "`

								LEFT JOIN `" . TABLE_RELATED_DOCUMENTS . "`
								ON(`" . TABLE_ORDER_FIRST_DEMANDS . "`.`orderDocumentsNumber` = `" . TABLE_RELATED_DOCUMENTS . "`.`relatedDocuments_M1`)

								LEFT JOIN `" . TABLE_ORDER_INVOICES . "`
								ON(`" . TABLE_RELATED_DOCUMENTS . "`.`relatedDocuments_RE` = `" . TABLE_ORDER_INVOICES . "`.`orderDocumentsNumber`)

								LEFT JOIN `" . TABLE_PAYMENT_TYPES . "`
								ON(`" . TABLE_ORDER_FIRST_DEMANDS . "`.`orderDocumentsPaymentType` = `" . TABLE_PAYMENT_TYPES . "`.`paymentTypesID`)

								LEFT JOIN `" . TABLE_PAYMENT_STATUS_TYPES . "`
								ON(`" . TABLE_ORDER_FIRST_DEMANDS . "`.`orderDocumentsStatus` = `" . TABLE_PAYMENT_STATUS_TYPES . "`.`paymentStatusTypesID`)

								LEFT JOIN `" . TABLE_PAYMENT_CONDITIONS . "`
								ON(`" . TABLE_ORDER_FIRST_DEMANDS . "`.`orderDocumentsPaymentCondition` = `" . TABLE_PAYMENT_CONDITIONS . "`.`paymentConditionsID`)

								WHERE 1

									/* BOF EXCLUDE BANK-ACCOUNT FAKTORING */
										AND `" . TABLE_ORDER_FIRST_DEMANDS . "`.`orderDocumentsBankAccount` != 'FAKT'
									/* EOF EXCLUDE BANK-ACCOUNT FAKTORING */

									AND `" . TABLE_ORDER_FIRST_DEMANDS . "`.`orderDocumentsStatus` != '2'
									AND `" . TABLE_ORDER_FIRST_DEMANDS . "`.`orderDocumentsStatus` != '3'
									AND `" . TABLE_ORDER_FIRST_DEMANDS . "`.`orderDocumentsStatus` != '4'
									AND `" . TABLE_ORDER_FIRST_DEMANDS . "`.`orderDocumentsStatus` != '10'
									AND `" . TABLE_ORDER_FIRST_DEMANDS . "`.`orderDocumentsStatus` != '12'
									AND `" . TABLE_RELATED_DOCUMENTS . "`.`relatedDocuments_GU` IS NULL
									AND `" . TABLE_RELATED_DOCUMENTS . "`.`relatedDocuments_MA` IS NOT NULL
									AND `" . TABLE_RELATED_DOCUMENTS . "`.`relatedDocuments_M1` IS NOT NULL
									AND `" . TABLE_RELATED_DOCUMENTS . "`.`relatedDocuments_M2` IS NULL

								HAVING
									DATE_FORMAT(NOW(), '%Y-%m-%d') > `orderDocumentsDeadline`
							";
							IF(DEBUG_MODE){
								$sql_getFirstDemands .= " AND `orderDocumentsAddressCompanyCustomerNumber` = '59027' ";
							}
						$arrSqlDocumentsToHandle[] = $sql_getFirstDemands;
					}
				// EOF GET OPEN FIRST DEMANDS M1 FOR CREATING M2

				if(!empty($arrSqlDocumentsToHandle)){
					$sql_getDocumentsToHandle = implode(" UNION ", $arrSqlDocumentsToHandle);
					$sql_getDocumentsToHandle .= " ORDER BY `relatedDocuments_RE` DESC ";
					$sql_getDocumentsToHandle .= " LIMIT " . LIMIT_ITEMS . " ";

					$rs_getDocumentsToHandle = $dbConnection->db_query($sql_getDocumentsToHandle);
					if(mysqli_error($db_open)) {
						fwrite($fp_log, date("Y-m-d H:i:s") . ":  sql_getDocumentsToHandle : " . $sql_getDocumentsToHandle . "\n" . "ERROR: " . mysqli_error($db_open) . "\n\n\n");
					}
					if(DEBUG_MODE){
						fwrite($fp_log, date("Y-m-d H:i:s") . ":  sql_getDocumentsToHandle : " . $sql_getDocumentsToHandle . "\n\n");
					}

					$countItems = 0;

					$arrLoadDocumentDatas = array();

					$_POST = array();

					while($ds_getDocumentsToHandle = mysqli_fetch_assoc($rs_getDocumentsToHandle)){
						// BOF SET $_POST AS EMPTY / NEEDED FOR USING OTHER SCRIPTS
							$_POST = array();
						// BOF SET $_POST AS EMPTY
						fwrite($fp_log, "\n" . str_repeat("-", 50) . "\n");
						fwrite($fp_log, ($countItems + 1) . " | START: " . date("Y-m-d H:i:s") . "\n");
						foreach(array_keys($ds_getDocumentsToHandle) as $field){
							#$ds_getDocumentsToHandle[$field] = utf8_decode($ds_getDocumentsToHandle[$field]);
							$ds_getDocumentsToHandle[$field] = html_entity_decode($ds_getDocumentsToHandle[$field]);
						}

						$arrLoadDocumentDatas = $ds_getDocumentsToHandle;
						$todayDate = date("d.m.Y");

						$thisOriginDocNumber = $ds_getDocumentsToHandle["orderDocumentsNumber"];
						// $thisOriginDocType = $ds_getDocumentsToHandle["orderDocumentsType"];
						$thisOriginDocType = substr($ds_getDocumentsToHandle["orderDocumentsNumber"], 0, 2);
						$thisConvertDocType = $arrDocumentsToHandle[MANDATOR][$thisOriginDocType];
						$_POST["convertDocType"] = $thisConvertDocType;
						$thisDocumentID = $ds_getDocumentsToHandle["orderDocumentsID"];

						// BOF SET DOCUMENT TYPE FOR DOCUMENT SUBJECT
							$thisSubject = $arrDocumentTypeDatas[$thisConvertDocType]["createdDocumentsTypesName"];
						// EOF SET DOCUMENT TYPE FOR DOCUMENT SUBJECT

						// BOF CHECK IF RELATED DOCUMENT EXISTS
							$arrRelatedDocumentNumbers = getRelatedDocuments(array($ds_getDocumentsToHandle["orderDocumentsNumber"]), '');
							if($arrRelatedDocumentNumbers[$thisConvertDocType] == ''){

							}
						// EOF CHECK IF RELATED DOCUMENT EXISTS

						// BOF USE INVOICE ADRESS FROM DOCUMENT
							$unserializedOrderDocumentsInvoiceRecipientData = array();
							unset($unserializedOrderDocumentsInvoiceRecipientData);
							if($ds_getDocumentsToHandle["orderDocumentsInvoiceRecipientData"] != ''){
								$unserializedOrderDocumentsInvoiceRecipientData = unserialize($ds_getDocumentsToHandle["orderDocumentsInvoiceRecipientData"]);
								if(!empty($unserializedOrderDocumentsInvoiceRecipientData)){
									foreach($unserializedOrderDocumentsInvoiceRecipientData as $thisKey => $thisValue){
										$unserializedOrderDocumentsInvoiceRecipientData[$thisKey] = utf8_decode($thisValue);
									}
								}
							}

							$unserializedOrderDocumentsDeliveryRecipientData = array();
							unset($unserializedOrderDocumentsDeliveryRecipientData);
							if($ds_getDocumentsToHandle["orderDocumentsDeliveryRecipientData"] != ''){
								$unserializedOrderDocumentsDeliveryRecipientData = unserialize($ds_getDocumentsToHandle["orderDocumentsDeliveryRecipientData"]);
								if(!empty($unserializedOrderDocumentsDeliveryRecipientData)){
									foreach($unserializedOrderDocumentsDeliveryRecipientData as $thisKey => $thisValue){
										$unserializedOrderDocumentsDeliveryRecipientData[$thisKey] = utf8_decode($thisValue);
									}
								}
							}
						// EOF USE INVOICE ADRESS FROM DOCUMENT

						// BOF GET CUSTOMER NUMBERS OF DELIVERY RECIPIENT AND INVOICE RECIPIENT
							$invoiceRecipientCustomerNumber = $unserializedOrderDocumentsInvoiceRecipientData["selectCustomersRecipientCustomerNumber"];
							$deliveryRecipientCustomerNumber = $unserializedOrderDocumentsDeliveryRecipientData["selectCustomersSecondaryRecipientCustomerNumber"];

							if($invoiceRecipientCustomerNumber == ''){
								$invoiceRecipientCustomerNumber = $ds_getDocumentsToHandle["orderDocumentsAddressCompanyCustomerNumber"];
							}
							if($deliveryRecipientCustomerNumber == ''){
								$deliveryRecipientCustomerNumber = $ds_getDocumentsToHandle["orderDocumentsCustomerNumber"];
							}
						// EOF GET CUSTOMER NUMBERS OF DELIVERY RECIPIENT AND INVOICE RECIPIENT

						// BOF GET DOCUMENT DETAILS
							$thisTableOrderDetails = constant("TABLE_ORDER_" . $thisOriginDocType  . "_DETAILS");
							$sql_getDocumentDetails = "
									SELECT
											`orderDocumentDetailID`,
											`orderDocumentDetailDocumentID`,
											`orderDocumentDetailOrderID`,
											`orderDocumentDetailOrderType`,
											`orderDocumentDetailPos`,
											`orderDocumentDetailProductNumber`,
											`orderDocumentDetailProductName`,
											`orderDocumentDetailProductKategorieID`,
											`orderDocumentDetailProductQuantity`,
											`orderDocumentDetailProductColorsText`,
											`orderDocumentDetailProductColorsCount`,
											`orderDocumentDetailProductPrintType`,
											`orderDocumentDetailsWithBorder`,
											`orderDocumentDetailsWithClearPaint`,
											`orderDocumentDetailKommission`,
											`orderDocumentDetailPrintText`,
											`orderDocumentDetailNotiz`,
											`orderDocumentDetailProductSinglePrice`,
											`orderDocumentDetailProductTotalPrice`,
											`orderDocumentDetailMwst`,
											`orderDocumentDetailDiscount`,
											`orderDocumentDetailDirectSale`,
											`orderDocumentDetailPosType`,
											`orderDocumentDetailGroupingID`
										FROM `" . $thisTableOrderDetails . "`
										WHERE 1
											AND `orderDocumentDetailDocumentID` = '" . $thisDocumentID . "'

										ORDER BY `orderDocumentDetailPos`
								";

							$rs_getDocumentDetails = $dbConnection->db_query($sql_getDocumentDetails, $db_open);
							if(mysqli_error($db_open)) {
								fwrite($fp_log, date("Y-m-d H:i:s") . ":  sql_getDocumentDetails : " . $sql_getDocumentDetails . "\n" . "ERROR: " . mysqli_error($db_open) . "\n\n\n");
							}
							if(DEBUG_MODE){
								fwrite($fp_log, date("Y-m-d H:i:s") . ":  sql_getDocumentDetails : " . $sql_getDocumentDetails . "\n");
							}

							$arrDocumentDetails = array();
							$arrLoadDocumentDetailDatas = array();
							$arrLoadDocumentDetailGroupIDs = array();
							while($ds_getDocumentDetails = mysqli_fetch_assoc($rs_getDocumentDetails)){
								foreach(array_keys($ds_getDocumentDetails) as $field) {
									#$ds_getDocumentDetails[$field] = utf8_decode($ds_getDocumentDetails[$field]);
									$ds_getDocumentDetails[$field] = html_entity_decode($ds_getDocumentDetails[$field]);

									$arrLoadDocumentDetailDatas[$ds_getDocumentDetails["orderDocumentDetailOrderID"]][$ds_getDocumentDetails["orderDocumentDetailPosType"]][$field] = ($ds_getDocumentDetails[$field]);
								}
								$arrLoadDocumentDetailGroupIDs[] = $ds_getDocumentDetails["orderDocumentDetailOrderID"];
							}

							if(!empty($arrLoadDocumentDetailGroupIDs)) {
								$arrLoadDocumentDetailGroupIDs = array_unique($arrLoadDocumentDetailGroupIDs);
							}

							$selectOrdersIDs = array();
							$selectOrdersDetails = array();
							if(!empty($arrLoadDocumentDetailDatas)){
								$countItem = 0;

								$selectOrdersIDs = array_keys($arrLoadDocumentDetailDatas);

								foreach($arrLoadDocumentDetailDatas as $thisLoadDocumentDetailDatasKey => $thisLoadDocumentDetailDatasValue) {

									$selectOrdersDetails[$thisLoadDocumentDetailDatasKey]["ordersID"]							= $thisLoadDocumentDetailDatasValue["product"]["orderDocumentDetailOrderID"];
									$selectOrdersDetails[$thisLoadDocumentDetailDatasKey]["ordersArtikelKategorieID"]			= $thisLoadDocumentDetailDatasValue["product"]["orderDocumentDetailProductKategorieID"];
									$selectOrdersDetails[$thisLoadDocumentDetailDatasKey]["ordersArtikelNummer"]				= $thisLoadDocumentDetailDatasValue["product"]["orderDocumentDetailProductNumber"];
									$selectOrdersDetails[$thisLoadDocumentDetailDatasKey]["ordersArtikelBezeichnung"]			= addslashes(preg_replace("/[,][ ]{1,}$/", "", $thisLoadDocumentDetailDatasValue["product"]["orderDocumentDetailProductName"]));

									$thisColorNames = $thisLoadDocumentDetailDatasValue["product"]["orderDocumentDetailProductColorsText"];
									$arrTempColors = explode(' / ', $thisColorNames);
									$arrNewColors = array();
									foreach($arrTempColors as $thisColorName){
										$thisColorName = trim($thisColorName);
										if(preg_match("/(weiss)|(schwarz)|(silber)/", $thisColorName)){
											$thisColorName = preg_replace("/\[Pröll [0-9]{1,5}\]/", "", $thisColorName);
										}
										$arrNewColors[] = $thisColorName;
									}
									if(!empty($arrNewColors)){ $thisColorNames = implode(" / ", $arrNewColors); }
									$selectOrdersDetails[$thisLoadDocumentDetailDatasKey]["ordersArtikelBezeichnungZusatz"]		= $thisColorNames;
									$selectOrdersDetails[$thisLoadDocumentDetailDatasKey]["ordersArtikelMitUmrandung"]			= $thisLoadDocumentDetailDatasValue["product"]["orderDocumentDetailsWithBorder"];
									$selectOrdersDetails[$thisLoadDocumentDetailDatasKey]["ordersArtikelMitKlarlack"]			= $thisLoadDocumentDetailDatasValue["product"]["orderDocumentDetailsWithClearPaint"];
									$selectOrdersDetails[$thisLoadDocumentDetailDatasKey]["ordersAufdruck"]						= $thisLoadDocumentDetailDatasValue["product"]["orderDocumentDetailPrintText"];
									$selectOrdersDetails[$thisLoadDocumentDetailDatasKey]["ordersKommission"]					= $thisLoadDocumentDetailDatasValue["product"]["orderDocumentDetailKommission"];
									$selectOrdersDetails[$thisLoadDocumentDetailDatasKey]["ordersNotiz"]						= $thisLoadDocumentDetailDatasValue["product"]["orderDocumentDetailNotiz"];
									$selectOrdersDetails[$thisLoadDocumentDetailDatasKey]["ordersAdditionalArtikelMenge"]		= $thisLoadDocumentDetailDatasValue["additionalProducts"]["orderDocumentDetailProductQuantity"];
									$selectOrdersDetails[$thisLoadDocumentDetailDatasKey]["ordersAdditionalArtikelKategorieID"]	= $thisLoadDocumentDetailDatasValue["additionalProducts"]["orderDocumentDetailProductKategorieID"];
									$selectOrdersDetails[$thisLoadDocumentDetailDatasKey]["orderPerShippingExpress"]			= $thisLoadDocumentDetailDatasValue["expressCosts"]["orderPerShippingExpress"];
									$selectOrdersDetails[$thisLoadDocumentDetailDatasKey]["orderShippingExpressValue"]			= $thisLoadDocumentDetailDatasValue["expressCosts"]["orderDocumentDetailProductSinglePrice"];
									$selectOrdersDetails[$thisLoadDocumentDetailDatasKey]["ordersOrderType"]					= $thisLoadDocumentDetailDatasValue["product"]["orderDocumentDetailOrderType"];
									$selectOrdersDetails[$thisLoadDocumentDetailDatasKey]["orderPerDirectSale"]					= $thisLoadDocumentDetailDatasValue["product"]["orderDocumentDetailDirectSale"];
									$selectOrdersDetails[$thisLoadDocumentDetailDatasKey]["ordersArtikelMenge"]					= $thisLoadDocumentDetailDatasValue["product"]["orderDocumentDetailProductQuantity"];
									$selectOrdersDetails[$thisLoadDocumentDetailDatasKey]["ordersSinglePreis"]					= $thisLoadDocumentDetailDatasValue["product"]["orderDocumentDetailProductSinglePrice"];
									$selectOrdersDetails[$thisLoadDocumentDetailDatasKey]["ordersArtikelPrintColorsCount"]		= $thisLoadDocumentDetailDatasValue["product"]["orderDocumentDetailProductColorsCount"];
									$selectOrdersDetails[$thisLoadDocumentDetailDatasKey]["ordersAdditionalCostsID"]			= $thisLoadDocumentDetailDatasValue["product"]["orderDocumentDetailProductPrintType"];
									$selectOrdersDetails[$thisLoadDocumentDetailDatasKey]["ordersAdditionalCosts"]				= $thisLoadDocumentDetailDatasValue["printCosts"]["orderDocumentDetailProductSinglePrice"];

									$countItem++;
								}
							}

							/*
							#####dd('selectOrdersIDs');
							#####dd('selectOrdersDetails');

							#####dd('arrRelatedDocumentNumbers');
							#####dd('arrLoadDocumentDatas');
							#####dd('arrLoadDocumentDetailDatas');
							*/
						// EOF GET DOCUMENT DETAILS

						// BOF GET CUSTOMER DATAS
							$customersData = array();
							$sql_getCustomerData = "
								SELECT
										`customersID`,
										`customersKundennummer`,
										`customersFirmenname`,
										`customersFirmennameZusatz`,
										`customersFirmenInhaberVorname`,
										`customersFirmenInhaberNachname`,
										`customersFirmenInhaberAnrede`,
										`customersFirmenInhaber2Vorname`,
										`customersFirmenInhaber2Nachname`,
										`customersFirmenInhaber2Anrede`,
										`customersAnsprechpartner1Vorname`,
										`customersAnsprechpartner1Nachname`,
										`customersAnsprechpartner1Anrede`,
										`customersAnsprechpartner2Vorname`,
										`customersAnsprechpartner2Nachname`,
										`customersAnsprechpartner2Anrede`,
										`customersTelefon1`,
										`customersTelefon2`,
										`customersMobil1`,
										`customersMobil2`,
										`customersFax1`,
										`customersFax2`,
										`customersMail1`,
										`customersMail2`,
										`customersHomepage`,
										`customersLieferadresseFirmenname`,
										`customersLieferadresseFirmennameZusatz`,
										`customersLieferadresseStrasse`,
										`customersLieferadresseHausnummer`,
										`customersLieferadressePLZ`,
										`customersLieferadresseOrt`,
										`customersLieferadresseLand`,
										`customersRechnungsadresseFirmenname`,
										`customersRechnungsadresseFirmennameZusatz`,
										`customersRechnungsadresseStrasse`,
										`customersRechnungsadresseHausnummer`,
										`customersRechnungsadressePLZ`,
										`customersRechnungsadresseOrt`,
										`customersRechnungsadresseLand`,
										`customersBankName`,
										`customersBankKontonummer`,
										`customersBankLeitzahl`,
										`customersBankIBAN`,
										`customersBankBIC`,
										`customersBezahlart`,
										`customersZahlungskondition`,
										`customersRabatt`,
										`customersRabattType`,
										`customersVertreterID`,
										`customersUseSalesmanDeliveryAdress`,
										`customersUseSalesmanInvoiceAdress`,
										`customersTyp`,
										`customersGruppe`,
										`customersNotiz`,
										`customersUserID`,
										`customersTimeCreated`,
										`customersActive`,
										`customersDatasUpdated`,
										`customersTaxAccountID`,
										`customersUstID`,
										`customersInvoicesNotPurchased`,
										`customersBadPaymentBehavior`,

										`customersSepaExists`,
										`customersSepaValidUntilDate`,
										`customersSepaRequestedDate`,
										`customersCompanyNotExists`,
										`customersPriceListsID`,

										`customersOriginalVertreterID`,

										`customersGetNoCollectedInvoice`,

										`customersNoDeliveries`,

										`customersSendPaperInvoice`,

										`customersSendNoFaktoringInfo`,

										`customersGroupCompanyNumber`

									FROM `" . TABLE_CUSTOMERS . "`
									WHERE 1
										AND `customersKundennummer` = '". $deliveryRecipientCustomerNumber. "'
									LIMIT 1
								";
							$rs_getCustomerData = $dbConnection->db_query($sql_getCustomerData, $db_open);
							if(mysqli_error($db_open)) {
								fwrite($fp_log, date("Y-m-d H:i:s") . ":  sql_getCustomerData : " . $sql_getCustomerData . "\n" . "ERROR: " . mysqli_error($db_open) . "\n\n\n");
							}
							if(DEBUG_MODE){
								fwrite($fp_log, date("Y-m-d H:i:s") . ":  sql_getCustomerData : " . $sql_getCustomerData . "\n" );
							}
							list(
								$customersData["customersID"],
								$customersData["Kundennummer"],
								$customersData["Firmenname"],
								$customersData["FirmennameZusatz"],
								$customersData["FirmenInhaberVorname"],
								$customersData["FirmenInhaberNachname"],
								$customersData["FirmenInhaberAnrede"],
								$customersData["FirmenInhaber2Vorname"],
								$customersData["FirmenInhaber2Nachname"],
								$customersData["FirmenInhaber2Anrede"],
								$customersData["Ansprechpartner1Vorname"],
								$customersData["Ansprechpartner1Nachname"],
								$customersData["Ansprechpartner1Anrede"],
								$customersData["Ansprechpartner2Vorname"],
								$customersData["Ansprechpartner2Nachname"],
								$customersData["Ansprechpartner2Anrede"],
								$customersData["Telefon1"],
								$customersData["Telefon2"],
								$customersData["Mobil1"],
								$customersData["Mobil2"],
								$customersData["Fax1"],
								$customersData["Fax2"],
								$customersData["Mail1"],
								$customersData["Mail2"],
								$customersData["Homepage"],
								$customersData["LieferadresseFirmenname"],
								$customersData["LieferadresseFirmennameZusatz"],
								$customersData["LieferadresseStrasse"],
								$customersData["LieferadresseHausnummer"],
								$customersData["LieferadressePLZ"],
								$customersData["LieferadresseOrt"],
								$customersData["LieferadresseLand"],
								$customersData["RechnungsadresseFirmenname"],
								$customersData["RechnungsadresseFirmennameZusatz"],
								$customersData["RechnungsadresseStrasse"],
								$customersData["RechnungsadresseHausnummer"],
								$customersData["RechnungsadressePLZ"],
								$customersData["RechnungsadresseOrt"],
								$customersData["RechnungsadresseLand"],
								$customersData["BankName"],
								$customersData["BankKontonummer"],
								$customersData["BankLeitzahl"],
								$customersData["BankIBAN"],
								$customersData["BankBIC"],
								$customersData["Bezahlart"],
								$customersData["Zahlungskondition"],
								$customersData["Rabatt"],
								$customersData["RabattType"],
								$customersData["VertreterID"],
								$customersData["UseSalesmanDeliveryAdress"],
								$customersData["UseSalesmanInvoiceAdress"],
								$customersData["Typ"],
								$customersData["Gruppe"],
								$customersData["Notiz"],
								$customersData["UserID"],
								$customersData["TimeCreated"],
								$customersData["Active"],
								$customersData["DatasUpdated"],
								$customersData["TaxAccountID"],
								$customersData["UstID"],
								$customersData["InvoicesNotPurchased"],
								$customersData["BadPaymentBehavior"],

								$customersData["SepaExists"],
								$customersData["SepaValidUntilDate"],
								$customersData["SepaRequestedDate"],
								$customersData["CompanyNotExists"],
								$customersData["PriceListsID"],

								$customersData["OriginalVertreterID"],

								$customersData["GetNoCollectedInvoice"],

								$customersData["NoDeliveries"],

								$customersData["SendPaperInvoice"],

								$customersData["SendNoFaktoringInfo"],

								$customersData["GroupCompanyNumber"]

							) = mysqli_fetch_array($rs_getCustomerData);

							/*
							$customersData["Manager"] = $customersData["FirmenInhaberVorname"] . ' ' . $customersData["FirmenInhaberNachname"];
							if($customersData["FirmenInhaber2Nachname"] != '') {
								$customersData["Manager"] .= ' & ' . $customersData["FirmenInhaber2Vorname"] . ' ' . $customersData["FirmenInhaber2Nachname"];
							}
							$customersData["Contact"] = $customersData["Ansprechpartner1Vorname"] . ' ' . $customersData["Ansprechpartner1Nachname"];

							if(!empty($customersData)) {
								foreach($customersData as $thisKey => $thisvalue) {
									$customersData[$thisKey] = ($thisvalue);
								}
							}
							*/
						// EOF GET CUSTOMER DATAS

						// BOF GET SALESMAN ADRESS
							$salesmanDatas = array();
							if($customersData["VertreterID"] != '' || $customersData["UseSalesmanDeliveryAdress"] == '1' || $customersData["UseSalesmanInvoiceAdress"] == '1') {
								$thisSalesmanID = $customersData["VertreterID"];
								if($arrLoadDocumentDatas["orderDocumentsSalesman"] > 0) {
									$thisSalesmanID = $arrLoadDocumentDatas["orderDocumentsSalesman"];
									$sql_getSalesmanData = "
										SELECT
												`customersID`,
												`customersKundennummer`,
												`customersFirmenname`,
												`customersFirmennameZusatz`,
												`customersFirmenInhaberVorname`,
												`customersFirmenInhaberNachname`,
												`customersFirmenInhaberAnrede`,

												`customersAnsprechpartner1Vorname`,
												`customersAnsprechpartner1Nachname`,
												`customersAnsprechpartner1Anrede`,
												`customersAnsprechpartner2Vorname`,
												`customersAnsprechpartner2Nachname`,
												`customersAnsprechpartner2Anrede`,

												`customersCompanyStrasse`,
												`customersCompanyHausnummer`,
												`customersCompanyCountry`,
												`customersCompanyPLZ`,
												`customersCompanyOrt`,
												IF(`customersMail1` != '', `customersMail1`, `customersMail2`) AS `customersMail`,

												IF(`customersMail1` != '', `customersMail1`, '') AS `customersMail1`,
												IF(`customersMail2` != '', `customersMail2`, '') AS `customersMail2`,

												IF(`customersFax1` != '', `customersFax1`, `customersFax2`) AS `customersFax`,

												`customersLieferadresseFirmenname`,
												`customersLieferadresseFirmennameZusatz`,
												`customersLieferadresseStrasse`,
												`customersLieferadresseHausnummer`,
												`customersLieferadressePLZ`,
												`customersLieferadresseOrt`,
												`customersLieferadresseLand`,
												`customersRechnungsadresseFirmenname`,
												`customersRechnungsadresseFirmennameZusatz`,
												`customersRechnungsadresseStrasse`,
												`customersRechnungsadresseHausnummer`,
												`customersRechnungsadressePLZ`,
												`customersRechnungsadresseOrt`,
												`customersRechnungsadresseLand`,
												`customersDatasUpdated`,

												`customersBankName`,
												`customersBankKontonummer`,
												`customersBankLeitzahl`,
												`customersBankIBAN`,
												`customersBankBIC`,

												`customersBezahlart`,
												`customersZahlungskondition`,
												`customersRabatt`,
												`customersRabattType`,
												`customersTyp`,

												`customersTaxAccountID`,
												`customersUstID`,

												`customersInvoicesNotPurchased`,
												`customersBadPaymentBehavior`,

												`customersSepaExists`,
												`customersSepaValidUntilDate`,
												`customersSepaRequestedDate`,
												`customersCompanyNotExists`,
												`customersPriceListsID`,

												`customersOriginalVertreterID`,

												`customersGetNoCollectedInvoice`,

												`customersNoDeliveries`,

												`customersSendPaperInvoice`,

												`customersSendNoFaktoringInfo`,

												`customersGroupCompanyNumber`


											FROM `" . TABLE_CUSTOMERS . "`

											WHERE `customersID` = '".$thisSalesmanID."'

											LIMIT 1
										";
									$rs_getSalesmanData = $dbConnection->db_query($sql_getSalesmanData, $db_open);
									if(mysqli_error($db_open)) {
										fwrite($fp_log, date("Y-m-d H:i:s") . ":  sql_getSalesmanData : " . $sql_getSalesmanData . "\n" . "ERROR: " . mysqli_error($db_open) . "\n\n\n");
									}
									if(DEBUG_MODE){
										fwrite($fp_log, date("Y-m-d H:i:s") . ":  sql_getSalesmanData : " . $sql_getSalesmanData . "\n" );
									}

									$salesmanDatas = array();
									while($ds_getSalesmanData = mysqli_fetch_assoc($rs_getSalesmanData)) {

										$salesmanDatas["salesmanID"] = $ds_getSalesmanData["customersID"];
										$salesmanDatas["salesmanKundennummer"] = $ds_getSalesmanData["customersKundennummer"];
										$salesmanDatas["salesmanTaxAccountID"] = $ds_getSalesmanData["customersTaxAccountID"];
										$salesmanDatas["salesmanUstID"] = $ds_getSalesmanData["customersUstID"];
										$salesmanDatas["salesmanInvoicesNotPurchased"] = $ds_getSalesmanData["customersInvoicesNotPurchased"];
										$salesmanDatas["salesmanBadPaymentBehavior"] = $ds_getSalesmanData["customersBadPaymentBehavior"];
										$salesmanDatas["salesmanFirmenname"] = $ds_getSalesmanData["customersFirmenname"];
										$salesmanDatas["salesmanFirmennameZusatz"] = $ds_getSalesmanData["customersFirmennameZusatz"];
										$salesmanDatas["salesmanMail"] = $ds_getSalesmanData["customersMail"];

										$salesmanDatas["salesmanMail1"] = $ds_getSalesmanData["customersMail1"];
										$salesmanDatas["salesmanMail2"] = $ds_getSalesmanData["customersMail2"];

										$salesmanDatas["salesmanFax"] = $ds_getSalesmanData["customersFax"];
										$salesmanDatas["salesmanBezahlart"] = $ds_getSalesmanData["customersBezahlart"];
										$salesmanDatas["salesmanZahlungskondition"] = $ds_getSalesmanData["customersZahlungskondition"];
										$salesmanDatas["salesmanRabatt"] = $ds_getSalesmanData["customersRabatt"];
										$salesmanDatas["salesmanRabattType"] = $ds_getSalesmanData["customersRabattType"];
										$salesmanDatas["salesmanTyp"] = $ds_getSalesmanData["customersTyp"];
										$salesmanDatas["salesmanManager"] = $ds_getSalesmanData["customersFirmenInhaberVorname"] . ' ' . $ds_getSalesmanData["customersFirmenInhaberNachname"];
										$salesmanDatas["salesmanContact"] = $ds_getSalesmanData["customersAnsprechpartner1Vorname"] . ' ' . $ds_getSalesmanData["customersAnsprechpartner1Nachname"];

										$salesmanDatas["salesmanSepaExists"] = $ds_getSalesmanData["customersSepaExists"];
										$salesmanDatas["salesmanSepaValidUntilDate"] = $ds_getSalesmanData["customersSepaValidUntilDate"];
										$salesmanDatas["salesmanSepaRequestedDate"] = $ds_getSalesmanData["customersSepaRequestedDate"];

										####
										$salesmanDatas["salesmanCompanyNotExists"] = $ds_getSalesmanData["customersCompanyNotExists"];
										$salesmanDatas["salesmanPriceListsID"] = $ds_getSalesmanData["customersPriceListsID"];

										$salesmanDatas["salesmanOriginalVertreterID"] = $ds_getSalesmanData["customersOriginalVertreterID"];

										$salesmanDatas["salesmanGetNoCollectedInvoice"] = $ds_getSalesmanData["customersGetNoCollectedInvoice"];

										$salesmanDatas["salesmanNoDeliveries"] = $ds_getSalesmanData["customersNoDeliveries"];

										$salesmanDatas["salesmanSendPaperInvoice"] = $ds_getSalesmanData["customersSendPaperInvoice"];
										$salesmanDatas["salesmanSendNoFaktoringInfo"] = $ds_getSalesmanData["customersSendNoFaktoringInfo"];

										$salesmanDatas["salesmanGroupCompanyNumber"] = $ds_getSalesmanData["customersGroupCompanyNumber"];

										if($arrLoadDocumentDatas["orderDocumentsAddressCompanyCustomerNumber"] == $salesmanDatas["salesmanKundennummer"]){
											$customersData["UseSalesmanInvoiceAdress"] = '1';
										}

										if($customersData["UseSalesmanDeliveryAdress"] == '1') {
											$salesmanDatas["customersLieferadresseFirmenname"] = $ds_getSalesmanData["customersLieferadresseFirmenname"];
											$salesmanDatas["customersLieferadresseFirmennameZusatz"] = $ds_getSalesmanData["customersLieferadresseFirmennameZusatz"];
											$salesmanDatas["customersLieferadresseStrasse"] = $ds_getSalesmanData["customersLieferadresseStrasse"];
											$salesmanDatas["customersLieferadresseHausnummer"] = $ds_getSalesmanData["customersLieferadresseHausnummer"];
											$salesmanDatas["customersLieferadressePLZ"] = $ds_getSalesmanData["customersLieferadressePLZ"];
											$salesmanDatas["customersLieferadresseOrt"] = $ds_getSalesmanData["customersLieferadresseOrt"];
											$salesmanDatas["customersLieferadresseLand"] = $ds_getSalesmanData["customersLieferadresseLand"];
										}
										if($customersData["UseSalesmanInvoiceAdress"] == '1') {
											$salesmanDatas["customersRechnungsadresseFirmenname"] = $ds_getSalesmanData["customersRechnungsadresseFirmenname"];
											$salesmanDatas["customersRechnungsadresseFirmennameZusatz"] = $ds_getSalesmanData["customersRechnungsadresseFirmennameZusatz"];
											$salesmanDatas["customersRechnungsadresseStrasse"] = $ds_getSalesmanData["customersRechnungsadresseStrasse"];
											$salesmanDatas["customersRechnungsadresseHausnummer"] = $ds_getSalesmanData["customersRechnungsadresseHausnummer"];
											$salesmanDatas["customersRechnungsadressePLZ"] = $ds_getSalesmanData["customersRechnungsadressePLZ"];
											$salesmanDatas["customersRechnungsadresseOrt"] = $ds_getSalesmanData["customersRechnungsadresseOrt"];
											$salesmanDatas["customersRechnungsadresseLand"] = $ds_getSalesmanData["customersRechnungsadresseLand"];

											$salesmanDatas["customersBankName"] = $ds_getSalesmanData["customersBankName"];
											$salesmanDatas["customersBankKontonummer"] = $ds_getSalesmanData["customersBankKontonummer"];
											$salesmanDatas["customersBankLeitzahl"] = $ds_getSalesmanData["customersBankLeitzahl"];
											$salesmanDatas["customersBankIBAN"] = $ds_getSalesmanData["customersBankIBAN"];
											$salesmanDatas["customersBankBIC"] = $ds_getSalesmanData["customersBankBIC"];
											$salesmanDatas["customersTaxAccountID"] = $ds_getSalesmanData["customersTaxAccountID"];
											$salesmanDatas["customersUstID"] = $ds_getSalesmanData["customersUstID"];

											$salesmanDatas["customersSepaExists"] = $ds_getSalesmanData["customersSepaExists"];
											$salesmanDatas["customersSepaValidUntilDate"] = $ds_getSalesmanData["customersSepaValidUntilDate"];
											$salesmanDatas["customersSepaRequestedDate"] = $ds_getSalesmanData["customersSepaRequestedDate"];

											$salesmanDatas["customersPriceListsID"] = $ds_getSalesmanData["customersPriceListsID"];
											$salesmanDatas["customersSendPaperInvoice"] = $ds_getSalesmanData["customersSendPaperInvoice"];
											$salesmanDatas["customersSendNoFaktoringInfo"] = $ds_getSalesmanData["customersSendNoFaktoringInfo"];
										}
									}
								}
							}
						// EOF GET SALESMAN ADRESS

						// BOF GET MAIL RECIPIENT
							$invoiceRecipientCustomerMail = trim($unserializedOrderDocumentsInvoiceRecipientData["selectCustomersRecipientMail"]);

							if($invoiceRecipientCustomerMail == ''){
								if(trim($customersData["Mail1"]) != "" && trim($customersData["Mail2"]) != "") {
									$invoiceRecipientCustomerMail = $customersData["Mail1"] . ';' . $customersData["Mail2"];
								}
								else if(trim($customersData["Mail1"]) != "") {
									$invoiceRecipientCustomerMail = $customersData["Mail1"];
								}
								else if(trim($customersData["Mail2"]) != "") {
									$invoiceRecipientCustomerMail = $customersData["Mail2"];
								}
								else {
									$invoiceRecipientCustomerMail = '';
								}

								if($customersData["UseSalesmanInvoiceAdress"] == '1') {
									if(trim($salesmanDatas["salesmanMail1"]) != "" && trim($salesmanDatas["salesmanMail2"]) != "") {
										$invoiceRecipientCustomerMail = $salesmanDatas["salesmanMail1"] . ';' . $salesmanDatas["salesmanMail2"];
									}
									else if(trim($salesmanDatas["salesmanMail1"]) != "") {
										$invoiceRecipientCustomerMail = $salesmanDatas["salesmanMail1"];
									}
									else if(trim($salesmanDatas["salesmanMail2"]) != "") {
										$invoiceRecipientCustomerMail = $salesmanDatas["salesmanMail2"];
									}
									else {
										$invoiceRecipientCustomerMail = '';
									}
								}
							}
							fwrite($fp_log, "invoiceRecipientCustomerMail: " . $invoiceRecipientCustomerMail . "\n");
						// EOF GET MAIL RECIPIENT

						// BOF USE FAKTORING-INFO
							$thisSendNoFaktoringInfo = $customersData["SendNoFaktoringInfo"];
							if($customersData["UseSalesmanInvoiceAdress"] == '1') {
								$thisSendNoFaktoringInfo = $salesmanDatas["salesmanSendNoFaktoringInfo"];
							}
						// EOF USE FAKTORING-INFO

						// BOF GET THIS DOCUMENT'S DATA
							// BOF DOCUMENT RECIPIENT
								$selectCustomersRecipientCustomerNumber = '';

								$selectCustomersRecipientName = '';
								$selectCustomersRecipientNameAdd = '';

								$selectCustomersRecipientManager = '';
								$selectCustomersRecipientContact = '';

								$selectCustomersRecipientStreet = '';
								$selectCustomersRecipientStreetNumber = '';

								$selectCustomersRecipientZipcode = '';
								$selectCustomersRecipientCity = '';
								$selectCustomersRecipientCountry = '';

								$selectCustomersRecipientMail = '';
								$selectCustomersRecipientFax = '';

								if(!empty($unserializedOrderDocumentsInvoiceRecipientData)){
									$selectCustomersRecipientCustomerNumber = $unserializedOrderDocumentsInvoiceRecipientData["selectCustomersRecipientCustomerNumber"];

									$selectCustomersRecipientName = $unserializedOrderDocumentsInvoiceRecipientData["selectCustomersRecipientName"];
									$selectCustomersRecipientNameAdd = $unserializedOrderDocumentsInvoiceRecipientData["selectCustomersRecipientNameAdd"];

									$selectCustomersRecipientManager = $unserializedOrderDocumentsInvoiceRecipientData["selectCustomersRecipientManager"];
									$selectCustomersRecipientContact = $unserializedOrderDocumentsInvoiceRecipientData["selectCustomersRecipientContact"];

									$selectCustomersRecipientStreet = $unserializedOrderDocumentsInvoiceRecipientData["selectCustomersRecipientStreet"];
									$selectCustomersRecipientStreetNumber = $unserializedOrderDocumentsInvoiceRecipientData["selectCustomersRecipientStreetNumber"];

									$selectCustomersRecipientZipcode = $unserializedOrderDocumentsInvoiceRecipientData["selectCustomersRecipientZipcode"];
									$selectCustomersRecipientCity = $unserializedOrderDocumentsInvoiceRecipientData["selectCustomersRecipientCity"];
									$selectCustomersRecipientCountry = $unserializedOrderDocumentsInvoiceRecipientData["selectCustomersRecipientCountry"];

									$selectCustomersRecipientMail = $unserializedOrderDocumentsInvoiceRecipientData["selectCustomersRecipientMail"];
									$selectCustomersRecipientFax = $unserializedOrderDocumentsInvoiceRecipientData["selectCustomersRecipientFax"];
								}
								else {
									if($customersData["UseSalesmanInvoiceAdress"] == '1') {
										$selectCustomersRecipientMail = $salesmanDatas["salesmanMail"];
										$selectCustomersRecipientMailCopy = '';
										$selectCustomersRecipientCustomerNumber = $salesmanDatas["salesmanKundennummer"];
										$selectCustomersRecipientName = $salesmanDatas["customersRechnungsadresseFirmenname"];
										$selectCustomersRecipientNameAdd = $salesmanDatas["customersRechnungsadresseFirmennameZusatz"];
										$selectCustomersRecipientManager = '';
										$selectCustomersRecipientContact = '';
										$selectCustomersRecipientFax = $salesmanDatas["salesmanFax"];
										$selectCustomersRecipientStreet = $salesmanDatas["customersRechnungsadresseStrasse"];
										$selectCustomersRecipientStreetNumber = $salesmanDatas["customersRechnungsadresseHausnummer"];
										$selectCustomersRecipientZipcode = $salesmanDatas["customersRechnungsadressePLZ"];
										$selectCustomersRecipientCity = $salesmanDatas["customersRechnungsadresseOrt"];
										$selectCustomersRecipientCountry = $salesmanDatas["customersRechnungsadresseLand"];

									}
									else {
										$selectCustomersRecipientMail = $customersData["customersMail1"] . ';' . $customersData["customersMail2"];
										$selectCustomersRecipientMailCopy = '';
										$selectCustomersRecipientCustomerNumber = $customersData["Kundennummer"];
										$selectCustomersRecipientName = $customersData["RechnungsadresseFirmenname"];
										$selectCustomersRecipientNameAdd = $customersData["RechnungsadresseFirmennameZusatz"];
										$selectCustomersRecipientManager = '';
										$selectCustomersRecipientContact = '';
										$selectCustomersRecipientFax = $customersData["Fax1"];
										$selectCustomersRecipientStreet = $customersData["RechnungsadresseStrasse"];
										$selectCustomersRecipientStreetNumber = $customersData["RechnungsadresseHausnummer"];
										$selectCustomersRecipientZipcode = $customersData["RechnungsadressePLZ"];
										$selectCustomersRecipientCity = $customersData["RechnungsadresseOrt"];
										$selectCustomersRecipientCountry = $customersData["RechnungsadresseLand"];
									}

								}
							// EOF DOCUMENT RECIPIENT

							// BOF DELIVERY RECIPIENT
								$selectCustomersSecondaryRecipientCustomerNumber = '';

								$selectCustomersSecondaryRecipientName = '';
								$selectCustomersSecondaryRecipientNameAdd = '';

								$selectCustomersSecondaryRecipientManager = '';

								$selectCustomersSecondaryRecipientStreet = '';
								$selectCustomersSecondaryRecipientStreetNumber = '';

								$selectCustomersSecondaryRecipientZipcode = '';
								$selectCustomersSecondaryRecipientCity = '';
								$selectCustomersSecondaryRecipientCountry = '';

								if(!empty($unserializedOrderDocumentsDeliveryRecipientData)){
									$selectCustomersSecondaryRecipientCustomerNumber = $unserializedOrderDocumentsDeliveryRecipientData["selectCustomersSecondaryRecipientCustomerNumber"];

									$selectCustomersSecondaryRecipientName = $unserializedOrderDocumentsDeliveryRecipientData["selectCustomersSecondaryRecipientName"];
									$selectCustomersSecondaryRecipientNameAdd = $unserializedOrderDocumentsDeliveryRecipientData["selectCustomersSecondaryRecipientNameAdd"];

									$selectCustomersSecondaryRecipientManager = $unserializedOrderDocumentsDeliveryRecipientData["selectCustomersSecondaryRecipientManager"];

									$selectCustomersSecondaryRecipientStreet = $unserializedOrderDocumentsDeliveryRecipientData["selectCustomersSecondaryRecipientStreet"];
									$selectCustomersSecondaryRecipientStreetNumber = $unserializedOrderDocumentsDeliveryRecipientData["selectCustomersSecondaryRecipientStreetNumber"];

									$selectCustomersSecondaryRecipientZipcode = $unserializedOrderDocumentsDeliveryRecipientData["selectCustomersSecondaryRecipientZipcode"];
									$selectCustomersSecondaryRecipientCity = $unserializedOrderDocumentsDeliveryRecipientData["selectCustomersSecondaryRecipientCity"];
									$selectCustomersSecondaryRecipientCountry = $unserializedOrderDocumentsDeliveryRecipientData["selectCustomersSecondaryRecipientCountry"];
								}
								else {
									if($customersData["UseSalesmanDeliveryAdress"] == '1') {
										$selectCustomersSecondaryRecipientMail = $salesmanDatas["salesmanMail"];
										$selectCustomersSecondaryRecipientMailCopy = '';
										$selectCustomersSecondaryRecipientCustomerNumber = $salesmanDatas["salesmanKundennummer"];
										$selectCustomersSecondaryRecipientName = $salesmanDatas["customersLieferadresseFirmenname"];
										$selectCustomersSecondaryRecipientNameAdd = $salesmanDatas["customersLieferadresseFirmennameZusatz"];
										$selectCustomersSecondaryRecipientManager = '';
										$selectCustomersSecondaryRecipientContact = '';
										$selectCustomersSecondaryRecipientFax = $salesmanDatas["salesmanFax"];
										$selectCustomersSecondaryRecipientStreet = $salesmanDatas["customersLieferadresseStrasse"];
										$selectCustomersSecondaryRecipientStreetNumber = $salesmanDatas["customersLieferadresseHausnummer"];
										$selectCustomersSecondaryRecipientZipcode = $salesmanDatas["customersLieferadressePLZ"];
										$selectCustomersSecondaryRecipientCity = $salesmanDatas["customersLieferadresseOrt"];
										$selectCustomersSecondaryRecipientCountry = $salesmanDatas["customersLieferadresseLand"];
									}
									else{
										$selectCustomersSecondaryRecipientMail = $customersData["customersMail1"] . ';' . $customersData["customersMail2"];
										$selectCustomersSecondaryRecipientMailCopy = '';
										$selectCustomersSecondaryRecipientCustomerNumber = $customersData["Kundennummer"];
										$selectCustomersSecondaryRecipientName = $customersData["LieferadresseFirmenname"];
										$selectCustomersSecondaryRecipientNameAdd = $customersData["LieferadresseFirmennameZusatz"];
										$selectCustomersSecondaryRecipientManager = '';
										$selectCustomersSecondaryRecipientContact = '';
										$selectCustomersSecondaryRecipientFax = $customersData["Fax1"];
										$selectCustomersSecondaryRecipientStreet = $customersData["LieferadresseStrasse"];
										$selectCustomersSecondaryRecipientStreetNumber = $customersData["LieferadresseHausnummer"];
										$selectCustomersSecondaryRecipientZipcode = $customersData["LieferadressePLZ"];
										$selectCustomersSecondaryRecipientCity = $customersData["LieferadresseOrt"];
										$selectCustomersSecondaryRecipientCountry = $customersData["LieferadresseLand"];
									}
								}
							// EOF DELIVERY RECIPIENT

							$loadedAdressDatas = array();
							$loadedAdressDatas = array(
									"ADRESS_COMPANY_CUSTOMER_NUMBER" => $selectCustomersRecipientName,

									"ADRESS_COMPANY" => $selectCustomersRecipientName,
									"ADRESS_MANAGER" => "",
									"ADRESS_CONTACT" => "",
									"ADRESS_STREET" => $selectCustomersRecipientStreet.' '.$selectCustomersRecipientStreetNumber,
									"ADRESS_CITY" => $selectCustomersRecipientZipcode.' '.$selectCustomersRecipientCity,
									"ADRESS_COUNTRY" => $arrCountryTypeDatas[$selectCustomersRecipientCountry]["countries_name"],
									"ADRESS_MAIL" => $invoiceRecipientCustomerMail,
									"ADRESS_FAX" => formatPhoneNumber($selectCustomersRecipientFax,''),
								);
							if($selectCustomersRecipientNameAdd != '') {
								$loadedAdressDatas["ADRESS_COMPANY"] .= '<br>' . $selectCustomersRecipientNameAdd;
							}

							if($selectCustomersRecipientManager != "") {
								$loadedAdressDatas["ADRESS_MANAGER"] = 'Inh. ' . $selectCustomersRecipientManager;
							}

							$_POST["selectDocumentDate"]				= $todayDate;
							$_POST["selectInvoiceDate"]					= $ds_getDocumentsToHandle["orderDocumentsInvoiceDate"];

							$_POST["selectCashOnDelivery"]				= $ds_getDocumentsToHandle["orderDocumentsCashOnDelivery"];
							$_POST["selectDiscount"]					= $ds_getDocumentsToHandle["orderDocumentsDiscount"];
							$_POST["selectDiscountType"]				= $ds_getDocumentsToHandle["orderDocumentsDiscountType"];
							$_POST["selectMwSt"]						= $ds_getDocumentsToHandle["orderDocumentsMwst"];
							$_POST["selectMwStType"]					= $ds_getDocumentsToHandle["orderDocumentsMwstType"];
							$_POST["selectPackagingCosts"]				= $ds_getDocumentsToHandle["orderDocumentsPackagingCosts"];
							$_POST["selectPackagingCostsMultiplier"]	= $ds_getDocumentsToHandle["orderDocumentsShippingCostsPackages"];
							$_POST["selectPaymentCondition"]			= $ds_getDocumentsToHandle["orderDocumentsPaymentCondition"];
							$_POST["selectPaymentPartValue"]			= 0;
							$_POST["selectPaymentRestValue"]			= 0;
							$_POST["selectPaymentType"]					= $ds_getDocumentsToHandle["orderDocumentsPaymentType"];
							$_POST["selectReminderChargesPrice"]		= constant('REMINDER_CHARGES_' . $thisConvertDocType);
							$_POST["selectReminderInterestPercent"]		= constant('REMINDER_INTEREST_' . $thisConvertDocType);
							$_POST["selectShippingCosts"]				= $ds_getDocumentsToHandle["orderDocumentsShippingCosts"];

							$loadedDocumentDatas = array(
								// "CUSTOMER_NUMBER" => $ds_getDocumentsToHandle["orderDocumentsCustomersOrderNumber"],
								"CUSTOMER_PHONE" => $customersData["Telefon1"],
								"CUSTOMER_MAIL" => $customersData["Mail1"],
								"CUSTOMERS_ORDER_NUMBER" => $ds_getDocumentsToHandle["orderDocumentsCustomersOrderNumber"],

								"SHIPPING_COSTS" => number_format($ds_getDocumentsToHandle["orderDocumentsShippingCosts"], 2, '.', ''),
								"SHIPPING_TYPE" => $ds_getDocumentsToHandle["orderDocumentsDeliveryType"],
								"SHIPPING_TYPE_NUMBER" => $ds_getDocumentsToHandle["orderDocumentsDeliveryTypeNumber"],
								"PACKAGING_COSTS" => number_format($ds_getDocumentsToHandle["orderDocumentsPackagingCosts"], 2, '.', ''),
								"CARD_CASH_ON_DELIVERY_VALUE" => number_format(SHIPPING_CASH_ON_DELIVERY, 2, '.', ''),
								"SHIPPING_DATE" => formatDate($ds_getDocumentsToHandle["orderDocumentsDeliveryDate"], 'display'),
								"BINDING_DATE" => formatDate($ds_getDocumentsToHandle["orderDocumentsBindingDate"], 'display'),
								"INVOICE_DATE" => formatDate($ds_getDocumentsToHandle["orderDocumentsInvoiceDate"], 'display'),

								"COMPANY_HEADER_IMAGE" => BASEPATH_Auftragslisten . DIRECTORY_PDF_IMAGES . "briefkopf.jpg",
								"COMPANY_NAME_LONG" => COMPANY_NAME_LONG,
								"COMPANY_PHONE" => COMPANY_PHONE,
								"COMPANY_FAX" => COMPANY_FAX,
								"COMPANY_INTERNET" => COMPANY_INTERNET,
								"COMPANY_MANAGER" => COMPANY_MANAGER,
								"COMPANY_MAIL" => COMPANY_MAIL,
								"COMPANY_STREET_NAME" => COMPANY_STREET_NAME,
								"COMPANY_STREET_NUMBER" => COMPANY_STREET_NUMBER,
								"COMPANY_ZIPCODE" => COMPANY_ZIPCODE,
								"COMPANY_CITY" => COMPANY_CITY,
								"COMPANY_TAX_NUMBER" => COMPANY_TAX_NUMBER,
								"COMPANY_UID_NUMBER" => COMPANY_UID_NUMBER,
								"COMPANY_VENUE" => COMPANY_VENUE,

								"BANK_ACCOUNT_TYPE" => $arrBankAccountTypeDatas[$ds_getDocumentsToHandle["orderDocumentsBankAccount"]]["bankAccountTypesShortName"],
								"BANK_ACCOUNT_CODE" => $arrBankAccountTypeDatas[$ds_getDocumentsToHandle["orderDocumentsBankAccount"]]["bankAccountTypesBankCode"],
								"BANK_ACCOUNT_NAME" => $arrBankAccountTypeDatas[$ds_getDocumentsToHandle["orderDocumentsBankAccount"]]["bankAccountTypesName"],
								"BANK_ACCOUNT_NUMBER" => $arrBankAccountTypeDatas[$ds_getDocumentsToHandle["orderDocumentsBankAccount"]]["bankAccountTypesAccountNumber"],
								"BANK_ACCOUNT_IBAN" => $arrBankAccountTypeDatas[$ds_getDocumentsToHandle["orderDocumentsBankAccount"]]["bankAccountTypesIBAN"],
								"BANK_ACCOUNT_BIC" => $arrBankAccountTypeDatas[$ds_getDocumentsToHandle["orderDocumentsBankAccount"]]["bankAccountTypesBIC"],
								"INTRO_TEXT" => $arrContentDatas[$thisConvertDocType]["INTRO_TEXT"],
								"OUTRO_TEXT" => $arrContentDatas[$thisConvertDocType]["OUTRO_TEXT"],
								"EDITOR_TEXT" => $arrContentDatas[$thisConvertDocType]["EDITOR_TEXT"],
								"REGARDS_TEXT" => $arrContentDatas[$thisConvertDocType]["REGARDS_TEXT"],
								"SALUTATION_TEXT" => $arrContentDatas[$thisConvertDocType]["SALUTATION_TEXT"],
								"PAYMENT_CONDITION" => '<span class="important2">' . $arrPaymentConditionDatas[$ds_getDocumentsToHandle["orderDocumentsPaymentCondition"]]["paymentConditionsName"] . '</span>',
								"PAYMENT_TYPE" => '<span class="important2">' . $arrPaymentTypeDatas[$ds_getDocumentsToHandle["orderDocumentsPaymentType"]]["paymentTypesName"] . '</span>',
							);

							$loadedAdressDatas["PAYMENT_CONDITIONS"] = '<br>' . $arrContentDatas[$thisConvertDocType]["PAYMENT_CONDITIONS"];

							if($ds_getDocumentsToHandle["orderDocumentsDeliveryType"] =="" || $ds_getDocumentsToHandle["orderDocumentsDeliveryType"] == 0 ){
								$loadedDocumentDatas["SHIPPING_TYPE"] = SHIPPING_TYPE;
							}

							if($ds_getDocumentsToHandle["orderDocumentsCashOnDelivery"] == 0 && $ds_getDocumentsToHandle["orderDocumentsPaymentType"] == 3){
								$loadedDocumentDatas["PAYMENT_TYPE"] .= ' <span class="important">(ohne Nachnahme-Geb&uuml;hr)</span>';
							}

							$loadedDocumentDatas["INVOICE_SKONTO"] = "";
							$loadedDocumentDatas["WARNING_INVOICE_IS_PAID"] = "";

							if($arrLoadDocumentDatas["orderDocumentsOrderDate"] != "") {
								$loadedDocumentDatas["ORDER_DATE"] = formatDate($arrLoadDocumentDatas["orderDocumentsOrderDate"], 'display');
							}

							$loadedDocumentDatas["DOCUMENT_DATE"] = $todayDate;
							$loadedDocumentDatas["DEMAND_DEADLINE_DATE"] = date("d.m.Y", mktime(0, 0, 0, date('m'), (date('d') + 7), date('Y')));
							$loadedDocumentDatas["ADVANCE_PAYMENT_DEADLINE_DATE"] = date("d.m.Y", mktime(0, 0, 0, date('m'), (date('d') + 28), date('Y')));
							if($ds_getDocumentsToHandle["orderDocumentsPaymentCondition"] == '9'){
								$loadedDocumentDatas["ADVANCE_PAYMENT_DEADLINE_DATE"] = date("d.m.Y", mktime(0, 0, 0, $arrTemp[1], ($arrTemp[0] + $arrPaymentConditionDatas[$ds_getDocumentsToHandle["orderDocumentsPaymentCondition"]]["paymentConditionsDaysLimit"]), $arrTemp[2]));
							}
							$loadedDocumentDatas["CREATION_DATE"] = date("d.m.Y");

							$loadedDocumentDatas["DOCUMENT_NUMBER_AN"] = "";
							$loadedDocumentDatas["DOCUMENT_NUMBER_AB"] = "";
							$loadedDocumentDatas["DOCUMENT_NUMBER_LS"] = "";
							$loadedDocumentDatas["DOCUMENT_NUMBER_RE"] = "";
							$loadedDocumentDatas["DOCUMENT_NUMBER_MA"] = "";
							$loadedDocumentDatas["DOCUMENT_NUMBER_M1"] = "";
							$loadedDocumentDatas["DOCUMENT_NUMBER_M2"] = "";
							$loadedDocumentDatas["DOCUMENT_NUMBER_M3"] = "";
							$loadedDocumentDatas["DOCUMENT_NUMBER_IK"] = "";
							$loadedDocumentDatas["DOCUMENT_NUMBER_GU"] = "";
							$loadedDocumentDatas["DOCUMENT_NUMBER_BR"] = "";

							// BOF RELATION DOCUMENTS
								$loadedDocumentDatas["DOCUMENT_NUMBER_" . substr($thisOriginDocNumber, 0, 2)] = $thisOriginDocNumber;
								if(!empty($arrRelatedDocumentNumbers)){
									$arrRelatedDocumentNumbers = array_unique($arrRelatedDocumentNumbers);
									foreach($arrRelatedDocumentNumbers as $thisKey => $thisValue){
										$loadedDocumentDatas["DOCUMENT_NUMBER_" . $thisKey] = $thisValue;
									}
								}
							// EOF RELATION DOCUMENTS

							$loadedDocumentDatas["CUSTOMER_VAT_ID"] = '';
							$loadedDocumentDatas["CUSTOMERS_TAX_ACCOUNT"] = '';

							if($customersData["VertreterID"] == 0){
								$customersData["VertreterID"] = $arrLoadDocumentDatas["orderDocumentsSalesman"];
							}

							if((int)$customersData["VertreterID"] > 0 && !in_array($customersData["VertreterID"], array("13301", "5684"))) {
								$loadedDocumentDatas["CUSTOMERS_SALESMAN"] = trim($salesmanDatas["salesmanFirmenname"]);
								if(trim($salesmanDatas["salesmanGroupCompanyNumber"]) != ""){
									$loadedDocumentDatas["CUSTOMERS_SALESMAN"] .= '<br>Betr.-Nr: ' . trim($salesmanDatas["salesmanGroupCompanyNumber"]);
								}
								if($customersData["UseSalesmanInvoiceAdress"] == '1') {
									$loadedDocumentDatas["CUSTOMERS_TAX_ACCOUNT"] = trim($salesmanDatas["salesmanTaxAccountID"]);
									if($salesmanDatas["customersRechnungsadresseLand"] > 0 && $salesmanDatas["customersRechnungsadresseLand"] != '81'){
										$loadedDocumentDatas["CUSTOMER_VAT_ID"] = trim($salesmanDatas["customersUstID"]);
									}
									$loadedDocumentDatas["CUSTOMER_NUMBER"] = trim($salesmanDatas["salesmanKundennummer"]);
									$loadedDocumentDatas["SALESMANS_CUSTOMER"] = trim($customersData["Firmenname"]);
									if($customersData["FirmennameZusatz"] != '') {
										$loadedDocumentDatas["SALESMANS_CUSTOMER"] .= ' ' . $customersData["FirmennameZusatz"]. '';
									}
									$loadedDocumentDatas["SALESMANS_CUSTOMER"] = wordwrap($loadedDocumentDatas["SALESMANS_CUSTOMER"], 28, "<br />", false);
									$loadedDocumentDatas["SALESMANS_CUSTOMER"] .= ' (' . $customersData["Kundennummer"] . ')';

									if(trim($customersData["GroupCompanyNumber"]) != ""){
										$loadedDocumentDatas["SALESMANS_CUSTOMER"] .= '<br>Betr.-Nr: ' . trim($customersData["GroupCompanyNumber"]);
									}
								}
								else {
									$loadedDocumentDatas["CUSTOMERS_TAX_ACCOUNT"] = trim($customersData["TaxAccountID"]);
									if($customersData["RechnungsadresseLand"] > 0 && $customersData["RechnungsadresseLand"] != '81'){
										$loadedDocumentDatas["CUSTOMER_VAT_ID"] = trim($customersData["UstID"]);
									}
									$loadedDocumentDatas["CUSTOMER_NUMBER"] = $ds_getDocumentsToHandle["orderDocumentsCustomerNumber"];
									$loadedDocumentDatas["SALESMANS_CUSTOMER"] = '';
								}
							}
							else {
								$loadedDocumentDatas["CUSTOMERS_SALESMAN"] = '';
								$loadedDocumentDatas["CUSTOMERS_SALESMAN_TEXT"] = '';
								if($customersData["VertreterID"] > 0){
									$loadedDocumentDatas["CUSTOMERS_SALESMAN"] = $arrAgentDatas2[$customersData["VertreterID"]]["customersFirmenname"];
								}
								if(in_array($customersData["VertreterID"], array("5684", "13301"))){
									$loadedDocumentDatas["CUSTOMERS_SALESMAN"] = '';
								}

								$loadedDocumentDatas["SALESMANS_CUSTOMER"] = '';
								$loadedDocumentDatas["CUSTOMER_NUMBER"] = $arrLoadDocumentDatas["orderDocumentsCustomerNumber"];
								$loadedDocumentDatas["CUSTOMERS_TAX_ACCOUNT"] = trim($customersData["TaxAccountID"]);
								if($customersData["RechnungsadresseLand"] > 0 && $customersData["RechnungsadresseLand"] != '81'){
									$loadedDocumentDatas["CUSTOMER_VAT_ID"] = trim($customersData["UstID"]);
								}
							}

							if($loadedDocumentDatas["CUSTOMERS_SALESMAN"] != ''){
								$loadedDocumentDatas["CUSTOMERS_SALESMAN"] = wordwrap($loadedDocumentDatas["CUSTOMERS_SALESMAN"], 30, '<br>', false);
							}

							$loadedDocumentDatas["ADDRESS_INVOICE"] = "";
							$loadedDocumentDatas["ADDRESS_DELIVERY"] = "";

							$loadedDocumentDatas["ADDRESS_DELIVERY"] .= '<span class="adressAdditional"><b>Lieferadresse:</b> ';

							$loadedDocumentDatas["ADDRESS_DELIVERY"] .= $selectCustomersSecondaryRecipientName;
							if(trim($selectCustomersSecondaryRecipientNameAdd) != '') { $loadedDocumentDatas["ADDRESS_DELIVERY"] .= ' &middot; ' . $selectCustomersSecondaryRecipientNameAdd; }
							if(trim($selectCustomersSecondaryRecipientContact) != '') { $loadedDocumentDatas["ADDRESS_DELIVERY"] .= ' &middot; ' . $selectCustomersSecondaryRecipientContact; }
							if(trim($selectCustomersSecondaryRecipientManager) != '') { $loadedDocumentDatas["ADDRESS_DELIVERY"] .= ' &middot; ' . $selectCustomersSecondaryRecipientManager; }
							$loadedDocumentDatas["ADDRESS_DELIVERY"] .= ' &middot; ' . $selectCustomersSecondaryRecipientStreet . ' ' . $selectCustomersSecondaryRecipientStreetNumber;
							$loadedDocumentDatas["ADDRESS_DELIVERY"] .= ' &middot; ' . $selectCustomersSecondaryRecipientZipcode . ' ' . $selectCustomersSecondaryRecipientCity;
							$loadedDocumentDatas["ADDRESS_DELIVERY"] .= ' &middot; ' . $arrCountryTypeDatas[$selectCustomersSecondaryRecipientCountry]["countries_name"];

							$loadedDocumentDatas["ADDRESS_DELIVERY"] .= '</span><br><br>';

							$loadedDocumentDatas["BANK_DATAS"] = "";
							if(in_array($ds_getDocumentsToHandle["orderDocumentsPaymentType"], array('4', '6'))){
								$loadedDocumentDatas["BANK_DATAS"] .= '<br><br><span class="adressAdditional"><b>Bankverbindung:</b> ';

								if($customersData["UseSalesmanInvoiceAdress"] == '1') {
									$loadedDocumentDatas["BANK_DATAS"] .=
										'' .
										$salesmanDatas["customersBankName"] .
										' &middot; BLZ: ' . $salesmanDatas["customersBankLeitzahl"] .
										' &middot; Konto: ' . $salesmanDatas["customersBankKontonummer"] .
										' &middot; IBAN: ' . $salesmanDatas["customersBankIBAN"] .
										' &middot; BIC: ' . $salesmanDatas["customersBankBIC"];
								}
								else {
									$loadedDocumentDatas["BANK_DATAS"] .=
										'' .
										$customersData["BankName"] .
										' &middot; BLZ: ' . $customersData["BankLeitzahl"] .
										' &middot; Konto: ' . $customersData["BankKontonummer"] .
										' &middot; IBAN: ' . $customersData["BankIBAN"] .
										' &middot; BIC: ' . $customersData["BankBIC"];
								}

								if(USE_DEBITING_DATE){
									// $loadedDocumentDatas["BANK_DATAS"] .= '<br />' . 'Die Abbuchung erfolgt am ' . formatDate($_REQUEST["selectDebitingDate"], 'display');
								}

								$loadedDocumentDatas["BANK_DATAS"] .= '</span><br><br>';
							}

							// BOF AD FOOTER NOTICE
								if(MANDATOR == 'b3' && (in_array($customersData["Typ"], array("3", "4")) || in_array($salesmanDatas["salesmanTyp"], array("3", "4")))){
									$loadedDocumentDatas["FOOTER_NOTICE"] = $arrContentDatas[$thisConvertDocType]["FOOTER_NOTICE"];
								}
								else {
									$loadedDocumentDatas["FOOTER_NOTICE"] = "";
								}
							// EOF AD FOOTER NOTICE
						// EOF GET THIS DOCUMENT'S DATA

						$loadPdfContent = "";

						$loadContentTemplate = implode('', file(DIRECTORY_PDF_TEMPLATES . '/template_BASIC.html'));
						$loadCardTemplate = implode('', file(DIRECTORY_PDF_TEMPLATES . '/template_CARD_DATAS.html'));

						$loadContentTemplate = preg_replace("/{###DOCUMENT_DATAS###}/", $arrDocumentDatas[$thisConvertDocType], $loadContentTemplate);

						$loadCardDatas = loadCardDatas($loadCardTemplate, $selectOrdersIDs, $selectOrdersDetails);

						$loadPdfContent = $loadContentTemplate;

						$loadPdfContent = preg_replace("/{###CARD_DATAS###}/", $loadCardDatas, $loadPdfContent);

						// BOF LOAD STYLES
							$loadTemplateStyles = implode('', file(DIRECTORY_PDF_TEMPLATES . 'template_STYLES.html'));
							$loadPdfContent = preg_replace("/{###STYLES###}/", $loadTemplateStyles, $loadPdfContent);
						// EOF LOAD STYLES

						// BOF LOAD ADRESS DATA
							if(!empty($loadedAdressDatas)) {
								foreach($loadedAdressDatas as $thisKey => $thisValue) {
									if($thisValue != "") {
										$loadPdfContent = preg_replace("/{###".$thisKey."###}/", $thisValue . "<br />", $loadPdfContent);
									}
									else {
										$loadPdfContent = preg_replace("/{###".$thisKey."###}/", "", $loadPdfContent);
									}
								}
							}
						// EOF LOAD ADRESS DATA

						// BOF LOAD DOCUMENT DATA
							if(!empty($loadedDocumentDatas)) {
								// if(constant('BANK_ACCOUNT_' . $ds_getDocumentsToHandle["orderDocumentsBankAccount"]. '_SHOW_WARNING')){
								if($arrBankAccountTypeDatas[$ds_getDocumentsToHandle["orderDocumentsBankAccount"]]["bankAccountTypesShowWarning"] == '1') {
									$loadPdfContent = preg_replace("/{###BANK_ACCOUNT_WARNING###}/", $arrContentDatas[$thisConvertDocType]['BANK_ACCOUNT_WARNING'], $loadPdfContent);
								}
								else {
									$loadPdfContent = preg_replace("/{###BANK_ACCOUNT_WARNING###}/", "", $loadPdfContent);
								}
								foreach($loadedDocumentDatas as $thisKey => $thisValue) {
									$loadPdfContent = preg_replace("/{###".$thisKey."###}/", $thisValue, $loadPdfContent);
									if(trim($thisValue) == '') {
										$loadPdfContent = preg_replace("/<!-- BOF_".$thisKey." -->(.*)<!-- EOF_".$thisKey." -->/", '', $loadPdfContent);
									}
								}
							}
							else {
								fwrite($fp_log, date("Y-m-d H:i:s") . ":  Array loadedDocumentDatas ist leer!" . "\n");
							}

							$loadSubjectText = $thisSubject . ' Nr.: ' . '{###DOCUMENT_NUMBER###}';

							if(in_array($thisConvertDocType, array('RE', 'MA', 'M1', 'M2', 'M3'))){
								$loadSubjectText .= ' <span class="subjectNotice">Bei Zahlung diese Nummer angeben!</span>';
							}

							$loadSubject = ''.$loadSubjectText. '';
							#$loadSubject .= '<br><br>';

							$loadPdfContent = preg_replace("/{###SUBJECT###}/", $loadSubject, $loadPdfContent);
							$loadPdfContent = removeUnnecessaryChars2($loadPdfContent);
						// BOF LOAD DOCUMENT DATA

						$pdfDisplayContent = $loadPdfContent;

						if(DEBUG_MODE){
							#fwrite($fp_log, "customersData: " . print_r($customersData, true) . "\n\n");
							#fwrite($fp_log, "salesmanDatas: " . print_r($salesmanDatas, true) . "\n\n");
							fwrite($fp_log, "loadedDocumentDatas: " . print_r($loadedDocumentDatas, true) . "\n\n");
							fwrite($fp_log, "loadedAdressDatas: " . print_r($loadedAdressDatas, true) . "\n\n");
							fwrite($fp_log, "unserializedOrderDocumentsDeliveryRecipientData: " . print_r($unserializedOrderDocumentsDeliveryRecipientData, true) . "\n\n");
							fwrite($fp_log, "unserializedOrderDocumentsInvoiceRecipientData: " . print_r($unserializedOrderDocumentsInvoiceRecipientData, true) . "\n\n");
						}

						// BOF GENERATE DOCUMENT NUMBER ON CURRENT YEAR AND MONTH
							$thisGeneratedDocumentNumber = generateDocumentNumber(constant("BASIC_NUMBER_" . $thisConvertDocType), $thisConvertDocType, date(ym), constant("TABLE_ORDER_" . $thisConvertDocType));
							$loadPdfContent = preg_replace("/{###DOCUMENT_NUMBER###}/", $thisGeneratedDocumentNumber, $loadPdfContent);
							fwrite($fp_log, "convert: " . $ds_getDocumentsToHandle["orderDocumentsNumber"] . "[" . $thisOriginDocType  . "]" . " to "  . $thisGeneratedDocumentNumber . "[" . $thisConvertDocType . "]" . "\n");
						// EOF GENERATE DOCUMENT NUMBER ON CURRENT YEAR AND MONTH

						$pdfCreateContent = $loadPdfContent;

						// BOF GET INPUT FIELDS FROM HTML TO GET VARS
							$rs_match = preg_match_all("/<input(.*)\/>/ismU", $pdfDisplayContent, $arrFound);
							$arrFoundInputFields = $arrFound[0];

							if(!empty($arrFoundInputFields)){

								foreach($arrFoundInputFields as $thisFoundInputFieldKey => $thisFoundInputFieldValue){
									// BOF GET VAR VALUE
										$rs_match = preg_match('/value="(.*)"/ismU', $thisFoundInputFieldValue, $arrFoundVarValue);
										$thisVarValue = $arrFoundVarValue[1];
									// EOF GET VAR VALUE

									// BOF GET VAR NAME
										$rs_match = preg_match('/name="(.*)"/ismU', $thisFoundInputFieldValue, $arrFoundInputFieldName);
										$thisInputFieldName = $arrFoundInputFieldName[1];
									// EOF GET VAR NAME

									// BOF CHECK IF NAME IS ARRAY TYPE
										$rs_match = preg_match("/^(.*)(\[)/ismU", $thisInputFieldName, $arrFound);
									// EOF CHECK IF NAME IS ARRAY TYPE

									// BOF SET VAR VALUES DEPENDING ON VAR TYPE
										if($rs_match){
											// BOF GET ARRAY NAME
												$thisVarName = $arrFound[1];
											// EOF GET ARRAY NAME

											// BOF GET ARRAY INDEXES
												$rs_match = preg_match_all("/\[(.*)\]/ismU", $thisInputFieldName, $arrFound);
												$thisArrIndexes = $arrFound[1];
											// EOF GET ARRAY INDEXES

											if(!is_array(${$thisVarName})){
												${$thisVarName} = array();
											}

											for($i = 0; $i < count($thisArrIndexes) ; $i++){
												if($i == 0){
													if(!is_array($tmpArray[$thisVarName][$thisArrIndexes[$i]])){
														$tmpArray[$thisVarName][$thisArrIndexes[$i]] = array();
													}
												}
												else if($i > 0){
													if(!is_array($tmpArray[$thisVarName][$thisArrIndexes[($i - 1)]][$thisArrIndexes[$i]])){
														$tmpArray[$thisVarName][$thisArrIndexes[($i - 1)]][$thisArrIndexes[$i]] = array();
													}
												}

												if($i == (count($thisArrIndexes) - 1)){
													if($i == 0){
														$tmpArray[$thisVarName][$thisArrIndexes[$i]] = $thisVarValue;
													}
													else {
														$tmpArray[$thisVarName][$thisArrIndexes[($i - 1)]][$thisArrIndexes[$i]] = $thisVarValue;
													}
												}
											}
										}
										else {
											$thisVarName = $thisInputFieldName;
											$tmpArray = $thisVarValue;
										}
									// EOF SET VAR VALUES DEPENDING ON VAR TYPE
								}
								if(!empty($tmpArray)){
									foreach($tmpArray as $tmpArrayKey => $tmpArrayValue){
										${$tmpArrayKey} = $tmpArrayValue;
									}
								}
							}
						// EOF GET INPUT FIELDS FROM HTML TO GET VARS

						// BOF REMOVE INPUT FIELDS IN PDF CREATION CONTENT
							$pdfCreateContent = preg_replace('/<input (.*) \/>/ismU', '', $pdfCreateContent);
							// <input type="hidden" name="arrItemDatas[4369][basketItemId]" value="4369" />
						// EOF REMOVE INPUT FIELDS IN PDF CREATION CONTENT

						// BOF CLEAN SPACES
							$pdfCreateContent = cleanSpaces($pdfCreateContent);
						// EOF CLEAN SPACES

						// BOF CREATE PDF_NAME
							$thisCreatedPdfName = $thisGeneratedDocumentNumber . '_KNR-' . $ds_getDocumentsToHandle["orderDocumentsCustomerNumber"] . '.pdf';
							if(DEBUG_MODE){
								$thisCreatedPdfName = '_testGeneratedDocument_' . date('Y-m-d_H-i-s') . '.pdf';
							}
							$thisCreatedPdfPath = DIRECTORY_CREATED_DOCUMENTS_CUSTOMERS . $thisCreatedPdfName;
							fwrite($fp_log, "thisCreatedPdfPath: " . $thisCreatedPdfPath . "\n");
						// EOF CREATE PDF_NAME

						#fwrite($fp_log, "pdfCreateContent: " . $pdfCreateContent . "\n");

						// BOF DELETE DATA IF DOCUMENT EXISTS
							/*
							// BOF DELETE FILE IF EXISTS
								if(file_exists(DIRECTORY_CREATED_DOCUMENTS_CUSTOMERS . $thisCreatedPdfName)) {
									// chmod(DIRECTORY_CREATED_DOCUMENTS_CUSTOMERS . $thisCreatedPdfName , "0777");
									unlink(DIRECTORY_CREATED_DOCUMENTS_CUSTOMERS . $thisCreatedPdfName);
								}
							// EOF DELETE FILE IF EXISTS

							// BOF DELETE DATA IF DOCUMENT-NUMBER EXISTS
								$sql_getDocumentID = "SELECT
											`orderDocumentsID` AS `deleteID`
											FROM
											`" . constant("TABLE_ORDER_" . $thisConvertDocType) . "`
											WHERE `orderDocumentsNumber` = '" . $thisGeneratedDocumentNumber. "'
									";
								$rs_getDocumentID = $dbConnection->db_query($sql_getDocumentID);
								if(mysqli_error($db_open)) {
									fwrite($fp_log, date("Y-m-d H:i:s") . ":  sql_getDocumentID : " . $sql_getDocumentID . "\n" . "ERROR: " . mysqli_error($db_open) . "\n\n\n");
								}
								list($deleteID) = mysqli_fetch_array($rs_getDocumentID);

								if($deleteID != '') {
									$sql_deleteDocumentData = "DELETE FROM `" . constant("TABLE_ORDER_" . $thisConvertDocType) . "` WHERE `orderDocumentsID` = '" . $deleteID. "'";
									$rs_deleteDocumentData = $dbConnection->db_query($sql_deleteDocumentData);
									if(mysqli_error($db_open)) {
										fwrite($fp_log, date("Y-m-d H:i:s") . ":  sql_deleteDocumentData : " . $sql_deleteDocumentData . "\n" . "ERROR: " . mysqli_error($db_open) . "\n\n\n");
									}

									$sql_deleteDocumentDetailsData = "DELETE FROM `" . constant("TABLE_ORDER_" . $thisConvertDocType  . "_DETAILS") . "` WHERE `orderDocumentDetailDocumentID` = '" . $deleteID. "'";
									$rs_deleteDocumentDetailsData = $dbConnection->db_query($sql_deleteDocumentDetailsData);
									if(mysqli_error($db_open)) {
										fwrite($fp_log, date("Y-m-d H:i:s") . ":  sql_deleteDocumentDetailsData : " . $sql_deleteDocumentDetailsData . "\n" . "ERROR: " . mysqli_error($db_open) . "\n\n\n");
									}
								}
							*/
							// EOF DELETE DATA IF DOCUMENT-NUMBER EXISTS
						// EOF DELETE DATA IF DOCUMENT EXISTS

						// BOF CREATE NEW DOCUMENT STATUS
							// BOF SET PAYMENT STATUS TYPE FOR SPECIAL CONVERTED DOCUMENTS
								$selectPaymentStatusNew = "";
								if(in_array($thisConvertDocType, array("MA", "M1", "M2", "M3"))){
									$selectPaymentStatusNew = $arrPaymentStatusTypeDatasToShortName[$thisConvertDocType];
								}
							// EOF SET PAYMENT STATUS TYPE FOR SPECIAL CONVERTED DOCUMENTS
						// EOF CREATE NEW DOCUMENT STATUS

						// BOF SERIALIZE ADRESS DELIVERY AND INVOICE DATA IF LOADED SERIALIZED DATAS ARE EMPTY
							/*
							if($ds_getDocumentsToHandle["orderDocumentsInvoiceRecipientData"] == ''){
								#$unserializedOrderDocumentsInvoiceRecipientData = unserialize($ds_getDocumentsToHandle["orderDocumentsInvoiceRecipientData"]);
								$thisArrOrderDocumentsInvoiceRecipientData = array();

								if($customersData["UseSalesmanInvoiceAdress"] == '1') {
									$thisArrOrderDocumentsInvoiceRecipientData = array(
										"selectCustomersRecipientMail" => $salesmanDatas["salesmanMail"],
										"selectCustomersRecipientMailCopy" => '',
										"selectCustomersRecipientCustomerNumber" => $salesmanDatas["salesmanKundennummer"],
										"selectCustomersRecipientName" => $salesmanDatas["customersRechnungsadresseFirmenname"],
										"selectCustomersRecipientNameAdd" => $salesmanDatas["customersRechnungsadresseFirmennameZusatz"],
										"selectCustomersRecipientManager" => '',
										"selectCustomersRecipientContact" => '',
										"selectCustomersRecipientFax" => $salesmanDatas["salesmanFax"],
										"selectCustomersRecipientStreet" => $salesmanDatas["customersRechnungsadresseStrasse"],
										"selectCustomersRecipientStreetNumber" => $salesmanDatas["customersRechnungsadresseHausnummer"],
										"selectCustomersRecipientZipcode" => $salesmanDatas["customersRechnungsadressePLZ"],
										"selectCustomersRecipientCity" => $salesmanDatas["customersRechnungsadresseOrt"],
										"selectCustomersRecipientCountry" => $salesmanDatas["customersRechnungsadresseLand"]
									);
								}
								else {
									$thisArrOrderDocumentsInvoiceRecipientData = array(
										"selectCustomersRecipientMail" => $customersData["customersMail1"] . ';' . $customersData["customersMail2"],
										"selectCustomersRecipientMailCopy" => '',
										"selectCustomersRecipientCustomerNumber" => $customersData["Kundennummer"],
										"selectCustomersRecipientName" => $customersData["RechnungsadresseFirmenname"],
										"selectCustomersRecipientNameAdd" => $customersData["RechnungsadresseFirmennameZusatz"],
										"selectCustomersRecipientManager" => '',
										"selectCustomersRecipientContact" => '',
										"selectCustomersRecipientFax" => $customersData["Fax1"],
										"selectCustomersRecipientStreet" => $customersData["RechnungsadresseStrasse"],
										"selectCustomersRecipientStreetNumber" => $customersData["RechnungsadresseHausnummer"],
										"selectCustomersRecipientZipcode" => $customersData["RechnungsadressePLZ"],
										"selectCustomersRecipientCity" => $customersData["RechnungsadresseOrt"],
										"selectCustomersRecipientCountry" => $customersData["RechnungsadresseLand"]
									);
								}
								$serializedOrderDocumentsInvoiceRecipientData = '';

								if(!empty($thisArrOrderDocumentsInvoiceRecipientData)){
									$arrTempOrderDocumentsInvoiceRecipientData = $thisArrOrderDocumentsInvoiceRecipientData;
									foreach($arrTempOrderDocumentsInvoiceRecipientData as $thisDataKey => $thisDataValue){
										$arrTempOrderDocumentsInvoiceRecipientData[$thisDataKey] = addslashes($thisDataValue);
									}
									#$serializedOrderDocumentsInvoiceRecipientData = (serialize($thisArrOrderDocumentsInvoiceRecipientData));
									$serializedOrderDocumentsInvoiceRecipientData = (serialize($arrTempOrderDocumentsInvoiceRecipientData));
								}
								$ds_getDocumentsToHandle["orderDocumentsInvoiceRecipientData"] = $serializedOrderDocumentsInvoiceRecipientData;
							}
							if($ds_getDocumentsToHandle["orderDocumentsDeliveryRecipientData"] == ''){
								#$unserializedOrderDocumentsDeliveryRecipientData = unserialize($ds_getDocumentsToHandle["orderDocumentsDeliveryRecipientData"]);
								$thisArrOrderDocumentsDeliveryRecipientData = array();

								if($customersData["UseSalesmanDeliveryAdress"] == '1') {
									$thisArrOrderDocumentsDeliveryRecipientData = array(
										"selectCustomersSecondaryRecipientMail" => $salesmanDatas["salesmanMail"],
										"selectCustomersSecondaryRecipientMailCopy" => '',
										"selectCustomersSecondaryRecipientCustomerNumber" => $salesmanDatas["salesmanKundennummer"],
										"selectCustomersSecondaryRecipientName" => $salesmanDatas["customersLieferadresseFirmenname"],
										"selectCustomersSecondaryRecipientNameAdd" => $salesmanDatas["customersLieferadresseFirmennameZusatz"],
										"selectCustomersSecondaryRecipientManager" => '',
										"selectCustomersSecondaryRecipientContact" => '',
										"selectCustomersSecondaryRecipientFax" => $salesmanDatas["salesmanFax"],
										"selectCustomersSecondaryRecipientStreet" => $salesmanDatas["customersLieferadresseStrasse"],
										"selectCustomersSecondaryRecipientStreetNumber" => $salesmanDatas["customersLieferadresseHausnummer"],
										"selectCustomersSecondaryRecipientZipcode" => $salesmanDatas["customersLieferadressePLZ"],
										"selectCustomersSecondaryRecipientCity" => $salesmanDatas["customersLieferadresseOrt"],
										"selectCustomersSecondaryRecipientCountry" => $salesmanDatas["customersLieferadresseLand"]
									);
								}
								else{
									$thisArrOrderDocumentsDeliveryRecipientData = array(
										"selectCustomersSecondaryRecipientMail" => $customersData["customersMail1"] . ';' . $customersData["customersMail2"],
										"selectCustomersSecondaryRecipientMailCopy" => '',
										"selectCustomersSecondaryRecipientCustomerNumber" => $customersData["Kundennummer"],
										"selectCustomersSecondaryRecipientName" => $customersData["LieferadresseFirmenname"],
										"selectCustomersSecondaryRecipientNameAdd" => $customersData["LieferadresseFirmennameZusatz"],
										"selectCustomersSecondaryRecipientManager" => '',
										"selectCustomersSecondaryRecipientContact" => '',
										"selectCustomersSecondaryRecipientFax" => $customersData["Fax1"],
										"selectCustomersSecondaryRecipientStreet" => $customersData["LieferadresseStrasse"],
										"selectCustomersSecondaryRecipientStreetNumber" => $customersData["LieferadresseHausnummer"],
										"selectCustomersSecondaryRecipientZipcode" => $customersData["LieferadressePLZ"],
										"selectCustomersSecondaryRecipientCity" => $customersData["LieferadresseOrt"],
										"selectCustomersSecondaryRecipientCountry" => $customersData["LieferadresseLand"]
									);
								}
								$serializedOrderDocumentsDeliveryRecipientData = '';

								if(!empty($thisArrOrderDocumentsDeliveryRecipientData)){
									$arrTempOrderDocumentsDeliverRecipientData = $thisArrOrderDocumentsDeliveryRecipientData;
									foreach($arrTempOrderDocumentsDeliverRecipientData as $thisDataKey => $thisDataValue){
										$arrTempOrderDocumentsDeliverRecipientData[$thisDataKey] = addslashes($thisDataValue);
									}
									#$serializedOrderDocumentsDeliveryRecipientData = (serialize($thisArrOrderDocumentsDeliveryRecipientData));
									$serializedOrderDocumentsDeliveryRecipientData = (serialize($arrTempOrderDocumentsDeliverRecipientData));
								}
								$ds_getDocumentsToHandle["orderDocumentsDeliveryRecipientData"] = $serializedOrderDocumentsDeliveryRecipientData;
							}
							*/
						// EOF SERIALIZE ADRESS DELIVERY AND INVOICE DATA IF LOADED SERIALIZED DATAS ARE EMPTY

						// BOF STORE DOCUMENT DATAS
							$sql_storeDocumentData = "
									INSERT INTO `" . constant("TABLE_ORDER_" . $thisConvertDocType) . "` (
											`orderDocumentsID`,
											`orderDocumentsNumber`,
											`orderDocumentsType`,
											`orderDocumentsOrdersIDs`,
											`orderDocumentsCustomerNumber`,
											`orderDocumentsCustomersOrderNumber`,

											`orderDocumentsProcessingDate`,
											`orderDocumentsDocumentDate`,
											`orderDocumentsOrderDate`,
											`orderDocumentsInvoiceDate`,
											`orderDocumentsCreditDate`,
											`orderDocumentsDeliveryDate`,
											`orderDocumentsBindingDate`,
											`orderDocumentsDebitingDate`,

											`orderDocumentsSalesman`,

											`orderDocumentsKommission`,

											`orderDocumentsAddressCompanyCustomerNumber`,

											`orderDocumentsAddressCompany`,
											`orderDocumentsAddressName`,
											`orderDocumentsAddressStreet`,
											`orderDocumentsAddressStreetNumber`,
											`orderDocumentsAddressZipcode`,
											`orderDocumentsAddressCity`,
											`orderDocumentsAddressCountry`,
											`orderDocumentsAddressMail`,

											`orderDocumentsSumPrice`,
											`orderDocumentsDiscount`,
											`orderDocumentsDiscountType`,
											`orderDocumentsDiscountPercent`,
											`orderDocumentsMwst`,
											`orderDocumentsMwstPrice`,
											`orderDocumentsMwstType`,
											`orderDocumentsShippingCosts`,
											`orderDocumentsShippingCostsPackages`,
											`orderDocumentsShippingCostsPerPackage`,
											`orderDocumentsPackagingCosts`,
											`orderDocumentsCashOnDelivery`,
											`orderDocumentsTotalPrice`,

											`orderDocumentsBankAccount`,

											`orderDocumentsPaymentCondition`,
											`orderDocumentsPaymentType`,

											`orderDocumentsSkonto`,

											`orderDocumentsDeliveryType`,
											`orderDocumentsDeliveryTypeNumber`,

											`orderDocumentsSubject`,
											`orderDocumentsContent`,

											`orderDocumentsDocumentPath`,
											`orderDocumentsTimestamp`,
											`orderDocumentsStatus`,

											`orderDocumentsSendMail`,

											`orderDocumentsInterestPercent`,
											`orderDocumentsInterestPrice`,
											`orderDocumentsChargesPrice`,

											`orderDocumentsDeadlineDate`,

											`orderDocumentsCustomerGroupID`,

											`orderDocumentsIsCollectiveInvoice`,

											`orderDocumentsInvoiceRecipientData`,
											`orderDocumentsDeliveryRecipientData`
										)
										VALUES (
											'%',
											'" . $thisGeneratedDocumentNumber . "',
											'" . $thisConvertDocType . "',

											'" . $ds_getDocumentsToHandle["orderDocumentsOrdersIDs"] . "',
											'" . $ds_getDocumentsToHandle["orderDocumentsCustomerNumber"] . "',
											'" . addslashes($ds_getDocumentsToHandle["orderDocumentsCustomersOrderNumber"]) . "',

											NOW(),
											'" . formatDate($todayDate, "store") . "',
											'" . $ds_getDocumentsToHandle["orderDocumentsOrderDate"] . "',
											'" . $ds_getDocumentsToHandle["orderDocumentsInvoiceDate"] . "',
											'" . $ds_getDocumentsToHandle["orderDocumentsCreditDate"] . "',
											'" . $ds_getDocumentsToHandle["orderDocumentsDeliveryDate"] . "',
											'" . $ds_getDocumentsToHandle["orderDocumentsBindingDate"] . "',
											'" . $ds_getDocumentsToHandle["orderDocumentsDebitingDate"] . "',

											'" . addslashes($ds_getDocumentsToHandle["orderDocumentsSalesman"]) . "',

											'" . addslashes($ds_getDocumentsToHandle["orderDocumentsKommission"]) . "',

											'" . $ds_getDocumentsToHandle["orderDocumentsAddressCompanyCustomerNumber"] . "',

											'" . addslashes($ds_getDocumentsToHandle["orderDocumentsAddressCompany"]) . "',
											'" . addslashes($ds_getDocumentsToHandle["orderDocumentsAddressName"]) . "',
											'" . addslashes($ds_getDocumentsToHandle["orderDocumentsAddressStreet"]) . "',
											'" . addslashes($ds_getDocumentsToHandle["orderDocumentsAddressStreetNumber"]) . "',
											'" . addslashes($ds_getDocumentsToHandle["orderDocumentsAddressZipcode"]) . "',
											'" . addslashes($ds_getDocumentsToHandle["orderDocumentsAddressCity"]) . "',
											'" . $ds_getDocumentsToHandle["orderDocumentsAddressCountry"] . "',
											'" . $ds_getDocumentsToHandle["orderDocumentsAddressMail"] . "',

											'" . convertDecimal($arrPriceDatas["basketTotalPrice"], 'store') . "',
											'" . convertDecimal($arrPriceDatas["basketDiscountValue"], 'store') . "',
											'" . $arrPriceDatas["basketDiscountType"] . "',
											'" . convertDecimal($arrPriceDatas["basketDiscountPercent"], 'store') . "',
											'" . $ds_getDocumentsToHandle["orderDocumentsMwst"] . "',
											'" . convertDecimal($arrPriceDatas["basketMmstPrice"], 'store') . "',
											'" . $ds_getDocumentsToHandle["orderDocumentsMwstType"] . "',
											'" . $ds_getDocumentsToHandle["orderDocumentsShippingCosts"] . "',
											'" . $ds_getDocumentsToHandle["orderDocumentsShippingCostsPackages"] . "',
											'" . $ds_getDocumentsToHandle["orderDocumentsShippingCostsPerPackage"] . "',
											'" . $ds_getDocumentsToHandle["orderDocumentsPackagingCosts"] . "',
											'" . $ds_getDocumentsToHandle["orderDocumentsCashOnDelivery"] . "',
											'" . convertDecimal($arrPriceDatas["basketCompletePrice"], 'store') . "',

											'" . $ds_getDocumentsToHandle["orderDocumentsBankAccount"] . "',

											'" . $ds_getDocumentsToHandle["orderDocumentsPaymentCondition"] . "',
											'" . $ds_getDocumentsToHandle["orderDocumentsPaymentType"] . "',

											'" . $ds_getDocumentsToHandle["orderDocumentsSkonto"] . "',

											'" . $ds_getDocumentsToHandle["orderDocumentsDeliveryType"] . "',
											'" . $ds_getDocumentsToHandle["orderDocumentsDeliveryTypeNumber"] . "',

											'" . $ds_getDocumentsToHandle["orderDocumentsSubject"] . "',
											'" . $ds_getDocumentsToHandle["orderDocumentsContent"] . "',

											'" . $thisCreatedPdfPath . "',
											'" . $ds_getDocumentsToHandle["orderDocumentsTimestamp"] . "',
											'" . $selectPaymentStatusNew . "',

											'" . $ds_getDocumentsToHandle["orderDocumentsSendMail"] . "',

											'" . convertDecimal($arrPriceDatas["basketInterestPercent"], 'store') . "',
											'" . convertDecimal($arrPriceDatas["basketInterestPrice"], 'store') . "',
											'" . convertDecimal($arrPriceDatas["basketChargesPrice"], 'store') . "',

											DATE_ADD('" . formatDate($todayDate, "store") . "', INTERVAL (10 + " . ADD_DAYS_TO_INTERVAL . ") DAY),

											'" . $ds_getDocumentsToHandle["orderDocumentsCustomerGroupID"] . "',

											'" . $ds_getDocumentsToHandle["orderDocumentsIsCollectiveInvoice"] . "',

											'" . $ds_getDocumentsToHandle["orderDocumentsInvoiceRecipientData"] . "',
											'" . $ds_getDocumentsToHandle["orderDocumentsDeliveryRecipientData"] . "'
										)
								";
								if(!DEBUG_MODE){
									$rs_storeDocumentData = $dbConnection->db_query($sql_storeDocumentData);
									if(mysqli_error($db_open)) {
										fwrite($fp_log, date("Y-m-d H:i:s") . ":  sql_storeDocumentData : " . $sql_storeDocumentData . "\n" . "ERROR: " . mysqli_error($db_open) . "\n\n\n");
									}
								}
								else {
									fwrite($fp_log, date("Y-m-d H:i:s") . ":  sql_storeDocumentData : " . $sql_storeDocumentData . "\n");
								}
							// GET THIS INSERT ID
								$insertedDocumentID = mysqli_insert_id();
						// EOF STORE DOCUMENT DATAS

						if($rs_storeDocumentData || DEBUG_MODE){
							// BOF STORE DOCUMENT DETAIL DATAS
								$sql_insertValues = array();
								foreach($arrItemIDs as $thisKey => $thisValue) {
									if(!isset($thisLastInsertID)) {
										$thisLastInsertID = 0;
									}
									$sql_insertDocumentDetails = "
											INSERT INTO `" . constant("TABLE_ORDER_" . $thisConvertDocType  . "_DETAILS") . "` (
												`orderDocumentDetailID`,
												`orderDocumentDetailDocumentID`,
												`orderDocumentDetailOrderID`,
												`orderDocumentDetailOrderType`,
												`orderDocumentDetailPos`,

												`orderDocumentDetailProductNumber`,
												`orderDocumentDetailProductName`,
												`orderDocumentDetailProductKategorieID`,
												`orderDocumentDetailProductQuantity`,
												`orderDocumentDetailProductColorsText`,
												`orderDocumentDetailProductColorsCount`,
												`orderDocumentDetailProductPrintType`,
												`orderDocumentDetailsWithBorder`,
												`orderDocumentDetailsWithClearPaint`,
												`orderDocumentDetailPrintText`,
												`orderDocumentDetailKommission`,
												`orderDocumentDetailNotiz`,
												`orderDocumentDetailProductSinglePrice`,
												`orderDocumentDetailProductTotalPrice`,

												`orderDocumentDetailMwst`,
												`orderDocumentDetailDiscount`,
												`orderDocumentDetailDirectSale`,
												`orderDocumentDetailPosType`,
												`orderDocumentDetailGroupingID`

											)
											VALUES (
												'%',
												'" . $insertedDocumentID . "',
												'" . $arrItemDatas[$thisKey]["basketItemId"] . "',
												'" . $arrItemDatas[$thisKey]["basketItemOrderType"] . "',
												'" . $arrItemDatas[$thisKey]["basketItemPos"] . "',

												'" . addslashes($arrItemDatas[$thisKey]["basketItemModel"]) . "',
												'" . (preg_replace("/,[ ]{0,}[0-9]{1,}\-farbiger Druck/", "", preg_replace("/\[[a-zA-Z]{0,}\]$/", "", preg_replace("/, ohne Druck/", "", addslashes(strip_tags(preg_replace("/<br>/", " ", $arrItemDatas[$thisKey]["basketItemDescription"]))))))) . "',
												'" . $arrItemDatas[$thisKey]["basketItemProductKategorieID"] . "',
												'" . $arrItemDatas[$thisKey]["basketItemQuantity"] . "',
												'" . addslashes(preg_replace("/Druckfarben: /", "", strip_tags($arrItemDatas[$thisKey]["basketItemColorsText"]))) . "',
												'" . $arrItemDatas[$thisKey]["basketItemColorsCount"] . "',

												'" . $arrItemDatas[$thisKey]["basketItemPrintType"] . "',

												'" . $arrItemDatas[$thisKey]["basketItemWithBorder"] . "',
												'" . $arrItemDatas[$thisKey]["basketItemWithClearPaint"] . "',
												'" . addslashes(preg_replace("/ - Mit Umrandung/", "", preg_replace("/Aufdruck: /", "", strip_tags($arrItemDatas[$thisKey]["basketItemPrintText"])))) . "',
												'" . addslashes(preg_replace("/Kommission: /", "", strip_tags($arrItemDatas[$thisKey]["basketItemKommission"]))) . "',
												'" . addslashes(preg_replace("/Notiz: /", "", strip_tags($arrItemDatas[$thisKey]["basketItemNotiz"]))) . "',
												'" . convertDecimal($arrItemDatas[$thisKey]["basketItemSinglePrice"], 'store') . "',
												'" . convertDecimal($arrItemDatas[$thisKey]["basketItemTotalPrice"], 'store') . "',

												'" . convertDecimal($arrPriceDatas["basketMmst"], 'store') . "',
												'" . convertDecimal($arrPriceDatas["basketDiscount"], 'store') . "',
												'" . $arrItemDatas[$thisKey]["basketItemPerDirectSale"] . "',
												'" . $arrItemDatas[$thisKey]["basketItemPosType"] . "',
												'" . $thisLastInsertID ."'
											)
										";
									if(!DEBUG_MODE){
										$rs_insertDocumentDetails = $dbConnection->db_query($sql_insertDocumentDetails);

										if(mysqli_error($db_open)) {
											fwrite($fp_log, date("Y-m-d H:i:s") . ":  sql_insertDocumentDetails : " . $sql_insertDocumentDetails . "\n" . "ERROR: " . mysqli_error($db_open) . "\n\n\n");
										}
									}
									else{
										fwrite($fp_log, date("Y-m-d H:i:s") . ":  sql_insertDocumentDetails : " . $sql_insertDocumentDetails . "\n");
										fwrite($fp_log, date("Y-m-d H:i:s") . ":  thisLastInsertID : " . $thisLastInsertID . "\n");
									}

									if($arrItemDatas[$thisKey]["basketItemPosType"] == 'product') {
										$thisLastInsertID = $dbConnection->db_getInsertID();
										$sql_updateDocumentDetails = "UPDATE `" . constant("TABLE_ORDER_" . $thisConvertDocType  . "_DETAILS") . "`
															SET `orderDocumentDetailGroupingID` = '" . $thisLastInsertID . "'
															WHERE `orderDocumentDetailID` = '" . $thisLastInsertID . "'
											";
										if(!DEBUG_MODE){
											$rs_updateDocumentDetails = $dbConnection->db_query($sql_updateDocumentDetails);
											if(mysqli_error($db_open)) {
												fwrite($fp_log, date("Y-m-d H:i:s") . ":  sql_updateDocumentDetails : " . $sql_updateDocumentDetails . "\n" . "ERROR: " . mysqli_error($db_open) . "\n\n\n");
											}
										}
										else{
											fwrite($fp_log, date("Y-m-d H:i:s") . ":  sql_updateDocumentDetails : " . $sql_updateDocumentDetails . "\n");
											fwrite($fp_log, date("Y-m-d H:i:s") . ":  thisLastInsertID : " . $thisLastInsertID . "\n");
										}
									}
								}
							// EOF STORE DOCUMENT DETAIL DATAS

							// BOF STORE CONVERT/COPY RELATION
								$thisRelationType = 'convert';
								$sql_insertDocumentRelation = "INSERT INTO `" . TABLE_DOCUMENTS_TO_DOCUMENTS . "` (
													`documentsToDocumentsOriginDocumentNumber`,
													`documentsToDocumentsCreatedDocumentNumber`,
													`documentsToDocumentsRelationType`,
													`documentsToDocumentsCreatedDocumentType`
												)
												VALUES (
													'" . $thisOriginDocNumber . "',
													'" . $thisGeneratedDocumentNumber . "',
													'" . $thisRelationType . "',
													'" . $thisConvertDocType . "'
												)
									";
								if(!DEBUG_MODE){
									$rs_insertDocumentRelation = $dbConnection->db_query($sql_insertDocumentRelation);
									if(mysqli_error($db_open)) {
										fwrite($fp_log, date("Y-m-d H:i:s") . ":  sql_insertDocumentRelation : " . $sql_insertDocumentRelation . "\n" . "ERROR: " . mysqli_error($db_open) . "\n\n\n");
									}
								}
								else{
									fwrite($fp_log, date("Y-m-d H:i:s") . ":  sql_insertDocumentRelation : " . $sql_insertDocumentRelation . "\n");
								}
							// EOF STORE CONVERT/COPY RELATION

							// BOF STORE RELATION DOCUMENTS
								if($thisRelationType == 'convert'){
									if(substr($thisOriginDocNumber, 0, 2) != substr($thisGeneratedDocumentNumber, 0, 2)){
										$thisGetRelated_sql = "
											SELECT
												`relatedDocuments_" . substr($thisOriginDocNumber, 0, 2) . "`
												FROM `" . TABLE_RELATED_DOCUMENTS . "`
												WHERE `relatedDocuments_" . substr($thisOriginDocNumber, 0, 2) . "` = '" . $thisOriginDocNumber . "';
											";
										$thisGetRelated_rs = $dbConnection->db_query($thisGetRelated_sql);
										if(mysqli_error($db_open)) {
											fwrite($fp_log, date("Y-m-d H:i:s") . ":  thisGetRelated_sql : " . $thisGetRelated_sql . "\n" . "ERROR: " . mysqli_error($db_open) . "\n\n\n");
										}

										if($dbConnection->db_getMysqlNumRows($thisGetRelated_rs) > 0){
											$thisRelated_sql = "
												UPDATE
													`" . TABLE_RELATED_DOCUMENTS . "`
													SET
														`relatedDocuments_" . substr($thisGeneratedDocumentNumber, 0, 2) . "` = '" . $thisGeneratedDocumentNumber . "'

													WHERE 1
														AND `relatedDocuments_" . substr($thisOriginDocNumber, 0, 2) . "` LIKE '%" . $thisOriginDocNumber . "%'

												";
										}
										else {
											$thisRelated_sql = "
												INSERT
													INTO `" . TABLE_RELATED_DOCUMENTS . "` (
															`relatedDocuments_" . substr($thisOriginDocNumber, 0, 2) . "`,
															`relatedDocuments_" . substr($thisGeneratedDocumentNumber, 0, 2) . "`
														)
														VALUES (
															'" . $thisOriginDocNumber . "',
															'" . $thisGeneratedDocumentNumber . "'
														)
											";
										}
										if(!DEBUG_MODE){
											$thisRelated_rs = $dbConnection->db_query($thisRelated_sql);
											if(mysqli_error($db_open)) {
												fwrite($fp_log, date("Y-m-d H:i:s") . ":  thisRelated_sql : " . $thisRelated_sql . "\n" . "ERROR: " . mysqli_error($db_open) . "\n\n\n");
											}
										}
										else{
											fwrite($fp_log, date("Y-m-d H:i:s") . ":  thisRelated_sql : " . $thisRelated_sql . "\n");
										}
									}
									else if(substr($thisOriginDocNumber, 0, 2) == substr($thisGeneratedDocumentNumber, 0, 2)) {
										$thisRelated_sql = "
											UPDATE
												`" . TABLE_RELATED_DOCUMENTS . "`
												SET `relatedDocuments_" . substr($thisOriginDocNumber, 0, 2) . "` = CONCAT(`relatedDocuments_" . substr($thisOriginDocNumber, 0, 2) . "`, ';', '" . $thisGeneratedDocumentNumber . "')
												WHERE 1
													AND `relatedDocuments_" . substr($thisOriginDocNumber, 0, 2) . "` LIKE '%" . $thisOriginDocNumber . "%'
											";
										if(!DEBUG_MODE){
											$thisRelated_rs = $dbConnection->db_query($thisRelated_sql);
											if(mysqli_error($db_open)) {
												fwrite($fp_log, date("Y-m-d H:i:s") . ":  thisRelated_sql : " . $thisRelated_sql . "\n" . "ERROR: " . mysqli_error($db_open) . "\n\n\n");
											}
										}
										else{
											fwrite($fp_log, date("Y-m-d H:i:s") . ":  thisRelated_sql : " . $thisRelated_sql . "\n");
										}
									}
								}
							// EOF STORE RELATION DOCUMENTS

							// BOF SET PAYMENT STATUS IN RELATED DOCUMENTS
								if(!empty($arrRelatedDocumentNumbers)){
									$arrRelatedDocumentNumbers = array_unique($arrRelatedDocumentNumbers);
									foreach($arrRelatedDocumentNumbers as $thisRelatedDocumentKey => $thisRelatedDocumentValue){
										if(in_array($thisRelatedDocumentKey, array("AB", "RE", "LS", "MA", "M1", "M2", "M3"))){
											fwrite($fp_log, date("Y-m-d H:i:s") . ":  thisRelatedDocument: " . $thisRelatedDocumentKey . " | " . $thisRelatedDocumentValue . "\n");
											$sql_updateRelatedDocumentStatus = "
													UPDATE
														`" . constant('TABLE_ORDER_' . $thisRelatedDocumentKey) . "`
														SET `" . constant('TABLE_ORDER_' . $thisRelatedDocumentKey) . "`.`orderDocumentsStatus` = '" . $selectPaymentStatusNew . "'
														WHERE 1
															AND `" . constant('TABLE_ORDER_' . $thisRelatedDocumentKey) . "`.`orderDocumentsNumber` = '" . $thisRelatedDocumentValue . "'

												";
											if(!DEBUG_MODE){
												$rs_updateRelatedDocumentStatus = $dbConnection->db_query($sql_updateRelatedDocumentStatus);
												if(mysqli_error($db_open)) {
													fwrite($fp_log, date("Y-m-d H:i:s") . ":  sql_updateRelatedDocumentStatus : " . $sql_updateRelatedDocumentStatus . "\n" . "ERROR: " . mysqli_error($db_open) . "\n\n\n");
												}
											}
											else{
												fwrite($fp_log, date("Y-m-d H:i:s") . ":  sql_updateRelatedDocumentStatus : " . $sql_updateRelatedDocumentStatus . "\n");
											}
										}
									}
								}
							// EOF SET PAYMENT STATUS IN RELATED DOCUMENTS

							// BOF SET CUSTOMER BEHAVIOUR
								if(in_array($thisConvertDocType, array('M1', 'M2', 'M3', 'IK'))){
									$sql_setCustomerBehaviour = "
											UPDATE `" . TABLE_CUSTOMERS . "`
											SET `customersBadPaymentBehavior` = '1'
											WHERE `customersKundennummer` = '" . $ds_getDocumentsToHandle["orderDocumentsAddressCompanyCustomerNumber"] . "'
										";
									if(!DEBUG_MODE){
										$rs_setCustomerBehaviour = $dbConnection->db_query($sql_setCustomerBehaviour);
										if(mysqli_error($db_open)) {
											fwrite($fp_log, date("Y-m-d H:i:s") . ":  sql_setCustomerBehaviour : " . $sql_setCustomerBehaviour . "\n" . "ERROR: " . mysqli_error($db_open) . "\n\n\n");
										}
									}
									else{
										fwrite($fp_log, date("Y-m-d H:i:s") . ":  sql_setCustomerBehaviour : " . $sql_setCustomerBehaviour . "\n");
									}
								}
							// EOF SET CUSTOMER BEHAVIOUR

							// BOF createPDF
								try {
									$html2pdf = new HTML2PDF('P', 'A4', 'de', true, 'UTF-8', array(18, 5, 5, 2));

									if(file_exists(DIRECTORY_CREATED_DOCUMENTS_CUSTOMERS . $thisCreatedPdfName)) {
										// chmod(DIRECTORY_CREATED_DOCUMENTS_CUSTOMERS . $thisCreatedPdfName , "0777");
										unlink(DIRECTORY_CREATED_DOCUMENTS_CUSTOMERS . $thisCreatedPdfName);
									}
									clearstatcache();

									$pdfContentPage = $pdfCreateContent;
									#$pdfContentPage = preg_replace('/pageset="old"/', '', $pdfContentPage);
									$pdfContentPage = preg_replace('/<page pageset="old" backtop="0mm" backbottom="55mm" backleft="0mm" backright="10mm">/', '<page>', $pdfContentPage);
									$pdfContentPage = stripslashes($pdfContentPage);

									ob_start();
									echo $pdfContentPage;
									$contentPDF = ob_get_clean();

									$html2pdf->writeHTML($contentPDF, isset($_GET['vuehtml']));
									$html2pdf->Output(DIRECTORY_CREATED_DOCUMENTS_CUSTOMERS . $thisCreatedPdfName, 'F', '');

									// BOF STORE ORDERS TO DOCUMENTS
										if(file_exists($thisCreatedPdfName)) {
											unlink($thisCreatedPdfName);

											$sql_updateOrdersToDocuments = array();
											foreach($thisDocumentOrdersIDs as $thisKey => $thisValue) {
												$arrSqlUpdateOrdersToDocuments[] = " `ordersToDocumentsOrderID` = '" . $thisValue . "' ";
											}

											$sql_updateOrdersToDocuments = "UPDATE `" . TABLE_ORDERS_TO_DOCUMENTS . "`
														SET `ordersToDocumentsStatus` = 'created'
														WHERE ". implode(" OR ", $arrSqlUpdateOrdersToDocuments) . "
												";
											if(!DEBUG_MODE){
												$rs_updateOrdersToDocuments = $dbConnection->db_query($sql_updateOrdersToDocuments);
												if(mysqli_error($db_open)) {
													fwrite($fp_log, date("Y-m-d H:i:s") . ":  sql_updateOrdersToDocuments : " . $sql_updateOrdersToDocuments . "\n" . "ERROR: " . mysqli_error($db_open) . "\n\n\n");
												}
											}
											else{
												fwrite($fp_log, date("Y-m-d H:i:s") . ":  sql_updateOrdersToDocuments : " . $sql_updateOrdersToDocuments . "\n");
											}
										}
										clearstatcache();
									// EOF STORE ORDERS TO DOCUMENTS
								}
								catch(HTML2PDF_exception $e) {
									// echo $e;
									fwrite($fp_log, "HTML2PDF_exception: " . $e . "\n");
									exit;
								}
								clearstatcache();
							// EOF createPDF

							// BOF STORE INTO CREATED DOCUMENTS
								$sql_replaceCreatedDocuments = "
										REPLACE INTO `" . TABLE_CREATED_DOCUMENTS . "` (
											`createdDocumentsID`,
											`createdDocumentsType`,
											`createdDocumentsNumber`,
											`createdDocumentsCustomerNumber`,
											`createdDocumentsTitle`,
											`createdDocumentsFilename`,
											`createdDocumentsContent`,
											`createdDocumentsUserID`,
											`createdDocumentsTimeCreated`,
											`createdDocumentsOrderIDs`
										)
										VALUES (
											'%',
											'" . $thisConvertDocType . "',
											'" . $thisGeneratedDocumentNumber . "',
											'" . $ds_getDocumentsToHandle["orderDocumentsCustomerNumber"] . "',
											'" . $thisGeneratedDocumentNumber . "',
											'" . DIRECTORY_CREATED_DOCUMENTS_CUSTOMERS . $thisCreatedPdfName . "',
											'',
											'0',
											NOW(),
											'".implode(";", $arrItemIDs) . "'
										)
									";

								// '".$_POST["documentOrdersIDs"] . "'

								if(!DEBUG_MODE){
									$rs_replaceCreatedDocuments = $dbConnection->db_query($sql_replaceCreatedDocuments);
									if(mysqli_error($db_open)) {
										fwrite($fp_log, date("Y-m-d H:i:s") . ":  sql_replaceCreatedDocuments : " . $sql_replaceCreatedDocuments . "\n" . "ERROR: " . mysqli_error($db_open) . "\n\n\n");
									}
								}
								else{
									fwrite($fp_log, date("Y-m-d H:i:s") . ":  sql_replaceCreatedDocuments : " . $sql_replaceCreatedDocuments . "\n");
								}

								$thisInsertID = $dbConnection->db_getInsertID();
								fwrite($fp_log, date("Y-m-d H:i:s") . ":  thisInsertID : " . $thisInsertID . "\n");

								$thisDocumentOrdersIDs = explode(";", $ds_getDocumentsToHandle["orderDocumentsOrdersIDs"]);
								if(!empty($thisDocumentOrdersIDs)) {
									$arrSql_replaceOrdersToDocuments = array();
									foreach($thisDocumentOrdersIDs as $thisKey => $thisValue) {
										$arrSql_replaceOrdersToDocuments[] = "	(
																'%',
																'".$thisValue."',
																'".$thisInsertID."',
																'unknown',
																'" . $thisConvertDocType . "',
																NOW()
															)";
									}
									if(!empty($arrSql_replaceOrdersToDocuments)) {
										$sql_replaceOrdersToDocuments = "
												REPLACE INTO `" . TABLE_ORDERS_TO_DOCUMENTS . "` (
														`ordersToDocumentsID`,
														`ordersToDocumentsOrderID`,
														`ordersToDocumentsDocumentID`,
														`ordersToDocumentsStatus`,
														`ordersToDocumentsType`,
														`ordersToDocumentsCreationTime`
													) VALUES " . implode(",", $arrSql_replaceOrdersToDocuments) . " ;
											";
										if(!DEBUG_MODE){
											$rs_replaceOrdersToDocuments = $dbConnection->db_query($sql_replaceOrdersToDocuments);
											if(mysqli_error($db_open)) {
												fwrite($fp_log, date("Y-m-d H:i:s") . ":  sql_replaceOrdersToDocuments : " . $sql_replaceOrdersToDocuments . "\n" . "ERROR: " . mysqli_error($db_open) . "\n\n\n");
											}
										}
										else{
											fwrite($fp_log, date("Y-m-d H:i:s") . ":  sql_replaceOrdersToDocuments : " . $sql_replaceOrdersToDocuments . "\n");
										}
									}

								}
							// EOF STORE INTO CREATED DOCUMENTS

							// BOF CREATE MAIL TEXT
								$thisDocumentMailContent = '';
								$thisDocumentMailContent .= $arrMailContentDatas[$thisConvertDocType]['MAIL_SALUTATION'];
								$thisDocumentMailContent .= $arrMailContentDatas[$thisConvertDocType]['MAIL_CONTENT'];
								if($ds_getDocumentsToHandle["orderDocumentsPaymentType"] != '8'){
									$thisDocumentMailContent .= $arrMailContentDatas[$thisConvertDocType]['MAIL_CONTENT_ADDITIONAL'];
								}
								$thisDocumentMailContent .= $arrMailContentDatas[$thisConvertDocType]['MAIL_REGARDS'];
								$thisDocumentMailContent = preg_replace("/{###DOCUMENT_NUMBER###}/", $thisGeneratedDocumentNumber, $thisDocumentMailContent);

								$thisDocumentMailContent = preg_replace("/{###DOCUMENT_TYPE_NAME###}/", $arrDocumentTypeDatas[$thisConvertDocType]["createdDocumentsTypesName"], $thisDocumentMailContent);
							// EOF CREATE MAIL TEXT

							// BOF SEND MAIL

								// BOF CREATE SUBJECT
									$thisMailSubject = strip_tags($loadSubjectText);
									$thisMailSubject = preg_replace("/{###DOCUMENT_NUMBER###}/", $thisGeneratedDocumentNumber, $thisMailSubject);
								// EOF CREATE SUBJECT

								// BOF GET ATTACHED FILE
									$arrAttachmentFiles = array (
										rawurldecode(DIRECTORY_CREATED_DOCUMENTS_CUSTOMERS . $thisCreatedPdfName) => str_replace(" ", "_", rawurldecode(myUnHtmlEntities($thisCreatedPdfName)))
									);

								// EOF GET ATTACHED FILE

								$thisMailSender = MAIL_ADDRESS_FROM_FINANCIAL;

								// BOF SEND MAIL TO CUSTOMER
									$arrMailRecipients = array(
										$invoiceRecipientCustomerMail
									);
									$thisMailRecipient = implode(";", $arrMailRecipients);

									if(DEBUG_MODE){
										$thisMailRecipient = "webmaster@burhan-ctr.de";
									}

									if($thisMailRecipient != ""){
										unset($createMail);
										if(!DEBUG_MODE){
											$createMail = new createMail(
																$thisMailSubject . ' [' . $ds_getDocumentsToHandle["orderDocumentsCustomerNumber"] . ' | ' . $customersData["Firmenname"] . ']',														// TEXT:	MAIL_SUBJECT
																$thisConvertDocType,											// STRING:	DOCUMENT TYPE
																$thisGeneratedDocumentNumber,									// STRING:	DOCUMENT NUMBER
																$arrAttachmentFiles,											// ARRAY:	ATTACHMENT FILES
																$thisMailRecipient,	 											// STRING:	RECIPIENTS
																$arrMailContentDatas[$thisConvertDocType],						// MAIL_TEXT_TEMPLATE
																$mailTemplate["html"],											// MAIL_BODY_TEMPLATE
																'',																// STRING:	ADDITIONAL TEXT
																$thisDocumentMailContent,										// STRING:	SEND ATTACHED TEXT
																true,															// BOOLEAN:	SEND ATTACHED DOCUMENT
																DIRECTORY_MAIL_IMAGES,											// DIRECTORY_MAIL_IMAGES
																$thisMailSender													// MAIL SENDER
															);
											#$createMail->setAttachmentPathPrefix('../');
											#$createMail->setRecipientBcc("webmaster@burhan-ctr.de");

											$createMailResult = $createMail->returnResult();
											$sendMailToClient = $createMailResult;
										}
										else{
											$sendMailToClient = false;
										}
									}

									fwrite($fp_log, "mail-recipient: " . $thisMailRecipient . "\n");
									fwrite($fp_log, "mail-sended: " . $sendMailToClient . "\n");
								// EOF SEND MAIL TO CUSTOMER
							// EOF SEND MAIL

							/*
							#####dd('unserializedOrderDocumentsInvoiceRecipientData');
							#####dd('unserializedOrderDocumentsDeliveryRecipientData');
							#####dd('invoiceRecipientCustomerNumber');
							#####dd('deliveryRecipientCustomerNumber');
							#####dd('ds_getDocumentsToHandle');
							*/

							/*
							#####dd('loadedDocumentDatas');
							#####dd('loadedAdressDatas');

							#####dd('thisOriginDocType');
							#####dd('thisConvertDocType');

							#
							#####dd('arrRelatedDocumentNumbers');
							*/

							######dd('arrItemIDs');
							######dd('arrItemDatas');
							######dd('arrPriceDatas');
						}
						else {
							fwrite($fp_log, ($countItems + 1) . " | WURDE NICHT AUSGEFUEHRT" . "\n");
						}

						fwrite($fp_log, ($countItems + 1) . " | ENDE: " . date("Y-m-d H:i:s") . "\n");
						fwrite($fp_log, "\n" . str_repeat("-", 50) . "\n");
						$countItems++;
					}
				}
			}
			else {
				fwrite($fp_log, "Das Array 'arrDocumentsToHandle' ist leer!" . "\n");
			}
		// EOF GET DOCUMENTS TO HANDLE
exit;
		// BOF UPDATE CUSTOMER SET PAYMENT BEHAVIOUR STATUS
			$sql_getCustomersBadBehaviour = "
					SELECT
						`tempTable`.`orderDocumentsAddressCompanyCustomerNumber`

						FROM(
							SELECT
								`orderDocumentsAddressCompanyCustomerNumber`
								FROM `" . TABLE_ORDER_FIRST_DEMANDS . "`
								WHERE 1
									AND `orderDocumentsAddressCompanyCustomerNumber` != ''
								GROUP BY `orderDocumentsAddressCompanyCustomerNumber`

							UNION

							SELECT
								`orderDocumentsAddressCompanyCustomerNumber`
								FROM `" . TABLE_ORDER_SECOND_DEMANDS . "`
								WHERE 1
									AND `orderDocumentsAddressCompanyCustomerNumber` != ''
								GROUP BY `orderDocumentsAddressCompanyCustomerNumber`
						) AS `tempTable`

						LEFT JOIN `" . TABLE_CUSTOMERS . "`
						ON(`tempTable`.`orderDocumentsAddressCompanyCustomerNumber` = `" . TABLE_CUSTOMERS . "`.`customersKundennummer`)

						WHERE 1
							AND `customersBadPaymentBehavior` != '1'

						GROUP BY `tempTable`.`orderDocumentsAddressCompanyCustomerNumber`
				";
			$rs_getCustomersBadBehaviour = $dbConnection->db_query($sql_getCustomersBadBehaviour);
			if(mysqli_error($db_open)) {
				fwrite($fp_log, date("Y-m-d H:i:s") . ":  sql_getCustomersBadBehaviour : " . $sql_getCustomersBadBehaviour . "\n" . "ERROR: " . mysqli_error($db_open) . "\n\n\n");
			}
			$countFoundCustomersBadBehaviour = $dbConnection->db_getMysqlNumRows($rs_getCustomersBadBehaviour);

			if($countFoundCustomersBadBehaviour > 0){
				while($ds_getCustomersBadBehaviour = mysqli_fetch_assoc($rs_getCustomersBadBehaviour)){
					$sql_updateCustomersBadBehaviour = "
						UPDATE
							`" . TABLE_CUSTOMERS . "`
							SET
								`" . TABLE_CUSTOMERS . "`.`customersBadPaymentBehavior` = '1'

							WHERE 1
								AND `" . TABLE_CUSTOMERS . "`.`customersKundennummer` = '" . $ds_getCustomersBadBehaviour["orderDocumentsAddressCompanyCustomerNumber"] . "'
								AND `" . TABLE_CUSTOMERS . "`.`customersBadPaymentBehavior` != '1'
						";
					#$rs_updateCustomersBadBehaviour = $dbConnection->db_query($sql_updateCustomersBadBehaviour);
					fwrite($fp_log, "KUNDEN-UPDATE : " . $ds_getCustomersBadBehaviour["orderDocumentsAddressCompanyCustomerNumber"] . "\n");
					if(mysqli_error($db_open)) {
						fwrite($fp_log, date("Y-m-d H:i:s") . ":  sql_updateCustomersBadBehaviour : " . $sql_updateCustomersBadBehaviour . "\n" . "ERROR: " . mysqli_error($db_open) . "\n\n\n");
					}
				}
			}
			else {
				fwrite($fp_log, "Keine Kunden mit schlechter Zahlungsmoral gefunden!" . "\n");
			}
		// EOF UPDATE CUSTOMER SET PAYMENT BEHAVIOUR STATUS

		// BOF UPDATE CUSTOMER PAYMENT ONLY PREPAY
			$sql_getCustomersNotPaidSecondDemand = "
					SELECT
						`tempTable`.`orderDocumentsAddressCompanyCustomerNumber`
						FROM (
							SELECT
								`" . TABLE_ORDER_SECOND_DEMANDS . "`.`orderDocumentsID`,
								`" . TABLE_ORDER_SECOND_DEMANDS . "`.`orderDocumentsNumber`,
								`" . TABLE_RELATED_DOCUMENTS . "`.`relatedDocuments_RE`,
								`" . TABLE_ORDER_INVOICES . "`.`orderDocumentsDocumentDate` AS `orderDocumentsInvoiceDate`,

								`" . TABLE_PAYMENT_STATUS_TYPES . "`.`paymentStatusTypesID`,
								`" . TABLE_PAYMENT_STATUS_TYPES . "`.`paymentStatusTypesShortName`,
								`" . TABLE_PAYMENT_STATUS_TYPES . "`.`paymentStatusTypesName`,

								`" . TABLE_PAYMENT_TYPES . "`.`paymentTypesID`,
								`" . TABLE_PAYMENT_TYPES . "`.`paymentTypesShortName`,
								`" . TABLE_PAYMENT_TYPES . "`.`paymentTypesName`,

								`" . TABLE_PAYMENT_CONDITIONS . "`.`paymentConditionsID`,
								`" . TABLE_PAYMENT_CONDITIONS . "`.`paymentConditionsShortName`,
								`" . TABLE_PAYMENT_CONDITIONS . "`.`paymentConditionsName`,

								DATE_ADD(`" . TABLE_ORDER_SECOND_DEMANDS . "`.`orderDocumentsDocumentDate`, INTERVAL (7 + 14) DAY) AS `orderDocumentsDeadline`,

								`" . TABLE_ORDER_SECOND_DEMANDS . "`.`orderDocumentsProcessingDate`,
								`" . TABLE_ORDER_SECOND_DEMANDS . "`.`orderDocumentsDocumentDate`,
								`" . TABLE_ORDER_SECOND_DEMANDS . "`.`orderDocumentsOrderDate`,

								IF(`" . TABLE_ORDER_SECOND_DEMANDS . "`.`orderDocumentsType` IS NULL || `" . TABLE_ORDER_SECOND_DEMANDS . "`.`orderDocumentsType` = '', 'M2', `" . TABLE_ORDER_SECOND_DEMANDS . "`.`orderDocumentsType`) AS `orderDocumentsType`,
								`" . TABLE_ORDER_SECOND_DEMANDS . "`.`orderDocumentsOrdersIDs`,
								`" . TABLE_ORDER_SECOND_DEMANDS . "`.`orderDocumentsCustomerNumber`,
								`" . TABLE_ORDER_SECOND_DEMANDS . "`.`orderDocumentsCustomersOrderNumber`,

								`" . TABLE_ORDER_SECOND_DEMANDS . "`.`orderDocumentsSalesman`,
								`" . TABLE_ORDER_SECOND_DEMANDS . "`.`orderDocumentsKommission`,
								`" . TABLE_ORDER_SECOND_DEMANDS . "`.`orderDocumentsAddressCompanyCustomerNumber`,
								`" . TABLE_ORDER_SECOND_DEMANDS . "`.`orderDocumentsAddressCompany`,
								`" . TABLE_ORDER_SECOND_DEMANDS . "`.`orderDocumentsAddressName`,
								`" . TABLE_ORDER_SECOND_DEMANDS . "`.`orderDocumentsAddressStreet`,
								`" . TABLE_ORDER_SECOND_DEMANDS . "`.`orderDocumentsAddressStreetNumber`,
								`" . TABLE_ORDER_SECOND_DEMANDS . "`.`orderDocumentsAddressZipcode`,
								`" . TABLE_ORDER_SECOND_DEMANDS . "`.`orderDocumentsAddressCity`,
								`" . TABLE_ORDER_SECOND_DEMANDS . "`.`orderDocumentsAddressCountry`,
								`" . TABLE_ORDER_SECOND_DEMANDS . "`.`orderDocumentsAddressMail`,
								`" . TABLE_ORDER_SECOND_DEMANDS . "`.`orderDocumentsSumPrice`,
								`" . TABLE_ORDER_SECOND_DEMANDS . "`.`orderDocumentsDiscount`,
								`" . TABLE_ORDER_SECOND_DEMANDS . "`.`orderDocumentsDiscountType`,
								`" . TABLE_ORDER_SECOND_DEMANDS . "`.`orderDocumentsDiscountPercent`,
								`" . TABLE_ORDER_SECOND_DEMANDS . "`.`orderDocumentsMwst`,
								`" . TABLE_ORDER_SECOND_DEMANDS . "`.`orderDocumentsMwstPrice`,
								`" . TABLE_ORDER_SECOND_DEMANDS . "`.`orderDocumentsMwstType`,
								`" . TABLE_ORDER_SECOND_DEMANDS . "`.`orderDocumentsShippingCosts`,
								`" . TABLE_ORDER_SECOND_DEMANDS . "`.`orderDocumentsShippingCostsPackages`,
								`" . TABLE_ORDER_SECOND_DEMANDS . "`.`orderDocumentsShippingCostsPerPackage`,
								`" . TABLE_ORDER_SECOND_DEMANDS . "`.`orderDocumentsPackagingCosts`,
								`" . TABLE_ORDER_SECOND_DEMANDS . "`.`orderDocumentsCashOnDelivery`,
								`" . TABLE_ORDER_SECOND_DEMANDS . "`.`orderDocumentsTotalPrice`,
								`" . TABLE_ORDER_SECOND_DEMANDS . "`.`orderDocumentsBankAccount`,
								`" . TABLE_ORDER_SECOND_DEMANDS . "`.`orderDocumentsPaymentCondition`,
								`" . TABLE_ORDER_SECOND_DEMANDS . "`.`orderDocumentsPaymentType`,
								`" . TABLE_ORDER_SECOND_DEMANDS . "`.`orderDocumentsSkonto`,
								`" . TABLE_ORDER_SECOND_DEMANDS . "`.`orderDocumentsDeliveryType`,
								`" . TABLE_ORDER_SECOND_DEMANDS . "`.`orderDocumentsDeliveryTypeNumber`,
								`" . TABLE_ORDER_SECOND_DEMANDS . "`.`orderDocumentsSubject`,
								`" . TABLE_ORDER_SECOND_DEMANDS . "`.`orderDocumentsContent`,
								`" . TABLE_ORDER_SECOND_DEMANDS . "`.`orderDocumentsDocumentPath`,
								`" . TABLE_ORDER_SECOND_DEMANDS . "`.`orderDocumentsTimestamp`,
								`" . TABLE_ORDER_SECOND_DEMANDS . "`.`orderDocumentsStatus`,
								`" . TABLE_ORDER_SECOND_DEMANDS . "`.`orderDocumentsSendMail`,
								`" . TABLE_ORDER_SECOND_DEMANDS . "`.`orderDocumentsInterestPercent`,
								`" . TABLE_ORDER_SECOND_DEMANDS . "`.`orderDocumentsInterestPrice`,
								`" . TABLE_ORDER_SECOND_DEMANDS . "`.`orderDocumentsChargesPrice`,
								`" . TABLE_ORDER_SECOND_DEMANDS . "`.`orderDocumentsDeadlineDate`,
								`" . TABLE_ORDER_SECOND_DEMANDS . "`.`orderDocumentsCustomerGroupID`,
								`" . TABLE_ORDER_SECOND_DEMANDS . "`.`orderDocumentsIsCollectiveInvoice`,
								`" . TABLE_ORDER_SECOND_DEMANDS . "`.`orderDocumentsStatusChangeDate`,
								`" . TABLE_ORDER_SECOND_DEMANDS . "`.`orderDocumentsInvoiceRecipientData`,
								`" . TABLE_ORDER_SECOND_DEMANDS . "`.`orderDocumentsDeliveryRecipientData`

							FROM `" . TABLE_ORDER_SECOND_DEMANDS . "`

							LEFT JOIN `" . TABLE_RELATED_DOCUMENTS . "`
							ON(`" . TABLE_ORDER_SECOND_DEMANDS . "`.`orderDocumentsNumber` = `" . TABLE_RELATED_DOCUMENTS . "`.`relatedDocuments_M2`)

							LEFT JOIN `" . TABLE_ORDER_INVOICES . "`
							ON(`" . TABLE_RELATED_DOCUMENTS . "`.`relatedDocuments_RE` = `" . TABLE_ORDER_INVOICES . "`.`orderDocumentsNumber`)

							LEFT JOIN `" . TABLE_PAYMENT_TYPES . "`
							ON(`" . TABLE_ORDER_SECOND_DEMANDS . "`.`orderDocumentsPaymentType` = `" . TABLE_PAYMENT_TYPES . "`.`paymentTypesID`)

							LEFT JOIN `" . TABLE_PAYMENT_STATUS_TYPES . "`
							ON(`" . TABLE_ORDER_SECOND_DEMANDS . "`.`orderDocumentsStatus` = `" . TABLE_PAYMENT_STATUS_TYPES . "`.`paymentStatusTypesID`)

							LEFT JOIN `" . TABLE_PAYMENT_CONDITIONS . "`
							ON(`" . TABLE_ORDER_SECOND_DEMANDS . "`.`orderDocumentsPaymentCondition` = `" . TABLE_PAYMENT_CONDITIONS . "`.`paymentConditionsID`)

							WHERE 1
								AND `" . TABLE_ORDER_SECOND_DEMANDS . "`.`orderDocumentsStatus` != '2'
								AND `" . TABLE_ORDER_SECOND_DEMANDS . "`.`orderDocumentsStatus` != '3'
								AND `" . TABLE_ORDER_SECOND_DEMANDS . "`.`orderDocumentsStatus` != '4'
								AND `" . TABLE_ORDER_SECOND_DEMANDS . "`.`orderDocumentsStatus` != '10'
								AND `" . TABLE_ORDER_SECOND_DEMANDS . "`.`orderDocumentsStatus` != '12'
								AND `" . TABLE_RELATED_DOCUMENTS . "`.`relatedDocuments_GU` IS NULL
								AND `" . TABLE_RELATED_DOCUMENTS . "`.`relatedDocuments_MA` IS NOT NULL
								AND `" . TABLE_RELATED_DOCUMENTS . "`.`relatedDocuments_M1` IS NOT NULL
								AND `" . TABLE_RELATED_DOCUMENTS . "`.`relatedDocuments_M2` IS NOT NULL

							HAVING
								DATE_FORMAT(NOW(), '%Y-%m-%d') > `orderDocumentsDeadline`
						) AS `tempTable`

						LEFT JOIN `" . TABLE_CUSTOMERS . "`
						ON(`" . TABLE_CUSTOMERS . "`.`customersKundennummer` = `tempTable`.`orderDocumentsAddressCompanyCustomerNumber`)

						WHERE 1
							AND `" . TABLE_CUSTOMERS . "`.`customersPaymentOnlyPrepayment` != '1'

						GROUP BY `tempTable`.`orderDocumentsAddressCompanyCustomerNumber`
				";
			$rs_getCustomersNotPaidSecondDemand = $dbConnection->db_query($sql_getCustomersNotPaidSecondDemand);
			if(mysqli_error($db_open)) {
				fwrite($fp_log, date("Y-m-d H:i:s") . ":  sql_getCustomersNotPaidSecondDemand : " . $sql_getCustomersNotPaidSecondDemand . "\n" . "ERROR: " . mysqli_error($db_open) . "\n\n\n");
			}
			$countFoundCustomersNotPaidSecondDemand = $dbConnection->db_getMysqlNumRows($rs_getCustomersNotPaidSecondDemand);

			if($countFoundCustomersNotPaidSecondDemand > 0){
				while($ds_getCustomersNotPaidSecondDemand = mysqli_fetch_assoc($rs_getCustomersNotPaidSecondDemand)){
					$sql_updateCustomersNotPaidSecondDemand = "
						UPDATE
							`" . TABLE_CUSTOMERS . "`
							SET
								`" . TABLE_CUSTOMERS . "`.`customersBezahlart` = '2',
								`" . TABLE_CUSTOMERS . "`.`customersZahlungskondition` = '2',
								`" . TABLE_CUSTOMERS . "`.`customersBadPaymentBehavior` = '1'
								`" . TABLE_CUSTOMERS . "`.`customersPaymentOnlyPrepayment` = '1'

							WHERE 1
								AND `" . TABLE_CUSTOMERS . "`.`customersKundennummer` = '" . $ds_getCustomersNotPaidSecondDemand["orderDocumentsAddressCompanyCustomerNumber"] . "'
								AND `" . TABLE_CUSTOMERS . "`.`customersPaymentOnlyPrepayment` != '1'
						";
					#$rs_updateCustomersNotPaidSecondDemand = $dbConnection->db_query($sql_updateCustomersNotPaidSecondDemand);
					fwrite($fp_log, "KUNDEN-UPDATE : " . $ds_getCustomersNotPaidSecondDemand["orderDocumentsAddressCompanyCustomerNumber"] . "\n");
					if(mysqli_error($db_open)) {
						fwrite($fp_log, date("Y-m-d H:i:s") . ":  sql_updateCustomersNotPaidSecondDemand : " . $sql_updateCustomersNotPaidSecondDemand . "\n" . "ERROR: " . mysqli_error($db_open) . "\n\n\n");
					}
				}
			}
			else {
				fwrite($fp_log, "Keine Kunden mit überfälliger Zahlung der zweiten Mahnung gefunden!" . "\n");
			}
		// EOF UPDATE CUSTOMER PAYMENT ONLY PREPAY
	}
	else {
		fwrite($fp_log, "'MANDATOR' ist leer!" . "\n");
	}

	fwrite($fp_log, "ENDE: " . date("Y-m-d H:i:s") . "\n\n" . str_repeat("#", 100) . "\n");

	if($dbConnection){
		$dbConnection->db_close();
	}
	if($fp_log){
		fclose($fp_log);
	}
?>