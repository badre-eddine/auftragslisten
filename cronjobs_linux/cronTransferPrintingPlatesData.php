<?php

	#error_reporting(0);
	error_reporting(E_ALL & ~E_NOTICE);
	set_time_limit(0);

	DEFINE('BASEPATH_Auftragslisten', "/var/www/Auftragslisten/");
	#DEFINE('BASEPATH_CRONJOBS', $_SERVER['DOCUMENT_ROOT'] . "/");
	DEFINE('BASEPATH_CRONJOBS', BASEPATH_Auftragslisten . "cronjobs_linux/");

	require_once(BASEPATH_CRONJOBS . 'cronjobsConfig.inc.php');

	require_once(BASEPATH_Auftragslisten . 'config/configParams_' . strtolower(MANDATOR) . '.inc.php');
	require_once(BASEPATH_Auftragslisten . 'config/configBasic.inc.php');
	require_once(BASEPATH_Auftragslisten . 'config/configTables.inc.php');
	require_once(BASEPATH_Auftragslisten . 'inc/functions.inc.php');
	require_once(BASEPATH_Auftragslisten . 'classes/DB_Connection.class.php');

	// BOF DEFINE DB_CONNECTION
		DEFINE("DB_HOST_EXTERN_PRODUCTION", "mysql.b3-werbepartner.de");
		DEFINE("DB_NAME_EXTERN_PRODUCTION", "usr_web23_9");
		DEFINE("DB_USER_EXTERN_PRODUCTION", "web23");
		DEFINE("DB_PASSWORD_EXTERN_PRODUCTION", "ZutXxKSq");
	// EOF DEFINE DB_CONNECTION

	DEFINE('DIRECTORY_DOCUMENTS', 'documents_common/');

	DEFINE('DIRECTORY_LAYOUT_PREVIEW_IMAGES', 'layoutPreviewImages/');
	DEFINE('DIRECTORY_LAYOUT_PREVIEW_IMAGES_BASEPATH_LOCAL', BASEPATH_Auftragslisten . DIRECTORY_DOCUMENTS . DIRECTORY_LAYOUT_PREVIEW_IMAGES);
	DEFINE('DIRECTORY_LAYOUT_PREVIEW_IMAGES_BASEPATH_EXTERNAL', "/html/productions.burhan-ctr.de/" . DIRECTORY_LAYOUT_PREVIEW_IMAGES);

	DEFINE('DIRECTORY_PRINT_PLATE_FILES', 'printPlateFiles/');
	DEFINE('DIRECTORY_PRINT_PLATE_FILES_BASEPATH_LOCAL', BASEPATH_Auftragslisten . DIRECTORY_DOCUMENTS . DIRECTORY_PRINT_PLATE_FILES);
	DEFINE('DIRECTORY_PRINT_PLATE_FILES_BASEPATH_EXTERNAL', "/html/productions.burhan-ctr.de/" . DIRECTORY_PRINT_PLATE_FILES);

	DEFINE('FILENAME_LOGFILE', BASEPATH_CRONJOBS . "logs/" . MANDATOR . "_" . basename($_SERVER["PHP_SELF"]) . ".txt");

	if(file_exists(FILENAME_LOGFILE)){
		unlink(FILENAME_LOGFILE);
	}
	$fp_log = fopen(FILENAME_LOGFILE, "w");

	fwrite($fp_log, date("Y-m-d H:i:s") . ": MANDATOR : " . MANDATOR . " | getMandator : " . $getMandator . "\n");

	$dbConnection = new DB_Connection();
	$db_open = $dbConnection->db_connect();

	if(constant("DB_HOST_EXTERN_PRODUCTION") != '' && constant("DB_NAME_EXTERN_PRODUCTION") != '' && constant("DB_USER_EXTERN_PRODUCTION") != '' && constant("DB_PASSWORD_EXTERN_PRODUCTION")) {
		$existsOnlineProduction = true;
	}
	else {
		$existsOnlineProduction = false;
	}

	fwrite($fp_log, date("Y-m-d H:i:s") . "\n");

	if($existsOnlineProduction){
		$dbConnection_ExternProduction = new DB_Connection(DB_HOST_EXTERN_PRODUCTION, '', DB_NAME_EXTERN_PRODUCTION, DB_USER_EXTERN_PRODUCTION, DB_PASSWORD_EXTERN_PRODUCTION);
		if(mysqli_error($db_openExternProduction)) { fwrite($fp_log, date("Y-m-d H:i:s") . ": " . mysqli_error($db_openExternProduction) . "\n"); }
		$db_openExternProduction = $dbConnection_ExternProduction->db_connect();
		if(mysqli_error($db_openExternProduction)) { fwrite($fp_log, date("Y-m-d H:i:s") . ": " . mysqli_error($db_openExternProduction) . "\n"); }

		// BOF GET EXTERNAL PRINTING PLATE NUMBERS AND IMPORT TO Auftragslisten
			$sql_extern = "
					SELECT
							`" . TABLE_PRINTING_PLATES_DATA . "`.`printingPlatesDataID`,
							`" . TABLE_PRINTING_PLATES_DATA . "`.`printingPlatesDataCustomerNumber`,
							`" . TABLE_PRINTING_PLATES_DATA . "`.`printingPlatesDataPlateNumber`,
							`" . TABLE_PRINTING_PLATES_DATA . "`.`printingPlatesDataModifiedDate`

						FROM `" . TABLE_PRINTING_PLATES_DATA . "`

						WHERE 1
							AND `printingPlatesDataPlateNumber` != ''
							AND `printingPlatesDataPlateNumber` IS NOT NULL
							AND `printingPlatesDataModifiedDate` > 0
				";

			$rs_extern = $dbConnection_ExternProduction->db_query($sql_extern);

			while($ds_extern = mysqli_fetch_assoc($rs_extern)){
				$sql_localUpdate = "
						UPDATE
							`" . TABLE_PRINTING_PLATES_DATA . "`

							SET
								`printingPlatesDataPlateNumber` = '" . $ds_extern["printingPlatesDataPlateNumber"] . "',
								`printingPlatesDataModifiedDate` = '" . $ds_extern["printingPlatesDataModifiedDate"] . "'

							WHERE 1
								AND `printingPlatesDataID` = '" . $ds_extern["printingPlatesDataID"] . "'
								AND `printingPlatesDataCustomerNumber` = '" . $ds_extern["printingPlatesDataCustomerNumber"] . "'
								AND `printingPlatesDataPlateNumber` != '" . $ds_extern["printingPlatesDataPlateNumber"] . "'
								AND `printingPlatesDataModifiedDate` < '" . $ds_extern["printingPlatesDataModifiedDate"] . "'
					";

				$rs_localUpdate = $dbConnection->db_query($sql_localUpdate);
			}
		// EOF GET EXTERNAL PRINTING PLATE NUMBERS AND IMPORT TO Auftragslisten

		// BOF CREATE EXTERNAL TMP-TABLE
			$sql_externalCreate = "CREATE TABLE IF NOT EXISTS `_tmp_" . TABLE_PRINTING_PLATES_DATA . "` LIKE `" . TABLE_PRINTING_PLATES_DATA . "`";
			$rs_externalCreate = $dbConnection_ExternProduction->db_query($sql_externalCreate);
		// EOF CREATE EXTERNAL TMP-TABLE
		
		// BOF EXPORT PRINTING PLATES DATAS FROM Auftragslisten TO ONLINE PRODUCTION			
			$sql_local = "

				SELECT
					`" . TABLE_PRINTING_PLATES_DATA . "`.`printingPlatesDataID`,
					`" . TABLE_PRINTING_PLATES_DATA . "`.`printingPlatesDataCustomerNumber`,
					`" . TABLE_PRINTING_PLATES_DATA . "`.`printingPlatesDataPlateNumber`,
					`" . TABLE_PRINTING_PLATES_DATA . "`.`printingPlatesDataPrintText`,
					`" . TABLE_PRINTING_PLATES_DATA . "`.`printingPlatesDataLayoutPdfFileName`,
					`" . TABLE_PRINTING_PLATES_DATA . "`.`printingPlatesDataLayoutThumbnailFileName`,
					`" . TABLE_PRINTING_PLATES_DATA . "`.`printingPlatesDataLayoutDataFileName`,
					`" . TABLE_PRINTING_PLATES_DATA . "`.`printingPlatesDataImage`,
					`" . TABLE_PRINTING_PLATES_DATA . "`.`printingPlatesDataUploadDate`,
					`" . TABLE_PRINTING_PLATES_DATA . "`.`printingPlatesDataActive`,
					`" . TABLE_PRINTING_PLATES_DATA . "`.`printingPlatesDataModifiedDate`,

					`" . TABLE_CUSTOMERS . "`.`customersID`,
					`" . TABLE_CUSTOMERS . "`.`customersKundennummer`,
					`" . TABLE_CUSTOMERS . "`.`customersFirmenname`,
					`" . TABLE_CUSTOMERS . "`.`customersFirmennameZusatz`,
					`" . TABLE_CUSTOMERS . "`.`customersCompanyStrasse`,
					`" . TABLE_CUSTOMERS . "`.`customersCompanyHausnummer`,
					`" . TABLE_CUSTOMERS . "`.`customersCompanyCountry`,
					`" . TABLE_CUSTOMERS . "`.`customersCompanyPLZ`,
					`" . TABLE_CUSTOMERS . "`.`customersCompanyOrt`

				FROM `" . TABLE_PRINTING_PLATES_DATA . "`

				LEFT JOIN `" . TABLE_CUSTOMERS . "`
				ON(`" . TABLE_PRINTING_PLATES_DATA . "`.`printingPlatesDataCustomerNumber` = `" . TABLE_CUSTOMERS . "`.`customersKundennummer`)
			";

			$rs_local = $dbConnection->db_query($sql_local);
			if(mysqli_error($db_open)) { fwrite($fp_log, date("Y-m-d H:i:s") . ": " . mysqli_error($db_open) . "\n" . $sql_local . "\n"); }

			$sql_extern = "";
			while($ds_local = mysqli_fetch_assoc($rs_local)){
				$thisPrintingPlatesDataLayoutThumbnailFileName = basename($ds_local["printingPlatesDataLayoutThumbnailFileName"]);
				$thisPrintingPlatesDataLayoutThumbnailFileLocalPath = DIRECTORY_LAYOUT_PREVIEW_IMAGES_BASEPATH_LOCAL . $thisPrintingPlatesDataLayoutThumbnailFileName;
				$thisPrintingPlatesDataLayoutThumbnailFileExternPath = DIRECTORY_LAYOUT_PREVIEW_IMAGES_BASEPATH_EXTERNAL . $thisPrintingPlatesDataLayoutThumbnailFileName;

				$thisPrintingPlatesDataLayoutDataFileName = basename($ds_local["printingPlatesDataLayoutDataFileName"]);
				$thisPrintingPlatesDataLayoutDataFileLocalPath = DIRECTORY_PRINT_PLATE_FILES_BASEPATH_LOCAL. $thisPrintingPlatesDataLayoutDataFileName;
				$thisPrintingPlatesDataLayoutDataFileExternPath = DIRECTORY_PRINT_PLATE_FILES_BASEPATH_EXTERNAL . $thisPrintingPlatesDataLayoutDataFileName;

				foreach($ds_local as $ds_localKey => $ds_localvalue){
					$ds_local[$ds_localKey] = addslashes($ds_localvalue);
				}

				// BOF CREATE SQL FOR ONLINE PRINTING PLATES
					$sql_extern = "";

					$sql_extern = "REPLACE INTO `_tmp_" . TABLE_PRINTING_PLATES_DATA . "` (
										`" . implode("`, `", array_keys($ds_local)) . "`
										)
										VALUES (
											'" . implode("', '", array_values($ds_local)) . "'
										);
						";

					$rs_extern = $dbConnection_ExternProduction->db_query($sql_extern);
					if(mysqli_error($db_openExternProduction)) { fwrite($fp_log, date("Y-m-d H:i:s") . ": " . mysqli_error($db_openExternProduction) . "\n"); }
				// EOF CREATE SQL FOR ONLINE PRINTING PLATES

				// BOF UPLOAD PRINTING PLATES PREVIEW AND PRINTING PLATES DATA
					$ftp_connect = ftp_connect(FTP_SERVER) or die(date("Y-m-d H:i:s") . ": Konnte keine FTP-Verbindung zum externen ONLINE-Server herstellen" . "\n");
					$rs_ftp = false;
					if($ftp_connect){
						$ftp_login = ftp_login ($ftp_connect, FTP_LOGIN, FTP_PASSWORD);

						#$fileListExtern = ftp_nlist($ftp_connect, PRODUCTION_SHEET_DIR_BASEPATH);

						// BOF HANDLE PRINTING PLATES PREVIEW
							if($ds_local["printingPlatesDataLayoutThumbnailFileName"] != ''){

								$filesizeLocal = filesize($thisPrintingPlatesDataLayoutThumbnailFileLocalPath);
								$filesizeExtern = ftp_size($ftp_connect, $thisPrintingPlatesDataLayoutThumbnailFileExternPath);

								#fwrite($fp_log, "size extern: " . $filesizeExtern . "\n");
								#fwrite($fp_log, "size local: " . $filesizeLocal . "\n");

								if($filesizeLocal != $filesizeExtern){
									// BOF DELETE EXISTING PRINTING PLATES PREVIEW
										if(ftp_size($ftp_connect, $thisPrintingPlatesDataLayoutThumbnailFileExternPath) != -1){
											ftp_delete($ftp_connect, $thisPrintingPlatesDataLayoutThumbnailFileExternPath);
										}
										fwrite($fp_log, date("Y-m-d H:i:s") . ": FILE " . $thisPrintingPlatesDataLayoutThumbnailFileExternPath . " SIZE " . ftp_size($ftp_connect, $thisPrintingPlatesDataLayoutThumbnailFileExternPath) . "\n");
									// EOF DELETE EXISTING PRINTING PLATES PREVIEW

									// BOF UPLOAD PRINTING PLATES PREVIEW
										$rs_ftp = ftp_put($ftp_connect, $thisPrintingPlatesDataLayoutThumbnailFileExternPath, $thisPrintingPlatesDataLayoutThumbnailFileLocalPath, FTP_BINARY);
										if(!$rs_ftp) {
											fwrite($fp_log, date("Y-m-d H:i:s") . ": Kein Upload " . $thisPrintingPlatesDataLayoutThumbnailFileName . "\n");
										}
										else{

										}
									// BOF UPLOAD PRINTING PLATES PREVIEW
								}
							}
						// EOF HANDLE PRINTING PLATES PREVIEW

						// BOF HANDLE PRINTING PLATES DATA
							/*
							if($ds_local["printingPlatesDataLayoutDataFileName"] != ''){

								// BOF DELETE EXISTING PRINTING PLATES DATA
									if(ftp_size($ftp_connect, $thisPrintingPlatesDataLayoutDataFileExternPath) != -1){
										ftp_delete($ftp_connect, $thisPrintingPlatesDataLayoutDataFileExternPath);
									}
									fwrite($fp_log, date("Y-m-d H:i:s") . ": FILE " . $thisPrintingPlatesDataLayoutDataFileExternPath . " SIZE " . ftp_size($ftp_connect, $thisPrintingPlatesDataLayoutDataFileExternPath) . "\n");
								// EOF DELETE EXISTING PRINTING PLATES DATA

								// BOF UPLOAD PRINTING PLATES DATA
									$rs_ftp = ftp_put($ftp_connect, $thisPrintingPlatesDataLayoutDataFileExternPath, $thisPrintingPlatesDataLayoutDataFileLocalPath, FTP_BINARY);
									if(!$rs_ftp) {
										fwrite($fp_log, date("Y-m-d H:i:s") . ": Kein Upload " . $thisPrintingPlatesDataLayoutDataFileName . "\n");
									}
									else{

									}
								// BOF UPLOAD PRINTING PLATES DATA
							}
							*/
						// EOF HANDLE PRINTING PLATES DATA

						ftp_close($ftp_connect);
					}
				// EOF UPLOAD PRINTING PLATES PREVIEW


			}
		// EOF EXPORT PRINTING PLATES DATAS FROM Auftragslisten TO ONLINE PRODUCTION
		
		// BOF RENAME / DELETE EXTERNAL TMP-TABLES
			$sql_externalDelete = "DROP TABLE IF EXISTS `_old_" . TABLE_PRINTING_PLATES_DATA . "`";
			$rs_externalDelete = $dbConnection_ExternProduction->db_query($sql_externalDelete);
			
			$sql_externalRename = "RENAME TABLE `" . TABLE_PRINTING_PLATES_DATA . "` TO `_old_" . TABLE_PRINTING_PLATES_DATA . "`";
			$rs_externalRename = $dbConnection_ExternProduction->db_query($sql_externalRename);
			
			$sql_externalRename = "RENAME TABLE `_tmp_" . TABLE_PRINTING_PLATES_DATA . "` TO `" . TABLE_PRINTING_PLATES_DATA . "`";
			$rs_externalRename = $dbConnection_ExternProduction->db_query($sql_externalRename);
		// EOF RENAME / DELETE EXTERNAL TMP-TABLES
	}

	#if(mysqli_error($db_open)) { fwrite($fp_log, date("Y-m-d H:i:s") . ": " . mysqli_error($db_open) . "\n"); }
	fwrite($fp_log, date("Y-m-d H:i:s") . "ENDE" . "\n" . str_repeat("#", 10) . "\n\n\n");

	if($dbConnection_ExternAcqisition){
		$dbConnection_ExternAcqisition->db_close();
	}
	if($dbConnection){
		$dbConnection->db_close();
	}
	if($fp_log){
		fclose($fp_log);
	}
?>