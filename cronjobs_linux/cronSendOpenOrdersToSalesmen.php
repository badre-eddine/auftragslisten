<?php

	#error_reporting(0);
	error_reporting(E_ALL & ~E_NOTICE);
	set_time_limit(0);

	if(!defined('MANDATOR')) {
		DEFINE('MANDATOR', 'bctr');
	}

	DEFINE('BASEPATH_Auftragslisten', "/var/www/Auftragslisten/");
	#DEFINE('BASEPATH_CRONJOBS', $_SERVER['DOCUMENT_ROOT'] . "/");
	DEFINE('BASEPATH_CRONJOBS', BASEPATH_Auftragslisten . "cronjobs_linux/");

	require_once(BASEPATH_CRONJOBS . 'cronjobsConfig.inc.php');

	#DEFINE('PATH_EXPORTED_FILES', DIRECTORY_CREATED_DOCUMENTS_SALESMEN);
	DEFINE('PATH_EXPORTED_FILES', BASEPATH_CRONJOBS . 'tmp/');
	DEFINE('DIRECTORY_HTML2PDF', BASEPATH_Auftragslisten . 'scriptPackages/html2pdf/');

	require_once(BASEPATH_Auftragslisten . 'config/configParams_' . strtolower(MANDATOR) . '.inc.php');
	require_once(BASEPATH_Auftragslisten . 'config/configBasic.inc.php');
	require_once(BASEPATH_Auftragslisten . 'config/configTables.inc.php');
	require_once(BASEPATH_Auftragslisten . 'inc/functions.inc.php');
	require_once(BASEPATH_Auftragslisten . 'classes/DB_Connection.class.php');
	require_once(BASEPATH_Auftragslisten . 'scriptPackages/htmlMimeMail/htmlMimeMail.php');
	require_once(BASEPATH_Auftragslisten . 'classes/createMail.class.php');

	require_once(BASEPATH_Auftragslisten . 'documents_' . MANDATOR . '/documentTemplates/templatesMail/template_BASIC.inc.php');
	require_once(BASEPATH_Auftragslisten . 'documents_' . MANDATOR . '/documentTemplates/templatesMail/template_TEXT_DATAS.inc.php');
	require_once(DIRECTORY_HTML2PDF . 'html2pdf.class.php');
	if(!is_dir(PATH_EXPORTED_FILES)){
		mkdir(PATH_EXPORTED_FILES);
	}
	chmod(PATH_EXPORTED_FILES, 0777);

	$exportAllStatusOF	= 1;
	$exportStatusFG 	= 0;
	$todayDate = date("Y-m-d");
	$todayWeekDay = date("w");
	$todayWeek = date("W");
	$intervalDay = 10;
	$thisWeekStart = date("Y-m-d", mktime(0, 0, 0, date("m"), (date("d") - $intervalDay), date("Y")));
	$thisWeekEnd = date("Y-m-d");

	$exportOrdersFreigabeDatumFrom	= "16.07.2014";
	$exportOrdersFreigabeDatumTo	= "19.07.2014";

	DEFINE('FILENAME_LOGFILE', BASEPATH_CRONJOBS . "logs/" . MANDATOR . "_" . basename($_SERVER["PHP_SELF"]) . ".txt");

	if(file_exists(FILENAME_LOGFILE)){
		unlink(FILENAME_LOGFILE);
	}
	$fp_log = fopen(FILENAME_LOGFILE, "w");

	fwrite($fp_log, date("Y-m-d H:i:s") . ": MANDATOR : " . MANDATOR . " | getMandator : " . $getMandator . "\n");

	$dbConnection = new DB_Connection();
	$db_open = $dbConnection->db_connect();

	$thisMessage = '';

	// BOF READ VERTRETER
		$arrSalesmenDatas = getSalesmen();
		$arrTemp = array();
		if(!empty($arrSalesmenDatas)) {
			foreach($arrSalesmenDatas as $thisKey => $thisValue) {
				if($arrSalesmenDatas[$thisKey]["salesmenExportDatas"] == '1'){
					$arrTemp[$thisKey] = $thisValue;
				}
			}
			$arrSalesmenDatas = $arrTemp;
		}
	// EOF READ VERTRETER


	// BOF READ BESTELL ARTEN
		$arrOrderTypeDatas = getOrderTypes();
	// EOF READ BESTELL ARTEN

	// BOF READ BESTELL STATUS TYPEN
		$arrOrderStatusTypeDatas = getOrderStatusTypes();
	// EOF READ BESTELL STATUS TYPEN

	// BOF GET OPEN ORDERS DEPENDING ON SALESMEN AND SEND PDF OR EXCEL
		if(!empty($arrSalesmenDatas)){
			foreach($arrSalesmenDatas as $thisSalesmanKey => $thisSalesmanValue){
				$thisMessage = '';
				$exportOrdersVertreterID = $thisSalesmanKey;
				$exportOrdersVertreterName = $arrSalesmenDatas[$exportOrdersVertreterID]["customersFirmenname"];
				$exportOrdersVertreterMail = $arrSalesmenDatas[$exportOrdersVertreterID]["customersMail1"];
				$sqlWhere = "";
				// BOF GET OPEN ORDERS
					$sql = "SELECT
								/*
								`ordersID`,
								*/

								`ordersBestellDatum`,

								/*
								`ordersFreigabeDatum`,
								`ordersBelichtungsDatum`,
								*/

								`ordersKundennummer`,
								`ordersKundenName`,
								`ordersKommission`,
								`ordersPLZ`,
								`ordersOrt`,
								`ordersArtikelMenge`,

								/*
								`ordersSinglePreis`,
								`ordersTotalPreis`,
								*/

								`ordersArtikelBezeichnung`,

								/*
								`ordersArtikelID`,
								`ordersArtikelNummer`,
								`ordersAdditionalArtikelKategorieID`,
								*/

								`ordersAdditionalArtikelMenge`,
								`ordersDruckFarbe`,

								/*
								`ordersAdditionalCosts`,
								`ordersVertreter`,
								`ordersVertreterID`,
								*/

								`ordersMailAdress`,
								/*

								`ordersMandant`,
								`ordersLieferwoche`,
								`ordersLieferDatum`,
								`ordersDruckerName`,
								*/

								`ordersNotizen`,
								`ordersStatus`,
								`ordersOrderType`

								/*
								,
								`ordersBankAccountType`,
								`ordersPaymentType`,
								`ordersPaymentStatusType`,
								`ordersUserID`,
								`ordersTimeCreated`
								*/

								FROM `" . TABLE_ORDERS . "`

								WHERE
						";
						if($exportAllStatusOF == 1) {
							$sqlWhere .= " (`ordersStatus` = 1) ";
						}
						if($exportAllStatusOF == 1 && $exportStatusFG == 1) {
							$sqlWhere .= " OR ";
						}
						if($exportStatusFG == 1) {
							$sqlWhere .= " (
								`ordersStatus` = 2
								AND `ordersOrderType` = 1
								AND `ordersBestellDatum`
									BETWEEN '" . formatDate($exportOrdersFreigabeDatumFrom, "store") . "' AND '" . formatDate($exportOrdersFreigabeDatumTo, "store")."'
							) ";
						}

						if($sqlWhere != "") {
							$sqlWhere = " ( " . $sqlWhere . " ) ";
						}

						if($exportOrdersVertreterID == "NO_SALESMAN") {
							$sqlWhere .= " AND (
											`ordersVertreterID` = ''
											OR
											`ordersVertreterID` = 0
										 )
							";
						}
						else if($exportOrdersVertreterID > 0) {
							$sqlWhere .= " AND (
											`ordersVertreterID` = '" . $exportOrdersVertreterID ."'
											/*
											OR `ordersVertreter` LIKE '" . $arrSalesmenDatas[$exportOrdersVertreterID]["customersFirmenname"] . "'
											*/
										 )
							";
						}
						$sql .= $sqlWhere;


						$sql .= " ORDER BY `ordersBestellDatum` ASC ";
#fwrite($fp_log, $sql);
#exit;
						$rs = $dbConnection->db_query($sql);

						if(mysqli_error($db_open)) { fwrite($fp_log, date("Y-m-d H:i:s") . ": " . mysqli_error($db_open) . "\n\n\n"); }

						$countTotalRows = $dbConnection->db_getMysqlNumRows($rs);
						$contentExportPDF = '';
						$contentExportCSV = '';
					// EOF GET OPEN ORDERS

					###################################################
					// BOF CREATE PDF WITH DATAS AND SEND ATTACHED MAIL
						if($countTotalRows > 0) {
							// BOF CREATE PDF WITH DATAS
								$contentExportPDF = '
									<style type="text/css">
									<!--
										table {
											border: 1pt solid #CCCCCC;
											border-collapse:collapse;
										}
										td {
											padding: 1pt;
											border: 0.5pt solid #CCCCCC;
											text-align:left;
											font-size:8pt;
											font-weight:normal;
											color:#000;
											vertical-align:top;
										}
										th {
											text-align:left;
											font-size:7pt;
											font-weight:bold;
											background-color:#DDDDDD;
											color:#000;
											padding: 1pt;
											border: 0.5pt solid #CCCCCC;
										}
										.rowColor {
											background-color:#DDD;
										}
									-->
									</style>
								';


								$contentExportPDF .= '<table width="100%">';

								$countRow = 0;
								while($ds = mysqli_fetch_assoc($rs)) {
									$thisVertreter = $ds["ordersVertreter"];
									if($countRow == 0) {
										$contentExportPDF .= '<tr>';
										$contentExportPDF .= '<th>BESTELLUNG</th>';
										$contentExportPDF .= '<th>K-NR</th>';
										$contentExportPDF .= '<th>KUNDE</th>';
										$contentExportPDF .= '<th>PLZ</th>';
										$contentExportPDF .= '<th>ORT</th>';
										$contentExportPDF .= '<th>MENGE</th>';
										$contentExportPDF .= '<th>ARTIKEL</th>';
										$contentExportPDF .= '<th>DRUCKFARBE</th>';
										// $contentExportPDF .= '<th>VERTRETER</th>';
										$contentExportPDF .= '<th>MAIL</th>';
										$contentExportPDF .= '<th>NOTIZ</th>';
										$contentExportPDF .= '<th>STATUS</th>';
										$contentExportPDF .= '<th>BESTELLART</th>';
										foreach(array_keys($ds) as $field) {
											// $contentExportPDF .= '<th>' . strtoupper(preg_replace('/orders/', '', $field)) . '</th>';
										}
										$contentExportPDF .= '</tr>';
									}

									$thisBackgroundColor = '#FFF';
									if($countRow%2 == 0) {
										$thisBackgroundColor = '#EEE';
									}
									/*
									if($ds["ordersStatus"] == 1) {
										$thisBackgroundColor = '#FEFF7F';
									}
									else if($ds["ordersStatus"] == 2) {
										$thisBackgroundColor = '#AEFC9F';
									}
									*/

									$contentExportPDF .= '<tr style="background-color:'.$thisBackgroundColor.';">';

									#$contentExportPDF .= '<td>';
									#$contentExportPDF .= formatDate($ds["ordersBelichtungsDatum"], 'display');
									#$contentExportPDF .= '</td>';

									$contentExportPDF .= '<td>';
									$contentExportPDF .= formatDate($ds["ordersBestellDatum"], 'display');
									$contentExportPDF .= '</td>';

									$contentExportPDF .= '<td>';
									$contentExportPDF .= ($ds["ordersKundennummer"]);
									$contentExportPDF .= '</td>';

									$contentExportPDF .= '<td>';
									$contentExportPDF .= wordwrap($ds["ordersKundenName"], 20, "<br>", false);
									if($ds["ordersKommission"] != ''){
										$contentExportPDF .= '<br>' . wordwrap("<b>Kommission</b> " . $ds["ordersKommission"], 20, "<br>", false);
									}
									$contentExportPDF .= '</td>';

									$contentExportPDF .= '<td>';
									$contentExportPDF .= nl2br($ds["ordersPLZ"]);
									$contentExportPDF .= '</td>';

									$contentExportPDF .= '<td>';
									$contentExportPDF .= nl2br($ds["ordersOrt"]);
									$contentExportPDF .= '</td>';

									$contentExportPDF .= '<td>';
									$contentExportPDF .= nl2br($ds["ordersArtikelMenge"]);
									$contentExportPDF .= '</td>';

									$contentExportPDF .= '<td>';
									$ds[$ds["ordersArtikelBezeichnung"]] = preg_replace("/\//", "/ ", $ds["ordersArtikelBezeichnung"]);
									$contentExportPDF .= wordwrap($ds["ordersArtikelBezeichnung"], 20, "<br>", false);
									if($ds["ordersAdditionalArtikelMenge"] > 0) {
										$contentExportPDF .= '<br> (+ '. $ds["ordersAdditionalArtikelMenge"] . ' Leisten)';
									}
									$contentExportPDF .= '</td>';

									$contentExportPDF .= '<td>';
									$contentExportPDF .= preg_replace('/\[(.*)\]/ismU', '', $ds["ordersDruckFarbe"]);
									$contentExportPDF .= '</td>';

									// $contentExportPDF .= '<th>VERTRETER</th>';

									$contentExportPDF .= '<td>';
									if(strlen($ds["ordersMailAdress"]) > 30) {
										if(preg_match("/@/",$ds["ordersMailAdress"])) {
											$contentExportPDF .= str_replace("@", "@<br>", $ds["ordersMailAdress"]);
										}
										else {
											$contentExportPDF .= wordwrap($ds["ordersMailAdress"], 30, "<br>", true);
										}
									}
									else {
										$contentExportPDF .= nl2br($ds["ordersMailAdress"]);
									}
									$contentExportPDF .= '</td>';

									$contentExportPDF .= '<td>';
									$contentExportPDF .= wordwrap($ds["ordersNotizen"], 20, " <br>", false);
									$contentExportPDF .= '</td>';

									$contentExportPDF .= '<td>';
									$contentExportPDF .= $arrOrderStatusTypeDatas[$ds["ordersStatus"]]["orderStatusTypesName"];
									$contentExportPDF .= '</td>';

									$contentExportPDF .= '<td>';
									$contentExportPDF .= $arrOrderTypeDatas[$ds["ordersOrderType"]]["orderTypesName"];
									$contentExportPDF .= '</td>';

									$contentExportPDF .= '</tr>';
									$countRow++;
								}
								$contentExportPDF .= '</table>';

								$pdfContentPage = $contentExportPDF;

								//echo $pdfContentPage;
								//exit;

								$pdfContentPage = removeUnnecessaryChars($pdfContentPage);
								$contentExportCSV = formatCSV($contentExportPDF);

								// BOF STORE CSV
									$thisVertreter = $arrSalesmenDatas[$exportOrdersVertreterID]["customersFirmenname"];
									$thisCreatedCsvName = convertChars($thisVertreter).'.csv';


									if(file_exists(PATH_EXPORTED_FILES . $thisCreatedCsvName)) {
										// chmod(PATH_EXPORTED_FILES . $thisCreatedCsvName , "0777");
										unlink(PATH_EXPORTED_FILES . $thisCreatedCsvName);
									}
									clearstatcache();
									$fp = fopen(PATH_EXPORTED_FILES . $thisCreatedCsvName, 'w');
									fwrite($fp, stripslashes($contentExportCSV));
									fclose($fp);
								// EOF STORE CSV

								// BOF WRITE TEMP HTML FILE
									$thisCreatedPdfName = 'Liste_' . convertChars($thisVertreter).'.pdf';

									$tempHtmlFileName = "_temp_".$thisCreatedPdfName.'.html';
									if(file_exists(PATH_EXPORTED_FILES . $tempHtmlFileName)) {
										unlink(PATH_EXPORTED_FILES . $tempHtmlFileName);
									}
									clearstatcache();
									$fp = fopen(PATH_EXPORTED_FILES . $tempHtmlFileName, 'w');
									fwrite($fp, stripslashes($pdfContentPage));
									fclose($fp);
								// EOF WRITE TEMP HTML FILE

								// BOF createPDF


								/*
								if(file_exists(PATH_EXPORTED_FILES . $thisCreatedPdfName)) {
									try {
										chmod(PATH_EXPORTED_FILES . $thisCreatedPdfName , "0777");
										unlink(PATH_EXPORTED_FILES . $thisCreatedPdfName);
									}
									catch (Exception $e) {}
								}
								clearstatcache();
								*/

								try {
									// $html2pdf = new HTML2PDF('P', 'A4', 'de', true, 'UTF-8', array(0, 0, 0, 0));
									// $html2pdf = new HTML2PDF('L', 'A4', 'de', true, 'UTF-8', array(22, 5, 5, 5));
									$html2pdf = new HTML2PDF('L', 'A4', 'de', true, 'UTF-8', array(10, 10, 10, 10));
									// $html2pdf = new HTML2PDF('P', 'A4', 'de');
									// $html2pdf = new HTML2PDF('P', 'A4', 'de', true, 'UTF-8');
									// $html2pdf->setModeDebug();
									// $html2pdf->setDefaultFont('Arial');
									// $html2pdf->createIndex('Inhaltsverzeichnis', 12, 8, false, true, 2);

									if(file_exists(PATH_EXPORTED_FILES . $thisCreatedPdfName)) {
										// chmod(PATH_EXPORTED_FILES . $thisCreatedPdfName , "0777");
										unlink(PATH_EXPORTED_FILES . $thisCreatedPdfName);
									}
									clearstatcache();

									// BOF READ TEMP HTML FILE
									if(file_exists(PATH_EXPORTED_FILES . $tempHtmlFileName)) {
										$pdfContentPage = "";
										$fp = fopen(PATH_EXPORTED_FILES . $tempHtmlFileName, 'r');
										$pdfContentPage = fread($fp, filesize(PATH_EXPORTED_FILES . $tempHtmlFileName));
										fclose($fp);
										$pdfContentPage = stripslashes($pdfContentPage);
										unlink(PATH_EXPORTED_FILES . $tempHtmlFileName);
									}
									clearstatcache();
									// EOF READ TEMP HTML FILE

									ob_start();
									echo $pdfContentPage;
									$contentPDF = ob_get_clean();

									$html2pdf->writeHTML($contentPDF, isset($_GET['vuehtml']));
									$html2pdf->Output(PATH_EXPORTED_FILES . $thisCreatedPdfName, 'F', '');

									if(file_exists($thisCreatedPdfName)) {
										unlink($thisCreatedPdfName);
									}
									clearstatcache();
								}
								catch(HTML2PDF_exception $e) {
									echo $e;
									fwrite($fp_log, date("Y-m-d H:i:s") . ': ' . $e . "-----\n");
									exit;
								}

								clearstatcache();
							// EOF CREATE PDF WITH DATAS

							// BOF SEND MAIL WITH ATTACHED DOCUMENT
								$sendAttachedDocument = '1';
								if($sendAttachedDocument == '1') {
									$selectSubject = 'Offene Bestellungen - Datenexport: ' . $thisCreatedPdfName;
									$generatedDocumentNumber = $thisCreatedPdfName;

									// BOF SEND MAIL

										// BOF CREATE SUBJECT
											$thisSubject = $selectSubject;
										// EOF CREATE SUBJECT

										// BOF GET ATTACHED FILE
											#$thisCreatedPdfName = utf8_decode($thisCreatedPdfName);
											$pathDocumentFolder = PATH_EXPORTED_FILES;

											$arrAttachmentFiles = array (
												$pathDocumentFolder . $thisCreatedPdfName => str_replace(" ", "_", ($thisCreatedPdfName))
											);
										// EOF GET ATTACHED FILE

										$documentType = 'SX';
										$selectCustomersRecipientName = $exportOrdersVertreterName;
										$selectMailtextTemplates = 'SX';
										
										#$selectCustomersRecipientMail = 'webmaster@burhan-ctr.de';
										$selectCustomersRecipientMail = $exportOrdersVertreterMail . ';' . 'webmaster@burhan-ctr.de';										
										
										$sendAttachedMailText = 'Sehr geehrte Damen und Herren,' . "\n\n" . 'anbei die Liste mit offenen Bestellungen im PDF-Format.';
										$sendAttachedMailText .= "\n\n" . 'Aufträge, die nicht innerhalb der folgenden 14 Tage freigeben werden, berechnen wir eine Stornogebühr in Höhe von ' . CANCELLATION_FEE . '.';
										$selectMailtextSender = 'webmaster@burhan-ctr.de';
										$createMail = new createMail(
															$thisSubject,													// TEXT:	MAIL_SUBJECT
															$documentType,													// STRING:	DOCUMENT TYPE
															$generatedDocumentNumber,										// STRING:	DOCUMENT NUMBER
															$arrAttachmentFiles,											// ARRAY:	ATTACHMENT FILES
															$selectCustomersRecipientMail,	 								// STRING:	RECIPIENTS
															$arrMailContentDatas[$selectMailtextTemplates],					// MAIL_TEXT_TEMPLATE
															$mailTemplate["html"],											// MAIL_BODY_TEMPLATE
															'',																// STRING:	ADDITIONAL TEXT
															$sendAttachedMailText,											// STRING:	SEND ATTACHED TEXT
															true,															// BOOLEAN:	SEND ATTACHED DOCUMENT
															DIRECTORY_MAIL_IMAGES,											// DIRECTORY_MAIL_IMAGES
															$selectMailtextSender											// STRING: SENDER
														);

										$createMailResult = $createMail->returnResult();
										$sendMailToClient = $createMailResult;
									// EOF SEND MAIL

									if(!$sendMailToClient) {
										$thisMessage .= ' Die Mail konnte nicht an ' . $selectCustomersRecipientName . ' (' . $selectCustomersRecipientMail . ') versendet werden! ' . "\n";
									}
									else {
										$thisMessage .= ' Die Mail wurde an ' . $selectCustomersRecipientName . ' (' . $selectCustomersRecipientMail . ') versendet! ' . "\n";

										// BOF DELETE CREATED FILES
											if(file_exists(PATH_EXPORTED_FILES . $thisCreatedPdfName)) {
												unlink(PATH_EXPORTED_FILES . $thisCreatedPdfName);
											}
											if(file_exists(PATH_EXPORTED_FILES . $thisCreatedCsvName)) {
												unlink(PATH_EXPORTED_FILES . $thisCreatedCsvName);
											}
										// EOF DELETE CREATED FILES
									}

									fwrite($fp_log, date("Y-m-d H:i:s") . ': ' . $thisMessage . "-----\n");
									$thisMessage = '';
								}
							// EOF SEND MAIL WITH ATTACHED DOCUMENT
						}
					// EOF CREATE PDF WITH DATAS AND SEND ATTACHED MAIL
					###################################################
			}
		}
	// EOF GET OPEN ORDERS DEPENDING ON SALESMEN AND SEND PDF OR EXCEL

	fwrite($fp_log, "ENDE: " . date("Y-m-d H:i:s") . "\n\n" . str_repeat("#", 100) . "\n");

	if($dbConnection){
		$dbConnection->db_close();
	}
	if($fp_log){
		fclose($fp_log);
	}
?>