<?php

	#error_reporting(0);
	ini_set('display_errors',1);
	error_reporting(E_ALL & ~E_NOTICE);

	DEFINE('BASEPATH_Auftragslisten', "/var/www/Auftragslisten/");
	#DEFINE('BASEPATH_CRONJOBS', $_SERVER['DOCUMENT_ROOT'] . "/");
	DEFINE('BASEPATH_CRONJOBS', BASEPATH_Auftragslisten . "cronjobs_linux/");

	require_once(BASEPATH_CRONJOBS . 'cronjobsConfig.inc.php');

	require_once(BASEPATH_Auftragslisten . 'config/configParams_' . strtolower(MANDATOR) . '.inc.php');
	require_once(BASEPATH_Auftragslisten . 'config/configBasic.inc.php');
	require_once(BASEPATH_Auftragslisten . 'config/configTables.inc.php');

	require_once(BASEPATH_Auftragslisten . 'inc/functions.inc.php');
	require_once(BASEPATH_Auftragslisten . 'classes/DB_Connection.class.php');

	DEFINE('FILENAME_LOGFILE', BASEPATH_CRONJOBS . "logs/" . MANDATOR . "_" . basename($_SERVER["PHP_SELF"]) . ".txt");

	if(file_exists(FILENAME_LOGFILE)){
		#unlink(FILENAME_LOGFILE);
	}
	$fp_log = fopen(FILENAME_LOGFILE, "w");

	fwrite($fp_log, date("Y-m-d H:i:s") . ": MANDATOR : " . MANDATOR . " | getMandator : " . $getMandator . "\n");

	$dbConnection = new DB_Connection();
	$db_open = $dbConnection->db_connect();

	if(DB_HOST_EXTERN_ACQUISITION != '' && DB_NAME_EXTERN_ACQUISITION != '' && DB_USER_EXTERN_ACQUISITION != '' && DB_PASSWORD_EXTERN_ACQUISITION) {
		$existsOnlineAquisition = true;
	}
	else {
		$existsOnlineAquisition = false;
	}
	fwrite($fp_log, date("Y-m-d H:i:s") . "\n");
	fwrite($fp_log, 'existsOnlineAquisition: ' . $existsOnlineAquisition . "\n");

	$dbConnection_ExternAcqisition = new DB_Connection(DB_HOST_EXTERN_ACQUISITION, '', DB_NAME_EXTERN_ACQUISITION, DB_USER_EXTERN_ACQUISITION, DB_PASSWORD_EXTERN_ACQUISITION);
	$db_openExternAcqisition = $dbConnection_ExternAcqisition->db_connect();

	// BOF BACK UP
		$thisTempTableName = "_SICH_" . TABLE_ACQUISATION_CUSTOMERS . "_" . date("Y-m-d_H-i-s");
		$sql_extern = "CREATE TABLE `" . $thisTempTableName . "`  LIKE `" . TABLE_ACQUISATION_CUSTOMERS . "` ";
		$rs_extern = $dbConnection_ExternAcqisition->db_query($sql_extern);

		$sql_extern = "INSERT INTO `" . $thisTempTableName . "` SELECT * FROM `" . TABLE_ACQUISATION_CUSTOMERS . "`";
		$rs_extern = $dbConnection_ExternAcqisition->db_query($sql_extern);
	// BOF BACK UP

	// BOF UPDATE CUSTUMERNUMBERS OKE-CUSTOMERS WITH Auftragslisten-CUSTUMERS
		/*
		$sql_local = "DROP TABLE IF EXISTS `_temp_" . MANDATOR . "_acquisition_customers`";
		$rs_local = $dbConnection->db_query($sql_local);

		// BOF GET DUMP EXTERNAL TABLE CUSTOMERS
			$sql_extern = "
					SHOW CREATE TABLE `" . TABLE_ACQUISATION_CUSTOMERS . "`;
				";
			$rs_extern = $dbConnection_ExternAcqisition->db_query($sql_extern);

			$getSqlDump_extern = "";
			while($ds_extern = mysqli_fetch_array($rs_extern)){
				if($ds_extern["Create Table"]	!= "" && $getSqlDump_extern == "")	{
					$getSqlDump_extern = $ds_extern["Create Table"];
				}
			}
		// EOF GET DUMP EXTERNAL TABLE CUSTOMERS

		$sql_local = preg_replace("/`" . TABLE_ACQUISATION_CUSTOMERS . "`/", "`_temp_" . MANDATOR . "_acquisition_customers`", $getSqlDump_extern);
		$rs_local = $dbConnection->db_query($sql_local);

		if(mysqli_error($db_open)) { fwrite($fp_log, date("Y-m-d H:i:s") . ": " . mysqli_error($db_open) . "\n"); }

		$sql_extern = "SELECT
					`" . TABLE_ACQUISATION_CUSTOMERS . "`.`customersID`,
					`" . TABLE_ACQUISATION_CUSTOMERS . "`.`customersKundennummer`,
					`" . TABLE_ACQUISATION_CUSTOMERS . "`.`customersFirmenname`,
					`" . TABLE_ACQUISATION_CUSTOMERS . "`.`customersFirmennameZusatz`,
					`" . TABLE_ACQUISATION_CUSTOMERS . "`.`customersFirmenInhaberVorname`,
					`" . TABLE_ACQUISATION_CUSTOMERS . "`.`customersFirmenInhaberNachname`,
					`" . TABLE_ACQUISATION_CUSTOMERS . "`.`customersFirmenInhaberAnrede`,
					`" . TABLE_ACQUISATION_CUSTOMERS . "`.`customersFirmenInhaber2Vorname`,
					`" . TABLE_ACQUISATION_CUSTOMERS . "`.`customersFirmenInhaber2Nachname`,
					`" . TABLE_ACQUISATION_CUSTOMERS . "`.`customersFirmenInhaber2Anrede`,
					`" . TABLE_ACQUISATION_CUSTOMERS . "`.`customersAnsprechpartner1Vorname`,
					`" . TABLE_ACQUISATION_CUSTOMERS . "`.`customersAnsprechpartner1Nachname`,
					`" . TABLE_ACQUISATION_CUSTOMERS . "`.`customersAnsprechpartner1Anrede`,
					`" . TABLE_ACQUISATION_CUSTOMERS . "`.`customersAnsprechpartner2Vorname`,
					`" . TABLE_ACQUISATION_CUSTOMERS . "`.`customersAnsprechpartner2Nachname`,
					`" . TABLE_ACQUISATION_CUSTOMERS . "`.`customersAnsprechpartner2Anrede`,
					`" . TABLE_ACQUISATION_CUSTOMERS . "`.`customersTelefon1`,
					`" . TABLE_ACQUISATION_CUSTOMERS . "`.`customersTelefon2`,
					`" . TABLE_ACQUISATION_CUSTOMERS . "`.`customersMobil1`,
					`" . TABLE_ACQUISATION_CUSTOMERS . "`.`customersMobil2`,
					`" . TABLE_ACQUISATION_CUSTOMERS . "`.`customersFax1`,
					`" . TABLE_ACQUISATION_CUSTOMERS . "`.`customersFax2`,
					`" . TABLE_ACQUISATION_CUSTOMERS . "`.`customersMail1`,
					`" . TABLE_ACQUISATION_CUSTOMERS . "`.`customersMail2`,
					`" . TABLE_ACQUISATION_CUSTOMERS . "`.`customersHomepage`,
					`" . TABLE_ACQUISATION_CUSTOMERS . "`.`customersCompanyStrasse`,
					`" . TABLE_ACQUISATION_CUSTOMERS . "`.`customersCompanyHausnummer`,
					`" . TABLE_ACQUISATION_CUSTOMERS . "`.`customersCompanyCountry`,
					`" . TABLE_ACQUISATION_CUSTOMERS . "`.`customersCompanyPLZ`,
					`" . TABLE_ACQUISATION_CUSTOMERS . "`.`customersCompanyOrt`,
					`" . TABLE_ACQUISATION_CUSTOMERS . "`.`customersLieferadresseFirmenname`,
					`" . TABLE_ACQUISATION_CUSTOMERS . "`.`customersLieferadresseFirmennameZusatz`,
					`" . TABLE_ACQUISATION_CUSTOMERS . "`.`customersLieferadresseStrasse`,
					`" . TABLE_ACQUISATION_CUSTOMERS . "`.`customersLieferadresseHausnummer`,
					`" . TABLE_ACQUISATION_CUSTOMERS . "`.`customersLieferadressePLZ`,
					`" . TABLE_ACQUISATION_CUSTOMERS . "`.`customersLieferadresseOrt`,
					`" . TABLE_ACQUISATION_CUSTOMERS . "`.`customersLieferadresseLand`,
					`" . TABLE_ACQUISATION_CUSTOMERS . "`.`customersRechnungsadresseFirmenname`,
					`" . TABLE_ACQUISATION_CUSTOMERS . "`.`customersRechnungsadresseFirmennameZusatz`,
					`" . TABLE_ACQUISATION_CUSTOMERS . "`.`customersRechnungsadresseStrasse`,
					`" . TABLE_ACQUISATION_CUSTOMERS . "`.`customersRechnungsadresseHausnummer`,
					`" . TABLE_ACQUISATION_CUSTOMERS . "`.`customersRechnungsadressePLZ`,
					`" . TABLE_ACQUISATION_CUSTOMERS . "`.`customersRechnungsadresseOrt`,
					`" . TABLE_ACQUISATION_CUSTOMERS . "`.`customersRechnungsadresseLand`,
					`" . TABLE_ACQUISATION_CUSTOMERS . "`.`customersBankName`,
					`" . TABLE_ACQUISATION_CUSTOMERS . "`.`customersKontoinhaber`,
					`" . TABLE_ACQUISATION_CUSTOMERS . "`.`customersBankKontonummer`,
					`" . TABLE_ACQUISATION_CUSTOMERS . "`.`customersBankLeitzahl`,
					`" . TABLE_ACQUISATION_CUSTOMERS . "`.`customersBankIBAN`,
					`" . TABLE_ACQUISATION_CUSTOMERS . "`.`customersBankBIC`,
					`" . TABLE_ACQUISATION_CUSTOMERS . "`.`customersBezahlart`,
					`" . TABLE_ACQUISATION_CUSTOMERS . "`.`customersZahlungskondition`,
					`" . TABLE_ACQUISATION_CUSTOMERS . "`.`customersRabatt`,
					`" . TABLE_ACQUISATION_CUSTOMERS . "`.`customersUseProductMwst`,
					`" . TABLE_ACQUISATION_CUSTOMERS . "`.`customersUseProductDiscount`,
					`" . TABLE_ACQUISATION_CUSTOMERS . "`.`customersUstID`,
					`" . TABLE_ACQUISATION_CUSTOMERS . "`.`customersVertreterID`,
					`" . TABLE_ACQUISATION_CUSTOMERS . "`.`customersVertreterName`,
					`" . TABLE_ACQUISATION_CUSTOMERS . "`.`customersUseSalesmanDeliveryAdress`,
					`" . TABLE_ACQUISATION_CUSTOMERS . "`.`customersUseSalesmanInvoiceAdress`,
					`" . TABLE_ACQUISATION_CUSTOMERS . "`.`customersTyp`,
					`" . TABLE_ACQUISATION_CUSTOMERS . "`.`customersGruppe`,
					`" . TABLE_ACQUISATION_CUSTOMERS . "`.`customersNotiz`,
					`" . TABLE_ACQUISATION_CUSTOMERS . "`.`customersUserID`,
					`" . TABLE_ACQUISATION_CUSTOMERS . "`.`customersTimeCreated`,
					`" . TABLE_ACQUISATION_CUSTOMERS . "`.`customersActive`,
					`" . TABLE_ACQUISATION_CUSTOMERS . "`.`customersDatasUpdated`,
					`" . TABLE_ACQUISATION_CUSTOMERS . "`.`customersTaxAccountID`,
					`" . TABLE_ACQUISATION_CUSTOMERS . "`.`customersInvoicesNotPurchased`,
					`" . TABLE_ACQUISATION_CUSTOMERS . "`.`customersBadPaymentBehavior`,
					`" . TABLE_ACQUISATION_CUSTOMERS . "`.`customersEntryDate`

				FROM `" . TABLE_ACQUISATION_CUSTOMERS . "`

				WHERE 1
					AND (
						`" . TABLE_ACQUISATION_CUSTOMERS . "`.`customersKundennummer` IS NULL
						OR
						`" . TABLE_ACQUISATION_CUSTOMERS . "`.`customersKundennummer` = ''
						OR
						`" . TABLE_ACQUISATION_CUSTOMERS . "`.`customersKundennummer` LIKE '%xx'
					)
			";

		$rs_extern = $dbConnection_ExternAcqisition->db_query($sql_extern);
		$countDatas = $dbConnection_ExternAcqisition->db_getMysqlNumRows($rs_extern);

		if(mysqli_error($db_openExternAcqisition)) { fwrite($fp_log, date("Y-m-d H:i:s") . ": " . mysqli_error($db_openExternAcqisition) . "\n"); }

		if($countDatas > 0) {
			$sqlTemp_local_fields = "INSERT INTO `_temp_" . MANDATOR . "_acquisition_customers` (
							`customersID`,
							`customersKundennummer`,
							`customersFirmenname`,
							`customersFirmennameZusatz`,
							`customersFirmenInhaberVorname`,
							`customersFirmenInhaberNachname`,
							`customersFirmenInhaberAnrede`,
							`customersFirmenInhaber2Vorname`,
							`customersFirmenInhaber2Nachname`,
							`customersFirmenInhaber2Anrede`,
							`customersAnsprechpartner1Vorname`,
							`customersAnsprechpartner1Nachname`,
							`customersAnsprechpartner1Anrede`,
							`customersAnsprechpartner2Vorname`,
							`customersAnsprechpartner2Nachname`,
							`customersAnsprechpartner2Anrede`,
							`customersTelefon1`,
							`customersTelefon2`,
							`customersMobil1`,
							`customersMobil2`,
							`customersFax1`,
							`customersFax2`,
							`customersMail1`,
							`customersMail2`,
							`customersHomepage`,
							`customersCompanyStrasse`,
							`customersCompanyHausnummer`,
							`customersCompanyCountry`,
							`customersCompanyPLZ`,
							`customersCompanyOrt`,
							`customersLieferadresseFirmenname`,
							`customersLieferadresseFirmennameZusatz`,
							`customersLieferadresseStrasse`,
							`customersLieferadresseHausnummer`,
							`customersLieferadressePLZ`,
							`customersLieferadresseOrt`,
							`customersLieferadresseLand`,
							`customersRechnungsadresseFirmenname`,
							`customersRechnungsadresseFirmennameZusatz`,
							`customersRechnungsadresseStrasse`,
							`customersRechnungsadresseHausnummer`,
							`customersRechnungsadressePLZ`,
							`customersRechnungsadresseOrt`,
							`customersRechnungsadresseLand`,
							`customersBankName`,
							`customersKontoinhaber`,
							`customersBankKontonummer`,
							`customersBankLeitzahl`,
							`customersBankIBAN`,
							`customersBankBIC`,
							`customersBezahlart`,
							`customersZahlungskondition`,
							`customersRabatt`,
							`customersUseProductMwst`,
							`customersUseProductDiscount`,
							`customersUstID`,
							`customersVertreterID`,
							`customersVertreterName`,
							`customersUseSalesmanDeliveryAdress`,
							`customersUseSalesmanInvoiceAdress`,
							`customersTyp`,
							`customersGruppe`,
							`customersNotiz`,
							`customersUserID`,
							`customersTimeCreated`,
							`customersActive`,
							`customersDatasUpdated`,
							`customersTaxAccountID`,
							`customersInvoicesNotPurchased`,
							`customersBadPaymentBehavior`,
							`customersEntryDate`
						)
						VALUES
				";
			$fileName = tempnam("/tmp", "FOO");

			$count = 0;
			$maxQueries = 400;
			$countQueries = 0;

			$sqlTemp_local_data = "";
			while ($ds = mysqli_fetch_assoc($rs_extern)) {
				foreach($ds as $thisKey => $thisValue){
					$ds[$thisKey] = addslashes($thisValue);
				}
				$sqlTemp_local_data .= "('" . implode("', '", $ds) . "')";


				if($countQueries%$maxQueries == 0){
					$sqlTemp_local_data .= ";";
					$sqlTemp_local = $sqlTemp_local_fields . $sqlTemp_local_data;
					$rsTemp_local = $dbConnection->db_query($sqlTemp_local);

					if(mysqli_error($db_open)) { fwrite($fp_log, date("Y-m-d H:i:s") . ": " . mysqli_error($db_open) . "\n\n\n"); }
					$sqlTemp_local_data = '';
				}
				else{
					if($count == ($countDatas - 1)) {
						#$sqlTemp_local .= ";";
						$sqlTemp_local_data .= ";";
					}
					else {
						#$sqlTemp_local .= ",";
						$sqlTemp_local_data .= ",";
					}
				}
				$count++;
				$countQueries++;
			}

			if($sqlTemp_local_data != ''){
				$sqlTemp_local = $sqlTemp_local_fields . $sqlTemp_local_data;
				$rsTemp_local = $dbConnection->db_query($sqlTemp_local);

				if(mysqli_error($db_open)) { fwrite($fp_log, date("Y-m-d H:i:s") . ": " . mysqli_error($db_open) . "\n\n\n"); }
				$sqlTemp_local_data = '';
			}
			#$rsTemp_local = $dbConnection->db_query($sqlTemp_local);
			#if(mysqli_error($db_open)) { fwrite($fp_log, date("Y-m-d H:i:s") . ": " . mysqli_error($db_open) . "\n"); }

			$sql_local = "
					SELECT
							`_temp_" . MANDATOR . "_acquisition_customers`.`customersMail1`,
							`_temp_" . MANDATOR . "_acquisition_customers`.`customersID` AS `acquisitionCustomerID`,
							CONCAT(
								'UPDATE `" . TABLE_ACQUISATION_CUSTOMERS . "` SET `customersKundennummer` = \'',
								`" . TABLE_CUSTOMERS . "`.`customersKundennummer`,
								'\' WHERE (`customersMail1` = \'',
								`_temp_" . MANDATOR . "_acquisition_customers`.`customersMail1`,
								'\'  OR `customersMail2` = \'',
								`_temp_" . MANDATOR . "_acquisition_customers`.`customersMail1`,
								'\')  AND (`customersKundennummer` IS NULL OR `customersKundennummer` = \'\' OR `customersKundennummer` LIKE \'%xx\');'
							) AS `sqlTemp_extern`

						FROM `_temp_" . MANDATOR . "_acquisition_customers`
						INNER  JOIN `" . TABLE_CUSTOMERS . "`
						ON(
							`_temp_" . MANDATOR . "_acquisition_customers`.`customersMail1` = `" . TABLE_CUSTOMERS . "`.`customersMail1`
							OR
							`_temp_" . MANDATOR . "_acquisition_customers`.`customersMail1` = `" . TABLE_CUSTOMERS . "`.`customersMail2`
						)

						WHERE `_temp_" . MANDATOR . "_acquisition_customers`.`customersMail1` != ''

						GROUP BY `_temp_" . MANDATOR . "_acquisition_customers`.`customersMail1`

					UNION

					SELECT
							`_temp_" . MANDATOR . "_acquisition_customers`.`customersMail2`,
							`_temp_" . MANDATOR . "_acquisition_customers`.`customersID` AS `acquisitionCustomerID`,
							CONCAT(
								'UPDATE `" . TABLE_ACQUISATION_CUSTOMERS . "` SET `customersKundennummer` = \'',
								`" . TABLE_CUSTOMERS . "`.`customersKundennummer`,
								'\' WHERE (`customersMail1` = \'',
								`_temp_" . MANDATOR . "_acquisition_customers`.`customersMail2`,
								'\' OR `customersMail2` = \'',
								`_temp_" . MANDATOR . "_acquisition_customers`.`customersMail2`,
								'\') AND (`customersKundennummer` IS NULL OR `customersKundennummer` = \'\' OR `customersKundennummer` LIKE \'%xx\');'
							) AS `sqlTemp_extern`

						FROM `_temp_" . MANDATOR . "_acquisition_customers`
						INNER  JOIN `" . TABLE_CUSTOMERS . "`
						ON(
							`_temp_" . MANDATOR . "_acquisition_customers`.`customersMail2` = `" . TABLE_CUSTOMERS . "`.`customersMail1`
							OR
							`_temp_" . MANDATOR . "_acquisition_customers`.`customersMail2` = `" . TABLE_CUSTOMERS . "`.`customersMail2`
						)

						WHERE `_temp_" . MANDATOR . "_acquisition_customers`.`customersMail2` != ''

						GROUP BY `_temp_" . MANDATOR . "_acquisition_customers`.`customersMail2`
				";

			$rs_local = $dbConnection->db_query($sql_local);
			if(mysqli_error($db_open)) { fwrite($fp_log, date("Y-m-d H:i:s") . ": " . mysqli_error($db_open) . "\n"); }

			$countUpdateRows = 0;
			while ($ds = mysqli_fetch_assoc($rs_local)) {
				$tempVar = $ds["acquisitionCustomerID"] . ' | ' . $ds["sqlTemp_extern"];

				$sqlTemp_local = "DELETE FROM `_temp_" . MANDATOR . "_acquisition_customers` WHERE `customersID` = '" . $ds["acquisitionCustomerID"] . "'";
				$rsTemp_local = $dbConnection->db_query($sqlTemp_local);
				if(mysqli_error($db_open)) { fwrite($fp_log, date("Y-m-d H:i:s") . ": " . mysqli_error($db_open) . "\n"); }

				$rsTemp_extern = $dbConnection_ExternAcqisition->db_query($ds["sqlTemp_extern"]);
				if($rsTemp_extern) {
					$countUpdateRows++;
				}
				if(mysqli_error($db_openExternAcqisition)) { fwrite($fp_log, date("Y-m-d H:i:s") . ": " . mysqli_error($db_openExternAcqisition) . "\n"); }
			}

			$sql_local = "
					SELECT
							`" . TABLE_CUSTOMERS . "`.`customersKundennummer`,
							`_temp_" . MANDATOR . "_acquisition_customers`.`customersID` AS `acquisitionCustomerID`,
							CONCAT(
								'UPDATE `" . TABLE_ACQUISATION_CUSTOMERS . "` SET `customersKundennummer` = \'',
								`" . TABLE_CUSTOMERS . "`.`customersKundennummer`,
								'\' WHERE `" . TABLE_ACQUISATION_CUSTOMERS . "`.`customersID` = \'',
								`_temp_" . MANDATOR . "_acquisition_customers`.`customersID`,
								'\' AND (`" . TABLE_ACQUISATION_CUSTOMERS . "`.`customersKundennummer` IS NULL OR `" . TABLE_ACQUISATION_CUSTOMERS . "`.`customersKundennummer` = \'\' OR `" . TABLE_ACQUISATION_CUSTOMERS . "`.`customersKundennummer` LIKE \'%xx\');'
							) AS `sqlTemp_extern`

						FROM `_temp_" . MANDATOR . "_acquisition_customers`
						INNER  JOIN `" . TABLE_CUSTOMERS . "`
						ON(
							REPLACE(`_temp_" . MANDATOR . "_acquisition_customers`.`customersCompanyStrasse`, ' ', '') = REPLACE(`" . TABLE_CUSTOMERS . "`.`customersCompanyStrasse`, ' ', '')
							AND
							`_temp_" . MANDATOR . "_acquisition_customers`.`customersCompanyHausnummer` = `" . TABLE_CUSTOMERS . "`.`customersCompanyHausnummer`
							AND
							`_temp_" . MANDATOR . "_acquisition_customers`.`customersCompanyPLZ` = `" . TABLE_CUSTOMERS . "`.`customersCompanyPLZ`
							AND (
								SUBSTRING(REPLACE(`_temp_" . MANDATOR . "_acquisition_customers`.`customersTelefon1`,' ', ''), (LENGTH(REPLACE(`_temp_" . MANDATOR . "_acquisition_customers`.`customersTelefon1`,' ', '')) - 3), LENGTH(REPLACE(`_temp_" . MANDATOR . "_acquisition_customers`.`customersTelefon1`,' ', '')))
								= SUBSTRING(REPLACE(`" . TABLE_CUSTOMERS . "`.`customersTelefon1`,' ', ''), (LENGTH(REPLACE(`" . TABLE_CUSTOMERS . "`.`customersTelefon1`,' ', '')) - 3), LENGTH(REPLACE(`" . TABLE_CUSTOMERS . "`.`customersTelefon1`,' ', '')))
								OR
								SUBSTRING(REPLACE(`_temp_" . MANDATOR . "_acquisition_customers`.`customersTelefon1`,' ', ''), (LENGTH(REPLACE(`_temp_" . MANDATOR . "_acquisition_customers`.`customersTelefon1`,' ', '')) - 3), LENGTH(REPLACE(`_temp_" . MANDATOR . "_acquisition_customers`.`customersTelefon1`,' ', '')))
								= SUBSTRING(REPLACE(`" . TABLE_CUSTOMERS . "`.`customersTelefon2`,' ', ''), (LENGTH(REPLACE(`" . TABLE_CUSTOMERS . "`.`customersTelefon2`,' ', '')) - 3), LENGTH(REPLACE(`" . TABLE_CUSTOMERS . "`.`customersTelefon2`,' ', '')))
							)
						)

						WHERE 1
							AND `_temp_" . MANDATOR . "_acquisition_customers`.`customersCompanyStrasse` != ''
							AND `_temp_" . MANDATOR . "_acquisition_customers`.`customersCompanyHausnummer` != ''
							AND `_temp_" . MANDATOR . "_acquisition_customers`.`customersCompanyPLZ` != ''
							AND `_temp_" . MANDATOR . "_acquisition_customers`.`customersTelefon1` != ''

						GROUP BY `" . TABLE_CUSTOMERS . "`.`customersKundennummer`

					UNION

					SELECT
							`" . TABLE_CUSTOMERS . "`.`customersKundennummer`,
							`_temp_" . MANDATOR . "_acquisition_customers`.`customersID` AS `acquisitionCustomerID`,
							CONCAT(
								'UPDATE `" . TABLE_ACQUISATION_CUSTOMERS . "` SET `customersKundennummer` = \'',
								`" . TABLE_CUSTOMERS . "`.`customersKundennummer`,
								'\' WHERE `" . TABLE_ACQUISATION_CUSTOMERS . "`.`customersID` = \'',
								`_temp_" . MANDATOR . "_acquisition_customers`.`customersID`,
								'\' AND (`" . TABLE_ACQUISATION_CUSTOMERS . "`.`customersKundennummer` IS NULL OR `" . TABLE_ACQUISATION_CUSTOMERS . "`.`customersKundennummer` = \'\' OR `" . TABLE_ACQUISATION_CUSTOMERS . "`.`customersKundennummer` LIKE \'%xx\');'
							) AS `sqlTemp_extern`

						FROM `_temp_" . MANDATOR . "_acquisition_customers`
						INNER  JOIN `" . TABLE_CUSTOMERS . "`
						ON(
							REPLACE(`_temp_" . MANDATOR . "_acquisition_customers`.`customersCompanyStrasse`, ' ', '') = REPLACE(`" . TABLE_CUSTOMERS . "`.`customersCompanyStrasse`, ' ', '')
							AND
							`_temp_" . MANDATOR . "_acquisition_customers`.`customersCompanyHausnummer` = `" . TABLE_CUSTOMERS . "`.`customersCompanyHausnummer`
							AND
							`_temp_" . MANDATOR . "_acquisition_customers`.`customersCompanyPLZ` = `" . TABLE_CUSTOMERS . "`.`customersCompanyPLZ`
							AND (
								SUBSTRING(REPLACE(`_temp_" . MANDATOR . "_acquisition_customers`.`customersTelefon2`,' ', ''), (LENGTH(REPLACE(`_temp_" . MANDATOR . "_acquisition_customers`.`customersTelefon2`,' ', '')) - 3), LENGTH(REPLACE(`_temp_" . MANDATOR . "_acquisition_customers`.`customersTelefon2`,' ', '')))
								= SUBSTRING(REPLACE(`" . TABLE_CUSTOMERS . "`.`customersTelefon1`,' ', ''), (LENGTH(REPLACE(`" . TABLE_CUSTOMERS . "`.`customersTelefon1`,' ', '')) - 3), LENGTH(REPLACE(`" . TABLE_CUSTOMERS . "`.`customersTelefon1`,' ', '')))
								OR
								SUBSTRING(REPLACE(`_temp_" . MANDATOR . "_acquisition_customers`.`customersTelefon2`,' ', ''), (LENGTH(REPLACE(`_temp_" . MANDATOR . "_acquisition_customers`.`customersTelefon2`,' ', '')) - 3), LENGTH(REPLACE(`_temp_" . MANDATOR . "_acquisition_customers`.`customersTelefon2`,' ', '')))
								= SUBSTRING(REPLACE(`" . TABLE_CUSTOMERS . "`.`customersTelefon2`,' ', ''), (LENGTH(REPLACE(`" . TABLE_CUSTOMERS . "`.`customersTelefon2`,' ', '')) - 3), LENGTH(REPLACE(`" . TABLE_CUSTOMERS . "`.`customersTelefon2`,' ', '')))
							)
						)

						WHERE 1
							AND `_temp_" . MANDATOR . "_acquisition_customers`.`customersCompanyStrasse` != ''
							AND `_temp_" . MANDATOR . "_acquisition_customers`.`customersCompanyHausnummer` != ''
							AND `_temp_" . MANDATOR . "_acquisition_customers`.`customersCompanyPLZ` != ''
							AND `_temp_" . MANDATOR . "_acquisition_customers`.`customersTelefon2` != ''

						GROUP BY `" . TABLE_CUSTOMERS . "`.`customersKundennummer`

					UNION

					SELECT
							`" . TABLE_CUSTOMERS . "`.`customersKundennummer`,
							`_temp_" . MANDATOR . "_acquisition_customers`.`customersID` AS `acquisitionCustomerID`,
							CONCAT(
								'UPDATE `" . TABLE_ACQUISATION_CUSTOMERS . "` SET `customersKundennummer` = \'',
								`" . TABLE_CUSTOMERS . "`.`customersKundennummer`,
								'\' WHERE `" . TABLE_ACQUISATION_CUSTOMERS . "`.`customersID` = \'',
								`_temp_" . MANDATOR . "_acquisition_customers`.`customersID`,
								'\' AND (`" . TABLE_ACQUISATION_CUSTOMERS . "`.`customersKundennummer` IS NULL OR `" . TABLE_ACQUISATION_CUSTOMERS . "`.`customersKundennummer` = \'\' OR `" . TABLE_ACQUISATION_CUSTOMERS . "`.`customersKundennummer` LIKE \'%xx\');'
							) AS `sqlTemp_extern`

						FROM `_temp_" . MANDATOR . "_acquisition_customers`
						INNER  JOIN `" . TABLE_CUSTOMERS . "`
						ON(
							REPLACE(`_temp_" . MANDATOR . "_acquisition_customers`.`customersCompanyStrasse`, ' ', '') = REPLACE(`" . TABLE_CUSTOMERS . "`.`customersCompanyStrasse`, ' ', '')
							AND
							`_temp_" . MANDATOR . "_acquisition_customers`.`customersCompanyHausnummer` = `" . TABLE_CUSTOMERS . "`.`customersCompanyHausnummer`
							AND
							`_temp_" . MANDATOR . "_acquisition_customers`.`customersCompanyPLZ` = `" . TABLE_CUSTOMERS . "`.`customersCompanyPLZ`
							AND (
								SUBSTRING(REPLACE(`_temp_" . MANDATOR . "_acquisition_customers`.`customersMobil1`,' ', ''), (LENGTH(REPLACE(`_temp_" . MANDATOR . "_acquisition_customers`.`customersMobil1`,' ', '')) - 3), LENGTH(REPLACE(`_temp_" . MANDATOR . "_acquisition_customers`.`customersMobil1`,' ', '')))
								= SUBSTRING(REPLACE(`" . TABLE_CUSTOMERS . "`.`customersMobil1`,' ', ''), (LENGTH(REPLACE(`" . TABLE_CUSTOMERS . "`.`customersMobil1`,' ', '')) - 3), LENGTH(REPLACE(`" . TABLE_CUSTOMERS . "`.`customersMobil1`,' ', '')))
								OR
								SUBSTRING(REPLACE(`_temp_" . MANDATOR . "_acquisition_customers`.`customersMobil1`,' ', ''), (LENGTH(REPLACE(`_temp_" . MANDATOR . "_acquisition_customers`.`customersMobil1`,' ', '')) - 3), LENGTH(REPLACE(`_temp_" . MANDATOR . "_acquisition_customers`.`customersMobil1`,' ', '')))
								= SUBSTRING(REPLACE(`" . TABLE_CUSTOMERS . "`.`customersMobil2`,' ', ''), (LENGTH(REPLACE(`" . TABLE_CUSTOMERS . "`.`customersMobil2`,' ', '')) - 3), LENGTH(REPLACE(`" . TABLE_CUSTOMERS . "`.`customersMobil2`,' ', '')))
							)
						)

						WHERE 1
							AND `_temp_" . MANDATOR . "_acquisition_customers`.`customersCompanyStrasse` != ''
							AND `_temp_" . MANDATOR . "_acquisition_customers`.`customersCompanyHausnummer` != ''
							AND `_temp_" . MANDATOR . "_acquisition_customers`.`customersCompanyPLZ` != ''
							AND `_temp_" . MANDATOR . "_acquisition_customers`.`customersMobil1` != ''
						GROUP BY `" . TABLE_CUSTOMERS . "`.`customersKundennummer`

					UNION

					SELECT
							`" . TABLE_CUSTOMERS . "`.`customersKundennummer`,
							`_temp_" . MANDATOR . "_acquisition_customers`.`customersID` AS `acquisitionCustomerID`,
							CONCAT(
								'UPDATE `" . TABLE_ACQUISATION_CUSTOMERS . "` SET `customersKundennummer` = \'',
								`" . TABLE_CUSTOMERS . "`.`customersKundennummer`,
								'\' WHERE `" . TABLE_ACQUISATION_CUSTOMERS . "`.`customersID` = \'',
								`_temp_" . MANDATOR . "_acquisition_customers`.`customersID`,
								'\' AND (`" . TABLE_ACQUISATION_CUSTOMERS . "`.`customersKundennummer` IS NULL OR `" . TABLE_ACQUISATION_CUSTOMERS . "`.`customersKundennummer` = \'\' OR `" . TABLE_ACQUISATION_CUSTOMERS . "`.`customersKundennummer` LIKE \'%xx\');'
							) AS `sqlTemp_extern`

						FROM `_temp_" . MANDATOR . "_acquisition_customers`
						INNER  JOIN `" . TABLE_CUSTOMERS . "`
						ON(
							REPLACE(`_temp_" . MANDATOR . "_acquisition_customers`.`customersCompanyStrasse`, ' ', '') = REPLACE(`" . TABLE_CUSTOMERS . "`.`customersCompanyStrasse`, ' ', '')
							AND
							`_temp_" . MANDATOR . "_acquisition_customers`.`customersCompanyHausnummer` = `" . TABLE_CUSTOMERS . "`.`customersCompanyHausnummer`
							AND
							`_temp_" . MANDATOR . "_acquisition_customers`.`customersCompanyPLZ` = `" . TABLE_CUSTOMERS . "`.`customersCompanyPLZ`
							AND (
								SUBSTRING(REPLACE(`_temp_" . MANDATOR . "_acquisition_customers`.`customersMobil2`,' ', ''), (LENGTH(REPLACE(`_temp_" . MANDATOR . "_acquisition_customers`.`customersMobil2`,' ', '')) - 3), LENGTH(REPLACE(`_temp_" . MANDATOR . "_acquisition_customers`.`customersMobil2`,' ', '')))
								= SUBSTRING(REPLACE(`" . TABLE_CUSTOMERS . "`.`customersMobil1`,' ', ''), (LENGTH(REPLACE(`" . TABLE_CUSTOMERS . "`.`customersMobil1`,' ', '')) - 3), LENGTH(REPLACE(`" . TABLE_CUSTOMERS . "`.`customersMobil1`,' ', '')))
								OR
								SUBSTRING(REPLACE(`_temp_" . MANDATOR . "_acquisition_customers`.`customersMobil2`,' ', ''), (LENGTH(REPLACE(`_temp_" . MANDATOR . "_acquisition_customers`.`customersMobil2`,' ', '')) - 3), LENGTH(REPLACE(`_temp_" . MANDATOR . "_acquisition_customers`.`customersMobil2`,' ', '')))
								= SUBSTRING(REPLACE(`" . TABLE_CUSTOMERS . "`.`customersMobil2`,' ', ''), (LENGTH(REPLACE(`" . TABLE_CUSTOMERS . "`.`customersMobil2`,' ', '')) - 3), LENGTH(REPLACE(`" . TABLE_CUSTOMERS . "`.`customersMobil2`,' ', '')))
							)
						)

						WHERE 1
							AND `_temp_" . MANDATOR . "_acquisition_customers`.`customersCompanyStrasse` != ''
							AND `_temp_" . MANDATOR . "_acquisition_customers`.`customersCompanyHausnummer` != ''
							AND `_temp_" . MANDATOR . "_acquisition_customers`.`customersCompanyPLZ` != ''
							AND `_temp_" . MANDATOR . "_acquisition_customers`.`customersMobil2` != ''

						GROUP BY `" . TABLE_CUSTOMERS . "`.`customersKundennummer`

					UNION

					SELECT
							`" . TABLE_CUSTOMERS . "`.`customersKundennummer`,
							`_temp_" . MANDATOR . "_acquisition_customers`.`customersID` AS `acquisitionCustomerID`,
							CONCAT(
								'UPDATE `" . TABLE_ACQUISATION_CUSTOMERS . "` SET `customersKundennummer` = \'',
								`" . TABLE_CUSTOMERS . "`.`customersKundennummer`,
								'\' WHERE `" . TABLE_ACQUISATION_CUSTOMERS . "`.`customersID` = \'',
								`_temp_" . MANDATOR . "_acquisition_customers`.`customersID`,
								'\' AND (`" . TABLE_ACQUISATION_CUSTOMERS . "`.`customersKundennummer` IS NULL OR `" . TABLE_ACQUISATION_CUSTOMERS . "`.`customersKundennummer` = \'\' OR `" . TABLE_ACQUISATION_CUSTOMERS . "`.`customersKundennummer` LIKE \'%xx\');'
							) AS `sqlTemp_extern`

						FROM `_temp_" . MANDATOR . "_acquisition_customers`
						INNER  JOIN `" . TABLE_CUSTOMERS . "`
						ON(
							REPLACE(`_temp_" . MANDATOR . "_acquisition_customers`.`customersCompanyStrasse`, ' ', '') = REPLACE(`" . TABLE_CUSTOMERS . "`.`customersCompanyStrasse`, ' ', '')
							AND
							`_temp_" . MANDATOR . "_acquisition_customers`.`customersCompanyHausnummer` = `" . TABLE_CUSTOMERS . "`.`customersCompanyHausnummer`
							AND
							`_temp_" . MANDATOR . "_acquisition_customers`.`customersCompanyPLZ` = `" . TABLE_CUSTOMERS . "`.`customersCompanyPLZ`
							AND (
								SUBSTRING(REPLACE(`_temp_" . MANDATOR . "_acquisition_customers`.`customersFax1`,' ', ''), (LENGTH(REPLACE(`_temp_" . MANDATOR . "_acquisition_customers`.`customersFax1`,' ', '')) - 3), LENGTH(REPLACE(`_temp_" . MANDATOR . "_acquisition_customers`.`customersFax1`,' ', '')))
								= SUBSTRING(REPLACE(`" . TABLE_CUSTOMERS . "`.`customersFax1`,' ', ''), (LENGTH(REPLACE(`" . TABLE_CUSTOMERS . "`.`customersFax1`,' ', '')) - 3), LENGTH(REPLACE(`" . TABLE_CUSTOMERS . "`.`customersFax1`,' ', '')))
								OR
								SUBSTRING(REPLACE(`_temp_" . MANDATOR . "_acquisition_customers`.`customersFax1`,' ', ''), (LENGTH(REPLACE(`_temp_" . MANDATOR . "_acquisition_customers`.`customersFax1`,' ', '')) - 3), LENGTH(REPLACE(`_temp_" . MANDATOR . "_acquisition_customers`.`customersFax1`,' ', '')))
								= SUBSTRING(REPLACE(`" . TABLE_CUSTOMERS . "`.`customersFax2`,' ', ''), (LENGTH(REPLACE(`" . TABLE_CUSTOMERS . "`.`customersFax2`,' ', '')) - 3), LENGTH(REPLACE(`" . TABLE_CUSTOMERS . "`.`customersFax2`,' ', '')))
							)
						)

						WHERE 1
							AND `_temp_" . MANDATOR . "_acquisition_customers`.`customersCompanyStrasse` != ''
							AND `_temp_" . MANDATOR . "_acquisition_customers`.`customersCompanyHausnummer` != ''
							AND `_temp_" . MANDATOR . "_acquisition_customers`.`customersCompanyPLZ` != ''
							AND `_temp_" . MANDATOR . "_acquisition_customers`.`customersFax1` != ''

						GROUP BY `" . TABLE_CUSTOMERS . "`.`customersKundennummer`

					UNION

					SELECT
							`" . TABLE_CUSTOMERS . "`.`customersKundennummer`,
							`_temp_" . MANDATOR . "_acquisition_customers`.`customersID` AS `acquisitionCustomerID`,
							CONCAT(
								'UPDATE `" . TABLE_ACQUISATION_CUSTOMERS . "` SET `customersKundennummer` = \'',
								`" . TABLE_CUSTOMERS . "`.`customersKundennummer`,
								'\' WHERE `" . TABLE_ACQUISATION_CUSTOMERS . "`.`customersID` = \'',
								`_temp_" . MANDATOR . "_acquisition_customers`.`customersID`,
								'\' AND (`" . TABLE_ACQUISATION_CUSTOMERS . "`.`customersKundennummer` IS NULL OR `" . TABLE_ACQUISATION_CUSTOMERS . "`.`customersKundennummer` = \'\' OR `" . TABLE_ACQUISATION_CUSTOMERS . "`.`customersKundennummer` LIKE \'%xx\');'
							) AS `sqlTemp_extern`

						FROM `_temp_" . MANDATOR . "_acquisition_customers`
						INNER  JOIN `" . TABLE_CUSTOMERS . "`
						ON(
							REPLACE(`_temp_" . MANDATOR . "_acquisition_customers`.`customersCompanyStrasse`, ' ', '') = REPLACE(`" . TABLE_CUSTOMERS . "`.`customersCompanyStrasse`, ' ', '')
							AND
							`_temp_" . MANDATOR . "_acquisition_customers`.`customersCompanyHausnummer` = `" . TABLE_CUSTOMERS . "`.`customersCompanyHausnummer`
							AND
							`_temp_" . MANDATOR . "_acquisition_customers`.`customersCompanyPLZ` = `" . TABLE_CUSTOMERS . "`.`customersCompanyPLZ`
							AND (
								SUBSTRING(REPLACE(`_temp_" . MANDATOR . "_acquisition_customers`.`customersFax2`,' ', ''), (LENGTH(REPLACE(`_temp_" . MANDATOR . "_acquisition_customers`.`customersFax2`,' ', '')) - 3), LENGTH(REPLACE(`_temp_" . MANDATOR . "_acquisition_customers`.`customersFax2`,' ', '')))
								= SUBSTRING(REPLACE(`" . TABLE_CUSTOMERS . "`.`customersFax1`,' ', ''), (LENGTH(REPLACE(`" . TABLE_CUSTOMERS . "`.`customersFax1`,' ', '')) - 3), LENGTH(REPLACE(`" . TABLE_CUSTOMERS . "`.`customersFax1`,' ', '')))
								OR
								SUBSTRING(REPLACE(`_temp_" . MANDATOR . "_acquisition_customers`.`customersFax2`,' ', ''), (LENGTH(REPLACE(`_temp_" . MANDATOR . "_acquisition_customers`.`customersFax2`,' ', '')) - 3), LENGTH(REPLACE(`_temp_" . MANDATOR . "_acquisition_customers`.`customersFax2`,' ', '')))
								= SUBSTRING(REPLACE(`" . TABLE_CUSTOMERS . "`.`customersFax2`,' ', ''), (LENGTH(REPLACE(`" . TABLE_CUSTOMERS . "`.`customersFax2`,' ', '')) - 3), LENGTH(REPLACE(`" . TABLE_CUSTOMERS . "`.`customersFax2`,' ', '')))
							)
						)

						WHERE 1
							AND `_temp_" . MANDATOR . "_acquisition_customers`.`customersCompanyStrasse` != ''
							AND `_temp_" . MANDATOR . "_acquisition_customers`.`customersCompanyHausnummer` != ''
							AND `_temp_" . MANDATOR . "_acquisition_customers`.`customersCompanyPLZ` != ''
							AND `_temp_" . MANDATOR . "_acquisition_customers`.`customersFax2` != ''

						GROUP BY `" . TABLE_CUSTOMERS . "`.`customersKundennummer`
				";

			$rs_local = $dbConnection->db_query($sql_local);

			if(mysqli_error($db_open)) { fwrite($fp_log, date("Y-m-d H:i:s") . ": " . mysqli_error($db_open) . "\n"); }

			$countUpdateRows = 0;
			while ($ds = mysqli_fetch_assoc($rs_local)) {
				$tempVar = $ds["acquisitionCustomerID"] . ' | ' . $ds["sqlTemp_extern"];
				#dd('tempVar');
				$sqlTemp_local = "DELETE FROM `_temp_" . MANDATOR . "_acquisition_customers` WHERE `customersID` = '" . $ds["acquisitionCustomerID"] . "'";
				$rsTemp_local = $dbConnection->db_query($sqlTemp_local);
				$rsTemp_extern = $dbConnection_ExternAcqisition->db_query($ds["sqlTemp_extern"]);
				if($rsTemp_extern) {
					$countUpdateRows++;
				}
			}



			##################################

			$sql_local = "DROP TABLE IF EXISTS `_temp_" . MANDATOR . "_acquisition_customers`";
			$rs_local = $dbConnection->db_query($sql_local);

			if(mysqli_error($db_open)) { fwrite($fp_log, date("Y-m-d H:i:s") . ": " . mysqli_error($db_open) . "\n"); }
		}
		*/
		// EOF UPDATE CUSTUMERNUMBERS OKE-CUSTOMERS WITH Auftragslisten-CUSTUMERS


		// BOF COPY AND CREATE CUSTOMERS FROM Auftragslisten FOR OKE UPLOAD
			// BOF COPY Auftragslisten CUSTOMERS
				$sql_local = "DROP TABLE IF EXISTS `temp_" . MANDATOR . "_upload_customers`";
				$rs_local = $dbConnection->db_query($sql_local);

				$sql_local = "CREATE TABLE `temp_" . MANDATOR . "_upload_customers` LIKE `" . TABLE_CUSTOMERS . "` ";
				$rs_local = $dbConnection->db_query($sql_local);

				$sql_local = "INSERT INTO `temp_" . MANDATOR . "_upload_customers` SELECT * FROM `" . TABLE_CUSTOMERS . "` ";
				$rs_local = $dbConnection->db_query($sql_local);
			// EOF COPY Auftragslisten CUSTOMERS

			// BOF CLEAN Auftragslisten CUSTOMERS: DELETE SALESMEN, AMAZON etc AND NON GERMAN CUSTOMERS
				$sql_local = "DELETE FROM `temp_" . MANDATOR . "_upload_customers`
								WHERE 1
									AND (
										`customersTyp` != 1
										AND
										`customersTyp` != 5
										AND
										`customersTyp` != 6
									)";
				$rs_local = $dbConnection->db_query($sql_local);

				$sql_local = "DELETE FROM `temp_" . MANDATOR . "_upload_customers`
								WHERE 1
									AND `customersCompanyCountry` != 81 ";
				$rs_local = $dbConnection->db_query($sql_local);
			// EOF CLEAN Auftragslisten CUSTOMERS: DELETE SALESMEN, AMAZON etc AND NON GERMAN CUSTOMERS

			// BOF DELETE CUSTOMERS THAT ARE IN OKE (DELETING CUSTOMER NUMBERS)
				$sql_extern = "SELECT `customersKundennummer` FROM `" . TABLE_ACQUISATION_CUSTOMERS . "` WHERE 1  AND `customersKundennummer` != ''";
				$rs_extern = $dbConnection_ExternAcqisition->db_query($sql_extern);
				while($ds = mysqli_fetch_assoc($rs_extern)){
					$sqlTemp_local = "DELETE FROM `temp_" . MANDATOR . "_upload_customers` WHERE `customersKundennummer` = '" . $ds["customersKundennummer"] .  "'";
					$rsTemp_local = $dbConnection->db_query($sqlTemp_local);
				}
			// EOF DELETE CUSTOMERS THAT ARE IN OKE (DELETING CUSTOMER NUMBERS)

			// BOF UPLOAD CUSTOMERS FROM Auftragslisten TO OKE
				$sql_local = "SELECT * FROM `temp_" . MANDATOR . "_upload_customers` WHERE 1 ";
				$rs_local = $dbConnection->db_query($sql_local);

				$sqlTemp_externPrefix = "
					INSERT INTO `" . TABLE_ACQUISATION_CUSTOMERS . "` (
							`customersID`,
							`customersKundennummer`,
							`customersFirmenname`,
							`customersFirmennameZusatz`,
							`customersFirmenInhaberVorname`,
							`customersFirmenInhaberNachname`,
							`customersFirmenInhaberAnrede`,
							`customersFirmenInhaber2Vorname`,
							`customersFirmenInhaber2Nachname`,
							`customersFirmenInhaber2Anrede`,
							`customersAnsprechpartner1Vorname`,
							`customersAnsprechpartner1Nachname`,
							`customersAnsprechpartner1Anrede`,
							`customersAnsprechpartner1Typ`,
							`customersAnsprechpartner2Vorname`,
							`customersAnsprechpartner2Nachname`,
							`customersAnsprechpartner2Anrede`,
							`customersAnsprechpartner2Typ`,
							`customersTelefon1`,
							`customersTelefon2`,
							`customersMobil1`,
							`customersMobil2`,
							`customersFax1`,
							`customersFax2`,
							`customersMail1`,
							`customersMail2`,
							`customersHomepage`,
							`customersCompanyStrasse`,
							`customersCompanyHausnummer`,
							`customersCompanyCountry`,
							`customersCompanyPLZ`,
							`customersCompanyOrt`,
							`customersLieferadresseFirmenname`,
							`customersLieferadresseFirmennameZusatz`,
							`customersLieferadresseStrasse`,
							`customersLieferadresseHausnummer`,
							`customersLieferadressePLZ`,
							`customersLieferadresseOrt`,
							`customersLieferadresseLand`,
							`customersRechnungsadresseFirmenname`,
							`customersRechnungsadresseFirmennameZusatz`,
							`customersRechnungsadresseStrasse`,
							`customersRechnungsadresseHausnummer`,
							`customersRechnungsadressePLZ`,
							`customersRechnungsadresseOrt`,
							`customersRechnungsadresseLand`,
							`customersBankName`,
							`customersKontoinhaber`,
							`customersBankKontonummer`,
							`customersBankLeitzahl`,
							`customersBankIBAN`,
							`customersBankBIC`,
							`customersBezahlart`,
							`customersZahlungskondition`,
							`customersRabatt`,
							`customersRabattType`,
							`customersUseProductMwst`,
							`customersUseProductDiscount`,
							`customersUstID`,
							`customersVertreterID`,
							`customersVertreterName`,
							`customersUseSalesmanDeliveryAdress`,
							`customersUseSalesmanInvoiceAdress`,
							`customersTyp`,
							`customersGruppe`,
							/* `customersNotiz`, */
							`customersUserID`,
							`customersTimeCreated`,
							`customersActive`,
							`customersDatasUpdated`,
							`customersTaxAccountID`,
							`customersInvoicesNotPurchased`,
							`customersBadPaymentBehavior`,
							`customersEntryDate`,
							`customersSepaExists`,
							`customersSepaValidUntilDate`,
							`customersSepaRequestedDate`,
							`customersLastVisit`,
							`customersNextVisit`,

							`customersCompanyNotExists`,
							`customersCompanyLocation`

						) VALUES
					";
				$arrSqlTemp_extern = array();
				$limitSql = 10;
				$countRow = 0;
				while($ds = mysqli_fetch_assoc($rs_local)){
					foreach(array_keys($ds) as $field){
						$ds[$field] = ($ds[$field]);
						$ds[$field] = preg_replace("/'/", "\'", $ds[$field]);
					}

					$arrSqlTemp_extern[] = "
						(
							'%',
							'" . $ds["customersKundennummer"] . "',
							'" . $ds["customersFirmenname"] . "',
							'" . $ds["customersFirmennameZusatz"] . "',
							'" . $ds["customersFirmenInhaberVorname"] . "',
							'" . $ds["customersFirmenInhaberNachname"] . "',
							'" . $ds["customersFirmenInhaberAnrede"] . "',
							'" . $ds["customersFirmenInhaber2Vorname"] . "',
							'" . $ds["customersFirmenInhaber2Nachname"] . "',
							'" . $ds["customersFirmenInhaber2Anrede"] . "',
							'" . $ds["customersAnsprechpartner1Vorname"] . "',
							'" . $ds["customersAnsprechpartner1Nachname"] . "',
							'" . $ds["customersAnsprechpartner1Anrede"] . "',
							'" . $ds["customersAnsprechpartner1Typ"] . "',
							'" . $ds["customersAnsprechpartner2Vorname"] . "',
							'" . $ds["customersAnsprechpartner2Nachname"] . "',
							'" . $ds["customersAnsprechpartner2Anrede"] . "',
							'" . $ds["customersAnsprechpartner2Typ"] . "',
							'" . $ds["customersTelefon1"] . "',
							'" . $ds["customersTelefon2"] . "',
							'" . $ds["customersMobil1"] . "',
							'" . $ds["customersMobil2"] . "',
							'" . $ds["customersFax1"] . "',
							'" . $ds["customersFax2"] . "',
							'" . $ds["customersMail1"] . "',
							'" . $ds["customersMail2"] . "',
							'" . $ds["customersHomepage"] . "',
							'" . $ds["customersCompanyStrasse"] . "',
							'" . $ds["customersCompanyHausnummer"] . "',
							'" . $ds["customersCompanyCountry"] . "',
							'" . $ds["customersCompanyPLZ"] . "',
							'" . $ds["customersCompanyOrt"] . "',
							'" . $ds["customersLieferadresseFirmenname"] . "',
							'" . $ds["customersLieferadresseFirmennameZusatz"] . "',
							'" . $ds["customersLieferadresseStrasse"] . "',
							'" . $ds["customersLieferadresseHausnummer"] . "',
							'" . $ds["customersLieferadressePLZ"] . "',
							'" . $ds["customersLieferadresseOrt"] . "',
							'" . $ds["customersLieferadresseLand"] . "',
							'" . $ds["customersRechnungsadresseFirmenname"] . "',
							'" . $ds["customersRechnungsadresseFirmennameZusatz"] . "',
							'" . $ds["customersRechnungsadresseStrasse"] . "',
							'" . $ds["customersRechnungsadresseHausnummer"] . "',
							'" . $ds["customersRechnungsadressePLZ"] . "',
							'" . $ds["customersRechnungsadresseOrt"] . "',
							'" . $ds["customersRechnungsadresseLand"] . "',
							'" . $ds["customersBankName"] . "',
							'" . $ds["customersKontoinhaber"] . "',
							'" . $ds["customersBankKontonummer"] . "',
							'" . $ds["customersBankLeitzahl"] . "',
							'" . $ds["customersBankIBAN"] . "',
							'" . $ds["customersBankBIC"] . "',
							'" . $ds["customersBezahlart"] . "',
							'" . $ds["customersZahlungskondition"] . "',
							'" . $ds["customersRabatt"] . "',
							'" . $ds["customersRabattType"] . "',
							'" . $ds["customersUseProductMwst"] . "',
							'" . $ds["customersUseProductDiscount"] . "',
							'" . $ds["customersUstID"] . "',
							'0',
							'',
							'" . $ds["customersUseSalesmanDeliveryAdress"] . "',
							'" . $ds["customersUseSalesmanInvoiceAdress"] . "',
							'" . $ds["customersTyp"] . "',
							'" . $ds["customersGruppe"] . "',

							'0',
							'" . $ds["customersTimeCreated"] . "',
							'" . $ds["customersActive"] . "',
							'" . $ds["customersDatasUpdated"] . "',
							'" . $ds["customersTaxAccountID"] . "',
							'" . $ds["customersInvoicesNotPurchased"] . "',
							'" . $ds["customersBadPaymentBehavior"] . "',
							'" . $ds["customersEntryDate"] . "',
							'" . $ds["customersSepaExists"] . "',
							'" . $ds["customersSepaValidUntilDate"] . "',
							'" . $ds["customersSepaRequestedDate"] . "',
							'" . $ds["customersLastVisit"] . "',
							'" . $ds["customersNextVisit"] . "',

							'" . $ds["customersCompanyNotExists"] . "',
							'" . $ds["customersCompanyLocation"] . "'
						)
					";

					if($countRow%$limitSql == 0 && $countRow > 0 && !empty($arrSqlTemp_extern)){
						$sqlTemp_extern = $sqlTemp_externPrefix . implode(", ", $arrSqlTemp_extern);
						$sqlTemp_extern = cleanSQL($sqlTemp_extern);
						$rsTemp_extern = $dbConnection_ExternAcqisition->db_query($sqlTemp_extern);
						$arrSqlTemp_extern = array();
						#if(!$rsTemp_extern){ echo $sqlTemp_extern . '' . "\n" . mysqli_error(); }
						if(!$rsTemp_extern){ 
							#echo $sqlTemp_extern . '' . "\n" . mysqli_error();
							if(mysqli_error($db_open)) { fwrite($fp_log, date("Y-m-d H:i:s") . ": " . mysqli_error() . "\n" . $sqlTemp_extern . "\n"); }
						}
					}
					$countRow++;
				}
				if(!empty($arrSqlTemp_extern)){
					$sqlTemp_extern = $sqlTemp_externPrefix . implode(", ", $arrSqlTemp_extern);
					$sqlTemp_extern = cleanSQL($sqlTemp_extern);
					$rsTemp_extern = $dbConnection_ExternAcqisition->db_query($sqlTemp_extern);
					$arrSqlTemp_extern = array();
					#if(!$rsTemp_extern){ echo $sqlTemp_extern . '' . "\n" . mysqli_error(); }
					if(!$rsTemp_extern){ 
						#echo $sqlTemp_extern . '' . "\n" . mysqli_error();
						if(mysqli_error($db_open)) { fwrite($fp_log, date("Y-m-d H:i:s") . ": " . mysqli_error() . "\n" . $sqlTemp_extern . "\n"); }
					}
				}
			// EOF UPLOAD CUSTOMERS FROM Auftragslisten TO OKE

		// EOF COPY AND CREATE CUSTOMERS FROM Auftragslisten FOR OKE UPLOAD

		// BOF UPDATE OKE CUSTOMERS WITH SPECIAL Auftragslisten CUSTOMERS DATA
		$todayDate = date("Y-m-d H:i:s");
		$todayMonth = date("m");
		$timeInterval = 2; // DAYS
		#$timeInterval = 20; // DAYS
		#$timeInterval = 0; // DAYS
		$selectDate = date("Y-m-d", mktime(0, 0, 0, date("m"), (date("d") - $timeInterval), date("Y") ));
		#$selectDate	= '2012-12-31';
		#$selectDate	= '0000-00-00';

		$where_local = "";
		if($timeInterval > 0){
			$where_local = " AND `customersTimeCreated` >= '" . $selectDate . "' ";
			#$where_local = " AND `customersTimeCreated` <= '" . $selectDate . "' ";
		}

		$sql_local = "SELECT
						*
						FROM `" . TABLE_CUSTOMERS . "`
						WHERE 1
							AND (
								`customersTyp` = 1
								OR
								`customersTyp` = 5
								OR
								`customersTyp` = 6
							)
							AND `customersCompanyCountry` = 81
							AND `customersKundennummer` != ''
							" . $where_local . "

			";

		$rs_local = $dbConnection->db_query($sql_local);

		if(mysqli_error($db_open)) { fwrite($fp_log, date("Y-m-d H:i:s") . ": " . mysqli_error($db_open) . "\n"); }

		$countDatas = $dbConnection->db_getMysqlNumRows($rs_local);
		$countRow = 0;


		while($ds = mysqli_fetch_assoc($rs_local)){
			$sqlTemp_extern = "
					UPDATE
						`" . TABLE_ACQUISATION_CUSTOMERS . "`

						SET

							`customersFirmenname` = '" . addslashes($ds["customersFirmenname"]) . "',
							`customersFirmennameZusatz` = '" . addslashes($ds["customersFirmennameZusatz"]) . "',

							`customersFirmenInhaberVorname` = '" . addslashes($ds["customersFirmenInhaberVorname"]) . "',
							`customersFirmenInhaberNachname` = '" . addslashes($ds["customersFirmenInhaberNachname"]) . "',
							`customersFirmenInhaberAnrede` = '" . addslashes($ds["customersFirmenInhaberAnrede"]) . "',
							`customersFirmenInhaber2Vorname` = '" . addslashes($ds["customersFirmenInhaber2Vorname"]) . "',
							`customersFirmenInhaber2Nachname` = '" . addslashes($ds["customersFirmenInhaber2Nachname"]) . "',
							`customersFirmenInhaber2Anrede` = '" . addslashes($ds["customersFirmenInhaber2Anrede"]) . "',
							`customersAnsprechpartner1Vorname` = '" . addslashes($ds["customersAnsprechpartner1Vorname"]) . "',
							`customersAnsprechpartner1Nachname` = '" . addslashes($ds["customersAnsprechpartner1Nachname"]) . "',
							`customersAnsprechpartner1Anrede` = '" . addslashes($ds["customersAnsprechpartner1Anrede"]) . "',
							`customersAnsprechpartner1Typ` = '" . addslashes($ds["customersAnsprechpartner1Typ"]) . "',
							`customersAnsprechpartner2Vorname` = '" . addslashes($ds["customersAnsprechpartner2Vorname"]) . "',
							`customersAnsprechpartner2Nachname` = '" . addslashes($ds["customersAnsprechpartner2Nachname"]) . "',
							`customersAnsprechpartner2Anrede` = '" . addslashes($ds["customersAnsprechpartner2Anrede"]) . "',
							`customersAnsprechpartner2Typ` = '" . addslashes($ds["customersAnsprechpartner2Typ"]) . "',
							`customersTelefon1` = '" . addslashes($ds["customersTelefon1"]) . "',
							`customersTelefon2` = '" . addslashes($ds["customersTelefon2"]) . "',
							`customersMobil1` = '" . addslashes($ds["customersMobil1"]) . "',
							`customersMobil2` = '" . addslashes($ds["customersMobil2"]) . "',
							`customersFax1` = '" . addslashes($ds["customersFax1"]) . "',
							`customersFax2` = '" . addslashes($ds["customersFax2"]) . "',
							`customersMail1` = '" . addslashes($ds["customersMail1"]) . "',
							`customersMail2` = '" . addslashes($ds["customersMail2"]) . "',
							`customersHomepage` = '" . addslashes($ds["customersHomepage"]) . "',
							`customersCompanyStrasse` = '" . addslashes($ds["customersCompanyStrasse"]) . "',
							`customersCompanyHausnummer` = '" . addslashes($ds["customersCompanyHausnummer"]) . "',
							`customersCompanyCountry` = '" . addslashes($ds["customersCompanyCountry"]) . "',
							`customersCompanyPLZ` = '" . addslashes($ds["customersCompanyPLZ"]) . "',
							`customersCompanyOrt` = '" . addslashes($ds["customersCompanyOrt"]) . "',
							`customersLieferadresseFirmenname` = '" . addslashes($ds["customersLieferadresseFirmenname"]) . "',
							`customersLieferadresseFirmennameZusatz` = '" . addslashes($ds["customersLieferadresseFirmennameZusatz"]) . "',
							`customersLieferadresseStrasse` = '" . addslashes($ds["customersLieferadresseStrasse"]) . "',
							`customersLieferadresseHausnummer` = '" . addslashes($ds["customersLieferadresseHausnummer"]) . "',
							`customersLieferadressePLZ` = '" . addslashes($ds["customersLieferadressePLZ"]) . "',
							`customersLieferadresseOrt` = '" . addslashes($ds["customersLieferadresseOrt"]) . "',
							`customersLieferadresseLand` = '" . addslashes($ds["customersLieferadresseLand"]) . "',
							`customersRechnungsadresseFirmenname` = '" . addslashes($ds["customersRechnungsadresseFirmenname"]) . "',
							`customersRechnungsadresseFirmennameZusatz` = '" . addslashes($ds["customersRechnungsadresseFirmennameZusatz"]) . "',
							`customersRechnungsadresseStrasse` = '" . addslashes($ds["customersRechnungsadresseStrasse"]) . "',
							`customersRechnungsadresseHausnummer` = '" . addslashes($ds["customersRechnungsadresseHausnummer"]) . "',
							`customersRechnungsadressePLZ` = '" . addslashes($ds["customersRechnungsadressePLZ"]) . "',
							`customersRechnungsadresseOrt` = '" . addslashes($ds["customersRechnungsadresseOrt"]) . "',
							`customersRechnungsadresseLand` = '" . addslashes($ds["customersRechnungsadresseLand"]) . "',
							`customersBankName` = '" . addslashes($ds["customersBankName"]) . "',
							`customersKontoinhaber` = '" . addslashes($ds["customersKontoinhaber"]) . "',
							`customersBankKontonummer` = '" . addslashes($ds["customersBankKontonummer"]) . "',
							`customersBankLeitzahl` = '" . addslashes($ds["customersBankLeitzahl"]) . "',
							`customersBankIBAN` = '" . addslashes($ds["customersBankIBAN"]) . "',
							`customersBankBIC` = '" . addslashes($ds["customersBankBIC"]) . "',
							`customersBezahlart` = '" . addslashes($ds["customersBezahlart"]) . "',
							`customersZahlungskondition` = '" . addslashes($ds["customersZahlungskondition"]) . "',
							`customersRabatt` = '" . addslashes($ds["customersRabatt"]) . "',
							`customersRabattType` = '" . addslashes($ds["customersRabattType"]) . "',
							`customersUseProductMwst` = '" . addslashes($ds["customersUseProductMwst"]) . "',
							`customersUseProductDiscount` = '" . addslashes($ds["customersUseProductDiscount"]) . "',
							`customersUstID` = '" . addslashes($ds["customersUstID"]) . "',

							`customersUseSalesmanDeliveryAdress` = '" . addslashes($ds["customersUseSalesmanDeliveryAdress"]) . "',
							`customersUseSalesmanInvoiceAdress` = '" . addslashes($ds["customersUseSalesmanInvoiceAdress"]) . "',

							`customersTyp` = '" . addslashes($ds["customersTyp"]) . "',

							`customersNotiz` = '" . addslashes($ds["customersNotiz"]) . "',
							`customersUserID` = '" . addslashes($ds["customersUserID"]) . "',
							`customersTimeCreated` = '" . addslashes($ds["customersTimeCreated"]) . "',

							`customersDatasUpdated` = '" . addslashes($ds["customersDatasUpdated"]) . "',
							`customersTaxAccountID` = '" . addslashes($ds["customersTaxAccountID"]) . "',

							`customersLastVisit` = '" . addslashes($ds["customersLastVisit"]) . "',
							`customersNextVisit` = '" . addslashes($ds["customersNextVisit"]) . "',
							`customersCompanyNotExists` = '" . addslashes($ds["customersCompanyNotExists"]) . "',

							`customersCompanyLocation` = '" . addslashes($ds["customersCompanyLocation"]) . "',

							`customersIsVisibleForUserID` = '" . addslashes($ds["customersIsVisibleForUserID"]) . "',
							`customersPriceListsID` = '" . addslashes($ds["customersPriceListsID"]) . "',
							`customersGetNoCollectedInvoice` = '" . addslashes($ds["customersGetNoCollectedInvoice"]) . "',
							`customersNoDeliveries` = '" . addslashes($ds["customersNoDeliveries"]) . "',
							`customersSendPaperInvoice` = '" . addslashes($ds["customersSendPaperInvoice"]) . "',


							`customersGruppe` = '" . addslashes($ds["customersGruppe"]) . "',
							`customersInvoicesNotPurchased` = '" . addslashes($ds["customersInvoicesNotPurchased"]) . "',
							`customersBadPaymentBehavior` = '" . addslashes($ds["customersBadPaymentBehavior"]) . "',
							`customersEntryDate` = '" . addslashes($ds["customersEntryDate"]) . "',
							`customersSepaExists` = '" . addslashes($ds["customersSepaExists"]) . "',
							`customersSepaValidUntilDate` = '" . addslashes($ds["customersSepaValidUntilDate"]) . "',
							`customersSepaRequestedDate` = '" . addslashes($ds["customersSepaRequestedDate"]) . "',
							`customersActive` = '" . addslashes($ds["customersActive"]) . "'


							,
							`customersVertreterID` = '" . addslashes($ds["customersVertreterID"]) . "',
							`customersVertreterName` = '" . addslashes($ds["customersVertreterName"]) . "'


						WHERE 1
							AND `customersKundennummer` = '" . $ds["customersKundennummer"] . "'

				";
			$sqlTemp_extern = cleanSQL($sqlTemp_extern);
			$rsTemp_extern = $dbConnection_ExternAcqisition->db_query($sqlTemp_extern);

			if(mysqli_error($db_openExternAcqisition)) { fwrite($fp_log, date("Y-m-d H:i:s") . ": " . mysqli_error($db_openExternAcqisition) . "\n" . $sqlTemp_extern . "\n"); }
			$countRow++;
		}
		// EOF UPDATE OKE CUSTOMERS WITH SPECIAL Auftragslisten CUSTOMERS DATA

		// BOF INSERT CUSTOMERS LAST ORDER DATAS FROM Auftragslisten FOR OKE UPLOAD
			$todayDate = date("Y-m-d H:i:s");
			$todayMonth = date("m");
			$timeInterval = 2; // DAYS
			$selectDate = date("Y-m-d", mktime(0, 0, 0, date("m"), (date("d") - $timeInterval), date("Y") ));

			#$selectDate = '0000-00-00';

			$exportCountryID = 81;

			$sql_local = "
					SELECT
						`tableCustomers`.`customersFirmenname`,
						`tableCustomers`.`customersKundennummer`,
						`tableCustomers`.`customersGruppe`,
						`tableCustomers`.`customersTyp`,

						`tempTable`.`orderDocumentsNumber`,
						`tempTable`.`orderDocumentsCustomerNumber`,
						`tempTable`.`orderDocumentsDocumentDate`,
						`tempTable`.`orderDocumentsStatus`,
						`tempTable`.`orderDocumentsSalesman`,

						`tempTable`.`orderDocumentDetailProductNumber`,
						`tempTable`.`orderDocumentDetailProductName`,
						`tempTable`.`orderDocumentDetailProductQuantity`,
						`tempTable`.`orderDocumentDetailProductSinglePrice`,
						`tempTable`.`orderDocumentDetailProductKategorieID`,

						`tempTable`.`orderDocumentsMandatory`,

						`tableSalesmen`.`customersFirmenname` AS `salesmenFirmenname`,

						`" . TABLE_ORDER_CATEGORIES . "`.`orderCategoriesName`

						FROM (
								SELECT
									`orderDocumentsDocumentDate`,
									`orderDocumentsNumber`,
									`orderDocumentsCustomerNumber`,
									`orderDocumentsStatus`,
									`orderDocumentsSalesman`,

									`orderDocumentDetailProductNumber`,
									`orderDocumentDetailProductName`,
									`orderDocumentDetailProductQuantity`,
									`orderDocumentDetailProductSinglePrice`,
									`orderDocumentDetailProductKategorieID`,

									'bctr' AS `orderDocumentsMandatory`

								FROM `bctr_orderConfirmations`
								LEFT JOIN `bctr_orderConfirmationsDetails`
								ON(`bctr_orderConfirmations`.`orderDocumentsID` = `bctr_orderConfirmationsDetails`.`orderDocumentDetailDocumentID`)

								WHERE 1
									AND `orderDocumentDetailProductKategorieID` LIKE '001%'
									AND `orderDocumentsDocumentDate` >= '" . $selectDate . "'

								UNION

								SELECT
									`orderDocumentsDocumentDate`,
									`orderDocumentsNumber`,
									`orderDocumentsCustomerNumber`,
									`orderDocumentsStatus`,
									`orderDocumentsSalesman`,

									`orderDocumentDetailProductNumber`,
									`orderDocumentDetailProductName`,
									`orderDocumentDetailProductQuantity`,
									`orderDocumentDetailProductSinglePrice`,
									`orderDocumentDetailProductKategorieID`,

									'b3' AS `orderDocumentsMandatory`

								FROM `b3_orderConfirmations`
								LEFT JOIN `b3_orderConfirmationsDetails`
								ON( `b3_orderConfirmations`.`orderDocumentsID` = `b3_orderConfirmationsDetails`.`orderDocumentDetailDocumentID`)

								WHERE 1
									AND `orderDocumentDetailProductKategorieID` LIKE '001%'
									AND `orderDocumentsDocumentDate` >= '" . $selectDate . "'

								ORDER BY `orderDocumentsDocumentDate` DESC
							) AS `tempTable`

							LEFT JOIN `" . TABLE_CUSTOMERS . "` AS `tableCustomers`
							ON(`tempTable`.`orderDocumentsCustomerNumber` = `tableCustomers`.`customersKundennummer`)

							LEFT JOIN `" . TABLE_ORDER_CATEGORIES . "`
							ON(`tempTable`.`orderDocumentDetailProductKategorieID` COLLATE utf8_unicode_ci = `" . TABLE_ORDER_CATEGORIES . "`.`orderCategoriesID`)

							/*
							LEFT JOIN `" . TABLE_CUSTOMER_TYPES . "`
							ON(`tableCustomers`.`customersTyp` = `" . TABLE_CUSTOMER_TYPES . "`.`customerTypesID`)

							LEFT JOIN `" . TABLE_CUSTOMER_GROUPS . "`
							ON(`tableCustomers`.`customersGruppe` = `" . TABLE_CUSTOMER_GROUPS . "`.`customerGroupsID`)

							LEFT JOIN `" . TABLE_CUSTOMERS . "` AS `tableSalesmen`
							ON(`tableCustomers`.`customersVertreterID` = `tableSalesmen`.`customersID`)
							*/

							LEFT JOIN `" . TABLE_CUSTOMERS . "` AS `tableSalesmen`
							ON(`tempTable`.`orderDocumentsSalesman` = `tableSalesmen`.`customersID`)

							WHERE 1
								AND `tableCustomers`.`customersCompanyCountry` = 81
								AND `tableCustomers`.`customersKundennummer` NOT LIKE 'AMZ%'
								AND `tableCustomers`.`customersKundennummer` NOT LIKE 'EBY%'
								AND (
									`tableCustomers`.`customersTyp` = 1
									OR
									`tableCustomers`.`customersTyp` = 5
									OR
									`tableCustomers`.`customersTyp` = 6
								)

							GROUP BY CONCAT(`tempTable`.`orderDocumentsMandatory`, `tempTable`.`orderDocumentsNumber`)
				";
			$rs_local = $dbConnection->db_query($sql_local);
			if(mysqli_error($db_open)) { fwrite($fp_log, date("Y-m-d H:i:s") . ": " . mysqli_error($db_open) . "\n"); }

			$arrSqlTemp_extern = array();
			#$limitSql = 60;
			$limitSql = 10;
			$countRow = 0;

			$fp_test = fopen('test.txt', 'w');
			fwrite($fp_test, $sql_local);
			fclose($fp_test);

			$sqlTemp_externPrefix = "
					REPLACE INTO `" . TABLE_ACQUISATION_CUSTOMERS_LAST_ORDERS . "` (
									`customersLastOrdersID`,
									`customersLastOrdersCustomerNumber`,
									`customersLastOrdersSalesmanID`,
									`customersLastOrdersDocumentNumber`,
									`customersLastOrdersDocumentDate`,
									`customersLastOrdersDocumentStatus`,
									`customersLastOrdersProductCategoryID`,
									`customersLastOrdersProductCategoryName`,
									`customersLastOrdersProductNumber`,
									`customersLastOrdersProductName`,
									`customersLastOrdersProductQuantity`,
									`customersLastOrdersProductSinglePrice`,
									`customersLastOrdersTimestamp`,

									`customersLastOrdersSalesmanCompanyName`,

									`customersLastOrdersMandatory`
								)
								VALUES
				";

			while($ds = mysqli_fetch_assoc($rs_local)){
				foreach(array_keys($ds) as $field){
					$ds[$field] = ($ds[$field]);
					$ds[$field] = preg_replace("/'/", "\'", $ds[$field]);
				}

				$arrSqlTemp_extern[] = "
					(
									'%',
									'" . $ds["customersKundennummer"] . "',
									'" . $ds["orderDocumentsSalesman"] . "',
									'" . $ds["orderDocumentsNumber"] . "',
									'" . $ds["orderDocumentsDocumentDate"] . "',
									'" . $ds["orderDocumentsStatus"] . "',
									'" . $ds["orderDocumentDetailProductKategorieID"] . "',
									'" . $ds["orderCategoriesName"] . "',
									'" . $ds["orderDocumentDetailProductNumber"] . "',
									'" . $ds["orderDocumentDetailProductName"] . "',
									'" . $ds["orderDocumentDetailProductQuantity"] . "',
									'" . $ds["orderDocumentDetailProductSinglePrice"] . "',
									NOW(),

									'" . $ds["salesmenFirmenname"] . "',

									'" . $ds["orderDocumentsMandatory"] . "'
								)
					";

				if($countRow%$limitSql == 0 && $countRow > 0 && !empty($arrSqlTemp_extern)){
					$sqlTemp_extern = $sqlTemp_externPrefix . implode(", ", $arrSqlTemp_extern);
					$sqlTemp_extern = cleanSQL($sqlTemp_extern);

					$rsTemp_extern = $dbConnection_ExternAcqisition->db_query($sqlTemp_extern);
					if(mysqli_error($db_openExternAcqisition)) { fwrite($fp_log, date("Y-m-d H:i:s") . ": " . mysqli_error($db_openExternAcqisition) . "\n"); }
					$arrSqlTemp_extern = array();
					if(!$rsTemp_extern){ echo $sqlTemp_extern . ''; }
				}
				$countRow++;
			}

			if(!empty($arrSqlTemp_extern)){
				$sqlTemp_extern = $sqlTemp_externPrefix . implode(", ", $arrSqlTemp_extern);
				$sqlTemp_extern = cleanSQL($sqlTemp_extern);

				$rsTemp_extern = $dbConnection_ExternAcqisition->db_query($sqlTemp_extern);
				if(mysqli_error($db_openExternAcqisition)) { fwrite($fp_log, date("Y-m-d H:i:s") . ": " . mysqli_error($db_openExternAcqisition) . "\n"); }
				$arrSqlTemp_extern = array();
			}
		// EOF INSERT CUSTOMERS LAST ORDER DATAS FROM Auftragslisten FOR OKE UPLOAD

		// BOF INSERT CUSTOMERS LAST ORDER HISTORIE DATAS FROM Auftragslisten FOR OKE UPLOAD
			$sql_extern = " DROP TABLE IF EXISTS `temp_customersLastOrdersHistory` ";
			$rs_extern = $dbConnection_ExternAcqisition->db_query($sql_extern);
			if(mysqli_error($db_openExternAcqisition)) { fwrite($fp_log, date("Y-m-d H:i:s") . ": " . mysqli_error($db_openExternAcqisition) . "\n"); }

			$sql_extern = " CREATE TABLE `temp_customersLastOrdersHistory` LIKE `common_customersLastOrdersHistory` ";
			$rs_extern = $dbConnection_ExternAcqisition->db_query($sql_extern);
			if(mysqli_error($db_openExternAcqisition)) { fwrite($fp_log, date("Y-m-d H:i:s") . ": " . mysqli_error($db_openExternAcqisition) . "\n"); }

			$sql_local = " SET @countRow = 0, @countRowID = '' ";
			$rs_local = $dbConnection->db_query($sql_local);
			if(mysqli_error($db_open)) { fwrite($fp_log, date("Y-m-d H:i:s") . ": " . mysqli_error($db_open) . "\n"); }

			$maxLastOrders = 5;

			$sql_local = "

					SELECT
						*
						FROM (

							SELECT

								@countRow := IF(@countRowID COLLATE utf8_unicode_ci = `tempTable`.`customersKundennummer`, @countRow := @countRow + 1, 1) AS `countRow`,
								@countRowID := `tempTable`.`customersKundennummer` AS `countRowID`,

								`tempTable`.`customersFirmenname`,
								`tempTable`.`customersKundennummer`,
								`tempTable`.`customersGruppe`,
								`tempTable`.`customersTyp`,

								`tempTable`.`orderDocumentsNumber`,
								`tempTable`.`orderDocumentsCustomerNumber`,
								`tempTable`.`orderDocumentsDocumentDate`,
								`tempTable`.`orderDocumentsStatus`,
								`tempTable`.`orderDocumentsSalesman`,

								`tempTable`.`orderDocumentDetailProductNumber`,
								`tempTable`.`orderDocumentDetailProductName`,
								`tempTable`.`orderDocumentDetailProductQuantity`,
								`tempTable`.`orderDocumentDetailProductSinglePrice`,
								`tempTable`.`orderDocumentDetailProductKategorieID`,

								`tempTable`.`orderDocumentsMandatory`,

								`tableSalesmen`.`customersFirmenname` AS `salesmenFirmenname`,

								`" . TABLE_ORDER_CATEGORIES . "`.`orderCategoriesName`

								FROM (

										SELECT
											`orderDocumentsDocumentDate`,
											`orderDocumentsNumber`,
											`orderDocumentsCustomerNumber`,
											`orderDocumentsStatus`,
											`orderDocumentsSalesman`,

											`orderDocumentDetailProductNumber`,
											`orderDocumentDetailProductName`,
											`orderDocumentDetailProductQuantity`,
											`orderDocumentDetailProductSinglePrice`,
											`orderDocumentDetailProductKategorieID`,

											`tableCustomers`.`customersFirmenname`,
											`tableCustomers`.`customersKundennummer`,
											`tableCustomers`.`customersGruppe`,
											`tableCustomers`.`customersTyp`,

											'bctr' AS `orderDocumentsMandatory`

										FROM `" . TABLE_CUSTOMERS . "` AS `tableCustomers`

										INNER JOIN `bctr_orderConfirmations`
										ON(`tableCustomers`.`customersKundennummer` = `bctr_orderConfirmations`.`orderDocumentsCustomerNumber`)

										LEFT JOIN `bctr_orderConfirmationsDetails`
										ON(`bctr_orderConfirmations`.`orderDocumentsID` = `bctr_orderConfirmationsDetails`.`orderDocumentDetailDocumentID`)

										WHERE 1
											AND `tableCustomers`.`customersCompanyCountry` = 81
											AND `tableCustomers`.`customersKundennummer` NOT LIKE 'AMZ%'
											AND `tableCustomers`.`customersKundennummer` NOT LIKE 'EBY%'
											AND (
												`tableCustomers`.`customersTyp` = 1
												OR
												`tableCustomers`.`customersTyp` = 5
												OR
												`tableCustomers`.`customersTyp` = 6
											)
											AND `bctr_orderConfirmationsDetails`.`orderDocumentDetailProductKategorieID` LIKE '001%'

										UNION

										SELECT
											`orderDocumentsDocumentDate`,
											`orderDocumentsNumber`,
											`orderDocumentsCustomerNumber`,
											`orderDocumentsStatus`,
											`orderDocumentsSalesman`,

											`orderDocumentDetailProductNumber`,
											`orderDocumentDetailProductName`,
											`orderDocumentDetailProductQuantity`,
											`orderDocumentDetailProductSinglePrice`,
											`orderDocumentDetailProductKategorieID`,

											`tableCustomers`.`customersFirmenname`,
											`tableCustomers`.`customersKundennummer`,
											`tableCustomers`.`customersGruppe`,
											`tableCustomers`.`customersTyp`,

											'b3' AS `orderDocumentsMandatory`

										FROM `" . TABLE_CUSTOMERS . "` AS `tableCustomers`

										INNER JOIN `b3_orderConfirmations`
										ON(`tableCustomers`.`customersKundennummer` = `b3_orderConfirmations`.`orderDocumentsCustomerNumber`)

										LEFT JOIN `b3_orderConfirmationsDetails`
										ON(`b3_orderConfirmations`.`orderDocumentsID` = `b3_orderConfirmationsDetails`.`orderDocumentDetailDocumentID`)

										WHERE 1
											AND `tableCustomers`.`customersCompanyCountry` = 81
											AND `tableCustomers`.`customersKundennummer` NOT LIKE 'AMZ%'
											AND `tableCustomers`.`customersKundennummer` NOT LIKE 'EBY%'
											AND (
												`tableCustomers`.`customersTyp` = 1
												OR
												`tableCustomers`.`customersTyp` = 5
												OR
												`tableCustomers`.`customersTyp` = 6
											)
											AND `b3_orderConfirmationsDetails`.`orderDocumentDetailProductKategorieID` LIKE '001%'
									ORDER BY
										`orderDocumentsCustomerNumber` ASC,
										`orderDocumentsDocumentDate` DESC
								)
								AS `tempTable`

								LEFT JOIN `" . TABLE_ORDER_CATEGORIES . "`
								ON(`tempTable`.`orderDocumentDetailProductKategorieID` COLLATE utf8_unicode_ci = `" . TABLE_ORDER_CATEGORIES . "`.`orderCategoriesID`)

								LEFT JOIN `" . TABLE_CUSTOMERS . "` AS `tableSalesmen`
								ON(`tempTable`.`orderDocumentsSalesman` = `tableSalesmen`.`customersID`)

						) AS `tempTable2`


						HAVING `countRow` < " . ($maxLastOrders + 1) . "
				";
			$rs_local = $dbConnection->db_query($sql_local);
			if(mysqli_error($db_open)) { fwrite($fp_log, date("Y-m-d H:i:s") . ": " . mysqli_error($db_open) . "\n"); }

			$arrSqlTemp_extern = array();
			#$limitSql = 60;
			$limitSql = 10;
			$countRow = 0;

			$sqlTemp_externPrefix = "
					INSERT INTO `temp_customersLastOrdersHistory` (
									`customersLastOrdersID`,
									`customersLastOrdersCustomerNumber`,
									`customersLastOrdersSalesmanID`,
									`customersLastOrdersDocumentNumber`,
									`customersLastOrdersDocumentDate`,
									`customersLastOrdersDocumentStatus`,
									`customersLastOrdersProductCategoryID`,
									`customersLastOrdersProductCategoryName`,
									`customersLastOrdersProductNumber`,
									`customersLastOrdersProductName`,
									`customersLastOrdersProductQuantity`,
									`customersLastOrdersProductSinglePrice`,
									`customersLastOrdersTimestamp`,

									`customersLastOrdersSalesmanCompanyName`,

									`customersLastOrdersMandatory`
								)
								VALUES
				";

			while($ds = mysqli_fetch_assoc($rs_local)){
				foreach(array_keys($ds) as $field){
					$ds[$field] = ($ds[$field]);
					$ds[$field] = preg_replace("/'/", "\'", $ds[$field]);
				}

				$arrSqlTemp_extern[] = "
								(
									'%',
									'" . $ds["customersKundennummer"] . "',
									'" . $ds["orderDocumentsSalesman"] . "',
									'" . $ds["orderDocumentsNumber"] . "',
									'" . $ds["orderDocumentsDocumentDate"] . "',
									'" . $ds["orderDocumentsStatus"] . "',
									'" . $ds["orderDocumentDetailProductKategorieID"] . "',
									'" . $ds["orderCategoriesName"] . "',
									'" . $ds["orderDocumentDetailProductNumber"] . "',
									'" . $ds["orderDocumentDetailProductName"] . "',
									'" . $ds["orderDocumentDetailProductQuantity"] . "',
									'" . $ds["orderDocumentDetailProductSinglePrice"] . "',
									NOW(),

									'" . $ds["salesmenFirmenname"] . "',

									'" . $ds["orderDocumentsMandatory"] . "'
								)
					";

				if($countRow%$limitSql == 0 && $countRow > 0 && !empty($arrSqlTemp_extern)){
					$sqlTemp_extern = $sqlTemp_externPrefix . implode(", ", $arrSqlTemp_extern);
					$sqlTemp_extern = cleanSQL($sqlTemp_extern);
					$rsTemp_extern = $dbConnection_ExternAcqisition->db_query($sqlTemp_extern);
					if(mysqli_error($db_openExternAcqisition)) { fwrite($fp_log, date("Y-m-d H:i:s") . ": " . mysqli_error($db_openExternAcqisition) . "\n"); }

					$arrSqlTemp_extern = array();
				}
				$countRow++;
			}

			if(!empty($arrSqlTemp_extern)){
				$sqlTemp_extern = $sqlTemp_externPrefix . implode(", ", $arrSqlTemp_extern);
				$sqlTemp_extern = cleanSQL($sqlTemp_extern);
				$rsTemp_extern = $dbConnection_ExternAcqisition->db_query($sqlTemp_extern);
				if(mysqli_error($db_openExternAcqisition)) { fwrite($fp_log, date("Y-m-d H:i:s") . ": " . mysqli_error($db_openExternAcqisition) . "\n"); }

				$arrSqlTemp_extern = array();
			}

			$sql_extern = " DROP TABLE IF EXISTS `old_customersLastOrdersHistory` ";
			$rs_extern = $dbConnection_ExternAcqisition->db_query($sql_extern);
			if(mysqli_error($db_openExternAcqisition)) { fwrite($fp_log, date("Y-m-d H:i:s") . ": " . mysqli_error($db_openExternAcqisition) . "\n"); }

			$sql_extern = " RENAME TABLE `common_customersLastOrdersHistory` TO `old_customersLastOrdersHistory` ";
			$rs_extern = $dbConnection_ExternAcqisition->db_query($sql_extern);
			if(mysqli_error($db_openExternAcqisition)) { fwrite($fp_log, date("Y-m-d H:i:s") . ": " . mysqli_error($db_openExternAcqisition) . "\n"); }

			$sql_extern = " RENAME TABLE `temp_customersLastOrdersHistory` TO `common_customersLastOrdersHistory` ";
			$rs_extern = $dbConnection_ExternAcqisition->db_query($sql_extern);
			if(mysqli_error($db_openExternAcqisition)) { fwrite($fp_log, date("Y-m-d H:i:s") . ": " . mysqli_error($db_openExternAcqisition) . "\n"); }
		// EOF INSERT CUSTOMERS LAST ORDER HISTORIE DATAS FROM Auftragslisten FOR OKE UPLOAD

		// BOF UPDATE EXTERNAL CUSTOMERS ACTIVITY STATUS AND IMPORT INTO Auftragslisten
			// BOF UPDATE EXTERNAL CUSTOMERS ACTIVITY STATUS
				$todayDate = date("Y-m-d");
				$todayYear = date("Y");
				$todayMonth = date("m");
				$todayDay = date("d");
				$timeInterval = 6; // Month
				$dateLimit = date("Y-m-d", mktime(0, 0, 0, ($todayMonth - $timeInterval), $todayDay, $todayYear));
				$startDate = "2015-10-01";


				$sql_extern = "
						INSERT INTO `" . TABLE_ACQUISATION_CUSTOMERS_ACTIVITY_STATUS . "` (

							`customerActivityStatusDate`,
							`customerActivityStatusZipCodeDouble`,
							`customerActivityStatusZipCodeTriple`,
							`customerActivityStatusType`,
							`customerActivityStatusCount`
						)

						SELECT
							DATE_FORMAT(NOW(),'%Y-%m-%d') AS `customerActivityStatusDate`,
							SUBSTRING(`tempTable`.`customerActivityStatusZipCodeTriple`, 1, 2) AS `customerActivityStatusZipCodeDouble`,
							`tempTable`.`customerActivityStatusZipCodeTriple`,
							`tempTable`.`customerActivityStatusType`,
							COUNT(`customersID`) AS `customerActivityStatusCount`

							FROM (
								SELECT

									`" . TABLE_ACQUISATION_CUSTOMERS . "`.`customersID`,

									/*
									`" . TABLE_ACQUISATION_CUSTOMERS . "`.`customersKundennummer`,
									`" . TABLE_ACQUISATION_CUSTOMERS . "`.`customersCompanyPLZ`,

									SUBSTRING(`" . TABLE_ACQUISATION_CUSTOMERS . "`.`customersCompanyPLZ`, 1, 2) AS `customerActivityStatusZipCodeDouble`,
									*/

									SUBSTRING(`" . TABLE_ACQUISATION_CUSTOMERS . "`.`customersCompanyPLZ`, 1, 3) AS `customerActivityStatusZipCodeTriple`,

									`" . TABLE_ACQUISATION_CUSTOMERS . "`.`customersEntryDate`,

									/*
									`" . TABLE_ACQUISATION_CUSTOMERS_LAST_ORDERS . "`.`customersLastOrdersDocumentDate`,
									`" . TABLE_ACQUISATION_CUSTOMERS_LAST_ORDERS . "`.`customersLastOrdersSalesmanID`,
									*/

									IF(`" . TABLE_ACQUISATION_CUSTOMERS . "`.`customersEntryDate` >= '" . $dateLimit . "' AND `" . TABLE_ACQUISATION_CUSTOMERS . "`.`customersEntryDate` >= '" . $startDate . "', 'blue',
										IF(`" . TABLE_ACQUISATION_CUSTOMERS_LAST_ORDERS . "`.`customersLastOrdersDocumentDate` > '" . $dateLimit . "' AND `" . TABLE_ACQUISATION_CUSTOMERS_LAST_ORDERS . "`.`customersLastOrdersDocumentDate` >= '" . $startDate . "', 'green',
											IF(`" . TABLE_ACQUISATION_CUSTOMERS_LAST_ORDERS . "`.`customersLastOrdersDocumentDate` <= '" . $dateLimit . "' OR `" . TABLE_ACQUISATION_CUSTOMERS_LAST_ORDERS . "`.`customersLastOrdersDocumentDate` < '" . $startDate . "', 'red',
												'red'
											)
										)
									) AS `customerActivityStatusType`

									FROM `" . TABLE_ACQUISATION_CUSTOMERS . "`

									LEFT JOIN `" . TABLE_ACQUISATION_CUSTOMERS_LAST_ORDERS . "`
									ON(
										`" . TABLE_ACQUISATION_CUSTOMERS . "`.`customersKundennummer` = `" . TABLE_ACQUISATION_CUSTOMERS_LAST_ORDERS . "`.`customersLastOrdersCustomerNumber`
										AND `" . TABLE_ACQUISATION_CUSTOMERS_LAST_ORDERS . "`.`customersLastOrdersMandatory` != 'b3'
									)

									WHERE 1
										AND `customersCompanyCountry` = 81
										AND `customersKundennummer` != ''
										AND `customersKundennummer` IS NOT NULL

									GROUP BY `" . TABLE_ACQUISATION_CUSTOMERS . "`.`customersKundennummer`

							) AS `tempTable`

						GROUP BY CONCAT(`customerActivityStatusZipCodeTriple`, ':', `customerActivityStatusType`)
					";

					// BOF OLD STATUS TYPE
						/*IF(`" . TABLE_ACQUISATION_CUSTOMERS_LAST_ORDERS . "`.`customersLastOrdersDocumentDate` > '" . $dateLimit . "' AND `" . TABLE_ACQUISATION_CUSTOMERS_LAST_ORDERS . "`.`customersLastOrdersDocumentDate` > '" . $startDate . "', 'green',
											IF(`" . TABLE_ACQUISATION_CUSTOMERS . "`.`customersEntryDate` >= '" . $dateLimit . "' AND `" . TABLE_ACQUISATION_CUSTOMERS . "`.`customersEntryDate` >= '" . $startDate . "', 'blue',
												IF(`" . TABLE_ACQUISATION_CUSTOMERS_LAST_ORDERS . "`.`customersLastOrdersDocumentDate` <= '" . $dateLimit . "' OR `" . TABLE_ACQUISATION_CUSTOMERS_LAST_ORDERS . "`.`customersLastOrdersDocumentDate` <= '" . $startDate . "', 'red',
													'red'
												)
											)
										) AS `customerActivityStatusType`
						*/
					// EOF OLD STATUS TYPE

				$sql_extern = cleanSQL($sql_extern);

				$rs_extern = $dbConnection_ExternAcqisition->db_query($sql_extern);
			// EOF UPDATE EXTERNAL CUSTOMERS ACTIVITY STATUS

			// BOF IMPORT EXTERNAL CUSTOMERS ACTIVITY STATUS INTO Auftragslisten
				$dayInterval = 0; // DAYS
				$sql_extern = "
					SELECT
							`customerActivityStatusDate`,
							`customerActivityStatusZipCodeDouble`,
							`customerActivityStatusZipCodeTriple`,
							`customerActivityStatusType`,
							`customerActivityStatusCount`

						FROM `" . TABLE_ACQUISATION_CUSTOMERS_ACTIVITY_STATUS . "`

						WHERE 1
							AND (
								`customerActivityStatusDate` <= '" . date("Y-m-d"). "'
								AND `customerActivityStatusDate` >= '" . date("Y-m-d", mktime(0, 0, 0, date("m"), (date("d") - $dayInterval), date("Y"))). "'
							)
					";
				$sql_extern = cleanSQL($sql_extern);

				$rs_extern = $dbConnection_ExternAcqisition->db_query($sql_extern);
				$countRow = 0;
				$limitSql = 20;
				$arrSqlTemp_local = array();
				$sqlTemp_localPrefix = "
							INSERT IGNORE INTO `" . TABLE_CUSTOMERS_ACTIVITY_STATUS . "` (
									`customerActivityStatusDate`,
									`customerActivityStatusZipCodeDouble`,
									`customerActivityStatusZipCodeTriple`,
									`customerActivityStatusType`,
									`customerActivityStatusCount`
								) VALUES
					";

				while($ds_extern = mysqli_fetch_assoc($rs_extern)){
					$arrSqlTemp_local[] = "
								(
									'" . $ds_extern["customerActivityStatusDate"] . "',
									'" . $ds_extern["customerActivityStatusZipCodeDouble"] . "',
									'" . $ds_extern["customerActivityStatusZipCodeTriple"] . "',
									'" . $ds_extern["customerActivityStatusType"] . "',
									'" . $ds_extern["customerActivityStatusCount"] . "'
								)
						";
					if($countRow%$limitSql == 0 && $countRow > 0 && !empty($arrSqlTemp_local)){
						$sqlTemp_local = $sqlTemp_localPrefix . implode(", ", $arrSqlTemp_local);
						$sqlTemp_local = cleanSQL($sqlTemp_local);
						$rsTemp_local = $dbConnection->db_query($sqlTemp_local);
						if(mysqli_error($db_open)) { fwrite($fp_log, date("Y-m-d H:i:s") . ": " . $sqlTemp_local . ":\n" . mysqli_error($db_open)); }

						$arrSqlTemp_local = array();
					}
					$countRow++;
				}
				if(!empty($arrSqlTemp_local)){
					$sqlTemp_local = $sqlTemp_localPrefix . implode(", ", $arrSqlTemp_local);
					$sqlTemp_local = cleanSQL($sqlTemp_local);

					$rsTemp_local = $dbConnection->db_query($sqlTemp_local);
					if(mysqli_error($db_open)) { fwrite($fp_log, date("Y-m-d H:i:s") . ": " . $sqlTemp_local . ":\n" . mysqli_error($db_open)); }

					$arrSqlTemp_local = array();
				}

			// EOF IMPORT EXTERNAL CUSTOMERS ACTIVITY STATUS INTO Auftragslisten

		// EOF UPDATE EXTERNAL CUSTOMERS ACTIVITY STATUS AND IMPORT INTO Auftragslisten

		// BOF DELETE OLDER BACK_UP_TABLES
			$dateToday = date("Y-m-d H:i:s");
			$timeInterval = 2; // DAYS
			$dateToDelete = date("Y-m-d", mktime(0, 0, 0, date("m"), (date("d") - $timeInterval), date("Y")));
			$deleteTableName = "_SICH_" . TABLE_ACQUISATION_CUSTOMERS . "_" . $dateToDelete;
			$sql_extern = "
					SELECT
						CONCAT( 'DROP TABLE ', GROUP_CONCAT('`', `TABLE_NAME`, '`') , ';' ) AS `statement`
						FROM `information_schema`.`TABLES`
						WHERE 1
							AND `table_schema` = '" . DB_NAME_EXTERN_ACQUISITION . "'
							AND `TABLE_NAME` LIKE '" . $deleteTableName . "%'
				";
			$rs_extern = $dbConnection_ExternAcqisition->db_query($sql_extern);
			list($thisSqlStatement_extern) = mysqli_fetch_array($rs_extern);

			if($thisSqlStatement_extern != ""){
				$rs_extern = $dbConnection_ExternAcqisition->db_query($thisSqlStatement_extern);
			}

			if(mysqli_error($db_openExternAcqisition)) { fwrite($fp_log, date("Y-m-d H:i:s") . ": " . mysqli_error($db_openExternAcqisition) . "\n"); }
		// EOF DELETE OLDER BACK_UP_TABLES

		/*
		// BOF UPDATE CUSTOMER NUMBERS IN CUSTOMER ORDERS
			$sql_extern = "
				UPDATE
					`common_orders`

					LEFT JOIN `" . TABLE_ACQUISATION_CUSTOMERS . "`
					ON(`common_orders`.`ordersCustomerID` = `" . TABLE_ACQUISATION_CUSTOMERS . "`.`customersID`)

					SET `common_orders`.`ordersKundennummer` = `" . TABLE_ACQUISATION_CUSTOMERS . "`.`customersKundennummer`
					WHERE 1
						AND `common_orders`.`ordersKundennummer` = ''
						AND `common_orders`.`ordersCustomerID` > 0
						AND `" . TABLE_ACQUISATION_CUSTOMERS . "`.`customersKundennummer` > 0
				";
			$rs_extern = $dbConnection_ExternAcqisition->db_query($sql_extern);
			if(mysqli_error($db_openExternAcqisition)) { fwrite($fp_log, date("Y-m-d H:i:s") . ": " . mysqli_error($db_openExternAcqisition) . "\n"); }
		// EOF UPDATE CUSTOMER NUMBERS IN CUSTOMER ORDERS
		*/

		/*
		// BOF UPDATE CUSTOMER NUMBERS IN SALESMEN REPORTS
			$sql_extern = "
				UPDATE
					`" . TABLE_ACQUISATION_USERS_DRIVERS_LOG_DETAILS . "`

					LEFT JOIN `" . TABLE_ACQUISATION_CUSTOMERS . "`
					ON(`" . TABLE_ACQUISATION_USERS_DRIVERS_LOG_DETAILS . "`.`usersDriversLogsDetailsCustomerID` = `" . TABLE_ACQUISATION_CUSTOMERS . "`.`customersID`)

					SET `" . TABLE_ACQUISATION_USERS_DRIVERS_LOG_DETAILS . "`.`usersDriversLogsDetailsCustomerNumber` = `" . TABLE_ACQUISATION_CUSTOMERS . "`.`customersKundennummer`
					WHERE 1
						AND `usersDriversLogsDetailsCustomerNumber` = ''
						AND `usersDriversLogsDetailsCustomerID` > 0
						AND `" . TABLE_ACQUISATION_CUSTOMERS . "`.`customersKundennummer` > 0	;
				";
			$rs_extern = $dbConnection_ExternAcqisition->db_query($sql_extern);
			if(mysqli_error($db_openExternAcqisition)) { fwrite($fp_log, date("Y-m-d H:i:s") . ": " . mysqli_error($db_openExternAcqisition) . "\n"); }
		// EOF UPDATE CUSTOMER NUMBERS IN SALESMEN REPORTS
		*/

		// BOF SET CUSTOMERS VISIBLE FOR HVs IN OKE DEPENDING ON Auftragslisten-ORDERS
			// BOF GET HVs FROM OKE
				$sql_extern = "
					SELECT
						*
						FROM `" . TABLE_ACQUISATION_USERS . "`
						WHERE 1
							AND `" . TABLE_ACQUISATION_USERS . "`.`usersGroupID` = 4
							AND `" . TABLE_ACQUISATION_USERS . "`.`usersActive` = '1'
							AND `" . TABLE_ACQUISATION_USERS . "`.`usersAuftragslistenCustomerID` > 0
					";
				$rs_extern = $dbConnection_ExternAcqisition->db_query($sql_extern);
				if(mysqli_error($db_openExternAcqisition)) { fwrite($fp_log, date("Y-m-d H:i:s") . ": " . mysqli_error($db_openExternAcqisition) . "\n"); }

				$arrHVsinOKE = array();
				while($ds_extern = mysqli_fetch_assoc($rs_extern)){
					foreach(array_keys($ds_extern) as $field){
						$arrHVsinOKE[$ds_extern["usersID"]][$field] = $ds_extern[$field];
					}
				}
			// EOF GET HVs FROM OKE

			if(!empty($arrHVsinOKE)){
				foreach($arrHVsinOKE as $thisHVsinOKEKey => $thisHVsinOKEValue){
					// BOF GET CUSTOMER NUMBERS FROM INVOICES AND CONFIRMATIONS WITH SALESMAN-ID
						$sql_lokal = "
								SELECT
									`orderDocumentsCustomerNumber`,
									`orderDocumentsSalesman`,
									`orderDocumentsNumber`
									FROM `bctr_orderConfirmations`
									WHERE 1
										AND `orderDocumentsSalesman` = " . $thisHVsinOKEValue["usersAuftragslistenCustomerID"] . "

								UNION

								SELECT
									`orderDocumentsCustomerNumber`,
									`orderDocumentsSalesman`,
									`orderDocumentsNumber`
									FROM `bctr_orderInvoices`
									WHERE 1
										AND `orderDocumentsSalesman` = " . $thisHVsinOKEValue["usersAuftragslistenCustomerID"] . "

								UNION

								SELECT
									`orderDocumentsCustomerNumber`,
									`orderDocumentsSalesman`,
									`orderDocumentsNumber`
									FROM `b3_orderConfirmations`
									WHERE 1
										AND `orderDocumentsSalesman` = " . $thisHVsinOKEValue["usersAuftragslistenCustomerID"] . "

								UNION

								SELECT
									`orderDocumentsCustomerNumber`,
									`orderDocumentsSalesman`,
									`orderDocumentsNumber`
									FROM `b3_orderInvoices`
									WHERE 1
										AND `orderDocumentsSalesman` = " . $thisHVsinOKEValue["usersAuftragslistenCustomerID"] . "

								UNION

								SELECT
									`ordersKundennummer` AS `orderDocumentsCustomerNumber`,
									`ordersVertreterID` AS `orderDocumentsSalesman`,
									'' AS `orderDocumentsNumber`
									FROM `" . TABLE_ORDERS . "`
									WHERE 1
										AND `ordersVertreterID` = " . $thisHVsinOKEValue["usersAuftragslistenCustomerID"] . "

								GROUP BY `orderDocumentsCustomerNumber`
							";

						$rs_lokal = $dbConnection->db_query($sql_lokal);
						if(mysqli_error($db_open)) { fwrite($fp_log, date("Y-m-d H:i:s") . ": " . mysqli_error($db_open) . "\n"); }

						$arrSqlWhere = array();
						while($ds_lokal = mysqli_fetch_assoc($rs_lokal)){
							$arrSqlWhere[] = " `" . TABLE_ACQUISATION_CUSTOMERS . "`.`customersKundennummer` = '" . $ds_lokal["orderDocumentsCustomerNumber"]. "' ";
						}
						if(!empty($arrSqlWhere)){
							$sql_externUpdate = "
									UPDATE
										`" . TABLE_ACQUISATION_CUSTOMERS . "`
										SET `" . TABLE_ACQUISATION_CUSTOMERS . "`.`customersIsVisibleForUserID` = '" . $thisHVsinOKEValue["usersID"] . "'
										/* SET `" . TABLE_ACQUISATION_CUSTOMERS . "`.`customersIsVisibleForUserID` = '0' */
										WHERE 1
											AND (
												" . implode(" OR ", $arrSqlWhere). "
											)
								";


							$rs_externUpdate = $dbConnection_ExternAcqisition->db_query($sql_externUpdate);
							if(mysqli_error($db_openExternAcqisition)) { fwrite($fp_log, date("Y-m-d H:i:s") . ": " . mysqli_error($db_openExternAcqisition) . "\n"); }

							if($rs_externUpdate){
								$successMessage .= count($arrSqlWhere) . ' OKE-Kunden wurden f&uuml;r ' . $thisHVsinOKEValue["usersFirstName"] . ' ' . $thisHVsinOKEValue["usersLastName"] . ' (Auftragslisten-ID: ' . $thisHVsinOKEValue["usersAuftragslistenCustomerID"] . ') zur Ansicht freigegeben. ' . '<br />';
							}
							else {
								$errorMessage .= count($arrSqlWhere) . ' OKE-Kunden konnten nicht f&uuml;r ' . $thisHVsinOKEValue["usersFirstName"] . ' ' . $thisHVsinOKEValue["usersLastName"] . ' (Auftragslisten-ID: ' . $thisHVsinOKEValue["usersAuftragslistenCustomerID"] . ') zur Ansicht freigegeben werden. ' . '<br />';
							}
						}
					// EOF GET CUSTOMER NUMBERS FROM INVOICES AND CONFIRMATIONS WITH SALESMAN-ID
				}
			}
		// EOF SET CUSTOMERS VISIBLE FOR HVs IN OKE DEPENDING ON Auftragslisten-ORDERS

		// BOF UPLOAD BACKUP
			# $thisBackUpPath = "/html/burhan-ctr.de/myBackups/_Auftragslisten/";
			# $fileTo	= $thisBackUpPath . "";
			# $fs_connection = new FTP_Connection(FTP_SERVER, FTP_PORT, '', FTP_LOGIN, FTP_PASSWORD);
			# $fs_open = $fs_connection->fs_connect();
			# $rs = $fs_connection->fs_put($fileFrom, $fileTo)
			# $fs_close = $fs_connection->fs_close();
		// EOF UPLOAD BACKUP

	fwrite($fp_log, date("Y-m-d H:i:s") . "ENDE" . "\n" . str_repeat("#", 10) . "\n\n\n");

	if($dbConnection_ExternAcqisition){
		$dbConnection_ExternAcqisition->db_close();
	}
	if($dbConnection){
		$dbConnection->db_close();
	}
	if($fp_log){
		fclose($fp_log);
	}
?>