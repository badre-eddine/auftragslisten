<?php

	#error_reporting(0);
	error_reporting(E_ALL & ~E_NOTICE);
	set_time_limit(0);

	DEFINE('BASEPATH_Auftragslisten', "/var/www/Auftragslisten/");
	#DEFINE('BASEPATH_CRONJOBS', $_SERVER['DOCUMENT_ROOT'] . "/");
	DEFINE('BASEPATH_CRONJOBS', BASEPATH_Auftragslisten . "cronjobs_linux/");

	require_once(BASEPATH_CRONJOBS . 'cronjobsConfig.inc.php');

	require_once(BASEPATH_Auftragslisten . 'config/configParams_' . strtolower(MANDATOR) . '.inc.php');
	require_once(BASEPATH_Auftragslisten . 'config/configBasic.inc.php');
	require_once(BASEPATH_Auftragslisten . 'config/configTables.inc.php');
	require_once(BASEPATH_Auftragslisten . 'inc/functions.inc.php');
	require_once(BASEPATH_Auftragslisten . 'classes/DB_Connection.class.php');

	DEFINE('FILENAME_LOGFILE', BASEPATH_CRONJOBS . "logs/" . MANDATOR . "_" . basename($_SERVER["PHP_SELF"]) . ".txt");

	if(file_exists(FILENAME_LOGFILE)){
		unlink(FILENAME_LOGFILE);
	}
	$fp_log = fopen(FILENAME_LOGFILE, "w");

	fwrite($fp_log, date("Y-m-d H:i:s") . ": MANDATOR : " . MANDATOR . " | getMandator : " . $getMandator . "\n");

	$dbConnection = new DB_Connection();
	$db_open = $dbConnection->db_connect();

	fwrite($fp_log, date("Y-m-d H:i:s") . "\n");

	$thisMessage = '';

	// BOF GET ORDER AND DOCUMENT DATA
		if(MANDATOR != ''){
			// BOF GET ORDER-IDs FROM PAYED VORKASSE CONFIRMATIONS
				$sql_getOrderAndDocumentData = "
						SELECT
								/* `" . TABLE_ORDER_CONFIRMATIONS . "`.`orderDocumentsID`, */
								`" . TABLE_ORDER_CONFIRMATIONS . "`.`orderDocumentsNumber`,
								/* `" . TABLE_ORDER_CONFIRMATIONS . "`.`orderDocumentsType`, */
								/* `" . TABLE_ORDER_CONFIRMATIONS . "`.`orderDocumentsOrdersIDs`, */
								`" . TABLE_ORDER_CONFIRMATIONS . "`.`orderDocumentsStatus`,
								`" . TABLE_ORDER_CONFIRMATIONS . "`.`orderDocumentsPaymentType`,
								`" . TABLE_ORDER_CONFIRMATIONS . "`.`orderDocumentsDocumentDate`,
								`" . TABLE_ORDER_CONFIRMATIONS . "`.`orderDocumentsTotalPrice`,

								`" . TABLE_ORDER_CONFIRMATIONS_DETAILS . "`.`orderDocumentDetailOrderID`,
								`" . TABLE_ORDER_CONFIRMATIONS_DETAILS . "`.`orderDocumentDetailProductPrintType`,
								/* `" . TABLE_ORDER_CONFIRMATIONS_DETAILS . "`.`orderDocumentDetailPosType`, */

								`" . TABLE_ORDER_CONFIRMATION_PAYMENTS . "`.`orderPaymentDate`,
								DATE_FORMAT(`" . TABLE_ORDER_CONFIRMATION_PAYMENTS . "`.`orderPaymentDate`, '%u')	AS `orderPaymentWeek`,

								`" . TABLE_ADDITIONAL_COSTS . "`.`additionalCostsShortName`,
								`" . TABLE_ADDITIONAL_COSTS . "`.`additionalCostsName`,
								`" . TABLE_ADDITIONAL_COSTS . "`.`additionalCostsDeliveryMaxTime`,

								/* DATE_ADD(`" . TABLE_ORDER_CONFIRMATION_PAYMENTS . "`.`orderPaymentDate`, INTERVAL `" . TABLE_ADDITIONAL_COSTS . "`.`additionalCostsDeliveryMaxTime`) AS `D_DAY`, */

								@num:=CAST(`" . TABLE_ADDITIONAL_COSTS . "`.`additionalCostsDeliveryMaxTime` AS UNSIGNED) AS `num`,
								@p  :=SUBSTR(`" . TABLE_ADDITIONAL_COSTS . "`.`additionalCostsDeliveryMaxTime`, CHAR_LENGTH(@num)+2) AS `p`,
								CASE
									WHEN @p='YEAR' THEN DATE_ADD(`" . TABLE_ORDER_CONFIRMATION_PAYMENTS . "`.`orderPaymentDate`, INTERVAL @num YEAR)
									WHEN @p='MONTH' THEN DATE_ADD(`" . TABLE_ORDER_CONFIRMATION_PAYMENTS . "`.`orderPaymentDate`, INTERVAL @num MONTH)
									WHEN @p='DAY' THEN DATE_ADD(`" . TABLE_ORDER_CONFIRMATION_PAYMENTS . "`.`orderPaymentDate`, INTERVAL @num DAY)
									WHEN @p='WEEK' THEN DATE_ADD(`" . TABLE_ORDER_CONFIRMATION_PAYMENTS . "`.`orderPaymentDate`, INTERVAL @num WEEK)
								END AS `orderCircaDeliveryDate`,

								CASE
									WHEN @p='YEAR' THEN DATE_FORMAT(DATE_ADD(`" . TABLE_ORDER_CONFIRMATION_PAYMENTS . "`.`orderPaymentDate`, INTERVAL @num YEAR), '%u')
									WHEN @p='MONTH' THEN DATE_FORMAT(DATE_ADD(`" . TABLE_ORDER_CONFIRMATION_PAYMENTS . "`.`orderPaymentDate`, INTERVAL @num MONTH), '%u')
									WHEN @p='DAY' THEN DATE_FORMAT(DATE_ADD(`" . TABLE_ORDER_CONFIRMATION_PAYMENTS . "`.`orderPaymentDate`, INTERVAL @num DAY), '%u')
									WHEN @p='WEEK' THEN DATE_FORMAT(DATE_ADD(`" . TABLE_ORDER_CONFIRMATION_PAYMENTS . "`.`orderPaymentDate`, INTERVAL @num WEEK), '%u')
								END AS `orderCircaDeliveryKW`,

								`" . TABLE_ORDERS . "`.`ordersKundennummer`,
								`" . TABLE_ORDERS . "`.`ordersLieferwoche`,
								`" . TABLE_ORDERS . "`.`ordersStatus`,
								`" . TABLE_ORDERS . "`.`ordersAdditionalCostsID`,
								`" . TABLE_ORDERS . "`.`ordersArtikelBezeichnung`,
								`" . TABLE_ORDERS . "`.`ordersArtikelBezeichnung`

							FROM `" . TABLE_ORDER_CONFIRMATIONS . "`

							LEFT JOIN `" . TABLE_ORDER_CONFIRMATIONS_DETAILS . "`
							ON(
								`" . TABLE_ORDER_CONFIRMATIONS . "`.`orderDocumentsID` = `" . TABLE_ORDER_CONFIRMATIONS_DETAILS . "`.`orderDocumentDetailDocumentID`
								AND
								`" . TABLE_ORDER_CONFIRMATIONS_DETAILS . "`.`orderDocumentDetailPosType` = 'product'
							)

							LEFT JOIN `" . TABLE_ORDER_CONFIRMATION_PAYMENTS . "`
							ON(`" . TABLE_ORDER_CONFIRMATIONS . "`.`orderDocumentsNumber` = `" . TABLE_ORDER_CONFIRMATION_PAYMENTS . "`.`orderPaymentOrderNumber`)

							LEFT JOIN `" . TABLE_ADDITIONAL_COSTS . "`
							ON(`" . TABLE_ORDER_CONFIRMATIONS_DETAILS . "`.`orderDocumentDetailProductPrintType` = `" . TABLE_ADDITIONAL_COSTS . "`.`additionalCostsID`)

							LEFT JOIN `" . TABLE_ORDERS . "`
							ON(`" . TABLE_ORDER_CONFIRMATIONS_DETAILS . "`.`orderDocumentDetailOrderID` = `" . TABLE_ORDERS . "`.`ordersID`)

							WHERE 1
								AND `" . TABLE_ORDER_CONFIRMATIONS . "`.`orderDocumentsPaymentType` = 2	/* VORKASSE */
								AND `" . TABLE_ORDER_CONFIRMATIONS . "`.`orderDocumentsStatus` = 2	/* BEZAHLT */
								AND `" . TABLE_ORDER_CONFIRMATION_PAYMENTS . "`.`orderPaymentDate` IS NOT NULL

								AND `" . TABLE_ORDERS . "`.`ordersStatus` != 1
								AND `" . TABLE_ORDERS . "`.`ordersStatus` != 6

							GROUP BY `" . TABLE_ORDER_CONFIRMATIONS_DETAILS . "`.`orderDocumentDetailOrderID`

							HAVING
								`orderCircaDeliveryKW` != `" . TABLE_ORDERS . "`.`ordersLieferwoche`


							ORDER BY
								`" . TABLE_ORDER_CONFIRMATIONS . "`.`orderDocumentsDocumentDate` DESC

							/* LIMIT 10	*/
					";

				$rs_getOrderAndDocumentData = $dbConnection->db_query($sql_getOrderAndDocumentData);
				if(mysqli_error($db_open)) {
					fwrite($fp_log, date("Y-m-d H:i:s") . ":  sql_getOrderAndDocumentData : " . $sql_getOrderAndDocumentData . "\n" . mysqli_error($db_open) . "\n\n\n");
				}

				while($ds_getOrderAndDocumentData = mysqli_fetch_assoc($rs_getOrderAndDocumentData)){
					$sql_updateOrderDeliveryKW = "
							UPDATE
								`" . TABLE_ORDERS . "`

								SET
									`" . TABLE_ORDERS . "`.`ordersLieferwoche` = '" . $ds_getOrderAndDocumentData["orderCircaDeliveryKW"] . "'

								WHERE 1
									AND `" . TABLE_ORDERS . "`.`ordersID` = '" . $ds_getOrderAndDocumentData["orderDocumentDetailOrderID"] . "'
									AND `" . TABLE_ORDERS . "`.`ordersLieferwoche` != '" . $ds_getOrderAndDocumentData["orderCircaDeliveryKW"] . "'
							;
						";

					$rs_updateOrderDeliveryKW = $dbConnection->db_query($sql_updateOrderDeliveryKW);
					if(mysqli_error($db_open)) {
						fwrite($fp_log, date("Y-m-d H:i:s") . ":  sql_updateOrderDeliveryKW : " . $sql_updateOrderDeliveryKW . "\n" . mysqli_error($db_open) . "\n\n\n");
					}
				}
			// EOF GET ORDER-IDs FROM PAYED VORKASSE CONFIRMATIONS
		}
		else {
			fwrite($fp_log, "'MANDATOR' ist leer!" . "\n");
		}
	// EOF GET ORDER AND DOCUMENT DATA

	fwrite($fp_log, date("Y-m-d H:i:s") . "ENDE" . "\n" . str_repeat("#", 10) . "\n\n\n");

	if($dbConnection){
		$dbConnection->db_close();
	}
	if($fp_log){
		fclose($fp_log);
	}
?>