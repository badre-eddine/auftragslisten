<?php
	// $url_dtd = "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"
		
	/*
	<!ATTLIST element-name attribute-name attribute-type attribute-value>
		DTD example:
			<!ATTLIST payment type CDATA "check">

		XML example:
			<payment type="check" />


		The attribute-type can be one of the following:
			Type 			Description
			CDATA 			The value is character data
			(en1|en2|..) 	The value must be one from an enumerated list
			ID 				The value is a unique id
			IDREF 			The value is the id of another element
			IDREFS 			The value is a list of other ids
			NMTOKEN 		The value is a valid XML name
			NMTOKENS 		The value is a list of valid XML names
			ENTITY 			The value is an entity
			ENTITIES 		The value is a list of entities
			NOTATION 		The value is a name of a notation
			xml: 			The value is a predefined xml value

		The attribute-value can be one of the following:
			Value 			Explanation
			#DEFAULT value 	The default value of the attribute
			#REQUIRED		The attribute is required
			#IMPLIED 		The attribute is optional
			#FIXED value 	The attribute value is fixed

		DTD:
			<!ELEMENT square EMPTY>
			<!ATTLIST square width CDATA "0">

		Valid XML:
			<square width="100" />



	<!ATTLIST element-name attribute-name attribute-type #REQUIRED>
		DTD:
			<!ATTLIST person number CDATA #REQUIRED>

		Valid XML:
			<person number="5677" />

		Invalid XML:
			<person />


	<!ATTLIST element-name attribute-name attribute-type #IMPLIED>
		DTD:
			<!ATTLIST contact fax CDATA #IMPLIED>

		Valid XML:
			<contact fax="555-667788" />

		Valid XML:
			<contact />


	<!ATTLIST element-name attribute-name attribute-type #FIXED "value">
		DTD:
			<!ATTLIST sender company CDATA #FIXED "Microsoft">

		Valid XML:
			<sender company="Microsoft" />

		Invalid XML:
			<sender company="W3Schools" />


	<!ATTLIST element-name attribute-name (en1|en2|..) default-value>
		DTD:
			<!ATTLIST payment type (check|cash) "cash">

		XML example:
			<payment type="check" />
			or
			<payment type="cash" />
	*/	
	
	
	// <!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd" [<!ATTLIST li rel CDATA #IMPLIED&gt;]>
	// <!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "myown.dtd">
	
	
	$content = '';
	$content = ' [';

	#$content .= '<!ATTLIST img rel CDATA #IMPLIED>';
	$content .= '<!ATTLIST img rel CDATA #IMPLIED %attrs;>';

	$content = ' ]';

	echo $content;

?>