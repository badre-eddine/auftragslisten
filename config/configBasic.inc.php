<?php
	
	if(isset($_SESSION["usersID"])){
		DEFINE("USERID", $_SESSION["usersID"]);

	}


	if(!defined('INCLUDE_CSS_MIN')) {
		DEFINE('INCLUDE_CSS_MIN', false);
	}
	if(!defined('INCLUDE_CSS_GZIP')) {
		DEFINE('INCLUDE_CSS_GZIP', false);
	}
	if(!defined('INCLUDE_JS_MIN')) {
		DEFINE('INCLUDE_JS_MIN', false);
	}
	if(!defined('INCLUDE_JS_GZIP')) {
		DEFINE('INCLUDE_JS_GZIP', false);
	}
	### DB_LOKAL / Auftragslisten DB
	if(!defined('DB_NAME')) {
		DEFINE('DB_NAME', 'burhanctrwawineu');	
	}
	DEFINE('DB_HOST', 'localhost');
	DEFINE('DB_PORT', '3307');
	DEFINE('DB_USER', 'root');
	DEFINE('DB_PASSWORD', '');

	// SOFTWARE START DATE
	DEFINE('SOFTWARE_START_DATE', '2013-03-20');
	DEFINE('SOFTWARE_START_YEAR', substr(SOFTWARE_START_DATE, 0, 4));

	// LOG_QUERIES
	if(!defined("LOG_SQL_QUERY")) { DEFINE('LOG_SQL_QUERY', false); }
	if(!defined("LOG_SQL_QUERY_ERRORS")) { DEFINE('LOG_SQL_QUERY_ERRORS', true); }

	// TIMEZONE / PHP_INI
	if(!defined("SET_DATE_DEFAULT_TIMEZONE")) { DEFINE('SET_DATE_DEFAULT_TIMEZONE', 'Europe/Berlin'); }
	if(!defined("LC_ALL")) { DEFINE('LC_ALL', '"de_DE@euro", "de_DE", "de", "ge"'); }

	// INI
	if(!defined("TRACK_ERRORS")) { DEFINE('TRACK_ERRORS', '1'); }
	if(!defined("AUTO_DETECT_LINE_ENDINGS")) { DEFINE('AUTO_DETECT_LINE_ENDINGS', false); }
	if(!defined("DEFAULT_SOCKET_TIMEOUT")) { DEFINE('DEFAULT_SOCKET_TIMEOUT', '7200'); }
	if(!defined("MEMORY_LIMIT")) { DEFINE('MEMORY_LIMIT', '512M'); }
	if(!defined("SENDMAIL_FROM")) { DEFINE('SENDMAIL_FROM', ''); }
	if(!defined("SESSION.GC_MAXLIFETIME")) { DEFINE('SESSION.GC_MAXLIFETIME', '7200'); }
	if(!defined("SMTP")) { DEFINE('SMTP', ''); }
	if(!defined("SMTP_PORT")) { DEFINE('SMTP_PORT', ''); }
	if(!defined("ZLIB.OUTPUT_COMPRESSION")) { DEFINE('ZLIB.OUTPUT_COMPRESSION', '5'); }
	if(!defined("SET_TIME_LIMIT")) { DEFINE('SET_TIME_LIMIT', '0'); }
	if(!defined("POST_MAX_SIZE")) { DEFINE('POST_MAX_SIZE', '5000M'); }
	if(!defined("UPLOAD_MAX_FILESIZE")) { DEFINE('UPLOAD_MAX_FILESIZE', '5000M'); }
	if(!defined("ERROR_REPORTING")) { DEFINE('ERROR_REPORTING', 0); }

	date_default_timezone_set(SET_DATE_DEFAULT_TIMEZONE);
	setlocale(LC_ALL, constant("LC_ALL"));
	setlocale(LC_TIME, constant("LC_ALL"));
	ini_set('memory_limit', MEMORY_LIMIT);
	set_time_limit(SET_TIME_LIMIT);
	ini_set('max_execution_time', SET_TIME_LIMIT);
	ini_set('post_max_size', POST_MAX_SIZE);
	ini_set('upload_max_filesize', UPLOAD_MAX_FILESIZE);
	// error_reporting(constant("ERROR_REPORTING"));
	if(preg_replace("/[0-9]/", "", constant("ERROR_REPORTING")) != "") {
		error_reporting(constant(constant("ERROR_REPORTING")));
	}
	else {
		error_reporting(intval(constant("ERROR_REPORTING")));
	}

	// AUTOMATIC LOGOUT TIME
	DEFINE('AUTOMATIC_LOGOUT_TIME', 0); // MINITS
	DEFINE('KEY_LOG', false); // BOOLEAN
	DEFINE('KEY_LOG_IN_DB', false); // BOOLEAN
	DEFINE('KEY_LOG_IN_FILE', false); // BOOLEAN

	// BASIC DOCUMENT NUMBERS
	DEFINE('BASIC_NUMBER_AN', BASIC_NUMBER_OFFER);
	DEFINE('BASIC_NUMBER_AB', BASIC_NUMBER_CONFIRMATION);
	DEFINE('BASIC_NUMBER_LS', BASIC_NUMBER_DELIVERY);
	DEFINE('BASIC_NUMBER_RE', BASIC_NUMBER_INVOICE);
	DEFINE('BASIC_NUMBER_GU', BASIC_NUMBER_CREDIT);
	DEFINE('BASIC_NUMBER_MA', BASIC_NUMBER_REMINDER);
	DEFINE('BASIC_NUMBER_M1', BASIC_NUMBER_FIRSTDEMAND);
	DEFINE('BASIC_NUMBER_M2', BASIC_NUMBER_SECONDDEMAND);
	DEFINE('BASIC_NUMBER_BR', BASIC_NUMBER_LETTER);

	// PAYMENT DEADLINES
	DEFINE('PAYMENT_DEADLINE_MA', 7); // DAYS
	DEFINE('PAYMENT_DEADLINE_M1', 7); // DAYS
	DEFINE('PAYMENT_DEADLINE_M2', 7); // DAYS
	DEFINE('BINDING_DEADLINE_AN', 1); // DAYS
	DEFINE('PAYMENT_DEADLINE_RE_10TOA', 10); // DAYS
	DEFINE('PAYMENT_DEADLINE_AB_VK', 28); // DAYS
	DEFINE('PAYMENT_DEADLINE_RE_14T', 14); // DAYS
	DEFINE('PAYMENT_DEADLINE_RE_2PCSK10T30TN', 30); // DAYS
	DEFINE('PAYMENT_DEADLINE_RE_2PCSK8T30TN', 30); // DAYS
	DEFINE('PAYMENT_DEADLINE_RE_DEFAULT', 7); // DAYS

	// PAYMENT SKONTO
	DEFINE('PAYMENT_SKONTO', 2); // %
	
	// FAKTORING BLOCKED AMOUNT //  SPERRBETRAG
	DEFINE('FAKT_BLOCKED_AMOUNT', '10'); // %
	
	// USING BCTR PRODUCT NAMES FOR B3
		DEFINE('USE_ONLY_PRODUCTS_FROM_BCTR', true); // BOOLEAN 	

	// -------------------------------------------------------------------
	if (strtoupper(substr(PHP_OS, 0, 3)) === 'WIN') {
		// echo 'This is a server using Windows!';
		DEFINE('APPLICATION_OS', 'WIN');

	}
	else {
		// echo 'This is a server not using Windows!';
		DEFINE('APPLICATION_OS', 'NO_WIN');
	}

	#DEFINE('NETWORK_PRINTER_NAME', 'ES2232(PCL)');
	#DEFINE('NETWORK_PRINTER_PORT', '9100');
	#DEFINE('NETWORK_PRINTER_HOST', '192.168.2.152');

	if(APPLICATION_OS != 'WIN') {
		DEFINE('TRAP_ERROR', '2>&1');
		DEFINE('ZIP_PATH', '7z');
		DEFINE('UNZIP_PATH', '7z');
		DEFINE('TAR_PATH', '7z');
		DEFINE('MYSQLDUMP_PATH', 'mysqldump');
		DEFINE('mysqli_PATH', 'mysql');
	}
	else {
		/*
		DEFINE('TRAP_ERROR', '');
		DEFINE('ZIP_PATH', 'C:/Program Files/7-Zip/7z.exe');
		DEFINE('TAR_PATH', 'C:/Program Files/7-Zip/7z.exe');
		DEFINE('MYSQLDUMP_PATH', 'C:/Auftragslisten_SERVER/mysql/bin/mysqldump.exe');
		*/
		DEFINE('TRAP_ERROR', '2>&1');
		DEFINE('ZIP_PATH', 'C:\Programme\7-Zip\7z.exe');
		DEFINE('UNZIP_PATH', 'C:\Programme\7-Zip\7z.exe');
		DEFINE('TAR_PATH', 'C:\Programme\7-Zip\7z.exe');
		if(preg_match("/127.0.0.1/", $_SERVER["HTTP_HOST"])) {
			DEFINE('SERVER_NAME', 'WEB_SERVER');
		}
		else {
			DEFINE('SERVER_NAME', 'Auftragslisten_SERVER');
		}
		DEFINE('MYSQLDUMP_PATH', 'C:\\' . SERVER_NAME .'\mysql\bin\mysqldump.exe');
		DEFINE('mysqli_PATH', 'C:\\' . SERVER_NAME .'\mysql\bin\mysql.exe');
	}

	// -------------------------------------------------------------------

	// BOF DEFINE DB_CONNECTION
		DEFINE("DB_HOST_EXTERN_PRODUCTION", "localhost");
		DEFINE("DB_NAME_EXTERN_PRODUCTION", "usr_web23_3");
		DEFINE("DB_USER_EXTERN_PRODUCTION", "root");
		DEFINE("DB_PORT_EXTERN_PRODUCTION", "3306");
		DEFINE("DB_PASSWORD_EXTERN_PRODUCTION", "");
	// EOF DEFINE DB_CONNECTION
	
	// PARCEL-TRACKING URLS
		#DEFINE('TRACKING_URL_DPD' , 'https://tracking.dpd.de/cgi-bin/delistrack?pknr={###TRACKING_NUMBER###}&amp;typ=1&amp;lang=de');
		DEFINE('TRACKING_URL_DPD' , 'https://tracking.dpd.de/parcelstatus?query={###TRACKING_NUMBER###}&amp;locale=de_DE');
		DEFINE('TRACKING_URL_POST' , 'https://www.deutschepost.de/sendung/simpleQueryResult.html');
		#DEFINE('TRACKING_URL_DHL' , 'https://nolp.dhl.de/nextt-online-public/set_identcodes.do?idc={###TRACKING_NUMBER###}');
		DEFINE('TRACKING_URL_DHL' , 'https://nolp.dhl.de/nextt-online-public/de/search?piececode={###TRACKING_NUMBER###}&amp;cid=dhlde');		
		DEFINE('TRACKING_URL_GLS' , 'https://gls-group.eu/DE/de/paketverfolgung?match={###TRACKING_NUMBER###}');

	// BOF SALEMESMEN AREA CONTRACT START DATE
		// $startDate = "2015-10-01";
		DEFINE('SALEMESMEN_AREA_CONTRACT_START_DATE', "2015-10-01");

	// BOF DEFINE ZIPCODE TESTAREAS
	$arrTestAreas = array(
			// "1" => array("name" => "Henry Schulz", "area" => "26;27;28;49"),
			// "2" => array("name" => "Klaus-Dieter Antz", "area" => "29;30;31;38;39"),
			// "3" => array("name" => "Gerhard Füller", "area" => "72;73;88;89"),
			// "4" => array("name" => "Matthias Köhring", "area" => "01;02;04;09"),
			"5" => array("name" => "Michael Weichselbaum", "area" => "90;91;92;93;94;95;96;98;99"),
			"6" => array("name" => "Lothar Weber", "area" => "77;78;79"),
			"7" => array("name" => "Thomas Hiepler", "area" => "32;33"),
			"8" => array("name" => "Faseler Marketing", "area" => "60;61;63;64;65;66;67;68;69")
		);
	// EOF DEFINE ZIPCODE TESTAREAS

	// BOF DEFINE SPECIAL MAIL ADRESSES
		// MAIL ADRESS FOR FAKTORING COMPANY (FAKT)
		DEFINE('MAIL_ADDRESS_FAKTORING_COMPANY', 'kundenbetreuung@universal-factoring.com');
		// MAIL ADRESS FOR FAKTORING COMPANY (FAKT)
		DEFINE('MAIL_ADDRESS_TAX_ACCOUNTANT', 'a.moneck@steuerberater-meyer.de');
	// EOF DEFINE SPECIAL MAIL ADRESSES
	
	// BOF DEFINE FACTORING MAX INVOICE LIMIT
		DEFINE('FACTORING_MAX_INVOICE_LIMIT', 10000);
	// EOF DEFINE FACTORING MAX INVOICE LIMIT
	
	// DPD TRACKING NUMBERS
		$arrDeliveryTrackingBasicNumbers = array(
			"DE" => "01485004",
			"TR" => "01485007"
		);

	// DPD FTP-INTERFACE
		DEFINE('DPD_FTP_INTERFACE_SERVER', 'ftp.dpd.de');
		DEFINE('DPD_FTP_INTERFACE_PORT', 21);
		DEFINE('DPD_FTP_INTERFACE_LOGIN', 'burhan149');
		DEFINE('DPD_FTP_INTERFACE_PASSWORD', 'xb25t');
	
	// DPD INTERVAL OF EVENT-DATE
		DEFINE('DPD_CHECK_EVENT_DATE_INTERVAL', '1 MONTH');
	
	// BOF DPD SCAN CODES
		$arrDpdScanCodes = array(
			"01" => "Konsolidierung/Dekonsolidierung",
			"02" => "Eingangsscannung",
			"03" => "Ausrollung",
			"04" => "UN-Retoure",
			"05" => "Einrollung",
			"06" => "Systemretoure",
			"07" => "Ausgang",
			"08" => "Lager",
			"09" => "Eingang-Differenz",
			"10" => "Hauptdepotdurchlauf",
			"12" => "In Verzollung",
			"13" => "Zugestellt Scan",
			"14" => "Keine Zustellung",
			"15" => "Abhol Scan",
			"17" => "Verzollt",
			"18" => "Infoscan",
			"20" => "Beladescan",
			"77" => "Rueckholung",
			"97" => "Dekonsolidierung"
		);
		$arrDpdScanCodesDescription = array(
			"01" => "Konsolidierung: Auf dem Transportweg zwischen Versanddepot und Empfangsdepot durchlaufen Pakete – bei langen Transportwegen - teilweise weitere Depots. Diese Depots werden auch als Konsolidierungsknoten bezeichnet. Durch den Vorgang der Konsolidierung werden die Pakete nach Zielen sortiert und nach Zielrichtung zusammengefasst.",
			"02" => "Eingang: Alle Pakete, die innerhalb eines Tages in einem Empfangsdepot eintreffen, werden dort gescannt. Nach der Scannung werden sie im Paketlebenslauf unter dem Begriff 'Eingang' aufgeführt.",
			"03" => "Ausrollung: Im Empfangsdepot werden die Pakete sortiert und kommen in den Auslieferungsvorgang an die Empfänger. Sie werden an den Fahrer übergeben und in die Zustellfahrzeuge verladen. Dieser Vorgang wird als 'Ausrollung' bezeichnet.",
			"04" => "Ausrollretoure: Pakete, die nicht zugestellt werden konnten, werden am selben Tag vom Zustellfahrer an das Depot zurückgeliefert. Sie werden dort als 'Ausrollretoure' erfasst. Der Grund, weshalb ein Paket nicht zugestellt werden konnte, wird mit einem Differenzcode erfasst und im Paketlebenslauf angezeigt.",
			"05" => "Einrollung: Nach der Abholung der Pakete vom Versender werden sie im Versanddepot von einem DPD Mitarbeiter gescannt. Diese erstmalige Erfassung eines Paketes im DPD System wird 'Einrollung' genannt.",
			"06" => "System-Retoure: Als 'System-Retoure' bezeichnet man Pakete, die aufgrund von Fehlern z. B. bei der Weiterleitung, Verladung oder Postleitzahlzuordnung im falschen Depot ankommen sind und an ein anderes Depot weitergeleitet oder an den Versender retourniert werden. Der Grund für die Systemretoure wird mit einem Differenzcode erfasst und im Paketlebenslauf angezeigt.",
			"08" => "Lager: Pakete, die am Eingangstag im Empfangsdepot nicht in den Zustellvorgang kommen, werden unter 'Lager' zusammengefasst. In diesen Fällen erfolgt die Zustellung am nächsten Tag. Der Grund für die Lagerscannung wird mit einem Differenzcode erfasst und im Paketlebenslauf angezeigt.",
			"10" => "HUB-Durchlauf: Teilweise durchlaufen Pakete im überregionalen oder grenzüberschreitenden Verkehr auf dem Weg vom Versanddepot zum Empfangsdepot ein so genanntes HUB. Im HUB (HUB = Haupt-Umschlagbasis) werden die Pakete gescannt und für den weiteren Transport sortiert. Pakete, die nach der letzten Abfahrt zum jeweiligen Empfangsdepot/nächsten HUB am HUB umgeschlagen werden, werden für den weiteren Versand am nächsten Tag vorbereitet.",
			"12" => "In Verzollung: Pakete, die sich in der Zollabfertigung befinden, sind 'In Verzollung'.",
			"13" => "Zustellung: Die Pakete, die dem Empfänger erfolgreich zugestellt und quittiert worden sind, werden mit dem Sammelbegriff 'Zustellung' bezeichnet.",
			"14" => "Zustellversuch nicht erfolgreich: Kann ein Paket dem Empfänger nicht zugestellt werden, erhält es das Kennzeichen 'Zustellversuch nicht erfolgreich'. Der Grund für 'Zustellversuch nicht erfolgreich' wird mit einem Differenzcode erfasst und im Paketlebenslauf angezeigt.",
			"17" => "Export/Import abgefertigt: Pakete, die vom Zoll freigegeben wurden, erhalten als Erfassungskennzeichen 'Export/Import abgefertigt'.",
			"77" => "Rückholung: Der Kunde kann sein Versanddepot mit der Rückholung von Paketen beauftragen. Dazu gibt er eine Adresse an, unter der das Paket zurückgeholt werden soll. Es kann dann zum Kunden zurück gebracht oder an einen Dritten (Hersteller o.ä.) weitergeleitet werden.",
		);
	// EOF DPD SCAN CODES
?>