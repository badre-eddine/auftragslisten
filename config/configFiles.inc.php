<?php
	$thisGzip = '';

	$prefixFilesMandator = MANDATOR;
	$prefixFilesCommon = 'common';
	
	// BASEPATH
	DEFINE('DIR_INSTALLATION_NAME', 'Auftragslisten/');
	DEFINE('BASEPATH', preg_replace("/\/$/", "", $_SERVER["DOCUMENT_ROOT"]) . '/' . DIR_INSTALLATION_NAME);
	// echo BASEPATH. 'classes/DB_Connection.class.php';exit();
	require_once(BASEPATH. 'classes/DB_Connection.class.php');

	// LOG_FILES
	DEFINE('PATH_LOGFILES', 'logFiles/');
	DEFINE('PATH_LOGFILES_DB_QUERY', BASEPATH . 'logFiles/db_query.log');
	DEFINE('PATH_LOGFILES_DB_QUERY_ERRORS', BASEPATH . 'logFiles/db_query_errors.log');

	// DPD INTERFACE TRACKING FILES
	DEFINE('PATH_DPD_INTERFACE_TRACKING_FILES', 'dpdTrackingFilesInterface/');
	DEFINE('PATH_DPD_INTERFACE_TRACKING_FILES_ZIPPED_FILES', PATH_DPD_INTERFACE_TRACKING_FILES . 'zippedFiles/');

	DEFINE('PATH_ONLINE_LOGFILES', 'logFilesOnline/');

	// ONLINE SHOPS
	DEFINE('PATH_ONLINE_SHOP_BCTR', 'http://www.burhan-ctr.de/');
	DEFINE('PATH_ONLINE_SHOP_B3', 'http://www.b3-werbepartner.de/');
	if(!defined("PATH_ONLINE_SHOP_" . strtoupper(MANDATOR))){
		DEFINE('PATH_ONLINE_SHOP_MANDATOR' , PATH_ONLINE_SHOP_BCTR);
	}

	// ONLINE ACQUISITION
	#DEFINE('PATH_ONLINE_ACQUISITION', 'http://www.burhan-ctr.de/Kundenerfassung/');
	DEFINE('PATH_ONLINE_ACQUISITION', 'http://oke.burhan-ctr.de/');

	// ONLINE ACQUISITION
	DEFINE('PATH_ONLINE_PRODUCTIONS', 'http://productions.burhan-ctr.de/');

	// SOUNDS
	DEFINE('DIRECTORY_SOUND_FILES', 'sounds/');

	// DOCUMENTS
	DEFINE('DIRECTORY_DOCUMENTS', 'documents_' . MANDATOR . '/');
	DEFINE('DIRECTORY_DOCUMENTS_COMMON', 'documents_common/');
	DEFINE('DIRECTORY_CREATED_DOCUMENTS', DIRECTORY_DOCUMENTS . 'documentsCreated');
	DEFINE('DIRECTORY_XML_FILES', DIRECTORY_DOCUMENTS . 'xml/');
	DEFINE('DIRECTORY_CREATED_DOCUMENTS_SALESMEN', DIRECTORY_DOCUMENTS . 'documentsCreated/salesmen/');
 
	DEFINE('DIRECTORY_CREATED_DOCUMENTS_PDF', DIRECTORY_DOCUMENTS_COMMON . 'aufdruckPDF/');
	DEFINE('DIRECTORY_CREATED_DOCUMENTS_GROUPS', DIRECTORY_DOCUMENTS . 'documentsCreated/groups/');

	// IMPORT DATA FILES
	DEFINE('DIRECTORY_IMPORT_FILES', DIRECTORY_DOCUMENTS . 'importDataFiles/');
	DEFINE('PATH_SENDED_LAYOUT_FILES', 'Z:\Vorlagen\Kunden_Mails');

	// EXPORT DATA FILES
	DEFINE('DIRECTORY_EXPORT_FILES', BASEPATH . DIRECTORY_DOCUMENTS . 'exportDataFiles/');
	DEFINE('PATH_EXPORT_FILES_TAX_ACCOUNTANT', DIRECTORY_EXPORT_FILES . 'buchungslisten/');

	// ATTACHMENTS
	DEFINE('DIRECTORY_ATTACHMENT_DOCUMENTS', DIRECTORY_DOCUMENTS . 'attachmentFiles/');
	DEFINE('PATH_AGB_PDF', DIRECTORY_ATTACHMENT_DOCUMENTS . 'AGB_' . strtoupper(MANDATOR) . '.pdf');
	DEFINE('PATH_VCARD', DIRECTORY_ATTACHMENT_DOCUMENTS . 'contact.vcf');
	DEFINE('PATH_SEPA_FORM', DIRECTORY_ATTACHMENT_DOCUMENTS . strtolower(MANDATOR) . '_sepa-formular.pdf');

	// PDF CREATION TEMPLATES
	DEFINE('DIRECTORY_CREATED_DOCUMENTS_CUSTOMERS', DIRECTORY_DOCUMENTS . 'documentsCreated/customers/');
	DEFINE('DIRECTORY_PDF_TEMPLATES', 'documents_' . MANDATOR . '/documentTemplates/templatesPDF/');
	DEFINE('DIRECTORY_PDF_IMAGES', 'documents_' . MANDATOR . '/documentTemplates/templatesPDF/images/');
	DEFINE('DIRECTORY_MAIL_TEMPLATES', 'documents_' . MANDATOR . '/documentTemplates/templatesMail/');
	DEFINE('DIRECTORY_MAIL_IMAGES', 'documents_' . MANDATOR . '/documentTemplates/templatesMail/images/');

	// VCARD CREATION TEMPLATES
	DEFINE('PATH_VCARD_TEMPLATE', 'documents_common/templates_vcard/templateVcard.inc.txt');

	// LANGUAGE FILES
	DEFINE('DIRECTORY_LANGUAGE_FILES', DIRECTORY_DOCUMENTS_COMMON . 'languageFiles/');
	DEFINE('LANGUAGE_FILE_NAME', DIRECTORY_LANGUAGE_FILES . '{###LANGUAGE_SHORT_NAME###}.php');

	// UPLOADED FILES
	#DEFINE('DIRECTORY_UPLOAD_FILES', 'documents_' . MANDATOR . '/documentsCustomerUpload/');
	DEFINE('DIRECTORY_UPLOAD_FILES', DIRECTORY_DOCUMENTS_COMMON . 'documentsCustomerUpload/');
	// DOWNLOAD FILES
	// DEFINE('DIRECTORY_DOWNLOAD_FILES', 'documents_' . MANDATOR . '/documentsDownload/');
	DEFINE('DIRECTORY_DOWNLOAD_FILES', DIRECTORY_DOCUMENTS_COMMON . 'documentsDownload/');
	// DOWNLOAD MANUALS
	// DEFINE('DIRECTORY_DOWNLOAD_MANUALS', 'documents_' . MANDATOR . '/documentsManuals/');
	DEFINE('DIRECTORY_DOWNLOAD_MANUALS', DIRECTORY_DOCUMENTS_COMMON . 'documentsManuals/');
	// DOWNLOAD CATALOGUES
	//DEFINE('DIRECTORY_DOWNLOAD_CATALOGUES', 'documents_' . MANDATOR . '/documentsCatalogues/');
	DEFINE('DIRECTORY_DOWNLOAD_CATALOGUES', DIRECTORY_DOCUMENTS_COMMON . 'documentsCatalogues/');
	DEFINE('DIRECTORY_DOWNLOAD_SHEETS', DIRECTORY_DOCUMENTS_COMMON . 'documentsSheets/');
	DEFINE('DIRECTORY_PRINT_PRODUCTION_FILES', DIRECTORY_DOCUMENTS_COMMON . 'printProductionFiles/');
	DEFINE('DIRECTORY_DOWNLOAD_CONTAINER_LISTS', DIRECTORY_DOCUMENTS_COMMON . 'containerLists/');
	DEFINE('DIRECTORY_LAYOUT_PREVIEW_IMAGES', DIRECTORY_DOCUMENTS_COMMON . 'layoutPreviewImages/');
	DEFINE('DIRECTORY_PRINT_PLATE_FILES', DIRECTORY_DOCUMENTS_COMMON . 'printPlateFiles/');


	// SCRIPT LIBRARY
	DEFINE('DIRECTORY_SCRIPT_PACKAGES', 'scriptPackages/');
	DEFINE('PATH_TREESIZE', 'scriptPackages/treesize/');
	DEFINE('PATH_PHP_EXCEL', 'scriptPackages/PHPExcel_1.7.9_doc/'); // PHPExcel_1.7.9_doc | PHPExcel_1.7.9_odt
	DEFINE('DIRECTORY_HTML2PDF', 'scriptPackages/html2pdf/');
	DEFINE('PATH_PDF2TEXT', 'scriptPackages/pdf2text/pdf2text.php');
	#DEFINE('PATH_PDF_TO_HTML', 'scriptPackages/pdf-to-html-master/');
	#DEFINE('DIRECTORY_PDF_MERGER', 'scriptPackages/PDFMerger/');
	DEFINE('DIRECTORY_PDF_MERGER', 'scriptPackages/PDFMerger-master/');
	DEFINE('DIRECTORY_JQUERY_UPLOADER', 'scriptPackages/ax-jquery-multiuploader/');
	DEFINE('DIRECTORY_CKEDITOR', 'scriptPackages/ckeditor/');
	
	#DEFINE('DIRECTORY_HTML_MIME_MAIL', 'scriptPackages/htmlMimeMail_3.0/'); 
	DEFINE('DIRECTORY_HTML_MIME_MAIL', 'scriptPackages/htmlMimeMail/'); 
	
	DEFINE('FILE_JQUERY_JS', 'scriptPackages/jquery-1.8.3.min.js' . $thisGzip);
	#DEFINE('FILE_JQUERY_JS', 'scriptPackages/jquery-2.0.2.min.js' . $thisGzip);
	DEFINE('FILE_JQUERY_UI_JS', 'scriptPackages/jquery-ui-custom/jquery-ui-1.9.2.custom.min.js' . $thisGzip);
	DEFINE('FILE_JQUERY_DATEPICKER_DE_JS', 'scriptPackages/jquery-ui-custom/jquery.ui.datepicker-de.js' . $thisGzip);
	DEFINE('FILE_JQUERY_TIMEPICKER_DE_JS', 'scriptPackages/jquery-ui-timepicker-0.3.3/jquery.ui.timepicker.js' . $thisGzip);
	DEFINE('FILE_JQUERY_TIMEPICKER_DE_CSS', 'scriptPackages/jquery-ui-timepicker-0.3.3/jquery.ui.timepicker.css' . $thisGzip);
	DEFINE('FILE_JQUERY_UI_CSS', 'scriptPackages/jquery-ui-custom/jquery-ui-1.8.18.custom.css' . $thisGzip);
	DEFINE('FILE_JQUERY_MEDIA_JS', 'scriptPackages/jquery.media.js' . $thisGzip);
	DEFINE('FILE_JQUERY_LAZYLOAD_JS', 'scriptPackages/jquery.lazyload.min.js' . $thisGzip);
	DEFINE('FILE_JQUERY_POWERTABLE', 'scriptPackages/powertable/jquery-powertable.js' . $thisGzip);
	DEFINE('FILE_JQUERY_GMAP', 'scriptPackages/gmap-master/jquery.gmap.min.js' . $thisGzip);
	DEFINE('FILE_JQUERY_CONFIRM_JS', 'scriptPackages/jquery-confirm/jquery-confirm.min.js');
	DEFINE('FILE_JQUERY_CONFIRM_CSS', 'scriptPackages/jquery-confirm/jquery-confirm.min.css');
	DEFINE('PATH_RGRAPH', 'scriptPackages/RGraph2_2012-12-31-stable/RGraph/');
	DEFINE('PATH_PHPTERM', 'scriptPackages/phpterm-0.3.0/phpterm.php');
	DEFINE('PATH_SERVER_INFO', 'scriptPackages/phpsysinfo-3.2.5/index.php');
	DEFINE('PATH_SERVER_INFO2', 'scriptPackages/linfo-2.0.3/index.php');
	DEFINE('PATH_PHP_PRINTIPP', 'scriptPackages/phpprintipp-0.83/php_classes/');
	DEFINE('PATH_MAILBOX_CLASS', 'scriptPackages/mailboxClasses/php5-imap-class/Imap.class.php');
	#DEFINE('PATH_GOOGLE_MAPS_API', 'http://maps.google.com/maps/api/js?sensor=true&callback=initialize');
	DEFINE('PATH_GOOGLE_MAPS_API_KEY_BROWSER', 'AIzaSyDxB0jouKhQua7mAkgdTVwE0HOQYMqtbf4'); // KEY FOR BROWSER APPS (WITH REFERERS)
	DEFINE('PATH_GOOGLE_MAPS_API_KEY_SERVER', 'AIzaSyAzfjA3cwfaR0euWJ6-4FUbmhmk59CFMP0'); // KEY FOR SERVER APPS (WITH IP LOCKING) 192.168.2.5
	DEFINE('PATH_GOOGLE_MAPS_API', 'http://maps.google.com/maps/api/js?sensor=true');
	DEFINE('PATH_GOOGLE_MAPS', 'http://maps.google.com/maps?q={###STREET_NAME###}+{###STREET_NUMBER###}+{###CITY_NAME###}+{###CITY_ZIP_CODE###}');
	DEFINE('PATH_GOOGLE_MAPS_URL_LATLON', 'http://www.google.de/maps?q={###LAT###},{###LNG###}');
	DEFINE('PATH_PR_CLASS', 'classes/googlePagerank.class.php');
	DEFINE('PATH_SEO_STATS', 'scriptPackages/SEOstats-master/SEOstats/');
	#DEFINE('PATH_ALEXA_RANK', 'http://data.alexa.com/data?cli=10&amp;url={###URL###}');
	DEFINE('PATH_ALEXA_RANK', 'http://data.alexa.com/data?cli=10&amp;dat=snbamz&amp;url={###URL###}');
	DEFINE('FILE_JQUERY_COLORBOX_JS', 'scriptPackages/colorbox-master/jquery.colorbox-min.js');
	DEFINE('FILE_JQUERY_COLORBOX_CSS', 'scriptPackages/colorbox-master/colorbox.css');
	DEFINE('FILE_JQUERY_TIMESELECTOR_JS', 'scriptPackages/timeselector-master/src/jquery.timeselector.js' . $thisGzip);
	DEFINE('FILE_JQUERY_TIMESELECTOR_CSS', 'scriptPackages/timeselector-master/src/theme/jquery.timeselector.css' . $thisGzip);
	DEFINE('FILE_GOOGLE_MAP_LABEL_JS', 'scriptPackages/googleMaplabel/maplabel.min.js' . $thisGzip);
	DEFINE('FILE_JQUERY_BLINK_JS', 'scriptPackages/jquerylightweight.modern-blink/jquery.modern-blink.js');
	DEFINE('FILE_EXPLORER', 'scriptPackages/encode-explorer_6.4.1/');
	DEFINE('FILE_MANAGER', 'scriptPackages/phpFileManager-0.9.8/');
	DEFINE('PATH_JAVASCRIPT_ALERT', 'scriptPackages/javascript-alert-v2.4/');
	DEFINE('FILE_JAVASCRIPT_ALERT_JS', 'scriptPackages/javascript-alert-v2.4/alert.js' . $thisGzip);
	DEFINE('FILE_JAVASCRIPT_ALERT_CSS', 'scriptPackages/javascript-alert-v2.4/alert.css' . $thisGzip);
	DEFINE('PATH_mysqli_DUMPER', 'scriptPackages/MySQLDumper1.24.4/');
	DEFINE('PATH_JQUERY_SELECT', 'scriptPackages/jquery_select2-3.5.2/');
	DEFINE('PATH_PHP_INCUTIO_XML_RPC', 'scriptPackages/php-ixr-1.7.4/IXR_Library.php');
	DEFINE('FILE_JAVASCRIPT_CLUSTERIZE_JS', 'scriptPackages/clusterize.js-master/clusterize.min.js');
	DEFINE('FILE_JAVASCRIPT_CLUSTERIZE_CSS', 'scriptPackages/clusterize.js-master/clusterize.css');
	DEFINE('PATH_PHP_OCR', 'phpOCR-0.0.3');

	// THUMBSHOTS
	// DEFINE('PATH_THUMBSHOT_SRC', 'http://www.thumbshots-server.com/webthumb/webthumb.php?url={###LINK###}');
	// DEFINE('PATH_THUMBSHOT_SRC', 'http://seitwert.websnapr.com/?url={###LINK###}&amp;size=S');
	DEFINE('PATH_THUMBSHOT_SRC', 'http://www.thumbshots.de/cgi-bin/show.cgi?url={###LINK###}');
	DEFINE('DIRECTORY_EXTERNAL_LINK_IMAGES', 'externalLinkImages/');

	// BACKUP
	DEFINE('BACKUP_DATABASE_FILE', 'backupDatabase_{###DATE###}');
	DEFINE('BACKUP_IMAGES_FILE', 'backupImages_{###DATE###}');
	DEFINE('BACKUP_FILES_FILE', 'backupFiles_{###DATE###}');
	DEFINE('BACKUP_COMPLETE_FILE', 'backupComplete_{###DATE###}');
	DEFINE('DELETE_BACKUPS_FILE', 'deleteBackups_{###DATE###}');
	DEFINE('DIRECTORY_BACKUP_FILES', 'backupFiles/');

	// MENUES
	DEFINE('FILE_MENUE_TOP', 'menues/menueTop.inc.php');
	DEFINE('FILE_MENUE_SIDEBAR', 'menues/menueSidebar.inc.php');
	DEFINE('FILE_MENUE_PAGES', 'menues/menuePages.inc.php');

	// PAGES
		// INDEX
			DEFINE('PAGE_INDEX', './');
			DEFINE('PAGE_ADMIN_INDEX', 'admin/');

		// HOME
			DEFINE('PAGE_QUICK_LINKS', 'quickLinks.php');
			DEFINE('PAGE_AFTER_LOGIN', 'quickLinks.php');
			DEFINE('PAGE_EXIT_LOCATION', 'quickLinks.php?noRights=true');
			DEFINE('PAGE_NOCONNEX_LOCATION', 'quickLinks.php?noConnex=true');


		// MASTER DATA
			DEFINE('PAGE_DISPLAY_PRODUCTS', 'displayProducts.php');
			DEFINE('PAGE_EDIT_CUSTOMER', 'editCustomer2.php');

			DEFINE('PAGE_IMPORT_CUSTOMER', 'compareAndImportCustomer.php');
			DEFINE('PAGE_COMPANY_DATAS', 'editConfigCompany.php');
			DEFINE('PAGE_DISPLAY_PERSONNEL', 'displayPersonnel.php');
			DEFINE('PAGE_DISPLAY_CALENDAR', 'displayCalendar.php');
			DEFINE('PAGE_EDIT_SALESMEN', 'displaySalesmen.php');
			DEFINE('PAGE_SHIPPING_DATAS', 'editConfigShipping.php');
			DEFINE('PAGE_DISPLAY_BANK_DATAS', 'displayBankAccounts.php');
			if(isset($_COOKIE["isAdmin"]) && $_COOKIE["isAdmin"] == '1'){
				DEFINE('PAGE_EDIT_DISTRIBUTORS', 'displaySuppliers2.php');
				DEFINE('PAGE_EDIT_SUPPLIERS', 'displaySuppliers2.php');
			}
			else {
				DEFINE('PAGE_EDIT_DISTRIBUTORS', 'displaySuppliers.php');
				DEFINE('PAGE_EDIT_SUPPLIERS', 'displaySuppliers.php');
			}
			DEFINE('PAGE_DISPLAY_ADDRESSES', 'displayAddresses.php');
			DEFINE('PAGE_DISPLAY_DISTRIBUTION', 'http://vertrieb.burhan-ctr.de');
			
			DEFINE('PAGE_SALESMEN_FLASH_MAP', 'displayFlashMapsSalesmen.php');
			#DEFINE('PAGE_SALESMEN_FLASH_MAP', 'displayFlashMapsSalesmen2.php');			
			#DEFINE('PAGE_SALESMEN_FLASH_MAP', 'displayFlashMapsSalesmen3.php');
			
			DEFINE('FILE_SALESMEN_FLASH_MAP', 'plz_karte3.swf');
			DEFINE('FILE_SALESMEN_FLASH_MAP_IMAGE', 'plz_karte3.jpg');

		// ORDERS & PROCESSES	
			
			DEFINE('PAGE_EDIT_PROCESS', 'editProcess.php');
			
			DEFINE('PAGE_DISPLAY_CUSTOMERS_ONLINE', 'displayOnlineAcquisition.php');
			DEFINE('PAGE_DISPLAY_ONLINE_ORDERS', 'displayOnlineOrders.php');
			DEFINE('PAGE_DISPLAY_ORDERS', 'displayOrdersOverview.php');
			DEFINE('PAGE_DISPLAY_EXTERNAL_PRODUCTIONS', 'displayExternalProductions.php');
			DEFINE('PAGE_EDIT_EXTERNAL_PRODUCTIONS', 'editExternalProductions.php');
			DEFINE('PAGE_DISPLAY_ORDERS_OVERVIEW', 'displayOrdersOverview.php');
			DEFINE('PAGE_DISPLAY_AMAZON_ORDERS', 'displayAmazonOrders.php');
			DEFINE('PAGE_SHOP_AMAZON', 'http://sellercentral.amazon.de/');
			DEFINE('PAGE_SHOP_AMAZON_ORDERS', 'http://sellercentral.amazon.de/gp/orders-v2/details/ref=ag_orddet_cont_myo?ie=UTF8&amp;orderID={###AMAZON_ORDER_ID##}');
			DEFINE('PAGE_DISPLAY_SUPPLIER_ORDERS', 'displaySupplierOrders.php');
			DEFINE('PAGE_DISPLAY_ORDERS_GRAPHICS', 'displayOrdersGraphics.php');
			DEFINE('PAGE_DISPLAY_SALESMEN_SALES', 'displaySalesmenSales.php');
			DEFINE('PAGE_DISPLAY_ZIPCODE_SALES', 'displayZipcodeSales.php');
			DEFINE('PAGE_DISPLAY_SALESMEN_DIRECT_SALES', 'displaySalesmenDirectSales.php');
			DEFINE('PAGE_DISPLAY_CUSTOMERS_ORDER_INTERVALS', 'displayCustomersOrderIntervals.php');
			DEFINE('PAGE_DISPLAY_ABS_PLATE_DATA', 'displayPrintingPlatesData.php');

		// BOOK MAKING
			DEFINE('PAGE_CREATE_DOCUMENT', 'createDocument.php');
			
			DEFINE('PAGE_CREATE_LETTER', 'createDocumentLetter.php');
			DEFINE('PAGE_CREATE_INVOICE', 'createDocumentInvoice.php');
			DEFINE('PAGE_CREATE_COLLECTIVE_INVOICE', 'createDocumentCollectiveInvoice.php');
			DEFINE('PAGE_CREATE_OFFER', 'createDocumentOffer.php');
			DEFINE('PAGE_CREATE_REMINDER', 'createDocumentReminder.php');
			DEFINE('PAGE_CREATE_CONFIRMATION', 'createDocumentConfirmation.php');
			DEFINE('PAGE_CREATE_DELIVERY', 'createDocumentDelivery.php');
			DEFINE('PAGE_CREATE_CREDIT', 'createDocumentCredit.php');
			DEFINE('PAGE_DOCUMENT_PREVIEW', 'displayDocumentPreview.php');
			/*
			DEFINE('PAGE_CREATE_LETTER', 'createDocument.php?documentType=BR');
			DEFINE('PAGE_CREATE_INVOICE', 'createDocument.php?documentType=RE');
			DEFINE('PAGE_CREATE_OFFER', 'createDocument.php?documentType=AN');
			DEFINE('PAGE_CREATE_REMINDER', 'createDocument.php?documentType=MA');
			DEFINE('PAGE_CREATE_CONFIRMATION', 'createDocument.php?documentType=AB');
			DEFINE('PAGE_CREATE_DELIVERY', 'createDocument.php?documentType=LS');
			DEFINE('PAGE_CREATE_CREDIT', 'createDocument.php?documentType=GU');
			*/

			DEFINE('PAGE_DISPLAY_DOCUMENT', 'displayDocument.php');
			DEFINE('PAGE_DISPLAY_DOCUMENTS', 'displayDocumentAll.php');
			DEFINE('PAGE_DISPLAY_LETTER', 'displayDocumentLetter.php');
			DEFINE('PAGE_DISPLAY_INVOICE', 'displayDocumentInvoice.php');
			DEFINE('PAGE_DISPLAY_OFFER', 'displayDocumentOffer.php');
			DEFINE('PAGE_DISPLAY_REMINDER', 'displayDocumentReminder.php');
			DEFINE('PAGE_DISPLAY_FIRST_DEMAND', 'displayDocumentFirstDemand.php');
			DEFINE('PAGE_DISPLAY_SECOND_DEMAND', 'displayDocumentSecondDemand.php');
			DEFINE('PAGE_DISPLAY_CONFIRMATION', 'displayDocumentConfirmation.php');
			DEFINE('PAGE_DISPLAY_DELIVERY', 'displayDocumentDelivery.php');
			DEFINE('PAGE_DISPLAY_CREDIT', 'displayDocumentCredit.php');
			DEFINE('PAGE_DISPLAY_SUMMONS', 'displayDocumentSummons.php');
			DEFINE('PAGE_DISPLAY_DEPRECIATION', 'displayDocumentDepreciation.php');
			DEFINE('PAGE_DISPLAY_REKLAMATION', 'displayDocumentReklamation.php');

			DEFINE('PAGE_DISPLAY_BR', PAGE_DISPLAY_LETTER);
			DEFINE('PAGE_DISPLAY_AN', PAGE_DISPLAY_OFFER);
			DEFINE('PAGE_DISPLAY_AB', PAGE_DISPLAY_CONFIRMATION);
			DEFINE('PAGE_DISPLAY_RE', PAGE_DISPLAY_INVOICE);
			DEFINE('PAGE_DISPLAY_MA', PAGE_DISPLAY_REMINDER);
			DEFINE('PAGE_DISPLAY_M1', PAGE_DISPLAY_FIRST_DEMAND);
			DEFINE('PAGE_DISPLAY_M2', PAGE_DISPLAY_SECOND_DEMAND);
			DEFINE('PAGE_DISPLAY_GU', PAGE_DISPLAY_CREDIT);
			DEFINE('PAGE_DISPLAY_LS', PAGE_DISPLAY_DELIVERY);
			DEFINE('PAGE_DISPLAY_KR', PAGE_DISPLAY_REKLAMATION);

			/*
			DEFINE('PAGE_DISPLAY_LETTER', 'displayOrderDocuments.php?documentType=BR');
			DEFINE('PAGE_DISPLAY_INVOICE', 'displayOrderDocuments.php?documentType=RE');
			DEFINE('PAGE_DISPLAY_OFFER', 'displayOrderDocuments.php?documentType=AN');
			DEFINE('PAGE_DISPLAY_REMINDER', 'displayOrderDocuments.php?documentType=MA');
			DEFINE('PAGE_DISPLAY_CONFIRMATION', 'displayOrderDocuments.php?documentType=AB');
			DEFINE('PAGE_DISPLAY_DELIVERY', 'displayOrderDocuments.php?documentType=LS');
			DEFINE('PAGE_DISPLAY_CREDIT', 'displayOrderDocuments.php?documentType=GU');
			*/

		// FINANCIAL
			DEFINE('PAGE_ALL_CONFIRMATIONS', 'displayAllConfirmations.php');
			DEFINE('PAGE_UNPAID_CONFIRMATIONS', 'displayUnpaidConfirmations.php');
			DEFINE('PAGE_PAID_CONFIRMATIONS', 'displayPaidConfirmations.php');
			DEFINE('PAGE_ALL_INVOICES', 'displayAllInvoices.php');
			DEFINE('PAGE_PAID_INVOICES', 'displayPaidInvoices.php');
			DEFINE('PAGE_PAIDPARTLY_INVOICES', 'displayPaidPartlyInvoices.php');
			DEFINE('PAGE_UNCREATED_INVOICES', 'displayUncreatedInvoices.php');
			DEFINE('PAGE_UNPAID_INVOICES', 'displayUnpaidInvoices.php');
			DEFINE('PAGE_REMIND_INVOICES', 'displayRemindInvoices.php');
			DEFINE('PAGE_REMINDED_INVOICES', 'displayRemindedInvoices.php');
			DEFINE('PAGE_FIRST_DEMANDED_INVOICES', 'displayFirstDemandedInvoices.php');
			DEFINE('PAGE_SECOND_DEMANDED_INVOICES', 'displaySecondDemandedInvoices.php');
			DEFINE('PAGE_CREDITED_INVOICES', 'displayCreditedInvoices.php');
			DEFINE('PAGE_ALL_CREDITS', 'displayAllCredits.php');
			DEFINE('PAGE_DISPLAY_CONFIRMATIONS_WITHOUT_INVOICE', 'displayConfirmationsNoInvoice.php');
			DEFINE('PAGE_SUMMONSED_INVOICES', 'displaySummonsedInvoices.php');
			DEFINE('PAGE_DEPRECIATED_INVOICES', 'displayDepreciatedInvoices.php');
			DEFINE('PAGE_CASHING_INVOICES', 'displayCashingInvoices.php');
			DEFINE('PAGE_DISPLAY_FINANCIAL_STATISTICS', 'displayFinancialStatistics.php');

			DEFINE('PAGE_ALL_AB', PAGE_ALL_CONFIRMATIONS);
			DEFINE('PAGE_ALL_RE', PAGE_ALL_INVOICES);
			DEFINE('PAGE_ALL_GU', PAGE_ALL_CREDITS);

		// PROVISIONS
			if(isset($_SESSION["usersID"] ) && $_SESSION["usersID"] == 'xxx1'){
				DEFINE('PAGE_SALESMEN_PROVISION', 'displaySalesmenProvision_NEW.php');
				DEFINE('TEST_TABLE_SALESMEN_PROVISIONS', 'test_salesmenProvisions');
				DEFINE('TEST_TABLE_SALESMEN_PROVISIONS_DETAILS', 'test_salesmenProvisionsDetails');
			}
			else {
				DEFINE('PAGE_SALESMEN_PROVISION', 'displaySalesmenProvision.php');
			}

			
		// PHONE_MARKETING
			DEFINE('PAGE_PHONE_MARKETING_DATES', 'displayPhoneMarketingDates.php');
			DEFINE('PAGE_PHONE_MARKETING_STATISTICS', 'displayPhoneMarketingStatistics.php');
			
			DEFINE('PAGE_GROUPS_PROVISION', 'displayGroupsProvision.php');
			DEFINE('PAGE_DISPLAY_SALESMEN_REPORTS', 'displaySalesmenReports.php');

		// DOCUMENTS
			DEFINE('PAGE_DOWNLOAD_DOCUMENTS', 'displayCustomerFiles.php');
			DEFINE('PAGE_DOWNLOAD_FILES', 'displayDownloadFiles.php');
			DEFINE('PAGE_DOWNLOAD_MANUALS', 'displayDownloadManuals.php');
			DEFINE('PAGE_UPLOADED_FILES', 'displayUploadedFiles.php');
			DEFINE('PAGE_DOWNLOAD_CATALOGUES', 'displayDownloadCatalogues.php');
			DEFINE('PAGE_DOWNLOAD_SHEETS', 'displayDownloadInstructionSheets.php');
			DEFINE('PAGE_DOWNLOAD_CONTAINER_LISTS', 'displayDownloadContainerLists.php');
			/*
			DEFINE('PAGE_DOWNLOAD_DOCUMENTS', 'displayDocuments.php?documentType=CF');
			DEFINE('PAGE_DOWNLOAD_FILES', 'displayDocuments.php?documentType=DF');
			DEFINE('PAGE_DOWNLOAD_MANUALS', 'displayDocuments.php?documentType=MF');
			DEFINE('PAGE_UPLOADED_FILES', 'displayDocuments.php?documentType=UF');
			DEFINE('PAGE_DOWNLOAD_CATALOGUES', 'displayDocuments.php?documentType=DC');
			*/

		// STATISTICS
			DEFINE('PAGE_DISPLAY_STATISTICS', 'displayStatistics.php');
			DEFINE('PAGE_DISPLAY_ONLINE_ORDERS_STATISTICS', 'displayOnlineOrdersStatistics.php');
			DEFINE('PAGE_DISPLAY_STOCK', 'displayStocks.php');
			DEFINE('PAGE_DISPLAY_STOCK_INVENTORY', 'displayStocksInventory.php');
			DEFINE('PAGE_DISPLAY_SALES', 'displaySales.php');
			DEFINE('PAGE_DISPLAY_SALES_AMAZON', 'displaySalesAmazonStatistics.php');
			DEFINE('PAGE_DISPLAY_NEW_CUSTOMERS_Auftragslisten', 'displayNewCustomersStatistics.php');
			DEFINE('PAGE_DISPLAY_NEW_CUSTOMERS_SHOP', 'displayNewShopCustomersStatistics.php');
			DEFINE('PAGE_DISPLAY_ORDERS_RECEIPTS', 'displayOrdersReceipts.php');
			DEFINE('PAGE_DISPLAY_SALEMEN_STATISTICS', 'displaySalesmenStatistics.php');
			DEFINE('PAGE_DISPLAY_PRODUCT_STATISTICS', 'displayProductStatistics.php');
			DEFINE('PAGE_DISPLAY_KZH_PRO_PLZ', 'displayKZHproPlzStatistics.php');
			DEFINE('PAGE_DISPLAY_CUSTOMER_PURCHASES', 'displayCustomerPurchases.php');
			DEFINE('PAGE_DISPLAY_RESELLERS_SALES', 'displayResellersSales.php');
			DEFINE('PAGE_DISPLAY_SALEMEN_CUSTOMER_CARES', 'displayCustomerCares.php');

		// EXTERNAL LINKS
			DEFINE('PAGE_EXTERNAL_LINKS', 'externalLinks.php');
			
		// STOCKS
			DEFINE('PAGE_PRODUCTS_STOCK_EDIT', 'editProductStockLists.php');
			DEFINE('PAGE_PRODUCTS_STOCK_OVERVIEW', 'displayProductStockLists.php');
			DEFINE('PAGE_PRODUCTS_STOCK_CONSUMPTION', 'displayProductStockConsumption.php');
			DEFINE('PAGE_PRODUCTS_STOCK_CATEGORIES', 'editProductStockCategories.php');
			DEFINE('PAGE_PRODUCTS_STOCK_OPTIONS', 'editProductStockOptions.php');
			DEFINE('PAGE_PRODUCTS_STOCK_PRODUCTS', 'editProductStockProducts.php');
			DEFINE('PAGE_PRODUCTS_STOCK_PACKAGING_UNIT', 'editProductStockPackagingUnits.php');
			DEFINE('PAGE_DISPLAY_PRINTERS_ORDERRS', 'displayPrintersOrders.php');
			

		// MAIL
			DEFINE('PAGE_MAIL_ACCOUNT', 'https://192.168.2.58/');
			DEFINE('PAGE_MAIL_ACCOUNT_LOCAL', 'https://192.168.2.58/');
			DEFINE('PAGE_MAIL_ACCOUNT_GLOBAL', 'https://burhan-ctr.no-ip.biz/');
			DEFINE('PAGE_MAIL_ACCOUNT_HOSTER_1', 'https://webmail-alfa3062.alfahosting-server.de/');
			DEFINE('PAGE_MAIL_ACCOUNT_HOSTER_2', 'http://alfa3062.alfahosting-server.de/poplogin/');
			DEFINE('PAGE_MAIL_ACCOUNT_HOSTER_3', 'http://www.burhan-ctr.de/mail/');
			DEFINE('PAGE_MAIL_ACCOUNT_HOSTER_4', 'http://mail.burhan-ctr.de/');

		// TOOLS
			DEFINE('PAGE_CREATE_STATIONARY', 'createStationary.php');
			DEFINE('PAGE_CREATE_BACKUP', 'createTotalBackUp.php');
			DEFINE('PAGE_CLOUD_BACKUP', 'displayCloudBackUp.php');
			DEFINE('PAGE_ONLINE_BACKUP', 'https://www.magentacloud.de/share/icryd1qrzr#login');
			DEFINE('PAGE_EXPORT_ORDERS', 'exportOrders.php');
			DEFINE('PAGE_CODE_GENERATOR', 'scriptPackages/EAN-CODE/');
			DEFINE('PAGE_IBAN_GENERATOR', 'http://www.iban-rechner.de/');
			DEFINE('PAGE_CALCULATOR', 'displayCalculator.php');
			DEFINE('PAGE_ONLINE_USERS', 'displayOnlineUsers.php');
			DEFINE('PAGE_IMPORT_DATAS', 'importDatas.php');
			DEFINE('PAGE_IMPORT_SHOP_TABLES', 'importShopTables.php');
			DEFINE('PAGE_EXPORT_DATAS', 'exportDatas.php');
			DEFINE('PAGE_EXPORT_CUSTOMERS_PER_ZIPCODES_DATAS', 'exportCustomerDatas.php');
			DEFINE('PAGE_EXPORT_CUSTOMER_MAILS', 'exportCustomerMails.php');
			DEFINE('PAGE_EXPORT_SHOP_CUSTOMERS_NO_ORDER_PER_ZIPCODES_DATAS', 'exportShopCustomerNoOrderDatas.php');
			DEFINE('PAGE_GENERATE_SALESMAN_REPORTS', 'generateSalesmanReports.php');
			DEFINE('PAGE_UPDATE_ONLINE_CUSTOMER_NUMBERS', 'updateOnlineCustomerNumbers.php');
			DEFINE('PAGE_UPDATE_ACQUISITION_CUSTOMER_NUMBERS', 'updateAcquisitionCustomerNumbers.php');
			DEFINE('PAGE_EXPORT_INVOICES', 'exportInvoices.php');
			DEFINE('PAGE_EDIT_INTERNAL_MESSAGES', 'editInternalMessages.php');
			DEFINE('PAGE_DISPLAY_MESSAGES', 'displayMessages.php');
			DEFINE('PAGE_SEARCH_TRACKING', 'displayDeliveryTracking.php');
			DEFINE('PAGE_DELIVERY_HISTORY', 'displayDeliveryHistory.php');
			DEFINE('PAGE_CONTAINER_LISTS', 'displayContainerLists.php');
			DEFINE('PAGE_EXPORT_TAXES_DATA', 'exportTaxAccountantData.php');
			DEFINE('PAGE_REVISE_DOCUMENT_DATAS', 'reviseDocumentDatas.php');
			DEFINE('PAGE_FIND_DUPLICATE_CUSTOMERS', 'findDuplicateCustomers.php');
			DEFINE('PAGE_DISPLAY_MAILS', 'displaySendedMails.php');
			DEFINE('PAGE_LOCALIZE_IP', 'http://www.ip-adress.com/ip_lokalisieren/');
			DEFINE('PAGE_DISPLAY_SHOP_CUSTUMER', 'http://www.burhan-ctr.de/admin/customers.php?cID={###SHOP_CUSTOMER_ID###}&amp;action=edit');
			DEFINE('PAGE_CREATE_PRINT_PRODUCTION_FILES', 'createPrintProductionFile.php');
			DEFINE('PAGE_WRITE_LETTER', 'writeLetter.php');
			DEFINE('PAGE_COMPRESS_JS', 'http://jscompress.com/');
			DEFINE('PAGE_CHECK_VAT_ID', 'https://evatr.bff-online.de/');
			DEFINE('PAGE_EXPORT_FACTORING', 'exportFactoring.php');
			DEFINE('PAGE_EXPORT_FACTORING_MISSING_CUSTOMERS', 'exportFactoringMissingCustomers.php');
			DEFINE('PAGE_IMPORT_FACTORING', 'importFactoring.php');
			DEFINE('URL_FACTORING_PORTAL', 'https://factoring-portal.universal-factoring.de');


		// SETTINGS & CONFIGURATION
			DEFINE('PAGE_CONFIG_FTP', 'editConfigFtp.php');
			DEFINE('PAGE_CONFIG_MAIL', 'editConfigMail.php');
			DEFINE('PAGE_CONFIG_DB', 'editConfigDB.php');
			DEFINE('PAGE_CONFIG_PARAMS', 'editConfigParams.php');
			DEFINE('PAGE_CONFIG_BASIC', 'editConfigBasic.php');
			DEFINE('PAGE_EDIT_USER', 'editUser.php');
			DEFINE('PAGE_EDIT_EAN_NUMBERS', 'editEanNumbers.php');
			// DEFINE('PAGE_BANK_DATAS', 'editConfigBank.php');

		// SYSTEM
			DEFINE('PAGE_PHP_INFO', 'phpInfo.php');
			DEFINE('PAGE_SERVER_INFO', 'sysInfo.php');
			DEFINE('PAGE_DB_INFO', 'dbInfo.php');
			DEFINE('PAGE_DB_LOG_FILES', 'editLogFiles.php');

			DEFINE('PATH_ONLINE_PROVIDER', 'http://alfahosting.de/kunden/index.php/Benutzer:Login');
			if(!defined("DB_NAME"))
				DEFINE('DB_NAME', 'burhanctrwawi');
			if(!defined("DB_NAME_EXTERN_SHOP"))
				DEFINE('DB_NAME_EXTERN_SHOP', 'burhanctrwawi');
			DEFINE('PATH_PHPMYADMIN_LOCAL_DIREKT_LINK', 'db_structure.php?db=' . DB_NAME . '&amp;table=&amp;server=1');
			DEFINE('PATH_PHPMYADMIN_LOCAL', '/phpmyadmin/' . PATH_PHPMYADMIN_LOCAL_DIREKT_LINK);
			DEFINE('PATH_PHPMYADMIN_EXTERN_DIREKT_LINK', 'http://myadmin-alfa3062.alfahosting-server.de/db_structure.php?db=' . DB_NAME_EXTERN_SHOP . '&amp;table=&amp;server=1');
			DEFINE('PATH_PHPMYADMIN_EXTERN', PATH_PHPMYADMIN_EXTERN_DIREKT_LINK);

			// DEFINE('PAGE_DISPLAY_PHPMYADMIN_LOCAL', 'displayPhpMyAdmin.php');
			DEFINE('PAGE_DISPLAY_PHPMYADMIN_LOCAL', PATH_PHPMYADMIN_LOCAL);
			DEFINE('PAGE_DISPLAY_PHPMYADMIN_EXTERN', PATH_PHPMYADMIN_EXTERN);
			DEFINE('PAGE_DISPLAY_PHPTERM', 'displayPhpTerm.php');
			DEFINE('PAGE_CLEAN_SYSTEM', 'cleanSystem.php');
			DEFINE('PAGE_DISPLAY_TREESIZE', 'displayTreesize.php');
			DEFINE('PAGE_DISPLAY_FILE_EXPLORER', 'displayFileExplorer.php');
			DEFINE('PAGE_DISPLAY_FILE_MANAGER', 'displayFileManager.php');
			DEFINE('PAGE_APACHE_MANUAL', 'documents_common/documentsManuals/apacheManual_httpd-docs-2.0.63.en.pdf');

		// SEO-TOOLS
			DEFINE('PAGE_SERVER_STATISTIC', 'http://alfa3062.alfahosting-server.de/user/web23/awstats.php');
			DEFINE('PAGE_SERVER_LOGS', 'displayServerLogs.php');
			DEFINE('PAGE_GOOGLE_INDEX', 'http://www.google.de/search?q=site%3A' . preg_replace("/(http|https):\/\//", "", constant("PATH_ONLINE_SHOP_" . strtoupper(MANDATOR))));
			DEFINE('PAGE_GOOGLE_ANALYTICS', 'http://www.google.com/intl/de/analytics/');
			DEFINE('PAGE_GOOGLE_WEBMASTERTOOLS', 'http://www.google.com/webmasters/');
			DEFINE('PAGE_BING_INDEX', 'http://www.bing.com/search?q=site%3A' .  preg_replace("/(http|https):\/\//", "", constant("PATH_ONLINE_SHOP_" . strtoupper(MANDATOR))));
			DEFINE('PAGE_BING_WEBMASTERTOOLS', 'http://www.bing.com/toolbox/webmaster/');
			DEFINE('PAGE_YAHOO_INDEX', 'http://de.search.yahoo.com/search?p=site%3A' .  preg_replace("/(http|https):\/\//", "", constant("PATH_ONLINE_SHOP_" . strtoupper(MANDATOR))));
			DEFINE('PAGE_SEITWERT_WEBPAGE_ANALYTICS', 'http://www.seitwert.de/');
			DEFINE('PAGE_SITESENTRAL_WEBPAGE_FIND_DUPLICATE_CONTENT', 'http://sitesentral.com/' . preg_replace("/(http|https):\/\//", "", constant("PATH_ONLINE_SHOP_" . strtoupper(MANDATOR))));
			DEFINE('PAGE_WEBPAGE_ANALYTICS', 'http://www.seitwert.de/');
			DEFINE('DISPLAY_SHOP_VISITORS', 'displayShopCounters.php');

			DEFINE('PAGE_COPYSCAPE_WEBPAGE_FIND_DUPLICATE_CONTENT', 'http://copyscape.com/?q=' . preg_replace("/(http|https):\/\//", "", constant("PATH_ONLINE_SHOP_" . strtoupper(MANDATOR))));
			DEFINE('PAGE_CHECK_DUPLICATE_CONTENT_1', 'http://www.copyscape.com/');
			DEFINE('PAGE_CHECK_DUPLICATE_CONTENT_2', 'http://www.seoreviewtools.com/duplicate-content-checker/');
			DEFINE('PAGE_CHECK_DUPLICATE_CONTENT_3', 'http://www.siteliner.com/');
			DEFINE('PAGE_CHECK_DUPLICATE_CONTENT_4', 'http://www.plagspotter.com/');
			DEFINE('PAGE_CHECK_DUPLICATE_CONTENT_5', 'http://smallseotools.com/plagiarism-checker/');

		// SEARCHES
			DEFINE('PATH_SEARCH', 'redirectSearches.php');

		// HELP
			DEFINE('PAGE_HELP', 'displayHelp.php');


		// INCLUDES LAYOUT & JAVASCRIPT
			DEFINE('FILE_FAVICON', 'favicon.ico');

			/*
			if(INCLUDE_CSS_MIN && !INCLUDE_CSS_GZIP) {
				$fileCssMin = '.min';
				$fileCssGzip = '';
			}
			else if(!INCLUDE_CSS_MIN && INCLUDE_CSS_GZIP) {
				$fileCssMin = '';
				$fileCssGzip = '';
			}
			else if(INCLUDE_CSS_MIN && INCLUDE_CSS_GZIP) {
				$fileCssMin = '.min';
				$fileCssGzip = '';
			}
			else {
				$fileCssMin = '';
				$fileCssGzip = '';
			}

			if(INCLUDE_JS_MIN && !INCLUDE_JS_GZIP) {
				$fileJsMin = '.min';
				$fileJsGzip = '';
			}
			else if(!INCLUDE_JS_MIN && INCLUDE_JS_GZIP) {
				$fileJsMin = '';
				$fileJsGzip = '';
			}
			else if(INCLUDE_JS_MIN && INCLUDE_JS_GZIP) {
				$fileJsMin = '.min';
				$fileJsGzip = '';
			}
			else {
				$fileJsMin = '';
				$fileJsGzip = '';
			}

			// if(DEBUG) {
			if(1) {
				$fileCssMin = '';
				$fileCssGzip = '';
				$fileJsMin = '';
				$fileJsGzip = '';
			}

			DEFINE('FILE_FUNCTIONS_JS', 'js/functions' . $fileJsMin . '.js' . $fileJsGzip);
			DEFINE('FILE_LAYOUT_CSS', 'css/layout' . $fileCssMin . '.css' . $fileCssGzip);
			DEFINE('FILE_PRINT_CSS', 'css/print' . $fileCssMin . '.css' . $fileCssGzip);
			DEFINE('FILE_BORDERS_CSS', 'css/borders' . $fileCssMin . '.css' . $fileCssGzip);
			*/

			$fileCssMin = '';
			$fileCssGzip = '';
			$fileJsMin = '';
			$fileJsGzip = '';

			if(isset($_COOKIE["isAdmin"]) && $_COOKIE["isAdmin"] == '1'){
				DEFINE('FILE_FUNCTIONS_JS', 'js/functions.min.js');
				#DEFINE('FILE_FUNCTIONS_JS', 'js/functions.min.js');
				#DEFINE('FILE_FUNCTIONS_JS', 'js/functions.js');
			}
			else {
				DEFINE('FILE_FUNCTIONS_JS', 'js/functions.min.js');
			}			
			DEFINE('FILE_LAYOUT_CSS', 'css/layout.min.css');
			DEFINE('FILE_PRINT_CSS', 'css/print.min.css');
			DEFINE('FILE_BORDERS_CSS', 'css/borders.min.css');

		// ICONS
			DEFINE('PATH_ICONS_MENUE_TOP', 'layout/menueIcons/menueSidebar/');
			DEFINE('PATH_ICONS_MENUE_TITLES', 'layout/menueIcons/menueSidebar/');
			DEFINE('PATH_ICONS_MENUE_SIDEBAR', 'layout/menueIcons/menueSidebar/');
			DEFINE('PATH_ICONS_MENUE_QUICKLINKS', 'layout/menueIcons/menueQuicklinks/');

		// XML
			#DEFINE('PATH_XML_FILE_SALESMEN', '/' . DIR_INSTALLATION_NAME . DIRECTORY_XML_FILES . 'salesmenDatas.xml');
			DEFINE('PATH_XML_FILE_SALESMEN', DIRECTORY_XML_FILES . 'salesmenDatas.xml');
			DEFINE('FILE_XML_ZIPCODES_DOUBLE', 'documents_common/xml/zipCodesDoubleDatas.xml');


?>