<?php
	DEFINE(USERID, $_SESSION["usersID"]);

	if(!defined('INCLUDE_CSS_MIN')) {
		DEFINE('INCLUDE_CSS_MIN', false);
	}
	if(!defined('INCLUDE_CSS_GZIP')) {
		DEFINE('INCLUDE_CSS_GZIP', false);
	}
	if(!defined('INCLUDE_JS_MIN')) {
		DEFINE('INCLUDE_JS_MIN', false);
	}
	if(!defined('INCLUDE_JS_GZIP')) {
		DEFINE('INCLUDE_JS_GZIP', false);
	}
	### DB_LOKAL / Auftragslisten DB
	DEFINE('DB_NAME', 'burhanctrAuftragslisten');
	DEFINE('DB_HOST', 'localhost');
	DEFINE('DB_PORT', '80');
	DEFINE('DB_USER', 'burhan');
	DEFINE('DB_PASSWORD', 'burhan');

	// TIMEZONE / PHP_INI
	if(!defined("SET_DATE_DEFAULT_TIMEZONE")) { DEFINE('SET_DATE_DEFAULT_TIMEZONE', 'Europe/Berlin'); }
	if(!defined("LC_ALL")) { DEFINE('LC_ALL', '"de_DE@euro", "de_DE", "de", "ge"'); }

	// INI
	if(!defined("TRACK_ERRORS")) { DEFINE('TRACK_ERRORS', '1'); }
	if(!defined("AUTO_DETECT_LINE_ENDINGS")) { DEFINE('AUTO_DETECT_LINE_ENDINGS', false); }
	if(!defined("DEFAULT_SOCKET_TIMEOUT")) { DEFINE('DEFAULT_SOCKET_TIMEOUT', '7200'); }
	if(!defined("MEMORY_LIMIT")) { DEFINE('MEMORY_LIMIT', '256M'); }
	if(!defined("SENDMAIL_FROM")) { DEFINE('SENDMAIL_FROM', ''); }
	if(!defined("SESSION.GC_MAXLIFETIME")) { DEFINE('SESSION.GC_MAXLIFETIME', '7200'); }
	if(!defined("SMTP")) { DEFINE('SMTP', ''); }
	if(!defined("SMTP_PORT")) { DEFINE('SMTP_PORT', ''); }
	if(!defined("ZLIB.OUTPUT_COMPRESSION")) { DEFINE('ZLIB.OUTPUT_COMPRESSION', '5'); }
	if(!defined("SET_TIME_LIMIT")) { DEFINE('SET_TIME_LIMIT', '0'); }
	if(!defined("POST_MAX_SIZE")) { DEFINE('POST_MAX_SIZE', '5000M'); }
	if(!defined("UPLOAD_MAX_FILESIZE")) { DEFINE('UPLOAD_MAX_FILESIZE', '5000M'); }
	if(!defined("ERROR_REPORTING")) { DEFINE('ERROR_REPORTING', 0); }

	date_default_timezone_set(SET_DATE_DEFAULT_TIMEZONE);
	setlocale(LC_ALL, constant("LC_ALL"));
	setlocale(LC_TIME, constant("LC_ALL"));
	ini_set('memory_limit', MEMORY_LIMIT);
	set_time_limit(SET_TIME_LIMIT);
	ini_set('max_execution_time', SET_TIME_LIMIT);
	ini_set('post_max_size', POST_MAX_SIZE);
	ini_set('upload_max_filesize', UPLOAD_MAX_FILESIZE);
	// error_reporting(constant("ERROR_REPORTING"));
	if(preg_replace("/[0-9]/", "", constant("ERROR_REPORTING")) != "") {
		error_reporting(constant(constant("ERROR_REPORTING")));
	}
	else {
		error_reporting(intval(constant("ERROR_REPORTING")));
	}

	// BASIC DOCUMENT NUMBERS
	DEFINE('BASIC_NUMBER_AN', BASIC_NUMBER_OFFER);
	DEFINE('BASIC_NUMBER_AB', BASIC_NUMBER_CONFIRMATION);
	DEFINE('BASIC_NUMBER_LS', BASIC_NUMBER_DELIVERY);
	DEFINE('BASIC_NUMBER_RE', BASIC_NUMBER_INVOICE);
	DEFINE('BASIC_NUMBER_GU', BASIC_NUMBER_CREDIT);
	DEFINE('BASIC_NUMBER_MA', BASIC_NUMBER_REMINDER);
	DEFINE('BASIC_NUMBER_M1', BASIC_NUMBER_FIRSTDEMAND);
	DEFINE('BASIC_NUMBER_M2', BASIC_NUMBER_SECONDDEMAND);
	DEFINE('BASIC_NUMBER_BR', BASIC_NUMBER_LETTER);

	// DEADLINES
	DEFINE('PAYMENT_DEADLINE_MA', 7); // DAYS
	DEFINE('PAYMENT_DEADLINE_M1', 7); // DAYS
	DEFINE('PAYMENT_DEADLINE_M2', 7); // DAYS
	DEFINE('BINDING_DEADLINE_AN', 1); // DAYS
	DEFINE('PAYMENT_DEADLINE_RE_10TOA', 10); // DAYS
	DEFINE('PAYMENT_DEADLINE_AB_VK', 28); // DAYS
	DEFINE('PAYMENT_DEADLINE_RE_14T', 14); // DAYS
	DEFINE('PAYMENT_DEADLINE_RE_2PCSK10T30TN', 30); // DAYS
	DEFINE('PAYMENT_DEADLINE_RE_2PCSK8T30TN', 30); // DAYS
	DEFINE('PAYMENT_DEADLINE_RE_DEFAULT', 7); // DAYS

	DEFINE('PAYMENT_SKONTO', 2); // %

	// -------------------------------------------------------------------
	if (strtoupper(substr(PHP_OS, 0, 3)) === 'WIN') {
		// echo 'This is a server using Windows!';
		DEFINE('APPLICATION_OS', 'WIN');

	}
	else {
		// echo 'This is a server not using Windows!';
		DEFINE('APPLICATION_OS', 'NO_WIN');
	}

	#DEFINE('NETWORK_PRINTER_NAME', 'ES2232(PCL)');
	#DEFINE('NETWORK_PRINTER_PORT', '9100');
	#DEFINE('NETWORK_PRINTER_HOST', '192.168.2.152');

	if(APPLICATION_OS != 'WIN') {
		DEFINE('TRAP_ERROR', '2>&1');
		DEFINE('ZIP_PATH', 'zip');
		DEFINE('UNZIP_PATH', 'unzip');
		DEFINE('TAR_PATH', 'tar');
		DEFINE('MYSQLDUMP_PATH', 'mysqldump');
		DEFINE('mysqli_PATH', 'mysql');
	}
	else {
		/*
		DEFINE('TRAP_ERROR', '');
		DEFINE('ZIP_PATH', 'C:/Program Files/7-Zip/7z.exe');
		DEFINE('TAR_PATH', 'C:/Program Files/7-Zip/7z.exe');
		DEFINE('MYSQLDUMP_PATH', 'C:/Auftragslisten_SERVER/mysql/bin/mysqldump.exe');
		*/
		DEFINE('TRAP_ERROR', '2>&1');
		DEFINE('ZIP_PATH', 'C:\Programme\7-Zip\7z.exe');
		DEFINE('UNZIP_PATH', 'C:\Programme\7-Zip\7z.exe');
		DEFINE('TAR_PATH', 'C:\Programme\7-Zip\7z.exe');
		if(preg_match("/127.0.0.1/", $_SERVER["HTTP_HOST"])) {
			DEFINE('SERVER_NAME', 'WEB_SERVER');
		}
		else {
			DEFINE('SERVER_NAME', 'Auftragslisten_SERVER');
		}
		DEFINE('MYSQLDUMP_PATH', 'C:\\' . SERVER_NAME .'\mysql\bin\mysqldump.exe');
		DEFINE('mysqli_PATH', 'C:\\' . SERVER_NAME .'\mysql\bin\mysql.exe');
	}

	// -------------------------------------------------------------------
?>