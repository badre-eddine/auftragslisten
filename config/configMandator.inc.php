<?php
// MANDATOR
	@session_start();

	if(isset($_POST["mandatorSwitch"]) && $_POST["mandatorSwitch"] != '') {
		DEFINE("MANDATOR", $_POST["mandatorSwitch"]);
		$cookieNameMandator = "mandator";
		$cookieValueMandator = $_POST["mandatorSwitch"];
		$cookieTime = time()+60*60*24*30;
		setcookie($cookieNameMandator, $cookieValueMandator, $cookieTime);
		$_SESSION["mandator"] = $_POST["mandatorSwitch"];
		header('location: ./');
	}
	else if(isset($_POST["mandator"]) && $_POST["mandator"] != '') {
		DEFINE("MANDATOR", $_POST["mandator"]);
	}
	else if(isset($_SESSION["mandator"]) && $_SESSION["mandator"] != ''){
		DEFINE("MANDATOR", $_SESSION["mandator"]);
	}
	else if(isset($_COOKIE["mandator"]) && $_COOKIE["mandator"] != ''){
		DEFINE("MANDATOR", $_COOKIE["mandator"]);
	}

	// if(!defined(MANDATOR)) {
	if($_SERVER["HTTP_REFERER"] == ''){
		session_unset();
		header('location: ./?isMandator=false&redirectURL=' . $_SERVER["REQUEST_URI"]);
		exit;
	}
?>