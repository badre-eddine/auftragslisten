<?php
	### FILES_TYPES
	if(!defined('INCLUDE_CSS_MIN')) {
		DEFINE('INCLUDE_CSS_MIN', false);
	}
	if(!defined('INCLUDE_CSS_GZIP')) {
		DEFINE('INCLUDE_CSS_GZIP', false);
	}
	if(!defined('INCLUDE_JS_MIN')) {
		DEFINE('INCLUDE_JS_MIN', false);
	}
	if(!defined('INCLUDE_JS_GZIP')) {
		DEFINE('INCLUDE_JS_GZIP', false);
	}
	### PHP_INI
	if(!defined('SET_DATE_DEFAULT_TIMEZONE')) {
		DEFINE('SET_DATE_DEFAULT_TIMEZONE', 'Europe/Berlin');
	}
	if(!defined('LC_ALL')) {
		DEFINE('LC_ALL', '"de_DE@euro", "de_DE", "de", "ge"');
	}

	DEFINE('DB_NAME', 'burhanctrwawi');
	DEFINE('DB_HOST', 'localhost');
	DEFINE('DB_PORT', '3306');
	DEFINE('DB_USER', 'root');
	DEFINE('DB_PASSWORD', '');
?>