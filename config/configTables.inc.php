<?php
	//  $prefixTablesMandator = 'bctr_';
	$prefixTablesMandator = MANDATOR . '_';
	$prefixTablesCommon = 'common_';

	DEFINE('TABLE_SETTINGS_IMPORT_FILES', $prefixTablesCommon . 'settingsImportFiles');

	DEFINE('TABLE_ADDITIONAL_COSTS', $prefixTablesMandator . 'additionalCosts');
	DEFINE('TABLE_AGENTS', $prefixTablesMandator . 'agents');

	DEFINE('TABLE_BANK_ACCOUNT_TYPES', $prefixTablesMandator . 'bankAccountTypes');
	DEFINE('TABLE_BANK_DATAS', $prefixTablesCommon . 'bankDatas');

	DEFINE('TABLE_CALENDAR', $prefixTablesMandator . 'calendar');

	DEFINE('TABLE_CONFIG_DATAS', $prefixTablesMandator . 'configDatas');
	DEFINE('TABLE_CONFIG_DATAS_ALL_MANDATORIES', '{###MANDATORY###}_' . 'configDatas');
	DEFINE('TABLE_COUNTRIES', $prefixTablesCommon . 'countries');
	DEFINE('TABLE_COUNTRIES_UNIONS', $prefixTablesCommon . 'countriesUnions');
	DEFINE('TABLE_CURRENCIES', $prefixTablesCommon . 'currencies');

	DEFINE('TABLE_CREATED_DOCUMENTS', $prefixTablesMandator . 'createdDocuments');
	DEFINE('TABLE_CREATED_DOCUMENTS_TYPES', $prefixTablesCommon . 'createdDocumentsTypes');

	#DEFINE('TABLE_CUSTOMERS', $prefixTablesMandator . 'customers');
	DEFINE('TABLE_CUSTOMERS', $prefixTablesCommon . 'customers');
	DEFINE('TABLE_CUSTOMERS_ACTIVITY_STATUS', $prefixTablesCommon . 'customerActivityStatus');
	DEFINE('TABLE_CUSTOMER_DUPLICATES', $prefixTablesCommon . 'customerDuplicates');
	DEFINE('TABLE_CUSTOMERS_TO_CUSTOMERS', $prefixTablesCommon . 'customersToCustomers');
	DEFINE('TABLE_CUSTOMERS_CONTACTS', $prefixTablesCommon . 'customersContacts');
	DEFINE('TABLE_CUSTOMERS_CALENDAR', $prefixTablesCommon . 'customersCalendar');
	DEFINE('TABLE_CUSTOMERS_RELATIONS', $prefixTablesCommon . 'customersRelations');
	DEFINE('TABLE_CUSTOMER_GROUPS', $prefixTablesCommon . 'customerGroups');
	DEFINE('TABLE_CUSTOMER_GROUPS_PRODUCTS', $prefixTablesCommon . 'customerGroupsProducts');
	DEFINE('TABLE_CUSTOMERS_PRICE_LISTS', $prefixTablesCommon . 'customersPriceLists');
	DEFINE('TABLE_CUSTOMERS_PRICE_LISTS_PRODUCTS_DATA', $prefixTablesCommon . 'customersPriceListsProductsData');
	DEFINE('TABLE_CUSTOMER_TYPES', $prefixTablesCommon . 'customerTypes');
	DEFINE('TABLE_CUSTOMERS_PHONE_MARKETING', $prefixTablesCommon . 'customersphonemarketing');
	DEFINE('TABLE_POSSIBLE_CUSTOMER_NUMBERS', $prefixTablesCommon . 'possiblecustomernumbers');
	DEFINE('TABLE_SALUTATION_TYPES', $prefixTablesCommon . 'salutationTypes');
	DEFINE('TABLE_OFFICIAL_HOLIDAYS', $prefixTablesCommon . 'holidays');
	DEFINE('TABLE_CONTACT_TYPES', $prefixTablesCommon . 'contactTypes');
	DEFINE('TABLE_IMPORT_CUSTOMERS_GROUP_MEMPERS', $prefixTablesCommon . 'importCustomerGroupMembers{###GROUP_NAME###}');
	DEFINE('TABLE_CUSTOMER_GROUPS_RELATION_DATA', $prefixTablesCommon . 'customerGroupsRelationData');

	DEFINE('TABLE_PRICE_LIST_DOMING', $prefixTablesCommon . 'priceListDoming');

	DEFINE('TABLE_MANDATORIES', $prefixTablesCommon . 'mandatories');

	DEFINE('TABLE_MESSAGES', $prefixTablesCommon . 'messages');
	DEFINE('TABLE_FINANCE_OFFICES', $prefixTablesCommon . 'financeOffices');
	DEFINE('TABLE_HEALTH_INSURANCES', $prefixTablesCommon . 'healthInsurances');

	DEFINE('TABLE_ORDERS', $prefixTablesCommon . 'ordersProcess');	
	DEFINE('TABLE_PRODUCTIONS_TRANSFER', $prefixTablesCommon . 'ordersProductionsTransfer');
	DEFINE('TABLE_CUSTOMERS_ORDERS_INTERVALS', $prefixTablesMandator . 'customersOrdersIntervals');
	DEFINE('TABLE_ORDER_CATEGORIES', $prefixTablesCommon . 'productCategories');

	// DEFINE('TABLE_ORDERS_', $prefixTablesMandator . 'orders');
	DEFINE('TABLE_ORDERS_SOURCE_TYPES', $prefixTablesCommon . 'ordersSourceTypes');
	DEFINE('TABLE_ORDER_STATUS_TYPES', $prefixTablesCommon . 'orderStatusTypes');
	DEFINE('TABLE_ORDER_STATUS_TYPES_EXTERN', $prefixTablesCommon . 'orderStatusTypesExtern');
	DEFINE('TABLE_ORDERS_TO_DOCUMENTS', $prefixTablesMandator . 'ordersToDocuments');
	DEFINE('TABLE_DOCUMENTS_TO_DOCUMENTS', $prefixTablesMandator . 'documentsToDocuments');
	DEFINE('TABLE_RELATED_DOCUMENTS', $prefixTablesMandator . 'relatedDocuments');
	DEFINE('TABLE_ORDER_TYPES', $prefixTablesCommon . 'orderTypes');
	
	DEFINE('TABLE_ORDER_OFFERS', $prefixTablesMandator . 'orderOffers');
	DEFINE('TABLE_ORDER_OFFERS_DETAILS', $prefixTablesMandator . 'orderOffersDetails');
	
	DEFINE('TABLE_ORDER_CONFIRMATIONS', $prefixTablesMandator . 'orderConfirmations');
	DEFINE('TABLE_ORDER_CONFIRMATIONS_DETAILS', $prefixTablesMandator . 'orderConfirmationsDetails');

	DEFINE('TABLE_ORDER_REKLAMATIONS', $prefixTablesMandator . 'OrderReklamation');
	DEFINE('TABLE_ORDER_REKLAMATIONS_DETAILS', $prefixTablesMandator . 'OrderReklamationDetails');
	
	DEFINE('TABLE_ORDER_DELIVERIES', $prefixTablesMandator . 'orderDeliveries');
	DEFINE('TABLE_ORDER_DELIVERIES_DETAILS', $prefixTablesMandator . 'orderDeliveriesDetails');
	
	DEFINE('TABLE_ORDER_INVOICES', $prefixTablesMandator . 'orderInvoices');
	DEFINE('TABLE_ORDER_INVOICES_DETAILS', $prefixTablesMandator . 'orderInvoicesDetails');
	
	DEFINE('TABLE_ORDER_CREDITS', $prefixTablesMandator . 'orderCredits');
	DEFINE('TABLE_ORDER_CREDITS_DETAILS', $prefixTablesMandator . 'orderCreditsDetails');
	
	DEFINE('TABLE_ORDER_REMINDERS', $prefixTablesMandator . 'orderReminders');
	DEFINE('TABLE_ORDER_REMINDERS_DETAILS', $prefixTablesMandator . 'orderRemindersDetails');

	DEFINE('TABLE_ORDER_FIRST_DEMANDS', $prefixTablesMandator . 'orderFirstDemands');
	DEFINE('TABLE_ORDER_FIRST_DEMANDS_DETAILS', $prefixTablesMandator . 'orderFirstDemandsDetails');

	DEFINE('TABLE_ORDER_SECOND_DEMANDS', $prefixTablesMandator . 'orderSecondDemands');
	DEFINE('TABLE_ORDER_SECOND_DEMANDS_DETAILS', $prefixTablesMandator . 'orderSecondDemandsDetails');

	DEFINE('TABLE_ORDER_THIRD_DEMANDS', $prefixTablesMandator . 'orderThirdDemands');
	DEFINE('TABLE_ORDER_THIRD_DEMANDS_DETAILS', $prefixTablesMandator . 'orderThirdDemandsDetails');


	DEFINE('TABLE_ORDER_LETTERS', $prefixTablesMandator . 'orderLetters');
	DEFINE('TABLE_ORDER_LETTERS_DETAILS', $prefixTablesMandator . 'orderLettersDetails');
	
	DEFINE('TABLE_ORDER_INVOICE_PAYMENTS', $prefixTablesMandator . 'orderInvoicePayments');
	DEFINE('TABLE_ORDER_CONFIRMATION_PAYMENTS', $prefixTablesMandator . 'orderConfirmationPayments');
	
	DEFINE('TABLE_FINANCIAL_PAYMENT_STATISTICS',  $prefixTablesMandator . 'financialPaymentStatistics');

	DEFINE('TABLE_SALESMEN_PROVISIONS', $prefixTablesCommon . 'salesmenProvisions');
	DEFINE('TABLE_SALESMEN_PROVISIONS_DETAILS', $prefixTablesCommon . 'salesmenProvisionsDetails');
	DEFINE('TABLE_SALESMEN_STOCKS', $prefixTablesMandator . 'salesmenStocks');

	DEFINE('TABLE_GROUPS_PROVISIONS', $prefixTablesCommon . 'groupsProvisions');
	DEFINE('TABLE_GROUPS_PROVISIONS_DETAILS', $prefixTablesCommon . 'groupsProvisionsDetails');

	DEFINE('TABLE_PAYMENT_TYPES', $prefixTablesCommon . 'paymentTypes');
	DEFINE('TABLE_PAYMENT_CONDITIONS', $prefixTablesCommon . 'paymentConditions');
	DEFINE('TABLE_PAYMENT_STATUS_TYPES', $prefixTablesCommon . 'paymentStatusTypes');

	DEFINE('TABLE_PRINTERS', $prefixTablesMandator . 'printers');
	DEFINE('TABLE_PRINT_TYPES', $prefixTablesCommon . 'printTypes');
	DEFINE('TABLE_PERSONNEL', $prefixTablesMandator . 'personnel');
	DEFINE('TABLE_PERSONNEL_TYPES', $prefixTablesCommon . 'personnelTypes');
	DEFINE('TABLE_PERSONNEL_CALENDAR', $prefixTablesMandator . 'personnelCalendar');
	DEFINE('TABLE_PERSONNEL_MAIL_ACCOUNTS', $prefixTablesCommon . 'personnelMailAccounts');
	// DEFINE('TABLE_PROCEDURES', $prefixTablesMandator . 'procedures');
	DEFINE('TABLE_SUPPLIERS', $prefixTablesCommon . 'suppliers');
	DEFINE('TABLE_SUPPLIER_GROUPS', $prefixTablesMandator . 'supplierGroups');
	DEFINE('TABLE_SUPPLIER_INVOICES', $prefixTablesMandator . 'supplierInvoices');

	DEFINE('TABLE_SALESMEN', $prefixTablesMandator . 'salesmen');
	DEFINE('TABLE_STOCK_ITEMS', $prefixTablesCommon . 'stockItems');
	DEFINE('TABLE_SUPPLIER_TYPES', $prefixTablesCommon . 'supplierTypes');
	DEFINE('TABLE_SUPPLIER_ORDERS', $prefixTablesCommon . 'supplierOrders');

	DEFINE('TABLE_USERS', $prefixTablesCommon . 'users');
	DEFINE('TABLE_USERS_ONLINE', $prefixTablesCommon . 'usersOnline');
	DEFINE('TABLE_USERS_KEYLOG', $prefixTablesCommon . 'keylog');
	DEFINE('TABLE_ACQUISATION_USERS_DRIVERS_LOG', $prefixTablesMandator . 'usersDriversLogs');
	DEFINE('TABLE_ACQUISATION_USERS_DRIVERS_LOG_DETAILS', $prefixTablesMandator . 'usersDriversLogsDetails');
	DEFINE('TABLE_ACQUISATION_MESSAGES', $prefixTablesCommon . 'messages');

	DEFINE('TABLE_ZIPCODES_CITIES', $prefixTablesCommon . 'zipcodesCities');
	DEFINE('TABLE_CITIES_PHONECODE', $prefixTablesCommon . 'zipCodesPhoneCodes');
	DEFINE('TABLE_ZIPCODES_DOUBLE', $prefixTablesCommon . 'zipcodesDouble');
	DEFINE('TABLE_PHONE_AREA_CODES', $prefixTablesCommon . 'phoneareacodes');


	DEFINE('TABLE_DELIVERY_DATAS', $prefixTablesCommon . 'deliveryDatas');
	DEFINE('TABLE_DELIVERY_DATAS_STATUS_IMPORT', $prefixTablesCommon . 'deliveryDatasStatusImport');
	DEFINE('TABLE_CONTAINER_LISTS', $prefixTablesCommon . 'containerLists');
	DEFINE('TABLE_PARCEL_INVOICES_DPD', $prefixTablesCommon . 'parcelInvoices_DPD');

	DEFINE('TABLE_GTIN_CODES', $prefixTablesCommon . 'codesGtin');
	
	DEFINE('TABLE_PRODUCT_TRANSLATIONS', $prefixTablesCommon . 'productTranslations');
	DEFINE('TABLE_STOCK_PRODUCT_DATA', $prefixTablesCommon . 'stockProductData');
	DEFINE('TABLE_STOCK_PRODUCT_DETAIL_DATA', $prefixTablesCommon . 'stockProductDetailData');
	DEFINE('TABLE_STOCK_PRODUCT_CATEGORIES', $prefixTablesCommon . 'stockProductCategories');
	DEFINE('TABLE_STOCK_PRODUCT_OPTIONS', $prefixTablesCommon . 'stockProductOptions');
	DEFINE('TABLE_STOCK_PRODUCTS', $prefixTablesCommon . 'stockProducts');
	DEFINE('TABLE_STOCK_PRODUCT_RECEIPTS', $prefixTablesCommon . 'stockProductReceipts');
	DEFINE('TABLE_STOCK_PRODUCT_USAGE', $prefixTablesCommon . 'stockProductUsage');
	DEFINE('TABLE_STOCK_PRODUCT_PRICES', $prefixTablesCommon . 'stockProductPrices');
	DEFINE('TABLE_PRODUCT_VPE', $prefixTablesCommon . 'productVPE');	
	DEFINE('TABLE_PRODUCT_PRICE_LISTS', $prefixTablesCommon . 'productPriceLists');

	// IMPORTET SHOP CATEGORIES, PRODUCTS AND PRICES
	DEFINE('LOAD_EXTERNAL_SHOP_DATAS', false);


	if(LOAD_EXTERNAL_SHOP_DATAS == true){
		DEFINE('TABLE_CATEGORIES', 'categories');
		DEFINE('TABLE_CATEGORIES_DESCRIPTION', 'categories_description');
		DEFINE('TABLE_PRODUCTS_TO_CATEGORIES', 'products_to_categories');
		DEFINE('TABLE_PRODUCTS', 'products');
		DEFINE('TABLE_PRODUCTS_VPE', 'products_vpe');
		DEFINE('TABLE_PRODUCTS_DESCRIPTION', 'products_description');
		DEFINE('TABLE_PRODUCTS_ATTRIBUTES', 'products_attributes');
		DEFINE('TABLE_PRODUCTS_OPTIONS', 'products_options');
		DEFINE('TABLE_PRODUCTS_OPTIONS_VALUES', 'products_options_values');
		DEFINE('TABLE_PERSONAL_OFFERS_BY_CUSTOMERS', 'personal_offers_by_customers_status_');
	}
	else {
		/*
		DEFINE('TABLE_CATEGORIES', $prefixTablesMandator . 'categories');
		DEFINE('TABLE_CATEGORIES_DESCRIPTION', $prefixTablesMandator . 'categories_description');
		DEFINE('TABLE_PRODUCTS_TO_CATEGORIES', $prefixTablesMandator . 'products_to_categories');
		DEFINE('TABLE_PRODUCTS', $prefixTablesMandator . 'products');
		DEFINE('TABLE_PRODUCTS_VPE', $prefixTablesMandator . 'products_vpe');
		DEFINE('TABLE_PRODUCTS_DESCRIPTION', $prefixTablesMandator . 'products_description');
		DEFINE('TABLE_PRODUCTS_ATTRIBUTES', $prefixTablesMandator . 'products_attributes');
		DEFINE('TABLE_PRODUCTS_OPTIONS', $prefixTablesMandator . 'products_options');
		DEFINE('TABLE_PRODUCTS_OPTIONS_VALUES', $prefixTablesMandator . 'products_options_values');
		DEFINE('TABLE_PERSONAL_OFFERS_BY_CUSTOMERS', $prefixTablesMandator . 'personal_offers_by_customers_status_');
		*/

		DEFINE('TABLE_CATEGORIES', $prefixTablesMandator . 'shop_categories');
		DEFINE('TABLE_CATEGORIES_DESCRIPTION', $prefixTablesMandator . 'shop_categories_description');
		DEFINE('TABLE_PRODUCTS_TO_CATEGORIES', $prefixTablesMandator . 'shop_products_to_categories');
		DEFINE('TABLE_PRODUCTS', $prefixTablesMandator . 'shop_products');
		DEFINE('TABLE_PRODUCTS_VPE', $prefixTablesMandator . 'shop_products_vpe');
		DEFINE('TABLE_PRODUCTS_DESCRIPTION', $prefixTablesMandator . 'shop_products_description');
		DEFINE('TABLE_PRODUCTS_ATTRIBUTES', $prefixTablesMandator . 'shop_products_attributes');
		DEFINE('TABLE_PRODUCTS_OPTIONS', $prefixTablesMandator . 'shop_products_options');
		DEFINE('TABLE_PRODUCTS_OPTIONS_VALUES', $prefixTablesMandator . 'shop_products_options_values');
		DEFINE('TABLE_PERSONAL_OFFERS_BY_CUSTOMERS', $prefixTablesMandator . 'shop_personal_offers_by_customers_status_');
	}

	// IMPORTET SHOP DATA TABLES
	DEFINE('TABLE_SHOP_CUSTOMERS', 'customers');
	DEFINE('TABLE_SHOP_ADDRESS_BOOK', 'address_book');
	DEFINE('TABLE_SHOP_ORDERS', 'orders');
	DEFINE('TABLE_SHOP_ORDERS_TOTAL', 'orders_total');
	DEFINE('TABLE_SHOP_ORDERS_PRODUCTS', 'orders_products');
	DEFINE('TABLE_SHOP_ORDERS_FILES', 'orders_files');
	DEFINE('TABLE_SHOP_PRODUCTS', 'products');
	DEFINE('TABLE_SHOP_ORDERS_PRODUCTS_ATTRIBUTES', 'orders_products_attributes');

	// IMPORTED ONLINE ACQUISATION
	#DEFINE('TABLE_ACQUISATION_SALUTATION_TYPES', 'bctr_c_civilite');
	DEFINE('TABLE_ACQUISATION_CUSTOMERS', 'bctr_customersAcquisition');
	DEFINE('TABLE_ACQUISATION_CUSTOMERS_ACTIVITY_STATUS', 'common_customerActivityStatus');
	DEFINE('TABLE_ACQUISATION_CUSTOMERS_HISTORY', 'bctr_customersAcquisitionHistory');
	DEFINE('TABLE_ACQUISATION_CUSTOMERS_LAST_ORDERS', 'common_customersLastOrders');
	DEFINE('TABLE_ACQUISATION_CUSTOMERS_LAST_ORDERS_HISTORY', 'common_customersLastOrdersHistory');


	#DEFINE('TABLE_ACQUISATION_CUSTOMERS', 'bctr_societe');
	#DEFINE('TABLE_ACQUISATION_REGIONS', 'bctr_c_regions');
	#DEFINE('TABLE_ACQUISATION_DEPARTMENTS', 'bctr_c_departements');
	#DEFINE('TABLE_ACQUISATION_COUNTRIES', 'bctr_c_pays');
	DEFINE('TABLE_ACQUISATION_USERS', 'bctr_users');
	#----------------DEFINE('TABLE_ACQUISATION_USERS', 'bctr_users');
	DEFINE('TABLE_ACQUISATION_USERS_ONLINE', 'bctr_users_online');
	#DEFINE('TABLE_ACQUISATION_ZIPCODES_CITIES', 'bctr_c_ziptown');
	DEFINE('TABLE_ACQUISATION_SALESMAN_DOCUMENTS', 'bctr_salesman_documents');

	// GEO-DB
	DEFINE('TABLE_GEODB_AREAS', 'geodb_areadatas');
	DEFINE('TABLE_GEODB_ZIPCODE_AREAS', 'geodb_zipcodeareadatas');
	DEFINE('TABLE_GEODB_CHANGELOG', 'geodb_changelog');
	DEFINE('TABLE_GEODB_COORDINATES', 'geodb_coordinates');
	DEFINE('TABLE_GEODB_FLOATDATA', 'geodb_floatdata');
	DEFINE('TABLE_GEODB_HIERARCHIES', 'geodb_hierarchies');
	DEFINE('TABLE_GEODB_INTDATA', 'geodb_intdata');
	DEFINE('TABLE_GEODB_LOCATIONS', 'geodb_locations');
	DEFINE('TABLE_GEODB_TEXTDATA', 'geodb_textdata');
	DEFINE('TABLE_GEODB_TYPE_NAMES', 'geodb_type_names');
	DEFINE('TABLE_GEODB_STREET_NAMES', 'geodb_streetNames');
			
	DEFINE('TABLE_GEODB_DOUBLEDIGIT_AREAS', 'geodb_zipcodeDoubleDigitAreadatas');
	DEFINE('TABLE_GEODB_DOUBLEDIGIT_AREAS_NEW', 'geodb_zipcodeDoubleDigitAreaDatasNew');

	// AMAZON
	DEFINE('TABLE_AMAZON_ORDERS', $prefixTablesCommon . 'ordersAmazon');
	DEFINE('TABLE_AMAZON_ORDERS_TRANSACTIONS', $prefixTablesCommon . 'ordersAmazonTransactions');
	DEFINE('TABLE_AMAZON_ORDERS_RATINGS', $prefixTablesCommon . 'ordersAmazonRatings');

	// ADDRESSES
	DEFINE('TABLE_ADDRESS_DATAS', $prefixTablesCommon . 'addresses');

	// LOG SENDED MAILS
	DEFINE('TABLE_SENDED_MAILS', $prefixTablesCommon . 'sendedMails');

	// DOWNLOAD FILES
	DEFINE('TABLE_DOWNLOAD_FILES', $prefixTablesCommon . 'downloadFiles');
	DEFINE('TABLE_DOWNLOAD_FILES_SUBCATEGORIES', $prefixTablesCommon . 'downloadFilesSubCategories');


	DEFINE('TABLE_CREATED_LAYOUT_FILES', $prefixTablesCommon . 'createdLayoutFiles');
	DEFINE('TABLE_PRINT_PRODUCTION_FILES', $prefixTablesCommon . 'printProductionFiles');
	DEFINE('TABLE_PRINTERS_PRINTING_ORDERS', $prefixTablesCommon . 'printersPrintingOrders');
	DEFINE('TABLE_PRINT_PRODUCTION_ORDERS', $prefixTablesCommon . 'printProductionOrders');
	DEFINE('TABLE_PRINTING_PLATES_DATA', $prefixTablesCommon . 'printingPlatesData');

	DEFINE('VIEW_CUSTOMERS_LAYOUT_FILES', '_view_' . $prefixTablesCommon . 'customersLayoutFiles');
	DEFINE('TABLE_CUSTOMERS_LAYOUT_FILES', $prefixTablesCommon . 'customersLayoutFiles');

	// --------------------------------------------------------------------------------

	DEFINE('TABLE_ORDER_AN', TABLE_ORDER_OFFERS);
	DEFINE('TABLE_ORDER_AN_DETAILS', TABLE_ORDER_OFFERS_DETAILS);
	DEFINE('TABLE_ORDER_AB', TABLE_ORDER_CONFIRMATIONS);
	DEFINE('TABLE_ORDER_AB_DETAILS', TABLE_ORDER_CONFIRMATIONS_DETAILS);
	DEFINE('TABLE_ORDER_RK', TABLE_ORDER_REKLAMATIONS);
	DEFINE('TABLE_ORDER_RK_DETAILS', TABLE_ORDER_REKLAMATIONS_DETAILS);
	DEFINE('TABLE_ORDER_LS', TABLE_ORDER_DELIVERIES);
	DEFINE('TABLE_ORDER_LS_DETAILS', TABLE_ORDER_DELIVERIES_DETAILS);
	DEFINE('TABLE_ORDER_RE', TABLE_ORDER_INVOICES);
	DEFINE('TABLE_ORDER_RE_DETAILS', TABLE_ORDER_INVOICES_DETAILS);
	DEFINE('TABLE_ORDER_GU', TABLE_ORDER_CREDITS);
	DEFINE('TABLE_ORDER_GU_DETAILS', TABLE_ORDER_CREDITS_DETAILS);
	DEFINE('TABLE_ORDER_MA', TABLE_ORDER_REMINDERS);
	DEFINE('TABLE_ORDER_MA_DETAILS', TABLE_ORDER_REMINDERS_DETAILS);
	DEFINE('TABLE_ORDER_M1', TABLE_ORDER_FIRST_DEMANDS);
	DEFINE('TABLE_ORDER_M1_DETAILS', TABLE_ORDER_FIRST_DEMANDS_DETAILS);
	DEFINE('TABLE_ORDER_M2', TABLE_ORDER_SECOND_DEMANDS);
	DEFINE('TABLE_ORDER_M2_DETAILS', TABLE_ORDER_SECOND_DEMANDS_DETAILS);
	DEFINE('TABLE_ORDER_M3', TABLE_ORDER_THIRD_DEMANDS);
	DEFINE('TABLE_ORDER_M3_DETAILS', TABLE_ORDER_THIRD_DEMANDS_DETAILS);
	DEFINE('TABLE_ORDER_BR', TABLE_ORDER_LETTERS);
	DEFINE('TABLE_ORDER_BR_DETAILS', TABLE_ORDER_LETTERS_DETAILS);

	DEFINE('TABLE_ORDER_AB_PAYMENTS', TABLE_ORDER_CONFIRMATION_PAYMENTS);
	DEFINE('TABLE_ORDER_RE_PAYMENTS', TABLE_ORDER_INVOICE_PAYMENTS);

	DEFINE('VIEW_CUSTOMERS_FOR_DPD_SOFTWARE', '_viewCustomersForDPD');
	DEFINE('VIEW_RELATED_DOCUMENTS_DATA', '_view_' . $prefixTablesMandator . 'relatedDocumentsData');
	DEFINE('VIEW_ORDERS_TO_CONFIRMATIONS', '_view_' . $prefixTablesCommon . 'ordersToConfirmations');

	DEFINE('TABLE_EXPORT_FACTORING_DATA', $prefixTablesMandator . 'exportFactoringData');
	DEFINE('TABLE_IMPORT_FACTORING_DATA', $prefixTablesMandator . 'importFactoringData');
	DEFINE('TABLE_IMPORT_FACTORING_DATA_RE', $prefixTablesMandator . 'importFactoringData_RE');
	DEFINE('TABLE_IMPORT_FACTORING_DATA_ZA', $prefixTablesMandator . 'importFactoringData_ZA');
	
	DEFINE('TABLE_EXPORT_DATEV_DATA', $prefixTablesMandator . 'exportDatevData');


?>