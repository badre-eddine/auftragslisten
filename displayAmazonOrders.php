<?php
	require_once('inc/requires.inc.php');

	if(!$arrGetUserRights["displayTracking"]) {
		#header('location: ' . PAGE_EXIT_LOCATION);
		#exit;
	}

	$jswindowMessage = "";
	$warningMessage = '';
	$errorMessage = '';
	$successMessage = '';
	$infoMessage = '';

	$dbConnection = new DB_Connection();
	$db_open = $dbConnection->db_connect();

	$defaultSortField = "BESTELLDATUM";
	$defaultSortDirection = "ASC";

	if($_REQUEST["sortField"] != "") {
		$sqlSortField = $_REQUEST["sortField"];
	}
	else {
		$sqlSortField = $defaultSortField;
	}

	if($_REQUEST["sortDirection"] != "") {
		$sqlSortDirection = $_REQUEST["sortDirection"];
	}
	else {
		$sqlSortDirection = $defaultSortDirection;
	}
	if($sqlSortDirection == "DESC") { $sqlSortDirection = "ASC"; }
	else { $sqlSortDirection = "DESC"; }

?>
<?php
	require_once('inc/headerHTML.inc.php');
	$thisTitle = "Amazon-Bestellungen";

	if($_POST["searchOrderNumber"] != "") {
		$thisTitle .= " - Bestellnummer ".$_POST["searchOrderNumber"];
	}
	if($_POST["searchCustomerName"] != "") {
		$thisTitle .= " - Kunde ".$_POST["searchCustomerName"];
	}
	if($_POST["searchWord"] != "") {
		$thisTitle .= " - Suchbegriff ".$_POST["searchWord"];
	}
	if($_POST["searchCustomerNumber"] != "") {
		$thisTitle .= " - Kundennummer ".$_POST["searchWord"];
	}
	if($_REQUEST["displayOrderStatus"] != "") {
		$thisTitle .= " - Status ".$arrOrderStatusTypeDatas[$_REQUEST["displayOrderStatus"]]["orderStatusTypesName"];
	}
	$headerHTML = preg_replace("/{###TITLE###}/", strip_tags($thisTitle), $headerHTML);
	echo $headerHTML;

	require_once(FILE_MENUE_TOP);

	$sqlWhere = "";

	// BOF CREATE SQL FOR DISPLAY ORDERS
	if($_REQUEST["searchOrderNumber"] != "") {
		$sqlWhere = " AND `" . TABLE_AMAZON_ORDERS . "`.`AMAZON-BESTELLNUMMER` = '" . $_REQUEST["searchOrderNumber"] . "' ";
	}
	else if($_REQUEST["searchCustomerName"] != "") {
		$sqlWhere = " AND (`" . TABLE_AMAZON_ORDERS . "`.`KAEUFER` LIKE '%" . addslashes($_REQUEST["searchCustomerName"]) . "%') ";
		// $sqlWhere = " AND MATCH(`ordersKundenName`) AGAINST ('" . $_REQUEST["searchCustomerName"] . "') ";
	}
	else if($_REQUEST["searchCustomerNumber"] != "") {
		$sqlWhere = " AND (
			`" . TABLE_AMAZON_ORDERS . "`.`KUNDENNUMMER` LIKE '%" . $_REQUEST["searchCustomerNumber"] . "%'
			OR
			`" . TABLE_CUSTOMERS . "`.`customersKundennummer` LIKE '%" . $_REQUEST["searchCustomerNumber"] . "%'

			) ";
	}

	else if($_REQUEST["searchWord"] != "") {
		$sqlWhere = " AND (
						`" . TABLE_AMAZON_ORDERS . "`.`AMAZON-BESTELLNUMMER` LIKE '" . addslashes($_REQUEST["searchWord"]) . "%'
						OR
						`" . TABLE_AMAZON_ORDERS . "`.`ASIN` LIKE '" . addslashes($_REQUEST["searchWord"]) . "%'
						OR
						`" . TABLE_AMAZON_ORDERS . "`.`SKU` LIKE '%" . addslashes($_REQUEST["searchWord"]) . "%'
						OR
						`" . TABLE_AMAZON_ORDERS . "`.`KAEUFER` LIKE '%" . addslashes($_REQUEST["searchWord"]) . "%'
						OR
						`" . TABLE_AMAZON_ORDERS . "`.`KUNDENNUMMER` LIKE '%" . addslashes($_REQUEST["searchWord"]) . "%'
						OR
						`" . TABLE_CUSTOMERS . "`.`customersKundennummer` LIKE '%" . addslashes($_REQUEST["searchWord"]) . "%'
						OR
						`" . TABLE_CUSTOMERS . "`.`customersFirmenname` LIKE '%" . addslashes($_REQUEST["searchWord"]) . "%'
						OR
						`" . TABLE_AMAZON_ORDERS . "`.`PRODUKTNAME` LIKE '%" . addslashes($_REQUEST["searchWord"]) . "%'
					) ";
					// MATCH(`ordersVertreter`) AGAINST ('" . $_REQUEST["searchWord"] . "')
					// MATCH(`ordersKundenName`) AGAINST ('" . $_REQUEST["searchWord"] . "')
	}
	else {
		$sqlWhere = "";
	}

	$sqlSortField = "ORDER BY `" . $sqlSortField . "` ";

	$sql = "SELECT
				SQL_CALC_FOUND_ROWS

				`" . TABLE_AMAZON_ORDERS . "`.`AMAZON-BESTELLNUMMER`,
				`" . TABLE_AMAZON_ORDERS . "`.`SKU`,

				`" . TABLE_AMAZON_ORDERS . "`.`COUNT`,
				`" . TABLE_AMAZON_ORDERS . "`.`BESTELLDATUM`,
				`" . TABLE_AMAZON_ORDERS . "`.`VERTRIEBSKANAL`,
				`" . TABLE_AMAZON_ORDERS . "`.`VERSAND_DURCH`,
				`" . TABLE_AMAZON_ORDERS . "`.`PRODUKTNAME`,
				`" . TABLE_AMAZON_ORDERS . "`.`MENGE`,
				`" . TABLE_AMAZON_ORDERS . "`.`ASIN`,
				`" . TABLE_AMAZON_ORDERS . "`.`KAEUFER`,
				`" . TABLE_AMAZON_ORDERS . "`.`VERSANDART`,
				`" . TABLE_AMAZON_ORDERS . "`.`STATUS`,
				`" . TABLE_AMAZON_ORDERS . "`.`LIEFERDATUM_VON`,
				`" . TABLE_AMAZON_ORDERS . "`.`LIEFERDATUM_BIS`,
				`" . TABLE_AMAZON_ORDERS . "`.`KUNDENNUMMER`,

				GROUP_CONCAT(`" . TABLE_CUSTOMERS . "`.`customersKundennummer` SEPARATOR ';') AS `customersKundennummer`,
				GROUP_CONCAT(`" . TABLE_CUSTOMERS . "`.`customersFirmenname` SEPARATOR ';') AS `customersFirmenname`,
				GROUP_CONCAT(`" . TABLE_CUSTOMERS . "`.`customersFirmennameZusatz` SEPARATOR ';') AS `customersFirmennameZusatz`

				FROM `" . TABLE_AMAZON_ORDERS . "`

				LEFT JOIN `" . TABLE_CUSTOMERS . "`
				ON(`" . TABLE_AMAZON_ORDERS . "`.`KAEUFER` = `" . TABLE_CUSTOMERS . "`.`customersFirmennameZusatz`)

				WHERE 1
					AND`" . TABLE_AMAZON_ORDERS . "`.`MENGE` > 0
					" . $sqlWhere . "

				GROUP BY CONCAT(
					`" . TABLE_AMAZON_ORDERS . "`.`AMAZON-BESTELLNUMMER`,
					'#',
					`" . TABLE_AMAZON_ORDERS . "`.`SKU`,
					'#'
				)

				". $sqlSortField . " ". $sqlSortDirection . "
		";

	// EOF CREATE SQL FOR DISPLAY ORDERS
	#dd('sql');
?>
<div id="mainArea">
	<div id="mainContent">
		<?php require_once(FILE_MENUE_SIDEBAR); ?>
		<div id="contentArea">
			<a name="top"></a>
			<div id="contentAreaElements">
				<h1><?php if(SHOW_PAGE_TITLE_ICONS) { echo '<img src="' . PATH_ICONS_MENUE_TITLES . 'amazon.png" alt="" />'; } ?> <?php echo $thisTitle; ?></h1>
				<?php
					echo '<div align="right"><a href="' . PAGE_SHOP_AMAZON . '" target="_blank">Zum Amazon-Shop</a></div>';
				?>
				<div id="searchFilterArea">
					<form name="formFilterSearch" method="post" action="<?php echo $_SERVER["PHP_SELF"]; ?>">
					<table border="0" cellpadding="0" cellspacing="0" width="" class="searchFilterContent">
						<tr>
							<td>
								<label for="searchOrderNumber">Bestellnummer:</label>
								<input type="text" name="searchOrderNumber" id="searchOrderNumber" class="inputField_70" value="<?php echo $_REQUEST["searchOrderNumber"]; ?>" />
							</td>
							<td>
								<label for="searchCustomerName">Besteller:</label>
								<input type="text" name="searchCustomerName" id="searchCustomerName" class="inputField_160" value="<?php echo $_REQUEST["searchCustomerName"]; ?>" />
							</td>
							<td>
								<label for="searchCustomerName">Kundennr:</label>
								<input type="text" name="searchCustomerNumber" id="searchCustomerNumber" class="inputField_160" value="<?php echo $_REQUEST["searchCustomerNumber"]; ?>" />
							</td>
							<td>
								<label for="searchWord">Suchbegriff:</label>
								<input type="text" name="searchWord" id="searchWord" class="inputField_70" value="<?php echo $_REQUEST["searchWord"]; ?>" />
							</td>
							<td>
								<input type="hidden" name="editID" id="editID" value="" />
								<input type="submit" name="submitSearch" class="inputButton0" value="Suchen" />
							</td>
						</tr>
					</table>
					<input type="hidden" name="sortDirection" id="inputSortDirection" value="<?php echo $_REQUEST["sortDirection"]; ?>" />
					<input type="hidden" name="sortRow" id="inputSortRow" value="<?php echo $_REQUEST["sortRow"]; ?>" />
					<input type="hidden" name="sortField" id="inputSortField" value="<?php echo $_REQUEST["sortField"]; ?>" />
					<input type="hidden" name="displayType" id="inputDisplayType" value="<?php echo $_REQUEST["displayType"]; ?>" />
					<input type="hidden" name="submitSearchForm" id="inputSubmitSearchForm" value="1" />
					<input type="hidden" name="displayOrderStatus" value="<?php echo $_REQUEST["displayOrderStatus"]; ?>" />
					</form>
				</div>

				<?php displayMessages(); ?>

				<?php
					if($_REQUEST["page"] == "" || $_REQUEST["page"] == 0) {
						$_REQUEST["page"] = 1;
					}
					if(MAX_ORDERS_PER_PAGE > 0) {
						$sql .= " LIMIT " . (($_REQUEST["page"] - 1) * MAX_ORDERS_PER_PAGE) . ", " . MAX_ORDERS_PER_PAGE." ";
					}

					$rs = mysqli_query($sql, $db_open);

					$sql_totalRows = "SELECT FOUND_ROWS() AS `countRows`";
					$rs_totalRows = $dbConnection->db_query($sql_totalRows);
					list($countRows) = mysqli_fetch_array($rs_totalRows);

					$pagesCount = ceil($countRows / MAX_ORDERS_PER_PAGE);

					if($countRows > 0) {
				?>
				<?php
					if($pagesCount > 1) {
						include(FILE_MENUE_PAGES);
					}
				?>
				<div class="contentDisplay">
					<table border="0" cellpadding="0" cellspacing="0" width="100%" class="displayOrders">
						<thead>
							<tr>
								<th style="width:45px;text-align:right;">#</th>
								<th class="sortColumn">Bestelldatum</th>
								<th class="sortColumn">Amazon-Bestellnummer</th>
								<th class="sortColumn">Produktname</th>
								<!--
								<th class="sortColumn">Versender</th>
								-->
								<th>Menge</th>
								<th>K&auml;ufername</th>
								<th>Kundenname</th>
								<th>Kunden-Nummer</th>
								<th>Status</th>
								<th>Info</th>
							</tr>
						</thead>

						<tbody>
					<?php

						$count = 0;
						while($ds = mysqli_fetch_assoc($rs)) {

							if($count%2 == 0){ $rowClass = 'row0'; }
							else { $rowClass = 'row1'; }

							if($ds["STATUS"] == "Versandt") {
								$rowClass = 'row3';
							}
							else if(preg_match("/Erstattung/", $ds["STATUS"])){
								$rowClass = 'row6';
							}

							echo '<tr class="'.$rowClass.'">';
								echo '<td style="text-align:right;"><b>' . ($count + 1) .'.</b></td>';

								echo '<td style="white-space:nowrap;">' . substr(formatDate($ds["BESTELLDATUM"], 'display'), 0, 10) .'</td>';

								echo '<td>';#
								echo '<a href="' . preg_replace("/{###AMAZON_ORDER_ID##}/", $ds["AMAZON-BESTELLNUMMER"], PAGE_SHOP_AMAZON_ORDERS) . '" target="_blank">'. $ds["AMAZON-BESTELLNUMMER"] . '</a>';
								echo '</td>';

								echo '<td style="white-space:nowrap;">' . $ds["PRODUKTNAME"] .'</td>';
								#echo '<td>' . $ds["VERSAND_DURCH"] .'</td>';
								echo '<td style="text-align:right;">' . number_format($ds["MENGE"], 0, ',', '') .'</td>';

								echo '<td style="white-space:nowrap;">';
								$arrTemp = explode(";", $ds["KAEUFER"]);
								if(!empty($arrTemp)){
									foreach($arrTemp as $thisTemp){
										echo '<span><a href="' . PAGE_EDIT_CUSTOMER . '?searchWord=' . $thisTemp . '">' . $thisTemp . '</a></span><br />';
									}
								}
								echo '</td>';

								echo '<td style="white-space:nowrap;">';
								$arrTemp = explode(";", $ds["customersFirmenname"]);
								if(!empty($arrTemp)){
									foreach($arrTemp as $thisTemp){
										echo '<span>' . $thisTemp . '</span><br />';
									}
								}
								echo '</td>';

								echo '<td>';
								$arrTemp = explode(";", $ds["customersKundennummer"]);
								if(!empty($arrTemp)){
									foreach($arrTemp as $thisTemp){
										echo '<span><a href="' . PAGE_EDIT_CUSTOMER . '?searchCustomerNumber=' . $thisTemp  . '">' . $thisTemp . '</span><br />';
									}
								}
								echo '</td>';

								echo '<td style="white-space:nowrap;">';
								echo preg_replace("/([a-z])([A-Z])/", "$1 - $2", $ds["STATUS"]);
								echo '</td>';

								echo '<td style="text-align:right;">';
								echo '<span class="toolItem">';
								echo '<img src="layout/icons/iconInfo.png" class="buttonNotice" alt="Details" title="Detailinformationen zu dieser Bestellung ansehen">';
								echo '<div class="displayNoticeArea">';

								echo '<table border="0" cellpadding="0" cellspacing="0" width="100%">';
								echo '<tr>';
								echo '<td style="width:120px;"><b>ASIN:</b></td>';
								echo '<td>' . $ds["ASIN"] .'</td>';
								echo '</tr>';
								echo '<tr>';
								echo '<td><b>SKU:</b></td>';
								echo '<td>' . $ds["SKU"] .'</td>';
								echo '</tr>';
								echo '<tr>';
								echo '<td><b>Lieferzeitraum ca:</b></td>';
								echo '<td style="white-space:nowrap;">' . formatDate($ds["LIEFERDATUM_VON"], 'display') .' - ' . formatDate($ds["LIEFERDATUM_BIS"], 'display') . '</td>';
								echo '</tr>';
								echo '</table>';

								echo '</div>';
								echo '</span>';
								echo '</td>';
							echo '<tr>';
							$count++;
						}
					?>
						</tbody>
					</table>
				</div>
				<?php
					if($pagesCount > 1) {
						include(FILE_MENUE_PAGES);
					}
				?>
				<div style="text-align:right;" class="menueToTop">
					<a href="#top">nach oben</a>
				</div>
				<?php
					}
					else {
						echo '<p class="warningArea">Es wurden keine Daten gefunden!</p>';
					}
				?>
				<div class="clear"></div>
			</div>
			<div class="clear"></div>
		</div>
	</div>
</div>
<script language="javascript" type="text/javascript">
	$(document).ready(function() {
		var animateTime = 500;
		setFocus('formFilterSearch', 'searchWord');
		colorRowMouseOver('.displayOrders tbody tr');

		$('#searchOrderNumber').keyup(function () {
			// loadSuggestions('searchOrderNumber', [{'triggerElement': '#searchOrderNumber', 'fieldNumber': '#searchOrderNumber', 'fieldName': '#searchCustomerName', 'fieldCity': '', 'fieldZipCode': ''}], 1);
		});
		$('#searchCustomerName').keyup(function () {
			// loadSuggestions('searchCustomerName', [{'triggerElement': '#searchCustomerName', 'fieldNumber': '#searchOrderNumber', 'fieldName': '#searchCustomerName', 'fieldCity': '', 'fieldZipCode': ''}], 1);
		});
		$('#searchSalesmanName').keyup(function () {
			// loadSuggestions('searchSalesmanName', [{'triggerElement': '#searchSalesmanName', 'fieldName': '#searchSalesmanName'}], 1);
		});
		$('#searchPLZ').keyup(function () {
			// loadSuggestions('searchPLZ', [{'triggerElement': '#searchPLZ', 'fieldZipCode': '#searchPLZ'}], 1);
		});

		$('.buttonNotice').click(function () {
			loadNotice($(this));
			$(this).mouseleave(function () {
				// $('#loadNoticeArea').fadeOut(animateTime);
			});
		});
	});
</script>

<?php require_once('inc/footerHTML.inc.php'); ?>