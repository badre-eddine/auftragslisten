<?php
	require_once('inc/requires.inc.php');

	// newsID#newsDate#newsTitle#newsText#newsType#newsActive

	if(!$arrGetUserRights["editInternalMessages"] && !$arrGetUserRights["displayInternalMessages"]) {
		header('location: ' . PAGE_EXIT_LOCATION);
		exit;
	}

	$dbConnection = new DB_Connection();
	$db_open = $dbConnection->db_connect();

	if(DB_HOST_EXTERN_ACQUISITION != '' && DB_NAME_EXTERN_ACQUISITION != '' && DB_USER_EXTERN_ACQUISITION != '' && DB_PASSWORD_EXTERN_ACQUISITION) {
		$existsOnlineAquisition = true;
	}
	else {
		$existsOnlineAquisition = false;
		$errorMessage .= 'Es sind keine Zugansdaten f&uuml;r die Datenbankverbindung zur ' . strtoupper(MANDATOR) . '-Kundenerfassung eingetragen. ' . '<br />';
	}

	$dbConnection_ExternAcqisition = new DB_Connection(DB_HOST_EXTERN_ACQUISITION, '', DB_NAME_EXTERN_ACQUISITION, DB_USER_EXTERN_ACQUISITION, DB_PASSWORD_EXTERN_ACQUISITION);
	$db_openExternAcqisition = $dbConnection_ExternAcqisition->db_connect();

	$jswindowMessage = '';
	$warningMessage = '';
	$errorMessage = '';
	$successMessage = '';
	$infoMessage = '';

	// BOF GET USER DATAS
		$userDatas = getUserDatas();
	// EOF GET USER DATAS

	// EOF DEFINE MESSAGE TYPES
		$arrMessageTypes = array(
			"internal" => "Interne Benachrichtigungen",
			"external" => "Externe Benachrichtigungen",
		);
	// EOF DEFINE MESSAGE TYPES


	// BOF DEFINE DUTY FIELDS
		$arrFormDutyFields = array(
			"editMessageNewsDateEntry" => "Eintrags-Datum",
			"editMessageNewsDateStart" => "Start-Datum",
			"editMessageNewsDateEnd" => "End-Datum",
			"editMessageNewsTitle" => "Titel",
			"editMessageNewsText" => "Text"
		);
		// CREATE JSON DUTY FIELD
		$jsonFormDutyFields = createJson($arrFormDutyFields);
	// BOF DEFINE DUTY FIELDS

	if($_REQUEST["editID"] == "")  {
		// $_REQUEST["editID"] = "NEW";
		// header("location: displayOrders.php");
		// exit;
	}


	// BOF STORE DATAS
		if($_POST["storeDatas"] != ""){
			if($_POST["editMessageNewsTitle"] != '' && $_POST["editMessageNewsDateEntry"] != ''){

				if($_POST["editMessageNewsID"] == ''){
					$_POST["editMessageNewsID"] = "%";
				}
				$sql = "
					REPLACE INTO `{###TABLE_MESSAGES###}` (
												`newsID`,
												`newsDateEntry`,
												`newsDateStart`,
												`newsDateEnd`,
												`newsTitle`,
												`newsText`,
												`newsType`,
												`newsRecipient`,
												`newsActive`
											)
											VALUES (
												'" . $_POST["editMessageNewsID"] . "',
												'" . formatDate($_POST["editMessageNewsDateEntry"], 'store') . "',
												'" . formatDate($_POST["editMessageNewsDateStart"], 'store') . "',
												'" . formatDate($_POST["editMessageNewsDateEnd"], 'store') . "',
												'" . $_POST["editMessageNewsTitle"] . "',
												'" . addslashes($_POST["editMessageNewsText"]) . "',
												'" . $_POST["editMessageNewsType"] . "',
												'" . $_POST["editMessageNewsRecipient"] . "',
												'" . $_POST["editMessageNewsActive"] . "'
											)
					";

				if($_POST["editMessageNewsType"] == "internal"){
					$sql_local = preg_replace("/{###TABLE_MESSAGES###}/", TABLE_MESSAGES, $sql);
					$rs_local = $dbConnection->db_query($sql_local);

					if($rs_local){
						$successMessage .= 'Die Benachrichtigungs-Daten wurden lokal gespeichert.' . '<br />';
						unset($_POST["addCustomerCalendarDates"]);
					}
					else {
						$errorMessage .= 'Die Benachrichtigungs-Daten konnten nicht lokal gespeichert werden.' . '<br />';
					}
				}
				else if($_POST["editMessageNewsType"] == "external"){
					$sql_extern = preg_replace("/{###TABLE_MESSAGES###}/", TABLE_ACQUISATION_MESSAGES, $sql);
					$rs_extern = $dbConnection_ExternAcqisition->db_query($sql_extern);
					if($rs_extern){
						$successMessage .= 'Die Benachrichtigungs-Daten wurden extern gespeichert.' . '<br />';
						unset($_POST["addCustomerCalendarDates"]);
					}
					else {
						$errorMessage .= 'Die Benachrichtigungs-Daten konnten nicht extern gespeichert werden.' . '<br />';
					}
				}
			}
			else {
				$errorMessage .= 'Sie haben die Benachrichtigungs-Daten nicht korrekt eingegeben.' . '<br />';
			}
		}
	// EOF STORE DATAS


	// BOF DELETE DATAS
	if((int)$_REQUEST["deleteNewsID"] > 0 || $_POST["deleteDatas"] != ""){
		$where = "";
		if((int)$_REQUEST["deleteNewsID"] > 0) {
			$where = " AND `newsID` = '" . $_REQUEST["deleteNewsID"] . "'
				AND `newsType` = '" . $_REQUEST["newsType"] . "'
			";
		}
		else if($_POST["deleteDatas"] != ""){
			$where = " AND `newsID` = '" . $_POST["editMessageNewsID"] . "'
						AND `newsType` = '" . $_POST["newsType"] . "'
			";
		}
		if($where != ""){
			$sql = "DELETE
						FROM `{###TABLE_MESSAGES###}`
							WHERE 1 " . $where . "
				";

			if($_REQUEST["newsType"] == "internal"){
				$sql_local = preg_replace("/{###TABLE_MESSAGES###}/", TABLE_MESSAGES, $sql);
				$rs_local = $dbConnection->db_query($sql_local);

				if($rs_local){
					$successMessage .= 'Die Benachrichtigungs-Daten wurden lokal entfernt.' . '<br />';
					unset($_POST["addCustomerCalendarDates"]);
				}
				else {
					$errorMessage .= 'Die Benachrichtigungs-Daten konnten nicht lokal entfernt werden.' . '<br />';
				}
			}
			else if($_REQUEST["newsType"] == "external"){
				$sql_extern = preg_replace("/{###TABLE_MESSAGES###}/", TABLE_ACQUISATION_MESSAGES, $sql);
				$rs_extern = $dbConnection_ExternAcqisition->db_query($sql_extern);
				if($rs_extern){
					$successMessage .= 'Die Benachrichtigungs-Daten wurden extern entfernt.' . '<br />';
					unset($_POST["addCustomerCalendarDates"]);
				}
				else {
					$errorMessage .= 'Die Benachrichtigungs-Daten konnten nicht extern entfernt werden.' . '<br />';
				}
			}
		}
		else {
			$errorMessage .= 'Die Benachrichtigung konnte nicht entfernt werden.' . '<br />';
		}
	}
	// EOF DELETE DATAS

	// BOF GET ALL DATAS
		if($_REQUEST["editID"] != "NEW"){
			$where = "";
			if($_REQUEST["searchMessageID"] > 0 ){
				$where = "
					AND `newsID` = '" . $_REQUEST["searchMessageID"] . "'
					AND `newsType` = '" . $_REQUEST["newsType"] . "'

				";
				$_REQUEST["editID"] = $_REQUEST["searchMessageID"];
			}

			$sql = "
					SELECT
						`newsID`,
						`newsDateEntry`,
						`newsDateStart`,
						`newsDateEnd`,
						`newsTitle`,
						`newsText`,
						`newsType`,
						`newsRecipient`,
						`newsActive`

						FROM `{###TABLE_MESSAGES###}`

						WHERE 1
							" . $where . "
						ORDER BY `newsActive` DESC, `newsDateEntry` DESC, `newsTitle` ASC
			";

			$sql_local = preg_replace("/{###TABLE_MESSAGES###}/", TABLE_MESSAGES, $sql);
			$rs_local = $dbConnection->db_query($sql_local);
			$countRows_local = $dbConnection->db_getMysqlNumRows($rs_local);

			$sql_extern = preg_replace("/{###TABLE_MESSAGES###}/", TABLE_ACQUISATION_MESSAGES, $sql);
			$rs_extern = $dbConnection_ExternAcqisition->db_query($sql_extern);
			$countRows_extern = $dbConnection_ExternAcqisition->db_getMysqlNumRows($rs_extern);

			$countRows = $countRows_local + $countRows_extern;

			if($countRows > 1){

				if($countRows_local > 0) {
					while($ds = mysqli_fetch_assoc($rs_local)){
						foreach(array_keys($ds) as $field){
							$arrMessageData[$ds["newsType"]][$ds["newsID"]][$field] = $ds[$field];
						}
					}
				}

				if($countRows_extern > 0) {
					while($ds = mysqli_fetch_assoc($rs_extern)){
						foreach(array_keys($ds) as $field){
							$arrMessageData[$ds["newsType"]][$ds["newsID"]][$field] = $ds[$field];
						}
					}

				}
			}
			else if($countRows == 1) {
				if($countRows_extern == 1){ $rs_this = $dbConnection_ExternAcqisition->db_query($sql_extern); }
				if($countRows_local == 1) { $rs_this = $dbConnection->db_query($sql_local); }
				$arrSelectedMessageData = array();
				list(
					$arrSelectedMessageData["newsID"],
					$arrSelectedMessageData["newsDateEntry"],
					$arrSelectedMessageData["newsDateStart"],
					$arrSelectedMessageData["newsDateEnd"],
					$arrSelectedMessageData["newsTitle"],
					$arrSelectedMessageData["newsText"],
					$arrSelectedMessageData["newsType"],
					$arrSelectedMessageData["newsRecipient"],
					$arrSelectedMessageData["newsActive"]
				) = mysqli_fetch_array($rs_this);
				$_REQUEST["editID"] = $arrSelectedMessageData["newsID"];
			}
			else {
				#$warningMessage .= 'Es sind keine Benachrichtigungen vorhanden.' . '<br />';
			}
		}
	// BOF GET ALL DATAS
?>

<?php
	require_once('inc/headerHTML.inc.php');
	$thisTitle = 'Interne Benachrichtigungen';
	$thisIcon = 'messages.png';

	$headerHTML = preg_replace("/{###TITLE###}/", strip_tags($thisTitle), $headerHTML);
	echo $headerHTML;

	require_once(FILE_MENUE_TOP);
?>

<div id="mainArea">
	<div id="mainContent">
		<?php require_once(FILE_MENUE_SIDEBAR); ?>
		<div id="contentArea">

			<div id="contentAreaElements">
				<h1><?php if(SHOW_PAGE_TITLE_ICONS) { echo '<img src="' . PATH_ICONS_MENUE_TITLES . $thisIcon . '" alt="" />'; } ?> <?php echo $thisTitle; ?></h1>

				<?php if($_REQUEST["editID"] != "NEW") { ?>
						<div class="menueActionsArea">
						<?php if($arrGetUserRights["editInternalMessages"]) { ?><a href="<?php echo PAGE_EDIT_INTERNAL_MESSAGES; ?>?editID=NEW" class="linkButton" title="Neue Benachrichtigung anlegen" >Benachrichtigung anlegen</a><?php } ?>

						<div class="clear"></div>
					</div>
				<?php
					}
				?>
				<?php displayMessages(); ?>
				<div class="adminInfo">Datensatz-ID: <?php echo $arrSelectedMessageData["newsID"]; ?></div>
				<div class="contentDisplay">
				<?php if($_REQUEST["editID"] == "") { ?>

				<?php
					if(!empty($arrMessageTypes)){
						echo '<div id="tabs">';
						echo '<ul>';
						$countMessageType = 1;
						foreach($arrMessageTypes as $thisMessageTypeKey => $thisMessageTypeValue){
							echo '<li><a href="#tabs-' . $thisMessageTypeKey . '">' . $thisMessageTypeValue . '</a></li>';
							$countMessageType++;
						}
						echo '</ul>';

						foreach($arrMessageTypes as $thisMessageTypeKey => $thisMessageTypeValue){
							?>
							<div id="tabs-<?php echo $thisMessageTypeKey; ?>">
								<div id="searchFilterArea">
									<form name="formFilterSearch_<?php echo $thisMessageTypeKey; ?>" method="get" action="<?php echo $_SERVER["PHP_SELF"]; ?>">
										<table border="0" cellpadding="0" cellspacing="0" class="searchFilterContent">
											<tr>
												<td>
													<label for="searchMessageID">Nachricht:</label>
													<select name="searchMessageID" id="searchMessageID" class="inputSelect_100">
														<option value=""> --- Bitte w&auml;hlen --- </option>
														<?php
															if(!empty($arrMessageData)){
																foreach($arrMessageData[$thisMessageTypeKey] as $thisKey => $thisValue){
																	$selected = '';
																	if($thisValue["newsID"] == $_REQUEST["searchMessageID"]){
																		$selected = ' selected="selected" ';
																	}
																	echo '<option value="' . $thisValue["newsID"] . '" ' . $selected . ' >' . htmlentities($thisValue["newsDateEntry"]) . ': ' . $thisValue["newsTitle"] . '</option>';
																}
															}
														?>
													</select>
												</td>
												<td>
													<label for="searchWord">Suchbegriff:</label>
													<input type="text" name="searchWord" id="searchWord" class="inputField_100" />
												</td>
												<td>
													<input type="hidden" name="xxxeditID" id="xxxeditID" value="" />
													<input type="hidden" name="searchNewsType" value="<?php echo $thisMessageTypeKey; ?>" />
													<input type="submit" name="submitSearch" class="inputButton0" value="Suchen" />
												</td>
											</tr>
										</table>
									</form>
								</div>
								<?php
									if($pagesCount > 1 && $_REQUEST["displayOrderStatus"] == "") { include(FILE_MENUE_PAGES); }

									echo '<table cellpadding="0" cellspacing="0" border="0" width="100%" class="displayOrders">
											<thead>
												<tr>
													<th style="width:45px;text-align:right;">#</th>
													<th>Eintrags-Datum</th>
													<th>Start-Datum</th>
													<th>End-Datum</th>
													<th>Titel</th>
													<th>Text</th>
													<th>Typ</th>
													<th>Empf&auml;nger</th>
													<th>Status</th>
													<th>Details</th>
												</tr>
											</thead>
											<tbody>
										';

									$count = 0;
									foreach($arrMessageData[$thisMessageTypeKey] as $thisKey => $thisValue) {
										if($count%2 == 0){ $rowClass = 'row0'; }
										else { $rowClass = 'row1'; }

										$thisStyle = '';
										if($thisValue["newsActive"] != '1') {
											$thisStyle = ' style="color:#FF0000; font-style:italic;" ';
										}

										echo '<tr ' . $thisStyle . ' class="'.$rowClass.'">';
											echo '<td style="text-align:right;"><span style="font-weight:bold;cursor:pointer;" title="Datensatz-ID: '.($thisValue["newsID"]).'">'.($count + 1).'.</span></td>';

											echo '<td>';
											echo formatDate($thisValue["newsDateEntry"], "display");
											echo '</td>';

											echo '<td>';
											echo formatDate($thisValue["newsDateStart"], "display");
											echo '</td>';

											echo '<td>';
											echo formatDate($thisValue["newsDateEnd"], "display");
											echo '</td>';

											echo '<td>';
											echo $thisValue["newsTitle"];
											echo '</td>';

											echo '<td>';
											echo $thisValue["newsText"];
											echo '</td>';

											echo '<td>';
											echo $thisValue["newsType"];
											echo '</td>';

											echo '<td>';
											echo $thisValue["newsRecipient"];
											echo '</td>';

											echo '<td>';
											echo $thisValue["newsActive"];
											echo '</td>';

											echo '<td style="white-space:nowrap;">';
											echo '<span class="toolItem"><a href="' . PAGE_EDIT_INTERNAL_MESSAGES . '?searchMessageID='.$thisValue["newsID"]. '&amp;newsType=' . $thisMessageTypeKey . '"><img src="layout/icons/iconEdit.gif" width="16" height="16" title="Benachrichtigung bearbeiten (Datensatz '.$thisValue["newsID"].')" alt="Bearbeiten" /></a></span>';
											echo '<span class="toolItem"><a href="' . PAGE_EDIT_INTERNAL_MESSAGES . '?deleteNewsID='.$thisValue["newsID"]. '&amp;newsType=' . $thisMessageTypeKey . '#tabs-' . $thisMessageTypeKey . '"><img src="layout/icons/iconDelete.png" width="16" height="16" title="Benachrichtigung enfernen (Datensatz '.$thisValue["newsID"].')" alt="entfernen" onclick="return showWarning(\'Wollen Sie diesen Eintrag wirklich entfernen?\')"/></a></span>';
											echo '</td>';
										echo '</tr>';
										$count++;
									}
									echo '	</tbody>
										</table>
									';

									if($pagesCount > 1 && $_REQUEST["displayOrderStatus"] == "") {
										include(FILE_MENUE_PAGES);
									}
								?>
							</div>
							<?php
						}
						echo '</div>';
					}

					}
				?>

				<?php if($_REQUEST["editID"] != ""){ ?>
					<div class="adminEditArea">
						<?php displayMessages(); ?>
						<form name="formEditMessageDatas" id="formEditMessageDatas" method="post" action="<?php echo $_SERVER["PHP_SELF"]; ?>#tabs-1" enctype="multipart/form-data" >
							<input type="hidden" name="editMessageNewsID" value="<?php echo $arrSelectedMessageData["newsID"]; ?>" />
							<br /><p class="warningArea">Pflichtfelder  m&uuml;ssen ausgef&uuml;llt werden!</p>
							<fieldset>
								<legend>Benachrichtigungsdaten</legend>
								<table border="0" width="100%" cellspacing="0" cellpadding="0">
									<tr>
										<td><b>Eintrags-Datum:</b></td>
										<td>
											<input type="text" name="editMessageNewsDateEntry" id="editMessageNewsDateEntry" maxlength="2" class="inputField_100" readonly="readonly" value="<?php if($arrSelectedMessageData["newsDateEntry"] == "") { echo date("d.m.Y"); } else { echo formatDate($arrSelectedMessageData["newsDateEntry"], "display"); } ?>" />
										</td>
									</tr>
									<tr>
										<td><b>Start-Datum:</b></td>
										<td>
											<input type="text" name="editMessageNewsDateStart" id="editMessageNewsDateStart" maxlength="2" class="inputField_100" readonly="readonly" value="<?php if($arrSelectedMessageData["newsDateStart"] == "") { echo date("d.m.Y"); } else { echo formatDate($arrSelectedMessageData["newsDateStart"], "display"); } ?>" />
										</td>
									</tr>
									<tr>
										<td><b>End-Datum:</b></td>
										<td>
											<input type="text" name="editMessageNewsDateEnd" id="editMessageNewsDateEnd" maxlength="2" class="inputField_100" readonly="readonly" value="<?php if($arrSelectedMessageData["newsDateEnd"] == "") { echo date("d.m.Y"); } else { echo formatDate($arrSelectedMessageData["newsDateEnd"], "display"); } ?>" />
										</td>
									</tr>
									<tr>
										<td><b>Titel:</b></td>
										<td><input type="text" name="editMessageNewsTitle"id="editMessageNewsTitle" class="inputField_510" value="<?php echo ($arrSelectedMessageData["newsTitle"]); ?>" /></td>
									</tr>
									<tr>
										<td><b>Text:</b></td>
										<td><textarea name="editMessageNewsText"id="editMessageNewsText" class="inputTextarea_510x140" cols="20" rows="40"><?php echo ($arrSelectedMessageData["newsText"]); ?></textarea></td>
									</tr>
									<tr>
										<td><b>Typ:</b></td>
										<td>
											<select name="editMessageNewsType"id="editMessageNewsType" class="inputField_510">
												<?php
													$selected = '';
													if($arrSelectedMessageData["newsType"] != "external"){
														$selected = ' selected="selected" ';
													}
												?>
												<option value="internal" <?php echo $selected; ?> >Interne Benachrichtigung</option>
												<?php
													$selected = '';
													if($arrSelectedMessageData["newsType"] == "external"){
														$selected = ' selected="selected" ';
													}
												?>
												<option value="external" <?php echo $selected; ?> >Externe Benachrichtigung</option>
											</select>
										</td>
									</tr>
									<tr>
										<td><b>Empf&auml;nger:</b></td>
										<td><input type="text" name="editMessageNewsRecipient"id="editMessageNewsRecipient" class="inputField_510" value="<?php echo ($arrSelectedMessageData["newsRecipient"]); ?>" /></td>
									</tr>
									<tr>
										<td><b>Status:</b></td>
										<?php
											$checked = '';
											if($arrSelectedMessageData["newsActive"] == "1"){
												$checked = ' checked="checked" ';
											}
										?>
										<td><input type="checkbox" name="editMessageNewsActive"id="editMessageNewsActive" value="1" <?php echo $checked; ?>/> <b>aktiv?</b></td>
									</tr>
								</table>
							</fieldset>

							<div class="actionButtonsArea">
								<?php
									if($arrGetUserRights["editInternalMessages"]) {
								?>
								<input type="submit" class="inputButton1 inputButtonGreen" name="storeDatas" value="Speichern" onclick="return checkFormDutyFields(this.form.name, <?php echo $jsonFormDutyFields; ?>);" />
								<input type="submit" class="inputButton1 inputButtonOrange" name="resetDatas" value="Abbrechen" onclick="return showWarning('Wollen Sie diesen Vorgang wirklich abbrechen??? ');" />
								<input type="submit" class="inputButton1 inputButtonRed" name="deleteDatas" value="Entfernen" onclick="return showWarning('Wollen Sie diese Nachricht wirklich entfernen??? ');" />
								<?php
									}
								?>
							</div>
						</form>
					</div>
				<?php } ?>
				</div>
			</div>
		</div>
		<div class="clear"></div>
	</div>
</div>

<script language="javascript" type="text/javascript">
	<!--
	/* <![CDATA[ */
	$(document).ready(function() {
		colorRowMouseOver('.displayOrders tbody tr');
		$('.buttonNotice').click(function () {
			loadNotice($(this));
		});
		setFormDutyFields(<?php echo $jsonFormDutyFields; ?>);

		$(function() {
			$.datepicker.setDefaults($.datepicker.regional[""]);
			$('#editMessageNewsDateEntry').datepicker($.datepicker.regional["de"]);
			$('#editMessageNewsDateStart').datepicker($.datepicker.regional["de"]);
			$('#editMessageNewsDateEnd').datepicker($.datepicker.regional["de"]);

			var htmlButtonClearField = '<span class="buttonClearField"><img src="layout/icons/iconDelete.png" width="16" height="16" alt="" title="Eingabefeld leeren" /><\/span>';
			$('#editMessageNewsDateEntry').parent().append(htmlButtonClearField);
			$('#editMessageNewsDateStart').parent().append(htmlButtonClearField);
			$('#editMessageNewsDateEnd').parent().append(htmlButtonClearField);

			$('.buttonClearField').click(function () {
				$(this).parent().find('input').val('');
				$(this).parent().find('input').attr('checked', false);
			});
		});

		$(function() {
			$('#tabs').tabs();
		});
	});
	/* ]]> */
	// -->
</script>

<?php require_once('inc/footerHTML.inc.php'); ?>