<?php
	require_once('inc/requires.inc.php');

	if(!$arrGetUserRights["displayOnlineOrders"] && !$arrGetUserRights["importOnlineOrders"]) {
		header('location: ' . PAGE_EXIT_LOCATION);
		exit;
	}

	if($_POST["displayOrdersCount"] !="") {
		$displayOrdersCount = $_POST["displayOrdersCount"];
	}
	else {
		$displayOrdersCount = DEFAULT_ONLINE_ORDERS_MONTHS;
	}

	/*
	$arrPaymentTypes = array (
		"banktransfer" => ("banktransfer"),
		"cash" => ("cash"),
		"cc" => ("cc"),
		"cod" => ("cod"),
		"invoice" => ("invoice"),
		"moneyorder" => ("moneyorder"),
	);
	*/

	$jswindowMessage = "";
	$warningMessage = '';
	$errorMessage = '';
	$successMessage = '';
	$infoMessage = '';

	$dbConnection = new DB_Connection();
	$db_open = $dbConnection->db_connect();

	$dbConnection_ExternShop = new DB_Connection(DB_HOST_EXTERN_SHOP, '', DB_NAME_EXTERN_SHOP, DB_USER_EXTERN_SHOP, DB_PASSWORD_EXTERN_SHOP);
	$dbOpen_ExternShop = $dbConnection_ExternShop->db_connect();

	$defaultORDER = "date_purchased";
	$defaultSORT = "ASC";

	if($_POST["searchInterval"] == ""){
		$_POST["searchInterval"] = "WEEK";
	}
?>

<?php
	require_once('inc/headerHTML.inc.php');
	$thisTitle = "Statistik Online-Bestellungen";

	if($displayOrdersCount > 0) {
		$thisTitle .= ': <span class="headerSelectedEntry">Umsatz der letzten ' . $displayOrdersCount . ' Monate</span>';
	}
	else {
		$thisTitle .= ': <span class="headerSelectedEntry">Umsatz aller Monate</span>';
	}

	$headerHTML = preg_replace("/{###TITLE###}/", strip_tags($thisTitle), $headerHTML);
	echo $headerHTML;

	require_once(FILE_MENUE_TOP);
?>
<div id="mainArea">
	<div id="mainContent">
		<?php require_once(FILE_MENUE_SIDEBAR); ?>
		<div id="contentArea">
			<div id="contentAreaElements">
				<h1><?php if(SHOW_PAGE_TITLE_ICONS) { echo '<img src="' . PATH_ICONS_MENUE_TITLES . 'statistics.png" alt="" />'; } ?> <?php echo $thisTitle; ?></h1>

				<div id="searchFilterArea">
					<form name="formDisplayOrders" action="<?php echo $_SERVER["PHP_SELF"]; ?>" method="post">
						<table class="noborder" cellspacing="0" cellpadding="0">
							<tr>
								<td>
									<label for="searchInterval">Zeitraum:</label>
									<select name="searchInterval" id="searchInterval" class="inputField_130">
										<option value=""></option>
										<option value="WEEK" <?php if($_POST["searchInterval"] == 'WEEK'){ echo ' selected="selected" '; } ?> >pro Woche</option>
										<option value="MONTH" <?php if($_POST["searchInterval"] == 'MONTH'){ echo ' selected="selected" '; } ?> >pro Monat</option>
										<option value="QUARTER" <?php if($_POST["searchInterval"] == 'QUARTER'){ echo ' selected="selected" '; } ?> >pro Vierteljahr</option>
										<option value="YEAR" <?php if($_POST["searchInterval"] == 'YEAR'){ echo ' selected="selected" '; } ?> >pro Jahr</option>
									 </select>
								</td>
								<td>
									<input type="hidden" name="editID" id="editID" value="" />
									<input type="submit" name="submitSearch" class="inputButton0" value="Anzeigen" />
								</td>
							</tr>
						</table>
					</form>
				</div>

				<div class="contentDisplay">
					<?php
						if(1) {
							// BOF GET DATAS
							$where = "";
							$dateField = "";

							if($_POST["searchInterval"] == "WEEK"){
								$where .= "";
								$dateField = "
									CONCAT(
										DATE_FORMAT(`" . TABLE_SHOP_ORDERS . "`.`date_purchased`, '%Y'),
										'#',
										DATE_FORMAT(`" . TABLE_SHOP_ORDERS . "`.`date_purchased`, '%u')
									) AS `interval`
								";
							}
							else if($_POST["searchInterval"] == "MONTH"){
								// DATE_FORMAT(`" . TABLE_ORDER_INVOICES . "`.`date_purchased`, '%M') AS `interval`,
								$where .= "";

								$dateField = "
									CONCAT(
										DATE_FORMAT(`" . TABLE_SHOP_ORDERS . "`.`date_purchased`, '%Y'),
										'#',
										DATE_FORMAT(`" . TABLE_SHOP_ORDERS . "`.`date_purchased`, '%m')
									) AS `interval`
								";
							}
							else if($_POST["searchInterval"] == "QUARTER"){
								$where .= "";

								$dateField = "
									CONCAT(
										DATE_FORMAT(`" . TABLE_SHOP_ORDERS . "`.`date_purchased`, '%Y'),
										'#',
										IF(DATE_FORMAT(`" . TABLE_SHOP_ORDERS . "`.`date_purchased`, '%m') = '01' OR DATE_FORMAT(`" . TABLE_SHOP_ORDERS . "`.`date_purchased`, '%m') = '02' OR DATE_FORMAT(`" . TABLE_SHOP_ORDERS . "`.`date_purchased`, '%m') = '03', '1. Quartal',
											IF(DATE_FORMAT(`" . TABLE_SHOP_ORDERS . "`.`date_purchased`, '%m') = '04' OR DATE_FORMAT(`" . TABLE_SHOP_ORDERS . "`.`date_purchased`, '%m') = '05' OR DATE_FORMAT(`" . TABLE_SHOP_ORDERS . "`.`date_purchased`, '%m') = '06', '2. Quartal',
												IF(DATE_FORMAT(`" . TABLE_SHOP_ORDERS . "`.`date_purchased`, '%m') = '07' OR DATE_FORMAT(`" . TABLE_SHOP_ORDERS . "`.`date_purchased`, '%m') = '08' OR DATE_FORMAT(`" . TABLE_SHOP_ORDERS . "`.`date_purchased`, '%m') = '09', '3. Quartal',
													IF(DATE_FORMAT(`" . TABLE_SHOP_ORDERS . "`.`date_purchased`, '%m') = '10' OR DATE_FORMAT(`" . TABLE_SHOP_ORDERS . "`.`date_purchased`, '%m') = '11' OR DATE_FORMAT(`" . TABLE_SHOP_ORDERS . "`.`date_purchased`, '%m') = '12', '4. Quartal',
														''
													)
												)
											)
										)
									) AS `interval`
								";

							}
							else if($_POST["searchInterval"] == "YEAR"){
								$where .= "";

								$dateField = "
									CONCAT(
										DATE_FORMAT(`" . TABLE_SHOP_ORDERS . "`.`date_purchased`, '%Y'),
										'#',
										DATE_FORMAT(`" . TABLE_SHOP_ORDERS . "`.`date_purchased`, '%Y')
									) AS `interval`
								";

							}

							$arrIntervals = array();
							$arrDatas = array();

							$sql = "
									SELECT
										SUM(`" . TABLE_SHOP_ORDERS_TOTAL . "`.`value`) AS `value`,
										COUNT(`" . TABLE_SHOP_ORDERS . "`.`date_purchased`) AS `countItems`,

										" . $dateField . "

									FROM `" . TABLE_SHOP_ORDERS_TOTAL . "`

									JOIN `" . TABLE_SHOP_ORDERS . "`
									ON (`" . TABLE_SHOP_ORDERS . "`.`orders_id` = `" . TABLE_SHOP_ORDERS_TOTAL . "`.`orders_id` AND `" . TABLE_SHOP_ORDERS_TOTAL . "`.`class` = 'ot_subtotal'  )

									WHERE 1


									GROUP BY `interval`

									ORDER BY `interval` DESC
							";
							dd('sql');
							echo mysqli_error();
							$rs = $dbConnection_ExternShop->db_query($sql);

							$countTotalRows = $dbConnection_ExternShop->db_getMysqlNumRows($rs);

							while($ds = mysqli_fetch_assoc($rs)) {
								$arrIntervals[] = $ds["interval"];
								$arrLabelsGraph[$ds["interval"]] = $ds["interval"];
								$arrDatasGraph["countItems"][$ds["interval"]] = $ds["countItems"];
								// $arrDatasGraph["totalPrice"][$ds["interval"]] = $ds["totalPrice"];
								foreach(array_keys($ds) as $field){
									$arrDatas[$ds["interval"]][$field] = $ds[$field];
								}
							}
				dd('arrDatas');
							if(!empty($arrIntervals)){
								$arrIntervals = array_unique($arrIntervals);
								arsort($arrIntervals);
							}

							if($countTotalRows > 0) {
							?>
								<!-- BOF GRAPH ELEMENTS -->
								<script type="text/javascript" language="javascript" src="<?php echo PATH_RGRAPH; ?>libraries/RGraph.common.core.js" ></script>
								<script type="text/javascript" language="javascript" src="<?php echo PATH_RGRAPH; ?>libraries/RGraph.bar.js" ></script>
								<script type="text/javascript" language="javascript" src="<?php echo PATH_RGRAPH; ?>libraries/RGraph.line.js" ></script>

								<div class="adminEditArea">
									<h2>Bestellungseing&auml;nge</h2>
									<canvas id="cvs_orders" width="900" height="250">[No canvas support]</canvas>

									<h2>Bestellwert ohne MwSt.</h2>
									<canvas id="cvs_cash" width="900" height="250">[No canvas support]</canvas>
								</div>
								<!-- EOF GRAPH ELEMENTS -->
							<?php
								echo '<table cellspacing="0" cellpadding="0" width="100%" class="displayOrders">';

								echo '<tr>';
								echo '<th>Beschreibung</th>';
								echo '<th>Anzahl der Bestellungen</th>';
								echo '<th>Netto-Gesamtbetrag</th>';
								echo '<th>Brutto-Gesamtbetrag</th>';
								echo '</tr>';

								$countRow = 0;
								$thisMarker = "";

								foreach($arrIntervals as $thisIntervalKey => $thisIntervalValue) {
									if($arrDatas[$thisIntervalValue]['countItems'] > 0){
										if($countRow == 0) {

										}

										$rowClass = 'row0';
										if($countRow%2 == 0) {
											$rowClass = 'row1';
										}
										$arrTemp = explode('#', $thisIntervalValue);
										if($thisMarker != $arrTemp[0]) {
											$thisMarker = $arrTemp[0];
											echo '<tr><td colspan="8" class="tableRowTitle1">' . $arrTemp[0] . '</td></tr>';
										}

										echo '<tr class="' . $rowClass . '">';

										echo '<td style="text-align:left;">';
										if($_POST["searchInterval"] == "MONTH"){
											$thisMonth = getTimeNames($arrTemp[1], 'month', 'long');
											echo $thisMonth;
										}
										else {
											if($_POST["searchInterval"] == "WEEK"){ echo 'KW '; }
											echo $arrTemp[1];
										}
										echo '</td>';
										echo '<td style="text-align:right;">'.$arrDatas[$thisIntervalValue]["countItems"].'</td>';
										echo '<td style="text-align:right;">'.number_format($arrDatas[$thisIntervalValue]["value"], 2, ',', '.').' &euro;</td>';
										echo '<td style="text-align:right;">'.number_format($arrDatas[$thisIntervalValue]["value"] * (1 + MWST/100), 2, ',', '.').' &euro;</td>';
										echo "</tr>";
										$totalOrders += $arrDatas[$thisIntervalValue]["countItems"];
										$totalPrice += $arrDatas[$thisIntervalValue]["value"];
										if($countRow == 0) { $timeInterval = htmlentities(utf8_decode($thisMonth)).' ' . $arrDatas[$thisIntervalValue]["year"]; }
										if($countRow == ($totalRows  - 1) && $countRow > 0) { $timeInterval = htmlentities(utf8_decode($thisMonth)).' ' . $arrDatas[$thisIntervalValue]["year"] . ' - ' . $timeInterval; }
										$countRow++;
									}
								}

								echo '<tr>';
								echo '<td colspan="4"><hr /></td>';
								echo '</tr>';

								echo '<tr>';
								echo '<td colspan="4" class="tableRowTotal">Insgesamt</td>';
								echo '</tr>';

								echo '<tr>';
								echo '<td style="text-align:left;"><b>' . ($timeInterval) . ':</b></td>';
								echo '<td style="text-align:right;"><b>' . $totalOrders . '</b></td>';
								echo '<td style="text-align:right;"><b>' . number_format($totalPrice, 2, ',', '.') . ' &euro;</b></td>';
								echo '<td style="text-align:right;"><b>' . number_format($totalPrice * (1 + MWST/100), 2, ',', '.') . ' &euro;</b></td>';
								echo '</tr>';
								echo '</table>';
							}
							else {
								$warningMessage .= 'F&uuml;r den gew&auml;hlten Zeitraum wurden keine Daten gefunden.';
							}
							// EOF GET DATAS
						}
					?>
					<?php displayMessages(); ?>


						<?php



/*
							while($ds = mysqli_fetch_array($rs)) {
								$arrGraphDatasOrders[$ds["labelDate"]] = $ds["countOrders"];
								$arrGraphDatasCash[$ds["labelDate"]] = $ds["value"];

								if($markerYear != $ds["year"]) {
									echo '<tr>';
									echo '<td colspan="4" class="tableRowTitle1">' . $ds["year"] . '</td>';
									echo '</tr>';
									$markerYear = $ds["year"];
								}
								if($countRow%2 == 0){ $rowClass = 'row1'; }
								else { $rowClass = 'row0'; }

								echo '<tr class="' . $rowClass . '">';
								// $thisMonth = date("M.", mktime(0, 0, 0, $ds["month"], 1,1));
								$thisMonth = getTimeNames(strftime("%m", strtotime($ds["date_purchased"])), 'month', 'long');
								echo '<td style="text-align:left;">' . htmlentities(utf8_decode($thisMonth)).' ' . $ds["year"] . '</td>';
								echo '<td style="text-align:right;">'.$ds["countOrders"].'</td>';
								echo '<td style="text-align:right;">'.number_format($ds["value"], 2, ',', '.').' &euro;</td>';
								echo '<td style="text-align:right;">'.number_format($ds["value"] * (1 + MWST/100), 2, ',', '.').' &euro;</td>';
								echo "</tr>";
								$totalOrders += $ds["countOrders"];
								$totalPrice += $ds["value"];
								if($countRow == 0) { $timeInterval = htmlentities(utf8_decode($thisMonth)).' ' . $ds["year"]; }
								if($countRow == ($totalRows  - 1) && $countRow > 0) { $timeInterval = htmlentities(utf8_decode($thisMonth)).' ' . $ds["year"] . ' - ' . $timeInterval; }
								$countRow++;
							}
*/
						?>

				</div>
			</div>
		</div>
		<div class="clear"></div>
	</div>
</div>

<script language="javascript" type="text/javascript">
	$(document).ready(function() {
		$('.buttonShopOrderInfo').css('cursor', 'pointer');
		$('.buttonShopOrderInfo').click(function () {
			loadOrderDetails($(this), '<?php echo BASEPATH; ?>');
		});
		colorRowMouseOver('.displayOrders tbody tr');
		// toggleAreas();

		// BOF GET DATAS FOR GRAPH
			<?php
				if(!empty($arrGraphDatasOrders)){
					$arrGraphDatasOrders = array_reverse($arrGraphDatasOrders, true);
				}
				if(!empty($arrGraphDatasCash)){
					$arrGraphDatasCash = array_reverse($arrGraphDatasCash, true);
				}
			?>

			createGraph('cvs_orders', [<?php echo implode(",", array_values($arrGraphDatasOrders)); ?>], [<?php echo "'" . implode("','", array_keys($arrGraphDatasOrders)) . "'"; ?>], 'Line', [['green','red']]);
			createGraph('cvs_cash', [<?php echo implode(",", array_values($arrGraphDatasCash)); ?>], [<?php echo "'" . implode("','", array_keys($arrGraphDatasCash)) . "'"; ?>], 'Line', [['green','red']]);
		// EOF GET DATAS FOR GRAPH
	});
</script>

<?php
	$dbConnection_ExternShop->db_close();
?>

<?php require_once('inc/footerHTML.inc.php'); ?>
