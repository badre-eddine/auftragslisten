
<?php
	require_once('inc/requires.inc.php');

	if(!$arrGetUserRights["displayOnlineAcquisition"] && !$arrGetUserRights["importOnlineAcquisition"]) {
		header('location: ' . PAGE_EXIT_LOCATION);
		exit;
	}

	if($_REQUEST["editID"] == "NEW" && $_REQUEST["importCustomerDatas"] != ""){
		#dd('_GET');
		#dd('_POST');
		#dd('_REQUEST');
		$_REQUEST["importID"] = 1;
		$arrImportCustomerDatas = unserialize($_REQUEST["importCustomerDatas"]);
		#dd('arrImportCustomerDatas');
	}

	$jswindowMessage = "";
	$warningMessage = '';
	$errorMessage = '';
	$successMessage = '';
	$infoMessage = '';

	$dbConnection = new DB_Connection();
	$db_open = $dbConnection->db_connect();
?>
<?php
	require_once('inc/headerHTML.inc.php');
	$thisTitle = "Kunden-Abgleich &amp; Kunden-Import";
	if($_REQUEST["source"] == "ONLINE-SHOP"){
		$thisTitle .= ' - Online-Shop';
	}
	else if($_REQUEST["source"] == "OKE"){
		$thisTitle .= ' - Online-Kundenerfassung';
	}
	$headerHTML = preg_replace("/{###TITLE###}/", strip_tags($thisTitle), $headerHTML);
	echo $headerHTML;

	require_once(FILE_MENUE_TOP);

?>

<div id="mainArea">
	<div id="mainContent">
		<?php require_once(FILE_MENUE_SIDEBAR); ?>
		<div id="contentArea">
			<div id="contentAreaElements">
				<h1><?php if(SHOW_PAGE_TITLE_ICONS) { echo '<img src="' . PATH_ICONS_MENUE_TITLES . 'acquisition.png" alt="" />'; } ?> <?php echo $thisTitle; ?></h1>

				<div class="contentDisplay">

				<?php
					if(!empty($arrImportCustomerDatas)) {
						if($_REQUEST["importID"] > 0){
							echo '<table border="0" cellpadding="0" cellspacing="0" width="100%" class="displayOrders">
										<thead>
											<tr>
												<!--<th>ID</th>-->
												<th>Quelle</th>
												<!--<th>K-Nr.</th>-->
												<th>Kunde</th>
												<th>Datum</th>
												<th>Adresse</th>
												<th>Telefon</th>
												<th>Mail / Homepage</th>
												<th style="width:60px;">Infos</th>
											</tr>
										</thead>
										<tbody>';
							$count = 0;


							if($count%2 == 0){ $rowClass = 'row0'; }
							else { $rowClass = 'row1'; }

							echo '<tr class="'.$rowClass.'" style="vertical-align:top; font-size:11px;">';

							// echo '<td style="width:45px;">'.($arrImportCustomerDatas["customersID"]).'</td>';

							echo '<td style="white-space:nowrap;" title="' . $arrImportCustomerDatas["customersVertreterName"] . ' (Vertreter-ID: ' . $arrImportCustomerDatas["customersVertreterID"] . ')">';
							if($arrImportCustomerDatas["customersVertreterName"] != ""){
								$thisSource = $arrImportCustomerDatas["customersVertreterName"];
							}
							else if($_REQUEST["source"] == "ONLINE-SHOP"){
								$thisSource = $_REQUEST["source"];
							}
							echo $thisSource;
							echo '</td>';

							// echo '<td>' . ($arrImportCustomerDatas["customersKundennummer"]) . '</td>';

							echo '<td>';
							echo '<b>Firma:</b><br />' . preg_replace("/\//", " / ", $arrImportCustomerDatas["customersFirmenname"]) . '<br />';
							echo '<hr />';
							echo '<b>Name:</b><br />' . $arrImportCustomerDatas["firstname"] . ' ' .  $arrImportCustomerDatas["lastname"] . '<br />';
							echo '<hr />';
							echo '<b>Ansprechpartner:</b><br />' . $arrImportCustomerDatas["customersAnsprechpartner1Vorname"] . ' ' .  $arrImportCustomerDatas["customersAnsprechpartner1Nachname"] . '<br />';
							echo '</td>';

							echo '<td>';
							echo '<b>Eingetragen am:</b><br />' . formatDate($arrImportCustomerDatas["customersEntryDate"] , "display", "date").'<br />';
							echo '<hr />';
							echo '<b>Ge&auml;ndert am:</b><br />' . formatDate($arrImportCustomerDatas["customersTimeCreated"] , "display", "date");
							echo '</td>';

							echo '<td>';

							echo '<b>Firmenadresse:</b><br/>' . $arrImportCustomerDatas["customersCompanyStrasse"] . ' ' . $arrImportCustomerDatas["customersCompanyHausnummer"] . '<br />' . $arrImportCustomerDatas["customersCompanyPLZ"] . ' ' . $arrImportCustomerDatas["customersCompanyOrt"] . '<hr />';

							echo '<b>Rechnungsadresse:</b><br/>' . $arrImportCustomerDatas["customersRechnungsadresseStrasse"] . ' ' . $arrImportCustomerDatas["customersRechnungsadresseHausnummer"] . '<br />' . $arrImportCustomerDatas["customersRechnungsadressePLZ"] . ' ' . $arrImportCustomerDatas["customersRechnungsadresseOrt"] . '<hr />';
							// echo '<td>' . $arrImportCustomerDatas["fk_departement"] . '</td>';
							// echo '<td>' . $arrImportCustomerDatas["fk_pays"] . '</td>';

							// echo '<td>' . $arrImportCustomerDatas["addressDelivery"] . '</td>';
							echo '<b>Lieferadresse:</b><br/>' . $arrImportCustomerDatas["customersLieferadresseStrasse"] . ' ' . $arrImportCustomerDatas["customersLieferadresseHausnummer"] . '<br />' . $arrImportCustomerDatas["customersLieferadressePLZ"] . ' ' . $arrImportCustomerDatas["customersLieferadresseOrt"] ;
							echo '</td>';
							$compareMail_1 = $arrImportCustomerDatas["customersMail1"];
							$compareMail_2 = $arrImportCustomerDatas["customersMail2"];
							echo '<td>';
							echo '<b>Tel 1:</b> ' . formatPhoneNumber($arrImportCustomerDatas["customersTelefon1"]) . '<br />';
							echo '<b>Tel 2:</b> ' . formatPhoneNumber($arrImportCustomerDatas["customersTelefon2"]) . '<hr />';

							echo '<b>Mobil 1:</b> ' . formatPhoneNumber($arrImportCustomerDatas["customersMobil1"]) . '<br />';
							echo '<b>Mobil 2:</b> ' . formatPhoneNumber($arrImportCustomerDatas["customersMobil2"]) . '<hr />';

							echo '<b>Fax 1:</b> ' . formatPhoneNumber($arrImportCustomerDatas["customersFax1"]). '<br />';
							echo '<b>Fax 2:</b> ' . formatPhoneNumber($arrImportCustomerDatas["customersFax2"]). '</td>';

							echo '<td><b>Mail 1:</b> ' . $arrImportCustomerDatas["customersMail1"] . '<br />';
							echo '<b>Mail 2:</b> ' . $arrImportCustomerDatas["customersMail2"] . '<hr />';
							echo '<b>Homepage:</b> ' . $arrImportCustomerDatas["customersHomepage"] ;
							echo '</td>';

							echo '<td style="white-space:nowrap;">';
							if($arrImportCustomerDatas["customersNotiz"] != "") {
								echo '<span class="toolItem">
											<img src="layout/icons/iconRTF.gif" class="buttonNotice" width="16" height="16" alt="Letzter Besuchsbericht" title="Letzten Besuchsbericht anzeigen" />
								';
								echo '<div class="displayNoticeArea">';
								echo nl2br(wordwrap($arrImportCustomerDatas["customersNotiz"], WORD_WRAP_WIDTH, "\n"));
								echo '</div>';

								echo '</span>';
							}
							else {
								echo '<span class="toolItem"><img src="layout/icons/spacer.gif" width="16" height="16" alt="" /></span>';
							}

							if($arrImportCustomerDatas["customersBankName"] != "" || $arrImportCustomerDatas["customersBankLeitzahl"] != "" || $arrImportCustomerDatas["customersBankKontonummer"] != "" || $arrImportCustomerDatas["customersBankIBAN"] != "" || $arrImportCustomerDatas["customersBankBIC"] != "" || $arrImportCustomerDatas["customersKontoinhaber"] != "" || $arrImportCustomerDatas["customersBankName"] != "" || $arrImportCustomerDatas["customersBankLeitzahl"] != "" || $arrImportCustomerDatas["customersBankKontonummer"] != "" || $arrImportCustomerDatas["customersBankIBAN"] != "" || $arrImportCustomerDatas["customersBankBIC"] != "" || $arrImportCustomerDatas["customersKontoinhaber"] != "") {
								echo '
									<span class="toolItem">
										<img src="layout/icons/iconBankDatas.gif" width="16" height="16" class="buttonNotice" title="Bankdaten anzeigen" alt="Bankdaten" />';
										echo '<div class="displayNoticeArea">';
										if(1) { echo '<div class="displayNoticeElement"><h3>Bankverbindung 1</h3></div>'; }
										if(1) { echo '<div class="displayNoticeElement"><h3>Bankverbindung 2</h3></div>'; }

										if(1) { echo '<div class="displayNoticeElement"> &bull; ' . '<b>1. Bank: </b>' . $arrImportCustomerDatas["customersBankName"] . '</div>'; }
										if(1) { echo '<div class="displayNoticeElement"> &bull; ' . '<b>2. Bank: </b>' . $arrImportCustomerDatas["customersBankName"] . '</div>'; }

										if(1) { echo '<div class="displayNoticeElement"> &bull; ' . '<b>1. BLZ: </b>' . $arrImportCustomerDatas["customersBankLeitzahl"] . '</div>'; }
										if(1) { echo '<div class="displayNoticeElement"> &bull; ' . '<b>2. BLZ: </b>' . $arrImportCustomerDatas["customersBankLeitzahl"] . '</div>'; }

										if(1) { echo '<div class="displayNoticeElement"> &bull; ' . '<b>1. Konto-Nr: </b>' . $arrImportCustomerDatas["customersBankKontonummer"] . '</div>'; }
										if(1) { echo '<div class="displayNoticeElement"> &bull; ' . '<b>2. Konto-Nr: </b>' . $arrImportCustomerDatas["customersBankKontonummer"] . '</div>'; }

										if(1) { echo '<div class="displayNoticeElement"> &bull; ' . '<b>1. IBAN: </b>' . '' . $arrImportCustomerDatas["customersBankIBAN"] . '</div>'; }
										if(1) { echo '<div class="displayNoticeElement"> &bull; ' . '<b>2. IBAN: </b>' . $arrImportCustomerDatas["customersBankIBAN"] . '</div>'; }

										if(1) { echo '<div class="displayNoticeElement"> &bull; ' . '<b>1. BIC: </b>' . $arrImportCustomerDatas["customersBankBIC"] . '</div>'; }
										if(1) { echo '<div class="displayNoticeElement"> &bull; ' . '<b>2. BIC: </b>' . $arrImportCustomerDatas["customersBankBIC"] . '</div>'; }

										if(1) { echo '<div class="displayNoticeElement"> &bull; ' . '<b>1. Kontoinh.: </b>' . $arrImportCustomerDatas["customersKontoinhaber"] . '</div>'; }
										if(1) { echo '<div class="displayNoticeElement"> &bull; ' . '<b>2. Kontoin.: </b>' . $arrImportCustomerDatas["customersKontoinhaber"] . '</div>'; }

								echo '</div>';
								echo '</span>';
							}
							else {
								echo '<span class="toolItem"><img src="layout/icons/spacer.gif" width="16" height="16" alt="" /></span>';
							}

							if(!empty($arrSelectedCustomerFiles[$arrImportCustomerDatas["customersID"] ])) {
								echo '
									<span class="toolItem">
										<img src="layout/icons/iconDocuments.gif" width="16" height="16" class="buttonNotice" title="Angeh&auml;ngte Dokumente anzeigen" alt="Dokumente" />';
										echo '<div class="displayNoticeArea">';

										echo '<table class="border" cellpadding="0" cellspacing="0" width="100%">';
										echo '<tbody>';

										echo '<tr>';
										echo '<th>Dateiname</th>';
										echo '<th>Upload-Datum</th>';
										echo '<th>Dateigr&ouml;&szlig;e</th>';
										echo '<th>Datei &ouml;ffnen</th>';
										echo '</tr>';

										$countRow = 0;
										foreach ($arrSelectedCustomerFiles[$arrImportCustomerDatas["customersID"] ] as $thisKey => $thisValue) {
											if($countRow%2 == 0){ $rowClass = 'row1'; }
											else { $rowClass = 'row0'; }
											echo '<tr class="' . $rowClass . '">';
											$thisFilename = $thisValue["filename"];
											if($thisValue["filename"] == '') {
												$thisFilename = getFilename($thisValue["path"]);
											}
											echo '<td><a href="' . PATH_ONLINE_ACQUISITION . $thisValue["path"] . '" title="' . basename($thisValue["path"]) . '" target="_blank" >' . $thisFilename . '</a></td>';
											echo '<td>' . formatDate($thisValue["uploadtime"], 'display', 'date') . '</td>';
											echo '<td>' . $thisValue["filesize"] . ' KB</td>';
											echo '<td>' . '<a href="' . PATH_ONLINE_ACQUISITION . $thisValue["path"] . '" target="_blank" >' . '<img src="layout/icons/icon' . strtoupper(getFileType(basename($thisValue["path"]))) . '.gif" alt="' . getFileType(basename($thisValue["path"])) . '-Datei anzeigen" title="' . getFileType(basename($thisValue["path"])) . '-Datei anzeigen" /></a></td>';
											$countRow++;
											echo '</tr>';
										}

										echo '</tbody>';
										echo '</table>';

								echo '</div>';
								echo '</span>';
							}
							else {
								echo '<span class="toolItem"><img src="layout/icons/spacer.gif" width="16" height="16" alt="" /></span>';
							}

							echo '</td>';

							echo '</tr>';

							$count++;
							$i++;

							echo '</tbody>';
							echo '</table>';
						}
					}

					if((int)$_REQUEST["importID"] > 0) {
						echo '<br />';
						echo '<h2 class="legend">Abgleich oder Import der vom Verteter eingetragene Daten mit den Auftragslisten-Kunden</h2>';


							if((int)$_REQUEST["importID"] > 0) {
							// BOF FIND EXISTING CUSTOMERS DEPENDING ON MAIL-ADDRESS
								$sqlCompareWHERE = array();
								if($compareMail_1 != '') {
									$sqlCompareWHERE[] = " `customersMail1` = '" . $compareMail_1 . "' ";
									$sqlCompareWHERE[] = " `customersMail2` = '" . $compareMail_1 . "' ";
								}
								if($compareMail_2 != '') {
									$sqlCompareWHERE[] = " `customersMail1` = '" . $compareMail_2 . "' ";
									$sqlCompareWHERE[] = " `customersMail2` = '" . $compareMail_2 . "' ";
								}

								$sqlCompare = "SELECT
												*
												FROM `" . TABLE_CUSTOMERS . "`" . "
												WHERE 1
									";
								if(!empty($sqlCompareWHERE)){
									$sqlCompareWHERE = ' AND ' .implode(' OR ', $sqlCompareWHERE);
									$sqlCompare .= $sqlCompareWHERE;
								}
								else {
									$infoMessage .= 'Der Vergleich per Mailadresse kann nicht durchgef&uuml;hrt werden, da keine Mailadresse eingetragen ist.' . '<br />';
									$sqlCompare = '';
								}
#dd('sqlCompare');
								if($sqlCompare != ''){
									$rs = $dbConnection->db_query($sqlCompare);
									if(!$rs) {
										$errorMessage .= ' Beim Abgleich der Daten ist ein Fehler aufgetreten. ' . '<br />';
									}
									else {
										$countRows = $dbConnection->db_getMysqlNumRows($rs);

										if($countRows > 0) {
											while($ds = mysqli_fetch_array($rs)){
												foreach(array_keys($ds) as $field){
													$arrFoundDatas[$ds["customersID"]][$field] = $ds[$field];
												}
											}
											$infoMessage .= ' Beim Vergleich der Kunden-Daten mit den vorhandenen Auftragslisten-Kunden-Daten aufgrund der Mail-Adresse wurden m&ouml;gliche &uuml;bereinstimmende Daten gefunden. ' . '<br />';
										}
										else {
											$infoMessage .= ' Der Vergleich der Kunden-Daten mit den vorhandenen Auftragslisten-Kunden-Daten aufgrund der Mail-Adresse lieferte kein Ergebnis. ' . '<br />';
										}
										displayMessages();
									}
								}
							// EOF FIND EXISTING CUSTOMERS DEPENDING ON MAIL-ADDRESS

							displayMessages();

							// BOF FIND EXISTING CUSTOMERS DEPENDING ON ADDRESS
								if(empty($arrFoundDatas)){
									$sqlCompare = "SELECT
												*
												FROM `" . TABLE_CUSTOMERS . "`" . "
												WHERE 1
													AND LOWER(REPLACE(REPLACE(REPLACE(CONCAT(`customersCompanyStrasse`, `customersCompanyHausnummer`), ' ', ''), 'strasse', 'str.'), 'straße', 'str.')) = '" .  strtolower(preg_replace("/ /", "", $arrImportCustomerDatas["customersCompanyStrasse"] . $arrImportCustomerDatas["customersCompanyHausnummer"])) . "'
													AND `customersCompanyPLZ` = '" . $arrImportCustomerDatas["customersCompanyPLZ"] . "'
													AND `customersCompanyCountry` = '" . $arrImportCustomerDatas["customersCompanyCountry"] . "'
										";
									$rs = $dbConnection->db_query($sqlCompare);

#dd('sqlCompare');
									if(!$rs) {
										$errorMessage .= ' Beim Abgleich der Daten ist ein Fehler aufgetreten. ' . '<br />';
									}
									else {
										$countRows = $dbConnection->db_getMysqlNumRows($rs);

										if($countRows > 0) {
											while($ds = mysqli_fetch_array($rs)){
												foreach(array_keys($ds) as $field){
													$arrFoundDatas[$ds["customersID"]][$field] = $ds[$field];
												}
											}
											$infoMessage .= ' Beim Vergleich der Kunden-Daten mit den vorhandenen Auftragslisten-Kunden-Daten aufgrund der Anschrift wurden m&ouml;gliche &uuml;bereinstimmende Daten gefunden. ' . '<br />';
										}
										else {
											$infoMessage .= ' Der Vergleich der Kunden-Daten mit den vorhandenen Auftragslisten-Kunden-Daten aufgrund der Anschrift lieferte kein Ergebnis. ' . '<br />';
										}
										displayMessages();
									}
								}
							// EOF FIND EXISTING CUSTOMERS DEPENDING ON ADDRESS

							// BOF FIND EXISTING CUSTOMERS DEPENDING ON PHONE NUMBERS
								if(empty($arrFoundDatas)){

								}
							// EOF FIND EXISTING CUSTOMERS DEPENDING ON PHONE NUMBERS
						}


						displayMessages();

						if(!empty($arrFoundDatas)){
							echo '<table cellpadding="0" cellspacing="0" width="100%" class="displayOrders">';
							echo '<tr>';
							echo '<th style="width:45px;text-align:right;">#</th>';
							echo '<th>Kundennummer</th>';
							echo '<th>Kundenname</th>';
							echo '<th>PLZ</th>';
							echo '<th>Ort</th>';
							echo '<th>Stra&szlig;e / Hausnummer</th>';
							echo '</tr>';
							$count = 0;
							foreach($arrFoundDatas as $thisKey => $thisValue){
								if($count%2 == 0){ $rowClass = 'row0'; }
								else { $rowClass = 'row1'; }

								echo '<tr class="'.$rowClass.'">';
								echo '<td style="text-align:right;">' . ($count + 1) . '</td>';
								echo '<td><a href="' . PAGE_EDIT_CUSTOMER . '?editCustomerNumber=' . ($thisValue["customersKundennummer"]) . '">' . ($thisValue["customersKundennummer"]) . '</a></td>';
								echo '<td>' . ($thisValue["customersFirmenname"]) . '</td>';
								echo '<td>' . ($thisValue["customersCompanyPLZ"]) . '</td>';
								echo '<td>' . ($thisValue["customersCompanyOrt"]) . '</td>';
								echo '<td>' . ($thisValue["customersLieferadresseStrasse"]) . ' ' . ($thisValue["customersLieferadresseHausnummer"]) . '</td>';
								echo '</tr>';
							}
							echo '</table>';
						}
						else {
							?>
							<hr />
							<h2>Kunden suchen</h2>
							<div id="searchFilterArea">
								<form name="formFilterSearch" method="post" action="<?php echo PAGE_EDIT_CUSTOMER; ?>" target="_blank">
								<table border="0" cellpadding="0" cellspacing="0" width="" class="searchFilterContent">
									<tr>
										<td>
											<label for="searchPLZ">PLZ:</label>
											<input type="text" name="searchPLZ" id="searchPLZ" class="inputField_40" value="<?php if( $_REQUEST["searchPLZ"] != '') { echo $_REQUEST["searchPLZ"]; } else { echo $arrImportCustomerDatas["customersCompanyPLZ"]; }  ?>" />
										</td>
										<td>
											<label for="searchWord">Suchbegriff:</label>
											<input type="text" name="searchWord" id="searchWord" class="inputField_70" value="<?php echo $_REQUEST["searchWord"]; ?>" />
										</td>
										<td>
											<input type="hidden" name="editID" id="editID" value="" />
											<input type="submit" name="submitSearch" class="inputButton0" value="Suchen" />
										</td>
									</tr>
								</table>
								</form>
							</div>

							<?php
								}

								if($arrGetUserRights["importOnlineAcquisition"]){
									echo '<hr />';
									echo '<h2>Daten als Neukunden importieren</h2>';

									/*
									if(!empty($arrOnlineCustomerDatas)){
										foreach($arrOnlineCustomerDatas as $thisOnlineCustomerDatasKey => $thisOnlineCustomerDatasValue){
											$arrOnlineCustomerDatas[$thisOnlineCustomerDatasKey] = stripslashes($thisOnlineCustomerDatasValue);
										}
									}
									*/

									$importURL = '';
									$importURL .= PAGE_EDIT_CUSTOMER;
									$importURL .= '?editID=NEW';
									$importURL .= '&importCustomerDatas=' . urlencode(serialize($arrImportCustomerDatas));
									#echo $importURL . '<br />';
									echo '<a href="' . (($importURL)) . '" >Diesen Kunden in der Kundenverwaltung anlegen</a>';
								}

						}
						displayMessages();
					?>
				</div>
			</div>
		</div>
		<div class="clear"></div>
	</div>
</div>

<script language="javascript" type="text/javascript">
	$(document).ready(function() {
		$('#searchSalesman').keyup(function () {
			// loadSearchSuggestions('#searchSalesman', '<?php echo $_SESSION["usersID"]; ?>');
		});
		$('#searchSalesman').keyup(function () {
			loadSuggestions('searchSalesmen', [{'triggerElement': '#searchSalesman', 'fieldName': '#searchSalesman', 'fieldNumber': '', 'fieldID': ''}], 1);
		});

		$('#searchSalesman').click(function () {
			$('#searchWord').val('');
		});
		$('#searchWord').click(function () {
			$('#searchSalesman').val('');
		});

		$('.buttonNotice').click(function () {
			loadNotice($(this));
		});
		setFocus('formFilterSearch', 'searchWord');
		colorRowMouseOver('.displayOrders tbody tr');
		$('.buttonShopOrderInfo').css('cursor', 'pointer');
		$('.buttonShopOrderInfo').click(function () {
			loadOrderDetails($(this), '<?php echo BASEPATH; ?>');
		});

		// toggleAreas();
	});
</script>

<?php require_once('inc/footerHTML.inc.php'); ?>