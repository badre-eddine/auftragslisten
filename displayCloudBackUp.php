<?php
	require_once('inc/requires.inc.php');

	if(!$arrGetUserRights["createTotalBackUp"]) {
		#header('location: ' . PAGE_EXIT_LOCATION);
		#exit;
	}

	$jswindowMessage = '';
	$warningMessage = '';
	$errorMessage = '';
	$successMessage = '';
	$infoMessage = '';


	$dbConnection = new DB_Connection();
	$db_open = $dbConnection->db_connect();
?>
<?php
	require_once('inc/headerHTML.inc.php');
	$thisTitle = "Back-Up auf Cloud sichern";

	$headerHTML = preg_replace("/{###TITLE###}/", strip_tags($thisTitle), $headerHTML);
	echo $headerHTML;

	require_once(FILE_MENUE_TOP);
?>

<div id="mainArea">
	<div id="mainContent">
		<?php require_once(FILE_MENUE_SIDEBAR); ?>
		<div id="contentArea">
			<a name="top"></a>
			<div id="contentAreaElements">
				<h1><?php if(SHOW_PAGE_TITLE_ICONS) { echo '<img src="' . PATH_ICONS_MENUE_TITLES . 'tracking.png" alt="" />'; } ?> <?php echo $thisTitle; ?></h1>

				<div class="contentDisplay">
					<?php displayMessages(); ?>
					<?php
						echo '<iframe id="iframeTracking" name="iframeTracking" class="iFrameModule" src="' . PAGE_ONLINE_BACKUP . '"></iframe>';
					?>
				</div>

			</div>
		</div>
		<div class="clear"></div>
	</div>
</div>
<script language="javascript" type="text/javascript">
	$(document).ready(function() {
		$(function() {
			$.datepicker.setDefaults($.datepicker.regional[""]);
			$('#exportOrdersFreigabeDatumFrom').datepicker($.datepicker.regional["de"]);
			$('#exportOrdersFreigabeDatumTo').datepicker($.datepicker.regional["de"]);
		});
	});
</script>
<?php require_once('inc/footerHTML.inc.php'); ?>