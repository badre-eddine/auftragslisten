REM BOF FIND FILES NEWER THAN DATE

	echo off

	REM C:\Auftragslisten_SERVER\webroot\Auftragslisten\cronjobs\getNewFiles.bat

	REM BOF SET TIMESTAMP AND PARAMS
	REM set CRONJOB_SAVESTAMP=%DATE:/=-%@%TIME::=-%
	REM set CRONJOB_SAVESTAMP=%DATE:/=-%_%TIME::=-%
	set CRONJOB_SAVESTAMP_HOUR=%time:~0,2%
	if "%CRONJOB_SAVESTAMP_HOUR:~0,1%" == " " set CRONJOB_SAVESTAMP_HOUR=0%CRONJOB_SAVESTAMP_HOUR:~1,1%
	REM ---------------------------------------------------------------------------------------------------------------
	set CRONJOB_SAVESTAMP_MINUTE=%time:~3,2%
	if "%CRONJOB_SAVESTAMP_MINUTE:~0,1%" == " " set CRONJOB_SAVESTAMP_MINUTE=0%CRONJOB_SAVESTAMP_MINUTE:~1,1%
	REM ---------------------------------------------------------------------------------------------------------------
	set CRONJOB_SAVESTAMP_SECONDS=%time:~6,2%
	if "%CRONJOB_SAVESTAMP_SECONDS:~0,1%" == " " set CRONJOB_SAVESTAMP_SECONDS=0%CRONJOB_SAVESTAMP_SECONDS:~1,1%
	REM ---------------------------------------------------------------------------------------------------------------
	set CRONJOB_SAVESTAMP_YEAR=%date:~-4%
	set CRONJOB_SAVESTAMP_MONTH=%date:~3,2%
	if "%CRONJOB_SAVESTAMP_MONTH:~0,1%" == " " set CRONJOB_SAVESTAMP_MONTH=0%CRONJOB_SAVESTAMP_MONTH:~1,1%
	set CRONJOB_SAVESTAMP_DAY=%date:~0,2%
	if "%CRONJOB_SAVESTAMP_DAY:~0,1%" == " " set CRONJOB_SAVESTAMP_DAY=0%CRONJOB_SAVESTAMP_DAY:~1,1%
	REM ---------------------------------------------------------------------------------------------------------------
	set CRONJOB_SAVESTAMP=%CRONJOB_SAVESTAMP_YEAR%-%CRONJOB_SAVESTAMP_MONTH%-%CRONJOB_SAVESTAMP_DAY%_%CRONJOB_SAVESTAMP_HOUR%-%CRONJOB_SAVESTAMP_MINUTE%-%CRONJOB_SAVESTAMP_SECONDS%
	REM echo CRONJOB_SAVESTAMP=%CRONJOB_SAVESTAMP%
REM EOF SET TIMESTAMP AND PARAMS

REM BOF SET NEW FILES BACK UP SETTINGS
	REM set BACKUP_START_DATE_NEW_FILES=10.05.2017
	set BACKUP_START_DATE_NEW_FILES=14.05.2017
	set BACKUP_PATH_NEW_FILES=Z:\Software\Auftragslisten_SICHERUNGEN\newFiles
	set LIST_NEW_FILES=C:\Auftragslisten_SERVER\webroot\Auftragslisten\cronjobs\newFiles_LIST.txt
	set LIST_NEW_FILES_COPY_COMMANDS=C:\Auftragslisten_SERVER\webroot\Auftragslisten\cronjobs\newFiles_COPY_COMMANDS.txt
	set LIST_NEW_FILES_COPY_RUN=C:\Auftragslisten_SERVER\webroot\Auftragslisten\cronjobs\newFiles_COPY_COMMANDS.bat
	set LIST_NEW_FILES_TMP=C:\Auftragslisten_SERVER\webroot\Auftragslisten\cronjobs\newFiles_COPY_TMP.txt
	set SOURCE_PATH=C:\Auftragslisten_SERVER\webroot\Auftragslisten\documents_bctr
REM EOF SET NEW FILES BACK UP SETTINGS

REM ---------------------------------------------------------------------------------------------------------------

REM BOF	CLEAR LIST FILES
	type nul >%LIST_NEW_FILES%
	type nul >%LIST_NEW_FILES_COPY_COMMANDS%
	type nul >%LIST_NEW_FILES_COPY_RUN%
REM EOF	CLEAR LIST FILES

REM ---------------------------------------------------------------------------------------------------------------

REM BOF GET NEW FILES

	REM forfiles /M myfile.txt /C "cmd /c echo @fdate @ftime"
	REM #forfiles /p %PATH_CLOUD_BACKUP% /s /m *.* /c "cmd /c del @path" /d %KEEP_BACKUP_FILES_DAY_LIMIT_CLOUD%

	REM forfiles /p "C:\Auftragslisten_SERVER\webroot\Auftragslisten\documents_bctr" /s /m *.* /C "cmd /c echo @path - @fdate @ftime" /d -2 >>C:\Auftragslisten_SERVER\webroot\Auftragslisten\cronjobs\listNewFiles.txt
	REM forfiles /p "C:\Auftragslisten_SERVER\webroot\Auftragslisten\documents_bctr" /s /m *.* /C "cmd /c echo @path - @fdate @ftime" /D 14.05.2017 >>C:\Auftragslisten_SERVER\webroot\Auftragslisten\cronjobs\listNewFiles.txt
	REM forfiles /p %SOURCE_PATH% /s /m *.* /C "cmd /c echo @path" /D %BACKUP_START_DATE_NEW_FILES% >>%LIST_NEW_FILES%

	forfiles /p "C:\Auftragslisten_SERVER\webroot\Auftragslisten\documents_b3" /s /m *.* /C "cmd /c echo @path" /D %BACKUP_START_DATE_NEW_FILES% >>%LIST_NEW_FILES%
	forfiles /p "C:\Auftragslisten_SERVER\webroot\Auftragslisten\documents_bctr" /s /m *.* /C "cmd /c echo @path" /D %BACKUP_START_DATE_NEW_FILES% >>%LIST_NEW_FILES%
	forfiles /p "C:\Auftragslisten_SERVER\webroot\Auftragslisten\documents_common" /s /m *.* /C "cmd /c echo @path" /D %BACKUP_START_DATE_NEW_FILES% >>%LIST_NEW_FILES%
	forfiles /p "C:\Auftragslisten_SERVER\webroot\Auftragslisten\dpdTrackingFilesInterface" /s /m *.* /C "cmd /c echo @path" /D %BACKUP_START_DATE_NEW_FILES% >>%LIST_NEW_FILES%
	forfiles /p "C:\Auftragslisten_SERVER\webroot\Auftragslisten\exportedFactoringFiles" /s /m *.* /C "cmd /c echo @path" /D %BACKUP_START_DATE_NEW_FILES% >>%LIST_NEW_FILES%
	forfiles /p "C:\Auftragslisten_SERVER\webroot\Auftragslisten\importedFactoringFiles" /s /m *.* /C "cmd /c echo @path" /D %BACKUP_START_DATE_NEW_FILES% >>%LIST_NEW_FILES%

REM BOF GET NEW FILES

REM ---------------------------------------------------------------------------------------------------------------

REM BOF DELETE OLDER COPOIED FILES
	rmdir Z:\Software\Auftragslisten_SICHERUNGEN\newFiles\Auftragslisten /s /q
REM EOF DELETE OLDER COPOIED FILES

REM ---------------------------------------------------------------------------------------------------------------

REM BOF COPY NEW FILES
	setlocal enableDelayedExpansion

	for /f %%T in (%LIST_NEW_FILES%) do (

		REM BOF DEFINE PATH_COPY_SOURCE
			set PATH_COPY_SOURCE=%%~fT
		REM EOF DEFINE PATH_COPY_SOURCE

		REM BOF DEFINE PATH_COPY_TARGET
			set tmpVar=%%~dpT
			set tmpVar2=!tmpVar:C:\Auftragslisten_SERVER\webroot\=Z:\Software\Auftragslisten_SICHERUNGEN\newFiles\!
			set PATH_COPY_TARGET=!tmpVar2!
		REM EOF DEFINE PATH_COPY_TARGET

		echo xcopy "!PATH_COPY_SOURCE!" "!PATH_COPY_TARGET!" /A >>%LIST_NEW_FILES_COPY_COMMANDS%		

		REM echo %%~fT
		REM echo %%~dpT

		REM echo full path: %%~fT >>%LIST_NEW_FILES_COPY_COMMANDS%
		REM echo directory: %%~dT >>%LIST_NEW_FILES_COPY_COMMANDS%
		REM echo path: %%~pT >>%LIST_NEW_FILES_COPY_COMMANDS%
		REM echo file name only: %%~nT >>%LIST_NEW_FILES_COPY_COMMANDS%
		REM echo extension only: %%~xT >>%LIST_NEW_FILES_COPY_COMMANDS%
		REM echo expanded path with short names: %%~sT >>%LIST_NEW_FILES_COPY_COMMANDS%
		REM echo attributes: %%~aT >>%LIST_NEW_FILES_COPY_COMMANDS%
		REM echo date and time: %%~tT >>%LIST_NEW_FILES_COPY_COMMANDS%
		REM echo size: %%~zT >>%LIST_NEW_FILES_COPY_COMMANDS%
		REM echo drive + path: %%~dpT >>%LIST_NEW_FILES_COPY_COMMANDS%
		REM echo name.ext: %%~nxT >>%LIST_NEW_FILES_COPY_COMMANDS%
		REM echo full path + short name: %%~fsT >>%LIST_NEW_FILES_COPY_COMMANDS%)
	)

REM BOF COPY NEW FILES

REM ---------------------------------------------------------------------------------------------------------------

REM BOF RUN COPY NEW FILES
	if exist "%LIST_NEW_FILES_COPY_COMMANDS%" (
		echo DATEI "%LIST_NEW_FILES_COPY_COMMANDS%" existiert
		copy "%LIST_NEW_FILES_COPY_COMMANDS%" "%LIST_NEW_FILES_COPY_RUN%"
		if exist "%LIST_NEW_FILES_COPY_RUN%" (
			echo DATEI "%LIST_NEW_FILES_COPY_RUN%" existiert
			call %LIST_NEW_FILES_COPY_RUN%
		) else (
			echo DATEI "%LIST_NEW_FILES_COPY_RUN%" existiert nicht
		)
	) else (
		echo DATEI "%LIST_NEW_FILES_COPY_COMMANDS%" existiert nicht
	)
REM EOF  RUN COPY NEW FILES