ECHO ON
REM ---------------------------------------------------------------------------------------------------------------
REM ---------------------------------------------------------------------------------------------------------------
REM BOF SET TIMESTAMP AND PARAMS
REM set CRONJOB_SAVESTAMP=%DATE:/=-%@%TIME::=-%
REM set CRONJOB_SAVESTAMP=%DATE:/=-%_%TIME::=-%
set CRONJOB_SAVESTAMP_HOUR=%time:~0,2%
if "%CRONJOB_SAVESTAMP_HOUR:~0,1%" == " " set CRONJOB_SAVESTAMP_HOUR=0%CRONJOB_SAVESTAMP_HOUR:~1,1%
REM ---------------------------------------------------------------------------------------------------------------
set CRONJOB_SAVESTAMP_MINUTE=%time:~3,2%
if "%CRONJOB_SAVESTAMP_MINUTE:~0,1%" == " " set CRONJOB_SAVESTAMP_MINUTE=0%CRONJOB_SAVESTAMP_MINUTE:~1,1%
REM ---------------------------------------------------------------------------------------------------------------
set CRONJOB_SAVESTAMP_SECONDS=%time:~6,2%
if "%CRONJOB_SAVESTAMP_SECONDS:~0,1%" == " " set CRONJOB_SAVESTAMP_SECONDS=0%CRONJOB_SAVESTAMP_SECONDS:~1,1%
REM ---------------------------------------------------------------------------------------------------------------
set CRONJOB_SAVESTAMP_YEAR=%date:~-4%
set CRONJOB_SAVESTAMP_MONTH=%date:~3,2%
if "%CRONJOB_SAVESTAMP_MONTH:~0,1%" == " " set CRONJOB_SAVESTAMP_MONTH=0%CRONJOB_SAVESTAMP_MONTH:~1,1%
set CRONJOB_SAVESTAMP_DAY=%date:~0,2%
if "%CRONJOB_SAVESTAMP_DAY:~0,1%" == " " set CRONJOB_SAVESTAMP_DAY=0%CRONJOB_SAVESTAMP_DAY:~1,1%
REM ---------------------------------------------------------------------------------------------------------------
set CRONJOB_SAVESTAMP=%CRONJOB_SAVESTAMP_YEAR%-%CRONJOB_SAVESTAMP_MONTH%-%CRONJOB_SAVESTAMP_DAY%_%CRONJOB_SAVESTAMP_HOUR%-%CRONJOB_SAVESTAMP_MINUTE%-%CRONJOB_SAVESTAMP_SECONDS%
REM echo CRONJOB_SAVESTAMP=%CRONJOB_SAVESTAMP%
REM EOF SET TIMESTAMP AND PARAMS
REM ---------------------------------------------------------------------------------------------------------------
REM ---------------------------------------------------------------------------------------------------------------
REM BOF SET FILENAMES
set PATH_Auftragslisten=C:\Auftragslisten_SERVER\webroot\Auftragslisten\
set CRONJOB_DIR_EXECUTE_FILES=C:\Auftragslisten_SERVER\webroot\Auftragslisten\cronjobs\
set CRONJOB_DIR_TEMP_FILES=C:\Auftragslisten_SERVER\webroot\Auftragslisten\cronjobs\tmp\
set CRONJOB_DIR_LOG_FILES=C:\Auftragslisten_SERVER\webroot\Auftragslisten\cronjobs\logs\
set CRONJOB_BACKUP_PATH=Z:\Software\Auftragslisten_SICHERUNGEN\cronBackUp\
set CRONJOB_FILENAME_BACKUP_FILES=Auftragslisten_backup_%CRONJOB_SAVESTAMP%_Files
set CRONJOB_FILENAME_BACKUP_DATABASE=Auftragslisten_backup_%CRONJOB_SAVESTAMP%_Database
set CRONJOB_FILENAME_BACKUP_TABLES=Auftragslisten_backup_%CRONJOB_SAVESTAMP%_DatabaseTables
set PATH_CLOUD_BACKUP=C:\CLOUD_BACKUP\MagentaCLOUD\Auftragslisten\
REM ---------------------------------------------------------------------------------------------------------------
set PATH_ZIP=C:\Programme\7-Zip\7z.exe
set PATH_PHP=C:\Auftragslisten_SERVER\php\php.exe
set PATH_MYSQLDUMP=C:\Auftragslisten_SERVER\mysql\bin\mysqldump.exe
set PATH_MYSQL=C:\Auftragslisten_SERVER\mysql\bin\mysql.exe
REM EOF SET FILENAMES
REM ---------------------------------------------------------------------------------------------------------------
REM ---------------------------------------------------------------------------------------------------------------
REM BOF SET NAME OF DATABASE
set mysqli_DATABASE_DB_NAME=burhanctrAuftragslisten
set mysqli_DATABASE_DB_USER=burhan
set mysqli_DATABASE_DB_PASS=burhan
set mysqli_DATABASE_DB_HOST=localhost
REM EOF SET NAME OF DATABASE
REM ---------------------------------------------------------------------------------------------------------------
REM ---------------------------------------------------------------------------------------------------------------
REM BOF DELETE OLDER BACKUP FILES IN CLOUD
set KEEP_BACKUP_FILES_DAY_LIMIT_CLOUD=-1
REM forfiles /p "C:\source_folder" /s /m *.* /c "cmd /c Del @path" /d -7
forfiles /p %PATH_CLOUD_BACKUP% /s /m *.* /c "cmd /c del @path" /d %KEEP_BACKUP_FILES_DAY_LIMIT_CLOUD%
REM EOF DELETE OLDER BACKUP FILES IN CLOUD
REM ---------------------------------------------------------------------------------------------------------------
REM ---------------------------------------------------------------------------------------------------------------
REM BOF BACK UP MYSQL DATABASE ALL TABLES IN ONE FILE
REM %PATH_MYSQLDUMP%  --databases burhanctrAuftragslisten -hlocalhost -u %mysqli_DATABASE_DB_USER% -p%mysqli_DATABASE_DB_PASS% > "%CRONJOB_BACKUP_PATH%%CRONJOB_FILENAME_BACKUP_DATABASE%.sql"
REM %PATH_ZIP% a "%CRONJOB_BACKUP_PATH%%CRONJOB_FILENAME_BACKUP_DATABASE%.7z" "%CRONJOB_BACKUP_PATH%%CRONJOB_FILENAME_BACKUP_DATABASE%.sql"
REM copy %CRONJOB_BACKUP_PATH%%CRONJOB_FILENAME_BACKUP_DATABASE%.7z %PATH_CLOUD_BACKUP%%CRONJOB_FILENAME_BACKUP_DATABASE%.7z
REM del %CRONJOB_BACKUP_PATH%%CRONJOB_FILENAME_BACKUP_DATABASE%.sql
REM EOF BACK UP MYSQL DATABASE IN ONE FILE IN ONE FILE
REM ---------------------------------------------------------------------------------------------------------------
REM ---------------------------------------------------------------------------------------------------------------
REM BOF BACK UP MYSQL DATABASE EACH TABLES IN ONE FILE
set FILE_TABLE_LIST=%CRONJOB_DIR_LOG_FILES%listBackUpTables.txt
%PATH_MYSQL% -u %mysqli_DATABASE_DB_USER% -p%mysqli_DATABASE_DB_PASS% %mysqli_DATABASE_DB_NAME% -e "SHOW TABLES;" --skip-column-names > %FILE_TABLE_LIST%
for /f %%T in (%FILE_TABLE_LIST%) do (
	%PATH_MYSQLDUMP% -u %mysqli_DATABASE_DB_USER% -p%mysqli_DATABASE_DB_PASS% %mysqli_DATABASE_DB_NAME% %%T > %CRONJOB_DIR_TEMP_FILES%%%T.sql
)
%PATH_ZIP% a -tzip "%CRONJOB_DIR_TEMP_FILES%%CRONJOB_FILENAME_BACKUP_TABLES%.zip" "%CRONJOB_DIR_TEMP_FILES%*.sql"
copy %CRONJOB_DIR_TEMP_FILES%%CRONJOB_FILENAME_BACKUP_TABLES%.zip %CRONJOB_BACKUP_PATH%%CRONJOB_FILENAME_BACKUP_TABLES%.zip
REM copy %CRONJOB_BACKUP_PATH%%CRONJOB_FILENAME_BACKUP_TABLES%.zip %PATH_CLOUD_BACKUP%%CRONJOB_FILENAME_BACKUP_TABLES%.zip
del %CRONJOB_DIR_TEMP_FILES%%CRONJOB_FILENAME_BACKUP_TABLES%.zip
del %CRONJOB_DIR_TEMP_FILES%*.sql
del %FILE_TABLE_LIST%
REM EOF BACK UP MYSQL DATABASE EACH TABLES IN ONE FILE
REM ---------------------------------------------------------------------------------------------------------------
REM ---------------------------------------------------------------------------------------------------------------
REM BOF BACKUP AND ZIP FILES
REM %PATH_ZIP% a %CRONJOB_BACKUP_PATH%%CRONJOB_FILENAME_BACKUP_FILES%.7z %PATH_Auftragslisten%
REM %PATH_ZIP% a -tzip "%CRONJOB_BACKUP_PATH%%CRONJOB_FILENAME_BACKUP_FILES%.zip" "%PATH_Auftragslisten%" -r -x!_tmp/
REM %PATH_ZIP% a -tzip -mx3 "%CRONJOB_BACKUP_PATH%%CRONJOB_FILENAME_BACKUP_FILES%.zip" "%PATH_Auftragslisten%" -r -x!_tmp/
REM %PATH_ZIP% a -tgzip "%CRONJOB_BACKUP_PATH%%CRONJOB_FILENAME_BACKUP_FILES%.gzip" "%PATH_Auftragslisten%" -r -x!_tmp/
REM %PATH_ZIP% a "%CRONJOB_BACKUP_PATH%%CRONJOB_FILENAME_BACKUP_FILES%.7z" "%PATH_Auftragslisten%" -r -x!_tmp/
%PATH_ZIP% a -mx1 -v2G "%CRONJOB_BACKUP_PATH%%CRONJOB_FILENAME_BACKUP_FILES%.7z" "%PATH_Auftragslisten%" -r -x!_tmp/
REM copy %CRONJOB_BACKUP_PATH%%CRONJOB_FILENAME_BACKUP_FILES%.7z %PATH_CLOUD_BACKUP%%CRONJOB_FILENAME_BACKUP_FILES%.7z
REM EOF BACKUP AND ZIP FILES
REM ---------------------------------------------------------------------------------------------------------------
REM ---------------------------------------------------------------------------------------------------------------
REM BOF DELETE OLDER BACKUP FILES ON LOCAL SERVER
REM set KEEP_BACKUP_FILES_DAY_LIMIT_LOCAL=-50
REM forfiles /p %CRONJOB_BACKUP_PATH% /s /m *.* /c "cmd /c del @path" /d %KEEP_BACKUP_FILES_DAY_LIMIT_LOCAL%
REM EOF DELETE OLDER BACKUP FILES ON LOCAL SERVER
REM ---------------------------------------------------------------------------------------------------------------
REM ---------------------------------------------------------------------------------------------------------------
>>%CRONJOB_DIR_LOG_FILES%cronBackupAuftragslisten2.txt