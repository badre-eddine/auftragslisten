ECHO OFF
set PATH_CLOUD_BACKUP=C:\CLOUD_BACKUP\MagentaCLOUD\Auftragslisten\
set CRONJOB_BACKUP_PATH=Z:\Software\Auftragslisten_SICHERUNGEN\cronBackUp\
set CRONJOB_DIR_EXECUTE_FILES=C:\Auftragslisten_SERVER\webroot\Auftragslisten\cronjobs\
set CRONJOB_DIR_LOG_FILES=%CRONJOB_DIR_EXECUTE_FILES%logs\


REM forfiles /p %PATH_CLOUD_BACKUP% /s /m *.* /c "cmd /c if @isdir==FALSE echo @fname - @file: @fsize"

REM BOF GET TOTAL FILESIZE
REM set TOTALSIZE=0
REM for /f %%i in ('forfiles /p %PATH_CLOUD_BACKUP% /s /m *.* /c "cmd /c if @isdir==FALSE echo @fsize"') ^
REM do (
REM 	set /a TOTALSIZE=TOTALSIZE + %%i
REM )
REM echo %TOTALSIZE%
REM EOF GET TOTAL FILESIZE

REM BOF READ FILENAME
REM for /f %%i in ('forfiles /p %PATH_CLOUD_BACKUP% /s /m *.* /c "cmd /c if @isdir==FALSE echo @fname"') ^
REM do (
REM 	set CURRENT_FILENAME = %%i
REM 	ECHO %CURRENT_FILENAME%
REM )
REM EOF READ FILENAME

set CRONJOB_SAVESTAMP_YEAR=%date:~-4%
set CRONJOB_SAVESTAMP_MONTH=%date:~3,2%
if "%CRONJOB_SAVESTAMP_MONTH:~0,1%" == " " set CRONJOB_SAVESTAMP_MONTH=0%CRONJOB_SAVESTAMP_MONTH:~1,1%
set CRONJOB_SAVESTAMP_DAY=%date:~0,2%
if "%CRONJOB_SAVESTAMP_DAY:~0,1%" == " " set CRONJOB_SAVESTAMP_DAY=0%CRONJOB_SAVESTAMP_DAY:~1,1%

set CURRENT_DATE=%CRONJOB_SAVESTAMP_YEAR%-%CRONJOB_SAVESTAMP_MONTH%-%CRONJOB_SAVESTAMP_DAY%
set CHECK_DATE=2016-11-10
set CURRENT_YEAR=%CRONJOB_SAVESTAMP_YEAR%
set CURRENT_MONTH=%CRONJOB_SAVESTAMP_MONTH%
set CURRENT_DAY=%CRONJOB_SAVESTAMP_DAY%

ECHO "CURRENT_DATE: "%CURRENT_DATE%
ECHO "CURRENT_YEAR: "%CURRENT_YEAR%
ECHO "CURRENT_MONTH: "%CURRENT_MONTH%
ECHO "CURRENT_DAY: "%CURRENT_DAY%

for /f %%G in ('dir /a-d /b %CRONJOB_BACKUP_PATH% ^|findstr /I "backup"') do (
	ECHO "FILENAME: "%%G
	set thisTest = findstr /I /R /C:"[0-9][0-9][0-9][0-9]-[0-9][0-9]-[0-9][0-9]_[0-9][0-9]-[0-9][0-9]-[0-9][0-9]" /C:"%%G"
	ECHO "TEST:" %thisTest%
)
timeout /t 500



REM for /f %%G in ('dir /a-d /b %CRONJOB_BACKUP_PATH% ^|findstr /I /R "[0-9][0-9][0-9][0-9]-[0-9][0-9]-[0-9][0-9]_[0-9][0-9]-[0-9][0-9]-[0-9][0-9]"') do (
for /f "TOKENS=3 delims=_" %%G in ('dir /a-d /b %CRONJOB_BACKUP_PATH% ^|findstr /I /R "[0-9][0-9][0-9][0-9]-[0-9][0-9]-[0-9][0-9]_[0-9][0-9]-[0-9][0-9]-[0-9][0-9]"') do (
	set fdate=%%G
	ECHO "FILENAME_DATE:" %%G
	ECHO "fdate:" %fdate%

	IF %%G EQU %CHECK_DATE% (
		ECHO "EQUAL_DATE:" %%G
	)

 	IF %%G EQU %CHECK_DATE% (
 		ECHO "EQUAL_DATE:" %%G
 	)

 	IF %%G GEQ %CHECK_DATE% (
 		ECHO "BIGGER_OR_EQUAL_DATE:" %%G
 	)

 	IF %%G LEQ %CHECK_DATE% (
 		ECHO "LOWER_OR_EQUAL_DATE:" %%G
 	)

 	IF %%G LSS %CHECK_DATE% (
 		ECHO "LOWER_DATE:" %%G
 	)

 	IF %%G GTR %CHECK_DATE% (
 		ECHO "BIGGER_DATE:" %%G
 	)

	ECHO '################################'
)

>>%CRONJOB_DIR_LOG_FILES%_testGetDateFromFilename.txt 2>&1