ECHO ON
REM ---------------------------------------------------------------------------------------------------------------
REM ---------------------------------------------------------------------------------------------------------------
REM BOF SET TIMESTAMP AND PARAMS
REM set CRONJOB_SAVESTAMP=%DATE:/=-%@%TIME::=-%
REM set CRONJOB_SAVESTAMP=%DATE:/=-%_%TIME::=-%
set CRONJOB_SAVESTAMP_HOUR=%time:~0,2%
if "%CRONJOB_SAVESTAMP_HOUR:~0,1%" == " " set CRONJOB_SAVESTAMP_HOUR=0%CRONJOB_SAVESTAMP_HOUR:~1,1%
REM ---------------------------------------------------------------------------------------------------------------
set CRONJOB_SAVESTAMP_MINUTE=%time:~3,2%
if "%CRONJOB_SAVESTAMP_MINUTE:~0,1%" == " " set CRONJOB_SAVESTAMP_MINUTE=0%CRONJOB_SAVESTAMP_MINUTE:~1,1%
REM ---------------------------------------------------------------------------------------------------------------
set CRONJOB_SAVESTAMP_SECONDS=%time:~6,2%
if "%CRONJOB_SAVESTAMP_SECONDS:~0,1%" == " " set CRONJOB_SAVESTAMP_SECONDS=0%CRONJOB_SAVESTAMP_SECONDS:~1,1%
REM ---------------------------------------------------------------------------------------------------------------
set CRONJOB_SAVESTAMP_YEAR=%date:~-4%
set CRONJOB_SAVESTAMP_MONTH=%date:~3,2%
if "%CRONJOB_SAVESTAMP_MONTH:~0,1%" == " " set CRONJOB_SAVESTAMP_MONTH=0%CRONJOB_SAVESTAMP_MONTH:~1,1%
set CRONJOB_SAVESTAMP_DAY=%date:~0,2%
if "%CRONJOB_SAVESTAMP_DAY:~0,1%" == " " set CRONJOB_SAVESTAMP_DAY=0%CRONJOB_SAVESTAMP_DAY:~1,1%
REM ---------------------------------------------------------------------------------------------------------------
set CRONJOB_SAVESTAMP=%CRONJOB_SAVESTAMP_YEAR%-%CRONJOB_SAVESTAMP_MONTH%-%CRONJOB_SAVESTAMP_DAY%_%CRONJOB_SAVESTAMP_HOUR%-%CRONJOB_SAVESTAMP_MINUTE%-%CRONJOB_SAVESTAMP_SECONDS%
REM echo CRONJOB_SAVESTAMP=%CRONJOB_SAVESTAMP%
REM EOF SET TIMESTAMP AND PARAMS
REM ---------------------------------------------------------------------------------------------------------------
REM ---------------------------------------------------------------------------------------------------------------
REM BOF SET FILENAMES
set PATH_Auftragslisten=C:\Auftragslisten_SERVER\webroot\Auftragslisten\
set CRONJOB_DIR_EXECUTE_FILES=C:\Auftragslisten_SERVER\webroot\Auftragslisten\cronjobs\
set CRONJOB_DIR_TEMP_FILES=C:\Auftragslisten_SERVER\webroot\Auftragslisten\cronjobs\tmp\
set CRONJOB_DIR_LOG_FILES=C:\Auftragslisten_SERVER\webroot\Auftragslisten\cronjobs\logs\
set PATH_PHP=C:\Auftragslisten_SERVER\php\php.exe
REM EOF SET FILENAMES
REM ---------------------------------------------------------------------------------------------------------------
REM ---------------------------------------------------------------------------------------------------------------
REM BOF RUN PHP SCRIPT
%PATH_PHP% %CRONJOB_DIR_EXECUTE_FILES%cronTransferProductionDatasToOnline.php >>%CRONJOB_DIR_LOG_FILES%cronTransferProductionDatasToOnline2.txt 2>&1
REM EOF RUN PHP SCRIPT
REM ---------------------------------------------------------------------------------------------------------------
REM nslookup www.burhan-ctr.de >>%CRONJOB_DIR_LOG_FILES%cronTransferProductionDatasToOnline2.txt
>>%CRONJOB_DIR_LOG_FILES%cronTransferProductionDatasToOnline2.txt 2>&1