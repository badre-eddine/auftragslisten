ECHO OFF
REM ECHO OFF
REM ---------------------------------------------------------------------------------------------------------------
REM To create the batch file
REM Open Notepad.
REM Paste the line "C:\xampp\php\php.exe C:\wamp\www\index.php"
REM Click "File" -> "Save As"
REM Ensure "Save as type:" is set to "All Files"
REM Save the file as "cron.bat" to your C drive
REM To schedule the batch file to run
REM Open Command Prompt
REM Paste the following "schtasks /create /sc minute /mo 20 /tn "PHP Cron Job" /tr C:\cron.bat"
REM Press Enter
REM This will make the script run every 20 minutes, the first time 20 minutes from now.
REM ---------------------------------------------------------------------------------------------------------------
REM BOF CREATE CRONJOB
REM "schtasks /create /sc minute /mo 20 /tn "cronJob_BackUp_Auftragslisten" /tr C:\Auftragslisten_SERVER\webroot\Auftragslisten\cronjobs\myTestCron.bat"
REM "schtasks /create /SC DAILY /MO 1 /ST 20:00 /TN "cronJob_BackUp_Auftragslisten" /tr C:\Auftragslisten_SERVER\webroot\Auftragslisten\cronjobs\myTestCron.bat"
REM "schtasks /create /SC DAILY /MO 1 /ST 17:00 /TN "cronJob_SendOpenOrdersToSalesmen" /tr C:\Auftragslisten_SERVER\webroot\Auftragslisten\cronjobs\cronSendPrepaymentNotice.bat"
REM EOF CREATE CRONJOB
REM ---------------------------------------------------------------------------------------------------------------
REM ---------------------------------------------------------------------------------------------------------------
REM BOF SET TIMESTAMP AND PARAMS
REM set CRONJOB_SAVESTAMP=%DATE:/=-%@%TIME::=-%
REM set CRONJOB_SAVESTAMP=%DATE:/=-%_%TIME::=-%
set CRONJOB_SAVESTAMP_HOUR=%time:~0,2%
if "%CRONJOB_SAVESTAMP_HOUR:~0,1%" == " " set CRONJOB_SAVESTAMP_HOUR=0%CRONJOB_SAVESTAMP_HOUR:~1,1%
REM ---------------------------------------------------------------------------------------------------------------
set CRONJOB_SAVESTAMP_MINUTE=%time:~3,2%
if "%CRONJOB_SAVESTAMP_MINUTE:~0,1%" == " " set CRONJOB_SAVESTAMP_MINUTE=0%CRONJOB_SAVESTAMP_MINUTE:~1,1%
REM ---------------------------------------------------------------------------------------------------------------
set CRONJOB_SAVESTAMP_SECONDS=%time:~6,2%
if "%CRONJOB_SAVESTAMP_SECONDS:~0,1%" == " " set CRONJOB_SAVESTAMP_SECONDS=0%CRONJOB_SAVESTAMP_SECONDS:~1,1%
REM ---------------------------------------------------------------------------------------------------------------
set CRONJOB_SAVESTAMP_YEAR=%date:~-4%
set CRONJOB_SAVESTAMP_MONTH=%date:~3,2%
if "%CRONJOB_SAVESTAMP_MONTH:~0,1%" == " " set CRONJOB_SAVESTAMP_MONTH=0%CRONJOB_SAVESTAMP_MONTH:~1,1%
set CRONJOB_SAVESTAMP_DAY=%date:~0,2%
if "%CRONJOB_SAVESTAMP_DAY:~0,1%" == " " set CRONJOB_SAVESTAMP_DAY=0%CRONJOB_SAVESTAMP_DAY:~1,1%
REM ---------------------------------------------------------------------------------------------------------------
set CRONJOB_SAVESTAMP=%CRONJOB_SAVESTAMP_YEAR%-%CRONJOB_SAVESTAMP_MONTH%-%CRONJOB_SAVESTAMP_DAY%_%CRONJOB_SAVESTAMP_HOUR%-%CRONJOB_SAVESTAMP_MINUTE%-%CRONJOB_SAVESTAMP_SECONDS%
REM echo CRONJOB_SAVESTAMP=%CRONJOB_SAVESTAMP%
REM EOF SET TIMESTAMP AND PARAMS
REM ---------------------------------------------------------------------------------------------------------------
REM ---------------------------------------------------------------------------------------------------------------
REM BOF SET FILENAMES
set PATH_Auftragslisten=C:\Auftragslisten_SERVER\webroot\Auftragslisten\
set CRONJOB_DIR_EXECUTE_FILES=C:\Auftragslisten_SERVER\webroot\Auftragslisten\cronjobs\
set CRONJOB_BACKUP_PATH=Z:\Software\Auftragslisten_SICHERUNGEN\cronBackUp\
set CRONJOB_FILENAME_BACKUP_FILES=Auftragslisten_backup_%CRONJOB_SAVESTAMP%_Files
set CRONJOB_FILENAME_BACKUP_DATABASE=Auftragslisten_backup_%CRONJOB_SAVESTAMP%_Database
REM ---------------------------------------------------------------------------------------------------------------
set PATH_ZIP=C:\Programme\7-Zip\7z.exe
set PATH_PHP=C:\Auftragslisten_SERVER\php\php.exe
set PATH_MYSQLDUMP=C:\Auftragslisten_SERVER\mysql\bin\mysqldump.exe
set PATH_MYSQL=C:\Auftragslisten_SERVER\mysql\bin\mysql.exe
REM EOF SET FILENAMES
REM ---------------------------------------------------------------------------------------------------------------
REM ---------------------------------------------------------------------------------------------------------------
REM BOF BACK UP MYSQL DATABASE
REM %PATH_MYSQLDUMP%  --databases burhanctrAuftragslisten -hlocalhost –uburhan -pburhan -r "%CRONJOB_DIR_EXECUTE_FILES%mysqlbackup.sql"
%PATH_MYSQLDUMP%  --databases burhanctrAuftragslisten -hlocalhost –uburhan -pburhan > "%CRONJOB_BACKUP_PATH%%CRONJOB_FILENAME_BACKUP_DATABASE%.sql"
%PATH_ZIP% a "%CRONJOB_BACKUP_PATH%%CRONJOB_FILENAME_BACKUP_DATABASE%.sql.7z" "%CRONJOB_BACKUP_PATH%%CRONJOB_FILENAME_BACKUP_DATABASE%.sql"
REM %PATH_ZIP% a -ttar %CRONJOB_BACKUP_PATH%%CRONJOB_FILENAME_BACKUP_DATABASE%.sql.tar %CRONJOB_BACKUP_PATH%%CRONJOB_FILENAME_BACKUP_DATABASE%.sql
REM %PATH_ZIP% a -tgzip %CRONJOB_BACKUP_PATH%%CRONJOB_FILENAME_BACKUP_DATABASE%.sql.tar.gz %CRONJOB_BACKUP_PATH%%CRONJOB_FILENAME_BACKUP_DATABASE%.sql.tar
REM EOF BACK UP MYSQL DATABASE
REM ---------------------------------------------------------------------------------------------------------------
REM ---------------------------------------------------------------------------------------------------------------
REM BOF BACKUP AND ZIP FILES
%PATH_ZIP% a %CRONJOB_BACKUP_PATH%%CRONJOB_FILENAME_BACKUP_FILES%.7z %PATH_Auftragslisten%
REM EOF BACK AND ZIP FILES
REM ---------------------------------------------------------------------------------------------------------------
REM ---------------------------------------------------------------------------------------------------------------
REM BOF DELETE FILES
del %CRONJOB_BACKUP_PATH%%CRONJOB_FILENAME_BACKUP_DATABASE%.sql
REM del %CRONJOB_BACKUP_PATH%%CRONJOB_FILENAME_BACKUP_DATABASE%.sql.tar
REM EOF DELETE FILES
REM ---------------------------------------------------------------------------------------------------------------
REM ---------------------------------------------------------------------------------------------------------------
REM BOF COPY FILES
REM copy %CRONJOB_BACKUP_PATH%%CRONJOB_FILENAME_BACKUP_FILES%.7z F:\myFILES\Auftragslisten\%CRONJOB_FILENAME_BACKUP_FILES%.7z
REM copy %CRONJOB_BACKUP_PATH%%CRONJOB_FILENAME_BACKUP_DATABASE%.7z F:\myFILES\Auftragslisten\%CRONJOB_FILENAME_BACKUP_DATABASE%.7z
REM EOF COPY FILES
REM ---------------------------------------------------------------------------------------------------------------
REM ---------------------------------------------------------------------------------------------------------------
REM BOF REMOVE/DELETE DIR
REM rmdir C:\WEB_SERVER\webroot\cronBackUp\ /s /q
REM EOF REMOVE/DELETE DIR
REM ---------------------------------------------------------------------------------------------------------------
REM ---------------------------------------------------------------------------------------------------------------
REM BOF RUN PHP SCRIPT
REM %PATH_PHP% %CRONJOB_DIR_EXECUTE_FILES%myTestCron.php testVar=meinVariablenwert
REM EOF RUN PHP SCRIPT
REM ---------------------------------------------------------------------------------------------------------------
REM ---------------------------------------------------------------------------------------------------------------
REM BOF UPLOAD BACKUP VIA FTP
REM echo OPEN alfa3062.alfahosting-server.de 21 >> upload.ftp
REM echo USER web23 >> upload.ftp
REM echo 4eYlDq2E >> upload.ftp
REM echo BINARY >> upload.ftp
REM echo CD \html\burhan-ctr.de\myBackups\_Auftragslisten\ >> upload.ftp
REM echo put %CRONJOB_BACKUP_PATH%%CRONJOB_FILENAME_BACKUP_FILES%.7z >> upload.ftp
REM echo put %CRONJOB_BACKUP_PATH%%CRONJOB_FILENAME_BACKUP_DATABASE%.sql.7z >> upload.ftp
REM echo bye >> upload.ftp
REM ---------------------------------------------------------------------------------------------------------------
REM ftp.exe -n -i -s:upload.ftp
REM echo cd \html\burhan-ctr.de\myBackups\_Auftragslisten\
REM echo put %CRONJOB_BACKUP_PATH%%CRONJOB_FILENAME_BACKUP_FILES%.7z
REM echo put %CRONJOB_BACKUP_PATH%%CRONJOB_FILENAME_BACKUP_DATABASE%.sql.7z
REM echo BYE
REM del upload.ftp
REM EOF UPLOAD BACKUP VIA FTP
REM ---------------------------------------------------------------------------------------------------------------
REM echo test >>C:\Auftragslisten_SERVER\webroot\Auftragslisten\cronjobs\logs\cronBackupAuftragslisten.txt
REM ---------------------------------------------------------------------------------------------------------------
REM ---------------------------------------------------------------------------------------------------------------
REM BOF UPLOAD BACKUP VIA FTP
REM echo OPEN alfa3062.alfahosting-server.de 21 >>upload2.ftp
REM echo USER web23 >>upload2.ftp
REM echo 4eYlDq2E >>upload2.ftp
REM echo BINARY >>upload2.ftp
REM echo CD \html\burhan-ctr.de\myBackups\_Auftragslisten\ >>upload2.ftp
REM echo put %CRONJOB_BACKUP_PATH%%CRONJOB_FILENAME_BACKUP_FILES%.7z >>upload2.ftp
REM echo put %CRONJOB_BACKUP_PATH%%CRONJOB_FILENAME_BACKUP_DATABASE%.sql.7z >>upload2.ftp
REM echo bye >>upload2.ftp
REM ftp.exe -n -i -s:upload2.ftp
REM del upload2.ftp
REM ---------------------------------------------------------------------------------------------------------------
>>C:\Auftragslisten_SERVER\webroot\Auftragslisten\cronjobs\logs\cronBackupAuftragslisten2.txt