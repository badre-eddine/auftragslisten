ECHO ON

REM ---------------------------------------------------------------------------------------------------------------
REM ---------------------------------------------------------------------------------------------------------------

REM BOF SET NAME OF DATABASE
	set mysqli_DATABASE_DB_NAME=burhanctrAuftragslisten
	set mysqli_DATABASE_DB_USER=burhan
	set mysqli_DATABASE_DB_PASS=burhan
	set mysqli_DATABASE_DB_HOST=localhost
REM EOF SET NAME OF DATABASE

REM ---------------------------------------------------------------------------------------------------------------
REM ---------------------------------------------------------------------------------------------------------------

REM BOF SET ACTIONS
	set ACTION_USE_CLOUD_STORAGE=false
	set ACTION_BACKUP_FILES=false
	set ACTION_BACKUP_DATABASE_COMPLETE=false
	set ACTION_BACKUP_DATABASE_TABLES=true
REM EOF SET ACTIONS

timeout \t 200
exit

REM ---------------------------------------------------------------------------------------------------------------
REM ---------------------------------------------------------------------------------------------------------------

REM BOF SET ARCHIVE TYPES 7z | zip
	set ARCHIVE_TYPE_DATABASE=7z
	set ARCHIVE_TYPE_FILES=7z
REM EOF SET ARCHIVE TYPES

REM ---------------------------------------------------------------------------------------------------------------
REM ---------------------------------------------------------------------------------------------------------------
REM BOF SET TIMESTAMP AND PARAMS
	REM set CRONJOB_SAVESTAMP=%DATE:/=-%@%TIME::=-%
	REM set CRONJOB_SAVESTAMP=%DATE:/=-%_%TIME::=-%
	set CRONJOB_SAVESTAMP_HOUR=%time:~0,2%
	if "%CRONJOB_SAVESTAMP_HOUR:~0,1%" == " " set CRONJOB_SAVESTAMP_HOUR=0%CRONJOB_SAVESTAMP_HOUR:~1,1%
	REM ---------------------------------------------------------------------------------------------------------------
	set CRONJOB_SAVESTAMP_MINUTE=%time:~3,2%
	if "%CRONJOB_SAVESTAMP_MINUTE:~0,1%" == " " set CRONJOB_SAVESTAMP_MINUTE=0%CRONJOB_SAVESTAMP_MINUTE:~1,1%
	REM ---------------------------------------------------------------------------------------------------------------
	set CRONJOB_SAVESTAMP_SECONDS=%time:~6,2%
	if "%CRONJOB_SAVESTAMP_SECONDS:~0,1%" == " " set CRONJOB_SAVESTAMP_SECONDS=0%CRONJOB_SAVESTAMP_SECONDS:~1,1%
	REM ---------------------------------------------------------------------------------------------------------------
	set CRONJOB_SAVESTAMP_YEAR=%date:~-4%
	set CRONJOB_SAVESTAMP_MONTH=%date:~3,2%
	if "%CRONJOB_SAVESTAMP_MONTH:~0,1%" == " " set CRONJOB_SAVESTAMP_MONTH=0%CRONJOB_SAVESTAMP_MONTH:~1,1%
	set CRONJOB_SAVESTAMP_DAY=%date:~0,2%
	if "%CRONJOB_SAVESTAMP_DAY:~0,1%" == " " set CRONJOB_SAVESTAMP_DAY=0%CRONJOB_SAVESTAMP_DAY:~1,1%
	REM ---------------------------------------------------------------------------------------------------------------
	set CRONJOB_SAVESTAMP=%CRONJOB_SAVESTAMP_YEAR%-%CRONJOB_SAVESTAMP_MONTH%-%CRONJOB_SAVESTAMP_DAY%_%CRONJOB_SAVESTAMP_HOUR%-%CRONJOB_SAVESTAMP_MINUTE%-%CRONJOB_SAVESTAMP_SECONDS%
	REM echo CRONJOB_SAVESTAMP=%CRONJOB_SAVESTAMP%
REM EOF SET TIMESTAMP AND PARAMS

REM ---------------------------------------------------------------------------------------------------------------
REM ---------------------------------------------------------------------------------------------------------------

REM BOF SET FILENAMES
	set PATH_Auftragslisten=C:\Auftragslisten_SERVER\webroot\Auftragslisten\
	set CRONJOB_DIR_EXECUTE_FILES=C:\Auftragslisten_SERVER\webroot\Auftragslisten\cronjobs\
	set CRONJOB_DIR_LOG_FILES=%CRONJOB_DIR_EXECUTE_FILES%logs\
	set CRONJOB_DIR_TEMP_FILES=%CRONJOB_DIR_EXECUTE_FILES%tmp\
	set CRONJOB_BACKUP_PATH=Z:\Software\Auftragslisten_SICHERUNGEN\cronBackUp\

	set CRONJOB_FILENAME_BACKUP_FILES=Auftragslisten_backup_%CRONJOB_SAVESTAMP%_Files.%ARCHIVE_TYPE_FILES%
	set CRONJOB_FILENAME_BACKUP_DATABASE=Auftragslisten_backup_%CRONJOB_SAVESTAMP%_Database.%ARCHIVE_TYPE_DATABASE%
	set CRONJOB_FILENAME_BACKUP_TABLES=Auftragslisten_backup_%CRONJOB_SAVESTAMP%_DatabaseTables.%ARCHIVE_TYPE_DATABASE%

	set TEMP_CRONJOB_BACKUP_PATH=E:\Auftragslisten_backups\
	set TEMP_CRONJOB_BACKUP_PATH_SQL_FILES=%TEMP_CRONJOB_BACKUP_PATH%tmp\

	set PATH_CLOUD_BACKUP=C:\CLOUD_BACKUP\MagentaCLOUD\Auftragslisten\

	REM -----------------------------------------------------------------------------------------------------------

	set PATH_ZIP=C:\Programme\7-Zip\7z.exe
	set PATH_PHP=C:\Auftragslisten_SERVER\php\php.exe
	set PATH_MYSQLDUMP=C:\Auftragslisten_SERVER\mysql\bin\mysqldump.exe
	set PATH_MYSQL=C:\Auftragslisten_SERVER\mysql\bin\mysql.exe
REM EOF SET FILENAMES

REM ---------------------------------------------------------------------------------------------------------------
REM ---------------------------------------------------------------------------------------------------------------

REM BOF BACK UP MYSQL DATABASE ALL TABLES IN ONE FILE
	if "%ACTION_BACKUP_DATABASE_COMPLETE%"=="true" (
		REM %PATH_MYSQLDUMP%  --databases burhanctrAuftragslisten -hlocalhost -u %mysqli_DATABASE_DB_USER% -p%mysqli_DATABASE_DB_PASS% > "%CRONJOB_BACKUP_PATH%%CRONJOB_FILENAME_BACKUP_DATABASE%.sql"
		REM xxxx %PATH_MYSQLDUMP%  --databases burhanctrAuftragslisten -hlocalhost -u %mysqli_DATABASE_DB_USER% -p%mysqli_DATABASE_DB_PASS% > "%TEMP_CRONJOB_BACKUP_PATH%%CRONJOB_FILENAME_BACKUP_DATABASE%.sql"
		REM %PATH_ZIP% a -t%ARCHIVE_TYPE_DATABASE% "%CRONJOB_BACKUP_PATH%%CRONJOB_FILENAME_BACKUP_DATABASE%" "%CRONJOB_BACKUP_PATH%%CRONJOB_FILENAME_BACKUP_DATABASE%.sql"
		REM xxxx %PATH_ZIP% a -t%ARCHIVE_TYPE_DATABASE% "%TEMP_CRONJOB_BACKUP_PATH%%CRONJOB_FILENAME_BACKUP_DATABASE%" "%TEMP_CRONJOB_BACKUP_PATH%%CRONJOB_FILENAME_BACKUP_DATABASE%.sql"
		REM xxxx copy "%TEMP_CRONJOB_BACKUP_PATH%%CRONJOB_FILENAME_BACKUP_DATABASE%" "%CRONJOB_BACKUP_PATH%%CRONJOB_FILENAME_BACKUP_DATABASE%"
		REM xxxx del "%TEMP_CRONJOB_BACKUP_PATH%%CRONJOB_FILENAME_BACKUP_DATABASE%"

		REM del %CRONJOB_BACKUP_PATH%%CRONJOB_FILENAME_BACKUP_DATABASE%.sql
		REM del %TEMP_CRONJOB_BACKUP_PATH%%CRONJOB_FILENAME_BACKUP_DATABASE%.sql
	)
REM EOF BACK UP MYSQL DATABASE IN ONE FILE IN ONE FILE

REM ---------------------------------------------------------------------------------------------------------------
REM ---------------------------------------------------------------------------------------------------------------

REM BOF BACK UP MYSQL DATABASE EACH TABLES IN ONE FILE
	if "%ACTION_BACKUP_DATABASE_TABLES%"=="true" (
		set FILE_TABLE_LIST=%CRONJOB_DIR_LOG_FILES%listBackUpTables.txt
		%PATH_MYSQL% -u %mysqli_DATABASE_DB_USER% -p%mysqli_DATABASE_DB_PASS% %mysqli_DATABASE_DB_NAME% -e "SHOW TABLES;" --skip-column-names > %FILE_TABLE_LIST%
		for /f %%T in (%FILE_TABLE_LIST%) do (
			%PATH_MYSQLDUMP% -u %mysqli_DATABASE_DB_USER% -p%mysqli_DATABASE_DB_PASS% %mysqli_DATABASE_DB_NAME% %%T > %CRONJOB_DIR_TEMP_FILES%%%T.sql
			REM %PATH_MYSQLDUMP% -u %mysqli_DATABASE_DB_USER% -p%mysqli_DATABASE_DB_PASS% %mysqli_DATABASE_DB_NAME% %%T > %TEMP_CRONJOB_BACKUP_PATH%%%T.sql
		)
		%PATH_ZIP% a -t%ARCHIVE_TYPE_DATABASE% "%CRONJOB_DIR_TEMP_FILES%%CRONJOB_FILENAME_BACKUP_TABLES%" "%CRONJOB_DIR_TEMP_FILES%*.sql"
		copy %CRONJOB_DIR_TEMP_FILES%%CRONJOB_FILENAME_BACKUP_TABLES% %CRONJOB_BACKUP_PATH%%CRONJOB_FILENAME_BACKUP_TABLES%

		REM %PATH_ZIP% a -t%ARCHIVE_TYPE_DATABASE% "%TEMP_CRONJOB_BACKUP_PATH%%CRONJOB_FILENAME_BACKUP_TABLES%" "%TEMP_CRONJOB_BACKUP_PATH%*.sql"
		REM xxx copy %TEMP_CRONJOB_BACKUP_PATH%%CRONJOB_FILENAME_BACKUP_TABLES% %CRONJOB_BACKUP_PATH%%CRONJOB_FILENAME_BACKUP_TABLES%

		del %CRONJOB_DIR_TEMP_FILES%%CRONJOB_FILENAME_BACKUP_TABLES%
		REM del %TEMP_CRONJOB_BACKUP_PATH%%CRONJOB_FILENAME_BACKUP_TABLES%
		del %CRONJOB_DIR_TEMP_FILES%*.sql
		del %TEMP_CRONJOB_BACKUP_PATH%*.sql
		del %FILE_TABLE_LIST%
	)
REM EOF BACK UP MYSQL DATABASE EACH TABLES IN ONE FILE

REM ---------------------------------------------------------------------------------------------------------------
REM ---------------------------------------------------------------------------------------------------------------

REM BOF BACKUP AND ZIP FILES
	if "%ACTION_BACKUP_FILES%"=="true" (
		REM %PATH_ZIP% -t%ARCHIVE_TYPE_FILES% a "%CRONJOB_BACKUP_PATH%%CRONJOB_FILENAME_BACKUP_FILES%" "%PATH_Auftragslisten%" -r -x!_tmp/
		REM %PATH_ZIP% -t%ARCHIVE_TYPE_FILES% a "%TEMP_CRONJOB_BACKUP_PATH%%CRONJOB_FILENAME_BACKUP_FILES%" "%PATH_Auftragslisten%" -r -x!_tmp/
		REM xxx copy %TEMP_CRONJOB_BACKUP_PATH%%CRONJOB_FILENAME_BACKUP_FILES% %CRONJOB_BACKUP_PATH%%CRONJOB_FILENAME_BACKUP_FILES%
		REM xxx del %TEMP_CRONJOB_BACKUP_PATH%%CRONJOB_FILENAME_BACKUP_FILES%
	)
REM EOF BACKUP AND ZIP FILES

REM ---------------------------------------------------------------------------------------------------------------
REM ---------------------------------------------------------------------------------------------------------------

REM BOF DELETE OLDER BACKUP FILES ON LOCAL SERVER
	REM set KEEP_BACKUP_FILES_DAY_LIMIT_LOCAL=-50
	REM forfiles /p %CRONJOB_BACKUP_PATH% /s /m *.* /c "cmd /c del @path" /d %KEEP_BACKUP_FILES_DAY_LIMIT_LOCAL%
REM EOF DELETE OLDER BACKUP FILES ON LOCAL SERVER

REM ---------------------------------------------------------------------------------------------------------------
REM ---------------------------------------------------------------------------------------------------------------

REM BOF CLOUD STORAGE
	if "%ACTION_USE_CLOUD_STORAGE%"=="true" (

		REM BOF DELETE OLDER BACKUP FILES IN CLOUD
			set KEEP_BACKUP_FILES_DAY_LIMIT_CLOUD=-1
			REM forfiles /p "C:\source_folder" /s /m *.* /c "cmd /c Del @path" /d -7
			forfiles /p %PATH_CLOUD_BACKUP% /s /m *.* /c "cmd /c del @path" /d %KEEP_BACKUP_FILES_DAY_LIMIT_CLOUD%
		REM EOF DELETE OLDER BACKUP FILES IN CLOUD

		REM BOF COPY BACKUP FILES TO CLOUD
			if "%ACTION_BACKUP_FILES%"=="true" (
				copy %CRONJOB_BACKUP_PATH%%CRONJOB_FILENAME_BACKUP_FILES% %PATH_CLOUD_BACKUP%%CRONJOB_FILENAME_BACKUP_FILES%
			)
		REM EOF COPY BACKUP FILES TO CLOUD

		REM BOF COPY BACKUP DATABASE COMPLETE TO CLOUD
			if "%ACTION_BACKUP_DATABASE_COMPLETE%"=="true" (
				copy %CRONJOB_BACKUP_PATH%%CRONJOB_FILENAME_BACKUP_DATABASE% %PATH_CLOUD_BACKUP%%CRONJOB_FILENAME_BACKUP_DATABASE%
			)
		REM EOF COPY BACKUP DATABASE COMPLETE TO CLOUD

		REM BOF COPY BACKUP DATABASE TABLES TO CLOUD
			if "%ACTION_BACKUP_DATABASE_TABLES%"=="true" (
				copy %CRONJOB_BACKUP_PATH%%CRONJOB_FILENAME_BACKUP_TABLES% %PATH_CLOUD_BACKUP%%CRONJOB_FILENAME_BACKUP_TABLES%
			)
		REM EOF COPY BACKUP DATABASE TABLES TO CLOUD
	)
REM EOF CLOUD STORAGE

REM ---------------------------------------------------------------------------------------------------------------
REM ---------------------------------------------------------------------------------------------------------------

>>%CRONJOB_DIR_LOG_FILES%cronBackupAuftragslisten2.txt
timeout \t 200
exit