ECHO ON

REM ---------------------------------------------------------------------------------------------------------------
REM ---------------------------------------------------------------------------------------------------------------

REM BOF SET NAME OF DATABASE
	set mysqli_DATABASE_DB_NAME=burhanctrAuftragslisten
	set mysqli_DATABASE_DB_USER=burhan
	set mysqli_DATABASE_DB_PASS=burhan
	set mysqli_DATABASE_DB_HOST=localhost
REM EOF SET NAME OF DATABASE

REM ---------------------------------------------------------------------------------------------------------------
REM ---------------------------------------------------------------------------------------------------------------

REM BOF SET ACTIONS
	set ACTION_USE_CLOUD_STORAGE=false	
	set ACTION_BACKUP_DATABASE_COMPLETE=false
	set ACTION_BACKUP_DATABASE_TABLES=true
	set ACTION_BACKUP_FILES=true
	REM MODE_BACKUP_FILES=update|new
	set MODE_BACKUP_FILES=new
REM EOF SET ACTIONS

REM ---------------------------------------------------------------------------------------------------------------
REM ---------------------------------------------------------------------------------------------------------------

REM BOF SET ARCHIVE TYPES 7z | zip
	set ARCHIVE_TYPE_DATABASE=7z
	set ARCHIVE_TYPE_FILES=7z
REM EOF SET ARCHIVE TYPES

REM ---------------------------------------------------------------------------------------------------------------
REM ---------------------------------------------------------------------------------------------------------------

REM BOF SET TIMESTAMP AND PARAMS
	REM set CRONJOB_SAVESTAMP=%DATE:/=-%@%TIME::=-%
	REM set CRONJOB_SAVESTAMP=%DATE:/=-%_%TIME::=-%
	set CRONJOB_SAVESTAMP_HOUR=%time:~0,2%
	if "%CRONJOB_SAVESTAMP_HOUR:~0,1%" == " " set CRONJOB_SAVESTAMP_HOUR=0%CRONJOB_SAVESTAMP_HOUR:~1,1%
	REM ---------------------------------------------------------------------------------------------------------------
	set CRONJOB_SAVESTAMP_MINUTE=%time:~3,2%
	if "%CRONJOB_SAVESTAMP_MINUTE:~0,1%" == " " set CRONJOB_SAVESTAMP_MINUTE=0%CRONJOB_SAVESTAMP_MINUTE:~1,1%
	REM ---------------------------------------------------------------------------------------------------------------
	set CRONJOB_SAVESTAMP_SECONDS=%time:~6,2%
	if "%CRONJOB_SAVESTAMP_SECONDS:~0,1%" == " " set CRONJOB_SAVESTAMP_SECONDS=0%CRONJOB_SAVESTAMP_SECONDS:~1,1%
	REM ---------------------------------------------------------------------------------------------------------------
	set CRONJOB_SAVESTAMP_YEAR=%date:~-4%
	set CRONJOB_SAVESTAMP_MONTH=%date:~3,2%
	if "%CRONJOB_SAVESTAMP_MONTH:~0,1%" == " " set CRONJOB_SAVESTAMP_MONTH=0%CRONJOB_SAVESTAMP_MONTH:~1,1%
	set CRONJOB_SAVESTAMP_DAY=%date:~0,2%
	if "%CRONJOB_SAVESTAMP_DAY:~0,1%" == " " set CRONJOB_SAVESTAMP_DAY=0%CRONJOB_SAVESTAMP_DAY:~1,1%
	REM ---------------------------------------------------------------------------------------------------------------
	set CRONJOB_SAVESTAMP=%CRONJOB_SAVESTAMP_YEAR%-%CRONJOB_SAVESTAMP_MONTH%-%CRONJOB_SAVESTAMP_DAY%_%CRONJOB_SAVESTAMP_HOUR%-%CRONJOB_SAVESTAMP_MINUTE%-%CRONJOB_SAVESTAMP_SECONDS%
	REM echo CRONJOB_SAVESTAMP=%CRONJOB_SAVESTAMP%
REM EOF SET TIMESTAMP AND PARAMS

REM ---------------------------------------------------------------------------------------------------------------
REM ---------------------------------------------------------------------------------------------------------------

REM BOF SET FILENAMES
	set PATH_Auftragslisten=C:\Auftragslisten_SERVER\webroot\Auftragslisten\
	set CRONJOB_DIR_EXECUTE_FILES=C:\Auftragslisten_SERVER\webroot\Auftragslisten\cronjobs\
	set CRONJOB_DIR_LOG_FILES=%CRONJOB_DIR_EXECUTE_FILES%logs\
	set CRONJOB_DIR_TEMP_FILES=%CRONJOB_DIR_EXECUTE_FILES%tmp\
	set CRONJOB_BACKUP_PATH=Z:\Software\Auftragslisten_SICHERUNGEN\cronBackUp\

	set CRONJOB_FILENAME_BACKUP_FILES=Auftragslisten_backup_%CRONJOB_SAVESTAMP%_Files.%ARCHIVE_TYPE_FILES%
	set CRONJOB_BASE_FILENAME_BACKUP_FILES=Auftragslisten_base_backup_Files.%ARCHIVE_TYPE_FILES%
	set CRONJOB_FILENAME_BACKUP_DATABASE=Auftragslisten_backup_%CRONJOB_SAVESTAMP%_Database.%ARCHIVE_TYPE_DATABASE%
	set CRONJOB_FILENAME_BACKUP_TABLES=Auftragslisten_backup_%CRONJOB_SAVESTAMP%_DatabaseTables.%ARCHIVE_TYPE_DATABASE%

	set TEMP_CRONJOB_BACKUP_PATH=E:\Auftragslisten_backups\
	set TEMP_CRONJOB_BACKUP_PATH_SQL_FILES=%TEMP_CRONJOB_BACKUP_PATH%tmp\

	set PATH_CLOUD_BACKUP=C:\CLOUD_BACKUP\MagentaCLOUD\Auftragslisten\

	REM -----------------------------------------------------------------------------------------------------------

	set PATH_ZIP=C:\Programme\7-Zip\7z.exe
	set PATH_PHP=C:\Auftragslisten_SERVER\php\php.exe
	set PATH_MYSQLDUMP=C:\Auftragslisten_SERVER\mysql\bin\mysqldump.exe
	set PATH_MYSQL=C:\Auftragslisten_SERVER\mysql\bin\mysql.exe

	REM -----------------------------------------------------------------------------------------------------------

	set KEEP_BACKUP_FILES_DAY_LIMIT_CLOUD=-1
	set FILE_TABLE_LIST=%CRONJOB_DIR_LOG_FILES%listBackUpTables.txt
REM EOF SET FILENAMES

REM ---------------------------------------------------------------------------------------------------------------
REM ---------------------------------------------------------------------------------------------------------------

REM BOF BACK UP MYSQL DATABASE ALL TABLES IN ONE FILE
	if "%ACTION_BACKUP_DATABASE_COMPLETE%"=="true" (
		%PATH_MYSQL% -u %mysqli_DATABASE_DB_USER% -p%mysqli_DATABASE_DB_PASS% %mysqli_DATABASE_DB_NAME% -e "FLUSH TABLES WITH READ LOCK;"
		%PATH_MYSQLDUMP%  --databases burhanctrAuftragslisten -hlocalhost -u %mysqli_DATABASE_DB_USER% -p%mysqli_DATABASE_DB_PASS% > "%CRONJOB_BACKUP_PATH%%CRONJOB_FILENAME_BACKUP_DATABASE%.sql"
		%PATH_MYSQL% -u %mysqli_DATABASE_DB_USER% -p%mysqli_DATABASE_DB_PASS% %mysqli_DATABASE_DB_NAME% -e "UNLOCK TABLES;"
		%PATH_ZIP% a -t%ARCHIVE_TYPE_DATABASE% "%CRONJOB_BACKUP_PATH%%CRONJOB_FILENAME_BACKUP_DATABASE%" "%CRONJOB_BACKUP_PATH%%CRONJOB_FILENAME_BACKUP_DATABASE%.sql"
		del %CRONJOB_BACKUP_PATH%%CRONJOB_FILENAME_BACKUP_DATABASE%.sql
	)
	timeout /t 60
REM EOF BACK UP MYSQL DATABASE IN ONE FILE IN ONE FILE

REM ---------------------------------------------------------------------------------------------------------------
REM ---------------------------------------------------------------------------------------------------------------

REM BOF BACK UP MYSQL DATABASE EACH TABLES IN ONE FILE
	if "%ACTION_BACKUP_DATABASE_TABLES%"=="true" (
		del %CRONJOB_DIR_TEMP_FILES%*.sql
		%PATH_MYSQL% -u %mysqli_DATABASE_DB_USER% -p%mysqli_DATABASE_DB_PASS% %mysqli_DATABASE_DB_NAME% -e "SHOW TABLES;" --skip-column-names > %FILE_TABLE_LIST%
		REM %PATH_MYSQL% -u %mysqli_DATABASE_DB_USER% -p%mysqli_DATABASE_DB_PASS% %mysqli_DATABASE_DB_NAME% -e "FLUSH TABLES WITH READ LOCK;"

		REM BOF LOCK ALL TABLES
			for /f %%T in (%FILE_TABLE_LIST%) do (
				%PATH_MYSQL% -u %mysqli_DATABASE_DB_USER% -p%mysqli_DATABASE_DB_PASS% %mysqli_DATABASE_DB_NAME% -e "LOCK TABLES %%T;"
			)
			timeout /t 4
		REM EOF LOCK ALL TABLES

		REM BOF CREATE TABLE DUMPS
			for /f %%T in (%FILE_TABLE_LIST%) do (
				%PATH_MYSQLDUMP% -u %mysqli_DATABASE_DB_USER% -p%mysqli_DATABASE_DB_PASS% %mysqli_DATABASE_DB_NAME% %%T > %CRONJOB_DIR_TEMP_FILES%%%T.sql
				timeout /t 4
			)
		REM BOF CREATE TABLE DUMPS

		REM BOF UNLOCK ALL TABLES
			%PATH_MYSQL% -u %mysqli_DATABASE_DB_USER% -p%mysqli_DATABASE_DB_PASS% %mysqli_DATABASE_DB_NAME% -e "UNLOCK TABLES;"
		REM EOF UNLOCK ALL TABLES

		%PATH_ZIP% a -t%ARCHIVE_TYPE_DATABASE% "%CRONJOB_DIR_TEMP_FILES%%CRONJOB_FILENAME_BACKUP_TABLES%" "%CRONJOB_DIR_TEMP_FILES%*.sql"
		copy %CRONJOB_DIR_TEMP_FILES%%CRONJOB_FILENAME_BACKUP_TABLES% %CRONJOB_BACKUP_PATH%%CRONJOB_FILENAME_BACKUP_TABLES%
		del %CRONJOB_DIR_TEMP_FILES%%CRONJOB_FILENAME_BACKUP_TABLES%
		del %CRONJOB_DIR_TEMP_FILES%*.sql
		del %FILE_TABLE_LIST%
	)
	timeout /t 60
REM EOF BACK UP MYSQL DATABASE EACH TABLES IN ONE FILE

REM ---------------------------------------------------------------------------------------------------------------
REM ---------------------------------------------------------------------------------------------------------------

REM BOF BACKUP AND ZIP FILES
	REM >>%CRONJOB_DIR_LOG_FILES%cronBackupAuftragslisten2.txt 2>&1
	if "%ACTION_BACKUP_FILES%"=="true" (
		if "%MODE_BACKUP_FILES%"=="new" (
			%PATH_ZIP% -t%ARCHIVE_TYPE_FILES% a "%CRONJOB_BACKUP_PATH%%CRONJOB_FILENAME_BACKUP_FILES%" "%PATH_Auftragslisten%" -rx!_tmp/ -mx=7 -w%CRONJOB_BACKUP_PATH%tmp >>%CRONJOB_DIR_LOG_FILES%cronBackupAuftragslisten2.txt 2>&1
		)
		if "%MODE_BACKUP_FILES%"=="update" (		
			REM -ms=off -mx=7
			%PATH_ZIP% -t%ARCHIVE_TYPE_FILES% u "%CRONJOB_BACKUP_PATH%%CRONJOB_BASE_FILENAME_BACKUP_FILES%" "%PATH_Auftragslisten%" -ms=off -mx=7 -rx!_tmp/ -w%CRONJOB_BACKUP_PATH%tmp >>%CRONJOB_DIR_LOG_FILES%cronBackupAuftragslisten2.txt 2>&1
			timeout /t 60
			copy "%CRONJOB_BACKUP_PATH%%CRONJOB_BASE_FILENAME_BACKUP_FILES%" %CRONJOB_BACKUP_PATH%%CRONJOB_FILENAME_BACKUP_FILES%
		)
	)
	timeout /t 60
REM EOF BACKUP AND ZIP FILES

REM ---------------------------------------------------------------------------------------------------------------
REM ---------------------------------------------------------------------------------------------------------------

REM BOF DELETE OLDER BACKUP FILES ON LOCAL SERVER
	REM set KEEP_BACKUP_FILES_DAY_LIMIT_LOCAL=-50
	REM forfiles /p %CRONJOB_BACKUP_PATH% /s /m *.* /c "cmd /c del @path" /d %KEEP_BACKUP_FILES_DAY_LIMIT_LOCAL%
REM EOF DELETE OLDER BACKUP FILES ON LOCAL SERVER

REM ---------------------------------------------------------------------------------------------------------------
REM ---------------------------------------------------------------------------------------------------------------

REM BOF COPY BACKUP FILES TO CLOUD STORAGE
	if "%ACTION_USE_CLOUD_STORAGE%"=="true" (

		REM BOF DELETE OLDER BACKUP FILES IN CLOUD
			REM forfiles /p "C:\source_folder" /s /m *.* /c "cmd /c Del @path" /d -7
			forfiles /p %PATH_CLOUD_BACKUP% /s /m *.* /c "cmd /c del @path" /d %KEEP_BACKUP_FILES_DAY_LIMIT_CLOUD%
		REM EOF DELETE OLDER BACKUP FILES IN CLOUD

		REM BOF COPY BACKUP FILES TO CLOUD
			if "%ACTION_BACKUP_FILES%"=="true" (
				copy %CRONJOB_BACKUP_PATH%%CRONJOB_FILENAME_BACKUP_FILES% %PATH_CLOUD_BACKUP%%CRONJOB_FILENAME_BACKUP_FILES%
			)
		REM EOF COPY BACKUP FILES TO CLOUD

		REM BOF COPY BACKUP DATABASE COMPLETE TO CLOUD
			if "%ACTION_BACKUP_DATABASE_COMPLETE%"=="true" (
				copy %CRONJOB_BACKUP_PATH%%CRONJOB_FILENAME_BACKUP_DATABASE% %PATH_CLOUD_BACKUP%%CRONJOB_FILENAME_BACKUP_DATABASE%
			)
		REM EOF COPY BACKUP DATABASE COMPLETE TO CLOUD

		REM BOF COPY BACKUP DATABASE TABLES TO CLOUD
			if "%ACTION_BACKUP_DATABASE_TABLES%"=="true" (
				copy %CRONJOB_BACKUP_PATH%%CRONJOB_FILENAME_BACKUP_TABLES% %PATH_CLOUD_BACKUP%%CRONJOB_FILENAME_BACKUP_TABLES%
			)
		REM EOF COPY BACKUP DATABASE TABLES TO CLOUD
	)
REM EOF COPY BACKUP FILES TO CLOUD STORAGE

REM ---------------------------------------------------------------------------------------------------------------
REM ---------------------------------------------------------------------------------------------------------------

timeout /t 200
>>%CRONJOB_DIR_LOG_FILES%cronBackupAuftragslisten2.txt 2>&1


