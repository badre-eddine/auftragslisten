<?php
	require_once('inc/requires.inc.php');

	if(!$arrGetUserRights["editSalesmen"] && !$arrGetUserRights["displaySalesmen"]) {
		header('location: ' . PAGE_EXIT_LOCATION);
		exit;
	}

	// BOF DEFINE DUTY FIELDS
		$arrFormDutyFields = array(
			// "editSalesmenKundennummer" => "Kundennummer",
			// "editSalesmenFirmenname" => "Firmenname",
			// "editSalesmenTyp" => "Kunden-Typ",
			// "editSalesmenGruppe[]" => "Kunden-Gruppe"
			// "editSalesmenGruppe" => "Kunden-Gruppe"
		);
		// CREATE JSON DUTY FIELD
		$jsonFormDutyFields = createJson($arrFormDutyFields);
	// BOF DEFINE DUTY FIELDS

	if($_REQUEST["editID"] == "")  {
		// header("location: displayOrders.php");
		// exit;
	}

	DEFINE('SHOW_COUNTRY_AREAS', true);
	DEFINE('SHOW_ZIPCODE_AREAS', true);
	DEFINE('AREA_COLORS_TYPE', 'perSalesman'); // perZipcode | perSalesman

	$jswindowMessage = '';
	$warningMessage = '';
	$errorMessage = '';
	$successMessage = '';
	$infoMessage = '';

	$dbConnection = new DB_Connection();
	$db_open = $dbConnection->db_connect();

	if(DB_HOST_EXTERN_ACQUISITION != '' && DB_NAME_EXTERN_ACQUISITION != '' && DB_USER_EXTERN_ACQUISITION != '' && DB_PASSWORD_EXTERN_ACQUISITION) {
		$existsOnlineAquisition = true;
		$dbConnection_ExternAcqisition = new DB_Connection(DB_HOST_EXTERN_ACQUISITION, '', DB_NAME_EXTERN_ACQUISITION, DB_USER_EXTERN_ACQUISITION, DB_PASSWORD_EXTERN_ACQUISITION);
		$db_openExternAcqisition = $dbConnection_ExternAcqisition->db_connect();
	}
	else {
		$existsOnlineAquisition = false;
		$errorMessage .= 'Es sind keine Zugansdaten f&uuml;r die Datenbankverbindung zur ' . strtoupper(MANDATOR) . '-Kundenerfassung eingetragen. ' . '<br />';
	}


	// BOF DOWNLOAD FILE
	if($_GET["downloadFile"] != "" && $_GET["documentType"] != "") {
		$fileDirectory = DIRECTORY_CREATED_DOCUMENTS_SALESMEN . '';
		$_GET["downloadFile"] = utf8_decode(urldecode($_GET["downloadFile"]));

		require_once('classes/downloadFile.class.php');
		$thisDownload = new downloadFile($fileDirectory, $_GET["downloadFile"]);
		$errorMessage .= $thisDownload->startDownload();
	}
	// EOF DOWNLOAD FILE


	// BOF SEND MAIL WITH ATTACHED FILE
	if($_POST["sendAttachedDocument"] == '1') {
		$thisCreatedPdfName = $_POST["mailDocumentFilename"];
		$_POST["selectSubject"] = $arrDocumentTypeDatas[$_REQUEST["documentType"]]["createdDocumentsTypesName"] . ' Nr.: ' . '{###DOCUMENT_NUMBER###}';
		$generatedDocumentNumber = substr($_POST["mailDocumentFilename"], 0, 13);

		require_once(DIRECTORY_HTML_MIME_MAIL . 'htmlMimeMail.php');
		require_once(DIRECTORY_MAIL_TEMPLATES . 'template_BASIC.inc.php');
		require_once(DIRECTORY_MAIL_TEMPLATES . 'template_TEXT_DATAS.inc.php');
		##require_once("inc/mail.inc.php");

		// BOF SEND MAIL
		require_once("classes/createMail.class.php");

		// BOF CREATE SUBJECT
		$thisSubject = $_POST["selectSubject"];
		if($_POST["sendAttachedMailSubject"] != '') {
			$thisSubject = $_POST["sendAttachedMailSubject"] . ', ' . $thisSubject;
		}
		// EOF CREATE SUBJECT

		// BOF GET ATTACHED FILE
		if($_REQUEST["documentType"] == 'PR') {
			$pathDocumentFolder = DIRECTORY_CREATED_DOCUMENTS_SALESMEN . '';
			$thisCreatedPdfName = utf8_decode($thisCreatedPdfName);
		}

		else {
			$pathDocumentFolder = DIRECTORY_CREATED_DOCUMENTS_SALESMEN;
		}

		$arrAttachmentFiles = array (
			rawurldecode($pathDocumentFolder . $thisCreatedPdfName) => str_replace(" ", "_", rawurldecode(myUnHtmlEntities($thisCreatedPdfName)))
		);
		// EOF GET ATTACHED FILE

		$createMail = new createMail(
							$thisSubject,													// TEXT:	MAIL_SUBJECT
							$_POST["documentType"],											// STRING:	DOCUMENT TYPE
							$generatedDocumentNumber,										// STRING:	DOCUMENT NUMBER
							$arrAttachmentFiles,											// ARRAY:	ATTACHMENT FILES
							$_POST["selectCustomersRecipientMail"],	 						// STRING:	RECIPIENTS
							$arrMailContentDatas[$_POST["selectMailtextTemplates"]],		// MAIL_TEXT_TEMPLATE
							$mailTemplate["html"],											// MAIL_BODY_TEMPLATE
							'',																// STRING:	ADDITIONAL TEXT
							$_POST["sendAttachedMailText"],									// STRING:	SEND ATTACHED TEXT
							true,															// BOOLEAN:	SEND ATTACHED DOCUMENT
							DIRECTORY_MAIL_IMAGES,											// DIRECTORY_MAIL_IMAGES
							$_POST["selectMailtextSender"]									// STRING: SENDER
						);

		$createMailResult = $createMail->returnResult();
		$sendMailToClient = $createMailResult;

		#$tmpErrorResult = $createMail->returnErrors();

		#if(!$sendMailToUs) { $errorMessage .= ' Die Mail-Kopie konnte nicht an ' . implode(', ', $arrOrderMail) . ' versendet werden! '.'<br />'; }
		#else { $successMessage .= ' Die Mail-Kopie wurde an ' . implode(', ', $arrOrderMail) . ' versendet! '.'<br />'; }

		if(!$sendMailToClient) { $errorMessage .= ' Die Mail konnte nicht an ' . $_POST["selectCustomersRecipientName"] . ' (' . $_POST["selectCustomersRecipientMail"] . ') versendet werden! '.'<br />'; }
		else {
			$successMessage .= ' Die Mail wurde an ' . $_POST["selectCustomersRecipientName"] . ' (' . $_POST["selectCustomersRecipientMail"] . ') versendet! '.'<br />';
		}
		if($_REQUEST["documentType"] == 'SP') {
			if(file_exists($targetPDF)){
				unlink($targetPDF);
			}
		}
	}
	// EOF SEND MAIL WITH ATTACHED FILE

	// BOF READ VERTRETER
		$arrAgentDatas = getAgents();
		$arrAgentDatas2 = getAgents2();
	// EOF READ VERTRETER

	// BOF READ SALUTATIONS
		$arrSalutationTypeDatas = getSalutationTypes();
	// EOF READ SALUTATIONS

	// BOF READ PAYMENT TYPES
		$arrPaymentTypeDatas = getPaymentTypes();
	// EOF READ PAYMENT TYPES

	// BOF READ PAYMENT CONDITIONS
		$arrPaymentConditionDatas = getPaymentConditions();
	// EOF READ PAYMENT CONDITIONS

	// BOF READ BESTELL ARTEN
		$arrOrderTypeDatas = getOrderTypes();
	// EOF READ BESTELL ARTEN

	// BOF READ ADDITIONAL COSTS
		$arrAdditionalCostsDatas = getAdditionalCosts();
	// EOF READ ADDITIONAL COSTS

	// BOF READ COUNTRIES
		$arrCountryTypeDatas = getCountryTypes();
	// EOF READ COUNTRIES

	// BOF READ CUSTOMER TYPES
		$arrSalesmenTypeDatas = getSalesmenTypes();
	// EOF READ CUSTOMER TYPES

	// BOF READ DOCUMENT TYPE DATAS
		$arrDocumentTypeDatas = getDocumentTypes();
	// EOF READ DOCUMENT TYPE DATAS

	// BOF READ CUSTOMER GROUP DATAS
		$arrSalesmenGroupDatas = getSalesmenGroups();
	// EOF READ CUSTOMER GROUP DATAS

	// BOF READ PERSONNEL DATAS
		$arrPersonnelDatas = getPersonnelDatas();
	// EOF READ PERSONNEL DATAS

	// BOF CANCEL FILES
		if($_POST["cancelDatas"] != "") {
			$_POST["editSalesmenID"] = '';
		}
	// BOF CANCEL FILES

	// BOF STORE FILES
		if($_POST["storeFiles"] != "") {
			// BOF STORE FILES
			// EOF STORE FILES
			// $_REQUEST["editID"] = $_POST["editSalesmenKundennummer2"];
		}
	// BOF STORE FILES

	// BOF STORE DATAS
		if($_POST["storeDatas"] != "") {
			$doAction = checkFormDutyFields($arrFormDutyFields);

			if($doAction) {
				if($_POST["editSalesmenID"] == "") {
					$_POST["editSalesmenID"] = "%";
					$sqlInsertType = " INSERT ";
					// BOF CREATE COSTUMER NUMBER
					// EOF CREATE COSTUMER NUMBER
				}
				else {
					$sqlInsertType = " REPLACE ";
				}

				if(trim($_POST["editSalesmenAreas"]) != "") {
					$strEditSalesmenAreas = removeUnnecessaryChars(preg_replace("/\\n/", ";", $_POST["editSalesmenAreas"]));
					$arrEditSalesmenAreas = explode(";", $strEditSalesmenAreas);
					sort($arrEditSalesmenAreas);
					$strEditSalesmenAreas = implode(";", $arrEditSalesmenAreas);
				}
				else {
					$strEditSalesmenAreas = '';
				}

				$strGoogleMapPoints = "";
				$strGoogleMapZipCodePolygons = "";
				if($_POST["editSalesmenUseMwst"] == ''){ $_POST["editSalesmenUseMwst"] = '0'; }
				if($_POST["editSalesmenExportDatas"] == ''){ $_POST["editSalesmenExportDatas"] = '0'; }
				if($_POST["editSalesmenActive"] == ''){ $_POST["editSalesmenActive"] = '0'; }
				if($_POST["editSalesmenAreaProtection"] == ''){ $_POST["editSalesmenAreaProtection"] = '0'; }
				if($_POST["editSalesmenAreasActive"] == ''){ $_POST["editSalesmenAreasActive"] = '0'; }

				$sql = "
					" . $sqlInsertType . " INTO `" . TABLE_SALESMEN . "`(
								`salesmenID`,
								`salesmenKundennummer`,
								`salesmenFirmenname`,
								`salesmenSubSalesmanID`,
								`salesmenAreas`,
								`salesmenAreasActive`,
								`salesmenAreaProtection`,
								`salesmenGetProvision`,
								`salesmenProvision`,
								`salesmenTypesID`,
								`salesmenTypesName`,
								`salesmenTimeCreated`,
								`salesmenActive`,
								`salesmenMapPoints`,
								`salesmenMapAreaPoints`,
								`salesmenExportDatas`,
								`salesmenUseMwst`,
								`salesmenPersonnelID`
							)
							VALUES (
								'".$_POST["editSalesmenID"]."',
								'".$_POST["editSalesmenKundennummer"]."',
								'".($_POST["editSalesmenFirmenname"])."',
								'".($_POST["editSalesmenSubSalesmanID"])."',
								'".$strEditSalesmenAreas."',
								'".($_POST["editSalesmenAreasActive"])."',
								'".($_POST["editSalesmenAreaProtection"])."',
								'".$_POST["editSalesmenGetProvision"]."',
								'".str_replace(",", ".", $_POST["editSalesmenProvision"])."',
								'".($_POST["editSalesmenTypesID"])."',
								'".($arrSalesmenTypeDatas[$_POST["editSalesmenTypesID"]]["salesmenTypesName"])."',
								NOW(),
								'".$_POST["editSalesmenActive"]."',
								'".addslashes($strGoogleMapPoints)."',
								'".$strGoogleMapZipCodePolygons."',
								'".$_POST["editSalesmenExportDatas"]."',
								'".$_POST["editSalesmenUseMwst"]."',
								'".$_POST["editSalesmenPersonnelID"]."'
							)
					";

				$rs = $dbConnection->db_query($sql);

				if($rs) {
					$successMessage .= ' Der Datensatz wurde gespeichert. ' . '<br />';
				}
				else {
					$errorMessage .= ' Der Datensatz konnte nicht gespeichert werden. ' . '<br />';
				}

				// BOF UPDATE CUSTOMERS WITH VERTRETER ID DEPENDING ON VERTRETER-TYP VERTRETER-ID, CUSTOMERS-ZIPCODE AND BURHAN-CUSTOMER
					// IF VERTRETER-TYP IS ADM OR EX-ADM
					if($_POST["editSalesmenTypesID"] == '9' || $_POST["editSalesmenTypesID"] == '10' || $_POST["editSalesmenTypesID"] == '4'){
						// BOF SET CUSTOMERS AS BURHAN CUSTOMERS WHERE NO SALESMAN IS SET
							// ENDKUNDEN, GEBRAUCHTWAGENHÄNDLER, VERTRAGSHÄNDLER
							$sql_update = "
									UPDATE `" . TABLE_CUSTOMERS . "`
										SET `customersVertreterID` = 13301
										WHERE 1
										AND (
											`customersVertreterID` = 0
											OR
											`customersVertreterID` IS NULL
											OR
											`customersVertreterID` = ''
										)
										AND (
											`customersTyp` = '1'
											OR
											`customersTyp` = '5'
											OR
											`customersTyp` = '6'
										)
								";
							$rs_update = $dbConnection->db_query($sql_update);
						// EOF SET CUSTOMERS AS BURHAN CUSTOMERS WHERE NO SALESMAN IS SET

						// BOF SET CUSTOMERS AS B3 CUSTOMERS WHERE NO SALESMAN IS SET
							// WIEDERVERKÄUFER
							/*
							$sql_update = "
									UPDATE
										`" . TABLE_CUSTOMERS . "`
										SET `customersVertreterID` = '5684'

										WHERE 1
											AND (
												`customersVertreterID` = '0'
												OR
												`customersVertreterID` IS NULL
												OR
											`customersVertreterID` = ''
											)
											AND `customersTyp` = '3'
								";
							#$rs_update = $dbConnection->db_query($sql_update);
							*/
							$sql_update = "
									UPDATE
										`" . TABLE_CUSTOMERS . "`
										SET `customersVertreterID` = '5684'
										WHERE 1
											AND (
												`customersTyp` = '8'
												OR
												`customersTyp` = '7'
											)
								";
							$rs_update = $dbConnection->db_query($sql_update);
							// AMAZON, EBAY
						// EOF SET CUSTOMERS AS B3 CUSTOMERS WHERE NO SALESMAN IS SET

						// BOF SET CUSTOMERS THIS ADM AREAS AS BURHAN CUSTOMERS
						$sql_reset = "
									UPDATE `" . TABLE_CUSTOMERS . "`
										SET
											`customersVertreterID` = '13301',
											`customersVertreterName` = 'BURHAN CTR e. K.'
										WHERE 1
											AND `customersVertreterID` = '" . $_POST["editOnlineSalesmenAuftragslistenCustomerID"] . "'
											AND (
												`customersTyp` = '1'
												OR
												`customersTyp` = '5'
												OR
												`customersTyp` = '6'
											)
								";
							#$rs_reset = $dbConnection->db_query($sql_reset);
							if($rs_reset) {
								$successMessage .= ' Die dem ADM zugeordneten Kunden wurden wieder als Burhan-Kunde zugeordnet.<br />';
							}
							else {
								$errorMessage .= ' Die dem ADM zugeordneten Kunden konnten nicht wieder als Burhan-Kunde zugeordnet werden.<br />';
							}
							#dd('sql_reset');
						// EOF SET CUSTOMERS THIS ADM AREAS AS BURHAN CUSTOMERS
						if($_POST["editSalesmenAreasActive"] == '1'){
							// $strEditSalesmenAreas = implode(";", $arrEditSalesmenAreas);
							if(!empty($arrEditSalesmenAreas)){
								$sql_set = "
									UPDATE `" . TABLE_CUSTOMERS . "`

										SET
											`customersVertreterID` = '" . $_POST["editOnlineSalesmenAuftragslistenCustomerID"] . "'
											/*
											,
											`customersVertreterName` = '" . $_POST["editSalesmenFirmenname"] . "'
											*/
										WHERE 1
											AND `customersVertreterID` = '13301'
											AND `customersCompanyCountry` = '81'

											/*
											AND (SUBSTRING(`customersCompanyPLZ`, 1, 2) = '" . implode("' OR SUBSTRING(`customersCompanyPLZ`, 1, 2) = '", $arrEditSalesmenAreas). "')
											*/

											AND (`customersCompanyPLZ` LIKE '" . implode("%' OR `customersCompanyPLZ` LIKE '", $arrEditSalesmenAreas). "%')

											AND (
												`customersTyp` = '1'
												OR
												`customersTyp` = '5'
												OR
												`customersTyp` = '6'
											)
									";
								#$rs_set = $dbConnection->db_query($sql_set);
								#dd('sql_set');
								if($rs_set) {
									$successMessage .= ' Der ADM wurde den Burhan-Kunden aus den PLZ-Gebieten. ' .  $strEditSalesmenAreas . ' zugeordnet.<br />';
								}
								else {
									$errorMessage .= ' Der ADM konnte nicht den Burhan-Kunden aus den PLZ-Gebieten. ' .  $strEditSalesmenAreas . ' zugeordnet werden.<br />';
								}
							}
						}
						else {
							$sql_reset = "
									UPDATE `" . TABLE_CUSTOMERS . "`
										SET
											`customersVertreterID` = '13301',
											`customersVertreterName` = 'BURHAN CTR e. K.'
										WHERE 1
											AND `customersVertreterID` = '" . $_POST["editOnlineSalesmenAuftragslistenCustomerID"] . "'
								";
							$rs_reset = $dbConnection->db_query($sql_reset);
							if($rs_reset) {
								$successMessage .= ' Die dem ADM zugeordneten Kunden wurden wieder als Burhan-Kunde zugeordnet.<br />';
							}
							else {
								$errorMessage .= ' Die dem ADM zugeordneten Kunden konnten nicht wieder als Burhan-Kunde zugeordnet werden.<br />';
							}
							# dd('sql_reset');
						}
					}
				// EOF UPDATE CUSTOMERS WITH VERTRETER ID DEPENDING ON VERTRETER-TYP, VERTRETER-ID, CUSTOMERS-ZIPCODE AND BURHAN-CUSTOMER

				// BOF STORE DATAS IN ONLINE AQUISITION
				if($existsOnlineAquisition) {
					$sql_extern = "
						UPDATE `" . DB_NAME_EXTERN_ACQUISITION . "`.`" . TABLE_ACQUISATION_USERS . "`

						SET
							`usersAuftragslistenCustomerID` = '" . $_POST["editOnlineSalesmenAuftragslistenCustomerID"] . "',
							`usersAreas` = '" . $strEditSalesmenAreas . "',
							`usersActive` = '" . $_POST["editSalesmenActive"] . "',
							`usersLocation` = '" . $_POST["editSalesmenLocation"] . "'

						WHERE 1
							AND `usersID` = '" .  $_POST["editOnlineSalesmenUserID"] . "'
					";

					$rs_extern = $dbConnection_ExternAcqisition->db_query($sql_extern);

					if($rs_extern) {
						$successMessage .= ' Der Datensatz wurde in der Online-Kundenerfassung gespeichert. ' . '<br />';
					}
					else {
						$errorMessage .= ' Der Datensatz konnte nicht in der Online-Kundenerfassung gespeichert werden. ' . '<br />';
					}
				}
				// EOF STORE DATAS IN ONLINE AQUISITION

				if($sqlInsertType == "INSERT") { $_REQUEST["editID"] = $dbConnection->db_getInsertID(); }
				else { $_REQUEST["editID"] = $_POST["editSalesmenID"]; }
				$_REQUEST["editID"] = '';
			}
			else {
				$errorMessage .= ' Der Datensatz konnte nicht gespeichert werden. Bitte füllen Sie alle Pflichtfelder aus. ' .'<br />';
			}

			// BOF STORE XML FILE FOR MAP

				$arrColorsPLZ = array(
						'0xFFF200',
						'0xFDB913',
						'0xF58220',
						'0xE31D3C',
						'0xF58F98',
						'0x9A258F',
						'0x0094C9',
						'0x00C0E8',
						'0x008060',
						'0x23AA4A',
						'0x80C342',
						'0x905501',

						'0xFFCCFF',
						'0xFFCC99',
						'0xCC6600',
						'0x99CCCC',
						'0x66FF33',
						'0xFFFF33',
						'0x9acd32',
						'0xee82ee',
						'0xd2b48c',
						'0x66cdaa',
						'0x98fb98',
						'0x00bfff',
						'0xf0b488',

						'0xFFFF00',
						'0xFFA500',
						'0xFF00FF',
						'0xFF0000',
						'0xC0C0C0',
						'0x808080',
						'0x808000',
						'0x800080',
						'0x800000',
						'0x00FFFF',
						'0x00FF00',
						'0x008080',
						'0x008000',
						'0x0000FF',
						'0x000080',
					);

				function getColors($index, $mode=''){
					global $arrColorsPLZ;
					if($mode == '' || $mode == 'perZipcode'){
						$thisColor = $arrColorsPLZ[$index];
					}
					else if($mode == 'perSalesman'){
						$thisColor = $arrColorsPLZ[$index];
					}
					return $thisColor;
				}

				$contentXML = '';
				$contentXML .= '<?xml version="1.0" encoding="utf-8" ?>' . "\n";
				$contentXML .= '<salesmenDatas>' . "\n";


				$sql_xml = "SELECT
							`" . TABLE_SALESMEN . "`.`salesmenID`,
							`" . TABLE_SALESMEN . "`.`salesmenKundennummer`,
							`" . TABLE_SALESMEN . "`.`salesmenFirmenname`,
							`" . TABLE_SALESMEN . "`.`salesmenSubSalesmanID`,
							`" . TABLE_SALESMEN . "`.`salesmenAreas`,
							`" . TABLE_SALESMEN . "`.`salesmenAreasActive`,
							`" . TABLE_SALESMEN . "`.`salesmenAreaProtection`,
							`" . TABLE_SALESMEN . "`.`salesmenGetProvision`,
							`" . TABLE_SALESMEN . "`.`salesmenProvision`,
							`" . TABLE_SALESMEN . "`.`salesmenTypesID`,
							`" . TABLE_SALESMEN . "`.`salesmenTypesName`,
							`" . TABLE_SALESMEN . "`.`salesmenTimeCreated`,
							`" . TABLE_SALESMEN . "`.`salesmenActive`,
							`" . TABLE_SALESMEN . "`.`salesmenMapPoints`,
							`" . TABLE_SALESMEN . "`.`salesmenMapAreaPoints`,
							`" . TABLE_SALESMEN . "`.`salesmenExportDatas`,
							`" . TABLE_SALESMEN . "`.`salesmenUseMwst`,
							`" . TABLE_SALESMEN . "`.`salesmenPersonnelID`,

							`" . TABLE_CUSTOMERS . "`.`customersTelefon1`,
							`" . TABLE_CUSTOMERS . "`.`customersTelefon2`,
							`" . TABLE_CUSTOMERS . "`.`customersMobil1`,
							`" . TABLE_CUSTOMERS . "`.`customersMobil2`,
							`" . TABLE_CUSTOMERS . "`.`customersFax1`,
							`" . TABLE_CUSTOMERS . "`.`customersFax2`,
							`" . TABLE_CUSTOMERS . "`.`customersMail1`,
							`" . TABLE_CUSTOMERS . "`.`customersMail2`

						FROM `" . TABLE_SALESMEN . "`

						LEFT JOIN `" . TABLE_CUSTOMERS . "`
						ON(`" . TABLE_SALESMEN . "`.`salesmenKundennummer` = `" . TABLE_CUSTOMERS . "`.`customersKundennummer`)

						WHERE 1
							AND `" . TABLE_SALESMEN . "`.`salesmenAreas` != ''
							AND `" . TABLE_SALESMEN . "`.`salesmenAreasActive` = '1'
							AND `" . TABLE_SALESMEN . "`.`salesmenActive` = '1'

						ORDER BY LENGTH(`" . TABLE_SALESMEN . "`.`salesmenAreas`)
					";
				$rs_xml = $dbConnection->db_query($sql_xml);

				$countItem = 0;

				/*
				while($ds_xml = mysqli_fetch_assoc($rs_xml)){
					$arrAreas = explode(';', $ds_xml["salesmenAreas"]);
					if(!empty($arrAreas)){
						foreach($arrAreas as $thisArea) {
							if(preg_match("/-/", $thisArea)){
								$arrTemp = explode("-", $thisArea);
								for($i = $arrTemp[0] ; $i < ($arrTemp[1] + 1) ; $i++){
									$thisZipcode = $i;
									if($thisZipcode < 10){ $thisZipcode = '0' . $thisZipcode; }
									$contentXML .= "\t" . '<areaPLZ code="' . $thisZipcode . '">' . "\n";
									$contentXML .= "\t\t" . '<salesman knr="' . $ds_xml["salesmenKundennummer"] . '" name1="' . $ds_xml["salesmenFirmenname"] . '" name2="" phone="' . formatPhoneNumber($ds_xml["customersTelefon1"]) . '" mobil="' . formatPhoneNumber($ds_xml["customersMobil1"]) . '" mail="' . $ds_xml["customersMail1"] . '" areacolor="' . $arrColorsPLZ[$countItem] . '"></salesman>' . "\n";
									$contentXML .= "\t" . '</areaPLZ>' . "\n";
								}
							}
							else {
								$contentXML .= "\t" . '<areaPLZ code="' . $thisArea . '">' . "\n";
								$contentXML .= "\t\t" . '<salesman knr="' . $ds_xml["salesmenKundennummer"] . '" name1="' . $ds_xml["salesmenFirmenname"] . '" name2="" phone="' . formatPhoneNumber($ds_xml["customersTelefon1"]) . '" mobil="' . formatPhoneNumber($ds_xml["customersMobil1"]) . '" mail="' . $ds_xml["customersMail1"] . '" areacolor="' . $arrColorsPLZ[$countItem] . '"></salesman>' . "\n";
								$contentXML .= "\t" . '</areaPLZ>' . "\n";
							}
						}
					}
					$countItem++;
				}
				*/

				$arrSalesmanDatasPerZipCode = array();
				$countSalesman = 0;
				while($ds_xml = mysqli_fetch_assoc($rs_xml)){
					$arrAreas = explode(';', $ds_xml["salesmenAreas"]);
					#dd('ds_xml');
					// BOF GET AREA COLOR PER SALESMAN
						if(AREA_COLORS_TYPE == 'perSalesman'){
							$thisColor = $arrColorsPLZ[$countSalesman];
						}
						else if(AREA_COLORS_TYPE == 'perZipcode'){
							$thisColor = '{###AREA_COLOR###}';
						}
					// EOF GET AREA COLOR PER SALESMAN

					if(!empty($arrAreas)){
						foreach($arrAreas as $thisAreaKey => $thisArea) {
							if(preg_match("/-/", $thisArea)){
								$arrTemp = explode("-", $thisArea);
								for($i = $arrTemp[0] ; $i < ($arrTemp[1] + 1) ; $i++){
									$thisZipcode = $i;
									if($thisZipcode < 10){ $thisZipcode = '0' . $thisZipcode; }								
									$arrSalesmanDatasPerZipCode[substr($thisZipcode, 0, 2)][] = '<salesman knr="' . $ds_xml["salesmenKundennummer"] . '" name1="' . $ds_xml["salesmenFirmenname"] . '" name2="" zipcode="' . $thisZipcode . '" phone="' . formatPhoneNumber($ds_xml["customersTelefon1"]) . '" mobil="' . formatPhoneNumber($ds_xml["customersMobil1"]) . '" mail="' . $ds_xml["customersMail1"] . '" areacolor="' . $thisColor . '"></salesman>';
								}
							}
							else {
								$arrSalesmanDatasPerZipCode[substr($thisArea, 0, 2)][] = '<salesman knr="' . $ds_xml["salesmenKundennummer"] . '" name1="' . $ds_xml["salesmenFirmenname"] . '" name2="" zipcode="' . $thisArea . '" phone="' . formatPhoneNumber($ds_xml["customersTelefon1"]) . '" mobil="' . formatPhoneNumber($ds_xml["customersMobil1"]) . '" mail="' . $ds_xml["customersMail1"] . '" areacolor="' . $thisColor . '"></salesman>';
							}
						}
					}
					$countSalesman++;
				}

				$countItem = 0;
				if(!empty($arrSalesmanDatasPerZipCode)){
					$arrZipcodes = array_keys($arrSalesmanDatasPerZipCode);
					sort($arrZipcodes);
					foreach($arrZipcodes as $thisZipcode){
						$thisZipcodeData = $arrSalesmanDatasPerZipCode[$thisZipcode];
						$thisZipcodeData = array_unique($thisZipcodeData);
						$contentXML .= "\t" . '<areaPLZ code="' . $thisZipcode . '">' . "\n";

						// BOF GET AREA COLOR PER ZIPCODE
							if(AREA_COLORS_TYPE == 'perZipcode'){
								$thisColor = $arrColorsPLZ[$countItem];
								$thisColor2 = getColors($countItem, 'perZipcode');
							}
						// EOF GET AREA COLOR PER ZIPCODE

						foreach($thisZipcodeData as $thisZipcodeDataKey => $thisZipcodeDataValue){
							$contentXML .= "\t\t" . preg_replace("/{###AREA_COLOR###}/", $thisColor, $thisZipcodeDataValue) . "\n";
						}

						$contentXML .= "\t" . '</areaPLZ>' . "\n";

						$countItem++;
						if($arrColorsPLZ[$countItem] == ''){
							$countItem = 0;
						}
					}
				}

				$contentXML .= '</salesmenDatas>' . "\n";


				if(file_exists(PATH_XML_FILE_SALESMEN)){
					unlink(PATH_XML_FILE_SALESMEN);
				}
				$fp = fopen(PATH_XML_FILE_SALESMEN, 'w');
				fwrite($fp, $contentXML);
				fclose($fp);
			// EOF STORE XML FILE FOR MAP
		}
	// EOF STORE DATAS

	// BOF DELETE DATAS
		if($_POST["deleteDatas"] != "") {
			$sql = "DELETE FROM `" . TABLE_SALESMEN . "` WHERE `salesmenID` = '".$_POST["editSalesmenID"]."'";
			$rs = $dbConnection->db_query($sql);

			if($rs) {
				$successMessage .= ' Der Datensatz wurde endgültig gelöscht. '.'<br />';
			}
			else {
				$errorMessage .= ' Der Datensatz konnte nicht gelöscht werden. '.'<br />';
			}
		}
	// EOF DELETE DATAS

	// BOF READ ALL DATAS
		$where = "";

		if($_REQUEST["searchSalesmenNumber"] != "") {
			$where = " AND (
							`" . TABLE_SALESMEN . "`.`salesmenKundennummer` = '" . $_REQUEST["searchSalesmenNumber"] . "'
							OR
							`" . TABLE_CUSTOMERS . "`.`customersKundennummer` = '" . $_REQUEST["searchSalesmenNumber"] . "'
						)
				";
			$_REQUEST["editID"] = "";
		}
		else if($_REQUEST["searchSalesmenName"] != "") {
			$where = " AND (
						`" . TABLE_SALESMEN . "`.`salesmenFirmenname` LIKE '%" . $_REQUEST["searchSalesmenName"] . "%'
						OR
						`" . TABLE_CUSTOMERS . "`.`customersFirmenname` LIKE '%" . $_REQUEST["searchSalesmenName"] . "%'
				)
			";
			$_REQUEST["editID"] = "";
		}

		else if($_REQUEST["searchSalesmenType"] != ""){
			$where = " AND `" . TABLE_CUSTOMERS . "`.`customersTyp` = '" . $_REQUEST["searchSalesmenType"] . "' ";
		}

		else if($_REQUEST["searchSalesmenPlzActive"] != ""){
			$where = " AND `" . TABLE_SALESMEN . "`.`salesmenAreasActive` = '" . $_REQUEST["searchSalesmenPlzActive"] . "' ";
		}

		else if($_REQUEST["searchWord"] != "") {
			$where = " AND (
							`" . TABLE_SALESMEN . "`.`salesmenKundennummer` LIKE '%" . $_REQUEST["searchWord"] . "%'
							OR
							`" . TABLE_CUSTOMERS . "`.`customersKundennummer` = '" . $_REQUEST["searchWord"] . "'
							OR
							`" . TABLE_SALESMEN . "`.`salesmenFirmenname` LIKE '%" . $_REQUEST["searchWord"] . "%'
							OR
							`" . TABLE_CUSTOMERS . "`.`customersFirmenname` LIKE '%" . $_REQUEST["searchWord"] . "%'

						) ";
			$_REQUEST["editID"] = "";
		}
		else if ($_GET["searchBoxSalesman"] != "") {
			$where = " AND (
							`salesmenKundennummer` LIKE '" . $_GET["searchBoxSalesman"] . "%'
							OR
							`salesmenFirmenname` LIKE '%" . $_GET["searchBoxSalesman"] . "%'

						) ";

			$_REQUEST["editID"] = "";
		}
		if($_POST["editSalesmenID"] > 0) {
			$_REQUEST["editID"] = $_POST["editSalesmenID"];
		}
		if($_REQUEST["editID"] == "") {
			$sql = "SELECT
						/* SQL_CALC_FOUND_ROWS */

						`" . TABLE_SALESMEN . "`.`salesmenID`,
						`" . TABLE_SALESMEN . "`.`salesmenKundennummer` AS `salesmenKundennummer2`,
						`" . TABLE_SALESMEN . "`.`salesmenFirmenname` AS `salesmenFirmenname2`,
						`" . TABLE_SALESMEN . "`.`salesmenSubSalesmanID`,
						`" . TABLE_SALESMEN . "`.`salesmenAreas`,
						`" . TABLE_SALESMEN . "`.`salesmenAreasActive`,
						`" . TABLE_SALESMEN . "`.`salesmenAreaProtection`,
						`" . TABLE_SALESMEN . "`.`salesmenGetProvision`,
						`" . TABLE_SALESMEN . "`.`salesmenProvision`,
						`" . TABLE_SALESMEN . "`.`salesmenTypesID`,
						`" . TABLE_SALESMEN . "`.`salesmenTypesName`,

						`" . TABLE_SALESMEN . "`.`salesmenActive`,
						IF(`" . TABLE_SALESMEN . "`.`salesmenActive` IS NULL OR `" . TABLE_SALESMEN . "`.`salesmenActive` = '', '0', `" . TABLE_SALESMEN . "`.`salesmenActive`) AS `salesmenActive2`,

						`" . TABLE_SALESMEN . "`.`salesmenExportDatas`,
						`" . TABLE_SALESMEN . "`.`salesmenUseMwst`,
						`" . TABLE_SALESMEN . "`.`salesmenPersonnelID`,

						`" . TABLE_CUSTOMERS . "`.`customersActive`,
						`" . TABLE_CUSTOMERS . "`.`customersID`,
						`" . TABLE_CUSTOMERS . "`.`customersKundennummer` AS `salesmenKundennummer`,
						`" . TABLE_CUSTOMERS . "`.`customersMail1` AS `salesmenMail1`,
						`" . TABLE_CUSTOMERS . "`.`customersMail2` AS `salesmenMail2`,
						`" . TABLE_CUSTOMERS . "`.`customersTelefon1` AS `salesmenTelefon1`,
						`" . TABLE_CUSTOMERS . "`.`customersTelefon2` AS `salesmenTelefon2`,
						`" . TABLE_CUSTOMERS . "`.`customersFax1` AS `salesmenFax1`,
						`" . TABLE_CUSTOMERS . "`.`customersFax2` AS `salesmenFax2`,
						`" . TABLE_CUSTOMERS . "`.`customersMobil1` AS `salesmenMobile1`,
						`" . TABLE_CUSTOMERS . "`.`customersMobil2` AS `salesmenMobile2`,

						`" . TABLE_CUSTOMERS . "`.`customersFirmenname` AS `salesmenFirmenname`,
						`" . TABLE_CUSTOMERS . "`.`customersFirmenInhaberVorname` AS `salesmenFirmenInhaberVorname`,
						`" . TABLE_CUSTOMERS . "`.`customersFirmenInhaberNachname` AS `salesmenFirmenInhaberNachname`,
						`" . TABLE_CUSTOMERS . "`.`customersCompanyPLZ` AS `salesmenCompanyPLZ`,
						`" . TABLE_CUSTOMERS . "`.`customersCompanyOrt` AS `salesmenCompanyOrt`,
						`" . TABLE_CUSTOMERS . "`.`customersCompanyCountry` AS `salesmenCompanyCountry`,
						`" . TABLE_CUSTOMERS . "`.`customersTyp` AS `salesmenTyp`

						FROM `" . TABLE_CUSTOMERS . "`

						LEFT JOIN `" . TABLE_SALESMEN . "`
						ON (`" . TABLE_CUSTOMERS . "`.`customersKundennummer` = `" . TABLE_SALESMEN . "`.`salesmenKundennummer`)

						WHERE 1
							AND (
								/*
								`customersTyp` = 2 */ /* Vertreter */
								/* OR */
								`customersTyp` = 4 /* Handelsvertreter */
								OR
								`customersTyp` = 9 /* Außendienst-Mitarbeiter */
								OR
								`customersTyp` = 10 /* EX-Außendienst-Mitarbeiter */
								OR
								`customersTyp` = 3 /* Wiederverkäufer */
							)
					";
			$sql .= $where;

			$sql .= " ORDER BY

							COALESCE(`" . TABLE_SALESMEN . "`.`salesmenActive`, 0) DESC,
							`" . TABLE_CUSTOMERS . "`.`customersTyp` DESC,
							`" . TABLE_CUSTOMERS . "`.`customersFirmenname` ASC
				";
				
				
			// BOF GET COUNT ALL ROWS
				$sql_getAllRows = "
						SELECT

							COUNT(`" . TABLE_CUSTOMERS . "`.`customersID`) AS `countAllRows`

							FROM `" . TABLE_CUSTOMERS . "`

							LEFT JOIN `" . TABLE_SALESMEN . "`
							ON (`" . TABLE_CUSTOMERS . "`.`customersKundennummer` = `" . TABLE_SALESMEN . "`.`salesmenKundennummer`)

							WHERE 1
								AND (
									/*
									`customersTyp` = 2 */ /* Vertreter */
									/* OR */
									`customersTyp` = 4 /* Handelsvertreter */
									OR
									`customersTyp` = 9 /* Außendienst-Mitarbeiter */
									OR
									`customersTyp` = 10 /* EX-Außendienst-Mitarbeiter */
									OR
									`customersTyp` = 3 /* Wiederverkäufer */
								)
							" . $where . "
					";

				$rs_getAllRows = $dbConnection->db_query($sql_getAllRows);
				list(
					$countAllRows
				) = mysqli_fetch_array($rs_getAllRows);
			// EOF GET COUNT ALL ROWS				

			if($_REQUEST["page"] == "" || $_REQUEST["page"] == 0) {
				$_REQUEST["page"] = 1;
			}

			if(MAX_SALESMEN_PER_PAGE > 0) {
				$sql .= " LIMIT " . (($_REQUEST["page"] - 1) * MAX_SALESMEN_PER_PAGE) . ", " . MAX_SALESMEN_PER_PAGE." ";
			}

			$rs = $dbConnection->db_query($sql);


			// OLD BOF GET ALL ROWS
				#$sql_totalRows = "SELECT FOUND_ROWS() AS `countRows`";
				#$rs_totalRows = $dbConnection->db_query($sql_totalRows);
				#list($thisNumRows) = mysqli_fetch_array($rs_totalRows);
			// OLD EOF GET ALL ROWS
			
			$countRows = $countAllRows;
			$thisNumRows = $countAllRows;

			$pagesCount = ceil($countRows / MAX_SALESMEN_PER_PAGE);
			
			if($thisNumRows == 1) {
				while($ds = mysqli_fetch_assoc($rs)){
					$_REQUEST["editID"] = $ds["salesmenID"];
				}
			}
			else if($thisNumRows > 1) {
				$pagesCount = ceil($thisNumRows / MAX_SALESMEN_PER_PAGE);

				$arrSalesmenDatas = array();
				while($ds = mysqli_fetch_assoc($rs)) {
					foreach(array_keys($ds) as $field) {
						$arrSalesmenDatas[$ds["customersID"]][$field] = $ds[$field];
					}
				}
			}
			else if($thisNumRows < 1) {
				$warningMessage .= ' Es wurden keine Daten gefunden!' . '<br />';
			}
			
		}
	// EOF READ ALL DATAS

	// BOF READ SELECTED DATAS
		if($_REQUEST["editID"] != "") {
			if($_REQUEST["editID"] != "NEW"){
				$sql = "SELECT
						`" . TABLE_SALESMEN . "`.`salesmenID`,
						`" . TABLE_SALESMEN . "`.`salesmenKundennummer`,
						`" . TABLE_SALESMEN . "`.`salesmenFirmenname`,
						`" . TABLE_SALESMEN . "`.`salesmenSubSalesmanID`,
						`" . TABLE_SALESMEN . "`.`salesmenAreas`,
						`" . TABLE_SALESMEN . "`.`salesmenAreasActive`,
						`" . TABLE_SALESMEN . "`.`salesmenAreaProtection`,
						`" . TABLE_SALESMEN . "`.`salesmenGetProvision`,
						`" . TABLE_SALESMEN . "`.`salesmenProvision`,
						/* `" . TABLE_SALESMEN . "`.`salesmenTypesID`, */
						`" . TABLE_CUSTOMERS. "`.`customersTyp` AS `salesmenTypesID`,
						`" . TABLE_SALESMEN . "`.`salesmenTypesName`,
						`" . TABLE_SALESMEN . "`.`salesmenActive`,
						`" . TABLE_SALESMEN . "`.`salesmenMapPoints`,
						`" . TABLE_SALESMEN . "`.`salesmenMapAreaPoints`,
						`" . TABLE_SALESMEN . "`.`salesmenExportDatas`,
						`" . TABLE_SALESMEN . "`.`salesmenUseMwst`,
						`" . TABLE_SALESMEN . "`.`salesmenPersonnelID`,

						`" . TABLE_CUSTOMERS. "`.`customersID`,
						`" . TABLE_CUSTOMERS. "`.`customersActive`,

						`" . TABLE_CUSTOMERS. "`.`customersKundennummer`,
						`" . TABLE_CUSTOMERS. "`.`customersFirmenname`,
						`" . TABLE_CUSTOMERS. "`.`customersFirmennameZusatz`,
						`" . TABLE_CUSTOMERS. "`.`customersFirmenInhaberVorname`,
						`" . TABLE_CUSTOMERS. "`.`customersFirmenInhaberNachname`,

						`" . TABLE_CUSTOMERS . "`.`customersMail1`,
						`" . TABLE_CUSTOMERS . "`.`customersMail2`,

						`" . TABLE_CUSTOMERS . "`.`customersCompanyLocation`

						FROM `" . TABLE_SALESMEN . "`

						LEFT JOIN `" . TABLE_CUSTOMERS. "`
						ON(`" . TABLE_SALESMEN . "`.`salesmenKundennummer` = `" . TABLE_CUSTOMERS. "`.`customersKundennummer`)

						WHERE 1
							AND `" . TABLE_SALESMEN . "`.`salesmenID` = '".$_REQUEST["editID"]."'
					";
			}
			else {
				$sql = "SELECT
						'%' AS `salesmenID`,
						`" . TABLE_CUSTOMERS . "`.`customersKundennummer` AS `salesmenKundennummer`,
						`" . TABLE_CUSTOMERS . "`.`customersFirmenname` AS `salesmenFirmenname`,
						`" . TABLE_CUSTOMERS . "`.`customersTyp` AS `salesmenSubSalesmanID`,
						'' AS `salesmenAreas`,
						'0' AS `salesmenAreasActive`,
						'0' AS `salesmenAreaProtection`,
						'0' AS `salesmenGetProvision`,
						0 AS `salesmenProvision`,
						`" . TABLE_CUSTOMERS . "`.`customersTyp` AS `salesmenTypesID`,
						'' AS `salesmenTypesName`,
						'1' AS `salesmenActive`,
						'' AS `salesmenMapPoints`,
						'' AS `salesmenMapAreaPoints`,
						'' AS `salesmenExportDatas`,
						'0' AS `salesmenUseMwst`,
						'0' AS `salesmenPersonnelID`,

						`" . TABLE_CUSTOMERS. "`.`customersID`,
						`" . TABLE_CUSTOMERS. "`.`customersActive`,

						`" . TABLE_CUSTOMERS. "`.`customersKundennummer`,
						`" . TABLE_CUSTOMERS. "`.`customersFirmenname`,
						`" . TABLE_CUSTOMERS. "`.`customersFirmennameZusatz`,
						`" . TABLE_CUSTOMERS. "`.`customersFirmenInhaberVorname`,
						`" . TABLE_CUSTOMERS. "`.`customersFirmenInhaberNachname`,

						`" . TABLE_CUSTOMERS . "`.`customersMail1`,
						`" . TABLE_CUSTOMERS . "`.`customersMail2`,

						`" . TABLE_CUSTOMERS . "`.`customersCompanyLocation`

						FROM `" . TABLE_CUSTOMERS . "`

						WHERE 1
							AND `" . TABLE_CUSTOMERS . "`.`customersKundennummer` = '".$_REQUEST["relatedCustomerNumber"]."'
					";
			}
						// WHERE `" . TABLE_SALESMEN . "`.`salesmenID` = '".$_REQUEST["editID"]."'

						//
			$rs = $dbConnection->db_query($sql);

			$salesmenDatas = array();
			list(
				$salesmenDatas["salesmenID"],
				$salesmenDatas["salesmenKundennummer"],
				$salesmenDatas["salesmenFirmenname"],
				$salesmenDatas["salesmenSubSalesmanID"],
				$salesmenDatas["salesmenAreas"],
				$salesmenDatas["salesmenAreasActive"],
				$salesmenDatas["salesmenAreaProtection"],
				$salesmenDatas["salesmenGetProvision"],
				$salesmenDatas["salesmenProvision"],
				$salesmenDatas["salesmenTypesID"],
				$salesmenDatas["salesmenTypesName"],
				$salesmenDatas["salesmenActive"],
				$salesmenDatas["salesmenMapPoints"],
				$salesmenDatas["salesmenMapAreaPoints"],
				$salesmenDatas["salesmenExportDatas"],
				$salesmenDatas["salesmenUseMwst"],
				$salesmenDatas["salesmenPersonnelID"],

				$salesmenDatas["customersID"],
				$salesmenDatas["customersActive"],

				$salesmenDatas["customersKundennummer"],
				$salesmenDatas["customersFirmenname"],
				$salesmenDatas["customersFirmennameZusatz"],
				$salesmenDatas["customersFirmenInhaberVorname"],
				$salesmenDatas["customersFirmenInhaberNachname"],

				$salesmenDatas["customersMail1"],
				$salesmenDatas["customersMail2"],

				$salesmenDatas["customersCompanyLocation"]

			) = mysqli_fetch_array($rs);

			if($salesmenDatas["customersMail1"] != "" && $salesmenDatas["customersMail2"] == ""){
				$salesmenDatas["customersMail"] = $salesmenDatas["customersMail1"];
			}
			else if($salesmenDatas["customersMail2"] != "" && $salesmenDatas["customersMail1"] == ""){
				$salesmenDatas["customersMail"] = $salesmenDatas["customersMail2"];
			}
			else if($salesmenDatas["customersMail1"] != "" && $salesmenDatas["customersMail2"] != ""){
				$salesmenDatas["customersMail"] = $salesmenDatas["customersMail1"] . ';' . $salesmenDatas["customersMail2"];
			}
		}
	// EOF READ SELECTED DATAS
#dd('salesmenDatas');
	// BOF READ ONLINE AQUISITION DATAS FROM  SELECTED
		if($existsOnlineAquisition) {
			$sql_extern = "
					SELECT
						`usersID`,
						`usersLogin`,
						`usersPassword`,
						`usersFirstName`,
						`usersLastName`,
						`usersAuftragslistenCustomerID`,
						`usersRights`,
						`userMailDatas`,
						`usersAreas`,
						`usersActive`
						FROM `" . DB_NAME_EXTERN_ACQUISITION . "`.`" . TABLE_ACQUISATION_USERS . "`

						WHERE 1
							AND `usersAuftragslistenCustomerID` = '" . $salesmenDatas["customersID"] . "'
						LIMIT 1
				";

			$rs_extern = $dbConnection_ExternAcqisition->db_query($sql_extern);
			echo mysqli_error();
			#dd('sql_extern');
			list(
				$salesmenDatas["usersID"],
				$salesmenDatas["usersLogin"],
				$salesmenDatas["usersPassword"],
				$salesmenDatas["usersFirstName"],
				$salesmenDatas["usersLastName"],
				$salesmenDatas["usersAuftragslistenCustomerID"],
				$salesmenDatas["usersRights"],
				$salesmenDatas["userMailDatas"],
				$salesmenDatas["usersAreas"],
				$salesmenDatas["usersActive"]
			) = mysqli_fetch_array($rs_extern);
			echo mysqli_error();
		}
	// EOF READ ONLINE AQUISITION DATAS FROM  SELECTED

	// BOF GET PDF-DOCUMENTS
	// EOF GET PDF-DOCUMENTS

	// BOF GET GOOGLE MAPS POINTS
		$strGoogleMapPoints = $salesmenDatas["salesmenMapPoints"];
		$strGoogleMapPoints = '';
	// EOF GET GOOGLE MAPS POINTS

	// BOF GET GOOGLE MAPS AREAS
		$sql = "SELECT
					`lat` AS `latitude`,
					`lon` AS `longitude`,
					`country`,
					`subcountry`,
					IF(`subcountry` != '' , CONCAT(`country`, '_', `subcountry`), `country`) AS `area`

					FROM `" . TABLE_GEODB_AREAS . "`

					WHERE 1
						AND `subcountry` = ''
						AND `country` = 'DE'
		";

		$rs = $dbConnection->db_query($sql);

		$arrGoogleMapCountryAreas = array();
		while($ds = mysqli_fetch_assoc($rs)) {
			$arrGoogleMapCountryAreas[$ds["area"]][] = 'new google.maps.LatLng(' . $ds["latitude"] . ', ' . $ds["longitude"] . ')';
		}
	// EOF GET GOOGLE MAPS AREAS

	// BOF GET SALESMAN AREAS FROM ZIPCODE POINTS
		#$arrZipCodePolygons = unserialize($salesmenDatas["salesmenMapAreaPoints"]);
		$arrZipCodePolygons = array();
	// EOF GET SALESMAN AREAS FROM ZIPCODE POINTS

	// BOF GET SALESMAN DOUBLEDIGIT AREAS
		if($salesmenDatas["salesmenAreas"] != ""){
			$arrDoubleDigitZipCodes = explode(';', $salesmenDatas["salesmenAreas"]);

			$arrSql = array();
			foreach($arrDoubleDigitZipCodes as $thisDoubleDigitZipCode){
				$arrSql[] = " `zipcodeDoubleDigit` = '" . $thisDoubleDigitZipCode . "'";
			}
			$sql = "
				SELECT
					`zipcodeDoubleDigit`,
					`coordinates`
					FROM `" . TABLE_GEODB_DOUBLEDIGIT_AREAS . "`
					WHERE 1
						AND (" . implode(" OR ", $arrSql) . ")
				";
			$rs = $dbConnection->db_query($sql);
			$arrGoogleMapDoubleDigitZipCodeAreas = array();
			while($ds = mysqli_fetch_assoc($rs)) {
				$thisCoordinates = $ds["coordinates"];
				$searchPattern = "/(\[)([0-9\.\-]*),([0-9\.\-]*)(\])/ismU";
				$replace = "new google.maps.LatLng($2, $3)";
				$thisCoordinates = preg_replace($searchPattern, $replace, $thisCoordinates);
				$arrGoogleMapDoubleDigitZipCodeAreas[$ds["zipcodeDoubleDigit"]] = $thisCoordinates;
			}
			$arrGoogleMapZipCodeAreas = $arrGoogleMapDoubleDigitZipCodeAreas;
		}
	// EOF GET SALESMAN DOUBLEDIGIT AREAS
?>
<?php
	require_once('inc/headerHTML.inc.php');
	if($_REQUEST["editID"] == "NEW") {
		$thisTitle = 'Vertreter anlegen';
		$thisIcon = 'addSalesman.png';
	}
	else {
		$thisTitle = 'Vertreterdaten verwalten';
		$thisIcon = 'salesman.png';
		if($salesmenDatas["salesmenKundennummer"] != '') {
			$thisTitle .= ': <span class="headerSelectedEntry">' . $salesmenDatas["salesmenKundennummer"] . ' &bull; ' . $salesmenDatas["salesmenFirmenname"] . '</span>';
		}
	}
	$headerHTML = preg_replace("/{###TITLE###}/", strip_tags($thisTitle), $headerHTML);
	echo $headerHTML;

	require_once(FILE_MENUE_TOP);
?>

<div id="mainArea">
	<div id="mainContent">
		<?php require_once(FILE_MENUE_SIDEBAR); ?>
		<div id="contentArea">
			<div id="contentAreaElements">
				<h1><?php if(SHOW_PAGE_TITLE_ICONS) { echo '<img src="' . PATH_ICONS_MENUE_TITLES . $thisIcon . '" alt="" />'; } ?> <?php echo $thisTitle; ?></h1>

				<?php if($_REQUEST["editID"] != "NEW"){ ?>
				<div class="menueActionsArea">
					<?php if($arrGetUserRights["editSalesmen"]) { ?><a href="<?php echo PAGE_EDIT_CUSTOMER; ?>?editID=NEW" class="linkButton" title="Neuen Vertreter anlegen" >Vertreter anlegen</a><?php } ?>
					<p class="infoArea">Vertreter werden als Kunden mit dem Kundentyp &quot;Au&szlig;endienst&quot;, &quot;Vertreter&quot; bzw. &quot;Handelsvertreter&quot; angelegt. Erst dann k&ouml;nnen diese hier bearbeitet werden</p>
					<div class="clear"></div>
				</div>
				<?php }				?>
				<div class="adminInfo">Kundendatensatz-ID: <?php echo $salesmenDatas["usersAuftragslistenCustomerID"]; ?> | Datensatz-ID: <?php echo $salesmenDatas["salesmenID"]; ?></div>
				<div class="contentDisplay">
				<?php
					if($_REQUEST["editID"] == "") {
				?>
				<div id="searchFilterArea">
					<form name="formFilterSearch" method="get" action="<?php echo $_SERVER["PHP_SELF"]; ?>">
						<table border="0" cellpadding="0" cellspacing="0" class="searchFilterContent">
							<tr>
								<td>
									<img src="layout/menueIcons/menueQuicklinks/googleMaps.png" class="buttonLoadMap" alt="Gesamtkarte" title="Gesamtkarte anzeigen" />
								</td>
								<td>
									<label for="searchSalesmenNumber">Kundennummer:</label>
									<input type="text" name="searchSalesmenNumber" id="searchSalesmenNumber" class="inputField_40" value="" />
								</td>
								<td>
									<label for="searchSalesmenName">Vertretername:</label>
									<input type="text" name="searchSalesmenName" id="searchSalesmenName" class="inputField_160" value="" />
								</td>

								<td>
									<label for="searchSalesmenType">Typ:</label>
									<select name="searchSalesmenType" id="searchSalesmenType" class="inputField_160">
										<option value="">Alle</option>
										<?php
											if(!empty($arrSalesmenTypeDatas)){
												foreach($arrSalesmenTypeDatas as $thisKey => $thisValue){
													$selected = '';
													if($thisKey == $_POST["searchSalesmenType"]){
														$selected = ' selected="selected" ';
													}
													echo '<option value="' . $thisKey . '" ' . $selected . '>' . $thisValue["salesmenTypesName"] . ' (' . $thisValue["salesmenTypesShortName"] . ')</option>';
												}
											}
										?>
									</select>
								</td>

								<td>
									<label for="searchSalesmenPlzActive">PLZ-Gebietsaktivit&auml;t:</label>
									<select name="searchSalesmenPlzActive" id="searchSalesmenPlzActive" class="inputField_160">
										<option value=""></option>
										<option value="1">PLZ-Gebiete aktiv</option>
										<option value="0">PLZ-Gebiete nicht aktiv</option>
									</select>
								</td>

								<td>
									<label for="searchWord">Suchbegriff:</label>
									<input type="text" name="searchWord" id="searchWord" class="inputField_100" />
								</td>
								<td>
									<input type="hidden" name="xxxeditID" id="xxxeditID" value="" />
									<input type="submit" name="submitSearch" class="inputButton0" value="Suchen" />
								</td>
							</tr>
						</table>
					</form>
				</div>

				<?php
					}
				?>
					<?php displayMessages(); ?>

					<?php
						if($_REQUEST["editID"] == "") {
							if(!empty($arrSalesmenDatas)) {

								if($pagesCount > 1 && $_REQUEST["displayOrderStatus"] == "") {
									include(FILE_MENUE_PAGES);
								}

								echo '<table cellpadding="0" cellspacing="0" border="0" width="100%" class="displayOrders">
										<thead>
											<tr>
												<th style="width:45px;text-align:right;">#</th>
												<th>Kundennr.</th>
												<th>Firmenname</th>
												<th>Inhaber</th>
												<th>Gebiete</th>
												<!--
												<th>OKE</th>
												-->
												<th colspan="2">Provision</th>
												<th>Export?</th>
												<th>Land</th>
												<th>PLZ</th>
												<th>Ort</th>
												<th>Telefon</th>
												<th>Typ</th>
												<th>Details</th>
											</tr>
										</thead>
										<tbody>
								';

								$count = 0;

								$marker = '';

								foreach($arrSalesmenDatas as $thisKey => $thisValue) {
									if($count%2 == 0){ $rowClass = 'row0'; }
									else { $rowClass = 'row1'; }

									if($marker != $thisValue["salesmenTyp"]){
										echo '<tr class="tableRowTitle1">';
										echo '<td colspan="14">';
										echo $arrSalesmenTypeDatas[$thisValue["salesmenTyp"]]["salesmenTypesName"];
										echo '</td>';
										echo '</tr>';
										$marker = $thisValue["salesmenTyp"];
									}

									$thisStyle = '';
									if($thisValue["salesmenActive"] != '1') {
										$thisStyle = ' style="color:#FF0000; font-style:italic;text-decoration:line-through;" ';
									}

									echo '<tr ' . $thisStyle . ' class="'.$rowClass.'">';
										echo '<td style="text-align:right;"><span style="font-weight:bold;cursor:pointer;" title="Datensatz-ID: '.($thisValue["salesmenID"]).'">'.($count + 1).'.</span></td>';
										echo '<td style="text-align:center;">';
										if($arrGetUserRights["editSalesmen"] || $arrGetUserRights["displaySalesmen"]) {
											echo '<a href="' . PAGE_EDIT_CUSTOMER . '?editCustomerNumber=' . $thisValue["salesmenKundennummer"] . '" title="Vertreter bearbeiten (Datensatz '.$thisValue["salesmenKundennummer"].')" >';
										}
										echo $thisValue["salesmenKundennummer"];
										if($arrGetUserRights["editSalesmen"] || $arrGetUserRights["displaySalesmen"]) {
											echo '</a>';
										}
										echo '</td>';

										echo '<td style="white-space:nowrap;">'.htmlentities($thisValue["salesmenFirmenname"]).'</td>';
										echo '<td style="white-space:nowrap;">'.trim(preg_replace("/:/ismU", "", preg_replace("/Inh./ismU", "", $thisValue["salesmenFirmenInhaberVorname"].' '.$thisValue["salesmenFirmenInhaberNachname"]))).'</td>';
										$thisStyle = '';
										if($thisValue["salesmenAreasActive"] != '1') {
											$thisStyle = ' style="color:#FF0000; font-style:italic;text-decoration:line-through;" ';
										}
										echo '<td ' . $thisStyle . '>';
										echo preg_replace("/;/", "; ", $thisValue["salesmenAreas"]);
										echo '</td>';

										#echo '<td ' . $thisStyle . '>';
										#echo $thisValue["salesmenActive"];
										#echo '</td>';

										echo '<td style="text-align:left;">';
										if($thisValue["salesmenGetProvision"] == '1'){
											echo '<span style="font-weight:bold;color:#009900;">ja</span>';
										}
										else {
											echo '<span style="font-weight:bold;color:#990000;">nein</span>';
										}
										echo '</td>';

										echo '<td style="text-align:right;white-space:nowrap;">'.number_format($thisValue["salesmenProvision"], "2", ",", "").' %</td>';

										echo '<td>';
										if($thisValue["salesmenExportDatas"] == '1'){
											echo '<span style="font-weight:bold;color:#009900;">ja</span>';
										}
										else {
											echo '<span style="font-weight:bold;color:#990000;">nein</span>';
										}
										echo '</td>';

										echo '<td>';
										echo $arrCountryTypeDatas[$thisValue["salesmenCompanyCountry"]]["countries_iso_code_3"];
										echo '</td>';

										echo '<td>';
										echo $thisValue["salesmenCompanyPLZ"];
										echo '</td>';

										echo '<td style="white-space:nowrap;">';
										echo htmlentities($thisValue["salesmenCompanyOrt"]);
										echo '</td>';

										echo '<td>';
										if($thisValue["salesmenTelefon1"] != '') {
											echo '<span style="white-space:nowrap;">' . formatPhoneNumber($thisValue["salesmenTelefon1"]) . '</span><br />';
										}
										else if($thisValue["salesmenTelefon2"] != '') {
											echo '<span style="white-space:nowrap;">' . formatPhoneNumber($thisValue["salesmenTelefon2"]) . '</span><br />';
										}
										if($thisValue["salesmenMobile1"] != '') {
											echo '<span style="white-space:nowrap;">' . formatPhoneNumber($thisValue["salesmenMobile1"]) . '</span><br />';
										}
										else if($thisValue["salesmenMobile2"] != '') {
											echo '<span style="white-space:nowrap;">' . formatPhoneNumber($thisValue["salesmenMobile2"]) . '</span>';
										}
										echo '</td>';

										echo '<td>';
										echo $arrSalesmenTypeDatas[$thisValue["salesmenTyp"]]["salesmenTypesName"];
										echo '</td>';

										echo '<td style="white-space:nowrap;">';
											if($thisValue["salesmenNotiz"] != "") {
												echo '<span class="toolItem"><img src="layout/icons/iconRtf.gif" class="buttonNotice" width="16" height="16" alt="Bemerkung: '.($thisValue["salesmenNotiz"]).'" /></span>';
											}
											else {
												echo '<span class="toolItem"><img src="layout/spacer.gif" width="16" height="16" alt="" /></span>';
											}
											if($arrGetUserRights["editSalesmen"] || $arrGetUserRights["displaySalesmen"]) {
												$thisSalesmanID = $thisValue["salesmenID"];
												if($thisValue["salesmenID"] == '' || $thisValue["salesmenID"] == null){
													$thisSalesmanID = 'NEW' . '&amp;relatedCustomerNumber=' . $thisValue["salesmenKundennummer"];
												}
												echo '<span class="toolItem"><a href="' . PAGE_EDIT_SALESMEN . '?editID=' . $thisSalesmanID . '"><img src="layout/icons/iconGoogleMap.png" width="16" height="16" title="Gebietskarte anzeigen und Vertretergebiet bearbeiten (Datensatz '.$thisValue["salesmenKundennummer"].')" alt="Gebietskarte" /></a></span>';
											}
											else {
												echo '<span class="toolItem"><img src="layout/spacer.gif" width="16" height="16" alt="" /></span>';
											}
											if($arrGetUserRights["editSalesmen"] || $arrGetUserRights["displaySalesmen"]) {
												echo '<span class="toolItem"><a href="' . PAGE_EDIT_CUSTOMER . '?editCustomerNumber=' . $thisValue["salesmenKundennummer"] . '"><img src="layout/icons/iconEdit.gif" width="16" height="16" title="Vertreterdaten bearbeiten (Datensatz '.$thisValue["salesmenKundennummer"].')" alt="Vertreterdaten" /></a></span>';
											}
											else {
												echo '<span class="toolItem"><img src="layout/spacer.gif" width="16" height="16" alt="" /></span>';
											}
											if($thisValue["salesmenMail1"] != "") {
												echo '<span class="toolItem"><a href="mailto:'.$thisValue["salesmenMail1"].'"><img src="layout/icons/iconMail.gif" width="16" height="14" title="Kunde &quot;'.htmlentities($thisValue["salesmenFirmenname"]). '&quot; (KNR: '.$thisValue["salesmenKundennummer"].') per Mail kontaktieren" alt="Mailkontakt" /></a></span>';
											}
											else {
												echo '<span class="toolItem"><img src="layout/spacer.gif" width="16" height="16" alt="" /></span>';
											}
										echo '</td>';
									echo '</tr>';
									$count++;
								}
								echo '	</tbody>
									</table>
								';

								if($pagesCount > 1 && $_REQUEST["displayOrderStatus"] == "") {
									include(FILE_MENUE_PAGES);
								}
							}
							else {

							}
						}
					?>

					<?php
						if($_REQUEST["editID"] != "")
						{
					?>
						<div id="tabs">
							<?php if($_REQUEST["editID"] != "NEW"){ ?>
							<ul>
								<li><a href="#tabs-1">Vertreter-Daten</a></li>
								<li><a href="#tabs-2">Provisions-Dateien</a></li>
							</ul>
							<?php } ?>
							<div id="tabs-1">
							<div class="adminEditArea">
							<form name="formEditSalesmenDatas" id="formEditSalesmenDatas" method="post" action="<?php echo $_SERVER["PHP_SELF"]; ?>" enctype="multipart/form-data" >
								<input type="hidden" name="editSalesmenID" value="<?php echo $salesmenDatas["salesmenID"]; ?>" />
								<input type="hidden" name="editOnlineSalesmenUserID" value="<?php echo $salesmenDatas["usersID"]; ?>" />
								<input type="hidden" name="editOnlineSalesmenAuftragslistenCustomerID" value="<?php echo $salesmenDatas["customersID"]; ?>" />
								<input type="hidden" name="storedSalesmenAreas" value="<?php echo $salesmenDatas["salesmenAreas"]; ?>" />
								<br /><p class="warningArea">Pflichtfelder  m&uuml;ssen ausgef&uuml;llt werden!</p>
								<fieldset>
									<legend>Vertreterdaten</legend>
									<table border="0" width="100%" cellspacing="0" cellpadding="0">
										<tr>
											<td style="width:200px;"><b>Kundennummer:</b></td>
											<td>
												<?php
													/*
													if($_REQUEST["editID"] == "NEW") {
														echo '<a href="javascript:void(\'0\');" onclick="createSalesmenNumber(\'formEditSalesmenDatas\', \'editSalesmenKundennummer\', \'editSalesmenKundennummer\'); "class="linkButton" id="generateSalesmenNumberButton">Kundennummer generieren</a>';
														$readonly = '';
													}
													else {
														$readonly = ' readonly="readonly" ';
													}
													*/
													if($_REQUEST["editID"] == "NEW") {
														$readonly = '';
														$description = '';
													}
													else if($salesmenDatas["salesmenKundennummer"] != ''){
														// $readonly = ' readonly="readonly" ';
														$readonly = '';
														$description = '';
													}
													echo '<input type="hidden" name="editSalesmenKundennummer" id="editSalesmenKundennummer" class="inputField_100" value="' . $salesmenDatas["salesmenKundennummer"] . '" />';
													echo '<span title="Vertreterdaten ansehen"><a href="' . PAGE_EDIT_CUSTOMER . '?editCustomerNumber=' . $salesmenDatas["salesmenKundennummer"] . '">' . $salesmenDatas["salesmenKundennummer"] . '</a></span>';
												?>
												<?php if($description != '') { echo '<br /><p class="infoArea">' . $description . '</p>'; } ?>
											</td>
										</tr>
										<tr>
											<td><b>Firmenname:</b></td>
											<td>
												<?php
													#echo ($salesmenDatas["customersFirmenname"]) . '<br />';
													#echo ($salesmenDatas["customersFirmennameZusatz"]) . '<br />';
													#echo ($salesmenDatas["customersFirmenInhaberVorname"]) . '<br />';
													#echo ($salesmenDatas["customersFirmenInhaberNachname"]) . '<br />';
												?>
												<input type="hidden" name="editSalesmenFirmenname"id="editSalesmenFirmenname" value="<?php echo ($salesmenDatas["salesmenFirmenname"]); ?>" />
												<?php echo ($salesmenDatas["salesmenFirmenname"]); ?>
												<br />
												<input type="checkbox" name="editSalesmenActive" value="1" <?php if($salesmenDatas["salesmenActive"] == '1'){ echo ' checked="checked" '; }?> > <b>Vertreter aktivieren?</b>
											</td>
										</tr>
										<tr>
											<td><b>Vertreter-Typ:</b></td>
											<td>
											<?php
												#dd('arrSalesmenTypeDatas');
												#dd('salesmenDatas');
											?>
												<input type="hidden" name="editSalesmenTypesID"id="editSalesmenTypesID" value="<?php echo $salesmenDatas["salesmenTypesID"]; ?>" />
												<?php echo ($arrSalesmenTypeDatas[$salesmenDatas["salesmenTypesID"]]["salesmenTypesName"]); ?>
												<!--
												<select name="editSalesmenTypesID" id="editSalesmenTyp" class="inputSelect_510" disabled="disabled">
													<option value=""> - </option>
													<?php
														if(!empty($arrSalesmenTypeDatas)) {
															foreach($arrSalesmenTypeDatas as $thisKey => $thisValue) {
																$selected = '';
																if($thisKey == $salesmenDatas["salesmenTypesID"]) {
																	$selected = ' selected="selected" ';
																}
																echo '
																	<option value="' . $thisKey . '" '.$selected.' >' . ($arrSalesmenTypeDatas[$thisKey]["salesmenTypesName"]). '</option>
																';
															}
														}
													?>
												</select>
												-->
											</td>
										</tr>
										<tr>
											<td><b>Mitarbeiter:</b></td>
											<td>
												<select name="editSalesmenPersonnelID"class="inputSelect_510" />
													<option value="0">kein Mitarbeiter</option>
													<?php
														if(!empty($arrPersonnelDatas)){
															foreach($arrPersonnelDatas as $thisPersonnelDataKey => $thisPersonnelDataValue){
																if($thisPersonnelDataValue["personnelActive"] == '1' && in_array($thisPersonnelDataValue["personnelGroup"], array(6, 7))){
																	$selected = '';
																	if($thisPersonnelDataKey == $salesmenDatas["salesmenPersonnelID"]){
																		$selected = ' selected="selected" ';
																	}
																	echo '<option value="' . $thisPersonnelDataKey . '" ' . $selected . '>' . $thisPersonnelDataValue["personnelFirstName"] . ' ' . $thisPersonnelDataValue["personnelLastName"] . '</option>';
																}
															}
														}
													?>
												</select>
											</td>
										</tr>
										<tr>
											<td><b>Unter-Vertreter:</b></td>
											<td>
												<?php
													if(!empty($arrAgentDatas2)){
														echo '<select name="editSalesmenSubSalesmanID" class="inputSelect_510">';
														echo '<option value="0" > - </option>';
														foreach($arrAgentDatas2 as $thisKey => $thisValue){
															$selected = '';
															if($thisValue["customersID"] == $salesmenDatas["salesmenSubSalesmanID"] && $salesmenDatas["salesmenSubSalesmanID"] > 0) {
																$selected = ' selected="selected" ';
															}
															echo '<option value="' . $thisValue["customersID"] . '" '.$selected.' >' . $thisValue["customersFirmenname"] . ' | ' . $thisValue["customersKundennummer"] . ' [' . $arrSalesmenTypeDatas[$thisValue["customersTyp"]]["salesmenTypesName"] . ']</option>';
														}
														echo '</select>';
													}
												?>
											</td>
										</tr>
										<tr>
											<td><b>Vertreter-Provision:</b></td>
											<td>
												<input type="text" name="editSalesmenProvision" id="editSalesmenProvision" class="inputField_100" value="<?php echo number_format($salesmenDatas["salesmenProvision"], "2", ",", ""); ?>" /> %
												<br />
												<?php
													$checked = '';
													if($salesmenDatas["salesmenGetProvision"] == '1'){
														$checked = ' checked="checked" ';
													}
												?>
												<input type="checkbox" name="editSalesmenGetProvision" id="editSalesmenGetProvision" <?php echo $checked; ?> value="1" /> <b>Vertreter erh&auml;lt Provision?</b>
												<br />
												<?php
													$checked = '';
													if($salesmenDatas["salesmenUseMwst"] == '1'){
														$checked = ' checked="checked" ';
													}
												?>
												<input type="checkbox" name="editSalesmenUseMwst" id="editSalesmenUseMwst" <?php echo $checked; ?> value="1" /> <b>MwSt. anzeigen?</b>

											</td>
										</tr>

										<tr>
											<td><b>Vertreter-Export:</b></td>
											<td>
												<?php
													$checked = '';
													if($salesmenDatas["salesmenExportDatas"] == '1'){
														$checked = ' checked="checked" ';
													}
												?>
												<input type="checkbox" name="editSalesmenExportDatas" id="editSalesmenExportDatas" <?php echo $checked; ?> value="1" /> <b>Vertreter erh&auml;lt Export-Datei mit offenen Auftr&auml;gen?</b>
											</td>
										</tr>

										<tr>
											<td><b>PLZ-Gebiete:</b></td>
											<td>
												<?php
													$checked = '';
													if($salesmenDatas["salesmenAreaProtection"] == '1'){
														$checked = ' checked="checked" ';
													}
												?>
												<input type="checkbox" name="editSalesmenAreaProtection" id="editSalesmenAreaProtection" <?php echo $checked; ?> value="1" /> <b>Vertreter hat Gebietsschutz?</b>
												<br />
												<?php
													$checked = '';
													if($salesmenDatas["salesmenAreasActive"] == '1'){
														$checked = ' checked="checked" ';
													}
												?>
												<input type="checkbox" name="editSalesmenAreasActive" id="editSalesmenAreasActive" <?php echo $checked; ?> value="1" /> <b>PLZ-Gebiete aktiv?</b>


												<p class="infoArea"><b>Pro Zeile</b> eine PLZ (z.B. 49) oder einen PLZ-Bereich (z.B. 36-39) eingeben</p>
												<textarea name="editSalesmenAreas"id="editSalesmenAreas" class="inputTextarea_510x140"><?php echo preg_replace("/;/", "\n", $salesmenDatas["salesmenAreas"]); ?></textarea/>
											</td>
										</tr>
										<tr>
											<td><b>Location:</b></td>
											<td>
												<input type="text" name="editSalesmenLocation" id="editSalesmenLocation" class="inputField_510" value="<?php echo $salesmenDatas["customersCompanyLocation"]; ?>" readonly="readonly" />
											</td>
										</tr>
										<tr>
											<td><b>Karte:</b></td>
											<td><p>PLZ-Fl&auml;chen werden nur n&auml;herungsweise dargestellt.</p>
												<!-- GOOGLE MAPS START -->
												<?php if($_REQUEST["editID"] != "NEW"){ ?>
												<script type="text/javascript" src="<?php echo PATH_GOOGLE_MAPS_API; ?>"></script>
												<script type="text/javascript" language="javascript">

																									// (c) Thorsten Hoff
												// BOF DEFINE DEFAULT VALUES
													var getDatasFromDB = true;
													// var filePath		= "inc/readGoogleMapsMarkers.php";
													var myPointDatas	= new Array();

													var elementId = 'myGmapCanvas';
													var default_arrayKey	= 0;
													var default_lat			= '52.103138';
													var default_lon			= '7.622817';
													var default_zoom		= 5;

													// var sidebarId		= 'myGmapSidebarContent';  	// id des Sitemap-div's im html-code
													var sidebarHtml		= '';              			// HTML-Code für die gesamte Sidebar
													var sidebarMarkers	= [];              			// Array für die Marker
													var sidebarCount	= 0;               			// ZÃ¤hler für die Marker (= Zeile in der Sidebar)
													var marker			= new Array();
													var activeMarker	= -1;
													var infowindow		= new Array();
												// EOF DEFINE DEFAULT VALUES

												// DEFINE POINT DATAS
													myPointDatas[0] = new Array();
													myPointDatas[0]["latitude"] = default_lat;
													myPointDatas[0]["longitude"] = default_lon;
													myPointDatas[0]["title"] = "BURHAN CTR";
													myPointDatas[0]["header"] = "BURHAN CTR";
													myPointDatas[0]["text"] = "";
													myPointDatas[0]["city"] = "";
													myPointDatas[0]["region_1"] = "";
													myPointDatas[0]["region_2"] = "";
													myPointDatas[0]["region_3"] = "";
													myPointDatas[0]["imagePath"] = "";
													<?php
														if($strGoogleMapPoints != '') {
															echo $strGoogleMapPoints;
														}
													?>

												// END DEFINE POINT DATAS

												// BOF CREATE GOOGLE MAP
													function initialize() {
														if (!document.getElementById(elementId)) {
															alert('Fehler: das Element mit der id '+ elementId+ ' konnte nicht auf dieser Webseite gefunden werden!');
															return false;
														}
														else {

														}

														<?php if(1) { ?>
														var latlng = new google.maps.LatLng(myPointDatas[default_arrayKey]["latitude"], myPointDatas[default_arrayKey]["longitude"]);
														<?php } ?>
														var myOptions = {
															zoom: default_zoom,
															center: latlng,
															panControl: true,
															zoomControl: true,
															navigationControl: true,
															mapTypeControl: true,
															scaleControl: true,
															overviewMapControl: true,
															streetViewControl: true,
															mapTypeId: google.maps.MapTypeId.TERRAIN
															// ROADMAP zeigt die normalen Standard-2D-Kacheln von Google Maps an.
															// SATELLITE zeigt Fotokacheln an.
															// HYBRID zeigt eine Mischung aus Fotokacheln und eine Kachelebene für markante Merkmale an (Straßen, Ortsnamen).
															// TERRAIN zeigt Reliefkacheln für Erhebungen und GewÃ¤sser (Berge, Flüsse usw.) an.
														};
														var map = new google.maps.Map(document.getElementById(elementId), myOptions);


														// BOF COUNTRY BORDERS
														<?php if(SHOW_COUNTRY_AREAS){ ?>
														var arrMyCountryAreaPoints = new Array();
														<?php
														if(!empty($arrGoogleMapCountryAreas)){
															$countItem = 0;
															foreach($arrGoogleMapCountryAreas as $thisKey => $thisValue) {
														?>
														arrMyCountryAreaPoints[<?php echo $countItem; ?>] = [<?php echo implode(", ", $thisValue); ?>];
														<?php
																$countItem++;
															}
														}
														?>
														function showCountryBorders(map){
															if(arrMyCountryAreaPoints.length > 0){
																var arrMyCountryAreaPolygon = new Array();
																for(i = 0 ; i < arrMyCountryAreaPoints.length ; i++){
																	arrMyCountryAreaPolygon[i] = new google.maps.Polygon({
																			paths: arrMyCountryAreaPoints[i],
																			strokeColor: "#00FF3A",
																			strokeOpacity: 0.8,
																			strokeWeight: 2,
																			fillColor: "transparent",
																			fillOpacity: 0.0
																			});
																	arrMyCountryAreaPolygon[i].setMap(map);
																}
															}
														}
														<?php } ?>
														// EOF COUNTRY BORDERS

														// BOF ZIPCODE AREAS
														<?php if(SHOW_ZIPCODE_AREAS){ ?>
														var arrMyZipcodeAreaPoints = new Array();
														<?php
														if(!empty($arrGoogleMapZipCodeAreas)){
															$countItem = 0;
															foreach($arrGoogleMapZipCodeAreas as $thisKey => $thisValue) {
														?>
														arrMyZipcodeAreaPoints[<?php echo $countItem; ?>] = [<?php echo $thisValue; ?>];
														<?php
																$countItem++;
															}
														}
														?>
														function showZipCodeAreas(map){
															if(arrMyZipcodeAreaPoints.length > 0){
																var arrMyZipcodeAreaPolygon = new Array();
																for(i = 0 ; i < arrMyZipcodeAreaPoints.length ; i++){
																	arrMyZipcodeAreaPolygon[i] = new google.maps.Polygon({
																			paths: arrMyZipcodeAreaPoints[i],
																			strokeColor: "#0000FF",
																			strokeOpacity: 0.3,
																			strokeWeight: 1,
																			fillColor: "#0000FF",
																			fillOpacity: 0.1,
																			zIndex:1
																			});
																	arrMyZipcodeAreaPolygon[i].setMap(map);
																}
															}
														}
														<?php } ?>
														// EOF ZIPCODE AREAS

														try{
															for (i = 0 ; i < myPointDatas.length ; i++) {
																if(myPointDatas[i]){
																	setGmapMarkers(map, i, "myPointDatas");
																}
															}
															<?php if(SHOW_COUNTRY_AREAS){ ?>
															showCountryBorders(map);
															<?php } ?>

															<?php if(SHOW_ZIPCODE_AREAS){ ?>
															showZipCodeAreas(map);
															<?php } ?>
														}
														catch(e){
															alert("error: " + e + " | i: " + i);
														}
														finally {}

														// setGmapMarkers(map, i);

														function setGmapMarkers(map, i) {
															var thisMarkerImage;
															if(i == 0) {
																thisMarkerImage = 'bctr_googleMarkerFlag.png';
																thisMarkerShadowImage = 'bctr_googleMarkerFlagShadow.png';
																thisMarkerSize = new Array(81, 84);
																thisMarkerShadowSize = new Array(81, 84);
																thisZindex = (myPointDatas.length + 10);
															}
															else {
																thisMarkerImage = 'googleMapsIcon.png';
																thisMarkerShadowImage = 'googleMapsIconShadow.png';
																thisMarkerSize = new Array(28, 20);
																thisMarkerShadowSize = new Array(28, 20);
																thisZindex = (myPointDatas.length - i);
															}

															var image = new google.maps.MarkerImage(
																		'layout/icons/' + thisMarkerImage,
																		new google.maps.Size(thisMarkerSize[0], thisMarkerSize[1]),
																		new google.maps.Point(0,0),
																		new google.maps.Point(0, thisMarkerSize[1])
															);

															var shadow = new google.maps.MarkerImage(
																			'layout/icons/' + thisMarkerShadowImage,
																			new google.maps.Size(thisMarkerShadowSize[0], thisMarkerShadowSize[1]),
																			new google.maps.Point(0, 0),
																			new google.maps.Point(0, thisMarkerShadowSize[1])
															);

															var shape = {
																// coord: [1, (thisMarkerSize[1] + 1 - 20), 1, thisMarkerSize[1], thisMarkerSize[0], thisMarkerSize[1], thisMarkerSize[0] , (thisMarkerSize[1] + 1 - 20)],
																coord: [1, 1, 1, 28, 20, 28, 20 , 1],
																type: 'poly'
															};

															var myLatLng = new google.maps.LatLng(myPointDatas[i]["latitude"], myPointDatas[i]["longitude"]);
															marker[i] = new google.maps.Marker({
																position: myLatLng,
																map: map,
																shadow: shadow,
																icon: image,
																shape: shape,
																title: myPointDatas[i]["title"],
																zIndex: thisZindex
															});

															shadow[i] = new google.maps.Marker({
																position: myLatLng,
																map: map,
																icon: shadow,
																shape: shape,
																title: myPointDatas[i]["title"],
																zIndex: thisZindex
															});

															// DEFINE INFO WINDOW
																var html		= new Array();
																var imageExists	= false;
																html[i] = "";
																html[i] += "<div class='myGmapInfo'>";
																// html[i] += "  <p class='headline'>" + myPointDatas[i]["title"] + "</p>";
																html[i] += "  <p class='headline'>" + myPointDatas[i]["header"] + "</p>";

																if(myPointDatas[i]["imagePath"] != "") {
																  imageExists = true;
																  html[i] += '  <img src="' + myPointDatas[i]["imagePath"] + '" + myPointDatas[i]["imageDimension"] + alt="' + myPointDatas[i]["title"] + '"/>';
																}

																html[i] += '<table border="0" cellpadding="0" cellspacing="0">';

																if(myPointDatas[i]["text"] != "") {
																  // html[i] += "  <p class='text'>" + myPointDatas[i]["text"].replace(/, /g, ",<br />") + "</p>";
																  html[i] += "  <tr><td><b>Vertreter:</b></td><td>" + myPointDatas[i]["text"] + "</td></tr>";
																}

																if(myPointDatas[i]["city"] != "") {
																  // html[i] += "  <tr><td><b>PLZ:</b></td><td>" + myPointDatas[i]["city"] + "</td></tr>";
																}

																if(myPointDatas[i]["region_1"] != "") {
																  html[i] += "  <tr><td><b>Bundesland:</b></td><td>" + myPointDatas[i]["region_1"] + "</td></tr>";
																}

																if(myPointDatas[i]["region_2"] != "") {
																  html[i] += "  <tr><td><b>Region:</b></td><td>" + myPointDatas[i]["region_2"] + "</td></tr>";
																}

																if(myPointDatas[i]["region_3"] != "") {
																  html[i] += "  <tr><td><b>Kreis:</b></td><td>" + myPointDatas[i]["region_3"] + "</td></tr>";
																}

																html[i] += "  <tr><td><b>Longitude:</b></td><td>" + myPointDatas[i]["longitude"] + "</td></tr>";
																html[i] += "  <tr><td><b>Latitude: </b></td><td>" + myPointDatas[i]["latitude"] + "</td></tr>";

																html[i] += '</table>';

																html[i] += "<div class='clear'></div>";
																html[i] += "</div>";

																infowindow[i] = new google.maps.InfoWindow({
																	content: html[i]
																});
															// END DEFINE INFO WINDOW

															// SHOW INFO WINDOW
																google.maps.event.addListener(marker[i], 'click', function() {
																	if(activeMarker > -1) {
																		infowindow[activeMarker].close();
																	}
																	infowindow[i].open(map,marker[i]);
																	activeMarker = i;
																});
																// createMarkerLinkInGmapSidebar(i, myPointDatas[i]["title"], marker[i], imageExists);
															// END SHOW INFO WINDOW
														}
													}
												</script>
												<!--
												<iframe width="570" height="320" frameborder="0" scrolling="no" marginheight="0" marginwidth="0" src="http://maps.google.de/maps?f=q&amp;hl=de&amp;geocode=&amp;q=48149+M%C3%BCnster,+Horstmarer+Landweg+82&amp;sll=51.973223,7.59696&amp;sspn=0.011659,0.029182&amp;ie=UTF8&amp;s=AARTsJq20aeZmBjHCd58PNXT-CM-I7ReoA&amp;ll=51.976792,7.604942&amp;spn=0.016918,0.048923&amp;z=14&amp;iwloc=addr&amp;output=embed"></iframe>
												<p> <a href="http://maps.google.de/maps?f=q&amp;hl=de&amp;geocode=&amp;q=48149+M%C3%BCnster,+Horstmarer+Landweg+82&amp;sll=51.973223,7.59696&amp;sspn=0.011659,0.029182&amp;ie=UTF8&amp;ll=51.976792,7.604942&amp;spn=0.016918,0.048923&amp;z=14&amp;iwloc=addr&amp;source=embed">Gr&ouml;&szlig;ere Kartenansicht</a></p>
												-->
												<div id="myGoogleMapsArea">
													<div id="myGmapCanvas"> </div>
												</div>
												<?php
													}
													else {
														echo 'Die Karte wird erst angezeigt, wenn PLZ-Gebiete eingetragen sind.';
													}
												?>
												<!-- GOOGLE MAPS END -->
											</td>
										</tr>
									</table>
								</fieldset>

								<div class="actionButtonsArea">
									<?php if($arrGetUserRights["editSalesmen"]) { ?>
										<input type="submit" class="inputButton1 inputButtonGreen" name="storeDatas" value="Speichern" onclick="return checkFormDutyFields(this.form.name, <?php echo $jsonFormDutyFields; ?>);" />
										<?php if($_REQUEST["editID"] != "NEW"){ ?>
											&nbsp;
											<input type="submit" class="inputButton1 inputButtonRed" name="deleteDatas" value="Entfernen" onclick="return showWarning(' Wollen Sie diesen Eintrag endgültig entfernen??? ');" />
											&nbsp;
										<?php } ?>
										<input type="submit" class="inputButton1 inputButtonOrange" name="cancelDatas" value="Abbrechen" onclick="return showWarning(' Wollen Sie diesen Vorgang wirklich abbrechen??? ');" />
									<?php } ?>
								</div>
							</form>
						</div>
						</div>
						<?php if($_REQUEST["editID"] != "NEW"){ ?>
						<div id="tabs-2">
							<fieldset>
								<legend>Provisions-Dateien</legend>
								<?php
								$sql = "SELECT
											`" . TABLE_SALESMEN_PROVISIONS . "`.`salesmenProvisionsID`,
											`" . TABLE_SALESMEN_PROVISIONS . "`.`salesmenProvisionsDocumentNumber`,
											`" . TABLE_SALESMEN_PROVISIONS . "`.`salesmenProvisionsSalesmanID`,
											`" . TABLE_SALESMEN_PROVISIONS . "`.`salesmenProvisionsDocumentDate`,
											`" . TABLE_SALESMEN_PROVISIONS . "`.`salesmenProvisionsSum`,
											`" . TABLE_SALESMEN_PROVISIONS . "`.`salesmenProvisionsMwst`,
											`" . TABLE_SALESMEN_PROVISIONS . "`.`salesmenProvisionsMwstValue`,
											`" . TABLE_SALESMEN_PROVISIONS . "`.`salesmenProvisionsTotal`,
											`" . TABLE_SALESMEN_PROVISIONS . "`.`salesmenProvisionsStatus`,
											`" . TABLE_SALESMEN_PROVISIONS . "`.`salesmenProvisionsDocumentPath`,
											`" . TABLE_SALESMEN_PROVISIONS . "`.`salesmenProvisionsContentDocumentNumber`,

											`" . TABLE_CREATED_DOCUMENTS . "`.`createdDocumentsID`,
											`" . TABLE_CREATED_DOCUMENTS . "`.`createdDocumentsType`,
											`" . TABLE_CREATED_DOCUMENTS . "`.`createdDocumentsNumber`,
											`" . TABLE_CREATED_DOCUMENTS . "`.`createdDocumentsCustomerNumber`,
											`" . TABLE_CREATED_DOCUMENTS . "`.`createdDocumentsTitle`,
											`" . TABLE_CREATED_DOCUMENTS . "`.`createdDocumentsFilename`,
											`" . TABLE_CREATED_DOCUMENTS . "`.`createdDocumentsContent`,
											`" . TABLE_CREATED_DOCUMENTS . "`.`createdDocumentsUserID`,
											`" . TABLE_CREATED_DOCUMENTS . "`.`createdDocumentsTimeCreated`

											FROM `" . TABLE_SALESMEN_PROVISIONS . "`

											LEFT JOIN `" . TABLE_CREATED_DOCUMENTS . "`
											ON(`" . TABLE_SALESMEN_PROVISIONS . "`.`salesmenProvisionsDocumentNumber` = `" . TABLE_CREATED_DOCUMENTS . "`.`createdDocumentsNumber`)

											WHERE 1
												AND `" . TABLE_SALESMEN_PROVISIONS . "`.`salesmenProvisionsSalesmanID` = " . $salesmenDatas["customersID"] . "

											/* GROUP BY `" . TABLE_SALESMEN_PROVISIONS . "`.`salesmenProvisionsDocumentNumber` */

											HAVING `" . TABLE_CREATED_DOCUMENTS . "`.`createdDocumentsID` IS NOT NULL

											ORDER BY `" . TABLE_SALESMEN_PROVISIONS . "`.`salesmenProvisionsDocumentNumber` DESC, `" . TABLE_CREATED_DOCUMENTS . "`.`createdDocumentsTimeCreated` DESC
									";

								#dd('sql');
								$rs = mysqli_query($sql, $db_open);

								$count = 0;
								#dd('salesmenDatas');
								echo '<table border="1" cellpadding="0" cellspacing="0" width="0" class="displayOrders">';
										echo '<tr>';
										echo '<th style="width:45px;">#</th>';
										echo '<th>Dokument-Nr</th>';
										echo '<th>Datum</th>';
										echo '<th>Summe</th>';
										echo '<th>MwSt.</th>';
										echo '<th>Gesamtbetrag</th>';
										echo '<th style="width:70px;">Info</th>';
										echo '</tr>';

								$thisMarker = '';
								while($ds = mysqli_fetch_assoc($rs)) {
									if($count%2 == 0){ $rowClass = 'row0'; }
									else { $rowClass = 'row1'; }
									#dd('ds');
									if($thisMarker != $ds["salesmenProvisionsDocumentNumber"]) {
										$thisMarker = $ds["salesmenProvisionsDocumentNumber"];
										echo '<tr class="'.$rowClass.'">';
										echo '<td style="text-align:right;"><b>' . ($count + 1) .'.</b></td>';
										echo '<td>';
										echo $ds["salesmenProvisionsDocumentNumber"];
										echo '</td>';
										echo '<td>';
										echo substr(formatDate($ds["createdDocumentsTimeCreated"], 'display'), 0, 10);
										echo '</td>';
										echo '<td style="text-align:right;">';
										echo number_format($ds["salesmenProvisionsSum"], 2, ',', '') . '&euro;';
										echo '</td>';
										echo '<td style="text-align:right;">';
										echo number_format($ds["salesmenProvisionsMwstValue"], 2, ',', '') . '&euro;' . ' (' . number_format($ds["salesmenProvisionsMwst"], 2, ',', '') . '%)';
										echo '</td>';
										echo '<td style="text-align:right;">';
										echo number_format($ds["salesmenProvisionsTotal"], 2, ',', '') . '&euro;';
										echo '</td>';
										echo '<td>';
										echo '<span class="toolItem">';

										$arrPathInfo = pathinfo($ds["salesmenProvisionsDocumentPath"]);
										if($arrPathInfo["extension"] != ''){
											echo '<a href="' . $_SERVER["PHP_SELF"] . '?editCustomerNumber=' . $salesmenDatas["salesmenKundennummer"] . '&editID=' . $salesmenDatas["customersID"] . '&downloadFile=' . urlencode(basename($ds["salesmenProvisionsDocumentPath"])) . '&documentType=PR">' . '<img src="layout/icons/icon' . getFileType(basename($ds["salesmenProvisionsDocumentPath"])) . '.gif" width="16" height="16" title="Dokument herunterladen" alt="Download" /></a>';
										}

										echo '</span>';

										echo '<span class="toolItem">';
										echo '<img src="layout/icons/mailAttachement.png" class="buttonSendDocument" rel="' . urlencode(basename($ds["salesmenProvisionsDocumentPath"])) . '#' . 'PR' . '" width="16" height="16" title="Dokument direkt per Mail versenden" alt="Dokument versenden" />';
										echo '</span>';

										echo '</td>';
										echo '</tr>';
										$count++;
									}
								}
								echo '</table>';
							?>
							</fieldset>
						</div>
						<?php } ?>
						</div>
					<?php
						}
					?>
				</div>
			</div>
		</div>
		<div class="clear"></div>
	</div>
</div>

<script language="javascript" type="text/javascript">
	$(document).ready(function() {
		<?php if($_REQUEST["editID"] != '' && $_REQUEST["editID"] != 'NEW'){ ?>
		$('body').attr('onload', 'initialize();');
		<?php } ?>

		$('.buttonLoadMap').click(function() {
			loadGoogleMapSalesmen('4');
		});
		<?php if($_REQUEST["openMap"] == true){ ?>
			loadGoogleMapSalesmen('4');
		<?php } ?>

		<?php if($_REQUEST["editID"] != "NEW"){ ?>
		$(function() {
			$('#tabs').tabs();
		});
		<?php } ?>
		colorRowMouseOver('.displayOrders tbody tr');
		$('.buttonNotice').click(function () {
			loadNotice($(this));
		});
		setFormDutyFields(<?php echo $jsonFormDutyFields; ?>);

		$('.buttonSendDocument').click(function() {
			var mailDocumentDatas = $(this).attr('rel');
			var mailAdress = '<?php if($salesmenDatas["customersMail"] != "") { echo $salesmenDatas["customersMail"]; } else { echo ''; }?>';
			sendAttachedDocument($(this), mailDocumentDatas, '<?php echo $salesmenDatas["customersKundennummer"]; ?>', '<?php echo $_REQUEST["editID"]; ?>', '', '<?php echo $_SERVER["PHP_SELF"]; ?>', mailAdress);
		});
	});
</script>

<?php require_once('inc/footerHTML.inc.php'); ?>