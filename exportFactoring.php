<?php
	require_once('inc/requires.inc.php');

	if(!$arrGetUserRights["editFinancialDatas"]) {
		header('location: ' . PAGE_EXIT_LOCATION);
		exit;
	}

	DEFINE('SEND_FAKTORING_EXPORT_PER_MAIL', false);

	DEFINE('KREDITOREN_ID_BCTR', '2311'); // BCTR
	DEFINE('KREDITOREN_ID_B3', '2349'); // B3

	DEFINE('KREDITOREN_ID', constant("KREDITOREN_ID_" . strtoupper(MANDATOR))); // B3

	#DEFINE('FAKTORING_DATE_START_BCTR', '2015-10-01');
	DEFINE('FAKTORING_DATE_START_BCTR', '2015-01-01');
	DEFINE('FAKTORING_DATE_START_B3', '2015-01-01');

	DEFINE('FAKTORING_DATE_START', constant("FAKTORING_DATE_" . strtoupper(MANDATOR)));

	DEFINE('FAKTORING_DAYS_START', 45); // DAYS
	$arrTempDate = explode("-", FAKTORING_DATE_START);
	DEFINE('EXPORT_DATE_START', date("Y-m-d", mktime(0, 0, 0, $arrTempDate[1], ($arrTempDate[2] - FAKTORING_DAYS_START), $arrTempDate[0])));


	$exportDateStartInterval = 1; // DAYS
	$todayDayNumber = date('w');
	if($todayDayNumber == 1){
		$exportDateStartInterval = 3;
	}
	else if($todayDayNumber == 0){
		$exportDateStartInterval = 2;
	}
	$exportDateEndInterval = 1; // DAYS

	$exportDateStart = date("d.m.Y", mktime(0, 0, 0, date("m"), (date("d") - $exportDateStartInterval), date("Y")));
	$exportDateEnd = date("d.m.Y", mktime(0, 0, 0, date("m"), (date("d") - $exportDateEndInterval), date("Y")));

	DEFINE('BASEPATH_Auftragslisten', '');
	DEFINE('PATH_EXPORTED_FILES', BASEPATH_Auftragslisten . 'exportedFactoringFiles/');

	require_once(BASEPATH_Auftragslisten . 'config/configParams_' . strtolower(MANDATOR) . '.inc.php');
	require_once(BASEPATH_Auftragslisten . 'config/configBasic.inc.php');
	require_once(BASEPATH_Auftragslisten . 'config/configTables.inc.php');
	require_once(BASEPATH_Auftragslisten . 'inc/functions.inc.php');
	require_once(BASEPATH_Auftragslisten . 'classes/DB_Connection.class.php');

	if(SEND_FAKTORING_EXPORT_PER_MAIL){
		require_once(BASEPATH_Auftragslisten . 'scriptPackages/htmlMimeMail/htmlMimeMail.php');
		require_once(BASEPATH_Auftragslisten . 'classes/createMail.class.php');

		require_once(BASEPATH_Auftragslisten . 'documents_' . MANDATOR . '/documentTemplates/templatesMail/template_BASIC.inc.php');
		require_once(BASEPATH_Auftragslisten . 'documents_' . MANDATOR . '/documentTemplates/templatesMail/template_TEXT_DATAS.inc.php');
	}

	function convertExportString($string){
		$string = utf8_decode($string);
		return $string;
	}


	$jswindowMessage = '';
	$warningMessage = '';
	$errorMessage = '';
	$successMessage = '';
	$infoMessage = '';

	$dbConnection = new DB_Connection();
	$db_open = $dbConnection->db_connect();

	if($_GET["downloadFile"] != "") {
		require_once('classes/downloadFile.class.php');
		$downloadPath = '';
		if($_GET["downloadType"] == "EXPORT"){
			$downloadPath = PATH_EXPORTED_FILES;
			$thisDownloadFile = $_GET["downloadFile"];
		}
		else if(in_array($_GET["downloadType"], array("GU", "RE"))){
			$downloadPath = DIRECTORY_CREATED_DOCUMENTS_CUSTOMERS;
			$thisDownloadFile = searchDownloadFile($downloadPath . $_GET["downloadFile"], $_GET["downloadType"]);
		}
		$thisDownload = new downloadFile($downloadPath, $thisDownloadFile);
		$errorMessage .= $thisDownload->startDownload();
	}
?>
<?php
	require_once('inc/headerHTML.inc.php');
	$thisTitle = "Faktoring-Daten exportieren";

	$headerHTML = preg_replace("/{###TITLE###}/", strip_tags($thisTitle), $headerHTML);
	echo $headerHTML;

	require_once(FILE_MENUE_TOP);

	// BOF EXPORT DOCUMENTS
		if($_POST["submitExportForm"] == 1 && (($_POST["exportDateStart"] != "" && $_POST["exportDateEnd"] != ""))) {
			$thisMessage = '';

			$dateFrom = formatDate($_POST["exportDateStart"], "store");
			$dateTo = formatDate($_POST["exportDateEnd"], "store");

			$arrFilesToZip = array();

			$fileNameExportTemplate = $dateFrom . '_' . $dateTo . '_' . MANDATOR . '_EXPORT_{###DOC_TYPE}';

			$pathExportTemplate = PATH_EXPORTED_FILES . $fileNameExportTemplate;

			$pathExportZIP = preg_replace("/{###DOC_TYPE}/", "DOCS", $pathExportTemplate) . '.zip';

			$arrDocumentsToExport = array("RE", "GU");

			$csvFieldSeparator = "\t";
			$csvLineSeparator = "\r\n";

			if(!empty($arrDocumentsToExport)){
				foreach($arrDocumentsToExport as $thisDocumentsToExport){
					$arrExportFiles[$thisDocumentsToExport] = array(
						"ZIP" => preg_replace("/{###DOC_TYPE}/", $thisDocumentsToExport, $pathExportTemplate) . '.zip',
						"CSV" => preg_replace("/{###DOC_TYPE}/", $thisDocumentsToExport, $pathExportTemplate) . '.csv'
					);
				}
				$fileNameExportBasic = preg_replace("/_{###DOC_TYPE}/", "", $fileNameExportTemplate);
			}

			if(!empty($arrExportFiles)){
				foreach($arrExportFiles as $thisExportDocType => $thisExportDocTypeFiles){
					foreach($thisExportDocTypeFiles as $thisFileType => $thisFileName){
						if(file_exists($thisFileName)){
							unlink($thisFileName);
						}
					}
				}
			}

			// BOF DEFINE WHERE CLAUSES
				$whereDate = "";

				$whereDate .= "

				";

				// NUR ZAHLSTATUS OFFEN?
				$whereInvoices = "
					/*
					AND `" . TABLE_ORDER_INVOICES . "`.`orderDocumentsDocumentDate` = '" . $dateTo . "'
					*/
					AND `" . TABLE_ORDER_INVOICES . "`.`orderDocumentsDocumentDate` BETWEEN '" . $dateFrom . "' AND '" . $dateTo."'

					AND (
						`" . TABLE_ORDER_INVOICES . "`.`orderDocumentsPaymentType` = '1'
						OR
						`" . TABLE_ORDER_INVOICES . "`.`orderDocumentsPaymentType` = '4'
						OR
						`" . TABLE_ORDER_INVOICES . "`.`orderDocumentsPaymentType` = '6'
					)
					AND `" . TABLE_ORDER_INVOICES . "`.`orderDocumentsStatus` != 2
					AND `" . TABLE_ORDER_INVOICES . "`.`orderDocumentsStatus` != 10
					AND `" . TABLE_ORDER_INVOICES . "`.`orderDocumentsBankAccount` = 'FAKT'
				";

				$whereCredits = "
					/*
					AND `" . TABLE_ORDER_CREDITS . "`.`orderDocumentsDocumentDate` = '" . $dateTo . "'
					*/
					AND `" . TABLE_ORDER_CREDITS . "`.`orderDocumentsDocumentDate` BETWEEN '" . $dateFrom . "' AND '" . $dateTo."'

					AND `" . TABLE_ORDER_CREDITS . "`.`orderDocumentsBankAccount` = 'FAKT'
				";


			// EOF DEFINE WHERE CLAUSES

			// BOF STORE EXPORT LOG DATA
				$arrLogExportData = array();
				if(!empty($arrDocumentsToExport)){
					foreach($arrDocumentsToExport as $thisDocumentsToExport){
						$arrLogExportData[$thisDocumentsToExport] = array(
							"exportFactoringData_datei_id" => "",
							"exportFactoringData_importart" => "",
							"exportFactoringData_kreditoren_id" => "",
							"exportFactoringData_anzahl_saetze" => 0,
							"exportFactoringData_summe_betraege" => 0,
							"exportFactoringData_iso_code" => "",
							"exportFactoringData_einreichungsnummer" => "",

							"exportFactoringData_mandatory" => "",
							"exportFactoringData_fileName" => "",
							"exportFactoringData_filePath" => "",
							"exportFactoringData_fileSize" => 0,
							"exportFactoringData_dateTime" => "",

							"exportFactoringData_selectedDateFrom" => "",
							"exportFactoringData_selectedDateTo" => "",
							"exportFactoringData_firstDocumentNumber" => "",
							"exportFactoringData_lastDocumentNumber" => "",
							"exportFactoringData_allDocumentNumbers" => ""
						);
					}
				}
			// EOF STORE EXPORT LOG DATA

			$arrDocumentNumbers = array();

			// BOF EXPORT REs
				$fp_export = fopen($arrExportFiles["RE"]["CSV"], 'a');

				$exportCSV_content = '';

				$sql_exportInvoicesHead = "
					SELECT
							'RE' AS `importart`,
							'" . KREDITOREN_ID . "' AS `kreditoren_id`,
							COUNT(`" . TABLE_ORDER_INVOICES . "`.`orderDocumentsNumber`) AS `anzahl_saetze`,
							SUM(`" . TABLE_ORDER_INVOICES . "`.`orderDocumentsTotalPrice`) AS `summe_betraege`,
							'EUR' AS `iso_code`,
							'" . basename($arrExportFiles["RE"]["CSV"]) . "' AS `einreichungsnummer`,
							MD5('" . basename($arrExportFiles["RE"]["CSV"]) . "') AS `datei_id`
						FROM `" . TABLE_ORDER_INVOICES . "`

						WHERE 1
							" . $whereInvoices . "

					";

				$rs_exportInvoicesHead = $dbConnection->db_query($sql_exportInvoicesHead);

				while($ds_exportInvoicesHead = mysqli_fetch_assoc($rs_exportInvoicesHead)){
					if($ds_exportInvoicesHead["anzahl_saetze"] == ''){
						$ds_exportInvoicesHead["anzahl_saetze"] = 0;
					}
					if($ds_exportInvoicesHead["summe_betraege"] == ''){
						$ds_exportInvoicesHead["summe_betraege"] = 0;
					}
					$exportCSV_content = implode($csvFieldSeparator, array_values($ds_exportInvoicesHead)) . $csvLineSeparator;
					$exportCSV_content = convertExportString($exportCSV_content);
					fwrite($fp_export, $exportCSV_content);

					$arrLogExportData["RE"]["exportFactoringData_datei_id"]				= $ds_exportInvoicesHead["datei_id"];
					$arrLogExportData["RE"]["exportFactoringData_einreichungsnummer"]	= $ds_exportInvoicesHead["einreichungsnummer"];
					$arrLogExportData["RE"]["exportFactoringData_importart"]			= $ds_exportInvoicesHead["importart"];
					$arrLogExportData["RE"]["exportFactoringData_kreditoren_id"]		= $ds_exportInvoicesHead["kreditoren_id"];
					$arrLogExportData["RE"]["exportFactoringData_anzahl_saetze"]		= $ds_exportInvoicesHead["anzahl_saetze"];
					$arrLogExportData["RE"]["exportFactoringData_summe_betraege"]		= $ds_exportInvoicesHead["summe_betraege"];
					$arrLogExportData["RE"]["exportFactoringData_iso_code"]				= $ds_exportInvoicesHead["iso_code"];
					$arrLogExportData["RE"]["exportFactoringData_filePath"]				= $arrExportFiles["RE"]["CSV"];
					$arrLogExportData["RE"]["exportFactoringData_fileName"]				= basename($arrExportFiles["RE"]["CSV"]);
					$arrLogExportData["RE"]["exportFactoringData_selectedDateFrom"]		= $dateFrom;
					$arrLogExportData["RE"]["exportFactoringData_selectedDateTo"]		= $dateTo;
					$arrLogExportData["RE"]["exportFactoringData_dateTime"]				= date("Y-m-d H:i:s");
					$arrLogExportData["RE"]["exportFactoringData_mandatory"]			= MANDATOR;
				}

				$sql_exportInvoicesData = "
					SELECT
						'" . KREDITOREN_ID . "' AS `kreditoren_id`,
						`" . TABLE_ORDER_INVOICES . "`.`orderDocumentsAddressCompanyCustomerNumber` AS `debitoren_id`,
						'EUR' AS `iso_code`,
						`" . TABLE_ORDER_INVOICES . "`.`orderDocumentsNumber` AS `rechnung_nr`,
						`" . TABLE_ORDER_INVOICES . "`.`orderDocumentsTotalPrice` AS `rechnung_betrag`,
						`" . TABLE_ORDER_INVOICES . "`.`orderDocumentsDocumentDate` AS `rechnung_datum`,

						IF(`" . TABLE_PAYMENT_CONDITIONS . "`.`paymentConditionsDaysLimit` = 0, DATE_ADD(`" . TABLE_ORDER_INVOICES . "`.`orderDocumentsDocumentDate`, INTERVAL " . PAYMENT_DEADLINE_RE_DEFAULT . " DAY),
							DATE_ADD(`" . TABLE_ORDER_INVOICES . "`.`orderDocumentsDocumentDate`, INTERVAL `" . TABLE_PAYMENT_CONDITIONS . "`.`paymentConditionsDaysLimit` DAY)
						) AS `faelligkeit_datum`,

						`" . TABLE_ORDER_INVOICES . "`.`orderDocumentsMwst` AS `mwst`,

						`" . TABLE_PAYMENT_CONDITIONS . "`.`paymentConditionsSkontoDaysLimit` AS `skonto_tage`,
						`" . TABLE_PAYMENT_CONDITIONS . "`.`paymentConditionsSkonto` AS `skonto_prozent`,
						`" . TABLE_PAYMENT_CONDITIONS . "`.`paymentConditionsDaysLimit` AS `netto_tage`,

						/*
						`" . TABLE_PAYMENT_CONDITIONS . "`.`paymentConditionsName` AS `zahlart`,
						*/

						`" . TABLE_ORDER_INVOICES . "`.`orderDocumentsAddressCompany` AS `debitoren_name`,
						`" . TABLE_ORDER_INVOICES . "`.`orderDocumentsAddressName` AS `debitoren_name2`,
						`" . TABLE_ORDER_INVOICES . "`.`orderDocumentsAddressStreet` AS `debitoren_str`,
						`" . TABLE_ORDER_INVOICES . "`.`orderDocumentsAddressStreetNumber` AS `debitoren_hnr`,
						`" . TABLE_ORDER_INVOICES . "`.`orderDocumentsAddressZipcode` AS `debitoren_plz`,
						`" . TABLE_ORDER_INVOICES . "`.`orderDocumentsAddressCity` AS `debitoren_ort`,

						/*
						`" . TABLE_COUNTRIES . "`.`countries_name_DE` AS `debitoren_land`,
						*/

						`" . TABLE_COUNTRIES . "`.`countries_iso_code_2` AS `debitoren_land_iso2`,

						`" . TABLE_CUSTOMERS . "`.`customersTelefon1` AS `debitoren_telefon1`,
						`" . TABLE_CUSTOMERS . "`.`customersTelefon2` AS `debitoren_telefon2`,
						`" . TABLE_CUSTOMERS . "`.`customersMobil1` AS `debitoren_mobil1`,
						`" . TABLE_CUSTOMERS . "`.`customersMobil2` AS `debitoren_mobil2`,
						`" . TABLE_CUSTOMERS . "`.`customersFax1` AS `debitoren_fax1`,
						`" . TABLE_CUSTOMERS . "`.`customersFax2` AS `debitoren_fax2`,
						`" . TABLE_CUSTOMERS . "`.`customersMail1` AS `debitoren_mail1`,
						`" . TABLE_CUSTOMERS . "`.`customersMail2` AS `debitoren_mail2`,
						`" . TABLE_CUSTOMERS . "`.`customersHomepage` AS `debitoren_www`


						FROM `" . TABLE_ORDER_INVOICES . "`

						LEFT JOIN `" . TABLE_COUNTRIES . "`
						ON(`" . TABLE_ORDER_INVOICES . "`.`orderDocumentsAddressCountry` = `" . TABLE_COUNTRIES . "`.`countries_id`)

						LEFT JOIN `" . TABLE_PAYMENT_CONDITIONS . "`
						ON(`" . TABLE_ORDER_INVOICES . "`.`orderDocumentsPaymentCondition` = `" . TABLE_PAYMENT_CONDITIONS . "`.`paymentConditionsID`)

						LEFT JOIN `" . TABLE_CUSTOMERS . "`
						ON(`" . TABLE_ORDER_INVOICES . "`.`orderDocumentsAddressCompanyCustomerNumber` = `" . TABLE_CUSTOMERS . "`.`customersKundennummer`)

						WHERE 1
							" . $whereInvoices . "
						ORDER BY `" . TABLE_ORDER_INVOICES . "`.`orderDocumentsNumber` ASC
					";
				$rs_exportInvoicesData = $dbConnection->db_query($sql_exportInvoicesData);

				$countTotal = $dbConnection->db_getMysqlNumRows($rs_exportInvoicesData);
				$countItem = 0;

				$arrInvoicesOverFactoringLimits = array();

				while($ds_exportInvoicesData = mysqli_fetch_assoc($rs_exportInvoicesData)){
					if($countItem == 0){
						$arrLogExportData["RE"]["exportFactoringData_firstDocumentNumber"]			= $ds_exportInvoicesData["rechnung_nr"];
					}
					else if($countItem == ($countTotal - 1)){
						$arrLogExportData["RE"]["exportFactoringData_lastDocumentNumber"]			= $ds_exportInvoicesData["rechnung_nr"];
					}
					$arrDocumentNumbers["RE"][] = $ds_exportInvoicesData["rechnung_nr"];

					if($ds_exportInvoicesData["rechnung_betrag"] >= FACTORING_MAX_INVOICE_LIMIT){
						$arrInvoicesOverFactoringLimits[$countItem] = array("DOC_NR" => $ds_exportInvoicesData["rechnung_nr"], "DOC_SUM" => $ds_exportInvoicesData["rechnung_betrag"]);
					}

					$exportCSV_content = implode($csvFieldSeparator, array_values($ds_exportInvoicesData)) . $csvLineSeparator;
					$exportCSV_content = convertExportString($exportCSV_content);
					fwrite($fp_export, $exportCSV_content);
					$countItem++;
				}

				$arrLogExportData["RE"]["exportFactoringData_allDocumentNumbers"]					= implode(";", $arrDocumentNumbers["RE"]);
				$arrLogExportData["RE"]["exportFactoringData_fileSize"]								= filesize($arrExportFiles["RE"]["CSV"]);
				if($fp_export){
					fclose($fp_export);
				}
			// EOF EXPORT REs

			// BOF EXPORT GUs
				$fp_export = fopen($arrExportFiles["GU"]["CSV"], 'a');

				$exportCSV_content = '';

				$xxx_sql_exportCreditsHead = "
					SELECT
							'GS' AS `importart`,
							'" . KREDITOREN_ID . "' AS `kreditoren_id`,
							COUNT(DISTINCT `" . TABLE_ORDER_CREDITS . "`.`orderDocumentsNumber`) AS `anzahl_saetze`,
							SUM(DISTINCT `" . TABLE_ORDER_CREDITS . "`.`orderDocumentsTotalPrice`) AS `summe_betraege`,
							'EUR' AS `iso_code`,
							'" . basename($arrExportFiles["GU"]["CSV"]) . "' AS `einreichungsnummer`,
							MD5('" . basename($arrExportFiles["GU"]["CSV"]) . "') AS `datei_id`

						FROM `" . TABLE_ORDER_CREDITS . "`

						LEFT JOIN `" . TABLE_RELATED_DOCUMENTS . "`
						ON(`" . TABLE_ORDER_CREDITS . "`.`orderDocumentsNumber` = `" . TABLE_RELATED_DOCUMENTS . "`.`relatedDocuments_GU`)

						LEFT JOIN `" . TABLE_ORDER_INVOICES . "`
						ON(`" . TABLE_RELATED_DOCUMENTS . "`.`relatedDocuments_RE` = `" . TABLE_ORDER_INVOICES . "`.`orderDocumentsNumber`)

						WHERE 1
							" . $whereCredits . "
					";

				$sql_exportCreditsHead = "
						SELECT
							'GS' AS `importart`,
							'" . KREDITOREN_ID . "' AS `kreditoren_id`,
							COUNT(`tempTable`.`orderDocumentsNumber`) AS `anzahl_saetze`,
							SUM(`tempTable`.`orderDocumentsTotalPrice`) AS `summe_betraege`,
							'EUR' AS `iso_code`,
							'" . basename($arrExportFiles["GU"]["CSV"]) . "' AS `einreichungsnummer`,
							MD5('" . basename($arrExportFiles["GU"]["CSV"]) . "') AS `datei_id`

							FROM (
								SELECT
									*
									FROM `" . TABLE_ORDER_CREDITS . "`

									LEFT JOIN `" . TABLE_RELATED_DOCUMENTS . "`
									ON(`" . TABLE_ORDER_CREDITS . "`.`orderDocumentsNumber` = `" . TABLE_RELATED_DOCUMENTS . "`.`relatedDocuments_GU`)

									WHERE 1
										" . $whereCredits . "

									GROUP BY `" . TABLE_ORDER_CREDITS . "`.`orderDocumentsNumber`
							)
							AS `tempTable`
					";


				$rs_exportCreditsHead = $dbConnection->db_query($sql_exportCreditsHead);

				while($ds_exportCreditsHead = mysqli_fetch_assoc($rs_exportCreditsHead)){
					if($ds_exportCreditsHead["anzahl_saetze"] == ''){
						$ds_exportCreditsHead["anzahl_saetze"] = 0;
					}
					if($ds_exportCreditsHead["summe_betraege"] == ''){
						$ds_exportCreditsHead["summe_betraege"] = 0;
					}

					$exportCSV_content = implode($csvFieldSeparator, array_values($ds_exportCreditsHead)) . $csvLineSeparator;
					$exportCSV_content = convertExportString($exportCSV_content);
					fwrite($fp_export, $exportCSV_content);

					$arrLogExportData["GU"]["exportFactoringData_datei_id"]				= $ds_exportCreditsHead["datei_id"];
					$arrLogExportData["GU"]["exportFactoringData_einreichungsnummer"]	= $ds_exportCreditsHead["einreichungsnummer"];
					$arrLogExportData["GU"]["exportFactoringData_importart"]			= $ds_exportCreditsHead["importart"];
					$arrLogExportData["GU"]["exportFactoringData_kreditoren_id"]		= $ds_exportCreditsHead["kreditoren_id"];
					$arrLogExportData["GU"]["exportFactoringData_anzahl_saetze"]		= $ds_exportCreditsHead["anzahl_saetze"];
					$arrLogExportData["GU"]["exportFactoringData_summe_betraege"]		= $ds_exportCreditsHead["summe_betraege"];
					$arrLogExportData["GU"]["exportFactoringData_iso_code"]				= $ds_exportCreditsHead["iso_code"];
					$arrLogExportData["GU"]["exportFactoringData_filePath"]				= $arrExportFiles["GU"]["CSV"];
					$arrLogExportData["GU"]["exportFactoringData_fileName"]				= basename($arrExportFiles["GU"]["CSV"]);
					$arrLogExportData["GU"]["exportFactoringData_selectedDateFrom"]		= $dateFrom;
					$arrLogExportData["GU"]["exportFactoringData_selectedDateTo"]		= $dateTo;
					$arrLogExportData["GU"]["exportFactoringData_dateTime"]				= date("Y-m-d H:i:s");
					$arrLogExportData["GU"]["exportFactoringData_mandatory"]			= MANDATOR;
				}

				$sql_exportCreditsData = "
					SELECT
						'" . KREDITOREN_ID . "' AS `kreditoren_id`,
						`" . TABLE_ORDER_CREDITS . "`.`orderDocumentsAddressCompanyCustomerNumber` AS `debitoren_id`,
						'EUR' AS `iso_code`,
						`" . TABLE_ORDER_CREDITS . "`.`orderDocumentsTotalPrice` AS `gutschrift_betrag`,
						`" . TABLE_ORDER_CREDITS . "`.`orderDocumentsNumber` AS `gutschrift_nr`,
						`" . TABLE_ORDER_CREDITS . "`.`orderDocumentsDocumentDate` AS `gutschrift_datum`,

						`" . TABLE_RELATED_DOCUMENTS . "`.`relatedDocuments_RE` AS `rechnung_nr`,
						`" . TABLE_ORDER_INVOICES . "`.`orderDocumentsTotalPrice` AS `rechnung_betrag`,
						`" . TABLE_ORDER_INVOICES . "`.`orderDocumentsDocumentDate` AS `rechnung_datum`

						FROM `" . TABLE_ORDER_CREDITS . "`

						LEFT JOIN `" . TABLE_RELATED_DOCUMENTS . "`
						ON(`" . TABLE_ORDER_CREDITS . "`.`orderDocumentsNumber` = `" . TABLE_RELATED_DOCUMENTS . "`.`relatedDocuments_GU`)

						LEFT JOIN `" . TABLE_ORDER_INVOICES . "`
						ON(`" . TABLE_RELATED_DOCUMENTS . "`.`relatedDocuments_RE` = `" . TABLE_ORDER_INVOICES . "`.`orderDocumentsNumber`)

						WHERE 1
							" . $whereCredits . "

						GROUP BY `" . TABLE_ORDER_CREDITS . "`.`orderDocumentsNumber`

						ORDER BY `" . TABLE_ORDER_CREDITS . "`.`orderDocumentsNumber` ASC
					";
				$rs_exportCreditsData = $dbConnection->db_query($sql_exportCreditsData);

				$countTotal = $dbConnection->db_getMysqlNumRows($rs_exportCreditsData);
				$countItem = 0;

				while($ds_exportCreditsData = mysqli_fetch_assoc($rs_exportCreditsData)){
					if($countItem == 0){
						$arrLogExportData["GU"]["exportFactoringData_firstDocumentNumber"]			= $ds_exportCreditsData["gutschrift_nr"];
					}
					else if($countItem == ($countTotal - 1)){
						$arrLogExportData["GU"]["exportFactoringData_lastDocumentNumber"]			= $ds_exportCreditsData["gutschrift_nr"];
					}
					$arrDocumentNumbers["GU"][] = $ds_exportCreditsData["gutschrift_nr"];

					$exportCSV_content = implode($csvFieldSeparator, array_values($ds_exportCreditsData)) . $csvLineSeparator;
					$exportCSV_content = convertExportString($exportCSV_content);
					fwrite($fp_export, $exportCSV_content);

					$countItem++;
				}

				$arrLogExportData["GU"]["exportFactoringData_allDocumentNumbers"]					= implode(";", $arrDocumentNumbers["GU"]);
				$arrLogExportData["GU"]["exportFactoringData_fileSize"]								= filesize($arrExportFiles["GU"]["CSV"]);

				if($fp_export){
					fclose($fp_export);
				}
			// EOF EXPORT GUs

			// BOF STORE EXPORT LOG DATA
				if(!empty($arrLogExportData)){
					foreach($arrLogExportData as $thisLogData){
						$sql_insertLogExportData = "
								REPLACE INTO `" . TABLE_EXPORT_FACTORING_DATA . "` (`" . implode("`, `", array_keys($thisLogData)) . "`) VALUES ('" . implode("', '", array_values($thisLogData)) . "');
							";
						$rs_insertLogExportData = $dbConnection->db_query($sql_insertLogExportData);
					}
				}
			// EOF STORE EXPORT LOG DATA

			// BOF EXPORT PDF DOCUMENTS
				/*
				if(!empty($arrDocumentNumbers)){
					$arrAllDocuments = array();
					foreach($arrDocumentNumbers as $thisArrDocumentNumbersKey => $thisArrDocumentNumbersValue){
						$arrAllDocuments = array_merge($arrAllDocuments, $thisArrDocumentNumbersValue);
					}

					$sql_exportPdfDocuments = "
						SELECT
								`createdDocumentsFilename`

							FROM `" . TABLE_CREATED_DOCUMENTS . "`

							WHERE 1
								AND (
									`createdDocumentsNumber` = '" . implode("' " . "\n" . " OR `createdDocumentsNumber` = '", $arrAllDocuments). "'
								)
						";
					$rs_exportPdfDocuments = $dbConnection->db_query($sql_exportPdfDocuments);

					while($ds_exportPdfDocuments = mysqli_fetch_assoc($rs_exportPdfDocuments)){
						if(file_exists($ds_exportPdfDocuments["createdDocumentsFilename"])){
							$arrFilesToZip[] = "./" . $ds_exportPdfDocuments["createdDocumentsFilename"];
						}
					}
				}
				*/
			// BOF EXPORT PDF DOCUMENTS

			// BOF CUSTOMERS EXPORT
				/*
				if($_POST["exportCustomers"] == '1'){
					$fileNameExportCustomers = date("Y-m-d") . '_EXPORT_CUSTOMERS.csv';

					$pathExportCustomers = PATH_EXPORTED_FILES . $fileNameExportCustomers;

					if(file_exists($pathExportCustomers)){
						unlink($pathExportCustomers);
					}

					$fp_export = fopen($pathExportCustomers, 'a');

					$sql_exportCustomers = "
							SELECT
								'" . KREDITOREN_ID . "' AS `Factoringvertrags_Nr`,
								`customersKundennummer` AS `Debitorennummer`,
								`customersFirmenname` AS `Debitorenname`,

								'' AS `Limitwunsch`,
								`customersCompanyStrasse` AS `Strasse`,
								`customersCompanyHausnummer` AS `Hausnr`,
								'' AS `Postfach`,
								`customersCompanyPLZ` AS `PLZ`,
								`customersCompanyOrt` AS `Ort`,
								`" . TABLE_COUNTRIES . "`.`countries_iso_code_2` AS `Nation`,

								'' AS `Adresse_Memo`,
								'EUR' AS `Rechnungswaehrung`,

								'' AS `Branche`,

								IF(`customersTelefon1` IS NULL, '' , `customersTelefon1`) AS `Tel_1`,
								IF(`customersTelefon2` IS NULL, '' , `customersTelefon2`) AS `Tel_2`,

								IF(`customersMobil1` IS NULL, '' , `customersMobil1`) AS `Mobil`,
								IF(`customersFax1` IS NULL, '' , `customersFax1`) AS `Fax`,

								IF(`customersMail1` IS NULL, '' , `customersMail1`) AS `Mail`,

								IF(`customersHomepage` IS NULL, '' , `customersHomepage`) AS `www`,

								'' AS `HR_Nr`,
								'' AS `HR_Ort`,
								'' AS `Crefo_Nr`,
								'' AS `Buergel_Nr`,
								'' AS `Schimmelpfennig_Nr`,
								'' AS `Leerspalte`,
								'' AS `Skt_%_1`,
								'' AS `Skt_Tage_1`,
								'' AS `Skt_%_2`,
								'' AS `Skt_Tage_2`,
								'' AS `Skt_%_3`,
								'' AS `Skt_Tage_3`,
								'' AS `Skt_%_4`,
								'' AS `Skt_Tage_4`,
								'' AS `Skt_%_5`,
								'' AS `Skt_Tage_5`,
								'' AS `Netto_Tage`,
								'' AS `Netto_Kennzeichen`,
								'' AS `Karenztage`


							FROM (
									SELECT
										`orderDocumentsCustomerNumber` AS `customerNumber`
										FROM `b3_orderInvoices`

										GROUP BY `orderDocumentsCustomerNumber`

									UNION

									SELECT
										`orderDocumentsAddressCompanyCustomerNumber` AS `customerNumber`
										FROM `b3_orderInvoices`

										GROUP BY `orderDocumentsAddressCompanyCustomerNumber`

									UNION

									SELECT
										`customersKundennummer` AS `customerNumber`
										FROM `b3_orderInvoices`

										INNER JOIN `common_customers`
										ON(`b3_orderInvoices`.`orderDocumentsSalesman` = `common_customers`.`customersID`)

										GROUP BY `customersKundennummer`

									UNION

									SELECT
										`orderDocumentsCustomerNumber` AS `customerNumber`
										FROM `bctr_orderInvoices`

										GROUP BY `orderDocumentsCustomerNumber`

									UNION

									SELECT
										`orderDocumentsAddressCompanyCustomerNumber` AS `customerNumber`
										FROM `bctr_orderInvoices`

										GROUP BY `orderDocumentsAddressCompanyCustomerNumber`

									UNION

									SELECT
										`customersKundennummer` AS `customerNumber`
										FROM `bctr_orderInvoices`

										INNER JOIN `common_customers`
										ON(`bctr_orderInvoices`.`orderDocumentsSalesman` = `common_customers`.`customersID`)


										GROUP BY `customersKundennummer`
									) AS `tempTable`
							INNER JOIN `common_customers`
							ON(`tempTable`.`customerNumber` = `common_customers`.`customersKundennummer`)

							LEFT JOIN `" . TABLE_COUNTRIES . "`
							ON(`" . TABLE_CUSTOMERS . "`.`customersCompanyCountry` = `" . TABLE_COUNTRIES . "`.`countries_id`)

							WHERE 1
						";

					$rs_exportCustomers = $dbConnection->db_query($sql_exportCustomers);

					$countRow = 0;

					$exportCustomersCSV_content = '';

					while($ds_exportCustomers = mysqli_fetch_assoc($rs_exportCustomers)){
						if($countRow == 0){
							$exportCustomersCSV_content = implode($csvFieldSeparator, array_keys($ds_exportCustomers)) . $csvLineSeparator;
							$exportCustomersCSV_content = convertExportString($exportCustomersCSV_content);
							fwrite($fp_export, $exportCustomersCSV_content);
						}

						$exportCustomersCSV_content = implode($csvFieldSeparator, array_values($ds_exportCustomers)) . $csvLineSeparator;
						$exportCustomersCSV_content = convertExportString($exportCustomersCSV_content);
						fwrite($fp_export, $exportCustomersCSV_content);
						$countRow++;
					}

					if($fp_export){
						fclose($fp_export);
					}
				}
				*/
			// EOF CUSTOMERS EXPORT
			#######################################

			// BOF ZIP FILES
				if(!empty($arrFilesToZip)){
					// BOF CREATE ZIP FILE
						if(file_exists($pathExportZIP)){
							unlink($pathExportZIP);
						}

						$createCommand = ZIP_PATH.' a ' . $pathExportZIP . ' ' . implode(" ", $arrFilesToZip) . '';
						$arrExecResults[] = $createCommand;
						$arrExecResults[] = exec($createCommand, $error);
						if(!empty($error)) {
							$arrExecResults[] = array('1. ZIP: error', $error);
						}
					// EOF CREATE ZIP FILE
					dd('createCommand');
					dd('arrExecResults');
				}
			// EOF ZIP FILES

			// BOF SET ATTECHMENT FILES
				$arrAttachmentFiles = array();
				if(!empty($arrExportFiles)){
					foreach($arrExportFiles as $thisExportDocType => $thisExportDocTypeFiles){
						foreach($thisExportDocTypeFiles as $thisFileType => $thisFileName){
							if(file_exists($thisFileName)){
								$arrAttachmentFiles[$thisFileName] = basename($thisFileName);
							}
						}
					}
				}
				if(file_exists($pathExportCustomers)){
					$arrAttachmentFiles[$pathExportCustomers] = basename($pathExportCustomers);
				}
				if(file_exists($pathExportZIP)){
					$arrAttachmentFiles[$pathExportZIP] = basename($pathExportZIP);
				}
			// EOF SET ATTECHMENT FILES

			// BOF SEND MAIL WITH ATTACHED DOCUMENT
				if(!empty($arrAttachmentFiles) && SEND_FAKTORING_EXPORT_PER_MAIL == true){
					$sendAttachedDocument = '1';
					if($sendAttachedDocument == '1') {
						#$arrMailContentDatas = array();

						#$selectSubject = 'Export Faktoring - ' . date("Y-m-d") . ': ' . $fileNameExportBasic;
						$selectSubject = 'Export Faktoring von ' . $_POST["exportDateStart"] . ' bis ' . $_POST["exportDateEnd"];

						$generatedDocumentNumber = basename($fileNameExportBasic);
						// BOF CREATE SUBJECT
							$thisSubject = $selectSubject;
						// EOF CREATE SUBJECT

						$documentType = 'SX';
						$selectCustomersRecipientName = "ThoHo";
						$selectMailtextTemplates = 'SX';

						// $selectCustomersRecipientMail = MAIL_ADDRESS_FAKTORING_COMPANY;
						$selectCustomersRecipientMail = 'webmaster@burhan-ctr.de';
						#$selectCustomersRecipientMail = $exportOrdersVertreterMail . ';' . 'webmaster@burhan-ctr.de';
						$sendAttachedMailText = 'Sehr geehrte Damen und Herren,' . "\n\n" . 'anbei die exportierten Daten vom ' . formatDate(date("Y-m-d"), "display") . ' .';
						$selectMailtextSender = 'webmaster@burhan-ctr.de';
						$createMail = new createMail(
											$thisSubject,													// TEXT:	MAIL_SUBJECT
											$documentType,													// STRING:	DOCUMENT TYPE
											$generatedDocumentNumber,										// STRING:	DOCUMENT NUMBER
											$arrAttachmentFiles,											// ARRAY:	ATTACHMENT FILES
											$selectCustomersRecipientMail,	 								// STRING:	RECIPIENTS
											$arrMailContentDatas[$selectMailtextTemplates],					// MAIL_TEXT_TEMPLATE
											$mailTemplate["html"],											// MAIL_BODY_TEMPLATE
											'',																// STRING:	ADDITIONAL TEXT
											$sendAttachedMailText,											// STRING:	SEND ATTACHED TEXT
											true,															// BOOLEAN:	SEND ATTACHED DOCUMENT
											DIRECTORY_MAIL_IMAGES,											// DIRECTORY_MAIL_IMAGES
											$selectMailtextSender											// STRING: SENDER
										);

						$createMailResult = $createMail->returnResult();
						$sendMailToClient = $createMailResult;
						// EOF SEND MAIL

						if(!$sendMailToClient) {
							$errorMessage .= ' Die Mail mit den erzeugten Dateien konnte nicht an ' . $selectCustomersRecipientName . ' (' . $selectCustomersRecipientMail . ') versendet werden! ' . "<br />";
						}
						else {
							$successMessage .= ' Die Mail mit den erzeugten Dateien wurde an ' . $selectCustomersRecipientName . ' (' . $selectCustomersRecipientMail . ') versendet! ' . "<br />";
						}
						if(file_exists($pathExportZIP)){
							unlink($pathExportZIP);
						}
					}
				}
			// EOF SEND MAIL WITH ATTACHED DOCUMENT
		}
	// EOF EXPORT DOCUMENTS
?>

<div id="mainArea">
	<div id="mainContent">
		<?php require_once(FILE_MENUE_SIDEBAR); ?>
		<div id="contentArea">
			<a name="top"></a>
			<div id="contentAreaElements">
				<h1><?php if(SHOW_PAGE_TITLE_ICONS) { echo '<img src="' . PATH_ICONS_MENUE_TITLES . 'export.png" alt="" />'; } ?> <?php echo $thisTitle; ?></h1>
				<p><b>Kreditoren-ID: <?php echo KREDITOREN_ID; ?></b></p>
				<div id="searchFilterArea">
					<div style="float:left;width:480px;margin:0;padding:0;">
						<form name="formExportDatas" method="post" action="<?php echo $_SERVER["PHP_SELF"]; ?>">
							<table border="0" cellpadding="0" cellspacing="0" class="searchFilterContent">
								<tr>
									<td><b>Rechnungs-Datum </b></td>
									<td>
										<label for="exportDateStart">von:</label>
										<input type="text" name="exportDateStart" id="exportDateStart" maxlength="2" class="inputField_70" readonly="readonly" value="<?php if($_POST["exportDateStart"] != '') { echo $_POST["exportDateStart"]; } else { echo $exportDateStart; } ?>" />
									</td>
									<td>
										<label for="exportDateEnd">bis:</label>
										<input type="text" name="exportDateEnd" id="exportDateEnd" maxlength="2" class="inputField_70" readonly="readonly" value="<?php if($_POST["exportDateEnd"] != '') { echo $_POST["exportDateEnd"]; } else { echo $exportDateEnd; } ?>" />
									</td>
									<!--
									<td>
										<label for="exportCustomers">Kunden exportieren?</label>
										<input type="checkbox" name="exportCustomers" id="exportCustomers" value="1" />
									</td>
									-->
									<td>
										<input type="hidden" name="editID" id="editID" value="" />
										<input type="submit" name="submitExport" class="inputButton0" value="Daten exportieren" />
									</td>
								</tr>
							</table>
							<input type="hidden" name="submitExportForm" id="submitExportForm" value="1" />
						</form>
					</div>
					<div style="float:left;margin:0;padding:0;border-left:1px solid #666;">
						<form name="formSearchDatas" method="post" action="<?php echo $_SERVER["PHP_SELF"]; ?>">
							<table border="0" cellpadding="0" cellspacing="0" class="searchFilterContent">
								<tr>
									<td>
										<label for="searchDocumentNumber">RE / GU suchen:</label>
										<input type="text" name="searchDocumentNumber" id="searchDocumentNumber" class="inputField_120" value="" />
									</td>
									<td>
										<input type="hidden" name="editID" id="editID" value="" />
										<input type="submit" name="submitSearch" class="inputButton0" value="Dokument in Export suchen" />
									</td>
								</tr>
							</table>
						</form>
					</div>
					
					<?php if($_COOKIE["isAdmin"] == '1xxx'){ ?>
					Bestimmte Rechnungen exportieren -  noch nicht implementiert
					<div style="float:left;margin:0;padding:0;border-left:1px solid #666;">
						<form name="formSpecialExportDatas" method="post" action="<?php echo $_SERVER["PHP_SELF"]; ?>">
							<table border="0" cellpadding="0" cellspacing="0" class="searchFilterContent">
								<tr>
									<td>
										<label for="exportDocumentNumbers">RE:</label>
										<textarea name="specialDocumentNumbers" id="specialDocumentNumbers" class="inputField_120" /></textarea>
									</td>
									<td>
										<input type="hidden" name="editID" id="editID" value="" />
										<input type="submit" name="submitSpecialExportForm" class="inputButton0" value="Bestimmte Rechnungen exportieren" />
									</td>
								</tr>
							</table>
						</form>
					</div>
					
					<div class="clear"></div>
				</div>
				<?php } ?>
				
				<?php
					if(!empty($arrInvoicesOverFactoringLimits)){
						$jswindowMessage .= 'Der Export enthält Rechnungen über dem Faktoring-Limit!';
						$warningMessage .= 'Der Export enth&auml;lt Rechnungen &uuml;ber dem Faktoring-Limit (' . number_format(FACTORING_MAX_INVOICE_LIMIT, 0, ',', '.') . ')' . '<br />';
						foreach($arrInvoicesOverFactoringLimits as $thisInvoicesOverFactoringLimitData){
							$warningMessage .= ' &bull; ' . $thisInvoicesOverFactoringLimitData["DOC_NR"] . ': ' . number_format($thisInvoicesOverFactoringLimitData["DOC_SUM"], 2, ',', '.') . ' EUR' . '<br />';
						}
					}
				?>


				<div class="contentDisplay">
					<?php displayMessages(); ?>
					<?php
						if($_POST["submitExportForm"] == 1) {
							if(!empty($arrExportFiles)){
								foreach($arrExportFiles as $thisExportDocType => $thisExportDocTypeFiles){
									foreach($thisExportDocTypeFiles as $thisFileType => $thisFileName){
										if(file_exists($thisFileName)){
											echo '<p class="dataTableContent"><b>DOWNLOAD ' . $thisExportDocType . '-Datei: </b><img src="layout/icons/iconEXCEL.gif" height="16" width="16" alt="" /> <a href="?downloadFile=' . basename($thisFileName).'&amp;downloadType=EXPORT">'.utf8_decode(basename($thisFileName)).'</a></p>';
										}
									}
								}
							}
							if(file_exists($pathExportCustomers)){
								echo '<p class="dataTableContent"><b>DOWNLOAD Kunden-Datei: </b><img src="layout/icons/iconEXCEL.gif" height="16" width="16" alt="" /> <a href="?downloadFile=' . basename($pathExportCustomers).'&amp;downloadType=EXPORT">'.utf8_decode(basename($pathExportCustomers)).'</a></p>';
							}
							echo '<hr />';
							// $_POST["editID"] = "";
						}
					?>

					<div style="text-align:right"><a href="<?php echo URL_FACTORING_PORTAL; ?>" target="_blank">Zum Factoring Portal</a></div>

					<?php
						if($_POST["searchDocumentNumber"] != ''){
							echo '<h2>Bisher exportierte Daten mit der RE- / GU-Nummer &quot;' . $_POST["searchDocumentNumber"] . '&quot;</h2>';
						}
						else {
							echo '<h2>Bisher exportierte Daten</h2>';
						}
					?>

					<?php

						// BOF SEARCH DOCUMENTS
							$where = "";
							if($_POST["submitSearch"] != "" && $_POST["searchDocumentNumber"] != ""){
								$where = "
									AND `exportFactoringData_allDocumentNumbers` LIKE '%" . $_POST["searchDocumentNumber"] . "%'
								";
							}
						// EOF SEARCH DOCUMENTS


						$sql = "
								SELECT
									SQL_CALC_FOUND_ROWS

									`exportFactoringData_datei_id`,
									`exportFactoringData_importart`,
									`exportFactoringData_kreditoren_id`,
									`exportFactoringData_anzahl_saetze`,
									`exportFactoringData_summe_betraege`,
									`exportFactoringData_iso_code`,
									`exportFactoringData_einreichungsnummer`,
									`exportFactoringData_mandatory`,
									`exportFactoringData_filename`,
									`exportFactoringData_filepath`,
									`exportFactoringData_filesize`,
									`exportFactoringData_datetime`,
									`exportFactoringData_selectedDateFrom`,
									`exportFactoringData_selectedDateTo`,
									`exportFactoringData_firstDocumentNumber`,
									`exportFactoringData_lastDocumentNumber`,
									`exportFactoringData_allDocumentNumbers`

								FROM `" . TABLE_EXPORT_FACTORING_DATA . "`
								WHERE 1
									 " . $where . "
								ORDER BY
									`exportFactoringData_datetime` DESC,
									`exportFactoringData_importart` DESC
							";

						if($_REQUEST["page"] == "" || $_REQUEST["page"] == 0) {
							$_REQUEST["page"] = 1;
						}

						if(MAX_ORDERS_PER_PAGE > 0) {
							$sql .= " LIMIT " . (($_REQUEST["page"] - 1) * MAX_ORDERS_PER_PAGE) . ", " . MAX_ORDERS_PER_PAGE." ";
						}

						$rs = $dbConnection->db_query($sql);

						$sql_totalRows = "SELECT FOUND_ROWS() AS `countRows`";
						$rs_totalRows = $dbConnection->db_query($sql_totalRows);
						list($thisNumRows) = mysqli_fetch_array($rs_totalRows);

						if($thisNumRows > 1) {
							$pagesCount = ceil($thisNumRows / MAX_ORDERS_PER_PAGE);
						}

						if($dbConnection->db_getMysqlNumRows($rs) > 0){

							if($pagesCount > 1) {
								include(FILE_MENUE_PAGES);
							}

							echo '<table cellspacing="0" cellpadding="0" width="100%" border="0" class="displayOrders">';

							echo '<colgroup>';
							echo '<col />';
							echo '<col />';
							echo '<col />';
							echo '<col />';
							echo '<col />';
							echo '<col />';
							echo '<col />';
							echo '<col />';
							echo '<col />';
							if($arrGetUserRights["adminArea"]) {
								echo '<col />';
							}
							echo '<col />';
							echo '</colgroup>';

							echo '<thead>';
							echo '<tr>';
							echo '<th style="width:45px !important;">#</th>';
							echo '<th style="width:70px;">Export-Datum</th>';
							echo '<th style="width:70px;">Import-Typ</th>';
							echo '<th style="width:70px;">von</th>';
							echo '<th style="width:70px;">bis</th>';
							echo '<th style="width:70px;">Kreditor</th>';
							echo '<th style="width:70px;">Anzahl GU/RE</th>';
							echo '<th style="width:70px;">Summe</th>';
							echo '<th style="width:70px;">Dateiname</th>';
							if($arrGetUserRights["adminArea"]) {
								echo '<th style="width:70px;">Datei-ID</th>';
							}
							echo '<th style="width:70px;">Info</th>';
							echo '</tr>';
							echo '</thead>';

							echo '<tbody>';

							$countRow = 0;

							$thisMarker = '';
							while($ds = mysqli_fetch_assoc($rs)){

								$rowClass = 'row0';
								if($countRow%2 == 0) {
									$rowClass = 'row1';
								}

								$rowStyle = '';
								$thisExportFactoringData_datetime = substr($ds["exportFactoringData_datetime"], 0, -3);
								if($thisMarker != $thisExportFactoringData_datetime){
									$thisMarker = $thisExportFactoringData_datetime;
									$rowStyle .= 'border-top:2px solid #000;';
								}

								if($ds["exportFactoringData_anzahl_saetze"] == 0){
									$rowStyle .= 'color:#666;';
								}

								if(date("Y-m-d") == $ds["exportFactoringData_selectedDateTo"]){
									$rowClass = 'row3';
								}

								echo '<tr class="' . $rowClass . '" style="' . $rowStyle . '">';


								echo '<td style="text-align:right;">';
								echo '<b>' . ($countRow + 1) . '.</b>';
								echo '</td>';

								echo '<td style="white-space:nowrap;">';
								echo formatDate($ds["exportFactoringData_datetime"], "display");
								echo '</td>';

								echo '<td>';
								echo '<b>' . $ds["exportFactoringData_importart"] . '</b>';
								echo '</td>';

								$thisCellStyle = '';
								if($exportDateStart == formatDate($ds["exportFactoringData_selectedDateFrom"], "display")){
									$thisCellStyle = 'background-color:#DBFFCF;';
								}
								echo '<td style="' . $thisCellStyle . '">';
								echo formatDate($ds["exportFactoringData_selectedDateFrom"], "display");
								echo '</td>';



								$thisCellStyle = '';
								if($exportDateEnd == formatDate($ds["exportFactoringData_selectedDateTo"], "display")){
									$thisCellStyle = 'background-color:#DBFFCF;';
								}
								echo '<td style="font-weight:bold;background-color:#FEFFAF;' . $thisCellStyle . '">';
								echo formatDate($ds["exportFactoringData_selectedDateTo"], "display");
								echo '</td>';

								if($arrGetUserRights["adminArea"]) {
								echo '<td>';
								echo $ds["exportFactoringData_kreditoren_id"];
								echo '</td>';
								}

								echo '<td style="text-align:right;">';
								echo $ds["exportFactoringData_anzahl_saetze"];
								echo '</td>';

								echo '<td style="text-align:right;background-color:#FEFFAF;">';
								echo '<b>' . number_format($ds["exportFactoringData_summe_betraege"], 2, ",", ".") . '</b>';
								echo '</td>';

								echo '<td style="white-space:nowrap;font-weight:bold;">';
								echo '<span style="font-size:12px;padding-right:10px;">' . basename($ds["exportFactoringData_filepath"]) . '</span>';
								echo '</td>';

								echo '<td style="font-size:10px;font-family:Courier,Times New Roman">';
								echo $ds["exportFactoringData_datei_id"];
								echo '</td>';


								echo '<td style="white-space:nowrap;">';
								echo '<span class="toolItem">';
								echo '<a href="?downloadFile=' . basename($ds["exportFactoringData_filepath"]).'&amp;downloadType=EXPORT"><img src="layout/icons/iconEXCEL.gif" height="16" width="16" alt="Download" title="' . $ds["exportFactoringData_filepath"] . ' &ouml;ffnen"/></a>';
								echo '</span>';

								echo '<span class="toolItem">';
								echo '<img src="layout/icons/iconInfo.png" class="buttonNotice" width="16" height="16" alt="Info" title="Enthaltene Rechnungen anzeigen" />';
								echo '<div class="displayNoticeArea">';
								echo '<p><b>Datei: ' . basename($ds["exportFactoringData_filepath"]) . '</b></p>';
								if($ds["exportFactoringData_allDocumentNumbers"] != ''){
									$arrDocumentNumbers = explode(';' , $ds["exportFactoringData_allDocumentNumbers"]);
									if(!empty($arrDocumentNumbers)){
										echo '<ol>';
										foreach($arrDocumentNumbers as $thisDocumentNumbers){
											$thisDocumentStyle = '';
											if($_POST["searchDocumentNumber"] != "" && ($_POST["searchDocumentNumber"] == $thisDocumentNumbers || preg_match("/" . $_POST["searchDocumentNumber"] . "/", $thisDocumentNumbers))){
												$thisDocumentStyle = 'color:#F00 !important;font-weight:bold;';
											}
											echo '<li style="border-bottom:1px dotted #CCC;' . $thisDocumentStyle . '"><img src="layout/icons/iconPDF.gif" width="14" height="14" alt="pdf" title="PDF-Datei &ouml;ffnen" /> <a href="?downloadFile=' . basename($thisDocumentNumbers). '&amp;downloadType=' . substr($thisDocumentNumbers, 0, 2) . '" style="' . $thisDocumentStyle . '" >' . $thisDocumentNumbers . '</a></li>';
										}
										echo '</ul>';
									}
								}
								else {
									echo '<p>Es sind keine Dokumente enthalten!</p>';
								}

								echo '</div>';
								echo '</span>';

								echo '</td>';

								echo '</tr>';

								$countRow++;
							}
							echo '</tbody>';
							echo '</table>';

							if($pagesCount > 1) {
								include(FILE_MENUE_PAGES);
							}
						}
						else {
							if($_POST["searchDocumentNumber"] != ''){
								$infoMessage = ' Das Dokument &quot;' . $_POST["searchDocumentNumber"] . '&quot; wurde in den Export-Dateien nicht gefunden'; ;
							}
							else {
								$infoMessage = ' Es liegen keine Exporte vor.';
							}
						}
						displayMessages();
					?>
				</div>

			</div>
		</div>
		<div class="clear"></div>
	</div>
</div>
<script language="javascript" type="text/javascript">
	$(document).ready(function() {
		$(function() {
			$.datepicker.setDefaults($.datepicker.regional[""]);
			$('#exportDateStart').datepicker($.datepicker.regional["de"]);
			$('#exportDateEnd').datepicker($.datepicker.regional["de"]);
			$('#exportDateStart').datepicker("option", "maxDate", "0");
			$('#exportDateEnd').datepicker("option", "maxDate", "0");

			var exportStartDate = new Date("<?php echo EXPORT_DATE_START; ?>");
			var exportStartMonth = exportStartDate.getMonth();
			var exportStartDay = exportStartDate.getDate();
			var exportStartYear = exportStartDate.getFullYear();

			$('#exportDateStart').datepicker("option", "minDate", new Date(exportStartYear, exportStartMonth, exportStartDay));
			$('#exportDateEnd').datepicker("option", "minDate", new Date(exportStartYear, exportStartMonth, exportStartDay));
		});

		$('.buttonNotice').click(function () {
			loadNotice($(this));
		});
	});
</script>
<?php require_once('inc/footerHTML.inc.php'); ?>