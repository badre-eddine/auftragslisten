<?php
	require_once('inc/requires.inc.php');

	if(!$arrGetUserRights["displayDocuments"]) {
		header('location: ' . PAGE_EXIT_LOCATION);
		exit;
	}

	$jswindowMessage = "";
	$warningMessage = '';
	$errorMessage = '';
	$successMessage = '';
	$infoMessage = '';

	$arrQueryVars = getUrlQueryString($_SERVER["REQUEST_URI"]);

	if(!empty($arrQueryVars["searchWord"]) && $arrQueryVars["searchWord"] != "") {
		$arrQueryVars["searchBoxFile"] = trim($arrQueryVars["searchWord"]);
	}

	if($_REQUEST["documentType"] == 'DF') {
		$thisDownloadFolder = DIRECTORY_DOWNLOAD_FILES;
	}
	else if($_REQUEST["documentType"] == 'CF') {
		$thisDownloadFolder = DIRECTORY_CREATED_DOCUMENTS_CUSTOMERS;
		if($arrQueryVars["thisDocumentType2"] == 'KA') {
			$thisDownloadFolder = DIRECTORY_UPLOAD_FILES;
		}
	}
	else if($_REQUEST["documentType"] == 'MF') {
		$thisDownloadFolder = DIRECTORY_DOWNLOAD_MANUALS;
	}
	else if($_REQUEST["documentType"] == 'UF') {
		$thisDownloadFolder = DIRECTORY_UPLOAD_FILES;
	}
	else if($_REQUEST["documentType"] == 'DC') {
		$thisDownloadFolder = DIRECTORY_DOWNLOAD_CATALOGUES;
	}
	else if($_REQUEST["documentType"] == 'IS') {
		$thisDownloadFolder = DIRECTORY_DOWNLOAD_SHEETS;
	}
	else if($_REQUEST["documentType"] == 'CL') {
		$thisDownloadFolder = DIRECTORY_DOWNLOAD_CONTAINER_LISTS;
	}
	else if($_REQUEST["documentType"] == 'KA') {
		$thisDownloadFolder = DIRECTORY_UPLOAD_FILES;
		$thisCreatedPdfName = utf8_decode($thisCreatedPdfName);
	}
	else if($_REQUEST["documentType"] == 'EX') {
		$thisDownloadFolder = DIRECTORY_EXPORT_FILES;
		$thisCreatedPdfName = utf8_decode($thisCreatedPdfName);
	}
	else if($_REQUEST["documentType"] == 'SX') {
		$thisDownloadFolder = DIRECTORY_CREATED_DOCUMENTS_SALESMEN;
		$thisCreatedPdfName = utf8_decode($thisCreatedPdfName);
	}
	else {
		$thisDownloadFolder = DIRECTORY_CREATED_DOCUMENTS_CUSTOMERS;
	}
	if($arrQueryVars["thisDocumentType2"] == 'PR') {
		$thisDownloadFolder = DIRECTORY_CREATED_DOCUMENTS_SALESMEN;
	}

	// BOF DOWNLOAD FILE
	if($arrQueryVars["downloadFile"] != "") {

		$arrQueryVars["downloadFile"] = utf8_decode(urldecode($arrQueryVars["downloadFile"]));

		$arrQueryVars["downloadFile"] = (urldecode($arrQueryVars["downloadFile"]));

		require_once('classes/downloadFile.class.php');
		$thisDownload = new downloadFile($thisDownloadFolder, $arrQueryVars["downloadFile"]);
		$errorMessage .= $thisDownload->startDownload();
	}
	// BOF DOWNLOAD FILE

	$dbConnection = new DB_Connection();
	$db_open = $dbConnection->db_connect();

	// BOF READ DOCUMENT TYPE DATAS
		$arrDocumentTypeDatas = getDocumentTypes();
	// EOF READ DOCUMENT TYPE DATAS

	// BOF READ MANDATORY DATAS
		$arrMandatoryDatas = getMandatories();
	// EOF READ MANDATORY DATAS

	// BOF GET DOCUMENT SUBCATEGORIES
		if($_REQUEST["documentType"] == 'DF' || $_REQUEST["documentType"] == 'DC' || $_REQUEST["documentType"] == 'MF'){
			$arrDownloadFileSubCategoriesData = getDownloadFileSubCategories($_REQUEST["documentType"]);
		}
	// BOF GET DOCUMENT SUBCATEGORIES

	if($_POST["sendAttachedDocument"] == '1') {
		$thisCreatedPdfName = urldecode($_POST["mailDocumentFilename"]);
		$_POST["selectSubject"] = $arrDocumentTypeDatas[$_REQUEST["documentType"]]["createdDocumentsTypesName"] . ' Nr.: ' . '{###DOCUMENT_NUMBER###}';
		$generatedDocumentNumber = substr($_POST["mailDocumentFilename"], 0, 13);

		require_once(DIRECTORY_HTML_MIME_MAIL . 'htmlMimeMail.php');
		require_once(DIRECTORY_MAIL_TEMPLATES . 'template_BASIC.inc.php');
		require_once(DIRECTORY_MAIL_TEMPLATES . 'template_TEXT_DATAS.inc.php');
		##require_once("inc/mail.inc.php");

		// BOF SEND MAIL
		require_once("classes/createMail.class.php");

		// BOF CREATE SUBJECT
		$thisSubject = $_POST["selectSubject"];
		if($_POST["sendAttachedMailSubject"] != '') {
			$thisSubject = $_POST["sendAttachedMailSubject"] . ', ' . $thisSubject;
		}
		// EOF CREATE SUBJECT

		// BOF GET ATTACHED FILE
		$arrAttachmentFiles = array (
			rawurldecode($thisDownloadFolder . $thisCreatedPdfName) => str_replace(" ", "_", rawurldecode(myUnHtmlEntities($thisCreatedPdfName)))
		);
		// EOF GET ATTACHED FILE

		$createMail = new createMail(
							$thisSubject,													// TEXT:	MAIL_SUBJECT
							$_POST["documentType"],											// STRING:	DOCUMENT TYPE
							$generatedDocumentNumber,										// STRING:	DOCUMENT NUMBER
							$arrAttachmentFiles,											// ARRAY:	ATTACHMENT FILES
							$_POST["selectCustomersRecipientMail"],	 										// STRING:	RECIPIENTS
							$arrMailContentDatas[$_POST["selectMailtextTemplates"]],		// MAIL_TEXT_TEMPLATE
							$mailTemplate["html"],											// MAIL_BODY_TEMPLATE
							'',																// STRING:	ADDITIONAL TEXT
							$_POST["sendAttachedMailText"],									// STRING:	SEND ATTACHED TEXT
							true,															// BOOLEAN:	SEND ATTACHED DOCUMENT
							DIRECTORY_MAIL_IMAGES,											// DIRECTORY_MAIL_IMAGES
							$_POST["selectMailtextSender"]									// STRING: SENDER
						);

		$createMailResult = $createMail->returnResult();
		$sendMailToClient = $createMailResult;

		#if(!$sendMailToUs) { $errorMessage .= ' Die Mail-Kopie konnte nicht an ' . implode(', ', $arrOrderMail) . ' versendet werden! '.'<br />'; }
		#else { $successMessage .= ' Die Mail-Kopie wurde an ' . implode(', ', $arrOrderMail) . ' versendet! '.'<br />'; }

		if(!$sendMailToClient) { $errorMessage .= ' Die Mail konnte nicht an ' . $_POST["selectCustomersRecipientName"] . ' (' . $_POST["selectCustomersRecipientMail"] . ') versendet werden! '.'<br />'; }
		else {
			$successMessage .= ' Die Mail wurde an ' . $_POST["selectCustomersRecipientName"] . ' (' . $_POST["selectCustomersRecipientMail"] . ') versendet! '.'<br />';
		}
	}

?>
<?php
	require_once('inc/headerHTML.inc.php');

	if($_GET["documentType"] != '') {
		$_REQUEST["documentType"] = $_GET["documentType"];
	}

	if($_REQUEST["documentType"] == 'DF') {
		$thisTitle = "Download Dokumente";
	}
	else if($_REQUEST["documentType"] == 'CF') {
		$thisTitle = "Kunden-Dokumente anzeigen";
		if(!empty($arrQueryVars["searchBoxFile"])) {
			$thisTitle .= '<span class="headerSelectedEntry"> - Suchbegriff: &quot;' . $arrQueryVars["searchBoxFile"] . '&quot;</span>';
		}
	}
	else if($_REQUEST["documentType"] == 'MF') {
		$thisTitle = "Auflistung Handbücher";
	}
	else if($_REQUEST["documentType"] == 'UF') {
		$thisTitle = "Auflistung hochgeladener Dokumente";
	}
	else if($_REQUEST["documentType"] == 'DC') {
		$thisTitle = "Auflistung der Kataloge und Preislisten";
	}
	else if($_REQUEST["documentType"] == 'IS') {
		$thisTitle = "Auflistung der Montageanleitungen";
	}
	else if($_REQUEST["documentType"] == 'CL') {
		$thisTitle = "Auflistung der Container-Excel-Listen";
	}
	$headerHTML = preg_replace("/{###TITLE###}/", strip_tags($thisTitle), $headerHTML);
	echo $headerHTML;

	require_once(FILE_MENUE_TOP);

	if($_REQUEST["documentType"] == 'DF') {
		$thisTitleIcon = 'documents5.png';
	}
	else if($_REQUEST["documentType"] == 'MF') {
		$thisTitleIcon = 'book.png';
	}
	else if($_REQUEST["documentType"] == 'CF') {
		$thisTitleIcon = 'documents3.png';
	}
	else if($_REQUEST["documentType"] == 'UF') {
		$thisTitleIcon = 'documents3.png';
	}
	else if($_REQUEST["documentType"] == 'DC') {
		$thisTitleIcon = 'catalogue.png';
	}
	else if($_REQUEST["documentType"] == 'IS') {
		$thisTitleIcon = 'montage.png';
	}
	else if($_REQUEST["documentType"] == 'CL') {
		$thisTitleIcon = 'ordersoverview.png';
	}
	else if($_REQUEST["documentType"] == 'PR') {
		$thisTitleIcon = 'documents5.png';
	}
?>
<div id="mainArea">
	<div id="mainContent">
		<?php require_once(FILE_MENUE_SIDEBAR); ?>
		<div id="contentArea">
			<div id="contentAreaElements">
				<h1><?php if(SHOW_PAGE_TITLE_ICONS) { echo '<img src="' . PATH_ICONS_MENUE_TITLES . $thisTitleIcon . '" alt="" />'; } ?> <?php echo $thisTitle; ?></h1>

				<?php displayMessages(); ?>

				<?php

					$formUpload = '';
					#if($_REQUEST["documentType"] == 'DF' || $_REQUEST["documentType"] == 'MF' || $_REQUEST["documentType"] == 'DC' || $_REQUEST["documentType"] == 'IS' || $_REQUEST["documentType"] == 'CL') {
					if(in_array($_REQUEST["documentType"], array('DF', 'MF', 'DC', 'IS', 'CL'))) {
						if($arrGetUserRights["uploadFiles"]) {

							// BOF DELETE UPLOADED FILE
							if($arrQueryVars["deleteFileName"] != '' && $arrQueryVars["deleteFileID"] != ''){
								if(file_exists($thisDownloadFolder . utf8_decode($arrQueryVars["deleteFileName"]))){
									$infoMessage .= ' Die Datei existiert. ' .'<br />';
									if(unlink($thisDownloadFolder . utf8_decode($arrQueryVars["deleteFileName"]))){
										$successMessage .= ' Die existierene Datei wurde aus dem Verzeichnis entfernt. ' .'<br />';
									}
									else {
										$errorMessage .= ' Die existierene Datei konnte nicht aus dem Verzeichnis entfernt werden. ' .'<br />';
									}
								}
								else {
									$errorMessage .= ' Die Datei existiert nicht. ' .'<br />';
								}
								$sql = "DELETE
													FROM `" . TABLE_DOWNLOAD_FILES . "`
													WHERE 1
														AND `downloadFilesFilePath` = '" . $thisDownloadFolder . utf8_decode($arrQueryVars["deleteFileName"]) . "'
														AND `downloadFilesID` = '" . $arrQueryVars["deleteFileID"] . "'
									";
								$rs = $dbConnection->db_query($sql);

								if($rs){
									$successMessage .= ' Die existierene Datei wurde aus der Datenbank entfernt. ' .'<br />';
								}
								else {
									$errorMessage .= ' Die existierene Datei konnte nicht aus der Datenbank entfernt werden. ' .'<br />';
								}
							}
							// EOF DELETE UPLOADED FILE

							// BOF STORE UPLOADED FILE
							if(!empty($_FILES)){
								if($_FILES["newUploadFile"]["name"] != "") {
									// convertChars
									#$thisUploadFileName = utf8_decode($_FILES["newUploadFile"]["name"]);
									$thisUploadFileName = ($_FILES["newUploadFile"]["name"]);
									$thisUploadFileName = convertChars($thisUploadFileName);
									$thisUploadFileName = $thisDownloadFolder . $thisUploadFileName;

									if(file_exists($thisUploadFileName)){
										$infoMessage .= ' Die Datei existiert schon. ' .'<br />';
										if(unlink($thisUploadFileName)){
											$successMessage .= ' Die existierene Datei wurde aus dem Verzeichnis entfernt. ' .'<br />';
											$sql = "DELETE
														FROM `" . TABLE_DOWNLOAD_FILES . "`
														WHERE `downloadFilesFilePath` = '" . $thisUploadFileName . "'
												";
											$rs = $dbConnection->db_query($sql);
											if($rs){
												$successMessage .= ' Die existierene Datei wurde aus der Datenbank entfernt. ' .'<br />';
											}
											else {
												$errorMessage .= ' Die existierene Datei konnte nicht aus der Datenbank entfernt werden. ' .'<br />';
											}
										}
										else {
											$errorMessage .= ' Die existierene Datei konnte nicht aus dem Verzeichnis entfernt werden. ' .'<br />';
										}
									}

									$rs_copyResult = copy($_FILES["newUploadFile"]["tmp_name"], $thisUploadFileName);
									#$rs_copyResult = move_uploaded_file($_FILES["newUploadFile"]["tmp_name"], "$thisUploadFileName");

									if($rs_copyResult) {
										$successMessage .= ' Die Datei wurde ins Auftragslisten-Verzeichnis kopiert. ' .'<br />';
										$thisLinkname = trim($_POST["newUploadFileLinkname"]);

										if($thisLinkname == ''){
											$thisLinkname = utf8_decode($_FILES["newUploadFile"]["name"]);
										}

										$sql = "INSERT INTO `" . TABLE_DOWNLOAD_FILES . "` (
														`downloadFilesID`,
														`downloadFilesFileName`,
														`downloadFilesFilePath`,
														`downloadFilesLinkName`,
														`downloadFilesFileSize`,
														`downloadFilesCategorie`,
														`downloadFilesSubCategorie`
													)
													VALUES (
														'%',
														'" . basename($thisUploadFileName) . "',
														'" . $thisUploadFileName . "',
														'" . ($thisLinkname) . "',
														'" . $_FILES["newUploadFile"]["size"] . "',
														'" . $_REQUEST["documentType"] . "',
														'" . $_REQUEST["newUploadFilesSubCategorie"] . "'
													)
											";
										$rs = $dbConnection->db_query($sql);
									}
									else {
										$errorMessage .= ' Die Datei konnte nicht ins Auftragslisten-Verzeichnis kopiert werden. ' .'<br />';
									}
									unlink($_FILES["newUploadFile"]["tmp_name"]);
								}
							}
							// EOF STORE UPLOADED FILE

							#<form name="formUploadFiles" method="post" action="' . $_SERVER["REQUEST_URI"] . '" enctype="multipart/form-data" >
							$formUpload = '
								<div class="adminEditArea" style="padding:0;width:860px;">
								<form name="formUploadFiles" id="formUploadFiles" method="post" action="' . $_SERVER["REDIRECT_URL"] . '#tabs-1" enctype="multipart/form-data" >
									<fieldset>
										<legend>Neue Datei hochladen</legend>
										<table border="0" width="100%" cellspacing="0" cellpadding="0">
											<tr>
												<td>
													<label for="newUploadFile">Datei: </label>
													<input type="file" name="newUploadFile" id="newUploadFile" class="inputField_260" />
												</td>

												<td>
													<label for="newUploadFileLinkname">Linktext: </label>
													<input type="text" name="newUploadFileLinkname" id="newUploadFileLinkname" class="inputField_120" />
												</td>
							';

							if(!empty($arrDownloadFileSubCategoriesData)){
								$formUpload .= '<td>';
								$formUpload .= '<label for="newUploadFilesSubCategorie">Kategorie: </label>';
								$formUpload .= '<select name="newUploadFilesSubCategorie" id="newUploadFilesSubCategorie" class="inputSelect_120">';
								$formUpload .= '<option value=""> --- Bitte w&auml;hlen --- </option>';
								foreach($arrDownloadFileSubCategoriesData as $thisSubCategoriesDataKey => $thisSubCategoriesDataValue){
									$formUpload .= '<option value="' . $thisSubCategoriesDataKey . '">' . $thisSubCategoriesDataValue["downloadFilesSubCategoriesName"] . '</option>';
								}
								$formUpload .= '</select>';
								$formUpload .= '</td>';
							}

							$formUpload .= '
												<td>
													<div class="actionButtonsArea">
														<input type="submit" name="uploadDatas" id="uploadDatas" class="inputButton0" value="Hochladen" />
													</div>
												</td>
											</tr>
										</table>
									</fieldset>
								</form>
								</div>
							';
						}
					}
				?>
				<?php
					if($_REQUEST["documentType"] != 'CF' && $_REQUEST["documentType"] != 'UF') {
						if($_REQUEST["documentType"] == 'DF') {
							$thisHeader = '<p>Anzeige wichtiger Dokumente und Formulare.</p>';
						}
						else if($_REQUEST["documentType"] == 'MF') {
							$thisHeader = '<p>Anzeige diverser Handb&uuml;cher.</p>';
						}

						else if($_REQUEST["documentType"] == 'DC') {
							$thisHeader = '<p>Anzeige der Kataloge und Preislisten.</p>';
						}

						else if($_REQUEST["documentType"] == 'IS') {
							$thisHeader = '<p>Anzeige diverser Montageanleitungen.</p>';
						}

						else if($_REQUEST["documentType"] == 'CL') {
							$thisHeader = '<p>Anzeige der Excel-Containerlisten.</p>';
						}
						echo $formUpload;
						echo $thisHeader;

						$orderByAdd = "";

						if($_REQUEST["documentType"] == 'CL') {
							$orderByAdd .= " STR_TO_DATE(SUBSTRING(`" . TABLE_DOWNLOAD_FILES . "`.`downloadFilesFileName`, 1, 10), '%d.%m.%Y') DESC, ";
						}

						$sql = "
							SELECT
									IF(`" . TABLE_DOWNLOAD_FILES_SUBCATEGORIES . "`.`downloadFilesSubCategoriesSort` IS NULL, 0, `" . TABLE_DOWNLOAD_FILES_SUBCATEGORIES . "`.`downloadFilesSubCategoriesSort`) AS `downloadFilesSubCategoriesSort`,

									`" . TABLE_DOWNLOAD_FILES . "`.`downloadFilesID`,
									`" . TABLE_DOWNLOAD_FILES . "`.`downloadFilesFileName`,
									`" . TABLE_DOWNLOAD_FILES . "`.`downloadFilesFilePath`,
									`" . TABLE_DOWNLOAD_FILES . "`.`downloadFilesLinkName`,
									`" . TABLE_DOWNLOAD_FILES . "`.`downloadFilesFileSize`,
									`" . TABLE_DOWNLOAD_FILES . "`.`downloadFilesCategorie`,
									`" . TABLE_DOWNLOAD_FILES . "`.`downloadFilesSubCategorie`,

									`" . TABLE_DOWNLOAD_FILES_SUBCATEGORIES . "`.`downloadFilesSubCategoriesName`
								FROM `" . TABLE_DOWNLOAD_FILES . "`

								LEFT JOIN `" . TABLE_DOWNLOAD_FILES_SUBCATEGORIES . "`
								ON(`" . TABLE_DOWNLOAD_FILES . "`.`downloadFilesSubCategorie` = `" . TABLE_DOWNLOAD_FILES_SUBCATEGORIES . "`.`downloadFilesSubCategoriesID`)

								WHERE 1
									AND `" . TABLE_DOWNLOAD_FILES . "`.`downloadFilesCategorie` = '" . $_REQUEST["documentType"] . "'

								ORDER BY
									" . $orderByAdd . "
									`" . TABLE_DOWNLOAD_FILES_SUBCATEGORIES . "`.`downloadFilesSubCategoriesSort`,
									`" . TABLE_DOWNLOAD_FILES . "`.`downloadFilesLinkName` ASC
							";
						#dd('sql');
						$rs = $dbConnection->db_query($sql);
						$arrDocuments = array();
						while($ds = mysqli_fetch_assoc($rs)){
							#dd('ds');
							$arrDocuments[$ds["downloadFilesSubCategorie"]][$ds["downloadFilesID"]] = array(
																		"file" => basename($ds["downloadFilesFilePath"]),
																		"text" => $ds["downloadFilesLinkName"],
																		"categorie" => $ds["downloadFilesSubCategoriesName"]
																	);
						}

						if(!empty($arrDocuments)) {
							$thisMarker = '';
							echo '<div id="tabs">';

							echo '<ul>';
								foreach($arrDocuments as $thisGroupKey => $thisGroupValue){
									echo '<li><a href="#tabs-' . $thisGroupKey . '">' . $arrDownloadFileSubCategoriesData[$thisGroupKey]["downloadFilesSubCategoriesName"]. '</a></li>';
								}
							echo '</ul>';

							$thisMarker = '';

							foreach($arrDocuments as $thisGroupKey => $thisGroupValue){
								echo '<div id="tabs-' . $thisGroupKey . '">';
								echo '<h2>' . $arrDownloadFileSubCategoriesData[$thisGroupKey]["downloadFilesSubCategoriesName"]. '</h2>';
								echo '<ul class="downloadDocuments">';
								foreach($thisGroupValue as $thisKey => $thisValue){
									$thisFileType = getFileType($thisValue["file"]);

									$thisFileNameGroup = $thisValue["text"];
									$thisFileNameGroup = preg_replace("/\(.*\)/", "", $thisFileNameGroup);

									if($thisMarker != $thisFileNameGroup && in_array($_REQUEST["documentType"], array("xxx"))){
										echo '<div class="clear"></div>';
										echo '<hr />';
										$thisMarker = $thisFileNameGroup;
									}

									echo '<li style="list-style-image: url(layout/icons/icon' . strtoupper($thisFileType) . '.gif);" >';
									echo '<p class="documentLink">';
									#echo '<img src="layout/icons/icon' . strtoupper($thisFileType) . '.gif" width="16" height="16" alt="' . strtoupper($thisFileType) . '"> ';
									echo '<a href="'. $_SERVER["REDIRECT_URL"] . '?downloadFile=' . $thisValue["file"]  . '">' . $thisValue["text"] . '</a>';
									echo '</p>';

									echo '<div class="documentToolItems">';
									echo '<span class="toolItem">';
									echo '<img src="layout/icons/mailAttachement.png" class="buttonSendDocument" rel="' . urlencode(basename($thisValue["file"])) . '#' . $_REQUEST["documentType"] . '" width="16" height="16" title="Dokument &quot;' . $arrDocumentDatas[$thisKey]["orderDocumentsNumber"] . '&quot; direkt per Mail versenden" alt="Dokument versenden" />';
									echo '</span>';
									if($arrGetUserRights["uploadFiles"]) {
										echo '<span class="toolItem">';
										echo '<a href="'. $_SERVER["REDIRECT_URL"] . '?deleteFileName=' . $thisValue["file"] . '&amp;deleteFileID=' . $thisKey . '#tabs-1" onclick="return showWarning(\'Wollen Sie diese Datei wirklich entfernen?\');">';
										echo '<img src="layout/icons/iconDelete.png" width="16" height="16" title="Diese Datei l&ouml;schen" alt="" />';
										echo '</a>';
										echo '</span>';
									}
									echo '</div>';
									echo '<div class="clear"></div>';
									echo '</li>';
								}
								echo '</ul>';
								echo '<div class="clear"></div>';
								echo '<br />';
								echo '<br />';
								echo '</div>';
							}
							echo '</div>';
						}
					}
					// ##############################################
					else if($_REQUEST["documentType"] == 'CF' || $_REQUEST["documentType"] == 'UF') {
						if($_REQUEST["documentType"] == 'CF') {
							echo '<p>Anzeige der erzeugten Dokumente und Dateien.</p>';
						}
						else if($_REQUEST["documentType"] == 'UF') {
							echo '<p>Anzeige der hochgeladenen Dokumente und Dateien.</p>';
						}
						else if($_REQUEST["documentType"] == 'KA') {
							echo '<p>Anzeige der Korrektur-Abz&uuml;ge.</p>';
						}
						// if($arrQueryVars["searchBoxFile"] == "" || $countRows < 1) {
						if(1) {
						?>
						<div id="searchFilterArea">
							<form name="formFilterSearch" method="get" action="<?php echo $_SERVER["REDIRECT_URL"]; ?>">
								<table border="0" cellpadding="0" cellspacing="0" class="searchFilterContent">
									<tr>
										<!--
										<td>
											<label for="searchCustomerNumber">Kundennummer:</label>
											<input type="text" name="searchCustomerNumber" id="searchCustomerNumber" class="inputField_40" value="" />
										</td>
										<td>
											<label for="searchCustomerName">Kundenname:</label>
											<input type="text" name="searchCustomerName" id="searchCustomerName" class="inputField_160" value="" />
										</td>
										<td>
											<label for="searchPLZ">PLZ:</label>
											<input type="text" name="searchPLZ" id="searchPLZ" class="inputField_40" />
										</td>
										-->
										<td>
											<label for="searchWord">Suchbegriff:</label>
											<input type="text" name="searchWord" id="searchWord" class="inputField_100" />
										</td>
										<td>
											<input type="submit" name="submitSearch" class="inputButton0" value="Suchen" />
										</td>
									</tr>
								</table>
							</form>
						</div>

						<?php
							}

						if(!empty($arrQueryVars["searchBoxFile"]) && $arrQueryVars["searchBoxFile"] != "") {

							$where = '';
							if($_REQUEST["documentType"] == 'CF') {
								// $where = " AND `createdDocumentsType` != 'KA' ";
							}
							else if($_REQUEST["documentType"] == 'UF') {
								// $where = " AND `createdDocumentsType` = 'KA' ";
							}

							$xxx_sql = "SELECT
											`createdDocumentsID`,
											`createdDocumentsType`,
											`createdDocumentsNumber`,
											`createdDocumentsCustomerNumber`,
											`createdDocumentsTitle`,
											`createdDocumentsFilename`,
											`createdDocumentsContent`,
											`createdDocumentsUserID`,
											`createdDocumentsTimeCreated`,
											`createdDocumentsOrderIDs`
										FROM `" . TABLE_CREATED_DOCUMENTS . "`
										WHERE (`createdDocumentsTitle` LIKE '%" . $arrQueryVars["searchBoxFile"] . "%'
												OR `createdDocumentsFilename` LIKE '%" . $arrQueryVars["searchBoxFile"] . "%'
												OR `createdDocumentsCustomerNumber` = '" . $arrQueryVars["searchBoxFile"] . "'
												OR `createdDocumentsContent` LIKE '%" . $arrQueryVars["searchBoxFile"] . "%'
											)
											AND `createdDocumentsNumber` != ''
											" . $where . "

										GROUP BY `createdDocumentsID`

										ORDER BY `createdDocumentsTimeCreated` DESC,`createdDocumentsNumber` DESC , `createdDocumentsCustomerNumber` ASC
								";

							$sql = "SELECT
											`createdDocumentsID`,
											`createdDocumentsType`,
											`createdDocumentsNumber`,
											`createdDocumentsCustomerNumber`,
											`createdDocumentsTitle`,
											`createdDocumentsFilename`,
											`createdDocumentsContent`,
											`createdDocumentsUserID`,
											`createdDocumentsTimeCreated`,
											`createdDocumentsOrderIDs`
										FROM `" . TABLE_CREATED_DOCUMENTS . "`
										WHERE (`createdDocumentsTitle` LIKE '%" . $arrQueryVars["searchBoxFile"] . "%'
												OR `createdDocumentsFilename` LIKE '%" . $arrQueryVars["searchBoxFile"] . "%'
												OR `createdDocumentsCustomerNumber` = '" . $arrQueryVars["searchBoxFile"] . "'
												OR `createdDocumentsContent` LIKE '%" . $arrQueryVars["searchBoxFile"] . "%'
											)
											AND `createdDocumentsNumber` != ''
											" . $where . "

										GROUP BY `createdDocumentsID`

									UNION

									SELECT
											`createdDocumentsID`,
											`createdDocumentsType`,
											`createdDocumentsNumber`,
											`createdDocumentsCustomerNumber`,
											`createdDocumentsTitle`,
											`createdDocumentsFilename`,
											`createdDocumentsContent`,
											`createdDocumentsUserID`,
											`createdDocumentsTimeCreated`,
											`createdDocumentsOrderIDs`
										FROM `" . TABLE_CUSTOMERS_LAYOUT_FILES . "`
										WHERE (`createdDocumentsTitle` LIKE '%" . $arrQueryVars["searchBoxFile"] . "%'
												OR `createdDocumentsFilename` LIKE '%" . $arrQueryVars["searchBoxFile"] . "%'
												OR `createdDocumentsCustomerNumber` = '" . $arrQueryVars["searchBoxFile"] . "'
												OR `createdDocumentsContent` LIKE '%" . $arrQueryVars["searchBoxFile"] . "%'
											)
											AND `createdDocumentsNumber` != ''
											" . $where . "

										GROUP BY `createdDocumentsID`

										ORDER BY `createdDocumentsTimeCreated` DESC,`createdDocumentsNumber` DESC , `createdDocumentsCustomerNumber` ASC
								";

							$rs = $dbConnection->db_query($sql);
							$countRows = $dbConnection->db_getMysqlNumRows($rs);

							if($countRows > 0) {
								$arrGetDocuments = array();
								while($ds = mysqli_fetch_assoc($rs)) {
									foreach(array_keys($ds) as $field) {
										$arrGetDocuments[$ds["createdDocumentsType"]][$ds["createdDocumentsID"]][$field] = $ds[$field];
									}
								}
							}

							if(!empty($arrGetDocuments)) {
								#dd('arrGetDocuments');
								#if(!empty($arrGetDocuments['AN'])){	$arrGetDocuments['AN'] = array_unique($arrGetDocuments['AN']); }
								#if(!empty($arrGetDocuments['AB'])){	$arrGetDocuments['AB'] = array_unique($arrGetDocuments['AB']); }
								#if(!empty($arrGetDocuments['RE'])){	$arrGetDocuments['RE'] = array_unique($arrGetDocuments['RE']); }
								#if(!empty($arrGetDocuments['LS'])){	$arrGetDocuments['LS'] = array_unique($arrGetDocuments['LS']); }
								#if(!empty($arrGetDocuments['GU'])){	$arrGetDocuments['GU'] = array_unique($arrGetDocuments['GU']); }
								#if(!empty($arrGetDocuments['BR'])){	$arrGetDocuments['BR'] = array_unique($arrGetDocuments['BR']); }
								#if(!empty($arrGetDocuments['PR'])){	$arrGetDocuments['PR'] = array_unique($arrGetDocuments['PR']); }

								echo '<div id="tabs">';
								echo '<ul>';
								foreach($arrGetDocuments as $thisDocumentTypeKey => $thisDocumentTypeValue) {
									echo '<li><a href="#tabs-' . $thisDocumentTypeKey . '">' . $arrDocumentTypeDatas[$thisDocumentTypeKey]["createdDocumentsTypesName2"] . '</a></li>';
								}
								echo '</ul>';

								$thisMarker = '';
								foreach($arrGetDocuments as $thisDocumentTypeKey => $thisDocumentTypeValue) {
									echo '<div id="tabs-' . $thisDocumentTypeKey . '">';
									echo '<h2>' . $arrDocumentTypeDatas[$thisDocumentTypeKey]["createdDocumentsTypesName2"] . '</h2>';
									echo '<table border="0" cellpadding="0" cellspacing="0" width="100%" class="displayOrders">';
									echo '<tr>';
									echo '<th style="width:45px;text-align:right;">#</th>';
									#echo '<th>Typ</th>';
									if($thisDocumentValue["createdDocumentsType"] != 'KA'){
										echo '<th>Nummer</th>';
									}
									echo '<th>K-Nr</th>';
									#echo '<th>Titel</th>';
									echo '<th>Erstelldatum</th>';
									echo '<th>Dateiname</th>';
									echo '<th>Info</th>';
									echo '</tr>';

									$count = 0;

									foreach($thisDocumentTypeValue as $thisDocumentValue) {
										if($thisMarker != $thisDocumentValue["createdDocumentsNumber"]) {
											$thisMarker = $thisDocumentValue["createdDocumentsNumber"];

											if($count%2 == 0){ $rowClass = 'row0'; }
											else { $rowClass = 'row1'; }

											echo '<tr class="'.$rowClass.'">';

											echo '<td style="text-align:right;"><b>' . ($count + 1) . '.</b></td>';
											#echo '<td title="' .$arrDocumentTypeDatas[$thisDocumentTypeKey]["createdDocumentsTypesName"] . '" style="cursor:pointer;">' .$arrDocumentTypeDatas[$thisDocumentTypeKey]["createdDocumentsTypesShortName"] . '</td>';

											if($thisDocumentValue["createdDocumentsType"] != 'KA'){
												echo '<td>';
												echo '<span title="' .$arrDocumentTypeDatas[$thisDocumentTypeKey]["createdDocumentsTypesName"] . '">';

												$thisLinkPage = PAGE_ALL_INVOICES;
												if(in_array($thisDocumentValue["createdDocumentsType"], array('AB', 'RE', 'GU', 'MA', 'M1', 'M2'))){
													if(defined("PAGE_ALL_" . $thisDocumentValue["createdDocumentsType"])){
														$thisLinkPage = constant("PAGE_ALL_" . $thisDocumentValue["createdDocumentsType"]);
													}
													else {
														$thisLinkPage = PAGE_ALL_INVOICES;
													}
												}
												else if(in_array($thisDocumentValue["createdDocumentsType"], array('BR', 'AN', 'LS'))){
													$thisLinkPage = constant("PAGE_DISPLAY_" . $thisDocumentValue["createdDocumentsType"]);
												}

												echo ' <a href="' . $thisLinkPage . '?searchDocumentNumber=' . $thisDocumentValue["createdDocumentsNumber"] . '">';
												echo $thisDocumentValue["createdDocumentsNumber"];
												echo '</a>';
												echo '</span>';
												echo '</td>';
											}
											echo '<td>';
											if($arrGetUserRights["editCustomers"] || $arrGetUserRights["displayCustomers"]) {
												#echo '<a href="' . PAGE_EDIT_CUSTOMER . '?editCustomerNumber=' . $thisDocumentValue["createdDocumentsCustomerNumber"] . '" title="Kunden anzeigen">';
												echo '<a href="' . getCustomersSourceTableFromCustomerNumber($thisDocumentValue["createdDocumentsCustomerNumber"]) . '?editCustomerNumber=' . $thisDocumentValue["createdDocumentsCustomerNumber"] . '" title="Kunden anzeigen">';
											}
											echo $thisDocumentValue["createdDocumentsCustomerNumber"];
											if($arrGetUserRights["editCustomers"] || $arrGetUserRights["displayCustomers"]) {
												echo '</a>';
											}
											echo '</td>';
											#echo '<td>' . $thisDocumentValue["createdDocumentsTitle"] . '</td>';
											echo '<td>' . substr(formatDate($thisDocumentValue["createdDocumentsTimeCreated"], 'display'), 0, 10) . '</td>';
											echo '<td>' . basename($thisDocumentValue["createdDocumentsFilename"]) . '</td>';
											echo '<td>';
											echo '<span class="toolItem">';
											echo '<a href="' . $_SERVER["REDIRECT_URL"] . '?downloadFile=' . urlencode(basename($thisDocumentValue["createdDocumentsFilename"])) . '&amp;thisDocumentType2=' . $thisDocumentValue["createdDocumentsType"] . '"><img src="layout/icons/icon' . getFileType(basename($thisDocumentValue["createdDocumentsFilename"])) . '.gif" width="16" height="16" title="Dokument herunterladen" alt="Download" /></a>';
											echo '</span>';
											echo '<span class="toolItem">';
											echo '<img src="layout/icons/mailAttachement.png" class="buttonSendDocument" rel="' . urlencode(basename($thisDocumentValue["createdDocumentsFilename"])) . '#' . $thisDocumentValue["createdDocumentsType"] . '" width="16" height="16" title="Dokument direkt per Mail versenden" alt="Dokument versenden" />';
											echo '</span>';
											echo '</td>';
											echo '</tr>';

											$count++;
										}
									}
									echo '</table>';
									echo '</div>';

								}
								echo '</div>';
							}
							else {
								$errorMessage .= ' Es konnten wurden keine Dokumente zum Suchbegriff &quot;' . $arrQueryVars["searchBoxFile"] . '&quot; gefunden. ' . '<br />';
							}
						}
						else {

						}
					}
				?>
				<div class="clear"></div>
				<?php displayMessages(); ?>
			</div>
		</div>
		<div class="clear"></div>
	</div>
	<div class="clear"></div>
</div>

<script language="javascript" type="text/javascript">
	$(document).ready(function() {
		$(function() {
			$( "#datepicker" ).datepicker();
			colorRowMouseOver('.displayOrders tbody tr');
			$('#tabs').tabs();
		});
		$('.buttonSendDocument').click(function() {
			var mailDocumentDatas = $(this).attr('rel');
			sendAttachedDocument($(this), mailDocumentDatas, '<?php echo $thisDocumentValue[$thisKey]["orderDocumentsCustomerNumber"]; ?>', '<?php echo $_REQUEST["editID"]; ?>', '', '<?php echo $_SERVER["REQUEST_URI"]; ?>', '');
		});
		$(function() {
			$('#tabs').tabs();

			$('.ui-tabs-nav li a').click(function(){
				var thisTabHref = $(this).attr('href');
				$('#formUploadFiles').attr('action', '<?php echo $_SERVER["REDIRECT_URL"]; ?>' + thisTabHref);
			});
		});
	});
</script>
<?php require_once('inc/footerHTML.inc.php'); ?>