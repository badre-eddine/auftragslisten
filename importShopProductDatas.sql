SELECT

	`tempTable`.`categories_name`,
	`tempTable`.`sort_order`,
	`tempTable`.`products_name`,
	`tempTable`.`products_model`,
	`tempTable`.`attributes_model`,
	`tempTable`.`products_prices`,


	`tempTable`.`categories_id`,
	`tempTable`.`language_id`,

	`tempTable`.`parent_id`,
	`tempTable`.`categories_status`,

	`tempTable`.`products_id`,
	`tempTable`.`products_ean`,
	`tempTable`.`products_quantity`,
	`tempTable`.`products_ebay_quantity`,
	`tempTable`.`products_shippingtime`,
	`tempTable`.`group_permission_0`,
	`tempTable`.`group_permission_1`,
	`tempTable`.`group_permission_2`,
	`tempTable`.`group_permission_3`,
	`tempTable`.`products_sort`,
	`tempTable`.`products_image`,
	`tempTable`.`products_price`,
	`tempTable`.`products_ek_price`,
	`tempTable`.`products_discount_allowed`,
	`tempTable`.`products_date_added`,
	`tempTable`.`products_last_modified`,
	`tempTable`.`products_date_available`,
	`tempTable`.`products_weight`,
	`tempTable`.`products_status`,
	`tempTable`.`free_shipping`,
	`tempTable`.`max_free_shipping_amount`,
	`tempTable`.`max_free_shipping_cart`,
	`tempTable`.`products_tax_class_id`,

	`tempTable`.`manufacturers_id`,
	`tempTable`.`products_ordered`,
	`tempTable`.`products_fsk18`,
	`tempTable`.`products_vpe`,
	`tempTable`.`products_vpe_status`,
	`tempTable`.`products_vpe_value`,

	`tempTable`.`nc_ultra_shipping_costs`,

	`tempTable`.`products_sperrgut`,

	`tempTable`.`products_videos`,

	`tempTable`.`products_description`,
	`tempTable`.`products_short_description`,

	`tempTable`.`products_viewed`,

	`tempTable`.`products_attributes_id`,
	`tempTable`.`options_id`,
	`tempTable`.`options_values_id`,
	`tempTable`.`options_values_price`,
	`tempTable`.`options_values_ek_price`,
	`tempTable`.`price_prefix`,
	`tempTable`.`attributes_stock`,
	`tempTable`.`options_values_weight`,
	`tempTable`.`weight_prefix`,
	`tempTable`.`sortorder`,

	`tempTable`.`price_id`

	FROM (

		SELECT
			`categories_description`.`categories_name`,
			`categories`.`sort_order`,
			`products_description`.`products_name`,
			`products`.`products_model`,
			`products_attributes`.`attributes_model`,
			CONCAT(
				`personal_offers_by_customers_status_3`.`quantity`,
				':',
				`personal_offers_by_customers_status_3`.`personal_offer`
			) AS `products_prices`,


			`categories_description`.`categories_id`,
			`categories_description`.`language_id`,

			`categories`.`parent_id`,
			`categories`.`categories_status`,

			`products`.`products_id`,
			`products`.`products_ean`,
			`products`.`products_quantity`,
			`products`.`products_ebay_quantity`,
			`products`.`products_shippingtime`,
			`products`.`group_permission_0`,
			`products`.`group_permission_1`,
			`products`.`group_permission_2`,
			`products`.`group_permission_3`,
			`products`.`products_sort`,
			`products`.`products_image`,
			`products`.`products_price`,
			`products`.`products_ek_price`,
			`products`.`products_discount_allowed`,
			`products`.`products_date_added`,
			`products`.`products_last_modified`,
			`products`.`products_date_available`,
			`products`.`products_weight`,
			`products`.`products_status`,
			`products`.`free_shipping`,
			`products`.`max_free_shipping_amount`,
			`products`.`max_free_shipping_cart`,
			`products`.`products_tax_class_id`,

			`products`.`manufacturers_id`,
			`products`.`products_ordered`,
			`products`.`products_fsk18`,
			`products`.`products_vpe`,
			`products`.`products_vpe_status`,
			`products`.`products_vpe_value`,

			`products`.`nc_ultra_shipping_costs`,

			`products`.`products_sperrgut`,

			`products`.`products_videos`,

			`products_description`.`products_description`,
			`products_description`.`products_short_description`,

			`products_description`.`products_viewed`,

			`products_attributes`.`products_attributes_id`,
			`products_attributes`.`options_id`,
			`products_attributes`.`options_values_id`,
			`products_attributes`.`options_values_price`,
			`products_attributes`.`options_values_ek_price`,
			`products_attributes`.`price_prefix`,
			`products_attributes`.`attributes_stock`,
			`products_attributes`.`options_values_weight`,
			`products_attributes`.`weight_prefix`,
			`products_attributes`.`sortorder`,

			`personal_offers_by_customers_status_3`.`price_id`

		FROM `categories_description`

		INNER JOIN `categories`
		ON(`categories_description`.`categories_id` = `categories`.`categories_id`)

		LEFT JOIN `products_to_categories`
		ON(`categories_description`.`categories_id` = `products_to_categories`.`categories_id`)

		LEFT JOIN `products`
		ON(`products_to_categories`.`products_id` = `products`.`products_id`)

		LEFT JOIN `products_description`
		ON(`products`.`products_id` = `products_description`.`products_id`)

		LEFT JOIN `products_attributes`
		ON(`products`.`products_id` = `products_attributes`.`products_id` AND `products_attributes`.`options_id` = 1)

		LEFT JOIN `personal_offers_by_customers_status_3`
		ON(`products`.`products_id` = `personal_offers_by_customers_status_3`.`products_id` AND `personal_offers_by_customers_status_3`.`personal_offer` > 0)

		WHERE 1
			AND `categories_description`.`categories_name` NOT LIKE '\_%'
			AND `categories_description`.`categories_name` NOT LIKE '{#%'
			AND `categories_description`.`language_id` = 2
			AND `categories`.`sort_order` > 99

		ORDER BY
			`categories`.`sort_order`,
			`categories_description`.`categories_name`,
			`products_description`.`products_name`,
			`personal_offers_by_customers_status_3`.`quantity`

	) AS `tempTable`


-- ###############################################################
-- ###############################################################
-- ###############################################################

-- BOF GET CATEGORIES
	SELECT
			`categories_description`.`categories_name`,
			`categories`.`sort_order`,
			`categories_description`.`categories_id`,
			`categories_description`.`language_id`,
			`categories`.`parent_id`,
			`categories`.`categories_status`

		FROM `categories_description`

		INNER JOIN `categories`
		ON(`categories_description`.`categories_id` = `categories`.`categories_id`)

		WHERE 1
			AND `categories_description`.`categories_name` NOT LIKE '\_%'
			AND `categories_description`.`categories_name` NOT LIKE '{#%'
			AND `categories_description`.`language_id` = 2
			AND `categories`.`sort_order` > 99

		ORDER BY
			`categories`.`sort_order`,
			`categories_description`.`categories_name`
-- EOF GET CATEGORIES

-- ###############################################################
-- ###############################################################
-- ###############################################################


SELECT
			`categories_description`.`categories_name`,
			`categories`.`sort_order`,
			`products_description`.`products_name`,
			`products`.`products_model`,
			GROUP_CONCAT(
				`products_attributes`.`attributes_model`
				SEPARATOR '#'
			) AS `products_attributes_model`,
			GROUP_CONCAT(
				`personal_offers_by_customers_status_3`.`quantity`,
				':',
				`personal_offers_by_customers_status_3`.`personal_offer`
				SEPARATOR '#'
			) AS `products_prices`,


			`categories_description`.`categories_id`,
			`categories_description`.`language_id`,

			`categories`.`parent_id`,
			`categories`.`categories_status`,

			`products`.`products_id`,
			`products`.`products_ean`,
			`products`.`products_quantity`,
			`products`.`products_ebay_quantity`,
			`products`.`products_shippingtime`,
			`products`.`group_permission_0`,
			`products`.`group_permission_1`,
			`products`.`group_permission_2`,
			`products`.`group_permission_3`,
			`products`.`products_sort`,
			`products`.`products_image`,
			`products`.`products_price`,
			`products`.`products_ek_price`,
			`products`.`products_discount_allowed`,
			`products`.`products_date_added`,
			`products`.`products_last_modified`,
			`products`.`products_date_available`,
			`products`.`products_weight`,
			`products`.`products_status`,
			`products`.`free_shipping`,
			`products`.`max_free_shipping_amount`,
			`products`.`max_free_shipping_cart`,
			`products`.`products_tax_class_id`,

			`products`.`manufacturers_id`,
			`products`.`products_ordered`,
			`products`.`products_fsk18`,
			`products`.`products_vpe`,
			`products`.`products_vpe_status`,
			`products`.`products_vpe_value`,

			`products`.`nc_ultra_shipping_costs`,

			`products`.`products_sperrgut`,

			`products`.`products_videos`,
			`products`.`products_image`,

			`products_description`.`products_description`,
			`products_description`.`products_short_description`,

			`products_description`.`products_viewed`,

			`products_attributes`.`products_attributes_id`,
			`products_attributes`.`options_id`,
			`products_attributes`.`options_values_id`,
			`products_attributes`.`options_values_price`,
			`products_attributes`.`options_values_ek_price`,
			`products_attributes`.`price_prefix`,
			`products_attributes`.`attributes_stock`,
			`products_attributes`.`options_values_weight`,
			`products_attributes`.`weight_prefix`,
			`products_attributes`.`sortorder`,

			`personal_offers_by_customers_status_3`.`price_id`

		FROM `categories_description`

		INNER JOIN `categories`
		ON(`categories_description`.`categories_id` = `categories`.`categories_id`)

		LEFT JOIN `products_to_categories`
		ON(`categories_description`.`categories_id` = `products_to_categories`.`categories_id`)

		LEFT JOIN `products`
		ON(`products_to_categories`.`products_id` = `products`.`products_id`)

		LEFT JOIN `products_description`
		ON(`products`.`products_id` = `products_description`.`products_id`)

		LEFT JOIN `products_attributes`
		ON(`products`.`products_id` = `products_attributes`.`products_id` AND `products_attributes`.`options_id` = 1)

		LEFT JOIN `personal_offers_by_customers_status_3`
		ON(`products`.`products_id` = `personal_offers_by_customers_status_3`.`products_id` AND `personal_offers_by_customers_status_3`.`personal_offer` > 0)

		WHERE 1
			AND `categories_description`.`categories_name` NOT LIKE '\_%'
			AND `categories_description`.`categories_name` NOT LIKE '{#%'
			AND `categories_description`.`language_id` = 2
			AND `categories`.`sort_order` > 99

		GROUP BY `products`.`products_id`

		ORDER BY
			`categories`.`sort_order`,
			`categories_description`.`categories_name`,
			`products_description`.`products_name`,
			`personal_offers_by_customers_status_3`.`quantity`