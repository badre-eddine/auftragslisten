<?php

	DEFINE('SET_DATE_DEFAULT_TIMEZONE', 'Europe/Berlin');
	DEFINE('LC_ALL', '"de_DE@euro", "de_DE", "de", "ge"');

	// DELETE FROM `bctr_createddocuments` WHERE `createdDocumentsType` = 'KA';

	date_default_timezone_set(SET_DATE_DEFAULT_TIMEZONE);
	setlocale(LC_ALL, constant("LC_ALL"));
	setlocale(LC_TIME, constant("LC_ALL"));

	$countFiles = 0;
	$arrNumberFiles = array();

	function read_dir($dir) {
		global $fp, $countFiles, $arrNumberFiles;
		if(preg_match("/\/$/", $dir)) {
			$dir = substr($dir, 0, (strlen($dir) - 1));
		}
		$array = array();
		$d = dir($dir);

		$contentStart = 'INSERT IGNORE INTO `bctr_createddocuments` (`createdDocumentsID`,`createdDocumentsType`,`createdDocumentsNumber`,`createdDocumentsCustomerNumber`,`createdDocumentsTitle`,`createdDocumentsFilename`,`createdDocumentsContent`,`createdDocumentsUserID`,`createdDocumentsTimeCreated`,`createdDocumentsOrderIDs`) VALUES ';

		while (false !== ($entry = $d->read())) {

			if($entry!='.' && $entry!='..')
			{
				$entry = $dir.'/'.$entry;

				$filePath = addslashes($entry);
				$fileName = basename(stripslashes($filePath));

				#echo "1:filePath: ".$filePath."<br>";
				#echo "fileName: ".$fileName."<br>";

				if(is_dir($entry))
				{
					$array[] = $entry;
					$array = array_merge($array, read_dir($entry));
				}
				else
				{
					$array[] = $entry;

					if(preg_match("/\.pdf$/", $fileName)) {

						$arrFileName = explode("_", $fileName);
						$thisFileTime = filemtime(stripslashes($filePath));

						$countFiles++;
						$arrNumberFiles[date("ym", $thisFileTime)][] = $countFiles;

						$thisDocumentNumber = "KA-" . date("ym", $thisFileTime) . str_repeat("0", (6 - strlen(count($arrNumberFiles[date("ym", $thisFileTime)])))) . count($arrNumberFiles[date("ym", $thisFileTime)]);

						$content = "
							'%',
							'KA',
							'" . $thisDocumentNumber . "',
							'" . $arrFileName[0] . "',
							'" . $thisDocumentNumber . "',
							'" . addslashes($fileName) . "',
							'',
							'0',
							'" . date("Y-m-d H:i:s", $thisFileTime) . "',
							''
						";

						$contentLine = $contentStart . "(" . $content . ");";
						$contentLine = preg_replace("/\n/", "", $contentLine);
						$contentLine = preg_replace("/\r/", "", $contentLine);
						$contentLine = preg_replace("/\t/", "", $contentLine);
						$contentLine .= "\n";
						echo $countFiles . " | " . $thisDocumentNumber . " | " . count($arrNumberFiles[date("ym", $thisFileTime)]) . "<br />";

						fwrite($fp, $contentLine);
					}
				}
				clearstatcache();
			}
		}
		$d->close();
		return $array;
	}
	if(file_exists("getPrintFiles.sql")){
		unlink("getPrintFiles.sql");
	}
	$fp = fopen("getPrintFiles.sql", "a+");
	echo 'read_dir<pre>';
	read_dir("documents_bctr/documentsCustomerUpload");
	echo '</pre>';
	fclose($fp);
?>