<?php
	require_once('inc/requires.inc.php');
	require_once('classes/createImage.class.php');

	if(!$arrGetUserRights["editCustomers"] && !$arrGetUserRights["displayCustomers"]) {
		header('location: ' . PAGE_EXIT_LOCATION);
		exit;
	}

	$dbConnection = new DB_Connection();
	$db_open = $dbConnection->db_connect();

	$jswindowMessage = '';
	$warningMessage = '';
	$errorMessage = '';
	$successMessage = '';
	$infoMessage = '';

	if($_REQUEST["searchStatus"] == ''){
		$_REQUEST["searchStatus"] = 1;
	}
	if($_REQUEST["searchSort"] == ''){
		$_REQUEST["searchSort"] = 1;
	}

	// BOF DEFINE PREVIEW IMAGE GENERATE PARAMS
		$previewImageDpi				= 96;
		$previewImageWidth				= 800;
		$previewImageHeight				= 270;
		$previewImageStartX				= 0;
		$previewImageStartY				= 160;
		$previewImagePdfSourcePageNum	= 0;
	// EOF DEFINE PREVIEW IMAGE GENERATE PARAMS

	// BOF READ COUNTRIES
		$arrCountryTypeDatas = getCountryTypes();
	// EOF READ COUNTRIES

	// BOF DOWNLOAD FILE
		if($_GET["downloadFile"] != "" && $_GET["documentType"] != "") {

			if($_GET["documentType"] == 'KA') {
				$fileDirectory = DIRECTORY_PRINT_PLATE_FILES . '';
			}
			else if($_GET["documentType"] == 'PP') {
				$fileDirectory = DIRECTORY_PRINT_PLATE_FILES . '';
			}

			$_GET["downloadFile"] = utf8_decode(urldecode($_GET["downloadFile"]));

			require_once('classes/downloadFile.class.php');
			$thisDownload = new downloadFile($fileDirectory, $_GET["downloadFile"]);
			$errorMessage .= $thisDownload->startDownload();
		}
	// EOF DOWNLOAD FILE

	function get_pdf_dimensions($path, $box="MediaBox") {
		//$box can be set to BleedBox, CropBox or MediaBox

		$stream = new SplFileObject($path);

		$result = false;

		while (!$stream->eof()) {
			if (preg_match("/".$box."\[[0-9]{1,}.[0-9]{1,} [0-9]{1,}.[0-9]{1,} ([0-9]{1,}.[0-9]{1,}) ([0-9]{1,}.[0-9]{1,})\]/", $stream->fgets(), $matches)) {
				$result["width"] = $matches[1];
				$result["height"] = $matches[2];
				break;
			}
		}

		$stream = null;

		return $result;
	}

	// BOF CHANGE STATUS
		if($_REQUEST["cancelDatas"] != ''){
			// unset($_REQUEST["storePrintingPlatesDataID"]);
			unset($_REQUEST["editPrintingPlatesDataID"]);
		}
	// EOF CHANGE STATUS

	// BOF CHANGE STATUS
		if($_REQUEST["changeStatusDataID"] != '' && $_REQUEST["changeStatusNew"] != ''){
			$sql_changeDataStatus = "
					UPDATE `" . TABLE_PRINTING_PLATES_DATA . "`

					SET `printingPlatesDataActive` = '" . $_REQUEST["changeStatusNew"] . "'
					WHERE 1
						AND `printingPlatesDataID` = '" . $_REQUEST["changeStatusDataID"] . "'
				";
			$rs_changeDataStatus = $dbConnection->db_query($sql_changeDataStatus, $db_open);
			if($rs_changeDataStatus){
				$successMessage .= 'Der Status der Daten wurde ge&auml;ndert.' . '<br />';
			}
			else{
				$errorMessage .= 'Der Status der Daten konnten nicht  ge&auml;ndert werden.' . '<br />';
			}
		}
	// EOF CHANGE STATUS

	// BOF STORE DATAS AND FILES
		if($_REQUEST["storeDatas"] != ""){

			set_time_limit(0);

			// BOF STORE DATAS
				$thisStoreID = $_REQUEST["storePrintingPlatesDataID"];
				if($thisStoreID == 'NEW'){
					$thisStoreID = '%';
				}
				else{
					// BOF DELETE FILES IF DATAS ARE UPDATED
						// BOF GET DATA TO DELETE
							$sql_getDataToDelete = "
									SELECT
										`printingPlatesDataID`,
										`printingPlatesDataCustomerNumber`,
										`printingPlatesDataPlateNumber`,
										`printingPlatesDataPrintText`,
										`printingPlatesDataLayoutPdfFileName`,
										`printingPlatesDataLayoutThumbnailFileName`,
										`printingPlatesDataLayoutDataFileName`,
										`printingPlatesDataImage`,
										`printingPlatesDataUploadDate`,
										`printingPlatesDataActive`,
										`printingPlatesDataModifiedDate`
										FROM `" . TABLE_PRINTING_PLATES_DATA . "`

										WHERE 1
											AND `printingPlatesDataID` = '" . $thisStoreID . "'
										LIMIT 1
								";
							$rs_getDataToDelete = $dbConnection->db_query($sql_getDataToDelete, $db_open);
							// dd('sql_getDataToDelete');
							$arrDatasToDelete = array();
							while($ds_getDataToDelete = mysqli_fetch_assoc($rs_getDataToDelete)){
								$arrDatasToDelete = $ds_getDataToDelete;
							}
						// EOF GET DATA TO DELETE

						// BOF DELETE PREVIEW IMAGE
							if($_FILES["uploadPrintingPlatesDataLayoutPdfFile"]["name"] != ""){
								if($arrDatasToDelete["printingPlatesDataLayoutThumbnailFileName"] != ""){
									$printingPlatesDataLayoutThumbnailFilePath = DIRECTORY_LAYOUT_PREVIEW_IMAGES . $arrDatasToDelete["printingPlatesDataLayoutThumbnailFileName"];
									if(file_exists($printingPlatesDataLayoutThumbnailFilePath)){
										$rs_unlink = unlink($printingPlatesDataLayoutThumbnailFilePath);
									}
								}
							}
						// EOF DELETE PREVIEW IMAGE

						// BOF DELETE PDF LAYOUT FILE
							if($_FILES["uploadPrintingPlatesDataLayoutPdfFile"]["name"] != ""){
								if($arrDatasToDelete["printingPlatesDataLayoutPdfFileName"] != ""){
									$printingPlatesDataLayoutPdfFilePath = DIRECTORY_PRINT_PLATE_FILES . $arrDatasToDelete["printingPlatesDataLayoutPdfFileName"];
									if(file_exists($printingPlatesDataLayoutPdfFilePath)){
										$rs_unlink = unlink($printingPlatesDataLayoutPdfFilePath);
									}
								}
							}
						// EOF DELETE PDF LAYOUT FILE

						// BOF DELETE DATA FILES
							if($_FILES["uploadPrintingPlatesDataLayoutDataFile"]["name"] != ""){
								if($arrDatasToDelete["printingPlatesDataLayoutDataFileName"] != ""){
									$printingPlatesDataLayoutDataFilePath = DIRECTORY_PRINT_PLATE_FILES . $arrDatasToDelete["printingPlatesDataLayoutDataFileName"];
									if(file_exists($printingPlatesDataLayoutDataFilePath)){
										$rs_unlink = unlink($printingPlatesDataLayoutDataFilePath);
									}
								}
							}
						// EOF DELETE DATA FILES

					// EOF DELETE FILES IF DATAS ARE UPDATED
				}

				$thisPrintingPlatesDataLayoutPdfFileName		= $_REQUEST["editPrintingPlatesDataLayoutPdfFileName"];
				$thisPrintingPlatesDataLayoutThumbnailFileName	= $_REQUEST["editPrintingPlatesDataLayoutThumbnailFileName"];
				$thisPrintingPlatesDataLayoutDataFileName		= $_REQUEST["editPrintingPlatesDataLayoutDataFileName"];

				if(!empty($_FILES)){
					if($_FILES["uploadPrintingPlatesDataLayoutPdfFile"]["name"] != ""){
						$thisPrintingPlatesDataLayoutPdfFileType	= strtolower(pathinfo($_FILES["uploadPrintingPlatesDataLayoutPdfFile"]["name"], PATHINFO_EXTENSION));
						if(in_array($thisPrintingPlatesDataLayoutPdfFileType, array('pdf'))){
							$thisPrintingPlatesDataLayoutPdfFileName	= convertChars($_FILES["uploadPrintingPlatesDataLayoutPdfFile"]["name"]);
							$thisPrintingPlatesDataLayoutThumbnailFileName	= pathinfo($thisPrintingPlatesDataLayoutPdfFileName, PATHINFO_FILENAME) . ".png";
						}
						else {
							$jswindowMessage .= 'Die Layout-Datei muss im Format PDF hochgeladen werden!' . "\\n";
							$errorMessage .= 'Die Layout-Datei muss im Format PDF hochgeladen werden!' . "<br />";
						}
					}
					if($_FILES["uploadPrintingPlatesDataLayoutDataFile"]["name"] != ""){
						$thisPrintingPlatesDataLayoutDataFileType	= strtolower(pathinfo($_FILES["uploadPrintingPlatesDataLayoutDataFile"]["name"], PATHINFO_EXTENSION));
						if(in_array($thisPrintingPlatesDataLayoutDataFileType, array('zip', 'rar'))){
							$thisPrintingPlatesDataLayoutDataFileName	= convertChars($_FILES["uploadPrintingPlatesDataLayoutDataFile"]["name"]);
						}
						else {
							/*
							$jswindowMessage = '';
							$warningMessage = '';
							$errorMessage = '';
							*/
							$jswindowMessage .= 'Die Daten-Datei muss im Format ZIP oder RAR hochgeladen werden!' . "\\n";
							$errorMessage .= 'Die Daten-Datei muss im Format ZIP oder RAR hochgeladen werden!' . "<br />";
						}
					}
				}
				if($_REQUEST["editPrintingPlatesDataPlateNumber"] != ''){
					$thisPrintingPlatesDataPlateNumber = $_REQUEST["editPrintingPlatesDataPlateNumber"];
				}
				else {
					// $thisPrintingPlatesDataPlateNumber = 'NULL';
					$thisPrintingPlatesDataPlateNumber = '';
				}
				$sql_insert = "
						REPLACE INTO `" . TABLE_PRINTING_PLATES_DATA . "` (
													`printingPlatesDataID`,
													`printingPlatesDataCustomerNumber`,
													`printingPlatesDataPlateNumber`,
													`printingPlatesDataPrintText`,
													`printingPlatesDataLayoutPdfFileName`,
													`printingPlatesDataLayoutThumbnailFileName`,
													`printingPlatesDataLayoutDataFileName`,
													`printingPlatesDataImage`,
													`printingPlatesDataUploadDate`,
													`printingPlatesDataActive`,
													`printingPlatesDataModifiedDate`
												)
												VALUES (
													'" . $thisStoreID . "',
													'" . $_REQUEST["editPrintingPlatesDataCustomerNumber"] . "',
													'" . $thisPrintingPlatesDataPlateNumber . "',
													'" . $_REQUEST["editPrintingPlatesDataPrintText"] . "',
													'" . $thisPrintingPlatesDataLayoutPdfFileName . "',
													'" . $thisPrintingPlatesDataLayoutThumbnailFileName . "',
													'" . $thisPrintingPlatesDataLayoutDataFileName . "',
													'" . $_REQUEST["editPrintingPlatesDataImage"] . "',
													'" . formatDate($_REQUEST["editPrintingPlatesDataUploadDate"], 'store') . "',
													'" . $_REQUEST["editPrintingPlatesDataActive"] . "',
													NOW()
												)
					";
				$rs_insert = $dbConnection->db_query($sql_insert, $db_open);
				// dd('sql_insert');
				if($rs_insert){
					$successMessage .= 'Die Daten wurden gespeichert!' . '<br />';
				}
				else{
					$errorMessage .= 'Die Daten konnten nicht gespeichert werden.' . '<br />';
				}

				if($thisStoreID == '%'){
					$_REQUEST["editPrintingPlatesDataID"] = $dbConnection->db_getInsertID();
				}
			// EOF STORE DATAS

			// BOF STORE FILES
				if(!empty($_FILES)){
					// BOF CREATE PREVIEW IMAGE AND STORE PDF-LAYOUT FILE
						if($_FILES["uploadPrintingPlatesDataLayoutPdfFile"]["name"] != ""){
							if(in_array($thisPrintingPlatesDataLayoutPdfFileType, array('pdf'))){
								$thisThumbnailImagePath = DIRECTORY_LAYOUT_PREVIEW_IMAGES . $thisPrintingPlatesDataLayoutThumbnailFileName;
								$thisLayoutPdfPath = $_FILES["uploadPrintingPlatesDataLayoutPdfFile"]["tmp_name"];

								// BOF STORE PDF-LAYOUT FILE
									$sourceFilePath = $_FILES["uploadPrintingPlatesDataLayoutPdfFile"]["tmp_name"];
									$targetFileName = $thisPrintingPlatesDataLayoutPdfFileName;
									$targetFilePath = DIRECTORY_PRINT_PLATE_FILES . $targetFileName;

									$rs_copy = copy($sourceFilePath, $targetFilePath);
									if($rs_copy){
										$successMessage .= 'Die Layout-Datei &quot;' . basename($targetFilePath) . '&quot; wurde gespeichert!' . '<br />';
									}
									else{
										$errorMessage .= 'Die Layout-Datei &quot;' . basename($targetFilePath) . '&quot; konnten nicht gespeichert werden.' . '<br />';
									}
									if(file_exists($sourceFilePath)){
										unlink($sourceFilePath);
									}

								// EOF STORE PDF-LAYOUT FILE

								// BOF CREATE PREVIEW IMAGE
									if(file_exists($thisThumbnailImagePath)){
										unlink($thisThumbnailImagePath);
									}

									$createThumbnail = new createImage(BASEPATH . $targetFilePath, BASEPATH . $thisThumbnailImagePath, $previewImageDpi, $previewImageWidth, $previewImageHeight, $previewImageStartX, $previewImageStartY);
								// EOF CREATE PREVIEW IMAGE

								if(file_exists($sourceFilePath)){
									unlink($sourceFilePath);
								}
							}
						}
					// EOF CREATE PREVIEW IMAGE AND STORE PDF-LAYOUT FILE

					// BOF STORE DATA FILE
						if($_FILES["uploadPrintingPlatesDataLayoutDataFile"]["name"] != ""){
							if(in_array($thisPrintingPlatesDataLayoutDataFileType, array('zip', 'rar'))){
								$sourceFilePath = $_FILES["uploadPrintingPlatesDataLayoutDataFile"]["tmp_name"];
								$targetFileName = $thisPrintingPlatesDataLayoutDataFileName;
								$targetFilePath = DIRECTORY_PRINT_PLATE_FILES . $targetFileName;

								if(file_exists($targetFilePath)){
									unlink($targetFilePath);
								}

								$rs_copy = copy($sourceFilePath, $targetFilePath);
								if($rs_copy){
									$successMessage .= 'Die Daten-Datei &quot;' . basename($targetFilePath) . '&quot; wurde gespeichert!' . '<br />';
								}
								else{
									$errorMessage .= 'Die Daten-Datei &quot;' . basename($targetFilePath) . '&quot; konnten nicht gespeichert werden.' . '<br />';
								}
								if(file_exists($sourceFilePath)){
									unlink($sourceFilePath);
								}
							}
						}
					// EOF STORE DATA FILE
					#clearstatcache();
					#flush();
				}
			// EOF STORE FILES
		}
	// EOF STORE DATAS AND FILES

	// BOF DELETE DATA AND FILES
		if($_REQUEST["deletePrintingPlatesDataID"] != '' || $_REQUEST["deleteDatas"] != ""){
			if($_REQUEST["deletePrintingPlatesDataID"] != ''){
				$thisDeletePrintingPlatesDataID = $_REQUEST["deletePrintingPlatesDataID"];
			}
			else if($_REQUEST["deleteDatas"] != ""){
				$thisDeletePrintingPlatesDataID = $_REQUEST["editPrintingPlatesDataID"];
			}

			// BOF GET DATA TO DELETE
				$sql_getDataToDelete = "
						SELECT
								`printingPlatesDataID`,
								`printingPlatesDataCustomerNumber`,
								`printingPlatesDataPlateNumber`,
								`printingPlatesDataPrintText`,
								`printingPlatesDataLayoutPdfFileName`,
								`printingPlatesDataLayoutThumbnailFileName`,
								`printingPlatesDataLayoutDataFileName`,
								`printingPlatesDataImage`,
								`printingPlatesDataUploadDate`,
								`printingPlatesDataActive`,
								`printingPlatesDataModifiedDate`

							FROM `" . TABLE_PRINTING_PLATES_DATA . "`

							WHERE 1
								AND `printingPlatesDataID` = '" . $thisDeletePrintingPlatesDataID . "'
							LIMIT 1
					";
				$rs_getDataToDelete = $dbConnection->db_query($sql_getDataToDelete, $db_open);

				$arrDatasToDelete = array();
				while($ds_getDataToDelete = mysqli_fetch_assoc($rs_getDataToDelete)){
					$arrDatasToDelete = $ds_getDataToDelete;
				}
			// EOF GET DATA TO DELETE

			// BOF DELETE PREVIEW IMAGE
				if($arrDatasToDelete["printingPlatesDataLayoutThumbnailFileName"] != ""){
					$printingPlatesDataLayoutThumbnailFilePath = DIRECTORY_LAYOUT_PREVIEW_IMAGES . $arrDatasToDelete["printingPlatesDataLayoutThumbnailFileName"];
					if(file_exists($printingPlatesDataLayoutThumbnailFilePath)){
						$rs_unlink = unlink($printingPlatesDataLayoutThumbnailFilePath);
						if($rs_unlink){
							$successMessage .= 'Das Vorschau-Bild &quot;' . basename($printingPlatesDataLayoutThumbnailFilePath) . '&quot; wurde entfernt.' . '<br />';
						}
						else{
							$errorMessage .= 'Das Vorschau-Bild &quot;' . basename($printingPlatesDataLayoutThumbnailFilePath) . '&quot; konnte nicht entfernt werden.' . '<br />';
						}
					}
				}
			// EOF DELETE PREVIEW IMAGE

			// BOF DELETE PDF LAYOUT FILE
				if($arrDatasToDelete["printingPlatesDataLayoutPdfFileName"] != ""){
					$printingPlatesDataLayoutPdfFilePath = DIRECTORY_PRINT_PLATE_FILES . $arrDatasToDelete["printingPlatesDataLayoutPdfFileName"];
					if(file_exists($printingPlatesDataLayoutPdfFilePath)){
						$rs_unlink = unlink($printingPlatesDataLayoutPdfFilePath);
						if($rs_unlink){
							$successMessage .= 'Die Layout-Datei &quot;' . basename($printingPlatesDataLayoutThumbnailFilePath) . '&quot; wurde entfernt.' . '<br />';
						}
						else{
							$errorMessage .= 'Die Layout-Datei &quot;' . basename($printingPlatesDataLayoutThumbnailFilePath) . '&quot; konnte nicht entfernt werden.' . '<br />';
						}
					}
				}
			// EOF DELETE PDF LAYOUT FILE

			// BOF DELETE DATA FILES
				if($arrDatasToDelete["printingPlatesDataLayoutDataFileName"] != ""){
					$printingPlatesDataLayoutDataFilePath = DIRECTORY_PRINT_PLATE_FILES . $arrDatasToDelete["printingPlatesDataLayoutDataFileName"];
					if(file_exists($printingPlatesDataLayoutDataFilePath)){
						$rs_unlink = unlink($printingPlatesDataLayoutDataFilePath);
						if($rs_unlink){
							$successMessage .= 'Die Daten-Datei &quot;' . basename($printingPlatesDataLayoutDataFilePath) . '&quot; wurde entfernt.' . '<br />';
						}
						else{
							$errorMessage .= 'Die Daten-Datei &quot;' . basename($printingPlatesDataLayoutDataFilePath) . '&quot; konnte nicht entfernt werden.' . '<br />';
						}
					}
				}
			// EOF DELETE DATA FILES

			// BOF DELETE DATAS
				$sql_delete = "
						DELETE
							FROM `" . TABLE_PRINTING_PLATES_DATA . "`

						WHERE 1
							AND `printingPlatesDataID` = '" . $_REQUEST["deletePrintingPlatesDataID"] . "'
					";
				$rs_delete = $dbConnection->db_query($sql_delete, $db_open);
				if($rs_delete){
					$successMessage .= 'Die Daten wurden entfernt.' . '<br />';
				}
				else{
					$errorMessage .= 'Die Daten konnten nicht entfernt werden.' . '<br />';
				}
			// EOF DELETE DATAS

			// unset($_REQUEST["storePrintingPlatesDataID"]);
			unset($_REQUEST["editPrintingPlatesDataID"]);
		}
	// EOF DELETE DATA AND FILES

	// BOF GET ALL DATA
		$where = "";
		if($_REQUEST["editPrintingPlatesDataID"] == ''){

			if($_REQUEST["searchCustomerNumber"] != ''){
				$where = " AND `" . TABLE_PRINTING_PLATES_DATA . "`.`printingPlatesDataCustomerNumber` = '" . $_REQUEST["searchCustomerNumber"] . "' ";
			}
			else if($_REQUEST["searchCustomerName"] != ''){
				$where = "
						AND (
							`" . TABLE_CUSTOMERS . "`.`customersFirmenname` LIKE '%" . $_REQUEST["searchCustomerName"] . "%'
							OR
							`" . TABLE_CUSTOMERS . "`.`customersFirmennameZusatz` LIKE '%" . $_REQUEST["searchCustomerName"] . "%'
							OR
							`" . TABLE_PRINTING_PLATES_DATA . "`.`printingPlatesDataLayoutPdfFileName` LIKE '%" . $_REQUEST["searchCustomerName"] . "%'
							OR
							`" . TABLE_PRINTING_PLATES_DATA . "`.`printingPlatesDataLayoutPdfFileName` LIKE '%" . convertChars($_REQUEST["searchCustomerName"]) . "%'
						)
					";
			}
			else if($_REQUEST["searchPLZ"] != ''){
				$where = " AND `" . TABLE_CUSTOMERS . "`.`customersCompanyPLZ` = '" . $_REQUEST["searchPLZ"] . "' ";
			}
			else if($_REQUEST["searchPrintingPlateNumber"] != ''){
				$where = " AND `" . TABLE_PRINTING_PLATES_DATA . "`.`printingPlatesDataPlateNumber` LIKE '%" . $_REQUEST["searchPrintingPlateNumber"] . "%' ";
			}
			else if($_REQUEST["searchWord"] != ''){
				$where = "
						AND (
							`" . TABLE_CUSTOMERS . "`.`customersFirmenname` LIKE '%" . $_REQUEST["searchWord"] . "%'
							OR
							`" . TABLE_CUSTOMERS . "`.`customersFirmennameZusatz` LIKE '%" . $_REQUEST["searchWord"] . "%'
							OR
							`" . TABLE_PRINTING_PLATES_DATA . "`.`printingPlatesDataLayoutPdfFileName` LIKE '%" . $_REQUEST["searchWord"] . "%'
							OR
							`" . TABLE_PRINTING_PLATES_DATA . "`.`printingPlatesDataLayoutPdfFileName` LIKE '%" . convertChars($_REQUEST["searchWord"]) . "%'
							OR
							`" . TABLE_CUSTOMERS . "`.`customersCompanyPLZ` = '" . $_REQUEST["searchWord"] . "'
							OR
							`" . TABLE_PRINTING_PLATES_DATA . "`.`printingPlatesDataPlateNumber` LIKE '%" . $_REQUEST["searchWord"] . "%'
						)
					";
			}

			if($_REQUEST["searchStatus"] != 'ALL'){
				if($_REQUEST["searchStatus"] == '1'){
					$where .= " AND `" . TABLE_PRINTING_PLATES_DATA . "`.`printingPlatesDataActive` = '1' ";
				}
				else if($_REQUEST["searchStatus"] == '0'){
					$where .= " AND `" . TABLE_PRINTING_PLATES_DATA . "`.`printingPlatesDataActive` != '1' ";
				}
			}

			$sort = "
				`" . TABLE_PRINTING_PLATES_DATA . "`.`printingPlatesDataCustomerNumber` ASC,
				`" . TABLE_PRINTING_PLATES_DATA . "`.`printingPlatesDataUploadDate` DESC
			";

			if($_REQUEST["searchSort"] == '1'){
				$sort = "
					LENGTH(TRIM(SUBSTRING(REPLACE(`" . TABLE_PRINTING_PLATES_DATA . "`.`printingPlatesDataPlateNumber`, 'G', ''), 1, 3))) ASC,
					TRIM(SUBSTRING(REPLACE(`" . TABLE_PRINTING_PLATES_DATA . "`.`printingPlatesDataPlateNumber`, 'G', ''), 1, 3)) ASC ,
					`" . TABLE_PRINTING_PLATES_DATA . "`.`printingPlatesDataUploadDate` DESC
				";
			}

			if($_REQUEST["searchSort"] == '2'){
				$sort = "
					`" . TABLE_PRINTING_PLATES_DATA . "`.`printingPlatesDataUploadDate` ASC
				";
			}

			$sql = "
				SELECT
						`" . TABLE_PRINTING_PLATES_DATA . "`.`printingPlatesDataID`,
						`" . TABLE_PRINTING_PLATES_DATA . "`.`printingPlatesDataCustomerNumber`,
						`" . TABLE_PRINTING_PLATES_DATA . "`.`printingPlatesDataPlateNumber`,
						`" . TABLE_PRINTING_PLATES_DATA . "`.`printingPlatesDataPrintText`,
						`" . TABLE_PRINTING_PLATES_DATA . "`.`printingPlatesDataLayoutPdfFileName`,
						`" . TABLE_PRINTING_PLATES_DATA . "`.`printingPlatesDataLayoutThumbnailFileName`,
						`" . TABLE_PRINTING_PLATES_DATA . "`.`printingPlatesDataLayoutDataFileName`,
						`" . TABLE_PRINTING_PLATES_DATA . "`.`printingPlatesDataImage`,
						`" . TABLE_PRINTING_PLATES_DATA . "`.`printingPlatesDataUploadDate`,
						`" . TABLE_PRINTING_PLATES_DATA . "`.`printingPlatesDataActive`,
						`" . TABLE_PRINTING_PLATES_DATA . "`.`printingPlatesDataModifiedDate`,

						`" . TABLE_CUSTOMERS . "`.`customersID`,
						`" . TABLE_CUSTOMERS . "`.`customersKundennummer`,
						`" . TABLE_CUSTOMERS . "`.`customersFirmenname`,
						`" . TABLE_CUSTOMERS . "`.`customersFirmennameZusatz`,
						`" . TABLE_CUSTOMERS . "`.`customersCompanyStrasse`,
						`" . TABLE_CUSTOMERS . "`.`customersCompanyHausnummer`,
						`" . TABLE_CUSTOMERS . "`.`customersCompanyCountry`,
						`" . TABLE_CUSTOMERS . "`.`customersCompanyPLZ`,
						`" . TABLE_CUSTOMERS . "`.`customersCompanyOrt`

					FROM `" . TABLE_PRINTING_PLATES_DATA . "`

					LEFT JOIN `" . TABLE_CUSTOMERS . "`
					ON(`" . TABLE_PRINTING_PLATES_DATA . "`.`printingPlatesDataCustomerNumber` = `" . TABLE_CUSTOMERS . "`.`customersKundennummer`)

					WHERE 1
						" . $where . "

					ORDER BY
						" . $sort . "

				";
		}
	// EOF GET ALL DATA


	// BOF GET SELECTED DATA
		if($_REQUEST["editPrintingPlatesDataID"] != '' && $_REQUEST["editPrintingPlatesDataID"] != 'NEW'){
			$where = " AND `" . TABLE_PRINTING_PLATES_DATA . "`.`printingPlatesDataID` = '" . $_REQUEST["editPrintingPlatesDataID"] . "' ";

			$sql = "
				SELECT
					`" . TABLE_PRINTING_PLATES_DATA . "`.`printingPlatesDataID`,
					`" . TABLE_PRINTING_PLATES_DATA . "`.`printingPlatesDataCustomerNumber`,
					`" . TABLE_PRINTING_PLATES_DATA . "`.`printingPlatesDataPlateNumber`,
					`" . TABLE_PRINTING_PLATES_DATA . "`.`printingPlatesDataPrintText`,
					`" . TABLE_PRINTING_PLATES_DATA . "`.`printingPlatesDataLayoutPdfFileName`,
					`" . TABLE_PRINTING_PLATES_DATA . "`.`printingPlatesDataLayoutThumbnailFileName`,
					`" . TABLE_PRINTING_PLATES_DATA . "`.`printingPlatesDataLayoutDataFileName`,
					`" . TABLE_PRINTING_PLATES_DATA . "`.`printingPlatesDataImage`,
					`" . TABLE_PRINTING_PLATES_DATA . "`.`printingPlatesDataUploadDate`,
					`" . TABLE_PRINTING_PLATES_DATA . "`.`printingPlatesDataActive`,
					`" . TABLE_PRINTING_PLATES_DATA . "`.`printingPlatesDataModifiedDate`,

					`" . TABLE_CUSTOMERS . "`.`customersID`,
					`" . TABLE_CUSTOMERS . "`.`customersKundennummer`,
					`" . TABLE_CUSTOMERS . "`.`customersFirmenname`,
					`" . TABLE_CUSTOMERS . "`.`customersFirmennameZusatz`,
					`" . TABLE_CUSTOMERS . "`.`customersCompanyStrasse`,
					`" . TABLE_CUSTOMERS . "`.`customersCompanyHausnummer`,
					`" . TABLE_CUSTOMERS . "`.`customersCompanyCountry`,
					`" . TABLE_CUSTOMERS . "`.`customersCompanyPLZ`,
					`" . TABLE_CUSTOMERS . "`.`customersCompanyOrt`

				FROM `" . TABLE_PRINTING_PLATES_DATA . "`

				LEFT JOIN `" . TABLE_CUSTOMERS . "`
				ON(`" . TABLE_PRINTING_PLATES_DATA . "`.`printingPlatesDataCustomerNumber` = `" . TABLE_CUSTOMERS . "`.`customersKundennummer`)

				WHERE 1
					" . $where . "

				LIMIT 1

			";
			$rs = $dbConnection->db_query($sql, $db_open);

			list(
				$arrSelectedPrintingPlateData["printingPlatesDataID"],
				$arrSelectedPrintingPlateData["printingPlatesDataCustomerNumber"],
				$arrSelectedPrintingPlateData["printingPlatesDataPlateNumber"],
				$arrSelectedPrintingPlateData["printingPlatesDataPrintText"],
				$arrSelectedPrintingPlateData["printingPlatesDataLayoutPdfFileName"],
				$arrSelectedPrintingPlateData["printingPlatesDataLayoutThumbnailFileName"],
				$arrSelectedPrintingPlateData["printingPlatesDataLayoutDataFileName"],
				$arrSelectedPrintingPlateData["printingPlatesDataImage"],
				$arrSelectedPrintingPlateData["printingPlatesDataUploadDate"],
				$arrSelectedPrintingPlateData["printingPlatesDataActive"],
				$arrSelectedPrintingPlateData["printingPlatesDataModifiedDate"],

				$arrSelectedPrintingPlateData["customersID"],
				$arrSelectedPrintingPlateData["customersKundennummer"],
				$arrSelectedPrintingPlateData["customersFirmenname"],
				$arrSelectedPrintingPlateData["customersFirmennameZusatz"],
				$arrSelectedPrintingPlateData["customersCompanyStrasse"],
				$arrSelectedPrintingPlateData["customersCompanyHausnummer"],
				$arrSelectedPrintingPlateData["customersCompanyCountry"],
				$arrSelectedPrintingPlateData["customersCompanyPLZ"],
				$arrSelectedPrintingPlateData["customersCompanyOrt"]
			) = mysqli_fetch_array($rs) ;
		}
	// EOF GET SELECTED DATA
?>

<?php
	require_once('inc/headerHTML.inc.php');
	$thisTitle = "ABS-Leisten-Daten";
	$thisIcon = 'abs-plate.png';

	$headerHTML = preg_replace("/{###TITLE###}/", strip_tags($thisTitle), $headerHTML);
	echo $headerHTML;

	require_once(FILE_MENUE_TOP);
	if($_REQUEST["editPrintingPlatesDataID"] != '') {
		if($_REQUEST["editPrintingPlatesDataID"] != 'NEW') {
			$thisTitle = "ABS-Leisten-Daten bearbeiten";
			$thisIcon = 'abs-plate.png';
			if($arrSelectedPrintingPlateData["customersKundennummer"] != '') {
				$thisTitle .= ': <span class="headerSelectedEntry">' . $arrSelectedPrintingPlateData["customersKundennummer"] . ' &bull; ' . htmlentities(utf8_decode($arrSelectedPrintingPlateData["customersFirmenname"])) . '</span>';
			}
		}
		else {
			$thisTitle = "ABS-Leisten-Daten hinzuf&uuml;gen";
			$thisIcon = 'abs-plate.png';
		}
	}
?>

<div id="mainArea">
	<div id="mainContent">
		<?php require_once(FILE_MENUE_SIDEBAR); ?>
		<div id="contentArea">

			<div id="contentAreaElements">
				<h1><?php if(SHOW_PAGE_TITLE_ICONS) { echo '<img src="' . PATH_ICONS_MENUE_TITLES . $thisIcon . '" alt="" />'; } ?> <?php echo $thisTitle; ?></h1>
				<?php displayMessages(); ?>

				<?php if($_REQUEST["editPrintingPlatesDataID"] == '') { ?>
					<div class="menueActionsArea">
						<a href="?editPrintingPlatesDataID=NEW" class="linkButton" title="Neue ABS-Leiste hinzuf&uuml;gen">Neue ABS-Leiste hinzuf&uuml;gen</a>
						<div class="clear"></div>
					</div>

					<div style="text-align:right">
						<img src="layout/icons/iconPDF.gif" width="16" height="16" alt="" title="Alte PDF-Liste" />
						<a href="/Auftragslisten/displayDownloadFiles.php?downloadFile=abs-plaka-cita-listesi_2017-02-06.pdf">Alte PDF-ErhabeneLeisten-Nummerierungsliste</a>
					</div>

					<div id="searchFilterArea">
						<form name="formFilterSearch" method="get" action="<?php echo $_SERVER["PHP_SELF"]; ?>">
							<table border="0" cellpadding="0" cellspacing="0" class="searchFilterContent">
								<tr>
									<td>
										<label for="searchCustomerNumber">Kundennr:</label>
										<input type="text" name="searchCustomerNumber" id="searchCustomerNumber" class="inputField_70" value="" />
									</td>
									<td>
										<label for="searchCustomerName">Kundenname:</label>
										<input type="text" name="searchCustomerName" id="searchCustomerName" class="inputField_100" value="" />
									</td>
									<td>
										<label for="searchPLZ">PLZ:</label>
										<input type="text" name="searchPLZ" id="searchPLZ" class="inputField_40" />
									</td>
									<td>
										<label for="searchPLZ">Klischee-Nr:</label>
										<input type="text" name="searchPrintingPlateNumber" id="searchPrintingPlateNumber" class="inputField_40" />
									</td>
									<td>
										<label for="searchWord">Suchbegriff:</label>
										<input type="text" name="searchWord" id="searchWord" class="inputField_100" />
									</td>
									<td>
										<label for="searchStatus">Status:</label>
										<select name="searchStatus" id="searchStatus" class="inputSelect_100">
											<option value="ALL" <?php if($_REQUEST["searchStatus"] == 'ALL'){ echo ' selected="selected" '; } ?>>Alle</option>
											<option value="1" <?php if($_REQUEST["searchStatus"] == '1'){ echo ' selected="selected" '; } ?>>Aktiv</option>
											<option value="0" <?php if($_REQUEST["searchStatus"] == '0'){ echo ' selected="selected" '; } ?>>Nicht aktiv</option>
										</select>
									</td>
									<td>
										<label for="searchSort">Sortierung:</label>
										<select name="searchSort" id="searchSort" class="inputSelect_170">
											<option value="0" <?php if($_REQUEST["searchSort"] == '0') { echo ' selected="selected" '; } ?> >nach Kunden-Nummer</option>
											<option value="1" <?php if($_REQUEST["searchSort"] == '1') { echo ' selected="selected" '; } ?> >nach Klichee-Nummer</option>
											<?php if($_COOKIE["isAdmin"] == '1'){ ?>
											<option value="2" <?php if($_REQUEST["searchSort"] == '2') { echo ' selected="selected" '; } ?> >nach Datum</option>
											<?php } ?>
										</select>
									</td>
									<td>
										<input type="hidden" name="xxxeditID" id="xxxeditID" value="" />
										<input type="submit" name="submitSearch" class="inputButton0" value="Suchen" />
									</td>
									<?php if($_COOKIE["isAdmin"] == '1'){ ?>
										<!-- <input type="checkbox" value="1" name="exportFilePdf" id="exportFilePdf" value="1" /> -->
									<?php } ?>
								</tr>
							</table>
						</form>
					</div>

					<?php
						// BOF GET ALL DATA
								$rs = $dbConnection->db_query($sql, $db_open);

								$countRows = $dbConnection->db_getMysqlNumRows($rs);

								if($countRows > 0){

									echo '<p class="infoMessage">Anzahl der Daten: ' . $countRows . '</p>';

									echo '<table border="0" cellpadding="0" cellspacing="0" width="100%" class="displayOrders">';
									echo '<colgroup>';
										echo '<col />';
										echo '<col />';
										echo '<col />';
										echo '<col />';
										echo '<col />';
										echo '<col />';
										echo '<col />';
										echo '<col />';
									echo '</colgroup>';


									echo '<thead>';
									echo '<tr>';

									echo '<th style="width:45px;text-align:right;">#</th>';
									echo '<th>KNR</th>';
									echo '<th>Firma</th>';
									echo '<th style="width:70px;">Aktiv?</th>';
									echo '<th>Bild</th>';
									echo '<th>Klischee-Nr</th>';
									echo '<th style="width:90px;">Datum</th>';
									echo '<th style="width:50px;">Aktion</th>';

									echo '</tr>';
									echo '</thead>';

									echo '<tbody>';

									$count = 0;

									$thisMarker = '';

									while($ds = mysqli_fetch_assoc($rs)){

										if($count%2 == 0){ $rowClass = 'row0'; }
										else { $rowClass = 'row1'; }

										$thisRowStyle = '';
										if($ds["printingPlatesDataActive"] != '1'){
											$thisRowStyle = 'color:#F00;opacity:0.3;';
										}

										if($thisMarker != $ds["printingPlatesDataCustomerNumber"]){
											$thisRowStyle .= 'border-top:2px solid #666;';
											$thisMarker = $ds["printingPlatesDataCustomerNumber"];
										}
										echo '<tr class="'.$rowClass.'" style="' . $thisRowStyle . '">';

										echo '<td style="text-align:right;"><b>' . ($count + 1) .'.</b></td>';

										echo '<td>';
										echo $ds["printingPlatesDataCustomerNumber"];
										echo '</td>';

										echo '<td>';
										echo '<b>' . htmlentities($ds["customersFirmenname"]) . '</b><br />';
										echo $ds["customersFirmennameZusatz"] . '<br />';
										echo htmlentities($ds["customersCompanyStrasse"] . ' ' . $ds["customersCompanyHausnummer"]) . '<br />';
										echo htmlentities($ds["customersCompanyPLZ"] . ' ' . $ds["customersCompanyOrt"]) . '<br />';
										echo htmlentities($arrCountryTypeDatas[$thisValue["customersCompanyCountry"]]["countries_name_DE"]) . '<br />';
										echo '</td>';

										echo '<td style="text-align:center;white-space:nowrap !important;">';
											echo '<span class="toolItem">';
											$thisPrintingPlatesStatusImg = 'iconOk.png';
											$thisPrintingPlatesStatusText = 'Aktiv';
											$thisPrintingPlatesStatusButtonText = 'deaktivieren';
											$thisPrintingPlatesStatusButtonText = 'deaktivieren';
											$thisPrintingPlatesStatusChangeNew = '0';

											if($ds["printingPlatesDataActive"] != '1'){
												$thisPrintingPlatesStatusImg = 'iconNotOk.png';
												$thisPrintingPlatesStatusText = 'Nicht aktiv';
												$thisPrintingPlatesStatusButtonText = 'aktivieren';
												$thisPrintingPlatesStatusChangeNew = '1';
											}

											echo '<a href="?changeStatusDataID=' . $ds["printingPlatesDataID"] . '&amp;changeStatusNew=' . $thisPrintingPlatesStatusChangeNew . '&amp;searchStatus=' . $_REQUEST["searchStatus"] . '&amp;searchSort=' . $_REQUEST["searchSort"] . '" style="display:block;float:left;border:1px solid #999;!important;width:18px;height:18px;" onclick="return showWarning(\'Wollen Sie die Daten des Kunden ' . htmlentities(addslashes($ds["customersFirmenname"])) . ' - KNR ' . $ds["printingPlatesDataCustomerNumber"]  . ' wirklich ' . $thisPrintingPlatesStatusButtonText . '?\')">';
											echo '<img src="layout/icons/' . $thisPrintingPlatesStatusImg . '" width="16" height="16" alt="' . $thisPrintingPlatesStatusText . '" title="Diese Daten ' . $thisPrintingPlatesStatusButtonText . '" />';
											echo '</a>';
											echo '</span>';

											if($ds["printingPlatesDataLayoutPdfFileName"] != ''){
												echo '<span class="toolItem">';

												/*
												// BOF COPY PDF FILE FOR MANUALLY IMPORTED DATA
												echo 'fe 1: ' . file_exists(DIRECTORY_PRINT_PLATE_FILES . $ds["printingPlatesDataLayoutPdfFileName"]) . '<br />';
												echo 'file 1: ' . DIRECTORY_PRINT_PLATE_FILES . $ds["printingPlatesDataLayoutPdfFileName"] . '<br />';

												echo 'fe 2: ' . file_exists(DIRECTORY_UPLOAD_FILES . $ds["printingPlatesDataLayoutPdfFileName"]) . '<br />';
												echo 'file 2: ' . DIRECTORY_UPLOAD_FILES . $ds["printingPlatesDataLayoutPdfFileName"] . '<br />';

												if($ds["printingPlatesDataLayoutPdfFileName"] != '' && !file_exists(DIRECTORY_PRINT_PLATE_FILES . $ds["printingPlatesDataLayoutPdfFileName"]) && file_exists(DIRECTORY_UPLOAD_FILES . $ds["printingPlatesDataLayoutPdfFileName"])){
													$rs_copy = copy(DIRECTORY_UPLOAD_FILES . $ds["printingPlatesDataLayoutPdfFileName"], DIRECTORY_PRINT_PLATE_FILES . $ds["printingPlatesDataLayoutPdfFileName"]);
													dd('rs_copy');
												}
												if($ds["printingPlatesDataLayoutPdfFileName"] != '' && !file_exists(DIRECTORY_PRINT_PLATE_FILES . $ds["printingPlatesDataLayoutPdfFileName"]) && file_exists(DIRECTORY_PRINT_PLATE_FILES . 'erhabene_Archiv/' . $ds["printingPlatesDataLayoutPdfFileName"])){
													$rs_copy = copy(DIRECTORY_PRINT_PLATE_FILES . 'erhabene_Archiv/' . $ds["printingPlatesDataLayoutPdfFileName"], DIRECTORY_PRINT_PLATE_FILES . $ds["printingPlatesDataLayoutPdfFileName"]);
													#dd('rs_copy');
												}
												// EOF COPY PDF FILE FOR MANUALLY IMPORTED DATA
												*/
												/*
												if($ds["printingPlatesDataLayoutPdfFileName"] != '' && !file_exists(DIRECTORY_PRINT_PLATE_FILES . $ds["printingPlatesDataLayoutPdfFileName"]) && file_exists(DIRECTORY_PRINT_PLATE_FILES . 'erhabene_Archiv/' . $ds["printingPlatesDataLayoutPdfFileName"])){
													$rs_copy = copy(DIRECTORY_PRINT_PLATE_FILES . 'erhabene_Archiv/' . $ds["printingPlatesDataLayoutPdfFileName"], DIRECTORY_PRINT_PLATE_FILES . $ds["printingPlatesDataLayoutPdfFileName"]);
													dd('rs_copy');
													unlink(DIRECTORY_PRINT_PLATE_FILES . 'erhabene_Archiv/' . $ds["printingPlatesDataLayoutPdfFileName"]);
												}
												else if($ds["printingPlatesDataLayoutPdfFileName"] != '' && !file_exists(DIRECTORY_PRINT_PLATE_FILES . $ds["printingPlatesDataLayoutPdfFileName"]) && file_exists(DIRECTORY_PRINT_PLATE_FILES . 'auto-converted_PDF/' . $ds["printingPlatesDataLayoutPdfFileName"])){
													$rs_copy = copy(DIRECTORY_PRINT_PLATE_FILES . 'auto-converted_PDF/' . $ds["printingPlatesDataLayoutPdfFileName"], DIRECTORY_PRINT_PLATE_FILES . $ds["printingPlatesDataLayoutPdfFileName"]);
													dd('rs_copy');
													unlink(DIRECTORY_PRINT_PLATE_FILES . 'auto-converted_PDF/' . $ds["printingPlatesDataLayoutPdfFileName"]);
												}
												*/

												echo '<a href="?documentType=KA&amp;downloadFile='.urlencode(basename($ds["printingPlatesDataLayoutPdfFileName"])).'" title="Datei anzeigen">';
												$thisImageName = 'icon' . strtoupper(getFileType(basename($ds["printingPlatesDataLayoutPdfFileName"]))) . '.gif';
												echo '<img src="layout/icons/' . $thisImageName.'" alt="" width="16" height="16" />';
												echo '</a>';
												echo '</span>';
											}
											if($ds["printingPlatesDataLayoutDataFileName"] != ''){
												echo '<span class="toolItem">';
												echo '<a href="?documentType=KA&amp;downloadFile='.urlencode(basename($ds["printingPlatesDataLayoutDataFileName"])).'" title="Datei anzeigen">';
												$thisImageName = 'icon' . strtoupper(getFileType(basename($ds["printingPlatesDataLayoutDataFileName"]))) . '.gif';
												echo '<img src="layout/icons/' . $thisImageName.'" alt="" width="16" height="16" />';
												echo '</a>';
												echo '</span>';
											}

										echo '</td>';

										echo '<td>';
										if($ds["printingPlatesDataLayoutThumbnailFileName"] == '' && $ds["printingPlatesDataLayoutPdfFileName"] != ''){
											$ds["printingPlatesDataLayoutThumbnailFileName"] = pathinfo($ds["printingPlatesDataLayoutPdfFileName"], PATHINFO_FILENAME) . ".png";
										}

										if($ds["printingPlatesDataLayoutThumbnailFileName"] != ''){
											$thisLayoutPdfPath = DIRECTORY_PRINT_PLATE_FILES . $ds["printingPlatesDataLayoutPdfFileName"];
											$thisThumbnailImagePath = DIRECTORY_LAYOUT_PREVIEW_IMAGES . $ds["printingPlatesDataLayoutThumbnailFileName"];

											if(!file_exists($thisThumbnailImagePath)){
												if(file_exists($thisLayoutPdfPath)){
													$createThumbnail = new createImage(BASEPATH . $thisLayoutPdfPath, BASEPATH . $thisThumbnailImagePath, $previewImageDpi, $previewImageWidth, $previewImageHeight, $previewImageStartX, $previewImageStartY);
												}
											}
											clearstatcache();
											flush();

											echo '<div class="previewImageContainer" style="background-color:#FFF;width:820px;height:50px;overflow:hidden;margin:0 10px 0 10px;">';
											echo '<img src="' . $thisThumbnailImagePath. '" alt="Leistenbild" title="Leistenbild" style="width:800px;border:1px solid #EEE;padding:4px;" />';
											echo '</div>';
											if($_COOKIE["isAdmin"]){
												echo '<br />' . htmlentities(basename($thisThumbnailImagePath));
											}
										}
										echo '</td>';

										echo '<td style="text-align:center;">';
											if($ds["printingPlatesDataPlateNumber"] != ''){
												echo '<b style="font-size:20px;line-height:20px;">' . $ds["printingPlatesDataPlateNumber"] . '</b>';
											}
										echo '</td>';

										echo '<td>';
										echo formatDate($ds["printingPlatesDataUploadDate"], 'display');
										echo '</td>';

										echo '<td style="text-align:center;white-space:nowrap !important;">';

											if($_COOKIE["isAdmin"] == '1'){
											echo '<span class="toolItem">';
											echo '<a href="?deletePrintingPlatesDataID=' . $ds["printingPlatesDataID"] . '" onclick="return showWarning(\'Wollen Sie die Daten des Kunden ' . addslashes(htmlentities($ds["customersFirmenname"])) . ' - KNR ' . $ds["printingPlatesDataCustomerNumber"]  . ' wirklich entfernen?\')">';
											echo '<img src="layout/icons/iconDelete.png" width="16" height="16" alt="" title="Diese Datei l&ouml;schen" />';
											echo '</a>';
											echo '</span>';
											}

											echo '<span class="toolItem">';
											echo '<a href="?editPrintingPlatesDataID=' . $ds["printingPlatesDataID"] . '">';
											echo '<img src="layout/icons/iconEdit.gif" width="16" height="16" alt="" title="Diese Datei bearbeiten" />';
											echo '</a>';
											echo '</span>';

											/*
											echo '<span class="toolItem">';
											echo '<img src="layout/icons/mailAttachement.png" class="buttonSendDocument" rel="' . urlencode(basename($thisValue2["createdDocumentsFilename"])) . '#KA" width="16" height="16" title="' . urlencode(basename($thisValue2["createdDocumentsFilename"])) . '&quot; direkt per Mail versenden" alt="Dokument versenden" />';
											echo '</span>';
											*/
										echo '</td>';

										echo '</tr>';

										$count++;
									}
									echo '</tbody>';
									echo '</table>';
								}
								else{
									$errorMessage = 'Keine Daten vorhanden!' . '<br />';
								}
						// BOF GET ALL DATA

						displayMessages();
					?>
				<?php } ?>

				<?php if($_REQUEST["editPrintingPlatesDataID"] != '') { ?>

				<?php
					$readonly = '';
					if($_REQUEST["editPrintingPlatesDataID"] != "NEW") {
						// $readonly = ' readonly="readonly" ';
					}
				?>
					<div class="adminInfo">Datensatz-ID: <?php echo $arrSelectedPrintingPlateData["printingPlatesDataID"]; ?></div>
					<div class="adminEditArea">
						<form name="formEditPrintingPlateDatas" id="formEditPrintingPlateDatas" method="post" action="<?php echo PAGE_DISPLAY_ABS_PLATE_DATA; ?>" enctype="multipart/form-data" >
							<input type="hidden" name="editPrintingPlatesDataID" value="<?php echo $arrSelectedPrintingPlateData["printingPlatesDataID"]; ?>" />
							<input type="hidden" name="storePrintingPlatesDataID" value="<?php echo $_REQUEST["editPrintingPlatesDataID"]; ?>" />

							<br /><p class="infoArea">Pflichtfelder m&uuml;ssen ausgef&uuml;llt werden!</p>
							<fieldset class="colored1">
								<legend>Kunden-Daten</legend>
								<table border="0" width="100%" cellspacing="0" cellpadding="0">
									<tr>
										<td style="width:200px;"><b>Kundennummer:</b></td>
										<td><input type="text" name="editPrintingPlatesDataCustomerNumber" id="editPrintingPlatesDataCustomerNumber" class="inputField_70" value="<?php echo $arrSelectedPrintingPlateData["printingPlatesDataCustomerNumber"]; ?>" <?php echo $readonly; ?> /></td>
									</tr>
									<tr>
										<td style="width:200px;"><b>Kunde:</b></td>
										<td>
											<?php if($arrSelectedPrintingPlateData["printingPlatesDataCustomerNumber"] != ""){ ?>
											<p>
												<b><?php echo $arrSelectedPrintingPlateData["customersFirmenname"]; ?></b>
												<br />
												<?php echo $arrSelectedPrintingPlateData["customersFirmennameZusatz"]; ?>
											</p>

											<p>
												<?php echo $arrSelectedPrintingPlateData["customersCompanyStrasse"]; ?> <?php echo $arrSelectedPrintingPlateData["customersCompanyHausnummer"]; ?>
												<br />
												<?php echo $arrSelectedPrintingPlateData["customersCompanyPLZ"]; ?> <?php echo $arrSelectedPrintingPlateData["customersCompanyOrt"]; ?>
												<br />
												<?php echo $arrCountryTypeDatas[$arrSelectedPrintingPlateData["customersCompanyCountry"]]["countries_name_DE"]; ?>
											</p>
											<?php } ?>

											<?php if($_REQUEST["editPrintingPlatesDataID"] == "NEW") { ?>
											<input type="text" name="editPrintingPlatesDataCustomerFirmenname" id="editPrintingPlatesDataCustomerFirmenname" value="" style="border:none;background-color:transparent;" />
											<!--
											<br />
											<input type="text" name="editPrintingPlatesDataCustomerFirmenname2" id="editPrintingPlatesDataCustomerFirmenname2" value="" style="border:none;background-color:transparent;" /><br />
											<input type="text" name="editPrintingPlatesDataCustomerFirmennameZusatz" id="editPrintingPlatesDataCustomerFirmennameZusatz" value="" style="border:none;background-color:transparent;" /><br />
											<input type="text" name="editPrintingPlatesDataCustomerStrasse" id="editPrintingPlatesDataCustomerStrasse" value="" style="border:none;background-color:transparent;" /> <input type="text" name="editPrintingPlatesDataCustomerHausnummer" id="editPrintingPlatesDataCustomerHausnummer" value="" style="border:none;background-color:transparent;" /><br />
											<input type="text" name="editPrintingPlatesDataCustomerPLZ" id="editPrintingPlatesDataCustomerPLZ" value="" style="border:none;background-color:transparent;" /> <input type="text" name="editPrintingPlatesDataCustomerOrt" id="editPrintingPlatesDataCustomerOrt" value="" style="border:none;background-color:transparent;" /><br />
											<input type="text" name="editPrintingPlatesDataCustomerLand" id="editPrintingPlatesDataCustomerLand" value="" style="border:none;background-color:transparent;" />
											-->
											<?php } ?>
										</td>
									</tr>
								</table>
							</fieldset>
							<fieldset class="colored1">
								<legend>ABS-Daten</legend>
								<table border="0" width="100%" cellspacing="0" cellpadding="0">
									<tr>
										<td style="width:200px;"><b>Klischee-Nr:</b></td>
										<td><input type="text" name="editPrintingPlatesDataPlateNumber" id="editPrintingPlatesDataPlateNumber" class="inputField_510" value="<?php echo $arrSelectedPrintingPlateData["printingPlatesDataPlateNumber"]; ?>" /></td>
									</tr>
									<tr>
										<td style="width:200px;"><b>Status:</b></td>
										<td>
											<?php
												$checked = '';
												if($arrSelectedPrintingPlateData["printingPlatesDataActive"] == '1'){
													$checked = ' checked="checked" ';
												}
												else if($_REQUEST["editPrintingPlatesDataID"] == "NEW") {
													$checked = ' checked="checked" ';
												}
											?>
											<input type="checkbox" name="editPrintingPlatesDataActive" id="editPrintingPlatesDataActive" value="1" <?php echo $checked; ?> /> Aktiv?
										</td>
									</tr>
									<tr>
										<td style="width:200px;"><b>Upload-Datum:</b></td>
										<td>
											<?php
												if($_REQUEST["editPrintingPlatesDataID"] == "NEW" || $arrSelectedPrintingPlateData["printingPlatesDataUploadDate"] == '') {
													$thisUploadDate = date('Y-m-d H:i:s');
												}
												else {
													$thisUploadDate = $arrSelectedPrintingPlateData["printingPlatesDataUploadDate"];
												}

											?>
											<input type="text" class="inputField_200" name="editPrintingPlatesDataUploadDate" id="editPrintingPlatesDataUploadDate" value="<?php echo formatDate($thisUploadDate, 'display'); ?>" />
										</td>
									</tr>
									<tr>
										<td style="width:200px;"><b>Drucktext:</b></td>
										<td><input type="text" name="editPrintingPlatesDataPrintText" id="editPrintingPlatesDataPrintText" class="inputField_510" value="<?php echo $arrSelectedPrintingPlateData["printingPlatesDataPrintText"]; ?>" /></td>
									</tr>
									<tr>
										<td style="width:200px;"><b>Vorschaubild:</b></td>
										<td>
											<?php
												// dd('arrSelectedPrintingPlateData');
												if($arrSelectedPrintingPlateData["printingPlatesDataLayoutThumbnailFileName"] == '' && $arrSelectedPrintingPlateData["printingPlatesDataLayoutPdfFileName"] != ''){
													$arrSelectedPrintingPlateData["printingPlatesDataLayoutThumbnailFileName"] = pathinfo($arrSelectedPrintingPlateData["printingPlatesDataLayoutPdfFileName"], PATHINFO_FILENAME) . ".png";
												}

												if($arrSelectedPrintingPlateData["printingPlatesDataLayoutThumbnailFileName"] != ''){
													$thisLayoutPdfPath = DIRECTORY_PRINT_PLATE_FILES . $arrSelectedPrintingPlateData["printingPlatesDataLayoutPdfFileName"];
													$thisThumbnailImagePath = DIRECTORY_LAYOUT_PREVIEW_IMAGES . $arrSelectedPrintingPlateData["printingPlatesDataLayoutThumbnailFileName"];


													if(!file_exists($thisThumbnailImagePath)){
														if(file_exists($thisLayoutPdfPath)){
															// var_dump(get_pdf_dimensions($thisLayoutPdfPath));
															$createThumbnail = new createImage(BASEPATH . $thisLayoutPdfPath, BASEPATH . $thisThumbnailImagePath, $previewImageDpi, $previewImageWidth, $previewImageHeight, $previewImageStartX, $previewImageStartY);
														}
													}
													clearstatcache();
													flush();


													echo '<div class="previewImageContainer" style="background-color:#FFF;width:100%;height:50px;overflow:hidden;">';
													echo '<img src="' . DIRECTORY_LAYOUT_PREVIEW_IMAGES . $arrSelectedPrintingPlateData["printingPlatesDataLayoutThumbnailFileName"] . '" alt="Leistenbild" title="Leistenbild" style="width:100%;border:1px solid #EEE;padding:4px;"/>';
													echo '</div>';
												}
											?>
											<input type="hidden" name="editPrintingPlatesDataLayoutThumbnailFileName" id="editPrintingPlatesDataLayoutThumbnailFileName" value="<?php echo $arrSelectedPrintingPlateData["printingPlatesDataLayoutThumbnailFileName"]; ?>" />
										</td>
									</tr>

									<tr>
										<td style="width:200px;"><b>Layout-Datei:</b></td>
										<td><b>Aktuelle Datei: </b>
											<?php
												if($arrSelectedPrintingPlateData["printingPlatesDataLayoutPdfFileName"] != ''){
													echo '<a href="?editPrintingPlatesDataID=' . $arrSelectedPrintingPlateData["printingPlatesDataID"] . '&amp;documentType=KA&amp;downloadFile=' . urlencode(basename($arrSelectedPrintingPlateData["printingPlatesDataLayoutPdfFileName"])) . '" title="Datei anzeigen">';
													$thisImageName = 'icon' . strtoupper(getFileType(basename($arrSelectedPrintingPlateData["printingPlatesDataLayoutPdfFileName"]))) . '.gif';
													echo '<img src="layout/icons/' . $thisImageName.'" alt="" width="16" height="16" /> ';
													echo $arrSelectedPrintingPlateData["printingPlatesDataLayoutPdfFileName"];
													echo '</a>';
												}
												else{
													echo '<p class="infoArea" style="width:510px">Keine Layout-Datei vorhanden!</p>';
												}
											?>
											<input type="file" name="uploadPrintingPlatesDataLayoutPdfFile" id="uploadPrintingPlatesDataLayoutPdfFile" class="inputFile_510" />
											<input type="hidden" name="editPrintingPlatesDataLayoutPdfFileName" id="editPrintingPlatesDataLayoutPdfFileName" value="<?php echo $arrSelectedPrintingPlateData["printingPlatesDataLayoutPdfFileName"]; ?>" />
											<p class="warningArea" style="width:510px">Die Daten-Datei muss im Format &quot;PDF&quot; sein!</p>
										</td>
									</tr>
									<?php if(!1){ ?>
									<tr>
										<td style="width:200px;"><b>Daten-Datei:</b></td>
										<td><b>Aktuelle Datei: </b>
											<?php
												if($arrSelectedPrintingPlateData["printingPlatesDataLayoutDataFileName"] != ''){
													echo '<a href="?editPrintingPlatesDataID=' . $arrSelectedPrintingPlateData["printingPlatesDataID"] . '&amp;documentType=PP&amp;downloadFile=' . urlencode(basename($arrSelectedPrintingPlateData["printingPlatesDataLayoutDataFileName"])) . '" title="Datei anzeigen">';
													$thisImageName = 'icon' . strtoupper(getFileType(basename($arrSelectedPrintingPlateData["printingPlatesDataLayoutDataFileName"]))) . '.gif';
													echo '<img src="layout/icons/' . $thisImageName.'" alt="" width="16" height="16" /> ';
													echo $arrSelectedPrintingPlateData["printingPlatesDataLayoutDataFileName"];
													echo '</a>';
												}
												else{
													echo '<p class="infoArea" style="width:510px">Keine Daten-Datei vorhanden!</p>';
												}
											?>
											<input type="file" name="uploadPrintingPlatesDataLayoutDataFile" id="uploadPrintingPlatesDataLayoutDataFile" class="inputFile_510" />
											<input type="hidden" name="editPrintingPlatesDataLayoutDataFileName" id="editPrintingPlatesDataLayoutDataFileName" value="<?php echo $arrSelectedPrintingPlateData["printingPlatesDataLayoutDataFileName"]; ?>" />
											<p class="warningArea" style="width:510px">Die Daten-Datei muss im Format &quot;rar&quot; oder  &quot;zip&quot; sein!</p>
										</td>
									</tr>
									<?php } ?>
								</table>
							</fieldset>

							<div class="actionButtonsArea">
								<?php if($arrGetUserRights["editCustomers"]) { ?>
									<input type="submit" class="inputButton1 inputButtonGreen" name="storeDatas" value="Speichern" onclick="return checkFormDutyFields(this.form.name, <?php echo $jsonFormDutyFields; ?>);" />
									<input type="submit" class="inputButton1 inputButtonRed" name="deleteDatas" value="Endg&uuml;ltig l&ouml;schen" onclick="return showWarning('Wollen Sie diesen Eintrag wirklich endgültig entfernen???\nOder soll der Kunde nur deaktiviert werden?');" />
									<input type="submit" class="inputButton1 inputButtonOrange" name="cancelDatas" value="Abbrechen" onclick="return showWarning('Wollen Sie diesen Vorgang wirklich abbrechen??? ');" />
								<?php } ?>
							</div>
						</form>
					</div>
				<?php } ?>

			</div>
		</div>
	</div>
</div>

<script language="javascript" type="text/javascript">
	<!--
	/* <![CDATA[ */
	$(document).ready(function() {
		$('#editPrintingPlatesDataCustomerNumber').keyup(function () {
			loadSuggestions('searchCustomerNumber', [{'triggerElement': '#editPrintingPlatesDataCustomerNumber', 'fieldNumber': '#editPrintingPlatesDataCustomerNumber', 'fieldName': '#editPrintingPlatesDataCustomerFirmenname', 'fieldCity': '#editPrintingPlatesDataCustomerOrt', 'fieldZipCode': '#editPrintingPlatesDataCustomerPLZ'}], 1);
			/*
			$('.searchResultItem').live('click', function(){
				var thisRecipientCustomerNumber = $(this).find('.customerNumber').text();
				var thisRecipientCustomerID = $(this).find('.customerID').text();
				reloadCustomerAdressFields('searchCustomerAdressDatas', 'company', [{'thisCustomerID': thisRecipientCustomerID, 'triggerElement': '#editPrintingPlatesDataCustomerNumber', 'fieldNumber': '#editPrintingPlatesDataCustomerNumber', 'fieldName': '#editPrintingPlatesDataCustomerFirmenname2', 'fieldNameAdd': '#editPrintingPlatesDataCustomerFirmennameZusatz', 'fieldCity': '#editPrintingPlatesDataCustomerOrt', 'fieldZipCode': '#editPrintingPlatesDataCustomerPLZ', 'fieldStreet': '#editPrintingPlatesDataCustomerStrasse', 'fieldStreetNumber': '#editPrintingPlatesDataCustomerHausnummer', 'fieldCountry': '#editPrintingPlatesDataCustomerLand'}]);
			});
			*/
		});
	});

	/* ]]> */
	// -->
</script>

<?php require_once('inc/footerHTML.inc.php'); ?>
