<?php
	require_once('inc/requires.inc.php');

	if(!$arrGetUserRights["displayTracking"]) {
		#header('location: ' . PAGE_EXIT_LOCATION);
		#exit;
	}

	$jswindowMessage = "";
	$warningMessage = '';
	$errorMessage = '';
	$successMessage = '';
	$infoMessage = '';

	$dbConnection = new DB_Connection();
	$db_open = $dbConnection->db_connect();

	// BOF READ MANDATORIES
		$arrMandatoryDatas = getMandatories();
		if(!empty($arrMandatoryDatas)){
			$arrTemp = array();
			foreach($arrMandatoryDatas as $thisKey => $thisValue){
				$arrTemp[$thisValue["mandatoriesShortName"]] = $thisValue["mandatoriesName"];
			}
		}
		$arrMandatoryDatas = $arrTemp;
	// EOF READ MANDATORIES

	$defaultSortField = "sendedMailsSendTime";
	$defaultSortDirection = "DESC";

	if($_REQUEST["sortField"] != "") {
		$sqlSortField = $_REQUEST["sortField"];
	}
	else {
		$sqlSortField = $defaultSortField;
	}

	if($_REQUEST["sortDirection"] != "") {
		$sqlSortDirection = $_REQUEST["sortDirection"];
	}
	else {
		$sqlSortDirection = $defaultSortDirection;
	}
	if($sqlSortDirection == "DESC") { $sqlSortDirection = "ASC"; }
	else { $sqlSortDirection = "DESC"; }

?>
<?php
	require_once('inc/headerHTML.inc.php');
	$thisTitle = "Aus dem Auftragslisten versendete Mails";

	if($_POST["searchMandatory"] != "") {
		$thisTitle .= " - Mandator ".$_POST["searchMandatory"];
	}
	if($_POST["searchMailSubject"] != "") {
		$thisTitle .= " - Betreff ".$_POST["searchMailSubject"];
	}
	if($_POST["searchMailRecipient"] != "") {
		$thisTitle .= " - Empf&auml;nger ".$_POST["searchMailRecipient"];
	}
	if($_POST["searchWord"] != "") {
		$thisTitle .= " - Suchbegriff ".$_POST["searchWord"];
	}
	if($_POST["searchSendDate"] != "") {
		$thisTitle .= " - Sendedatum ".$_POST["searchSendDate"];
	}
	if($_REQUEST["displayOrderStatus"] != "") {
		$thisTitle .= " - Status ".$arrOrderStatusTypeDatas[$_REQUEST["displayOrderStatus"]]["orderStatusTypesName"];
	}
	$headerHTML = preg_replace("/{###TITLE###}/", strip_tags($thisTitle), $headerHTML);
	echo $headerHTML;

	require_once(FILE_MENUE_TOP);

	$sqlWhere = "";

	// BOF CREATE SQL FOR DISPLAY ORDERS
	if($_REQUEST["searchMailRecipient"] != "") {
		$sqlWhere = " AND `sendedMailsRecipient` LIKE '%" . $_REQUEST["searchMailRecipient"] . "%' ";
	}
	else if($_REQUEST["searchMailSubject"] != "") {
		$sqlWhere = " AND (`sendedMailsSubject` = '" . $_REQUEST["searchMailSubject"] . "') ";
	}
	else if($_REQUEST["searchMandatory"] != "") {
		$sqlWhere = " AND (`sendedMailsMandatory` = '" . $_REQUEST["searchMandatory"] . "') ";
	}
	else if($_REQUEST["searchSendDate"] != "") {
		$sqlWhere = " AND (DATE_FORMAT(`sendedMailsSendTime`, '%Y-%m-%d') = '" . formatDate($_REQUEST["searchSendDate"], "store") . "') ";
	}

	else if($_REQUEST["searchWord"] != "") {
		$sqlWhere = " AND (
						`sendedMailsRecipient` LIKE '%" . $_REQUEST["searchWord"] . "%'
						OR
						`sendedMailsSubject` LIKE '%" . $_REQUEST["searchWord"] . "%'
					) ";
					// MATCH(`ordersVertreter`) AGAINST ('" . $_REQUEST["searchWord"] . "')
					// MATCH(`ordersKundenName`) AGAINST ('" . $_REQUEST["searchWord"] . "')
	}
	else {
		$sqlWhere = "";
	}

	$sqlSortField = "ORDER BY `" . $sqlSortField . "` ";

	$sql = "SELECT
					/* SQL_CALC_FOUND_ROWS */

					`" . TABLE_SENDED_MAILS . "`.`sendedMailsID`,
					`" . TABLE_SENDED_MAILS . "`.`sendedMailsMandatory`,
					`" . TABLE_SENDED_MAILS . "`.`sendedMailsRecipient`,
					`" . TABLE_SENDED_MAILS . "`.`sendedMailsSender`,
					`" . TABLE_SENDED_MAILS . "`.`sendedMailsSendTime`,
					`" . TABLE_SENDED_MAILS . "`.`sendedMailsSubject`,
					`" . TABLE_SENDED_MAILS . "`.`sendedMailsContent`,
					`" . TABLE_SENDED_MAILS . "`.`sendedMailsDocumentType`,
					`" . TABLE_SENDED_MAILS . "`.`sendedMailsDocumentNumber`,
					`" . TABLE_SENDED_MAILS . "`.`sendedMailsUser`,
					`" . TABLE_SENDED_MAILS . "`.`sendedMailsIsSended`,

					`" . TABLE_USERS . "`.`usersFirstName`,
					`" . TABLE_USERS . "`.`usersLastName`

				FROM `" . TABLE_SENDED_MAILS . "`

				LEFT JOIN `" . TABLE_USERS . "`
				ON(`" . TABLE_SENDED_MAILS . "`.`sendedMailsUser` = `" . TABLE_USERS . "`.`usersID`)

				WHERE 1
					AND `" . TABLE_SENDED_MAILS . "`.`sendedMailsMandatory` = '" . MANDATOR. "'
					" . $sqlWhere . "
				ORDER BY `sendedMailsSendTime` DESC, `sendedMailsRecipient`
		";
	// EOF CREATE SQL FOR DISPLAY ORDERS
?>
<div id="mainArea">
	<div id="mainContent">
		<?php require_once(FILE_MENUE_SIDEBAR); ?>
		<div id="contentArea">
			<a name="top"></a>
			<div id="contentAreaElements">
				<h1><?php if(SHOW_PAGE_TITLE_ICONS) { echo '<img src="' . PATH_ICONS_MENUE_TITLES . 'sendedMails.png" alt="" />'; } ?> <?php echo $thisTitle; ?></h1>

				<div id="searchFilterArea">
					<form name="formFilterSearch" method="post" action="<?php echo $_SERVER["PHP_SELF"]; ?>">
					<table border="0" cellpadding="0" cellspacing="0" class="searchFilterContent">
						<tr>
							<!--
							<td>
								<label for="searchMandatory">Mandator:</label>
								<input type="text" name="searchMandatory" id="searchMandatory" class="inputField_70" value="<?php echo $_REQUEST["searchMandatory"]; ?>" />
							</td>
							-->
							<td>
								<label for="searchMailSubject">Betreff:</label>
								<input type="text" name="searchMailSubject" id="searchMailSubject" class="inputField_160" value="<?php echo $_REQUEST["searchMailSubject"]; ?>" />
							</td>
							<td>
								<label for="searchMailRecipient">Mail-Adresse:</label>
								<input type="text" name="searchMailRecipient" id="searchMailRecipient" class="inputField_160" value="<?php echo $_REQUEST["searchMailRecipient"]; ?>" />
							</td>
							<td>
								<label for="searchSendDate">Sendedatum:</label>
								<input type="text" name="searchSendDate" id="searchSendDate" class="inputField_70" value="<?php echo $_REQUEST["searchSendDate"]; ?>" />
							</td>
							<td>
								<label for="searchWord">Suchbegriff:</label>
								<input type="text" name="searchWord" id="searchWord" class="inputField_160" value="<?php echo $_REQUEST["searchWord"]; ?>" />
							</td>
							<td>
								<input type="hidden" name="editID" id="editID" value="" />
								<input type="submit" name="submitSearch" class="inputButton0" value="Suchen" />
							</td>
						</tr>
					</table>
					<input type="hidden" name="sortDirection" id="inputSortDirection" value="<?php echo $_REQUEST["sortDirection"]; ?>" />
					<input type="hidden" name="sortRow" id="inputSortRow" value="<?php echo $_REQUEST["sortRow"]; ?>" />
					<input type="hidden" name="sortField" id="inputSortField" value="<?php echo $_REQUEST["sortField"]; ?>" />
					<input type="hidden" name="displayType" id="inputDisplayType" value="<?php echo $_REQUEST["displayType"]; ?>" />
					<input type="hidden" name="submitSearchForm" id="inputSubmitSearchForm" value="1" />
					<input type="hidden" name="displayOrderStatus" value="<?php echo $_REQUEST["displayOrderStatus"]; ?>" />
					</form>
				</div>

				<?php displayMessages(); ?>

				<?php

					// BOF GET COUNT ALL ROWS
						$sql_getAllRows = "
								SELECT

									COUNT(`" . TABLE_SENDED_MAILS . "`.`sendedMailsID`) AS `countAllRows`

								FROM `" . TABLE_SENDED_MAILS . "`							

								WHERE 1
									" . $sqlWhere . "
									" . $sqlGroup . "
							";

						$rs_getAllRows = $dbConnection->db_query($sql_getAllRows);
						list(
							$countAllRows
						) = mysqli_fetch_array($rs_getAllRows);
					// EOF GET COUNT ALL ROWS

					if($_REQUEST["page"] == "" || $_REQUEST["page"] == 0) {
						$_REQUEST["page"] = 1;
					}
					if(MAX_ORDERS_PER_PAGE > 0) {
						$sql .= " LIMIT " . (($_REQUEST["page"] - 1) * MAX_ORDERS_PER_PAGE) . ", " . MAX_ORDERS_PER_PAGE." ";
					}

					$rs = mysqli_query($sql, $db_open);
#dd('sql');
					// OLD BOF GET ALL ROWS
						/*
						$sql_totalRows = "SELECT FOUND_ROWS() AS `countRows`";
						$rs_totalRows = $dbConnection->db_query($sql_totalRows);
						list($countRows) = mysqli_fetch_array($rs_totalRows);
						*/
					// OLD EOF GET ALL ROWS

					$countRows = $countAllRows;

					$pagesCount = ceil($countRows / MAX_ORDERS_PER_PAGE);

					if($countRows > 0) {
				?>
				<?php
					if($pagesCount > 1) {
						include(FILE_MENUE_PAGES);
					}
				?>
				<div class="contentDisplay">
					<table border="0" cellpadding="0" cellspacing="0" width="100%" class="displayOrders">
						<thead>
							<tr>
								<th style="width:45px;text-align:right;">#</th>
								<th>Sendedatum</th>
								<th class="sortColumn">Empf&auml;nger</th>
								<th class="sortColumn">Absender</th>
								<th>Betreff</th>
								<th>Dokument</th>
								<th>Mandant</th>
								<th>Sender</th>
								<th>Status</th>
								<th style="width:50px;">Info</th>
							</tr>
						</thead>

						<tbody>
					<?php
						$rs = mysqli_query($sql, $db_open);

						$count = 0;
						while($ds = mysqli_fetch_assoc($rs)) {

							if($count%2 == 0){ $rowClass = 'row0'; }
							else { $rowClass = 'row1'; }

							if($ds["sendedMailsIsSended"] == '1'){
								$rowClass = 'row3';
							}
							else {
								$rowClass = 'row6';
							}

							echo '<tr class="'.$rowClass.'">';
								echo '<td style="text-align:right;" title="' . $ds["sendedMailsID"] . '"><b>' . ($count + 1) .'.</b></td>';

								echo '<td style="white-space:nowrap;" title="' . formatDate($ds["sendedMailsSendTime"], 'display') . '">' . substr(formatDate($ds["sendedMailsSendTime"], 'display'), 0, 10) .'</td>';

								echo '<td style="white-space:nowrap;font-size:11px;">';
								$strMailRecipients = $ds["sendedMailsRecipient"];

								$arrMailRecipients = explode(';', $strMailRecipients);
								if(!empty($arrMailRecipients)){
									$countRecipients = 0;
									foreach($arrMailRecipients as $thisMailRecipient){
										$thisStyle = '';
										if($countRecipients > 0){
											$thisStyle = 'border-top:1px dotted #999;';
										}
										#dd('thisMailRecipient');
										#echo '<span style="display:block;float:right;margin: 0 0 0 4px;padding: 0 0 0 4px;"><a href="mailto:'. $thisMailRecipient.'"><img src="layout/icons/iconMail.gif" width="16" height="14" title="Empf&auml;nger &quot;'.( $thisMailRecipient). '&quot; per Mail kontaktieren" alt="per Mail kontaktieren" /></a></span>';
										echo '<span style="line-height:14px;display:block;margin:0 4px 0 0;padding:0 4px 0 0;font-size:10px;' . $thisStyle . '" title="' . $thisMailRecipient . '">';
										echo '' . substr($thisMailRecipient, 0, 40) . '';
										#echo '' . preg_replace('/;/', '</div><div style="font-size:10px;border-top:1px dotted #333;">', $ds["sendedMailsRecipient"]) . '';
										echo ' </span>';
										$countRecipients++;
									}
								}
								echo '</td>';

								echo '<td style="white-space:nowrap;font-size:10px;">';
								echo $ds["sendedMailsSender"];
								echo '</td>';

								$thisSendedMailsSubject = $ds["sendedMailsSubject"];
								echo '<td style="white-space:nowrap;font-size:11px;" title="'. $thisSendedMailsSubject . '">';
								$thisSendedMailsSubject = ''. substr(preg_replace("/[ ]*(\/|\-)[ ]*/", "$1", $thisSendedMailsSubject), 0, 100) . '';
								#$thisSendedMailsSubject = preg_replace("/([,;:])/", "\n", $thisSendedMailsSubject);
								#$thisSendedMailsSubject = preg_replace("/([,;])/", "\n", $thisSendedMailsSubject);
								$thisSendedMailsSubject = preg_replace("/([:])/", "", $thisSendedMailsSubject);
								$thisSendedMailsSubject = preg_replace("/.*Nr./", "", $thisSendedMailsSubject);
								$thisSendedMailsSubject = preg_replace("/([A-Z]{2}-[0-9]{10})/", "$1\n", $thisSendedMailsSubject);
								if(preg_match("/^KORREKTUR/ismU", $ds["sendedMailsSubject"])){
									$thisSendedMailsSubject = 'KORREKTUR ' . $thisSendedMailsSubject;
								}

								$thisSendedMailsSubject = trim($thisSendedMailsSubject);
								$thisSendedMailsSubject = nl2br($thisSendedMailsSubject);
								echo $thisSendedMailsSubject;
								echo '</td>';

								echo '<td style="white-space:nowrap;font-size:11px;">';
								echo '<a href="' . PAGE_DOWNLOAD_DOCUMENTS . '?searchBoxFile='. $ds["sendedMailsDocumentNumber"] . '" >'. $ds["sendedMailsDocumentNumber"] . '</a>';
								echo '</td>';

								echo '<td style="white-space:nowrap;font-size:11px;">';
								echo ''. $arrMandatoryDatas[$ds["sendedMailsMandatory"]] . '';
								echo '</td>';

								echo '<td style="white-space:nowrap;font-size:11px;">';
								$thisUsername = $ds["usersFirstName"] . ' ' . $ds["usersLastName"];
								if($ds["sendedMailsUser"] == 0){
									$thisUsername = "Buchhaltung";
								}
								echo $thisUsername;
								if($arrGetUserRights["adminArea"] == 1){
									echo ' (' . $ds["sendedMailsUser"] . ')';
								}
								echo '</td>';

								echo '<td style="white-space:nowrap;">';
								if($ds["sendedMailsIsSended"] == '1'){
									$thisStatus = 'versendet';
									$thisStatus = '<img src="layout/icons/iconOK.png" width="16" height="16" alt="versendet" title="Mail wurde versendet" />';
								}
								else {
									$thisStatus = 'nicht versendet';
									$thisStatus = '<img src="layout/icons/iconAttention.png" width="16" height="16" alt="nicht versendet" title="Mail wurde nicht versendet" />';
								}
								echo ''. $thisStatus . '';
								echo '</td>';

								echo '<td>';
								echo '<span class="toolItem">';
								echo '<img src="layout/icons/iconInfo.png" class="buttonShopOrderInfo" alt="Details" title="Detailinformationen ansehen">';
								echo '<div class="orderDetailsContent" style="display:none; visibility:hidden;">';
									$thisSendedMailsContent = $ds["sendedMailsContent"];
									$thisSendedMailsContent = preg_replace("/<style.*<\/style>/", "", $thisSendedMailsContent);
									$thisSendedMailsContent = preg_replace("/(<[a-zA-Z])/", "\n$1", $thisSendedMailsContent);
									$thisSendedMailsContent = strip_tags($thisSendedMailsContent);
									$thisSendedMailsContent = nl2br($thisSendedMailsContent);
									#$thisSendedMailsContent = preg_replace("/(data:image/jpeg;base64,/9j/4QAYRXhpZgAASUkqAAgAAAAAAAAAAAAAAP/sABFEdWNreQABAAQAAABkAAD/4QMpaHR0cDovL25zLmFkb2JlLmNvbS94YXAvMS4wLwA8P3hwYWNrZXQgYmVnaW49Iu+7vyIgaWQ9Ilc1TTBNcENlaGlIenJlU3pOVGN6a2M5ZCI/PiA8eDp4bXBtZXRhIHhtbG5zOng9ImFkb2JlOm5zOm1ldGEvIiB4OnhtcHRrPSJBZG9iZSBYTVAgQ29yZSA1LjAtYzA2MCA2MS4xMzQ3NzcsIDIwMTAvMDIvMTItMTc6MzI6MDAgICAgICAgICI+IDxyZGY6UkRGIHhtbG5zOnJkZj0iaHR0cDovL3d3dy53My5vcmcvMTk5OS8wMi8yMi1yZGYtc3ludGF4LW5zIyI+IDxyZGY6RGVzY3JpcHRpb24gcmRmOmFib3V0PSIiIHhtbG5zOnhtcD0iaHR0cDovL25zLmFkb2JlLmNvbS94YXAvMS4wLyIgeG1sbnM6eG1wTU09Imh0dHA6Ly9ucy5hZG9iZS5jb20veGFwLzEuMC9tbS8iIHhtbG5zOnN0UmVmPSJodHRwOi8vbnMuYWRvYmUuY29tL3hhcC8xLjAvc1R5cGUvUmVzb3VyY2VSZWYjIiB4bXA6Q3JlYXRvclRvb2w9IkFkb2JlIFBob3Rvc2hvcCBDUzUgV2luZG93cyIgeG1wTU06SW5zdGFuY2VJRD0ieG1wLmlpZDo3QjRCODdENDAwMzExMUU0OEUxOERERjU0NEYyMUQ1RSIgeG1wTU06RG9jdW1lbnRJRD0ieG1wLmRpZDo3QjRCODdENTAwMzExMUU0OEUxOERERjU0NEYyMUQ1RSI+IDx4bXBNTTpEZXJpdmVkRnJvbSBzdFJlZjppbnN0YW5jZUlEPSJ4bXAuaWlkOjdCNEI4N0QyMDAzMTExRTQ4RTE4RERGNTQ0RjIxRDVFIiBzdFJlZjpkb2N1bWVudElEPSJ4bXAuZGlkOjdCNEI4N0QzMDAzMTExRTQ4RTE4RERGNTQ0RjIxRDVFIi8+IDwvcmRmOkRlc2NyaXB0aW9uPiA8L3JkZjpSREY+IDwveDp4bXBtZXRhPiA8P3hwYWNrZXQgZW5kPSJyIj8+/+4ADkFkb2JlAGTAAAAAAf/bAIQAAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQICAgICAgICAgICAwMDAwMDAwMDAwEBAQEBAQECAQECAgIBAgIDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMD/8AAEQgATAJKAwERAAIRAQMRAf/EAPcAAQACAgIDAQEAAAAAAAAAAAAJCgcIBgsBBAUCAwEBAAAGAwEAAAAAAAAAAAAAAAECBgcICQMFCgQQAAAGAgIBAQQDBwwMCwkAAAIDBAUGBwEIAAkREiETFArwMRVBUSIWF1kaYXGxttZ3l9dYmBk5gZHRsiPUdpY3V7cYocEyQlQ1VTZ4qNjhUnIzJCV1tScRAAEDAwEEBAYEFgoNDQAAAAEAAgMRBAUGITESB0FREwhhIjIUFQlxspMWgZGx0UJS0iNUdJS0NVV1lVZ2FzcYGaHBkuIzU7PT1DZicqJDY3MkNESFVzg58OHxwoNkhCVFtdUmRv/aAAwDAQACEQMRAD8Av8cInCJwicInCJwicInCJwicInCJwicInCJwicInCJwicInCJwicInCJwicInCJwicInCJwicInCJwicInCJwicInCJwicInCJwicInCJwicInCJwicInCJwicInCJwicInCJwicInCJwicInCJwicInCJwicInCJwicInCJwicInCJwicInCJwicInCJwicInCJwi0F377M9O+tKuUNibX2iTEMSASxPCIQyNy2T2LP3BEAAj0MTibUA1YpAUI0ATlikSZuS5GHJ6grGfPCKMZm+Z665pPS0otJibL/AGyUlwOUTeoqlsOpXmDSHYXMZ+0EpzNUUuMG811JXAT23GJDiiXUxYn92ablOMBJvpIpUeuve6rOx7UyrdqarLJZ0k5afdy+BDfUr+81jPG0YkcpgT8uTJG0ShaxuZQwlKBpEnxqXJSgJIAGhDgi3i4ROEThFHd2a76566Ncm+/S6lNulQ6WnBqwSwwE6IrvGFE1E6BA8GyNRFpgWAluw15yIn4POTfV7Bh8e34MnkbfE4+XJXfF5tCwudwipoOodJVXaB0Pn+ZetcXy/wBKtifqTMXsdrbNlkEUZmlNGB8jtjG13uOwKBwPzT8iz7P6PLGf1cbbtmf2Nfs8oI82tIjaTde5fvlm+z1Xne4ftZjsGR4MpAfiNT9Kfkn5vH/zbNn/AKfuR/K1o/5a59y/fKP6rnvd/a3CffOH5lP0p+Sfm8f/ADbNn/p+4/K1o/5a59y/fJ+q573f2twn3zh+ZXL4t80vFzHFGXYeidksLKYcWFwX19d0CsBzRE5z/hjU7LI43VxbgYAPn0l4VF5Hn2eQ/Xzmt+amjp38DppovC+JwHw28VPg0XSZ31aXe/wdo67Zp+zvg0V4LXI2kkp8DY3vic4+BtT1Kb/Sbs70+39IeUdBWIfidxpOQslFRT9pUQe148iUFliC5LIY7C+KXMuDxiJw4oBrG4w4seCzx4xgWa5schZZK3F3j5Y5rY7nMIcPY2bj4DtWGGrdHau0DnZdL64xl9iNRQeXb3cL4JQOhwa8Diaeh7atPQSudb+bdp9FtVrL2cU1+rtAuu8R3AYOikiaInvZ0ikrVGiCwyFW0viduAnNdQmiEJKb5CDOPGPPnnJdXEdpbSXUteyijc91Np4Wgk0HSaBdbhcVeZ/NWeAxwacjf3cNtCHHhaZZ5GxRhzjsa0ve2rjsAqehROaK9/pG5+2VYatKtR3Wqj7ObZ6uQzgV3s05TthsEii2VHJVMfS11HDzwuJKPJITAqg+7FnznAvq5TWnNZ4bVE8kGL7btIog93Gzh2E02bTU1KyG58d0jnP3b8Pj87zRt8dDj8nePtYDbXkd04ysidM4OawAsbwNJDjsJ2KxfyrVjOnCJwiqV2h80qkrh9tBuDoi+vjZWkqn0bNdMbHx9sUPBECkrxHFLmU2DqdYJFlwMZxGgIEcZkGB4Dkefr5Rd3rzBWWb9ATdt6Q7VsexlW8TwCPGru2ippsWV+k+5bz21ryedz0wNrjH8vm2F1eF776KOfsLN0jJz5uRx8QMT+Fu91BTerBu6e26XTzT6z9s1ECV2GnriHtUrxAkkiTRlW95dVzWhKbgSJS1PCZvEWY6YyI0SU3HgGfwfbyr55W28D53+RG0uNOpoqf2AsW8ZZTZbIW2Ns6G6up44o+I8IL5XtYyp6BxOFT0Daq84vmmFABjB/uBOuRAEIOcY2ej+fwgZzjPj/8Ajn1ezltvyt6SpWl1Q/4L98tgh9Vz3tw8x+j8EXNO2mVgP7S8fpTSj+QA7fzno/8AxN8flb0h8tc+5H46j+q473H2uwn3zg+ZT9KaUfyAHb+c9H/4m+Pyt6Q+Wufcj8dP1XHe4+12E++cHzK99s+aaafiysSLQqcpW0QwYNURjYGDSFwKLznODBgb3uFQ1KcIGPbgPxYci+r2c5Yea+j5HcL5Zo/C6J1P7niXV5P1ZPe/x1ubiLB426DRXhhydoXnwASPjqfACpV9Qu77QjcGTtNcMM7kNMW6/H5TMFU3+wlV9I5Mp/8ApQgTQ+QFOL3Xk0cDjVOcARNT0scPSUMwRAS8erlZ4rOYfORmTFXMU4G8Nd4w9lpo4fBCxJ5kcoOaXKDIMxfM7AZTCXUhpGbqFzIpf8VOOKGX/s5HKXrnbK3KqVWL80ujgL3YzeHRF9eGuvZdOIwY6Y2PYG9Q6kweVOsXUOZbWOplWUf2gY1COARk4zIAjwHI859vKLudeYK1zvvdl7b0j2rY9jKs4ngEeNXdtFTRZVYDuZ88tS8lXc/8VbYx3LdmOub0yOvYm3HYWskkUx83I4y8Pjdws3uFCN6titK4Lo2NrmEsRIXFAjXBKEL1iKCrTlqAliF4DgQgYM8Zz4x58crRYqA1FRuX0OFFcYmkkBDofLJcYkG4FxWNP0kMQFmhTjWgZGtU5iSAPEWaEkakKX0YHkIsByLz4z48cKBNBUqrHWXzQySw5tU8WO0WfWJstGxK0gYX3GxkfdFDGVZEuY4qmejGYFUohOOWvL2E8aYKgnJuAZBgwOc+rFF2uu8FeZz3vQ9v6Q7V0e1lG8TK8XjV3bDQ02rK3UXcw56aW5MDn5l7bGN5cnHW18JGX0T7jze74OwPm4HHxntG8TK1btruVsPlaLFNOEXALStOu6Sr2X2xbUxYoBXEBY10kl8vki0CBnY2ZuJycoVKTheRmGC8YASSUEw9QcIJRQBmDCDJQJAFSqyb180tUyewlCSNabXHJaWJXGkJ7GHO4ow2I7NgM5wW/tdJuLPlVlApD4NLTKn1I65Iz7UmDvBGaDk5kaRhyHo99yag0MgaTEDuoX7t/SBwjrWZeL7gPetzGgRzCs9MP8xfB20dm+4hZk5YeHjEkdg5wlNW+M2J3DO4U4YjVWTqJvSp9mKlg16UdM2ywKtsVnC9xOUtWFJZKxNhQciWo1iFcQkcmZ7ZnNIejcG9YSQtb1yc1OoKLOKGANdNc17Q9hBYRUEbQQdxB6QVhvJFJDI6GZrmTscWua4FrmuaS1zXNIBa5pBDmkAgggiqy7yZSr0XNyRMza4O7moAkbWpCrcnBWb590lQoSDFStQZ6cZF6CE5QhZ8YznxjhFWFhvzRutrzZyltmeuVwQmgVDquSMF7pnqPy9z+wy1mSGqZTKompGnkkYjTqkD8SPKJY9K0RIwiPIAH3giqPZrrTLsy/BOuOC9Y8sq5pawvGwtDz4ta7NtATsFVk5d9znvE23KK053xYB91oK7s23gdbTRT3Udo7ay5ls43GdkLm+OXBrixhDnhoKsswWfQe0oiwT+tZhGZ9BpU3JniMzGGvjbJIy/NawsJyVwaHtoUq25wSHlDwIIyjBBzjP18rBYxNcHDiaQWlcv4UVpX2B7hp9D9VrC2bVV6rtIuCLYegBCEUlTRE94Pl8vZYknz+MKtofSEBaI15wcPIkpvqCXkOPGc+cfPd3Mdpay3k1exijc91Np4Wgk0HSaDcu30/hL/U2fsNNYoMdlclewWsAc4MaZriVsMQc47GtL3t4nHY0VPQortDO/MrdnauBawqdSXSqDZ6wTh7STcd3s06ToDIU0lOxqJRH0leRtQb9olG+kJoVWMF5x5yEXnlNad1nhtUTyQYvtu1iYHO42cIoTQU2mu1X759d0/nH3bcfjcnzSt8fBaZW5lgtzbXkd0TJCztHhwjA4AG7nHYTsViXlWrG1OEThE4ROEThE4ROEThE4ROEThE4ROEThE4ROEThE4ROEThE4ROEThE4ROEThE4ROEThF/M00sgsw44wBRJQBmmmmjCAsosAcjGYYMecBAAAcZznOc4xjGOEVMnv++ZBBrpHHfW/retitX+8kMjNjtx3S1u0bl+ahwmTJVIY7XDGoMXs0qmqhWcIlxcDiVzayYSnpTCRrDPKSZreI0UCaKgNs/Ze2d7u8RunaSwbHt5ZMIgS5wqxpu+K5I3rYrh4VN6tCyLheG9uwzyYByVekKAUNOtxjBofAyhD5G8PRvUDU+wpkKjgLJYWqTNqlM580QmHRDRR+2bl06sZxdgQmkJDM7Lqi4KSsBrMbkLs5tj8WfYr40fDsqcx0dEMjwjyUaJQT6ePafHUywrqDbfYH1R7CjnmvtoPVaVMueY+rtQqZmMlkRoyHuaVOtb5HslQlcPs2nlWjeUBhKkr45C3viMg8koRmTPUUKJo4f2Sh8Rdkr1Pdr1Q9kcDl7M3zyqHi+6fWpG6y2eq3d4Ph0mZnIjB8ctKvG+WkoZokhcpLCYHKN0TlObSsJMSqwBGEAhyKKl44ROEVeX5mX+rxhv8A4tKH/v5XylNc/wBUch9LO/aWSfc3/wB6zl9+NNn7cqtV1B6UUtvxuRIaUv4ycigMa1um9po0MDmbnB1yqVNVk1lEUBjg6s3pWnoCGyWKhYIwIIcm+gWfOMeM2o5TYjFZRt8clbwz8Do+HjbxcNQ6tOqvStoXrUeaXM3lxnNExcvdQ5fBw3dhkHTtsrh0AmcyeNrHSBvlFjSQ0ncFZt/Rs+sT/oewH84ue/47y8PvR0p9rrT9wFqc/Se7yf4e6r+r5U/Rs+sT/oewH84ue/47x70dKfa60/cBP0nu8n+Huq/q+VYB2T+Xk62q+q6TzhFsRberxEeZXE8Njzq7m1/gjW4hSKTm1RKUVnkLGlW14Vlg+IIAcnOOTAGAkwkwWDQ/Jd6E0jexmOSwhbUb2VY4eEEHf8BVLpnvn96jRl+3KYzXeclcwgmO8lZdwPpt4ZIp2ODmnpALTTcRvVPqnLbn1K2dU1/Vc9jSWhU9kNLlBpGwkL24mXoSpuRHlDQFuV5A5Gwa6I54LNaFfnIk7iVgQcHlFjDZHTEkmmuYpw2HmdNjX3RhdtqHsoTUgbOOM/JDqPRVbie8dZWHeO9XzDzo5s4qDE8ybHT8WUtn8HZyW9wJ2xdjEX/PG2mSY6rbZxIBkjc0VaCb8nzAQsi6o9jxCAIsQjqtFkAshyIGc2nDs5CL0ZEDOcZ9mfGc45kJn/sFe/Sk38m5aGeVn50tL/jJi/r6BVRukL+ts1M//C7B/wCyF+5ZPkv9lr36Tb7cLdr64X82OjPxnuf/AG+Zdi1zIJaEE4ROEXUrbNf9c7T/AL5OxX+0+ccxj1F+dAfdC39rGvRt3cf+GNN+J+oPri+XYJd2H9Tns5+9FDP2xRHmR2X+xtz/AIiT2jl56tCf1wwX3VsfriJUtuvbXOAbc7/a0a3WwplRFZ2W6XIZLiIbIFEWfV5cFpmZztkSkviMBitGm+3mEjJ3uvSMZXqDgQfPnGOfKnF43LZS5gycEc8TbVrmh4qAeOlR4aL0K+tN5i6/5bcsNN5Tl3msjg8lc6muIppbOUwvlibZl4jeQDVgf4wHXtVu79G260/+zNgf5wE7/wAZ5fX3m6TH/p1r+5/51pF/Sr7zf4fap+rXfGT9G360/wDs3YL+cDO/8Z495uk/tda/uT8dP0q+81+H2qfq13xlxGX/ACz/AF9vLSYmh8n2Tr95wA34V8b7meJDgo8YQ4JNUNEmTOTctAQLHqwWIOAi8+BeceznFNojSEzCx+OtqEdAIPwCCvuxve9702Ju23tjr/UvbN6JLgSsPTRzJWOaR7IVRnfDTOT6Q7LTnVexJQgsZOzs0YsSCT1uIEyOUhgUoWvBMTe3ZEiLTAjNhx50jygpQc35LLCpIAoTZBgWMYsZrfT/ALxM5b5DASyRxStLo9tXMcwjiZX5NhqDR1dhoa71uu7mvPH9OLk1qHl3z/xdhk77Fyw2908RNZDeQXbHmC6bGKi1vonRvBlgLfGayRtKlqvCdGu0ti7XdfNfSe3pGtmVn1lK5nSMumbrkZj1MB14vJTR6TP6sWMfaUgdokvQDXqvORqlmDDTM5NGPPMh8DkvTGFtcoQGunha4gbg7c4DwcQNPAtB3OLQQ5V82dSctWzOuIsHmbi0ZK7ypImPrC539mYXM4+jj4qLr1th/wDrbZb98+/P9rEv5j7nPzr/AOsIPaMW9zkz/wAKi4/EjOfXt4u2Lif/AHWjWPvMDPjP6mcNyb2cyZd5R9ledpnkD2AuQcgpljC7AiMpq2ywY9Qx1jPQBD6gh8iFFnUIfwh5CEOPOfrznGMci3yh7Klf5J9grqxtcs4zZmposfV+X7VjOP1vyt1/nmMeA/Oq37oT/FevRZzxr+qxjcPwJwHxbRdsPj6sfrY/Y5k2vOqvAxhAEQxiCEIQ5EIQs4CEIQ485ELOfZgOMfXnhFQt7yezk3c+2l+rlMvhanVGiJoMMqkTQqGNDf8AdcUVKUSkRKtMcNK9VPV7sAZaPIcCTOz6TlUHJpCVMYKzXNDWgsYXabxT6Xsrfn7x/e2H5AHoe8b+pvsrbH6tvue/lEzkPeB5kWvFoXF3X/lFrK2rcjfxH/OntOx1pZvpwAgtmuQN7YjWGkun7vUUm57Rk1XITtXmiepqqeLvCEWWNBYao4tIWnNS4Bk3EPJeDimhQ++fgin1SUiyL3mRem3UfL3MO0p74wD2nlCCnjGCn8J18XyQZTyNu9bBLv1gnKm170TOQsjme941tH5ztfnDM2ZAG2mzxfNQfnD7oGguyGg8DSTKd0u9lBugt+G1laDrgjUzY2WNSSbKXFb8O20ZbjkYSzs1up/eYGmQw+XCGQ3ywOfdFEiCmdMjD7hXg+uOVusQ9jdMZJ/jgf5O4neN5hJPVvj8FW9Sws9Zp3Rjh76fvKcvLThxlxIPfBbRN2QzuIa3KMa0UEcxoy9psZLwz+S+QjsFU6ghWQQqSnkqUykkChMpTjAcQoIOAEwk8g4sQizSDSxYEEQc5wLGcZxnxy+S01rgVvf6JrQ/e7mv7WnPhF1MkecstECiq0tA5uhw2uLIEjcyJDXB5XrnQSJtQo2tvTBEpcFyhWqAAogrAjThiwAGBCzjGcRsji7jOa2u8TaFouJryYN4vJqC51CeitKV6CvU1yy5pYLkn3IdKc1NTRXU2n8RpDEvuG24a6YRSiKFz42uIa8s7TicwkcbQWjaQpH+vbsr2D66pW4OdMGJrBpWSu5xtla3yhctbY6odClo8vT3Xhh4gZquziVODC1pAyMIFpuRgVpyz/CgFaaY19lNLXXve1eyU20Z4Q5wrLENwr/GRdRFTTaCdyw67xncb5ad5PTJ5/8AdFu8b6Zv2unks4HNix+Tdvk7Jpp6OyQdUSQyNZE+SrZGROIer/mmO9euG+lZBs3XubBeim8wlBNYQ9p8sdiVrIRFYMOjU8iSgwS5lcic+fdm4yajWF+Dkpx5AwGCv7bXNve27Lq0e2W2kFWuaagjrB/5EbitH2cwec0xmbnTupLO5x+oLOV0U9tcRuimhkaaFkjHAEHqO5wo5pLSCdF/mE/6qi//APKSk/8AbRBefBqD7BXv0rL/ACblWvJn88mkPxpxP1/bqrP0W/1rmuX+RN8/tKScspyZ2ZS9H/dme3C3JeuK/qZoX7uZD61C7ETmQS0SJwicInCJwicInCJwicInCJwicInCJwicInCJwicInCJwicInCJwigr3G7+NOtKt8am0VtYT0gkkwkMJarJsJYEtBDKpaLNZHc+EyZ0Wm4GWqaCZCmQpnowwSbDWicQLPJxRZuARAJ3Ip0QDCYEIwCCMAw4GAYM4EAQBYxkIgixnOBhHjPnGcezOOQReuuXImxIocHJYlb0CQoR6tauUEpUiUkHtGcoUHjLJJKDj6xCFjGOEVfDc/5nTqv1BOfo63Ws57I2THnw1iXQKgW3EnABQkKyavU5nS81sr8SNMZ6SQjLcjcHnDx7v1FhMMBEAncirvSn5v/e+/pmqh2lWl1Px8RYVi0p1tqRvErStDEEgIBPk7cCH2todDWltNznIlq15TI/fCLCIecZ92OJaW71AEFYTnPzK/ZrUxGXKXbsaCSyZAyad+S+oqTnNsNJKkryLDQ7TVldWONkG5MD7swSN0WFh859Jws8gBU0UVyWtvnYNt2Zmck9r6d0RPZAem9TW6wyZzWvGdAvwaWLOFLC5kTpYqbTCfWAIPtHB5YshFkwz05AObs3KHEFHrvh80h2MbtU5JKFbwQLXCvZ0mUtM+U059vkTuUxhT6cKYiKbOK/49kjrsT6iXICEshQ4JhCTGG4SmqCD5hHt27lAu6t6rftK3DS5tjlhA2uOGxclXYbXZLlY0r8JjwG5QuaQJpAlKBXgOQHAwMAhAFnwIOfGcchFRRS121VnDTKR613lqGKnGaOJMRmWbl0ouUUm+pDnhXRrlczS51JsIzQqfPhJi92quYVsnFOm4HvxrGE2HCMXiNGjJWH/ORTYVyKPiJ2bItwU986mxVU0JJ9Im9gL1qLIUkoj7TfawmMVACrzXcYi06lykteQlsBF0WcBIGtj6NMX4OU+rMzh8kNxUFyOevTBK9j2Xa7MAlKqWOyxXWm6tFMrdJSJnU1xxeKrUk3sNgbWEkmYCq+wI/DHJ+LEnGhUtC1I5oFJpRRBJiiUGmz5FRUnfUfojsYHfrTTbfSSuXiN1JbEntORx6azy6ozDS81LBbEaGOVzoytks+UWxPq9wnyY0r2FakGNyVKMZyYSEJS0qJcSKIu0Fx5+79f6nJUThFXl+Zl/q8Yb/wCLSh/7+V8pTXP9Uch9LO/aWSfc3/3rOX3402ftyoI+ghtd3ncrZBnjyZQrfnbrlvttY0aQ4tOqVu66zqbStqZKoNOTlEKD1hoAgGIwsIRZxnIg4x5xbbkt5OR/t4vauWwf1wtfT2gqb/RuS+uYlHpH+tbssTsLGmddL9zDHQhmbCHIwUmUm5McCkRIFo/e4uDGDciUgFn1eMefr59t/g+a8t/PJY3rG2TpnmMGRgowuPCKdmaUFNldioDQnOb1YmO0PhsdrbQWRu9aQYq1jv522t05s14yFjbmVrm37GkSSh7gQ1oINQ0bl9f+jd7Hv5Fu5n+cSn+OHnzegecP0dF7qz+bVVfly9U9/s6yX1Jef/IrFFoaX7XVa1mSu79PtmY3Ho8VlYdLJzC3+xGRhIJxg49wPVtkhnpjMkRhxkw1UIkssgOMjEMIcCzjqsrprmxNbubcXElxERtZHOASP7UBlfYrtVz+V/eH9Vfi89BcYnS1vgMm2QGO6yOImuIonA+K8ySzXYiodvH2fi0rXYCsr9b9i68VxvXrhY200aIsGhy5XG0bGoKciCo3XVsPr23IqzuqdJBYOTTeBRZ3VlC+H9YCmxUaS5mBPCl9IeLlXc4OxzL8fkYHx6icXNjkfuFB40QYRVkhoak14hUCnT23rPNP85dZ8q7HmDoXN2WR7ucLba4u7OzHzxz5CG2+SluWuc2+sQ57WRxt4BavcHvY4+My6r39IVi3ql2dEjSnqcN5FdOq3JBYzMpW5vs6IHrVp3oxn0J0pIciGLP4IQ485zjHL7Zlhkwt4xu1xtJQPc3LSVyzuIrXmZpq5mNIY9RYxzj1AX0FSqlXSo5oWjtm1EVOKktMnWk3kxpTTc48HOrzUchLbEJePPkRysRIvTjH3A5z9zPLG8mXsGXvGE+M60bQddJG1+Et3vrgYJncq9H3DWOMDNUThzhuaX4+bhBPRxUNOui7GrmQy0GpwiZzjGPOfZjHCLqTNhlqR7V7JuTSeW4IXmwL+WNKogQBkOCV1syYnNxyUwAhFnFLylBYihBznBmBhzjOcZxzGLPnj5olrNp9IQjZt2gMr8Km1ejPkC5uK9V/LcZD5zEdF52SrtniSXF72bvYfUFp6QQdy7B7uyAMvp32eAYHIRl1JDwDBnHgYRhkURCIIsfcyHOPGeZIZX7HXP8AiJPauXnv0Ns1jhAfttY/XMSqVdLY/d9uulxnj1e7W7Mj8efHn06yWbnx5+57eWA5M/Zu6+k2+3W+D1wf5otJ/jZdfWBUtUh+Z/sJrlEzZG7SxoWIIvO55D0i5VcRCVQ4EQuYvkTw4mpQsJmEwl4mXJ3u/UL0YH48+zlyMxzL0/hMpLirxtwbmEgO4WAipAdsJcOgrXHyk9Xj3hudfLvG80NFegvevlWSOg84vHRzUilfC/jYIXBvjsdTxjUUK+V+lGWf/Ikj/wDDQV+57nX/AJX9L/KXX7hvzSuP+qe71w2k6Zp9Pv8A6OuMy35oLYFcxrkcC03rNkkign3La9TK4HZzZWlTk0sWFqljj0WG4PoAkBGD4UClDkQh4H7/ABgGQD4pucWm44yYobt76bBwtaPgku2L7cN6pvvK5C/ZbZTIaWsbRxHFL53LcECu3hijhDnmm0Cor1qAiYT3ZrfzaN5eFxb3shthdDshRoYTCEXwiFjbAKDyozE2lAtcnBuqan4hhab6Fj24AIIAM49QpOPGLIre3TdT80sxHPFbmDGxjha4g9nEwmrnFxp2j3dTemg2BZ16Vve7l6svlPf4m/z0WoeamReJ57eF8Zvb+5jYWW8DbeN0gx9jDUgvuHB/jPkIc93Cuwm6u9LVmg+mdZ0BIHppk1hEqJBO7VkLEmMTszlZU9djpDJ07OYoCWtXMkfMUFtSBSoAUcoRISjBllZF7oGRuNsIMXj4cdbV83gjaxtd5DRSp8JO0+ErQFrnWOZ5h60y2vdRFrs7mchPeT8PkiSd5fwMrt4I28MbK7eBja7V1ruw3sdtk8Y/1o37j+1bEw8/8PMdc5+df/WEHtGLf3yb/wCFRcfiRm/r27U+/al1c747Fdgewt11RqPN7bq6eAqFZBZ0z2TVjM2Oraz0lXkecSkrRJLSjTygwjkLUsLEE5GV6xByMPqCLAs3T1jg9Z5PIxzabvfNrNsXC5vaFtX8RPFQAjcQPgLWf3TOc/dE5b6JyWL7wui3am1RcZITWtwLOG47K183iZ2PHLNGW/PWvfwgEeNWu2i0B/oTuyv839ZX8LVH/wAePKQ96PNX7aj3Z3zKym/St9WN/smf967T+lL5rx0x9jbI0Oz276B2SnaWdrcnZ0OzaVLLMlNrcjOWrzApEt1qFSwQEhA84JKLMNNz+AAIhZxjMRpHmt0ZUe7O+ZUHd6v1YvCSeUz6U+1dr/SlqZQ6hEpt/VxS3E5Tt5+xesRyEj0+j3CIy4oGNKT7vAv8H7snIQ+nznx48covR0V3BzFt4b13HeMuZQ91a8TwHBxr01NSsxO9vltL5/1eOYzeirTzDRt3p7FTWNtwCPze0kmtnQQ9mC4M7OMtbwhxDaUqV2vOPqx+tj9jmU680Sq8/MA9oa+qWFXoPrvJgo7asyLhV7BT1hXmhdqcqV8yYSREWVwQqistFoWomINILEP1HNbCM5XgADlCE7FFa41ZFpXFdpFwuyswLYWHr6ZHD5Vn906jeumWnc77r2Y7z/M5uGmEtvy4xRjnzF23ZwxE1ZZwO3edXdC1v8VFxzOFA0OqYUfTTHZSiZHyd8XVfrhQUMbZtsRZzC2iUK4XCFi0pjg9TVwR6QonK+b0fchZok2iMCMsv4pyHgYUmAG2c0HpZ2or+TUecdXDwPL3uedk0g8Y8RPyDPKedx2N3Lbh36O83ZcgNEWHdw5FRCHmdlLGKzgt7Fpc/EY2QCCFkDGVcL68r2Vo3bI1pfcGr3NKm7bu+aHJdZ0uoifrRhCbXINaBqcNVrbeQiQYgZjZluMaHLBMdyBQ8HEDyaqWeRHGuAhqcjEaLI83Gfze0ox7msbcuY0kAhgoQNlRV24jd4Fr9svVS97O+sIru4hwFvJNE17opr9wmjL2hxZLSFwErCaPo40eDRx3qu2kQ5GU9tytpwnjhjm7Jo8zuTtiTLyYWrNNw0MskdcpkxTu5tzaZhIoUYLDhTgvBmcYELPLF6ju8HNmTkdNGaGF54+FwDTHJWviUJ8WvjN+VNRuW6vu4aK544Xk8OXHeZtsZk8hZwusm3EUxu2ZHGuj4Ay+D420njYewkeaiZgZJsfxK4J8vj2eHSJsaOuzYKShMl0LYFJ+qk/kLiYNxsivWcZihdT745OKwYnCxatbzw4awgxgxzjCcI8YEchUjFkXoPV7NUYzhuS0ZqAASt+WG5srR1O+S6nVHSF5/u+v3Vb7ux8zDFhWSzcp806SbE3Dqu7IA1lx0zv4+14hwE7ZbcskFSH0s729/omtD97ua/tac+V2sM11T1EKD0kp1ZUpjjkqlPeuqx6dQnNGUenPJueujCj05xQgmEnFGhwIIg5wIIsYzjPnmM+F/Oz/AOPn+I9eiPmjs9VHGRv94WH/AJW2V5jtR6OK/wBwTpJsNrYpZ6f2xUowLn1OaThJVd9KU2C/JNmtqFMYc1zE1KDJaeSIQ4WZHgAVgVJWMei+OptJ4rVNr2N63humj53K0DjYf+s3rYdnVQ7Vpf7vXea5o92nVPp/QVyJsLO9vnuMnc42V8wbPHYNsU4bsjuYgJGbA7jZVhpsxuU7YaEbHrVzCbYGp+19aFEN0lj70lHgD7FzF3vk7VKmf3gI5c1JyhSj96hWEiUJBiDg9GcmVl/gWNhudVcrMn5vcDtsRI6tKnsZR1sP97lA3jp6QRtW57Laf7sXrOuXpz+n5fQXOnHWwa5zgz0lYOp4sV5G2gyWML9kc7KmMHxHRurG6d3cnuYqPsB6pL0qadtKWkdsWhZSip3q5c64cI3YpLbcEIG5y6mZKcQhxJmQwkjKg5tNAW7tnrEWcUYWV8UZeT3xYrU2lLy9xclaWkvGw7Hxns3bHt+IRsPQVqHm5EczO753ltJaL5m2Btrp2qsU62uY6vs76EZC3pNaT0AeKeXGaSxHxZGDYToh0W/1ruuf+RF7/tKS8tlyZ+yd59LM9uFsy9cV/UzQv3cyH1qF2IvMglokThE4ROEThE4ROEThE4ROEThE4ROEThE4ROEThE4ROEThE4ROEThF15El6dLC7vu8Heu4ZzOX+P6QUTfrTT8yneXBM4TaVr6/gsVG6UvWxppeC0YkKpRktS4qAGgZUK4rBYVJwAlciHECg3FF2DrW2M0Tj7azt5ZTVH4yzI21CUcpMEQ2szMhLSpizVaw4w7JSNEmDgRhpgheA+oQs5855BF1k/zH3f5Kty7KlOl2oM/XsenleOShgsaZxF1NTD2ZmrYsAJaYY6o8knnU9F1iYJTakLH8O8rCxuBwjycIAkTsaDtO5Sk0VRcIQhx4DjGMfexjxzmUi2NoixNjEAF1V0VYT9Ggyl0IkimINMkbY6XLn9rTCSIMFhchpiHt6ISmiClSiMyLPkXuw+rOfMHEDaVM09C2Db+wLbGtXZbBbnjFR3InYlJrbIav2z1mqiyzEZ/gAjEipZKYYhsmLLgBzjID212b1YMZ9RZuPYLknCHDYSo1I3r2XyxevHYcRmZTRsx0QsFcEQwy3XyQyW8NcVLmdjIhieaTtZ7X27AmjBwfIz2WYSD3ITPBTZnAcY4pI32E8UrSqyIERX73hA3TiE2OwLCxKWOZQNzULGd2RecZCM9vdEbZIY84BAIOTUTikTKic58CByZruJSkUWPPvezOPVj1YxnGcZyEWPIRYxnGM+M4z5xn6s49uOTKC3ho681MA1U2AgMJJcklrPUgapAVKy8hAni1SOUadYTZZjIcAJhyeYSMt4RtGD/wcksyxaEsYBHCznieNtTuU7dywpr1Bj59K5WhaF7gzyaJ1VY1pQt5aFylucmmU1PHVM+blCFYkMKNTqTC2IwoszAsCJMGEYchEHAsRk2ABG9avJdFtjsPYFvxq/t4yQHGbIi2vl3Mu8ro3N6dDH1tsy2PK4WkmL2ma0qVpOLs4tqA6Et6gJggqXpYaH1ZAIXJXeSKqZWc9K+mLRjr/wBlbq2c1ngLtC5PcEXboW3w09/WvMCqWM5cCJBMWqqW14Gud44jsSToUa5zTDWnIiPs5KQgIRpyxlmyIpXuEThFXm+ZixnPXlDcYxnOf97ShvZjHnP/AC5X9WMcpXXJppHIE7vNnLJTubNc/vXcvWtBLjqmz2DafLKqndem8cp67tjnTYKOU+hujL9TMmqFwiy+c4r8SBO/TKFTEp6Suo47IwKTClEOASIrJIfADRZ8+fHixfL3WeH0q27GTErjO5hb2bQ7yQQa7RTfsW6zv9dz7m/3nstpe85bejI4MNaXkVx57O+3JdPMyRnZARScY4WniOyhU3v6UvY/8gBo/nOJP4pOXK/K/pb5W69zHzS18fql+9R/G6X+r5P6Mn6UvY/8gBo/nOJP4pOPyv6W+Vuvcx80n6pfvUfxul/q+T+jLGdrfM2bLTKJOUfq7T2pa2eXNIpRGyuxLWebWbECdSVkowwmCsMEh5L0IAM58lqnVMUZgXgWMYxn1fNdc5NOxxk2kNzLN0AhrB4KkuP7AJXc4P1RXeMvL5kepMrpnGYeo7Sds8905ra7S2JkDAXAVoHvY2u9wG1QcaSaf2lvxe8doGn05kgJPlhMpve0CUyZLDKphauXlyeePzysagFMqCRu5q05Kwx1D5UfFKiMYLJSEmHF0rpDC5rVOrjrG/hNvYtn7baC0PcBRrGA0LgNhc+lNnWVkP3qucHJ3u1d1Fvc+0Bl49Ra2lxIxj+GRkxtLd8omurq8fE58UMsh4mW1ox7nM4gXUbGSey42Do+HbIUTa2v88wsxDLbr+SV8+nIjc4cUSGQtZ7eFyQGmCzgS9rOMAoJ956gCNKDgeBByLGchXAOBa4Vad46x0haJqvaQ6NzmSAghzTQtI2hwPQQaEHoIXWhbKa4bG9eF3t0AucUhquxK+lSGR0xfLAaNqi83HHXMWIfZtazE0BzMld1vw+Mq2VcIKsg0RyU8g0jPkeN+X03qLQOf9PYFjpcaHEtIBcGsd5UUrW7adAcNhAB2OqFv+5Wd4bu/wDfk5Gt5Ec/L6DD8xxBCx/bSstjNdWwDYMpi7mX5yZyRxS20hBDnSRlr4nBymbpj5lXdaBMjMzW/Q1G7Bha0KNGdLmOXS2kpVIQJExSb493R5jdoRULu4ZKycoPS+4SiOGLJacoGcADVlnznwzmAZK1nin6eAte34AJa74YWNmqvVCc5ra5dNy/1Jp/M4Rzvnb5xPZzFnRx8LLiAupsJjlc0naAAs/4+aVsX2j/AKP5ozn2Cwm/3oUwRZx585JyrxTJnjPjxj3mCM/+96P+bzsfyw6U6rn9wPmlQf6p3vYec9hw6b7Gv8J6RPDu6uw4/B5NVodt739bw7KwOU11HWitdSa0lLcuZ5O6QOQPkytNbHlwisLG1LZcjaocyQ34tEAaZQpRNZqkRB5mCzChZCMHS5LnHDJGYNPWkr7xwo10lKA9BEbOJzj4DQdau7oH1SOocbeR5nn/AKrxWM0lC4PngsXO7WVjTVzDe3TYILdrgKOkaJXtBPC0OoRr31Q9bE/7ArogatJFlrLp5VMojEmtez3REtRRqbJ4m4IHxkpytjTTEaqYOUpVIyftVwSCEgbWsJuRqMqTk5Jvz8v9FZaTLjVWo2uY8OMjGv8ALkkdX548fIgVJAO0mmygXZ9+nvh8rLTlV+i13d57e6w5t4LK+urSpsrWwtuENx9rL/pEsvA1s8rKsYwOq90j9lwXvWTkpepjbxImKCUnTV8xJ05JWPACiSZhGSyigB+4EAA4xjH3scvRlKejLmv0PJ7Ry1DaJBdrbCgb/S9l9cxKon0tA9XbtpYAYc+BL9lwixnGcfg51ms7GcefZ7M+eWB5MEHNXVCD/kbf5Rb4/XDQzRcotKdqxzf/ALZc7wR/oB6wrUj38vD1gvz/ACKRK60tZMvlEkkUrdCm/Ya5kCD7YlL0vkLyYjQkTDBCJOe6ORwwlF4wAGBeMY8Yxy+8+BwlzK64ubS3knfvc6NrnH2SRU/BWjfC82uaum8ZFhNPamz9hhoARHBb391DDGHEucGRxytY2riXGgFSSTtK486fLidYi1scUSCG3ayL1aFSmQvSPYq4FKtoWHEiAmc0iZxlKxtUKUR2QmgLUEnEDEH0mAEHOQ54jpnThFDY2lD/AIJnxl2rOfHPGN7ZItZapbI1wcCMpe1BBqDtmI2EV6uvYqpnY11cbA9dsjdzZulc7W1cdlvwEQ2Ra2gSduRJHQZSZDHLyQNHqS11KBHKcJC3PyUyuh3pEUNOaZhMGy+ruXF3hpvTmluN9tG7jMQ8Z8RG3iYDXjYN/CQXAdYW3nuuesI0pzZxB5Id6fzaHI5CDzSPKvPY2eQbIOAQ35jLPMrp2wNuoyyGR9HVhk37T9OHa7BOu/3VG3nU0XNoN/dyQt+wMFhjOhtarRLzz8rB3H9kNGHm24KMRhHh9CcJ3aiE+MKilhWfiCKo0VzMscsGYrNmO3ydKNeKNilO6lN0b+tp2E7iNyxt74Hq5tZ8oPOuZvJ+O+1BysNZZ4iH3GRxrHeNxSOaHOvrIA1bdMrIxtO2af4Q3yIZNodY0XZJvX8qj02hsmbkzvHZVFHlvkEefWpaQBQjcWl4a1CpAvRKiDAjLMKMEAQRYzjPjPLtrWEup62FCLLrsn+CLx+VC/fb6c+PZa8w/U+7zGbOFv5WaEivpCD2jF6KuTMMzvVTXErWPMfvIzm3hNP89vOmlF2xsT/7rRr/ACfZv/1yfmTK86bPJHsBcg4Uyxfd/wDoWt796+f/ALVHbkW+UPZUDuXVl65AH+U7U38EX+n/AFY/5uf9bkA/U9vMYtPlp5qAAivn9x8V69GHPGCZvqrY5Cx4j95OANeE032nTSi7DDth7KI5100AB8YkzJMtjbQUKorQNXOKzPu3R9CmEc6zyWpEigpzS1nXqTOFTsqLyD3pok6IoeFCsnHMhszmLHAY2TKZB3Dbxjd0ucfJY0dLnHYPh7gtCvKflTrbnZzAxvLTl9auutSZKYMbsJjgiG2a6uHAeJBbx1kkcabAGjxnALr7YJBLt2Xu9hr+I/bFw7K7I2EvVHPL8I0S6XTZ+PwvmFjTNWUA3DHCooiEJa4HYxhO2tactOSH1e5LFjVYW2X5maqfPcEth2GQja2CEHxWN6OIjY0fJOq47AvQ/rbP8r/Vs92W2wWn4mXOqZQ6OzikHBPmMs9g7e+uB5YtYdj5DujhbHbsPG8BXoYH0IaWp9UaZ1muFPObCzXUgdbOm8ui09mNVDte7ZIQUU8WLMW+DvbZ9unMCUOW6NErzFf2C0BCnTixnIxiyagxVhb49uIjiZ6NYwMEZALS0fLA76naa7ztK85WZ1/rXUGuLjmXlsndv17dXrruS+bI6OcXDj5cb2EOiDBRkTWECKNrWNoAF6P6OX1cf6ubj/nI3Z+7Lny+9rT30Dae5M+ZVTHntzuJqdY6or91L3+eXBbJ+Wz68pDA5WzVmnuWr7DXMywmGWBi67LmZUUkeC/W1OyuJSuSuEfkjcSsCH4lGpK9CgjIwYEWLOBh4ptLabnidC+xtQx7SDSNoNCKbCBUHqI2gr7cZ3h+feHyVvl8frLUovrWdk0fHkbqVnHG4OaJI5JXMkYSKPY8FrmkgjaqU1gV3d+s90Pla2CkeKi2O19nbS4lujHlUlUx+Wx5bh2gtrV6vUFFGOMTkQE4HBqVenJSlKMxMbjOcHA5jnkrPK8tNUMuLd5dCCXROOxs0RPjRv8ACPJcOggOHQvQRy61byz9ZB3aLvT2pIWW+pmBkWRgjAdNisrGwm3yNoNruwlNZIT5L43S2r9xCvhdd3ZKwdhmkNlr5Ma1MOzFS1xJovsPAUADUxCR8MjD2BksGNpzyy/fQeyW5H9ooslZGFGcI5EMWTEwsiyWwmYsdQY2LKY9wdBIN1drXDymO6nNOw9e/cV52+bnKjWnJDmFk+WWv7Z1vqLGzFpPC4R3ELtsF3blw8eC4jo9hFeE8UbvHY4Lr+qPCL8YtX8+kXj8t+rPtyHOA+25a88Zz7Pu8x7wrm/lapUV9IT/ABHrfDzSt5x6p6OQsf2fvBw+3hNP4W26aUXbTF//ACy//gD/AHuOZNLzsKPrfzrV1v7DIB9g2yxGMFlR1vWgq284kWnQ2RWrqcEYiRoXARYiX+LqFOf/ALgxuAT25cUIWMgLOwUeV8N/j7LK2j7C/jbLZvG1rhs8BHSCOgihHQVVGi9a6t5dantdZ6GyFzi9UWT+KG4gdwvb1tcPJkjePFkieHRyNJa5pBVAbenQi99DbKb6w2VjTS8xt4cyldS3hG0KsdYWOMk9SY1lpFSwOTYDZRQEYhKWBWeI3zjOUh6sjIR5x61RofM6OfJlcBJLJiHMc15b/CRscCHMlA2PYQacVPCaHat8/dy76HKDvaY/H8qu8Hj8ZZc1LW8t7myMoDLK+vLaRssNzj5nEOsb8SMDnW3aASDibE58bjGNvOi78Htb1ywPGQi/Eq+A/hY9P4X4lJvIfGfGfOPHOw5MEelL3hP+jM9uqJ9cQ2VuidB9s0tk9N39aim3zQE/HXYi8yDWiZOEThE4ROEThE4ROEThE4ROEThE4ROEThE4ROEThE4ROEThE4ROEVauoLAvzp8vHcer5vo9tttVrbsds9ONqKPvvT2vGy5HZtdboSsK6ZVTbFc5kcfe4m4xuXIlgG52CeqTL0pycGQleM+giid7wd8e07YDXt6hRdXHdYuuNsrkFXwuvLVnLEu3j3NlsqcTmhtrGN11BHBcprWEPBhoS5ANUtTpAICjPeuSlOoEhOIqRm6LNUkHuodR0o9M0tg9HQyIVOusNjKPLarMsWONeFVvz1tNVlEKlLE+Wi6OwWowwOBfZBCUP1Bxzmj3bdykcsra76IzqeRAi6LQjztGarWR+RymEIHUxXGVlkR6IpgqpfZi1yGgVrolrjWpBpZskl+ExgFSgZTQ0YUuanOUsHP6GqIb1rEji1aWKDFTCmmWwKZxCoMCVZyCFQZVAFCjKoWMDbanWv7dPG6Lkl5xghUfIz3QZGMGGISzs5T4ePTb5PwE2V8KkAiQYXbdUBiW6Kd5vKjYszGMdUdiusLQOf3NrKrMTi/FqM7EQZ5Jj8wnVJErTAZUR6VFtUgaycmmR5yEEQkyqStDVuwqNK71GfcWvMsqQ/Lojf4dbNZLTPMauapXkUoryQpR/hE/EGmJkUghz3gsWMqGd+QtjsjHn0nJwZ8eeVruJSEUWWNY4BSbdXVp7J3mwv1mt1azCt61q7X+OqjWYu4Lcshtmz6zZsKTIgmvTTVMNa4Gca6I2oAXh8ULUqFKoRhGerJlkO3hCmaNlV69s6ubUr7jisYmlYOBWwl7mqJY3UGwMeG+w4u0uB2AMOJBXKBOVmsmRU2l4E1t6zCcxO0EAOGWUnyWMUWuaNgQglb4UX1+lQZTJINMrAYJ5aFwtcJ1hcIHTq1on4IZb18WZXJjBAWuWN68cXltpQOu2d9fZWgQnnJYsQmJLWnhPO9zjjc7iUQKL36nq6Lsub0ZYHXTaivHY67dgtcI/Z7jMPd0/qzqjUCNgU7O2gzmpkpSJ+E7wSVAYxSNyHhMjQ4X4REBXiKNxAuJ3qKtmfKy6avtFXvvZbtTTCbuejs4jlMN9B4mITWpwnbjJ2jM+UPT0wYCEnMireOrUrSqVeMACocTkwM5NIVAKEkmpRXQ+QROEThFFP3LNev6jRuWP+yetV3bZV5CLArCTpad15dJM0We4Sj8a0kdZX5oVxSTxJ0+CjuJAapVhyqyVkgIvUAWcByHhuIILqJ1vcsbJbvFHNcKgjqIOwhdjhsxltPZS3zmBup7LNWkokgnge6OaGRvkvjkaQ5j29DgQQqgX5Tep78w93B/593Z/Hjzp/etpr6AtPc2/GV0v0ie8Cf/ANxqv753X84n5Tep78w93B/5+XZ/Hjx71tNfQFp7m34yh+kT3gfw41X987r+cT8pvU9+Ye7g/wDPy7P48ePetpr6AtPc2/GT9InvA/hxqv753X84vtMVydS7GtJXGfL9dpkjESoweNFODbFnjOs93kAiSFbJMLdemkRBWQZ9gSQ5MwMWDMjxgOA/RBgMHbPD7eztWPHSI2V+IulznOTm/qa28z1DqrUd7aEULJcjdOaQegt7WhHwFK1U/wAwdT9FQ1srymekrsOrCDs4TAtsVhFCw2PMyT3xozjRFo255IKyMwwWc5ELyL7nnxjGOdsrahobUjyianrJO8k7yT0k7Vkz9JzTfmjez/8Agkjv7oOFMsfWT8w1VFxRw+IWz0l9hdlRVT7z38dnNAQaTspuTiRkG5E3PDqsTZyaQYIAs4D5EAWcZ9mfHClexkgo8AivSKqLGX351NzB0XOw/l6OyuIGOBx6o9vrKOyOrmYCg3IhBGlba/suOJkRSYYvUWSSEBGPTgOQZB5DnrLnCYe7dxXVpbSP63RsJ+HRV5p7mjzO0i0N0vqTPY+NooGwX91G0DqDWy0A8AFFitLMOrJOvwrUdJ3dA5JPWeITOrmdnAQDAfgzASRGpLbTL8AT+vGQZwdgXkOPVnPt8/J71tNfQFp7m34yqn9IrvA9OuNVkfdK5+bWwNZbW9VlUPRMhYvl1+wWRu6Xx9nrbNrMdqHteSzQGkmN51hTmRngUJ/RgADxCEfkGc4EPPqF57G1xuOsRSyt4Yv7RjWn4YFVQWoda601c4u1XmMrk6mpF1d3E7TXrbJI5v7ClLY/mVI3GWpGxRvp17Ko+yNxQU7e0MlKRJqa0KcP/JJRoEL2QlTFB8+wIAYxz7qqmA0NAa0ANHQsQ7K989TbZ0fYWvFz9PXag81pZ7NhilTezwFLGXQ1EFUnXkjQvTFLEDqgUkLEZYwjKNBnyHxnzjOcZkexsjS14BjIoQdxB3grlgmmtpmXNu90dxG8OY5po5rmkFrmkbQ5pAII2giqj61j2z0E1IviCbIVB00dxpVoVuGV4ia+VKZNLmlCKaRR3hj+M1jkFhuDSqNUMD4oLAIwkWSxCwMPgQcZx19lhsTjXmXHW0EEjm8JLGBpIrWhp0V6FW+rOaXM3X1pFYa71FmszYwSmWOO9u5bhkchbwmRjZHENeW+KXDaRsUvX6Tmm/NG9n/8Ekd/dBzslQifpOab80b2f/wSR390HCLjMy+ZAhVhxV+g876auySYQ+UtipnkUYkdIxF4Y3prVg92oQObauez0itMbj6wjBnGM+M49uMZ4Ur2te0seAWkUIO4qEFzmPVesc1y5t6Se5eLolixQqSx2NyOwUEdYyzzMmCbWJvzaRgW1oTiznBCYGcFJy/BZeAlhCHHSy6b07PI6WaxtXyuNSTG2pPWdm9Xax3PrnnhsfDicRrLU1tiraIRRQx5G4EccYHCI2t4yAwDYG7qbKU2LfbR7tg106845KoXrf1K9xzbCJcvIdVMMmqd2sCMMjqUasUKXCJtMunDwni5zuauGJYFDggpQPARCDkWPPO4YxsbQxgowAADqA2AK1c00txM+4uHF88jy5zjvc5xq5x8JJJPhK0MmM36vZy/S9/e+ljugwfN5HI5Q/IEMkniRqMcZU9rZC8FJ0JVnhJTolDkvMFgoOMBCHPjHs51smEw0t35/LawOvuIO7QsHHxDceKlajoKru05sc08fpg6JsNSZuHRhhfCbBl3M20MUhLpIjAHcHZyOc5z20o4uJO9TrovmZ0CBIkQpuors/LTIk5CRODNTsBmQEJygklByMchyYPISy8Y85znOfu552ioDdsG5e1+k5pvzRvZ/wDwSR390HCL5D/8ywySZieo48dQ3aAqaZA0uLI6pQVWyJRqG11SHIFxAVSSSEqk2Tkp4g4MLGEwGc+Q5xnGM8IduwqD+EWJ1iwCXQWaMHS13PfaldzCITiNJV8gnC9pJfIM/N8kjuFbYps0xIsQJXRrJEIgwIixgD6RYyHPjnVx4PDR3Xn8VrA2+4i7tAwB/Ed54t9T0lV/e82OamR0z7ycjqXOT6MEMcPmEl5M607KKnZRdgXcHZx8LeBtKN4RTctndpd9dD9zrfPvXYrpN7fJvY5kaa4akckj1ZUTamaLNIvfJmVhjURttkjrKjOXiGqU/DJSxKlQ8mnZGPxnHPe43H5JjY8jDHPG11QHtDgDSlQD00O9dTpTXGtNB3suS0Rlshh8lNF2UktnO+3kfEXBxjc+MhxYXNBLa0JAruXL9QuyfRfReYyiw9c+ivtAjU9ljIkjTlMpTHn6zJIhjiRQctywR1zsaxpOoirU4rjvfLi27KX7QMLJyp97hORgtZY3H41rmY6GGBrzVwY0NqRsBNN9Ao6t13rfX13Df66zGSzN9bxGOKS9uJbh0UZdxFkZlc7gaXeMQ2lTtKkL/Sc035o3s/8A4JI7+6Dn2qlU/Sc035o3s/8A4JI7+6DhE/Sc035o3s//AIJI7+6DhFHtt/2UaN70zOLWJsh0Y9pMqnUOjqiIs0tjbDIK3fTIuoXGOoY+8uFeWPGj5A1o3M8w9KUuEowjMONyR7v3xvr+O9xuPyTGx5GCKdjTUB7Q4A9YruKqnSeuda6CvJcjofL5LD39xEI5ZLO4lt3yRh3EGPMTml7Q7xgHVoakUqVh3WncfRLUe23m5aM6ce5eNyiRwSUVhI29xdJhI45IK9lmQGOMTeWmQWS6Er29IsKLVIzDPUpSKS8GFGBFkWcwssbjsawx46GOCNxqQxoaCd1SB00UNV641pr29jyeuctkMxkoYuyjlvJ33EjIql3ZtfIS4M4iSG1oCahYKiU46vIXIYbJGfpX7nxqYJKojMY+hXSOdLGot3hL+3SSPhUt5tn5JUoUro1EiyULGQCAH05x45wRYTDRXfn8drA2+4i7tAwcfEd54t9T0ldtc81+aV5pf3j3mpM3LorsGQ+YPvJnWfYxkGOLsC7s+zYQC1lKCgop3w/M5JsYxj+iM7PseMYx4/JLHc+MY9mPb+MHnPO0VArz+k5pvzRvZ/8AwSR390HCLFN1fMB09sXWsqp+7OlLsdseuJo2mtUiislpaMrEC1KZkAwmFC/GAKhEuSnFgNIUEDLPIOAEYBhEHGcQIBBDqFh3g7im0EOBIcCCCCQQQaggihBB2gggg7QarWDrF2W0bordaAONE9SPafW8+vKXxyh2Gw7zKd5nXFERS15vHmp1VohyaYvamNw9sN+FPc14viXALcjEHJ2QesIuuscNisZJJLjreGCSY1eWNDeI+Gn/AEKudXczuZHMGysMdrrPZXM4/FRuZZx3lxJO22a+nEIuMkjiAAJJLiABWivFc7JUMnCJwicInCJwicInCJwicInCJwicInCJwicInCJwicInCJwicIvyMWAAEPP1ACIWf1g4znP7HCKgPuP399jDtM7ixDbkovUanXHYS16Q1ogsUqF/2J25uJsqGYOVcvE1Yo4gXJGVli7g+IM+pYvNQnHO2DUraFUSmOHgigvl2s/b3s9spDdmZLrz2Y7fSMtNJEa2fXHQk0qUhvG9oHZvISVileMO0RgbW1HvRjiSnSfDoSlmcGBIxnIh5m2AFFKPoB8tBtVK5JHZDZWpcF1+bSz8LlNibj2Q3bDSBmzjKczCyP60VOsiMFk7mbgwQiC5Q8ATEG4zlQlN9PuhQqaU6EV4/UDr4prU+IPqQ0xxum1LCjLXFbdua1SG17k87jrUmMTN8Db2YCQqLQCnWMs8wtqhrEjQR1vJFn0JcmCGYKCKkf3sfLjRPW16tva7VCM/Ba8S6ITiXusDbPSArXm0I4QonaclgQkjCIVPTmPNbq2kpgFjwzOYkRXqwQYXgMzXFvsKBAKxr8l1V2ZfudtrOV4ESiORjWhuhj4zuASlBEiJsKdNynLYpa1JRqdzayS4rgxSEYchLH7jAsf4TGMxk8pG7lb72A+Xg6kdinxzkkn1WZYI9PqdcS/KqUkknp0h8GtNOUYUu7ZBHRoa140ak7Ayixk5I8FlljLGUAIMSKKh03a6R9AuravdfNkKNPmUZZ6o3Eru97AaLNsFbMGSQslJVdb9hjyma1zaIap1YkccPPID78grBJigQ/fDwQXiJJO9FUR6metjdDuH2cn2Y1YM7isEe3JY97ebOvDo8qTlSOZuZr2+R5U5mKQK59YE6UmmnYbzjxk59qld/gwgCZyOoG8I3qArXwLsjGzoR65GPX6t9fI7Vz5FGutIk8RRDPofK3SKWbIgSoJQpy6yaYso0zqrc54eSALucnGmNVIgARevCMssgPEoqrL2R9MNkam2TsVcNe1LfV96KUrTFPxxgoCAkNLB+Ux6eH2WzdbCcZh6NJImLV2uXpA3uM3MaCVkkfXlQUHJmAiOWoCKNbpz7sdiae7JpDtLsPIRSyh7HhkeqfZCLxltLbY1SVItjqmRVzN62rNhwWSyQagXw8opSBGnUDIaHRwGowNUsEqzO4ACo2qC7VNqdW19a217ZXBG6szwgROrS6N6gpWgcm1xTlrEC9CrIEMhUjWJTgGFGAzkIwCxnGc4zyRRX0OEThE4ReM+39bhE8ff+n/HwieM+z+x5/t/f8cIvPCLx49vtz5/sfTz44RPp+pwifT6ez7/AAie36eeET6eeET73CJ4+n0+/wAImce3z9PqzwiePp/c4RPHtznhE9vCJ7fb/wC3hE8fV5+n/BjhE+n09n6vCJ48fVwieM/T7/CJ9Pp/Y4RM48/T6fe4ReeEThF4zjPs+n3+ET28Int+/wAIvPt4RePGfp5+r+39zhF54RePHt+n7PCLzwi8eM/d/se32fsfXwif3fp9/hE8Z+n9zhF54ROEThE4ROEThE4ROEThE4ROEThE4ROEThE4ROEThE4ROEThE4RaqbyXvLNYdP8AZDYGBV29WzN6mqKZTGI12wNit4cJRJG1qO+xERja34E4KmklxMLPcMJ8CPCgKOEWEQ8Yxkiof/Ko3lqU+bQbH7E7jWxWcO2QZa7gkB1yBaro2R1pboQ+usqeLrlcKkEowmZgSZ8maxLhxOCoJWJsO6koORErzgcmf5SLsZE6hOrTkK0h5KlIpJKUJlKc0BydQnOAEwk8g4sQizSTSxYEEQc5CIOcZxnxyVF/fhE4Ranb41/Y1saT7bVdUDKySK0rH1yuSCQBikaktEzOsqlsAfWFnRK1ZxJ6dPk5YvDgoZofdBO9PryEHqFgi68TQ3cuW/Lx9ibsg2t14sOGUhtLrdr43zdG4R0JM7hklh0BjTTL5NGywKcs8mIY7Yb35G7p0aozCtMYnVliF4JCY2ndvRdgXrf2T6JbcCiybXnaOo7IepkmWKI7FG6UJUEzXmNqDDm6oAxF4w3yELo0t+ffKU2U/viScZMEH0Y9XIkEb0VQr5yjcZ7izhUOoDTl3agSaqDJ+cv+zF6ZueUkonprRIkSJ6F7tKrVoGyu06U4BWc+lG8KiTM5weIOItFSoE0U7/y/s86+YDoNrFrXqteNaTKwTK5FOLHZE4gRyy5RZzoWlkFpuz7F3gtHIVqqPOzyBHgeQnBIbCUnoF8P7rOZVFT78IvwMATAiAMIRgGEQBgGHAgCALGQiCIIsZwIIsZ8Zxn2Zxwip4dw3QyhTbM1t2O6JU+nXOA5SsZt29aYI1pAJ7YqmdoFcSsqfV3CCCiUa+YucLfXFPIWVJjAn4BgFaYgThhThdEEjci04+X37wotpzmzOrXsosdVXf8Au9zCUxmhrbsNG+JUzaxRx1PbHKoZfgTWa5syZrGlE5MKxYHBOG9QYlGMstOmwKCK7rSGwdKbJw4yf0PZkTtOHEOh7Ipfok5AcEiR2TJka41vWAzgtQjVCb3BOpLCaAGTUygo4HqKMAMRFmThFo1vztDOtYKvrA6p4lGJfcF/7IUnq7VBc/XPDbWscmtzyQbURNLJVx4ox9HEYoyty1WNGjySrdVhZDeSaSaqAaAi5RWrJuhCH5ZKdl9mtQplUrLH3pwfm+tdPrW1/fm4aNL8WW/rLKn++1/Rprj7MnINOWlnR/OTCsecKU+AZFkiht65PmAYru/v5Y2sT4ip2L1DarVLJPoHJI/OmdVbs/YqjdF7NPWu+YAKbPUhgcvslkSZm8IRK2WPmKYcSaYICweAn5Isc1d3CXnLtT2jYqxux7pypi3HyvZ9YjTphN6TnqO53QyMPMxQQ+Bt6lX2coJuvfLMLjBAG1Wjgaw8w1wLylbFwsAJOIp74TN7/vzUaqbNhTZGdXL/ALYqap7DVxS+a0lFys1QSGWs0ak8zgU0r2MWjr5KX91Y0q1a0gzh/Yz0i7BahQnF7k1AYRQqOfYZ2UxLRjbvciQT7RyQq9etl7X1Wi1fM+oN9xlO8yStN1WLVUuzn+TLd+ZSYYxvDMJa75jidAnPIVHEE4eDCyDBqSLJpvZnaMSuPX+Aw7eXrH7FXy0Nk65oqea5aYVjIYdsLB4fM3Vc0Te7crUPYDtonbIvRadJl1kJTxGG9vygJNLNdkB2SfekWKt5PmEIPqR2M1/rEixSbnrdWsogte7xWZLbCYWCyoHNrtLETBg0rHF05ZVE5Y6NKPb3i0TUTM/5ZWx3IIMygUlGCERb8PV87s2/vnsnq9rvaWqNSV1r5RGq9qkyG3NYLa2GlU3dtinO+0a1OS4wfc3WxiYmaOkU2SNPjCFeaqy4DyI0GCw+sij8f+3fcFwnFHUukZKZqiwgXN2G0PshMY3qNtlvyzu0s0pd9fW+NSbXWi9drWq+73eKz1Pd2Bu5akLwpjxqVRhRnCdvVKjCKYuibptKQ6rze3Z3KG6dzWPt1jPDS4KtK9mtD0oyYqyGrWttddfNp5/NbkJLCvSDyN1E4p0ToQYH4UsvBYjBkUFzD3LX5jSuIbQP/YJ0/OGwT/rrC75J67mylrHZ9jZVKZnAWiwY7r1FDUnZHPLDBYU3SvSVAyLcVu4CPOWp1ImzKcefBFshKeyvcB82gvepWU+L6/xGpK51NlSOKj6rewPsftZPJ9g6fVWRMYzZDhp7eFeoK3FAnhGJuS5dWFH9qC97goWTEh+OEW1jds1uNe2x0n1Y17k+vdaumvut2t1uX3et86x3fISLEnWx6KbHRqMVXq0XslRs8qNhbUdauTg6Ck80fHRnULE7MalOUplK4RFq9a3Z/tvrnZMy1jtOOa7z+7692V63ourtquIPZkXqWa68b67BvVOKlJ1WPFtziTVFdUFTxJw9wStmUibVwz0q4BJhGDUvCLY7s77A7h0tkrMyVZHKwfyl/X92kbXKfx+aJQ6qirA0lq2nJtUrSX+LszioQQqRu9gLypAQIAlqxOQSFGsQjAYYYRafNXapdUkvLY2FWNvP1Z6Ewao3HXhirorbCpJY8Sa13G0NSqO2CsJczSJ97FNZmdYmhkktvCPCVC1KhpEB6L4k4Ro/eHEXF/6Xnb2UtOrLVF09HMhFxbDbr1Cq2Sh+nm1u3te39WOr0FYZhBNh9VNZ6Ku6PXK/wa1XB1UNYykcimadHluVLiF6pEQI3JFl3artC2l1a0tr63YjFWPbTZC1NpCazgdZG6LbbaDOshq6FV4+XPc5DZr/ALP2tLLmcJQy1dWkiMa5OWt+w1DivQEltqsaU4paRe7De71ivDsL1MoTXiJMc60wvGDMrPYGy6khcNZHdmLgoCW7P0XSDMcgkQWhA+J6ggCk6Toz0LipSOL2iSmGIDEpwVRFxnTPse3J2fPgMilNg1zCWmabDWpWgIBBemnstuaMkRCtdnLCopIU57wwjZRVrPEHySM0C+MXvbsgJaooesya4pMp0w8GEWvdgfME27U9HbqONs0xXtYXBXt2bOwbRSdv7dKnLXfaiO68bNvVIySEOeUc3SPzDsFCI8xnOLowieEOXpHkDq2lgTe+REEUk+wtvdjcD3X101xru9NJkEE2oTbNyaIOc00kvWVy2q49QzNBZC1MEkcWPsOgzPZ70/o5yFMoc0rfFiCjEmTgIchOwSURbBbxbO27rNXWuMardjr2a7BbP7FVXqtCpFM26VMVMxWbTaMzCXSSzJbHGN6dpWZEmWO126noY+S+FrV601Ih+1AesavBFqrAew+R1hcchgW2Wy0Kd8Nta35M2SEt3UN2GaPKZd/u7x3M3sySwnYTZm+bSo+eReGw9tUnqCmss0C8CxIoTOGChACqIo0U3zEuwa3rzVSwnWGJEdropIiTtun56SWiiiiq3mnTtumrYYbPiVEzD8lANTkx6k4wTwSfiUJRphZKAIJXCLdC3Oz/AGDfdoYpXdESWAwCi5VoXrFt8zSAzro3S7GLaeXzY6Z3S3IWVRBdOLxrt9h8GaYnWaU7LysZ1CEtxU/DnKwGq0RIiLHMd7m7zZ00TSTmJRmXEsvaPA9JbJl8d1F2yqWyphUcy04sfZ5bJoVoZMZTYG2dZ2i0PUbQNLeQ5kyYmTJAKVDagGI0IEhFIJqz2FOO0W+OzWusah8mjtN0trvrvZ8aX2vrtsZrhdDhNrVmFysUuTPUS2Kjdcvq+EJG6AtompWkjSYgxQYrD8aqyDJaci0Oi3eY8RW0pQkvStJy4Ql77AbI0SrqD0Ppbt1ZKuLireS2e1jtJy2rgxFn0lsdJJE11xkY62gcfTzFnVKDwGAVlN600oilF3w2psTXaEa+tVJxSKSC4drdla01iqxdaZMpRVxA32fx+ZzVwntktTCBtlTi1RmH185CLZSFTUsc3MaZH8Uk94M0sixbel27kaLa8bDbZbMWvrRsVXdI03LJmiqqiNSba10mshmKIbeCMlK7Lm+7O0DM2xc1SaMhwwGLGnklH4VBOwFMJOoIsYbJ3n2YaY6yzzcq3rG0RtqD0hDMWtb2vtda63jVcncoW1lErZlGa12Xl24tkR5dK2RvNMy0rnCtCE8hUJwEiRtolQcpyLn3Z92OKevmp9Ub4IiauU1ta2zcdrq3WtLX0xnVjo6gW65bC3U8Kq4iUVemVSnsYtyqVuIAc54UtCFIcrGuASUESxKRa+g7Cd2h62673ipg2rLdLOxHZGpqV00rptcZhP4pRkLtKLTKeIbC2auqEz5fGrofiYJAV60bJDW+MIwOhydqKdlORGLwEWaNdNz7FVbVx/W7YPY2FSOVzpstFDXlet3UJ2I6QqZxIqqLQusxfITsFsnetr0rPovFGLBpqglpJPw4AWpFCVf7oRYFRFLpwicInCJwicInCJwicInCJwicInCJwicInCJwicInCJwicInCJwi/AwBMCIAwhGAYRAGAYcCAIAsZCIIgixnAgixnxnGfZnHCKKpT0gdU6x6kr+o0xrgbhLprMrCfi8PVhAalMosNkXxubqEjAXMgsTQ0ydlcjiFbWjTJ2szAsC+H9YACCRSRVvXkQqOv4TVlfNRjFBa6irFCYYxGOju9ZZYvGW1MzsLQB1f17o9LSWxrRlEFjUqDjfdl4xkWfHCLm3CJwicItWNsdJdUt5q/Kq/bKj4ZdcLSrcOTagkxLgkc2RxwScn+0I7J4+vZpVG12SFAwZOQLU5mQizjOfHCLVWt+lDrFqGQQaWVprAkh0orZxhDzCZC02zeoXpkeq5d3R4hr4ByPtA9Wue2bL2rQ/GKhnqVDKoG1HjNbM/CcItqts9JNU96q+R1dtpSMOuyEtjsS+s7dJgOSNwYXgr0Yy4R2Sx5wZpRHlKkovBSgSJaR8SR5KN9ZechyRa61H08dblDyeu5pUWsbJBpbVCiOqa/kjTPrbMeY+dFT305kEBwXz9YocwEEydelOCsEoArb1AkajBqTASQkUmPCJwicIo+9luqzr428ncbtTYDV2AzG0ok8t7+yWYzKpVW9iFuTT68t2XGcVdIYZKH1ClEP1BSL1SlJ68BFkrIghzgizxrZqXrzqFE3aDa5Vq31jFXx6Of3NnbniTPRJ7kaI30ZLPlL2+rEiFCQb8OkSEmFpESQstOnKKIKKLARbGcIsWXTSVU7E1tIqhuqEtFg11Kgt+XmNvPxRZQ1TO5o3tjdm5xblKF3YpBH3xuTrm5yQKEy9vXJylCY4o4sAwkWp7l1j6tP0UnsDlbzuJO4TZ8Ifq4ncRsbsi7GLGjUhhkoKKTSBmUMk52qkDcl+1URWUxqhOWSrykNOIwbgk84BhFmqf6ca0WZBqVreU1QzFQ3XGd1/ZlDtUQcpHXJ1TTerizk8Ie4M8V49RZ7YfspAqPRmpiVIUi9AoOSqyj0xxpQyLiCvr90+cNT0ejzlSzU56vtic0lnrZzks5cVDCoFKVs2TPceny2UH2XH5W0S1wNcG54RPJDq1qvSNIoIyWX6SLayNR9BE48yxhrUPiptYGxG0oVMlk0kmkhPSoCAJyDXqXTF2fZXJHMZZeMnLXFaqWKTPIzTRjzkWSLWR10V1We6Ps7XB0q34qmLktmVXlZEN/HexifxjtKbXCXfknlH4xJ5eVK2j7TtkkLt8EgXJW4nx8KUnLR+U/CLmNqaqUNc9oU3dk/g5yq39f3N3cqlsuMS+c15NowTIkeW+SRtRI67k0Vc5PAZMl8Bc427mL4+5ZAASlEaIsAgkXF4Do7qpWlM27r/FaeZRVTfzraT7eLBKXmVz90t56ulU7q7OdrIms9f5LOpm4yoT4pLGc4OR5iZMIBCfJJBRJYCLC5fVVpsgdml/jLZsnXr60VRWNHheqn3z3yqF2eaupch/S1XFpg5VhsrElk7DBU0qcS0Cx7G4OBQFhoff5wLOOEX15B1faUvjJQ7C31pN63T6zIrQQUo60TsTsrrrLogVdithcbdPUzqhreriay5xshzjSRW8q3twclS9WEw80wRqhQM0i2VrqgK9rGtHqpGVdasohkh+3gvGbivy99gJerTyVABsd0GLKvWyLGslM1GIwekhGU7ASoxDGNOWUMYxCIsVP2hOpsm1jrXTt5qYs/X+mkVcpKjjCWa2K2Sur1NR/Cfk2kVf261y9FccPm8PAiCBJIG9/TvgSxmgErEA84JhFwyXdbGrExs+XXMfjZWIWbYLDX8bn8rqLereajFc7bKsjw4tARTVHS+xsCa5Y6x1jNNJLcHAhQ4GiPONOOMOONMGRcssfQzW21FlfPkna7gbZxWVet9URu2662j2kp+9nCuGwRR6WGz+/KkuWEXRbDGJzKy4nFSl+efiXc01wOyNcccoGRYLqHps626LqC8KFrbW4tuqTY9bHXW5obI7avSwkswfIi4fbEYkpblYlnSp7jMrYXvAV6V1aFKBxJcAAVBOwoAAzBFkFv6yNNk6O2E79BbKs9fdlHTnWqwJXe2zm0mwthH0NZaQ9HPqsiVl3nc9h2FWkSlgVHvVpEZc2jJqospTkXxBJJoCLNtaak671C4XO5QOtG5uO2GLhRFzpnh4k0waZ6lryqo9SUTQujFM3uQMqZuQ1bFkLSemSp05C8okRqsB6g044wi1zx1Q6QJobTkDY4Lb0Lj2vEisOS0QKuNutwqzkVPKbVakzHOmOt5pX18xqZQ+BuzKlwkJjSFeTHW5MYaUjRJwHnBMIsqxHQ7W6ISump19l29O5pr5L7KndOSy7toNodhpFBpNblfo6unixA8XvcljL1yJxgyTKJIhWGKUDQJWsUN5KVSuWnHkXGoL1p6PVhDIhX1dUGxwmIQLagzdeIs8ZlE/Z8M2zRxrnk2yiVyKWFOSgI294UNuWU44yPfYo8NnwH2cACUJF8Kt+sHUynnRvcKrztJXje1z55s1FBof2Ab9sFSFzCSTZwsaTKzKWbtmiKjUtEnmrurXubSayDaHExWeBSmNLOMAIi5hMeuvS6wqEmusM5oeOy6jrBtma3tJoRIXuZOuTLesOxXm2JbPmOTLJIbMIi/OE8kK5YUNocEIEJSoxKlCSjFlPwi2AlNJVhNLVqi7ZNGftKz6PbrFaauk321IUf4sN9stzA02An+xW92Sx56+32+MIS/W4pFY0vuPUmySIZmRkXqXpQVP7LV4vqq8IM2T+DL3NjfcNS9Q5ti1qkcXdUr5GJTGZGwLmmTRGWxp5RFKm51a1iNxQqAYGQcAXt4RaXyXqJ0dnahQqsqPbHW4cfXtqVUVm6N9t+LmLaoDeEVzB7aYI8TaWzMuJjIZ7EvCBwUtwUqwwgAcYNDkAc4Is7l6G6hFXE3X8GiIcK3WrXHOpCCZGiej1KfXnKgB/5Ovss51MYjScFhymw5DSieMITDEmFfwphhIiLC5HU1pG1rq8dIlFr7rR2qyhIFq9C3inN3d4KWeUlB1c7SJ7r2tXp3qbYuGOUwZYk5SteNGa8mr1hYDQl++yUSSAsiyNFeurUGHkwrLfWkgeHiA7DNe1rJNp5cl42bZrnsAxV/I6qYrEndrWPZMqsq0VbFXEsXMyJDJnV3akaAZRZKUGEyb3RFnlloKpY7ec82TZ4n8HdVm1/A6tnE0+3ZKo+24JWbtLXyEMX4uqnk+Jtv2I6Tl1N+KRoE6xT8V6VBpoCyQlkWm856jdE7Ak2JU6wO5o4sJt502AamOrNyN0KUgUYvR8G+mPVxQqsac2CglcQWzHg2UOY1T2ztSFxUGOSoRhwhKDsjItxLr17pzYusz6guyEIrBgJy2POxTa8OL2S7Nj/EXFI8RWVsMubHNBMY5M447oCVaF6b16Z1SqQYNLUBM8iyRYkgGi1B14pk5hDhsdYrVNIVIq7lkM2G3V3O2lrOQw+WEkpZE0utVbKX7a9bKxOiEnKYakTVlWFIceQE0JKg8BhFjJs6sdMW8MbblkXvGYQqIubE6xynbN3L3PtrXhrNiqxO4RNATrbZ+wMuoMyPxFYjIMaWsyODbWvKYn4UgrBJWAEW2Nq0PVN2uVPPFnxX8ZnGg7eZ74qZR9uSRl/FS1mGJzSDNMq9zHnhpIffhItYTwl+BcgrG0z4z3g04jiiDCyLXIjrS0iSQS1KtR0aiRVhcNiJLdlVbIZxZyKAMNpIHhNI0tiVFDEs1KjdAzMqSIy3L7RgiaOKxOQfihDyozkzJF/eveurWCtrigF+tpWw01teqkE0ba3k15brbp7IJ4SksVnIj84DG4zsFsDZsSazpMzpSSFRxaDBwwkFZwLAiy8hIt4+EThE4ROEThE4ROEThE4ROEThE4ROEThE4ROEThE4ROEThE4ROEThE4ROEThE4ROEThE4ROEThE4ROEThE4ROEThE4ROEThE4ROEThE4ROEThE4ROEThE4ROEThE4ROEThE4ROEThE4ROEThE4ROEThE4ROEThE4ROEThE4ROEThE4ROEThE4ROEThE4ROEThE4ROEThE4ROEThE4ROEThE4ROEThE4Rf/Z)/", DIRECTORY_MAIL_IMAGES."$1", $thisSendedMailsContent);
									#dd('thisSendedMailsContent');
								echo $thisSendedMailsContent;
								echo '</div>';
								echo '</span>';
								echo '</td>';

							echo '<tr>';
							$count++;
						}
					?>
						</tbody>
					</table>
				</div>				
				<?php
					if($pagesCount > 1) {
						include(FILE_MENUE_PAGES);
					}
				?>
				<div style="text-align:right;" class="menueToTop">
					<a href="#top">nach oben</a>
				</div>
				<?php
					}
					else {
						echo '<p class="warningArea">Es wurden keine Daten gefunden!</p>';
					}
				?>
				<div class="clear"></div>
			</div>
			<div class="clear"></div>
		</div>
	</div>
</div>
<script language="javascript" type="text/javascript">
	$(document).ready(function() {
		$('.buttonShopOrderInfo').css('cursor', 'pointer');
		$('.buttonShopOrderInfo').click(function () {
			loadOrderDetails($(this), '<?php echo BASEPATH; ?>', 'Inhalt der E-Mail');
		});
		setFocus('formFilterSearch', 'searchWord');
		colorRowMouseOver('.displayOrders tbody tr');

		$('#searchOrderNumber').keyup(function () {
			// loadSuggestions('searchOrderNumber', [{'triggerElement': '#searchOrderNumber', 'fieldNumber': '#searchOrderNumber', 'fieldName': '#searchCustomerName', 'fieldCity': '', 'fieldZipCode': ''}], 1);
		});
		$('#searchCustomerName').keyup(function () {
			// loadSuggestions('searchCustomerName', [{'triggerElement': '#searchCustomerName', 'fieldNumber': '#searchOrderNumber', 'fieldName': '#searchCustomerName', 'fieldCity': '', 'fieldZipCode': ''}], 1);
		});
		$('#searchSalesmanName').keyup(function () {
			// loadSuggestions('searchSalesmanName', [{'triggerElement': '#searchSalesmanName', 'fieldName': '#searchSalesmanName'}], 1);
		});
		$('#searchPLZ').keyup(function () {
			// loadSuggestions('searchPLZ', [{'triggerElement': '#searchPLZ', 'fieldZipCode': '#searchPLZ'}], 1);
		});

		$('.buttonNotice').click(function () {
			loadNotice($(this));
		});

		$(document).scroll(function(){
			fixTableHeader('.displayOrders');
		});

		$.datepicker.setDefaults($.datepicker.regional[""]);
		$('#searchSendDate').datepicker($.datepicker.regional["de"]);

	});
</script>

<?php require_once('inc/footerHTML.inc.php'); ?>