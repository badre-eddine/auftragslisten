<?php
	require_once('inc/requires.inc.php');

	if(!$arrGetUserRights["displayOnlineOrders"] && !$arrGetUserRights["importOnlineOrders"]) {
		header('location: ' . PAGE_EXIT_LOCATION);
		exit;
	}

	if($_POST["displayOrdersCount"] !="")
	{
		$displayOrdersCount = $_POST["displayOrdersCount"];
	}
	else {
		$displayOrdersCount = DEFAULT_ONLINE_ORDERS_MONTHS;
	}

	$jswindowMessage = "";
	$warningMessage = '';
	$errorMessage = '';
	$successMessage = '';
	$infoMessage = '';

	$dbConnection = new DB_Connection();
	$db_open = $dbConnection->db_connect();

	$dbConnection_ExternShop = new DB_Connection(DB_HOST_EXTERN_SHOP, '', DB_NAME_EXTERN_SHOP, DB_USER_EXTERN_SHOP, DB_PASSWORD_EXTERN_SHOP);
	$dbOpen_ExternShop = $dbConnection_ExternShop->db_connect();

	// BOF GET TOTAL COUNTS
		$sql = "SELECT

					SUM(`counterCounts`) AS `totalCounterCount`

				FROM `xtc_counter`

				WHERE 1
			";
		$rs = $dbConnection_ExternShop->db_query($sql);
		list($totalCounterCount) = mysqli_fetch_array($rs);
	// EOF GET TOTAL COUNTS

	// BOF READ PAYMENT TYPES
		$arrPaymentTypeDatas = getPaymentTypes();
	// EOF READ PAYMENT TYPES
?>

<?php
	require_once('inc/headerHTML.inc.php');
	$thisTitle = "Besucherzahlen &amp; soziale Netzwerke";
	$headerHTML = preg_replace("/{###TITLE###}/", strip_tags($thisTitle), $headerHTML);
	echo $headerHTML;

	require_once(FILE_MENUE_TOP);
?>
<div id="mainArea">
	<div id="mainContent">
		<?php require_once(FILE_MENUE_SIDEBAR); ?>
		<div id="contentArea">
			<div id="contentAreaElements">
				<h1><?php if(SHOW_PAGE_TITLE_ICONS) { echo '<img src="' . PATH_ICONS_MENUE_TITLES . 'shop.png" alt="" />'; } ?> <?php echo $thisTitle; ?></h1>
				<div class="contentDisplay">
				<p>Anzeige der Benutzer-IPs im Online-Shop &quot;<?php echo COMPANY_INTERNET; ?>&quot;.</p>

				<div id="tabs">
					<div class="adminEditArea">
						<ul>
							<li><a href="#tabs-1">Soziale Netzwerke</a></li>
							<li><a href="#tabs-4">Page Ranks</a></li>
							<li><a href="#tabs-2">Besuche pro IP</a></li>
							<li><a href="#tabs-3">Besuche pro IP (1.Quadrant)</a></li>
						</ul>
					</div>

					<?php displayMessages(); ?>

					<div id="tabs-2">
						<h2>Besuche pro IP</h2>
						<p><b>Besuche insgesamt:</b> <?php echo number_format($totalCounterCount, 0, ',', '.'); ?></p>
						<table cellspacing="0" cellpadding="0" width="100%" class="displayOrders">
							<thead>
								<tr>
									<th style="width:45px;text-align:right;">#</th>
									<th style="width:120px;">IP</th>
									<th style="width:60px;">Besuche</th>
									<th style="width:60px;">%</th>
									<th style="width:90px;">Letzter Besuch</th>
									<th>UserAgent</th>
									<th style="width:50px">Info</th>
								</tr>
							</thead>
							<tbody>
						<?php
							$sql = "SELECT
										`counterIP`,
										`counterDatetime`,
										`counterTime`,
										`counterCounts`,
										`counterUserAgent`

									FROM `xtc_counter`

									HAVING `counterCounts` > 100
									ORDER BY `counterCounts` DESC
								";
							$rs = $dbConnection_ExternShop->db_query($sql);


							$countRow = 0;
							$rs = $dbConnection_ExternShop->db_query($sql);


							while ($ds = mysqli_fetch_array($rs)) {
								if($countRow%2 == 0){ $rowClass = 'row1'; }
								else { $rowClass = 'row0'; }

								echo '<tr class="' . $rowClass . '">';

								echo '<td style="text-align:right;">';
								echo '<b>'.($countRow + 1).'.</b> ';

								echo '<td>';
								echo '<a href="' . PAGE_LOCALIZE_IP . $ds["counterIP"] . '" target="_blank">' . $ds["counterIP"] . '</a>';
								echo '</td>';

								echo '<td style="text-align:right;">';
								echo number_format($ds["counterCounts"], 0, ",", ".");
								echo '</td>';

								echo '<td>';
								echo number_format(($ds["counterCounts"] / $totalCounterCount) * 100, 2, ",", "");
								echo '</td>';

								echo '<td>';
								echo date("Y-m-d", $ds["counterTime"]);
								echo '</td>';

								echo '<td>';
								echo $ds["counterUserAgent"];
								echo '</td>';

								echo '<td>';
								echo '<span class="toolItem">';
								echo '<img src="layout/icons/iconInfo.png" class="buttonShopOrderInfo" alt="Details" title="Detailinformationen ansehen">';
								echo '<div class="orderDetailsContent" style="display:none; visibility:hidden;">';
								echo '</div>';
								echo '</span>';
								echo '</td>';

								echo '</tr>';
								$countRow++;
							}
						?>
							</tbody>
						</table>
					</div>

					<div id="tabs-3">
						<h2>Besuche pro IP (1.Quadrant)</h2>

						<p><b>Besuche insgesamt:</b> <?php echo number_format($totalCounterCount, 0, ',', '.'); ?></p>
						<table cellspacing="0" cellpadding="0" width="100%" class="displayOrders">
							<thead>
								<tr>
									<th style="width:45px;text-align:right;">#</th>
									<th style="width:50px;">IP</th>
									<th>Besuche</th>
									<th style="width:60px;">%</th>
									<th>UserAgent</th>
									<th style="width:20px">Info</th>
								</tr>
							</thead>
							<tbody>
						<?php
							$sql = "SELECT
										SUBSTRING(`counterIP`, 1, (POSITION('.' IN `counterIP`) - 1)) AS `counterIP_first_part`,
										SUM(`counterCounts`) AS `counterCounts`,
										MIN(`counterDatetime`) AS `counterDatetimeMin`,
										MAX(`counterDatetime`) AS `counterDatetimeMax`,
										GROUP_CONCAT(`counterUserAgent` SEPARATOR '###') AS `counterUserAgent`

									FROM `xtc_counter`

									GROUP BY `counterIP_first_part`

									HAVING `counterCounts` > 500

									ORDER BY `counterCounts` DESC
								";
							$rs = $dbConnection_ExternShop->db_query($sql);


							$countRow = 0;
							$rs = $dbConnection_ExternShop->db_query($sql);

							while ($ds = mysqli_fetch_array($rs)) {
								if($countRow%2 == 0){ $rowClass = 'row1'; }
								else { $rowClass = 'row0'; }

								echo '<tr class="' . $rowClass . '">';

								echo '<td style="text-align:right;">';
								echo '<b>'.($countRow + 1).'.</b> ';

								echo '<td>';
								echo $ds["counterIP_first_part"];
								echo '</td>';

								echo '<td>';
								echo $ds["counterCounts"];
								echo '</td>';

								echo '<td>';
								echo number_format(($ds["counterCounts"] / $totalCounterCount) * 100, 2, ",", "");
								echo '</td>';

								echo '<td>';
								$strUserAgent = $ds["counterUserAgent"];
								$arrTemp = explode("###", $strUserAgent);
								$arrUserAgent = array_unique($arrTemp);
								sort($arrUserAgent);

								echo $arrUserAgent[0];
								echo '</td>';

								echo '<td>';
								echo '<span class="toolItem">';
								echo '<img src="layout/icons/iconInfo.png" class="buttonShopOrderInfo" alt="Details" title="Detailinformationen ansehen">';
								echo '<div class="orderDetailsContent" style="display:none; visibility:hidden;">';
								if(!empty($arrUserAgent)){
									echo '<ul>';
									echo '<li>';
									echo implode("</li><li>", $arrUserAgent);
									echo '</li>';
									echo '</ul>';
								}
								echo '</div>';
								echo '</span>';
								echo '</td>';

								echo '</tr>';
								$countRow++;
							}
						?>
							</tbody>
						</table>
					</div>

					<div id="tabs-1">
						<h2>Soziale Netzwerke</h2>
						<table cellspacing="0" cellpadding="0" width="100%" class="displayOrders">
							<tr>
								<th style="width:45px;text-align:right;">#</th>
								<th>Soziales Netzwerk</th>
								<th style="width:50px;">Seite</th>
							</tr>

							<!-- BOF FACEBOOK BOX -->
							<?php
								// $facebookUrl = 'https://de-de.facebook.com/Burhan.Car.Trade.Requirement';
								$facebookUrl = 'https://www.facebook.com/Burhan.Car.Trade.Requirement';
								// $facebookUrl = 'http://www.facebook.de/Burhan.Car.Trade.Requirement/';
								$facebookLayout = 'button_count'; // standard | button_count | box_count
								$facebookDataFont="trebuchet ms";
								$facebookDataAction="like"; // like | recommend
								$facebookShowFaces = false; // false | true
								$facebookColorscheme = light; // light | dark
								$facebookWidth = 160; //190;
								$facebookHeight = 20;// 24;
							?>
							<tr class="row0">
								<td style="text-align:right;"><b>1.</b> </td>
								<td>
									<script language="javascript" type="text/javascript">
										<!--
										/* <![CDATA[ */
										var contentFacebookPlugin = '';
										contentFacebookPlugin += '<iframe id="iframeFACEBOOK" src="http://www.facebook.com/plugins/like.php?href=<?php echo $facebookUrl; ?>/&amp;layout=<?php echo $facebookLayout; ?>&amp;show_faces=<?php echo $facebookShowFaces; ?>&amp;width=<?php echo $facebookWidth; ?>&amp;action=like&amp;colorscheme=<?php echo $facebookColorscheme; ?>&amp;height=<?php echo $facebookHeight; ?>" scrolling="no" frameborder="0" style="border:none; overflow:hidden; width:<?php echo $facebookWidth; ?>px; height:<?php echo $facebookHeight; ?>px;" allowTransparency="false"></iframe>			';
										document.write(contentFacebookPlugin);
										// $('#iframeFACEBOOK').contents().find('body *').css('background-color', '#FF0000');
										/* ]]> */
										// -->
									</script>
								</td>
								<td><span class="toolItem"><a href="<?php echo $facebookUrl; ?>" target="_blank"><img src="layout/icons/iconWeb.gif" width="16" height="16" title="Netzwerk aufrufen" alt="Homepage" /></a></span></td>
							</tr>
							<!-- EOF FACEBOOK BOX -->

							<!-- BOF TWITTER BOX -->
							<?php
								$twitterUrl = 'https://twitter.com/Burhan_CTR';
								$twitterDataShowCount="true"; // false | true
								$twitterDataLang="de";
								$twitterDataShowScreenName="false";	 // false | true
							?>
							<tr class="row1">
								<td style="text-align:right;"><b>2.</b> </td>
								<td>
									<script language="javascript" type="text/javascript">
										<!--
										/* <![CDATA[ */
										var contentTwitterPlugin = '';
										contentTwitterPlugin += '<a href="<?php echo $twitterUrl; ?>" class="twitter-follow-button" data-show-count="<?php echo $twitterDataShowCount; ?>" data-lang="<?php echo $twitterDataLang; ?>" data-show-screen-name="<?php echo $twitterDataShowScreenName; ?>">@twitter folgen</a>';
										document.write(contentTwitterPlugin);
										$(document).ready(function() {
											!function(d,s,id){	var js,fjs=d.getElementsByTagName(s)[0],p=/^http:/.test(d.location)?'http':'https';if(!d.getElementById(id)){	js=d.createElement(s);js.id=id;js.src=p+'://platform.twitter.com/widgets.js';fjs.parentNode.insertBefore(js,fjs);}	}(document, 'script', 'twitter-wjs');
										});
										/* ]]> */
										// -->
									</script>
								</td>
								<td><span class="toolItem"><a href="<?php echo $twitterUrl; ?>" target="_blank"><img src="layout/icons/iconWeb.gif" width="16" height="16" title="Netzwerk aufrufen" alt="Homepage" /></a></span></td>
							</tr>
							<!-- EOF TWITTER BOX -->

							<!-- BOF GOOGLE+ BOX -->
							<?php
								// $googleplusUrl = 'https://plus.google.com/114741124923997755317/posts';
								$googleplusUrl = 'https://plus.google.com/b/114741124923997755317/114741124923997755317/posts';
							?>
							<tr class="row0">
								<td style="text-align:right;"><b>3.</b></td>
								<td>
									<script language="javascript" type="text/javascript">
										<!--
										/* <![CDATA[ */
										$(document).ready(function() {
											window.___gcfg = {lang: 'de'};
											(function() {
												var po = document.createElement('script'); po.type = 'text/javascript';
												po.async = true;
												po.src = 'https://apis.google.com/js/plusone.js';
												var s = document.getElementsByTagName('script')[0];
												s.parentNode.insertBefore(po, s);
											})();
										});
										var contentGooglePlusPlugin = '';
										contentGooglePlusPlugin += '<div class="g-follow" data-annotation="bubble" data-height="20" data-href="<?php echo $googleplusUrl; ?>" data-rel="publisher"></div>';
										document.write(contentGooglePlusPlugin);
										/* ]]> */
										// -->
									</script>
								</td>
								<td><span class="toolItem"><a href="<?php echo $googleplusUrl; ?>" target="_blank"><img src="layout/icons/iconWeb.gif" width="16" height="16" title="Netzwerk aufrufen" alt="Homepage" /></a></span>	</td>
							</tr>
							<!-- EOF GOOGLE+ BOX -->

							<!-- BOF YOUTUBE BOX -->
							<?php
								// $youTubeUrl = 'http://www.youtube.com/channel/UCIHJU1XPSnTgV3AAgN4DeRQ';
								// $youTubeUrl = 'http://www.youtube.com/user/BurhanCarTrade/';
								$youTubeUrl = 'http://www.youtube.com/BurhanCarTrade/';
								$youTubeUserChannel = 'BurhanCarTrade';
								$youTubeUserChannelID = 'UCIHJU1XPSnTgV3AAgN4DeRQ';
								$youTubeUserChannelApiKey = 'AIzaSyDxB0jouKhQua7mAkgdTVwE0HOQYMqtbf4';
								function getYouTubeSubscriberCount($user) {
									$data = file_get_contents(sprintf('http://gdata.youtube.com/feeds/api/users/%s', $user));
									$matches = array();
									preg_match("~subscriberCount='(?<count>\d+)'~", $data, $matches);
									return isset($matches['count']) ? $matches['count'] : 0 ;
								}
								function getYouTubeTotalViews($user) {
									$data = file_get_contents(sprintf('http://gdata.youtube.com/feeds/api/users/%s', $user));
									$matches = array();
									preg_match("~totalUploadViews='(?<count>\d+)'~", $data, $matches);
									return isset($matches['count']) ? $matches['count'] : 0 ;
								}
								function getYouTubeTotalViews2($youTubeUserChannelID, $youTubeUserChannelApiKey) {
									$getUrl = "https://www.googleapis.com/youtube/v3/channels?part=statistics&id=" .  $youTubeUserChannelID . "&key=" . $youTubeUserChannelApiKey;
									$data = file_get_contents($getUrl);
									$jsonData = json_decode($data, true);
									$youTubeTotalViews = $jsonData['items']['0']['statistics']['viewCount'];
									$youTubeTotalViews = number_format($youTubeTotalViews, 0, ',', '.');
									return $youTubeTotalViews;
								}
								// $youTubeSubscriberCount = getYouTubeSubscriberCount($youTubeUserChannel);
								#$youTubeTotalViews = getYouTubeTotalViews($youTubeUserChannel);
								$youTubeTotalViews = getYouTubeTotalViews2($youTubeUserChannelID, $youTubeUserChannelApiKey);
							?>
							<tr class="row1">
								<td style="text-align:right;"><b>4.</b></td>
								<td>
									<div style="position:relative;">
									<a href="<?php echo $youTubeUrl; ?>" class="youTubeButton" target="_blank"><img src="layout/icons/youTube.png" width="60" height="25" alt="Burhan CTR auf youTube" title="Burhan CTR auf youTube" /></a>
									<span class="note" style="position:absolute;top:6px; left:70px;border-top:1px solid #CCCCCC;border-right:1px solid #CCCCCC;border-bottom:1px solid #CCCCCC;border-left:0px solid #CCCCCC;padding:2px; background-color:#FFF;color:#666; border-radius:2px;"><?php echo $youTubeTotalViews; ?> Views</span>
									<span style="display:block;position:absolute;top:6px; left:65px;padding:0;margin:0;width:5px;height:20px; background: url(http://ssl.gstatic.com/s2/oz/images/sprites/plus-button-e63a56248fe873b92b3cab26945f7793.png) no-repeat scroll -27px -36px rgba(0, 0, 0, 0);"></span>
									</div>
								</td>
								<td><span class="toolItem"><br /><a href="<?php echo $youTubeUrl; ?>" target="_blank"><img src="layout/icons/iconWeb.gif" width="16" height="16" title="Netzwerk aufrufen" alt="Homepage" /></a></span></td>
							</tr>
							<!-- EOF YOUTUBE BOX -->
						</table>
					</div>

					<div id="tabs-4">
						<h2>Page Ranks</h2>
						<table cellspacing="0" cellpadding="0" width="100%" class="displayOrders">
							<tr>
								<th style="width:45px;text-align:right;">#</th>
								<th>Anbieter</th>
								<th style="width:50px;">Info</th>
							</tr>
							<!-- BOF GOOGLE PAGE RANK BOX -->
							<?php
								require_once(PATH_PR_CLASS);
								$urlPageRank = constant("PATH_ONLINE_SHOP_" . strtoupper(MANDATOR));
								$pageRankGoogle = new GOOGLE_PAGERANK();
								$googleUrl = 'http://www.google.de/';
								$googleRank = $pageRankGoogle->get_google_pagerank($urlPageRank);
							?>
							<tr class="row0">
								<td style="text-align:right;"><b>5.</b></td>
								<td>
									<img src="layout/icons/iconGooglePagerank.png" width="45" height="30" alt="Google PageRank" title="Google PageRank: <?php echo $urlPageRank; ?>" />  <?php echo $googleRank; ?>
								</td>
								<td><span class="toolItem"><br /><a href="<?php echo $googleUrl; ?>" target="_blank"><img src="layout/icons/iconWeb.gif" width="16" height="16" title="Netzwerk aufrufen" alt="Homepage" /></a></span></td>
							</tr>
							<!-- EOF GOOGLE PAGE RANK BOX -->

							<!-- BOF ALEXA PAGE RANK BOX -->
							<?php
								$urlAlexaPageRank = constant("PATH_ONLINE_SHOP_" . strtoupper(MANDATOR));
								$alexaDataUrl = preg_replace("/{###URL###}/", $urlAlexaPageRank, PATH_ALEXA_RANK);
								$alexaUrl = 'http://www.alexa.com/';
								$alexaContentXML = implode('', file($alexaDataUrl));
								$alexaDataXml = new SimpleXMLElement($alexaContentXML);

								$objAlexaData = $alexaDataXml->SD[1]->POPULARITY->attributes();
								$arrAlexaData = (array) $objAlexaData;
								$arrAlexaData = $arrAlexaData['@attributes'];
								$alexaRank = $arrAlexaData["TEXT"];

								// BOF ALEXA SEO STATS
								// BOF ALEXA SEO STATS
							?>
							<tr class="row1">
								<td style="text-align:right;"><b>6.</b></td>
								<td>
									<img src="layout/icons/iconAlexaPagerank.png" width="45" height="30" alt="Alexa PageRank" title="Alexa PageRank: <?php echo $urlPageRank; ?>" />  <?php echo $alexaRank; ?>
								</td>
								<td><span class="toolItem"><br /><a href="<?php echo $alexaUrl; ?>" target="_blank"><img src="layout/icons/iconWeb.gif" width="16" height="16" title="Netzwerk aufrufen" alt="Homepage" /></a></span></td>
							</tr>
							<!-- EOF ALEXA PAGE RANK BOX -->
						</table>
					</div>
					</div>
				</div>
			</div>
		</div>
		<div class="clear"></div>
	</div>
</div>

<script language="javascript" type="text/javascript">
	$(document).ready(function() {
		$('.buttonShopOrderInfo').css('cursor', 'pointer');
		$('.buttonShopOrderInfo').click(function () {
			loadOrderDetails($(this), '<?php echo BASEPATH; ?>', '');
		});
		colorRowMouseOver('.displayOrders tbody tr');
		// toggleAreas();

		$(function() {
			$('#tabs').tabs();
		});
	});
</script>

<?php
	$dbConnection_ExternShop->db_close();
?>

<?php require_once('inc/footerHTML.inc.php'); ?>