<?php
	require_once('inc/requires.inc.php');

	if(!$arrGetUserRights["editFinancialDatas"]) {
		header('location: ' . PAGE_EXIT_LOCATION);
		exit;
	}

	DEFINE('BASEPATH_Auftragslisten', '');
	DEFINE('PATH_IMPORTED_FACTORING_FILES', BASEPATH_Auftragslisten . 'importedFactoringFiles/' . MANDATOR . '/');

	DEFINE('KREDITOREN_ID_BCTR', '2311'); // BCTR
	DEFINE('KREDITOREN_ID_B3', '2349'); // B3

	DEFINE('DATEV_GEGENKONTO', '1364'); // DATEVE - GEGENKONTO B3 & BCTR

	DEFINE('KREDITOREN_ID', constant("KREDITOREN_ID_" . strtoupper(MANDATOR))); // B3

	// firmen_id: 1 = Universal Factoring GmbH
	
	// BOF SET SEARCH DATA FOR STORING CHANGED IMPORTED DATA		
		if($_REQUEST["reSubmitSearch"] != '') {
			$_REQUEST["submitSearch"] = $_REQUEST["reSubmitSearch"];
		}

		if($_REQUEST["reSearchImportedFactoringFile"] != '') {
			$_REQUEST["searchImportedFactoringFile"] = $_REQUEST["reSearchImportedFactoringFile"];
		}

		if($_REQUEST["reFilterFactoringFileTypes"] != '') {
			$_REQUEST["filterFactoringFileTypes"] = $_REQUEST["reFilterFactoringFileTypes"];
		}

		if($_REQUEST["reFilterFactoringFileDate"] != '') {
			$_REQUEST["filterFactoringFileDate"] = $_REQUEST["reFilterFactoringFileDate"];
		}		
	// EOF SET SEARCH DATA FOR STORING CHANGED IMPORTED DATA


	$arrFactoringFileTypes = array(
		"RE" => "auszahlung_anhang_re_{###AZ_NUMBER###}.txt",
		"ZA" => "auszahlung_anhang_za_{###AZ_NUMBER###}.txt",
		"GS" => "auszahlung_anhang_gs_{###AZ_NUMBER###}.txt"
	);

	$arrBuchungstexte = array(
		"RE" => "Auszahlung Factoring",
		"ZA" => "Auszahlung 10%",
		"GS" => "Abzug Gutschrift",
	);

	function convertCharsForDATEV($string){
		$contentExportCSV = $string;

		$getEncodingContentExportCSV = mb_detect_encoding($contentExportCSV);

		$sourceEncoding = 'UTF-8';
		$targetEncoding = 'ISO-8859-1'; // ISO-8859-1 | ASCII | CP437
		$targetEncoding = 'ASCII'; // ISO-8859-1 | ASCII | CP437

		if($getEncodingContentExportCSV == $sourceEncoding){
			#$contentExportCSV = iconv($sourceEncoding, $targetEncoding, $contentExportCSV);
			## $contentExportCSV = mb_convert_encoding($contentExportCSV, $targetEncoding, $sourceEncoding);
			$contentExportCSV = utf8_decode($contentExportCSV);
		}
		$getEncodingContentExportCSV = mb_detect_encoding($contentExportCSV);

		return $contentExportCSV;
	}

	function cutDocumentNumber($string){
		if(preg_match("/^(RE|GU|AB|MA|M1|M2|M3)-[0-9]{10}$/", $string)){
			$string = substr($string, 3);
		}
		return $string;
	}

	function getFactoringFileType($fileName){
		global $arrFactoringFileTypes;
		$factoringFileType = "";
		if(!empty($arrFactoringFileTypes)){
			foreach($arrFactoringFileTypes as $thisFactoringFileTypesKey => $thisFactoringFileTypesValue){
				$pattern = $thisFactoringFileTypesValue;
				$pattern = preg_replace("/\./", "\.", $pattern);
				$pattern = preg_replace("/\{###AZ_NUMBER###\}/", "[0-9]{1,}", $pattern);
				if(preg_match("/" . $pattern . "/", $fileName)){
					$factoringFileType = $thisFactoringFileTypesKey;
				}
			}
		}
		return $factoringFileType;
	}

	// BOF FIELD RELATIONS TXT / DB TO PDF
		function getFactoringFieldTitle($string ,$type='RE'){
			global $arrFieldRelationsDBtoPDF;
			$newFieldTitle = $string;
			if($arrFieldRelationsDBtoPDF[$type][$string] != ''){
				$newFieldTitle = $arrFieldRelationsDBtoPDF[$type][$string];
			}
			return $newFieldTitle;
		}

		$arrFieldRelationsDBtoPDF = array();

		// BOF TXT FIELDS
			/*
			auszahlung_anhang_re
			deb_id_vom_kreditor	trans_kz	sortierung	rechnung_nr	rechnung_betrag	sperrkonto_prz	rechnung_datum	buchungsbetrag_sk_index_5_1	buchungsbetrag_sk_index_5_2	summe_hw_kre	az_nummer	factoringart	kreditoren_id	kreditorenname_1	simulation	buchungsbetrag_sk_index_5_3	rechnungs_id	auszahlung_datum	fir_re_nummer	firmen_id	buchungsbetrag_sk_index_5_4	buchungsbetrag_sonst_hw_kre	buchungsbetrag_sk_index_5_5	anzeigeformat	anzeigeformat	buchungsbetrag_sk_index_5_6

			auszahlung_anhang_za
			deb_id_vom_kreditor	trans_kz	sortierung	beleg_nr	beleg_betrag	sperrkonto_prz	beleg_datum	buchungsbetrag_sk_index_7_1	buchungsbetrag_sk_index_7_2	summe_hw_kre	az_nummer	factoringart	zahlung_nr	zahlung_datum	zahlung_betrag	kreditoren_id	kreditorenname_1	table_name	simulation	zahlungs_id	auszahlung_datum	fir_re_nummer	firmen_id	buchungsbetrag_sk_index_7_3	buchungsbetrag_sk_index_7_4	buchungsbetrag_sk_index_7_5	buchungsbetrag_sonst_hw_kre	anzeigeformat	anzeigeformat	anzeigeformat

			auszahlung_anhang_gs
			deb_id_vom_kreditor	trans_kz	sortierung	rechnung_nr	rechnung_betrag	rechnung_datum	buchungsbetrag_sk_index_6_1	buchungsbetrag_sk_index_6_2	az_nummer	gutschrift_nr	gutschrift_datum	gutschrift_betrag	summe_hw_kre	kreditoren_id	kreditorenname_1	simulation	buchungsbetrag_sk_index_6_3	gutschrifts_id	auszahlung_datum	fir_re_nummer	firmen_id	buchungsbetrag_sk_index_6_4	buchungsbetrag_sk_index_6_5	buchungsbetrag_sonst_hw_kre	anzeigeformat	anzeigeformat	anzeigeformat	belegart_id
			*/
		// EOF TXT FIELDS

		# BOF auszahlung_anhang_re_xxx.txt
			$arrFieldRelationsDBtoPDF["RE"] = array(
				"importFiletype" => "Dateityp",
				"importFilename" => "Dateiname",
				"importDate" => "Import-Datum",

				"customersTaxAccountID" => "FiBu-Nr",
				"customersFirmenname" => "Kunde",

				"deb_id_vom_kreditor" => "Debitor",
				"trans_kz" => "Trans-Art",
				"sortierung" => "Sortierung",
				"rechnung_nr" => "RE-Nr.",
				"rechnung_betrag" => "RE-Betrag",
				"sperrkonto_prz" => "SperrKto. %",
				"rechnung_datum" => "RE-Datum",
				"buchungsbetrag_sk_index_5_1" => "eingereichte Ford.",
				"buchungsbetrag_sk_index_5_2" => "nicht angekaufte Ford.",
				"summe_hw_kre" => "Summe",
				"az_nummer" => "Ausz-Nr.",
				"factoringart" => "Vertragsart",
				"kreditoren_id" => "Kunden-Nr",
				"kreditorenname_1" => "Kunden-Name",
				"simulation" => "",
				"buchungsbetrag_sk_index_5_3" => "Sperrkonto",
				"rechnungs_id" => "",
				"auszahlung_datum" => "Auszahlungsdatum",
				"fir_re_nummer" => "Rechnungsnummer Auszahlung",
				"firmen_id" => "",				// 1 = Universal Factoring GmbH
				"buchungsbetrag_sk_index_5_4" => "Warenstreit",
				"buchungsbetrag_sonst_hw_kre" => "Sonstiges",
				"buchungsbetrag_sk_index_5_5" => "Factoring Geb.",
				"anzeigeformat" => "",
				"anzeigeformat" => "",
				"buchungsbetrag_sk_index_5_6" => "Umsatzsteuer"
			);
		# EOF auszahlung_anhang_re_xxx.txt


		# BOF auszahlung_anhang_za_xxx.txt
			$arrFieldRelationsDBtoPDF["ZA"] = array(
				"importFiletype" => "Dateityp",
				"importFilename" => "Dateiname",
				"importDate" => "Import-Datum",

				"customersTaxAccountID" => "FiBu-Nr",
				"customersFirmenname" => "Kunde",

				"deb_id_vom_kreditor" => "Debitor",
				"trans_kz" => "Trans-Art",
				"sortierung" => "Sortierung",
				"beleg_nr" => "Beleg-Nr.",
				"beleg_betrag" => "Beleg-Betrag",
				"sperrkonto_prz" => "Sperrkonto Satz",
				"beleg_datum" => "Beleg-Datum",
				"buchungsbetrag_sk_index_7_1" => "eingereichte Ford.",
				"buchungsbetrag_sk_index_7_2" => "nicht angekaufte Ford.",
				"summe_hw_kre" => "Summe",
				"az_nummer" => "Ausz-Nr.",
				"factoringart" => "Vertragsart",
				"zahlung_nr" => "Zahlung-Nr.",
				"zahlung_datum" => "Zahlungseingang",
				"zahlung_betrag" => "Zahlbetrag",
				"kreditoren_id" => "Kunden-Nr",
				"kreditorenname_1" => "Kunden-Name",
				"table_name" => "Zuordnung Beleg Art",
				"simulation" => "",
				"zahlungs_id" => "Journal-Nr.",
				"auszahlung_datum" => "Auszahlungsdatum",
				"fir_re_nummer" => "Rechnungsnummer Auszahlung",
				"firmen_id" => "",					// 1 = Universal Factoring GmbH
				"buchungsbetrag_sk_index_7_3" => "Sperrkonto",
				"buchungsbetrag_sk_index_7_4" => "Warenstreit",
				"buchungsbetrag_sk_index_7_5" => "Sonstiges Div",
				"buchungsbetrag_sonst_hw_kre" => "Umsatzsteuer", // "Sonstiges div. ",
				"anzeigeformat" => "",
				"anzeigeformat" => "",
				"anzeigeformat" => ""
			);
		# EOF auszahlung_anhang_za_xxx.txt

		# BOF auszahlung_anhang_gs_xxx.txt
			$arrFieldRelationsDBtoPDF["GS"] = array(
				"importFiletype" => "Dateityp",

				"customersTaxAccountID" => "FiBu-Nr",
				"customersFirmenname" => "Kunde",

				"importFilename" => "Dateiname",
				"importDate" => "Import-Datum",

				"deb_id_vom_kreditor" => "Debitor",
				"trans_kz" => "Trans-Art",
				"sortierung" => "Sortierung",
				"rechnung_nr" => "RE-Nr.",
				"rechnung_betrag" => "RE-Betrag",
				"rechnung_datum" => "RE-Datum",
				"buchungsbetrag_sk_index_6_1" => "eingereichte Ford.",
				"buchungsbetrag_sk_index_6_2" => "nicht angekaufte Ford.",
				"az_nummer" => "Ausz-Nr.",
				"gutschrift_nr" => "GU-Nr.",
				"gutschrift_datum" => "GU-Datum",
				"gutschrift_betrag" => "GU-Betrag",
				"summe_hw_kre" => "Summe",
				"kreditoren_id" => "Kunden-Nr",
				"kreditorenname_1" => "Kunden-Name",
				"simulation" => "",
				"buchungsbetrag_sk_index_6_3" => "Sperrkonto",
				"gutschrifts_id" => "",
				"auszahlung_datum" => "Auszahlungsdatum",
				"fir_re_nummer" => "Rechnungsnummer Auszahlung",
				"firmen_id" => "",					// 1 = Universal Factoring GmbH
				"buchungsbetrag_sk_index_6_4" => "Warenstreit",
				"buchungsbetrag_sk_index_6_5" => "Sonstiges Div",
				"buchungsbetrag_sonst_hw_kre" => "Umsatzsteuer",
				"anzeigeformat" => "",
				"anzeigeformat2" => "",
				"anzeigeformat3" => "",
				"belegart_id" => ""
			);
		# EOF auszahlung_anhang_gs_xxx.txt

	// EOF FIELD RELATIONS TXT / DB TO PDF

	// BOF TRANSACTION LEGEND
		$arrTransactionsTypes = array(
			"RANK" => "Ankauf Rechnung",
			"RENK" => "Nachkauf Rechnung",
			"RERK" => "Rückkauf Rechnung",
			"REWS" => "Warenstreit Rechnung",
			"RADF" => "Ausbuchung Delkrederefall",
			"READ" => "Ausbuchung Rechnung Debitor",
			"REAF" => "Ausbuchung Rechnung Firma",
			"REAK" => "Ausbuchung Rechnung Kunde",
			"DERE" => "Debitorische Rechnung",
			"REDF" => "Delkrederefall Rechnung",
			"REST" => "Storno Rechnung",
			"REDS" => "Storno Delkrederefall Rechnung",
			"RADS" => "Storno Ausbuchung Delkrederefall Rechnung",

			"ZAEG" => "Zahlungseingang",
			"ZADZ" => "Direktzahlung",
			"ZANZ" => "Nachträgliche Zuordnung der Zahlung",
			"ZASN" => "Stornierung Zuordnung Zahlung",
			"ZAAF" => "Ausbuchung Zahlung Firma",
			"ZAAK" => "Ausbuchung Zahlung Kunde",
			"ZAAD" => "Ausbuchung Zahlung Debitor",
			"ZAST" => "Storno der Zahlung",
			"ZARL" => "Rücklastschrift der Zahlung",

			"GSEG" => "Gutschriftseingang",
			"GSNZ" => "Nachträgliche Zuordnung der Gutschrift",
			"GSSN" => "Storno Zuordnung Gutschrift",
			"GSSP" => "Änderung Sperrkonto Gutschrift",
			"GSAF" => "Ausbuchung Gutschrift Firma",
			"GSAK" => "Ausbuchung Gutschrift Kunde",
			"GSAD" => "Ausbuchung Gutschrift Debitor",
			"GSST" => "Storno der Gutschrift"
		);
		if(!empty($arrTransactionsTypes)){
			#ksort($arrTransactionsTypes);
		}
	// EOF TRANSACTION LEGEND

	$jswindowMessage = '';
	$warningMessage = '';
	$errorMessage = '';
	$successMessage = '';
	$infoMessage = '';

	function formatDecimal($value){
		$formattedValue = number_format($value, 2, ",", ".");
		if($value < 0){
			$formattedValue = '<span style="color:#990000;">' . $formattedValue . '</span>';
		}
		return $formattedValue;
	}

	$dbConnection = new DB_Connection();
	$db_open = $dbConnection->db_connect();

	if($_GET["downloadFile"] != "") {
		require_once('classes/downloadFile.class.php');
		$downloadPath = '';
		if($_GET["downloadType"] == "EXPORT"){
			$downloadPath = PATH_IMPORTED_FACTORING_FILES;
			$thisDownloadFile = $_GET["downloadFile"];
		}
		else if(in_array($_GET["downloadType"], array("GU", "RE"))){
			$downloadPath = DIRECTORY_CREATED_DOCUMENTS_CUSTOMERS;
			$thisDownloadFile = searchDownloadFile($downloadPath . $_GET["downloadFile"], $_GET["downloadType"]);
		}
		$thisDownload = new downloadFile($downloadPath, $thisDownloadFile);
		$errorMessage .= $thisDownload->startDownload();
	}

	// BOF UPLOAD FILE
		if($_POST["submitFileUpload"] != ''){
			if($_FILES["importFactoringFile"] != ''){	

				if($_FILES["importFactoringFile"]["name"] != "" && $_FILES["importFactoringFile"]["tmp_name"] != "" && preg_match("/^[a-zA-Z]{1,}_[a-zA-Z]{1,}_[a-zA-Z]{2}_[0-9]{1,}\.txt$/", $_FILES["importFactoringFile"]["name"])){
					// BOF CHECK FILE NAME AND GET IMPORT FILE TYPE
						// auszahlung_anhang_re_158.txt || auszahlung_anhang_za_158.txt || auszahlung_anhang_gs_158.txt
						$importFactoringFileType = getFactoringFileType($_FILES["importFactoringFile"]["name"]);
					// EOF CHECK FILE NAME AND GET IMPORT FILE TYPE


					if(in_array($importFactoringFileType, array("RE", "ZA", "GS"))){
						#$importFactoringFileCopyName = MANDATOR . '_' . $_FILES["importFactoringFile"]["name"];
						$importFactoringFileCopyName = $_FILES["importFactoringFile"]["name"];

						if(file_exists(PATH_IMPORTED_FACTORING_FILES . $importFactoringFileCopyName)){
							$warningMessage .= 'Die Datei &quot;' . $importFactoringFileCopyName . '&quot; existiert schon und wird umbenannt!' . '<br />';

							$backUpFilename = md5(basename($importFactoringFileCopyName) . date('YmdHis'));

							$arrFileInfo = pathinfo(basename($importFactoringFileCopyName));
							$backUpFilename = $arrFileInfo["filename"] . '_' . md5(basename($importFactoringFileCopyName) . date('YmdHis')) . '.' . $arrFileInfo["extension"];
							$rs_copy = copy(PATH_IMPORTED_FACTORING_FILES . $importFactoringFileCopyName, PATH_IMPORTED_FACTORING_FILES . $backUpFilename);
						}
						if(1) {
							$rs_copy = copy($_FILES["importFactoringFile"]["tmp_name"], PATH_IMPORTED_FACTORING_FILES . $importFactoringFileCopyName);
							if($rs_copy){
								$successMessage .= 'Die Datei &quot;' . $importFactoringFileCopyName . '&quot; wurde ins Auftragslisten-Verzeichnis kopiert!' . '<br />';
								unlink($_FILES["importFactoringFile"]["tmp_name"]);
							}
							else {
								$errorMessage .= 'Die Datei &quot;' . $importFactoringFileCopyName . '&quot; konnte nicht ins Auftragslisten-Verzeichnis kopiert werden!' . '<br />';
							}
						}


						/*
						if(file_exists(PATH_IMPORTED_FACTORING_FILES . $importFactoringFileCopyName)){
							$warningMessage .= 'Die Datei &quot;' . $importFactoringFileCopyName . '&quot; existiert schon und wird nicht ins Auftragslisten-Verzeichnis kopiert!' . '<br />';
						}
						else {
							$rs_copy = copy($_FILES["importFactoringFile"]["tmp_name"], PATH_IMPORTED_FACTORING_FILES . $importFactoringFileCopyName);
							if($rs_copy){
								$successMessage .= 'Die Datei &quot;' . $importFactoringFileCopyName . '&quot; wurde ins Auftragslisten-Verzeichnis kopiert!' . '<br />';
							}
							else {
								$errorMessage .= 'Die Datei &quot;' . $importFactoringFileCopyName . '&quot; konnte nicht ins Auftragslisten-Verzeichnis kopiert werden!' . '<br />';
							}
						}
						*/

						if(file_exists(PATH_IMPORTED_FACTORING_FILES . $importFactoringFileCopyName)){
							$infoMessage .= 'Die Datei &quot;' . $importFactoringFileCopyName . '&quot; wird in die Auftragslisten-Datenbank eingelesen!' . '<br />';

							// BOF DELETE FILE DATAS FROM DB IF ALREADY INSERTED
								$sql_deleteFileData = "
										DELETE FROM
										`" . TABLE_IMPORT_FACTORING_DATA . "`
										WHERE 1
											AND `importFilename` = '" . $importFactoringFileCopyName . "'
											AND `importFiletype` = '" . $importFactoringFileType . "'
									";
								$rs_deleteFileData = $dbConnection->db_query($sql_deleteFileData);
							// EOF DELETE FILE DATAS FROM DB IF ALREADY INSERTED

							// BOF READ FILE AND INSERT INTO DB
								$fieldSeperator = "\t";

								$fp_importFile = fopen(PATH_IMPORTED_FACTORING_FILES . $importFactoringFileCopyName, 'r');
								$countLine;
								while($line_importFile = fgets($fp_importFile)){
									$newLineContent = $line_importFile;

									// $newLineContent = preg_replace("/" . $fieldSeperator . "/", ";", $newLineContent);

									// BOF FORMAT DOUBLE
										$newLineContent = preg_replace("/([0-9]{1,}),([0-9]{1,})/", "$1.$2", $newLineContent);
									// EOF FORMAT DOUBLE

									// BOF FORMAT DATE
										$newLineContent = preg_replace("/([0-9]{2})\.([0-9]{2})\.([0-9]{4})/", "$3-$2-$1", $newLineContent);
									// EOF FORMAT DATE

									// BOF ADD SLASHES
										$newLineContent = addslashes($newLineContent);
										// $newLineContent = preg_replace("/'/", "\'", $newLineContent);
									// EOF ADD SLASHES

									$newLineContent = $importFactoringFileCopyName . $fieldSeperator . $importFactoringFileType . $fieldSeperator . date("Y-m-d") . $fieldSeperator . $newLineContent;

									$arrNewLineContent = explode($fieldSeperator, $newLineContent);

									// BOF INSERT RE
										if($importFactoringFileType == 'RE'){
											$sql_insertLine = "
												INSERT INTO `" . TABLE_IMPORT_FACTORING_DATA . "` (
																	`importFilename`,
																	`importFiletype`,
																	`importDate`,

																	`deb_id_vom_kreditor`,
																	`trans_kz`,
																	`sortierung`,
																	`rechnung_nr`,
																	`rechnung_betrag`,
																	`sperrkonto_prz`,
																	`rechnung_datum`,
																	`buchungsbetrag_sk_index_5_1`,
																	`buchungsbetrag_sk_index_5_2`,
																	`summe_hw_kre`,
																	`az_nummer`,
																	`factoringart`,
																	`kreditoren_id`,
																	`kreditorenname_1`,
																	`simulation`,
																	`buchungsbetrag_sk_index_5_3`,
																	`rechnungs_id`,
																	`auszahlung_datum`,
																	`fir_re_nummer`,
																	`firmen_id`,
																	`buchungsbetrag_sk_index_5_4`,
																	`buchungsbetrag_sonst_hw_kre`,
																	`buchungsbetrag_sk_index_5_5`,
																	`anzeigeformat`,
																	`anzeigeformat2`,
																	`buchungsbetrag_sk_index_5_6`,
																	
																	`beleg_nr`
															)
															VALUES (
																'" . implode("', '", $arrNewLineContent). "',
																
																'0'
															);
												";
										}
									// EOF INSERT RE

									// BOF INSERT GS
										if($importFactoringFileType == 'GS'){
											$sql_insertLine = "
												INSERT INTO `" . TABLE_IMPORT_FACTORING_DATA . "` (
																	`importFilename`,
																	`importFiletype`,
																	`importDate`,

																	`deb_id_vom_kreditor`,
																	`trans_kz`,
																	`sortierung`,
																	`rechnung_nr`,
																	`rechnung_betrag`,
																	`rechnung_datum`,
																	`buchungsbetrag_sk_index_6_1`,
																	`buchungsbetrag_sk_index_6_2`,
																	`az_nummer`,
																	`gutschrift_nr`,
																	`gutschrift_datum`,
																	`gutschrift_betrag`,
																	`summe_hw_kre`,
																	`kreditoren_id`,
																	`kreditorenname_1`,
																	`simulation`,
																	`buchungsbetrag_sk_index_6_3`,
																	`gutschrifts_id`,
																	`auszahlung_datum`,
																	`fir_re_nummer`,
																	`firmen_id`,
																	`buchungsbetrag_sk_index_6_4`,
																	`buchungsbetrag_sk_index_6_5`,
																	`buchungsbetrag_sonst_hw_kre`,
																	`anzeigeformat`,
																	`anzeigeformat2`,
																	`anzeigeformat3`,
																	`belegart_id`,
																	
																	`beleg_nr`
																)
																VALUES (
																	'" . implode("', '", $arrNewLineContent). "',
																	
																	'0'
																);
												";
										}
									// EOF INSERT GS

									// BOF INSERT ZA
										if($importFactoringFileType == 'ZA'){
											$sql_insertLine = "
												INSERT INTO `" . TABLE_IMPORT_FACTORING_DATA . "` (
																	`importFilename`,
																	`importFiletype`,
																	`importDate`,

																	`deb_id_vom_kreditor`,
																	`trans_kz`,
																	`sortierung`,
																	`beleg_nr`,
																	`beleg_betrag`,
																	`sperrkonto_prz`,
																	`beleg_datum`,
																	`buchungsbetrag_sk_index_7_1`,
																	`buchungsbetrag_sk_index_7_2`,
																	`summe_hw_kre`,
																	`az_nummer`,
																	`factoringart`,
																	`zahlung_nr`,
																	`zahlung_datum`,
																	`zahlung_betrag`,
																	`kreditoren_id`,
																	`kreditorenname_1`,
																	`table_name`,
																	`simulation`,
																	`zahlungs_id`,
																	`auszahlung_datum`,
																	`fir_re_nummer`,
																	`firmen_id`,
																	`buchungsbetrag_sk_index_7_3`,
																	`buchungsbetrag_sk_index_7_4`,
																	`buchungsbetrag_sk_index_7_5`,
																	`buchungsbetrag_sonst_hw_kre`,
																	`anzeigeformat`,
																	`anzeigeformat2`,
																	`anzeigeformat3`,
																	
																	`beleg_done`
																)
																VALUES (
																	'" . implode("', '", $arrNewLineContent). "',
																	
																	'0'
																);
												";
										}
									// BOF INSERT ZA

									if($sql_insertLine != ''){
										$rs_insertLine = $dbConnection->db_query($sql_insertLine);

										if($rs_insertLine){
											$successMessage .= 'Zeile #' . ($countLine + 1) . ' der Datei &quot;' . $importFactoringFileCopyName . '&quot; wurde in die Auftragslisten-DB eingelesen!' . '<br />';
										}
										else {
											$errorMessage .= 'Zeile #' . ($countLine + 1) . ' der Datei &quot;' . $importFactoringFileCopyName . '&quot; konnte nicht in die Auftragslisten-DB eingelesen werden!' . '<br />';
										}

										$countLine++;
									}
								}
								fclose($fp_importFile);

								// BOF DELETE LINES WITH FIELD-NAMES
									$sql_deleteFieldnameRow = "DELETE FROM `" . TABLE_IMPORT_FACTORING_DATA . "` WHERE `az_nummer` = 'az_nummer';";
									$rs_deleteFieldnameRow = $dbConnection->db_query($sql_deleteFieldnameRow);
								// BOF DELETE LINES WITH FIELD-NAMES

								// BOF CHECK IF FILE BELONGS TO CURRENT MANDATOR
									if(KREDITOREN_ID != ''){
										$sql_selectWrongMandator = "
												SELECT
													`importFilename`
													FROM `" . TABLE_IMPORT_FACTORING_DATA . "`
													WHERE `kreditoren_id` != '" . KREDITOREN_ID . "'
													GROUP BY `importFilename`;
											";
										$rs_selectWrongMandator = $dbConnection->db_query($sql_selectWrongMandator);
										while($ds_selectWrongMandator = mysqli_fetch_assoc($rs_selectWrongMandator)){
											if($ds_selectWrongMandator["importFilename"] != ''){
												$fileToDelete = PATH_IMPORTED_FACTORING_FILES . $ds_selectWrongMandator["importFilename"];
												if(file_exists($fileToDelete)){
													$rs_fileDelete = unlink($fileToDelete);
													if($rs_fileDelete){
														$errorMessage .= 'Falsche Kreditoren-ID! Die Datei &quot;' . $ds_selectWrongMandator["importFilename"] . '&quot; wurde entfernt!' . '<br />';
													}
													else {
														$errorMessage .= 'Falsche Kreditoren-ID! Die Datei &quot;' . $ds_selectWrongMandator["importFilename"] . '&quot; konnte nicht entfernt werden!' . '<br />';
													}
												}
											}
										}

										$sql_deleteWrongMandator = "DELETE FROM `" . TABLE_IMPORT_FACTORING_DATA . "` WHERE `kreditoren_id` != '" . KREDITOREN_ID . "';";
										$rs_deleteWrongMandator = $dbConnection->db_query($sql_deleteWrongMandator);
										$countDeletedLines = $dbConnection->db_getMysqlAffectedRows($rs_deleteWrongMandator);

										if($rs_deleteWrongMandator && $countDeletedLines > 0){
											$errorMessage .= 'Falsche Kreditoren-ID! ' . $countDeletedLines . ' Zeilen aus der DB entfernt!' . '<br />';
										}
									}
								// EOF CHECK IF FILE BELONGS TO CURRENT MANDATOR
							// EOF READ FILE AND INSERT INTO DB

						}
					}
					else {
						$warningMessage .= 'Es konnte keine Datei-Zuordnung (RE / ZA / GS) gefunden werden!' . '<br />';
					}
				}
				else {
					$warningMessage .= 'Der Dateiname entspricht nicht dem gültigen Format!' . '<br />';
				}
			}
			else {
				$warningMessage .= 'Sie haben keine Datei zum Anzeigen ausgew&auml;hlt!' . '<br />';
			}
		}
	// EOF UPLOAD FILE


/*
UPDATE `" . TABLE_IMPORT_FACTORING_DATA . "` SET `anzeigeformat` = '#,##0.00\' €\';-#,##0.00\' €\'' WHERE `anzeigeformat` != '';
UPDATE `" . TABLE_IMPORT_FACTORING_DATA . "` SET `anzeigeformat2` = '#,##0.00\' €\';[RED]-#,##0.00\' €\'' WHERE `anzeigeformat2` != '';
UPDATE `" . TABLE_IMPORT_FACTORING_DATA . "` SET `anzeigeformat3` = '#,##0.00\' €\';[RED]-#,##0.00\' €\'' WHERE `anzeigeformat3` != '';
UPDATE `" . TABLE_IMPORT_FACTORING_DATA . "` SET `importFilename` = CONCAT('auszahlung_anhang_re_', `az_nummer`, '.txt') WHERE `importFiletype` = 'RE';
UPDATE `" . TABLE_IMPORT_FACTORING_DATA . "` SET `importFilename` = CONCAT('auszahlung_anhang_gs_', `az_nummer`, '.txt') WHERE `importFiletype` = 'GS';
UPDATE `" . TABLE_IMPORT_FACTORING_DATA . "` SET `importFilename` = CONCAT('auszahlung_anhang_za_', `az_nummer`, '.txt') WHERE `importFiletype` = 'ZA';
*/

	// BOF GET LAST IMPORT DATA
		$arrLastImportData = array();
		$sql = "
				SELECT
					GROUP_CONCAT(DISTINCT `importFilename` SEPARATOR ';') AS `MAX_FILE_NAME`,
					`auszahlung_datum` AS `MAX_PAYMENT_DATE`,
					`az_nummer` AS `MAX_AZ_NUMBER`

				FROM `" . TABLE_IMPORT_FACTORING_DATA . "`
				WHERE 1
				GROUP BY `az_nummer`
				ORDER BY `az_nummer` DESC
				LIMIT 1
			";

		$rs = $dbConnection->db_query($sql);

		while($ds = mysqli_fetch_assoc($rs )){
			foreach(array_keys($ds) as $field){
				$arrLastImportData[$field] = $ds[$field];
			}
		}
	// EOF GET LAST IMPORT DATA

	// BOF GET IMPORTED FACTORING FILENAMES AND FILEDATES
		$sql ="
				SELECT
						`importFilename`,
						GROUP_CONCAT(DISTINCT `auszahlung_datum`) AS `auszahlung_datum`,
						GROUP_CONCAT(DISTINCT `az_nummer`) AS `az_nummer`,
						GROUP_CONCAT(DISTINCT `importFiletype`) AS `importFiletype`

					FROM `" . TABLE_IMPORT_FACTORING_DATA . "`

					WHERE 1

					GROUP BY `importFilename`
					ORDER BY LENGTH(`az_nummer`) DESC, `az_nummer` DESC

			";
		$rs = $dbConnection->db_query($sql);

		$arrImportedFilenames = array();
		$arrImportedFileDates = array();
		while($ds = mysqli_fetch_assoc($rs)){
			$arrImportedFilenames[] = array("file" => $ds["importFilename"], "date" => $ds["auszahlung_datum"], "az" => $ds["az_nummer"], "typ" => $ds["importFiletype"]);
			$arrImportedFileDates[] = substr($ds["auszahlung_datum"], 0, 7);
		}
		if(!empty($arrImportedFileDates)){
			$arrImportedFileDates = array_unique($arrImportedFileDates);
			rsort($arrImportedFileDates);
		}
	// EOF GET IMPORTED FACTORING FILENAMES AND FILEDATES

	

	// BOF GET IMPORTED FACTORING FILENAMES AND FILEDATES WITHOUT RE-NR
		$sql ="
				SELECT
						`importFilename`

					FROM `" . TABLE_IMPORT_FACTORING_DATA . "`

					WHERE 1
						AND (
							(`importFiletype` = 'RE' AND (`rechnung_nr` IS NULL OR `rechnung_nr` = ''))
							OR
							(`importFiletype` = 'ZA' AND (`beleg_nr` IS NULL OR `beleg_nr` = ''))
							OR
							(`importFiletype` = 'GS' AND (`gutschrift_nr` IS NULL OR `gutschrift_nr` = ''))
						)

					GROUP BY `importFilename`
					ORDER BY LENGTH(`az_nummer`) DESC, `az_nummer` DESC

			";
		$rs = $dbConnection->db_query($sql);

		$arrImportedFilesWithoutDocumentNumber = array();
		while($ds = mysqli_fetch_assoc($rs)){
			$arrImportedFilesWithoutDocumentNumber[] = $ds["importFilename"];
		}
		if(!empty($arrImportedFilesWithoutDocumentNumber)){
			$arrImportedFilesWithoutDocumentNumber = array_unique($arrImportedFilesWithoutDocumentNumber);
		}
	// EOF GET IMPORTED FACTORING FILENAMES AND FILEDATES WITHOUT RE-NR

	// BOF EXPORT DATA FOR DATEV
		// [exportDataForDatev] => 1
		// [exportFilename] => bctr_auszahlung_anhang_re_160.txt | bctr_auszahlung_anhang_za_160.txt | bctr_auszahlung_anhang_gs_160.txt
		if($_REQUEST["exportDataForDatev"] == '1' && $_REQUEST["exportFilename"] != ''){

			// BOF CHECK FILE NAME AND GET IMPORT FILE TYPE
				$exportFactoringFileType = getFactoringFileType($_REQUEST["exportFilename"]);
			// EOF CHECK FILE NAME AND GET IMPORT FILE TYPE

			// BOF CREATE TMP FILENAME
				$exportStoreFilename = MANDATOR . "_export_factoring_datev_" . $_REQUEST["exportFilename"] . '.csv';
				$exportDownloadFilename = "export_factoring_datev_" . $exportFactoringFileType . '.txt';
				$exportDownloadFilePath = PATH_EXPORT_FILES_TAX_ACCOUNTANT . $exportDownloadFilename;

				if(file_exists($exportDownloadFilePath)){
					unlink($exportDownloadFilePath);
				}
			// EOF CREATE TMP FILENAME

			// BOF WRITE EXPORT FILE
				$fieldSeparator = ";"; // ; | \t

				// BOF CREATE EXPORT SQL DEPENDING ON TYPE
					if($exportFactoringFileType == 'RE'){
						$sql_exportForDATEV = "
								SELECT
									'" . DATEV_GEGENKONTO . "' AS `gegenkonto`,
									`" . TABLE_IMPORT_FACTORING_DATA . "`.`deb_id_vom_kreditor`,
									`" . TABLE_CUSTOMERS . "`.`customersFirmenname`,

									`" . TABLE_CUSTOMERS . "`.`customersTaxAccountID`,
									`" . TABLE_IMPORT_FACTORING_DATA . "`.`rechnung_nr`,

									DATE_FORMAT(`" . TABLE_IMPORT_FACTORING_DATA . "`.`auszahlung_datum`, '%d.%m.%Y') AS `auszahlung_datum`,

									(`" . TABLE_IMPORT_FACTORING_DATA . "`.`summe_hw_kre` - (`" . TABLE_IMPORT_FACTORING_DATA . "`.`buchungsbetrag_sk_index_5_6` + `" . TABLE_IMPORT_FACTORING_DATA . "`.`buchungsbetrag_sk_index_5_5`)) AS `summe_buchung`,

									CONCAT(
										'" . $arrBuchungstexte[$exportFactoringFileType] . "',
										' - AZ',
										`" . TABLE_IMPORT_FACTORING_DATA . "`.`az_nummer`
									) AS `buchungstext`
								FROM `" . TABLE_IMPORT_FACTORING_DATA . "`

								LEFT JOIN `" . TABLE_CUSTOMERS . "`
								ON(`" . TABLE_IMPORT_FACTORING_DATA . "`.`deb_id_vom_kreditor` = `" . TABLE_CUSTOMERS . "`.`customersKundennummer`)

								WHERE 1
									AND `" . TABLE_IMPORT_FACTORING_DATA . "`.`importFilename` = '" . $_REQUEST["exportFilename"] . "'

								HAVING
									`summe_buchung` > 0 OR `summe_buchung` < 0


								ORDER BY
									`" . TABLE_IMPORT_FACTORING_DATA . "`.`deb_id_vom_kreditor` ASC,
									`" . TABLE_IMPORT_FACTORING_DATA . "`.`rechnung_nr` ASC,
									`" . TABLE_IMPORT_FACTORING_DATA . "`.`sortierung` ASC

							";
					}
					else if($exportFactoringFileType == 'ZA'){
						$sql_exportForDATEV = "
								SELECT
									'" . DATEV_GEGENKONTO . "' AS `gegenkonto`,
									`" . TABLE_IMPORT_FACTORING_DATA . "`.`deb_id_vom_kreditor`,
									`" . TABLE_CUSTOMERS . "`.`customersFirmenname`,

									`" . TABLE_CUSTOMERS . "`.`customersTaxAccountID`,
									`" . TABLE_IMPORT_FACTORING_DATA . "`.`beleg_nr`,

									DATE_FORMAT(`" . TABLE_IMPORT_FACTORING_DATA . "`.`auszahlung_datum`, '%d.%m.%Y') AS `auszahlung_datum`,

									`" . TABLE_IMPORT_FACTORING_DATA . "`.`summe_hw_kre` AS `summe_buchung`,

									CONCAT(
										'" . $arrBuchungstexte[$exportFactoringFileType] . "',
										' - AZ',
										`" . TABLE_IMPORT_FACTORING_DATA . "`.`az_nummer`
									) AS `buchungstext`
								FROM `" . TABLE_IMPORT_FACTORING_DATA . "`

								LEFT JOIN `" . TABLE_CUSTOMERS . "`
								ON(`" . TABLE_IMPORT_FACTORING_DATA . "`.`deb_id_vom_kreditor` = `" . TABLE_CUSTOMERS . "`.`customersKundennummer`)

								WHERE 1
									AND `" . TABLE_IMPORT_FACTORING_DATA . "`.`importFilename` = '" . $_REQUEST["exportFilename"] . "'

								HAVING
									`summe_buchung` > 0 OR `summe_buchung` < 0

								ORDER BY
									`" . TABLE_IMPORT_FACTORING_DATA . "`.`deb_id_vom_kreditor` ASC,
									`" . TABLE_IMPORT_FACTORING_DATA . "`.`beleg_nr` ASC,
									`" . TABLE_IMPORT_FACTORING_DATA . "`.`sortierung` ASC
							";
					}
					else if($exportFactoringFileType == 'GS'){
						$sql_exportForDATEV = "
								SELECT
									'" . DATEV_GEGENKONTO . "' AS `gegenkonto`,
									`" . TABLE_IMPORT_FACTORING_DATA . "`.`deb_id_vom_kreditor`,
									`" . TABLE_CUSTOMERS . "`.`customersFirmenname`,

									`" . TABLE_CUSTOMERS . "`.`customersTaxAccountID`,
									`" . TABLE_IMPORT_FACTORING_DATA . "`.`gutschrift_nr`,

									DATE_FORMAT(`" . TABLE_IMPORT_FACTORING_DATA . "`.`auszahlung_datum`, '%d.%m.%Y') AS `auszahlung_datum`,

									SUM(`" . TABLE_IMPORT_FACTORING_DATA . "`.`summe_hw_kre`) AS `summe_buchung`,

									CONCAT(
										'" . $arrBuchungstexte[$exportFactoringFileType] . "',
										' - AZ',
										`" . TABLE_IMPORT_FACTORING_DATA . "`.`az_nummer`
									) AS `buchungstext`
								FROM `" . TABLE_IMPORT_FACTORING_DATA . "`

								LEFT JOIN `" . TABLE_CUSTOMERS . "`
								ON(`" . TABLE_IMPORT_FACTORING_DATA . "`.`deb_id_vom_kreditor` = `" . TABLE_CUSTOMERS . "`.`customersKundennummer`)

								WHERE 1
									AND `" . TABLE_IMPORT_FACTORING_DATA . "`.`importFilename` = '" . $_REQUEST["exportFilename"] . "'

								GROUP BY `" . TABLE_IMPORT_FACTORING_DATA . "`.`gutschrift_nr`

								HAVING
									`summe_buchung` > 0 OR `summe_buchung` < 0

								ORDER BY
									`" . TABLE_IMPORT_FACTORING_DATA . "`.`deb_id_vom_kreditor` ASC,
									`" . TABLE_IMPORT_FACTORING_DATA . "`.`gutschrift_nr` ASC,
									`" . TABLE_IMPORT_FACTORING_DATA . "`.`sortierung` ASC
							";
					}
				// EOF CREATE EXPORT SQL DEPENDING ON TYPE

				// BOF READ AND WRITE EXPORT DATA
					if($sql_exportForDATEV != ""){
						$rs_exportForDATEV = $dbConnection->db_query($sql_exportForDATEV);

						if($rs_exportForDATEV){
							$countDataRows = $dbConnection->db_getMysqlNumRows($rs_exportForDATEV);
							if($countDataRows > 0){
								$fp_exportForDATEV = fopen($exportDownloadFilePath, 'a+');

								if($fp_exportForDATEV){
									$countDataRow = 0;
									while($ds_exportForDATEV = mysqli_fetch_assoc($rs_exportForDATEV)){
										if($countDataRow == 0){
											#$lineContent = strtoupper(implode($fieldSeparator, array_keys($ds_exportForDATEV)));
											$lineContent = '';
											$arrNewKeys = array();
											foreach(array_keys($ds_exportForDATEV) as $thisFieldKey){
												$thisNewKey = getFactoringFieldTitle($thisFieldKey, $exportFactoringFileType);
												$thisNewKey = preg_replace("/[ ]{1,}/", "_", $thisNewKey);
												#$thisNewKey = '"' . $thisNewKey . '"';
												$arrNewKeys[] = $thisNewKey;
											}
											$lineContent = strtoupper(implode($fieldSeparator, $arrNewKeys));
											fwrite($fp_exportForDATEV, $lineContent . "\n");
										}

										// BOF SHORT DOC-NUMBER
											$ds_exportForDATEV["rechnung_nr"] = cutDocumentNumber($ds_exportForDATEV["rechnung_nr"]);
											$ds_exportForDATEV["beleg_nr"] = cutDocumentNumber($ds_exportForDATEV["beleg_nr"]);
											$ds_exportForDATEV["gutschrift_nr"] = cutDocumentNumber($ds_exportForDATEV["gutschrift_nr"]);
										// EOF SHORT DOC-NUMBER

										// BOF CONVERT DOUBLE
											$ds_exportForDATEV["summe_buchung"] = number_format($ds_exportForDATEV["summe_buchung"], 2, ',', '');
										// EOF CONVERT DOUBLE

										// BOF CANGE CHARSET DS
											if(!empty($ds_exportForDATEV)){
												foreach($ds_exportForDATEV as $dsKey => $dsValue){
													$newValue = $dsValue;
													$newValue = convertCharsForDATEV($newValue);
													#$newValue = '"' . $newValue . '"';
													$ds_exportForDATEV[$dsKey] = $newValue;
												}
											}
										// EOF CANGE CHARSET DS

										$lineContent = implode($fieldSeparator, array_values($ds_exportForDATEV));
										fwrite($fp_exportForDATEV, $lineContent . "\n");
										$countDataRow++;
									}
									fclose($fp_exportForDATEV);
								}
								else {

								}
							}
							else {
								$infoMessage .= ' Es konnte keine Daten gefunden! ' . '</br>';
							}
						}
					}
					else {
						$errorMessage .= ' Es konnte keine Datenbankabfrage generiert werden! ' . '</br>';
					}
				// BOF READ AND WRITE EXPORT DATA
			// EOF WRITE EXPORT FILE

			// BOF DOWNLOAD EXPORT FILE	AND DELETE TMP FILE
				if(file_exists($exportDownloadFilePath)){
					/*
					require_once('classes/downloadFile.class.php');
					$downloadPath = PATH_EXPORT_FILES_TAX_ACCOUNTANT;
					$thisDownloadFile = $exportDownloadFilename;
					$thisDownload = new downloadFile($downloadPath, $thisDownloadFile);
					$errorMessage .= $thisDownload->startDownload();

					*/
					header('Content-Description: File Transfer');
					header('Content-Type: application/octet-stream');
					header('Content-Disposition: attachment; filename=' . basename($exportStoreFilename));
					header('Content-Transfer-Encoding: binary');
					header('Expires: 0');
					header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
					header('Pragma: public');
					header('Content-Length: ' . filesize($exportDownloadFilePath));
					header('Connection: Close');
					ob_clean();
					flush();
					readfile($exportDownloadFilePath);

					if(file_exists($exportDownloadFilePath)){
						unlink($exportDownloadFilePath);
					}
					exit;
				}
			// EOF DOWNLOAD EXPORT FILE	AND DELETE TMP FILE
		}
	// EOF EXPORT DATA FOR DATEV
	
	// BOF STORE CHANGED IMPORT DATA
		if(!empty($_POST["changeImportData"]) && $_POST["reSubmitSearch"] != ''){
			$arrUpdateFields = array();
			foreach($_POST["changeImportData"] as $thisChangeImportDataKey => $thisChangeImportDataValue){					
				$arrUpdateData = array();					
				
				if(trim($thisChangeImportDataValue["new_deb_id_vom_kreditor"]) != ""){
					#$arrUpdateData[] = " `deb_id_vom_kreditor` = '" . trim($thisChangeImportDataValue["new_deb_id_vom_kreditor"]) . "' ";
					$arrUpdateFields["deb_id_vom_kreditor"][] = " WHEN `" . TABLE_IMPORT_FACTORING_DATA . "`.`importID` = '" . $thisChangeImportDataKey . "' THEN '" . trim($thisChangeImportDataValue["new_deb_id_vom_kreditor"]) . "' ";
				}		
				if(trim($thisChangeImportDataValue["new_beleg_nr"]) != ""){
					#$arrUpdateData[] = " `beleg_nr` = '" . trim($thisChangeImportDataValue["new_beleg_nr"]) . "' ";
					$arrUpdateFields["beleg_nr"][] = " WHEN `" . TABLE_IMPORT_FACTORING_DATA . "`.`importID` = '" . $thisChangeImportDataKey . "' THEN '" . trim($thisChangeImportDataValue["new_beleg_nr"]) . "' ";
				}
				if($thisChangeImportDataValue["new_beleg_done"] != ""){
					#$arrUpdateData[] = " `beleg_done` = '1' ";
					$arrUpdateFields["beleg_done"][] = " WHEN `" . TABLE_IMPORT_FACTORING_DATA . "`.`importID` = '" . $thisChangeImportDataKey . "' THEN '1' ";
				}
				else if($thisChangeImportDataValue["new_beleg_done"] == ""){
					#$arrUpdateData[] = " `beleg_done` = '0' ";
					$arrUpdateFields["beleg_done"][] = " WHEN `" . TABLE_IMPORT_FACTORING_DATA . "`.`importID` = '" . $thisChangeImportDataKey . "' THEN '0' ";
				}				
				
				/*
				$rs_updateDataAll = true;
				if(!empty($arrUpdateData)){
					$sql_updateData = "
							UPDATE `" . TABLE_IMPORT_FACTORING_DATA . "`
								SET
									" . implode(", ", $arrUpdateData) . "
							WHERE 1
								AND `" . TABLE_IMPORT_FACTORING_DATA . "`.`importID` = '" . $thisChangeImportDataKey . "'
						";
					
					#$rs_updateData = $dbConnection->db_query($sql_updateData);
					if($rs_updateData){
						
					}
					else {						
						$rs_updateDataAll = false;
					}
				}
				*/
			}
			/*
			if($rs_updateDataAll){
				$successMessage .= 'Die Daten wurden gespeichert!' . '<br />';
			}
			else {
				$errorMessage .= 'Die Daten konnten nicht gespeichert werden!' . '<br />';					
			}
			*/
					
			if(!empty($arrUpdateFields)){
				$arrUpdateData = array();
				foreach($arrUpdateFields as $thisUpdateField => $thisUpdateValue){
					$arrUpdateData[] = "
						`" . $thisUpdateField . "` = (
							CASE
							" . implode("\n", $thisUpdateValue) .  "
							ELSE `" . $thisUpdateField . "` 
							END
						)
					";
				}
				
				if(!empty($arrUpdateData)){
					$sql_updateData = "
							UPDATE `" . TABLE_IMPORT_FACTORING_DATA . "`
								SET
								
								" . implode(",", $arrUpdateData). "
						";
					$rs_updateData = $dbConnection->db_query($sql_updateData);
					if($rs_updateData){
						$successMessage .= 'Die Daten wurden gespeichert!' . '<br />';
					}
					else {
						$errorMessage .= 'Die Daten konnten nicht gespeichert werden!' . '<br />';
					}
				}
			}				
			dd('sql_updateData');
		}
	// EOF STORE CHANGED IMPORT DATA

	// BOF GET SELECTED FACTORING IMPORTED FILE DATA
		if($_REQUEST["submitSearch"] != ''){
			if(!empty($_REQUEST["searchImportedFactoringFile"])){

				// BOF CHECK FILE NAME AND GET IMPORT FILE TYPE
					// auszahlung_anhang_re_158.txt || auszahlung_anhang_za_158.txt || auszahlung_anhang_gs_158.txt
					$importFactoringFileType = getFactoringFileType($_REQUEST["searchImportedFactoringFile"]);
				// EOF CHECK FILE NAME AND GET IMPORT FILE TYPE

				if($importFactoringFileType == 'RE'){
					$sql_getSelectedDatas = "
							SELECT
									`" . TABLE_IMPORT_FACTORING_DATA . "`.`importID`,
									`" . TABLE_IMPORT_FACTORING_DATA . "`.`importFilename`,
									`" . TABLE_IMPORT_FACTORING_DATA . "`.`importDate`,
									`" . TABLE_IMPORT_FACTORING_DATA . "`.`importFiletype`,

									`" . TABLE_IMPORT_FACTORING_DATA . "`.`deb_id_vom_kreditor`,
									`" . TABLE_IMPORT_FACTORING_DATA . "`.`trans_kz`,
									`" . TABLE_IMPORT_FACTORING_DATA . "`.`sortierung`,
									`" . TABLE_IMPORT_FACTORING_DATA . "`.`rechnung_nr`,
									`" . TABLE_IMPORT_FACTORING_DATA . "`.`rechnung_betrag`,
									`" . TABLE_IMPORT_FACTORING_DATA . "`.`sperrkonto_prz`,
									`" . TABLE_IMPORT_FACTORING_DATA . "`.`rechnung_datum`,
									`" . TABLE_IMPORT_FACTORING_DATA . "`.`buchungsbetrag_sk_index_5_1`,
									`" . TABLE_IMPORT_FACTORING_DATA . "`.`buchungsbetrag_sk_index_5_2`,
									`" . TABLE_IMPORT_FACTORING_DATA . "`.`summe_hw_kre`,
									`" . TABLE_IMPORT_FACTORING_DATA . "`.`az_nummer`,
									`" . TABLE_IMPORT_FACTORING_DATA . "`.`factoringart`,
									`" . TABLE_IMPORT_FACTORING_DATA . "`.`kreditoren_id`,
									`" . TABLE_IMPORT_FACTORING_DATA . "`.`kreditorenname_1`,
									`" . TABLE_IMPORT_FACTORING_DATA . "`.`simulation`,
									`" . TABLE_IMPORT_FACTORING_DATA . "`.`buchungsbetrag_sk_index_5_3`,
									`" . TABLE_IMPORT_FACTORING_DATA . "`.`rechnungs_id`,
									`" . TABLE_IMPORT_FACTORING_DATA . "`.`auszahlung_datum`,
									`" . TABLE_IMPORT_FACTORING_DATA . "`.`fir_re_nummer`,
									`" . TABLE_IMPORT_FACTORING_DATA . "`.`firmen_id`,
									`" . TABLE_IMPORT_FACTORING_DATA . "`.`buchungsbetrag_sk_index_5_4`,
									`" . TABLE_IMPORT_FACTORING_DATA . "`.`buchungsbetrag_sonst_hw_kre`,
									`" . TABLE_IMPORT_FACTORING_DATA . "`.`buchungsbetrag_sk_index_5_5`,
									`" . TABLE_IMPORT_FACTORING_DATA . "`.`anzeigeformat`,
									`" . TABLE_IMPORT_FACTORING_DATA . "`.`anzeigeformat2`,
									`" . TABLE_IMPORT_FACTORING_DATA . "`.`buchungsbetrag_sk_index_5_6`,
									
									`" . TABLE_IMPORT_FACTORING_DATA . "`.`beleg_done`,

									`" . TABLE_CUSTOMERS . "`.`customersID`,
									`" . TABLE_CUSTOMERS . "`.`customersKundennummer`,
									`" . TABLE_CUSTOMERS . "`.`customersTaxAccountID`,
									`" . TABLE_CUSTOMERS . "`.`customersFirmenname`

								FROM `" . TABLE_IMPORT_FACTORING_DATA . "`

								LEFT JOIN `" . TABLE_CUSTOMERS . "`
								ON(`" . TABLE_IMPORT_FACTORING_DATA . "`.`deb_id_vom_kreditor` = `" . TABLE_CUSTOMERS . "`.`customersKundennummer`)

								WHERE 1
									AND `" . TABLE_IMPORT_FACTORING_DATA . "`.`importFilename` = '" . $_REQUEST["searchImportedFactoringFile"] . "'

								ORDER BY
									`" . TABLE_IMPORT_FACTORING_DATA . "`.`deb_id_vom_kreditor` ASC,
									`" . TABLE_IMPORT_FACTORING_DATA . "`.`rechnung_nr` ASC,
									`" . TABLE_IMPORT_FACTORING_DATA . "`.`sortierung` ASC
						";
				}
				else if($importFactoringFileType == 'GS'){
					$sql_getSelectedDatas = "
							SELECT
									`" . TABLE_IMPORT_FACTORING_DATA . "`.`importID`,
									`" . TABLE_IMPORT_FACTORING_DATA . "`.`importFilename`,
									`" . TABLE_IMPORT_FACTORING_DATA . "`.`importDate`,
									`" . TABLE_IMPORT_FACTORING_DATA . "`.`importFiletype`,
									`" . TABLE_IMPORT_FACTORING_DATA . "`.`deb_id_vom_kreditor`,
									`" . TABLE_IMPORT_FACTORING_DATA . "`.`trans_kz`,
									`" . TABLE_IMPORT_FACTORING_DATA . "`.`sortierung`,

									`" . TABLE_IMPORT_FACTORING_DATA . "`.`rechnung_nr`,
									`" . TABLE_IMPORT_FACTORING_DATA . "`.`rechnung_betrag`,
									`" . TABLE_IMPORT_FACTORING_DATA . "`.`rechnung_datum`,
									`" . TABLE_IMPORT_FACTORING_DATA . "`.`buchungsbetrag_sk_index_6_1`,
									`" . TABLE_IMPORT_FACTORING_DATA . "`.`buchungsbetrag_sk_index_6_2`,
									`" . TABLE_IMPORT_FACTORING_DATA . "`.`az_nummer`,
									`" . TABLE_IMPORT_FACTORING_DATA . "`.`gutschrift_nr`,
									`" . TABLE_IMPORT_FACTORING_DATA . "`.`gutschrift_datum`,
									`" . TABLE_IMPORT_FACTORING_DATA . "`.`gutschrift_betrag`,
									`" . TABLE_IMPORT_FACTORING_DATA . "`.`summe_hw_kre`,
									`" . TABLE_IMPORT_FACTORING_DATA . "`.`kreditoren_id`,
									`" . TABLE_IMPORT_FACTORING_DATA . "`.`kreditorenname_1`,
									`" . TABLE_IMPORT_FACTORING_DATA . "`.`simulation`,
									`" . TABLE_IMPORT_FACTORING_DATA . "`.`buchungsbetrag_sk_index_6_3`,
									`" . TABLE_IMPORT_FACTORING_DATA . "`.`gutschrifts_id`,
									`" . TABLE_IMPORT_FACTORING_DATA . "`.`auszahlung_datum`,
									`" . TABLE_IMPORT_FACTORING_DATA . "`.`fir_re_nummer`,
									`" . TABLE_IMPORT_FACTORING_DATA . "`.`firmen_id`,
									`" . TABLE_IMPORT_FACTORING_DATA . "`.`buchungsbetrag_sk_index_6_4`,
									`" . TABLE_IMPORT_FACTORING_DATA . "`.`buchungsbetrag_sk_index_6_5`,
									`" . TABLE_IMPORT_FACTORING_DATA . "`.`buchungsbetrag_sonst_hw_kre`,
									`" . TABLE_IMPORT_FACTORING_DATA . "`.`belegart_id`,
									
									`" . TABLE_IMPORT_FACTORING_DATA . "`.`beleg_done`,

									`" . TABLE_CUSTOMERS . "`.`customersID`,
									`" . TABLE_CUSTOMERS . "`.`customersKundennummer`,
									`" . TABLE_CUSTOMERS . "`.`customersTaxAccountID`,
									`" . TABLE_CUSTOMERS . "`.`customersFirmenname`

								FROM `" . TABLE_IMPORT_FACTORING_DATA . "`

								LEFT JOIN `" . TABLE_CUSTOMERS . "`
								ON(`" . TABLE_IMPORT_FACTORING_DATA . "`.`deb_id_vom_kreditor` = `" . TABLE_CUSTOMERS . "`.`customersKundennummer`)

								WHERE 1
									AND `" . TABLE_IMPORT_FACTORING_DATA . "`.`importFilename` = '" . $_REQUEST["searchImportedFactoringFile"] . "'

								ORDER BY
									`" . TABLE_IMPORT_FACTORING_DATA . "`.`deb_id_vom_kreditor` ASC,
									`" . TABLE_IMPORT_FACTORING_DATA . "`.`gutschrift_nr` ASC,
									`" . TABLE_IMPORT_FACTORING_DATA . "`.`sortierung` DESC
						";
				}
				else if($importFactoringFileType == 'ZA'){
					$sql_getSelectedDatas = "
							SELECT
									`" . TABLE_IMPORT_FACTORING_DATA . "`.`importID`,
									`" . TABLE_IMPORT_FACTORING_DATA . "`.`importFilename`,
									`" . TABLE_IMPORT_FACTORING_DATA . "`.`importDate`,
									`" . TABLE_IMPORT_FACTORING_DATA . "`.`importFiletype`,
									`" . TABLE_IMPORT_FACTORING_DATA . "`.`deb_id_vom_kreditor`,
									`" . TABLE_IMPORT_FACTORING_DATA . "`.`trans_kz`,
									`" . TABLE_IMPORT_FACTORING_DATA . "`.`sortierung`,

									`" . TABLE_IMPORT_FACTORING_DATA . "`.`beleg_nr`,
									`" . TABLE_IMPORT_FACTORING_DATA . "`.`beleg_betrag`,
									`" . TABLE_IMPORT_FACTORING_DATA . "`.`sperrkonto_prz`,
									`" . TABLE_IMPORT_FACTORING_DATA . "`.`beleg_datum`,
									`" . TABLE_IMPORT_FACTORING_DATA . "`.`buchungsbetrag_sk_index_7_1`,
									`" . TABLE_IMPORT_FACTORING_DATA . "`.`buchungsbetrag_sk_index_7_2`,
									`" . TABLE_IMPORT_FACTORING_DATA . "`.`summe_hw_kre`,
									`" . TABLE_IMPORT_FACTORING_DATA . "`.`az_nummer`,
									`" . TABLE_IMPORT_FACTORING_DATA . "`.`factoringart`,
									`" . TABLE_IMPORT_FACTORING_DATA . "`.`zahlung_nr`,
									`" . TABLE_IMPORT_FACTORING_DATA . "`.`zahlung_datum`,
									`" . TABLE_IMPORT_FACTORING_DATA . "`.`zahlung_betrag`,
									`" . TABLE_IMPORT_FACTORING_DATA . "`.`kreditoren_id`,
									`" . TABLE_IMPORT_FACTORING_DATA . "`.`kreditorenname_1`,
									`" . TABLE_IMPORT_FACTORING_DATA . "`.`table_name`,
									`" . TABLE_IMPORT_FACTORING_DATA . "`.`simulation`,
									`" . TABLE_IMPORT_FACTORING_DATA . "`.`zahlungs_id`,
									`" . TABLE_IMPORT_FACTORING_DATA . "`.`auszahlung_datum`,
									`" . TABLE_IMPORT_FACTORING_DATA . "`.`fir_re_nummer`,
									`" . TABLE_IMPORT_FACTORING_DATA . "`.`firmen_id`,
									`" . TABLE_IMPORT_FACTORING_DATA . "`.`buchungsbetrag_sk_index_7_3`,
									`" . TABLE_IMPORT_FACTORING_DATA . "`.`buchungsbetrag_sk_index_7_4`,
									`" . TABLE_IMPORT_FACTORING_DATA . "`.`buchungsbetrag_sk_index_7_5`,
									`" . TABLE_IMPORT_FACTORING_DATA . "`.`buchungsbetrag_sonst_hw_kre`,
									
									`" . TABLE_IMPORT_FACTORING_DATA . "`.`beleg_done`,

									`" . TABLE_CUSTOMERS . "`.`customersID`,
									`" . TABLE_CUSTOMERS . "`.`customersKundennummer`,
									`" . TABLE_CUSTOMERS . "`.`customersTaxAccountID`,
									`" . TABLE_CUSTOMERS . "`.`customersFirmenname`

								FROM `" . TABLE_IMPORT_FACTORING_DATA . "`

								LEFT JOIN `" . TABLE_CUSTOMERS . "`
								ON(`" . TABLE_IMPORT_FACTORING_DATA . "`.`deb_id_vom_kreditor` = `" . TABLE_CUSTOMERS . "`.`customersKundennummer`)

								WHERE 1
									AND `" . TABLE_IMPORT_FACTORING_DATA . "`.`importFilename` = '" . $_REQUEST["searchImportedFactoringFile"] . "'

								ORDER BY
									`" . TABLE_IMPORT_FACTORING_DATA . "`.`deb_id_vom_kreditor` ASC,
									`" . TABLE_IMPORT_FACTORING_DATA . "`.`beleg_nr` ASC,
									`" . TABLE_IMPORT_FACTORING_DATA . "`.`sortierung` ASC
						";
				}
			}
			else {
				$warningMessage .= 'Sie haben keine Datei zum Lesen ausgew&auml;hlt!' . '<br />';
			}
		}
	// EOF GET SELECTED FACTORING IMPORTED FILE DATA
?>
<?php
	require_once('inc/headerHTML.inc.php');
	$thisTitle = "Faktoring-Daten importieren";

	$headerHTML = preg_replace("/{###TITLE###}/", strip_tags($thisTitle), $headerHTML);
	echo $headerHTML;

	require_once(FILE_MENUE_TOP);
?>

<div id="menueSidebarToggleArea">
	<img src="layout/icons/iconToggle.png" class="buttonToggleSidebarMenue" width="10" height="10" alt="" title="Seitennavigation ein-/ausblenden" />
	<div id="menueSidebarToggleContent">
	<?php require_once(FILE_MENUE_SIDEBAR); ?>
	<div class="clear"></div>
	</div>
</div>
<div id="contentArea2">
	<a name="top"></a>
	<div id="contentAreaElements">
		<h1><?php if(SHOW_PAGE_TITLE_ICONS) { echo '<img src="' . PATH_ICONS_MENUE_TITLES . 'export.png" alt="" />'; } ?> <?php echo $thisTitle; ?></h1>
		<p><b>Kreditoren-ID: <?php echo KREDITOREN_ID; ?> [<?php echo strtoupper(MANDATOR); ?>]</b></p>
		<div style="text-align:right"><a href="<?php echo URL_FACTORING_PORTAL; ?>" target="_blank">Zum Factoring Portal</a></div>

		<p class="infoArea" style="font-weight:normal">
			<span style="padding-right:10px;"><b>Letzte importiere AZ-Daten: </b></span>
			<span style="padding-right:10px;"><b>AZ-Nr: </b><?php echo $arrLastImportData["MAX_AZ_NUMBER"]; ?></span>
			<span style="padding-right:10px;"><b>Auszahlung: </b><?php echo formatDate(substr($arrLastImportData["MAX_PAYMENT_DATE"], 0, 10), 'display'); ?></span>
			<span style="padding-right:10px;"><b>Dateien: </b><?php echo preg_replace("/;/", " &bull; ", $arrLastImportData["MAX_FILE_NAME"]); ?></span>
		</p>

		<div class="seachFormsArea">
			<h2>Datei-Upload</h2>
			<div class="searchFilterArea">
				<form name="formUploadImportFile" method="post" action="<?php echo $_SERVER["PHP_SELF"]; ?>" enctype="multipart/form-data" >
					<table border="0" cellpadding="0" cellspacing="0" class="searchFilterContent">
						<tr>
							<td style="width:100px;"><label for="importFactoringFile">Datei hochladen: </label></td>
							<td>
								<input type="file" name="importFactoringFile" id="importFactoringFile" class="inputFile_510" />
								<br /><span class="infotext">G&uuml;ltige Formate: auszahlung_anhang_re_158.txt || auszahlung_anhang_za_158.txt || auszahlung_anhang_gs_158.txt</span>
							</td>
							<td><input type="submit" name="submitFileUpload" class="inputButton0" value="Datei importieren" /></td>
						</tr>
					</table>
				</form>
			</div>

			<hr />

			<?php if($arrGetUserRights["displayFactoringData"] == '1') { ?>
			<h2>RE / GU-Nummer suchen</h2>
			<div class="searchFilterArea">
				<form name="formSearchImportedDocumentNumber" method="post" action="<?php echo $_SERVER["PHP_SELF"]; ?>">
				<table border="0" cellpadding="0" cellspacing="0" class="searchFilterContent">
					<tr>
						<td style="width:100px;"><label for="searchImportedDocumentNumber">RE / GU-Nummer: </label></td>
						<td><input type="text" name="searchImportedDocumentNumber" id="searchImportedDocumentNumber" class="inputField_120" /></td>
						<td> | </td>
						<td style="width:100px;"><label for="searchImportedDocumentSum">Summe: </label></td>
						<td><input type="text" name="searchImportedDocumentSum" id="searchImportedDocumentSum" class="inputField_120" /></td>
						<td>
							<input type="submit" name="submitSearchImportedDocumentNumber" class="inputButton0" value="RE / GU suchen" />
						</td>
					</tr>
				</table>
				</form>
			</div>

			<?php
				if($_POST["submitSearchImportedDocumentNumber"] != ''){
					if($_POST["searchImportedDocumentNumber"] != '' || $_POST["searchImportedDocumentSum"] != ''){
						$searchDocumentNumberType = '';
						if(preg_match("/^(GU|RE)/", $_POST["searchImportedDocumentNumber"])){
							$searchDocumentNumberType = substr($_POST["searchImportedDocumentNumber"], 0, 2);
						}

						// BOF SEARCH DOCUMENT NUMBERS
							if(trim($_POST["searchImportedDocumentSum"] == '')){
								// BOF SEARCH RE
									if($searchDocumentNumberType == 'RE'){
										$sql_searchDocumentNumber = "
												SELECT

													`importFilename`,
													`importFiletype`,
													`importDate`

													FROM `" . TABLE_IMPORT_FACTORING_DATA . "`

													WHERE 1
														AND `rechnung_nr` = '" . $_POST["searchImportedDocumentNumber"] . "'
											";
									}
								// EOF SEARCH RE

								// BOF SEARCH GU
									if($searchDocumentNumberType == 'GU'){
										$sql_searchDocumentNumber = "
												SELECT

													`importFilename`,
													`importFiletype`,
													`importDate`

													FROM `" . TABLE_IMPORT_FACTORING_DATA . "`

													WHERE 1
														AND `gutschrift_nr` = '" . $_POST["searchImportedDocumentNumber"] . "'
											";
									}
								// EOF SEARCH GU

								// BOF SEARCH RE AND GU
									if($searchDocumentNumberType == ''){
										$sql_searchDocumentNumber = "
												SELECT

													`importFilename`,
													`importFiletype`,
													`importDate`

													FROM `" . TABLE_IMPORT_FACTORING_DATA . "`

													WHERE 1
														AND (
															`gutschrift_nr` LIKE '%" . $_POST["searchImportedDocumentNumber"] . "'
															OR `rechnung_nr` LIKE '%" . $_POST["searchImportedDocumentNumber"] . "'
														)
											";
									}
								// EOF SEARCH RE AND GU
							}
							else {
								$sql_searchDocumentNumber = "
												SELECT

													`importFilename`,
													`importFiletype`,
													`importDate`

													FROM `" . TABLE_IMPORT_FACTORING_DATA . "`

													WHERE 1
														AND (
															`rechnung_betrag` = '" . convertDecimal($_POST["searchImportedDocumentSum"], 'store') . "'
															OR `gutschrift_betrag` = '" . convertDecimal($_POST["searchImportedDocumentSum"], 'store') . "'
															OR `buchungsbetrag_sk_index_7_1` = '" . convertDecimal($_POST["searchImportedDocumentSum"], 'store') . "'
														)
													ORDER BY
														LENGTH(`importFilename`) DESC,
														`importFilename` DESC
											";
								
							}
						// EOF SEARCH DOCUMENT NUMBERS
						
						if($sql_searchDocumentNumber != ""){
							$rs_searchDocumentNumber = $dbConnection->db_query($sql_searchDocumentNumber);
							$countRows = $dbConnection->db_getMysqlNumRows($rs_searchDocumentNumber);
							if($countRows > 0){
								echo '<table cellspacing="0" cellpadding="0" width="100%" border="0" class="displayOrders">';
								echo '<thead>';

								echo '<tr>';

								echo '<th>#</th>';
								echo '<th>Datei</th>';
								echo '<th>Import-Datum</th>';
								echo '<th></th>';
								echo '</tr>';

								echo '</thead>';

								echo '<tbody>';
								$countRow = 0;

								while($ds_searchDocumentNumber = mysqli_fetch_assoc($rs_searchDocumentNumber)){
									$rowClass = 'row0';
									if($countRow%2 == 0) {
										$rowClass = 'row1';
									}

									echo '<tr class="' . $rowClass . '" ' . $rowStyle . '>';

									echo '<td style="text-align:right;">';
									echo '<b>' . ($countRow + 1) . '.</b>';
									echo '</td>';

									echo '<td>';
									echo $ds_searchDocumentNumber["importFilename"];
									echo '</td>';

									echo '<td>';
									echo formatDate($ds_searchDocumentNumber["importDate"], "display");
									echo '</td>';

									echo '<td>';
									echo $ds_searchDocumentNumber["importFiletype"];
									echo '</td>';

									echo '</tr>';

									$countRow++;
								}
								echo '</tbody>';
								echo '</table>';
							}
							else{
								$warningMessage = 'Es wurde keine Daten gefunden! ' . '<br />';
							}
						}
					}
					else{
						$warningMessage = 'Es wurde keine Dokument-Nummer angegeben! ' . '<br />';
					}
					echo '<hr />';
				}
			?>
			<?php } ?>

			<h2>Bisher hochgeladene Daten anzeigen</h2>
			<div id="searchFilterArea">
				<form name="formSelectImportedDatas" method="post" action="<?php echo $_SERVER["PHP_SELF"]; ?>">
				<table border="0" cellpadding="0" cellspacing="0" class="searchFilterContent">
					<tr>
						<td style="width:100px;"><b>Dateien: </b></td>
						<td>
							<label for="searchImportedFactoringFile">Bisher hochgeladene Datei ausw&auml;hlen:</label>
							<?php
								$thisStyle = '';
								/*
								if($importFactoringFileType == "RE"){
									$thisStyle .= 'border-right:8px solid #00FF00;';
								}
								else if($importFactoringFileType == "GS"){
									$thisStyle .= 'border-right:8px solid #0000FF;';
								}
								else if($importFactoringFileType == "ZA"){
									$thisStyle .= 'border-right:8px solid #FF0000;';
								}
								*/
							?>
							<select name="searchImportedFactoringFile" id="searchImportedFactoringFile" class="inputSelect_510" style="<?php echo $thisStyle; ?>" >
								<option value=""> -- Bitte w&auml;hlen</option>
								<?php
									if(!empty($arrImportedFilenames)){
										$marker = '';
										foreach($arrImportedFilenames as $thisImportedFilenamesKey => $thisImportedFilenamesValue){
											$selected = '';
											if($_REQUEST["searchImportedFactoringFile"] == $thisImportedFilenamesValue["file"]){
												$selected = ' selected="selected" ';
											}

											$thisStyle = '';
											$thisStyle .= 'border-right: 2px solid transparent;';
											if($thisImportedFilenamesValue["typ"] == "RE"){
												$thisStyle .= 'border-left:8px solid #00FF00;';
											}
											else if($thisImportedFilenamesValue["typ"] == "GS"){
												$thisStyle .= 'border-left:8px solid #0000FF;';
											}
											else if($thisImportedFilenamesValue["typ"] == "ZA"){
												$thisStyle .= 'border-left:8px solid #FF0000;';
											}

											if($marker != $thisImportedFilenamesValue["az"]){
												$marker = $thisImportedFilenamesValue["az"];
												$thisStyle .= 'border-top:2px solid #000000;';
											}

											$thisClass = '';
											$thisClass .= $thisImportedFilenamesValue["typ"]. ' date_' . substr($thisImportedFilenamesValue["date"], 0, 7);


											// BOF FILES CONTAINING DATA WITHOUT DOC-NR
												if(in_array($thisImportedFilenamesValue["file"], $arrImportedFilesWithoutDocumentNumber)){
													$thisStyle .= 'background-image:url(layout/icons/iconAttention.png);background-repeat:no-repeat;background-position:right 0;';
													$thisClass .= ' NoDocNr';
												}
											// EOF FILES CONTAINING DATA WITHOUT DOC-NR

											#echo '<option class="' . $thisImportedFilenamesValue["typ"]. '" style="' . $thisStyle . '" value="' . $thisImportedFilenamesValue["file"] . '" ' . $selected . '>AZ ' . $thisImportedFilenamesValue["az"] . ' [' . $thisImportedFilenamesValue["typ"]. ']' . ' | ' . substr(formatDate($thisImportedFilenamesValue["date"], 'display'), 0, 10) . ' - ' . $thisImportedFilenamesValue["file"] . '</option>';
											echo '<option class="' . $thisClass . '" style="' . $thisStyle . '" value="' . $thisImportedFilenamesValue["file"] . '" ' . $selected . '>AZ ' . $thisImportedFilenamesValue["az"] . ' [' . $thisImportedFilenamesValue["typ"]. ']' . ' | ' . substr(formatDate($thisImportedFilenamesValue["date"], 'display'), 0, 10) . ' - ' . $thisImportedFilenamesValue["file"] . '</option>';
										}
									}
								?>
							</select>
						</td>

						<td>
							<label for="filterFactoringFileTypes">Dateien filtern</label>
							<?php
								$checked = '';
								if(!isset($_REQUEST["filterFactoringFileTypes"]) || $_REQUEST["filterFactoringFileTypes"] == ''){
									$checked = ' checked="checked" ';
								}
								echo '<span><input type="radio" name="filterFactoringFileTypes" id="filterFactoringFileTypes" value="" ' . $checked . ' />alle</span>';
							?>
							<?php
								if(!empty($arrFactoringFileTypes)){
									foreach($arrFactoringFileTypes as $thisFactoringFileTypeKey => $thisFactoringFileTypeValue){
										$thisStyle = '';
										if($thisFactoringFileTypeKey == "RE"){
											$thisStyle .= 'border-bottom:4px solid #00FF00;';
										}
										else if($thisFactoringFileTypeKey == "GS"){
											$thisStyle .= 'border-bottom:4px solid #0000FF;';
										}
										else if($thisFactoringFileTypeKey == "ZA"){
											$thisStyle .= 'border-bottom:4px solid #FF0000;';
										}

										$checked = '';
										if($thisFactoringFileTypeKey == $_REQUEST["filterFactoringFileTypes"]){
											$checked = ' checked="checked" ';
										}
										echo '<span style="margin-left:10px;' . $thisStyle . '" title="Nur ' . $thisFactoringFileTypeKey . '-Dokumente anzeigen"><input type="radio" name="filterFactoringFileTypes" id="filterFactoringFileTypes_' . $thisFactoringFileTypeKey . '" value="' . $thisFactoringFileTypeKey . '" ' . $checked . ' />nur ' . $thisFactoringFileTypeKey . '</span>';
									}
								}
							?>

							<span style="margin-left:10px;">
								<select name="filterFactoringFileDate" id="filterFactoringFileDate" class="inputField_100">
									<option value=""></option>
									<?php
										if(!empty($arrImportedFileDates)){
											foreach($arrImportedFileDates as $thisImportedFileDate){
												$selected = '';
												if($thisImportedFileDate == $_REQUEST["filterFactoringFileDate"]){
													$selected = ' selected="selected" ';
												}
												$strDate = $thisImportedFileDate;
												$arrDate = explode("-", $strDate);
												$thisDate = getTimeNames($arrDate[1], 'month', '') . ' ' . $arrDate[0];
												echo '<option value="date_' . $thisImportedFileDate . '" ' . $selected . '>' . $thisImportedFileDate . ' [' . $thisDate . ']</option>';
											}
										}
									?>
								</select>
							</span>

							<span style="margin-left:10px;" title="Dateien anzeigen, die Daten ohne Beleg-Nummer enthalten">
							<?php
								$checked = '';
								if($_REQUEST["filterFactoringFileNoDocNr"] == 'NoDocNr'){
									$checked = ' checked = "checked" ';
								}
								echo '<input type="checkbox" name="filterFactoringFileNoDocNr" id="filterFactoringFileNoDocNr" value="NoDocNr" ' . $checked . ' />';
								echo '<img src="layout/icons/iconAttention.png" width="16" height="16" alt="" /> ohne Beleg';
							?>
							</span>
						</td>

						<td>
							<input type="submit" name="submitSearch" class="inputButton0" value="Datei anzeigen" />
						</td>
					</tr>
				</table>
				</form>
			</div>
		</div>

		<?php
			if($_REQUEST["submitSearch"] != ''){
				if(!empty($_REQUEST["searchImportedFactoringFile"])){
					echo '<hr />';
					echo '<h2>Daten der Datei &quot;' . $_REQUEST["searchImportedFactoringFile"] . '&quot; [' . $importFactoringFileType . ']' . '</h2>';

					$rs_getSelectedDatas = $dbConnection->db_query($sql_getSelectedDatas);
					$countRows = $dbConnection->db_getMysqlNumRows($rs_getSelectedDatas);

					if($countRows > 0){

						// BOF EXPORT DATA FOR DATEV
							if(1 || $_COOKIE["isAdmin"]){
								echo '<div class="actionButtonsArea">';
									echo '<div style="margin: 0 0 10px 0;padding:0 0 4px 0;border-bottom:1px solid #333;text-align:right">';
										echo '<a href="?exportDataForDatev=1&amp;exportFilename=' . $_REQUEST["searchImportedFactoringFile"] . '" class="linkButton">Daten f&uuml;r DATEV exportieren</a>';
										echo '<div class="clear"></div>';
									echo '</div>';
								echo '</div>';
							}
						// EOF EXPORT DATA FOR DATEV

						echo '<span class="buttonTransactionsLegend"></span>';

						displayMessages();
						
						if($importFactoringFileType == 'RE'){
							echo '<table cellspacing="0" cellpadding="0" width="100%" border="0" class="displayOrders">';

							echo '<colgroup>';
							for($i = 0 ; $i < 24 ; $i++){
								echo '<col />';
							}
							echo '</colgroup>';

							echo '<thead>';

							echo '<tr>';

							echo '<th>#</th>';
							echo '<th>' . getFactoringFieldTitle("az_nummer", "RE") . '</th>';
							#echo '<th>' . getFactoringFieldTitle("importFilename", "RE") . '</th>';
							#echo '<th>' . getFactoringFieldTitle("importDate", "RE") . '</th>';
							echo '<th>' . getFactoringFieldTitle("deb_id_vom_kreditor", "RE") . '</th>';
							echo '<th>' . getFactoringFieldTitle("FiBu-Nr", "RE") . '</th>';
							echo '<th>' . getFactoringFieldTitle("Firma", "RE") . '</th>';

							echo '<th class="spacer"></th>';

							#echo '<th>' . getFactoringFieldTitle("sortierung", "RE") . '</th>';

							echo '<th>' . getFactoringFieldTitle("rechnung_nr", "RE") . '</th>';
							echo '<th>' . getFactoringFieldTitle("rechnung_datum", "RE") . '</th>';
							echo '<th>' . getFactoringFieldTitle("rechnung_betrag", "RE") . '</th>';

							echo '<th class="spacer"></th>';

							echo '<th>' . getFactoringFieldTitle("sperrkonto_prz", "RE") . '</th>';
							echo '<th>' . getFactoringFieldTitle("trans_kz", "RE") . '</th>';

							echo '<th class="spacer"></th>';

							echo '<th>' . getFactoringFieldTitle("buchungsbetrag_sk_index_5_1", "RE") . '</th>';
							echo '<th>' . getFactoringFieldTitle("buchungsbetrag_sk_index_5_2", "RE") . '</th>';


							#echo '<th>' . getFactoringFieldTitle("factoringart", "RE") . '</th>';
							#echo '<th>' . getFactoringFieldTitle("kreditoren_id", "RE") . '</th>';
							#echo '<th>' . getFactoringFieldTitle("kreditorenname_1", "RE") . '</th>';
							#echo '<th>' . getFactoringFieldTitle("simulation", "RE") . '</th>';
							echo '<th>' . getFactoringFieldTitle("buchungsbetrag_sk_index_5_3", "RE") . '</th>';
							#echo '<th>' . getFactoringFieldTitle("rechnungs_id", "RE") . '</th>';
							#echo '<th>' . getFactoringFieldTitle("auszahlung_datum", "RE") . '</th>';
							#echo '<th>' . getFactoringFieldTitle("fir_re_nummer", "RE") . '</th>';
							#echo '<th>' . getFactoringFieldTitle("firmen_id", "RE") . '</th>';
							echo '<th>' . getFactoringFieldTitle("buchungsbetrag_sk_index_5_4", "RE") . '</th>';
							echo '<th>' . getFactoringFieldTitle("buchungsbetrag_sk_index_5_5", "RE") . '</th>';
							echo '<th>' . getFactoringFieldTitle("buchungsbetrag_sonst_hw_kre", "RE") . '</th>';
							echo '<th>' . getFactoringFieldTitle("buchungsbetrag_sk_index_5_6", "RE") . '</th>';

							echo '<th class="spacer"></th>';

							echo '<th>' . getFactoringFieldTitle("summe_hw_kre", "RE") . '</th>';

							echo '<th class="spacer"></th>';

							echo '<th>&sum; Buchung</th>';

							echo '</tr>';

							echo '</thead>';

							echo '<tbody>';

							$countRow = 0;

							$marker = '';

							$arrTotalSums = array();
							$arrTotalSums["rechnung_betrag"]				= 0;
							$arrTotalSums["buchungsbetrag_sk_index_5_1"]	= 0;
							$arrTotalSums["buchungsbetrag_sk_index_5_2"]	= 0;
							$arrTotalSums["buchungsbetrag_sk_index_5_3"]	= 0;
							$arrTotalSums["buchungsbetrag_sk_index_5_4"]	= 0;
							$arrTotalSums["buchungsbetrag_sk_index_5_5"]	= 0;
							$arrTotalSums["buchungsbetrag_sonst_hw_kre"]	= 0;
							$arrTotalSums["buchungsbetrag_sk_index_5_6"]	= 0;
							$arrTotalSums["summe_hw_kre"]					= 0;
							$arrTotalSums["summe_buchung"]					= 0;

							while($ds_getSelectedDatas = mysqli_fetch_assoc($rs_getSelectedDatas)){
								$ds_getSelectedDatas["summe_buchung"] = ($ds_getSelectedDatas["summe_hw_kre"] - ($ds_getSelectedDatas["buchungsbetrag_sk_index_5_6"] + $ds_getSelectedDatas["buchungsbetrag_sk_index_5_5"]));

								$rowStyle = '';
								if($marker != $ds_getSelectedDatas["rechnung_nr"]){
									$marker = $ds_getSelectedDatas["rechnung_nr"];
									$rowStyle = 'style="border-top:2px solid #000;"';
								}

								$rowClass = 'row0';
								if($countRow%2 == 0) {
									$rowClass = 'row1';
								}

								$arrTotalSums["rechnung_betrag"]				+= $ds_getSelectedDatas["rechnung_betrag"];
								$arrTotalSums["buchungsbetrag_sk_index_5_1"]	+= $ds_getSelectedDatas["buchungsbetrag_sk_index_5_1"];
								$arrTotalSums["buchungsbetrag_sk_index_5_2"]	+= $ds_getSelectedDatas["buchungsbetrag_sk_index_5_2"];
								$arrTotalSums["buchungsbetrag_sk_index_5_3"]	+= $ds_getSelectedDatas["buchungsbetrag_sk_index_5_3"];
								$arrTotalSums["buchungsbetrag_sk_index_5_4"]	+= $ds_getSelectedDatas["buchungsbetrag_sk_index_5_4"];
								$arrTotalSums["buchungsbetrag_sk_index_5_5"]	+= $ds_getSelectedDatas["buchungsbetrag_sk_index_5_5"];
								$arrTotalSums["buchungsbetrag_sk_index_5_6"]	+= $ds_getSelectedDatas["buchungsbetrag_sk_index_5_6"];
								$arrTotalSums["buchungsbetrag_sonst_hw_kre"]	+= $ds_getSelectedDatas["buchungsbetrag_sonst_hw_kre"];
								$arrTotalSums["summe_hw_kre"]					+= $ds_getSelectedDatas["summe_hw_kre"];
								$arrTotalSums["summe_buchung"]					+= $ds_getSelectedDatas["summe_buchung"];

								echo '<tr class="' . $rowClass . '" ' . $rowStyle . '>';

								echo '<td style="text-align:right;">';
								echo '<b>' . ($countRow + 1) . '.</b>';
								echo '</td>';

								echo '<td style="white-space:nowrap;">';
								echo '<span title="' . $ds_getSelectedDatas["importFilename"] . '" style="cursor:pointer;">';
								echo '<b>' . $ds_getSelectedDatas["az_nummer"] . '</b>';
								echo ' - ' . $ds_getSelectedDatas["importFiletype"] . '';
								echo '</span>';
								echo '</td>';

								#echo '<td>';
								#echo $ds_getSelectedDatas["importFilename"];
								#echo '</td>';

								#echo '<td>';
								#echo formatDate($ds_getSelectedDatas["importDate"], 'display');
								#echo '</td>';

								echo '<td style="font-weight:bold;">';
								echo '<a href="' . PAGE_EDIT_CUSTOMER . '?searchCustomerNumber=' . $ds_getSelectedDatas["deb_id_vom_kreditor"] . '" target="_blank">' . $ds_getSelectedDatas["deb_id_vom_kreditor"] . '</a>';
								echo '</td>';

								echo '<td style="font-style:italic;background-color:#FEFFAF;">';
								echo $ds_getSelectedDatas["customersTaxAccountID"];
								echo '</td>';

								echo '<td style="font-weight:bold;">';
								echo htmlentities($ds_getSelectedDatas["customersFirmenname"]);
								echo '</td>';

								echo '<td class="spacer"></td>';

								#echo '<td style="text-align:right;">';
								#echo $ds_getSelectedDatas["sortierung"];
								#echo '</td>';

								echo '<td style="font-weight:bold;">';
								#echo '<a href="?downloadFile=' . basename($ds["orderDocumentsDocumentPath"]) . '&amp;thisDocumentType=' . $_REQUEST["documentType"] . '"><img src="layout/icons/iconPDF.gif" width="16" height="16" alt="' . $arrDocumentTypeDatas[$_REQUEST["documentType"]]["createdDocumentsTypesName"]. ' Dokument herunterladen" title="Dokument herunterladen" /></a>';
								$thisDocumentNumber = $ds_getSelectedDatas["rechnung_nr"];
								$thisDocumentsPage = PAGE_ALL_INVOICES;
								if(substr($thisDocumentNumber, 0, 2) == 'GU'){
									$thisDocumentsPage = PAGE_ALL_CREDITS;
								}
								echo '<a href="' . $thisDocumentsPage . '?searchDocumentNumber=' . $thisDocumentNumber . '" target="_blank">';
								echo $thisDocumentNumber;
								echo '</a>';
								echo '</td>';

								echo '<td>';
								echo substr(formatDate($ds_getSelectedDatas["rechnung_datum"], 'display'), 0, 10);
								echo '</td>';

								echo '<td style="text-align:right;white-space:nowrap;background-color:#FEFFAF;">';
								echo formatDecimal($ds_getSelectedDatas["rechnung_betrag"]) . ' &euro;';
								echo '</td>';

								echo '<td class="spacer"></td>';

								echo '<td style="text-align:right;white-space:nowrap;">';
								echo formatDecimal($ds_getSelectedDatas["sperrkonto_prz"]) . ' %';
								echo '</td>';

								echo '<td>';
								echo '<span style="cursor:pointer;" class="trans_kz" title="' . $arrTransactionsTypes[$ds_getSelectedDatas["trans_kz"]] . '">' . $ds_getSelectedDatas["trans_kz"] . '</span>';
								echo '</td>';

								echo '<td class="spacer"></td>';

								echo '<td style="text-align:right;white-space:nowrap;">';
								echo formatDecimal($ds_getSelectedDatas["buchungsbetrag_sk_index_5_1"]) . ' &euro;';
								echo '</td>';

								echo '<td style="text-align:right;white-space:nowrap;">';
								echo formatDecimal($ds_getSelectedDatas["buchungsbetrag_sk_index_5_2"]) . ' &euro;';
								echo '</td>';

								#echo '<td>';
								#echo $ds_getSelectedDatas["factoringart"];
								#echo '</td>';

								#echo '<td>';
								#echo $ds_getSelectedDatas["kreditoren_id"];
								#echo '</td>';

								#echo '<td>';
								#echo $ds_getSelectedDatas["kreditorenname_1"];
								#echo '</td>';

								#echo '<td style="text-align:right;">';
								#echo $ds_getSelectedDatas["simulation"];
								#echo '</td>';

								echo '<td style="text-align:right;white-space:nowrap;">';
								echo formatDecimal($ds_getSelectedDatas["buchungsbetrag_sk_index_5_3"]) . ' &euro;';
								echo '</td>';

								#echo '<td>';
								#echo $ds_getSelectedDatas["rechnungs_id"];
								#echo '</td>';

								#echo '<td>';
								#echo substr(formatDate($ds_getSelectedDatas["auszahlung_datum"], 'display'), 0, 10);
								#echo '</td>';

								#echo '<td>';
								#echo $ds_getSelectedDatas["fir_re_nummer"];
								#echo '</td>';

								#echo '<td>';
								#echo $ds_getSelectedDatas["firmen_id"];
								#echo '</td>';

								echo '<td style="text-align:right;white-space:nowrap;">';
								echo formatDecimal($ds_getSelectedDatas["buchungsbetrag_sk_index_5_4"]) . ' &euro;';
								echo '</td>';

								echo '<td style="text-align:right;white-space:nowrap;">';
								echo formatDecimal($ds_getSelectedDatas["buchungsbetrag_sk_index_5_5"]) . ' &euro;';
								echo '</td>';

								echo '<td style="text-align:right;white-space:nowrap;">';
								echo formatDecimal($ds_getSelectedDatas["buchungsbetrag_sonst_hw_kre"]) . ' &euro;';
								echo '</td>';

								echo '<td style="text-align:right;white-space:nowrap;">';
								echo formatDecimal($ds_getSelectedDatas["buchungsbetrag_sk_index_5_6"]) . ' &euro;';
								echo '</td>';

								echo '<td class="spacer"></td>';

								echo '<td style="text-align:right;white-space:nowrap;background-color:#FEFFAF;">';
								echo formatDecimal($ds_getSelectedDatas["summe_hw_kre"]) . ' &euro;';
								echo '</td>';

								echo '<td class="spacer"></td>';

								echo '<td style="text-align:right;white-space:nowrap;background-color:#FEFFAF;">';
								if($ds_getSelectedDatas["summe_buchung"] != 0){
									echo '<b>' . formatDecimal($ds_getSelectedDatas["summe_buchung"]) . ' &euro;' . '</b>';
								}

								echo '</td>';

								echo '</tr>';

								$countRow++;
							}

							echo '<tr>';
							echo '<td colspan="24"><hr /></td>';
							echo '</tr>';

							echo '<tr class="row2" style="font-weight:bold;">';
							echo '<td colspan="12">Insg.:</td>';

							echo '<td class="spacer"></td>';

							echo '<td style="text-align:right;white-space:nowrap;">';
							echo formatDecimal($arrTotalSums["buchungsbetrag_sk_index_5_1"]) . ' &euro;';
							echo '</td>';

							echo '<td style="text-align:right;white-space:nowrap;">';
							echo formatDecimal($arrTotalSums["buchungsbetrag_sk_index_5_2"]) . ' &euro;';
							echo '</td>';

							echo '<td style="text-align:right;white-space:nowrap;">';
							echo formatDecimal($arrTotalSums["buchungsbetrag_sk_index_5_3"]) . ' &euro;';
							echo '</td>';

							echo '<td style="text-align:right;white-space:nowrap;">';
							echo formatDecimal($arrTotalSums["buchungsbetrag_sk_index_5_4"]) . ' &euro;';
							echo '</td>';

							echo '<td style="text-align:right;white-space:nowrap;">';
							echo formatDecimal($arrTotalSums["buchungsbetrag_sk_index_5_5"]) . ' &euro;';
							echo '</td>';

							echo '<td style="text-align:right;white-space:nowrap;">';
							echo formatDecimal($arrTotalSums["buchungsbetrag_sonst_hw_kre"]) . ' &euro;';
							echo '</td>';

							echo '<td style="text-align:right;white-space:nowrap;">';
							echo formatDecimal($arrTotalSums["buchungsbetrag_sk_index_5_6"]) . ' &euro;';
							echo '</td>';

							echo '<td class="spacer"></td>';

							echo '<td style="text-align:right;white-space:nowrap;">';
							echo formatDecimal($arrTotalSums["summe_hw_kre"]) . ' &euro;';
							echo '</td>';

							echo '<td class="spacer"></td>';

							echo '<td style="text-align:right;white-space:nowrap;">';
							echo formatDecimal($arrTotalSums["summe_buchung"]) . ' &euro;';
							echo '</td>';

							echo '</tr>';

							echo '</tbody>';

							echo '</table>';
						}
						else if($importFactoringFileType == 'GS'){
							echo '<table cellspacing="0" cellpadding="0" width="100%" border="0" class="displayOrders">';

							echo '<colgroup>';
							for($i = 0 ; $i < 23 ; $i++){
								echo '<col />';
							}
							echo '</colgroup>';

							echo '<thead>';

							echo '<tr>';

							echo '<th>#</th>';
							echo '<th>' . getFactoringFieldTitle("az_nummer", "GS") . '</th>';
							#echo '<th>' . getFactoringFieldTitle("importFilename", "GS") . '</th>';
							#echo '<th>' . getFactoringFieldTitle("importDate", "GS") . '</th>';
							echo '<th>' . getFactoringFieldTitle("deb_id_vom_kreditor", "GS") . '</th>';
							echo '<th>' . getFactoringFieldTitle("FiBu-Nr", "GS") . '</th>';
							echo '<th>' . getFactoringFieldTitle("Firma", "GS") . '</th>';

							echo '<th class="spacer"></th>';

							echo '<th>' . getFactoringFieldTitle("trans_kz", "GS") . '</th>';

							echo '<th>' . getFactoringFieldTitle("gutschrift_nr", "GS") . '</th>';
							echo '<th>' . getFactoringFieldTitle("gutschrift_datum", "GS") . '</th>';
							echo '<th>' . getFactoringFieldTitle("gutschrift_betrag", "GS") . '</th>';

							echo '<th class="spacer"></th>';

							echo '<th>' . getFactoringFieldTitle("rechnung_nr", "GS") . '</th>';
							echo '<th>' . getFactoringFieldTitle("rechnung_datum", "GS") . '</th>';
							echo '<th>' . getFactoringFieldTitle("rechnung_betrag", "GS") . '</th>';

							echo '<th class="spacer"></th>';

							echo '<th>' . getFactoringFieldTitle("buchungsbetrag_sk_index_6_1", "GS") . '</th>';
							echo '<th>' . getFactoringFieldTitle("buchungsbetrag_sk_index_6_2", "GS") . '</th>';
							echo '<th>' . getFactoringFieldTitle("buchungsbetrag_sk_index_6_3", "GS") . '</th>';
							echo '<th>' . getFactoringFieldTitle("buchungsbetrag_sk_index_6_4", "GS") . '</th>';
							echo '<th>' . getFactoringFieldTitle("buchungsbetrag_sk_index_6_5", "GS") . '</th>';

							echo '<th>' . getFactoringFieldTitle("buchungsbetrag_sonst_hw_kre", "GS") . '</th>';

							echo '<th class="spacer"></th>';
							echo '<th>' . getFactoringFieldTitle("summe_hw_kre", "GS") . '</th>';


							echo '</tr>';

							echo '</thead>';

							echo '<tbody>';

							$countRow = 0;

							$marker = '';

							$arrTotalSums = array();
							$arrTotalSums["rechnung_betrag"]				= 0;
							$arrTotalSums["gutschrift_betrag"]				= 0;
							$arrTotalSums["buchungsbetrag_sk_index_6_1"]	= 0;
							$arrTotalSums["buchungsbetrag_sk_index_6_2"]	= 0;
							$arrTotalSums["buchungsbetrag_sk_index_6_3"]	= 0;
							$arrTotalSums["buchungsbetrag_sk_index_6_4"]	= 0;
							$arrTotalSums["buchungsbetrag_sk_index_6_5"]	= 0;
							$arrTotalSums["buchungsbetrag_sonst_hw_kre"]	= 0;
							$arrTotalSums["summe_hw_kre"]					= 0;

							while($ds_getSelectedDatas = mysqli_fetch_assoc($rs_getSelectedDatas)){
								$rowStyle = '';
								if($marker != $ds_getSelectedDatas["gutschrift_nr"]){
									$marker = $ds_getSelectedDatas["gutschrift_nr"];
									$rowStyle = 'style="border-top:2px solid #000;"';
								}

								$rowClass = 'row0';
								if($countRow%2 == 0) {
									$rowClass = 'row1';
								}

								$arrTotalSums["rechnung_betrag"]				+= $ds_getSelectedDatas["rechnung_betrag"];
								$arrTotalSums["gutschrift_betrag"]				+= $ds_getSelectedDatas["gutschrift_betrag"];
								$arrTotalSums["buchungsbetrag_sk_index_6_1"]	+= $ds_getSelectedDatas["buchungsbetrag_sk_index_6_1"];
								$arrTotalSums["buchungsbetrag_sk_index_6_2"]	+= $ds_getSelectedDatas["buchungsbetrag_sk_index_6_2"];
								$arrTotalSums["buchungsbetrag_sk_index_6_3"]	+= $ds_getSelectedDatas["buchungsbetrag_sk_index_6_3"];
								$arrTotalSums["buchungsbetrag_sk_index_6_4"]	+= $ds_getSelectedDatas["buchungsbetrag_sk_index_6_4"];
								$arrTotalSums["buchungsbetrag_sk_index_6_5"]	+= $ds_getSelectedDatas["buchungsbetrag_sk_index_6_5"];
								$arrTotalSums["buchungsbetrag_sonst_hw_kre"]	+= $ds_getSelectedDatas["buchungsbetrag_sonst_hw_kre"];
								$arrTotalSums["summe_hw_kre"]					+= $ds_getSelectedDatas["summe_hw_kre"];

								echo '<tr class="' . $rowClass . '" ' . $rowStyle . '>';

								echo '<td style="text-align:right;">';
								echo '<b>' . ($countRow + 1) . '.</b>';
								echo '</td>';

								echo '<td style="white-space:nowrap;">';
								echo '<span title="' . $ds_getSelectedDatas["importFilename"] . '" style="cursor:pointer;">';
								echo '<b>' . $ds_getSelectedDatas["az_nummer"] . '</b>';
								echo ' - ' . $ds_getSelectedDatas["importFiletype"] . '';
								echo '</span>';
								echo '</td>';

								#echo '<td>';
								#echo $ds_getSelectedDatas["importFilename"];
								#echo '</td>';

								#echo '<td>';
								#echo formatDate($ds_getSelectedDatas["importDate"], 'display');
								#echo '</td>';

								echo '<td style="font-weight:bold;">';
								echo '<a href="' . PAGE_EDIT_CUSTOMER . '?searchCustomerNumber=' . $ds_getSelectedDatas["deb_id_vom_kreditor"] . '" target="_blank">' . $ds_getSelectedDatas["deb_id_vom_kreditor"] . '</a>';
								echo '</td>';

								echo '<td style="font-style:italic;background-color:#FEFFAF;">';
								echo $ds_getSelectedDatas["customersTaxAccountID"];
								echo '</td>';

								echo '<td style="font-weight:bold;">';
								echo $ds_getSelectedDatas["customersFirmenname"];
								echo '</td>';

								echo '<td class="spacer"></td>';

								echo '<td>';
								echo '<span style="cursor:pointer;" class="trans_kz" title="' . $arrTransactionsTypes[$ds_getSelectedDatas["trans_kz"]] . '">' . $ds_getSelectedDatas["trans_kz"] . '</span>';
								echo '</td>';

								echo '<td style="font-weight:bold;">';
								$thisDocumentNumber = $ds_getSelectedDatas["gutschrift_nr"];
								$thisDocumentsPage = PAGE_ALL_INVOICES;
								if(substr($thisDocumentNumber, 0, 2) == 'GU'){
									$thisDocumentsPage = PAGE_ALL_CREDITS;
								}
								echo '<a href="' . $thisDocumentsPage . '?searchDocumentNumber=' . $thisDocumentNumber . '" target="_blank">';
								echo $thisDocumentNumber;
								echo '</a>';
								echo '</td>';

								echo '<td>';
								echo substr(formatDate($ds_getSelectedDatas["gutschrift_datum"], 'display'), 0, 10);
								echo '</td>';

								echo '<td style="text-align:right;white-space:nowrap;">';
								echo formatDecimal($ds_getSelectedDatas["gutschrift_betrag"]) . ' &euro;';
								echo '</td>';

								echo '<td class="spacer"></td>';

								echo '<td style="font-weight:bold;">';
								if($ds_getSelectedDatas["rechnung_nr"] != ""){
									$thisDocumentNumber = $ds_getSelectedDatas["rechnung_nr"];
									$thisDocumentsPage = PAGE_ALL_INVOICES;
									if(substr($thisDocumentNumber, 0, 2) == 'GU'){
										$thisDocumentsPage = PAGE_ALL_CREDITS;
									}
									echo '<a href="' . $thisDocumentsPage . '?searchDocumentNumber=' . $thisDocumentNumber . '" target="_blank">';
									echo $thisDocumentNumber;
									echo '</a>';
								}
								echo '</td>';

								echo '<td>';
								if($ds_getSelectedDatas["rechnung_nr"] != ""){
									echo substr(formatDate($ds_getSelectedDatas["rechnung_datum"], 'display'), 0, 10);
								}
								echo '</td>';

								echo '<td style="text-align:right;white-space:nowrap;">';
								if($ds_getSelectedDatas["rechnung_nr"] != ""){
									echo formatDecimal($ds_getSelectedDatas["rechnung_betrag"]) . ' &euro;';
								}
								echo '</td>';

								echo '<td class="spacer"></td>';

								echo '<td style="text-align:right;white-space:nowrap;">';
								echo formatDecimal($ds_getSelectedDatas["buchungsbetrag_sk_index_6_1"]) . ' &euro;';
								echo '</td>';

								echo '<td style="text-align:right;white-space:nowrap;">';
								echo formatDecimal($ds_getSelectedDatas["buchungsbetrag_sk_index_6_2"]) . ' &euro;';
								echo '</td>';

								echo '<td style="text-align:right;white-space:nowrap;">';
								echo formatDecimal($ds_getSelectedDatas["buchungsbetrag_sk_index_6_3"]) . ' &euro;';
								echo '</td>';

								echo '<td style="text-align:right;white-space:nowrap;">';
								echo formatDecimal($ds_getSelectedDatas["buchungsbetrag_sk_index_6_4"]) . ' &euro;';
								echo '</td>';

								echo '<td style="text-align:right;white-space:nowrap;">';
								echo formatDecimal($ds_getSelectedDatas["buchungsbetrag_sk_index_6_5"]) . ' &euro;';
								echo '</td>';

								echo '<td style="text-align:right;white-space:nowrap;">';
								echo formatDecimal($ds_getSelectedDatas["buchungsbetrag_sonst_hw_kre"]) . ' &euro;';
								echo '</td>';

								echo '<td class="spacer"></td>';

								echo '<td style="text-align:right;white-space:nowrap;">';
								echo formatDecimal($ds_getSelectedDatas["summe_hw_kre"]) . ' &euro;';
								echo '</td>';


								echo '</tr>';

								$countRow = 0;
							}

							echo '<tr>';
							echo '<td colspan="23"><hr /></td>';
							echo '</tr>';

							echo '<tr class="row2" style="font-weight:bold;">';
							echo '<td colspan="14">Insg.:</td>';

							echo '<td class="spacer"></td>';

							echo '<td style="text-align:right;white-space:nowrap;">';
							echo formatDecimal($arrTotalSums["buchungsbetrag_sk_index_6_1"]) . ' &euro;';
							echo '</td>';

							echo '<td style="text-align:right;white-space:nowrap;">';
							echo formatDecimal($arrTotalSums["buchungsbetrag_sk_index_6_2"]) . ' &euro;';
							echo '</td>';

							echo '<td style="text-align:right;white-space:nowrap;">';
							echo formatDecimal($arrTotalSums["buchungsbetrag_sk_index_6_3"]) . ' &euro;';
							echo '</td>';

							echo '<td style="text-align:right;white-space:nowrap;">';
							echo formatDecimal($arrTotalSums["buchungsbetrag_sk_index_6_4"]) . ' &euro;';
							echo '</td>';

							echo '<td style="text-align:right;white-space:nowrap;">';
							echo formatDecimal($arrTotalSums["buchungsbetrag_sk_index_6_5"]) . ' &euro;';
							echo '</td>';

							echo '<td style="text-align:right;white-space:nowrap;">';
							echo formatDecimal($arrTotalSums["buchungsbetrag_sonst_hw_kre"]) . ' &euro;';
							echo '</td>';

							echo '<td class="spacer"></td>';


							echo '<td style="text-align:right;white-space:nowrap;">';
							echo formatDecimal($arrTotalSums["summe_hw_kre"]) . ' &euro;';
							echo '</td>';

							echo '</tr>';

							echo '</tbody>';
							echo '</table>';
						}
						else if($importFactoringFileType == 'ZA'){
							
							echo '<form name="formStoreImportedFactoringFile" method="post" action="' . $_SERVER["PHP_SELF"] . '" enctype="multipart/form-data" >';
							echo '<input type="hidden" name="reSearchImportedFactoringFile" value="' . $_REQUEST["searchImportedFactoringFile"] . '" />';
							echo '<input type="hidden" name="reFilterFactoringFileTypes" value="' . $_REQUEST["filterFactoringFileTypes"] . '" />';
							echo '<input type="hidden" name="reFilterFactoringFileDate" value="' . $_REQUEST["filterFactoringFileDate"] . '" />';
							echo '<input type="hidden" name="reSubmitSearch" value="' . $_REQUEST["submitSearch"] . '" />';							
							
							echo '<table cellspacing="0" cellpadding="0" width="100%" border="0" class="displayOrders">';

							echo '<colgroup>';
							for($i = 0 ; $i < 21 ; $i++){
								echo '<col />';
							}
							echo '</colgroup>';

							echo '<thead>';

							echo '<tr>';

							echo '<th>#</th>';
							echo '<th>' . getFactoringFieldTitle("az_nummer", "ZA") . '</th>';
							#echo '<th>' . getFactoringFieldTitle("importFilename", "ZA") . '</th>';
							#echo '<th>' . getFactoringFieldTitle("importDate", "ZA") . '</th>';
							echo '<th>' . getFactoringFieldTitle("deb_id_vom_kreditor", "ZA") . '</th>';
							echo '<th>' . getFactoringFieldTitle("FiBu-Nr", "ZA") . '</th>';
							echo '<th>' . getFactoringFieldTitle("Firma", "ZA") . '</th>';

							echo '<th class="spacer"></th>';

							echo '<th>' . getFactoringFieldTitle("beleg_nr", "ZA") . '</th>';
							echo '<th>' . getFactoringFieldTitle("zahlung_nr", "ZA") . '</th>';
							echo '<th>' . getFactoringFieldTitle("zahlung_betrag", "ZA") . '</th>';
							echo '<th>' . getFactoringFieldTitle("zahlung_datum", "ZA") . '</th>';
							echo '<th>' . getFactoringFieldTitle("trans_kz", "ZA") . '</th>';
							echo '<th>' . getFactoringFieldTitle("beleg_betrag", "ZA") . '</th>';

							echo '<th class="spacer"></th>';

							echo '<th>' . getFactoringFieldTitle("buchungsbetrag_sk_index_7_1", "ZA") . '</th>';
							echo '<th>' . getFactoringFieldTitle("buchungsbetrag_sk_index_7_2", "ZA") . '</th>';
							echo '<th>' . getFactoringFieldTitle("buchungsbetrag_sk_index_7_3", "ZA") . '</th>';
							echo '<th>' . getFactoringFieldTitle("buchungsbetrag_sk_index_7_4", "ZA") . '</th>';
							echo '<th>' . getFactoringFieldTitle("buchungsbetrag_sk_index_7_5", "ZA") . '</th>';
							echo '<th>' . getFactoringFieldTitle("buchungsbetrag_sonst_hw_kre", "ZA") . '</th>';

							echo '<th class="spacer"></th>';

							echo '<th>' . getFactoringFieldTitle("summe_hw_kre", "ZA") . '</th>';

							echo '</tr>';
							echo '</thead>';

							echo '<tbody>';

							$countRow = 0;

							$marker = '';

							$arrTotalSums = array();
							$arrTotalSums["zahlung_betrag"]					= 0;
							$arrTotalSums["beleg_betrag"]					= 0;
							$arrTotalSums["buchungsbetrag_sk_index_7_1"]	= 0;
							$arrTotalSums["buchungsbetrag_sk_index_7_2"]	= 0;
							$arrTotalSums["buchungsbetrag_sk_index_7_3"]	= 0;
							$arrTotalSums["buchungsbetrag_sk_index_7_4"]	= 0;
							$arrTotalSums["buchungsbetrag_sk_index_7_5"]	= 0;
							$arrTotalSums["buchungsbetrag_sonst_hw_kre"]	= 0;
							$arrTotalSums["summe_hw_kre"]					= 0;

							while($ds_getSelectedDatas = mysqli_fetch_assoc($rs_getSelectedDatas)){

								$rowStyle = '';
								if($marker != $ds_getSelectedDatas["beleg_nr"]){
									$marker = $ds_getSelectedDatas["beleg_nr"];
									$rowStyle = 'style="border-top:2px solid #000;"';
								}

								$rowClass = 'row0';
								if($countRow%2 == 0) {
									$rowClass = 'row1';
								}
								
								if($ds_getSelectedDatas["beleg_done"] == '1'){
									$rowClass = 'row2';
								}

								$arrTotalSums["zahlung_betrag"]					+= $ds_getSelectedDatas["zahlung_betrag"];
								$arrTotalSums["beleg_betrag"]					+= $ds_getSelectedDatas["beleg_betrag"];
								$arrTotalSums["buchungsbetrag_sk_index_7_1"]	+= $ds_getSelectedDatas["buchungsbetrag_sk_index_7_1"];
								$arrTotalSums["buchungsbetrag_sk_index_7_2"]	+= $ds_getSelectedDatas["buchungsbetrag_sk_index_7_2"];
								$arrTotalSums["buchungsbetrag_sk_index_7_3"]	+= $ds_getSelectedDatas["buchungsbetrag_sk_index_7_3"];
								$arrTotalSums["buchungsbetrag_sk_index_7_4"]	+= $ds_getSelectedDatas["buchungsbetrag_sk_index_7_4"];
								$arrTotalSums["buchungsbetrag_sk_index_7_5"]	+= $ds_getSelectedDatas["buchungsbetrag_sk_index_7_5"];
								$arrTotalSums["buchungsbetrag_sonst_hw_kre"]	+= $ds_getSelectedDatas["buchungsbetrag_sonst_hw_kre"];
								$arrTotalSums["summe_hw_kre"]					+= $ds_getSelectedDatas["summe_hw_kre"];

								echo '<tr class="' . $rowClass . '" ' . $rowStyle . '>';

								echo '<td style="text-align:right;" title="' . $ds_getSelectedDatas["importID"] . '">';
								echo '<b>' . ($countRow + 1) . '.</b>';
								echo '</td>';

								echo '<td style="white-space:nowrap;">';
								echo '<span title="' . $ds_getSelectedDatas["importFilename"] . '" style="cursor:pointer;">';
								echo '<b>' . $ds_getSelectedDatas["az_nummer"] . '</b>';
								echo ' - ' . $ds_getSelectedDatas["importFiletype"] . '';
								echo '</span>';
								echo '</td>';

								#echo '<td>';
								#echo $ds_getSelectedDatas["importFilename"];
								#echo '</td>';

								#echo '<td>';
								#echo formatDate($ds_getSelectedDatas["importDate"], 'display');
								#echo '</td>';

								echo '<td style="font-weight:bold;">';
								
								if(($ds_getSelectedDatas["deb_id_vom_kreditor"] == '' || $ds_getSelectedDatas["deb_id_vom_kreditor"] == '999999')){											
									echo '<input type="text" name="changeImportData[' . $ds_getSelectedDatas["importID"] . '][new_deb_id_vom_kreditor]" class="inputField_60" title="Kunden-Nr eintragen" value="' . $ds_getSelectedDatas["deb_id_vom_kreditor"] . ' "/>';											
								}
								else {
									echo '<a href="' . PAGE_EDIT_CUSTOMER . '?searchCustomerNumber=' . $ds_getSelectedDatas["deb_id_vom_kreditor"] . '" target="_blank">' . $ds_getSelectedDatas["deb_id_vom_kreditor"] . '</a>';
								}
								echo '</td>';

								echo '<td style="font-style:italic;background-color:#FEFFAF;">';
								if($ds_getSelectedDatas["customersTaxAccountID"] == ''){
									echo '';
								}
								else {
									echo $ds_getSelectedDatas["customersTaxAccountID"];
								}
								echo '</td>';

								echo '<td style="font-weight:bold;">';
								echo htmlentities($ds_getSelectedDatas["customersFirmenname"]);
								echo '</td>';

								echo '<td class="spacer"></td>';

								echo '<td style="font-weight:bold;white-space:nowrap;">';
								
								
								if($ds_getSelectedDatas["beleg_nr"] == ''){											
									echo '<input type="text" name="changeImportData[' . $ds_getSelectedDatas["importID"] . '][new_beleg_nr]" class="inputField_100" value="' . $ds_getSelectedDatas["beleg_nr"] . ' "/>';									
								}
								else {
									$thisDocumentNumber = $ds_getSelectedDatas["beleg_nr"];
									$thisDocumentsPage = PAGE_ALL_INVOICES;
									if(substr($thisDocumentNumber, 0, 2) == 'GU'){
										$thisDocumentsPage = PAGE_ALL_CREDITS;
									}
									echo '<a href="' . $thisDocumentsPage . '?searchDocumentNumber=' . $thisDocumentNumber . '" target="_blank">';
									echo $thisDocumentNumber;
									echo '</a>';
								}								
								
								$checked = '';
								if($ds_getSelectedDatas["beleg_done"] == '1'){
									$checked = ' checked="checked" ';
								}
								echo '<input type="checkbox" name="changeImportData[' . $ds_getSelectedDatas["importID"] . '][new_beleg_done]" title="Beleg als bearbeitet markieren" value="1" ' . $checked . ' />';
								echo '<input type="hidden" name="changeImportData[' . $ds_getSelectedDatas["importID"] . '][import_beleg_id]" value="' . $ds_getSelectedDatas["importID"] . '" />';
								echo '<input type="image" class="buttonSaveStatus" src="layout/icons/iconSave.png" width="16" height="16" title="Daten speichern" alt="Speichern" />';
																
								echo '</td>';

								echo '<td style="font-weight:bold;">';
								echo $ds_getSelectedDatas["zahlung_nr"];
								echo '</td>';

								echo '<td style="text-align:right;white-space:nowrap;background-color:#FEFFAF;">';
								echo formatDecimal($ds_getSelectedDatas["zahlung_betrag"]) . ' &euro;';
								echo '</td>';

								echo '<td>';
								echo substr(formatDate($ds_getSelectedDatas["zahlung_datum"], 'display'), 0, 10);
								echo '</td>';

								echo '<td>';
								echo '<span style="cursor:pointer;" class="trans_kz" title="' . $arrTransactionsTypes[$ds_getSelectedDatas["trans_kz"]] . '">' . $ds_getSelectedDatas["trans_kz"] . '</span>';
								echo '</td>';

								echo '<td style="text-align:right;white-space:nowrap;background-color:#FEFFAF;">';
								echo formatDecimal($ds_getSelectedDatas["beleg_betrag"]) . ' &euro;';
								echo '</td>';


								echo '<td class="spacer"></td>';

								echo '<td style="text-align:right;white-space:nowrap;">';
								echo formatDecimal($ds_getSelectedDatas["buchungsbetrag_sk_index_7_1"]) . ' &euro;';
								echo '</td>';

								echo '<td style="text-align:right;white-space:nowrap;">';
								echo formatDecimal($ds_getSelectedDatas["buchungsbetrag_sk_index_7_2"]) . ' &euro;';
								echo '</td>';

								echo '<td style="text-align:right;white-space:nowrap;">';
								echo formatDecimal($ds_getSelectedDatas["buchungsbetrag_sk_index_7_3"]) . ' &euro;';
								echo '</td>';

								echo '<td style="text-align:right;white-space:nowrap;">';
								echo formatDecimal($ds_getSelectedDatas["buchungsbetrag_sk_index_7_4"]) . ' &euro;';
								echo '</td>';

								echo '<td style="text-align:right;white-space:nowrap;">';
								echo formatDecimal($ds_getSelectedDatas["buchungsbetrag_sonst_hw_kre"]) . ' &euro;';
								echo '</td>';

								echo '<td style="text-align:right;white-space:nowrap;">';
								echo formatDecimal($ds_getSelectedDatas["buchungsbetrag_sk_index_7_5"]) . ' &euro;';
								echo '</td>';

								echo '<td class="spacer"></td>';

								echo '<td style="text-align:right;white-space:nowrap;background-color:#FEFFAF;">';
								echo formatDecimal($ds_getSelectedDatas["summe_hw_kre"]) . ' &euro;';
								echo '</td>';

								echo '</tr>';

								$countRow++;
							}

							echo '<tr>';
							echo '<td colspan="21"><hr /></td>';
							echo '</tr>';

							echo '<tr class="row2" style="font-weight:bold;">';
							echo '<td colspan="11">Insg.:</td>';

							echo '<td style="text-align:right;white-space:nowrap;">';
							echo formatDecimal($arrTotalSums["beleg_betrag"]) . ' &euro;';
							echo '</td>';

							echo '<td class="spacer"></td>';

							echo '<td style="text-align:right;white-space:nowrap;">';
							echo formatDecimal($arrTotalSums["buchungsbetrag_sk_index_7_1"]) . ' &euro;';
							echo '</td>';

							echo '<td style="text-align:right;white-space:nowrap;">';
							echo formatDecimal($arrTotalSums["buchungsbetrag_sk_index_7_2"]) . ' &euro;';
							echo '</td>';

							echo '<td style="text-align:right;white-space:nowrap;">';
							echo formatDecimal($arrTotalSums["buchungsbetrag_sk_index_7_3"]) . ' &euro;';
							echo '</td>';

							echo '<td style="text-align:right;white-space:nowrap;">';
							echo formatDecimal($arrTotalSums["buchungsbetrag_sk_index_7_4"]) . ' &euro;';
							echo '</td>';

							echo '<td style="text-align:right;white-space:nowrap;">';
							echo formatDecimal($arrTotalSums["buchungsbetrag_sonst_hw_kre"]) . ' &euro;';
							echo '</td>';

							echo '<td style="text-align:right;white-space:nowrap;">';
							echo formatDecimal($arrTotalSums["buchungsbetrag_sk_index_7_5"]) . ' &euro;';
							echo '</td>';

							echo '<td class="spacer"></td>';

							echo '<td style="text-align:right;white-space:nowrap;">';
							echo formatDecimal($arrTotalSums["summe_hw_kre"]) . ' &euro;';
							echo '</td>';

							echo '</tr>';

							echo '</tbody>';

							echo '</table>';
							
							echo '</form>';
						}
					}
					else{
						$infoMessage .= 'Es wurden keine Daten gefunden' . '<br />';
					}
				}
			}
		?>

		<?php displayMessages(); ?>
	</div>
	<div class="clear"></div>
</div>

<script language="javascript" type="text/javascript">
	<!--
	/* <![CDATA[ */
	function createFactoringTransactionsLegend(){
		var legendContentFactoringTransactions = '';
		legendContentFactoringTransactions += ' ';
		legendContentFactoringTransactions += '';

		<?php
			$legendContentFactoringTransactions = '';
			if(!empty($arrTransactionsTypes)){
				$legendContentFactoringTransactions .= '<div class="buttonLegendTransActionTypes" title="Legende Transaktionsarten anzeigen" style="padding:0;margin: 0 0 6px 0;cursor:help;line-height:20px;" alt="Legende Transaktionsarten" >';
				$legendContentFactoringTransactions .= '<img src="layout/icons/iconInfo.png" width="16" height="16"/> Transaktionsarten anzeigen';
				$legendContentFactoringTransactions .= '</div>';
				$legendContentFactoringTransactions .= '<div class="displayNoticeArea legendTransActionTypes">';
					#$legendContentFactoringTransactions .= '<h3>Legende Transaktionsarten:</h3>';
					$legendContentFactoringTransactions .= '<table border="1" cellpadding="0" cellspacing="0">';
					$legendContentFactoringTransactions .= '<thead>';
					$legendContentFactoringTransactions .= '<tr>';
						$legendContentFactoringTransactions .= '<th>Abk.</th>';
						$legendContentFactoringTransactions .= '<th>Bedeutung</th>';
					$legendContentFactoringTransactions .= '</tr>';
					$legendContentFactoringTransactions .= '</thead>';

					$legendContentFactoringTransactions .= '<tbody>';
					$countRow = 0;

					foreach($arrTransactionsTypes as $thisTransactionsTypesKey => $thisTransactionsTypesValue){
						$rowClass = 'row0';
						if($countRow%2 == 0) {
							$rowClass = 'row1';
						}

						$legendContentFactoringTransactions .= '<tr class="' . $rowClass . '" id="' . $thisTransactionsTypesKey . '">';
						$legendContentFactoringTransactions .= '<td>' . $thisTransactionsTypesKey . '</td>';
						$legendContentFactoringTransactions .= '<td>' . $thisTransactionsTypesValue . '</td>';
						$legendContentFactoringTransactions .= '</tr>';

						$countRow++;
					}
					$legendContentFactoringTransactions .= '</tbody>';
					$legendContentFactoringTransactions .= '</table>';
				$legendContentFactoringTransactions .= '</div>';
			}
			$legendContentFactoringTransactions = removeUnnecessaryChars($legendContentFactoringTransactions);
			$legendContentFactoringTransactions = preg_replace("/<\//", "<\/", $legendContentFactoringTransactions);
		?>
		legendContentFactoringTransactions += '<?php echo $legendContentFactoringTransactions; ?>';

		return legendContentFactoringTransactions;
	}
	$(document).ready(function() {
		$('.buttonNotice').click(function () {
			loadNotice($(this));
		});
		colorRowMouseOver('.displayOrders tbody tr');

		$('.trans_kz').live('dblclick', function(){
			var trans_kz = $(this).html();
			$('#loadNoticeArea').find('tbody tr').css('background-color', '');
			$('#loadNoticeArea').find('#' + trans_kz).css('background-color', '#C3F7B2');
		});

		// BOF TRANSAKTION LAYER
			var legendContentFactoringTransactions = createFactoringTransactionsLegend();
			$('.buttonTransactionsLegend').append(legendContentFactoringTransactions);

			$('.buttonLegendTransActionTypes').live('click', function (){
				loadNotice($(this));
			});
		// EOF TRANSAKTION LAYER

		// BOF FILE FILTER
			function filterFactoringFilesNew(){
				var selectedFilterFactoringFileTypeValue = $('input[name="filterFactoringFileTypes"]:checked').val();
				var selectedFilterFactoringFileDateValue = $('#filterFactoringFileDate').val();
				var selectedFilterFactoringFileNoDocNrValue = $('#filterFactoringFileNoDocNr:checked').val();
				if(selectedFilterFactoringFileNoDocNrValue != 'NoDocNr'){
					selectedFilterFactoringFileNoDocNrValue = '';
				}

				if(selectedFilterFactoringFileTypeValue == '' && selectedFilterFactoringFileDateValue == '' && selectedFilterFactoringFileNoDocNrValue == ''){
					$('#searchImportedFactoringFile option').show();
				}
				else {
					var classSelector = '';
					/*
					if(selectedFilterFactoringFileTypeValue != '' && selectedFilterFactoringFileDateValue != ''){
						classSelector = '.' + selectedFilterFactoringFileTypeValue + '.' + selectedFilterFactoringFileDateValue;
					}
					else if(selectedFilterFactoringFileTypeValue != '' && selectedFilterFactoringFileDateValue == ''){
						classSelector = '.' + selectedFilterFactoringFileTypeValue;
					}
					else if(selectedFilterFactoringFileTypeValue == '' && selectedFilterFactoringFileDateValue != ''){
						classSelector = '.' + selectedFilterFactoringFileDateValue;
					}
					*/

					if(selectedFilterFactoringFileTypeValue != ''){
						classSelector += '.' + selectedFilterFactoringFileTypeValue;
					}
					if(selectedFilterFactoringFileDateValue != ''){
						classSelector += '.' + selectedFilterFactoringFileDateValue;
					}
					if(selectedFilterFactoringFileNoDocNrValue != ''){
						classSelector += '.' + selectedFilterFactoringFileNoDocNrValue;
					}
					// alert(classSelector);
					$('#searchImportedFactoringFile option').hide();
					$('#searchImportedFactoringFile option' + classSelector).show();
				}
			}

			<?php if(isset($_REQUEST["filterFactoringFileTypes"]) || isset($_REQUEST["filterFactoringFileDate"]) || isset($_REQUEST["selectedFilterFactoringFileNoDocNrValue"])) { ?>
			filterFactoringFilesNew();
			<?php } ?>

			$('input[name="filterFactoringFileTypes"]').live('click', function(){
				filterFactoringFilesNew();
			});
		// EOF FILE FILTER

		// BOF DATE FILTER
			$('#filterFactoringFileDate').live('change', function(){
				filterFactoringFilesNew();
			});
		// EOF DATE FILTER

		// BOF NO DOCUMENT NUMBER FILTER
			$('#filterFactoringFileNoDocNr').live('change', function(){
				filterFactoringFilesNew();
			});
		// EOF NO DOCUMENT NUMBER FILTER

		// BOF MARK ACTIVE ROWS
			$('table.displayOrders td').find('a').attr('class', 'markActiveRow');
			$('.markActiveRow').live('click', function(){
				$('table.displayOrders td').removeClass('activeRow');
				$(this).parent().removeClass('activeRow');
				$(this).parent().parent().find('td:first').attr('class', 'activeRow');
				$(this).parent().attr('class', 'activeRow');
			});
		// EOF MARK ACTIVE ROWS
		
		$('.buttonToggleSidebarMenue').click(function() {
			$('#menueSidebarToggleContent').toggle();
		});
		
		<?php if($_COOKIE["isAdmin"] == '1'){ ?>	
		// BOF LOAD TAX ACCOUNT ID
			$('input:text[name*="new_deb_id_vom_kreditor"]').live('keyup', function () {
				
			});
		// EOF LOAD TAX ACCOUNT ID
		<?php } ?>
		
	});
	/* ]]> */
	// -->
</script>

<?php require_once('inc/footerHTML.inc.php'); ?>